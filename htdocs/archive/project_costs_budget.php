<?php
/********************************************************************

    project_costs_budget.php

    View or edit the costs of a project

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-09
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2014, OMEGA SA, All Rights Reserved.

*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/project_cost_functions.php";
require_once "include/save_functions.php";
require_once "../shared/func_eventmails.php";

if (has_access("can_view_project_costs") 
   or has_access("can_view_budget_in_projects")
   or has_access('can_edit_project_costs_budget')
   )
{  
}
else {
	redirect("noaccess.php");
}

if(!param("pid"))
{
	$link = "welcome.php";
	redirect($link);
}

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));
// get company's address
$client_address = get_address($project["order_client_address"]);

$currency_symbol = get_currency_symbol($project["order_client_currency"]);
$order_currency = get_order_currency($project["project_order"]);

/********************************************************************
    check if project cost positions are present and create if not
*********************************************************************/

$sql = "select count(costsheet_id) as num_recs from costsheets " . 
       " where costsheet_version = 0
			and costsheet_project_id = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

if($row["num_recs"] == 0) //make the user select a cost template
{
	$link = "project_costs_overview.php?pid=" . param("pid");
	redirect($link);
}


/********************************************************************
	get budget data from cost sheet
*********************************************************************/
$budget = get_project_budget_totals(param("pid"), $order_currency);

$code_data = array();
$budget_data = array();
$currency_data = array();
$currency_data_chf = array();
$text_data = array();
$comment_data = array();
$amounts_chf = array();
$list_has_positions = array();
$costsheet_partner_contribution = array();
$costsheet_currencies = array();



$sql = "select costsheet_id, costsheet_pcost_group_id, costsheet_currency_id, currency_symbol, " . 
       " costsheet_exchangerate, currency_factor, costsheet_code, costsheet_text, " . 
       "costsheet_budget_amount, costsheet_partner_contribution, costsheet_comment, costsheet_company " . 
	   "from costsheets " .
	   " left join currencies on currency_id = costsheet_currency_id " . 
	   "where costsheet_version = 0
	        and (costsheet_budget_amount > 0 or costsheet_real_amount > 0) 
			and costsheet_project_id = " . param("pid");


$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$code_data[$row["costsheet_id"]] = $row["costsheet_code"];
	$budget_data[$row["costsheet_id"]] = $row["costsheet_budget_amount"];
	$currency_data[$row["costsheet_id"]] = $row["currency_symbol"];
	$text_data[$row["costsheet_id"]] = $row["costsheet_text"];
	$comment_data[$row["costsheet_id"]] = $row["costsheet_comment"];
	$company_data[$row["costsheet_id"]] = $row["costsheet_company"];

	$list_has_positions[$row["costsheet_pcost_group_id"]] = true;

	$costsheet_partner_contribution[$row["costsheet_id"]] = $row["costsheet_partner_contribution"];

	$costsheet_currencies[$row["costsheet_id"]] = $row["costsheet_currency_id"];

	$amounts_chf[$row["costsheet_id"]] = round(($row["costsheet_exchangerate"] * $row["costsheet_budget_amount"])/$row["currency_factor"], 2);

	$amounts_chf[$row["costsheet_id"]] = number_format($amounts_chf[$row["costsheet_id"]], 2, '.', "'");


	

}


/********************************************************************
	Create Form
*********************************************************************/ 

$form = new Form("projects", "projects");


$form->add_section("Project");
$form->add_hidden("pid", param('pid'));
$form->add_hidden("order_id", $project["project_order"]);


require_once "include/project_head_small.php";


$form->add_section("Currency and Exchange Rate");
			
$form->add_lookup("order_client_currency", "Currency", "currencies", "concat(currency_symbol, ': ', currency_name)", 0, $project["order_client_currency"]);

$form->add_label("order_client_exchange_rate", "Exchange Rate", 0, $project["order_client_exchange_rate"]);

$form->add_section("Business Partner Contribution");
$form->add_label("project_share_other", "Business Partner Contribution in Percent", 0, $project["project_share_other"] . "%");


$link = "javascript:popup('/user/project_costs_budget_pdf.php?pid=" . param("pid") . "', 800, 600);";
$form->add_button("print", "Print Budget in " . $currency_symbol, $link);

$link = "javascript:popup('/user/project_costs_budget_pdf.php?pid=" . param("pid") . "&sc=1', 800, 600);";
$form->add_button("print2", "Print Budget in CHF", $link);


$form->populate();

/********************************************************************
	Compose Cost Sheet
*********************************************************************/ 

//add all cost groups and cost sub groups

$list_names = array();
$group_ids = array();
$group_titles = array();
$sub_group_ids = array();

//get all subgroups
$sql = "select costsheet_id, costsheet_pcost_group_id, costsheet_pcost_subgroup_id,  
	   costsheet_code, costsheet_text, costsheet_comment, 
	   concat(pcost_subgroup_code, ' ', pcost_subgroup_name) as subgroup 
	   from costsheets 
	    left join pcost_subgroups on pcost_subgroup_id = costsheet_pcost_subgroup_id  
        where costsheet_version = 0
		    and (costsheet_budget_amount > 0 or costsheet_real_amount > 0) 
			and costsheet_project_id = " . dbquote(param("pid")) . 
		" and costsheet_is_in_budget = 1";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$sub_group_ids[$row['subgroup']] = $row['costsheet_pcost_subgroup_id'];
}


$sql2 = "select DISTINCT costsheet_pcost_group_id, pcost_group_code, pcost_group_name " .
	   "from costsheets " .
	   "left join pcost_groups on pcost_group_id = costsheet_pcost_group_id " .
	   " where costsheet_version = 0
	        and (costsheet_budget_amount > 0 or costsheet_real_amount > 0) 
			and costsheet_project_id = " . dbquote(param("pid")) . " and costsheet_is_in_budget = 1" .
	   " order by pcost_group_code";

$res = mysql_query($sql2) or dberror($sql2);
while ($row = mysql_fetch_assoc($res))
{	
	
	$listname = "list" . $row["pcost_group_code"];
	$list_names[] = $listname;
	$group_ids[] = $row["costsheet_pcost_group_id"];
	$group_titles[] = $row["pcost_group_name"];
	

	$toggler = '<div class="toggler_pointer" id="l' . $row["costsheet_pcost_group_id"] . '_on"><span class="fa fa-minus-square toggler"></span>' .$row["pcost_group_name"] . '</div>';

	$sql = "select costsheet_id, costsheet_pcost_group_id, costsheet_pcost_subgroup_id, " . 
		   "costsheet_code, costsheet_text, costsheet_comment, " .
		   "concat(pcost_subgroup_code, ' ', pcost_subgroup_name) as subgroup  " .
		   "from costsheets " .
		   " left join pcost_subgroups on pcost_subgroup_id = costsheet_pcost_subgroup_id"; 

	$list_filter = "costsheet_version = 0
	        and (costsheet_budget_amount > 0 or costsheet_real_amount > 0) 
			and costsheet_project_id = " . param("pid") . " and costsheet_pcost_group_id = " . $row["costsheet_pcost_group_id"] . " and costsheet_is_in_budget = 1";
	
	
	
	//compose list
	$$listname = new ListView($sql, LIST_HAS_HEADER | LIST_HAS_FOOTER);

	$$listname->set_entity("costsheets");
	$$listname->set_order("LENGTH(costsheet_code), COALESCE(costsheet_code,'Z')");
	$$listname->set_group("subgroup");
	$$listname->set_filter($list_filter);
	$$listname->set_title($toggler);

	
	$$listname->add_text_column("costsheet_code", "Code", 0, $code_data);
	$$listname->add_text_column("costsheet_text", "Text", 0, $text_data);
	$$listname->add_text_column("costsheet_company", "Supplier", 0, $company_data);
	$$listname->add_text_column("costsheet_budget_amount", "Budget", 0, $budget_data);
	$$listname->add_text_column("currency", "", 0, $currency_data);
	$$listname->add_text_column("chf", "CHF", 0, $amounts_chf);
	$$listname->add_text_column("costsheet_partner_contribution", "% paid by<br />Business Partner", COLUMN_UNDERSTAND_HTML, $costsheet_partner_contribution);	
	$$listname->add_text_column("costsheet_comment", "Comment", 0, $comment_data);
	
	if(array_key_exists($row["costsheet_pcost_group_id"], $list_has_positions))
	{
		foreach($budget["subgroup_totals"] as $subgroup=>$budget_sub_group_total)
		{
			
			if(array_key_exists($subgroup, $sub_group_ids))
			{
				$$listname->set_group_footer("costsheet_text", $subgroup , "Subgroup Total " . $currency_symbol . "/CHF");
				$$listname->set_group_footer("costsheet_budget_amount",  $subgroup , number_format($budget_sub_group_total, 2, ".", "'"), 'sgt' . $sub_group_ids[$subgroup]);
			}
		}

		foreach($budget["subgroup_totals_chf"] as $subgroup=>$budget_sub_group_total)
		{
		
			if(array_key_exists($subgroup, $sub_group_ids))
			{
				$$listname->set_group_footer("chf",  $subgroup , number_format($budget_sub_group_total, 2, ".", "'"), 'sgt_chf' . $sub_group_ids[$subgroup]);
			}
		}

		$$listname->set_footer("costsheet_code", "Total " . $currency_symbol . "/CHF");
		$$listname->set_footer("costsheet_budget_amount", number_format($budget["group_totals"][$row["costsheet_pcost_group_id"]], 2, ".", "'"), 'gt' . $row["costsheet_pcost_group_id"]);

		$$listname->set_footer("chf", number_format($budget["group_totals_chf"][$row["costsheet_pcost_group_id"]], 2, ".", "'"), 'gt_chf' . $row["costsheet_pcost_group_id"]);
	}

	$$listname->populate();
	$$listname->process();
}


$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Project Costs - Budget");

require_once("include/costsheet_tabs.php");
$form->render();



foreach($list_names as $key=>$listname)
{
		
	if(array_key_exists("costsheet", $_SESSION) and array_key_exists($listname, $_SESSION["costsheet"]))
	{
		
		if($_SESSION["costsheet"][$listname] == 0)
		{
			echo '<p>&nbsp;</p>';
			$toggler = '<div class="toggler_pointer" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
			echo $toggler;
			
			echo '<div style="display:none;" id="' . $listname . '">';
			$$listname->render();
			echo '</div>';
		}
		else
		{
			echo '<p>&nbsp;</p>';
			$toggler = '<div class="toggler_pointer toggler_pointer_off" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
			echo $toggler;
			
			echo '<div id="' . $listname . '">';
			$$listname->render();
			echo '</div>';
		}
	}
	else
	{
		echo '<p>&nbsp;</p>';
		$toggler = '<div class="toggler_pointer toggler_pointer_off" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
		echo $toggler;
		
		echo '<div id="' . $listname . '">';
		$$listname->render();
		echo '</div>';
	}
}


?>
<script language="javascript">

jQuery(document).ready(function($) {
	
	<?php
	foreach($list_names as $key=>$listname)
	{
	?>
		$('#l<?php echo $group_ids[$key];?>_on').click(function()  {
			$('#<?php echo $listname;?>').css('display', 'none');
			$('#l<?php echo $group_ids[$key];?>_off').css('display', 'block');

			$.ajax({
				type: "POST",
				data: "listname=<?php echo $listname;?>&visibility=0",
				url: "../shared/ajx_costsheet_liststaes.php",
				success: function(msg){
				}
			});
		});
		
		$('#l<?php echo $group_ids[$key];?>_off').click(function()  {
			$('#<?php echo $listname;?>').css('display', 'block');
			$('#l<?php echo $group_ids[$key];?>_off').css('display', 'none');

			$.ajax({
				type: "POST",
				data: "listname=<?php echo $listname;?>&visibility=1",
				url: "../shared/ajx_costsheet_liststaes.php",
				success: function(msg){
				}
			});
		});
	<?php
	}
	?>

});



</script>

<?php

$page->footer();

?>