<?php
/********************************************************************

    projects_archive.php

    Archive: Selection of the year.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-11-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-12-03
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/access_filters.php";

check_access("has_access_to_archive");

set_referer("projects_archive_projects.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());


$country_filter = "";
$tmp = array();
$sql = "select * from country_access " .
	   "where country_access_user = " . user_id();


$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{            
	$tmp[] = $row["country_access_country"];
}

if(count($tmp) > 0) {
	$country_filter = " country_id IN (" . implode(",", $tmp) . ") ";
}

$country_access_filter = get_users_regional_access_to_countries(1, user_id());
if($country_access_filter)
{
	if($country_filter)
	{
		$country_filter = "(" . $country_filter .  " or " . $country_access_filter . ")";
	}
	else
	{
		$country_filter = "(" . $country_access_filter . ")";
	}
}



// create sql for product lines
if (has_access("has_access_to_all_projects"))
{
    $sql_years = "select distinct left(order_date, 4) as year " .
				  "from orders ".
				  "where order_type = 1 " .
				  "    and order_archive_date <> 0 ".
				  "    and order_archive_date is not null " .
				  "order by year";


	$sql_product_lines = "select distinct product_line_id, product_line_name ".
                         "from projects ".
                         "left join product_lines on  project_product_line = product_line_id ".
                         "left join orders on project_order = order_id ".
                         "where product_line_name<> '' " . 
		                 "    and order_archive_date is not null ".
                         "    and order_archive_date <> '0000-00-00' " .
                         " order by product_line_name";


	$sql_countries = "select distinct country_id, country_name ".
                     "from projects ".
                     "left join orders on project_order = order_id ".
                     "left join countries on order_shop_address_country = countries.country_id ".
                     "where order_archive_date is not null ".
		             "    and country_name <> '' " . 
                     "    and order_archive_date <> '0000-00-00' " .
		             " and order_type = 1 " . 
                     "order by country_name";

	$sql_pms = "select DISTINCT project_retail_coordinator, " . 
		       "concat(user_name, ' ', user_firstname) as username " . 
		       "from projects " .
		       "left join orders on project_order = order_id ".
		       "left join users on user_id = project_retail_coordinator " . 
		       "where order_archive_date is not null ".
			   "    and order_archive_date <> '0000-00-00' " .
			   " and order_type = 1 " .
		       " and user_name <> '' " . 
			   "order by username";

	
}
else
{
    
	if(has_access("has_access_to_all_travalling_retail_data"))
	{
		$condition = get_user_specific_order_list(user_id(), 1, $user_roles, true);
	}
	else
	{
		$condition = get_user_specific_order_list(user_id(), 1, $user_roles, false);
	}
	
	$sql_product_lines = "select distinct product_line_id, product_line_name ".
						 "from projects ".
						 "left join product_lines on  project_product_line = product_line_id ".
						 "left join orders on project_order = order_id ".
						 "left join project_costs on project_cost_order = order_id " .
						 "left join addresses on order_client_address = address_id ".
						 "left join order_items on order_item_order = order_id ".
						 "left join countries on order_shop_address_country = countries.country_id ".
						 "where product_line_id > 0 " . 
						 "    and order_archive_date is not null ".
						 "    and order_archive_date <> '0000-00-00' ";
	if($condition)
	{
		$sql_product_lines .= " or (" . $condition . ") ";
	}

	
	
	$filter_tmp = " and (order_item_supplier_address = " . $user_data["address"] . " " .
				  "    or order_item_forwarder_address = " . $user_data["address"] . " " .
				  "    or order_retail_operator = " . user_id() . " " .
				  "    or project_retail_coordinator = " . user_id()  . " " .
				  "    or project_design_contractor = " . user_id()  . " " .
				  "    or project_design_supervisor = " . user_id()   . " " .
				  "    or order_client_address = " . $user_data["address"] . ") ";
   
	
	if($country_filter == '')
	{
		$sql_product_lines = $sql_product_lines . $filter_tmp . " order by product_line_name";
	}
	else
	{
		$sql_product_lines = $sql_product_lines . " and " . $country_filter . " order by product_line_name";
	}


	$sql_countries = "select distinct country_id, country_name ".
					 "from projects ".
					 "left join orders on project_order = order_id ".
					 "left join project_costs on project_cost_order = order_id " .
					 "left join countries on order_shop_address_country = countries.country_id ".
					 "left join order_items on order_item_order = order_id ".
					 "where country_name <> '' " .
					 "    and order_archive_date is not null ".
					 "    and order_archive_date <> '0000-00-00' " .
					 " and order_type = 1 ";
	
	

	
	if($country_filter == '')
	{
		$sql_countries = $sql_countries . $filter_tmp;
	}
	else
	{
		$sql_countries = $sql_countries . " and " . $country_filter;
	}



	if($condition)
	{
		$sql_countries .= " or (" . $condition . ") ";
	}


	$sql_countries .= " order by country_name";




	$sql_pms = "select DISTINCT project_retail_coordinator, " . 
			   "concat(user_name, ' ', user_firstname) as username " . 
			   "from projects " .
			   "left join users on user_id = project_retail_coordinator " .
			   "left join orders on project_order = order_id ".
			   "left join project_costs on project_cost_order = order_id " .
			   "left join order_items on order_item_order = order_id ".
			   "left join countries on country_id = order_shop_address_country " . 
			   "where order_archive_date is not null ".
			   "    and order_archive_date <> '0000-00-00' " .
			   " and order_type = 1 " . 
			   " and user_name <> '' ";

	if($country_filter == '')
	{
		$sql_pms = $sql_pms . $filter_tmp;
	}
	else
	{
		$sql_pms = $sql_pms . " and " . $country_filter;
	}


	if($condition)
	{
		$sql_pms .= " or (" . $condition . ") ";
	}

	$sql_pms .= " order by username";


	$sql_years = "select distinct left(order_date, 4) as year " .
			  "from projects ".
			   "left join orders on project_order = order_id ".
		       "left join project_costs on project_cost_order = order_id " .
			   "left join order_items on order_item_order = order_id ".
			   "left join countries on country_id = order_shop_address_country " . 
			  "where order_type = 1 " .
			  "    and order_archive_date <> 0 ".
			  "    and order_archive_date is not null ";
	
	if($country_filter == '')
	{
		$sql_years = $sql_years .  $filter_tmp;
	}
	else
	{
		$sql_years = $sql_years . " and " . $country_filter;
	}



	if($condition)
	{
		$sql_years .= " or (" . $condition . ") ";
	}

	$sql_years .= " order by year";
}


$sql_legal_types = "select project_costtype_id, project_costtype_text from project_costtypes where project_costtype_id in (1,2,6) order by project_costtype_text";

$slq_pos_types = "select postype_id, postype_name from postypes where postype_id in (1,2,3) order by postype_name";



/********************************************************************
    build form
*********************************************************************/
$form = new Form("orders", "orders");

$form->add_edit("search_term", "Search Term");

$form->add_list("legaltype", "Legal Types", $sql_legal_types, 0);
$form->add_list("postype", "POS Types", $slq_pos_types, 0);



$form->add_list("productline", "Product Line", $sql_product_lines, 0);
$form->add_list("country", "Country", $sql_countries, 0);
$form->add_list("pm", "Project Leader", $sql_pms, 0);
$form->add_section(" ");
$form->add_list("from_year", "From Year", $sql_years, 0);
$form->add_list("to_year", "To Year", $sql_years, 0);




$form->add_button("proceed", "Show Projects");



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("proceed"))
{
    $y1 = $form->value("from_year");
    $y2 = $form->value("to_year");
    $p = $form->value("productline");
    $c = $form->value("country");
	$st = $form->value("search_term");
	$pm = $form->value("pm");
	$lt = $form->value("legaltype");
	$pt = $form->value("postype");

    $link = "projects_archive_projects.php?y1=". $y1 . "&y2=" . $y2 . "&p=" . $p . "&c=" . $c . "&st=" . $st . "&pm=" . $pm . "&lt=" . $lt . "&pt=" . $pt;
    redirect($link);
}


   
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

//$page->register_action('orders', 'Orders', "orders_archive.php");
$page->register_action('projects', 'Search Projects', "projects_archive.php");
$page->register_action('welcome', 'Quit Archive', "../user/welcome.php");


$page->header();
$page->title("Projects: Archive");
$form->render();

?>

<script type="text/javascript">
    
	var selectedInput = null;
	$(document).ready(function(){
	  //$("#search_term").focus();

	  $('input').focus(function() {
				selectedInput = this;
				
			});

	});

	document.onkeydown = process_key;
	
	function process_key(e)
	{
	  if( !e ) 
	  {
		if( window.event ) 
		{
		  e = window.event;
		} 
		else 
		{
		  return;
		}
	  }

	  if(selectedInput.name == "search_term" && e.keyCode==13)
	  {
		button('proceed');
	  }
	}
</script>

<?php
$page->footer();

?>