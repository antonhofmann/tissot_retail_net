<?php
/********************************************************************

    project_edit_material_replacements.php

    Edit Replacements of Materials

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2015-06-30
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2015-06-30
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

if(!has_access("can_view_material_replacements") and !has_access("can_edit_material_replacements"))
{
	redirect("noaccess.php");
}


register_param("pid");
set_referer("project_edit_material_replacement.php");
set_referer("project_edit_material_replacement_create_order.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);


$sql_items = "select order_item_replacement_id, " . 
             " if(order_item_replacement_item_id > 0, item_code, 'Special Item') as item_shortcut, " . 
			 " order_item_replacement_item_text, order_item_replacement_quantity, " . 
			 " replacements_payer_name_name, order_number, order_actual_order_state_code, " .
			 " order_item_replacement_catalog_order_id, order_item_arrival, order_archive_date, order_number " . 
             " from order_item_replacements " . 
			 " left join items on item_id = order_item_replacement_item_id " . 
			 " left join order_items on order_item_id = order_item_replacement_order_item_id " . 
			 " left join replacements_payer_names on replacements_payer_name_id = order_item_replacement_item_to_be_payed_by " . 
			 " left join orders on order_id = order_item_replacement_catalog_order_id";

$list_filter = "order_item_replacement_order_id = " . dbquote($project["project_order"]);


//check if we have items to be replaced
$has_items_to_be_replaced = false;

$sql = $sql_items  . " where (order_item_replacement_catalog_order_id = 0 or order_item_replacement_catalog_order_id is null) and " . $list_filter;
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$has_items_to_be_replaced = true;
}

/*
//check if we have item having arrived
$has_items_with_an_arrivale_date = false;
$sql = "select order_item_id " . 
       " from order_items " .
	   " where order_item_order = " . dbquote($project["project_order"]) . 
	   "  and order_item_type <=2 " . 
	   "  and order_item_arrival is not null and order_item_arrival <> '0000-00-00' ";

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$has_items_with_an_arrivale_date = true;
}
*/
$has_items_with_an_arrivale_date = true;


//get links for the list of catalogue orders
$links_for_catalogue_orders = array();
$sql = $sql_items . " where " . $list_filter;
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	if($row["order_archive_date"] == NULL or $row["order_archive_date"] == '0000-00-00')
	{
		$link='<a href="/user/order_edit_material_list.php?oid=' . $row["order_item_replacement_catalog_order_id"] . '" target="_blank">' . $row["order_number"] . '</a>';
		$links_for_catalogue_orders[$row["order_item_replacement_id"]] = $link;
	}
	else
	{
		$link='<a href="/archive/order_view_material_list.php?oid=' . $row["order_item_replacement_catalog_order_id"] . '" target="_blank">' . $row["order_number"] . '</a>';
		$links_for_catalogue_orders[$row["order_item_replacement_id"]] = $link;
	}
}

//attachments for damaged goods
$sql_attachment = "select distinct order_file_id, order_file_visited, ".
                  "    order_file_title, order_file_description, ".
                  "    order_file_path, file_type_name, ".
                  "    order_files.date_created, ".
                  "    order_file_category_name, order_file_category_priority, ".
                  "    concat(user_name, ' ', user_firstname) as owner_fullname ".
                  "from order_files " . 
                  "left join order_file_categories on order_file_category_id = order_file_category ".
                  "left join users on user_id = order_file_owner ".
                  "left join file_types on order_file_type = file_type_id ";


$attachments_filter = "order_file_order = " . $project["project_order"];
$attachments_filter .= " and order_file_category_type = 2 and order_file_category = 22";

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid",$project["project_order"]);

require_once "include/project_head_small.php";



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();

/********************************************************************
    Create List for Catalog Items
*********************************************************************/ 
$list = new ListView($sql_items);

$list->set_title("Replacements");
$list->set_entity("order_item_replacements");
$list->set_filter($list_filter);
$list->set_order("item_shortcut");

$link="project_edit_material_replacement.php?pid=" . param("pid");

$list->add_column("item_shortcut", "Item Code", $link, "", "", COLUMN_NO_WRAP);
$list->add_column("order_item_replacement_item_text", "Name");
$list->add_column("order_item_replacement_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
$list->add_column("replacements_payer_name_name", "Paid by");


//$link="/user/order_edit_material_list.php?oid={order_item_replacement_catalog_order_id}";
//$list->add_column("order_number", "Catalogue Order", $link);
$list->add_text_column("order_number", "Catalogue Order", COLUMN_UNDERSTAND_HTML | COLUMN_NO_WRAP, $links_for_catalogue_orders);
$list->add_column("order_actual_order_state_code", "Order Status");



if(has_access("can_edit_material_replacements"))
{
	if($has_items_to_be_replaced == true)
	{
		$list->add_button("create_catalog_order", "Create Catalogue Order");
	}
	
	
	if($has_items_with_an_arrivale_date == true)
	{
		$list->add_button("add_item", "Add Item");
	}
	else
	{
		$form->error("No items have an arrival date. Replacement is not possible.");
	}
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$list->populate();
$list->process();

if ($list->button("add_item"))
{
    $link = "project_edit_material_replacement.php?pid=" . param("pid"); 
    redirect($link);
}
elseif($list->button("create_catalog_order"))
{
	$link = "project_edit_material_replacement_create_order.php?pid=" . param("pid"); 
    redirect($link);
}
/********************************************************************
    Create List of attachments
*********************************************************************/ 
$attachments = new ListView($sql_attachment);
$attachments->set_title("Attachments for Damaged Goods");

$attachments->set_entity("order_files");
$attachments->set_filter($attachments_filter);
$attachments->set_order("order_files.date_created DESC");

$link = "http://" . $_SERVER["HTTP_HOST"] . "/include/openfile.php?id={order_file_id}";
$attachments->add_column("order_file_title", "Title", $link, "", "", COLUMN_NO_WRAP);
$attachments->add_column("file_type_name", "Type", "", "", "", COLUMN_NO_WRAP);
$attachments->add_column("order_file_description", "Description");

$attachments->add_column("date_created", "Date/Time", "", "", "", COLUMN_NO_WRAP);
$attachments->add_column("owner_fullname", "Made by", "", "", "", COLUMN_NO_WRAP);

$attachments->populate();
$attachments->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Replacement of Materials");
$form->render();
echo "<br>";
$list->render();

echo "<br>";
$attachments->render();

require_once "include/project_footer_logistic_state.php";
$page->footer();

?>