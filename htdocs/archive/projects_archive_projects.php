<?php
/********************************************************************

    projects_archive_projects.php

    Entry page for the projects section.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-11-07
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2002-12-03
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/access_filters.php";

check_access("has_access_to_archive");

register_param("y1");
register_param("y2");
register_param("c");
register_param("p");
register_param("pn");

/********************************************************************
    prepare all data needed
*********************************************************************/
if (!param("y1"))
{
    $y1 = 0;
}
else
{
	$y1 = param("y1");
}
if (!param("y2"))
{
    $y2 = 0;
}
else
{
	$y2 = param("y2");
}

if (!param("p"))
{
    $p = 0;
}
else
{
	$p = param("p");
}

if (!param("c"))
{
    $c = 0;
}
else
{
	$c = param("c");
}

if (!param("st"))
{
    $st = '';
}
else
{
	$st = param("st");
}

if (!param("pm"))
{
    $pm = 0;
}
else
{
	$pm = param("pm");
}


if (!param("lt"))
{
    $lt = 0;
}
else
{
	$lt = param("lt");
}


if (!param("pt"))
{
    $pt = 0;
}
else
{
	$pt = param("pt");
}


// get user_data
$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());

//get country access
$country_filter = "";
$tmp = array();
$sql = "select * from country_access " .
	   "where country_access_user = " . user_id();


$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{            
	$tmp[] = $row["country_access_country"];
}

if(count($tmp) > 0) {
	$country_filter = " country_id IN (" . implode(",", $tmp) . ") ";
}

$country_access_filter = get_users_regional_access_to_countries(1, user_id());
if($country_access_filter)
{
	if($country_filter)
	{
		$country_filter = "(" . $country_filter .  " or " . $country_access_filter . ")";
	}
	else
	{
		$country_filter = "(" . $country_access_filter . ")";
	}
}


// create sql
$sql = "select distinct project_id, project_number, " .
       "left(projects.date_created, 10), ".
       "    product_line_name, postype_name, " . "concat(order_shop_address_place,', ', order_shop_address_company), country_name, ".
        "    rtcs.user_name as rtc, rtos.user_name as rto,".
       "    order_id, order_actual_order_state_code, project_costtype_text,  ".
	   " project_cost_cms_completed, project_cost_cms_approved, " . 
	   " project_actual_opening_date, projectkind_code " . 
       "from projects ".
       "left join orders on project_order = order_id ".
	   "left join project_costs on project_cost_order = order_id " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
       "left join order_items on order_item_order = order_id ".
       "left join product_lines on project_product_line = product_line_id ".
       "left join postypes on postype_id = project_postype ".
       "left join countries on order_shop_address_country = countries.country_id ".
       "left join users as rtcs on project_retail_coordinator = rtcs.user_id " .
	   "left join projectkinds on projectkind_id = project_projectkind ".
	   "left join users as rtos on order_retail_operator = rtos.user_id " . 
	   "left join project_states on project_state_id = project_state ";



// create list filter
if(has_access("has_access_to_all_travalling_retail_data"))
{
	$condition = get_user_specific_order_list(user_id(), 1, $user_roles, true);
}
else
{
	$condition = get_user_specific_order_list(user_id(), 1, $user_roles, false);
}


$list_filter = "";
$list_filter_without_condition = "";
if($y1 > 0)
{
	$list_filter = " left(order_date,4) >= " .$y1;
}

if($list_filter and $y2 > 0)
{
	$list_filter .= " and left(order_date,4) <= " .$y2;
}
elseif($y2 > 0)
{
	$list_filter = " left(order_date,4) <= " .$y2;
}

if($list_filter  and $p > 0)
{
	$list_filter .= " and product_line_id = " . $p;
}
elseif($p > 0)
{
	$list_filter = " product_line_id = " . $p;
}

if($list_filter and $c > 0)
{
	$list_filter .= " and country_id = " . $c;
}
elseif($c > 0)
{
	 $list_filter = " country_id = " . $c;
}


if($list_filter and $pm > 0)
{
	$list_filter .= "    and project_retail_coordinator = " . $pm;
}
elseif($pm > 0)
{
	 $list_filter = "  project_retail_coordinator = " . $pm;
}



if($list_filter and $lt > 0)
{
	$list_filter .= "    and project_cost_type = " . $lt;
}
elseif($lt > 0)
{
	 $list_filter = "  project_cost_type = " . $lt;
}


if($list_filter and $pt > 0)
{
	$list_filter .= "    and project_postype = " . $pt;
}
elseif($pt > 0)
{
	 $list_filter = "  project_postype = " . $pt;
}



if (has_access("has_access_to_all_projects"))
{
	if($list_filter and $st != '')
    {
		$list_filter .= ' and (project_number like "%' . $st . '%" 
	                  or postype_name like "%' . $st . '%" 
					  or project_costtype_text like "%' . $st . '%"
					  or project_state_text like "%' . $st . '%"
					  or product_line_name like "%' . $st . '%"
					  or postype_name like "%' . $st . '%"
					  or order_actual_order_state_code like "%' . $st . '%"
					  or order_shop_address_place like "%' . $st . '%"
					  or order_shop_address_company like "%' . $st . '%"
					  or country_name like "%' . $st . '%")';
    }
	elseif($st != '')
	{
		$list_filter = ' (project_number like "%' . $st . '%" 
	                  or postype_name like "%' . $st . '%" 
					  or project_costtype_text like "%' . $st . '%"
					  or project_state_text like "%' . $st . '%"
					  or product_line_name like "%' . $st . '%"
					  or postype_name like "%' . $st . '%"
					  or order_actual_order_state_code like "%' . $st . '%"
					  or order_shop_address_place like "%' . $st . '%"
					  or order_shop_address_company like "%' . $st . '%"
					  or country_name like "%' . $st . '%")';
	}

	
}
elseif($country_filter != '')
{

	if($list_filter and $st != '')
    {
		$list_filter .= ' and (project_number like "%' . $st . '%" 
	                  or postype_name like "%' . $st . '%" 
					  or project_costtype_text like "%' . $st . '%"
					  or project_state_text like "%' . $st . '%"
					  or product_line_name like "%' . $st . '%"
					  or postype_name like "%' . $st . '%"
					  or order_actual_order_state_code like "%' . $st . '%"
					  or order_shop_address_place like "%' . $st . '%"
					  or order_shop_address_company like "%' . $st . '%"
					  or country_name like "%' . $st . '%")';
    }
	elseif($st != '')
	{
		$list_filter = ' (project_number like "%' . $st . '%" 
	                  or postype_name like "%' . $st . '%" 
					  or project_costtype_text like "%' . $st . '%"
					  or project_state_text like "%' . $st . '%"
					  or product_line_name like "%' . $st . '%"
					  or postype_name like "%' . $st . '%"
					  or order_actual_order_state_code like "%' . $st . '%"
					  or order_shop_address_place like "%' . $st . '%"
					  or order_shop_address_company like "%' . $st . '%"
					  or country_name like "%' . $st . '%")';
	}

	
	
	$tmp_filter = "";
	if($user_data["user_can_only_see_projects_of_his_address"] == 1)
	{
		
		if($list_filter)
		{
			$tmp_filter = $list_filter;
			$list_filter .= " and order_client_address = " . $user_data["address"];
		}
		else
		{
			$list_filter = " order_client_address = " . $user_data["address"];
		}
	}
	
	
	if($country_access_filter)
	{
		if($list_filter and $tmp_filter)
		{
			$list_filter = "((" . $list_filter . ") and (" . $country_filter . " and " . $tmp_filter . ")) ";
		}
		elseif($list_filter)
		{
			$list_filter = "((" . $list_filter . ") and (" . $country_filter . ")) ";
		}
		else
		{
			$list_filter =  $country_filter;
		}
	}
	else
	{
		if($list_filter and $tmp_filter)
		{
			$list_filter = "((" . $list_filter . ") or (" . $country_filter . " and " . $tmp_filter . ")) ";
		}
		elseif($list_filter)
		{
			$list_filter = "((" . $list_filter . ") or (" . $country_filter . ")) ";
		}
		else
		{
			$list_filter =  $country_filter;
		}
	}

}
elseif (has_access("has_access_to_all_projects_of_his_country"))
{

	if ($condition == "")
    {
		if($list_filter and $st != '')
		{
			$list_filter .= ' and (project_number like "%' . $st . '%" 
						  or postype_name like "%' . $st . '%" 
						  or project_costtype_text like "%' . $st . '%"
						  or project_state_text like "%' . $st . '%"
						  or product_line_name like "%' . $st . '%"
						  or postype_name like "%' . $st . '%"
						  or order_actual_order_state_code like "%' . $st . '%"
						  or order_shop_address_place like "%' . $st . '%"
						  or order_shop_address_company like "%' . $st . '%"
						  or country_name like "%' . $st . '%")';
		}
		elseif($st != '')
		{
			$list_filter = ' (project_number like "%' . $st . '%" 
						  or postype_name like "%' . $st . '%" 
						  or project_costtype_text like "%' . $st . '%"
						  or project_state_text like "%' . $st . '%"
						  or product_line_name like "%' . $st . '%"
						  or postype_name like "%' . $st . '%"
						  or order_actual_order_state_code like "%' . $st . '%"
						  or order_shop_address_place like "%' . $st . '%"
						  or order_shop_address_company like "%' . $st . '%"
						  or country_name like "%' . $st . '%")';
		}

		if($list_filter)
		{
			$list_filter .= "    and (order_item_supplier_address = " . $user_data["address"] . " " .
						   "    or order_item_forwarder_address = " . $user_data["address"] . " " .
						   "    or order_retail_operator = " . user_id() . " " .
						   "    or project_retail_coordinator = " . user_id()  . " " .
						   "    or project_design_contractor = " .  user_id()  . " " .
						   "    or project_design_supervisor = " .   user_id()   . " " .
						   "    or country_id = " . $user_data["country"] . 
						   "    or order_client_address = " . $user_data["address"] . ")";
		}
		else
		{
			$list_filter = "    (order_item_supplier_address = " . $user_data["address"] . " " .
						   "    or order_item_forwarder_address = " . $user_data["address"] . " " .
						   "    or order_retail_operator = " . user_id() . " " .
						   "    or project_retail_coordinator = " . user_id()  . " " .
						   "    or project_design_contractor = " .  user_id()  . " " .
						   "    or project_design_supervisor = " .   user_id()   . " " .
						   "    or country_id = " . $user_data["country"] . 
						   "    or order_client_address = " . $user_data["address"] . ")";
		}
	}
	else
	{
		if($list_filter and $st != '')
		{
			$list_filter .= ' and (project_number like "%' . $st . '%" 
						  or postype_name like "%' . $st . '%" 
						  or project_costtype_text like "%' . $st . '%"
						  or project_state_text like "%' . $st . '%"
						  or product_line_name like "%' . $st . '%"
						  or postype_name like "%' . $st . '%"
						  or order_actual_order_state_code like "%' . $st . '%"
						  or order_shop_address_place like "%' . $st . '%"
						  or order_shop_address_company like "%' . $st . '%"
						  or country_name like "%' . $st . '%")';
		}
		elseif($st != '')
		{
			$list_filter = ' (project_number like "%' . $st . '%" 
						  or postype_name like "%' . $st . '%" 
						  or project_costtype_text like "%' . $st . '%"
						  or project_state_text like "%' . $st . '%"
						  or product_line_name like "%' . $st . '%"
						  or postype_name like "%' . $st . '%"
						  or order_actual_order_state_code like "%' . $st . '%"
						  or order_shop_address_place like "%' . $st . '%"
						  or order_shop_address_company like "%' . $st . '%"
						  or country_name like "%' . $st . '%")';
		}
		
		$list_filter_without_condition = $list_filter;

		if($list_filter)
		{
			$list_filter .= "   and (" . $condition . ")";
		}
		else
		{
			$list_filter .= "   (" . $condition . ")";
		}
	}

}
else
{

	if ($condition == "")
    {
		if($list_filter)
		{
			$list_filter .= "    and (order_item_supplier_address = " . $user_data["address"] . " " .
						   "    or order_item_forwarder_address = " . $user_data["address"] . " " .
						   "    or order_retail_operator = " . user_id() . " " .
						   "    or project_retail_coordinator = " . user_id()  . " " .
						   "    or project_design_contractor = " .  user_id()  . " " .
						   "    or project_design_supervisor = " .   user_id()   . " " .
						   "    or order_client_address = " . $user_data["address"] . ")";
		}
		else
		{
			$list_filter = "    (order_item_supplier_address = " . $user_data["address"] . " " .
						   "    or order_item_forwarder_address = " . $user_data["address"] . " " .
						   "    or order_retail_operator = " . user_id() . " " .
						   "    or project_retail_coordinator = " . user_id()  . " " .
						   "    or project_design_contractor = " .  user_id()  . " " .
						   "    or project_design_supervisor = " .   user_id()   . " " .
						   "    or order_client_address = " . $user_data["address"] . ")";

		}
    }
    else
    {
		$list_filter_without_condition = $list_filter;
		if($list_filter)
		{
			$list_filter .= "   and (" . $condition . ")";
		}
		else
		{
			$list_filter = "   (" . $condition . ")";
		}
    }


	if($list_filter and $st != '')
	{
		$list_filter .= ' and (project_number like "%' . $st . '%" 
					  or postype_name like "%' . $st . '%" 
					  or project_costtype_text like "%' . $st . '%"
					  or project_state_text like "%' . $st . '%"
					  or product_line_name like "%' . $st . '%"
					  or postype_name like "%' . $st . '%"
					  or order_actual_order_state_code like "%' . $st . '%"
					  or order_shop_address_place like "%' . $st . '%"
					  or order_shop_address_company like "%' . $st . '%"
					  or country_name like "%' . $st . '%")';
	}
	elseif($st  != '')
	{
		$list_filter = ' (project_number like "%' . $st . '%" 
					  or postype_name like "%' . $st . '%" 
					  or project_costtype_text like "%' . $st . '%"
					  or project_state_text like "%' . $st . '%"
					  or product_line_name like "%' . $st . '%"
					  or postype_name like "%' . $st . '%"
					  or order_actual_order_state_code like "%' . $st . '%"
					  or order_shop_address_place like "%' . $st . '%"
					  or order_shop_address_company like "%' . $st . '%"
					  or country_name like "%' . $st . '%")';
	}

}

if($list_filter)
{
	$list_filter .= " and (order_actual_order_state_code >= '890' or (order_archive_date is not null and order_archive_date <> '0000-00-00')) ";
	$list_filter_without_condition .= " and (order_actual_order_state_code >= '890' or (order_archive_date is not null and order_archive_date <> '0000-00-00')) ";
}
else
{
	$list_filter = " (order_actual_order_state_code >= '890' or (order_archive_date is not null and order_archive_date <> '0000-00-00')) ";
	$list_filter_without_condition = " (order_actual_order_state_code >= '890' or (order_archive_date is not null and order_archive_date <> '0000-00-00')) ";
}


if($list_filter and $user_data["user_can_only_see_projects_of_his_address"] == 1)
{
	
	//$list_filter.= " and order_client_address = " . dbquote($user_data["address"]);
	//$list_filter_without_condition .= " and order_client_address = " . dbquote($user_data["address"]);
}

//echo $sql . " where " .$list_filter;

$cms_states = array();
$aods = array();
$duty_free_projects = array();

if (has_access("has_access_to_all_projects"))
{
    $sql_tmp = $sql . " where " . $list_filter;
    $res = mysql_query($sql_tmp);

    while($row = mysql_fetch_assoc($res))
    {
		
		if($row["order_actual_order_state_code"] != '900')
		{
			if($row['project_actual_opening_date'] != NULL and $row['project_actual_opening_date'] != '0000-00-00')
			{
				$aods[$row["project_id"]] = "/pictures/ok.gif";
			}
			else
			{
				$aods[$row["project_id"]] = "/pictures/not_ok.gif";
			}

			if($row['project_cost_cms_completed'] == 1)
			{
				$cms_states[$row["project_id"]] = "/pictures/ok.gif";
			}
			else
			{
				$cms_states[$row["project_id"]] = "/pictures/not_ok.gif";
			}
		}


		//check if project is a duty free project
		$project_design_objective_item_ids = get_project_design_objective_item_ids($row["project_id"]);
		
		if(array_key_exists(161 ,$project_design_objective_item_ids))
		{
			$duty_free_projects[$row["project_id"]] = "/pictures/dutyfree.png";
		}
    }
}


//count projects
$sql_count = "select count(distinct project_id) as num_recs " . 
       "from projects ".
       "left join orders on project_order = order_id ".
	   "left join order_items on order_item_order = order_id ".
	   "left join project_costs on project_cost_order = order_id " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
       "left join product_lines on project_product_line = product_line_id ".
       "left join postypes on postype_id = project_postype ".
       "left join countries on order_shop_address_country = countries.country_id ".
       "left join users as rtcs on project_retail_coordinator = rtcs.user_id " . 
	   "left join users as rtos on order_retail_operator = rtos.user_id " . 
	    "left join project_states on project_state_id = project_state ";

$sql_count = $sql_count . " where " . $list_filter;
$res = mysql_query($sql_count);
$row = mysql_fetch_assoc($res);

/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);

$list->set_entity("projects in the records");
$list->set_order("left(projects.date_created, 10) desc");
$list->set_filter($list_filter);

$list->add_hidden("y1", param("y1"));
$list->add_hidden("y2", param("y2"));
$list->add_hidden("p", param("p"));
$list->add_hidden("c", param("c"));
$list->add_hidden("pm", param("pm"));
$list->add_hidden("pt", param("pt"));
$list->add_hidden("lt", param("lt"));

if (preg_match("/" . APPLICATION_URLPART . "/", $_SERVER["HTTP_HOST"]))
{
	$link = APPLICATION_URL . "/archive";
}
else
{
	$link = "/archive";
}

$link .= "/project_task_center.php?pid={project_id}&y1=" .param("y1") . "&y2=" . param("y2");
$list->add_column("project_number", "Project No.", $link, "", "", COLUMN_NO_WRAP);



$list->add_column("project_costtype_text", "Legal Type");
$list->add_column("projectkind_code", "Kind", "", 0, "", COLUMN_NO_WRAP);

$list->add_column("product_line_name", "Product Line");


$list->add_column("postype_name", "POS Type");


$list->add_column("order_actual_order_state_code", "Status");
$list->add_column("left(projects.date_created, 10)", "Submitted", "", "", "", COLUMN_NO_WRAP);
$list->add_column("country_name", "Country");
$list->add_column("concat(order_shop_address_place,', ', order_shop_address_company)", "POS Location");

$list->add_column("rtc", "PL");
//$list->add_column("rto", "RTLC");

$list->add_image_column("dutyfree", "DF", 0, $duty_free_projects);

if (has_access("has_access_to_all_projects"))
{
	$list->add_image_column("cms", "CMS", 0, $cms_states);
	$list->add_image_column("aod", "AOD", 0, $aods);
}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->process();

$page = new Page("projects");

//$page->register_action('orders', 'Orders', "orders_archive.php");
$page->register_action('projects', 'Search Projects', "projects_archive.php");

//check if there are changes in project numbers
$sql_y = "select DISTINCT YEAR(oldproject_numbers.date_created) as year " . 
	   "from oldproject_numbers "  .
	   "left join projects on project_id = oldproject_number_project_id " . 
	   "left join orders on project_order = order_id ".
	   "inner join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
       "left join product_lines on project_product_line = product_line_id ".
       "left join postypes on postype_id = project_postype ".
	   "left join project_states on project_state_id = project_state ".
       "left join countries on order_shop_address_country = countries.country_id ".
       "left join users as rtcs on project_retail_coordinator = rtcs.user_id " .
	   "left join projectkinds on projectkind_id = project_projectkind ".
	   "left join users as rtos on order_retail_operator = rtos.user_id ";

	   " where " . $list_filter .
	   " order by year DESC";

$res = mysql_query($sql_y) or dberror($sql_y);
if ($row = mysql_fetch_assoc($res))
{
	$page->register_action('changedprojectnumbers', 'Info about Changed Project Numbers', "changed_projectnumbers.php");
}

$page->register_action('welcome', 'Quit Archive', "../user/welcome.php");

$page->header();
$page->title("Projects: Archive");
$list->render();
$page->footer();

?>