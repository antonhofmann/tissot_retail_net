<?php
/********************************************************************

    order_shipping_documents_assign.php

    Assign shipping documents

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-10
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-10
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.



*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("has_access_to_shipping_details_in_orders");

/********************************************************************
    prepare all data needed
*********************************************************************/
$order = get_order(param("oid"));

// get company's address
$client_address = get_address($order["order_client_address"]);
// get user data
$user_data = get_user(user_id());


// get standard shipping documents
$standard_documents = array();
$sql_docs = "select standard_shipping_document_id, standard_shipping_document_name " . 
           " from address_shipping_documents " . 
		   " left join standard_shipping_documents on standard_shipping_document_id = address_shipping_document_document_id " . 
		   " where address_shipping_document_address_id = " . dbquote($order["order_client_address"]) . 
		   " order by standard_shipping_document_name";


$res = mysql_query($sql_docs) or dberror($sql_docs);
while ($row = mysql_fetch_assoc($res))
{
	$standard_documents[$row["standard_shipping_document_id"]] = $row["standard_shipping_document_name"];
}
			  

//get suppliers and forwarders
$sql_suppliers = "select DISTINCT order_item_supplier_address, " .
					" address_company as supplier " . 
				   "from order_items ".
				   "left join addresses on address_id = order_item_supplier_address " . 
				   " where order_item_order = " . dbquote($order["order_id"])  .
				    " and order_item_supplier_address > 0 " .
				   " ORDER BY supplier";



$sql_forwarders =  "select DISTINCT order_item_forwarder_address, " .
					" address_company as forwarder " . 
				   "from order_items ".
				   "left join addresses on address_id = order_item_forwarder_address " . 
				   " where order_item_order = " . dbquote($order["order_id"])  .
				    " and order_item_forwarder_address > 0 " .
				   " ORDER BY forwarder";

// build filter for the list of attachments
$list_filter = "order_file_order = " . $order["order_id"];

$file_category_filter = "";

$user_roles = get_user_roles(user_id());
$categroy_restrictions = get_file_category_restirctions($user_roles);

if(count($categroy_restrictions) > 0)
{
	$file_category_filter = " and order_file_category IN (" . implode(",", $categroy_restrictions). ") ";
}

$list_filter .=  $file_category_filter;



$sql_attachment = "select distinct order_file_id,  ".
                  "    order_file_title, order_file_description, ".
                  "    order_file_path, file_type_name, ".
                  "    order_files.date_created, ".
                  "    order_file_category_name, order_file_category_priority, ".
                  "    concat(address_company, ' - ', user_name, ' ', user_firstname) as owner_fullname, address_id ".
                  "from order_files  " .
                  "left join order_file_categories on order_file_category_id = order_file_category ".
                  "left join users on user_id = order_file_owner ".
		          "left join addresses on address_id = user_address " .
                  "left join file_types on order_file_type = file_type_id " . 
				  "LEFT JOIN order_shipping_documents ON order_shipping_document_file_order_file_id = order_file_id";

if (has_access("has_access_to_all_attachments_in_orders"))
{
	
}
else
{
	$file_ids = array();
	$sql_order_addresses = 'select order_file_address_file from order_file_addresses ' .
		                  'where order_file_address_address = ' . $user_data["address"];

	$res = mysql_query($sql_order_addresses) or dberror($sql_oder_addresses);
    while ($row = mysql_fetch_assoc($res)) {
		$file_ids[] = $row['order_file_address_file'];
	}

	$file_filter = "";
	if(count($file_ids) > 0) {
		$file_filter = ' or order_file_id IN (' . implode(',', $file_ids) . ')';
	}
	
	$list_filter = $list_filter . " and (order_file_owner = " . user_id() . $file_filter . ")";
}

$list_filter .= " and order_file_category_type = 1 and order_file_category_id = 15";

$list_filter .= " and order_shipping_document_file_order_file_id is null";


$suppliers = array();
$sql = $sql_attachment . " where " . $list_filter;

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$suppliers[$row["order_file_id"]] = $row["address_id"];
}



/********************************************************************
    Create Form
*********************************************************************/ 
$form = new Form("order_files", "order_file");
$form->add_hidden("oid", param("oid"));
require_once "include/order_head_small.php";

/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql_attachment);
$list->set_title("Order Attachments");

$list->set_entity("order_files");
$list->set_filter($list_filter);
$list->set_order("order_files.date_created DESC");
$list->set_group("owner_fullname");


$list->add_column("date_created", "Date/Time", "", "", "", COLUMN_NO_WRAP);

$link = "http://" . $_SERVER["HTTP_HOST"] . "/include/openfile.php?id={order_file_id}";
$list->add_column("order_file_title", "Title", $link, "", "", COLUMN_NO_WRAP);
$list->add_list_column("doc", "Document", $sql_docs);
$list->add_list_column("supplier", "Supplier", $sql_suppliers, 0, $suppliers);
$list->add_list_column("forwarder", "Forwarder", $sql_forwarders);


$list->add_column("order_file_description", "Description");

$list->add_button("save", "Save");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$list->populate();
$list->process();


if($list->button("save"))
{
	$doc_ids = array();
    $supplier_ids = array();
	$forwarder_ids = array();

	foreach($list->columns as $key=>$column)
	{
		if($column["name"] == "doc")
		{
			foreach($column["values"] as $order_file_id=>$value)
			{
				$doc_ids[$order_file_id] = $value;
			}
		}
		if($column["name"] == "supplier")
		{
			foreach($column["values"] as $order_file_id=>$value)
			{
				$supplier_ids[$order_file_id] = $value;
			}
		}
		if($column["name"] == "forwarder")
		{
			foreach($column["values"] as $order_file_id=>$value)
			{
				$forwarder_ids[$order_file_id] = $value;
			}
		}
	}

	foreach($doc_ids as $order_file_id=>$value)
	{
		if($doc_ids[$order_file_id] > 0
			and $supplier_ids[$order_file_id] > 0
			and $forwarder_ids[$order_file_id] > 0)
		{
		
			//check if doc is already there
			$insert_record = false;

			$sql = "select order_shipping_document_file_order_file_id " . 
				   " from order_shipping_documents " . 
				   " where order_shipping_document_order_id = " . dbquote($order["order_id"]) . 
				   " and order_shipping_document_supplier_id = " . dbquote($supplier_ids[$order_file_id]) .
				   " and order_shipping_document_forwarder_id = " . dbquote($forwarder_ids[$order_file_id]) .
				   " and order_shipping_document_standard_document_id = " . dbquote($doc_ids[$order_file_id]);


			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				if($row["order_shipping_document_file_order_file_id"] > 0
					and $row["order_shipping_document_file_order_file_id"] != $order_file_id)
				{
					$insert_record = true;
				}
				elseif($row["order_shipping_document_file_order_file_id"] == NULL)
				{
					$sql_u = "Update order_shipping_documents SET " . 
						     " order_shipping_document_file_order_file_id = " . dbquote($order_file_id) . ", " . 
						     " user_modified = " . dbquote(user_login()) . ", " . 
						     " date_modified = " . dbquote(date("Y-m-d H:i:s")) . 
						     " where order_shipping_document_order_id = " . dbquote($order["order_id"]) . 
							 " and order_shipping_document_supplier_id = " . dbquote($supplier_ids[$order_file_id]) .
				             " and order_shipping_document_forwarder_id = " . dbquote($forwarder_ids[$order_file_id]) .
				             " and order_shipping_document_standard_document_id = " . dbquote($doc_ids[$order_file_id]);

					$result = mysql_query($sql_u) or dberror($sql_u);
				}
			}
			else
			{
				$insert_record = true;
			}


			if($insert_record == true)
			{
				$sql_i = "INSERT into order_shipping_documents (" . 
					     "order_shipping_document_order_id, order_shipping_document_supplier_id, order_shipping_document_forwarder_id, order_shipping_document_standard_document_id, order_shipping_document_name, order_shipping_document_file_order_file_id, user_created, date_created" . 
					     ") VALUES (" . 
					     dbquote($order["order_id"]) . ", " .
					     dbquote($supplier_ids[$order_file_id]) . ", " .
					     dbquote($forwarder_ids[$order_file_id]) . ", " .
					     dbquote($doc_ids[$order_file_id]) . ", " .
					     dbquote($standard_documents[$doc_ids[$order_file_id]]) . ", " .
					     dbquote($order_file_id) . ", " .
					     dbquote(user_login()) . ", " .
					     dbquote(date("Y-m-d H:i:s")) . ") ";

				$result = mysql_query($sql_i) or dberror($sql_i);
			}
		}
	}
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$page = new Page("orders");

require "include/order_page_actions.php";
$page->header();
$page->title("Shipping Details - Assign Existing Documents");

require_once("include/tabs_shipping_details.php");


$form->render();

$list->render();


$page->footer();


?>