<?php
	require_once "../include/frame.php";
	
	if(has_access("can_perform_all_queries"))
	{
		$location = APPLICATION_URL . "/mis/welcome.php";
		if (preg_match("/" . APPLICATION_URLPART . "/", $_SERVER["HTTP_HOST"]))
		{
			header("location: $location");
		}
		else
		{
			header("location: /mis/welcome.php");
		}
	}
	else
	{
		$location = APPLICATION_URL . "/mis/welcome.php";
		if (preg_match("/" . APPLICATION_URLPART . "/", $_SERVER["HTTP_HOST"]))
		{
			header("location: $location");
		}
		else
		{
			header("location: /mis/welcome.php");
		}
	}

?>