<?php
/********************************************************************

    projects_queries.php

    List all Porjects Queries.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2011-07-30
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2011-07-30
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";

check_access("can_perform_queries");
set_referer("projects_query.php");


//create a copy of an existing query
if(param("copy_id"))
{
	$sql = "select * from mis_queries where mis_query_id = " . param("copy_id");
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$sql_i = "Insert into mis_queries (" . 
			     "mis_query_context, " . 
			     "mis_query_owner, " . 
			     "mis_query_name, " . 
			     "mis_query_filter, " .
			     "mis_query_milestone_filter, " .
			     "user_created, " . 
			     "date_created) VALUES (" . 
			     dbquote($row["mis_query_context"]) . ", " . 
			     user_id() . ", " .
			     dbquote($row["mis_query_name"] .  " - copy") . ", " .
			     dbquote($row["mis_query_filter"]) . ", " .
			     dbquote($row["mis_query_milestone_filter"]) . ", " .
			     dbquote(user_login()) . ", " .
			     "CURRENT_TIMESTAMP)";
		$result = mysql_query($sql_i);
	}


}
elseif(param("remove_id"))
{
	$sql_d = "delete from mis_querypermissions " . 
		   " where mis_querypermission_query = " . param("remove_id") . 
		   " and mis_querypermission_user = " . user_id();
	$result = mysql_query($sql_d);

}
elseif(param("delete_id"))
{
	$sql = "delete from mis_querypermissions " . 
		   " where mis_querypermission_query = " . param("delete_id"); 
	$result = mysql_query($sql) or dberror($sql);

	$sql = "delete from mis_queries " . 
		   " where mis_query_id = " . param("delete_id"); 
	$result = mysql_query($sql) or dberror($sql);

	redirect("projects_queries.php?qt=" . param("qt"));
}

$qt = 1;
if(param("qt"))
{
	$qt = param("qt");
}

//build query for the list
$xls_file = "projects_query_1_xls.php";
$sql = "select mis_query_id, mis_query_name, mis_query_name,  mis_query_owner, " .
       "concat(user_name, ' ', user_firstname) as uname, " .
	   "mis_queries.date_created as cdate, mis_queries.date_modified as mdate " . 
       "from mis_queries " .
	   "left join users on user_id = mis_query_owner ";

$list_filter = "mis_query_context = 'projects_in_process' and mis_query_owner = " . user_id();
if($qt == 6)
{
	$list_filter = "mis_query_context = 'masterplan' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'masterplan'";
	$xls_file = "projects_query_6_xls.php";
}
elseif($qt == 7)
{
	$list_filter = "mis_query_context = 'cmsoverview' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'cmsoverview'";
	$xls_file = "projects_query_7_xls.php";
}
elseif($qt == 8)
{
	$list_filter = "mis_query_context = 'klapproved' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'klapproved'";
	$xls_file = "projects_query_8_xls.php";
}
elseif($qt == 9)
{
	$list_filter = "mis_query_context = 'projectsheets' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'projectsheets'";
	$xls_file = "projects_query_9_xls.php";
}
elseif($qt == '10co')
{
	$list_filter = "mis_query_context = 'productionplanning_co' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'productionplanning_co'";
	$xls_file = "projects_query_10co_xls.php";
}
elseif($qt == 10)
{
	$list_filter = "mis_query_context = 'productionplanning' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'productionplanning'";
	$xls_file = "projects_query_10_xls.php";
}
elseif($qt == 11)
{
	$list_filter = "mis_query_context = 'projectmilestones' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'projectmilestones'";
	$xls_file = "projects_query_11_xls.php";
}
elseif($qt == 12)
{
	$list_filter = "mis_query_context = 'cmsatatus' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'cmsatatus'";
	$xls_file = "projects_query_12_xls.php";
}
elseif($qt == 13)
{
	$list_filter = "mis_query_context = 'transportationcost' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'transportationcost'";
	$xls_file = "projects_query_13_xls.php";
}
elseif($qt == 14)
{
	$list_filter = "mis_query_context = 'posequipment' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'posequipment'";
	$xls_file = "projects_query_14_xls.php";
}
elseif($qt == 15)
{
	$list_filter = "mis_query_context = 'deliverybyitem' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'deliverybyitem'";
	$xls_file = "projects_query_15_xls.php";
}
elseif($qt == 16)
{
	$list_filter = "mis_query_context = 'cmscompleted' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'cmscompleted'";
	$xls_file = "projects_query_16_xls.php";
}

elseif($qt == 22)
{
	$list_filter = "mis_query_context = 'orderstates' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'orderstates'";
	$xls_file = "projects_query_22_xls.php";
}
elseif($qt == 30)
{
	$list_filter = "mis_query_context = 'poslocations' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'poslocations'";
	$xls_file = "projects_query_30_xls.php";
}
elseif($qt == 31)
{
	$list_filter = "mis_query_context = 'masterplan' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'masterplan'";
	$xls_file = "projects_query_31_xls.php";
}
elseif($qt == 35)
{
	$list_filter = "mis_query_context = 'masterplano' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'masterplano'";
	$xls_file = "projects_query_35_xls.php";
}
elseif($qt == 40)
{
	$list_filter = "mis_query_context = 'shipments' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'shipments'";
	$xls_file = "projects_query_40_xls.php";
}
elseif($qt == 41)
{
	$list_filter = "mis_query_context = 'shipmentsf' and mis_query_owner = " . user_id();
	$list_filter2 = "mis_query_context = 'shipmentsf'";
	$xls_file = "projects_query_41_xls.php";
}


$queries = array();
$query_links = array();
$query_permissions = array();
$copy_links = array();
$delete_links = array();
$permitted_users = array();

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
	$link_excel = sprintf('<a href="%s?query_id=%d" title="Export Excel"><img src="/pictures/ico_xls.gif" border="0"></a>',
		$xls_file,
		$row['mis_query_id']
		);
	
	
	
	if($qt == 31)
	{
		$link_gantt = sprintf('<a target="_blank" href="/project/projectworkflow/%d" title="Export Gantt" style="margin-left: 5px;"><img src="/pictures/ico_gantt.png" border="0"/></a>', $row["mis_query_id"]);
		$queries[$row["mis_query_id"]] = $link_excel . $link_gantt;
	}
	else
	{
		$queries[$row["mis_query_id"]] = $link_excel;
	}
	

	

	if(user_id() == $row["mis_query_owner"])
	{
		$query_links[$row["mis_query_id"]] = "<a href='projects_query.php?id=" .  $row["mis_query_id"] . "&qt=" . $qt . "'>" . $row["mis_query_name"] . "</a>";

		$delete_links[$row["mis_query_id"]] = '<img data-id="'. $row["mis_query_id"]. '" class="delete_query" src="/pictures/remove.gif" border="0" style="margin-top:3px;cursor:pointer;" alt="Remove"/>';
	}
	else
	{
		$query_links[$row["mis_query_id"]] = $row["mis_query_name"];
		
		$delete_links[$row["mis_query_id"]] = '<a title="Remove query from my list" href="projects_queries.php?remove_id=' .  $row["mis_query_id"] . '&qt=' . param("qt") . '"><img src="/pictures/remove.gif" border="0" style="margin-top:3px;" alet="Remove"/></a>';
	}

	$copy_links[$row["mis_query_id"]] = "<a href='projects_queries.php?copy_id=" .  $row["mis_query_id"] . "&qt=" . $qt ."'>create copy</a>";

	//get permissions

	$tmp_users = array();
	
	$sql_p = "select * from mis_querypermissions " .
		     "left join mis_queries on mis_query_id = mis_querypermission_query " . 
		     " left join users on user_id = mis_querypermission_user " . 
			 "where  mis_querypermission_query = " . $row["mis_query_id"] . 
			 " and " . $list_filter2;

	//echo $sql_p . '<br />';
		     

	$res_p = mysql_query($sql_p) or dberror($sql_p);
	while ($row_p = mysql_fetch_assoc($res_p))
	{
		if($row_p["mis_querypermission_user"] == user_id())
		{
			$query_permissions[$row_p["mis_querypermission_query"]] = $row_p["mis_querypermission_query"];
		}
		else {
			$tmp_users[] = $row_p["user_name"] . " " . $row_p["user_firstname"];
		}
	}

	if(count($tmp_users) > 0) {
		$permitted_users[$row["mis_query_id"]] = implode(', ', $tmp_users);
	}


	
}

if(count($query_permissions) > 0)
{
	$permission_filter =  implode(',', $query_permissions);
	$permission_filter = " or mis_query_id in (" . $permission_filter . ") ";
	$list_filter .= $permission_filter;
}
	


$list = new ListView($sql);

$list->set_entity("mis_queries");
$list->set_order("mis_query_name");
$list->set_filter($list_filter);

$list->add_hidden("qt", $qt);
$list->add_text_column("queries", " ", COLUMN_NO_WRAP | COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $queries);
$list->add_text_column("querylinks", "Query Name", COLUMN_NO_WRAP | COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $query_links);
$list->add_column("uname", "Owner", "", "", "", COLUMN_NO_WRAP);

$list->add_column("cdate", "Created", "", "", "", COLUMN_NO_WRAP);
$list->add_column("mdate", "Modified", "", "", "", COLUMN_NO_WRAP);


$list->add_text_column("copy", "", COLUMN_NO_WRAP | COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $copy_links);
$list->add_text_column("remove", "", COLUMN_NO_WRAP | COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $delete_links);
$list->add_text_column("permitted_users", "Granted Access", COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $permitted_users);


$list->add_button(LIST_BUTTON_NEW, "New Query", "projects_query.php?qt=" . $qt);

$list->process();

$page = new Page("queries");

require "include/mis_page_actions.php";

$page->header();
if($qt == 6 or $qt == 31) {
	$page->title("Project Queries - Masterplan");
}
elseif($qt == 7) {
	$page->title("Project Queries - CMS Overview");
}
elseif($qt == 8) {
	$page->title("Project Queries - KL approved versus real cost");
}
elseif($qt == 9) {
	$page->title("Project Queries - Project Sheets");
}
elseif($qt == '10co') {
	$page->title("Project Queries - Production Order Planning Catalogue Orders");
}
elseif($qt == 10) {
	$page->title("Project Queries - Production Order Planning Projects");
}
elseif($qt == 11) {
	$page->title("Project Queries - Project Milestones");
}
elseif($qt == 12) {
	$page->title("Project Queries - CMS Statusreport");
}
elseif($qt == 13) {
	$page->title("Project Queries - Transportation Cost");
}
elseif($qt == 14) {
	$page->title("Project Queries - Equipment of POS Locations");
}
elseif($qt == 15) {
	$page->title("Item Queries - Delivery");
}
elseif($qt == 16) {
	$page->title("CMS Completed Projects");
}
elseif($qt == 22) {
	$page->title("Order States in Catalogue Orders and Projects");
}
elseif($qt == 30) {
	$page->title("Status Report POS Locations");
}
elseif($qt == 35) {
	$page->title("Order Queries - Masterplan");
}
elseif($qt == 40) {
	$page->title("Purchase Volume by Supplier");
}
elseif($qt == 41) {
	$page->title("Shipments by Forwarder");
}


$list->render();


?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('.delete_query').click(function(e) {
    e.preventDefault();
	

	$.nyroModalManual({
	  url: '/mis/delete_qurey.php?id=' + $(this).attr("data-id") + '&context=mis' + '&qt=<?php echo param("qt");?>'
    });

	
    return false;
  });
  
  
});
</script>


<?php

$page->footer();

?>
