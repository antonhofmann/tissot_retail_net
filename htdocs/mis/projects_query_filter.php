<?php
/********************************************************************

    projects_query_filter.php

    Filter Selection for the Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2011-07-30
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2011-07-30
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";


check_access("can_perform_queries");

require_once "include/query_get_functions.php";

/********************************************************************
    prepare all data needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("projects_queries.php");
}

$qt = 1;
if(param("qt"))
{
	$qt = param("qt");
}

$query_id = param("query_id");

$mis_query = get_query_name($query_id);
$icon = "../pictures/add_item.gif";
$link = "javascript:open_selector('')";


$filter = get_query_filter($query_id);

// create sql for filter criteria
$states = array();
$states[1] = "in progress";
$states[2] = "on hold";
$states[4] = "operating";
$states[6] = "cancelled";


$cms_states = array();
$cms_states[1] = "Logistics Completion overdue";
$cms_states[2] = "Development Completion overdue";
$cms_states[3] = "Approval overdue";

$date_bases = array();
$date_bases[1] = "Arrival Date";
$date_bases[2] = "Pickup Date";


$shipment_bases = array();
$shipment_bases[1] = "Shipments based on the arrival date";
$shipment_bases[2] = "Forecast based on the order date";

$months = array();
for($i=1;$i<10;$i++)
{
	$months["0" . $i] = "0" . $i;
}
for($i=10;$i<13;$i++)
{
	$months[$i] = $i;
}
$delivery_weeks = array();
for($i=1;$i<53;$i++)
{
	$delivery_weeks[$i] = $i;
}

$days = array();
for($i=1;$i<10;$i++)
{
	$days["0" . $i] = "0" . $i;
}
for($i=10;$i<32;$i++)
{
	$days[$i] = $i;
}

if($qt == '10co') {
	$sql_years = "select distinct YEAR(order_preferred_delivery_date) as year " .
				 "from orders " .
				 "where YEAR(order_preferred_delivery_date) > 0 and order_type = 2  ".
				 "order by YEAR(order_preferred_delivery_date) DESC";

	$sql_years2 = "select distinct YEAR(order_item_ready_for_pickup) as year " .
				  "from order_items " .
		          "left join orders on order_id = order_item_order " .
				  "where YEAR(order_item_ready_for_pickup) > 0 and order_type = 2 ".
				  "order by YEAR(order_item_ready_for_pickup) DESC";
}
elseif($qt == 10) {
	$sql_years = "select distinct YEAR(order_item_preferred_arrival_date) as year " .
				 "from orders " .
				 "left join order_items on order_id = order_item_order " . 
				 "where YEAR(order_item_preferred_arrival_date) > 0 and order_type = 1 ".
				 "order by YEAR(order_item_preferred_arrival_date) DESC ";

	$sql_years2 = "select distinct YEAR(order_item_ready_for_pickup) as year " .
				  "from order_items " .
		          "left join orders on order_id = order_item_order " .
				  "where YEAR(order_item_ready_for_pickup) > 0 and order_type = 1 ".
				  "order by YEAR(order_item_ready_for_pickup) DESC";
}
elseif($qt == 22 or $qt == 30) {
	$sql_years = "select distinct YEAR(date_created) as year " .
				"from actual_order_states " .
				"where YEAR(date_created) > 0 ".
				"order by YEAR(date_created) DESC";
}

if($qt == 7 or $qt == 8 or $qt == 12 or $qt == 13 or $qt == 16) {
	$sql_years = "select distinct YEAR(project_actual_opening_date) as year " .
				"from projects " .
				"where YEAR(project_actual_opening_date) > 0 ".
				"order by YEAR(project_actual_opening_date) DESC";

	$sql_years1 = "select distinct YEAR(project_shop_closingdate) as year " .
				  "from projects " .
				  "where YEAR(project_shop_closingdate) > 0 ".
				  "order by YEAR(project_shop_closingdate) DESC";
}




$sql_order_states = "select order_state_code, concat(order_state_code, ' ', order_state_name) as name " .
                    "from order_states ". 
					"left join order_state_groups on order_state_group_id = order_state_group " . 
					"where order_state_active = 1 and order_state_group_order_type = 1 and order_state_code < '890' " . 
					"order by order_state_code";


$sql_order_states_development = "select order_state_code, concat(order_state_code, ' ', order_state_name) as name " .
                    "from order_states ". 
					"left join order_state_groups on order_state_group_id = order_state_group " . 
					"where order_state_used_for_logistics <> 1 and order_state_group_order_type = 1 and order_state_code < '910' " . 
					"order by order_state_code";

$sql_order_states_logistic = "select order_state_code, concat(order_state_code, ' ', order_state_name) as name " .
                    "from order_states ". 
					"left join order_state_groups on order_state_group_id = order_state_group " . 
					"where order_state_used_for_logistics = 1 and order_state_group_order_type = 1 and order_state_code < '910' " . 
					"order by order_state_code";


$sql_order_states2 = "select order_state_code, concat(order_state_code, ' ', order_state_name) as name " .
                    "from order_states ". 
					"left join order_state_groups on order_state_group_id = order_state_group " . 
					"where order_state_group_order_type = 2 and order_state_code < '910' " . 
					"order by order_state_code";

$sql_client_types = "select client_type_id, client_type_code ".
					"from client_types";




$sql_product_line_subclasses = "select productline_subclass_id, productline_subclass_name ".
						 "from productline_subclasses " . 
						 "   order by productline_subclass_name";


$sql_project_kinds = "select projectkind_id, projectkind_name ".
						 "from projectkinds " .
						 "where projectkind_id > 0 " .
						 "order by projectkind_name";


$sql_pos_types = "select DISTINCT postype_id, postype_name ".
                     "from postypes " .
                     "order by postype_name";


$sql_pos_type_subclasses = "select possubclass_id, possubclass_name ".
							 "from possubclasses " .
							 "order by possubclass_name";


$sql_project_cost_types = "select project_costtype_id, project_costtype_text ".
						  "from project_costtypes " .
                          "order by project_costtype_text";

$sql_salesregion = "select salesregion_id, salesregion_name ".
               "from salesregions order by salesregion_name";

$sql_supplyingregion = "select region_id, region_name ".
						"from regions order by region_name";

$sql_country = "select DISTINCT country_id, country_name " .
			   "from posaddresses " .
			   "left join countries on country_id = posaddress_country " . 
			   "order by country_name";

$sql_store_coordinators = "select user_id, concat(user_name, ' ', user_firstname) as username " . 
                          "from users " . 
						  "left join user_roles on user_role_user = user_id " . 
						  "where user_role_role in (3, 80) " .
						  " order by username";

$sql_local_store_coordinators = "select user_id, concat(user_name, ' ', user_firstname) as username " . 
                          "from users " . 
						  "left join user_roles on user_role_user = user_id " . 
						  "where user_role_role = 10 " .
						  " order by username";

$sql_retail_operators = "select user_id, concat(user_name, ' ', user_firstname) as username " . 
                          "from users " . 
						  "left join user_roles on user_role_user = user_id " . 
						  "where user_role_role = 2 " .
						  " order by username";


$sql_dcontrs = "select user_id, concat(address_company, ' ', user_name, ' ', user_firstname) as username " . 
				  "from users " . 
				  "left join user_roles on user_role_user = user_id " . 
				  "left join addresses on address_id = user_address " . 
				  "where user_role_role = 7 " .
				  " order by username";


$sql_areas = "select posareatype_id, posareatype_name from posareatypes ". 
             "order by posareatype_name";



$sql_product_lines = "select product_line_id, product_line_name ".
                     "from product_lines " . 
                     "order by product_line_name";



$sql_items2 = "select item_id, item_code from items ". 
			 "where item_type < 3 " .
			 "order by item_code ";

$sql_item_category = "select item_category_id, item_category_name " . 
                     " from item_categories " . 
					 " order by item_category_name";


$sql_owners = "select distinct order_franchisee_address_id, " .
              " address_company " . 
			  " from orders " .
			  " left join addresses on address_id = order_franchisee_address_id " . 
			  " where order_actual_order_state_code <> '900' and order_type = 1 ".
			  " and order_franchisee_address_id > 0 and address_company <> '' " . 
			  " order by address_company";

$sql_suppliers = "select DISTINCT address_id, address_company " .
                 "from suppliers " .
				 "left join addresses on address_id = supplier_address " . 
				 "where address_type in(2, 8) " .
				 "order by address_company";



$sql_forwarders = "select DISTINCT address_id, address_company " .
                 "from addresses " .
				 "where address_type in(3) " .
				 "order by address_company";


$sql_design_objectives = "select design_objective_item_id, design_objective_item_name, " .
						 "    design_objective_group_name, postype_name " .
						 "from design_objective_items left join design_objective_groups on design_objective_item_group = design_objective_group_id " .
						 "    left join postypes on design_objective_group_postype = postype_id " . 
						 " where postype_name <> '' " . 
						 " order by postype_name, design_objective_group_name, design_objective_item_name ";


$sql_delivery_years = "select DISTINCT YEAR(order_item_arrival) as year " . 
                    "from order_items " . 
					" where order_item_type < 3 and YEAR(order_item_arrival) > 1999 and YEAR(order_item_arrival) <= " . date("Y") . 
					" order by year DESC";

$sql_order_years = "select DISTINCT YEAR(order_item_ordered) as year " . 
                    "from order_items " . 
					" where order_item_type < 3 and YEAR(order_item_ordered) > 1999 and YEAR(order_item_ordered) <= " . date("Y") . 
					" order by year DESC";

$sql_clients = "select DISTINCT order_client_address, concat(country_name, ', ', address_company) as company  " .
				   "from orders " . 
				   "left join addresses on address_id = order_client_address ".
				   "left join countries on country_id = address_country " .
				   "where address_id > 0 ".
				   "order by country_name, address_company";

//get parameters
$client_types = array();
$product_lines = array();
$product_line_subclasses = array();
$project_kinds = array();
$pos_types = array();
$pos_type_subclasses = array();
$project_cost_types = array();
$salesregions = array();
$supplyingregions = array();
$countries = array();
$store_coordinators = array();
$retail_operators = array();
$design_contractors = array();
$areas = array();
$items = array();
$from_state = 0;
$to_state = 0;
$from_state2 = 0;
$to_state2 = 0;
$detail = 0;
$visuals = 0;
$include_archive = 0;
$exclude_e_stores = 0;
$owners = array();
$suppliers = array();
$forwarders = array();
$design_objectives = array();
$clients = array();
$item_catrgories = array();


$subfy = 0;
$subfm = 0;
$subfd = 0;
$subty = 0;
$subtm = 0;
$subtd = 0;
$date_base = 1;


//check if filter is present
$sql = "select mis_query_filter from mis_queries " .
	   "where mis_query_id = " . $query_id;

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{

	$filters = array();
	$filters = unserialize($row["mis_query_filter"]);

	foreach($filters as $key=>$value)
	{
		
		if(array_key_exists("ptst", $filters))
		{
			$treatment_state = $filters["ptst"];
		}
		if(array_key_exists("fst", $filters))
		{
			$from_state = $filters["fst"];
		}
		if(array_key_exists("tst", $filters))
		{
			$to_state = $filters["tst"];
		}

		if(array_key_exists("fst2", $filters))
		{
			$from_state2 = $filters["fst2"];
		}
		if(array_key_exists("tst2", $filters))
		{
			$to_state2 = $filters["tst2"];
		}

		if(array_key_exists("fdy", $filters))
		{
			$from_year = $filters["fdy"];
		}
		if(array_key_exists("fdm", $filters))
		{
			$from_month = $filters["fdm"];
		}
		if(array_key_exists("tdy", $filters))
		{
			$to_year = $filters["tdy"];
		}
		if(array_key_exists("tdm", $filters))
		{
			$to_month = $filters["tdm"];
		}

		if(array_key_exists("fdy1", $filters))
		{
			$from_year1 = $filters["fdy1"];
		}
		if(array_key_exists("fdm1", $filters))
		{
			$from_month1 = $filters["fdm1"];
		}
		if(array_key_exists("tdy1", $filters))
		{
			$to_year1 = $filters["tdy1"];
		}
		if(array_key_exists("tdm1", $filters))
		{
			$to_month1 = $filters["tdm1"];
		}

		if(array_key_exists("fdy2", $filters))
		{
			$from_year2 = $filters["fdy2"];
		}
		if(array_key_exists("fdm2", $filters))
		{
			$from_month2 = $filters["fdm2"];
		}
		if(array_key_exists("tdy2", $filters))
		{
			$to_year2 = $filters["tdy2"];
		}
		if(array_key_exists("tdm2", $filters))
		{
			$to_month2 = $filters["tdm2"];
		}

		if(array_key_exists("clients", $filters))
		{
			$clients = explode("-", $filters["clients"]);
		}
		
		if(array_key_exists("clt", $filters))
		{
			$client_types = explode("-", $filters["clt"]);
		}
		if(array_key_exists("pl", $filters))
		{
			$product_lines = explode("-", $filters["pl"]);
		}

		

		if(array_key_exists("plsc", $filters))
		{
			$product_line_subclasses = explode("-", $filters["plsc"]);
		}
		if(array_key_exists("pk", $filters))
		{
			$project_kinds = explode("-", $filters["pk"]);
		}
		if(array_key_exists("pt", $filters))
		{
			$pos_types = explode("-", $filters["pt"]);
		}
		if(array_key_exists("ptsc", $filters))
		{
			$pos_type_subclasses = explode("-", $filters["ptsc"]);
		}
		if(array_key_exists("pct", $filters))
		{
			$project_cost_types = explode("-", $filters["pct"]);
		}
		if(array_key_exists("gr", $filters))
		{
			$salesregions = explode("-", $filters["gr"]);
		}
		if(array_key_exists("areas", $filters))
		{
			$areas = explode("-", $filters["areas"]);
		}
		if(array_key_exists("items", $filters))
		{
			$items = explode("-", $filters["items"]);
		}
		if(array_key_exists("re", $filters))
		{
			$supplyingregions = explode("-", $filters["re"]);
		}
		if(array_key_exists("co", $filters))
		{
			$countries = explode("-", $filters["co"]);
		}
		if(array_key_exists("rtc", $filters))
		{
			$store_coordinators = explode("-", $filters["rtc"]);
		}
		if(array_key_exists("lrtc", $filters))
		{
			$local_store_coordinators = explode("-", $filters["lrtc"]);
		}
		if(array_key_exists("rto", $filters))
		{
			$retail_operators = explode("-", $filters["rto"]);
		}
		if(array_key_exists("dcontr", $filters))
		{
			$design_contractors = explode("-", $filters["dcontr"]);
		}
		if(array_key_exists("detail", $filters))
		{
			$detail = $filters["detail"];
		}
		if(array_key_exists("idvi", $filters))
		{
			$visuals = $filters["idvi"];
		}
		if(array_key_exists("arch", $filters))
		{
			$include_archive = $filters["arch"];
		}

		if(array_key_exists("exclude_e_stores", $filters))
		{
			$exclude_e_stores = $filters["exclude_e_stores"];
		}
		if(array_key_exists("subfy", $filters))
		{
			$subfy = $filters["subfy"];
		}
		if(array_key_exists("subfm", $filters))
		{
			$subfm = $filters["subfm"];
		}
		if(array_key_exists("subfd", $filters))
		{
			$subfd = $filters["subfd"];
		}
		if(array_key_exists("subty", $filters))
		{
			$subty = $filters["subty"];
		}
		if(array_key_exists("subtm", $filters))
		{
			$subtm = $filters["subtm"];
		}
		if(array_key_exists("subtd", $filters))
		{
			$subtd = $filters["subtd"];
		}
		
		$cms_state = 0;
		if(array_key_exists("cmsst", $filters))
		{
			$cms_state = $filters["cmsst"];
		}

		if(array_key_exists("owners", $filters))
		{
			$owners = explode("-", $filters["owners"]);
		}

		if(array_key_exists("supp", $filters))
		{
			$suppliers = explode("-", $filters["supp"]);
		}

		if(array_key_exists("forw", $filters))
		{
			$forwarders = explode("-", $filters["forw"]);
		}

		if(array_key_exists("dos", $filters))
		{
			$design_objectives = explode("-", $filters["dos"]);
		}

		if(array_key_exists("dba", $filters))
		{
			$date_base = $filters["dba"];
		}


		if(array_key_exists("icat", $filters))
		{
			$item_categories = explode("-", $filters["icat"]);
		}
	}
}

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("mis_queries", "query_generator");

$form->add_hidden("query_id", param("query_id"));
$form->add_hidden("qt", $qt);
$form->add_section($mis_query["name"]);

$form->add_section(" ");
$form->add_comment("Please select the the filter criteria of your query.");

$link = "javascript:open_selector('')";

$form->add_hidden("subfd");
$form->add_hidden("subtd");



if($qt == 35) {
	$form->add_section("Order States");
	
	$form->add_list("from_state", "From Status", $sql_order_states2, SUBMIT, $from_state);
	$form->add_list("to_state", "To Status", $sql_order_states2, SUBMIT, $to_state);
	
	$form->add_hidden("subfy");
	$form->add_hidden("subfm");
	$form->add_hidden("subty");
	$form->add_hidden("subtm");


	$form->add_hidden("treatment_state");
	$form->add_hidden("arch");
	$form->add_hidden("fdy");
	$form->add_hidden("fdm");
	$form->add_hidden("tdy");
	$form->add_hidden("tdm");
	$form->add_hidden("idvi");

	$form->add_hidden("fdy1");
	$form->add_hidden("fdm1");
	$form->add_hidden("tdy1");
	$form->add_hidden("tdm1");
	$form->add_hidden("fdy2");
	$form->add_hidden("fdm2");
	$form->add_hidden("tdy2");
	$form->add_hidden("tdm2");

	$form->add_hidden("detail");
	
}
elseif($qt == 6) {
	$form->add_section("Project States");
	$form->add_list("treatment_state", "Project's Treatment State", $states, SUBMIT, $treatment_state);
	$form->add_list("from_state", "From Status", $sql_order_states, SUBMIT, $from_state);
	$form->add_list("to_state", "To Status", $sql_order_states, SUBMIT, $to_state);
	$form->add_hidden("fdy");
	$form->add_hidden("fdm");
	$form->add_hidden("tdy");
	$form->add_hidden("tdm");
	$form->add_section("Visuals");
	$form->add_checkbox("idvi", "Show only POS with Visuals", $visuals, SUBMIT, "Visuals");

	$form->add_hidden("fdy1");
	$form->add_hidden("fdm1");
	$form->add_hidden("tdy1");
	$form->add_hidden("tdm1");
	$form->add_hidden("fdy2");
	$form->add_hidden("fdm2");
	$form->add_hidden("tdy2");
	$form->add_hidden("tdm2");

	//$form->add_section("List Parameters");
	//$form->add_checkbox("detail", "Show Details for Suppliers and Forwarders", $detail, SUBMIT, "Order and Delivery");
	$form->add_hidden("detail", 0);

	

}
elseif($qt == 31) {
	$form->add_section("Project States");
	$form->add_list("treatment_state", "Project's Treatment State", $states, SUBMIT, $treatment_state);

	
	//$form->add_list("from_state", "From Development Status", $sql_order_states_development, SUBMIT, $from_state);
	//$form->add_list("to_state", "To Development Status", $sql_order_states_development, SUBMIT, $to_state);
	
	//$form->add_list("from_state2", "From Logistic Status", $sql_order_states_logistic, SUBMIT, $from_state2);
	//$form->add_list("to_state2", "To Logistic Status", $sql_order_states_logistic, SUBMIT, $to_state2);

	$form->add_list("from_state", "From Status", $sql_order_states, SUBMIT, $from_state);
	$form->add_list("to_state", "To Status", $sql_order_states, SUBMIT, $to_state);

	$form->add_hidden("from_state2");
	$form->add_hidden("to_state2");

	$form->add_hidden("fdy");
	$form->add_hidden("fdm");
	$form->add_hidden("tdy");
	$form->add_hidden("tdm");
	$form->add_section("Visuals");
	$form->add_checkbox("idvi", "Show only POS with Visuals", $visuals, SUBMIT, "Visuals");

	$form->add_hidden("fdy1");
	$form->add_hidden("fdm1");
	$form->add_hidden("tdy1");
	$form->add_hidden("tdm1");
	$form->add_hidden("fdy2");
	$form->add_hidden("fdm2");
	$form->add_hidden("tdy2");
	$form->add_hidden("tdm2");

	//$form->add_section("List Parameters");
	//$form->add_checkbox("detail", "Show Details for Suppliers and Forwarders", $detail, SUBMIT, "Order and Delivery");
	$form->add_hidden("detail", 0);
	
	

}
elseif($qt == '10co')
{
	$form->add_section("Order States");
	$form->add_hidden("treatment_state");
	$form->add_list("from_state", "From Status", $sql_order_states2, SUBMIT, $from_state);
	$form->add_list("to_state", "To Status", $sql_order_states2, SUBMIT, $to_state);
	
	$form->add_section("Preferred Arrival");
	$form->add_list("fdy", "From", $sql_years, SUBMIT, $from_year);
	$form->add_list("fdm", "", $months, SUBMIT, $from_month);
	$form->add_list("tdy", "To", $sql_years, SUBMIT, $to_year);
	$form->add_list("tdm", "", $months, SUBMIT, $to_month);

	$form->add_hidden("fdy1");
	$form->add_hidden("fdm1");
	$form->add_hidden("tdy1");
	$form->add_hidden("tdm1");
	

	$form->add_section("Ready for Pickup");
	$form->add_list("fdy2", "From", $sql_years2, SUBMIT, $from_year2);
	$form->add_list("fdm2", "", $months, SUBMIT, $from_month2);
	$form->add_list("tdy2", "To", $sql_years2, SUBMIT, $to_year2);
	$form->add_list("tdm2", "", $months, SUBMIT, $to_month2);


	$form->add_hidden("detail");
	$form->add_hidden("idvi");

	
}
elseif($qt == 10)
{
	$form->add_section("Project States");
	$form->add_hidden("treatment_state");
	$form->add_list("from_state", "From Status", $sql_order_states, SUBMIT, $from_state);
	$form->add_list("to_state", "To Status", $sql_order_states, SUBMIT, $to_state);
	
	
	
	$form->add_section("Preferred Arrival");
	$form->add_list("fdy", "From", $sql_years, SUBMIT, $from_year);
	$form->add_list("fdm", "", $months, SUBMIT, $from_month);
	$form->add_list("tdy", "To", $sql_years, SUBMIT, $to_year);
	$form->add_list("tdm", "", $months, SUBMIT, $to_month);

	$form->add_section("Agreed Opening Date");
	$form->add_list("fdy1", "From", $sql_years, SUBMIT, $from_year1);
	$form->add_list("fdm1", "", $months, SUBMIT, $from_month1);
	$form->add_list("tdy1", "To", $sql_years, SUBMIT, $to_year1);
	$form->add_list("tdm1", "", $months, SUBMIT, $to_month1);
	

	$form->add_section("Ready for Pickup or Pickup Date");
	$form->add_list("fdy2", "From", $sql_years2, SUBMIT, $from_year2);
	$form->add_list("fdm2", "", $months, SUBMIT, $from_month2);
	$form->add_list("tdy2", "To", $sql_years2, SUBMIT, $to_year2);
	$form->add_list("tdm2", "", $months, SUBMIT, $to_month2);


	$form->add_hidden("detail");
	$form->add_section("Visuals");
	$form->add_checkbox("idvi", "Show only POS with Visuals", $visuals, SUBMIT, "Visuals");

	
}
elseif($qt == 11)
{
	$form->add_section("Project States");
	$form->add_hidden("treatment_state");
	$form->add_list("from_state", "From Status", $sql_order_states, SUBMIT, $from_state);
	$form->add_list("to_state", "To Status", $sql_order_states, SUBMIT, $to_state);
	$form->add_hidden("fdy");
	$form->add_hidden("fdm");
	$form->add_hidden("tdy");
	$form->add_hidden("tdm");
	$form->add_hidden("fdy1");
	$form->add_hidden("fdm1");
	$form->add_hidden("tdy1");
	$form->add_hidden("tdm1");
	$form->add_hidden("fdy2");
	$form->add_hidden("fdm2");
	$form->add_hidden("tdy2");
	$form->add_hidden("tdm2");


	$form->add_hidden("detail");
	$form->add_section("Archived Projects");
	$form->add_checkbox("arch", "Include projects from archive", $include_archive, SUBMIT, "Archive");
	$form->add_hidden("idvi");

	$sql_submitted = "select distinct YEAR(date_created) as year " .
              "from projects " .
              "where YEAR(date_created) > 0 ".
              "order by YEAR(date_created) DESC";


	$months = array();
	for($i=1;$i<10;$i++)
	{
		$months["0" . $i] = "0" . $i;
	}
	for($i=10;$i<13;$i++)
	{
		$months[$i] = $i;
	}

	$form->add_section("LN posted or AF/CER posted");
	$form->add_list("subfy", "Posted from", $sql_submitted, SUBMIT, $subfy);
	$form->add_list("subfm", "", $months, SUBMIT, $subfm);
	$form->add_list("subty", "Posted to", $sql_submitted, SUBMIT, $subty);
	$form->add_list("subtm", "", $months, SUBMIT, $subtm);

	
}
elseif($qt == 15) {
	$form->add_hidden("treatment_state");
	$form->add_hidden("from_state");
	$form->add_hidden("to_state");
	//$form->add_hidden("fdm");
	//$form->add_hidden("tdm");
	$form->add_hidden("detail");
	$form->add_hidden("idvi");
	$form->add_hidden("fdy1");
	$form->add_hidden("fdm1");
	$form->add_hidden("tdy1");
	$form->add_hidden("tdm1");
	$form->add_hidden("fdy2");
	$form->add_hidden("fdm2");
	$form->add_hidden("tdy2");
	$form->add_hidden("tdm2");

	

}
elseif($qt == 30)
{
	$form->add_Section("Selected Filter Criteria");	
	$selected_salesregions = "";
	if(count($salesregions) > 0)
	{
		$res = mysql_query($sql_salesregion) or dberror($sql_salesregion);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["salesregion_id"], $salesregions))
			{
				$selected_salesregions .= $row["salesregion_name"] . ", ";
			}
		}
		$selected_salesregions = substr($selected_salesregions, 0, strlen($selected_salesregions) - 2);
	}
	$form->add_label_selector("salesregions", "Geographical Regions", 0, $selected_salesregions, $icon, $link);


	$selected_countries = "";
	if(count($countries) > 0)
	{
		$res = mysql_query($sql_country) or dberror($sql_country);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["country_id"], $countries))
			{
				$selected_countries .= $row["country_name"] . ", ";
			}
		}
		$selected_countries = substr($selected_countries, 0, strlen($selected_countries) - 2);
	}
	$form->add_label_selector("countries", "Countries", 0, $selected_countries, $icon, $link);
}

if($qt != 11 and $qt != 22 and $qt != 35) {
	$form->add_hidden("arch");
	$form->add_hidden("subfy");
	$form->add_hidden("subfm");
	$form->add_hidden("subty");
	$form->add_hidden("subtm");
}


if($qt != 22 and $qt != 30) {
	$form->add_Section("Selected Filter Criteria");	
}

if($qt != 30) {
	$form->add_hidden("exclude_e_stores");
}

if($qt == 15)
{
	
	
	$form->add_list("fdy", "From Year", $sql_delivery_years, SUBMIT, $from_year);
	$form->add_list("fdm", "From Week", $delivery_weeks, SUBMIT, $from_month);


	$form->add_list("tdy", "To Year", $sql_delivery_years, SUBMIT, $to_year);
	$form->add_list("tdm", "To Week", $delivery_weeks, SUBMIT, $to_month);

	$form->add_list("dba", "Date Base", $date_bases, SUBMIT, $date_base);

	$selected_countries = "";
	if(count($countries) > 0)
	{
		$res = mysql_query($sql_country) or dberror($sql_country);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["country_id"], $countries))
			{
				$selected_countries .= $row["country_name"] . ", ";
			}
		}
		$selected_countries = substr($selected_countries, 0, strlen($selected_countries) - 2);
	}
	$form->add_label_selector("countries", "Countries", 0, $selected_countries, $icon, $link);

}
elseif($qt == 40 or $qt == 41) {
	

	if($qt == 40) {
		$form->add_list("from_state2", "From Logistic Status", $sql_order_states_logistic, SUBMIT, $from_state2);
		$form->add_list("to_state2", "To Logistic Status", $sql_order_states_logistic, SUBMIT, $to_state2);
	}
	
	if($qt == 40) {
		

		$form->add_list("fdy", "Ordered From Year", $sql_order_years, SUBMIT, $from_year);
		$form->add_list("fdm", "Ordered From Month", $months, SUBMIT, $from_month);
		$form->add_list("tdy", "Ordered To Year", $sql_order_years, SUBMIT, $to_year);
		$form->add_list("tdm", "Ordered To Month", $months, SUBMIT, $to_month);

		$form->add_hidden("dba");
	}
	else {
		
		if(!$date_base){$date_base = 1;}
		
		if($data_base == 2 or param("dba") == 2) {
			$form->add_list("dba", "Data Base", $shipment_bases, SUBMIT, $date_base);
			$form->add_list("fdy", "Ordered From Year", $sql_delivery_years, SUBMIT, $from_year);
			$form->add_list("fdm", "Ordered From Month", $months, SUBMIT, $from_month);
			$form->add_list("tdy", "Ordered To Year", $sql_delivery_years, SUBMIT, $to_year);
			$form->add_list("tdm", "Ordered To Month", $months, SUBMIT, $to_month);
		}
		else {
			$form->add_list("dba", "Data Base", $shipment_bases, SUBMIT, $date_base);
			$form->add_list("fdy", "Delivered From Year", $sql_delivery_years, SUBMIT, $from_year);
			$form->add_list("fdm", "Delivered From Month", $months, SUBMIT, $from_month);
			$form->add_list("tdy", "Delivered To Year", $sql_delivery_years, SUBMIT, $to_year);
			$form->add_list("tdm", "Delivered To Month", $months, SUBMIT, $to_month);
		}
	}

	
	$form->add_hidden("treatment_state");
	$form->add_hidden("treatment_state");
	$form->add_hidden("from_state");
	$form->add_hidden("to_state");
	//$form->add_hidden("fdm");
	//$form->add_hidden("tdm");
	$form->add_hidden("detail");
	$form->add_hidden("idvi");
	$form->add_hidden("fdy1");
	$form->add_hidden("fdm1");
	$form->add_hidden("tdy1");
	$form->add_hidden("tdm1");
	$form->add_hidden("fdy2");
	$form->add_hidden("fdm2");
	$form->add_hidden("tdy2");
	$form->add_hidden("tdm2");
}
else
{
	$form->add_hidden("dba");
}

if($qt == 14 or $qt == 6 or $qt == 31 or $qt == 7 
	or $qt == 15 or $qt == 16 or $qt == '10co' 
	or $qt == 10 or $qt == 35 or $qt == 22
	or $qt == 40 or $qt == 41)
{
	$selected_suppliers = "";
	if(count($suppliers) > 0)
	{
		$res = mysql_query($sql_suppliers) or dberror($sql_suppliers);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["address_id"], $suppliers))
			{
				$selected_suppliers .= $row["address_company"] . ", ";
			}
		}
		$selected_suppliers = substr($selected_suppliers, 0, strlen($selected_suppliers) - 2);
	}
	$form->add_label_selector("supp", "Suppliers", 0, $selected_suppliers, $icon, $link);
}

if($qt == 40 or $qt == 41)
{
	$selected_forwarders = "";
	if(count($forwarders) > 0)
	{
		$res = mysql_query($sql_forwarders) or dberror($sql_forwarders);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["address_id"], $forwarders))
			{
				$selected_forwarders .= $row["address_company"] . ", ";
			}
		}
		$selected_forwarders = substr($selected_forwarders, 0, strlen($selected_forwarders) - 2);
	}
	$form->add_label_selector("forw", "Forwarders", 0, $selected_forwarders, $icon, $link);
}

if($qt != 11 and $qt != 14 and $qt != 15 and $qt != 22)
{

	$selected_client_types = "";
	if(count($client_types) > 0)
	{
		$res = mysql_query($sql_client_types) or dberror($sql_clientypes);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["client_type_id"], $client_types))
			{
				$selected_client_types .= $row["client_type_code"] . ", ";
			}
		}
		$selected_client_types = substr($selected_client_types, 0, strlen($selected_client_types) - 2);
	}
	$form->add_label_selector("client_types", "Client Type", 0, $selected_client_types, $icon, $link);
}
if($qt != 11 and $qt != '10co' and $qt != 22 and $qt != 35 and $qt != 40 and $qt != 41)
{
	$selected_pls = "";
	if(count($product_lines) > 0)
	{
		$res = mysql_query($sql_product_lines) or dberror($sql_product_lines);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["product_line_id"], $product_lines))
			{
				$selected_pls .= $row["product_line_name"] . ", ";
			}
		}
		$selected_pls = substr($selected_pls, 0, strlen($selected_pls) - 2);
	}
	$form->add_label_selector("pls", "Product Lines", 0, $selected_pls, $icon, $link);
}

if($qt != 11 and $qt != 14 and $qt != 15 and $qt != '10co' and $qt != 22 and $qt != 35  and $qt != 40 and $qt != 41)
{
	$selected_plscs = "";
	if(count($product_line_subclasses) > 0)
	{
		$res = mysql_query($sql_product_line_subclasses) or dberror($sql_product_line_subclasses);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["productline_subclass_id"], $product_line_subclasses))
			{
				$selected_plscs .= $row["productline_subclass_name"] . ", ";
			}
		}
		$selected_plscs = substr($selected_plscs, 0, strlen($selected_plscs) - 2);
	}
	$form->add_label_selector("plscs", "Product Line Subclasses", 0, $selected_plscs, $icon, $link);
}

if($qt != 14 and $qt != 15 and $qt != '10co' and $qt != 22 and $qt != 35  and $qt != 40 and $qt != 41)
{
	$selected_pks = "";
	if(count($project_kinds) > 0)
	{
		$res = mysql_query($sql_project_kinds) or dberror($sql_project_kinds);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["projectkind_id"], $project_kinds))
			{
				$selected_pks .= $row["projectkind_name"] . ", ";
			}
		}
		$selected_pks = substr($selected_pks, 0, strlen($selected_pks) - 2);
	}
	$form->add_label_selector("projectkinds", "Project Types", 0, $selected_pks, $icon, $link);

	$selected_pts = "";
	if(count($pos_types) > 0)
	{
		$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["postype_id"], $pos_types))
			{
				$selected_pts .= $row["postype_name"] . ", ";
			}
		}
		$selected_pts = substr($selected_pts, 0, strlen($selected_pts) - 2);
	}
	$form->add_label_selector("pts", "POS Types", 0, $selected_pts, $icon, $link);
}

if($qt != 11 and $qt != 14 and $qt != 15 and $qt != '10co' and $qt != 22 and $qt != 35  and $qt != 40 and $qt != 41)
{
	$selected_pos_type_subclasses = "";
	if(count($pos_type_subclasses) > 0)
	{
		if(in_array('ns', $pos_type_subclasses))
		{
			$selected_pos_type_subclasses .= "POS without a subclass" . ", ";
		}
		
		$res = mysql_query($sql_pos_type_subclasses) or dberror($sql_pos_type_subclasses);
		while($row = mysql_fetch_assoc($res))
		{
			
			if(in_array($row["possubclass_id"], $pos_type_subclasses))
			{
				$selected_pos_type_subclasses .= $row["possubclass_name"] . ", ";
			}
		}
		$selected_pos_type_subclasses = substr($selected_pos_type_subclasses, 0, strlen($selected_pos_type_subclasses) - 2);
	}
	$form->add_label_selector("pos_type_subclasses", "POS Type Subclasses", 0, $selected_pos_type_subclasses, $icon, $link);
}

if($qt != 14 and $qt != 15 and $qt != '10co' and $qt != 22 and $qt != 35  and $qt != 40 and $qt != 41)
{
	$selected_pcts = "";
	if(count($project_cost_types) > 0)
	{
		$res = mysql_query($sql_project_cost_types) or dberror($sql_project_cost_types);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["project_costtype_id"], $project_cost_types))
			{
				$selected_pcts .= $row["project_costtype_text"] . ", ";
			}
		}
		$selected_pcts = substr($selected_pcts, 0, strlen($selected_pcts) - 2);
	}
	$form->add_label_selector("pcts", "Legal Types", 0, $selected_pcts, $icon, $link);
}


if($qt == 11)
{
	$selected_salesregions = "";
	if(count($salesregions) > 0)
	{
		$res = mysql_query($sql_salesregion) or dberror($sql_salesregion);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["salesregion_id"], $salesregions))
			{
				$selected_salesregions .= $row["salesregion_name"] . ", ";
			}
		}
		$selected_salesregions = substr($selected_salesregions, 0, strlen($selected_salesregions) - 2);
	}
	$form->add_label_selector("salesregions", "Geographical Regions", 0, $selected_salesregions, $icon, $link);
}
if($qt != 11 and $qt != 14 and $qt != 15 and $qt != 22 and $qt != 30)
{
	$selected_salesregions = "";
	if(count($salesregions) > 0)
	{
		$res = mysql_query($sql_salesregion) or dberror($sql_salesregion);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["salesregion_id"], $salesregions))
			{
				$selected_salesregions .= $row["salesregion_name"] . ", ";
			}
		}
		$selected_salesregions = substr($selected_salesregions, 0, strlen($selected_salesregions) - 2);
	}
	$form->add_label_selector("salesregions", "Geographical Regions", 0, $selected_salesregions, $icon, $link);


	$selected_supplyingregions = "";
	if(count($supplyingregions) > 0)
	{
		$res = mysql_query($sql_supplyingregion) or dberror($sql_supplyingregion);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["region_id"], $supplyingregions))
			{
				$selected_supplyingregions .= $row["region_name"] . ", ";
			}
		}
		$selected_supplyingregions = substr($selected_supplyingregions, 0, strlen($selected_supplyingregions) - 2);
	}
	$form->add_label_selector("supplyingregions", "Supplied Regions", 0, $selected_supplyingregions, $icon, $link);

	$selected_countries = "";
	if(count($countries) > 0)
	{
		$res = mysql_query($sql_country) or dberror($sql_country);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["country_id"], $countries))
			{
				$selected_countries .= $row["country_name"] . ", ";
			}
		}
		$selected_countries = substr($selected_countries, 0, strlen($selected_countries) - 2);
	}
	$form->add_label_selector("countries", "Countries", 0, $selected_countries, $icon, $link);

	
	
	
	
	if($qt == 6 or $qt == 31)
	{
		$selected_owners = "";
		if(count($owners) > 0)
		{
			$res = mysql_query($sql_owners) or dberror($sql_owners);
			while($row = mysql_fetch_assoc($res))
			{
				if(in_array($row["order_franchisee_address_id"], $owners))
				{
					$selected_owners .= $row["address_company"] . ", ";
				}
			}
			$selected_owners = substr($selected_owners, 0, strlen($selected_owners) - 2);
		}
		$form->add_label_selector("owners", "Owner/Franchisee", 0, $selected_owners, $icon, $link);
	}
	
	
	if($qt != '10co' and $qt != 22 and $qt != 35 and $qt != 40 and $qt != 41)
	{
	
		$selected_store_coordinators = "";
		if(count($store_coordinators) > 0)
		{
			$res = mysql_query($sql_store_coordinators) or dberror($sql_store_coordinators);
			while($row = mysql_fetch_assoc($res))
			{
				if(in_array($row["user_id"], $store_coordinators))
				{
					$selected_store_coordinators .= $row["username"] . ", ";
				}
			}
			$selected_store_coordinators = substr($selected_store_coordinators, 0, strlen($selected_store_coordinators) - 2);
		}
		$form->add_label_selector("storecoordinators", "Project Leaders", 0, $selected_store_coordinators, $icon, $link);


		$selected_local_store_coordinators = "";
		if(count($local_store_coordinators) > 0)
		{
			$res = mysql_query($sql_local_store_coordinators) or dberror($sql_local_store_coordinators);
			while($row = mysql_fetch_assoc($res))
			{
				if(in_array($row["user_id"], $local_store_coordinators))
				{
					$selected_local_store_coordinators .= $row["username"] . ", ";
				}
			}
			$selected_local_store_coordinators = substr($selected_local_store_coordinators, 0, strlen($selected_local_store_coordinators) - 2);
		}
		$form->add_label_selector("local_storecoordinators", "Local Project Leaders", 0, $selected_local_store_coordinators, $icon, $link);



		$selected_areas = "";
		if(count($areas) > 0)
		{
			$res = mysql_query($sql_areas) or dberror($sql_areas);
			while($row = mysql_fetch_assoc($res))
			{
				if(in_array($row["posareatype_id"], $areas))
				{
					$selected_areas .= $row["posareatype_name"] . ", ";
				}
			}
			$selected_areas = substr($selected_areas, 0, strlen($selected_areas) - 2);
		}
		$form->add_label_selector("areas", "Neighbourhood Locations", 0, $selected_areas, $icon, $link);
	}
}



if($qt == 11 or $qt == 14) // milestones, equipment of pos
{
	$selected_countries = "";
	if(count($countries) > 0)
	{
		$res = mysql_query($sql_country) or dberror($sql_country);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["country_id"], $countries))
			{
				$selected_countries .= $row["country_name"] . ", ";
			}
		}
		$selected_countries = substr($selected_countries, 0, strlen($selected_countries) - 2);
	}
	$form->add_label_selector("countries", "Countries", 0, $selected_countries, $icon, $link);
}

if($qt == 10 or $qt == 14 or $qt == 15) // production order planning or POS Equipment
{
	$selected_item_categories = "";
	if(count($item_categories) > 0)
	{
		$res = mysql_query($sql_item_category) or dberror($sql_item_category);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["item_category_id"], $item_categories))
			{
				$selected_item_categories .= $row["item_category_name"] . ", ";
			}
		}
		$selected_item_categories = substr($selected_item_categories, 0, strlen($selected_item_categories) - 2);
	}
	$form->add_label_selector("item_categories", "Item Categories", 0, $selected_item_categories, $icon, $link);
	
	
	$selected_items = "";
	$number_of_items = 0;
	if(count($items) > 0)
	{
		$res = mysql_query($sql_items2) or dberror($sql_items2);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["item_id"], $items))
			{
				$selected_items .= $row["item_code"] . ", ";

				$number_of_items++;
				if($number_of_items == 8)
				{
					$selected_items .= "<br />";
					$number_of_items = 0;
				}

			}
		}
		$selected_items = substr($selected_items, 0, strlen($selected_items) - 2);
	}
	$form->add_label_selector("items", "Items", RENDER_HTML, $selected_items, $icon, $link);
}

if($qt == 9 or $qt == 6 or $qt == 31 or $qt == 35) {
	
	if($qt == 9) {
		$form->add_hidden("treatment_state");
		$form->add_hidden("from_state");
		$form->add_hidden("to_state");
	}

	$form->add_hidden("fdy");
	$form->add_hidden("fdm");
	$form->add_hidden("tdy");
	$form->add_hidden("tdm");
	$form->add_hidden("fdy1");
	$form->add_hidden("fdm1");
	$form->add_hidden("tdy1");
	$form->add_hidden("tdm1");

	$form->add_hidden("fdy2");
	$form->add_hidden("fdm2");
	$form->add_hidden("tdy2");
	$form->add_hidden("tdm2");

	
	if($qt == 9) {
		$form->add_hidden("detail");
	}


	
	$selected_retail_operators = "";
	if(count($selected_retail_operators) > 0)
	{
		$res = mysql_query($sql_retail_operators) or dberror($sql_retail_operators);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["user_id"], $retail_operators))
			{
				$selected_retail_operators .= $row["username"] . ", ";
			}
		}
		$selected_retail_operators = substr($selected_retail_operators, 0, strlen($selected_retail_operators) - 2);
	}
	$form->add_label_selector("retailoperators", "Logistics Coordinators", 0, $selected_retail_operators, $icon, $link);


	
	if($qt == 31) {
		$selected_dcontrs = "";
		if(count($selected_dcontrs) > 0)
		{
			$res = mysql_query($sql_dcontrs) or dberror($sql_dcontrs);
			while($row = mysql_fetch_assoc($res))
			{
				if(in_array($row["user_id"], $design_contractors))
				{
					$selected_dcontrs .= $row["username"] . ", ";
				}
			}
			$selected_dcontrs = substr($selected_dcontrs, 0, strlen($selected_dcontrs) - 2);
		}
		$form->add_label_selector("designcontractors", "Design Contractors", 0, $selected_dcontrs, $icon, $link);
	}
		

	if($qt == 9) {
		$form->add_section("Visuals");
		$form->add_checkbox("idvi", "Show only POS with Visuals", $visuals, SUBMIT, "Visuals");
	}
}
elseif($qt == 12 or $qt == 22)
{
	$selected_retail_operators = "";
	if(count($selected_retail_operators) > 0)
	{
		$res = mysql_query($sql_retail_operators) or dberror($sql_retail_operators);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["user_id"], $retail_operators))
			{
				$selected_retail_operators .= $row["username"] . ", ";
			}
		}
		$selected_retail_operators = substr($selected_retail_operators, 0, strlen($selected_retail_operators) - 2);
	}
	$form->add_label_selector("retailoperators", "Logistics Coordinators", 0, $selected_retail_operators, $icon, $link);
}
else
{
	$form->add_hidden("retailoperators");
}


if($qt == 22) // clients
{
	$selected_clients = "";
	if(count($clients) > 0)
	{
		$res = mysql_query($sql_clients) or dberror($sql_clients);
		$i = 1;
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["order_client_address"], $clients))
			{
				$selected_clients .= $row["company"] . ", ";
				$i++;
				if($i == 3)
				{
					$i = 1;
					$selected_clients .= '<br />';

				}
			}
		}
		$selected_clients = substr($selected_clients, 0, strlen($selected_clients) - 2);
	}
	$form->add_label_selector("clients", "Clients", RENDER_HTML, $selected_clients, $icon, $link);
}
elseif($qt != 22) // clients
{
	$form->add_hidden("clients");
}


if($qt == 7 or $qt == 8 or $qt == 13 or $qt == 16) {
	$form->add_hidden("treatment_state");
	$form->add_hidden("from_state");
	$form->add_hidden("to_state");
	$form->add_section("Actual Opening Year");
	$form->add_list("fdy", "Opened From", $sql_years, SUBMIT, $from_year);
	$form->add_hidden("fdm");
	$form->add_list("tdy", "Opened To", $sql_years, SUBMIT, $to_year);
	$form->add_hidden("tdm");
	$form->add_hidden("detail");

	$form->add_section("Closing Year");
	$form->add_list("fdy1", "Closed From", $sql_years1, SUBMIT, $from_year1);
	$form->add_hidden("fdm1");
	$form->add_list("tdy1", "Closed To", $sql_years1, SUBMIT, $to_year1);
	$form->add_hidden("tdm1");

	$form->add_hidden("fdy2");
	$form->add_hidden("fdm2");
	$form->add_hidden("tdy2");
	$form->add_hidden("tdm2");



	$form->add_section("Visuals");
	$form->add_checkbox("idvi", "Show only POS with Visuals", $visuals, SUBMIT, "Visuals");
}


if($qt == 12) {
	$form->add_hidden("treatment_state");
	$form->add_hidden("from_state");
	$form->add_hidden("to_state");

	$form->add_section("Actual Opening Year");
	$form->add_list("fdy", "Opened From", $sql_years, SUBMIT, $from_year);
	$form->add_hidden("fdm");

	$form->add_hidden("fdm");
	$form->add_list("tdy", "Opened To", $sql_years, SUBMIT, $to_year);
	$form->add_hidden("tdm");
	$form->add_hidden("detail");

	$form->add_hidden("fdy1");
	$form->add_hidden("tdy1");
	$form->add_hidden("fdm1");
	$form->add_hidden("tdm1");

	$form->add_hidden("fdy2");
	$form->add_hidden("fdm2");
	$form->add_hidden("tdy2");
	$form->add_hidden("tdm2");

	$form->add_hidden("idvi");

	$form->add_section("CMS State");
	$form->add_list("cmsst", "CMS State", $cms_states, SUBMIT, $cms_state);
}
else
{
	$form->add_hidden("cmsst");
}

if($qt == 6 or $qt == 31) 
{
	$selected_design_objectives = "";
	if(count($design_objectives) > 0)
	{
		$res = mysql_query($sql_design_objectives) or dberror($sql_design_objectives);
		while($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["design_objective_item_id"], $design_objectives))
			{
				$selected_design_objectives .= $row["postype_name"] . " - " . $row["design_objective_group_name"] . " - " . $row["design_objective_item_name"] . ", ";
			}
		}
		$selected_design_objectives = substr($selected_design_objectives, 0, strlen($selected_design_objectives) - 2);
	}
	$form->add_label_selector("design_objectives", "Design Objectives", 0, $selected_design_objectives, $icon, $link);
}




if($qt == 22 ) // order states
{
	$form->add_section("Order State in Projects");
	$form->add_list("from_state", "From Status", $sql_order_states, SUBMIT, $from_state);
	$form->add_list("from_state2", "To Status", $sql_order_states, SUBMIT, $from_state2);
	
	$form->add_section("Order State in Catalogue Orders");
	$form->add_list("to_state", "From State", $sql_order_states2, SUBMIT, $to_state);
	$form->add_list("to_state2", "To State", $sql_order_states2, SUBMIT, $to_state2);
	
	
	
	$form->add_section("Time Period");
	$form->add_hidden("treatment_state");
	$form->add_hidden("idvi");
	$form->add_list("fdy", "From Year", $sql_years, SUBMIT, $from_year);
	$form->add_hidden("fdm");
	$form->add_list("tdy", "To Year", $sql_years, SUBMIT, $from_year);
	$form->add_hidden("tdm");
	$form->add_hidden("fdy1");
	$form->add_hidden("fdm1");
	$form->add_hidden("tdy1");
	$form->add_hidden("tdm1");
	$form->add_hidden("fdy2");
	$form->add_hidden("fdm2");
	$form->add_hidden("tdy2");
	$form->add_hidden("tdm2");
	$form->add_hidden("detail");
	$form->add_hidden("arch");
	$form->add_hidden("subfy");
	$form->add_hidden("subfm");
	$form->add_hidden("subty");
	$form->add_hidden("subtm");
	$form->add_hidden("cmsst");
}
elseif($qt != 22 and $qt != 31 and $qt != 40) // order states
{
	$form->add_hidden("from_state2");
	$form->add_hidden("to_state2");
}

if($qt == 8)
{
	$form->add_checkbox("arch", "Include only projects from archive", $include_archive, SUBMIT, "Archive");
}

if($qt == 30)
{

	$form->add_hidden("from_state");
	$form->add_hidden("to_state");
	$form->add_hidden("treatment_state");
	$form->add_hidden("fdm");
	$form->add_hidden("tdm");
	$form->add_hidden("fdy1");
	$form->add_hidden("fdm1");
	$form->add_hidden("tdy1");
	$form->add_hidden("tdm1");
	$form->add_hidden("fdy2");
	$form->add_hidden("fdm2");
	$form->add_hidden("tdy2");
	$form->add_hidden("tdm2");
	$form->add_hidden("detail");
	$form->add_hidden("subfy");
	$form->add_hidden("subfm");
	$form->add_hidden("subty");
	$form->add_hidden("subtm");
	$form->add_hidden("cmsst");
	
	$form->add_list("fdy", "Past Data From Year", $sql_years, SUBMIT, $from_year);
	$form->add_list("tdy", "Past Data To Year", $sql_years, SUBMIT, $to_year);

	$form->add_checkbox("idvi", "Include only projects where the year of the agreed opening date is equal to the 'To Year'", $visuals, SUBMIT, "Agreed Opening Date");

	$form->add_checkbox("arch", "Do not show countries", $include_archive, SUBMIT, "Country Option");

	$form->add_checkbox("exclude_e_stores", "Exclude E-Stores", $exclude_e_stores, SUBMIT, "Country Option");
}


//$form->add_button("save", "Save Filter");

$form->add_button("export_excel", "Export Excel");

if($qt == 31)
{
	$form->add_button("export_gantt", "Export Gantt");
}

$form->add_button("back", "Back to the List of Queries");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Process buttons
*********************************************************************/ 
if($form->button("save") 
	or $form->button("from_state")
	or $form->button("to_state")
	or $form->button("from_state2")
	or $form->button("to_state2")
	or $form->button("treatment_state")
	or $form->button("fdy")
	or $form->button("fdm")
	or $form->button("tdy")
	or $form->button("tdm")
	or $form->button("fdy1")
	or $form->button("fdm1")
	or $form->button("tdy1")
	or $form->button("tdm1")
	or $form->button("fdy2")
	or $form->button("fdm2")
	or $form->button("tdy2")
	or $form->button("tdm2")
	or $form->button("detail")
	or $form->button("idvi")
	or $form->button("arch")
	or $form->button("subfy")
	or $form->button("subfm")
	or $form->button("subfd")
	or $form->button("subty")
	or $form->button("subty")
	or $form->button("subtm")
	or $form->button("subtd")
	or $form->button("cmsst")
	or $form->button("dba")
	or $form->button("exclude_e_stores")
	
	)
{
	$new_filter = array();

	$new_filter["ptst"] =  $form->value("treatment_state");
	$new_filter["fst"] =  $form->value("from_state");
	$new_filter["tst"] =  $form->value("to_state");

	$new_filter["fst2"] =  $form->value("from_state2");
	$new_filter["tst2"] =  $form->value("to_state2");

	$new_filter["fdy"] =  $form->value("fdy");
	$new_filter["fdm"] =  $form->value("fdm");
	$new_filter["tdy"] =  $form->value("tdy");
	$new_filter["tdm"] =  $form->value("tdm");

	$new_filter["fdy1"] =  $form->value("fdy1");
	$new_filter["fdm1"] =  $form->value("fdm1");
	$new_filter["tdy1"] =  $form->value("tdy1");
	$new_filter["tdm1"] =  $form->value("tdm1");

	$new_filter["fdy2"] =  $form->value("fdy2");
	$new_filter["fdm2"] =  $form->value("fdm2");
	$new_filter["tdy2"] =  $form->value("tdy2");
	$new_filter["tdm2"] =  $form->value("tdm2");

	$new_filter["dba"] =  $form->value("dba");
	
	if(array_key_exists("clients", $filter))
	{
		$new_filter["clients"] =  $filter["clients"];
	}
	else
	{
		$new_filter["clients"] = "";
	}

	$new_filter["clt"] =  $filter["clt"];
	$new_filter["pl"] =  $filter["pl"];
	$new_filter["plsc"] =  $filter["plsc"];
	$new_filter["pk"] =  $filter["pk"];
	$new_filter["pt"] =  $filter["pt"];
	$new_filter["ptsc"] =  $filter["ptsc"];
	$new_filter["pct"] =  $filter["pct"];
	$new_filter["gr"] =  $filter["gr"];
	$new_filter["re"] =  $filter["re"];
	$new_filter["co"] =  $filter["co"];
	$new_filter["rtc"] =  $filter["rtc"];
	if(array_key_exists("lrtc", $filter))
	{
		$new_filter["lrtc"] =  $filter["lrtc"];
	}
	else
	{
		$new_filter["lrtc"] =  "";
	}
	$new_filter["rto"] =  $filter["rto"];
	
	if(array_key_exists("dcontr", $filter))
	{
		$new_filter["dcontr"] =  $filter["dcontr"];
	}
	else
	{
		$new_filter["dcontr"] =  "";
	}
	$new_filter["detail"] = $form->value("detail");
	$new_filter["idvi"] = $form->value("idvi");
	$new_filter["arch"] = $form->value("arch");

	$new_filter["subfy"] = $form->value("subfy");
	$new_filter["subfm"] = $form->value("subfm");
	$new_filter["subfd"] = $form->value("subfd");
	$new_filter["subty"] = $form->value("subty");
	$new_filter["subtm"] = $form->value("subtm");
	$new_filter["subtd"] = $form->value("subtd");
	

	$new_filter["cmsst"] = $form->value("cmsst");

	$new_filter["areas"] =$filter["areas"];
	$new_filter["items"] =$filter["items"];

	$new_filter["owners"] = $filter["owners"];
	$new_filter["supp"] = $filter["supp"];

	if(array_key_exists("forw", $filter))
	{
		$new_filter["forw"] =  $filter["forw"];
	}
	else
	{
		$new_filter["forw"] =  "";
	}


	$new_filter["dos"] = $filter["dos"];

	$new_filter["exclude_e_stores"] =  $form->value("exclude_e_stores");

	if(array_key_exists("icat", $filter))
	{
		$new_filter["icat"] =  $filter["icat"];
	}
	else
	{
		$new_filter["icat"] =  "";
	}

	
		
	$sql = "update mis_queries " . 
		   "set mis_query_filter = " . dbquote(serialize($new_filter)) . 
		   " where mis_query_id = " . param("query_id");
	

	$result = mysql_query($sql) or dberror($sql);
}
elseif($form->button("back"))
{
	redirect("projects_queries.php?qt=" . $qt);
}
elseif($form->button("export_excel"))
{
	$supported_qt = array(
		1,2,3,4,5,6,7,8,9,'10co',10,11,12,13,14,15,16,17,22,30,31,35, 40, 41
		);
	if (in_array($qt, $supported_qt)) {
		redirect(sprintf('projects_query_%s_xls.php?query_id=%d',
			$qt,
			param("query_id")
		));
	}
}
elseif($form->button("export_gantt"))
{
	redirect(sprintf('/project/projectworkflow/%d', param('query_id')));
}

/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page("queries");

require "include/mis_page_actions.php";

$page->header();
if($qt == 6 or $qt == 31) {
	$page->title("Masterplan: Edit Project Query - Filter");
}
elseif($qt == 31) {
	$page->title("Masterplan: Edit Project Query - Filter");
}
elseif($qt == 7) {
	$page->title("CMS Overview: Edit Project Query - Filter");
}
elseif($qt == 8) {
	$page->title("KL approved versus real cost: Edit Project Query - Filter");
}
elseif($qt == 9) {
	$page->title("Project Sheets: Edit Project Query - Filter");
}
elseif($qt == '10co') {
	$page->title("Production Order Planning: Edit Catalogue Orders Query - Filter");
}
elseif($qt == 10) {
	$page->title("Production Order Planning: Edit Project Query - Filter");
}
elseif($qt == 11) {
	$page->title("Project Milestones: Edit Project Query - Filter");
}
elseif($qt == 12) {
	$page->title("CMS Statusreport: Edit Project Query - Filter");
}
elseif($qt == 13) {
	$page->title("Transportation Cost: Edit Project Query - Filter");
}
elseif($qt == 14) {
	$page->title("Equipment of POS Locations: Edit Query - Filter");
}
elseif($qt == 15) {
	$page->title("Item Delivery: Edit Query - Filter");
}
elseif($qt == 16) {
	$page->title("CMS Completed Projects: Edit Query - Filter");
}
elseif($qt == 22) {
	$page->title("Order States in Catalogue Orders and Projects: Edit Query - Filter");
}
elseif($qt == 30) {
	$page->title("Status Report POS Locations: Edit Query - Filter");
}
elseif($qt == 6 or $qt == 35) {
	$page->title("Masterplan: Edit Order Query - Filter");
}
elseif($qt == 40) {
	$page->title("Purchase Volume by Supplier");
}
elseif($qt == 41) {
	$page->title("Shipments by Forwarder");
}

require_once("include/project_query_tabs.php");

$form->render();

?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#client_types_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=clt&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#pls_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=pl&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#plscs_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=plsc&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#projectkinds_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=pk&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#pts_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=pt&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#pos_type_subclasses_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=ptsc&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#pcts_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=pct&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#salesregions_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=gr&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#areas_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=areas&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#items_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=items&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#item_categories_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=icat&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#supplyingregions_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=re&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#countries_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=co&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#storecoordinators_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=rtc&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#local_storecoordinators_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=lrtc&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#retailoperators_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=rto&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#owners_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=owners&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#supp_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=supp&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#forw_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=forw&qt=<?php echo $qt;?>'
    });
    return false;
  });
  $('#designcontractors_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=dcontr&qt=<?php echo $qt;?>'
    });
    return false;
  });
  
  $('#design_objectives_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=dos&qt=<?php echo $qt;?>'
    });
    return false;
  });

  $('#clients_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/projects_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=clients&qt=<?php echo $qt;?>'
    });
    return false;
  });
});
</script>


<?php
$page->footer();
?>