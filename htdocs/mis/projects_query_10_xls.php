<?php
/********************************************************************

    projects_query_10_xls.php

    Generate Excel-File for Production Order Planning

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2010-11-03
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2010-11-03
    Version:        1.0.0

    Copyright (c) 2010, Swatch AG, All Rights Reserved.

*********************************************************************/


require_once "../include/frame.php";

check_access("can_perform_queries");

require_once "../include/xls/Writer.php";
require_once "include/get_functions.php";
require_once("projects_query_filter_strings.php");

/********************************************************************
    prepare Data Needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("projects_queries.php");
}


//check if filter is present
$query_name = "";
$print_query_filter = 0;
$sql = "select mis_query_name, mis_query_filter, mis_print_filter from mis_queries " .
	   "where mis_query_id = " . param("query_id");

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{

	$query_name = $row["mis_query_name"];

	if($row["mis_print_filter"] == 1)
	{
		$print_query_filter = 1;
	}

	$filters = array();
	$filters = unserialize($row["mis_query_filter"]);

	foreach($filters as $key=>$value)
	{
		$st =  $filters["ptst"]; // Project State
		$fosc = $filters["fst"]; //from order state
		$tosc = $filters["tst"]; // to order state

		$clt = $filters["clt"]; // Client Types
		$pl = $filters["pl"]; // product lines
		$pls = $filters["plsc"]; // product line subclasses
		$pk = $filters["pk"]; // product kinds
		$pt = $filters["pt"]; // project types
		$suc = $filters["ptsc"]; // POS Type Subclass
		$lpt = $filters["pct"]; // project cost type
		$gr = $filters["gr"]; // Geografical Regions
		$re = $filters["re"]; // Supplied Regions
		$cnt = $filters["co"]; // Countries
		$sc = $filters["rtc"]; // Retail Coordinators
		$lrtc = $filters["lrtc"]; // Local Project Leader

		$fdy = $filters["fdy"]; // preferred delivery Year From
		$fdm = $filters["fdm"]; // preferred delivery  Month From
		$tdy = $filters["tdy"]; // preferred delivery  Year To
		$tdm = $filters["tdm"]; // preferred delivery  Month To


		$fdy1 = $filters["fdy1"]; // agreed opening date Year From
		$fdm1 = $filters["fdm1"]; // agreed opening date  Month From
		$tdy1 = $filters["tdy1"]; // agreed opening date  Year To
		$tdm1 = $filters["tdm1"]; // agreed opening date Month To


		if(array_key_exists("fdy2", $filters))
		{
			$fdy2 = $filters["fdy2"]; // ready for pickup date Year From
			$fdm2 = $filters["fdm2"]; // ready for pickup date  Month From
			$tdy2 = $filters["tdy2"]; // ready for pickup date  Year To
			$tdm2 = $filters["tdm2"]; // ready for pickup date Month To
		
		}
		else
		{
			$fdy2 = "";
			$fdm2 = "";
			$tdy2 = "";
			$tdm2 = "";
		}

		

		
		$idvi = 0;  // Visuals
		if(array_key_exists("idvi", $filters))
		{
			$idvi = $filters["idvi"];
		}
		
		if(array_key_exists("areas", $filters))
		{
			$areas = $filters["areas"]; // Neighbourhood Locations
		
		}
		else
		{
			$areas = "";
		}

		if(array_key_exists("items", $filters))
		{
			$items = $filters["items"]; // Items
		
		}
		else
		{
			$items = "";
		}


		if(array_key_exists("icat", $filters))
		{
			$icats = $filters["icat"]; // Items
		
		}
		else
		{
			$icats = "";
		}

		$supp = $filters["supp"]; // suppliers
	}
}
else
{
	redirect("projects_queries.php");
}


$header = "Production Order Planning Projects - " . $query_name . " for: ";
$header .= " (" . date("d.m.Y G:i") . ")";


$product_lines = array();
$pl_h = substr($pl,0, strlen($pl)-1); // remove last comma
$pl_h = str_replace("-", ",", $pl_h);

if(strlen($pl_h) > 0)
{
	$sql = "select product_line_id, product_line_name ".
		   "from product_lines  " . 
		   "where (product_line_id IN (" . $pl_h . ")) " . 
		   "order by product_line_name";
}
else
{
	$sql = "select distinct product_line_id, product_line_name ".
		   "from projects " . 
		   "left join product_lines on product_line_id = project_product_line " .
		   "left join orders on order_id = project_order " . 
		   "where product_line_name <> '' and (order_archive_date is null or order_archive_date = '0000-00-00') " .  
		   "   order by product_line_name";
}
$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
	$product_lines[$row["product_line_id"]] = $row["product_line_name"];
	
}



/********************************************************************
    prepare Data
*********************************************************************/

$filter = "";

$clt = substr($clt,0, strlen($clt)-1); // remove last comma
$clt = str_replace("-", ",", $clt);
if($clt) // client type
{
    $filter =  " (address_client_type IN (" . $clt . "))";
	$_filter_strings["Client Types"] = get_filter_string("clt", $clt);
}

$pl = substr($pl,0, strlen($pl)-1); // remove last comma
$pl = str_replace("-", ",", $pl);
if($pl and $filter) // product line
{
	$filter .=  " and (project_product_line IN (" . $pl . "))";
	$_filter_strings["Product Lines"] = get_filter_string("pl", $pl);
}
elseif($pl) // product line
{
    $filter =  "  (project_product_line IN (" . $pl . "))";
	$_filter_strings["Product Lines"] = get_filter_string("pl", $pl);
}

$pls = substr($pls,0, strlen($pls)-1); // remove last comma
$pls = str_replace("-", ",", $pls);
if($pls and $filter) // product line subclasses
{
	$filter .=  " and (project_product_line_subclass IN (" . $pls . "))";
	$_filter_strings["Product Line Subclasses"] = get_filter_string("pls", $pls);
}
elseif($pls) // product line
{
    $filter =  "  (project_product_line_subclass IN (" . $pls . "))";
	$_filter_strings["Product Line Subclasses"] = get_filter_string("pls", $pls);
}

$pk = substr($pk,0, strlen($pk)-1); // remove last comma
$pk = str_replace("-", ",", $pk);

if($pk and $filter) // product line
{
	$filter .=  " and (project_projectkind IN (" . $pk . "))";
	$_filter_strings["Project Types"] = get_filter_string("pk", $pk);
}
elseif($pk) // product kind
{
    $filter =  "  (project_projectkind IN (" . $pk . "))";
	$_filter_strings["Project Types"] = get_filter_string("pk", $pk);
}


$pt = substr($pt,0, strlen($pt)-1); // remove last comma
$pt = str_replace("-", ",", $pt);
if($pt and $filter) // project type
{
    $filter .=  " and (project_postype IN (" . $pt . "))";
	$_filter_strings["POS Types"] = get_filter_string("pt", $pt);
}
elseif($pt)
{
    $filter =  "  (project_postype IN (" . $pt . "))";
	$_filter_strings["POS Types"] = get_filter_string("pt", $pt);
}

$suc = substr($suc,0, strlen($suc)-1); // remove last comma
$suc = str_replace("-", ",", $suc);
if($suc and $filter) // Pos Type Sub classe
{
    $filter .=  " and (project_pos_subclass IN (" . $suc . "))";
	$_filter_strings["POS Type Subclasses"] = get_filter_string("suc", $suc);
}
elseif($suc)
{
    $filter =  "   (project_pos_subclass IN (" . $suc . "))";
	$_filter_strings["POS Type Subclasses"] = get_filter_string("suc", $suc);
}

$lpt = substr($lpt,0, strlen($lpt)-1); // remove last comma
$lpt = str_replace("-", ",", $lpt);
if($lpt and $filter) // project cost type
{
    $filter .=  " and (project_cost_type IN (" . $lpt . "))";
	$_filter_strings["Legal Types"] = get_filter_string("lpt", $lpt);
}
elseif($lpt)
{
   $filter =  "  (project_cost_type IN (" . $lpt . "))";
   $_filter_strings["Legal Types"] = get_filter_string("lpt", $lpt);
}


$gr = substr($gr,0, strlen($gr)-1); // remove last comma
$gr = str_replace("-", ",", $gr);
$re = substr($re,0, strlen($re)-1); // remove last comma
$re = str_replace("-", ",", $re);

if($filter) {

	if($gr and $re) 
	{
		$filter .=  " and (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	}
	elseif($gr) // geografical regions
	{
		$filter .=  " and (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	}
	elseif($re) //supplying
	{
		$filter .=  " and (country_region IN (" . $re . "))";
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	}
}
else
{
	if($gr and $re) 
	{
		$filter .=  "  (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	}
	elseif($gr) // geografical regions
	{
		$filter .=  "  (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	}
	elseif($re) //supplying
	{
		$filter .=  "  (country_region IN (" . $re . "))";
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	}
}

$cnt = substr($cnt,0, strlen($cnt)-1); // remove last comma
$cnt = str_replace("-", ",", $cnt);
if($cnt and $filter) // country
{
    $filter .=  " and (order_shop_address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);

}
elseif($cnt)
{
    $filter =  "  (order_shop_address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);
}


$sc = substr($sc,0, strlen($sc)-1); // remove last comma
$sc = str_replace("-", ",", $sc);
if($sc and $filter) // Retail Coordinator
{
    $filter .=  " and (project_retail_coordinator IN (" . $sc . "))";
	$_filter_strings["Project Leader"] = get_filter_string("sc", $sc);

}
elseif($sc)
{
    $filter =  "  (project_retail_coordinator IN (" . $sc . "))";
	$_filter_strings["Project Leader"] = get_filter_string("sc", $sc);
}

$lrtc = substr($lrtc,0, strlen($lrtc)-1); // remove last comma
$lrtc = str_replace("-", ",", $lrtc);
if($lrtc and $filter) // Retail Coordinator
{
    $filter .=  " and (project_local_retail_coordinator IN (" . $lrtc . "))";
	$_filter_strings["Local Project Leader"] = get_filter_string("lrtc", $lrtc);

}
elseif($lrtc)
{
    $filter =  "   (project_local_retail_coordinator IN (" . $lrtc . "))";
	$_filter_strings["Local Project Leader"] = get_filter_string("lrtc", $lrtc);
}


//get order_state_ids
$order_state_filter = "";
if($fosc and $tosc)
{
	$order_state_filter = "order_state_code >= '" . $fosc . "'";
	$order_state_filter .= " and order_state_code <= '" . $tosc . "'";

}
elseif($fosc)
{
	$order_state_filter = "order_state_code >= '" . $fosc . "'";

}
elseif($tosc)
{
	$order_state_filter = " order_state_code <= '" . $tosc . "'";

}

$order_ids = array();
if($order_state_filter)
{
	
	//remove all orders where all items are already in step 730
	$sql = "select distinct actual_order_state_order " . 
		   " from actual_order_states " . 
		   " left join orders on order_id = actual_order_state_order " .
		   " left join projects on project_order = order_id " .
		   " left join order_states on order_state_id = actual_order_state_state " . 
		   " left join order_items on order_item_order = order_id " . 
		   " where order_actual_order_state_code < '890' " . 
		   " and (order_archive_date is null or order_archive_date = '0000-00-00') " .
		   " and (project_actual_opening_date is null or project_actual_opening_date = '0000-00-00') " .
		   " and (order_item_pickup is null or order_item_pickup = '0000-00-00' ) " .
		   " and order_item_type = 1 " .
		   " and " . $order_state_filter;
	
	$res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
		$order_ids[$row["actual_order_state_order"]] = $row["actual_order_state_order"];
	}



	if(count($order_ids) > 0)
	{
		if($filter)
		{
			$filter .= " and order_id in (" . implode(',', $order_ids). ") ";
		}
		else
		{
			$filter = " order_id in (" . implode(',', $order_ids). ") ";
		}
	}
}


if($filter and $idvi) // Visuals
{
    $filter.=  " and project_uses_icedunes_visuals = 1";
	$_filter_strings["Only Projects with Visulas"] = "yes";
}
elseif($idvi)
{
	$filter =  "   project_uses_icedunes_visuals = 1 ";
	$_filter_strings["Only Projects with Visulas"] = "yes";
}


$filter_for_preferred_arrival_is_set = false;
if($filter) // preferred delivery from
{
	if($fdy > 0 and $fdm > 0)
	{
		$tmp = "" . $fdy . "" . $fdm;
		$filter.=  " and concat(YEAR(order_item_preferred_arrival_date), DATE_FORMAT(order_item_preferred_arrival_date,'%m')) >= " . $tmp;
		$_filter_strings["Preferred delivery from"] = $fdy . "-" . $fdm;
		$filter_for_preferred_arrival_is_set = true;
	}
	elseif($fdy > 0)
	{
		$filter.=  " and YEAR(order_item_preferred_arrival_date) >= " . $fdy;
		$_filter_strings["Preferred delivery from"] = $fdy;
		$filter_for_preferred_arrival_is_set = true;
	}
	elseif($fdm > 0)
	{
		$filter.=  " and MONTH(order_item_preferred_arrival_date) >= " . $fdm;
		$_filter_strings["Preferred delivery from"] = $fdm;
		$filter_for_preferred_arrival_is_set = true;
	}
}
else
{
	if($fdy > 0 and $fdm > 0)
	{
		$tmp = "" . $fdy . "" . $fdm;
		$filter.=  " concat(YEAR(order_item_preferred_arrival_date), DATE_FORMAT(order_item_preferred_arrival_date,'%m')) >= " . $tmp;
		$_filter_strings["Preferred delivery from"] = $fdy . "-" . $fdm;;
		$filter_for_preferred_arrival_is_set = true;
	}
	elseif($fdy > 0)
	{
		$filter.=  " YEAR(order_item_preferred_arrival_date) >= " . $fdy;
		$_filter_strings["Preferred delivery from"] = $fdy;
		$filter_for_preferred_arrival_is_set = true;
	}
	elseif($fdm > 0)
	{
		$filter.=  " MONTH(order_item_preferred_arrival_date) >= " . $fdm;
		$_filter_strings["Preferred delivery from"] = $fdm;
		$filter_for_preferred_arrival_is_set = true;
	}
}

if($filter) // preferred delivery to
{
	if($tdy > 0 and $tdm > 0)
	{
		$tmp = "" . $tdy . "" . $tdm;
		$filter.=  " and concat(YEAR(order_item_preferred_arrival_date), DATE_FORMAT(order_item_preferred_arrival_date,'%m')) <= " . $tmp;
		$_filter_strings["Preferred delivery to"] = $tdy . "-" . $tdm;
		$filter_for_preferred_arrival_is_set = true;
	}
	elseif($tdy > 0)
	{
		$filter.=  " and YEAR(order_item_preferred_arrival_date) <= " . $tdy;
		$_filter_strings["Preferred delivery to"] = $tdy;
		$filter_for_preferred_arrival_is_set = true;
	}
	elseif($tdm > 0)
	{
		$filter.=  " and MONTH(order_item_preferred_arrival_date) <= " . $tdm;
		$_filter_strings["Preferred delivery to"] = $tdm;
		$filter_for_preferred_arrival_is_set = true;
	}
}
else
{
	if($tdy > 0 and $tdm > 0)
	{
		$tmp = "" . $tdy . "" . $tdm;
		$filter.=  "  concat(YEAR(order_item_preferred_arrival_date), DATE_FORMAT(order_item_preferred_arrival_date,'%m')) <= " . $tmp;
		$_filter_strings["Preferred delivery to"] = $tdy . "-" . $tdm;
		$filter_for_preferred_arrival_is_set = true;
	}
	elseif($tdy > 0)
	{
		$filter.=  "  YEAR(order_item_preferred_arrival_date) <= " . $tdy;
		$_filter_strings["Preferred delivery to"] = $tdy;
		$filter_for_preferred_arrival_is_set = true;
	}
	elseif($tdm > 0)
	{
		$filter.=  "  MONTH(order_item_preferred_arrival_date) <= " . $tdm;
		$_filter_strings["Preferred delivery to"] = $tdm;
		$filter_for_preferred_arrival_is_set = true;
	}
}


$agreed_opening_date_filter_used = false;
if($filter) // agreed opening date from
{
	if($fdy1 > 0 and $fdm1 > 0)
	{
		$tmp = "" . $fdy1 . "" . $fdm1;
		$filter.=  " and concat(YEAR(project_real_opening_date), DATE_FORMAT(project_real_opening_date,'%m')) >= " . $tmp;
		$_filter_strings["Agreed opening date from"] = $fdy1 . "-" . $fdm1;
		$agreed_opening_date_filter_used = true;
	}
	elseif($fdy1 > 0)
	{
		$filter.=  " and YEAR(project_real_opening_date) >= " . $fdy1;
		$_filter_strings["Agreed opening date from"] = $fdy1;
		$agreed_opening_date_filter_used = true;
	}
	elseif($fdm1 > 0)
	{
		$filter.=  " and MONTH(project_real_opening_date) >= " . $fdm1;
		$_filter_strings["Agreed opening date from"] = $fdm1;
		$agreed_opening_date_filter_used = true;
	}
}
else
{
	if($fdy1 > 0 and $fdm1 > 0)
	{
		$tmp = "" . $fdy1 . "" . $fdm1;
		$filter.=  " concat(YEAR(project_real_opening_date), DATE_FORMAT(project_real_opening_date,'%m')) >= " . $tmp;
		$_filter_strings["Agreed opening date from"] = $fdy1 . "-" . $fdm1;
		$agreed_opening_date_filter_used = true;
	}
	elseif($fdy1 > 0)
	{
		$filter.=  " YEAR(project_real_opening_date) >= " . $fdy1;
		$_filter_strings["Agreed opening date from"] = $fdy1;
		$agreed_opening_date_filter_used = true;
	}
	elseif($fdm1 > 0)
	{
		$filter.=  " MONTH(project_real_opening_date) >= " . $fdm1;
		$_filter_strings["Agreed opening date from"] = $fdm1;
		$agreed_opening_date_filter_used = true;
	}
}

if($filter) // agreed opening date to
{
	if($tdy1 > 0 and $tdm1 > 0)
	{
		$tmp = "" . $tdy1 . "" . $tdm1;
		$filter.=  " and concat(YEAR(project_real_opening_date), DATE_FORMAT(project_real_opening_date,'%m')) <= " . $tmp;
		$_filter_strings["Agreed opening date to"] = $tdy1 . "-" . $tdm1;
		$agreed_opening_date_filter_used = true;
	}
	elseif($tdy1 > 0)
	{
		$filter.=  " and YEAR(project_real_opening_date) <= " . $tdy1;
		$_filter_strings["Agreed opening date to"] = $tdy1;
		$agreed_opening_date_filter_used = true;
	}
	elseif($tdm1 > 0)
	{
		$filter.=  " and MONTH(project_real_opening_date) <= " . $tdm1;
		$_filter_strings["Agreed opening date to"] = $tdm1;
		$agreed_opening_date_filter_used = true;
	}
}
else
{
	if($tdy1 > 0 and $tdm1 > 0)
	{
		$tmp = "" . $tdy1 . "" . $tdm1;
		$filter.=  "  concat(YEAR(project_real_opening_date), DATE_FORMAT(project_real_opening_date,'%m')) <= " . $tmp;
		$_filter_strings["Agreed opening date to"] = $tdy1 . "-" . $tdm1;
		$agreed_opening_date_filter_used = true;
	}
	elseif($tdy1 > 0)
	{
		$filter.=  "  YEAR(project_real_opening_date) <= " . $tdy1;
		$_filter_strings["Agreed opening date to"] = $tdy1;
		$agreed_opening_date_filter_used = true;
	}
	elseif($tdm1 > 0)
	{
		$filter.=  "  MONTH(project_real_opening_date) <= " . $tdm1;
		$_filter_strings["Agreed opening date to"] = $tdm1;
		$agreed_opening_date_filter_used = true;
	}
}


$pickup_date_filter_used = false;
$pickup_date_filter = "";

	
if($fdy2 > 0 and $fdm2 > 0)
{
	$tmp = "" . $fdy2 . "" . $fdm2;
	$pickup_date_filter.=  " and concat(YEAR(order_item_ready_for_pickup), DATE_FORMAT(order_item_ready_for_pickup,'%m')) >= " . $tmp;
	$_filter_strings["Ready for Pickup date from"] = $fdy2 . "-" . $fdm2;
	$pickup_date_filter_used = true;
}
elseif($fdy2 > 0)
{
	$pickup_date_filter.=  " and YEAR(order_item_ready_for_pickup) >= " . $fdy2;
	$_filter_strings["Ready for Pickup date from"] = $fdy2;
	$pickup_date_filter_used = true;
}
elseif($fdm2 > 0)
{
	$pickup_date_filter.=  " and MONTH(order_item_ready_for_pickup) >= " . $fdm2;
	$_filter_strings["Ready for Pickup date from"] = $fdm2;
	$pickup_date_filter_used = true;
}


if($tdy2 > 0 and $tdm2 > 0)
{
	$tmp = "" . $tdy2 . "" . $tdm2;
	$pickup_date_filter.=  " and concat(YEAR(order_item_ready_for_pickup), DATE_FORMAT(order_item_ready_for_pickup,'%m')) <= " . $tmp;
	$_filter_strings["Ready for Pickup date to"] = $tdy2 . "-" . $tdm2;
	$pickup_date_filter_used = true;
}
elseif($tdy2 > 0)
{
	$pickup_date_filter.=  " and YEAR(order_item_ready_for_pickup) <= " . $tdy2;
	$_filter_strings["Ready for Pickup date to"] = $tdy2;
	$pickup_date_filter_used = true;
}
elseif($tdm2 > 0)
{
	$pickup_date_filter.=  " and MONTH(order_item_ready_for_pickup) <= " . $tdm2;
	$_filter_strings["Ready for Pickup date to"] = $tdm2;
	$agreed_opening_date_filter_used = true;
}


//filter posareas get all matching orders
$areas = substr($areas,0, strlen($areas)-1); // remove last comma
$areas = str_replace("-", ",", $areas);
$filter2 = "";

if($areas)
{
	
	$order_ids = array();

	if($filter){$filter = "where " . $filter;}
	$sql = "select DISTINCT project_order " . 
		   "from projects " .
		   "left join orders on order_id = project_order " .
		   "left join order_items on order_item_order = order_id " .
		   "left join items on order_item_id = order_item_item " .
			"left join countries on country_id = order_shop_address_country " .
			 "left join addresses on address_id = order_client_address " .
			 "left join project_states on project_state_id = project_state " .
		   "left join project_costs on project_cost_order = project_order " .
		   $filter;
	
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$sql_a = "select DISTINCT posorder_order " . 
				 "from posorders " . 
				 "left join posareas on posorder_posaddress = posarea_posaddress " .
				 "where posorder_order = " . $row["project_order"] . 
				 " and posarea_area IN (" . $areas . ")";

		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			$order_ids[$row["project_order"]] = $row["project_order"];
		}

		$sql_a = "select DISTINCT posorder_order " . 
				 "from posorderspipeline " . 
				 "left join posareaspipeline on posorder_posaddress = posarea_posaddress " .
				 "where posorder_order = " . $row["project_order"] . 
				 " and posarea_area IN (" . $areas . ")";

		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			$order_ids[$row["project_order"]] = $row["project_order"];
		}
	
	}

	if(count($order_ids) > 0)
	{
		$filter2 =  " and project_order IN (" . implode ( ',' , $order_ids ) . ") ";
		
	}
	$_filter_strings["Neighbourhood Locations"] = get_filter_string("areas", $areas);
}



//suppliers

$suppliers = substr($supp,0, strlen($supp)-1); // remove last comma
$suppliers = str_replace("-", ",", $suppliers);

$supplier_filter = "";
$supplier_filter2 = "";
if($supp)
{
	$supplier_filter = " and supplier_address IN (" . $suppliers . ") ";
	$supplier_filter2 = " and order_item_supplier_address IN (" . $suppliers . ") ";
}


//items
$items = substr($items,0, strlen($items)-1); // remove last comma
$items = str_replace("-", ",", $items);

$icats = substr($icats,0, strlen($icats)-1); // remove last comma
$icats = str_replace("-", ",", $icats);

$item_filter = "";
$order_item_filter = "";

$item_category_filter = "";
$order_item_category_filter = "";

if($items)
{
	$item_filter = " and item_id IN (" . $items . ") ";
	$order_item_filter = " and order_item_item IN (" . $items . ") ";
}
elseif($icats)
{
	$item_category_filter = " and item_category IN (" . $icats . ") ";
	$order_item_category_filter = " and item_category IN (" . $icats . ") ";
}


//get items filtered by ready for pickup date
$items_filtered_by_pick_up_date = array();
if($pickup_date_filter_used == true)
{
	
	$sql_i = "select DISTINCT item_id " . 
		 "from projects " . 
		 "left join orders on order_id = project_order " .
		 "left join order_items on order_item_order = order_id " .
		 "left join items on item_id = order_item_item " .
		 "left join suppliers on supplier_item = item_id " .
		 "left join addresses on address_id = order_client_address " .
		 "left join countries on country_id = order_shop_address_country " . 
		 "left join project_costs on project_cost_order = project_order " .
		 " where order_actual_order_state_code < '890' " . 
		 " and (order_archive_date is null or order_archive_date = '0000-00-00') " .
		 " and (project_actual_opening_date is null or project_actual_opening_date = '0000-00-00') " .
		 " and item_id > 0 " . 
		 $filter2 .
		 $item_filter .
		 $item_category_filter . 
		 $supplier_filter .
		 $pickup_date_filter;

	$res_i = mysql_query($sql_i) or dberror($sql_i);
	while ($row_i = mysql_fetch_assoc($res_i))
	{
		$items_filtered_by_pick_up_date[] = $row_i["item_id"];
	}
}

if(count($items_filtered_by_pick_up_date) > 0)
{
	
	$item_filter = " and item_id IN (" . implode(',', $items_filtered_by_pick_up_date). ") ";
	$order_item_filter = " and order_item_item IN (" . implode(',', $items_filtered_by_pick_up_date). ") ";

	
}


/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "production_order_planning_" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename); 
$xls->setVersion(8);
$sheet =& $xls->addWorksheet("Summary");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
$header_row =& $xls->addFormat();
$header_row->setSize(10);
$header_row->setAlign('left');
$header_row->setBold();

$header_row_vertical =& $xls->addFormat();
$header_row_vertical->setSize(10);
$header_row_vertical->setAlign('left');
$header_row_vertical->setBold();
$header_row_vertical->setTextRotation(270);

$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);


$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_decimal =& $xls->addFormat();
$f_decimal->setSize(8);
$f_decimal->setAlign('right');
$f_decimal->setBorder(1);
$f_decimal->setNumFormat('0.00');

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_date =& $xls->addFormat();
$f_date->setSize(8);
$f_date->setAlign('left');
$f_date->setBorder(1);
$f_date->setNumFormat('DD.MM.YYYY');

$f_used =& $xls->addFormat();
$f_used->setSize(8);
$f_used->setBorder(1);
$f_used->setPattern(2);
$f_used->setBgColor('yellow');


$f_vertical =& $xls->addFormat();
$f_vertical->setSize(9);
$f_vertical->setAlign('left');
$f_vertical->setBorder(1);
$f_vertical->setTextRotation(270);



/********************************************************************
    write all data summary
*********************************************************************/
$sheet->write(0, 0, $header, $header_row);
$row_index = 2;

if($print_query_filter == 1)
{
	foreach($_filter_strings as $key=>$value)
	{
		 $sheet->write($row_index, 0,$key . ": " . $value);
		 $row_index++;
	}
	$row_index++;
}

$row_index_pl_header = $row_index;


$col_index = 0;


$product_lines_with_items = array();


//itemlist
$items_used = array();
$filter_tmp = $filter;
if($filter and substr($filter, 0, 4 !=' and')){$filter_tmp= ' and ' . $filter;}

$itemrowpositions = array();
$itemtotals = array();
$category = '';

$sql_i = 'SELECT DISTINCT item_category_id, item_category_name, item_id, ' . 
		 'item_code, item_name, item_category ' . 
		 'from items ' .
		 "left join suppliers on supplier_item = item_id " .
		 'left join item_categories on item_category_id = item_category ' .
		 'where item_visible_in_production_order = 1 and item_type = 1 ' . 
		 $item_filter .
		 $item_category_filter . 
		 $supplier_filter .
		 ' order by item_category_sortorder, item_code ';

$res_i = mysql_query($sql_i) or dberror($sql_i);
while ($row_i = mysql_fetch_assoc($res_i))
{
		
	
	if($pickup_date_filter_used == true)
	{
		$item_total = 0;
		
		//only consider items not having a request for delivery
		if($tosc <= '700')
		{
			$sql_s = "select sum(order_item_quantity) as amount " . 
					 "from projects " . 
					 "left join orders on order_id = project_order " .
					 "left join order_items on order_item_order = order_id " .
				     "left join items on order_item_id = order_item_item " .
					 "left join countries on country_id = order_shop_address_country " . 
					 "left join project_costs on project_cost_order = project_order " .
					 "left join addresses on address_id = order_client_address " . 
					 " where order_actual_order_state_code < '890' " . 
					 " and (order_archive_date is null or order_archive_date = '0000-00-00') " .
					 " and (project_actual_opening_date is null or project_actual_opening_date = '0000-00-00') " .
					 $filter_tmp . 
					 $filter2 .
					 $order_item_filter .
					 $supplier_filter2 .
					 $pickup_date_filter . 
					 " and order_item_item = " . $row_i['item_id'] . 
					 " and order_item_quantity > 0 " . 
					 " and (order_item_ready_for_pickup is null or order_item_ready_for_pickup = '0000-00-00' ) " .
			         " and (order_item_pickup is null or order_item_pickup = '0000-00-00' ) ";
		}
		else
		{
			$sql_s = "select sum(order_item_quantity) as amount " . 
					 "from projects " . 
					 "left join orders on order_id = project_order " .
					 "left join order_items on order_item_order = order_id " .
				     "left join items on order_item_id = order_item_item " .
					 "left join countries on country_id = order_shop_address_country " . 
					 "left join project_costs on project_cost_order = project_order " .
					 "left join addresses on address_id = order_client_address " . 
					 " where order_actual_order_state_code < '890' " . 
					 " and (order_archive_date is null or order_archive_date = '0000-00-00') " .
					 " and (project_actual_opening_date is null or project_actual_opening_date = '0000-00-00') " .
					 $filter_tmp . 
					 $filter2 .
					 $order_item_filter .
					 $supplier_filter2 .
					 $pickup_date_filter . 
					 " and order_item_item = " . $row_i['item_id'] . 
					 " and order_item_quantity > 0 " . 
					 " and (order_item_pickup is null or order_item_pickup = '0000-00-00' ) ";
		}


		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$item_total = $row_s['amount'];
		}

		if($item_total > 0)
		{
		
			if($category != $row_i["item_category_id"])
			{
				$category = $row_i["item_category_id"];
				$sheet->write($row_index, $col_index, $row_i['item_category_name'], $header_row);
				$row_index++;

			}
			
			$sheet->write($row_index, $col_index, $row_i['item_code'], $f_normal);
			$sheet->setColumn($col_index, $col_index, 20);
			$sheet->write($row_index, $col_index+1, $row_i['item_name'], $f_normal);
			$sheet->setColumn($col_index, $col_index+1, 60);

			$sheet->write($row_index, $col_index+2, $item_total, $f_number);
			$items_used[] = $row_i['item_id'];
			
			$row_index++;
		}
		
	}
	else
	{
		$item_total = 0;
		
		//only consider items not having a request for delivery
		if($tosc <= '700')
		{
			$sql_s = "select sum(order_item_quantity) as amount " . 
					 "from projects " . 
					 "left join orders on order_id = project_order " .
					 "left join order_items on order_item_order = order_id " .
				     "left join items on order_item_id = order_item_item " .
					 "left join countries on country_id = order_shop_address_country " . 
					 "left join project_costs on project_cost_order = project_order " .
					 "left join addresses on address_id = order_client_address " .
					 " where order_actual_order_state_code < '890' " . 
					 " and (order_archive_date is null or order_archive_date = '0000-00-00') " .
					 " and (project_actual_opening_date is null or project_actual_opening_date = '0000-00-00') " .
					 " and (order_item_pickup is null or order_item_pickup = '0000-00-00' ) " .
				     " and (order_item_ready_for_pickup is null or order_item_ready_for_pickup = '0000-00-00' ) " .
					 $filter_tmp . 
					 $filter2 .
					 $order_item_filter .
					 $supplier_filter2 .
					 $pickup_date_filter . 
					 " and order_item_item = " . $row_i['item_id']; 
		}
		else
		{
			$sql_s = "select sum(order_item_quantity) as amount " . 
					 "from projects " . 
					 "left join orders on order_id = project_order " .
					 "left join order_items on order_item_order = order_id " .
				     "left join items on order_item_id = order_item_item " .
					 "left join countries on country_id = order_shop_address_country " . 
					 "left join project_costs on project_cost_order = project_order " .
					 "left join addresses on address_id = order_client_address " .
					 " where order_actual_order_state_code < '890' " . 
					 " and (order_archive_date is null or order_archive_date = '0000-00-00') " .
					 " and (project_actual_opening_date is null or project_actual_opening_date = '0000-00-00') " .
					 " and (order_item_pickup is null or order_item_pickup = '0000-00-00' ) " .
					 $filter_tmp . 
					 $filter2 .
					 $order_item_filter .
					 $supplier_filter2 .
					 $pickup_date_filter . 
					 " and order_item_item = " . $row_i['item_id']; 
		}

		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$item_total = $row_s['amount'];
		}

		if($item_total > 0)
		{
		
			if($category != $row_i["item_category_id"])
			{
				$category = $row_i["item_category_id"];
				$sheet->write($row_index, $col_index, $row_i['item_category_name'], $header_row);
				$row_index++;

			}
			
			$sheet->write($row_index, $col_index, $row_i['item_code'], $f_normal);
			$sheet->setColumn($col_index, $col_index, 20);
			$sheet->write($row_index, $col_index+1, $row_i['item_name'], $f_normal);
			$sheet->setColumn($col_index, $col_index+1, 60);

			$sheet->write($row_index, $col_index+2, $item_total, $f_number);
			$items_used[] = $row_i['item_id'];
			
			$row_index++;
		}
	}
}


/********************************************************************
    write all data detail for product lines
*********************************************************************/

$sheetnumber = 2;

	
$sheet =& $xls->addWorksheet('Projects');
$sheet->setInputEncoding("UTF-8");
$header = "Production Order Planning Projects - " . $query_name . " for: ";

$header .= " (" . date("d.m.Y G:i") . ")";

$xls->activesheet = 2;
$sheet->write(0, 0, $header, $header_row);


$row_index = 3;
$col_index = 10;
$num_of_item_columns = 10;


//itemlist
$itemcolpositions = array();
$itemtotals = array();
$category = '';

$sheet->write(3, 0, "Counter", $f_vertical);
$sheet->write(3, 2, "POS Name", $f_vertical);
$sheet->write(3, 3, "Country", $f_vertical);
$sheet->write(3, 4, "Project Number", $f_vertical);
$sheet->write(3, 5, "POs Type", $f_vertical);
$sheet->write(3, 6, "Project Type", $f_vertical);
$sheet->write(3, 7, "Treatment State", $f_vertical);
//$sheet->write(3, 8, "Project Status Development", $f_vertical);
$sheet->write(3, 8, "Project Status", $f_vertical);
//$sheet->write(3, 9, "Project Status Logistics", $f_vertical);

if(count($items_used) > 0)
{
	$item_filter .= ' and item_id IN (' . implode(',', $items_used) . ') ';
}

$sql_i = 'SELECT DISTINCT item_category_id, item_category_name, item_id,	item_code, item_category ' .
		 ' FROM items ' .
	     ' LEFT JOIN suppliers ON supplier_item = item_id ' . 
         ' LEFT JOIN item_categories ON item_category_id = item_category ' . 
		 'where item_visible_in_production_order = 1 and item_type = 1 ' . 
		 $item_filter .
		 $item_category_filter . 
         $supplier_filter .
		 ' order by item_category_sortorder, item_code ';



$res_i = mysql_query($sql_i) or dberror($sql_i);

while ($row_i = mysql_fetch_assoc($res_i))
{
	
	if($category != $row_i["item_category_id"])
	{
		$category = $row_i["item_category_id"];
		$sheet->write($row_index-1, $col_index, $row_i['item_category_name'], $header_row_vertical);
	}
	
	
	$sheet->write($row_index, $col_index, $row_i['item_code'],$f_vertical);

	$itemcolpositions[$row_i['item_id']] = $col_index;
	$itemtotals[$row_i['item_id']] = 0;
	$col_index++;
	$num_of_item_columns++;
}

for($i=10;$i<=$num_of_item_columns;$i++)
{
	$sheet->setColumn($i, $i, 3);
}


//project list
$columnnumber = 1;
$row_index = 7;
$col_index = 0;


//compose project_filter
$filter_tmp = $filter;
if($filter and substr($filter, 0, 4 !=' and')){$filter_tmp= ' and ' . $filter;}


$selected_projects = array();
$sql_s = "select DISTINCT project_id " . 
		 "from projects " . 
		 "left join orders on order_id = project_order " .
		 "left join order_items on order_item_order = order_id " .
		 "left join items on item_id = order_item_item " . 
		 "left join countries on country_id = order_shop_address_country " . 
		 "left join project_costs on project_cost_order = project_order " .
		 "left join addresses on address_id = order_client_address " .
		 " where order_actual_order_state_code < '890' " . 
		 " and (order_archive_date is null or order_archive_date = '0000-00-00') " .
		 " and (project_actual_opening_date is null or project_actual_opening_date = '0000-00-00') " .
		 " and order_item_quantity > 0 and order_item_type = 1 " . 
		 " and (order_item_pickup is null or order_item_pickup = '0000-00-00' ) " .
		 $filter_tmp . 
		 $filter2 .
		 $order_item_filter .
		 $order_item_category_filter .
		 $supplier_filter2 .
		 $pickup_date_filter;


$res_s = mysql_query($sql_s) or dberror($sql_s);
while($row_s = mysql_fetch_assoc($res_s))
{
	
	//check if all items were requested for delivery to forwarders
	if($tosc <= '700')
	{
		//get the number of items in the order
		$num_items = 0;
		$sql_i ="select count(order_item_id) as num_items " . 
				 "from projects " . 
				 "left join orders on order_id = project_order " .
				 "left join order_items on order_item_order = order_id " .
				 "left join items on item_id = order_item_item " . 
				 "left join countries on country_id = order_shop_address_country " . 
				 "left join project_costs on project_cost_order = project_order " .
				 "left join addresses on address_id = order_client_address " .
				 " where order_actual_order_state_code < '890' " . 
				 " and (order_archive_date is null or order_archive_date = '0000-00-00') " .
				 " and (project_actual_opening_date is null or project_actual_opening_date = '0000-00-00') " .
				 " and order_item_quantity > 0 and order_item_type = 1 " . 
				 " and (order_item_pickup is null or order_item_pickup = '0000-00-00' ) " .
				 $filter_tmp . 
				 $filter2 .
				 $order_item_filter .
			     $order_item_category_filter .
				 $supplier_filter2 .
				 $pickup_date_filter . 
			     " and project_id = " . $row_s["project_id"];

		$res_i = mysql_query($sql_i) or dberror($sql_i);
		if ($row_i = mysql_fetch_assoc($res_i))
		{
			$num_items = $row_i["num_items"];
		}

		//get the number of items having a request for delivery
		$num_items2 = 0;
		$sql_i ="select count(DISTINCT order_item_id) as num_items " . 
				 "from projects " . 
				 "left join orders on order_id = project_order " .
				 "left join order_items on order_item_order = order_id " .
				 "left join items on item_id = order_item_item " . 
				 "left join countries on country_id = order_shop_address_country " . 
				 "left join project_costs on project_cost_order = project_order " .
				 "left join addresses on address_id = order_client_address " .
			     " left join dates on date_order_item = order_item_id " . 
				 " where order_actual_order_state_code < '890' " . 
				 " and (order_archive_date is null or order_archive_date = '0000-00-00') " .
				 " and (project_actual_opening_date is null or project_actual_opening_date = '0000-00-00') " .
				 " and order_item_quantity > 0 and order_item_type = 1 " . 
				 " and (order_item_pickup is null or order_item_pickup = '0000-00-00' ) " .
				 $filter_tmp . 
				 $filter2 .
				 $order_item_filter .
			     $order_item_category_filter .
				 $supplier_filter2 .
				 $pickup_date_filter . 
			     " and date_type = 1" . 
			     " and project_id = ". $row_s["project_id"];

		//echo $sql . "<br />";

		$res_i = mysql_query($sql_i) or dberror($sql_i);
		if ($row_i = mysql_fetch_assoc($res_i))
		{
			$num_items2 = $row_i["num_items"];
		}

		//echo $num_items . "->" . $num_items2 . "<br />";

		if($num_items2 < $num_items)
		{
			$selected_projects[] = $row_s['project_id'];
		}
	}
	else
	{
		$selected_projects[] = $row_s['project_id'];
	}
}

$project_filter = "project_id = 0";
if(count($selected_projects) > 0)
{
	$project_filter = 'project_id IN (' . implode(',', $selected_projects) . ') ';
}


if($pickup_date_filter_used == true)
{
	$tmp = array();
	$project_items_filter = "";
	foreach($itemcolpositions as $item_id=>$value)
	{
		$tmp[] = $item_id;
	}
	if(count($tmp) > 0)
	{
		$project_items_filter = " and order_item_item IN (" . implode(',', $tmp) . ")";
	}


	$sheet->write($row_index-1, 0, "Ready for Pickup", $header_row);

	$sheet->write(3, 1, "Ready for Pickup Date", $f_vertical);

	//ready for pickup
	$sql_p = "select DISTINCT order_id, order_shop_address_company, order_number, " .
		     "order_actual_order_state_code, order_development_status, order_logistic_status,  " . 
			 "project_real_opening_date, order_item_preferred_arrival_date, order_number, " . 
			 " country_name, project_product_line,  projectkind_name, ".
			 "project_state_text, order_item_ready_for_pickup, postype_name " . 
			 "from projects " . 
			 "left join orders on order_id = project_order " .
			 "left join countries on country_id = order_shop_address_country " . 
			 "left join addresses on address_id = order_client_address " .
		     " left join projectkinds on  projectkind_id = project_projectkind " .
			 "left join project_states on project_state_id = project_state " .
			 "left join project_costs on project_cost_order = project_order " .
		     "left join order_items on order_item_order = order_id  " .
		     "left join postypes on postype_id = project_postype " . 
			 " where " . $project_filter . 
		     " and (order_item_pickup is null or order_item_pickup = '0000-00-00' ) " .
		     " and order_item_quantity > 0 and order_item_type = 1 " . 
		     $project_items_filter . 
		     $pickup_date_filter;
	$sql_p .= " order by project_state_id, order_item_ready_for_pickup, order_number";


	$res = mysql_query($sql_p) or dberror($sql_p);
	while ($row = mysql_fetch_assoc($res))
	{
		$ready_for_pickup_date = mysql_date_to_xls_date($row['order_item_ready_for_pickup']);
		

		$sheet->write($row_index, $col_index, $columnnumber, $f_normal);
		$col_index++;
		$sheet->write($row_index, $col_index, $ready_for_pickup_date, $f_date);
		$col_index++;
		$sheet->write($row_index, $col_index, $row['order_shop_address_company'], $f_normal);
		$col_index++;
		$sheet->write($row_index, $col_index, $row['country_name'], $f_normal);
		$col_index++;
		$sheet->write($row_index, $col_index, $row['order_number'], $f_normal);
		$col_index++;

		$sheet->write($row_index, $col_index, $row['postype_name'], $f_normal);
		$col_index++;
		
		$sheet->write($row_index, $col_index, $row['projectkind_name'], $f_normal);
		$col_index++;

		$sheet->write($row_index, $col_index, $row['project_state_text'], $f_normal);
		$col_index++;

		$sheet->write($row_index, $col_index, $row['order_actual_order_state_code'], $f_normal);
		$col_index++;

		//$sheet->write($row_index, $col_index, $row['order_development_status'], $f_normal);
		//$col_index++;

		//$sheet->write($row_index, $col_index, $row['order_logistic_status'], $f_normal);
		//$col_index++;

		
		
		foreach($itemcolpositions as $item_id=>$colposition) {
			
			$sql_s = 'select sum(order_item_quantity) as amount ' . 
					 'from order_items ' . 
					 'where order_item_item = "' . $item_id . '" ' .
					 ' and order_item_order = "' . $row['order_id'] . '"' . 
				     " and (order_item_pickup is null or order_item_pickup = '0000-00-00' ) " .
				     " and order_item_ready_for_pickup = " . dbquote($row['order_item_ready_for_pickup']);


			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$sheet->write($row_index, $col_index, $row_s['amount'], $f_number);
				$itemtotals[$item_id] = $itemtotals[$item_id] + $row_s['amount'];
				$col_index++;
			
			}
			
		}
		
		$col_index = 0;
		$row_index++;
		$columnnumber++;
	}


	
	
	
	$col_index = 9;
	$sheet->write(4, $col_index, "Item Totals", $f_normal);
	$col_index++;
	foreach($itemtotals as $item_id=>$total)
	{
		$sheet->write(4, $col_index, $total, $f_number);
		$col_index++;
	}
}
else
{
	if($filter_for_preferred_arrival_is_set == true)
	{
		
		
		$sql_p = "select DISTINCT order_id, order_shop_address_company, order_number, order_actual_order_state_code, " . 
				 "project_real_opening_date, order_item_preferred_arrival_date, order_number, " . 
			     "order_actual_order_state_code, order_development_status, order_logistic_status, " .
				 " country_name, project_product_line, projectkind_name, ".
				 " project_state_text, postype_name " . 
				 "from projects " . 
				 "left join orders on order_id = project_order " .
				 "left join countries on country_id = order_shop_address_country " . 
				 "left join addresses on address_id = order_client_address " .
				 "left join project_states on project_state_id = project_state " .
			     " left join projectkinds on  projectkind_id = project_projectkind " .
				 "left join project_costs on project_cost_order = project_order " .
				 "left join order_items on order_item_order = order_id  " .
			     "left join postypes on postype_id = project_postype " .
				 " where " . $project_filter . 
				 " and order_item_preferred_arrival_date is not null and order_item_preferred_arrival_date <> '0000-00-00'";
	}
	else
	{
		$sql_p = "select DISTINCT order_id, order_shop_address_company, order_number, order_actual_order_state_code, " . 
				 "project_real_opening_date, order_number, " . 
			     " order_development_status, order_logistic_status, " .
				 " country_name, project_product_line, projectkind_name, ".
				 " project_state_text, postype_name " . 
				 "from projects " . 
				 "left join orders on order_id = project_order " .
				 "left join countries on country_id = order_shop_address_country " . 
				 "left join addresses on address_id = order_client_address " .
				 "left join project_states on project_state_id = project_state " .
			     " left join projectkinds on  projectkind_id = project_projectkind " .
				 "left join project_costs on project_cost_order = project_order " .
			     "left join postypes on postype_id = project_postype " .
				 " where " . $project_filter;
	}

	if($filter_for_preferred_arrival_is_set == true)
	{
		$sql_p .= " order by project_state_id, order_item_preferred_arrival_date, order_number";
	}
	else
	{
		$sql_p .= " order by project_state_id, project_real_opening_date, order_number";
	}



	if($agreed_opening_date_filter_used == true)
	{
		$sheet->write(3, 1, "Agreed Opening Date", $f_vertical);
	}
	elseif($filter_for_preferred_arrival_is_set == true)
	{
		$sheet->write(3, 1, "Preferred Arrival Date", $f_vertical);
	}
	else
	{
		$sheet->write(3, 1, "Agreed Opening Date", $f_vertical);
	}

	$res = mysql_query($sql_p) or dberror($sql_p);
	while ($row = mysql_fetch_assoc($res))
	{
		$preferred_arrival_date = "";
		if($agreed_opening_date_filter_used == true)
		{
			$preferred_arrival_date = mysql_date_to_xls_date($row['project_real_opening_date']);
		}
		elseif($filter_for_preferred_arrival_is_set == true)
		{
			$preferred_arrival_date = mysql_date_to_xls_date($row['order_item_preferred_arrival_date']);
		}
		else
		{
			$preferred_arrival_date = mysql_date_to_xls_date($row['project_real_opening_date']);
		}

		$sheet->write($row_index, $col_index, $columnnumber, $f_normal);
		$col_index++;
		$sheet->write($row_index, $col_index, $preferred_arrival_date, $f_date);
		$col_index++;
		$sheet->write($row_index, $col_index, $row['order_shop_address_company'], $f_normal);
		$col_index++;
		$sheet->write($row_index, $col_index, $row['country_name'], $f_normal);
		$col_index++;
		$sheet->write($row_index, $col_index, $row['order_number'], $f_normal);
		$col_index++;

		$sheet->write($row_index, $col_index, $row['postype_name'], $f_normal);
		$col_index++;
		
		$sheet->write($row_index, $col_index, $row['projectkind_name'], $f_normal);
		$col_index++;

		$sheet->write($row_index, $col_index, $row['project_state_text'], $f_normal);
		$col_index++;

		//$sheet->write($row_index, $col_index, $row['order_development_status'], $f_normal);
		//$col_index++;

		$sheet->write($row_index, $col_index, $row['order_actual_order_state_code'], $f_normal);
		$col_index++;

		//$sheet->write($row_index, $col_index, $row['order_logistic_status'], $f_normal);
		//$col_index++;

		foreach($itemcolpositions as $item_id=>$colposition) {
			
			$sql_s = 'select sum(order_item_quantity) as amount ' . 
					 'from order_items ' . 
					 'where order_item_item = "' . $item_id . '" ' .
					 ' and order_item_order = "' . $row['order_id'] . '"' .
				     " and (order_item_pickup is null or order_item_pickup = '0000-00-00' ) ";

			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$sheet->write($row_index, $col_index, $row_s['amount'], $f_number);
				$itemtotals[$item_id] = $itemtotals[$item_id] + $row_s['amount'];
				$col_index++;
			
			}
			
		}
		
		$col_index = 0;
		$row_index++;
		$columnnumber++;
	}

	$col_index = 9;
	$sheet->write(4, $col_index, "Item Totals", $f_normal);
	$col_index++;
	foreach($itemtotals as $item_id=>$total)
	{
		$sheet->write(4, $col_index, $total, $f_number);
		$col_index++;
	}
}



$sheet->setColumn(0, 0, 3);
$sheet->setColumn(1, 1, 8);
$sheet->setColumn(2, 2, 30);
$sheet->setColumn(3, 3, 20);
$sheet->setColumn(4, 4, 10);
$sheet->setColumn(5, 5, 10);
$sheet->setColumn(6, 6, 8);
$sheet->setColumn(7, 7, 12);
$sheet->setColumn(8, 8, 8);
$sheet->setColumn(9, 9, 8);

$xls->close(); 

?>
