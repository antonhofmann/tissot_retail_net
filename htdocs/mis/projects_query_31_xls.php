<?php
/********************************************************************

    projects_query_31_xls.php

    Masterplan Projects Version after June 2015

    Created by:     Christoph Leuenberger (christoph.leuenberger@mediaparx.ch)
    Date created:   2015-06-22
    Modified by:    Anton Hofmann (anton.hofmann@codingroom.ch)
    Date modified:  2019-01-31
    Version:        1.1.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

/**
 * @todo document history of file here
 * @todo get rid of all unnecessary queries that get data
 */
require_once "../include/frame.php";
require_once "../public/autorun.php";

if(!has_access("can_print_masterplan")
 and !has_access("can_perform_queries"))
{
	redirect("noaccess.php");
}


require_once "../include/phpexcel/PHPExcel.php";

require_once "include/query_get_functions.php";
require_once "projects_query_filter_strings.php";

require_once "include/parse_projects_filter.php";

if (param('content_type') == 'json') {
  print json_encode($projects);
  exit;
}


/**
 * Get the latest col (letter) from an array and advance it by one
 * @param  array $arr
 * @return string
 */
function nextCol($arr) {
  end($arr);
  $key = key($arr);
  return ++$key;
}


/********************************************************************
   Get masterplan data
*********************************************************************/
$PmMapper = new Project_Masterplan_Datamapper;
$ValueMapper = new Project_Masterplan_Values_Datamapper();
$ValueMapper->setDateFormat($ValueMapper::DATE_DISPLAY);

$masterplanID = $PmMapper::DEFAULT_PROJECT_MASTERPLAN;
$phases = $PmMapper->getPhases($masterplanID);
$subphases = array();
$steps = array();

foreach ($phases as $phase) {
  // get subphases
  $phaseSubphases = $PmMapper->getPhases($masterplanID, $phase->id);

  foreach ($phaseSubphases as $subphase) {
    if ($subphase->show_in_excel_masterplan != '1') {
      continue;
    }
    // add parent phase title for convenience
    $subphase->phase = $phase->title;
    $subphases[] = $subphase;

    // get steps
    $subphaseSteps = $PmMapper->getSteps($subphase->id);
    foreach ($subphaseSteps as $step) {
      $step['phase'] = $subphase->title;
      $step['parent_phase'] = $phase->title;
      $step['parent_phase_id'] = $phase->id;
      $steps[] = $step;
    }
  }
}

/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$captions = array();

$captions['A'] = "Nr";
$captions['B'] = "Name";
$captions['C'] = "Country";
$captions['D'] = "City";
$captions['E'] = "POS Location";
$captions['F'] = "POS Type";
$captions['G'] = "Legal Type";
$captions['H'] = "N=New, R=Renovation, TR=Take Over/Renovation, \nT=Take Over, L=Lease Renewal, \nRL=Relocation, ER=Equip Retailer";
$captions['I'] = "POS Subclass";
$captions['J'] = "Product Line";
$captions['K'] = "Latest Step";
$captions['L'] = "Latest Milestone";
$captions['M'] = "HQ PL";
$captions['N'] = "Local PL";
$captions['O'] = "Net Surface";
$captions['P'] = "Gross Surface";

$captions['Q'] = "Takeover Handover";
$captions['R'] = "Project Opening";
$captions['S'] = "HQ feedback on agreed opening date";
$captions['T'] = "Actual Opening";

$colwidth = array();
foreach($captions as $col=>$caption){
	$colwidth[$col] = 5;
}

$nextCol = nextCol($captions);

/********************************************************************
  Determine subphase ranges
*********************************************************************/
$phase00 = 'POS INFORMATION';
$subphase01 = 'CONTACTS';
$subphase02 = 'SURFACE SQM';
$subphase03 = 'DATES';


$subPhaseRanges = array();
// add empty A-M cols
$captions2 = array();

$col = $nextCol;
$lastCol = $col;
$lastPhaseID = null;
foreach ($steps as $step) {
  if (!array_key_exists($step['phase_id'], $subPhaseRanges)) {
    $subPhaseRanges[$step['phase_id']] = array(
      'from'  => $col,
      'to'    => '',
      'color' => $step['color'],
      'phase' => $step['phase'],
      'parent_phase' => $step['parent_phase'],
      'parent_phase_id' => $step['parent_phase_id'],
      );
  }
  if ($lastPhaseID && $step['phase_id'] != $lastPhaseID) {
    $subPhaseRanges[$lastPhaseID]['to'] = $lastCol;
  }
  $lastCol = $col;
  $captions2[$col] = $step['title'];
  if ($step['step_nr']) {
    $captions2[$col] .= ' (' . $step['step_nr'] . ')';
  }
  $col++;
  $lastPhaseID = $step['phase_id'];
}
// set 'to' of last col
$subPhaseRanges[$lastPhaseID]['to'] = $lastCol;


/********************************************************************
  Determine phase ranges
*********************************************************************/
$phaseRanges = array();
$lastSubphase = null;
foreach ($subPhaseRanges as $subphase) {
  if (!array_key_exists($subphase['parent_phase_id'], $phaseRanges)) {
    $phaseRanges[$subphase['parent_phase_id']] = array(
      'from'  => $subphase['from'],
      'to'    => '',
      'phase' => $subphase['parent_phase'],
      );
  }
  if ($lastSubphase && $subphase['parent_phase_id'] != $lastSubphase['parent_phase_id']) {
    $phaseRanges[$lastSubphase['parent_phase_id']]['to'] = $lastSubphase['to'];
  }
  $lastSubphase = $subphase;
}
// set 'to' of last phase
$phaseRanges[$lastSubphase['parent_phase_id']]['to'] = $lastSubphase['to'];

//var_dump($phaseRanges);
//exit;


/********************************************************************
    Start output
*********************************************************************/
$objPHPExcel = new PHPExcel();

$logo = new PHPExcel_Worksheet_Drawing();
$logo->setName('Logo');
$logo->setDescription('Logo');
$logo->setPath('../pictures/tissot_logo.jpg');
$logo->setHeight(36);
$logo->setWidth(113);


$logo2 = new PHPExcel_Worksheet_Drawing();
$logo2->setName('Logo');
$logo2->setDescription('Logo');
$logo2->setPath('../pictures/tissot_logo.jpg');
$logo2->setHeight(36);
$logo2->setWidth(113);

$sheet = $objPHPExcel->getActiveSheet();
$objPHPExcel->getActiveSheet()->setTitle('Masterplan');

$logo->setWorksheet($objPHPExcel->getActiveSheet());


//output formats
$borderColor = '919699';

$default_border = array(
    'style' => PHPExcel_Style_Border::BORDER_THIN,
    'color' => array('rgb'=>$borderColor)
);

$style_normal_border = array(
  'borders' => array(
    'outline' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN,
      'color' => array('rgb' => $borderColor),
    ),
  ),
  'alignment' => array(
    'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
  ),
  'alignment' => array(
    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
  ),
);


$style_normal_border_red = array(
  'borders' => array(
    'outline' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN,
      'color' => array('rgb' => $borderColor),
    ),
  ),
  'alignment' => array(
    'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
  ),
  'alignment' => array(
    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
  ),
  'font' => array(
        'bold' => true,
    'color' => array('rgb'=>'ff1c23')
    ),
  
);


$style_title = array(
  'alignment' => array(
    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
  ),
  'alignment' => array(
    'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
  ),
    'font' => array(
        'bold' => true,
    'size' => 16
    )
);

$style_header = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
  'alignment' => array(
    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
  ),
    'font' => array(
        'bold' => true,
    )
);

$style_header_center = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
  'alignment' => array(
    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
  ),
    'font' => array(
        'bold' => true,
    )
);

$style_phase = array(
  'borders' => array(
    'bottom' => $default_border,
    'left' => $default_border,
    'top' => $default_border,
    'right' => $default_border,
    ),
  'alignment' => array(
    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    ),
  'fill' => array(
    'type' => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array('rgb'=>'a2fd9b'),
    ),
  'font' => array(
    'bold' => true,
    )
);

$style_subphase = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
  'alignment' => array(
    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
  ),
  'fill' => array(
    'type' => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array('rgb'=>'a2fd9b'),
    ),
  'font' => array(
    'bold' => true,
    )
);

$style_value = array(
  'borders' => array(
    'bottom' => $default_border,
    'left' => $default_border,
    'top' => $default_border,
    'right' => $default_border,
    ),
  'alignment' => array(
    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
    ),
  'fill' => array(
    'type' => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array('rgb'=>'a2fd9b'),
    ),
  'font' => array(
    'bold' => false,
    )
);

$style_zebra = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb'=>'fdfa9b'),
    ),
    'alignment' => array(
    'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
  ),
  'font' => array(
        'bold' => false,
    )
);

$style_zebra_off = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
    'alignment' => array(
    'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
  ),
  'font' => array(
        'bold' => false,
    )
);



//default styles
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(8); 
$sheet->getDefaultColumnDimension()->setWidth(12);

// HEADERS ROW 1
$sheet->setCellValue('D1', 'PROJECTS MASTERPLAN: ' . $query_title);
$sheet->getStyle('D1')->applyFromArray( $style_title );
$sheet->getRowDimension('1')->setRowHeight(36);


$row_index = 2;
if($print_query_filter == 1)
{
  foreach($_filter_strings as $key=>$value)
  {
     $sheet->setCellValue('A' .$row_index , $key . ": " . $value);
     $row_index++;
  }
  $row_index++;
}

$lastCol = nextCol($captions2);
$colTo = $lastCol--;

/********************************************************************
    Define phases and subphases in sheet
*********************************************************************/

$range = 'A' . $row_index . ':' . 'T' . $row_index;
$sheet->setCellValue('A' . $row_index, strtoupper($phase00));
$sheet->mergeCells($range);
$sheet->getStyle($range)->applyFromArray( $style_header_center );


// define phase cell ranges and set phase title
foreach ($phaseRanges as $phase) {
  $sheet->setCellValue($phase['from'] . $row_index, strtoupper($phase['phase']));

  $range = $phase['from'] . $row_index . ':' . $phase['to'] . $row_index;
  $sheet->mergeCells($range);
  $sheet->getStyle($range)->applyFromArray( $style_header_center );

  // make phases that are only 1 column wide wider than default width
  if ($phase['from'] == $phase['to']) {
    $len = strlen($phase['phase']);
    $colwidth[$phase['from']] = $len > 15 ? $len + 3 : 15;
  }
}

$row_index++;

// rotate text in row 2
$cellrange = 'A' .$row_index . ':' . $colTo . $row_index;
$sheet->getStyle($cellrange)->getAlignment()->setWrapText(true);
//$sheet->getStyle($cellrange)->getAlignment()->setTextRotation(90);
$sheet->getRowDimension($row_index)->setRowHeight(30);

$range = 'M' . $row_index . ':' . 'N' . $row_index;
$sheet->setCellValue('M' . $row_index, strtoupper($subphase01));
$sheet->mergeCells($range);
$sheet->getStyle($range)->applyFromArray( $style_header_center );

$range = 'O' . $row_index . ':' . 'P' . $row_index;
$sheet->setCellValue('O' . $row_index, strtoupper($subphase02));
$sheet->mergeCells($range);
$sheet->getStyle($range)->applyFromArray( $style_header_center );

$range = 'Q' . $row_index . ':' . 'T' . $row_index;
$sheet->setCellValue('Q' . $row_index, strtoupper($subphase03));
$sheet->mergeCells($range);
$sheet->getStyle($range)->applyFromArray( $style_header_center );

// define subphase cell ranges and set title
foreach ($subPhaseRanges as $subphase) {
  $sheet->setCellValue($subphase['from'] . $row_index, strtoupper($subphase['phase']));

  $range = $subphase['from'] . $row_index . ':' . $subphase['to'] . $row_index;
  $sheet->mergeCells($range);
  $style_subphase['fill']['color'] = array('rgb' => $subphase['color']);
  $sheet->getStyle($range)->applyFromArray($style_subphase);
}

$row_index++;


// HEADERS ROW 4
foreach($captions as $col=>$caption){
  $sheet->setCellValue($col . $row_index, $caption);
  $sheet->getStyle($col . $row_index)->applyFromArray( $style_header );
}

foreach($captions2 as $col=>$caption){
  $sheet->setCellValue($col . $row_index, $caption);
  $sheet->getStyle($col . $row_index)->applyFromArray( $style_header );
}

$cellrange = 'A' .$row_index . ':' . $colTo . $row_index;
$sheet->getStyle($cellrange)->getAlignment()->setWrapText(true);
$sheet->getStyle($cellrange)->getAlignment()->setTextRotation(90);
$sheet->getRowDimension($row_index)->setRowHeight(220);

// set styles according to subphase colors
foreach ($subPhaseRanges as $subphase) {
  $range = $subphase['from'] . $row_index . ':' . $subphase['to'] . $row_index;
  $style_phase['fill']['color'] = array('rgb' => $subphase['color']);
  $sheet->getStyle($range)->applyFromArray($style_phase);
}

$row_index++;

/*
if($print_query_filter == 1)
{
  $row_index++;
  $row_index++;
  $row_index++;
}
*/

//OUTPUT DATA
$zebra_counter = 0;
$i = 1;

$unapproved_cer_projects = array();

foreach ($projects as $row)
{
 
  
  // get project's milestones
  $no_ln_needed = "";
  $no_cer_needed = "";
  $sql_m = "select ln_no_ln_submission_needed " . 
         " from ln_basicdata " . 
         " where ln_basicdata_version = 0 " . 
         " and ln_basicdata_project = " . $row["project_id"];

  $res_m = mysql_query($sql_m) or dberror($sql_m);
  if ($row_m = mysql_fetch_assoc($res_m))
  {
    if($row_m["ln_no_ln_submission_needed"] == 1)
    {
      $no_ln_needed = "No LN needed";
    }
  }


  $sql_m = "select cer_basicdata_no_cer_submission_needed " . 
         " from cer_basicdata " . 
         " where cer_basicdata_version = 0 " . 
         " and cer_basicdata_project = " . $row["project_id"];

  $res_m = mysql_query($sql_m) or dberror($sql_m);
  if ($row_m = mysql_fetch_assoc($res_m))
  {
    if($row_m["cer_basicdata_no_cer_submission_needed"] == 1)
    {
      $no_cer_needed = "No AF/CER needed";
    }
  }
  
  $milestones = array();
  $last_milestone = "";
   

  $sql_m = "select project_milestone_milestone,  " . 
		 "DATE_FORMAT(project_milestone_date, '%d.%m.%Y') as milestone_date, " .
		 "project_milestone_date, milestone_text " .
		 "from project_milestones " . 
		 "left join milestones on milestone_id = project_milestone_milestone " .
		 "where project_milestone_project = " . $row["project_id"] .
		 " and project_milestone_date is not null and project_milestone_date <> '0000-00-00' " . 
		 " order by milestone_code DESC";
	//" order by project_milestone_date DESC, milestone_code DESC";

  $res_m = mysql_query($sql_m) or dberror($sql_m);
  if ($row_m = mysql_fetch_assoc($res_m))
  {
    $milestones[$row_m["project_milestone_milestone"]] = $row_m["project_milestone_date"];

    if($row_m["project_milestone_date"] != NULL and $row_m["project_milestone_date"] != '0000-00-00')
    {
      
      if($no_cer_needed != "" and $no_ln_needed != "")
      {
        $last_milestone = $row_m["milestone_date"] . " (" . $no_ln_needed . ", " . $no_cer_needed . ")";
      }
      elseif($no_cer_needed != "")
      {
        $last_milestone = $row_m["milestone_date"] . " (" . $no_cer_needed . ")";
      }
      elseif($no_ln_needed != "")
      {
        $last_milestone = $row_m["milestone_date"] . " (" . $no_ln_needed . ")";
      }
      else
      {
        $last_milestone = $row_m["milestone_text"] . ": " . $row_m["milestone_date"];
      }
    }
  }

  $lease_dates = $ValueMapper->loadPOSLeaseData($row['project_order']);
  
    
  $cell_index = 'A';

  $sheet->setCellValue($cell_index . $row_index, $i);
  if($colwidth[$cell_index] < strlen($i)){$colwidth[$cell_index] = strlen($i);}
  $cell_index++;

  


  // owner company address
  $sheet->setCellValue($cell_index . $row_index,$row["address_company"]);
  if($colwidth[$cell_index] < strlen($row["address_company"])){$colwidth[$cell_index] = strlen($row["address_company"]);}
  $cell_index++;

  // country
  $sheet->setCellValue($cell_index . $row_index, $row["country_name"]);
  if($colwidth[$cell_index] < strlen($row["country_name"])){$colwidth[$cell_index] = 3+strlen($row["country_name"]);}
  $cell_index++;
	
  //city
  $sheet->setCellValue($cell_index . $row_index, $row["order_shop_address_place"]);
  if($colwidth[$cell_index] < strlen($row["order_shop_address_place"])){$colwidth[$cell_index] = 3+strlen($row["order_shop_address_place"]);}
  $cell_index++;


  // pos location
  $sheet->setCellValue($cell_index . $row_index,$row["order_shop_address_company"]);
  if($colwidth[$cell_index] < strlen($row["order_shop_address_company"])){$colwidth[$cell_index] = strlen($row["order_shop_address_company"]);}
  $cell_index++;
  
  
  //POS type
  $sheet->setCellValue($cell_index . $row_index, $row["postype_name"]);
  if($colwidth[$cell_index] < strlen($row["postype_name"])){$colwidth[$cell_index] = 2+strlen($row["postype_name"]);}
  $cell_index++;
  
  


  // project cost type - legal type
  $sheet->setCellValue($cell_index . $row_index, $row["project_costtype_text"]);
  if($colwidth[$cell_index] < strlen($row["project_costtype_text"])){$colwidth[$cell_index] = 2+strlen($row["project_costtype_text"]);}
  $cell_index++;

  // projectkind
  $sheet->setCellValue($cell_index . $row_index, $row["projectkind_code"]);
  $cell_index++;
  
  
  // pos sublass
  $sheet->setCellValue($cell_index . $row_index, $row["possubclass_name"]);
  if($colwidth[$cell_index] < strlen($row["possubclass_name"])){$colwidth[$cell_index] = strlen($row["possubclass_name"]);}
  $cell_index++;

  // product line
  $sheet->setCellValue($cell_index . $row_index, $row["product_line_name"]);
  if($colwidth[$cell_index] < strlen($row["product_line_name"])){$colwidth[$cell_index] = 2+strlen($row["product_line_name"]);}
  $cell_index++;

  // order state development status
  $sheet->setCellValue($cell_index . $row_index, $row["order_actual_order_state_code"]);
  $cell_index++;


  // latest milestone
  $sheet->setCellValue($cell_index . $row_index, $last_milestone);
  if($colwidth[$cell_index] < strlen($last_milestone)){$colwidth[$cell_index] = 2+strlen($last_milestone);}
  $cell_index++;
  
  
  //PL HQ
  $sheet->setCellValue($cell_index . $row_index, $row["project_leader"]);
  if($colwidth[$cell_index] < strlen($row["project_leader"])){$colwidth[$cell_index] = 2+strlen($row["project_leader"]);}
  $cell_index++;


  //local PL
  $sheet->setCellValue($cell_index . $row_index, $row["local_project_leader"]);
  if($colwidth[$cell_index] < strlen($row["local_project_leader"])){$colwidth[$cell_index] = 2+strlen($row["local_project_leader"]);}
  $cell_index++;


  // total sqms
  $sheet->setCellValue($cell_index . $row_index, $row["project_cost_sqms"]);
  if($colwidth[$cell_index] < strlen($row["project_cost_sqms"])){$colwidth[$cell_index] = 2+strlen($row["project_cost_sqms"]);}
  $cell_index++;

  // net surface
  $sheet->setCellValue($cell_index . $row_index, $row["project_cost_totalsqms"]);
  if($colwidth[$cell_index] < strlen($row["project_cost_totalsqms"])){$colwidth[$cell_index] = 2+strlen($row["project_cost_totalsqms"]);}
  $cell_index++;

  // handover
  $handoverdate = $lease_dates['handover_date'];
  if($lease_dates['handover_date'] == '00.00.0000') {
	  $handoverdate= '';
  }
  $sheet->setCellValue($cell_index . $row_index, $handoverdate);
  if($colwidth[$cell_index] < strlen($handoverdate)){$colwidth[$cell_index] = 2+strlen($handoverdate);}
  $cell_index++;

  // project opening
  $sheet->setCellValue($cell_index . $row_index, $row["submission_date"]);
  if($colwidth[$cell_index] < strlen($row["submission_date"])){$colwidth[$cell_index] = 2+strlen($row["submission_date"]);}
  $cell_index++;

  // agreed opening
  $sheet->setCellValue($cell_index . $row_index, $row["realistic_opening_date"]);
  if($colwidth[$cell_index] < strlen($row["realistic_opening_date"])){$colwidth[$cell_index] = 2+strlen($row["realistic_opening_date"]);}
  $cell_index++;

  // actual opening
  $sheet->setCellValue($cell_index . $row_index, $row["actual_opening_date"]);
  if($colwidth[$cell_index] < strlen($row["actual_opening_date"])){$colwidth[$cell_index] = 2+strlen($row["actual_opening_date"]);}
  $cell_index++;

  /**
   * Apply step values based on masterplan template
   */
  $projectValues = $ValueMapper->loadProjectData($row['project_id']);
  foreach ($steps as $step) {
    $value = $projectValues[$step['value']];
    
    
	// set background color
    $style_value['fill']['color'] = array('rgb' => $step['color']);
    $sheet->getStyle($cell_index . $row_index)->applyFromArray($style_value);
    $colwidth[$cell_index] = strlen($value) > $colwidth[$cell_index] ? strlen($value) : $colwidth[$cell_index];

	if ($step['step_type'] != 'info' && $value) {
	  if(is_date_value($value))
	  {
		$mysql_date_value = from_system_date($value);
		$value = mysql_date_to_xls_date($mysql_date_value);
	  }
	  
	  $sheet->setCellValue($cell_index . $row_index, $value);
	  $sheet->getStyle($cell_index . $row_index)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
    }
	else
	{
		$sheet->setCellValue($cell_index . $row_index, $value);
	}    
    $cell_index++;
  }

  // mark cell red if cer is not approved
  if ($row['order_actual_order_state_code'] == 620 &&
    (!$projectValues['cer_approved_below_50k'] && !$projectValues['af_cer_approved'])) {
    $sheet->getStyle($latestMilestoneCol . $row_index . ':' . $latestMilestoneCol . $row_index)->applyFromArray($style_normal_border_red);
    $unapproved_cer_projects[] = $row['project_id'];
  }

  // apply styling for meta cols in this row
  foreach($metaCols as $col => $caption) {
    $sheet->getStyle($col . $row_index)->applyFromArray($style_normal_border);
  }

  $i++;
  $row_index++;
}


//Format column heights and witdhs
foreach($colwidth as $col=>$width) {
  $sheet->getColumnDimension($col)->setWidth($width);
}

//reset active cell

$objPHPExcel->setActiveSheetIndex(0);
$sheet->setCellValue('A1', "");
$sheet->getStyle('A1')->applyFromArray($style_title);
$objPHPExcel->getActiveSheet()->insertNewRowBefore(1, 1);
$sheet->setCellValue('A1', "");
$sheet->getStyle('A1')->applyFromArray($style_title);

$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(1);



/********************************************************************
    Start output
*********************************************************************/
$filename = 'masterplan_project_states_' . date('Ymd H:i:s') . '.xls';
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
header('Pragma: ');

//send data
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
?>