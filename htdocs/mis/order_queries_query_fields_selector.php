<?php
/********************************************************************

    order_queries_query_fields_selector.php

    Enter selected fields for the query

    
    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2016-08-19
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2016-08-19
    Version:        1.0.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../include/page_modal.php";

if(!has_access("can_perform_all_queries") 
  and !has_access("can_perform_projects_query"))
{
	redirect("noaccess.php");
}
require_once "include/orders_query_check_access.php";
require_once "include/orders_query_get_functions.php";

/********************************************************************
    prepare all data needed
*********************************************************************/
$query_id = param("query_id");

$fields = array();
$selected_fields = array();
$oldfields = array();

$sql = "select orderquery_fields from orderqueries " .
	   "where orderquery_id = " . $query_id;

if(param("t") == 'cl') // client address
{
	$db_info = query_clientaddress_fields();
	$fields = $db_info["fields"];
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["orderquery_fields"]);
		if(is_array($oldfields) and array_key_exists("cl", $oldfields))
		{
			$selected_fields = $oldfields["cl"];
		}
	}
}
elseif(param("t") == "ord") // orders
{
	$db_info = query_orders_fields();
	$fields = $db_info["fields"];
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["orderquery_fields"]);
		if(is_array($oldfields) and array_key_exists("ord", $oldfields))
		{
			$selected_fields = $oldfields["ord"];
		}
	}
}
if(!$selected_fields)
{
	$selected_fields = array();
}



/********************************************************************
    save data
*********************************************************************/
if(param("save_form"))
{
	$oldfields["cl"] = array();
	$oldfields["ord"] = array();
	

	//get Query Fields
	$sql = "select orderquery_fields from orderqueries " .
		   "where orderquery_id = " . $query_id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["orderquery_fields"]);
	}
	
	
	if(!$oldfields["cl"]) // client
	{
		$oldfields["cl"] = array();
	}
	
	if(!$oldfields["ord"]) // orders
	{
		$oldfields["ord"] = array();
	}
	
	
	$new_selected_fields = array();
	foreach($fields as $key=>$caption)
	{
		$post_key = str_replace(".", "_", $key);
		if($_POST[$post_key])
		{
			$new_selected_fields[$key] = $caption;
		}
	}

	
	if(param("t") == "cl") // client
	{
		$updatefields["cl"] = $new_selected_fields;
		$updatefields["ord"] = $oldfields["ord"];
	}
	elseif(param("t") == "ord") // orders
	{
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["ord"] = $new_selected_fields;
	}
	
	$sql = "update orderqueries " . 
		   "set orderquery_fields = " . dbquote(serialize($updatefields)) . " " . 
		   " where orderquery_id = " . param("query_id");

	$result = mysql_query($sql) or dberror($sql);

	//update field order and sortorder
	$sql = "select orderquery_fields, orderquery_field_order, orderquery_order from orderqueries " .
		   "where orderquery_id = " . $query_id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields["cl"] = array();
		$oldfields["ord"] = array();

		$oldfields = unserialize($row["orderquery_fields"]);

		

		$oldfields = array_merge($oldfields["cl"], $oldfields["ord"]);

		$old_field_order = decode_field_array($row["orderquery_field_order"]);
		$old_query_order = decode_field_array($row["orderquery_order"]);

		//remove fields not present anymore
		foreach($old_field_order as $key=>$fieldname)
		{
			if(!array_key_exists($fieldname, $oldfields))
			{
				foreach($old_field_order as $key=>$value)
				{
					if($value == $fieldname)
					{
						unset($old_field_order[$key]);
					}
				}

				foreach($old_query_order as $key=>$value)
				{
					if($value == $fieldname)
					{
						unset($old_query_order[$key]);
					}
				}
			}
		}

		//add new elements at the end of the arrays
		foreach($oldfields as $fieldname=>$caption)
		{
			if(!in_array($fieldname, $old_field_order))
			{
				$old_field_order[] = $fieldname;
			}
			if(!in_array($fieldname, $old_query_order))
			{
				$old_query_order[] = $fieldname;
			}
		}



		$field_order = encode_field_array($old_field_order);
		$query_order = encode_field_array($old_query_order);
		
		//update query order and field order
		$sql = "Update orderqueries SET " . 
			   "orderquery_field_order = " . dbquote($field_order) . ", " . 
			   "orderquery_order = " . dbquote($query_order) . ", " . 
			   "date_modified = " . dbquote(date("Y-m-d H:s:i")) . ", " . 
			   "user_modified = " . dbquote(user_login()) .
		       " where orderquery_id = " . $query_id;


		$result = mysql_query($sql) or dberror($sql);
	}
}

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new form("orderqueries", "Field Selector");
$form->add_hidden("save_form", "1");
$form->add_hidden("query_id", $query_id);
$form->add_hidden("t", param("t"));


$form->add_label("L1", "", "", "Select the Fields to be included in the Query");

foreach($fields as $key=>$caption)
{
	if(array_key_exists($key,$selected_fields))
	{
		$form->add_checkbox($key, $caption, true);
	}
	else
	{
		$form->add_checkbox($key, $caption, false);
	}
	
}

$form->add_input_submit("submit", "Save Selection", 0);

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Process buttons
*********************************************************************/ 

/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page_Modal("query_generator");

//require "include/benchmark_page_actions.php";

$page->header();
$page->title("Field Selector");

$form->render();

if(param("save_form"))
{
?>

<script languege="javascript">
var back_link = "order_queries_query_fields.php?query_id=<?php echo $query_id;?>"; 
$.nyroModalRemove();
</script>

<?php
}
$page->footer();
