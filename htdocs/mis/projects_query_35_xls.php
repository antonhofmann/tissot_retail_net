<?php
/********************************************************************

    projects_query_35_xls.php

    Masterplan Catalogue Orders

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2015-07-07
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2015-07-07
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_queries");

require_once "../include/phpexcel/PHPExcel.php";
require_once "include/query_get_functions.php";
require_once("projects_query_filter_strings.php");


/********************************************************************
    prepare all data needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("projects_queries.php");
}


//check if filter is present
$query_title = "";
$print_query_filter = 0;
$sql = "select mis_query_name, mis_query_filter, mis_print_filter from mis_queries " .
	   "where mis_query_id = " . param("query_id");

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$query_title = $row["mis_query_name"] . ' (' . date("d.m.Y H:i:s"). ')';

	if($row["mis_print_filter"] == 1)
	{
		$print_query_filter = 1;
	}
	

	$filters = array();
	$filters = unserialize($row["mis_query_filter"]);

	foreach($filters as $key=>$value)
	{
		
		$st =  $filters["ptst"]; // Project State


		$fosc = $filters["fst"]; //from order state
		$tosc = $filters["tst"]; // to order state

		$clt = $filters["clt"]; // Client Types
		$gr = $filters["gr"]; // Geografical Regions
		$re = $filters["re"]; // Supplied Regions
		$cnt = $filters["co"]; // Countries


		$rto = $filters["rto"]; // Retail Operators

		
		
		if(array_key_exists("supp", $filters))
		{
			$supp = $filters["supp"]; // Suppliers
		
		}
		else
		{
			$supp = "";
		}
	}
}
else
{
	redirect("projects_queries.php");
}




//consider only catalogue orders
$filter = "where order_type = 2"; 


$clt = substr($clt,0, strlen($clt)-1); // remove last comma
$clt = str_replace("-", ",", $clt);
if($clt and $filter) // client type
{
    $filter .=  " and (address_client_type IN (" . $clt . "))";
	$_filter_strings["Client Types"] = get_filter_string("clt", $clt);
}
elseif($clt)
{
	$filter =  " where (address_client_type IN (" . $clt . "))";
	$_filter_strings["Client Types"] = get_filter_string("clt", $clt);
}


$gr = substr($gr,0, strlen($gr)-1); // remove last comma
$gr = str_replace("-", ",", $gr);
$re = substr($re,0, strlen($re)-1); // remove last comma
$re = str_replace("-", ",", $re);

if($filter) {

	if($gr and $re) 
	{
		$filter .=  " and (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	}
	elseif($gr) // geografical regions
	{
		$filter .=  " and (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	}
	elseif($re) //supplying
	{
		$filter .=  " and (country_region IN (" . $re . "))";
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	}
}
else
{
	if($gr and $re) 
	{
		$filter .=  " where (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	}
	elseif($gr) // geografical regions
	{
		$filter .=  " where (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	}
	elseif($re) //supplying
	{
		$filter .=  " where (country_region IN (" . $re . "))";
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	}
}

$cnt = substr($cnt,0, strlen($cnt)-1); // remove last comma
$cnt = str_replace("-", ",", $cnt);
if($cnt and $filter) // country
{
    $filter .=  " and (address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);

}
elseif($cnt)
{
    $filter =  " where (address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);
}


$rto = substr($rto,0, strlen($rto)-1); // remove last comma
$rto = str_replace("-", ",", $rto);
if($rto and $filter) // Retail Operator
{
    $filter .=  " and (order_retail_operator IN (" . $rto . "))";
	$_filter_strings["Logistics Coordinator"] = get_filter_string("rto", $rto);

}
elseif($rto)
{
    $filter =  " where (order_retail_operator IN (" . $rto . "))";
	$_filter_strings["Logistics Coordinator"] = get_filter_string("rto", $rto);
}


if($filter and $fosc) // from order state
{
    $filter.=  " and order_actual_order_state_code >= '" . $fosc . "' ";
	$_filter_strings["From Order State Code"] = $fosc;
}
elseif($fosc)
{
	 $filter.=  " where order_actual_order_state_code >= '" . $fosc . "' ";
	 $_filter_strings["From Order State Code"] = $fosc;
}

if($filter and $tosc) // to order state
{
    $filter.=  " and order_actual_order_state_code <= '" . $tosc . "' ";
	$_filter_strings["To Order State Code"] = $tosc;
}
elseif($tosc)
{
	$filter.=  " where order_actual_order_state_code <= '" . $tosc . "' ";
	$_filter_strings["To Order State Code"] = $tosc;
}

if($tosc < 890)
{
	$filter.= " and (order_archive_date is null or order_archive_date = '0000-00-00') ";
}


if($gr and $re) 
{
	$order = " order by region_name, salesregion_name, country_name, order_number, order_shop_address_company, order_shop_address_place ";

	$order2 = " order by order_actual_order_state_code, region_name, salesregion_name, country_name, order_number, order_shop_address_company, order_shop_address_place ";
}
elseif($gr) 
{
	$order = " order by salesregion_name, country_name, order_number, order_shop_address_company, order_shop_address_place ";
	$order2 = " order by order_actual_order_state_code, salesregion_name, country_name, order_number, order_shop_address_company, order_shop_address_place ";
}
elseif($re) 
{
	$order = " order by region_name, country_name, order_number, order_shop_address_company, order_shop_address_place ";
	$order2 = " order by  order_actual_order_state_code, oregion_name, country_name, order_number, order_shop_address_company, order_shop_address_place ";
}
else
{
	$order = " order by country_name, order_number, order_shop_address_company, order_shop_address_place ";
	$order2 = " order by order_actual_order_state_code, country_name, order_number, order_shop_address_company, order_shop_address_place ";
}


//get orders of the indicated suppliers

$filter_suppliers = '';
$supp = substr($supp,0, strlen($supp)-1); // remove last comma
$supp = str_replace("-", ",", $supp);
if($supp and $filter)
{
    $filter_suppliers =  " and (order_item_supplier_address IN (" . $supp . "))";
	$_filter_strings["Supplier"] = get_filter_string("supp", $supp);
}
elseif($supp)
{
    $filter_suppliers =  " where (order_item_supplier_address IN (" . $supp . "))";
	$_filter_strings["Supplier"] = get_filter_string("supp", $supp);
}


$supplier_orders = array();
if($filter_suppliers)
{
	$sql = "select DISTINCT order_id " . 
	       "from orders " .
		   "inner join order_items on order_item_order = order_id " .
		   "left join addresses on address_id = order_client_address " .
		   "left join countries on country_id = address_country " .
		   "left join salesregions on salesregion_id = country_salesregion " .
		   "left join regions on region_id = country_region " .
		   "left join users on user_id = order_retail_operator " . 
		   "left join transportation_types as transportation_types1 on order_preferred_transportation_arranged = transportation_types1. transportation_type_id ".
		   "left join transportation_types as transportation_types2 on order_preferred_transportation_mode = transportation_types2. transportation_type_id ".
		   $filter . 
		   $filter_suppliers;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$supplier_orders[] = $row["order_id"];
	}


	if($filter and count($supplier_orders) > 0)
	{
		$filter .= ' and order_id in (' . implode(',', $supplier_orders) . ')';
	}
	elseif(count($supplier_orders) > 0)
	{
		$filter = ' where order_id in (' . implode(',', $supplier_orders) . ')';
	}
	elseif(count($supplier_orders) == 0)
	{
		if($filter)
		{
			$filter .= ' and order_id = 0';
		}
		elseif(count($supplier_orders) > 0)
		{
			$filter = ' where order_id = 0 ';
		}
	}
}


//prevent from huge queries

$sql_d = "select order_id, order_number,  " .
		   "user_name, order_actual_order_state_code, " .
		   "DATE_FORMAT(orders.date_created, '%d.%m.%Y') as order_date, " .
           "orders.date_created as project_date_created, " .
		   "country_name,  order_shop_address_company, " .
		   "order_shop_address_place, order_shop_address_address, order_actual_order_state_code, " . 
		   "address_country, country_region,  " . 
		   "transportation_types1.transportation_type_name as transportation_type_name_arranged, " . 
		   "transportation_types2.transportation_type_name as transportation_type_name_mode, " . 
		   "region_name, salesregion_name, " .
		   " address_company " .
		   "from orders " .
		   "left join addresses on address_id = order_client_address " .
		   "left join countries on country_id = address_country " .
		   "left join salesregions on salesregion_id = country_salesregion " .
		   "left join regions on region_id = country_region " .
		   "left join users on user_id = order_retail_operator " . 
		   "left join transportation_types as transportation_types1 on order_preferred_transportation_arranged = transportation_types1. transportation_type_id ".
		   "left join transportation_types as transportation_types2 on order_preferred_transportation_mode = transportation_types2. transportation_type_id ".
		   $filter .
		   $order;


/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$captions = array();

$captions['A'] = "Nr";
$captions['B'] = "Order Number";
$captions['C'] = "Client";

if($gr) 
{
	$captions['D'] = "Sales Region/Country";
}
elseif($re) 
{
	$captions['D'] = "Supplied Region/Country";
}
else
{
	$captions['D'] = "Country";
}
$captions['E'] = "Order Status";
$captions['F'] = "Order Submitted";

$captions['G'] = "Supplier";
$captions['H'] = "Category";
$captions['I'] = "Order Confirmed (210)";
$captions['J'] = "Order Submitted to Supplier (700)";
$captions['K'] = "Ready for Pickup";
$captions['L'] = "Pickup";
$captions['M'] = "Shipment Code";
$captions['N'] = "Forwarder";
$captions['O'] = "Expected Arrival";
$captions['P'] = "Arrival";
$captions['Q'] = "Delivery Confirmed (800)";

$colwidth = array();
$colwidth['A'] = "5";
$colwidth['B'] = "5";
$colwidth['C'] = "5";
$colwidth['D'] = "5";
$colwidth['E'] = "5";
$colwidth['F'] = "5";
$colwidth['G'] = "5";
$colwidth['H'] = "5";
$colwidth['I'] = "5";
$colwidth['J'] = "5";
$colwidth['K'] = "5";
$colwidth['L'] = "5";
$colwidth['M'] = "5";
$colwidth['N'] = "5";
$colwidth['O'] = "5";
$colwidth['P'] = "5";
$colwidth['Q'] = "5";


/********************************************************************
    Start output
*********************************************************************/
$objPHPExcel = new PHPExcel();

$logo = new PHPExcel_Worksheet_Drawing();
$logo->setName('Logo');
$logo->setDescription('Logo');
$logo->setPath('../pictures/brand_logo.jpg');
$logo->setHeight(36);
$logo->setWidth(113);

$sheet = $objPHPExcel->getActiveSheet();
$objPHPExcel->getActiveSheet()->setTitle('Masterplan');

$logo->setWorksheet($objPHPExcel->getActiveSheet());



//output formats
$default_border = array(
    'style' => PHPExcel_Style_Border::BORDER_THIN,
    'color' => array('rgb'=>'1006A3')
);

$style_normal_border = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '1006A3'),
		),
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
);


$style_normal_border_red = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '1006A3'),
		),
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
	'font' => array(
        'bold' => true,
		'color' => array('rgb'=>'ff1c23')
    ),
	
);


$style_title = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
    'font' => array(
        'bold' => true,
		'size' => 16
    )
);

$style_header = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'font' => array(
        'bold' => true,
    )
);

$style_header_center = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	),
    'font' => array(
        'bold' => true,
    )
);


$style_lease_normal = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb'=>'bfe3ff'),
    ),
    'font' => array(
        'bold' => false,
    )
);


$style_preparation_normal = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb'=>'fdfa9b'),
    ),
    'font' => array(
        'bold' => false,
    )
);

$style_approval_normal = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb'=>'fdccc2'),
    ),
    'font' => array(
        'bold' => false,
    )
);


$style_construction_normal = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb'=>'a2fd9b'),
    ),
    'font' => array(
        'bold' => false,
    )
);


$style_lease = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb'=>'bfe3ff'),
    ),
    'font' => array(
        'bold' => true,
    )
);


$style_preparation = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb'=>'fdfa9b'),
    ),
    'font' => array(
        'bold' => true,
    )
);

$style_approval = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb'=>'fdccc2'),
    ),
    'font' => array(
        'bold' => true,
    )
);


$style_construction = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb'=>'a2fd9b'),
    ),
    'font' => array(
        'bold' => true,
    )
);


$style_zebra = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb'=>'fdfa9b'),
    ),
    'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
	'font' => array(
        'bold' => false,
    )
);

$style_zebra_off = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
    'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
	'font' => array(
        'bold' => false,
    )
);



//default styles
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(8); 


// HEADRES ROW 1
$sheet->setCellValue('D1', 'ORDERS MASTERPLAN: ' . $query_title);
$sheet->getStyle('D1')->applyFromArray( $style_title );
$sheet->getRowDimension('1')->setRowHeight(36);


$row_index = 2;
if($print_query_filter == 1)
{
	foreach($_filter_strings as $key=>$value)
	{
		 $sheet->setCellValue('A' .$row_index , $key . ": " . $value);
		 $row_index++;
	}
	$row_index++;
}


// HEADRES ROWS (Captions)
foreach($captions as $col=>$caption){
	$sheet->setCellValue($col . $row_index, $caption);
	$sheet->getStyle($col . $row_index)->applyFromArray( $style_header );
}
$cellrange = 'A' .$row_index . ':Q' .$row_index;
$sheet->getStyle($cellrange)->getAlignment()->setWrapText(true);
$sheet->getStyle($cellrange)->getAlignment()->setTextRotation(90);
$sheet->getRowDimension($row_index)->setRowHeight(140);
$row_index++;



$row_index++;

//OUTPUT DATA
$zebra_counter = 0;
$i = 1;


$res = mysql_query($sql_d) or dberror($sql_d);
while ($row = mysql_fetch_assoc($res))
{
	
	//get project steps
	$order_states = array();

	$sql_m = 'select actual_order_state_state, ' . 
	         'DATE_FORMAT(date_created, "%d.%m.%Y") as step_date, date_created as order_state_date_created ' . 
		     ' from actual_order_states ' . 
		     ' where actual_order_state_order = ' . $row['order_id'] . 
		     ' order by date_created ASC';
	
	$res_m = mysql_query($sql_m) or dberror($sql_m);
	while ($row_m = mysql_fetch_assoc($res_m))
	{
		$order_states[$row_m["actual_order_state_state"]] = $row_m["step_date"];
	}

	
	$row_index2 = $row_index;
	$cell_index2 = $cell_index;
	$sql_m = 'select DISTINCT suppliers.address_shortcut as supplier, forwarders.address_shortcut as forwarder,' . 
			 'DATE_FORMAT(order_item_ordered, "%d.%m.%Y") as date_ordered, ' .
			 'DATE_FORMAT(order_item_ready_for_pickup, "%d.%m.%Y") as date_ready_for_pickup, ' .
			 'DATE_FORMAT(order_item_pickup, "%d.%m.%Y") as pickup_date, ' .
			 'DATE_FORMAT(order_item_expected_arrival, "%d.%m.%Y") as expected_arrival_date, ' .
			 'DATE_FORMAT(order_item_arrival, "%d.%m.%Y") as arrival_date, ' .
			 'item_category_name, order_item_shipment_code ' .
			 'from order_items ' .
			 'left join addresses as suppliers on suppliers.address_id = order_item_supplier_address ' .
			 'left join addresses as forwarders on forwarders.address_id = order_item_forwarder_address ' .
			 'left join items on item_id = order_item_item ' . 
			 'left join item_categories on item_category_id = item_category ' .
			 'where order_item_order = ' . $row["order_id"] . 
			 ' and order_item_supplier_address > 0 ' . 
			 str_replace('where', 'and', $filter_suppliers) . 
			 ' order by order_item_ordered ASC, suppliers.address_shortcut';

	$res_m = mysql_query($sql_m) or dberror($sql_m);
	while ($row_m = mysql_fetch_assoc($res_m))
	{
	
		/*
		if($zebra_counter == 1) { 
			$sheet->getStyle('A' . $row_index . ':Q' . $row_index)->applyFromArray( $style_zebra_off );
			$zebra_counter = 0;
		}
		else
		{   
			$sheet->getStyle('A' . $row_index . ':Q' . $row_index)->applyFromArray( $style_zebra );
			$zebra_counter = 1;
		}
		*/
		
		$cell_index = 0;

		$posaddress = $row["order_shop_address_company"] . ', ' . $row["order_shop_address_place"];
		
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $i);
		if($colwidth['A'] < strlen($i)){$colwidth['A'] = strlen($i)+1;}
		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["order_number"]);
		if($colwidth['B'] < strlen($row["order_number"])){$colwidth['B'] = 2+strlen($row["order_number"]);}
		$cell_index++;


		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["address_company"]);
		if($colwidth['C'] < strlen($row["address_company"])){$colwidth['C'] = 2+strlen($row["address_company"]);}
		$cell_index++;

		if($gr) 
		{
			$tmp = $row["salesregion_name"] . " - " . $row["country_name"];
		}
		elseif($gr) 
		{
			$tmp = $row["region_name"] . " - " . $row["country_name"];
		}
		else
		{
			$tmp = $row["country_name"];
		}
		
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $tmp);
		if($colwidth['D'] < strlen($tmp)){$colwidth['D'] = 3+strlen($tmp);}
		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index,$row["order_actual_order_state_code"]);
		if($colwidth['E'] < strlen($row["order_actual_order_state_code"])){$colwidth['E'] = strlen($row["order_actual_order_state_code"]);}
		$cell_index++;


		$sheet->setCellValueByColumnAndRow($cell_index, $row_index,$row["order_date"]);
		if($colwidth['F'] < strlen($row["order_date"])){$colwidth['F'] = strlen($row["order_date"]);}
		$sheet->getStyle('F' . $row_index . ':F' . $row_index)->getNumberFormat()->setFormatCode('dd/mm/yyyy');
		$cell_index++;


		//list all order_items
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index,$row_m["supplier"]);
		if($colwidth['G'] < strlen($row_m["supplier"])){$colwidth['G'] = strlen($row_m["supplier"]);}
		$cell_index++;
		
		$tmp = "n.a.";
		if($row_m["item_category_name"])
		{
			$tmp = $row_m["item_category_name"];
		}
		
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index,$tmp);
		if($colwidth['H'] < strlen($tmp)){$colwidth['H'] = strlen($tmp);}
		$cell_index++;

		if(array_key_exists(47, $order_states)) // step 210 confirm order
		{
			$tmp = $order_states[47];
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index,$tmp);
			if($colwidth['I'] < strlen($tmp)){$colwidth['I'] = strlen($tmp);}
			
			
			$sheet->getStyle('I' . $row_index . ':I' . $row_index)->getNumberFormat()->setFormatCode('dd/mm/yyyy');
			
			$cell_index++;
		}
		else
		{
			$tmp = "";
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index,$tmp);
			if($colwidth['I'] < strlen($tmp)){$colwidth['I'] = strlen($tmp);}
			$cell_index++;
		}


		if($row_m["date_ordered"]) 
		{
			$tmp = $row_m["date_ordered"];
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index,$tmp);
			if($colwidth['J'] < strlen($tmp)){$colwidth['J'] = strlen($tmp);}
			
			
			$sheet->getStyle('J' . $row_index . ':J' . $row_index)->getNumberFormat()->setFormatCode('dd/mm/yyyy');
			
			$cell_index++;
		}
		else
		{
			$tmp = "";
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index,$tmp);
			if($colwidth['J'] < strlen($tmp)){$colwidth['J'] = strlen($tmp);}
			$cell_index++;
		}

		if($row_m["date_ready_for_pickup"]) 
		{
			$tmp = $row_m["date_ready_for_pickup"];
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index,$tmp);
			if($colwidth['K'] < strlen($tmp)){$colwidth['K'] = strlen($tmp);}
			
			
			$sheet->getStyle('K' . $row_index . ':K' . $row_index)->getNumberFormat()->setFormatCode('dd/mm/yyyy');
			
			$cell_index++;
		}
		else
		{
			$tmp = "";
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index,$tmp);
			if($colwidth['K'] < strlen($tmp)){$colwidth['K'] = strlen($tmp);}
			$cell_index++;
		}

		if($row_m["pickup_date"]) 
		{
			$tmp = $row_m["pickup_date"];
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index,$tmp);
			if($colwidth['L'] < strlen($tmp)){$colwidth['L'] = strlen($tmp);}
			
			
			$sheet->getStyle('L' . $row_index . ':L' . $row_index)->getNumberFormat()->setFormatCode('dd/mm/yyyy');
			
			$cell_index++;
		}
		else
		{
			$tmp = "";
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index,$tmp);
			if($colwidth['L'] < strlen($tmp)){$colwidth['L'] = strlen($tmp);}
			$cell_index++;
		}

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index,$row_m["order_item_shipment_code"]);
		if($colwidth['M'] < strlen($row_m["order_item_shipment_code"])){$colwidth['M'] = strlen($row_m["order_item_shipment_code"]);}
		$cell_index++;


		$sheet->setCellValueByColumnAndRow($cell_index, $row_index,$row_m["forwarder"]);
		if($colwidth['N'] < strlen($row_m["forwarder"])){$colwidth['N'] = strlen($row_m["forwarder"]);}
		$cell_index++;

		if($row_m["expected_arrival_date"]) 
		{
			$tmp = $row_m["expected_arrival_date"];
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index,$tmp);
			if($colwidth['O'] < strlen($tmp)){$colwidth['O'] = strlen($tmp);}
			
			
			$sheet->getStyle('O' . $row_index . ':O' . $row_index)->getNumberFormat()->setFormatCode('dd/mm/yyyy');
			
			$cell_index++;
		}
		else
		{
			$tmp = "";
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index,$tmp);
			if($colwidth['O'] < strlen($tmp)){$colwidth['O'] = strlen($tmp);}
			$cell_index++;
		}


		if($row_m["arrival_date"]) 
		{
			$tmp = $row_m["arrival_date"];
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index,$tmp);
			if($colwidth['P'] < strlen($tmp)){$colwidth['P'] = strlen($tmp);}
			
			
			$sheet->getStyle('P' . $row_index . ':P' . $row_index)->getNumberFormat()->setFormatCode('dd/mm/yyyy');
			
			$cell_index++;
		}
		else
		{
			$tmp = "";
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index,$tmp);
			if($colwidth['P'] < strlen($tmp)){$colwidth['P'] = strlen($tmp);}
			$cell_index++;
		}


		if(array_key_exists(64, $order_states)) // step 800 confirm delivery
		{
			$tmp = $order_states[64];
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index,$tmp);
			if($colwidth['Q'] < strlen($tmp)){$colwidth['Q'] = strlen($tmp);}
			
			
			$sheet->getStyle('Q' . $row_index . ':Q' . $row_index)->getNumberFormat()->setFormatCode('dd/mm/yyyy');
			
			$cell_index++;
		}
		else
		{
			$tmp = "";
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index,$tmp);
			if($colwidth['Q'] < strlen($tmp)){$colwidth['Q'] = strlen($tmp);}
			$cell_index++;
		}


		$row_index++;
		
	}

	
	
	$i++;
	//$row_index++;
}






//Format column heights and witdhs
foreach($colwidth as $col=>$width) {
	$sheet->getColumnDimension($col)->setWidth($width);
}


//reset active cell

$objPHPExcel->setActiveSheetIndex(0);
$sheet->setCellValue('A1', "");
$sheet->getStyle('A1')->applyFromArray($style_title);
$objPHPExcel->getActiveSheet()->insertNewRowBefore(1, 1);
$sheet->setCellValue('A1', "");
$sheet->getStyle('A1')->applyFromArray($style_title);



$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(1);




/********************************************************************
    Start output
*********************************************************************/
$filename = 'masterplan_order_states_' . date('Ymd H:i:s') . '.xls';
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
header('Pragma: ');

//send data
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
?>