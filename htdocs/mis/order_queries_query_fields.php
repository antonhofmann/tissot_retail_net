<?php
/********************************************************************

    orders_queries_query_fields.php

    Field Selection for the Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2016-08-19
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2016-08-19
    Version:        1.0.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if(!has_access("can_perform_all_queries") 
  and !has_access("can_perform_projects_query"))
{
	redirect("noaccess.php");
}
require_once "include/orders_query_check_access.php";

require_once "include/orders_query_get_functions.php";


/********************************************************************
    prepare all data needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("order_queries.php");
}

$query_id = param("query_id");

$orderquery = get_query_name($query_id);
$icon = "../pictures/add_item.gif";
$link = "javascript:open_selector('')";


//get Query Fields
$fields = "";
$fields_clientaddresses = array();
$fields_orders = array();


$sql = "select orderquery_fields from orderqueries " .
	   "where orderquery_id = " . $query_id;

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$fields = array();
	
	$fields = unserialize($row["orderquery_fields"]);
	foreach($fields as $field=>$caption)
	{
		if(array_key_exists("cl", $fields)) //Clienst
		{
			$fields_clientaddresses = $fields["cl"];
		}
		if(array_key_exists("ord", $fields)) // orders
		{
			$fields_orders = $fields["ord"];
		}
	}
}

/********************************************************************
    create form
*********************************************************************/

$form = new Form("orderqueries", "query_generator");

$form->add_hidden("query_id", param("query_id"));
$form->add_section($orderquery["name"]);

$form->add_section(" ");
$form->add_comment("Please select the fields that should be shown in the query.");
$form->add_section("Selected Fields");



//Client Adress Fields
$selected_clientaddress_fields = "";
if($fields_clientaddresses)
{
	foreach($fields_clientaddresses as $key=>$caption)
	{
		$caption = str_replace("Client ", "", $caption);
		if(array_key_exists($key,$fields_clientaddresses))
		{
			$selected_clientaddress_fields .= str_replace('---- ', "", $caption) . ", ";
		}
	}
	$selected_clientaddress_fields = substr($selected_clientaddress_fields, 0, strlen($selected_clientaddress_fields) - 2);
}
$form->add_label_selector("clientaddresses", "Client", 0, $selected_clientaddress_fields, $icon, $link);


//Order Fields
$selected_order_fields = "";
if($fields_orders)
{
	foreach($fields_orders as $key=>$caption)
	{
		if(array_key_exists($key,$fields_orders))
		{
			$selected_order_fields .= str_replace('---- ', "", $caption) . ", ";
		}
	}
	$selected_order_fields = substr($selected_order_fields, 0, strlen($selected_order_fields) - 2);
}
$form->add_label_selector("order", "Replacement Order", 0, $selected_order_fields, $icon, $link);




if(check_if_query_has_fields($query_id) == true)
{
	$form->add_button("execute", "Execute Query");
}
$form->add_button("back", "Back to the List of Queries");


/********************************************************************
    process form
*********************************************************************/
$form->populate();
$form->process();

if($form->button("back"))
{
	redirect("orderqueries.php");
}
elseif($form->button("execute"))
{
	redirect("order_queries_query_xls.php?query_id=" . param("query_id"));
}

/********************************************************************
    render
*********************************************************************/

$page = new Page("queries");

require_once("include/mis_page_actions.php");


$page->header();
$page->title("Edit Order Query - Fields");

require_once("include/orders_query_tabs.php");
$form->render();

?>


<script type="text/javascript">
  jQuery(document).ready(function($) {
  
  $('#clientaddresses_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/order_queries_query_fields_selector.php?query_id=<?php echo $query_id;?>&t=cl'
    });
    return false;
  });
  $('#order_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/order_queries_query_fields_selector.php?query_id=<?php echo $query_id;?>&t=ord'
    });
    return false;
  });
});
</script>


<?php

$page->footer();

?>