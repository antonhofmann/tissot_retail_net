<?php
/********************************************************************

    project_queries_query_fields.php

    Field Selection for the Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2014-09-14
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2014-09-14
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if(!has_access("can_perform_all_queries") 
  and !has_access("can_perform_projects_query"))
{
	redirect("noaccess.php");
}
require_once "include/projects_query_check_access.php";

require_once "include/projects_query_get_functions.php";


/********************************************************************
    prepare all data needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("project_queries.php");
}

$query_id = param("query_id");

$projectquery = get_query_name($query_id);
$icon = "../pictures/add_item.gif";
$link = "javascript:open_selector('')";


//get Query Fields
$fields = "";
$fields_posaddress = array();
$fields_clientaddresses = array();
$fields_franchiseeaddresses = array();
$fields_distributionchannels = array();
$fields_projects = array();
$fields_cer = array();
$fields_logistics = array();
$fields_rental = array();
$fields_project_sheets = array();
$fields_cms = array();
$fields_staff = array();
$fields_milestones = array();
$fieldsitems = array();


$fields_budget = array();
$fields_investments = array();
$fields_approved_investments = array();
$fields_real_costs = array();
$fields_cost_comparison = array();

$sql = "select projectquery_fields from projectqueries " .
	   "where projectquery_id = " . $query_id;

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$fields = array();
	
	$fields = unserialize($row["projectquery_fields"]);
	foreach($fields as $field=>$caption)
	{
		if(array_key_exists("pl", $fields)) // POS Locations
		{
			$fields_posaddress = $fields["pl"];
		}
		if(array_key_exists("cl", $fields)) //Clienst
		{
			$fields_clientaddresses = $fields["cl"];
		}
		if(array_key_exists("fr", $fields)) // Franchisees/Owners
		{
			$fields_franchiseeaddresses = $fields["fr"];
		}
		if(array_key_exists("dcs", $fields)) //Distirbution Channels
		{
			$fields_distributionchannels = $fields["dcs"];
		}
		if(array_key_exists("pro", $fields)) // projects
		{
			$fields_projects = $fields["pro"];
		}
		if(array_key_exists("cer", $fields)) // cer
		{
			$fields_cer = $fields["cer"];
		}
		if(array_key_exists("log", $fields)) // projects
		{
			$fields_logistics = $fields["log"];
		}
		if(array_key_exists("ren", $fields)) // rental details
		{
			$fields_rental = $fields["ren"];
		}
		if(array_key_exists("pros", $fields)) // project sheets
		{
			$fields_project_sheets = $fields["pros"];
		}
		if(array_key_exists("cms", $fields)) // CMS
		{
			$fields_cms = $fields["cms"];
		}
		if(array_key_exists("sta", $fields)) // staff
		{
			$fields_staff = $fields["sta"];
		}

		if(array_key_exists("bud", $fields)) // budget
		{
			$fields_budget = $fields["bud"];
		}

		if(array_key_exists("inv", $fields)) // investments
		{
			$fields_investments = $fields["inv"];;
		}

		if(array_key_exists("ainv", $fields)) // approved investments
		{
			$fields_approved_investments = $fields["ainv"];
		}
		if(array_key_exists("cos", $fields)) // real costs
		{
			$fields_real_costs = $fields["cos"];
		}
		if(array_key_exists("comp", $fields)) // cost comparison
		{
			$fields_cost_comparison = $fields["comp"];
		}
		if(array_key_exists("mst", $fields)) // milestones
		{
			$fields_milestones = $fields["mst"];
		}
		if(array_key_exists("ost", $fields)) // order states
		{
			$fields_orderstates = $fields["ost"];
		}
		if(array_key_exists("item", $fields)) // items
		{
			$fields_items = $fields["item"];
		}
	}

	
}

/********************************************************************
    create form
*********************************************************************/

$form = new Form("projectqueries", "query_generator");

$form->add_hidden("query_id", param("query_id"));
$form->add_section($projectquery["name"]);

$form->add_section(" ");
$form->add_comment("Please select the fields that should be shown in the query.");
$form->add_section("Selected Fields");




//Client Adress Fields
$selected_clientaddress_fields = "";
if($fields_clientaddresses)
{
	foreach($fields_clientaddresses as $key=>$caption)
	{
		$caption = str_replace("Client ", "", $caption);
		if(array_key_exists($key,$fields_clientaddresses))
		{
			$selected_clientaddress_fields .= str_replace('---- ', "", $caption) . ", ";
		}
	}
	$selected_clientaddress_fields = substr($selected_clientaddress_fields, 0, strlen($selected_clientaddress_fields) - 2);
}
$form->add_label_selector("clientaddresses", "Client", 0, $selected_clientaddress_fields, $icon, $link);


//franchisee Adress Fields
$selected_franchiseeaddress_fields = "";
if($fields_franchiseeaddresses)
{
	foreach($fields_franchiseeaddresses as $key=>$caption)
	{
		$caption = str_replace("Owner ", "", $caption);
		if(array_key_exists($key,$fields_franchiseeaddresses))
		{
			$selected_franchiseeaddress_fields .= str_replace('---- ', "", $caption) . ", ";
		}
	}
	$selected_franchiseeaddress_fields = substr($selected_franchiseeaddress_fields, 0, strlen($selected_franchiseeaddress_fields) - 2);
}
$form->add_label_selector("franchiseeaddresses", "Owner Company", 0, $selected_franchiseeaddress_fields, $icon, $link);



//POS Location Fields
$selected_projectaddress_fields = "";
if($fields_posaddress)
{
	foreach($fields_posaddress as $key=>$caption)
	{
		if(array_key_exists($key,$fields_posaddress))
		{
			$selected_projectaddress_fields .= str_replace('---- ', "", $caption) . ", ";
		}
	}
	$selected_projectaddress_fields = substr($selected_projectaddress_fields, 0, strlen($selected_projectaddress_fields) - 2);
}
$form->add_label_selector("posaddress", "POS Location", 0, $selected_projectaddress_fields, $icon, $link);




//POS Distribution Channel Fields Fields
$selected_distributionchannel_fields = "";
if($fields_distributionchannels)
{
	foreach($fields_distributionchannels as $key=>$caption)
	{
		//$caption = str_replace("POS ", "", $caption);
		if(array_key_exists($key,$fields_distributionchannels))
		{
			$selected_distributionchannel_fields .= str_replace('---- ', "", $caption) . ", ";
		}
	}
	$selected_distributionchannel_fields = substr($selected_distributionchannel_fields, 0, strlen($selected_distributionchannel_fields) - 2);
}
$form->add_label_selector("distributionchannel", "Distributionchannel", 0, $selected_distributionchannel_fields, $icon, $link);

//Project Fields
$selected_project_fields = "";
if($fields_projects)
{
	foreach($fields_projects as $key=>$caption)
	{
		if(array_key_exists($key,$fields_projects))
		{
			$selected_project_fields .= str_replace('---- ', "", $caption) . ", ";
		}
	}
	$selected_project_fields = substr($selected_project_fields, 0, strlen($selected_project_fields) - 2);
}
$form->add_label_selector("project", "Project", 0, $selected_project_fields, $icon, $link);


//CER Fields
$selected_cer_fields = "";
if($fields_cer)
{
	foreach($fields_cer as $key=>$caption)
	{
		if(array_key_exists($key,$fields_cer))
		{
			$selected_cer_fields .= str_replace('---- ', "", $caption) . ", ";
		}
	}
	$selected_cer_fields = substr($selected_cer_fields, 0, strlen($selected_cer_fields) - 2);
}
$form->add_label_selector("cer", "CER/AF/LN", 0, $selected_cer_fields, $icon, $link);

//Logistics Fields
/*
$selected_logitic_fields = "";
if($fields_logistics)
{
	foreach($fields_logistics as $key=>$caption)
	{
		if(array_key_exists($key,$fields_logistics))
		{
			$selected_logitic_fields .= str_replace('---- ', "", $caption) . ", ";
		}
	}
	$selected_logitic_fields = substr($selected_logitic_fields, 0, strlen($selected_logitic_fields) - 2);
}
$form->add_label_selector("logistics", "Logistics", 0, $selected_logitic_fields, $icon, $link);
*/

//rental Fields
$selected_rental_fields = "";
if($fields_rental)
{
	foreach($fields_rental as $key=>$caption)
	{
		if(array_key_exists($key,$fields_rental))
		{
			$selected_rental_fields .= str_replace('---- ', "", $caption) . ", ";
		}
	}
	$selected_rental_fields = substr($selected_rental_fields, 0, strlen($selected_rental_fields) - 2);
}
$form->add_label_selector("rental", "Lease Details", 0, $selected_rental_fields, $icon, $link);


//ProjectSheet Fields
/*
$selected_project_sheet_fields = "";
if($fields_project_sheets)
{
	foreach($fields_project_sheets as $key=>$caption)
	{
		if(array_key_exists($key,$fields_project_sheets))
		{
			$selected_project_sheet_fields .= str_replace('---- ', "", $caption) . ", ";
		}
	}
	$selected_project_sheet_fields = substr($selected_project_sheet_fields, 0, strlen($selected_project_sheet_fields) - 2);
}
$form->add_label_selector("project_sheet", "Project Sheet", 0, $selected_project_sheet_fields, $icon, $link);
*/


//Staff Fields
$selected_staff_fields = "";
if($fields_staff)
{
	foreach($fields_staff as $key=>$caption)
	{
		if(array_key_exists($key,$fields_staff))
		{
			$selected_staff_fields .= str_replace('---- ', "", $caption) . ", ";
		}
	}
	$selected_staff_fields = substr($selected_staff_fields, 0, strlen($selected_staff_fields) - 2);
}
$form->add_label_selector("staff", "Staff", 0, $selected_staff_fields, $icon, $link);



//budget
$selected_budget_fields = "";
if($fields_budget)
{
	foreach($fields_budget as $key=>$caption)
	{
		if(array_key_exists($key,$fields_budget))
		{
			$selected_budget_fields .= str_replace('---- ', "", $caption) . ", ";
		}
	}
	$selected_budget_fields = substr($selected_budget_fields, 0, strlen($selected_budget_fields) - 2);
}
$form->add_label_selector("budget", "Budget", 0, $selected_budget_fields, $icon, $link);



//investments
$selected_investment_fields = "";
if($fields_investments)
{
	foreach($fields_investments as $key=>$caption)
	{
		if(array_key_exists($key,$fields_investments))
		{
			$selected_investment_fields .= str_replace('---- ', "", $caption) . ", ";
		}
	}
	$selected_investment_fields = substr($selected_investment_fields, 0, strlen($selected_investment_fields) - 2);
}
$form->add_label_selector("investments", "Investments/Intangibles", 0, $selected_investment_fields, $icon, $link);



//approved investments
$selected_approved_investment_fields = "";
if($fields_approved_investments)
{
	foreach($fields_approved_investments as $key=>$caption)
	{
		if(array_key_exists($key,$fields_approved_investments))
		{
			$selected_approved_investment_fields .= str_replace('---- ', "", $caption) . ", ";
		}
	}
	$selected_approved_investment_fields = substr($selected_approved_investment_fields, 0, strlen($selected_approved_investment_fields) - 2);
}
$form->add_label_selector("approved_investments", "Approved Investments/Intangibles", 0, $selected_approved_investment_fields, $icon, $link);


//real cost Fields
$selected_real_cost_fields = "";
if($fields_real_costs)
{
	foreach($fields_real_costs as $key=>$caption)
	{
		if(array_key_exists($key,$fields_real_costs))
		{
			$selected_real_cost_fields .= str_replace('---- ', "", $caption) . ", ";
		}
	}
	$selected_real_cost_fields = substr($selected_real_cost_fields, 0, strlen($selected_real_cost_fields) - 2);
}
$form->add_label_selector("real_costs", "Real Cost", 0, $selected_real_cost_fields, $icon, $link);


//cost_comparison Fields
$selected_cost_comparison_fields = "";
if($fields_cost_comparison)
{
	foreach($fields_cost_comparison as $key=>$caption)
	{
		if(array_key_exists($key,$fields_cost_comparison))
		{
			$selected_cost_comparison_fields .= str_replace('---- ', "", $caption) . ", ";
		}
	}
	$selected_cost_comparison_fields = substr($selected_cost_comparison_fields, 0, strlen($selected_cost_comparison_fields) - 2);
}
$form->add_label_selector("cost_comparison", "Cost Comparison", 0, $selected_cost_comparison_fields, $icon, $link);


//order states
$selected_orderstates_fields = "";
if($fields_orderstates)
{
	foreach($fields_orderstates as $key=>$caption)
	{
		if(array_key_exists($key,$fields_orderstates))
		{
			$selected_orderstates_fields .= str_replace('---- ', "", $caption) . ", ";
		}
	}
	$selected_orderstates_fields = substr($selected_orderstates_fields, 0, strlen($selected_orderstates_fields) - 2);
}
$form->add_label_selector("orderstates", "Project Workflow", 0, $selected_orderstates_fields, $icon, $link);



//CMS Fields
/*
$selected_cms_fields = "";
if($fields_cms)
{
	foreach($fields_cms as $key=>$caption)
	{
		if(array_key_exists($key,$fields_cms))
		{
			$selected_cms_fields .= str_replace('---- ', "", $caption) . ", ";
		}
	}
	$selected_cms_fields = substr($selected_cms_fields, 0, strlen($selected_cms_fields) - 2);
}
$form->add_label_selector("cms", "CMS Workflow", 0, $selected_cms_fields, $icon, $link);
*/


//milestones
$selected_milestones_fields = "";
if($fields_milestones)
{
	foreach($fields_milestones as $key=>$caption)
	{
		if(array_key_exists($key,$fields_milestones))
		{
			$selected_milestones_fields .= str_replace('---- ', "", $caption) . ", ";
		}
	}
	$selected_milestones_fields = substr($selected_milestones_fields, 0, strlen($selected_milestones_fields) - 2);
}
$form->add_label_selector("milestones", "Milestones", 0, $selected_milestones_fields, $icon, $link);


//itames
/*
$selected_itemss_fields = "";
if($fields_items)
{
	foreach($fields_items as $key=>$caption)
	{
		if(array_key_exists($key,$fields_items))
		{
			$selected_itemss_fields .= str_replace('---- ', "", $caption) . ", ";
		}
	}
	$selected_itemss_fields = substr($selected_itemss_fields, 0, strlen($selected_itemss_fields) - 2);
}
$form->add_label_selector("items", "Standard Items", 0, $selected_itemss_fields, $icon, $link);
*/
if(check_if_query_has_fields($query_id) == true)
{
	$form->add_button("execute", "Execute Query");
}
$form->add_button("back", "Back to the List of Queries");


/********************************************************************
    process form
*********************************************************************/
$form->populate();
$form->process();

if($form->button("back"))
{
	redirect("project_queries.php");
}
elseif($form->button("execute"))
{
	redirect("project_queries_query_xls.php?query_id=" . param("query_id"));
}

/********************************************************************
    render
*********************************************************************/

$page = new Page("queries");

require_once("include/mis_page_actions.php");


$page->header();
$page->title("Edit Project Query - Fields");

require_once("include/projects_query_tabs.php");
$form->render();

?>


<script type="text/javascript">
  jQuery(document).ready(function($) {
  
  $('#clientaddresses_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_fields_selector.php?query_id=<?php echo $query_id;?>&t=cl'
    });
    return false;
  });
  $('#franchiseeaddresses_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_fields_selector.php?query_id=<?php echo $query_id;?>&t=fr'
    });
    return false;
  });
  
  $('#posaddress_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_fields_selector.php?query_id=<?php echo $query_id;?>&t=pl'
    });
    return false;
  });

  $('#distributionchannel_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_fields_selector.php?query_id=<?php echo $query_id;?>&t=dcs'
    });
    return false;
  });

  $('#project_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_fields_selector.php?query_id=<?php echo $query_id;?>&t=pro'
    });
    return false;
  });

   $('#cer_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_fields_selector.php?query_id=<?php echo $query_id;?>&t=cer'
    });
    return false;
  });

  $('#logistics_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_fields_selector.php?query_id=<?php echo $query_id;?>&t=log'
    });
    return false;
  });

  $('#rental_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_fields_selector.php?query_id=<?php echo $query_id;?>&t=ren'
    });
    return false;
  });

  $('#project_sheet_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_fields_selector.php?query_id=<?php echo $query_id;?>&t=pros'
    });
    return false;
  });

  $('#cms_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_fields_selector.php?query_id=<?php echo $query_id;?>&t=cms'
    });
    return false;
  });

  $('#staff_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_fields_selector.php?query_id=<?php echo $query_id;?>&t=sta'
    });
    return false;
  });

  $('#budget_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_fields_selector.php?query_id=<?php echo $query_id;?>&t=bud'
    });
    return false;
  });

  $('#investments_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_fields_selector.php?query_id=<?php echo $query_id;?>&t=inv'
    });
    return false;
  });

  $('#approved_investments_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_fields_selector.php?query_id=<?php echo $query_id;?>&t=ainv'
    });
    return false;
  });

  $('#real_costs_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_fields_selector.php?query_id=<?php echo $query_id;?>&t=cos'
    });
    return false;
  });

  $('#cost_comparison_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_fields_selector.php?query_id=<?php echo $query_id;?>&t=comp'
    });
    return false;
  });

  $('#milestones_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_fields_selector.php?query_id=<?php echo $query_id;?>&t=mst'
    });
    return false;
  });
  $('#orderstates_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_fields_selector.php?query_id=<?php echo $query_id;?>&t=ost'
    });
    return false;
  });
  $('#items_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_fields_selector.php?query_id=<?php echo $query_id;?>&t=item'
    });
    return false;
  });
});
</script>


<?php

$page->footer();

?>