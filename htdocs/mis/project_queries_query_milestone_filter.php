<?php
/********************************************************************

    project_queries_query_milestone_filter

    Milestone Filter Selection for the Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2014-09-14
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2014-09-14
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";


if(!has_access("can_perform_all_queries") 
  and !has_access("can_perform_projects_query"))
{
	redirect("noaccess.php");
}
require_once "include/projects_query_check_access.php";

require_once "include/projects_query_get_functions.php";

/********************************************************************
    prepare all data needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("project_queries.php");
}

$query_id = param("query_id");

$projectquery = get_query_name($query_id);


$icon = "../pictures/add_item.gif";
$link = "javascript:open_selector('')";


$milestone_filter = get_query_milestone_filter($query_id);




//get milestones
$milestones = array();
$sql = "select milestone_id, concat(milestone_code, ' ', milestone_text) as mname ". 
       " from milestones " . 
	   " where milestone_active = 1 " . 
	   " order by milestone_code";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$milestones[$row["milestone_id"]] = $row["mname"];

	if(!array_key_exists($row["milestone_id"], $milestone_filter))
	{
		$tmp = array();
		$tmp["mfrom" . $row["milestone_id"]] = "";
		$tmp["mto" . $row["milestone_id"]] = "";
		$tmp["mhasdate" . $row["milestone_id"]] = "";
		$tmp["mhasnodate" . $row["milestone_id"]] = "";
		$tmp["logical" . $row["milestone_id"]] = "";
		$milestone_filter[$row["milestone_id"]] = $tmp;
	}
}

$logical = array();
$logical[1] = "and";
$logical[2] = "or";

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projectqueries", "query_generator");

$form->add_hidden("query_id", param("query_id"));
$form->add_section($projectquery["name"]);




$form->add_comment("Please select the the milestone filter criteria of your query.");

$first = true;
foreach($milestones as $id=>$mname)
{
	$form->add_Section($mname);

	if($first == false)
	{
		$form->add_list("logical" . $id, "Condition", $logical, 0, $milestone_filter[$id]["logical" . $id]);
	}
	else
	{
		$form->add_hidden("logical" . $id,"");
	}
	$form->add_checkbox("mhasdate" . $id, "has a date", $milestone_filter[$id]["mhasdate" . $id], "", "Milestone");
	$form->add_checkbox("mhasnodate" . $id, "has no date", $milestone_filter[$id]["mhasnodate" . $id], "", "Milestone");
	$form->add_edit("mfrom" . $id, "From Date", 0, to_system_date($milestone_filter[$id]["mfrom" . $id]), TYPE_DATE, 10);
	$form->add_edit("mto" . $id, "To Date", 0, to_system_date($milestone_filter[$id]["mto" . $id]), TYPE_DATE, 10);
	
	$first = false;
}



$form->add_button("save", "Save Milestone Filter");

if(check_if_query_has_fields($query_id) == true)
{
	$form->add_button("execute", "Execute Query");
}
$form->add_button("back", "Back to the List of Queries");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Process buttons
*********************************************************************/ 
if($form->button("save"))
{
	

	if($form->validate())
	{
		$milestone_filter = array();
		
		foreach($milestones as $id=>$mname)
		{
			$tmp = array();
			$tmp["mfrom" . $id] = from_system_date($form->value("mfrom" . $id));
			$tmp["mto" . $id] = from_system_date($form->value("mto" . $id));
			$tmp["mhasdate" . $id] = $form->value("mhasdate" . $id);
			$tmp["mhasnodate" . $id] = $form->value("mhasnodate" . $id);
			$tmp["logical" . $id] = $form->value("logical" . $id);
			$milestone_filter[$id] = $tmp;
		}

		$sql = "update projectqueries " . 
			   "set projectquery_milestone_filter = " . dbquote(serialize($milestone_filter))  . ", " . 
			   "date_modified = " . dbquote(date("Y-m-d H:s:i")) . ", " . 
				"user_modified = " . dbquote(user_login()) .
			   " where projectquery_id = " . param("query_id");

		$result = mysql_query($sql) or dberror($sql);
	}
}
elseif($form->button("back"))
{
	redirect("project_queries.php");
}
elseif($form->button("execute"))
{
	redirect("project_queries_query_xls.php?query_id=" . param("query_id"));
}

/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page("queries");

require_once("include/mis_page_actions.php");


$page->header();
$page->title("Edit Project Query - Milestone Filter");

require_once("include/projects_query_tabs.php");

$form->render();


$page->footer();
?>