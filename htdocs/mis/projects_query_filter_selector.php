<?php
/********************************************************************

    projects_query_filter_selector.php

    Enter Filter Criteria for the Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2011-07-30
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2011-07-30
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../include/page_modal.php";

check_access("can_perform_queries");

require_once "include/query_get_functions.php";

/********************************************************************
    prepare all data needed
*********************************************************************/
$query_id = param("query_id");

$qt = 1;
if(param("qt"))
{
	$qt = param("qt");
}


/********************************************************************
    get existent filter
*********************************************************************/
$filter = array();
$salesregions = array();

$sql = "select mis_query_filter from mis_queries " .
	   "where mis_query_id = " . $query_id;

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{

	$filter = unserialize($row["mis_query_filter"]);


	if(array_key_exists("clients", $filter))
	{
		$clients = explode("-", $filter["clients"]);
	}
	else
	{
		$clients = array();
	}

	$client_types = explode("-", $filter["clt"]);
	$product_lines = explode("-", $filter["pl"]);
	$product_line_subclasses = explode("-", $filter["plsc"]);
	$project_kinds = explode("-", $filter["pk"]);
	$pos_types = explode("-", $filter["pt"]);
	$pos_type_subclasses = explode("-", $filter["ptsc"]);
	$project_cost_types = explode("-", $filter["pct"]);
	$salesregions = explode("-", $filter["gr"]);
	$supplyingregions = explode("-", $filter["re"]);
	$countries = explode("-", $filter["co"]);
	$store_coordinators = explode("-", $filter["rtc"]);
	$local_store_coordinators = explode("-", $filter["lrtc"]);
	$retail_operators = explode("-", $filter["rto"]);
	$design_contractors = explode("-", $filter["dcontr"]);
	$areas = explode("-", $filter["areas"]);
	$items = explode("-", $filter["items"]);
	$owners = explode("-", $filter["owners"]);
	$suppliers = explode("-", $filter["supp"]);

	if(array_key_exists("forw", $filter))
	{
		$forwarders = explode("-", $filter["forw"]);
	}
	else
	{
		$forwarders = array();
	}


	$design_objectives = explode("-", $filter["dos"]);

	if(array_key_exists("icat", $filter))
	{
		$item_categories = explode("-", $filter["icat"]);
	}
	else
	{
		$item_categories = array();
	}
}


//get  parameters


$sql_client_types = "select client_type_id, client_type_code ".
					"from client_types";

$tmp_filter = "";
$supplier_filter = "";
if(count($filter["supp"]) > 0 and $filter["supp"] !='' )
{

	$suppliers = explode("-", $filter["supp"]);
	$tmp_filter = "";
	foreach($suppliers as $key=>$supplier)
	{
		$tmp_filter = $tmp_filter . $supplier . ",";
	}
	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);
	$supplier_filter = $tmp_filter;

	if($tmp_filter)
	{
		$tmp_filter = " and order_item_supplier_address in (" . $tmp_filter . ") ";
	}


	//get all product lines of the supplier
	$product_line_ids = array();
	
	$sql = "select DISTINCT project_product_line " .
			 " from order_items " .
			 " inner join orders on order_id = order_item_order " . 
			 " left join projects on project_order = order_id " . 
			 " where order_item_type < 3 " .
			 $tmp_filter ;


	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$product_line_ids[] = $row["project_product_line"];
	}

	
	$tmp_filter = "";
	if(count($product_line_ids) > 0)
	{
		$tmp_filter = " and product_line_id IN (" . implode(",", $product_line_ids) . ")";
	}
	
	$sql_product_lines = "select product_line_id, product_line_name " . 
					 "from product_lines " .
		             "where (product_line_id > 0) " .
					 $tmp_filter . 
					 "order by product_line_name";
}
else
{

	$sql_product_lines = "select DISTINCT product_line_id, product_line_name " .
						 " from projects " . 
						 " left join product_lines on product_line_id = project_product_line " . 
						 " left join orders on order_id = project_order " . 
						 " where product_line_id > 0 and order_actual_order_state_code < '900' " .
						 " and project_projectkind in (1, 2, 3, 6, 7, 8, 9) " . 
						 " order by product_line_name";
}


$tmp_filter = "";
$productline_filter = "";
if(count($filter["pl"]) > 0)
{
	$productlines = explode("-", $filter["pl"]);
	$tmp_filter = "";
	foreach($productlines as $key=>$productline)
	{
		$tmp_filter = $tmp_filter . $productline . ",";
	}
	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);
	$productline_filter = $tmp_filter;

	if($tmp_filter)
	{
		$tmp_filter = "where productline_subclass_productline_line_id in (" . $tmp_filter . ") ";
	}
}



$tmp_filter = "";
$item_category_filter = "";
if(count($filter["icat"]) > 0)
{
	$item_categories = explode("-", $filter["icat"]);
	$tmp_filter = "";
	foreach($item_categories as $key=>$item_category)
	{
		$tmp_filter = $tmp_filter . $item_category . ",";
	}
	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);
	$item_category_filter = $tmp_filter;

}

$sql_product_line_subclasses = "select DISTINCT productline_subclass_id, productline_subclass_name ".
							   "from productline_subclass_productlines " .
								    " left join productline_subclasses on  productline_subclass_id = productline_subclass_productline_class_id " .
							   $tmp_filter .
						       "   order by productline_subclass_name";



$sql_pos_types = "select DISTINCT postype_id, postype_name ".
                     "from postypes " .
                     "order by postype_name";


$sql_pos_type_subclasses = "select possubclass_id, possubclass_name ".
							 "from possubclasses " .
							 "order by possubclass_name";

$sql_project_kinds = "select projectkind_id, projectkind_name ".
						 "from projectkinds " .
						 "where projectkind_id > 0 " .
						 "order by projectkind_name";


$sql_project_cost_types = "select project_costtype_id, project_costtype_text ".
						  "from project_costtypes " .
                          "order by project_costtype_text";

$salesregions = array();
$supplyingregions = array();

$sql_salesregion = "select salesregion_id, salesregion_name ".
					"from salesregions order by salesregion_name";

$sql_supplyingregion = "select region_id, region_name ".
						"from regions order by region_name";



$tmp_filter = "";
if(count($filter["gr"]) > 0 and count($filter["re"]) > 0)
{
	$salesregions = explode("-", $filter["gr"]);
	$tmp_filter1 = "";
	foreach($salesregions as $key=>$salesregion)
	{
		$tmp_filter1 = $tmp_filter1 . $salesregion . ",";
	}
	$tmp_filter1 = substr($tmp_filter1, 0, strlen($tmp_filter1) - 2);

	if($tmp_filter1)
	{
		$tmp_filter1 = "country_salesregion in (" . $tmp_filter1 . ") ";
	}

	$supplyingregions = explode("-", $filter["re"]);
	$tmp_filter2 = "";
	foreach($supplyingregions as $key=>$supplyingregion)
	{
		$tmp_filter2 = $tmp_filter2 . $supplyingregion . ",";
	}
	$tmp_filter2 = substr($tmp_filter2, 0, strlen($tmp_filter2) - 2);

	if($tmp_filter2)
	{
		$tmp_filter2 = "country_region in (" . $tmp_filter2 . ") ";
	}

	if($tmp_filter1 and $tmp_filter2)
	{
		$tmp_filter = "where (" . $tmp_filter1 . ' OR ' . $tmp_filter2 . ')';
	}
	elseif($tmp_filter1)
	{
		$tmp_filter = "where " . $tmp_filter1;
	}
	elseif($tmp_filter2)
	{
		$tmp_filter = "where " . $tmp_filter2;
	}
}
elseif(count($filter["gr"]) > 0)
{
	$salesregions = explode("-", $filter["gr"]);
	$tmp_filter = "";
	foreach($salesregions as $key=>$salesregion)
	{
		$tmp_filter = $tmp_filter . $salesregion . ",";
	}
	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

	if($tmp_filter)
	{
		$tmp_filter = "where country_salesregion in (" . $tmp_filter . ") ";
	}
}
elseif(count($filter["re"]) > 0)
{
	$supplyingregions = explode("-", $filter["re"]);
	$tmp_filter = "";
	foreach($supplyingregions as $key=>$supplyingregion)
	{
		$tmp_filter = $tmp_filter . $supplyingregion . ",";
	}
	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

	if($tmp_filter)
	{
		$tmp_filter = "where country_region in (" . $tmp_filter . ") ";
	}
}

if($tmp_filter) {
	$tmp_filter .= ' and country_name <> ""';
}
else
{
	$tmp_filter .= ' where country_name <> ""';
}


$sql_country = "select DISTINCT country_id, country_name " .
			   "from orders " .
			   "left join countries on country_id = order_shop_address_country " .
			   $tmp_filter . 
			   " order by country_name";


$sql_store_coordinators = "select user_id, concat(user_name, ' ', user_firstname) as username " . 
                          "from users " . 
						  "left join user_roles on user_role_user = user_id " . 
						  "where user_role_role in (3, 80) and user_active = 1 " .
						  " order by username";

$sql_local_store_coordinators = "select user_id, concat(user_name, ' ', user_firstname) as username " . 
                          "from users " . 
						  "left join user_roles on user_role_user = user_id " . 
						  "where user_role_role = 10 and user_active = 1 " .
						  " order by username";

$sql_retail_operators = "select user_id, concat(user_name, ' ', user_firstname) as username " . 
                          "from users " . 
						  "left join user_roles on user_role_user = user_id " . 
						  "where user_role_role = 2 and user_active = 1 " .
						  " order by username";



$sql_dcontrs = "select user_id, concat(address_company, ' ', user_name, ' ', user_firstname) as username " . 
				  "from users " . 
				  "left join user_roles on user_role_user = user_id " . 
				  "left join addresses on address_id = user_address " . 
				  "where user_role_role = 7 " .
				  " order by username";

$sql_areas = "select posareatype_id, posareatype_name from posareatypes ". 
             "order by posareatype_name";



$tmp_filter = "";
if($productline_filter)
{
	$tmp_filter .= " and product_line_id IN (" . $productline_filter . ") " ;
}
if($supplier_filter)
{
	$tmp_filter .= " and supplier_address IN (" . $supplier_filter  . ") ";
}


$sql_item_category = "select DISTINCT item_category_id, item_category_name " . 
					 "from items  " . 
					 "left join suppliers on supplier_item = item_id ".
					  " inner join item_supplying_groups on item_supplying_group_item_id = item_id " .
					" inner join product_line_supplying_groups on product_line_supplying_group_group_id = item_supplying_group_supplying_group_id " . 
					" inner join product_lines on product_line_id = product_line_supplying_group_line_id " . 
					 " left join item_categories on item_category_id = item_category " . 
					 "where item_type < 3 and item_category_id > 0 " . 
					 $tmp_filter . 
					 " order by item_category_name";


$tmp_filter = "";
$tmp_filter2 = "";

if($productline_filter or $supplier_filter or $item_category_filter)
{
	if($productline_filter)
	{
		$tmp_filter .= " and project_product_line IN (" . $productline_filter . ") ";
	}
	if($supplier_filter)
	{
		$tmp_filter .= " and order_item_supplier_address IN (" . $supplier_filter  . ") ";
	}
	if($item_category_filter)
	{
		$tmp_filter .= " and item_category IN (" . $item_category_filter  . ") ";
	}
}
else
{
	
	$categories = array();
	$sql_c = "select item_category_id ".
			 "from item_categories " .
	         " order by item_category_name";
	
	$res = mysql_query($sql_c) or dberror($sql_c);
	while ($row = mysql_fetch_assoc($res))
	{
		$categories[$row["item_category_id"]] = $row["item_category_id"];
	}
	
	if(count($categories))
	{
		$tmp_filter .= " and item_category IN (" . implode(',', $categories)  . ") ";
	}
	
	if(array_key_exists("selected_item_category", $_POST) and $_POST["selected_item_category"] > 0)
	{
		$tmp_filter .= " and item_category IN (" . $_POST["selected_item_category"]  . ") ";
	}

	if(array_key_exists("selected_product_line", $_POST) and $_POST["selected_product_line"] > 0)
	{
		$tmp_filter .= " and project_product_line IN (" . $_POST["selected_product_line"]  . ") ";
	}

}


if($qt == '10co') // only items in catalogue orders
{
	
	$tmp_filter .= " and (item_visible = 1 or item_visible_in_production_order = 1) ";
}

$items_filter = $filter["items"]; // Items
$items_filter = substr($items_filter,0, strlen($items_filter)-1); // remove last comma
$items_filter = str_replace("-", ",", $items_filter);

if($items_filter)
{
	$tmp_filter2 = " or (item_id IN (" . $items_filter . ")) ";
}

$items_were_selected = false;
foreach($_POST as $key=>$value)
{
	if(substr($key,0,3) == 'IT_' and $_POST[$key] == 1)
	{
		$items_were_selected = true;
	}
}

if(param("save_form") == 'items' and $items_were_selected == false)
{
	$sql_items = "select DISTINCT item_id, concat(item_code, ' ', item_name) as itemname " . 
				 "from items  " . 
				 "where item_type = 0 " . 
				 " order by item_code";
}
elseif(array_key_exists("item_search_term", $_POST) and $_POST["item_search_term"] != '')
{
	$tmp_filter = ' and (item_code LIKE "%' . $_POST["item_search_term"] . '%" or item_name LIKE "%' . $_POST["item_search_term"] . '%")';

	$sql_items = "select DISTINCT item_id, concat(item_code, ' ', item_name) as itemname " . 
				 "from items  " . 
				 "where (item_type < 3 " . 
				 $tmp_filter . ") " . 
				 $tmp_filter2 . 
				 " order by item_code";
}
else
{
	/* not used since not applicable for old product lines
	$sql_items = "select DISTINCT item_id, concat(item_code, ' ', item_name) as itemname " . 
				 "from items  " . 
				 "left join suppliers on supplier_item = item_id ".
		          " inner join item_supplying_groups on item_supplying_group_item_id = item_id " .
				 " inner join product_line_supplying_groups on product_line_supplying_group_group_id = item_supplying_group_supplying_group_id " . 
				 " inner join product_lines on product_line_id = product_line_supplying_group_line_id " . 
				 "where (item_type < 3 " . 
				 $tmp_filter . ") " . 
				 $tmp_filter2 . 
				 " order by item_code";

	*/
	$sql_items = "select DISTINCT item_id, concat(item_code, ' ', item_name) as itemname " .
		         " from order_items " . 
		         " inner join items on item_id = order_item_item " . 
		         " inner join orders on order_id = order_item_order " . 
				 " left join projects on project_order = order_id " . 
		         "where (order_item_type < 3 " . 
				 $tmp_filter . ") " . 
				 $tmp_filter2 . 
				 " order by item_code";
}


$country_filter = $filter["co"]; // Countries
$country_filter = substr($country_filter,0, strlen($country_filter)-1); // remove last comma
$country_filter = str_replace("-", ",", $country_filter);

if($country_filter)
{
	$sql_owners = "select distinct order_franchisee_address_id, " .
				  " concat(address_company, ' - ', country_name) as company " . 
				  " from orders " .
				  " left join addresses on address_id = order_franchisee_address_id " . 
				  " left join countries on country_id = address_country " . 
				  " where order_actual_order_state_code <> '900' and order_type = 1 " .
				  " and address_company <> '' and order_franchisee_address_id > 0 " . 
		          " and country_id IN (" . $country_filter . ") " .
				  " order by address_company, country_name";
}
else
{
	$sql_owners = "select distinct order_franchisee_address_id, " .
				  " concat(address_company, ' - ', country_name) as company " . 
				  " from orders " .
				  " left join addresses on address_id = order_franchisee_address_id " . 
				  " left join countries on country_id = address_country " . 
				  " where order_actual_order_state_code <> '900' and order_type = 1 " .
				  " and address_company <> ''  and order_franchisee_address_id > 0 " . 
				  " order by address_company, country_name";
}




$sql_suppliers = "select DISTINCT address_id, address_company " .
                 "from suppliers " .
				 "left join addresses on address_id = supplier_address " . 
				 "where address_type IN (2, 8) " .
				 "order by address_company";

$sql_forwarders = "select DISTINCT address_id, address_company " .
                 "from addresses " .
				 "where address_type IN (3) " .
				 "order by address_company";

$sql_design_objectives = "select design_objective_item_id, design_objective_item_name, " .
						 "    design_objective_group_name, postype_name " .
						 "from design_objective_items left join design_objective_groups on design_objective_item_group = design_objective_group_id " .
						 "    left join postypes on design_objective_group_postype = postype_id " . 
						 " where postype_name <> '' " . 
						 " order by postype_name, design_objective_group_name, design_objective_item_name ";



$sql_clients = "select DISTINCT order_client_address, concat(country_name, ', ', address_company) as company  " .
				   "from orders " . 
				   "left join addresses on address_id = order_client_address ".
				   "left join countries on country_id = address_country " .
				   "where address_id > 0 ".
				   "order by country_name, address_company";


/********************************************************************
    save data
*********************************************************************/
if(param("save_form") != '')
{
	$new_filter = array();
	
	$new_filter["ptst"] = $filter["ptst"];
	$new_filter["fst"] =  $filter["fst"];
	$new_filter["tst"] =  $filter["tst"];

	$new_filter["fdy"] =  $filter["fdy"];
	$new_filter["fdm"] =  $filter["fdm"];
	$new_filter["tdy"] =  $filter["tdy"];
	$new_filter["tdm"] =  $filter["tdm"];

	$new_filter["fdy1"] =  $filter["fdy1"];
	$new_filter["fdm1"] =  $filter["fdm1"];
	$new_filter["tdy1"] =  $filter["tdy1"];
	$new_filter["tdm1"] =  $filter["tdm1"];

	$new_filter["fdy2"] =  $filter["fdy2"];
	$new_filter["fdm2"] =  $filter["fdm2"];
	$new_filter["tdy2"] =  $filter["tdy2"];
	$new_filter["tdm2"] =  $filter["tdm2"];

	if(array_key_exists("clients", $filter))
	{
		$new_filter["clients"] =  $filter["clients"];
	}
	else
	{
		$new_filter["clients"] =  "";
	}

	if(array_key_exists("fst2", $filter))
	{
		$new_filter["fst2"] =  $filter["fst2"];
	}
	else
	{
		$new_filter["fst2"] =  "";
	}

	if(array_key_exists("tst2", $filter))
	{
		$new_filter["tst2"] =  $filter["tst2"];
	}
	else
	{
		$new_filter["tst2"] =  "";
	}

	
	
	$new_filter["clt"] =  $filter["clt"];
	$new_filter["pl"] =  $filter["pl"];
	$new_filter["plsc"] =  $filter["plsc"];
	$new_filter["pk"] =  $filter["pk"];
	$new_filter["pt"] =  $filter["pt"];
	$new_filter["ptsc"] =  $filter["ptsc"];
	$new_filter["pct"] =  $filter["pct"];
	$new_filter["gr"] =  $filter["gr"];
	$new_filter["re"] =  $filter["re"];
	$new_filter["co"] =  $filter["co"];
	$new_filter["rtc"] =  $filter["rtc"];
	$new_filter["lrtc"] =  $filter["lrtc"];
	$new_filter["rto"] =  $filter["rto"];
	$new_filter["dcontr"] =  $filter["dcontr"];
	$new_filter["detail"] =  $filter["detail"];

	$new_filter["idvi"] = $filter["idvi"];
	$new_filter["arch"] = $filter["arch"];

	$new_filter["subfy"] = $filter["subfy"];
	$new_filter["subfm"] = $filter["subfm"];
	$new_filter["subfd"] = $filter["subfd"];
	$new_filter["subty"] = $filter["subty"];
	$new_filter["subtm"] = $filter["subtm"];
	$new_filter["subtd"] = $filter["subtd"];
	$new_filter["areas"] = $filter["areas"];
	$new_filter["items"] = $filter["items"];

	$new_filter["owners"] = $filter["owners"];
	$new_filter["supp"] = $filter["supp"];

	if(array_key_exists("forw", $filter))
	{
		$new_filter["forw"] =  $filter["forw"];
	}
	else
	{
		$new_filter["icat"] =  "";
	}


	$new_filter["dos"] = $filter["dos"];


	if(array_key_exists("icat", $filter))
	{
		$new_filter["icat"] =  $filter["icat"];
	}
	else
	{
		$new_filter["icat"] =  "";
	}

	if(array_key_exists("exclude_e_stores", $filter))
	{
		$new_filter["exclude_e_stores"] =  $filter["exclude_e_stores"];
	}
	else
	{
		$new_filter["exclude_e_stores"] =  "";
	}


	if(param("save_form") == "clients") //Clients
	{
		$CLIENTS = "";

		$res = mysql_query($sql_clients) or dberror($sql_clients);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["CLIENTS_" . $row["order_client_address"]])
			{
				$CLIENTS .=$row["order_client_address"] . "-";
			}
		}
		
		$new_filter["clients"] = $CLIENTS;
	}
	elseif(param("save_form") == "clt") //Client Types
	{
		$CLT = "";

		$res = mysql_query($sql_client_types) or dberror($sql_client_types);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["CLT_" . $row["client_type_id"]])
			{
				$CLT .=$row["client_type_id"] . "-";
			}
		}
		
		$new_filter["clt"] = $CLT;
	}
	elseif(param("save_form") == "pl") //Product Lines
	{
		$PL = "";

		$res = mysql_query($sql_product_lines) or dberror($sql_product_lines);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PL_" . $row["product_line_id"]])
			{
				$PL .=$row["product_line_id"] . "-";
			}
		}
		
		$new_filter["pl"] = $PL;
	}
	elseif(param("save_form") == "plsc") //Product Line Subclasses
	{
		$PLSC = "";

		$res = mysql_query($sql_product_line_subclasses) or dberror($sql_product_line_subclasses);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PLSC_" . $row["productline_subclass_id"]])
			{
				$PLSC .=$row["productline_subclass_id"] . "-";
			}
		}
		
		$new_filter["plsc"] = $PLSC;
	}
	elseif(param("save_form") == "pt") //POS Types
	{
		$PT = "";

		$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PT_" . $row["postype_id"]])
			{
				$PT .=$row["postype_id"] . "-";
			}
		}
		
		$new_filter["pt"] = $PT;
	}
	elseif(param("save_form") == "ptsc") //POS Type Subclasses
	{
		$PTSC = "";

		if($_POST["PTSC_ns"])
		{
			$PTSC .="ns-";
		}
		
		$res = mysql_query($sql_pos_type_subclasses) or dberror($sql_pos_type_subclasses);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PTSC_" . $row["possubclass_id"]])
			{
				$PTSC .=$row["possubclass_id"] . "-";
			}
		}
		
		$new_filter["ptsc"] = $PTSC;
	}
	elseif(param("save_form") == "pk") //Product Kinds
	{
		$PK = "";

		$res = mysql_query($sql_project_kinds) or dberror($sql_project_kinds);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PK_" . $row["projectkind_id"]])
			{
				$PK .=$row["projectkind_id"] . "-";
			}
		}
		
		$new_filter["pk"] = $PK;
	}
	elseif(param("save_form") == "pct") //project cost types
	{
		$PCT = "";

		$res = mysql_query($sql_project_cost_types) or dberror($sql_project_cost_types);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PCT_" . $row["project_costtype_id"]])
			{
				$PCT .=$row["project_costtype_id"] . "-";
			}
		}
		$new_filter["pct"] = $PCT;
	}
	elseif(param("save_form") == "gr") //Geographicals Regions
	{
		$GR = "";

		$res = mysql_query($sql_salesregion) or dberror($sql_salesregion);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["GR_" . $row["salesregion_id"]])
			{
				$GR .=$row["salesregion_id"] . "-";
			}
		}
		
		$new_filter["gr"] = $GR;
	}
	elseif(param("save_form") == "areas") //neighbourhood Areas
	{
		$AR = "";

		$res = mysql_query($sql_areas) or dberror($sql_areas);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["AR_" . $row["posareatype_id"]])
			{
				$AR .=$row["posareatype_id"] . "-";
			}
		}
		
		$new_filter["areas"] = $AR;
	}
	elseif(param("save_form") == "items") //Items
	{
		$IT = "";

		$res = mysql_query($sql_items) or dberror($sql_items);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["IT_" . $row["item_id"]])
			{
				$IT .=$row["item_id"] . "-";
			}
		}
		
		$new_filter["items"] = $IT;

	}
	elseif(param("save_form") == "re") //Supplied Regions
	{
		$RE = "";

		$res = mysql_query($sql_supplyingregion) or dberror($sql_supplyingregion);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["RE_" . $row["region_id"]])
			{
				$RE .=$row["region_id"] . "-";
			}
		}
		
		$new_filter["re"] = $RE;
	}
	elseif(param("save_form") == "co") //Countries
	{
		$CO = "";

		$res = mysql_query($sql_country) or dberror($sql_country);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["CO_" . $row["country_id"]])
			{
				$CO .=$row["country_id"] . "-";
			}
		}
		
		$new_filter["co"] = $CO;
	}
	elseif(param("save_form") == "rtc") //Store Coordinators
	{
		$RTC = "";

		$res = mysql_query($sql_store_coordinators) or dberror($sql_store_coordinators);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["RTC_" . $row["user_id"]])
			{
				$RTC .=$row["user_id"] . "-";
			}
		}
		
		$new_filter["rtc"] = $RTC;
	}
	elseif(param("save_form") == "lrtc") //Local Store Coordinators
	{
		$LRTC = "";

		$res = mysql_query($sql_local_store_coordinators) or dberror($sql_local_store_coordinators);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["LRTC_" . $row["user_id"]])
			{
				$LRTC .=$row["user_id"] . "-";
			}
		}
		
		$new_filter["lrtc"] = $LRTC;
	}
	elseif(param("save_form") == "rto") //Retail Coordinators
	{
		$RTO = "";

		$res = mysql_query($sql_retail_operators) or dberror($sql_retail_operators);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["RTO_" . $row["user_id"]])
			{
				$RTO .=$row["user_id"] . "-";
			}
		}
		
		$new_filter["rto"] = $RTO;
	}
	elseif(param("save_form") == "dcontr") //Design Contractors
	{
		$DCONTR = "";

		$res = mysql_query($sql_dcontrs) or dberror($sql_dcontrs);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["DCONTR_" . $row["user_id"]])
			{
				$DCONTR .=$row["user_id"] . "-";
			}
		}
		
		$new_filter["dcontr"] = $DCONTR;
	}
	elseif(param("save_form") == "owners") //owner companies
	{
		$OWNERS = "";

		$res = mysql_query($sql_owners) or dberror($sql_owners);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["OWNERS_" . $row["order_franchisee_address_id"]])
			{
				$OWNERS .=$row["order_franchisee_address_id"] . "-";
			}
		}
		
		$new_filter["owners"] = $OWNERS;
	}
	elseif(param("save_form") == "supp") //Suppliers
	{
		$SUPP = "";

		$res = mysql_query($sql_suppliers) or dberror($sql_suppliers);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["SUPP_" . $row["address_id"]])
			{
				$SUPP .=$row["address_id"] . "-";
			}
		}
		
		$new_filter["supp"] = $SUPP;
	}
	elseif(param("save_form") == "forw") //Forwarders
	{
		$FORW = "";

		$res = mysql_query($sql_forwarders) or dberror($sql_forwarders);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["FORW_" . $row["address_id"]])
			{
				$FORW .=$row["address_id"] . "-";
			}
		}
		
		$new_filter["forw"] = $FORW;
	}
	elseif(param("save_form") == "dos") //Design Objectives
	{
		$DOS = "";

		$res = mysql_query($sql_design_objectives) or dberror($sql_design_objectives);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["DOS_" . $row["design_objective_item_id"]])
			{
				$DOS .=$row["design_objective_item_id"] . "-";
			}
		}
		
		$new_filter["dos"] = $DOS;
	}
	elseif(param("save_form") == "icat") //Item categories
	{
		$ICAT = "";

		$res = mysql_query($sql_item_category) or dberror($sql_item_category);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["ICAT_" . $row["item_category_id"]])
			{
				$ICAT .=$row["item_category_id"] . "-";
			}
		}
		$new_filter["icat"] = $ICAT;
	}

	$sql = "update mis_queries " . 
		   "set mis_query_filter = " . dbquote(serialize($new_filter)) . 
		   " where mis_query_id = " . param("query_id");

	$result = mysql_query($sql) or dberror($sql);
	
}



/********************************************************************
    Create Form
*********************************************************************/ 

$form = new form("cer_benchmarks", "benchmark_selector");
$form->add_hidden("query_id", $query_id);
$form->add_hidden("qt", $qt);

if(param("s") == "owners") // Owner Companies
{
	$page_title = "Owner/Franchisee Selector";
	$form->add_hidden("save_form", "owners");

	$res = mysql_query($sql_owners) or dberror($sql_owners);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["order_franchisee_address_id"], $owners))
		{
			$form->add_checkbox("OWNERS_" . $row["order_franchisee_address_id"], $row["company"], true);
		}
		else
		{
			$form->add_checkbox("OWNERS_" . $row["order_franchisee_address_id"], $row["company"], false);
		}
		
	}
}
elseif(param("s") == "supp") // Suppliers
{
	$page_title = "Supplier Selector";
	$form->add_hidden("save_form", "supp");

	$res = mysql_query($sql_suppliers) or dberror($sql_suppliers);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["address_id"], $suppliers))
		{
			$form->add_checkbox("SUPP_" . $row["address_id"], $row["address_company"], true);
		}
		else
		{
			$form->add_checkbox("SUPP_" . $row["address_id"], $row["address_company"], false);
		}
		
	}
}
elseif(param("s") == "forw") // Forwarders
{
	$page_title = "Forwarder Selector";
	$form->add_hidden("save_form", "forw");

	$res = mysql_query($sql_forwarders) or dberror($sql_forwarders);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["address_id"], $forwarders))
		{
			$form->add_checkbox("FORW_" . $row["address_id"], $row["address_company"], true);
		}
		else
		{
			$form->add_checkbox("FORW_" . $row["address_id"], $row["address_company"], false);
		}
		
	}
}
elseif(param("s") == "dos") // Design Objectives
{
	$page_title = "Design Objectives Selector";
	$form->add_hidden("save_form", "dos");

	$res = mysql_query($sql_design_objectives) or dberror($sql_design_objectives);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["design_objective_item_id"], $design_objectives))
		{
			$form->add_checkbox("DOS_" . $row["design_objective_item_id"], $row["postype_name"] . " - " . $row["design_objective_group_name"] . " - " . $row["design_objective_item_name"], true);
		}
		else
		{
			$form->add_checkbox("DOS_" . $row["design_objective_item_id"], $row["postype_name"] . " - " . $row["design_objective_group_name"] . " - " . $row["design_objective_item_name"], false);
		}
		
	}
}
elseif(param("s") == "clients") // Clients
{
	$page_title = "Client Selector";
	$form->add_hidden("save_form", "clients");

	$res = mysql_query($sql_clients) or dberror($sql_clients);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["order_client_address"], $clients))
		{
			$form->add_checkbox("CLIENTS_" . $row["order_client_address"], $row["company"], true);
		}
		else
		{
			$form->add_checkbox("CLIENTS_" . $row["order_client_address"], $row["company"], false);
		}
		
	}
}
elseif(param("s") == "clt") // Client Types
{
	$page_title = "Client Type Selector";
	$form->add_hidden("save_form", "clt");

	$res = mysql_query($sql_client_types) or dberror($sql_client_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["client_type_id"], $client_types))
		{
			$form->add_checkbox("CLT_" . $row["client_type_id"], $row["client_type_code"], true);
		}
		else
		{
			$form->add_checkbox("CLT_" . $row["client_type_id"], $row["client_type_code"], false);
		}
		
	}
}
elseif(param("s") == "pl") // Product Lines
{
	$page_title = "Product Line Selector";
	$form->add_hidden("save_form", "pl");

	$res = mysql_query($sql_product_lines) or dberror($sql_product_lines);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["product_line_id"], $product_lines))
		{
			$form->add_checkbox("PL_" . $row["product_line_id"], $row["product_line_name"], true);
		}
		else
		{
			$form->add_checkbox("PL_" . $row["product_line_id"], $row["product_line_name"], false);
		}
		
	}
}
elseif(param("s") == "plsc") // Product Line Subclasses
{
	$page_title = "Product Line Subclass Selector";
	$form->add_hidden("save_form", "plsc");

	$res = mysql_query($sql_product_line_subclasses) or dberror($sql_product_line_subclasses);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["productline_subclass_id"], $product_line_subclasses))
		{
			$form->add_checkbox("PLSC_" . $row["productline_subclass_id"], $row["productline_subclass_name"], true);
		}
		else
		{
			$form->add_checkbox("PLSC_" . $row["productline_subclass_id"], $row["productline_subclass_name"], false);
		}
		
	}
}
elseif(param("s") == "pk") // Project Types
{
	$page_title = "Project Type Selector";
	$form->add_hidden("save_form", "pk");

	$res = mysql_query($sql_project_kinds) or dberror($sql_project_kinds);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["projectkind_id"], $project_kinds))
		{
			$form->add_checkbox("PK_" . $row["projectkind_id"], $row["projectkind_name"], true);
		}
		else
		{
			$form->add_checkbox("PK_" . $row["projectkind_id"], $row["projectkind_name"], false);
		}
		
	}
}
elseif(param("s") == "pt") // POS Types
{
	$page_title = "POS Type Selector";
	$form->add_hidden("save_form", "pt");

	$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["postype_id"], $pos_types))
		{
			$form->add_checkbox("PT_" . $row["postype_id"], $row["postype_name"], true);
		}
		else
		{
			$form->add_checkbox("PT_" . $row["postype_id"], $row["postype_name"], false);
		}
	}
}
elseif(param("s") == "ptsc") // POS Type Subclass
{
	$page_title = "POS Type Subclass Selector";
	$form->add_hidden("save_form", "ptsc");


	if(in_array('ns', $pos_type_subclasses))
	{
		$form->add_checkbox("PTSC_ns", "POS without a subclass", true);
	}
	else
	{
		$form->add_checkbox("PTSC_ns", "POS without a subclass", false);
	}

	$res = mysql_query($sql_pos_type_subclasses) or dberror($sql_pos_type_subclasses);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["possubclass_id"], $pos_type_subclasses))
		{
			$form->add_checkbox("PTSC_" . $row["possubclass_id"], $row["possubclass_name"], true);
		}
		else
		{
			$form->add_checkbox("PTSC_" . $row["possubclass_id"], $row["possubclass_name"], false);
		}
		
	}
}
elseif(param("s") == "pct") // Project Cost Types
{
	$page_title = "Legal Type Selector";
	$form->add_hidden("save_form", "pct");

	$res = mysql_query($sql_project_cost_types) or dberror($sql_project_cost_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["project_costtype_id"], $project_cost_types))
		{
			$form->add_checkbox("PCT_" . $row["project_costtype_id"], $row["project_costtype_text"], true);
		}
		else
		{
			$form->add_checkbox("PCT_" . $row["project_costtype_id"], $row["project_costtype_text"], false);
		}
		
	}
}
elseif(param("s") == "gr") // Sales regions
{
	$page_title = "Geographical Region Selector";
	$form->add_hidden("save_form", "gr");

	$res = mysql_query($sql_salesregion) or dberror($sql_salesregion);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["salesregion_id"], $salesregions))
		{
			$form->add_checkbox("GR_" . $row["salesregion_id"], $row["salesregion_name"], true);
		}
		else
		{
			$form->add_checkbox("GR_" . $row["salesregion_id"], $row["salesregion_name"], false);
		}
		
	}
}
elseif(param("s") == "areas") // Neighbourhood Areas
{
	$page_title = "Neighbourhood Locations Selector";
	$form->add_hidden("save_form", "areas");

	$res = mysql_query($sql_areas) or dberror($sql_areas);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["posareatype_id"], $areas))
		{
			$form->add_checkbox("AR_" . $row["posareatype_id"], $row["posareatype_name"], true);
		}
		else
		{
			$form->add_checkbox("AR_" . $row["posareatype_id"], $row["posareatype_name"], false);
		}
		
	}
}
elseif(param("s") == "re") // Supplied regions
{
	$page_title = "Supplied Region Selector";
	$form->add_hidden("save_form", "re");

	$res = mysql_query($sql_supplyingregion) or dberror($sql_supplyingregion);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["region_id"], $supplyingregions))
		{
			$form->add_checkbox("RE_" . $row["region_id"], $row["region_name"], true);
		}
		else
		{
			$form->add_checkbox("RE_" . $row["region_id"], $row["region_name"], false);
		}
		
	}
}
elseif(param("s") == "co") // countries
{
	$page_title = "Country Selector";
	$form->add_hidden("save_form", "co");

	$res = mysql_query($sql_country) or dberror($sql_country);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["country_id"], $countries))
		{
			$form->add_checkbox("CO_" . $row["country_id"], $row["country_name"], true);
		}
		else
		{
			$form->add_checkbox("CO_" . $row["country_id"], $row["country_name"], false);
		}
		
	}
}
elseif(param("s") == "rtc") // store coordinatros
{
	$page_title = "Project Leader Selector";
	$form->add_hidden("save_form", "rtc");

	$res = mysql_query($sql_store_coordinators) or dberror($sql_store_coordinators);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["user_id"], $store_coordinators))
		{
			$form->add_checkbox("RTC_" . $row["user_id"], $row["username"], true);
		}
		else
		{
			$form->add_checkbox("RTC_" . $row["user_id"], $row["username"], false);
		}
		
	}
}
elseif(param("s") == "lrtc") // localstore coordinatros
{
	$page_title = "Local Project Leader Selector";
	$form->add_hidden("save_form", "lrtc");

	$res = mysql_query($sql_local_store_coordinators) or dberror($sql_local_store_coordinators);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["user_id"], $local_store_coordinators))
		{
			$form->add_checkbox("LRTC_" . $row["user_id"], $row["username"], true);
		}
		else
		{
			$form->add_checkbox("LRTC_" . $row["user_id"], $row["username"], false);
		}
		
	}
}
elseif(param("s") == "rto") // retail operators
{
	$page_title = "Logistics Coordinator Selector";
	$form->add_hidden("save_form", "rto");

	$res = mysql_query($sql_retail_operators) or dberror($sql_retail_operators);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["user_id"], $retail_operators))
		{
			$form->add_checkbox("RTO_" . $row["user_id"], $row["username"], true);
		}
		else
		{
			$form->add_checkbox("RTO_" . $row["user_id"], $row["username"], false);
		}
		
	}
}
elseif(param("s") == "dcontr") // design contractors
{
	$page_title = "Design Contfractor Selector";
	$form->add_hidden("save_form", "dcontr");

	$res = mysql_query($sql_dcontrs) or dberror($sql_dcontrs);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["user_id"], $design_contractors))
		{
			$form->add_checkbox("DCONTR_" . $row["user_id"], $row["username"], true);
		}
		else
		{
			$form->add_checkbox("DCONTR_" . $row["user_id"], $row["username"], false);
		}
		
	}
}
elseif(param("s") == "icat") // Item Categories
{
	$page_title = "Item Category Selector";
	$form->add_hidden("save_form", "icat");

	$res = mysql_query($sql_item_category) or dberror($sql_item_category);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["item_category_id"], $item_categories))
		{
			$form->add_checkbox("ICAT_" . $row["item_category_id"], $row["item_category_name"], true);
		}
		else
		{
			$form->add_checkbox("ICAT_" . $row["item_category_id"], $row["item_category_name"], false);
		}
		
	}
}
elseif(param("s") == "items") // items
{
	$page_title = "Item Selector";

	if($productline_filter or $supplier_filter or $item_category_filter)
	{
			
		if($items_filter)
		{
			$form->add_section("Selected Items");
			$res = mysql_query($sql_items) or dberror($sql_items);
			while($row = mysql_fetch_assoc($res))
			{
				if(in_array($row["item_id"], $items))
				{
					$form->add_checkbox("IT_" . $row["item_id"], $row["itemname"], true);
				}
			}

			
		}
		
		
		$form->add_section("Unselected Items");
		$res = mysql_query($sql_items) or dberror($sql_items);
		while($row = mysql_fetch_assoc($res))
		{
			if(!in_array($row["item_id"], $items))
			{
				$form->add_checkbox("IT_" . $row["item_id"], $row["itemname"], false);
			}
		}
		
		$form->add_hidden("save_form", 'items');
		
	}
	else
	{
	
		$form->add_section("Select");

		if(array_key_exists("item_search_term", $_POST) and $_POST["item_search_term"] != '')
		{
			$form->add_list("selected_product_line", "Product Line", $sql_product_lines, 0);
		}
		elseif(array_key_exists("selected_product_line", $_POST))
		{
			$form->add_list("selected_product_line", "Product Line", $sql_product_lines, 0, $_POST["selected_product_line"]);
		}
		else
		{
			$form->add_list("selected_product_line", "Product Line", $sql_product_lines, 0);
		}

		if(array_key_exists("item_search_term", $_POST) and $_POST["item_search_term"] != '')
		{
			$form->add_list("selected_item_category", "Item Category", $sql_item_category);
		}
		elseif(array_key_exists("selected_item_category", $_POST))
		{
			$form->add_list("selected_item_category", "Item Category", $sql_item_category, 0, $_POST["selected_item_category"]);
		}
		else
		{
			$form->add_list("selected_item_category", "Item Category", $sql_item_category);
		}

		if(array_key_exists("item_search_term", $_POST))
		{
		    $form->add_edit("item_search_term", "Search Term", 0 , $_POST["item_search_term"]);
		}
		else
		{
			$form->add_edit("item_search_term", "Search Term", 0);
		}

		$form->add_hidden("s", "items");

		$tmp = "<input id=\"load_items\" type=\"submit\" value=\"Show Selected Items\" class=\"modalbutton\" onmouseout=\"this.style.color='#006699'\" onmouseover=\"this.style.color='#FF0000';this.style.cursor='pointer'\" style=\"color: rgb(0, 102, 153); cursor: pointer;\">";
		$form->add_label("reload", "", RENDER_HTML, $tmp);

		if($items_filter)
		{
			$form->add_section("Selected Items");
			$res = mysql_query($sql_items) or dberror($sql_items);
			while($row = mysql_fetch_assoc($res))
			{
				if(in_array($row["item_id"], $items))
				{
					$form->add_checkbox("IT_" . $row["item_id"], $row["itemname"], true);
				}
			}

			
		}
		
		if($productline_filter or $item_category_filter
			or (array_key_exists("selected_product_line", $_POST) or array_key_exists("selected_item_category", $_POST)
			and  $_POST["selected_product_line"] > 0
			and  $_POST["selected_item_category"] > 0)
			)
		{
			$form->add_section("Unselected Items");
			$res = mysql_query($sql_items) or dberror($sql_items);
			while($row = mysql_fetch_assoc($res))
			{
				if(!in_array($row["item_id"], $items))
				{
					$form->add_checkbox("IT_" . $row["item_id"], $row["itemname"], false);
				}
			}
		}

		if(array_key_exists("save_form", $_POST) and $_POST["save_form"] == 'items')
		{
			$form->add_hidden("save_form", 'items');
		}
		else
		{
			$form->add_hidden("save_form", '');
		}
	}
	
}

if(param("s") == "items") // items
{

	$tmp = "<input id=\"load_items\" type=\"submit\" value=\"Save Item Selection\" class=\"modalbutton\" onmouseout=\"this.style.color='#006699'\" onmouseover=\"this.style.color='#FF0000';this.style.cursor='pointer'\" style=\"color: rgb(0, 102, 153); cursor: pointer;\"  onclick=\"javascript:$('#save_form').val('items');\">";
	$form->add_label("save_items", "", RENDER_HTML, $tmp);
}
else
{
	$form->add_input_submit("submit", "Save Selection", 0);
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Process buttons
*********************************************************************/ 

/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page_Modal("cer_benchmarks");

$page->header();
$page->title($page_title);

$form->render();

if(param("save_form") != '')
{
?>

<script languege="javascript">
	var back_link = "projects_query_filter.php?query_id=<?php echo $query_id;?>&qt=<?php echo $qt;?>"; 
	$.nyroModalRemove();

</script>

<?php
}

?>

</script>


<?php
$page->footer();
