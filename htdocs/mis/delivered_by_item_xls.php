<?php
/********************************************************************

    delivered_by_item_xls.php

    Show data in Excel

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2003-06-26
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-06-27
    Version:        1.0.0

    Copyright (c) 2003, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/calculate_functions.php";

check_access("can_perform_queries");


/********************************************************************
    prepare all data needed
*********************************************************************/
$from_date = $_REQUEST["p1"];
$to_date = $_REQUEST["p2"];
$country = $_REQUEST["p3"];
$sql_order_items = $_REQUEST["p4"];
$list_filter = $_REQUEST["p5"];


$items_consumed = array();
$turn_over = array();
$list_total = 0;


$sql = $sql_order_items . " where " . $list_filter;
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
    $items_delivered = get_item_total_consumption_data($row["item_id"], $from_date, $to_date, $country, "");
    $items_consumed[$row["item_id"]] = $items_delivered["item_consumption"];
    $turn_over[$row["item_id"]] = $items_delivered["turn_over"];
    $list_total = $list_total + $items_delivered["turn_over_1"];
}

if($country)
{
    $sql = "select country_name from countries where country_id = " . $country;
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $country_name = $row["country_name"];
    }
}

/********************************************************************
    Populate list
*********************************************************************/ 

// list of ordered items
$list2 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list2->set_entity("order_items");
$list2->set_order("item_code");
$list2->set_filter($list_filter);

$list2->add_column("item_code", "Code", "", LIST_FILTER_FREE);
$list2->add_column("item_name", "Name", "", LIST_FILTER_FREE);
$list2->add_text_column("consumption", "Delivered", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $items_consumed);
$list2->add_text_column("turn_over", "Turn Over", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $turn_over);

$list2->set_footer("item_code", "Total");
$list2->set_footer("turn_over", number_format($list_total,2, ".", ""));

$list2->process();

header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: filename=qeury.xls");

echo "<style type='text/css'>";
include("../include/main.css"); 
echo "</style>";

echo "Delivered Items<br>";
if($from_date)
{
    echo "From " . from_system_date($from_date) . "<br>";
}

if($to_date)
{
    echo "To " . from_system_date($to_date) . "<br>";
}
if($country)
{
    echo "Country " . $country_name . "<br>";
}

$list2->render();
?>
