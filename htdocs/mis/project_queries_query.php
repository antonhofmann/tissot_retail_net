<?php
/********************************************************************

    project_queries_query.php

    Creation and mutation of queries.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2014-09-14
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2014-09-14
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

if(!has_access("can_perform_all_queries") 
  and !has_access("can_perform_projects_query"))
{
	redirect("noaccess.php");
}
require_once "include/projects_query_check_access.php";

require_once "include/projects_query_get_functions.php";




if(param("copy_id"))
{
	
	$sql = "select * from projectqueries where projectquery_id = " . param("copy_id");
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		
		$sql = "INSERT INTO projectqueries (". 
			   "projectquery_owner, projectquery_group, projectquery_name, projectquery_print_filter, " . 
			   "projectquery_fields, projectquery_field_order, projectquery_filter, projectquery_order, " . "projectquery_grouping01, projectquery_grouping02, projectquery_show_number_of_projects, " . "projectquery_sum_fields, projectquery_avg_fields, " . 
			   "projectquery_area_perception, projectquery_milestone_filter, " . 
			   "user_created, date_created) ". 
			   "VALUES (" . 
			   dbquote(user_id()) . ", " .
			   dbquote($row["projectquery_group"]) . ", " .
			   dbquote($row["projectquery_name"]. " - copy") . ", " .
			   dbquote($row["projectquery_print_filter"]) . ", " .
			   dbquote($row["projectquery_fields"]) . ", " .
			   dbquote($row["projectquery_field_order"]) . ", " .
			   dbquote($row["projectquery_filter"]) . ", " .
			   dbquote($row["projectquery_order"]) . ", " .
			   dbquote($row["projectquery_grouping01"]) . ", " .
			   dbquote($row["projectquery_grouping02"]) . ", " .
			   dbquote($row["projectquery_show_number_of_projects"]) . ", " .
			   dbquote($row["projectquery_sum_fields"]) . ", " .
			   dbquote($row["projectquery_avg_fields"]) . ", " .
			   dbquote($row["projectquery_area_perception"]) . ", " .
			   dbquote($row["projectquery_milestone_filter"]) . ", " .
			   dbquote(user_login()) . ", " .
			   dbquote(date("Y-m-d")) . ")";

			   
						
		$result = mysql_query($sql) or dberror($sql);
		$new_id = mysql_insert_id();
		redirect("project_queries_query.php?query_id=" . $new_id);

	}
}

			

if(param("query_id"))
{
	param("id", param("query_id"));
	$query_id = param("query_id");
}
if(id())
{
	param("query_id",id());
	$query_id = id();
}

$user = get_user(user_id());

//update filter in case elements are missing
if(isset($query_id))
{
	$filter = get_query_filter($query_id);

	$new_filter["cl"] =  $filter["cl"]; //client
	$new_filter["fr"] =  $filter["fr"]; //owner
	$new_filter["supp"] =  $filter["supp"]; //supplier
	$new_filter["forw"] =  $filter["forw"]; //forwarder
	$new_filter["hs"] =  $filter["hs"]; //handling state
	$new_filter["fos"] =  $filter["fos"]; //from global project status
	$new_filter["tos"] =  $filter["tos"]; //to global project status
	if(array_key_exists("fosd", $filter))
	{
		$new_filter["fosd"] =  $filter["fosd"]; //from project development status
		$new_filter["tosd"] =  $filter["tosd"]; //to project development status
		$new_filter["fosl"] =  $filter["fosl"]; //from project logistic status
		$new_filter["tosl"] =  $filter["tosl"]; //to project logistic status
	}
	else
	{
		$new_filter["fosd"] =  ""; //from project development status
		$new_filter["tosd"] =  ""; //to project development status
		$new_filter["fosl"] =  ""; //from project logistic status
		$new_filter["tosl"] =  ""; //to project logistic status
	}
	$new_filter["sufrom"] =  $filter["sufrom"]; // submitted from
	$new_filter["suto"] =  $filter["suto"]; // subnmitted to
	$new_filter["opfrom"] =  $filter["opfrom"]; // opened from
	$new_filter["opto"] =  $filter["opto"]; // opened to
	$new_filter["agrfrom"] =  $filter["agrfrom"]; // agreed opening from
	$new_filter["agrto"] =  $filter["agrto"]; // agreed opening to
	$new_filter["clfrom"] =  $filter["clfrom"]; // closed from
	$new_filter["clto"] =  $filter["clto"]; // closed to
	$new_filter["cafrom"] =  $filter["cafrom"]; // cancelled from
	$new_filter["cato"] =  $filter["cato"]; // cancelled to
	$new_filter["latp"] =  $filter["latp"]; //only the latest project per pos
	$new_filter["pshe"] =  $filter["pshe"]; //only project on project sheet list
	$new_filter["lopr"] =  $filter["lopr"]; //locally produced
	$new_filter["typb1"] =  $filter["typb1"]; //POS with Store Furniture
	$new_filter["typb2"] =  $filter["typb2"]; //POS with SIS Furniture
	$new_filter["idvs"] =  $filter["idvs"]; //Visuals
	$new_filter["noln"] =  $filter["noln"]; //no ln needed
	$new_filter["lnre"] =  $filter["lnre"]; //LN rejected
	$new_filter["nocer"] =  $filter["nocer"]; //no cer/af needed
	$new_filter["cerre"] =  $filter["cerre"]; //CER/AF rejected
	$new_filter["ct"] =  $filter["ct"]; //client type
	$new_filter["re"] =  $filter["re"]; //geografical region
	$new_filter["gr"] =  $filter["gr"]; // Supplied region
	$new_filter["co"] =  $filter["co"]; // country
	$new_filter["ci"] =  $filter["ci"]; // city
	$new_filter["pct"] =  $filter["pct"]; // Legal Type
	$new_filter["pk"] =  $filter["pk"]; // project Type
	$new_filter["ptsc"] =  $filter["ptsc"]; // project Type subclasses
	$new_filter["pt"] =  $filter["pt"]; // POS Type
	$new_filter["sc"] =  $filter["sc"]; // POS Subclass
	$new_filter["pl"] =  $filter["pl"]; // Product line
	$new_filter["fscs"] =  $filter["fscs"]; // Product line subcalss
	$new_filter["ts"] =  $filter["ts"]; //treatment state
	$new_filter["rtc"] =  $filter["rtc"]; // Project Leader
	$new_filter["lrtc"] =  $filter["lrtc"]; // Local project Leader
	$new_filter["dsup"] =  $filter["dsup"]; // Design supervisor
	$new_filter["rto"] =  $filter["rto"]; // Logistics Coordinator
	$new_filter["dcontr"] =  $filter["dcontr"]; // Design Contractor
	$new_filter["dcs"] =  $filter["dcs"]; // Distribution Channel
	$new_filter["dos"] =  $filter["dos"]; // Design Objectives
	$new_filter["cmgr"] =  $filter["cmgr"]; // Cost Monitoring Groups
	$new_filter["icat"] =  $filter["icat"]; // Item Categories
	$new_filter["item"] =  $filter["item"]; // Items
	$new_filter["ar"] =  $filter["ar"]; // Neighbourhood Areas
	$new_filter["cmsst"] =  $filter["cmsst"]; // CMS State
	$new_filter["flagship"] =  $filter["flagship"]; // Flagship Option

	
	$sql = "update posqueries " . 
			   "set posquery_filter = " . dbquote(serialize($new_filter)) . 
			   " where posquery_id = " . param("query_id");

	$result = mysql_query($sql) or dberror($sql);
}

//prepare data
$access_roles = array();
$sql_access_roles = "select role_id from roles where role_include_in_queryselection_posindex = 1";
$res = mysql_query($sql_access_roles) or dberror($sql_access_roles);
while ($row = mysql_fetch_assoc($res))
{
	$access_roles[] = $row["role_id"];
}



if(count($access_roles) > 0)
{
	$sql_persons_hq = "select distinct user_id, concat(user_name, ' ' , user_firstname, '(', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " . 
		   "where user_id = 2 " . 
		   "or (address_id = 13 and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and (user_role_role IN (" . implode(",", $access_roles). ") or user_address = " . $user["address"] . "))" .
		   " or (address_type = 7 and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and (user_role_role IN (" . implode(",", $access_roles). ") or user_address = " . $user["address"] . "))" . 
		   "order by username";

	$sql_persons_sub = "select distinct user_id, " . 
		   "concat(country_name, ' - ', user_name, ' ' , user_firstname, '(', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " .
		   " left join countries on country_id = address_country " . 
		   "where address_id <> 13 and address_client_type in (2, 3) and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and (user_role_role IN (" . implode(",", $access_roles). ") or user_address = " . $user["address"] . ") " . 
		   "order by country_name, username";

	$sql_persons_ag = "select distinct user_id, " . 
		   "concat(country_name, ' - ', user_name, ' ' , user_firstname, '(', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " .
		    " left join countries on country_id = address_country " .  
		   "where address_id <> 13 and address_client_type = 1 and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and (user_role_role IN (" . implode(",", $access_roles). ") or user_address = " . $user["address"] . ") " . 
		   "order by country_name, username";
}
else
{
	$sql_persons_hq = "select distinct user_id, concat(user_name, ' ' , user_firstname, ' (', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " .
		   "where user_id = 2 or (address_id = 13  and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and user_address = " . $user["address"] . ") " . 
		   "order by username";

	$sql_persons_sub = "select distinct user_id, " . 
		   "concat(country_name, ' - ', user_name, ' ' , user_firstname, ' (', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " .
		   " left join countries on country_id = address_country " . 
		   "where address_id <> 13 and address_client_type in (2, 3) and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and user_address = " . $user["address"] . " " . 
		   "order by country_name, username";

	$sql_persons_ag = "select distinct user_id, " . 
		   "concat(country_name, ' - ', user_name, ' ' , user_firstname, ' (', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " .
		   " left join countries on country_id = address_country " . 
		   "where address_id <> 13 and address_client_type = 1 and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and user_address = " . $user["address"] . " " . 
		   "order by country_name, username";
}


$qgroups = array();
$query_groups = "select DISTINCT projectquery_id, projectquery_group " . 
       "from projectqueries " . 
	   " where projectquery_group <> '' and projectquery_owner = " . user_id() . 
	   " order by projectquery_group";
$res = mysql_query($query_groups) or dberror($query_groups);
while ($row = mysql_fetch_assoc($res))
{
	if(!in_array($row["projectquery_group"], $qgroups))
	{
		$qgroups[$row["projectquery_id"]] = $row["projectquery_group"];
	}
}



$form = new Form("projectqueries", "Query");

$form->add_hidden("query_id", param("query_id"));

$form->add_edit("projectquery_name", "Name*", NOTNULL);

$form->add_checkbox("projectquery_print_filter", "Print Query Filter Information in Excel Sheet", "", "", "Filter");


$form->add_section("Group of Queries");
$form->add_comment("Please select to which group the query belongs or enter a new group.");
$form->add_list("existing_groups", "Existing Groups", $qgroups, SUBMIT, "");
$form->add_edit("projectquery_group", "Group");


$form->add_section("Access Rights");
$form->add_comment("The following persons can have access to my queries");

$form->add_checklist("Persons0", "Persons HQ", "projectquery_permissions",
    $sql_persons_hq, 0, "", true);

$form->add_checklist("Persons1", "Subs and Affiliates", "projectquery_permissions",
    $sql_persons_sub, 0, "", true);

$form->add_checklist("Persons2", "Agents", "projectquery_permissions",
    $sql_persons_ag, 0, "", true);


$form->add_hidden("projectquery_owner", user_id());



$form->add_button("save", "Save Query");
if(param("query_id") > 0)
{
	$form->add_button("copy_query", "Copy Query");
	$form->add_button("delete", "Delete Query");
}
$form->add_button(FORM_BUTTON_BACK, "Back to the List of Queries");

$form->populate();
$form->process();


if($form->button("existing_groups"))
{
	$form->value("projectquery_group", $qgroups[$form->value("existing_groups")]);
	$form->value("existing_groups", "");
}
elseif($form->button("save"))
{
	if($form->validate())
	{
		$form->save();

		

		if(!param("query_id")) // new record
		{
			//create fields
			$fields = array();

			$fields["cl"] = array(); // client
			$fields["fr"] = array(); //franchisee owner
			$fields["pl"] = array(); // POS locations
			$fields["dcs"] = array(); // Distribution Channel
			$fields["pro"] = array(); // Project
			$fields["cer"] = array(); // Project
			$fields["log"] = array(); // Logitic details
			$fields["ren"] = array(); // Rental Details
			$fields["pros"] = array(); // Project Sheet
			$fields["cms"] = array(); // Project Sheet
			$fields["sta"] = array(); // Staff
			$fields["bud"] = array(); // Budget
			$fields["inv"] = array(); // Investments
			$fields["ainv"] = array(); // Approved Investments
			$fields["cos"] = array(); // Real Cost
			$fields["comp"] = array(); // Cost Comparison
			$fields["mst"] = array(); // Milestones
			$fields["ost"] = array(); // Order states
			$fields["item"] = array(); // Items
			
			$sql_u = "update projectqueries " . 
					 "set projectquery_fields = " . dbquote(serialize($fields)) . 
					 " where projectquery_id = " . id();

			$result = mysql_query($sql_u) or dberror($sql_u);

			//create filter
			$filter = array();
			
			$filter["cl"] =  ""; //client address
			$filter["fr"] =  ""; // franchisee address
			$filter["hs"] =  ""; // handling state
			$filter["fos"] =  ""; // from global project status
			$filter["tos"] =  ""; // to global project status
			$filter["fosd"] =  ""; // from project development status
			$filter["tosd"] =  ""; // to project development status
			$filter["fosl"] =  ""; // from project logistic status
			$filter["tosl"] =  ""; // to project logistic status
			$filter["sufrom"] =  ""; // submitted from
			$filter["suto"] =  ""; // submitted to
			$filter["opfrom"] =  ""; // opened from
			$filter["opto"] =  ""; // opened to
			$filter["agrfrom"] =  ""; // agreed opening from
			$filter["agrto"] =  ""; // agreed opening to
			$filter["clfrom"] =  ""; // closed from
			$filter["clto"] =  ""; // closed to
			$filter["cafrom"] =  ""; // cancelled from
			$filter["cato"] =  ""; // cancelled to
			$filter["pshe"] =  ""; // only projects on project sheet list
			$filter["latp"] =  ""; // Only latest projects of POS Locations
			$filter["lopr"] =  ""; // POS with locally produced projects
			$filter["typb1"] =  ""; // POS with Store Furniture
			$filter["typb2"] =  ""; // POS with SIS Furniture
			$filter["idvs"] =  ""; // Visuals
			$filter["noln"] =  ""; //no ln needed
			$filter["lnre"] =  ""; //LN rejected
			$filter["nocer"] =  ""; //no cer/af needed
			$filter["cerre"] =  ""; //CER/AF rejected
			$filter["ct"] =  ""; //Client type
			$filter["re"] =  ""; //Geographical region
			$filter["gr"] =  ""; //Supplied region
			$filter["co"] =  ""; // country
			$filter["ci"] =  ""; // city
			$filter["pct"] =  ""; //cost type, legal type
			$filter["pk"] =  ""; //project Type
			$filter["ptsc"] =  ""; //project type subclasses
			$filter["pt"] =  ""; // pos type
			$filter["sc"] =  ""; // POS Type Subclass
			$filter["pl"] =  ""; // product line, furniture type
			$filter["fscs"] =  ""; // furniture subclasses
			$filter["ts"] =  ""; // treatment state
			$filter["rtc"]=  ""; // Project Leader
			$filter["lrtc"]=  ""; // Local project Leader
			$filter["dsup"]=  ""; // Design supervisor
			$filter["rto"]=  ""; // Logistics Coordinator
			$filter["dcontr"]=  ""; // Design Contractor
			$filter["dcs"] =  ""; //Distribution Channel
			$filter["dos"] =  ""; //design objectives
			$filter["cmgr"] =  ""; //cost monitoring groups
			$filter["icat"] =  ""; //item categories
			$filter["item"] =  ""; //items
			$filter["ar"] =  ""; //Neighbourhood Area
			$filter["cmsst"] =  ""; //CMS State
			$filter["flagship"] =  ""; //Flagship option

			//check if user can select Geographical region and country
			// and set filter
			if(!has_access("has_access_to_all_projects"))
			{
				$predefined_filter = user_predefined_filter(user_id());
				$filter["re"] =  $predefined_filter["re"];
				$filter["gr"] =  $predefined_filter["gr"];
				$filter["co"] =  $predefined_filter["co"];
			}

			$sql_u = "update projectqueries " . 
				     "set projectquery_filter = " . dbquote(serialize($filter)) . 
				     " where projectquery_id = " . id();

			$result = mysql_query($sql_u) or dberror($sql_u);

			$sum_fields = array();
			$sql_u = "update projectqueries " . 
				     "set projectquery_sum_fields = " . dbquote(serialize($sum_fields)) . 
				     " where projectquery_id = " . id();


			$avg_fields = array();
			$sql_u = "update projectqueries " . 
				     "set projectquery_avg_fields = " . dbquote(serialize($avg_fields)) . 
				     " where projectquery_id = " . id();

			$result = mysql_query($sql_u) or dberror($sql_u);
		}


		redirect("project_queries_query.php?query_id=" .  id());
	}
}
elseif($form->button("delete"))
{
	$sql = "delete from projectquery_permissions " . 
		   " where projectquery_permission_query = " . id();
	$result = mysql_query($sql) or dberror($sql);

	$sql = "delete from projectqueries " . 
		   " where projectquery_id = " . id();
	$result = mysql_query($sql) or dberror($sql);

	redirect("project_queries.php");


}

$page = new Page("queries");

require_once("include/mis_page_actions.php");

$page->header();
$page->title(id() ? "Edit Project Query - Name" : "Add Project Query");

if(id() > 0 or param("query_id") > 0)
{
	require_once("include/projects_query_tabs.php");
}

$form->render();
$page->footer();

?>