<?php
/********************************************************************

    project_queries_query_filter.php

    Filter Selection for the Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2014-09-14
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2014-09-14
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";


if(!has_access("can_perform_all_queries") 
  and !has_access("can_perform_projects_query"))
{
	redirect("noaccess.php");
}
require_once "include/projects_query_check_access.php";

require_once "include/projects_query_get_functions.php";

/********************************************************************
    prepare all data needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("project_queries.php");
}

$query_id = param("query_id");

$projectquery = get_query_name($query_id);


$icon = "../pictures/add_item.gif";
$link = "javascript:open_selector('')";


$filter = get_query_filter($query_id);



$franchisee_filter = "";
if(param("client_id") > 0)
{
	$franchisee_filter = " and address_parent = " . param("client_id") . " ";
}

if(has_access("has_access_to_all_projects"))
{

	$sql_franchisees = "select DISTINCT order_franchisee_address_id, " . 
		               "concat(country_name, ', ', address_company) as company  " .
					   "from orders " . 
					   "left join addresses on address_id = order_franchisee_address_id ".
					   "left join countries on country_id = address_country " .
					   "where address_id > 0 ".
		               $franchisee_filter . 
					   "order by country_name, address_company";


}
else
{
	$predefined_filter = user_predefined_filter(user_id());

	$sql_franchisees = "select DISTINCT order_franchisee_address_id, " . 
		               "concat(country_name, ', ', address_company) as company  " .
					   "from orders " . 
					   "left join addresses on address_id = order_franchisee_address_id ".
					   "left join countries on country_id = address_country " .
					   "where address_id > 0 " .
		               $franchisee_filter . 
		               " and address_parent IN " . $predefined_filter["user_address_id_filter"] . 
					   " order by country_name, address_company";
}

if(has_access("has_access_to_all_projects"))
{
	$sql_clients = "select DISTINCT order_client_address, concat(country_name, ', ', address_company) as company  " .
				   "from orders " . 
				   "left join addresses on address_id = order_client_address ".
				   "left join countries on country_id = address_country " .
				   "where address_id > 0 ".
				   "order by country_name, address_company";
}
else
{
	$sql_clients = "select DISTINCT order_client_address, concat(country_name, ', ', address_company) as company  " .
				   "from orders " . 
				   "left join addresses on address_id = order_client_address ".
				   "left join countries on country_id = address_country " .
				   "left join users on user_address = address_id " . 
				   "where address_id > 0 " .
		           " and user_id = " . user_id() . 
				   " order by country_name, address_company";
}


$sql_suppliers = "select DISTINCT address_id, " . 
				   "concat(country_name, ', ', address_company) as company  " .
				   "from addresses " .
				   "left join countries on country_id = address_country " .
				   "where address_id > 0 ".
				   " and address_type = 2 " . 
				   "order by country_name, address_company";

$sql_forwarders = "select DISTINCT address_id, " . 
				   "concat(country_name, ', ', address_company) as company  " .
				   "from addresses " .
				   "left join countries on country_id = address_country " .
				   "where address_id > 0 ".
				   " and address_type = 3 " . 
				   "order by country_name, address_company";

$sql_items = "select item_id, concat(item_code, ' ', item_name) as item_name " . 
             " from items " . 
			 " where item_type = 1 " . 
			 " order by item_code";


//get benchmark parameters
$clienttypes = array();
$salesregions = array();
$regions = array();
$countries = array();
$cities = array();
$areas = array();
$product_lines = array();
$furniture_subclasses = array();
$pos_types = array();
$subclasses = array();
$project_kinds = array();
$project_type_subclasses = array();
$project_cost_types = array();
$project_states = array();
$rtcs = array();
$lrtcs = array();
$dsups = array();
$rtos = array();
$dcontrs = array();
$distribution_channels = array();
$design_objectives = array();
$costmonitoring_groups = array();
$item_categories = array();
$items = array();

$client = "";
$franchisee = "";
$supplier = "";
$forwarder = "";

$handling_state = "";
$fos = "";
$tos = "";
$fosd = "";
$tosd = "";
$fosl = "";
$tosl = "";
$sufrom = "";
$suto = "";
$opfrom = "";
$opto = "";
$clfrom = "";
$clto = "";
$cafrom = "";
$cato = "";
$pshe = "";
$latp = "";
$lopr = "";
$sppr = "";
$typb1 = "";
$typb2 = "";
$idvs = "";
$cms_state = "";
$noln = "";
$lnre = "";
$nocer = "";
$cerre = "";
$flagship = "";


//check if filter is present
$sql = "select projectquery_fields, projectquery_filter, projectquery_area_perception from projectqueries " .
	   "where projectquery_id = " . $query_id;



$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$fields_projectaddresses = unserialize($row["projectquery_fields"]);

	$filters = array();
	$filters = unserialize($row["projectquery_filter"]);



	//foreach($filters as $key=>$value)
	//{
		//client
		if(array_key_exists("cl", $filters))
		{
			$client = $filters["cl"];

		}
		//franchisee, owner
		if(array_key_exists("fr", $filters))
		{
			$franchisee = $filters["fr"];

		}
		//supplier
		if(array_key_exists("supp", $filters))
		{
			$supplier = $filters["supp"];

		}
		//forwarder
		if(array_key_exists("forw", $filters))
		{
			$forwarder = $filters["forw"];

		}
		//handling state
		if(array_key_exists("hs", $filters))
		{
			$handling_state = $filters["hs"];

		}
		//from global project status
		if(array_key_exists("fos", $filters))
		{
			$fos = $filters["fos"];

		}
		//to global project status
		if(array_key_exists("tos", $filters))
		{
			$tos = $filters["tos"];

		}

		//from project development status
		if(array_key_exists("fosd", $filters))
		{
			$fosd = $filters["fosd"];

		}
		//to project development status
		if(array_key_exists("tos", $filters))
		{
			$tosd = $filters["tosd"];

		}

		//from project logistic status
		if(array_key_exists("fosl", $filters))
		{
			$fosl = $filters["fosl"];

		}
		//to project logistic status
		if(array_key_exists("tosl", $filters))
		{
			$tosl = $filters["tosl"];

		}

		//submitted from
		if(array_key_exists("sufrom", $filters))
		{
			$sufrom = $filters["sufrom"];

		}
		//submitted to
		if(array_key_exists("suto", $filters))
		{
			$suto = $filters["suto"];

		}

		//opened from
		if(array_key_exists("opfrom", $filters))
		{
			$opfrom = $filters["opfrom"];

		}
		//opened to
		if(array_key_exists("opto", $filters))
		{
			$opto = $filters["opto"];

		}

		//agreed opening from
		if(array_key_exists("agrfrom", $filters))
		{
			$agrfrom = $filters["agrfrom"];

		}
		//agreed opening to
		if(array_key_exists("agrto", $filters))
		{
			$agrto = $filters["agrto"];

		}

		//closed from
		if(array_key_exists("clfrom", $filters))
		{
			$clfrom = $filters["clfrom"];

		}
		//closed to
		if(array_key_exists("clto", $filters))
		{
			$clto = $filters["clto"];

		}

		//cancelled from
		if(array_key_exists("cafrom", $filters))
		{
			$cafrom = $filters["cafrom"];

		}
		//cancelled to
		if(array_key_exists("cato", $filters))
		{
			$cato = $filters["cato"];

		}

		//only latest project of POS Locations
		if(array_key_exists("latp", $filters))
		{
			$latp = $filters["latp"];

		}

		//only project on project sheet list
		if(array_key_exists("pshe", $filters))
		{
			$pshe = $filters["pshe"];

		}

		//locally produced
		if(array_key_exists("lopr", $filters))
		{
			$lopr = $filters["lopr"];

		}
		
				
		//IPOS with Store Furniture
		if(array_key_exists("typb1", $filters))
		{
			$typb1 = $filters["typb1"];

		}
		
		//POS with SIS Furniture
		if(array_key_exists("typb2", $filters))
		{
			$typb2 = $filters["typb2"];

		}
		
		//Visuals
		if(array_key_exists("idvs", $filters))
		{
			$idvs = $filters["idvs"];

		}

		//needs no ln
		if(array_key_exists("noln", $filters))
		{
			$noln = $filters["noln"];

		}
		//ln rejected
		if(array_key_exists("lnre", $filters))
		{
			$lnre = $filters["lnre"];

		}

		//needs no cer/af
		if(array_key_exists("nocer", $filters))
		{
			$nocer = $filters["nocer"];

		}
		//cer rejected
		if(array_key_exists("cerre", $filters))
		{
			$cerre = $filters["cerre"];

		}
		
		//client type
		if(array_key_exists("ct", $filters))
		{
			$clienttypes = explode("-", $filters["ct"]);
		}
		//geografical region
		if(array_key_exists("re", $filters))
		{
			$salesregions = explode("-", $filters["re"]);
		}
		// sales region
		if(array_key_exists("gr", $filters))
		{
			$regions = explode("-", $filters["gr"]);
		}
		//country
		if(array_key_exists("co", $filters))
		{
			$countries = explode("-", $filters["co"]);
		}
		// city
		if(array_key_exists("ci", $filters))
		{
			$cities = explode("-", $filters["ci"]);
		}

		//legal type
		if(array_key_exists("pct", $filters))
		{
			$project_cost_types = explode("-", $filters["pct"]);
		}

		//project Type
		if(array_key_exists("pk", $filters))
		{
			$project_kinds = explode("-", $filters["pk"]);
		}

		//project Type subclasses
		if(array_key_exists("ptsc", $filters))
		{
			$project_type_subclasses = explode("-", $filters["ptsc"]);
		}
		//POS Type
		if(array_key_exists("pt", $filters))
		{
			$pos_types = explode("-", $filters["pt"]);
		}
		// POS Type subclass
		if(array_key_exists("sc", $filters))
		{
			$subclasses = explode("-", $filters["sc"]);
		}
		//product line
		if(array_key_exists("pl", $filters))
		{
			$product_lines = explode("-", $filters["pl"]);
		}
		//product line subclass
		if(array_key_exists("fscs", $filters))
		{
			$furniture_subclasses = explode("-", $filters["fscs"]);
		}

		//treatment states
		if(array_key_exists("ts", $filters))
		{
			$project_states = explode("-", $filters["ts"]);

		}

		//Project Leaders
		if(array_key_exists("rtc", $filters))
		{
			$rtcs =  explode("-", $filters["rtc"]);

		}

		//Local project Leaders
		if(array_key_exists("lrtc", $filters))
		{
			$lrtcs =  explode("-", $filters["lrtc"]);
		}

		//Design supervisors
		if(array_key_exists("dsup", $filters))
		{
			$dsups =  explode("-", $filters["dsup"]);
		}

		//Logistics coordinatros
		if(array_key_exists("rto", $filters))
		{
			$rtos =  explode("-", $filters["rto"]);
		}


		//Design Contractors
		if(array_key_exists("dcontr", $filters))
		{
			$dcontrs =  explode("-", $filters["dcontr"]);
		}


		//Distribution Channel
		if(array_key_exists("dcs", $filters))
		{
			$distribution_channels =  explode("-", $filters["dcs"]);
		}

		//Design Objectives
		if(array_key_exists("dos", $filters))
		{
			$design_objectives = explode("-", $filters["dos"]);
		}

		//Cost Monitoring Groups
		if(array_key_exists("cmgr", $filters))
		{
			$costmonitoring_groups = explode("-", $filters["cmgr"]);
		}

		//item categories
		if(array_key_exists("icat", $filters))
		{
			$item_categories = explode("-", $filters["icat"]);
		}

		//items
		if(array_key_exists("item", $filters))
		{
			$items = explode("-", $filters["item"]);
		}
		
		
		//Neigbourhood areas
		if(array_key_exists("ar", $filters))
		{
			$areas = explode("-", $filters["ar"]);
		}

		//CMS State
		if(array_key_exists("cmsst", $filters))
		{
			$cms_state = $filters["cmsst"];
		}

		//flagship option
		if(array_key_exists("flagship", $filters))
		{
			$flagship = $filters["flagship"];
		}
		
	//}
}




//SQL for Filters
// create sql for filter criteria

$sql_salesregion = "select salesregion_id, salesregion_name ".
               "from salesregions order by salesregion_name";

$sql_region = "select region_id, region_name ".
			   "from regions order by region_name";


$sql_country = "select DISTINCT country_id, country_name " .
			   "from orders " .
			   "left join countries on country_id = order_shop_address_country " . 
			   " where country_id > 0 " . 
			   "order by country_name";



$tmp_filter = "";
if(count($cities) > 0)
{
	
	$cities = explode("-", $filter["ci"]);
	
	foreach($cities as $key=>$city)
	{
		$tmp_filter = $tmp_filter . $city . ",";
	}

	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);
	
	if($tmp_filter)
	{
		$tmp_filter = " where place_id IN ( " . $tmp_filter . ") ";
	}

	$sql_cities = "select DISTINCT place_id, place_name ".
		          "from places " . 
		          $tmp_filter . 
				  "order by place_name";
}
elseif(count($countries) > 0)
{
	
	$countries = explode("-", $filter["co"]);
	
	foreach($countries as $key=>$country)
	{
		$tmp_filter = $tmp_filter . $country . ",";
	}

	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);
	
	if($tmp_filter)
	{
		$tmp_filter = " where place_country IN ( " . $tmp_filter . ") ";
	}

	$sql_cities = "select DISTINCT place_id, place_name ".
		          " from places " .
				  $tmp_filter . 
				  "order by place_name";
}
else
{
	$sql_cities = "select DISTINCT place_id, place_name ".
				  "from places order by place_name";
}

$sql_areas = "select posareatype_id, posareatype_name ".
              "from posareatypes order by posareatype_name";


$sql_product_lines = "select product_line_id, product_line_name ".
                     "from product_lines " . 
                     "   order by product_line_name";

$sql_furniture_subclasses = "select productline_subclass_id, productline_subclass_name ".
                     "from productline_subclasses " . 
                     "   order by productline_subclass_name";

$sql_pos_types = "select DISTINCT postype_id, postype_name ".
                     "from postypes " .
                     "order by postype_name";

$sql_possubclasses = "select possubclass_id, possubclass_name ".
                     "from possubclasses " .
                     "order by possubclass_name";

$sql_project_cost_types = "select project_costtype_id, project_costtype_text ".
						  "from project_costtypes " .
                          "order by project_costtype_text";


$sql_project_kinds = "select projectkind_id, projectkind_name ".
						 "from projectkinds " .
						 "where projectkind_id > 0 " .
						 "order by projectkind_name";

$sql_project_type_subclasses = "select project_type_subclass_id, project_type_subclass_name ".
						 "from project_type_subclasses " .
						 "order by project_type_subclass_name";

$sql_years = "select DISTINCT YEAR(order_date) as year1, YEAR(order_date) as year2 " .
             "from orders " . 
			 "where order_type = 1 " . 
			 "order by year1";


$sql_distribution_channels = "select mps_distchannel_id, " . 
                             "concat(mps_distchannel_group_name, ' - ', mps_distchannel_name, ' - ', mps_distchannel_code) as distributionchannel ".
							"from mps_distchannels " .
							"left join mps_distchannel_groups on mps_distchannel_groups.mps_distchannel_group_id = mps_distchannels.mps_distchannel_group_id " .
							"order by mps_distchannel_group_name, mps_distchannel_name, mps_distchannel_code";


$sql_design_objectives = "select design_objective_item_id, design_objective_item_name, " .
						 "    design_objective_group_name, postype_name " .
						 "from design_objective_items left join design_objective_groups on design_objective_item_group = design_objective_group_id " .
						 "    left join postypes on design_objective_group_postype = postype_id " . 
						 " where postype_name <> '' " . 
						 " order by postype_name, design_objective_group_name, design_objective_item_name ";

$sql_client_types = "select client_type_id, client_type_code ".
					"from client_types";


$sql_costmonitoring_groups = "select costmonitoringgroup_id, costmonitoringgroup_text " . 
                             "from costmonitoringgroups " . 
							 " order by costmonitoringgroup_text";

$sql_item_categories = "select item_category_id, item_category_name " . 
                             "from item_categories " . 
							 " order by item_category_name";


$handling_states = array();
$handling_states[1] = "ongoing projects without an actual opening date";
$handling_states[2] = "ongoing projects with an actual opening date";
$handling_states[3] = "projects with a closing date";
$handling_states[4] = "ongoing projects with a project sheet";


$cms_states = array();
$cms_states[1] = "Completion overdue";
$cms_states[2] = "Approval overdue";
$cms_states[3] = "Completion or Approval overdue";
$cms_states[4] = "Completion and Approval overdue";
$cms_states[5] = "Logistics CMS completed";
$cms_states[6] = "Development CMS approved";

$sql_order_states = "select order_state_code, concat(order_state_code, ' ', order_state_name) as name " .
                    "from order_states ". 
					"left join order_state_groups on order_state_group_id = order_state_group " . 
					"where order_state_not_used_anymore <> 1 " . 
					" and order_state_group_order_type = 1 and order_state_code < '910' " . 
					"order by order_state_code";

$sql_development_states = "select order_state_code, concat(order_state_code, ' ', order_state_name) as name " .
                    "from order_states ". 
					"left join order_state_groups on order_state_group_id = order_state_group " . 
					"where order_state_not_used_anymore <> 1 " . 
					" and order_state_group_order_type = 1 and order_state_code < '910' " . 
					" and order_state_used_for_logistics <> 1 " . 
					"order by order_state_code";


$sql_logistic_states = "select order_state_code, concat(order_state_code, ' ', order_state_name) as name " .
                    "from order_states ". 
					"left join order_state_groups on order_state_group_id = order_state_group " . 
					"where order_state_not_used_anymore <> 1 " . 
					" and order_state_group_order_type = 1 and order_state_code < '910' " . 
					" and order_state_used_for_logistics = 1 " . 
					"order by order_state_code";

$sql_rtcs = "select distinct user_id, concat(user_name, ' ', user_firstname) as username " . 
                          "from projects " .
						  " left join users on user_id = project_retail_coordinator " . 
						  " where user_id > 0 " . 
						  " order by username";

$sql_lrtcs = "select distinct user_id, concat(user_name, ' ', user_firstname) as username " . 
                          "from projects " .
						  " left join users on user_id = project_local_retail_coordinator " .
						  " where user_id > 0 " .
						  " order by username";

$sql_dsups = "select distinct user_id, concat(user_name, ' ', user_firstname) as username " . 
                          "from projects " .
						  " left join users on user_id = project_design_supervisor " . 
						  " where user_id > 0 " .
						  " order by username";

$sql_rtos = "select distinct user_id, concat(user_name, ' ', user_firstname) as username " . 
                          "from orders " .
						  " left join users on user_id = order_retail_operator " . 
						  " where order_type = 1 " .
						  " and user_id > 0 " .
						  " order by username";

$sql_dcontrs = "select distinct user_id, concat(user_name, ' ', user_firstname) as username " . 
                          "from projects " .
						  " left join users on user_id = project_design_contractor " . 
						  " where user_id > 0 " .
						  " order by username";


$sql_project_states = "select project_state_id, project_state_text " . 
                        " from project_states " . 
						" order by project_state_text";


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projectqueries", "query_generator");

$form->add_hidden("query_id", param("query_id"));
$form->add_section($projectquery["name"]);


$form->add_comment("Please select the the filter criteria of your query.");



$form->add_section("Addresses");
$link = "javascript:open_selector('')";



$form->add_list("client_id", "Client",	$sql_clients, 0, $client);
$form->add_list("franchisee_id", "Owner Company", $sql_franchisees, 0, $franchisee);

$form->add_list("supplier_id", "Supplier", $sql_suppliers, SUBMIT, $supplier);
$form->add_list("forwarder_id", "Forwarder", $sql_forwarders, 0, $forwarder);


$form->add_Section("Selected Filter Criteria");


$selected_clienttypes = "";
if(count($clienttypes) > 0)
{
	$res = mysql_query($sql_client_types) or dberror($sql_client_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["client_type_id"], $clienttypes))
		{
			$selected_clienttypes .= $row["client_type_code"] . ", ";
		}
	}
	$selected_clienttypes = substr($selected_clienttypes, 0, strlen($selected_clienttypes) - 2);
}

$form->add_label_selector("clienttypes", "Client Types", 0, $selected_clienttypes, $icon, $link);

$selected_salesregions = "";
if(count($salesregions) > 0)
{
	$res = mysql_query($sql_salesregion) or dberror($sql_salesregion);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["salesregion_id"], $salesregions))
		{
			$selected_salesregions .= $row["salesregion_name"] . ", ";
		}
	}
	$selected_salesregions = substr($selected_salesregions, 0, strlen($selected_salesregions) - 2);
}

$form->add_label_selector("salesregions", "Geographical Regions", 0, $selected_salesregions, $icon, $link);

$selected_regions = "";
if(count($regions) > 0)
{
	$res = mysql_query($sql_region) or dberror($sql_region);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["region_id"], $regions))
		{
			$selected_regions .= $row["region_name"] . ", ";
		}
	}
	$selected_regions = substr($selected_regions, 0, strlen($selected_regions) - 2);
}

$form->add_label_selector("regions", "Supplied Regions", 0, $selected_regions, $icon, $link);


$selected_countries = "";
if(count($countries) > 0)
{
	$res = mysql_query($sql_country) or dberror($sql_country);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["country_id"], $countries))
		{
			$selected_countries .= $row["country_name"] . ", ";
		}
	}
	$selected_countries = substr($selected_countries, 0, strlen($selected_countries) - 2);
}
$form->add_label_selector("countries", "Countries", 0, $selected_countries, $icon, $link);


$selected_cities = "";
if(count($cities) > 0)
{
	$res = mysql_query($sql_cities) or dberror($sql_cities);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array(trim($row["place_id"]), $cities))
		{
			$selected_cities .= $row["place_name"] . ", ";
		}
	}
	$selected_cities = substr($selected_cities, 0, strlen($selected_cities) - 2);
}
$form->add_label_selector("cities", "Cities", 0, $selected_cities, $icon, $link);





$selected_pcts = "";
if(count($project_cost_types) > 0)
{
	$res = mysql_query($sql_project_cost_types) or dberror($sql_project_cost_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["project_costtype_id"], $project_cost_types))
		{
			$selected_pcts .= $row["project_costtype_text"] . ", ";
		}
	}
	$selected_pcts = substr($selected_pcts, 0, strlen($selected_pcts) - 2);
}
$form->add_label_selector("pcts", "Legal Types", 0, $selected_pcts, $icon, $link);



$selected_pks = "";
if(count($project_kinds) > 0)
{
	$res = mysql_query($sql_project_kinds) or dberror($sql_project_kinds);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["projectkind_id"], $project_kinds))
		{
			$selected_pks .= $row["projectkind_name"] . ", ";
		}
	}
	$selected_pks = substr($selected_pks, 0, strlen($selected_pks) - 2);
}
$form->add_label_selector("projectkinds", "Project Types", 0, $selected_pks, $icon, $link);



$selected_ptscs = "";
if(count($project_type_subclasses) > 0)
{
	$res = mysql_query($sql_project_type_subclasses) or dberror($sql_project_type_subclasses);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["project_type_subclass_id"], $project_type_subclasses))
		{
			$selected_ptscs .= $row["project_type_subclass_name"] . ", ";
		}
	}
	$selected_ptscs = substr($selected_ptscs, 0, strlen($selected_ptscs) - 2);
}
$form->add_label_selector("project_type_subclasses", "Project Type Subclasses", 0, $selected_ptscs, $icon, $link);

$selected_pts = "";
if(count($pos_types) > 0)
{
	$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["postype_id"], $pos_types))
		{
			$selected_pts .= $row["postype_name"] . ", ";
		}
	}
	$selected_pts = substr($selected_pts, 0, strlen($selected_pts) - 2);
}
$form->add_label_selector("pts", "POS Types", 0, $selected_pts, $icon, $link);

$selected_subclasses = "";
if(count($subclasses) > 0)
{
	$res = mysql_query($sql_possubclasses) or dberror($sql_possubclasses);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["possubclass_id"], $subclasses))
		{
			$selected_subclasses .= $row["possubclass_name"] . ", ";
		}
	}
	$selected_subclasses = substr($selected_subclasses, 0, strlen($selected_subclasses) - 2);
}
$form->add_label_selector("subclasses", "POS Type Subclasses", 0, $selected_subclasses, $icon, $link);


$selected_pls = "";
if(count($product_lines) > 0)
{
	$res = mysql_query($sql_product_lines) or dberror($sql_product_lines);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["product_line_id"], $product_lines))
		{
			$selected_pls .= $row["product_line_name"] . ", ";
		}
	}
	$selected_pls = substr($selected_pls, 0, strlen($selected_pls) - 2);
}
$form->add_label_selector("pls", "Furniture Types", 0, $selected_pls, $icon, $link);


$selected_fscs = "";
if(count($furniture_subclasses) > 0)
{
	$res = mysql_query($sql_furniture_subclasses) or dberror($sql_furniture_subclasses);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["productline_subclass_id"], $furniture_subclasses))
		{
			$selected_fscs .= $row["productline_subclass_name"] . ", ";
		}
	}
	$selected_fscs = substr($selected_fscs, 0, strlen($selected_fscs) - 2);
}
$form->add_label_selector("fscs", "Furniture Subclasses", 0, $selected_fscs, $icon, $link);


$selected_project_states = "";
if(count($project_states) > 0)
{
	$res = mysql_query($sql_project_states) or dberror($sql_project_states);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["project_state_id"], $project_states))
		{
			$selected_project_states .= $row["project_state_text"] . ", ";
		}
	}
	$selected_project_states = substr($selected_project_states, 0, strlen($selected_project_states) - 2);
}
$form->add_label_selector("project_states", "Treatment States", 0, $selected_project_states, $icon, $link);


$selected_rtcs = "";
if(count($rtcs) > 0)
{
	$res = mysql_query($sql_rtcs) or dberror($sql_rtcs);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["user_id"], $rtcs))
		{
			$selected_rtcs .= $row["username"] . ", ";

		}
	}
	$selected_rtcs = substr($selected_rtcs, 0, strlen($selected_rtcs) - 2);
}
$form->add_label_selector("rtcs", "Project Leaders", 0, $selected_rtcs, $icon, $link);


$selected_lrtcs = "";
if(count($lrtcs) > 0)
{
	$res = mysql_query($sql_lrtcs) or dberror($sql_lrtcs);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["user_id"], $lrtcs))
		{
			$selected_lrtcs .= $row["username"] . ", ";
		}
	}
	$selected_lrtcs = substr($selected_lrtcs, 0, strlen($selected_lrtcs) - 2);
}
$form->add_label_selector("lrtcs", "Local Project Leaders", 0, $selected_lrtcs, $icon, $link);


$selected_dsups = "";
if(count($dsups) > 0)
{
	$res = mysql_query($sql_dsups) or dberror($sql_dsups);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["user_id"], $dsups))
		{
			$selected_dsups .= $row["username"] . ", ";
		}
	}
	$selected_dsups = substr($selected_dsups, 0, strlen($selected_dsups) - 2);
}
$form->add_label_selector("dsups", "Design Supervisors", 0, $selected_dsups, $icon, $link);

/*
$selected_rtos = "";
if(count($rtos) > 0)
{
	$res = mysql_query($sql_rtos) or dberror($sql_rtos);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["user_id"], $rtos))
		{
			$selected_rtos .= $row["username"] . ", ";
		}
	}
	$selected_rtos = substr($selected_rtos, 0, strlen($selected_rtos) - 2);
}
$form->add_label_selector("rtos", "Logistic Coordinatros", 0, $selected_rtos, $icon, $link);
*/

//design contractors
$selected_dcontrs = "";
if(count($dcontrs) > 0)
{
	$res = mysql_query($sql_dcontrs) or dberror($sql_dcontrs);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["user_id"], $dcontrs))
		{
			$selected_dcontrs .= $row["username"] . ", ";
		}
	}
	$selected_dcontrs = substr($selected_dcontrs, 0, strlen($selected_dcontrs) - 2);
}
$form->add_label_selector("dcontrs", "Design Contractors", 0, $selected_dcontrs, $icon, $link);




$selected_dcs = "";
if(count($distribution_channels) > 0)
{
	$res = mysql_query($sql_distribution_channels) or dberror($sql_distribution_channels);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["mps_distchannel_id"], $distribution_channels))
		{
			$selected_dcs .= $row["distributionchannel"] . ", ";
		}
	}
	$selected_dcs = substr($selected_dcs, 0, strlen($selected_dcs) - 2);
}
$form->add_label_selector("distributionchannel", "Distribution Channels", 0, $selected_dcs, $icon, $link);


/*
$selected_design_objectives = "";
if(count($design_objectives) > 0)
{
	$res = mysql_query($sql_design_objectives) or dberror($sql_design_objectives);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["design_objective_item_id"], $design_objectives))
		{
			$selected_design_objectives .= $row["postype_name"] . " - " . $row["design_objective_group_name"] . " - " . $row["design_objective_item_name"] . ", ";
		}
	}
	$selected_design_objectives = substr($selected_design_objectives, 0, strlen($selected_design_objectives) - 2);
}
$form->add_label_selector("design_objectives", "Design Objectives", 0, $selected_design_objectives, $icon, $link);


$selected_costmonitoring_groups = "";
if(count($costmonitoring_groups) > 0)
{
	$res = mysql_query($sql_costmonitoring_groups) or dberror($sql_costmonitoring_groups );
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["costmonitoringgroup_id"], $costmonitoring_groups))
		{
			$selected_costmonitoring_groups .= $row["costmonitoringgroup_text"] . ", ";
		}
	}
	$selected_costmonitoring_groups = substr($selected_costmonitoring_groups, 0, strlen($selected_costmonitoring_groups) - 2);
}
//$form->add_label_selector("costmonitoring_groups", "Cost Monitoring Groups", 0, $selected_costmonitoring_groups, $icon, $link);


$selected_item_categories = "";
if(count($item_categories) > 0)
{
	$res = mysql_query($sql_item_categories) or dberror($sql_item_categories );
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["item_category_id"], $item_categories))
		{
			$selected_item_categories .= $row["item_category_name"] . ", ";
		}
	}
	$selected_item_categories = substr($selected_item_categories, 0, strlen($selected_item_categories) - 2);
}
$form->add_label_selector("item_categories", "Item Categories", 0, $selected_item_categories, $icon, $link);


$selected_items = "";
if(count($items) > 0)
{
	$res = mysql_query($sql_items) or dberror($sql_items );
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["item_id"], $items))
		{
			$selected_items .= $row["item_name"] . ", ";
		}
	}
	$selected_items = substr($selected_items, 0, strlen($selected_items) - 2);
}
$form->add_label_selector("items", "Items", 0, $selected_items, $icon, $link);

*/


$selected_areas = "";
if(count($areas) > 0)
{
	$res = mysql_query($sql_areas) or dberror($sql_areas);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["posareatype_id"], $areas))
		{
			$selected_areas .= $row["posareatype_name"] . ", ";
		}
	}
	$selected_areas = substr($selected_areas, 0, strlen($selected_areas) - 2);
}
$form->add_label_selector("areas", "POS Environment", 0, $selected_areas, $icon, $link);

$form->add_checkbox("LATP", "Include only the latest operating project for each POS Location", $latp, 0, "Latest Operating Project");

$form->add_checkbox("PSHE", "Include only projects being included in the list of project sheets", $pshe, 0, "In the List of Project Sheets");





$form->add_section("Project");
$form->add_list("hs", "Projects",	$handling_states, 0, $handling_state);

//$form->add_list("fos", "From Global Project Status", $sql_order_states, 0, $fos);
//$form->add_list("tos", "To Global Project Status", $sql_order_states, 0, $tos);

//$form->add_list("fosd", "From Project Development Status", $sql_development_states, 0, $fosd);
//$form->add_list("tosd", "To Project Development Status", $sql_development_states, 0, $tosd);

//$form->add_list("fosl", "From Project Logistic Status", $sql_logistic_states, 0, $fosl);
//$form->add_list("tosl", "To Project Logistic Status", $sql_logistic_states, 0, $tosl);


$form->add_list("fos", "From Status", $sql_order_states, 0, $fos);
$form->add_list("tos", "To Status", $sql_order_states, 0, $tos);

$form->add_hidden("fosd");
$form->add_hidden("tosd");

$form->add_hidden("fosl");
$form->add_hidden("tosl");

$form->add_edit("sufrom", "Submitted from", 0, to_system_date($sufrom), TYPE_DATE, 10);
$form->add_edit("suto", "Submitted to", 0, to_system_date($suto), TYPE_DATE, 10);
$form->add_edit("agrfrom", "Agreed Opening Date from", 0, to_system_date($agrfrom), TYPE_DATE, 10);
$form->add_edit("agrto", "Agreed Opening Date to", 0, to_system_date($agrto), TYPE_DATE, 10);
$form->add_edit("opfrom", "Actual Opening Date from", 0, to_system_date($opfrom), TYPE_DATE, 10);
$form->add_edit("opto", "Actual Opening Date to", 0, to_system_date($opto), TYPE_DATE, 10);
$form->add_edit("clfrom", "Closed from", 0, to_system_date($clfrom), TYPE_DATE, 10);
$form->add_edit("clto", "Closed to", 0, to_system_date($clto), TYPE_DATE, 10);
$form->add_edit("cafrom", "Cancelled from", 0, to_system_date($cafrom), TYPE_DATE, 10);
$form->add_edit("cato", "Cancelled to", 0, to_system_date($cato), TYPE_DATE, 10);

$form->add_checkbox("LOPR", "Show only projects with locally produced projects", $lopr, 0, "Local Production");
$form->add_checkbox("flagship", "Show only flag ship projects", $flagship, 0, "Flag Ship Option");

$form->add_section("Miscellaneous Parameters");
$form->add_checkbox("TYPB1", "Show only projects with Store Furniture", $typb1, 0, "Furniture");
$form->add_checkbox("TYPB2", "Show only projects with SIS Furniture", $typb2, 0, "Furniture");
$form->add_checkbox("IDVS", "Show only projects with Visuals", $idvs, 0, "Visuals");


$form->add_section("CMS Status");
$form->add_list("cmsst", "CMS Status", $cms_states, 0, $cms_state);


$form->add_section("LN/CER/AF");
$form->add_checkbox("noln", "Show only projects where no LN is needed", $noln, 0, "No LN needed");
$form->add_checkbox("lnre", "Show only projects where LN is actually rejected", $lnre, 0, "LN rejected");
$form->add_checkbox("nocer", "Show only projects where no CER/AF is needed", $nocer, 0, "No CRE/AF needed");
$form->add_checkbox("cerre", "Show only projects where CER/AF is actually rejected", $cerre, 0, "CER/AF rejected");

$form->add_button("save", "Save Filter");

if(check_if_query_has_fields($query_id) == true)
{
	$form->add_button("execute", "Execute Query");
}
$form->add_button("back", "Back to the List of Queries");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Process buttons
*********************************************************************/ 
if($form->button("save") or $form->button("supplier_id"))
{
	

	if($form->validate())
	{
		$new_filter = array();
		$new_filter["cl"] =  $form->value("client_id"); //client
		$new_filter["fr"] =  $form->value("franchisee_id"); //owner
		$new_filter["supp"] =  $form->value("supplier_id"); //supplier
		$new_filter["forw"] =  $form->value("forwarder_id"); //forwarder
		$new_filter["hs"] =  $form->value("hs"); //handling state
		$new_filter["fos"] =  $form->value("fos"); //from global project status
		$new_filter["tos"] =  $form->value("tos"); //to global project status
		$new_filter["fosd"] =  $form->value("fosd"); //from project development status
		$new_filter["tosd"] =  $form->value("tosd"); //to project development status
		$new_filter["fosl"] =  $form->value("fosl"); //from project logistic status
		$new_filter["tosl"] =  $form->value("tosl"); //to project logistic status
		$new_filter["sufrom"] =  from_system_date($form->value("sufrom")); //submitted from
		$new_filter["suto"] =  from_system_date($form->value("suto")); //submitted to
		$new_filter["opfrom"] =  from_system_date($form->value("opfrom")); //opened from
		$new_filter["opto"] =  from_system_date($form->value("opto")); //opened to
		$new_filter["agrfrom"] =  from_system_date($form->value("agrfrom")); //agreed opening from
		$new_filter["agrto"] =  from_system_date($form->value("agrto")); //agreed opening to
		$new_filter["clfrom"] =  from_system_date($form->value("clfrom")); //closed from
		$new_filter["clto"] =  from_system_date($form->value("clto")); //closed to
		$new_filter["cafrom"] =  from_system_date($form->value("cafrom")); //cancelled from
		$new_filter["cato"] =  from_system_date($form->value("cato")); //cancelled to
		$new_filter["pshe"] =  $form->value("PSHE"); //only projects included in the list of project sheets
		$new_filter["latp"] =  $form->value("LATP"); //only the latest project of POS locations
		$new_filter["lopr"] =  $form->value("LOPR"); //locally produced
		$new_filter["typb1"] =  $form->value("TYPB1"); //POS with Store Furniture
		$new_filter["typb2"] =  $form->value("TYPB2"); //POS with SIS Furniture
		$new_filter["idvs"] =  $form->value("IDVS"); //Visuals
		$new_filter["noln"] =  $form->value("noln"); //no ln needed
		$new_filter["lnre"] =  $form->value("lnre"); //LN rejected
		$new_filter["nocer"] =  $form->value("nocer"); //no cer/af needed
		$new_filter["cerre"] =  $form->value("cerre"); //CER/AF rejected
		$new_filter["ct"] =  $filter["ct"]; //client types
		$new_filter["re"] =  $filter["re"]; //geografical region
		$new_filter["gr"] =  $filter["gr"]; // Supplied region
		$new_filter["co"] =  $filter["co"]; // country
		$new_filter["ci"] =  $filter["ci"]; // city
		$new_filter["pct"] =  $filter["pct"]; // Legal Type
		$new_filter["pk"] =  $filter["pk"]; // project Type
		$new_filter["ptsc"] =  $filter["ptsc"]; // project Type subclasses
		$new_filter["pt"] =  $filter["pt"]; // POS Type
		$new_filter["sc"] =  $filter["sc"]; // POS Subclass
		$new_filter["pl"] =  $filter["pl"]; // Product line
		$new_filter["fscs"] =  $filter["fscs"]; // Product line subcalss
		$new_filter["ts"] =  $filter["ts"]; //treatment state
		$new_filter["rtc"] =  $filter["rtc"]; // Project Leader
		$new_filter["lrtc"] =  $filter["lrtc"]; // Local project Leader
		$new_filter["dsup"] =  $filter["dsup"]; // Design supervisor
		$new_filter["rto"] =  $filter["rto"]; // Logistics Coordinator
		$new_filter["dcontr"] =  $filter["dcontr"]; // Design Contractor
		$new_filter["dcs"] =  $filter["dcs"]; // Distribution Channel
		$new_filter["dos"] =  $filter["dos"]; // Design Objectives
		$new_filter["cmgr"] =  $filter["cmgr"]; // Cost Monitoring Groups
		$new_filter["icat"] =  $filter["icat"]; // Item Categories
		$new_filter["item"] =  $filter["item"]; // Items
		$new_filter["ar"] =  $filter["ar"]; // Neighbourhood Areas
		$new_filter["cmsst"] =  $form->value("cmsst"); // CMS State
		$new_filter["flagship"] =  $form->value("flagship"); // CMS State


		$sql = "update projectqueries " . 
			   "set projectquery_filter = " . dbquote(serialize($new_filter))  . ", " . 
			   "date_modified = " . dbquote(date("Y-m-d H:s:i")) . ", " . 
				"user_modified = " . dbquote(user_login()) .
			   " where projectquery_id = " . param("query_id");

		$result = mysql_query($sql) or dberror($sql);
	}
}
elseif($form->button("back"))
{
	redirect("project_queries.php");
}
elseif($form->button("execute"))
{
	redirect("project_queries_query_xls.php?query_id=" . param("query_id"));
}

/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page("queries");

require_once("include/mis_page_actions.php");


$page->header();
$page->title("Edit Project Query - Filter");

require_once("include/projects_query_tabs.php");

$form->render();

?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#clienttypes_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=ct'
    });
    return false;
  });
  $('#salesregions_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=re'
    });
    return false;
  });
  $('#regions_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=gr'
    });
    return false;
  });
  $('#countries_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=co'
    });
    return false;
  });
  $('#cities_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=ci'
    });
    return false;
  });
  $('#areas_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=ar'
    });
    return false;
  });
  $('#pcts_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=pct'
    });
    return false;
  });
  $('#pls_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=pl'
    });
    return false;
  });
  $('#fscs_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=fscs'
    });
    return false;
  });
  $('#project_states_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=ts'
    });
    return false;
  });
  $('#pts_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=pt'
    });
    return false;
  });
  $('#subclasses_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=sc'
    });
    return false;
  });
  $('#projectkinds_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=pk'
    });
    return false;
  });
  $('#project_type_subclasses_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=ptsc'
    });
    return false;
  });
  
  $('#rtcs_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=rtc'
    });
    return false;
  });
  $('#lrtcs_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=lrtc'
    });
    return false;
  });
  $('#dsups_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=dsup'
    });
    return false;
  });
  $('#rtos_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=rto'
    });
    return false;
  });
  $('#dcontrs_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=dcontr'
    });
    return false;
  });
  $('#distributionchannel_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=dcs'
    });
    return false;
  });
  $('#design_objectives_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=dos'
    });
    return false;
  });

  $('#costmonitoring_groups_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=cmgr'
    });
    return false;
  });

  $('#item_categories_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=icat'
    });
    return false;
  });

  $('#items_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/project_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=item'
    });
    return false;
  });
  
});
</script>


<?php
$page->footer();
?>