<?php
/********************************************************************

    poslocation_status_query.php

    Status Report POS Locations.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2015-03-24
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2015-03-24
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_all_queries");
set_referer("project_queries_query.php");


require_once "include/projects_query_get_functions.php";


if(param("remove_id"))
{
	$sql = "delete from projectquery_permissions " . 
		   " where projectquery_permission_query = " . param("remove_id") .
		   " and projectquery_permission_user = " . user_id();  
	$result = mysql_query($sql) or dberror($sql);

	redirect("poslocation_status_query.php");

}
elseif(param("delete_id"))
{
	$sql = "delete from projectquery_permissions " . 
		   " where projectquery_permission_query = " . param("delete_id"); 
	$result = mysql_query($sql) or dberror($sql);

	$sql = "delete from projectqueries " . 
		   " where projectquery_id = " . param("delete_id"); 
	$result = mysql_query($sql) or dberror($sql);

	redirect("poslocation_status_query.php");
}


//build query for the list

$sql = "select projectquery_id, projectquery_group, projectquery_name, projectquery_fields, projectquery_owner, " .
       "concat(user_name, ' ', user_firstname) as uname, projectqueries.date_created as cdate, projectqueries.date_modified as mdate " . 
       "from projectqueries " .
	   "left join users on user_id = projectquery_owner ";

$list_filter = "projectquery_owner = " . user_id();

$list_filter2 = $list_filter;


if(param('group_filter')) {
	$list_filter .= " and projectquery_group  = " . dbquote(param('group_filter')) . " ";
}




$query_permissions = array();
$queries = array();
$query_links = array();
$copy_links = array();
$delete_links = array();


//$sql_a = $sql . " where projectquery_owner = " . user_id();

$sql_p = $sql;

if(param('group_filter')) {
	$sql_p .= " where projectquery_group  = " . dbquote(param('group_filter')) . " ";
}

$res = mysql_query($sql_p) or dberror($sql_p);

while ($row = mysql_fetch_assoc($res))
{
	if(check_if_query_has_fields($row["projectquery_id"]) == true)
	{
		$link = "<a href='project_queries_query_xls.php?query_id=" .  $row["projectquery_id"] . "'><img src=\"/pictures/ico_xls.gif\" border='0'/></a>";
		$queries[$row["projectquery_id"]] = $link;
	}
	else
	{
		$queries[$row["projectquery_id"]] = "<img src=\"/pictures/wf_warning.gif\" border='0'/>";
	}

	if(user_id() == $row["projectquery_owner"])
	{
		$query_links[$row["projectquery_id"]] = "<a href='project_queries_query.php?id=" .  $row["projectquery_id"] . "'>" . $row["projectquery_name"] . "</a>";

		$delete_links[$row["projectquery_id"]] = '<img data-id="'. $row["projectquery_id"]. '" class="delete_query" src="/pictures/remove.gif" border="0" style="margin-top:3px;cursor:pointer;" alt="Remove"/>';
	}
	else
	{
		$query_links[$row["projectquery_id"]] = $row["projectquery_name"];
		
		$delete_links[$row["projectquery_id"]] = '<a title="Remove query from my list" href="project_queries.php?remove_id=' .  $row["projectquery_id"] . '"><img src="/pictures/remove.gif" border="0" style="margin-top:3px;" alet="Remove"/></a>';
	}

	$copy_links[$row["projectquery_id"]] = "<a href='project_queries_query.php?copy_id=" .  $row["projectquery_id"] . "'>create copy</a>";

	//get permissions
	
	$sql_p = "select * from projectquery_permissions " . 
			 "where projectquery_permission_query = " . $row["projectquery_id"];

	$res_p = mysql_query($sql_p) or dberror($sql_p);
	while ($row_p = mysql_fetch_assoc($res_p))
	{
		if($row_p["projectquery_permission_user"] == user_id())
		{
			$query_permissions[$row_p["projectquery_permission_query"]] = $row_p["projectquery_permission_query"];
		}
	}
}

if(count($query_permissions) > 0)
{
	$permission_filter =  implode(',', $query_permissions);
	$permission_filter = " or projectquery_id in (" . $permission_filter . ") ";
	$list_filter .= $permission_filter;
}


//compose group filter
$group_filter = array();

$sql_g = "select DISTINCT projectquery_group, projectquery_group " .
                "from projectqueries " .
			    "left join users on user_id = projectquery_owner " .
				" where projectquery_group is not NULL " . 
				" and projectquery_group <> '' " . 
				" and (" . $list_filter2 . ") " . 
				" order by projectquery_group ";


$res = mysql_query($sql_g) or dberror($sql_g);

while ($row = mysql_fetch_assoc($res))
{
	$group_filter[$row["projectquery_group"]] = $row["projectquery_group"];
}





$list = new ListView($sql);

$list->set_entity("projectqueries");
$list->set_order("projectquery_group, projectquery_name");
$list->set_filter($list_filter);
$list->set_group("projectquery_group");

if(count($group_filter) > 0)
{
	$list->add_listfilters("group_filter", "Group", 'select', $group_filter, param("group_filter"));
}

$list->add_text_column("queries", " ", COLUMN_NO_WRAP | COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $queries);
$list->add_text_column("querylinks", "Query Name", COLUMN_NO_WRAP | COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $query_links);
$list->add_column("uname", "Owner", "", "", "", COLUMN_NO_WRAP);

$list->add_column("cdate", "Created", "", "", "", COLUMN_NO_WRAP);
$list->add_column("mdate", "Modified", "", "", "", COLUMN_NO_WRAP);


$list->add_text_column("querylinks", "", COLUMN_NO_WRAP | COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $copy_links);
$list->add_text_column("removelinks", "", COLUMN_NO_WRAP | COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $delete_links);

$list->add_button(LIST_BUTTON_NEW, "New Query", "project_queries_query.php");

$list->process();



$page = new Page("queries");

require_once("include/mis_page_actions.php");

$page->header();
$page->title("POS Locations Status Report");
$list->render();

?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('.delete_query').click(function(e) {
    e.preventDefault();
	

	$.nyroModalManual({
	  url: '/mis/delete_qurey.php?id=' + $(this).attr("data-id") + '&context=project'
    });

	
    return false;
  });
  
  
});
</script>


<?php
$page->footer();

?>
