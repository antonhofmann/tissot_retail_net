<?php
/********************************************************************

    traffic_by_user_xls.php

    Enter parameters for the query of traffic

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-05-17
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-05-17
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_queries");
require_once "../include/phpexcel/PHPExcel.php";


/********************************************************************
    prepare all data needed
*********************************************************************/
$selected_year = date("Y");
if(param("year")) {
	$selected_year = param("year");
}

$selected_module = "user";
if(param("module")) {
	$selected_module = param("module");
}

$number_of_days = 365;
if($selected_year == date("Y"))
{
	$start = strtotime(date("Y") . '-01-01');
	$end = strtotime(date("Y-m-d"));
	$number_of_days = ceil(abs($end - $start) / 86400);

}


$modules = array();
$modules["archive"] = "Archive";
$modules["admin"] = "System Administration";
$modules["cer"] = "CER/AF";
$modules["mis"] = "Management Information";
$modules["pos"] = "POS Index";
$modules["administration"] = "Merchandising Planning System Administration";
$modules["mps"] = "Merchandising Planning Tissot";
$modules["red"] = "Retail Environment Development";
$modules["scpps"] = "Stock Contol and Production Planning";
$modules["sec"] = "Security";
$modules["user"] = "Projects and Catalogue Orders";


$stats = array();
$access_totals = 0;

$table_names = array();
//get all statsitic tables
$sql = 'SHOW TABLES FROM db_retailnet where Tables_in_db_retailnet like "statistic%"';
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$table_names[] = $row["Tables_in_db_retailnet"];
}

foreach($table_names as $key=>$table_name)
{

	$sql = "SELECT statistic_id, statistic_url, user_id, address_id, address_company, country_name, " .
		   "user_name, user_firstname " . 
		   "FROM " . $table_name . 
		   " left join users on user_id = statistic_user " .
	       " left join addresses on address_id = user_address " . 
		   " left join countries on country_id = address_country " . 
		   " WHERE year(statistic_date) = " . $selected_year .
		   " and statistic_url like '%/" . $selected_module . "/%'" .
		   " and statistic_url not like '%login%'" .
		   " and statistic_url not like '%logoff%'" .
		   " order by country_name, address_company, user_name, user_firstname";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$user = $row["user_name"] . ' ' . $row["user_firstname"];
		$user_key = $row["address_id"]. $row["user_id"];

		if(!$row["user_id"])
		{
			$row["country_name"] = 'n.a.';
			$row["address_company"] = 'n.a.';
			$row["user_name"] = 'n.a.';
			$row["user_firstname"] = 'n.a.';
			$user_key = "na";
		}

		
		
		if(array_key_exists($user_key, $stats))
		{
			$tmp = $stats[$user_key]["count"] + 1;
			$stats[$user_key] = array("country"=>$row["country_name"], "company"=>$row["address_company"], "user"=>$user, "count"=>$tmp);
		}
		else
		{
			$stats[$user_key] = array("country"=>$row["country_name"], "company"=>$row["address_company"], "user"=>$user, "count"=>1);
		}
		$access_totals++;
	}
}


array_sort_by_column($stats, 'count', SORT_DESC);

/********************************************************************
    XLS Params
*********************************************************************/
$captions['A'] = "Country";
$captions['B'] = "Company";
$captions['C'] = "User";
$captions['D'] = "Access Count";

$colwidth = array();
$colwidth['A'] = "15";
$colwidth['B'] = "15";
$colwidth['C'] = "15";
$colwidth['D'] = "15";

//output formats
$default_border = array(
    'style' => PHPExcel_Style_Border::BORDER_THIN,
    'color' => array('rgb'=>'1006A3')
);

$style_normal_border = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '1006A3'),
		),
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	)
);


$style_normal_border_right = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '1006A3'),
		),
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	),
);


$style_title = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
    'font' => array(
        'bold' => true,
		'size' => 16
    )
);

$style_header = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'font' => array(
        'bold' => true,
    )
);

$style_header_right = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	),
    'font' => array(
        'bold' => true,
    )
);

$style_bold = array(
	'font' => array(
        'bold' => true
    )
);



/********************************************************************
    Start output
*********************************************************************/
$objPHPExcel = new PHPExcel();

$logo = new PHPExcel_Worksheet_Drawing();
$logo->setName('Logo');
$logo->setDescription('Logo');
$logo->setPath('../pictures/brand_logo.jpg');
$logo->setHeight(36);
$logo->setWidth(113);

$sheet = $objPHPExcel->getActiveSheet();
$objPHPExcel->getActiveSheet()->setTitle('Traffic by User');

$logo->setWorksheet($objPHPExcel->getActiveSheet());


//default styles
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(10); 


// HEADRES ROW 1
$sheet->setCellValue('B1', 'Traffic ' . $modules[$selected_module] . ' (' . date("d.m.Y H:i:s") . ')');
$sheet->getStyle('B1')->applyFromArray( $style_title );
$sheet->getRowDimension('1')->setRowHeight(36);


$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(1);


$row_index = 3;


$access_info = "Acess Total " . $selected_year . " = " . 
               number_format ( $access_totals , 0 , '.' , "'" ) . 
			   " ("  . number_format( $access_totals/$number_of_days , 0 , '.' , "'" ) . " per day) ";

$sheet->setCellValueByColumnAndRow(0, $row_index, $access_info);

$row_index = 5;

foreach($stats as $address_id=>$data)
{
	$cell_index = 0;
	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $data["country"]);
	if($colwidth['A'] < strlen($data["country"])){$colwidth['A'] = strlen($data["country"]);}
	$cell_index++;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $data["company"]);
	if($colwidth['B'] < strlen($data["company"])){$colwidth['B'] = strlen($data["company"]);}
	$cell_index++;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $data["user"]);
	if($colwidth['C'] < strlen($data["user"])){$colwidth['C'] = strlen($data["user"]);}
	$cell_index++;


	$sheet->setCellValueByColumnAndRow($cell_index, $row_index,$data["count"]);
	if($colwidth['D'] < strlen(number_format ( $data["count"] , 0 , '.' , "'" ))){$colwidth['D'] = strlen(number_format ( $data["count"] , 0 , '.' , "'" ));}
	

	$row_index++;

}


//Format column heights and witdhs
foreach($colwidth as $col=>$width) {
	$sheet->getColumnDimension($col)->setWidth($width);
}


/********************************************************************
    Start output
*********************************************************************/
$filename = 'traffic_by_user_' . $selected_module . '_'. date('Ymd H:i:s') . '.xls';
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
header('Pragma: ');

//send data
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');


?>