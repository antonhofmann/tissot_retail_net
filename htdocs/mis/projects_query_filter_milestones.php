<?php
/********************************************************************

    projects_query_filter_milestones.php

    Milestone Filter Selection for the Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2014-08-22
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2014-08-22
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";


check_access("can_perform_queries");

require_once "include/query_get_functions.php";

/********************************************************************
    prepare all data needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("projects_queries.php");
}

$qt = 6;
if(param("qt"))
{
	$qt = param("qt");
}

$query_id = param("query_id");

$mis_query = get_query_name($query_id);

$from_dates = array();
$to_dates = array();
$mfrom = "";
$mto = "";
$sql = "select mis_query_milestone_filter " . 
       "from mis_queries ". 
	   " where mis_query_id = " . dbquote($query_id);

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$tmp = unserialize($row["mis_query_milestone_filter"]);

	//var_dump($tmp);

	if(is_array($tmp) and array_key_exists("from_dates",$tmp))
	{

		foreach($tmp["from_dates"] as $milestone_id=>$date)
		{
			$from_dates[$milestone_id] = to_system_date($date);
		}
	}

	if(is_array($tmp ) and array_key_exists("to_dates",$tmp))
	{
		foreach($tmp["to_dates"] as $milestone_id=>$date)
		{
			$to_dates[$milestone_id] = to_system_date($date);
		}
	}

	if(is_array($tmp ) and array_key_exists("mfrom",$tmp))
	{
		$mfrom = $tmp["mfrom"];
		$mto = $tmp["mto"];
	}
}


//var_dump($to_dates);

/********************************************************************
    Create Form
*********************************************************************/ 
$sql = "select milestone_code, concat(milestone_code, ' ' , milestone_text) as mn from milestones ";
$sql .= " where milestone_active = 1 ";
$sql .= " order by milestone_code";

$form = new Form("mis_queries", "query_generator");

$form->add_hidden("query_id", param("query_id"));
$form->add_hidden("qt", $qt);
$form->add_section($mis_query["name"]);

$form->add_section("Include only Projects with approved milestones");
$form->add_list("mfrom", "From", $sql, 0, $mfrom);
$form->add_list("mto", "To", $sql, 0, $mto);

$form->populate();



/********************************************************************
    Create List of milestones
*********************************************************************/ 
$sql = "select milestone_id, milestone_code, milestone_text from milestones ";
$list_filter = "milestone_active = 1";


$list = new ListView($sql);

$list->set_entity("milestones");
$list->set_order("milestone_code");
$list->set_filter($list_filter);

$list->add_hidden("qt", $qt);
$list->add_column("milestone_code", "Code", "");
$list->add_column("milestone_text", "Name", "");
$list->add_edit_column("from_date", "From date", 10, 0, $from_dates);
$list->add_edit_column("to_date", "To date", 10, 0, $to_dates);

$list->add_button("save", "Save");
$list->add_button("execute", "Execute Query");

$list->populate();
$list->process();


if($list->button("save") or $list->button("execute"))
{
	$tmp = array();


	$tmp["mfrom"] = $form->value('mfrom');
	$tmp["mto"] = $form->value('mto');



	$fd = array();
	foreach($list->values("from_date") as $key => $value)
    {
		$fd[$key] = from_system_date($value);
	}

	$tmp["from_dates"] = $fd;


	$td = array();
	foreach($list->values("to_date") as $key => $value)
    {
		$td[$key] = from_system_date($value);
	}

	$tmp["to_dates"] = $td;

	$tmp = serialize($tmp);

	$sql = "update mis_queries SET " . 
		   "mis_query_milestone_filter = " . dbquote($tmp) . 
		    " where mis_query_id = " . dbquote($query_id);
	
	$result = mysql_query($sql) or dberror($sql);
	$form->message("Your data has been saved.");

	if($list->button("execute"))
	{
		redirect("projects_query_6_xls.php?query_id=" . param("query_id"));
	}
	
}



/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page("queries");

require "include/mis_page_actions.php";

$page->header();
$page->title("Masterplan: Edit Project Query - Milestone Filter");

require_once("include/project_query_tabs.php");

$form->render();
$list->render();

$page->footer();
?>