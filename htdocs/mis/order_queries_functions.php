<?php
/********************************************************************

    order_queries_functions.php

    Set Functions for the Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2016-08-19
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2016-08-19
    Version:        1.0.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if(!has_access("can_perform_all_queries") 
  and !has_access("can_perform_projects_query"))
{
	redirect("noaccess.php");
}
require_once "include/orders_query_check_access.php";

require_once "include/orders_query_get_functions.php";


/********************************************************************
    prepare all data needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("order_queries.php");
}

$query_id = param("query_id");
param("id", param("query_id"));

$orderquery = get_query_name($query_id);
$icon = "../pictures/add_item.gif";
$link = "javascript:open_selector('')";


//get Query Fields
$fields = array();
$sum_fields = array();
$avg_fields = array();
$show_number_of_orders = 0;

$sql = "select orderquery_fields, orderquery_sum_fields, orderquery_avg_fields, " .
       "orderquery_show_number_of_orders " . 
       "from orderqueries " .
	   "where orderquery_id = " . $query_id;

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$fields = unserialize($row["orderquery_fields"]);
	if($fields)
	{
		$fields = array_merge($fields["cl"], $fields["ord"]);
	}

	$sum_fields = unserialize($row["orderquery_sum_fields"]);
	$avg_fields = unserialize($row["orderquery_avg_fields"]);
	$show_number_of_orders = $row["orderquery_show_number_of_orders"];
}

$groups = $query_function_fields;
$choices = array();
foreach($groups as $key=>$field)
{
	if(array_key_exists($field, $fields))
	{
		$choices[$key] = $fields[$field];
	}
}


/********************************************************************
    create form
*********************************************************************/

$form = new Form("orderqueries", "query_generator");

$form->add_hidden("query_id", param("query_id"));
$form->add_hidden("form_save", 1);
$form->add_section($orderquery["name"]);


$form->add_checkbox("orderquery_show_number_of_orders", "Show Number of orders", $$show_number_of_orders, 0, "Number of orders");


if(count($choices) > 0)
{
	$form->add_section("Fields to sum up");
	$form->add_comment("Show totals of the following fields.");


	foreach($choices as $key=>$caption)
	{
		if(is_array($sum_fields) and in_array($key,$sum_fields))
		{
			$form->add_checkbox("t_" . $key, "Show totals of " . str_replace('----', "", $caption), true, 0, $caption);
		}
		else
		{
			$form->add_checkbox("t_" .$key, "Show totals of " . str_replace('----', "", $caption), false, 0, $caption);
		}
	}


	$form->add_section("Fields to build average");
	$form->add_comment("Show averages of the following fields.");


	foreach($choices as $key=>$caption)
	{
		if(in_array($key,$avg_fields))
		{
			$form->add_checkbox("a_" . $key, "Show average of " . str_replace('----', "", $caption), true, 0, $caption);
		}
		else
		{
			$form->add_checkbox("a_" .$key, "Show average of " . str_replace('----', "", $caption), false, 0, $caption);
		}
	}
}


$form->add_button("submit", "Save Functions", 0);


if(check_if_query_has_fields($query_id) == true)
{
	$form->add_button("execute", "Execute Query");
}
$form->add_button("back", "Back to the List of Queries");


/********************************************************************
    process form
*********************************************************************/
$form->populate();
$form->process();

if($form->button("back"))
{
	redirect("order_queries.php");
}
elseif($form->button("submit"))
{
	
	if(count($choices) > 0)
	{

		$sum_fields = array();
		foreach($choices as $key=>$caption)
		{
			if($form->value("t_" . $key) == 1)
			{
				$sum_fields[] = $key;
			}
		}
		
		$sql = "Update orderqueries SET " . 
			   "orderquery_sum_fields = " . dbquote(serialize($sum_fields)) . 
			   " where orderquery_id = " . param("query_id");


		$result = mysql_query($sql) or dberror($sql);
	}


	if(count($choices) > 0)
	{

		$avg_fields = array();
		foreach($choices as $key=>$caption)
		{
			if($form->value("a_" . $key) == 1)
			{
				$avg_fields[] = $key;
			}
		}
		
		$sql = "Update orderqueries SET " . 
			   "orderquery_avg_fields = " . dbquote(serialize($avg_fields)) . 
			   " where orderquery_id = " . param("query_id");



		$result = mysql_query($sql) or dberror($sql);

	}

	$sql = "Update orderqueries SET " . 
		   "orderquery_show_number_of_orders = " . dbquote($form->value("orderquery_show_number_of_orders"))  . ", " .
		   "date_modified = " . dbquote(date("Y-m-d H:s:i")) . ", " . 
		   "user_modified = " . dbquote(user_login()) .
		   " where orderquery_id = " . param("query_id");

	$result = mysql_query($sql) or dberror($sql);


	

	redirect("order_queries_functions.php?query_id=" . param("query_id"));

}
elseif($form->button("execute"))
{
	redirect("order_queries_query_xls.php?query_id=" . param("query_id"));
}


/********************************************************************
    render
*********************************************************************/

$page = new Page("queries");
require_once("include/mis_page_actions.php");

$page->header();
$page->title("Edit Order Query - Functions");

require_once("include/orders_query_tabs.php");

$form->render();

$page->footer();

?>


