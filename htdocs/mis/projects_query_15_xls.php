<?php
/********************************************************************

    projects_query_15_xls.php

    Print delivery of items

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-04-16
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-04-16
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_queries");

require_once "../libraries/phpexcel/Classes/PHPExcel.php";
require_once "include/get_functions.php";
require_once("projects_query_filter_strings.php");


ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 240);

/********************************************************************
    prepare all data needed
*********************************************************************/
function get_monday_of_week($kw,$year = false) {
     if ($year == false) $year = date("Y");
     if ($kw < 0 || $kw > 53) return false;
     $dayofweek = (intval(date("w",mktime(0,0,0,1,1,$year))) == 0) ? 7 : intval(date("w",mktime(0,0,0,1,1,$year)));
     $dayofyear = 7*($kw-2)+(9-$dayofweek);
     $days      = 0;
     $i           = 1; 
     while (true) {
         if ($days+date('t',mktime(0,0,0,$i,1,$year)) < $dayofyear)
		 {
			 $days += date('t',mktime(0,0,0,$i++,1,$year));
		 }
         else
		 {
			 $date = (($dayofyear-$days < 1) ? 1 : ($dayofyear-$days)).".".($i).".$year";

			 $tmp= explode('.', $date);
			 $date = $tmp[2] . '-' . $tmp[1] . '-' . $tmp[0];
			 return $date;
		 }
     }
}


/********************************************************************
    prepare all data needed
*********************************************************************/

if(!param("query_id"))
{
	redirect("projects_queries.php");
}

$from_date = '';
$to_date = '';
$date_base = 1;

$print_query_filter = 0;
$sql = "select mis_query_filter, mis_print_filter from mis_queries " .
	   "where mis_query_id = " . param("query_id");

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{

	if($row["mis_print_filter"] == 1)
	{
		$print_query_filter = 1;
	}
	
	$filters = array();
	$filters = unserialize($row["mis_query_filter"]);

	foreach($filters as $key=>$value)
	{
		$pl = $filters["pl"]; // product lines
		$cnt = $filters["co"]; // Countries

		if(array_key_exists("items", $filters))
		{
			$items = $filters["items"]; // Items
		
		}
		else
		{
			$items = "";
		}

		if(array_key_exists("icat", $filters))
		{
			$icats = $filters["icat"]; // Item categories
		
		}
		else
		{
			$icats = "";
		}

		
	}

	$fdy = $filters["fdy"]; // Delivered from year
	$tdy = $filters["tdy"]; // Delivered to year

	$fdm = $filters["fdm"]; // Delivered from week
	$tdm = $filters["tdm"]; // Delivered to week


	if($fdm and $tdm)
	{
		$from_date = get_monday_of_week($fdm,$fdy);
		
		$tdm++;
		$to_date = get_monday_of_week($tdm,$tdy);
	}
	elseif($fdm)
	{
		$from_date = get_monday_of_week($fdm,$fdy);
	}
	elseif($tdm)
	{
		$tdm++;
		$to_date = get_monday_of_week($tdm,$tdy);
	}
	elseif($fdy and $tdy)
	{
		$from_date = $fdy . "-01-01";
		$to_date = $tdy . "-12-31";
	}
	elseif($fdy )
	{
		$from_date = $fdy . "-01-01";
	}
	elseif($tdy)
	{
		$to_date = $tdy . "-12-31";
	}

	$suppliers = "";
	if(array_key_exists("supp", $filters))
	{
		$suppliers = $filters["supp"];
	}

	if(array_key_exists("dba", $filters))
	{
		$date_base = $filters["dba"];
	}
}
else
{
	redirect("projects_queries.php");
}

//items
$items = substr($items,0, strlen($items)-1); // remove last comma
$items = str_replace("-", ",", $items);

$item_filter = "";
$item_filter2 = "";
if($items)
{
	$item_filter = " and order_item_item IN (" . $items . ") ";
	$item_filter2 = " and item_id IN (" . $items . ") ";
}

if(!$item_filter)
{
	
	//product lines
	$pls = substr($pl,0, strlen($pl)-1); // remove last comma
	$pls = str_replace("-", ",", $pls);

	if($pls)
	{
		$item_filter .= " and product_line_id IN (" . $pls . ") ";
		$item_filter2 .= " and product_line_id IN (" . $pls . ") ";
	}

	//item categories
	$icats = substr($icats,0, strlen($icats)-1); // remove last comma
	$icats = str_replace("-", ",", $icats);
	if($icats)
	{
		$item_filter .= " and item_category IN (" . $icats . ") ";
		$item_filter2 .= " and item_category IN (" . $icats . ") ";
	}
}

if($date_base == 2)
{
	if($item_filter and $from_date)
	{
		$item_filter .= " and order_item_pickup >= " . dbquote($from_date);
		$_filter_strings["Picked Up"] = to_system_date($from_date);
	}
	elseif($from_date)
	{
		$item_filter = " and order_item_pickup >= " . dbquote($from_date);
		$_filter_strings["Picked Up"] = to_system_date($from_date);
		 
	}

	if($item_filter and $to_date > 0)
	{
		$item_filter .= " and order_item_pickup < " . dbquote($to_date);
		if($_filter_strings["Picked Up"])
		{
			$_filter_strings["Picked Up"] .= " - " .to_system_date($to_date);
		}
		else
		{
			$_filter_strings["Picked Up"] = " - " .to_system_date($to_date);
		}
	}
	elseif($to_date)
	{
		$item_filter = " and order_item_pickup < " . dbquote($to_date);
		if($_filter_strings["Picked Up"])
		{
			$_filter_strings["Picked Up"] .= " - " .to_system_date($to_date);
		}
		else
		{
			$_filter_strings["Picked Up"] = " - " .to_system_date($to_date);
		}
	}
}
else
{

	if($item_filter and $from_date)
	{
		$item_filter .= " and order_item_arrival >= " . dbquote($from_date);
		$_filter_strings["Delievery"] = to_system_date($from_date);
	}
	elseif($from_date)
	{
		$item_filter = " and order_item_arrival >= " . dbquote($from_date);
		$_filter_strings["Delievery"] = to_system_date($from_date);
		 
	}

	if($item_filter and $to_date > 0)
	{
		$item_filter .= " and order_item_arrival < " . dbquote($to_date);
		if($_filter_strings["Delievery"])
		{
			$_filter_strings["Delievery"] .= " - " .to_system_date($to_date);
		}
		else
		{
			$_filter_strings["Delievery"] = " - " .to_system_date($to_date);
		}
	}
	elseif($to_date)
	{
		$item_filter = " and order_item_arrival < " . dbquote($to_date);
		if($_filter_strings["Delievery"])
		{
			$_filter_strings["Delievery"] .= " - " .to_system_date($to_date);
		}
		else
		{
			$_filter_strings["Delievery"] = " - " .to_system_date($to_date);
		}
	}
}


//countries
$country_filter = "";
$country_filter1 = "";
$cnt = substr($cnt,0, strlen($cnt)-1); // remove last comma
$cnt = str_replace("-", ",", $cnt);
if($cnt)
{
    $country_filter =  " where (posaddress_country IN (" . $cnt . "))";
	$country_filter1 =  " and (address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);
}

//suppliers
$suppliers = substr($suppliers,0, strlen($suppliers)-1); // remove last comma
$suppliers = str_replace("-", ",", $suppliers);

$supplier_filter = "";
$supplier_filter2 = "";
if($suppliers)
{
	$supplier_filter = " and order_item_supplier_address IN (" . $suppliers . ") ";
	$supplier_filter2 = " and supplier_address IN (" . $suppliers . ") ";
	$_filter_strings["Supplier"] = get_filter_string("supp", $suppliers);
}


if (!$item_filter && !$supplier_filter2 && !$country_filter1) {
	die("Please select at least one query filter");
}


//get items in selection
$items = array();

if($supplier_filter2)
{
	$sql_o = "select DISTINCT item_id, concat(item_code, ' ', item_name) as item_code " .
			 "from order_items " .
		     "left join items on item_id = order_item_item " .
		     " left join item_supplying_groups on item_supplying_group_item_id = item_id " .
			 " left join product_line_supplying_groups on product_line_supplying_group_group_id = item_supplying_group_supplying_group_id " . 
			 " left join product_lines on product_line_id = product_line_supplying_group_line_id " .  
		     "left join suppliers on supplier_item = item_id " . 
		     "left join orders on order_id = order_item_order " . 
			 "left join addresses on address_id = order_client_address " .
			 "where item_type < 3 " .
		     $item_filter .
		     $supplier_filter2 .
		     $country_filter1;
}
else
{
	
	$sql_o = "select DISTINCT item_id, concat(item_code, ' ', item_name) as item_code " .
			 "from order_items " .
		     "left join items on item_id = order_item_item " .
		     " left join item_supplying_groups on item_supplying_group_item_id = item_id " .
			 " left join product_line_supplying_groups on product_line_supplying_group_group_id = item_supplying_group_supplying_group_id " . 
			 " left join product_lines on product_line_id = product_line_supplying_group_line_id " .  
		     "left join orders on order_id = order_item_order " . 
			 "left join addresses on address_id = order_client_address " .
		     "where item_type < 3 " .
		     $item_filter .
		     $country_filter1;
}


$res_o = mysql_query($sql_o) or dberror($sql_o);
while ($row_o = mysql_fetch_assoc($res_o))
{
	$items[$row_o["item_id"]] = $row_o["item_code"];
}



$captions['A'] = "Nr";
$captions['B'] = "Country";
$captions['C'] = "Client";
$captions['D'] = "Project Number";
$captions['E'] = "POS Type";
$captions['F'] = "POS Location";
if($date_base == 2)
{
	$captions['G'] = "Pickup Date";
}
else
{
	$captions['G'] = "Arrival Date";
}


$colwidth = array();
$colwidth['A'] = "5";
$colwidth['B'] = "15";
$colwidth['C'] = "15";
$colwidth['D'] = "5";
$colwidth['E'] = "5";
$colwidth['F'] = "10";
$colwidth['G'] = "10";

$achar = "G";
$colchars = 1;
foreach($items as $item_id=>$item_code)
{
	if(($achar == 'Z' and $colchars == 21) or $colchars == 22) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['U'. $achar] = $item_code;
		$colchars = 21;
		$colwidth['U' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 20) or $colchars == 21) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['T'. $achar] = $item_code;
		$colchars = 20;
		$colwidth['T' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 19) or $colchars == 20) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['S'. $achar] = $item_code;
		$colchars = 19;
		$colwidth['S' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 18) or $colchars == 19) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['R'. $achar] = $item_code;
		$colchars = 19;
		$colwidth['R' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 17) or $colchars == 18) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['Q'. $achar] = $item_code;
		$colchars = 18;
		$colwidth['Q' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 16) or $colchars == 17) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['P'. $achar] = $item_code;
		$colchars = 17;
		$colwidth['P' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 15) or $colchars == 16) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['O'. $achar] = $item_code;
		$colchars = 16;
		$colwidth['O' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 14) or $colchars == 15) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['N'. $achar] = $item_code;
		$colchars = 15;
		$colwidth['N' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 13) or $colchars == 14) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['M'. $achar] = $item_code;
		$colchars = 14;
		$colwidth['M' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 12) or $colchars == 13) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['L'. $achar] = $item_code;
		$colchars = 13;
		$colwidth['L' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 11) or $colchars == 12) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['K'. $achar] = $item_code;
		$colchars = 12;
		$colwidth['K' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 10) or $colchars == 11) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['J'. $achar] = $item_code;
		$colchars = 11;
		$colwidth['J' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 9) or $colchars == 10) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['I'. $achar] = $item_code;
		$colchars = 10;
		$colwidth['I' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 8) or $colchars == 9) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['H'. $achar] = $item_code;
		$colchars = 9;
		$colwidth['H' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 7) or $colchars == 8) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['G'. $achar] = $item_code;
		$colchars = 8;
		$colwidth['G' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 6) or $colchars == 7) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['F'. $achar] = $item_code;
		$colchars = 7;
		$colwidth['F' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 5) or $colchars == 6) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['E'. $achar] = $item_code;
		$colchars = 6;
		$colwidth['E' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 4) or $colchars == 5) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['D'. $achar] = $item_code;
		$colchars = 5;
		$colwidth['D' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 3) or $colchars == 4) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['C'. $achar] = $item_code;
		$colchars = 4;
		$colwidth['C' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 2) or $colchars == 3) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['B'. $achar] = $item_code;
		$colchars = 3;
		$colwidth['B' . $achar] = "5";
	}
	elseif($achar == 'Z' or $colchars == 2) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['A'. $achar] = $item_code;
		$colchars = 2;
		$colwidth['A' . $achar] = "5";
	}
	else
	{
		$achar = chr(ord($achar) + 1);
		$captions[$achar] = $item_code;
		$colwidth[$achar] = "5";
	}
}



/********************************************************************
    Styles
*********************************************************************/
//output formats
$default_border = array(
    'style' => PHPExcel_Style_Border::BORDER_THIN,
    'color' => array('rgb'=>'1006A3')
);

$style_normal_border = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '1006A3'),
		),
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	)
);


$style_normal_border_right = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '1006A3'),
		),
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	),
);


$style_title = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
    'font' => array(
        'bold' => true,
		'size' => 16
    )
);

$style_header = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'font' => array(
        'bold' => true,
    )
);

$style_header_right = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	),
    'font' => array(
        'bold' => true,
    )
);

$style_bold = array(
	'font' => array(
        'bold' => true
    )
);


/********************************************************************
    Start output
*********************************************************************/
$objPHPExcel = new PHPExcel();

$logo = new PHPExcel_Worksheet_Drawing();
$logo->setName('Logo');
$logo->setDescription('Logo');
$logo->setPath('../pictures/brand_logo.jpg');
$logo->setHeight(36);
$logo->setWidth(113);

$sheet = $objPHPExcel->getActiveSheet();
$objPHPExcel->getActiveSheet()->setTitle('Projects');

$logo->setWorksheet($objPHPExcel->getActiveSheet());


//default styles
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(8); 


// HEADRES ROW 1
if($date_base == 2)
{
	$sheet->setCellValue('D1', 'Pickup of Items in Projects (' . date("d.m.Y H:i:s") . ')');
}
else
{
	$sheet->setCellValue('D1', 'Delivery of Items in Projects (' . date("d.m.Y H:i:s") . ')');
}
$sheet->getStyle('D1')->applyFromArray( $style_title );
$sheet->getRowDimension('1')->setRowHeight(36);


$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(1);


$row_index = 2;

if($print_query_filter == 1)
{
	foreach($_filter_strings as $key=>$value)
	{
		 $sheet->setCellValueByColumnAndRow(0, $row_index, $key . ": " . $value);
		 $row_index++;
	}
	$row_index++;
}



// HEADRES 
foreach($captions as $col=>$caption){
    $sheet->setCellValue($col . $row_index, $caption);
	$sheet->getStyle($col . $row_index)->applyFromArray( $style_header );
	$sheet->getStyle($col . $row_index)->getAlignment()->setTextRotation(90);
}

$sheet->getRowDimension($row_index)->setRowHeight(150);



$row_index++;
$row_index_data = $row_index;

//OUTPUT DATA
$zebra_counter = 0;
$row_index = $row_index_data;
$i = 1;
$cell_index = 0;
$item_totals = array();

//orders and order_items
$orders = array();
$order_items = array();
$order_countries = array();
$totals_delivered_to_country = array();


if($date_base == 2)
{
	$sql_o = "select DISTINCT order_item_order, order_number, address_company, " . 
			 "addresscountries.country_name as country_name, order_shop_address_company, " .
			 "shopcountries.country_name as shop_country_name, postype_name, order_item_pickup " . 
			 "from order_items " .
			 "left join orders on order_id = order_item_order " . 
			 "left join addresses on address_id = order_client_address " . 
			 "left join countries as addresscountries on addresscountries.country_id = address_country " . 
			 "left join countries as shopcountries on shopcountries.country_id = order_shop_address_country " . 
			 "left join items on item_id = order_item_item " .
			 " left join item_supplying_groups on item_supplying_group_item_id = item_id " .
			 " left join product_line_supplying_groups on product_line_supplying_group_group_id = item_supplying_group_supplying_group_id " . 
			 " left join product_lines on product_line_id = product_line_supplying_group_line_id " .
		     " left join projects on project_order = order_id " .
		     "left join postypes on postype_id = project_postype " . 
			 "where order_type = 1 and order_item_pickup <> '0000-00-00' and order_item_pickup is not null " .
			 $item_filter . 
			 $supplier_filter . 
			 $country_filter1 .
			 " order by shop_country_name, country_name, address_company, order_number, order_item_pickup";
}
else
{
	$sql_o = "select DISTINCT order_item_order, order_number, address_company, " . 
			 "addresscountries.country_name as country_name, order_shop_address_company, " .
			 "shopcountries.country_name as shop_country_name, postype_name, order_item_arrival " . 
			 "from order_items " .
			 "left join orders on order_id = order_item_order " . 
			 "left join addresses on address_id = order_client_address " . 
			 "left join countries as addresscountries on addresscountries.country_id = address_country " . 
			 "left join countries as shopcountries on shopcountries.country_id = order_shop_address_country " . 
			 "left join items on item_id = order_item_item " .
			 " left join item_supplying_groups on item_supplying_group_item_id = item_id " .
			 " left join product_line_supplying_groups on product_line_supplying_group_group_id = item_supplying_group_supplying_group_id " . 
			 " left join product_lines on product_line_id = product_line_supplying_group_line_id " .
		     " left join projects on project_order = order_id " .
		     " left join postypes on postype_id = project_postype " . 
			 "where order_type = 1 and order_item_arrival <> '0000-00-00' and order_item_arrival is not null " .
			 $item_filter . 
			 $supplier_filter . 
			 $country_filter1 .
		 " order by shop_country_name, country_name, address_company, order_number, order_item_arrival";
}



$tmp = array();
$res_o = mysql_query($sql_o) or dberror($sql_o);
while ($row_o = mysql_fetch_assoc($res_o))
{
	if($date_base == 2)
	{
		$orders[$row_o["order_item_order"] . "@" . $row_o["order_item_pickup"]] = $row_o["order_number"];
	}
	else
	{
		$orders[$row_o["order_item_order"] . "@" . $row_o["order_item_arrival"]] = $row_o["order_number"];
	}
	$order_countries[$row_o["order_item_order"]] = $row_o["shop_country_name"];

		
	if(count($order_countries) == 1 )
	{
		$old_country = $row_o["shop_country_name"];
	}
	elseif(count($order_countries) > 0 and $row_o["shop_country_name"] != $old_country)
	{
		
		if($date_base == 2)
		{
			$sheet->setCellValueByColumnAndRow(5, $row_index, "Total picked up to " . $old_country);
		}
		else
		{
			$sheet->setCellValueByColumnAndRow(5, $row_index, "Total delivered to " . $old_country);
		}
		
		
		$old_country = $row_o["shop_country_name"];
		$row_index = $row_index + 2;
	}


	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $i);
	if($colwidth['A'] < strlen($i)){$colwidth['A'] = strlen($i);}
	$cell_index++;

	
	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row_o["country_name"]);
	if($colwidth['B'] < strlen($row_o["country_name"])){$colwidth['B'] = 2+strlen($row_o["country_name"]);}
	$cell_index++;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row_o["address_company"]);
	if($colwidth['C'] < strlen($row_o["address_company"])){$colwidth['C'] = 2+strlen($row_o["address_company"]);}
	$cell_index++;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row_o["order_number"]);
	if($colwidth['D'] < strlen($row_o["order_number"])){$colwidth['D'] = 2+strlen($row_o["order_number"]);}
	$cell_index++;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row_o["postype_name"]);
	if($colwidth['E'] < strlen($row_o["postype_name"])){$colwidth['E'] = 2+strlen($row_o["postype_name"]);}
	$cell_index++;

	$pos_address = $row_o["shop_country_name"] . " - " . $row_o["order_shop_address_company"];
	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $pos_address);
	if($colwidth['F'] < strlen($pos_address)){$colwidth['F'] = 2+strlen($pos_address);}
	$cell_index++;

	$i++;
	$row_index++;
	$cell_index = 0;
}


//output items
$row_index = $row_index_data;
$item_totals = array();
$old_country = "";
$country_totals = array();
$old_country_for_totals = 0;

foreach($orders as $key=>$order)
{
	$tmp = explode('@', $key);
	$order_id = $tmp[0];
	$delivery_date = $tmp[1];



	$cell_index = 7;
	if($old_country == "")
	{
		$old_country = $order_countries[$order_id];
		$totals_delivered_to_country[$old_country] = array();
	}
	elseif($order_countries[$order_id] != $old_country)
	{
		$old_country_for_totals = $old_country;
		$old_country = $order_countries[$order_id];
		$totals_delivered_to_country[$old_country_for_totals] = array();

		foreach($items as $item_id=>$item_code)
		{
			if(array_key_exists($item_id, $country_totals))
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $country_totals[$item_id]);

				
				if(!array_key_exists($item_id, $totals_delivered_to_country[$old_country_for_totals]))
				{
					$totals_delivered_to_country[$old_country_for_totals][$item_id] = $country_totals[$item_id];
				}
				else
				{
					$totals_delivered_to_country[$old_country_for_totals][$item_id] = $totals_delivered_to_country[$old_country_for_totals][$item_id] + $country_totals[$item_id];
				}
			}

			$cell_index++;
		}
		$row_index = $row_index + 2;
		$country_totals = array();
	}

	$cell_index = 7;

	foreach($items as $item_id=>$item_code)
	{
		
		
		if($date_base == 2)
		{
			$sql_i = "select order_item_item, order_item_quantity, order_item_system_price, order_item_pickup " .
					 "from order_items  " .
					 "where order_item_order =  " .  $order_id . 
					 " and order_item_item = " . dbquote($item_id) . 
					 " and order_item_pickup = " . dbquote($delivery_date) .
					 " order by order_item_pickup";
		}
		else
		{
			$sql_i = "select order_item_item, order_item_quantity, order_item_system_price, order_item_arrival " .
					 "from order_items  " .
					 "where order_item_order =  " .  $order_id . 
					 " and order_item_item = " . dbquote($item_id) . 
					 " and order_item_arrival = " . dbquote($delivery_date) .
					 " order by order_item_arrival";
		}


		$res_i = mysql_query($sql_i) or dberror($sql_i);
		if ($row_i = mysql_fetch_assoc($res_i))
		{
			if($date_base == 2)
			{
				$sheet->setCellValueByColumnAndRow(6, $row_index, mysql_date_to_xls_date($row_i["order_item_pickup"]));
			}
			else
			{
				$sheet->setCellValueByColumnAndRow(6, $row_index, mysql_date_to_xls_date($row_i["order_item_arrival"]));
			}
			$sheet->getStyle('F' . $row_index . ':F' . $row_index)->getNumberFormat()->setFormatCode('dd/mm/yyyy');
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row_i["order_item_quantity"]);
					
			if(array_key_exists($row_i["order_item_item"], $country_totals))
			{
				$country_totals[$row_i["order_item_item"]] = $country_totals[$row_i["order_item_item"]] + $row_i["order_item_quantity"];
			}
			else
			{
				$country_totals[$row_i["order_item_item"]] = $row_i["order_item_quantity"];
			}
			
			if(array_key_exists($row_i["order_item_item"], $item_totals))
			{
				$item_totals[$row_i["order_item_item"]] = $item_totals[$row_i["order_item_item"]] + $row_i["order_item_quantity"];
			}
			else
			{
				$item_totals[$row_i["order_item_item"]] = $row_i["order_item_quantity"];
			}

		}
		$cell_index++;
	}

	foreach($colwidth as $col=>$width) {
		$sheet->getStyle($col . $row_index)->applyFromArray($style_normal_border);
	}
	
	
	$row_index++;
}



if(count($orders) > 0)
{
	if($date_base == 2)
	{
		$sheet->setCellValueByColumnAndRow(5, $row_index, "Total picked up to " . $old_country);
	}
	else
	{
		$sheet->setCellValueByColumnAndRow(5, $row_index, "Total delivered to " . $old_country);
	}

	$cell_index = 7;
	foreach($items as $item_id=>$item_code)
	{
		if(array_key_exists($item_id, $country_totals))
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $country_totals[$item_id]);

			if(array_key_exists($old_country, $totals_delivered_to_country))
			{
				if(!array_key_exists($item_id, $totals_delivered_to_country[$old_country]))
				{
					$totals_delivered_to_country[$old_country][$item_id] = $country_totals[$item_id];
				}
				else
				{
					$totals_delivered_to_country[$old_country][$item_id] = $totals_delivered_to_country[$old_country][$item_id] + $country_totals[$item_id];
				}
			}
			else
			{
				$totals_delivered_to_country[$old_country][$item_id] = $country_totals[$item_id];
			}
		}
		$cell_index++;
	}
	$row_index = $row_index + 2;
}



//print totals
if(count($orders) > 0)
{
	$cell_index = 7;
	$row_index++;
	$sheet->setCellValueByColumnAndRow(1, $row_index, "List Total");
	foreach($items as $item_id=>$item)
	{
		if(array_key_exists($item_id, $item_totals))
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $item_totals[$item_id]);
		}
		$cell_index++;

	}
}

//Format column heights and witdhs
foreach($colwidth as $col=>$width) {
	$sheet->getColumnDimension($col)->setWidth($width);
}





//TAB 2: Catalogue Orders
$logo2 = new PHPExcel_Worksheet_Drawing();
$logo2->setName('Logo');
$logo2->setDescription('Logo');
$logo2->setPath('../pictures/brand_logo.jpg');
$logo2->setHeight(36);
$logo2->setWidth(113);

$sheet2 = $objPHPExcel->createSheet();
$sheet2->setTitle('Catalogue Orders');
$objPHPExcel->setActiveSheetIndex(1);
$logo2->setWorksheet($objPHPExcel->getActiveSheet());
$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->setActiveSheetIndex(1);
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(1);


$captions['A'] = "Nr";
$captions['B'] = "Country";
$captions['C'] = "Client";
$captions['D'] = "Order Number";
$captions['E'] = "Delivered to";

if($date_base == 2)
{
	$captions['F'] = "Pickup Date";
}
else
{
	$captions['F'] = "Arrival Date";
}


$colwidth = array();
$colwidth['A'] = "5";
$colwidth['B'] = "15";
$colwidth['C'] = "15";
$colwidth['D'] = "5";
$colwidth['E'] = "5";
$colwidth['F'] = "15";

$achar = "F";
$colchars = 1;
foreach($items as $item_id=>$item_code)
{
	if(($achar == 'Z' and $colchars == 11) or $colchars == 12) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['K'. $achar] = $item_code;
		$colchars = 12;
		$colwidth['K' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 10) or $colchars == 11) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['J'. $achar] = $item_code;
		$colchars = 11;
		$colwidth['J' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 9) or $colchars == 10) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['I'. $achar] = $item_code;
		$colchars = 10;
		$colwidth['I' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 8) or $colchars == 9) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['H'. $achar] = $item_code;
		$colchars = 9;
		$colwidth['H' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 7) or $colchars == 8) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['G'. $achar] = $item_code;
		$colchars = 8;
		$colwidth['G' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 6) or $colchars == 7) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['F'. $achar] = $item_code;
		$colchars = 7;
		$colwidth['F' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 5) or $colchars == 6) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['E'. $achar] = $item_code;
		$colchars = 6;
		$colwidth['E' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 4) or $colchars == 5) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['D'. $achar] = $item_code;
		$colchars = 5;
		$colwidth['D' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 3) or $colchars == 4) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['C'. $achar] = $item_code;
		$colchars = 4;
		$colwidth['C' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 2) or $colchars == 3) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['B'. $achar] = $item_code;
		$colchars = 3;
		$colwidth['B' . $achar] = "5";
	}
	elseif($achar == 'Z' or $colchars == 2) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['A'. $achar] = $item_code;
		$colchars = 2;
		$colwidth['A' . $achar] = "5";
	}
	else
	{
		$achar = chr(ord($achar) + 1);
		$captions[$achar] = $item_code;
		$colwidth[$achar] = "5";
	}
}


// HEADRES ROW 1

if($date_base == 2)
{
	$sheet2->setCellValue('D1', 'Pickup of Items in Catalogue Orders (' . date("d.m.Y H:i:s") . ')');
}
else
{
	$sheet2->setCellValue('D1', 'Delivery of Items in Catalogue Orders (' . date("d.m.Y H:i:s") . ')');
}

$sheet2->getStyle('D1')->applyFromArray( $style_title );
$sheet2->getRowDimension('1')->setRowHeight(36);



$row_index = 2;

if($print_query_filter == 1)
{
	foreach($_filter_strings as $key=>$value)
	{
		 $sheet2->setCellValueByColumnAndRow(0, $row_index, $key . ": " . $value);
		 $row_index++;
	}
	$row_index++;
}

// HEADRES 
foreach($captions as $col=>$caption){
    $sheet2->setCellValue($col . $row_index, $caption);
	$sheet2->getStyle($col . $row_index)->applyFromArray( $style_header );
	$sheet2->getStyle($col . $row_index)->getAlignment()->setTextRotation(90);
}

$sheet2->getRowDimension($row_index)->setRowHeight(150);



//OUTPUT DATA
$zebra_counter = 0;
$row_index++;
$row_index_data = $row_index;
$i = 1;
$cell_index = 0;
$item_totals = array();
$order_countries = array();


//orders and order_items
$orders = array();
$order_items = array();

if($date_base == 2)
{
	$sql_o = "select DISTINCT order_item_order, order_number, address_company, " . 
			 "addresscountries.country_name as country_name,  " .
			 "delievrycountries.country_name as delivery_country_name, order_item_pickup,  " .
			 "order_address_company " . 
			 "from order_items " .
			 "left join orders on order_id = order_item_order " . 
			 "left join addresses on address_id = order_client_address " . 
			 "left join countries as addresscountries on addresscountries.country_id = address_country " . 
			 "left join items on item_id = order_item_item " .
			 " left join item_supplying_groups on item_supplying_group_item_id = item_id " .
			 " left join product_line_supplying_groups on product_line_supplying_group_group_id = item_supplying_group_supplying_group_id " . 
			 " left join product_lines on product_line_id = product_line_supplying_group_line_id " .  
			 "left join order_addresses on order_address_order_item = order_item_id " .
			 "left join countries as delievrycountries on delievrycountries.country_id = order_address_country " . 
			 "where order_type = 2 " . 
			 "and order_item_pickup <> '0000-00-00' and order_item_pickup is not null " .
			 "and order_address_type = 2 " . 
			 $item_filter .
			 $supplier_filter .
			 $country_filter1 .
			 " order by delivery_country_name, address_company, order_number";
}
else
{
	$sql_o = "select DISTINCT order_item_order, order_number, address_company, " . 
			 "addresscountries.country_name as country_name,  " .
			 "delievrycountries.country_name as delivery_country_name, order_item_arrival,  " .
			 "order_address_company " . 
			 "from order_items " .
			 "left join orders on order_id = order_item_order " . 
			 "left join addresses on address_id = order_client_address " . 
			 "left join countries as addresscountries on addresscountries.country_id = address_country " . 
			 "left join items on item_id = order_item_item " .
			 " left join item_supplying_groups on item_supplying_group_item_id = item_id " .
			 " left join product_line_supplying_groups on product_line_supplying_group_group_id = item_supplying_group_supplying_group_id " . 
			 " left join product_lines on product_line_id = product_line_supplying_group_line_id " .  
			 "left join order_addresses on order_address_order_item = order_item_id " .
			 "left join countries as delievrycountries on delievrycountries.country_id = order_address_country " . 
			 "where order_type = 2 " . 
			 "and order_item_arrival <> '0000-00-00' and order_item_arrival is not null " .
			 "and order_address_type = 2 " . 
			 $item_filter .
			 $supplier_filter .
			 $country_filter1 .
			 " order by delivery_country_name, address_company, order_number";
}


$tmp = array();
$res_o = mysql_query($sql_o) or dberror($sql_o);
while ($row_o = mysql_fetch_assoc($res_o))
{
	if($date_base == 2)
	{
		$orders[$row_o["order_item_order"] . "@" . $row_o["order_item_pickup"]] = array("order_number"=>$row_o["order_number"], "address_company"=>$row_o["address_company"], "country_name"=>$row_o["country_name"]);
	}
	else
	{
		$orders[$row_o["order_item_order"] . "@" . $row_o["order_item_arrival"]] = array("order_number"=>$row_o["order_number"], "address_company"=>$row_o["address_company"], "country_name"=>$row_o["country_name"]);
	}

	$order_countries[$row_o["order_item_order"]] = $row_o["delivery_country_name"];

	if(count($order_countries) == 1 )
	{
		$old_country = $row_o["delivery_country_name"];
	}
	elseif(count($order_countries) > 0 and $row_o["delivery_country_name"] != $old_country)
	{
		
		
		if($date_base == 2)
		{
			$sheet2->setCellValueByColumnAndRow(4, $row_index, "Total picked up to " . $old_country);
		}
		else
		{
			$sheet2->setCellValueByColumnAndRow(4, $row_index, "Total delivered to " . $old_country);
		}

		$old_country = $row_o["delivery_country_name"];

		$row_index = $row_index + 2;
	}
	


	$sheet2->setCellValueByColumnAndRow($cell_index, $row_index, $i);
	if($colwidth['A'] < strlen($i)){$colwidth['A'] = strlen($i);}
	$cell_index++;

	
	$sheet2->setCellValueByColumnAndRow($cell_index, $row_index, $row_o["country_name"]);
	if($colwidth['B'] < strlen($row_o["country_name"])){$colwidth['B'] = 2+strlen($row_o["country_name"]);}
	$cell_index++;

	$sheet2->setCellValueByColumnAndRow($cell_index, $row_index, $row_o["address_company"]);
	if($colwidth['C'] < strlen($row_o["address_company"])){$colwidth['C'] = 2+strlen($row_o["address_company"]);}
	$cell_index++;

	$sheet2->setCellValueByColumnAndRow($cell_index, $row_index, $row_o["order_number"]);
	if($colwidth['D'] < strlen($row_o["order_number"])){$colwidth['D'] = 2+strlen($row_o["order_number"]);}
	$cell_index++;

	$delivered_to = $row_o["delivery_country_name"] . " - " . $row_o["order_address_company"];
	$sheet2->setCellValueByColumnAndRow($cell_index, $row_index, $delivered_to);
	if($colwidth['E'] < strlen($delivered_to)){$colwidth['E'] = 2+strlen($delivered_to);}
	$cell_index++;

	$i++;
	$row_index++;
	$cell_index = 0;
}



//output items
$row_index = $row_index_data;
$old_country = "";
$country_totals = array();


foreach($orders as $key=>$order)
{
	$tmp = explode('@', $key);
	$order_id = $tmp[0];
	$delivery_date = $tmp[1];
	
	$cell_index = 6;
	
	if($old_country == "")
	{
		$old_country = $order_countries[$order_id];
		
		if(!array_key_exists($old_country, $totals_delivered_to_country))
		{
			$totals_delivered_to_country[$old_country] = array();
		}
	}
	elseif($order_countries[$order_id] != $old_country)
	{
		$old_country_for_totals = $old_country;
		$old_country = $order_countries[$order_id];
		
		if(!array_key_exists($old_country_for_totals, $totals_delivered_to_country))
		{
			$totals_delivered_to_country[$old_country_for_totals] = array();
		}

		foreach($items as $item_id=>$item_code)
		{
			if(array_key_exists($item_id, $country_totals))
			{
				$sheet2->setCellValueByColumnAndRow($cell_index, $row_index, $country_totals[$item_id]);
				
				if(!array_key_exists($item_id, $totals_delivered_to_country[$old_country_for_totals]))
				{
					$totals_delivered_to_country[$old_country_for_totals][$item_id] = $country_totals[$item_id];
				}
				else
				{
					$totals_delivered_to_country[$old_country_for_totals][$item_id] = $totals_delivered_to_country[$old_country_for_totals][$item_id] + $country_totals[$item_id];
				}
			}
			$cell_index++;
		}
		$row_index = $row_index + 2;
		$country_totals = array();
		
	}
	
	$cell_index = 6;
	foreach($items as $item_id=>$item_code)
	{
		if($date_base == 2)
		{
			$sql_i = "select order_item_item, order_item_quantity, order_item_pickup " .
					 "from order_items " .
					 "where order_item_order =  " .  $order_id . 
					 " and order_item_item = " . dbquote($item_id) . 
					 " and order_item_pickup = " . dbquote($delivery_date) . 
					 " order by order_item_pickup";
		}
		else
		{
			$sql_i = "select order_item_item, order_item_quantity, order_item_arrival " .
					 "from order_items " .
					 "where order_item_order =  " .  $order_id . 
					 " and order_item_item = " . dbquote($item_id) . 
					 " and order_item_arrival = " . dbquote($delivery_date) . 
					 " order by order_item_arrival";
		}

		$res_i = mysql_query($sql_i) or dberror($sql_i);
		if ($row_i = mysql_fetch_assoc($res_i))
		{
			if($date_base == 2)
			{
				$sheet2->setCellValueByColumnAndRow(5, $row_index, mysql_date_to_xls_date($row_i["order_item_pickup"]));
			}
			else
			{
				$sheet2->setCellValueByColumnAndRow(5, $row_index, mysql_date_to_xls_date($row_i["order_item_arrival"]));
			}
			$sheet2->getStyle('F' . $row_index . ':F' . $row_index)->getNumberFormat()->setFormatCode('dd/mm/yyyy');
			$sheet2->setCellValueByColumnAndRow($cell_index, $row_index, $row_i["order_item_quantity"]);

			if(array_key_exists($row_i["order_item_item"], $item_totals))
			{
				$item_totals[$row_i["order_item_item"]] = $item_totals[$row_i["order_item_item"]] + $row_i["order_item_quantity"];
			}
			else
			{
				$item_totals[$row_i["order_item_item"]] = $row_i["order_item_quantity"];
			}

			if(array_key_exists($row_i["order_item_item"], $country_totals))
			{
				$country_totals[$row_i["order_item_item"]] = $country_totals[$row_i["order_item_item"]] + $row_i["order_item_quantity"];
			}
			else
			{
				$country_totals[$row_i["order_item_item"]] = $row_i["order_item_quantity"];
			}

		}
		$cell_index++;
	}

	foreach($colwidth as $col=>$width) {
		$sheet2->getStyle($col . $row_index)->applyFromArray($style_normal_border);
	}
	
	
	$row_index++;
}


if(count($orders) > 0)
{
	if($date_base == 2)
	{
		$sheet2->setCellValueByColumnAndRow(4, $row_index, "Total picked up to " . $old_country);
	}
	else
	{
		$sheet2->setCellValueByColumnAndRow(4, $row_index, "Total delivered to " . $old_country);
	}

	$cell_index = 6;
	foreach($items as $item_id=>$item_code)
	{
		if(array_key_exists($item_id, $country_totals))
		{
			$sheet2->setCellValueByColumnAndRow($cell_index, $row_index, $country_totals[$item_id]);

			if(array_key_exists($old_country, $totals_delivered_to_country))
			{
				if(!array_key_exists($item_id, $totals_delivered_to_country[$old_country]))
				{
					$totals_delivered_to_country[$old_country][$item_id] = $country_totals[$item_id];
				}
				else
				{
					$totals_delivered_to_country[$old_country][$item_id] = $totals_delivered_to_country[$old_country][$item_id] + $country_totals[$item_id];
				}
			}
			else
			{
				$totals_delivered_to_country[$old_country][$item_id] = $country_totals[$item_id];
			}
		}
		
		$cell_index++;
	}
	$row_index = $row_index + 2;
}



if(count($orders) > 0)
{
	//print totals
	$cell_index = 6;
	$row_index++;
	$sheet2->setCellValueByColumnAndRow(1, $row_index, "List Total");
	foreach($items as $item_id=>$item)
	{
		if(array_key_exists($item_id, $item_totals))
		{
			$sheet2->setCellValueByColumnAndRow($cell_index, $row_index, $item_totals[$item_id]);
		}
		$cell_index++;

	}
}
//Format column heights and witdhs
foreach($colwidth as $col=>$width) {
	$sheet2->getColumnDimension($col)->setWidth($width);
}



//TAB 3: Country Overview
$logo3 = new PHPExcel_Worksheet_Drawing();
$logo3->setName('Logo');
$logo3->setDescription('Logo');
$logo3->setPath('../pictures/brand_logo.jpg');
$logo3->setHeight(36);
$logo3->setWidth(113);

$sheet3 = $objPHPExcel->createSheet();
if($date_base == 2)
{
	$sheet3->setTitle('Pickups by Country');
}
else
{
	$sheet3->setTitle('Delivered by Country');
}
$objPHPExcel->setActiveSheetIndex(2);
$logo3->setWorksheet($objPHPExcel->getActiveSheet());

$objPHPExcel->setActiveSheetIndex(2);
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(1);

$captions = array();
$captions['A'] = "Delivery to";


$colwidth = array();
$colwidth['A'] = "5";


$achar = "A";
$colchars = 1;
foreach($items as $item_id=>$item_code)
{
	if(($achar == 'Z' and $colchars == 11) or $colchars == 12) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['K'. $achar] = $item_code;
		$colchars = 12;
		$colwidth['K' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 10) or $colchars == 11) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['J'. $achar] = $item_code;
		$colchars = 11;
		$colwidth['J' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 9) or $colchars == 10) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['I'. $achar] = $item_code;
		$colchars = 10;
		$colwidth['I' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 8) or $colchars == 9) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['H'. $achar] = $item_code;
		$colchars = 9;
		$colwidth['H' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 7) or $colchars == 8) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['G'. $achar] = $item_code;
		$colchars = 8;
		$colwidth['G' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 6) or $colchars == 7) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['F'. $achar] = $item_code;
		$colchars = 7;
		$colwidth['F' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 5) or $colchars == 6) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['E'. $achar] = $item_code;
		$colchars = 6;
		$colwidth['E' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 4) or $colchars == 5) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['D'. $achar] = $item_code;
		$colchars = 5;
		$colwidth['D' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 3) or $colchars == 4) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['C'. $achar] = $item_code;
		$colchars = 4;
		$colwidth['C' . $achar] = "5";
	}
	elseif(($achar == 'Z' and $colchars == 2) or $colchars == 3) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['B'. $achar] = $item_code;
		$colchars = 3;
		$colwidth['B' . $achar] = "5";
	}
	elseif($achar == 'Z' or $colchars == 2) {
		if($achar == 'Z'){
			$achar = 'A';
		}
		else
		{
			$achar = chr(ord($achar) + 1);
		}
		$captions['A'. $achar] = $item_code;
		$colchars = 2;
		$colwidth['A' . $achar] = "5";
	}
	else
	{
		$achar = chr(ord($achar) + 1);
		$captions[$achar] = $item_code;
		$colwidth[$achar] = "5";
	}
}


// HEADRES ROW 1

if($date_base == 2)
{
	$sheet3->setCellValue('D1', 'Pickup of Items by Country (' . date("d.m.Y H:i:s") . ')');
}
else
{
	$sheet3->setCellValue('D1', 'Delivery of Items by Country (' . date("d.m.Y H:i:s") . ')');
}
$sheet3->getStyle('D1')->applyFromArray( $style_title );
$sheet3->getRowDimension('1')->setRowHeight(36);



$row_index = 2;

if($print_query_filter == 1)
{
	foreach($_filter_strings as $key=>$value)
	{
		 $sheet3->setCellValueByColumnAndRow(0, $row_index, $key . ": " . $value);
		 $row_index++;
	}
	$row_index++;
}

// HEADRES 
foreach($captions as $col=>$caption){
    $sheet3->setCellValue($col . $row_index, $caption);
	$sheet3->getStyle($col . $row_index)->applyFromArray( $style_header );
	$sheet3->getStyle($col . $row_index)->getAlignment()->setTextRotation(90);
}

$sheet3->getRowDimension($row_index)->setRowHeight(150);



//output items
$row_index++;
$old_country = "";
$country_totals = array();
$list_totals = array();
ksort($totals_delivered_to_country);
foreach($totals_delivered_to_country as $country_name=>$itemtotals)
{
		$cell_index = 0;
		$sheet3->setCellValueByColumnAndRow($cell_index, $row_index, $country_name);
		if($colwidth['A'] < strlen($country_name)){$colwidth['A'] = 2+strlen($country_name);}

		$cell_index++;
		foreach($items as $item_id=>$item_code)
		{
			
			if(array_key_exists($item_id, $itemtotals))
			{
				$sheet3->setCellValueByColumnAndRow($cell_index, $row_index, $itemtotals[$item_id]);

				if(!array_key_exists($item_id, $list_totals))
				{
					$list_totals[$item_id] = $itemtotals[$item_id];
				}
				else
				{
					$list_totals[$item_id] = $list_totals[$item_id] + $itemtotals[$item_id];
				}
			}
			$cell_index++;
		}


		foreach($colwidth as $col=>$width) {
			$sheet3->getStyle($col . $row_index)->applyFromArray($style_normal_border);
		}

		$row_index++;

		
}

$row_index++;
$cell_index = 1;
$sheet3->setCellValueByColumnAndRow(0, $row_index, "List Total");
foreach($items as $item_id=>$item)
{
	if(array_key_exists($item_id, $list_totals))
	{
		$sheet3->setCellValueByColumnAndRow($cell_index, $row_index, $list_totals[$item_id]);
	}
	$cell_index++;

}


//Format column heights and witdhs
foreach($colwidth as $col=>$width) {
	$sheet3->getColumnDimension($col)->setWidth($width);
}



/********************************************************************
    Activate Sheet 1
*********************************************************************/

$objPHPExcel->setActiveSheetIndex(2);
$sheet3->setCellValue('A1', "");
$sheet3->getStyle('A1')->applyFromArray($style_title);
$objPHPExcel->getActiveSheet()->insertNewRowBefore(1, 1);
$sheet3->setCellValue('A1', "");
$sheet3->getStyle('A1')->applyFromArray($style_title);


$objPHPExcel->setActiveSheetIndex(1);
$sheet2->setCellValue('A1', "");
$sheet2->getStyle('A1')->applyFromArray($style_title);
$objPHPExcel->getActiveSheet()->insertNewRowBefore(1, 1);
$sheet2->setCellValue('A1', "");
$sheet2->getStyle('A1')->applyFromArray($style_title);


$objPHPExcel->setActiveSheetIndex(0);
$sheet->setCellValue('A1', "");
$sheet->getStyle('A1')->applyFromArray($style_title);
$objPHPExcel->getActiveSheet()->insertNewRowBefore(1, 1);
$sheet->setCellValue('A1', "");
$sheet->getStyle('A1')->applyFromArray($style_title);


/********************************************************************
    Start output
*********************************************************************/
if($date_base == 2)
{
	$filename = 'pickup_of_items_' . date('Ymd H:i:s') . '.xlsx';
}
else
{
	$filename = 'delivery_of_items_' . date('Ymd H:i:s') . '.xlsx';
}
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
header('Pragma: ');

//send data
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>