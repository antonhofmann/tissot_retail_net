<?php
/********************************************************************

    order_queries_get_query_params.php

    Get Query Params

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2016-08-19
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2016-08-19
    Version:        1.0.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.

*********************************************************************/
$start_time = time();
//check if user has access to the query

$captions = array();
$sql = "";
$outputfields = "";
$order_clause = "";
$total_records = 0;
$total_records_per_group = 0;

$sql = "select * from orderqueries where orderquery_id =" . $query_id;
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$print_query_filter = $row["orderquery_print_filter"];
	$orderquery_field_order = $row["orderquery_field_order"];
	
	$selected_field_order = decode_field_array($orderquery_field_order);
	$orderquery_order = decode_field_array($row["orderquery_order"]);

	$orderquery_area_perception_filter = "";
	$orderquery_area_perception = explode  ("@@" , $row["orderquery_area_perception"]);
	if(array_key_exists(1,$orderquery_area_perception))
	{
		$orderquery_area_perception_filter = $orderquery_area_perception[1];
	}
	

	$db_info_client = query_clientaddress_fields();
	$db_info_orders = query_orders_fields();


	$db_info["fields"] = array_merge($db_info_client["fields"], $db_info_orders["fields"]);

	$db_info["attributes"] = array_merge($db_info_client["attributes"], $db_info_orders["attributes"]);
	
	$db_info["datatypes"] = array_merge($db_info_client["datatypes"], $db_info_orders["datatypes"]);
	
	$db_info["calculated_content"] = array_merge($db_info_client["calculated_content"], $db_info_orders["calculated_content"]);

	$db_info["calculated_content_key"] = array_merge($db_info_client["calculated_content_key"], $db_info_orders["calculated_content_key"]);

	$db_info["calculated_content_field"] = array_merge($db_info_client["calculated_content_field"], $db_info_orders["calculated_content_field"]);

	$db_info["calculated_content_sort_order"] = array_merge($db_info_client["calculated_content_sort_order"], $db_info_orders["calculated_content_sort_order"]);

	$db_info["content_by_function"] = array_merge($db_info_client["content_by_function"], $db_info_orders["content_by_function"]);

	$db_info["content_by_function_params"] = array_merge($db_info_client["content_by_function_params"], $db_info_orders["content_by_function_params"]);

	
	$query_groups = $query_group_fields;
	$query_group01 = $row["orderquery_grouping01"];
	$query_group02 = $row["orderquery_grouping02"];
	$query_group03 = $row["orderquery_grouping03"];
	$show_number_of_orders = $row["orderquery_show_number_of_orders"];

	$general_sortorder = $row["orderquery_order_desc"];

	$query_filter = unserialize($row["orderquery_filter"]);

			
	$functionfields = $query_function_fields;
	$sum_fields = unserialize($row["orderquery_sum_fields"]);
	$avg_fields = unserialize($row["orderquery_avg_fields"]);



	if($sum_fields == false)
	{
		$sum_fields = array();
	}

	if($avg_fields == false)
	{
		$avg_fields = array();
	}
	
	$sum_field_names = array();
	foreach($sum_fields as $key=>$fieldindex)
	{
		$sum_field_names[] = str_replace(".", "", $functionfields[$fieldindex]);
	}

	$avg_field_names = array();
	foreach($avg_fields as $key=>$fieldindex)
	{
		$avg_field_names[] = str_replace(".", "", $functionfields[$fieldindex]);
	}


	
	//integrity check
	//check if sumupfields or avg fields are present in the field list
	foreach($sum_fields as $key=>$index)
	{
		if(!in_array($functionfields[$index], $selected_field_order))
		{
			unset($sum_fields[$key]);
		}
	}
	foreach($avg_fields as $key=>$index)
	{
		if(!in_array($functionfields[$index], $selected_field_order))
		{
			unset($avg_fields[$key]);
		}
	}


	$use_zebra = $row["orderquery_zebra"];
	$zebra_color = array(255, 255, 255);
	if($use_zebra == 1 and $row["orderquery_zebra_color"])
	{
		$zebra_color = hex2rgb($row["orderquery_zebra_color"]);
	}



	$totalisation_fields = array();
	$totalisation_fields_format = array();
	$totalisation_fields_column_index = array();

	if($sum_fields)
	{
		foreach($sum_fields as $key=>$index)
		{
			if(in_array($functionfields[$index], $db_info["calculated_content_field"]))
			{
				$new_field_name = str_replace(".", "", $functionfields[$index]);
				$totalisation_fields[$new_field_name] = "0";
				$totalisation_fields_column_index[$new_field_name] = "0";
			}
			elseif(array_key_exists($functionfields[$index], $db_info["content_by_function"]))
			{
				$new_field_name = str_replace(".", "", $functionfields[$index]);
				$totalisation_fields[$new_field_name] = "0";
				$totalisation_fields_column_index[$new_field_name] = "0";
			}
			elseif(in_array($functionfields[$index], $selected_field_order))
			{
				$new_field_name = str_replace(".", "", $db_info["attributes"][$functionfields[$index]]);
				$totalisation_fields[$new_field_name] = "0";
				$totalisation_fields_column_index[$new_field_name] = "0";
				
			}
			$totalisation_fields_format[$new_field_name] = $db_info["datatypes"][$functionfields[$index]];
			
		}
	}
	
	if($avg_fields)
	{
		foreach($avg_fields as $key=>$index)
		{
			if(in_array($functionfields[$index], $db_info["calculated_content_field"]))
			{
				$new_field_name = str_replace(".", "", $functionfields[$index]);
				$totalisation_fields[$new_field_name] = "0";
				$totalisation_fields_column_index[$new_field_name] = "0";
			}
			elseif(array_key_exists($functionfields[$index], $db_info["content_by_function"]))
			{
				$new_field_name = str_replace(".", "", $functionfields[$index]);
				$totalisation_fields[$new_field_name] = "0";
				$totalisation_fields_column_index[$new_field_name] = "0";
			}
			elseif(in_array($functionfields[$index], $selected_field_order))
			{
				$new_field_name = str_replace(".", "", $db_info["attributes"][$functionfields[$index]]);
				$totalisation_fields[$new_field_name] = "0";
				$totalisation_fields_column_index[$new_field_name] = "0";
			}
			$totalisation_fields_format[$new_field_name] = $db_info["datatypes"][$functionfields[$index]];
		}
	}
	
	//create captions and fields for the sql
	

	
	foreach($selected_field_order as $key=>$field)
	{
		
		if($db_info["attributes"][$field] == "calculated_content")
		{
			$captions[] = str_replace('---- ', "", $db_info["fields"][$field]);
			$outputfields .= $db_info["calculated_content_key"][$field] . ", ";

		}
		elseif($db_info["attributes"][$field] == "content_by_function")
		{
			$captions[] = str_replace('---- ', "", $db_info["fields"][$field]);
		}
		elseif($db_info["attributes"][$field] == "subgroup_category_order_state")
		{
			foreach($subgroup_category_order_state_captions as $key=>$value)
			{
				$captions[] = $value;
			}
		}
		elseif($db_info["attributes"][$field] == "subgroup_category_delivery_state")
		{
			foreach($subgroup_category_delivery_state_captions as $key=>$value)
			{
				$captions[] = $value;
			}
		}
		elseif($db_info["attributes"][$field] == "content_by_subquery")
		{
			$captions[] = str_replace('---- ', "", $db_info["fields"][$field]);

			$subquery = "(" . $db_info["calculated_content"][$field] . ") as " . $field;
			$outputfields .= $subquery . ", ";
		}
		else
		{
			$captions[] = str_replace('---- ', "", $db_info["fields"][$field]);
			//$outputfields .= $db_info["attributes"][$field] . " as " . str_replace(".", "", $db_info["attributes"][$field]) . ", ";

			if(strpos($db_info["attributes"][$field], "IF(") === 0)
			{
				$outputfields .= $db_info["attributes"][$field] . ", ";
			}
			else
			{
				$outputfields .= $db_info["attributes"][$field] . " as " . str_replace(".", "", $db_info["attributes"][$field]) . ", ";
			}

			//echo $db_info["attributes"][$field] . " " . str_replace(".", "", $db_info["attributes"][$field]) . "<br />";
		}
	}

	$outputfields = substr($outputfields, 0, strlen($outputfields)-2);
	
	
	//create filter
		
	$filter = " where orders.order_type = 2"; //only orders
	$filter_clienttypes = "";
	$filter_salesregions = "";
	$filter_regions = "";
	$filter_countries = "";
	$filter_cities = "";
	$filter_project_costtypes = "";
	$filter_project_kinds = "";
	$filter_pos_types = "";
	$filter_item_categories = "";


	
	if($query_filter["cl"] > 0) // client address
	{
		$filter .= " and orders.order_client_address = " . $query_filter["cl"] . " ";
	}

	$order_item_filter = "";
	if($query_filter["supp"] > 0) // supplier address
	{
		$filter .= " and order_items.order_item_supplier_address = " . $query_filter["supp"] . " ";

		$order_item_filter .= " and order_items.order_item_supplier_address = " . $query_filter["supp"] . " ";
	}

	if($query_filter["forw"] > 0) // forwarder address
	{
		$filter .= " and order_items.order_item_forwarder_address = " . $query_filter["forw"] . " ";
		$order_item_filter .= " and order_items.order_item_forwarder_address = " . $query_filter["forw"] . " ";
	}


	
	if($query_filter["sufrom"] != '') // submited from
	{
		$filter .= " and orders.order_date >= '" . $query_filter["sufrom"] . "' ";
	}

	if($query_filter["suto"] != '') // submitted to
	{
		$filter .= " and orders.order_date <= '" . $query_filter["suto"] . "' ";
	}
	
		
	if($query_filter["ct"]) //client types
	{
		$tmp = explode("-", $query_filter["ct"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_clienttypes .= $value . ",";
			}
		}
		$filter_clienttypes = "(" . substr($filter_clienttypes, 0, strlen($filter_clienttypes)-1) . ")";
		if($filter_regions != "()")
		{
			
			$filter .= " and clients.address_client_type in " . $filter_clienttypes . " ";
			
			
		}
	}
		
	if($query_filter["re"]) //Geographical region
	{
		$tmp = explode("-", $query_filter["re"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_salesregions .= $value . ",";
			}
		}
		$filter_salesregions = "(" . substr($filter_salesregions, 0, strlen($filter_salesregions)-1) . ")";
		if($filter_regions != "()")
		{
			
			$filter .= " and countries.country_salesregion in " . $filter_salesregions . " ";
			
			
		}
	}

	if($query_filter["gr"]) //Supplied region
	{
		$tmp = explode("-", $query_filter["gr"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_regions .= $value . ",";
			}
		}
		$filter_regions = "(" . substr($filter_regions, 0, strlen($filter_regions)-1) . ")";
		if($filter_regions != "()")
		{
			$filter .= " and countries.country_region in " . $filter_regions . " ";
		}
	}
	
	
	if($query_filter["co"]) // country
	{
		$tmp = explode("-", $query_filter["co"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_countries .= $value . ",";
			}
		}
		$filter_countries = "(" . substr($filter_countries, 0, strlen($filter_countries)-1) . ")";
		if($filter_countries != "()")
		{
			$filter .= " and projectorders.order_shop_address_country in " . $filter_countries . " ";
		}
	}

		
	if($query_filter["pct"]) //cost type, legal type
	{
		$tmp = explode("-", $query_filter["pct"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_project_costtypes .= $value . ",";
			}
		}
		$filter_project_costtypes = "(" . substr($filter_project_costtypes, 0, strlen($filter_project_costtypes)-1) . ")";
		if($filter_project_costtypes != "()")
		{
			$filter .= " and project_cost_type in " . $filter_project_costtypes . " ";
		}
	}


	//project Types
	$posaddress_ids_with_project_kinds_indicated = array();
	if($query_filter["pk"]) // project Type
	{
		$tmp = explode("-", $query_filter["pk"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_project_kinds .= $value . ",";
			}
		}

				
		$filter_project_kinds = "(" . substr($filter_project_kinds, 0, strlen($filter_project_kinds)-1) . ")";
		if($filter_project_kinds != "()")
		{
			$filter .= " and project_projectkind in " . $filter_project_kinds;
		}
	}


	
	if($query_filter["pt"]) // pos type
	{
		$tmp = explode("-", $query_filter["pt"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_pos_types .= $value . ",";
			}
		}
		$filter_pos_types = "(" . substr($filter_pos_types, 0, strlen($filter_pos_types)-1) . ")";
		if($filter_pos_types != "()")
		{
			$filter .= " and project_postype in " . $filter_pos_types . " ";
		}
	}

	
	
	//item categories
	$project_with_item_category_inicated = array();
	if($query_filter["icat"]) //Cost Monitoring Groups
	{
		
		$tmp = explode("-", $query_filter["icat"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_item_categories .= $value . ",";
			}
		}
		
		$filter_item_categories = "(" . substr($filter_item_categories, 0, strlen($filter_item_categories)-1) . ")";
		if($filter_item_categories != "()")
		{
		
			$sql_p = "select DISTINCT order_item_order " .
					 "from order_items " .
				     " left join items on item_id = order_item_item " . 
					 "where item_category IN " . $filter_item_categories; 
			
			$res_p = mysql_query($sql_p) or dberror($sql_p);
			while ($row_p = mysql_fetch_assoc($res_p))
			{
				$project_with_item_category_inicated[$row_p["order_item_order"]] = $row_p["order_item_order"];
			}
		}

		if(count($project_with_item_category_inicated) > 0)
		{
			$filter .= " and orders.order_id IN (" . implode(",", $project_with_item_category_inicated) . ") ";
		}

	}

	
	// COMPOSE SQL
	if(strpos($outputfields, "order_items.") === 0) {
		$outputfields = " orders.order_id, project_id, " . $outputfields;
	}
	else {
		$outputfields = " orders.order_id, project_id, order_items.order_item_id, " . $outputfields;
	}
	
	
	$sql = "select DISTINCT " . $outputfields . " from orders ";
	
	$sql_join = "";
	$sql_join .= "INNER join order_item_replacements as order_item_replacements on order_item_replacements.order_item_replacement_catalog_order_id = orders.order_id ";



	$sql_join .= "INNER JOIN order_items AS repleaced_items ON repleaced_items.order_item_id = order_item_replacements.order_item_replacement_order_item_id ";
	
	$sql_join .= "LEFT JOIN order_item_replacement_reasons ON order_item_replacement_reasons.order_item_replacement_reason_id = order_item_replacements.order_item_replacement_reason_id ";

	$sql_join .= "LEFT JOIN replacements_payer_names as replacements_payer_names ON replacements_payer_names.replacements_payer_name_id = order_item_replacements.order_item_replacement_item_to_be_payed_by ";

	$sql_join .= "LEFT JOIN replacements_payer_names as replacements_payer_names2 ON replacements_payer_names2.replacements_payer_name_id = order_item_replacements.order_item_replacement_ferigeht_to_be_payed_by ";

	$sql_join .= "LEFT JOIN order_item_warranty_types ON order_item_warranty_types.order_item_warranty_type_id = order_item_replacements.order_item_replacement_warranty_type_id ";
	
		
	
	//projects
	$sql_join .= "left join projects as projects on projects.project_order = order_item_replacements.order_item_replacement_order_id ";
	$sql_join .= "LEFT JOIN orders as projectorders ON projects.project_order = projectorders.order_id "; 
	$sql_join .= "left join project_costs as project_costs on project_costs.project_cost_order = projects.project_order ";
	
	
	$sql_join .= "left join postypes as postypes on postypes.postype_id = projects.project_postype ";
	
	$sql_join .= "left join project_costtypes as project_costtypes on project_costtypes.project_costtype_id = project_costs.project_cost_type ";

	$sql_join .= "left join projectkinds as projectkinds on projectkinds.projectkind_id = projects.project_projectkind ";
	
	$sql_join .= "left join product_lines as product_lines on product_lines.product_line_id = projects.project_product_line ";
	$sql_join .= "left join productline_subclasses as productline_subclasses on productline_subclasses.productline_subclass_id = projects.project_product_line_subclass ";


	$sql_join .= "left join countries as countries on countries.country_id = projectorders.order_shop_address_country ";
	$sql_join .= "left join salesregions as salesregions on salesregion_id = countries.country_salesregion ";
	$sql_join .= "left join regions on region_id = countries.country_region ";
	
	
	//client
	$sql_join .= "left join addresses as clients on clients.address_id = orders.order_client_address ";
	$sql_join .= "left join client_types as clienttypes on clienttypes.client_type_id = clients.address_client_type ";
	$sql_join .= "left join countries as clientcountries on clientcountries.country_id = clients.address_country ";


	//order_items

	$sql_join .= "left JOIN order_items ON order_items.order_item_order = orders.order_id ";
	$sql_join .= "left join items on item_id = order_items.order_item_item ";

	$sql_join .= "left join addresses as suppliers on suppliers.address_id = order_items.order_item_supplier_address ";
	$sql_join .= "left join addresses as forwarders on forwarders.address_id = order_items.order_item_forwarder_address ";

	
	
	
	$sql_tmp = $sql;
	$sql .= $sql_join . " " . $filter;

	

	//create captions and fields for the sql
	$query_sortorder = array();
	foreach($orderquery_order as $key=>$value)
	{
		if($db_info["attributes"][$value] == 'calculated_content')
		{

		}
		elseif($db_info["attributes"][$value] == 'content_by_function')
		{

		}
		elseif($db_info["attributes"][$value] == 'content_by_subquery')
		{
			$query_sortorder[] = $value;
		}
		else
		{
			if(strpos($db_info["attributes"][$value], "IF(") === 0)
			{
				$tmp = explode(' AS ', $db_info["attributes"][$value]);
				$query_sortorder[] = $tmp[1];
			}
			else
			{
				$query_sortorder[] = $db_info["attributes"][$value];
			}
		}
	}

	
	$order_clause = implode(',', $query_sortorder );

	

	
	//add grouping as primary sort order
	if($query_group03 != NULL and $query_group03 != '')
	{
		
		if($db_info["attributes"][$query_groups[$query_group03]] == "calculated_content")
		{
			$tmp = explode('.', $query_groups[$query_group03]);
			$field_name = $tmp[1];
			$new_order_field =  $tmp[1] . ", ";
			$order_clause = str_replace($new_order_field, "", $order_clause);
			$order_clause = $tmp[1] . ", " . $order_clause;
		}
		elseif(strpos($db_info["attributes"][$query_groups[$query_group03]], "IF(") === 0)
		{
			$tmp = explode(' AS ', $db_info["attributes"][$query_groups[$query_group03]]);
			$field_name = $tmp[1];

			$new_order_field =  $tmp[1] . ", ";
			$order_clause = str_replace($new_order_field, "", $order_clause);
			$order_clause = $tmp[1] . ", " . $order_clause;
		}
		else
		{
			$new_order_field = $db_info["attributes"][$query_groups[$query_group03]] . ", ";
			$order_clause = str_replace($new_order_field, "", $order_clause);
			$order_clause = $db_info["attributes"][$query_groups[$query_group03]] . ", " . $order_clause;
		}

		
		

		//move grouping field to first position of output fields
		$old_position = array_search  ( $query_groups[$query_group03]  , $selected_field_order );
		if($old_position > 0)
		{
			$group_field_caption = $captions[$old_position];
			for($i=$old_position;$i>0;$i--)
			{
				$selected_field_order[$i] = $selected_field_order[$i-1];
				$captions[$i] = $captions[$i-1];
			}
			$selected_field_order[0] = $query_groups[$query_group03];
			$captions[0] = $group_field_caption;
		}
		if(substr($order_clause, strlen($order_clause)-2, 2) == ", ")
		{
			$order_clause = substr($order_clause, 0, strlen($order_clause)-2);
		}
	}
	if($query_group02 != NULL and $query_group02 != '')
	{
		if($db_info["attributes"][$query_groups[$query_group02]] == "calculated_content")
		{
			$tmp = explode('.', $query_groups[$query_group02]);
			$field_name = $tmp[1];
			$new_order_field =  $tmp[1] . ", ";
			$order_clause = str_replace($new_order_field, "", $order_clause);
			$order_clause = $tmp[1] . ", " . $order_clause;
		}
		elseif(strpos($db_info["attributes"][$query_groups[$query_group02]], "IF(") === 0)
		{
			$tmp = explode(' AS ', $db_info["attributes"][$query_groups[$query_group02]]);
			$field_name = $tmp[1];

			$new_order_field =  $tmp[1] . ", ";
			$order_clause = str_replace($new_order_field, "", $order_clause);
			$order_clause = $tmp[1] . ", " . $order_clause;
		}
		else
		{
			$new_order_field = $db_info["attributes"][$query_groups[$query_group02]] . ", ";
			$order_clause = str_replace($new_order_field, "", $order_clause);
			$order_clause = $db_info["attributes"][$query_groups[$query_group02]] . ", " . $order_clause;
		}
		

		//move grouping field to first position of output fields
		$old_position = array_search  ( $query_groups[$query_group02]  , $selected_field_order );
		if($old_position > 0)
		{
			$group_field_caption = $captions[$old_position];
			for($i=$old_position;$i>0;$i--)
			{
				$selected_field_order[$i] = $selected_field_order[$i-1];
				$captions[$i] = $captions[$i-1];
			}
			$selected_field_order[0] = $query_groups[$query_group02];
			$captions[0] = $group_field_caption;
		}
		if(substr($order_clause, strlen($order_clause)-2, 2) == ", ")
		{
			$order_clause = substr($order_clause, 0, strlen($order_clause)-2);
		}
	}
	if($query_group01 != NULL and $query_group01 != '')
	{

		if($db_info["attributes"][$query_groups[$query_group01]] == "calculated_content")
		{
			$tmp = explode('.', $query_groups[$query_group01]);
			$field_name = $tmp[1];
			$new_order_field =  $tmp[1] . ", ";
			$order_clause = str_replace($new_order_field, "", $order_clause);
			$order_clause = $tmp[1] . ", " . $order_clause;
		}
		elseif(strpos($db_info["attributes"][$query_groups[$query_group01]], "IF(") === 0)
		{
			$tmp = explode(' AS ', $db_info["attributes"][$query_groups[$query_group01]]);
			$field_name = $tmp[1];

			$new_order_field =  $tmp[1] . ", ";
			$order_clause = str_replace($new_order_field, "", $order_clause);
			$order_clause = $tmp[1] . ", " . $order_clause;
		}
		else
		{
			$new_order_field = $db_info["attributes"][$query_groups[$query_group01]] . ", ";
			$order_clause = str_replace($new_order_field, "", $order_clause);
			$order_clause = $db_info["attributes"][$query_groups[$query_group01]] . ", " . $order_clause;
		}

		//move grouping field to first position of output fields
		$old_position = array_search  ( $query_groups[$query_group01]  , $selected_field_order );
		if($old_position > 0)
		{
			$group_field_caption = $captions[$old_position];
			for($i=$old_position;$i>0;$i--)
			{
				$selected_field_order[$i] = $selected_field_order[$i-1];
				$captions[$i] = $captions[$i-1];
			}
			$selected_field_order[0] = $query_groups[$query_group01];
			$captions[0] = $group_field_caption;
		}

		if(substr($order_clause, strlen($order_clause)-2, 2) == ", ")
		{
			$order_clause = substr($order_clause, 0, strlen($order_clause)-2);
		}

	}


	if($general_sortorder == 1)
	{
		$order_clause = str_replace(', ', ',', $order_clause);
		$order_clause = str_replace(',', ' DESC,', $order_clause) . ' DESC';
	}

		
	
	if($order_clause)
	{
		$sql .= " order by " . $order_clause;
	}



	
	
	//echo '<pre>';



	//echo $order_clause . "/";cde();
	
	//echo date("Y-m-d H:i:s") . "<br />";
	
	//echo $sql;die;

	//var_dump($query_filter);
	//abc();

	//var_dump($selected_field_order);
	//abc();


}
else
{

}

?>