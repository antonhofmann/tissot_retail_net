<?php
/********************************************************************

    project_queries_query_field_order.php

    Field Order for the Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2014-09-14
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2014-09-14
    Version:        1.0.0

    Copyright (c) 201,4 Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if(!has_access("can_perform_all_queries") 
  and !has_access("can_perform_projects_query"))
{
	redirect("noaccess.php");
}
require_once "include/projects_query_check_access.php";

require_once "include/projects_query_get_functions.php";
require_once "project_queries_get_query_params.php";

/********************************************************************
    prepare all data needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("project_queries.php");
}

$query_id = param("query_id");

$projectquery = get_query_name($query_id);
$icon = "../pictures/add_item.gif";
$link = "javascript:open_selector('')";


//get Query Fields
$fields = array();
$selected_field_order = array();

$sql = "select projectquery_fields, projectquery_field_order from projectqueries " .
	   "where projectquery_id = " . $query_id;

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$tmp = unserialize($row["projectquery_fields"]);


	
	foreach($tmp as $key=>$field_array)
	{
		foreach($field_array as $field=>$caption)
		{
			$fields[$field] = $caption;
		}
	}
	
	

	$fields_order = $row["projectquery_field_order"];
	$fields_order_initial_value = $row["projectquery_field_order"];

}


if($fields_order)
{
	$fields_order = str_replace("selected_field_order[]=", "", $fields_order);
	$selected_field_order = explode("&", $fields_order);

	$table = "";
	$table .= '<table id="selected_field_order" cellspacing="0" cellpadding="2">';
	foreach($selected_field_order as $key=>$field)
	{

		if(array_key_exists($field,$fields)
			)
		{
			$table .= '<tr id="' . $field . '"><td>' . str_replace('---- ', '', $fields[$field]) . '</td></tr>';
		}
	}
	$table .= '</table>';
}
elseif($selected_field_order)
{
	$selected_field_order = $fields;
	$table = "";
	$table .= '<table id="selected_field_order" cellspacing="0" cellpadding="2">';
	foreach($selected_field_order as $key=>$caption)
	{
		if(array_key_exists($key,$fields)
			and $db_info["attributes"][$field] != "item_quantity_block")
		{
			$table .= '<tr id="' . $key . '"><td>' . str_replace('---- ', '', $fields[$field]) . '</td></tr>';
		}
	}
	$table .= '</table>';
}
else
{
	$table = "";
}



/********************************************************************
    create form
*********************************************************************/

$form = new Form("projectqueries", "query_generator");

$form->add_hidden("query_id", param("query_id"));
$form->add_hidden("form_save", 1);
$form->add_hidden("projectquery_field_order", $fields_order_initial_value);

$form->add_section($projectquery["name"]);

$form->add_section(" ");
$form->add_comment("Please set the output order of the fields just by dragging and dropping the lines in the following list of selected fields.");

$form->add_table($table);

$form->add_button("submit", "Save Field Order", 0);
if(check_if_query_has_fields($query_id) == true)
{
	$form->add_button("execute", "Execute Query");
}
$form->add_button("back", "Back to the List of Queries");


/********************************************************************
    process form
*********************************************************************/
$form->populate();
$form->process();

if($form->button("back"))
{
	redirect("project_queries.php");
}
elseif($form->button("submit"))
{
	
		
	$sql = "Update projectqueries SET " . 
		   "projectquery_field_order = " . dbquote($form->value("projectquery_field_order")) . ", " . 
		   "date_modified = " . dbquote(date("Y-m-d H:s:i")) . ", " . 
		    "user_modified = " . dbquote(user_login()) .
		   " where projectquery_id = " . param("query_id");

	$result = mysql_query($sql) or dberror($sql);

	redirect("project_queries_query_field_order.php?query_id=" . param("query_id"));
}
elseif($form->button("execute"))
{
	redirect("project_queries_query_xls.php?query_id=" . param("query_id"));
}


/********************************************************************
    render
*********************************************************************/

$page = new Page("queries");

require_once("include/mis_page_actions.php");


$page->header();
$page->title("Edit Project Query - Field Order");

require_once("include/projects_query_tabs.php");

$form->render();

?>

<script type="text/javascript">
$(document).ready(function() {
    // Initialise the table
    $("#selected_field_order").tableDnD();

	$('#selected_field_order').tableDnD({
        onDragClass: "myDragClass",
		onDrop: function(table, row) {
			document.forms[0].projectquery_field_order.value = $.tableDnD.serialize();
        }
    });
});
</script>

<?php

$page->footer();

?>


