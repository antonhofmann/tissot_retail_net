<?php
/********************************************************************

    projects_query_40_xls.php

    Generate Excel File for Shipments Forwarders

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2017-01-23
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2017-01-23
    Version:        1.0.0

    Copyright (c) 2017, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_queries");

require_once "../include/xls/Writer.php";
require_once("projects_query_filter_strings.php");

/********************************************************************
    prepare Data Needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("projects_queries.php");
}


$months = array();
$months[1] = "Jan";
$months[2] = "Feb";
$months[3] = "Mar";
$months[4] = "Apr";
$months[5] = "Mai";
$months[6] = "Jun";
$months[7] = "Jul";
$months[8] = "Aug";
$months[9] = "Sep";
$months[10] = "Okt";
$months[11] = "Nov";
$months[12] = "Dez";


//check if filter is present
$query_name = "";
$print_query_filter = 0;
$sql = "select mis_query_name, mis_query_filter, mis_print_filter from mis_queries " .
	   "where mis_query_id = " . param("query_id");

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$query_name = $row["mis_query_name"];
	if($row["mis_print_filter"] == 1)
	{
		$print_query_filter = 1;
	}

	$filters = array();
	$filters = unserialize($row["mis_query_filter"]);

	foreach($filters as $key=>$value)
	{
		$supp = $filters["supp"]; // Suppliers
		$forw = $filters["forw"]; // Forwarders
		$clt = $filters["clt"]; // Client Types
		$gr = $filters["gr"]; // Geografical Regions
		$re = $filters["re"]; // Supplied Regions
		$cnt = $filters["co"]; // Countries


		$fdy = $filters["fdy"]; // Delievery Year From
		$fdm = $filters["fdm"]; // Delievery Month From
		$tdy = $filters["tdy"]; // Delievery Year To
		$tdm = $filters["tdm"]; // Delievery Month To

		$dba = $filters["dba"]; // Data base

	}
}
else
{
	redirect("projects_queries.php");
}

$header = $query_name;
$header = "Shipments by Forwarders: " . $header . " (" . date("d.m.Y G:i") . ")";
if($dba == 2) {
	$header = "Orders to Suppliers by Forwarders: " . $header . " (" . date("d.m.Y G:i") . ")";
}


$data_types = array();
$data_types[1] = "Shipments";
if($dba == 2) {
	$data_types[1] = "Orders";	
}
$data_types[2] = "Weights";
$data_types[3] = "Volumes";


/********************************************************************
    prepare Data
*********************************************************************/
$filter = "";

$supp = substr($supp,0, strlen($supp)-1); // remove last comma
$supp = str_replace("-", ",", $supp);
if($supp) // Suppliers
{
    $filter =  " (order_item_supplier_address IN (" . $supp . "))";
	$_filter_strings["supp"] = get_filter_string("supp", $supp);
}

$forw = substr($forw,0, strlen($forw)-1); // remove last comma
$forw = str_replace("-", ",", $forw);
if($forw) // Forwarders
{
    if($filter)
	{
		$filter .=  " and (order_item_forwarder_address IN (" . $forw . "))";
	}
	else
	{
		$filter =  " (order_item_forwarder_address IN (" . $forw . "))";
	}
	$_filter_strings["forw"] = get_filter_string("forw", $forw);
}

$clt = substr($clt,0, strlen($clt)-1); // remove last comma
$clt = str_replace("-", ",", $clt);
if($clt) // client type
{
    if($filter)
	{
		$filter .=  " and (clients.address_client_type IN (" . $clt . "))";
	}
	else
	{
		$filter =  "  (clients.address_client_type IN (" . $clt . "))";
	}
	$_filter_strings["Client Types"] = get_filter_string("clt", $clt);
}

$gr = substr($gr,0, strlen($gr)-1); // remove last comma
$gr = str_replace("-", ",", $gr);
$re = substr($re,0, strlen($re)-1); // remove last comma
$re = str_replace("-", ",", $re);

if($filter) {

	if($gr and $re) 
	{
		$filter .=  " and (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	}
	elseif($gr) // geografical regions
	{
		$filter .=  " and (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	}
	elseif($re) //supplying
	{
		$filter .=  " and (country_region IN (" . $re . "))";
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	}
}
else
{
	if($gr and $re) 
	{
		$filter .=  " (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	}
	elseif($gr) // geografical regions
	{
		$filter .=  " (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	}
	elseif($re) //supplying
	{
		$filter .=  " (country_region IN (" . $re . "))";
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	}
}

$cnt = substr($cnt,0, strlen($cnt)-1); // remove last comma
$cnt = str_replace("-", ",", $cnt);
if($cnt and $filter) // country
{
    $filter .=  " and (order_address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);

}
elseif($cnt)
{
    $filter =  " (order_address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);
}

$date_field = "order_item_arrival";
$date_string = "Delivered";
if($dba == 2) {
	$date_field = "order_item_ordered";
	$date_string = "Ordered";
}

if($filter) // delivery from
{
	if($fdy > 0 and $fdm > 0)
	{
		$tmp = "" . $fdy . "" . $fdm;
		$filter.=  " and concat(YEAR(" . $date_field . "), DATE_FORMAT(" . $date_field . ",'%m')) >= " . $tmp;
		$_filter_strings[$date_string . " from"] = $fdy . "-" . $fdm;
		$filter_for_preferred_arrival_is_set = true;
	}
	elseif($fdy > 0)
	{
		$filter.=  " and YEAR(" . $date_field . ") >= " . $fdy;
		$_filter_strings[$date_string . " from"] = $fdy;
		$filter_for_preferred_arrival_is_set = true;
	}
	elseif($fdm > 0)
	{
		$filter.=  " and MONTH(" . $date_field . ") >= " . $fdm;
		$_filter_strings[$date_string . " from"] = $fdm;
		$filter_for_preferred_arrival_is_set = true;
	}
}
else
{
	if($fdy > 0 and $fdm > 0)
	{
		$tmp = "" . $fdy . "" . $fdm;
		$filter.=  " concat(YEAR(" . $date_field . "), DATE_FORMAT(" . $date_field . ",'%m')) >= " . $tmp;
		$_filter_strings[$date_string . " from"] = $fdy . "-" . $fdm;;
	}
	elseif($fdy > 0)
	{
		$filter.=  " YEAR(" . $date_field . ") >= " . $fdy;
		$_filter_strings[$date_string . " from"] = $fdy;
	}
	elseif($fdm > 0)
	{
		$filter.=  " MONTH(" . $date_field . ") >= " . $fdm;
		$_filter_strings[$date_string . " from"] = $fdm;
	}
}



if($filter) // delivery to
{
	if($tdy > 0 and $tdm > 0)
	{
		$tmp = "" . $tdy . "" . $tdm;
		$filter.=  " and concat(YEAR(" . $date_field . "), DATE_FORMAT(" . $date_field . ",'%m')) <= " . $tmp;
		$_filter_strings[$date_string . " to"] = $tdy . "-" . $tdm;
	}
	elseif($tdy > 0)
	{
		$filter.=  " and YEAR(" . $date_field . ") <= " . $tdy;
		$_filter_strings[$date_string . " to"] = $tdy;
	}
	elseif($tdm > 0)
	{
		$filter.=  " and MONTH(" . $date_field . ") <= " . $tdm;
		$_filter_strings[$date_string . " to"] = $tdm;
	}
}
else
{
	if($tdy > 0 and $tdm > 0)
	{
		$tmp = "" . $tdy . "" . $tdm;
		$filter.=  "  concat(YEAR(" . $date_field . "), DATE_FORMAT(" . $date_field . ",'%m')) <= " . $tmp;
		$_filter_strings[$date_string . " to"] = $tdy . "-" . $tdm;
	}
	elseif($tdy > 0)
	{
		$filter.=  "  YEAR(" . $date_field . ") <= " . $tdy;
		$_filter_strings[$date_string . " to"] = $tdy;
	}
	elseif($tdm > 0)
	{
		$filter.=  "  MONTH(" . $date_field . ") <= " . $tdm;
		$_filter_strings[$date_string . " to"] = $tdm;
	}
}

if($dba == 2) {
	
	if($filter) {
		$filter .= " and (order_archive_date is null or order_archive_date = '0000-00-00') ";
	}
	else {
		$filter .= " (order_archive_date is null or order_archive_date = '0000-00-00') ";
	}
}

$filter.= " and order_item_type < 3 ";
$filter.= " and order_address_type = 2 ";
$filter.= " and order_item_forwarder_address > 0 ";

//prepare all data by supplier and item and month
$forwarders = array();
$forwarder_shippings = array();
$forwarder_weights = array();
$forwarder_volumes = array();

$region_shippings = array();
$region_weights = array();
$region_volumes = array();

$country_shippings = array();
$country_weights = array();
$country_volumes = array();

$purchases = array();


$first_year = 9999;
$last_year = 0;

$sql_d = "SELECT DISTINCT order_item_forwarder_address, 
			forwarders.address_shortcut as address_shortcut
		  FROM order_items
		  LEFT JOIN addresses AS forwarders ON forwarders.address_id = order_item_forwarder_address
		  left join orders on order_id = order_item_order 
		  left join order_addresses on order_address_order_item = order_item_id 
		  left join countries on country_id = order_address_country 
		  left join regions on region_id = country_region 
		  left join addresses as clients on clients.address_id = order_client_address " .
		 " where " . $filter;


$res = mysql_query($sql_d) or dberror($sql_d);
while ($row = mysql_fetch_assoc($res))
{ 
	
	$forwarders[$row["order_item_forwarder_address"]] = $row["address_shortcut"];
}

natcasesort($forwarders);

/*
echo '<pre>';
print_r($forwarders);
die;
*/

foreach($forwarders as $id=>$company)
{
	$f_filter = $filter . " and order_item_forwarder_address = " . dbquote($id);
	
	/*list 1 forwarer by month*/
	$sql_d = "SELECT count(order_item_id) as num_recs,
	                sum(order_item_quantity*order_item_packaging_number*order_item_packaging_weight_gross) as gross_weight,
					sum(order_item_quantity*order_item_packaging_number*order_item_packaging_length*order_item_packaging_width*order_item_packaging_height/1000000) as volume,
					YEAR (" . $date_field . ") AS dy,
					MONTH (" . $date_field . ") AS dm
				FROM
					order_items
				LEFT JOIN order_addresses ON order_address_order_item = order_item_id
				LEFT JOIN countries ON country_id = order_address_country
				LEFT JOIN regions ON region_id = country_region
				LEFT JOIN orders ON order_id = order_item_order
				LEFT JOIN addresses AS clients ON clients.address_id = order_client_address 
				left join order_item_packaging on order_item_packaging_order_item_id = order_item_id " .
			 " where " . $f_filter .
			 " group by dy, dm";

	$res = mysql_query($sql_d) or dberror($sql_d);
	while ($row = mysql_fetch_assoc($res))
	{ 
		$forwarder_shippings[$row["dy"]][$company][$row["dm"]] = $row["num_recs"];
		$forwarder_weights[$row["dy"]][$company][$row["dm"]] = $row["gross_weight"];
		$forwarder_volumes[$row["dy"]][$company][$row["dm"]] = $row["volume"];
	}

	/*list 2 forwarder by region and month*/
	$sql_d = "SELECT count(order_item_id) as num_recs,
	                sum(order_item_quantity*order_item_packaging_number*order_item_packaging_weight_gross) as gross_weight,
					sum(order_item_quantity*order_item_packaging_number*order_item_packaging_length*order_item_packaging_width*order_item_packaging_height/1000000) as volume,
					YEAR (" . $date_field . ") AS dy,
					region_name, 
					MONTH (" . $date_field . ") AS dm
				FROM
					order_items
				LEFT JOIN order_addresses ON order_address_order_item = order_item_id
				LEFT JOIN countries ON country_id = order_address_country
				LEFT JOIN regions ON region_id = country_region
				LEFT JOIN orders ON order_id = order_item_order
				LEFT JOIN addresses AS clients ON clients.address_id = order_client_address 
				left join order_item_packaging on order_item_packaging_order_item_id = order_item_id " .
			 " where " . $f_filter .
			 " group by dy, region_name, dm";

	$res = mysql_query($sql_d) or dberror($sql_d);
	while ($row = mysql_fetch_assoc($res))
	{ 
		$region_shippings[$row["dy"]][$company . " " . $row["region_name"]][$row["dm"]] = $row["num_recs"];
		$region_weights[$row["dy"]][$company . " " . $row["region_name"]][$row["dm"]] = $row["gross_weight"];
		$region_volumes[$row["dy"]][$company . " " . $row["region_name"]][$row["dm"]] = $row["volume"];
	}


	/*list 3 forwarder by country and month*/
	$sql_d = "SELECT count(order_item_id) as num_recs,
	                sum(order_item_quantity*order_item_packaging_number*order_item_packaging_weight_gross) as gross_weight,
					sum(order_item_quantity*order_item_packaging_number*order_item_packaging_length*order_item_packaging_width*order_item_packaging_height/1000000) as volume,
					YEAR (" . $date_field . ") AS dy,
					country_name, 
					MONTH (" . $date_field . ") AS dm
				FROM
					order_items
				LEFT JOIN order_addresses ON order_address_order_item = order_item_id
				LEFT JOIN countries ON country_id = order_address_country
				LEFT JOIN regions ON region_id = country_region
				LEFT JOIN orders ON order_id = order_item_order
				LEFT JOIN addresses AS clients ON clients.address_id = order_client_address 
				left join order_item_packaging on order_item_packaging_order_item_id = order_item_id " .
			 " where " . $f_filter .
			 " group by dy, country_name, dm";

	$res = mysql_query($sql_d) or dberror($sql_d);
	while ($row = mysql_fetch_assoc($res))
	{ 
		$country_shippings[$row["dy"]][$company . " " . $row["country_name"]][$row["dm"]] = $row["num_recs"];
		$country_weights[$row["dy"]][$company . " " . $row["country_name"]][$row["dm"]] = $row["gross_weight"];
		$country_volumes[$row["dy"]][$company . " " . $row["country_name"]][$row["dm"]] = $row["volume"];
	}


	/*list 4 forwarder purchses and month*/
	$sql_d = "SELECT	sum(order_item_quantity*(order_item_supplier_exchange_rate*order_item_supplier_price/currency_factor)) as amont,
				YEAR (" . $date_field . ") AS dy,
				MONTH (" . $date_field . ") AS dm
			FROM
				order_items
			LEFT JOIN order_addresses ON order_address_order_item = order_item_id
			LEFT JOIN countries ON country_id = order_address_country
			LEFT JOIN regions ON region_id = country_region
			LEFT JOIN addresses AS forwarders ON forwarders.address_id = order_item_forwarder_address
			LEFT JOIN currencies ON currency_id = order_item_supplier_currency
			LEFT JOIN orders ON order_id = order_item_order
			LEFT JOIN addresses AS clients ON clients.address_id = order_client_address " .
			 " where " . $f_filter . 
			" group by dy, dm";

	$res = mysql_query($sql_d) or dberror($sql_d);
	while ($row = mysql_fetch_assoc($res))
	{ 
		$purchases[$row["dy"]][$company][$row["dm"]] = $row["amont"];
	}
}


/*
echo '<pre>';
print_r($purchases);
die;
*/

/********************************************************************
    prepare Excel Sheet
*********************************************************************/

$filename = "shipments_by_forwarders" . date("Ymd") . ".xls";
if($dba == 2) {
	$filename = "forcast_shipments_by_forwarders" . date("Ymd") . ".xls";
}

$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename); 
$xls->setVersion(8);
$sheet =& $xls->addWorksheet("By month");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();


//formats
$header_row =& $xls->addFormat();
$header_row->setSize(10);
$header_row->setAlign('left');
$header_row->setBold();


$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);


$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);


$f_number_bold =& $xls->addFormat();
$f_number_bold->setSize(8);
$f_number_bold->setAlign('right');
$f_number_bold->setBorder(1);
$f_number_bold->setBold();

$f_decimal =& $xls->addFormat();
$f_decimal->setSize(8);
$f_decimal->setAlign('right');
$f_decimal->setBorder(1);
$f_decimal->setNumFormat('0.00');

$f_decimal_bold =& $xls->addFormat();
$f_decimal_bold->setSize(8);
$f_decimal_bold->setAlign('right');
$f_decimal_bold->setBorder(1);
$f_decimal_bold->setNumFormat(43);
$f_decimal_bold->setBold();

$f_date =& $xls->addFormat();
$f_date->setSize(8);
$f_date->setAlign('left');
$f_date->setBorder(1);
$f_date->setNumFormat('DD.MM.YYYY');

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
//$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
//$f_caption->setTextRotation(270);
$f_caption->setTextWrap();



/********************************************************************
   forwarder data by item and month
*********************************************************************/
$sheet->write(0, 0, $header, $header_row);

$row_index = 2;

if($print_query_filter == 1)
{
	foreach($_filter_strings as $key=>$value)
	{
		 $sheet->write($row_index, 0,$key . ": " . $value);
		 $row_index++;
	}
}

$cell_index = 0;

foreach($forwarder_shippings as $year=>$company_data) 
{
	$cell_index = 0;
	$row_index = $row_index+2;
    
	foreach($company_data as $company=>$item_data) {
		
		$forwarder_totals = array();
		$sheet->write($row_index, $cell_index, $company, $f_caption);
		$cell_index++;

		foreach($months as $m=>$mname) {
			$sheet->write($row_index, $cell_index, $year . " " . $mname, $f_caption);
			$cell_index++;
		}

		$sheet->write($row_index, $cell_index, "Total " . $year, $f_caption);
		$cell_index++;

		$row_index++;
		$cell_index = 0;

		
		$item_total = 0;
		$sheet->write($row_index, $cell_index, $data_types[1], $f_normal);
		$cell_index++;

		foreach($months as $m=>$mname) {
			if(array_key_exists($m, $item_data)) {

				$sheet->write($row_index, $cell_index, $item_data[$m], $f_number);
				$cell_index++;

				$sk = $year . "-" . $m;
				if(array_key_exists($sk, $forwarder_totals)) {
					$forwarder_totals[$sk] = $forwarder_totals[$sk] +$item_data[$m];
				}
				else {
					$forwarder_totals[$sk] = $item_data[$m];
				}
				$item_total = $item_total + $item_data[$m];


			}
			else {
				$sheet->write($row_index, $cell_index, '', $f_number);
				$cell_index++;

				$sk = $year . "-" . $m;
				if(!array_key_exists($sk, $forwarder_totals)) {
					$forwarder_totals[$sk] = 0;
				}
			}
		}

		$sheet->write($row_index, $cell_index, $item_total, $f_number);

		
		$row_index++;
		$cell_index = 0;
		

		//volumes
		$item_total = 0;
		$sheet->write($row_index, $cell_index, $data_types[3], $f_normal);
		$cell_index++;

		$item_data = $forwarder_volumes[$year][$company];

		foreach($months as $m=>$mname) {
			if(array_key_exists($m, $item_data)) {

				$sheet->write($row_index, $cell_index, $item_data[$m], $f_number);
				$cell_index++;

				$sk = $year . "-" . $m;
				if(array_key_exists($sk, $forwarder_totals)) {
					$forwarder_totals[$sk] = $forwarder_totals[$sk] +$item_data[$m];
				}
				else {
					$forwarder_totals[$sk] = $item_data[$m];
				}
				$item_total = $item_total + $item_data[$m];


			}
			else {
				$sheet->write($row_index, $cell_index, '', $f_number);
				$cell_index++;

				$sk = $year . "-" . $m;
				if(!array_key_exists($sk, $forwarder_totals)) {
					$forwarder_totals[$sk] = 0;
				}
			}
		}

		$sheet->write($row_index, $cell_index, $item_total, $f_number);

		$row_index++;
		$cell_index = 0;

		//weights
		$item_total = 0;
		$sheet->write($row_index, $cell_index, $data_types[2], $f_normal);
		$cell_index++;

		$item_data = $forwarder_weights[$year][$company];

		foreach($months as $m=>$mname) {
			if(array_key_exists($m, $item_data)) {

				$sheet->write($row_index, $cell_index, $item_data[$m], $f_number);
				$cell_index++;

				$sk = $year . "-" . $m;
				if(array_key_exists($sk, $forwarder_totals)) {
					$forwarder_totals[$sk] = $forwarder_totals[$sk] +$item_data[$m];
				}
				else {
					$forwarder_totals[$sk] = $item_data[$m];
				}
				$item_total = $item_total + $item_data[$m];


			}
			else {
				$sheet->write($row_index, $cell_index, '', $f_number);
				$cell_index++;

				$sk = $year . "-" . $m;
				if(!array_key_exists($sk, $forwarder_totals)) {
					$forwarder_totals[$sk] = 0;
				}
			}
		}

		$sheet->write($row_index, $cell_index, $item_total, $f_number);

		


		

		
		$cell_index = 0;

		$row_index++;
		$row_index++;
		$row_index++;

	}
}

$sheet->setColumn(0, 0, 30);
$sheet->setColumn(13, 13, 10);


/********************************************************************
    forwarder data by region item and month
*********************************************************************/
$sheetnumber = 2;
$sheet =& $xls->addWorksheet('By region');

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

$xls->activesheet = 2;

$sheet->write(0, 0, $header, $header_row);

$row_index = 2;

if($print_query_filter == 1)
{
	foreach($_filter_strings as $key=>$value)
	{
		 $sheet->write($row_index, 0,$key . ": " . $value);
		 $row_index++;
	}
}

$cell_index = 0;

foreach($region_shippings as $year=>$company_data) 
{
	$cell_index = 0;
	$row_index = $row_index+2;
    
	foreach($company_data as $company=>$item_data) {
		
		$forwarder_totals = array();
		$sheet->write($row_index, $cell_index, $company, $f_caption);
		$cell_index++;

		foreach($months as $m=>$mname) {
			$sheet->write($row_index, $cell_index, $year . " " . $mname, $f_caption);
			$cell_index++;
		}

		$sheet->write($row_index, $cell_index, "Total " . $year, $f_caption);
		$cell_index++;

		$row_index++;
		$cell_index = 0;

		
		$item_total = 0;
		$sheet->write($row_index, $cell_index, $data_types[1], $f_normal);
		$cell_index++;

		foreach($months as $m=>$mname) {
			if(array_key_exists($m, $item_data)) {

				$sheet->write($row_index, $cell_index, $item_data[$m], $f_number);
				$cell_index++;

				$sk = $year . "-" . $m;
				if(array_key_exists($sk, $forwarder_totals)) {
					$forwarder_totals[$sk] = $forwarder_totals[$sk] +$item_data[$m];
				}
				else {
					$forwarder_totals[$sk] = $item_data[$m];
				}
				$item_total = $item_total + $item_data[$m];


			}
			else {
				$sheet->write($row_index, $cell_index, '', $f_number);
				$cell_index++;

				$sk = $year . "-" . $m;
				if(!array_key_exists($sk, $forwarder_totals)) {
					$forwarder_totals[$sk] = 0;
				}
			}
		}

		$sheet->write($row_index, $cell_index, $item_total, $f_number);

		
		$row_index++;
		$cell_index = 0;
		

		//volumes
		$item_total = 0;
		$sheet->write($row_index, $cell_index, $data_types[3], $f_normal);
		$cell_index++;

		$item_data = $region_volumes[$year][$company];

		foreach($months as $m=>$mname) {
			if(array_key_exists($m, $item_data)) {

				$sheet->write($row_index, $cell_index, $item_data[$m], $f_number);
				$cell_index++;

				$sk = $year . "-" . $m;
				if(array_key_exists($sk, $forwarder_totals)) {
					$forwarder_totals[$sk] = $forwarder_totals[$sk] +$item_data[$m];
				}
				else {
					$forwarder_totals[$sk] = $item_data[$m];
				}
				$item_total = $item_total + $item_data[$m];


			}
			else {
				$sheet->write($row_index, $cell_index, '', $f_number);
				$cell_index++;

				$sk = $year . "-" . $m;
				if(!array_key_exists($sk, $forwarder_totals)) {
					$forwarder_totals[$sk] = 0;
				}
			}
		}

		$sheet->write($row_index, $cell_index, $item_total, $f_number);

		$row_index++;
		$cell_index = 0;

		//weights
		$item_total = 0;
		$sheet->write($row_index, $cell_index, $data_types[2], $f_normal);
		$cell_index++;

		$item_data = $region_weights[$year][$company];

		foreach($months as $m=>$mname) {
			if(array_key_exists($m, $item_data)) {

				$sheet->write($row_index, $cell_index, $item_data[$m], $f_number);
				$cell_index++;

				$sk = $year . "-" . $m;
				if(array_key_exists($sk, $forwarder_totals)) {
					$forwarder_totals[$sk] = $forwarder_totals[$sk] +$item_data[$m];
				}
				else {
					$forwarder_totals[$sk] = $item_data[$m];
				}
				$item_total = $item_total + $item_data[$m];


			}
			else {
				$sheet->write($row_index, $cell_index, '', $f_number);
				$cell_index++;

				$sk = $year . "-" . $m;
				if(!array_key_exists($sk, $forwarder_totals)) {
					$forwarder_totals[$sk] = 0;
				}
			}
		}

		$sheet->write($row_index, $cell_index, $item_total, $f_number);

		


		

		
		$cell_index = 0;

		$row_index++;
		$row_index++;
		$row_index++;

	}
}

$sheet->setColumn(0, 0, 30);
$sheet->setColumn(13, 13, 10);


/********************************************************************
    forwarder data by country item and month
*********************************************************************/
$sheetnumber = 3;
$sheet =& $xls->addWorksheet('By country');

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

$xls->activesheet = 2;

$sheet->write(0, 0, $header, $header_row);

$row_index = 2;

if($print_query_filter == 1)
{
	foreach($_filter_strings as $key=>$value)
	{
		 $sheet->write($row_index, 0,$key . ": " . $value);
		 $row_index++;
	}
}

$cell_index = 0;

foreach($country_shippings as $year=>$company_data) 
{
	$cell_index = 0;
	$row_index = $row_index+2;
    
	foreach($company_data as $company=>$item_data) {
		
		$forwarder_totals = array();
		$sheet->write($row_index, $cell_index, $company, $f_caption);
		$cell_index++;

		foreach($months as $m=>$mname) {
			$sheet->write($row_index, $cell_index, $year . " " . $mname, $f_caption);
			$cell_index++;
		}

		$sheet->write($row_index, $cell_index, "Total " . $year, $f_caption);
		$cell_index++;

		$row_index++;
		$cell_index = 0;

		
		$item_total = 0;
		$sheet->write($row_index, $cell_index, $data_types[1], $f_normal);
		$cell_index++;

		foreach($months as $m=>$mname) {
			if(array_key_exists($m, $item_data)) {

				$sheet->write($row_index, $cell_index, $item_data[$m], $f_number);
				$cell_index++;

				$sk = $year . "-" . $m;
				if(array_key_exists($sk, $forwarder_totals)) {
					$forwarder_totals[$sk] = $forwarder_totals[$sk] +$item_data[$m];
				}
				else {
					$forwarder_totals[$sk] = $item_data[$m];
				}
				$item_total = $item_total + $item_data[$m];


			}
			else {
				$sheet->write($row_index, $cell_index, '', $f_number);
				$cell_index++;

				$sk = $year . "-" . $m;
				if(!array_key_exists($sk, $forwarder_totals)) {
					$forwarder_totals[$sk] = 0;
				}
			}
		}

		$sheet->write($row_index, $cell_index, $item_total, $f_number);

		
		$row_index++;
		$cell_index = 0;
		

		//volumes
		$item_total = 0;
		$sheet->write($row_index, $cell_index, $data_types[3], $f_normal);
		$cell_index++;

		$item_data = $country_volumes[$year][$company];

		foreach($months as $m=>$mname) {
			if(array_key_exists($m, $item_data)) {

				$sheet->write($row_index, $cell_index, $item_data[$m], $f_number);
				$cell_index++;

				$sk = $year . "-" . $m;
				if(array_key_exists($sk, $forwarder_totals)) {
					$forwarder_totals[$sk] = $forwarder_totals[$sk] +$item_data[$m];
				}
				else {
					$forwarder_totals[$sk] = $item_data[$m];
				}
				$item_total = $item_total + $item_data[$m];


			}
			else {
				$sheet->write($row_index, $cell_index, '', $f_number);
				$cell_index++;

				$sk = $year . "-" . $m;
				if(!array_key_exists($sk, $forwarder_totals)) {
					$forwarder_totals[$sk] = 0;
				}
			}
		}

		$sheet->write($row_index, $cell_index, $item_total, $f_number);

		$row_index++;
		$cell_index = 0;

		//weights
		$item_total = 0;
		$sheet->write($row_index, $cell_index, $data_types[2], $f_normal);
		$cell_index++;

		$item_data = $country_weights[$year][$company];

		foreach($months as $m=>$mname) {
			if(array_key_exists($m, $item_data)) {

				$sheet->write($row_index, $cell_index, $item_data[$m], $f_number);
				$cell_index++;

				$sk = $year . "-" . $m;
				if(array_key_exists($sk, $forwarder_totals)) {
					$forwarder_totals[$sk] = $forwarder_totals[$sk] +$item_data[$m];
				}
				else {
					$forwarder_totals[$sk] = $item_data[$m];
				}
				$item_total = $item_total + $item_data[$m];


			}
			else {
				$sheet->write($row_index, $cell_index, '', $f_number);
				$cell_index++;

				$sk = $year . "-" . $m;
				if(!array_key_exists($sk, $forwarder_totals)) {
					$forwarder_totals[$sk] = 0;
				}
			}
		}

		$sheet->write($row_index, $cell_index, $item_total, $f_number);

		


		

		
		$cell_index = 0;

		$row_index++;
		$row_index++;
		$row_index++;

	}
}

$sheet->setColumn(0, 0, 30);
$sheet->setColumn(13, 13, 10);


/********************************************************************
   forwarder purchase volume data by item and month
*********************************************************************/
$sheetnumber = 4;
$sheet =& $xls->addWorksheet('By purchased volume');

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

$xls->activesheet = 4;

$sheet->write(0, 0, $header, $header_row);

$row_index = 2;

if($print_query_filter == 1)
{
	foreach($_filter_strings as $key=>$value)
	{
		 $sheet->write($row_index, 0,$key . ": " . $value);
		 $row_index++;
	}
}

$cell_index = 0;

foreach($purchases as $year=>$company_data) 
{
	$cell_index = 0;
	$row_index = $row_index+2;
	

	$sheet->write($row_index, $cell_index, "Forwarder" . $year, $f_caption);
	$cell_index++;

	foreach($months as $m=>$mname) {
		$sheet->write($row_index, $cell_index, $year . " " . $mname, $f_caption);
		$cell_index++;
	}

	$sheet->write($row_index, $cell_index, "Total " . $year, $f_caption);
    $row_index++;
	$cell_index = 0;

	$list_totals = array();
	foreach($company_data as $company=>$monthly_data) {
		$sheet->write($row_index, $cell_index, $company . " (CHF)", $f_normal);
		$cell_index++;
		
		$supplier_total = 0;
		foreach($months as $m=>$mname) {
			if(array_key_exists($m, $monthly_data)) {

				$sheet->write($row_index, $cell_index, $monthly_data[$m], $f_decimal);
				$cell_index++;

				$sk = $year . "-" . $m;
				if(array_key_exists($sk, $list_totals)) {
					$list_totals[$sk] = $list_totals[$sk] +$monthly_data[$m];
				}
				else {
					$list_totals[$sk] = $monthly_data[$m];
				}
				$supplier_total = $supplier_total + $monthly_data[$m];


			}
			else {
				$sheet->write($row_index, $cell_index, '', $f_decimal);
				$cell_index++;

				$sk = $year . "-" . $m;
				if(!array_key_exists($sk, $list_totals)) {
					$list_totals[$sk] = 0;
				}
			}
		}
		
		//output item_total
		$sheet->write($row_index, $cell_index, $supplier_total, $f_decimal);
		
		$cell_index = 0;
		$row_index++;

	}

	$cell_index = 0;
	$sheet->write($row_index, $cell_index, 'Totals CHF', $f_caption);
	$cell_index++;
	
	$yearly_total = 0;
	foreach($list_totals as $sk=>$list_total) {
		if($list_total > 0) {
			$sheet->write($row_index, $cell_index, $list_total, $f_decimal_bold);

			$yearly_total = $yearly_total + $list_total;
		}
		else {
			$sheet->write($row_index, $cell_index,'', $f_decimal_bold);
		}
		$cell_index++;
	}

	$sheet->write($row_index, $cell_index, $yearly_total, $f_decimal_bold);
	
	$cell_index = 0;
	$row_index++;
	$row_index++;
	$row_index++;

}

$sheet->setColumn(0, 0, 30);
$sheet->setColumn(1, 12, 10);
$sheet->setColumn(13, 13, 10);


$xls->activesheet = 1;

$xls->close(); 

?>