<?php
/********************************************************************

    projects_query_22_xls.php

    Print order state statistics

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-02-12
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-02-12
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_queries");

require_once "../include/phpexcel/PHPExcel.php";
require_once "include/get_functions.php";

/********************************************************************
    prepare all data needed
*********************************************************************/

if(!param("query_id"))
{
	redirect("projects_queries.php");
}


$print_query_filter = 0;
$project_order_state = 0;
$project_order_state2 = 0;
$catalog_order_state = 0;
$catalog_order_state2 = 0;


$sql = "select mis_query_filter, mis_print_filter from mis_queries " .
	   "where mis_query_id = " . param("query_id");

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{

	$filters = array();
	$filters = unserialize($row["mis_query_filter"]);

	
	$project_order_state = 1*$filters["fst"]; // project state
	$catalog_order_state = 1*$filters["tst"]; // order state

	if(array_key_exists("fst2", $filters))
	{
		$project_order_state2 = 1*$filters["fst2"]; // project state
	}
	if(array_key_exists("tst2", $filters))
	{
		$catalog_order_state2 = 1*$filters["tst2"]; // order state
	}

	$from_year = $filters["fdy"]; // from year
	$to_year = $filters["tdy"]; // to year
	
	$rto_filter = "";
	if($filters["rto"])
	{
		$filters["rto"] = substr($filters["rto"],0, strlen($filters["rto"])-1); // remove last comma
		$rtos = explode("-", $filters["rto"]);
		if(count($rtos) > 0)
		{
			$rto_filter = " and order_retail_operator in (" . implode(', ', $rtos) . ")";
		}
	}

	$supplier_filter = "";
	if($filters["supp"])
	{
		$filters["supp"] = substr($filters["supp"],0, strlen($filters["supp"])-1); // remove last comma
		$supps = explode("-", $filters["supp"]);
		if(count($supps) > 0)
		{
			$supplier_filter = " and order_item_supplier_address in (" . implode(', ', $supps) . ")";
		}
	}

	$client_filter = "";
	if(array_key_exists("clients", $filters) and $filters["clients"])
	{
		$filters["clients"] = substr($filters["clients"],0, strlen($filters["clients"])-1); // remove last comma
		$clients = explode("-", $filters["clients"]);
		if(count($clients) > 0)
		{
			$client_filter = " and order_client_address in (" . implode(', ', $clients) . ")";
		}
	}

	

	if($filters["fst"])
	{
		$_filter_strings["Project State"] = "From Project State: " . $filters["fst"];
	}

	if($filters["fst2"])
	{
		$_filter_strings["Project State2"] = "To Project State: " . $filters["fst2"];
	}

	if($filters["tst"])
	{
		$_filter_strings["Order State"] = "From Order State: " . $filters["tst"];
	}

	if($filters["tst2"])
	{
		$_filter_strings["Order State2"] = "To Order State: " . $filters["tst2"];
	}

	if($filters["fdy"])
	{
		$_filter_strings["From Year"] = "From Year: " . $filters["fdy"];
	}

	if($filters["tdy"])
	{
		$_filter_strings["To Year"] = "To Year: " . $filters["tdy"];
	}

	if($filters["rto"])
	{
		$tmp = array();
		foreach($rtos as $key=>$user_id)
		{
			$user = get_user($user_id);
			$tmp[] = $user["name"] . " " . $user["firstname"];
		}

		

		$tmp_lcs = implode(', ', $tmp);

		$_filter_strings["Logistic Coordinator"] = "Logistic Coordinators: " . $tmp_lcs;
	
	}

	if($filters["supp"])
	{
		$tmp = array();
		foreach($supps as $key=>$adress_id)
		{
			$address = get_address($adress_id);
			$tmp[] = $address["shortcut"];
		}
		$tmp_supps = implode(', ', $tmp);
		$_filter_strings["Suppliers"] = "Suppliers: " . $tmp_supps;
	
	}


	if(array_key_exists("clients", $filters) and $filters["clients"])
	{
		$tmp = array();
		foreach($clients as $key=>$adress_id)
		{
			$address = get_address($adress_id);
			$tmp[] = $address["company"];
		}
		$tmp_clients = implode(', ', $tmp);
		$_filter_strings["Clients"] = "Clients: " . $tmp_clients;
	
	}


	if($row["mis_print_filter"] == 1)
	{
		$print_query_filter = 1;
	}
}
else
{
	redirect("projects_queries.php");
}


//////////// PROJECTS
$project_order_states = array();
$project_order_states_per_client = array();
$project_order_states_per_supplier = array();
$basic_filter = 'where order_type = 1 ';
$basic_filter .= $rto_filter;
$basic_filter .= $supplier_filter;
$basic_filter .= $client_filter;

if($project_order_state and $project_order_state2)
{
	
	$sql = "select order_state_code from order_states " . 
		   " left join order_state_groups on order_state_group_id = order_state_group " . 
		   " where order_state_group_order_type = 1 " . 
		   " and order_state_code >= '" . $project_order_state . "' " .
		   " and order_state_code <= '" . $project_order_state2 . "' " .
		   " order by order_state_code";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$project_order_states[$row["order_state_code"]] = array();
		$project_order_states_per_client[$row["order_state_code"]] = array();
		$project_order_states_per_supplier[$row["order_state_code"]] = array();
	}
}
elseif($project_order_state)
{
	
	$sql = "select order_state_code from order_states " . 
		   " left join order_state_groups on order_state_group_id = order_state_group " . 
		   " where order_state_group_order_type = 1 " . 
		   " and order_state_code >= '" . $project_order_state . "' " .
		   " order by order_state_code";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$project_order_states[$row["order_state_code"]] = array();
		$project_order_states_per_client[$row["order_state_code"]] = array();
		$project_order_states_per_supplier[$row["order_state_code"]] = array();
	}
}
elseif($project_order_state2)
{
	
	$sql = "select order_state_code from order_states " . 
		   " left join order_state_groups on order_state_group_id = order_state_group " . 
		   " where order_state_group_order_type = 1 " . 
		   " and order_state_code <= '" . $project_order_state2 . "' " .
		   " order by order_state_code";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$project_order_states[$row["order_state_code"]] = array();
		$project_order_states_per_client[$row["order_state_code"]] = array();
		$project_order_states_per_supplier[$row["order_state_code"]] = array();
	}
}

foreach($project_order_states as $state=>$values)
{
	if($supplier_filter
	 or $state == '700'
	 or $state == '710'
	 or $state == '720'
	 or $state == '745')
	{

		$filter = $basic_filter . " and order_item_type in (1, 2) ";
		$filter = $filter . ' and order_state_code = "' . $state . '" ';


		if($from_year > 0)
		{
			$filter .= ' and year(order_items.order_item_ordered) >= ' . $from_year;
		}
		if($to_year > 0)
		{
			$filter .= ' and year(order_items.order_item_ordered) <= ' . $to_year;
		}


		$sql = 'SELECT 
				order_state_code,concat(order_state_code, " ", order_state_name) as order_state,
				year(order_items.order_item_ordered) as o_year,
				month(order_items.order_item_ordered) as o_month,
				count(distinct order_item_id) as num_items
				FROM
				actual_order_states
				left JOIN orders ON actual_order_states.actual_order_state_order = orders.order_id
				left JOIN order_items ON orders.order_id = order_items.order_item_order
				left JOIN order_states ON actual_order_states.actual_order_state_state = order_states.order_state_id 
				' . $filter . '
				group by order_state_code, o_year, o_month
				order by order_state_code, o_year, o_month';
	}
	else
	{
		$filter = $basic_filter;
		if($state)
		{
			$filter .= ' and order_state_code = "'. $state . '" ';
		}
		else
		{
			$filter .= ' and order_state_code = "xxx" ';
		}

		if($from_year > 0)
		{
			$filter .= ' and year(actual_order_states.date_created) >= ' . $from_year;
		}
		

		if($filter and $to_year > 0)
		{
			$filter .= ' and year(actual_order_states.date_created) <= ' . $to_year;
		}
			

		
		$sql = 'SELECT 
				order_state_code,concat(order_state_code, " ", order_state_name) as order_state, 
				year(actual_order_states.date_created) as o_year,
				month(actual_order_states.date_created) as o_month,
				count(distinct order_id) as num_items
				FROM
				actual_order_states
				left JOIN orders ON actual_order_states.actual_order_state_order = orders.order_id
				left JOIN order_states ON actual_order_states.actual_order_state_state = order_states.order_state_id
				left join addresses on address_id = order_client_address 
				' . $filter . '
				group by order_state_code, o_year, o_month 
				order by order_state_code, o_year, o_month';

	}
	
	
	$tmp_project_order_states = array();
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$order_state = array();
		$order_state['step'] = $row["order_state"];
		$order_state['year'] = $row["o_year"];
		$order_state['month'] = $row["o_month"];
		$order_state['occurence'] = $row["num_items"];
		$tmp_project_order_states[] = $order_state;
	}
	
	$project_order_states[$state] = $tmp_project_order_states;
}


//per client
$basic_filter = 'where order_type = 1 ';
$basic_filter .= $rto_filter;
$basic_filter .= $supplier_filter;
$basic_filter .= $client_filter;

foreach($project_order_states_per_client as $state=>$values)
{
	if($supplier_filter
	 or $state == '700'
	 or $state == '710'
	 or $state == '720'
	 or $state == '745')
	{

		$filter = $basic_filter . " and order_item_type in (1, 2) ";
		$filter .= ' and order_state_code = "' . $state . '" ';

		if($from_year > 0)
		{
			$filter .= ' and year(order_items.order_item_ordered) >= ' . $from_year;
		}
		if($to_year > 0)
		{
			$filter .= ' and year(order_items.order_item_ordered) <= ' . $to_year;
		}


		$sql = 'SELECT address_company,
				order_state_code,concat(order_state_code, " ", order_state_name) as order_state,
				year(order_items.order_item_ordered) as o_year,
				month(order_items.order_item_ordered) as o_month,
				count(distinct order_item_id) as num_items
				FROM
				actual_order_states
				left JOIN orders ON actual_order_states.actual_order_state_order = orders.order_id
				left JOIN order_items ON orders.order_id = order_items.order_item_order
				left JOIN order_states ON actual_order_states.actual_order_state_state = order_states.order_state_id 
				left join addresses on address_id = order_client_address
				' . $filter . '
				group by o_year, o_month, address_company, order_state_code, order_state
				order by o_year, o_month, address_company, order_state_code';
	}
	else
	{
		$filter = $basic_filter;
		if($project_order_state)
		{
			$filter .= ' and order_state_code = "'. $state . '" ';
		}
		else
		{
			$filter .= ' and order_state_code = "xxx" ';
		}

		if($from_year > 0)
		{
			$filter .= ' and year(actual_order_states.date_created) >= ' . $from_year;
		}
		

		if($filter and $to_year > 0)
		{
			$filter .= ' and year(actual_order_states.date_created) <= ' . $to_year;
		}
			

		
		$sql = 'SELECT address_company,
				order_state_code,concat(order_state_code, " ", order_state_name) as order_state, order_type,
				year(actual_order_states.date_created) as o_year,
				month(actual_order_states.date_created) as o_month,
				count(distinct order_id) as num_items
				FROM
				actual_order_states
				left JOIN orders ON actual_order_states.actual_order_state_order = orders.order_id
				left JOIN order_states ON actual_order_states.actual_order_state_state = order_states.order_state_id
				left join addresses on address_id = order_client_address
				' . $filter . '
				group by o_year, o_month, address_company, order_state_code, order_state
				order by o_year, o_month, address_company, order_state_code';

	}

	$tmp_project_order_states_per_client = array();

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$order_state = array();
		$order_state['company'] = $row["address_company"];
		$order_state['step'] = $row["order_state"];
		$order_state['year'] = $row["o_year"];
		$order_state['month'] = $row["o_month"];
		$order_state['occurence'] = $row["num_items"];
		$tmp_project_order_states_per_client[] = $order_state;
	}

	$project_order_states_per_client[$state] = $tmp_project_order_states_per_client;

}

//suppliers
$basic_filter = 'where order_type = 1 ';
$basic_filter .= $rto_filter;
$basic_filter .= $supplier_filter;
$basic_filter .= $client_filter;
$tmp_project_order_states_per_supplier = array();

foreach($project_order_states_per_supplier as $state=>$values)
{
	if($supplier_filter
	 or $state == '700'
	 or $state == '710'
	 or $state == '720'
	 or $state == '745')
	{

		$filter = $basic_filter . " and order_item_type in (1, 2) ";
		$filter .= ' and order_state_code = "' . $state . '" ';

		if($from_year > 0)
		{
			$filter .= ' and year(order_items.order_item_ordered) >= ' . $from_year;
		}
		if($to_year > 0)
		{
			$filter .= ' and year(order_items.order_item_ordered) <= ' . $to_year;
		}


		$sql = 'SELECT address_company,
				order_state_code,concat(order_state_code, " ", order_state_name) as order_state,
				year(order_items.order_item_ordered) as o_year,
				month(order_items.order_item_ordered) as o_month,
				count(distinct order_item_id) as num_items
				FROM
				actual_order_states
				left JOIN orders ON actual_order_states.actual_order_state_order = orders.order_id
				left JOIN order_items ON orders.order_id = order_items.order_item_order
				left JOIN order_states ON actual_order_states.actual_order_state_state = order_states.order_state_id 
				left JOIN addresses ON order_items.order_item_supplier_address = addresses.address_id
				' . $filter . '
				group by o_year, o_month, address_company, order_state_code, order_state
				order by o_year, o_month, address_company, order_state_code';
		
		$tmp_project_order_states_per_supplier = array();
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$order_state = array();
			$order_state['company'] = $row["address_company"];
			$order_state['step'] = $row["order_state"];
			$order_state['year'] = $row["o_year"];
			$order_state['month'] = $row["o_month"];
			$order_state['occurence'] = $row["num_items"];
			$tmp_project_order_states_per_supplier[] = $order_state;
		}
	}


	$project_order_states_per_supplier[$state] = $tmp_project_order_states_per_supplier;

}

////////////CATALOGUE ORDERS
//get all data
$order_states = array();
$order_states_per_client  = array();
$order_states_per_supplier  = array();

$basic_filter = 'where order_type = 2 ';
$basic_filter .= $rto_filter;
$basic_filter .= $supplier_filter;
$basic_filter .= $client_filter;


if($catalog_order_state and $catalog_order_state2)
{
	
	$sql = "select order_state_code from order_states " . 
		   " left join order_state_groups on order_state_group_id = order_state_group " . 
		   " where order_state_group_order_type = 2 " . 
		   " and order_state_code >= '" . $catalog_order_state . "' " .
		   " and order_state_code <= '" . $catalog_order_state2 . "' " .
		   " order by order_state_code";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$order_states[$row["order_state_code"]] = array();
		$order_states_per_client[$row["order_state_code"]] = array();
		$order_states_per_supplier[$row["order_state_code"]] = array();
	}
}
elseif($catalog_order_state)
{
	
	$sql = "select order_state_code from order_states " . 
		   " left join order_state_groups on order_state_group_id = order_state_group " . 
		   " where order_state_group_order_type = 2 " . 
		   " and order_state_code >= '" . $catalog_order_state . "' " .
		   " order by order_state_code";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$order_states[$row["order_state_code"]] = array();
		$order_states_per_client[$row["order_state_code"]] = array();
		$order_states_per_supplier[$row["order_state_code"]] = array();
	}
}
elseif($catalog_order_state2)
{
	
	$sql = "select order_state_code from order_states " . 
		   " left join order_state_groups on order_state_group_id = order_state_group " . 
		   " where order_state_group_order_type = 2 " . 
		   " and order_state_code <= '" . $catalog_order_state2 . "' " .
		   " order by order_state_code";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$order_states[$row["order_state_code"]] = array();
		$order_states_per_client[$row["order_state_code"]] = array();
		$order_states_per_supplier[$row["order_state_code"]] = array();
	}
}

foreach($order_states as $state=>$values)
{

	if($supplier_filter
	 or $state == '700'
	 or $state == '710'
	 or $state == '720'
	 or $state == '745')
	{

		$filter = $basic_filter . " and order_item_type in (1, 2) ";
		$filter .= ' and order_state_code = "'. $state . '" ';

		if($from_year > 0)
		{
			$filter .= ' and year(order_items.order_item_ordered) >= ' . $from_year;
		}
		if($to_year > 0)
		{
			$filter .= ' and year(order_items.order_item_ordered) <= ' . $to_year;
		}


		$sql = 'SELECT 
				order_state_code,concat(order_state_code, " ", order_state_name) as order_state, order_type,
				year(order_items.order_item_ordered) as o_year,
				month(order_items.order_item_ordered) as o_month,
				count(distinct order_item_id) as num_items
				FROM
				actual_order_states
				left JOIN orders ON actual_order_states.actual_order_state_order = orders.order_id
				left JOIN order_items ON orders.order_id = order_items.order_item_order
				left JOIN order_states ON actual_order_states.actual_order_state_state = order_states.order_state_id 
				' . $filter . '
				group by order_state_code, o_year, o_month, order_type
				order by order_state_code, o_year, o_month, order_type';
	}
	else
	{
		$filter = $basic_filter;
		if($state)
		{
			$filter .= ' and order_state_code = "'. $state . '" ';
		}
		else
		{
			$filter .= ' and order_state_code = "xxx" ';
		}

		if($from_year > 0)
		{
			$filter .= ' and year(actual_order_states.date_created) >= ' . $from_year;
		}
		

		if($filter and $to_year > 0)
		{
			$filter .= ' and year(actual_order_states.date_created) <= ' . $to_year;
		}
			

		
		$sql = 'SELECT 
				order_state_code,concat(order_state_code, " ", order_state_name) as order_state, order_type,
				year(actual_order_states.date_created) as o_year,
				month(actual_order_states.date_created) as o_month,
				count(distinct order_id) as num_items
				FROM
				actual_order_states
				left JOIN orders ON actual_order_states.actual_order_state_order = orders.order_id
				left JOIN order_states ON actual_order_states.actual_order_state_state = order_states.order_state_id
				' . $filter . '
				group by order_state_code, o_year, o_month, order_type
				order by order_state_code, o_year, o_month, order_type';

	}

	$tmp_sorder_states = array();
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$order_state = array();
		$order_state['step'] = $row["order_state"];
		$order_state['year'] = $row["o_year"];
		$order_state['month'] = $row["o_month"];
		$order_state['occurence'] = $row["num_items"];
		$tmp_sorder_states[] = $order_state;
	}

	$order_states[$state] = $tmp_sorder_states;

}


$basic_filter = 'where order_type = 2 ';
$basic_filter .= $rto_filter;
$basic_filter .= $supplier_filter;
$basic_filter .= $client_filter;


foreach($order_states_per_client as $state=>$values)
{
	if($supplier_filter
	 or $state == '700'
	 or $state == '710'
	 or $state == '720'
	 or $state == '745')
	{

		$filter = $basic_filter . " and order_item_type in (1, 2) ";
		$filter .= ' and order_state_code = "' . $state . '" ';

		if($from_year > 0)
		{
			$filter .= ' and year(order_items.order_item_ordered) >= ' . $from_year;
		}
		if($to_year > 0)
		{
			$filter .= ' and year(order_items.order_item_ordered) <= ' . $to_year;
		}


		$sql = 'SELECT address_company,
				order_state_code,concat(order_state_code, " ", order_state_name) as order_state, order_type,
				year(order_items.order_item_ordered) as o_year,
				month(order_items.order_item_ordered) as o_month,
				count(distinct order_item_id) as num_items
				FROM
				actual_order_states
				left JOIN orders ON actual_order_states.actual_order_state_order = orders.order_id
				left JOIN order_items ON orders.order_id = order_items.order_item_order
				left JOIN order_states ON actual_order_states.actual_order_state_state = order_states.order_state_id 
				left join addresses on address_id = order_client_address
				' . $filter . '
				group by o_year, o_month, address_company, order_state_code, order_state
				order by o_year, o_month, address_company, order_state_code';
	}
	else
	{
		$filter = $basic_filter;
		if($state)
		{
			$filter .= ' and order_state_code = "'. $state . '" ';
		}
		else
		{
			$filter .= ' and order_state_code = "xxx" ';
		}

		if($from_year > 0)
		{
			$filter .= ' and year(actual_order_states.date_created) >= ' . $from_year;
		}
		

		if($filter and $to_year > 0)
		{
			$filter .= ' and year(actual_order_states.date_created) <= ' . $to_year;
		}
			

		
		$sql = 'SELECT address_company,
				order_state_code,concat(order_state_code, " ", order_state_name) as order_state, order_type,
				year(actual_order_states.date_created) as o_year,
				month(actual_order_states.date_created) as o_month,
				count(distinct order_id) as num_items
				FROM
				actual_order_states
				left JOIN orders ON actual_order_states.actual_order_state_order = orders.order_id
				left JOIN order_states ON actual_order_states.actual_order_state_state = order_states.order_state_id
				left join addresses on address_id = order_client_address
				' . $filter . '
				group by o_year, o_month, address_company, order_state_code, order_state
				order by o_year, o_month, address_company, order_state_code';

	}

	$tmp_order_states_per_client = array();

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$order_state = array();
		$order_state['company'] = $row["address_company"];
		$order_state['step'] = $row["order_state"];
		$order_state['year'] = $row["o_year"];
		$order_state['month'] = $row["o_month"];
		$order_state['occurence'] = $row["num_items"];
		$tmp_order_states_per_client[] = $order_state;
	}

	$order_states_per_client[$state] = $tmp_order_states_per_client;

}


$basic_filter = 'where order_type = 2 ';
$basic_filter .= $rto_filter;
$basic_filter .= $supplier_filter;
$basic_filter .= $client_filter;
$tmp_order_states_per_supplier = array();

foreach($order_states_per_supplier as $state=>$values)
{
	if($supplier_filter
	 or $state == '700'
	 or $state == '710'
	 or $state == '720'
	 or $state == '745')
	{

		$filter = $basic_filter . " and order_item_type in (1, 2) ";
		$filter .= ' and order_state_code = "' . $state . '" ';

		if($from_year > 0)
		{
			$filter .= ' and year(order_items.order_item_ordered) >= ' . $from_year;
		}
		if($to_year > 0)
		{
			$filter .= ' and year(order_items.order_item_ordered) <= ' . $to_year;
		}


		$sql = 'SELECT address_company,
				order_state_code,concat(order_state_code, " ", order_state_name) as order_state, order_type,
				year(order_items.order_item_ordered) as o_year,
				month(order_items.order_item_ordered) as o_month,
				count(distinct order_item_id) as num_items
				FROM
				actual_order_states
				left JOIN orders ON actual_order_states.actual_order_state_order = orders.order_id
				left JOIN order_items ON orders.order_id = order_items.order_item_order
				left JOIN order_states ON actual_order_states.actual_order_state_state = order_states.order_state_id 
				left JOIN addresses ON order_items.order_item_supplier_address = addresses.address_id
				' . $filter . '
				group by o_year, o_month, address_company, order_state_code, order_state
				order by o_year, o_month, address_company, order_state_code';
		
		$tmp_order_states_per_supplier = array();

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$order_state = array();
			$order_state['company'] = $row["address_company"];
			$order_state['step'] = $row["order_state"];
			$order_state['year'] = $row["o_year"];
			$order_state['month'] = $row["o_month"];
			$order_state['occurence'] = $row["num_items"];
			$tmp_order_states_per_supplier[] = $order_state;
		}

	}
	
	$order_states_per_supplier[$state] = $tmp_order_states_per_supplier;
}


$captions['A'] = "State";
$captions['B'] = "Year";
$captions['C'] = "Month";
$captions['D'] = "Occurence";

$colwidth = array();
$colwidth['A'] = "40";
$colwidth['B'] = "5";
$colwidth['C'] = "5";
$colwidth['D'] = "8";


/********************************************************************
    Start output
*********************************************************************/
$objPHPExcel = new PHPExcel();



//output formats
$default_border = array(
    'style' => PHPExcel_Style_Border::BORDER_THIN,
    'color' => array('rgb'=>'1006A3')
);

$style_normal_border = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '1006A3'),
		),
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
);


$style_normal_border_right = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '1006A3'),
		),
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	),
);


$style_title = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
    'font' => array(
        'bold' => true,
		'size' => 16
    )
);


$style_title2 = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
    'font' => array(
        'bold' => true,
		'size' => 10
    )
);

$style_header = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'font' => array(
        'bold' => true,
    )
);

$style_header_right = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	),
    'font' => array(
        'bold' => true,
    )
);


$logo = new PHPExcel_Worksheet_Drawing();
$logo->setName('Logo');
$logo->setDescription('Logo');
$logo->setPath('../pictures/brand_logo.jpg');
$logo->setHeight(36);
$logo->setWidth(113);

$sheet = $objPHPExcel->getActiveSheet();
$objPHPExcel->getActiveSheet()->setTitle('Order States');
$logo->setWorksheet($objPHPExcel->getActiveSheet(0));

//default styles
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(8); 


// HEADRES ROW 1
$sheet->setCellValue('B1', 'Order States (' . date("d.m.Y H:i:s") . ')');
$sheet->getStyle('B1')->applyFromArray( $style_title );
$sheet->getRowDimension('1')->setRowHeight(36);


//$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
//$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3);
//$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
//$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(1);


//query strings
$cell_index = 0;
$row_index = 3;
if($print_query_filter == 1)
{
	foreach($_filter_strings as $key=>$value)
	{
		 $sheet->setCellValueByColumnAndRow($cell_index, $row_index, $value);
		 $row_index++;
	}
	$row_index++;
}

// Captions 
foreach($captions as $col=>$caption){
    $sheet->setCellValue($col . $row_index, $caption);
	$sheet->getStyle($col . $row_index)->applyFromArray( $style_header );
	$sheet->getStyle($col . $row_index)->getAlignment()->setTextRotation(90);
}

$sheet->getRowDimension($row_index)->setRowHeight(50);
$row_index++;
$row_index++;

//OUTPUT DATA


$zebra_counter = 0;
$i = 1;

////PROJECTS
if(count($project_order_states) > 0)
{
	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "PROJECTS");
	$sheet->getStyle('A' . $row_index)->applyFromArray( $style_title2 );
	$row_index++;
}

foreach($project_order_states as $state=>$tmp_project_order_states)
{
	$total = 0;

	foreach($tmp_project_order_states as $key=>$order_state)
	{
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["step"]);
		//if($colwidth['A'] < strlen($order_state["country_name"])){$colwidth['A'] = 2+strlen($order_state["country_name"]);}
		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["year"]);
		//if($colwidth['B'] < strlen($order_state["posaddress_name"])){$colwidth['B'] = 2+strlen($order_state["posaddress_name"]);}
		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["month"]);
		//if($colwidth['C'] < strlen($order_state["project_number"])){$colwidth['C'] = 2+strlen($order_state["project_number"]);}
		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["occurence"]);
		//if($colwidth['D'] < strlen($order_state["project_number"])){$colwidth['D'] = 2+strlen($order_state["project_number"]);}
		$cell_index++;

		$total = $total + $order_state["occurence"];

		$i++;
		$row_index++;
		$cell_index = 0;
		

	}
	if($total > 0)
	{
		$sheet->setCellValueByColumnAndRow(0, $row_index, "Total");
		$sheet->setCellValueByColumnAndRow(3, $row_index, $total);

		$sheet->getStyle('A' . $row_index)->applyFromArray( $style_title2 );
		$sheet->getStyle('D' . $row_index)->applyFromArray( $style_title2 );

		$row_index++;
		$row_index++;
	}
}


//CATALOGUE ORDERS
if(count($order_states) > 0)
{
	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "CATALOGUE ORDERS");
	$sheet->getStyle('A' . $row_index)->applyFromArray( $style_title2 );
	$row_index++;
}

foreach($order_states as $state=>$tmp_order_states)
{
	$total = 0;
	foreach($tmp_order_states as $key=>$order_state)
	{
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["step"]);
		//if($colwidth['A'] < strlen($order_state["country_name"])){$colwidth['A'] = 2+strlen($order_state["country_name"]);}
		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["year"]);
		//if($colwidth['B'] < strlen($order_state["posaddress_name"])){$colwidth['B'] = 2+strlen($order_state["posaddress_name"]);}
		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["month"]);
		//if($colwidth['C'] < strlen($order_state["project_number"])){$colwidth['C'] = 2+strlen($order_state["project_number"]);}
		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["occurence"]);
		//if($colwidth['D'] < strlen($order_state["project_number"])){$colwidth['D'] = 2+strlen($order_state["project_number"]);}
		$cell_index++;

		$total = $total + $order_state["occurence"];

		$i++;
		$row_index++;
		$cell_index = 0;
		

	}
	if($total > 0)
	{
		$sheet->setCellValueByColumnAndRow(0, $row_index, "Total");
		$sheet->setCellValueByColumnAndRow(3, $row_index, $total);
		$sheet->getStyle('A' . $row_index)->applyFromArray( $style_title2 );
		$sheet->getStyle('D' . $row_index)->applyFromArray( $style_title2 );

		$row_index++;
		$row_index++;
		
	}
}


//Format column heights and witdhs

foreach($colwidth as $col=>$width) {
	$sheet->getColumnDimension($col)->setWidth($width);
}



/********************************************************************
    Output data per Client
*********************************************************************/

$captions['A'] = "Client";
$captions['B'] = "State";
$captions['C'] = "Year";
$captions['D'] = "Month";
$captions['E'] = "Occurence";

$colwidth = array();
$colwidth['A'] = "60";
$colwidth['B'] = "40";
$colwidth['C'] = "5";
$colwidth['D'] = "5";
$colwidth['E'] = "8";



$logo2 = new PHPExcel_Worksheet_Drawing();
$logo2->setName('Logo');
$logo2->setDescription('Logo');
$logo2->setPath('../pictures/brand_logo.jpg');
$logo2->setHeight(36);
$logo2->setWidth(113);


$sheet2 = $objPHPExcel->createSheet();
$sheet2->setTitle('Order States per Client');
$objPHPExcel->setActiveSheetIndex(1);
$logo2->setWorksheet($objPHPExcel->getActiveSheet());
$objPHPExcel->setActiveSheetIndex(0);

// HEADRES ROW 1
$sheet2->setCellValue('B1', 'Order States per Client (' . date("d.m.Y H:i:s") . ')');
$sheet2->getStyle('B1')->applyFromArray( $style_title );
$sheet2->getRowDimension('1')->setRowHeight(36);

//$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
//$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(1);


// HEADRES 
foreach($captions as $col=>$caption){
    $sheet2->setCellValue($col . '3', $caption);
	$sheet2->getStyle($col . '3')->applyFromArray( $style_header );
	$sheet2->getStyle($col . '3')->getAlignment()->setTextRotation(90);
}

$sheet2->getRowDimension('3')->setRowHeight(50);


$zebra_counter = 0;
$row_index = 5;
$i = 1;
$cell_index = 0;

////PROJECTS
if(count($project_order_states_per_client) > 0)
{
	$sheet2->setCellValueByColumnAndRow($cell_index, $row_index, "PROJECTS");
	$sheet2->getStyle('A' . $row_index)->applyFromArray( $style_title2 );
	$row_index++;
}

foreach($project_order_states_per_client as $state=>$tmp_project_order_states_per_client)
{
	$total = 0;

	foreach($tmp_project_order_states_per_client as $key=>$order_state)
	{
		
		$sheet2->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["company"]);
		//if($colwidth['A'] < strlen($order_state["country_name"])){$colwidth['A'] = 2+strlen($order_state["country_name"]);}
		$cell_index++;
		
		$sheet2->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["step"]);
		//if($colwidth['A'] < strlen($order_state["country_name"])){$colwidth['A'] = 2+strlen($order_state["country_name"]);}
		$cell_index++;

		$sheet2->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["year"]);
		//if($colwidth['B'] < strlen($order_state["posaddress_name"])){$colwidth['B'] = 2+strlen($order_state["posaddress_name"]);}
		$cell_index++;

		$sheet2->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["month"]);
		//if($colwidth['C'] < strlen($order_state["project_number"])){$colwidth['C'] = 2+strlen($order_state["project_number"]);}
		$cell_index++;

		$sheet2->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["occurence"]);
		//if($colwidth['D'] < strlen($order_state["project_number"])){$colwidth['D'] = 2+strlen($order_state["project_number"]);}
		$cell_index++;

		$total = $total + $order_state["occurence"];

		$i++;
		$row_index++;
		$cell_index = 0;
		

	}
	if($total > 0)
	{
		$sheet2->setCellValueByColumnAndRow(0, $row_index, "Total");
		$sheet2->setCellValueByColumnAndRow(4, $row_index, $total);

		$sheet2->getStyle('A' . $row_index)->applyFromArray( $style_title2 );
		$sheet2->getStyle('E' . $row_index)->applyFromArray( $style_title2 );

		$row_index++;
		$row_index++;
	}
	
}



////CATALOGUE ORDERS
if(count($order_states_per_client) > 0)
{
	$sheet2->setCellValueByColumnAndRow($cell_index, $row_index, "CATALOGUE ORDERS");
	$sheet2->getStyle('A' . $row_index)->applyFromArray( $style_title2 );
	$row_index++;
}


$total = 0;
foreach($order_states_per_client as $state=>$tmp_order_states_per_client)
{
	foreach($tmp_order_states_per_client as $key=>$order_state)
	{
		$sheet2->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["company"]);
		//if($colwidth['A'] < strlen($order_state["country_name"])){$colwidth['A'] = 2+strlen($order_state["country_name"]);}
		$cell_index++;
		
		$sheet2->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["step"]);
		//if($colwidth['A'] < strlen($order_state["country_name"])){$colwidth['A'] = 2+strlen($order_state["country_name"]);}
		$cell_index++;

		$sheet2->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["year"]);
		//if($colwidth['B'] < strlen($order_state["posaddress_name"])){$colwidth['B'] = 2+strlen($order_state["posaddress_name"]);}
		$cell_index++;

		$sheet2->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["month"]);
		//if($colwidth['C'] < strlen($order_state["project_number"])){$colwidth['C'] = 2+strlen($order_state["project_number"]);}
		$cell_index++;

		$sheet2->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["occurence"]);
		//if($colwidth['D'] < strlen($order_state["project_number"])){$colwidth['D'] = 2+strlen($order_state["project_number"]);}
		$cell_index++;

		$total = $total + $order_state["occurence"];

		$i++;
		$row_index++;
		$cell_index = 0;
		

	}
	if($total > 0)
	{
		$sheet2->setCellValueByColumnAndRow(0, $row_index, "Total");
		$sheet2->setCellValueByColumnAndRow(4, $row_index, $total);
		$sheet2->getStyle('A' . $row_index)->applyFromArray( $style_title2 );
		$sheet2->getStyle('E' . $row_index)->applyFromArray( $style_title2 );

		$row_index++;
		$row_index++;
		
	}
}

foreach($colwidth as $col=>$width) {
	$sheet2->getColumnDimension($col)->setWidth($width);
}


/********************************************************************
    Output data per Supplier
*********************************************************************/
if(count($project_order_states_per_supplier) > 0 or count($order_states_per_supplier) > 0)
{
	$captions['A'] = "Supplier";
	$captions['B'] = "State";
	$captions['C'] = "Year";
	$captions['D'] = "Month";
	$captions['E'] = "Occurence";

	$colwidth = array();
	$colwidth['A'] = "60";
	$colwidth['B'] = "40";
	$colwidth['C'] = "5";
	$colwidth['D'] = "5";
	$colwidth['E'] = "8";



	$logo3 = new PHPExcel_Worksheet_Drawing();
	$logo3->setName('Logo');
	$logo3->setDescription('Logo');
	$logo3->setPath('../pictures/brand_logo.jpg');
	$logo3->setHeight(36);
	$logo3->setWidth(113);


	$sheet3 = $objPHPExcel->createSheet();
	$sheet3->setTitle('Order States per Supplier');
	$objPHPExcel->setActiveSheetIndex(2);
	$logo3->setWorksheet($objPHPExcel->getActiveSheet());
	$objPHPExcel->setActiveSheetIndex(0);

	// HEADRES ROW 1
	$sheet3->setCellValue('B1', 'Order States per Supplier (' . date("d.m.Y H:i:s") . ')');
	$sheet3->getStyle('B1')->applyFromArray( $style_title );
	$sheet3->getRowDimension('1')->setRowHeight(36);

	//$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
	//$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(1);


	// HEADRES 
	foreach($captions as $col=>$caption){
		$sheet3->setCellValue($col . '3', $caption);
		$sheet3->getStyle($col . '3')->applyFromArray( $style_header );
		$sheet3->getStyle($col . '3')->getAlignment()->setTextRotation(90);
	}

	$sheet3->getRowDimension('3')->setRowHeight(50);


	$zebra_counter = 0;
	$row_index = 5;
	$i = 1;
	$cell_index = 0;

	////PROJECTS
	if(count($project_order_states_per_supplier) > 0)
	{
		$sheet3->setCellValueByColumnAndRow($cell_index, $row_index, "PROJECTS");
		$sheet3->getStyle('A' . $row_index)->applyFromArray( $style_title2 );
		$row_index++;
	}

	foreach($project_order_states_per_supplier as $state=>$tmp_project_order_states_per_supplier)
	{
		$total = 0;

		foreach($tmp_project_order_states_per_supplier as $key=>$order_state)
		{
			
			$sheet3->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["company"]);
			//if($colwidth['A'] < strlen($order_state["country_name"])){$colwidth['A'] = 2+strlen($order_state["country_name"]);}
			$cell_index++;
			
			$sheet3->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["step"]);
			//if($colwidth['A'] < strlen($order_state["country_name"])){$colwidth['A'] = 2+strlen($order_state["country_name"]);}
			$cell_index++;

			$sheet3->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["year"]);
			//if($colwidth['B'] < strlen($order_state["posaddress_name"])){$colwidth['B'] = 2+strlen($order_state["posaddress_name"]);}
			$cell_index++;

			$sheet3->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["month"]);
			//if($colwidth['C'] < strlen($order_state["project_number"])){$colwidth['C'] = 2+strlen($order_state["project_number"]);}
			$cell_index++;

			$sheet3->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["occurence"]);
			//if($colwidth['D'] < strlen($order_state["project_number"])){$colwidth['D'] = 2+strlen($order_state["project_number"]);}
			$cell_index++;

			$total = $total + $order_state["occurence"];

			$i++;
			$row_index++;
			$cell_index = 0;
			

		}
		if($total > 0)
		{
			$sheet3->setCellValueByColumnAndRow(0, $row_index, "Total");
			$sheet3->setCellValueByColumnAndRow(4, $row_index, $total);

			$sheet3->getStyle('A' . $row_index)->applyFromArray( $style_title2 );
			$sheet3->getStyle('E' . $row_index)->applyFromArray( $style_title2 );
			$row_index++;
			$row_index++;
		}
	}

	if(count($order_states_per_supplier) > 0)
	{
		$sheet3->setCellValueByColumnAndRow($cell_index, $row_index, "CATALOGUE ORDERS");
		$sheet3->getStyle('A' . $row_index)->applyFromArray( $style_title2 );
		$row_index++;
	}

	foreach($order_states_per_supplier as $state=>$tmp_order_states_per_supplier)
	{
		$total = 0;

		foreach($tmp_order_states_per_supplier as $key=>$order_state)
		{
			
			$sheet3->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["company"]);
			//if($colwidth['A'] < strlen($order_state["country_name"])){$colwidth['A'] = 2+strlen($order_state["country_name"]);}
			$cell_index++;
			
			$sheet3->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["step"]);
			//if($colwidth['A'] < strlen($order_state["country_name"])){$colwidth['A'] = 2+strlen($order_state["country_name"]);}
			$cell_index++;

			$sheet3->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["year"]);
			//if($colwidth['B'] < strlen($order_state["posaddress_name"])){$colwidth['B'] = 2+strlen($order_state["posaddress_name"]);}
			$cell_index++;

			$sheet3->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["month"]);
			//if($colwidth['C'] < strlen($order_state["project_number"])){$colwidth['C'] = 2+strlen($order_state["project_number"]);}
			$cell_index++;

			$sheet3->setCellValueByColumnAndRow($cell_index, $row_index, $order_state["occurence"]);
			//if($colwidth['D'] < strlen($order_state["project_number"])){$colwidth['D'] = 2+strlen($order_state["project_number"]);}
			$cell_index++;

			$total = $total + $order_state["occurence"];

			$i++;
			$row_index++;
			$cell_index = 0;
			

		}
		if($total > 0)
		{
			$sheet3->setCellValueByColumnAndRow(0, $row_index, "Total");
			$sheet3->setCellValueByColumnAndRow(4, $row_index, $total);
			$sheet3->getStyle('A' . $row_index)->applyFromArray( $style_title2 );
			$sheet3->getStyle('E' . $row_index)->applyFromArray( $style_title2 );

			$row_index++;
			$row_index++;
			
		}
	}

	foreach($colwidth as $col=>$width) {
		$sheet3->getColumnDimension($col)->setWidth($width);
	}
}

/********************************************************************
    Activate Sheet 1
*********************************************************************/

$objPHPExcel->setActiveSheetIndex(0);
$sheet->setCellValue('A1', "");
$sheet->getStyle('A1')->applyFromArray($style_title);
//$objPHPExcel->getActiveSheet()->insertNewRowBefore(1, 1);
//$sheet->setCellValue('A1', "");
//$sheet->getStyle('A1')->applyFromArray($style_title);


/********************************************************************
    Start output
*********************************************************************/
$filename = 'Order_States_' . date('Ymd H:i:s') . '.xls';
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
header('Pragma: ');

//send data
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
?>