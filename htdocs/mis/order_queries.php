<?php
/********************************************************************

    order_queries.php

    Lists all Catalog Order Queries.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2016-08-19
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2016-08-19
    Version:        1.0.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if(!has_access("can_perform_all_queries") 
  and !has_access("can_perform_projects_query"))
{
	redirect("noaccess.php");
}



require_once "include/orders_query_get_functions.php";


if(param("remove_id"))
{
	$sql = "delete from orderquery_permissions " . 
		   " where orderquery_permission_query = " . param("remove_id") .
		   " and orderquery_permission_user = " . user_id(); 
	$result = mysql_query($sql) or dberror($sql);

	redirect("order_queries.php");

}
elseif(param("delete_id"))
{
	$sql = "delete from orderquery_permissions " . 
		   " where orderquery_permission_query = " . param("delete_id"); 
	$result = mysql_query($sql) or dberror($sql);

	$sql = "delete from orderqueries " . 
		   " where orderquery_id = " . param("delete_id"); 
	$result = mysql_query($sql) or dberror($sql);

	redirect("order_queries.php");
}


//build query for the list

$sql = "select orderquery_id, orderquery_group, orderquery_name, orderquery_fields, orderquery_owner, " .
       "concat(user_name, ' ', user_firstname) as uname, orderqueries.date_created as cdate, orderqueries.date_modified as mdate " . 
       "from orderqueries " .
	   "left join users on user_id = orderquery_owner ";

$list_filter = "orderquery_owner = " . user_id();

$list_filter2 = $list_filter;


if(param('group_filter')) {
	$list_filter .= " and orderquery_group  = " . dbquote(param('group_filter')) . " ";
}




$query_permissions = array();
$queries = array();
$query_links = array();
$copy_links = array();
$delete_links = array();
$permitted_users = array();


//$sql_a = $sql . " where orderquery_owner = " . user_id();

$sql_p = $sql;

if(param('group_filter')) {
	$sql_p .= " where orderquery_group  = " . dbquote(param('group_filter')) . " ";
}

$res = mysql_query($sql_p) or dberror($sql_p);

while ($row = mysql_fetch_assoc($res))
{
	if(check_if_query_has_fields($row["orderquery_id"]) == true)
	{
		$link = "<a href='order_queries_query_xls.php?query_id=" .  $row["orderquery_id"] . "'><img src=\"/pictures/ico_xls.gif\" border='0'/></a>";
		$queries[$row["orderquery_id"]] = $link;
	}
	else
	{
		$queries[$row["orderquery_id"]] = "<img src=\"/pictures/wf_warning.gif\" border='0'/>";
	}

	if(user_id() == $row["orderquery_owner"])
	{
		$query_links[$row["orderquery_id"]] = "<a href='order_queries_query.php?id=" .  $row["orderquery_id"] . "'>" . $row["orderquery_name"] . "</a>";

		$delete_links[$row["orderquery_id"]] = '<img data-id="'. $row["orderquery_id"]. '" class="delete_query" src="/pictures/remove.gif" border="0" style="margin-top:3px;cursor:pointer;" alt="Remove"/>';
	}
	else
	{
		$query_links[$row["orderquery_id"]] = $row["orderquery_name"];
		
		$delete_links[$row["orderquery_id"]] = '<a title="Remove query from my list" href="order_queries.php?remove_id=' .  $row["orderquery_id"] . '"><img src="/pictures/remove.gif" border="0" style="margin-top:3px;" alet="Remove"/></a>';
	}

	$copy_links[$row["orderquery_id"]] = "<a href='order_queries_query.php?copy_id=" .  $row["orderquery_id"] . "'>create copy</a>";

	//get permissions
	$tmp_users = array();
	$sql_p = "select * from orderquery_permissions " . 
		     " left join users on user_id = orderquery_permission_user " . 
			 "where orderquery_permission_query = " . $row["orderquery_id"];

	$res_p = mysql_query($sql_p) or dberror($sql_p);
	while ($row_p = mysql_fetch_assoc($res_p))
	{
		if($row_p["orderquery_permission_user"] == user_id())
		{
			$query_permissions[$row_p["orderquery_permission_query"]] = $row_p["orderquery_permission_query"];
		}
		else {
			$tmp_users[] = $row_p["user_name"] . " " . $row_p["user_firstname"];
		}
	}

	if(count($tmp_users) > 0) {
		$permitted_users[$row["orderquery_id"]] = implode(', ', $tmp_users);
	}
}

if(count($query_permissions) > 0)
{
	$permission_filter =  implode(',', $query_permissions);
	$permission_filter = " or orderquery_id in (" . $permission_filter . ") ";
	$list_filter .= $permission_filter;
}


//compose group filter
$group_filter = array();

$sql_g = "select DISTINCT orderquery_group, orderquery_group " .
                "from orderqueries " .
			    "left join users on user_id = orderquery_owner " .
				" where orderquery_group is not NULL " . 
				" and orderquery_group <> '' " . 
				" and (" . $list_filter2 . ") " . 
				" order by orderquery_group ";


$res = mysql_query($sql_g) or dberror($sql_g);

while ($row = mysql_fetch_assoc($res))
{
	$group_filter[$row["orderquery_group"]] = $row["orderquery_group"];
}





$list = new ListView($sql);

$list->set_entity("orderqueries");
$list->set_order("orderquery_group, orderquery_name");
$list->set_filter($list_filter);
$list->set_group("orderquery_group");

if(count($group_filter) > 0)
{
	$list->add_listfilters("group_filter", "Group", 'select', $group_filter, param("group_filter"));
}

$list->add_text_column("queries", " ", COLUMN_NO_WRAP | COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $queries);
$list->add_text_column("querylinks", "Query Name", COLUMN_NO_WRAP | COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $query_links);
$list->add_column("uname", "Owner", "");

$list->add_column("cdate", "Created", "", "", "", COLUMN_NO_WRAP);
$list->add_column("mdate", "Modified", "", "", "", COLUMN_NO_WRAP);


$list->add_text_column("querylinks", "", COLUMN_NO_WRAP | COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $copy_links);
$list->add_text_column("removelinks", "", COLUMN_NO_WRAP | COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $delete_links);
$list->add_text_column("permitted_users", "Granted Access", COLUMN_ALIGN_LEFT | COLUMN_UNDERSTAND_HTML, $permitted_users);

$list->add_button(LIST_BUTTON_NEW, "New Query", "order_queries_query.php");

$list->process();



$page = new Page("queries");

require_once("include/mis_page_actions.php");

$page->header();
$page->title("Replacment Order Queries");
$list->render();


?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('.delete_query').click(function(e) {
    e.preventDefault();
	

	$.nyroModalManual({
	  url: '/mis/delete_qurey.php?id=' + $(this).attr("data-id")  + '&context=order'
    });

	
    return false;
  });
  
  
});
</script>


<?php
$page->footer();

?>
