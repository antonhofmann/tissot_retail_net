<?php
/********************************************************************

    project_queries_query_fields_selector.php

    Enter selected fields for the query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2014-09-14
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2014-09-14
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../include/page_modal.php";

if(!has_access("can_perform_all_queries") 
  and !has_access("can_perform_projects_query"))
{
	redirect("noaccess.php");
}
require_once "include/projects_query_check_access.php";
require_once "include/projects_query_get_functions.php";

/********************************************************************
    prepare all data needed
*********************************************************************/
$query_id = param("query_id");

$fields = array();
$selected_fields = array();
$oldfields = array();

$sql = "select projectquery_fields from projectqueries " .
	   "where projectquery_id = " . $query_id;

if(param("t") == 'pl') // pos location
{
	$db_info = query_posaddress_fields();
	$fields = $db_info["fields"];
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["projectquery_fields"]);
		if(is_array($oldfields) and array_key_exists("pl", $oldfields))
		{
			$selected_fields = $oldfields["pl"];
		}
	}
}
elseif(param("t") == 'cl') // client address
{
	$db_info = query_clientaddress_fields();
	$fields = $db_info["fields"];
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["projectquery_fields"]);
		if(is_array($oldfields) and array_key_exists("cl", $oldfields))
		{
			$selected_fields = $oldfields["cl"];
		}
	}
}
elseif(param("t") == 'fr') // franchisee address
{
	$db_info = query_franchiseeaddress_fields();
	$fields = $db_info["fields"];
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["projectquery_fields"]);
		if(is_array($oldfields) and array_key_exists("fr", $oldfields))
		{
			$selected_fields = $oldfields["fr"];
		}
	}
}
elseif(param("t") == 'dcs') // distibution channel
{
	$db_info = query_distribution_channel_fields();
	$fields = $db_info["fields"];
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["projectquery_fields"]);
		if(is_array($oldfields) and array_key_exists("dcs", $oldfields))
		{
			$selected_fields = $oldfields["dcs"];
		}
	}
}
elseif(param("t") == "pro") // projects
{
	
	
	
	
	$db_info = query_projects_fields();
	$fields = $db_info["fields"];
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["projectquery_fields"]);
		if(is_array($oldfields) and array_key_exists("pro", $oldfields))
		{
			$selected_fields = $oldfields["pro"];
		}
	}
}
elseif(param("t") == "cer") // cer
{
	$db_info = query_cer_fields();
	$fields = $db_info["fields"];
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["projectquery_fields"]);
		if(is_array($oldfields) and array_key_exists("cer", $oldfields))
		{
			$selected_fields = $oldfields["cer"];
		}
	}
}
elseif(param("t") == "log") // projects
{
	$db_info = query_logistics_fields();
	$fields = $db_info["fields"];
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["projectquery_fields"]);
		if(is_array($oldfields) and array_key_exists("log", $oldfields))
		{
			$selected_fields = $oldfields["log"];
		}
	}
}
elseif(param("t") == "ren") // rental details
{
	$db_info = query_rental_fields();
	$fields = $db_info["fields"];
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["projectquery_fields"]);
		if(is_array($oldfields) and array_key_exists("ren", $oldfields))
		{
			$selected_fields = $oldfields["ren"];
		}
	}
}
elseif(param("t") == "pros") // project sheets
{
	$db_info = query_projects_sheet_fields();
	$fields = $db_info["fields"];
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["projectquery_fields"]);
		if(is_array($oldfields) and array_key_exists("pros", $oldfields))
		{
			$selected_fields = $oldfields["pros"];
		}
	}
}
elseif(param("t") == "cms") // CMS
{
	$db_info = query_cms_fields();
	$fields = $db_info["fields"];
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["projectquery_fields"]);
		if(is_array($oldfields) and array_key_exists("cms", $oldfields))
		{
			$selected_fields = $oldfields["cms"];
		}
	}
}
elseif(param("t") == "sta") // staff
{
	$db_info = query_staff_fields();
	$fields = $db_info["fields"];
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["projectquery_fields"]);
		if(is_array($oldfields) and array_key_exists("sta", $oldfields))
		{
			$selected_fields = $oldfields["sta"];
		}
	}
}
elseif(param("t") == "bud") // budget
{
	$db_info = query_budget_fields();
	$fields = $db_info["fields"];
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["projectquery_fields"]);
		if(is_array($oldfields) and array_key_exists("bud", $oldfields))
		{
			$selected_fields = $oldfields["bud"];
		}
	}
}
elseif(param("t") == "inv") // investments
{
	$db_info = query_investment_fields();
	$fields = $db_info["fields"];
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["projectquery_fields"]);
		if(is_array($oldfields) and array_key_exists("inv", $oldfields))
		{
			$selected_fields = $oldfields["inv"];
		}
	}
}
elseif(param("t") == "ainv") // approved investments
{
	$db_info = query_approved_investments_fields();
	$fields = $db_info["fields"];
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["projectquery_fields"]);
		if(is_array($oldfields) and array_key_exists("ainv", $oldfields))
		{
			$selected_fields = $oldfields["ainv"];
		}
	}
}
elseif(param("t") == "cos") // costing
{
	$db_info = query_real_costs_fields();
	$fields = $db_info["fields"];
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["projectquery_fields"]);
		if(is_array($oldfields) and array_key_exists("cos", $oldfields))
		{
			$selected_fields = $oldfields["cos"];
		}
	}
}
elseif(param("t") == "comp") // cost comparison
{
	$db_info = query_cost_comparison_fields();
	$fields = $db_info["fields"];
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["projectquery_fields"]);
		if(is_array($oldfields) and array_key_exists("comp", $oldfields))
		{
			$selected_fields = $oldfields["comp"];
		}
	}
}
elseif(param("t") == "mst") // milestones
{
	$db_info = query_milestone_fields();
	$fields = $db_info["fields"];
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["projectquery_fields"]);
		if(is_array($oldfields) and array_key_exists("mst", $oldfields))
		{
			$selected_fields = $oldfields["mst"];
		}
	}
}
elseif(param("t") == "ost") // order states
{
	$db_info = query_orderstate_fields();
	$fields = $db_info["fields"];
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["projectquery_fields"]);
		if(is_array($oldfields) and array_key_exists("ost", $oldfields))
		{
			$selected_fields = $oldfields["ost"];
		}
	}
}
elseif(param("t") == "item") // items
{
	$db_info = query_item_fields();
	$fields = $db_info["fields"];
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["projectquery_fields"]);
		if(is_array($oldfields) and array_key_exists("item", $oldfields))
		{
			$selected_fields = $oldfields["item"];
		}
	}
}



if(!$selected_fields)
{
	$selected_fields = array();
}



/********************************************************************
    save data
*********************************************************************/
if(param("save_form"))
{
	$oldfields["pl"] = array();
	$oldfields["cl"] = array();
	$oldfields["fr"] = array();
	$oldfields["dcs"] = array();
	$oldfields["pro"] = array();
	$oldfields["cer"] = array();
	$oldfields["log"] = array();
	$oldfields["ren"] = array();
	$oldfields["pros"] = array();
	$oldfields["cms"] = array();
	$oldfields["sta"] = array();
	$oldfields["bud"] = array();
	$oldfields["inv"] = array();
	$oldfields["ainv"] = array();
	$oldfields["cos"] = array();
	$oldfields["comp"] = array();
	$oldfields["mst"] = array();
	$oldfields["ost"] = array();
	$oldfields["item"] = array();
	

	//get Query Fields
	$sql = "select projectquery_fields from projectqueries " .
		   "where projectquery_id = " . $query_id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields = unserialize($row["projectquery_fields"]);
	}
	
	if(!$oldfields["pl"]) // pos location
	{
		$oldfields["pl"] = array();
	}
	if(!$oldfields["cl"]) // client
	{
		$oldfields["cl"] = array();
	}
	if(!$oldfields["fr"]) // franchisee/owner
	{
		$oldfields["fr"] = array();
	}
	if(!$oldfields["dcs"]) // disribution channel
	{
		$oldfields["dcs"] = array();
	}
	if(!$oldfields["pro"]) // projects
	{
		$oldfields["pro"] = array();
	}
	if(!$oldfields["cer"]) // cer
	{
		$oldfields["cer"] = array();
	}
	if(!$oldfields["log"]) // logistics details
	{
		$oldfields["log"] = array();
	}
	if(!$oldfields["ren"]) // rental details
	{
		$oldfields["ren"] = array();
	}
	if(!$oldfields["pros"]) // project sheet
	{
		$oldfields["pros"] = array();
	}
	if(!$oldfields["cms"]) // CMS
	{
		$oldfields["cms"] = array();
	}
	if(!$oldfields["sta"]) // staff
	{
		$oldfields["sta"] = array();
	}
	if(!$oldfields["bud"]) // budet
	{
		$oldfields["bud"] = array();
	}
	if(!$oldfields["inv"]) // investments
	{
		$oldfields["inv"] = array();
	}
	if(!$oldfields["ainv"]) // approved investments
	{
		$oldfields["ainv"] = array();
	}
	if(!$oldfields["cos"]) // real costs
	{
		$oldfields["cos"] = array();
	}
	if(!$oldfields["comp"]) // cost comparison
	{
		$oldfields["comp"] = array();
	}
	if(!$oldfields["mst"]) // milestones
	{
		$oldfields["mst"] = array();
	}
	if(!$oldfields["ost"]) // order states
	{
		$oldfields["ost"] = array();
	}
	if(!$oldfields["item"]) // items
	{
		$oldfields["item"] = array();
	}

	
	
	
	
	
	$new_selected_fields = array();
	foreach($fields as $key=>$caption)
	{
		$post_key = str_replace(".", "_", $key);
		if($_POST[$post_key])
		{
			$new_selected_fields[$key] = $caption;
		}
	}

	
	if(param("t") == "pl") //pos locations
	{
		$updatefields["pl"] = $new_selected_fields;
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["pro"] = $oldfields["pro"];
		$updatefields["cer"] = $oldfields["cer"];
		$updatefields["log"] = $oldfields["log"];
		$updatefields["ren"] = $oldfields["ren"];
		$updatefields["pros"] = $oldfields["pros"];
		$updatefields["cms"] = $oldfields["cms"];
		$updatefields["sta"] = $oldfields["sta"];
		$updatefields["bud"] = $oldfields["bud"];
		$updatefields["inv"] = $oldfields["inv"];
		$updatefields["ainv"] = $oldfields["ainv"];
		$updatefields["cos"] = $oldfields["cos"];
		$updatefields["comp"] = $oldfields["comp"];
		$updatefields["mst"] = $oldfields["mst"];
		$updatefields["ost"] = $oldfields["ost"];
		$updatefields["item"] = $oldfields["item"];
	}
	elseif(param("t") == "cl") // client
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $new_selected_fields;
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["pro"] = $oldfields["pro"];
		$updatefields["cer"] = $oldfields["cer"];
		$updatefields["log"] = $oldfields["log"];
		$updatefields["ren"] = $oldfields["ren"];
		$updatefields["pros"] = $oldfields["pros"];
		$updatefields["cms"] = $oldfields["cms"];
		$updatefields["sta"] = $oldfields["sta"];
		$updatefields["bud"] = $oldfields["bud"];
		$updatefields["inv"] = $oldfields["inv"];
		$updatefields["ainv"] = $oldfields["ainv"];
		$updatefields["cos"] = $oldfields["cos"];
		$updatefields["comp"] = $oldfields["comp"];
		$updatefields["mst"] = $oldfields["mst"];
		$updatefields["ost"] = $oldfields["ost"];
		$updatefields["item"] = $oldfields["item"];
	}
	elseif(param("t") == "fr") //franchisee/owner
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $new_selected_fields;
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["pro"] = $oldfields["pro"];
		$updatefields["cer"] = $oldfields["cer"];
		$updatefields["log"] = $oldfields["log"];
		$updatefields["ren"] = $oldfields["ren"];
		$updatefields["pros"] = $oldfields["pros"];
		$updatefields["cms"] = $oldfields["cms"];
		$updatefields["sta"] = $oldfields["sta"];
		$updatefields["bud"] = $oldfields["bud"];
		$updatefields["inv"] = $oldfields["inv"];
		$updatefields["ainv"] = $oldfields["ainv"];
		$updatefields["cos"] = $oldfields["cos"];
		$updatefields["comp"] = $oldfields["comp"];
		$updatefields["mst"] = $oldfields["mst"];
		$updatefields["ost"] = $oldfields["ost"];
		$updatefields["item"] = $oldfields["item"];

	}
	elseif(param("t") == "dcs") // distribution channels
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["dcs"] = $new_selected_fields;
		$updatefields["pro"] = $oldfields["pro"];
		$updatefields["cer"] = $oldfields["cer"];
		$updatefields["log"] = $oldfields["log"];
		$updatefields["ren"] = $oldfields["ren"];
		$updatefields["pros"] = $oldfields["pros"];
		$updatefields["cms"] = $oldfields["cms"];
		$updatefields["sta"] = $oldfields["sta"];
		$updatefields["bud"] = $oldfields["bud"];
		$updatefields["inv"] = $oldfields["inv"];
		$updatefields["ainv"] = $oldfields["ainv"];
		$updatefields["cos"] = $oldfields["cos"];
		$updatefields["comp"] = $oldfields["comp"];
		$updatefields["mst"] = $oldfields["mst"];
		$updatefields["ost"] = $oldfields["ost"];
		$updatefields["item"] = $oldfields["item"];
	}
	elseif(param("t") == "pro") // projects
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["pro"] = $new_selected_fields;
		$updatefields["cer"] = $oldfields["cer"];
		$updatefields["log"] = $oldfields["log"];
		$updatefields["ren"] = $oldfields["ren"];
		$updatefields["pros"] = $oldfields["pros"];
		$updatefields["cms"] = $oldfields["cms"];
		$updatefields["sta"] = $oldfields["sta"];
		$updatefields["bud"] = $oldfields["bud"];
		$updatefields["inv"] = $oldfields["inv"];
		$updatefields["ainv"] = $oldfields["ainv"];
		$updatefields["cos"] = $oldfields["cos"];
		$updatefields["comp"] = $oldfields["comp"];
		$updatefields["mst"] = $oldfields["mst"];
		$updatefields["ost"] = $oldfields["ost"];
		$updatefields["item"] = $oldfields["item"];
	}
	elseif(param("t") == "cer") // cer
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["pro"] = $oldfields["pro"];
		$updatefields["cer"] = $new_selected_fields;
		$updatefields["log"] = $oldfields["log"];
		$updatefields["ren"] = $oldfields["ren"];
		$updatefields["pros"] = $oldfields["pros"];
		$updatefields["cms"] = $oldfields["cms"];
		$updatefields["sta"] = $oldfields["sta"];
		$updatefields["bud"] = $oldfields["bud"];
		$updatefields["inv"] = $oldfields["inv"];
		$updatefields["ainv"] = $oldfields["ainv"];
		$updatefields["cos"] = $oldfields["cos"];
		$updatefields["comp"] = $oldfields["comp"];
		$updatefields["mst"] = $oldfields["mst"];
		$updatefields["ost"] = $oldfields["ost"];
		$updatefields["item"] = $oldfields["item"];
	}
	elseif(param("t") == "log") // logistics details
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["pro"] = $oldfields["pro"];
		$updatefields["cer"] = $oldfields["cer"];
		$updatefields["log"] = $new_selected_fields;
		$updatefields["ren"] = $oldfields["ren"];
		$updatefields["pros"] = $oldfields["pros"];
		$updatefields["cms"] = $oldfields["cms"];
		$updatefields["sta"] = $oldfields["sta"];
		$updatefields["bud"] = $oldfields["bud"];
		$updatefields["inv"] = $oldfields["inv"];
		$updatefields["ainv"] = $oldfields["ainv"];
		$updatefields["cos"] = $oldfields["cos"];
		$updatefields["comp"] = $oldfields["comp"];
		$updatefields["mst"] = $oldfields["mst"];
		$updatefields["ost"] = $oldfields["ost"];
		$updatefields["item"] = $oldfields["item"];
	}
	elseif(param("t") == "ren") // rental details
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["pro"] = $oldfields["pro"];
		$updatefields["cer"] = $oldfields["cer"];
		$updatefields["log"] = $oldfields["log"];
		$updatefields["ren"] = $new_selected_fields;
		$updatefields["pros"] = $oldfields["pros"];
		$updatefields["cms"] = $oldfields["cms"];
		$updatefields["sta"] = $oldfields["sta"];
		$updatefields["bud"] = $oldfields["bud"];
		$updatefields["inv"] = $oldfields["inv"];
		$updatefields["ainv"] = $oldfields["ainv"];
		$updatefields["cos"] = $oldfields["cos"];
		$updatefields["comp"] = $oldfields["comp"];
		$updatefields["mst"] = $oldfields["mst"];
		$updatefields["ost"] = $oldfields["ost"];
		$updatefields["item"] = $oldfields["item"];
	}
	elseif(param("t") == "pros") // project sheet
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["pro"] = $oldfields["pro"];
		$updatefields["cer"] = $oldfields["cer"];
		$updatefields["log"] = $oldfields["log"];
		$updatefields["ren"] = $oldfields["ren"];
		$updatefields["pros"] = $new_selected_fields;
		$updatefields["cms"] = $oldfields["cms"];
		$updatefields["sta"] = $oldfields["sta"];
		$updatefields["bud"] = $oldfields["bud"];
		$updatefields["inv"] = $oldfields["inv"];
		$updatefields["ainv"] = $oldfields["ainv"];
		$updatefields["cos"] = $oldfields["cos"];
		$updatefields["comp"] = $oldfields["comp"];
		$updatefields["mst"] = $oldfields["mst"];
		$updatefields["ost"] = $oldfields["ost"];
		$updatefields["item"] = $oldfields["item"];
	}
	elseif(param("t") == "cms") // CMS
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["pro"] = $oldfields["pro"];
		$updatefields["cer"] = $oldfields["cer"];
		$updatefields["log"] = $oldfields["log"];
		$updatefields["ren"] = $oldfields["ren"];
		$updatefields["pros"] = $oldfields["pros"];
		$updatefields["cms"] = $new_selected_fields;
		$updatefields["sta"] = $oldfields["sta"];
		$updatefields["bud"] = $oldfields["bud"];
		$updatefields["inv"] = $oldfields["inv"];
		$updatefields["ainv"] = $oldfields["ainv"];
		$updatefields["cos"] = $oldfields["cos"];
		$updatefields["comp"] = $oldfields["comp"];
		$updatefields["mst"] = $oldfields["mst"];
		$updatefields["ost"] = $oldfields["ost"];
		$updatefields["item"] = $oldfields["item"];
	}
	elseif(param("t") == "sta") // staff
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["pro"] = $oldfields["pro"];
		$updatefields["cer"] = $oldfields["cer"];
		$updatefields["log"] = $oldfields["log"];
		$updatefields["ren"] = $oldfields["ren"];
		$updatefields["pros"] = $oldfields["pros"];
		$updatefields["cms"] = $oldfields["cms"];
		$updatefields["sta"] = $new_selected_fields;
		$updatefields["bud"] = $oldfields["bud"];
		$updatefields["inv"] = $oldfields["inv"];
		$updatefields["ainv"] = $oldfields["ainv"];
		$updatefields["cos"] = $oldfields["cos"];
		$updatefields["comp"] = $oldfields["comp"];
		$updatefields["mst"] = $oldfields["mst"];
		$updatefields["ost"] = $oldfields["ost"];
		$updatefields["item"] = $oldfields["item"];
	}
	elseif(param("t") == "bud") // budget
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["pro"] = $oldfields["pro"];
		$updatefields["cer"] = $oldfields["cer"];
		$updatefields["log"] = $oldfields["log"];
		$updatefields["ren"] = $oldfields["ren"];
		$updatefields["pros"] = $oldfields["pros"];
		$updatefields["cms"] = $oldfields["cms"];
		$updatefields["sta"] = $oldfields["sta"];
		$updatefields["bud"] = $new_selected_fields;
		$updatefields["inv"] = $oldfields["inv"];
		$updatefields["ainv"] = $oldfields["ainv"];
		$updatefields["cos"] = $oldfields["cos"];
		$updatefields["comp"] = $oldfields["comp"];
		$updatefields["mst"] = $oldfields["mst"];
		$updatefields["ost"] = $oldfields["ost"];
		$updatefields["item"] = $oldfields["item"];
	}
	elseif(param("t") == "inv") // investments
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["pro"] = $oldfields["pro"];
		$updatefields["cer"] = $oldfields["cer"];
		$updatefields["log"] = $oldfields["log"];
		$updatefields["ren"] = $oldfields["ren"];
		$updatefields["pros"] = $oldfields["pros"];
		$updatefields["cms"] = $oldfields["cms"];
		$updatefields["sta"] = $oldfields["sta"];
		$updatefields["bud"] = $oldfields["bud"];
		$updatefields["inv"] = $new_selected_fields;
		$updatefields["ainv"] = $oldfields["ainv"];
		$updatefields["cos"] = $oldfields["cos"];
		$updatefields["comp"] = $oldfields["comp"];
		$updatefields["mst"] = $oldfields["mst"];
		$updatefields["ost"] = $oldfields["ost"];
		$updatefields["item"] = $oldfields["item"];
	}
	elseif(param("t") == "ainv") // approved investments
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["pro"] = $oldfields["pro"];
		$updatefields["cer"] = $oldfields["cer"];
		$updatefields["log"] = $oldfields["log"];
		$updatefields["ren"] = $oldfields["ren"];
		$updatefields["pros"] = $oldfields["pros"];
		$updatefields["cms"] = $oldfields["cms"];
		$updatefields["sta"] = $oldfields["sta"];
		$updatefields["bud"] = $oldfields["bud"];
		$updatefields["inv"] = $oldfields["inv"];
		$updatefields["ainv"] = $new_selected_fields;
		$updatefields["cos"] = $oldfields["cos"];
		$updatefields["comp"] = $oldfields["comp"];
		$updatefields["mst"] = $oldfields["mst"];
		$updatefields["ost"] = $oldfields["ost"];
		$updatefields["item"] = $oldfields["item"];
	}
	elseif(param("t") == "cos") // real costs
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["pro"] = $oldfields["pro"];
		$updatefields["cer"] = $oldfields["cer"];
		$updatefields["log"] = $oldfields["log"];
		$updatefields["ren"] = $oldfields["ren"];
		$updatefields["pros"] = $oldfields["pros"];
		$updatefields["cms"] = $oldfields["cms"];
		$updatefields["sta"] = $oldfields["sta"];
		$updatefields["bud"] = $oldfields["bud"];
		$updatefields["inv"] = $oldfields["inv"];
		$updatefields["ainv"] = $oldfields["ainv"];
		$updatefields["cos"] = $new_selected_fields;
		$updatefields["comp"] = $oldfields["comp"];
		$updatefields["mst"] = $oldfields["mst"];
		$updatefields["ost"] = $oldfields["ost"];
		$updatefields["item"] = $oldfields["item"];
	}
	elseif(param("t") == "comp") // cost comparison
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["pro"] = $oldfields["pro"];
		$updatefields["cer"] = $oldfields["cer"];
		$updatefields["log"] = $oldfields["log"];
		$updatefields["ren"] = $oldfields["ren"];
		$updatefields["pros"] = $oldfields["pros"];
		$updatefields["cms"] = $oldfields["cms"];
		$updatefields["sta"] = $oldfields["sta"];
		$updatefields["bud"] = $oldfields["bud"];
		$updatefields["inv"] = $oldfields["inv"];
		$updatefields["ainv"] = $oldfields["ainv"];
		$updatefields["cos"] = $oldfields["cos"];
		$updatefields["comp"] = $new_selected_fields;
		$updatefields["mst"] = $oldfields["mst"];
		$updatefields["ost"] = $oldfields["ost"];
		$updatefields["item"] = $oldfields["item"];
	}
	elseif(param("t") == "mst") // costing
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["pro"] = $oldfields["pro"];
		$updatefields["cer"] = $oldfields["cer"];
		$updatefields["log"] = $oldfields["log"];
		$updatefields["ren"] = $oldfields["ren"];
		$updatefields["pros"] = $oldfields["pros"];
		$updatefields["cms"] = $oldfields["cms"];
		$updatefields["sta"] = $oldfields["sta"];
		$updatefields["bud"] = $oldfields["bud"];
		$updatefields["inv"] = $oldfields["inv"];
		$updatefields["ainv"] = $oldfields["ainv"];
		$updatefields["cos"] = $oldfields["cos"];
		$updatefields["comp"] = $oldfields["comp"];
		$updatefields["mst"] = $new_selected_fields;
		$updatefields["ost"] = $oldfields["ost"];
		$updatefields["item"] = $oldfields["item"];
	}
	elseif(param("t") == "ost") // costing
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["pro"] = $oldfields["pro"];
		$updatefields["cer"] = $oldfields["cer"];
		$updatefields["log"] = $oldfields["log"];
		$updatefields["ren"] = $oldfields["ren"];
		$updatefields["pros"] = $oldfields["pros"];
		$updatefields["cms"] = $oldfields["cms"];
		$updatefields["sta"] = $oldfields["sta"];
		$updatefields["bud"] = $oldfields["bud"];
		$updatefields["inv"] = $oldfields["inv"];
		$updatefields["ainv"] = $oldfields["ainv"];
		$updatefields["cos"] = $oldfields["cos"];
		$updatefields["comp"] = $oldfields["comp"];
		$updatefields["mst"] = $oldfields["mst"];
		$updatefields["ost"] = $new_selected_fields;
		$updatefields["item"] = $oldfields["item"];
	}
	elseif(param("t") == "item") // items
	{
		$updatefields["pl"] = $oldfields["pl"];
		$updatefields["cl"] = $oldfields["cl"];
		$updatefields["fr"] = $oldfields["fr"];
		$updatefields["dcs"] = $oldfields["dcs"];
		$updatefields["pro"] = $oldfields["pro"];
		$updatefields["cer"] = $oldfields["cer"];
		$updatefields["log"] = $oldfields["log"];
		$updatefields["ren"] = $oldfields["ren"];
		$updatefields["pros"] = $oldfields["pros"];
		$updatefields["cms"] = $oldfields["cms"];
		$updatefields["sta"] = $oldfields["sta"];
		$updatefields["bud"] = $oldfields["bud"];
		$updatefields["inv"] = $oldfields["inv"];
		$updatefields["ainv"] = $oldfields["ainv"];
		$updatefields["cos"] = $oldfields["cos"];
		$updatefields["comp"] = $oldfields["comp"];
		$updatefields["mst"] = $oldfields["mst"];
		$updatefields["ost"] = $oldfields["ost"];
		$updatefields["item"] = $new_selected_fields;
	}


	$sql = "update projectqueries " . 
		   "set projectquery_fields = " . dbquote(serialize($updatefields)) . " " . 
		   " where projectquery_id = " . param("query_id");

	$result = mysql_query($sql) or dberror($sql);

	//update field order and sortorder
	$sql = "select projectquery_fields, projectquery_field_order, projectquery_order from projectqueries " .
		   "where projectquery_id = " . $query_id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$oldfields["pl"] = array();
		$oldfields["cl"] = array();
		$oldfields["fr"] = array();
		$oldfields["dcs"] = array();
		$oldfields["pro"] = array();
		$oldfields["cer"] = array();
		$oldfields["log"] = array();
		$oldfields["ren"] = array();
		$oldfields["pros"] = array();
		$oldfields["cms"] = array();
		$oldfields["sta"] = array();
		$oldfields["bud"] = array();
		$oldfields["inv"] = array();
		$oldfields["ainv"] = array();
		$oldfields["cos"] = array();
		$oldfields["comp"] = array();
		$oldfields["mst"] = array();
		$oldfields["ost"] = array();
		$oldfields["item"] = array();


		$oldfields = unserialize($row["projectquery_fields"]);

		

		$oldfields = array_merge($oldfields["pl"], $oldfields["cl"], $oldfields["fr"], $oldfields["dcs"], $oldfields["pro"], $oldfields["cer"], $oldfields["log"], $oldfields["ren"], $oldfields["pros"], $oldfields["cms"],  $oldfields["sta"], $oldfields["bud"], $oldfields["inv"], $oldfields["ainv"], $oldfields["cos"], $oldfields["comp"], $oldfields["mst"], $oldfields["ost"], $oldfields["item"]);

		$old_field_order = decode_field_array($row["projectquery_field_order"]);
		$old_query_order = decode_field_array($row["projectquery_order"]);

		//remove fields not present anymore
		foreach($old_field_order as $key=>$fieldname)
		{
			if(!array_key_exists($fieldname, $oldfields))
			{
				foreach($old_field_order as $key=>$value)
				{
					if($value == $fieldname)
					{
						unset($old_field_order[$key]);
					}
				}

				foreach($old_query_order as $key=>$value)
				{
					if($value == $fieldname)
					{
						unset($old_query_order[$key]);
					}
				}
			}
		}

		//add new elements at the end of the arrays
		foreach($oldfields as $fieldname=>$caption)
		{
			if(!in_array($fieldname, $old_field_order))
			{
				$old_field_order[] = $fieldname;
			}
			if(!in_array($fieldname, $old_query_order))
			{
				$old_query_order[] = $fieldname;
			}
		}



		$field_order = encode_field_array($old_field_order);
		$query_order = encode_field_array($old_query_order);
		
		//update query order and field order
		$sql = "Update projectqueries SET " . 
			   "projectquery_field_order = " . dbquote($field_order) . ", " . 
			   "projectquery_order = " . dbquote($query_order) . ", " . 
			   "date_modified = " . dbquote(date("Y-m-d H:s:i")) . ", " . 
			   "user_modified = " . dbquote(user_login()) .
		       " where projectquery_id = " . $query_id;


		$result = mysql_query($sql) or dberror($sql);
	}
}

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new form("projectqueries", "Field Selector");
$form->add_hidden("save_form", "1");
$form->add_hidden("query_id", $query_id);
$form->add_hidden("t", param("t"));


$form->add_label("L1", "", "", "Select the Fields to be included in the Query");

foreach($fields as $key=>$caption)
{
	if(array_key_exists($key,$selected_fields))
	{
		$form->add_checkbox($key, $caption, true);
	}
	else
	{
		$form->add_checkbox($key, $caption, false);
	}
	
}

$form->add_input_submit("submit", "Save Selection", 0);

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Process buttons
*********************************************************************/ 

/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page_Modal("query_generator");

//require "include/benchmark_page_actions.php";

$page->header();
$page->title("Field Selector");

$form->render();

if(param("save_form"))
{
?>

<script languege="javascript">
var back_link = "project_queries_query_fields.php?query_id=<?php echo $query_id;?>"; 
$.nyroModalRemove();
</script>

<?php
}
$page->footer();
