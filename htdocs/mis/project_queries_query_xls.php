<?php
/********************************************************************

    project_queries_query_xls.php

    Generate Excel File from Query Definition

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2014-09-14
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2014-09-14
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if(!has_access("can_perform_all_queries") 
  and !has_access("can_perform_projects_query"))
{
	redirect("noaccess.php");
}
require_once "include/projects_query_check_access.php";

require_once "../include/xls/Writer.php";
require_once "include/projects_query_get_functions.php";


/********************************************************************
    prepare all data needed
*********************************************************************/
if(!param("query_id"))
{
	exit;
}

//update filter in case elements are missing

$filter = get_query_filter($query_id);

$new_filter["cl"] =  $filter["cl"]; //client
$new_filter["fr"] =  $filter["fr"]; //owner
$new_filter["supp"] =  $filter["supp"]; //supplier
$new_filter["forw"] =  $filter["forw"]; //forwarder
$new_filter["hs"] =  $filter["hs"]; //treatment state
$new_filter["fos"] =  $filter["fos"]; //from global project status
$new_filter["tos"] =  $filter["tos"]; //to global project status

if(array_key_exists("fosd", $filter))
{
	$new_filter["fosd"] =  $filter["fosd"]; //from project development status
	$new_filter["tosd"] =  $filter["tosd"]; //to project development status
	$new_filter["fosl"] =  $filter["fosl"]; //from project logistic status
	$new_filter["tosl"] =  $filter["tosl"]; //to project logistic status
}
else
{
	$new_filter["fosd"] =  ""; //from project development status
	$new_filter["tosd"] =  ""; //to project development status
	$new_filter["fosl"] =  ""; //from project logistic status
	$new_filter["tosl"] =  ""; //to project logistic status
}
$new_filter["sufrom"] =  $filter["sufrom"]; // submitted from
$new_filter["suto"] =  $filter["suto"]; // submitted to
$new_filter["opfrom"] =  $filter["opfrom"]; // opened from
$new_filter["opto"] =  $filter["opto"]; // opened to
$new_filter["clfrom"] =  $filter["clfrom"]; // closed from
$new_filter["clto"] =  $filter["clto"]; // closed to
$new_filter["cafrom"] =  $filter["cafrom"]; // cancelled from
$new_filter["cato"] =  $filter["cato"]; // cancelled to
$new_filter["pshe"] =  $filter["pshe"]; //only projects in the list of project sheets
$new_filter["latp"] =  $filter["latp"]; //only the latest project of POS locations
$new_filter["lopr"] =  $filter["lopr"]; //locally produced
$new_filter["typb1"] =  $filter["typb1"]; //POS with Store Furniture
$new_filter["typb2"] =  $filter["typb2"]; //POS with SIS Furniture
$new_filter["idvs"] =  $filter["idvs"]; //Visuals

//add filters that were added to the system after having gone online
if(array_key_exists("noln", $filter))
{
	$new_filter["agrfrom"] =  $filter["agrfrom"]; // agreed opening from
	$new_filter["agrto"] =  $filter["agrto"]; // agreed opening to
	
	$new_filter["noln"] =  $filter["noln"]; //no ln needed
	$new_filter["lnre"] =  $filter["lnre"]; //LN rejected
	$new_filter["nocer"] =  $filter["nocer"]; //no cer/af needed
	$new_filter["cerre"] =  $filter["cerre"]; //CER/AF rejected

	
}
else
{
	$new_filter["agrfrom"] =  ""; // agreed opening from
	$new_filter["agrto"] =  ""; // agreed opening to
	
	$new_filter["noln"] =  0; //no ln needed
	$new_filter["lnre"] =  0; //LN rejected
	$new_filter["nocer"] =  0; //no cer/af needed
	$new_filter["cerre"] =  0; //CER/AF rejected
}

$new_filter["ct"] =  $filter["ct"]; //client types
$new_filter["re"] =  $filter["re"]; //geografical region
$new_filter["gr"] =  $filter["gr"]; // Supplied region
$new_filter["co"] =  $filter["co"]; // country
$new_filter["ci"] =  $filter["ci"]; // city
$new_filter["pct"] =  $filter["pct"]; // Legal Type
$new_filter["pk"] =  $filter["pk"]; // project Type
$new_filter["ptsc"] =  $filter["ptsc"]; // project Type subclasses
$new_filter["pt"] =  $filter["pt"]; // POS Type
$new_filter["sc"] =  $filter["sc"]; // POS Subclass
$new_filter["pl"] =  $filter["pl"]; // Product line
$new_filter["fscs"] =  $filter["fscs"]; // Product line subcalss
$new_filter["ts"] =  $filter["ts"]; //treatment state
$new_filter["rtc"] =  $filter["rtc"]; // Project Leader
$new_filter["lrtc"] =  $filter["lrtc"]; // Local project Leader
$new_filter["dsup"] =  $filter["dsup"]; // Design supervisor
$new_filter["rto"] =  $filter["rto"]; // Logistics Coordinator
if(array_key_exists("dcontr", $filter))
{
	$new_filter["dcontr"] =  $filter["dcontr"]; // Design Contractor
}
$new_filter["dcs"] =  $filter["dcs"]; // Distribution Channel
$new_filter["dos"] =  $filter["dos"]; // Design Objectives
$new_filter["cmgr"] =  $filter["cmgr"]; // Cost Monitoring Groups
$new_filter["icat"] =  $filter["icat"]; // Item categories
$new_filter["item"] =  $filter["item"]; // Items
$new_filter["ar"] =  $filter["ar"]; // Neighbourhood Areas
$new_filter["cmsst"] =  $filter["cmsst"]; // CMS State
$new_filter["flagship"] =  $filter["flagship"]; // Flagship Option

$sql = "update projectqueries " . 
		   "set projectquery_filter = " . dbquote(serialize($new_filter)) . 
		   " where projectquery_id = " . param("query_id");

$result = mysql_query($sql) or dberror($sql);


$projectquery = get_query_name($query_id);
$query_name = str_replace(" ", "", $projectquery["name"]);

include("project_queries_get_query_params.php");



$project_states = array();
$project_states[1] = "in progress";
$project_states[2] = "on hold";
$project_states[4] = "operating";
$project_states[6] = "cancelled";

$handling_states = array();
$handling_states[1] = "ongoing projects without an actual opening date";
$handling_states[2] = "ongoing projects with an actual opening date";
$handling_states[3] = "projects with a closing date";
$handling_states[4] = "ongoing projects with a project sheet";


$cms_states = array();
$cms_states[1] = "Completion overdue";
$cms_states[2] = "Approval overdue";
$cms_states[3] = "Completion or Approval overdue";
$cms_states[4] = "Completion and Approval overdue";
$cms_states[5] = "Logistics CMS completed";
$cms_states[6] = "Development CMS completed";

$milestone_filter = get_query_milestone_filter($query_id);



/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "query_" . $query_name . "_" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename); 

$xls->setVersion(8);

$sheet =& $xls->addWorksheet("Project Query");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setPaper(8); //A3
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
//standard cell background color

$xls->setCustomColor(8, $zebra_color[0], $zebra_color[1], $zebra_color[2]);

$f_header =& $xls->addFormat();
$f_header->setSize(12);
$f_header->setAlign('left');
$f_header->setBold();

$f_title =& $xls->addFormat();
$f_title->setSize(10);
$f_title->setAlign('left');
$f_title->setBold();

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_group_title =& $xls->addFormat();
$f_group_title->setSize(8);
$f_group_title->setAlign('left');
$f_group_title->setBold();

$f_empty =& $xls->addFormat();
$f_empty->setSize(8);
$f_empty->setAlign('left');
$f_empty->setBorder(1);

$f_text_without_border =& $xls->addFormat();
$f_text_without_border->setSize(8);
$f_text_without_border->setAlign('left');
$f_text_without_border->setBorder(0);

$f_text =& $xls->addFormat();
$f_text->setSize(8);
$f_text->setAlign('left');
$f_text->setBorder(1);

$f_text_zebra =& $xls->addFormat();
$f_text_zebra->setSize(8);
$f_text_zebra->setAlign('left');
$f_text_zebra->setBorder(1);
$f_text_zebra->setFgColor(8);

$f_text_bold =& $xls->addFormat();
$f_text_bold->setSize(8);
$f_text_bold->setAlign('left');
$f_text_bold->setBorder(1);
$f_text_bold->setBold();

$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);
$f_number->setNumFormat(41);

$f_number_zebra =& $xls->addFormat();
$f_number_zebra->setSize(8);
$f_number_zebra->setAlign('right');
$f_number_zebra->setBorder(1);
$f_number_zebra->setNumFormat(41);
$f_number_zebra->setFgColor(8);

$f_number_bold =& $xls->addFormat();
$f_number_bold->setSize(8);
$f_number_bold->setAlign('right');
$f_number_bold->setBorder(0);
$f_number_bold->setNumFormat(0);
$f_number_bold->setBold();


$xls->setCustomColor(12, 219, 17, 17);
$f_number_red =& $xls->addFormat();
$f_number_red->setSize(8);
$f_number_red->setAlign('right');
$f_number_red->setBorder(1);
$f_number_red->setColor("white");
$f_number_red->setFgColor(12);
$f_number_red->setPattern(1);
$f_number_red->setBold();

$xls->setCustomColor(13, 246, 194, 54);
$f_number_orange =& $xls->addFormat();
$f_number_orange->setSize(8);
$f_number_orange->setAlign('right');
$f_number_orange->setBorder(1);
$f_number_orange->setFgColor(13);
$f_number_orange->setPattern(1);
$f_number_orange->setBold();

$xls->setCustomColor(14, 255, 246, 14);
$f_number_yellow =& $xls->addFormat();
$f_number_yellow->setSize(8);
$f_number_yellow->setAlign('right');
$f_number_yellow->setBorder(1);
$f_number_yellow->setFgColor(14);;
$f_number_yellow->setPattern(1);
$f_number_yellow->setBold();


$xls->setCustomColor(16, 4, 143, 31);
$f_number_green =& $xls->addFormat();
$f_number_green->setSize(8);
$f_number_green->setAlign('right');
$f_number_green->setBorder(1);
$f_number_green->setColor("white");
$f_number_green->setFgColor(16);;
$f_number_green->setPattern(1);
$f_number_green->setBold();


$f_decimal2 =& $xls->addFormat();
$f_decimal2->setSize(8);
$f_decimal2->setAlign('right');
$f_decimal2->setBorder(1);
$f_decimal2->setNumFormat(43);

$f_decimal2_zebra =& $xls->addFormat();
$f_decimal2_zebra->setSize(8);
$f_decimal2_zebra->setAlign('right');
$f_decimal2_zebra->setBorder(1);
$f_decimal2_zebra->setNumFormat(43);
$f_decimal2_zebra->setFgColor(8);



$f_decimal2_red =& $xls->addFormat();
$f_decimal2_red->setSize(8);
$f_decimal2_red->setAlign('right');
$f_decimal2_red->setBorder(1);
$f_decimal2_red->setColor("white");
$f_decimal2_red->setFgColor(12);
$f_decimal2_red->setPattern(1);
$f_decimal2_red->setNumFormat(43);

$f_decimal2_orange =& $xls->addFormat();
$f_decimal2_orange->setSize(8);
$f_decimal2_orange->setAlign('right');
$f_decimal2_orange->setBorder(1);
$f_decimal2_orange->setFgColor(13);
$f_decimal2_orange->setPattern(1);
$f_decimal2_orange->setBold();
$f_decimal2_orange->setNumFormat(43);

$f_decimal2_yellow =& $xls->addFormat();
$f_decimal2_yellow->setSize(8);
$f_decimal2_yellow->setAlign('right');
$f_decimal2_yellow->setBorder(1);
$f_decimal2_yellow->setFgColor(14);;
$f_decimal2_yellow->setPattern(1);
$f_decimal2_yellow->setBold();
$f_decimal2_yellow->setNumFormat(43);

$f_decimal2_green =& $xls->addFormat();
$f_decimal2_green->setSize(8);
$f_decimal2_green->setAlign('right');
$f_decimal2_green->setBorder(1);
$f_decimal2_green->setColor("white");
$f_decimal2_green->setFgColor(16);;
$f_decimal2_green->setPattern(1);
$f_decimal2_green->setBold();
$f_decimal2_green->setNumFormat(43);

$f_decimal2_bold =& $xls->addFormat();
$f_decimal2_bold->setSize(8);
$f_decimal2_bold->setAlign('right');
$f_decimal2_bold->setBorder(1);
$f_decimal2_bold->setBold();
//$f_decimal2_bold->setNumFormat(43);
$f_decimal2_bold->setNumFormat(41);

$f_group_decimal2_bold =& $xls->addFormat();
$f_group_decimal2_bold->setSize(8);
$f_group_decimal2_bold->setAlign('right');
$f_group_decimal2_bold->setBorder(0);
$f_group_decimal2_bold->setBold();
//$f_group_decimal2_bold->setNumFormat(43);
$f_group_decimal2_bold->setNumFormat(41);


$f_percent2 =& $xls->addFormat();
$f_percent2->setSize(8);
$f_percent2->setAlign('right');
$f_percent2->setBorder(1);
$f_percent2->setNumFormat(10);


$f_percent2_zebra =& $xls->addFormat();
$f_percent2_zebra->setSize(8);
$f_percent2_zebra->setAlign('right');
$f_percent2_zebra->setBorder(1);
$f_percent2_zebra->setNumFormat(10);
$f_percent2_zebra->setFgColor(8);


$f_percent2_red =& $xls->addFormat();
$f_percent2_red->setSize(8);
$f_percent2_red->setAlign('right');
$f_percent2_red->setBorder(1);
$f_percent2_red->setColor("white");
$f_percent2_red->setFgColor(12);
$f_percent2_red->setPattern(1);
$f_percent2_red->setBold();
$f_percent2_red->setNumFormat(10);

$f_percent2_orange =& $xls->addFormat();
$f_percent2_orange->setSize(8);
$f_percent2_orange->setAlign('right');
$f_percent2_orange->setBorder(1);
$f_percent2_orange->setFgColor(13);
$f_percent2_orange->setPattern(1);
$f_percent2_orange->setBold();
$f_percent2_orange->setNumFormat(10);

$f_percent2_yellow =& $xls->addFormat();
$f_percent2_yellow->setSize(8);
$f_percent2_yellow->setAlign('right');
$f_percent2_yellow->setBorder(1);
$f_percent2_yellow->setFgColor(14);;
$f_percent2_yellow->setPattern(1);
$f_percent2_yellow->setBold();
$f_percent2_yellow->setNumFormat(10);

$f_percent2_green =& $xls->addFormat();
$f_percent2_green->setSize(8);
$f_percent2_green->setAlign('right');
$f_percent2_green->setBorder(1);
$f_percent2_green->setColor("white");
$f_percent2_green->setFgColor(16);;
$f_percent2_green->setPattern(1);
$f_percent2_green->setBold();
$f_percent2_green->setNumFormat(10);


$f_group_percent2_bold =& $xls->addFormat();
$f_group_percent2_bold->setSize(8);
$f_group_percent2_bold->setAlign('right');
$f_group_percent2_bold->setNumFormat(10);
$f_group_percent2_bold->setBold();


$f_date =& $xls->addFormat();
$f_date->setSize(8);
$f_date->setAlign('left');
$f_date->setBorder(1);
$f_date->setNumFormat('14');

$f_date_zebra =& $xls->addFormat();
$f_date_zebra->setSize(8);
$f_date_zebra->setAlign('left');
$f_date_zebra->setBorder(1);
$f_date_zebra->setNumFormat('14');
$f_date_zebra->setFgColor(8);

$f_date_bold =& $xls->addFormat();
$f_date_bold->setSize(8);
$f_date_bold->setAlign('left');
$f_date_bold->setBorder(1);
$f_date_bold->setBold();

$f_unused =& $xls->addFormat();
$f_unused->setSize(8);
$f_unused->setBorder(1);
$f_unused->setPattern(2);
$f_unused->setBgColor('silver');

$f_border_left=& $xls->addFormat();
$f_border_left->setTop(1);
$f_border_left->setBottom(1);
$f_border_left->setLeft(1);
$f_border_left->setSize(8);
$f_border_left->setAlign('left');
$f_border_left->setBold();

$f_border_right=& $xls->addFormat();
$f_border_right->setTop(1);
$f_border_right->setBottom(1);
$f_border_right->setRight(1);


$f_border_middle=& $xls->addFormat();
$f_border_middle->setTop(1);
$f_border_middle->setBottom(1);


/********************************************************************
    write all captions
*********************************************************************/
$row_index = 2;
$cell_index = 0;

//set initial column widths
$col_widths = array();
for($i=0;$i<count($captions);$i++)
{
	$col_widths[$i] = 8;
}

$sheet->write(0, 0, $projectquery["name"] . " (" . date("d.m.Y") . ")", $f_header);

$query_has_filter = 0;
if($print_query_filter == 1)
{
		
	if($query_filter["cl"] > 0) // client address
	{
		$client = "";
		$sql_f = "select address_company, address_place, country_name " . 
			     "from addresses " . 
			     "left join countries on country_id = address_country " . 
			     "where address_id = " . $query_filter["cl"];
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$client = $row_f["address_company"] . ", " . $row_f["address_place"] . ", " . $row_f["country_name"];
		}

		if($client)
		{
			$sheet->write($row_index, 0, "Client: " . $client, $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}
	if($query_filter["fr"] > 0) // franchisee address
	{
		$client = "";
		$sql_f = "select address_company, address_place, country_name " . 
			     "from addresses " . 
			     "left join countries on country_id = address_country " . 
			     "where address_id = " . $query_filter["fr"];
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$client = $row_f["address_company"] . ", " . $row_f["address_place"] . ", " . $row_f["country_name"];
		}

		if($client)
		{
			$sheet->write($row_index, 0, "Franchisee: " . $client, $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}

	if($query_filter["supp"] > 0) // supplier address
	{
		$supp = "";
		$sql_f = "select address_company, address_place, country_name " . 
			     "from addresses " . 
			     "left join countries on country_id = address_country " . 
			     "where address_id = " . $query_filter["supp"];
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$supp = $row_f["address_company"] . ", " . $row_f["address_place"] . ", " . $row_f["country_name"];
		}

		if($supp)
		{
			$sheet->write($row_index, 0, "Supplier: " . $supp, $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}

	if($query_filter["forw"] > 0) // forwarder address
	{
		$forw = "";
		$sql_f = "select address_company, address_place, country_name " . 
			     "from addresses " . 
			     "left join countries on country_id = address_country " . 
			     "where address_id = " . $query_filter["forw"];
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$forw = $row_f["address_company"] . ", " . $row_f["address_place"] . ", " . $row_f["country_name"];
		}

		if($forw)
		{
			$sheet->write($row_index, 0, "Forwarder: " . $forw, $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}

	
	if($query_filter["hs"] > 0) // handling state
	{
		$sheet->write($row_index, 0, "Projects: " . $handling_states[$query_filter["hs"]], $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["fos"] > 0) // from global project status
	{
		$sheet->write($row_index, 0, "From Global Project Status: " . $query_filter["fos"], $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["tos"] > 0) // to global project status
	{
		$sheet->write($row_index, 0, "To Global Project Status: " . $query_filter["tos"], $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["fosd"] > 0) // from project development status
	{
		$sheet->write($row_index, 0, "From Project Development Status: " . $query_filter["fosd"], $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["tosd"] > 0) // to project development status
	{
		$sheet->write($row_index, 0, "To Project Development Status: " . $query_filter["tosd"], $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["fosl"] > 0) // from project logistic status
	{
		$sheet->write($row_index, 0, "From Project Logistic Status: " . $query_filter["fosl"], $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["tosl"] > 0) // to project logistic status
	{
		$sheet->write($row_index, 0, "To Project Logistic Status: " . $query_filter["tosl"], $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["sufrom"] != '') // submitted from
	{
		$sheet->write($row_index, 0, "Submitted from: " . to_system_date($query_filter["sufrom"]), $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["suto"] != '') // submitted to
	{
		$sheet->write($row_index, 0, "Submitted to: " . to_system_date($query_filter["suto"]), $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["opfrom"] != '') // opened from
	{
		$sheet->write($row_index, 0, "Actual Opening Date from: " . to_system_date($query_filter["opfrom"]), $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["opto"] != '') // opened to
	{
		$sheet->write($row_index, 0, "Actual Opening Date to: " . to_system_date($query_filter["opto"]), $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["agrfrom"] != '') // opened from
	{
		$sheet->write($row_index, 0, "Agreed Opening Date from: " . to_system_date($query_filter["agrfrom"]), $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["agrto"] != '') // opened to
	{
		$sheet->write($row_index, 0, "Agreed Opening Date to: " . to_system_date($query_filter["agrto"]), $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["clfrom"] != '') // closed from
	{
		$sheet->write($row_index, 0, "Closed from: " . to_system_date($query_filter["clfrom"]), $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["clto"] != '') // closed to
	{
		$sheet->write($row_index, 0, "Closed to: " . to_system_date($query_filter["clto"]), $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["cafrom"] != '') // cancelled from
	{
		$sheet->write($row_index, 0, "Cancelled from: " . to_system_date($query_filter["cafrom"]), $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["cato"] != '') // cancelled to
	{
		$sheet->write($row_index, 0, "Cancelled to: " . to_system_date($query_filter["cato"]), $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["latp"] > 0) // only the latest operating project of POS locations
	{
		$sheet->write($row_index, 0, "Only the latest operating project of POS locations: yes", $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["pshe"] > 0) // only project in the list of project sheets
	{
		$sheet->write($row_index, 0, "Only projects included in the list of project sheets: yes", $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["lopr"] > 0) // POS with locally produced projects
	{
		$sheet->write($row_index, 0, "Only POS with locally produced projects: yes", $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}


	if($query_filter["typb1"] > 0) // PSO with Store Furniture
	{
		$sheet->write($row_index, 0, "Only POS with STORE Furniture", $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["typb2"] > 0) // POS with SIS Furniture
	{
		$sheet->write($row_index, 0, "Only POS with SIS Furniture", $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["idvs"] > 0) // POS Locations with Visuals
	{
		$sheet->write($row_index, 0, "Only POS with Visuals", $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}
	
	if($query_filter["noln"] > 0) //No LN needed
	{
		$sheet->write($row_index, 0, "Projects where no LN is needed", $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["lnre"] > 0) //LN rejected
	{
		$sheet->write($row_index, 0, "Projects with rejected LN", $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["nocer"] > 0) //no cer/af needed
	{
		$sheet->write($row_index, 0, "Projects where no CER/AF is needed", $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["cerre"] > 0) //CER/AF rejected
	{
		$sheet->write($row_index, 0, "Project with rejected CER/AF", $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["ct"]) //Client Types
	{
		$filter = "";
		$filter_clienttypes = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["ct"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_clienttypes .= $value . ",";
			}
		}
		$filter_clienttypes = "(" . substr($filter_clienttypes, 0, strlen($filter_clienttypes)-1) . ")";
		if($filter_clienttypes != "()")
		{
			$filter .= "client_type_id in " . $filter_clienttypes . " ";
		}

		$sql_f = "select client_type_code from client_types where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["client_type_code"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Client Types: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}
	if($query_filter["re"]) //Geographical region
	{
		$filter = "";
		$filter_salesregions = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["re"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_salesregions .= $value . ",";
			}
		}
		$filter_salesregions = "(" . substr($filter_salesregions, 0, strlen($filter_salesregions)-1) . ")";
		if($filter_salesregions != "()")
		{
			$filter .= "salesregion_id in " . $filter_salesregions . " ";
		}

		$sql_f = "select salesregion_name from salesregions where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["salesregion_name"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Geographical Regions: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}
	if($query_filter["gr"]) //Supplied region
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["gr"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "region_id in " . $filter_string . " ";
		}

		$sql_f = "select region_name from regions where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["region_name"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Supplied Regions: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}
	if($query_filter["co"]) //countries
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["co"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "country_id in " . $filter_string . " ";
		}

		$sql_f = "select country_name from countries where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["country_name"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Countries: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}
	if($query_filter["ci"]) //cities
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["ci"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}

		if($filter_string)
		{
			$filter_string = substr($filter_string, 0, strlen($filter_string)-1);
			$sheet->write($row_index, 0, "Cities: " . $filter_string, $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}
	if($query_filter["pct"]) //cost type, legal type
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["pct"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "project_costtype_id in " . $filter_string . " ";
		}

		$sql_f = "select project_costtype_text from project_costtypes where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["project_costtype_text"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Legal Types: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}
	if($query_filter["pk"]) // project Type
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["pk"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "projectkind_id in " . $filter_string . " ";
		}

		$sql_f = "select projectkind_name from projectkinds where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["projectkind_name"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Project Types: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}
	if($query_filter["ptsc"]) // project Type subclasses
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["ptsc"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "project_type_subclass_id in " . $filter_string . " ";
		}

		$sql_f = "select project_type_subclass_name from project_type_subclasses where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["project_type_subclass_name"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Project Type Subclasses: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}
	if($query_filter["pt"]) // product line, furniture type
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["pt"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= "'" . $value . "',";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "postype_name in " . $filter_string . " ";
		}

		$sql_f = "select DISTINCT postype_name from postypes where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["postype_name"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "POS Types: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}
	if($query_filter["sc"]) // POS Type Subclass
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["sc"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "possubclass_id in " . $filter_string . " ";
		}

		$sql_f = "select possubclass_name from possubclasses where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["possubclass_name"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "POS Type Subclasses: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}
	if($query_filter["pl"]) // product line, furniture type
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["pl"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "product_line_id in " . $filter_string . " ";
		}

		$sql_f = "select product_line_name from product_lines where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["product_line_name"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Furnitue Types: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}
	if($query_filter["fscs"]) // furniture subclasses
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["fscs"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "productline_subclass_id in " . $filter_string . " ";
		}

		$sql_f = "select productline_subclass_name from productline_subclasses where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["productline_subclass_name"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Furnitue Subclasses: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}

	if($query_filter["ts"]) // treatment states
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["ts"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "project_state_id in " . $filter_string . " ";
		}

		$sql_f = "select project_state_text from project_states where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["project_state_text"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Treatment States: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}

	
	if($query_filter["rtc"]) // project Leaders
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["rtc"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "user_id in " . $filter_string . " ";
		}

		$sql_f = "select concat(user_name, ' ', user_firstname) as username from users where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["username"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Project Leaders: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}


	}
	if($query_filter["lrtc"]) // local project Leaders
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["lrtc"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "user_id in " . $filter_string . " ";
		}

		$sql_f = "select concat(user_name, ' ', user_firstname) as username from users where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["username"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Local Project Leaders: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}


	}
	if($query_filter["dsup"]) // design supervisor
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["dsup"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "user_id in " . $filter_string . " ";
		}

		$sql_f = "select concat(user_name, ' ', user_firstname) as username from users where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["username"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Design Supervisors: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}


	}
	if($query_filter["rto"]) // logistics coordinator
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["rto"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "user_id in " . $filter_string . " ";
		}

		$sql_f = "select concat(user_name, ' ', user_firstname) as username from users where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["username"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Logistics Coordinators: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}


	}


	if($query_filter["dcontr"]) // Design Contractor
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["dcontr"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "user_id in " . $filter_string . " ";
		}

		$sql_f = "select concat(address_company, ' ', user_name, ' ', user_firstname) as username from users left join addresses on address_id = user_address where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["username"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Design Contractors: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}


	}
	if($query_filter["dcs"]) // distribution channel
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["dcs"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "mps_distchannel_id in " . $filter_string . " ";
		}

		$sql_f = "select mps_distchannel_code from mps_distchannels where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["mps_distchannel_code"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Distribution Channels: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}
	if($query_filter["dos"]) // Design Objectives
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["dos"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "design_objective_item_id in " . $filter_string . " ";
		}

		$sql_f = "select design_objective_item_id, design_objective_item_name, " .
						 "    design_objective_group_name, postype_name " .
						 "from design_objective_items left join design_objective_groups on design_objective_item_group = design_objective_group_id " .
						 "    left join postypes on design_objective_group_postype = postype_id " . 
						 " where " .  $filter .
						 " order by postype_name, design_objective_group_name, design_objective_item_name ";

		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["postype_name"] . " - " . $row_f["design_objective_group_name"] . " - " . $row_f["design_objective_item_name"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Design Objectives: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}
	
	if($query_filter["cmgr"]) // Cost Monitoring Groups
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["cmgr"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "costmonitoringgroup_id in " . $filter_string . " ";
		}

		$sql_f = "select costmonitoringgroup_id, costmonitoringgroup_text " . 
						 "from costmonitoringgroups " . 
						 " where " .  $filter .
						 " order by costmonitoringgroup_text";

		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["costmonitoringgroup_text"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Cost Monitoring Groups: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}
	if($query_filter["icat"]) // Item Categories
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["icat"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "item_category_id in " . $filter_string . " ";
		}

		$sql_f = "select item_category_id, item_category_name " . 
						 "from item_categories " . 
						 " where " .  $filter .
						 " order by item_category_name";

		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["item_category_name"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Item Categories: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}

	if($query_filter["item"]) // Items
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["item"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "item_id in " . $filter_string . " ";
		}

		$sql_f = "select item_id, item_code " . 
						 "from items " . 
						 " where " .  $filter .
						 " order by item_code";

		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["item_code"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Items: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}

	if($query_filter["ar"]) //neighbourhood areas
	{
		$filter = "";
		$filter_string = "";
		$filter_array = array();
		$tmp = explode("-", $query_filter["ar"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_string .= $value . ",";
			}
		}
		$filter_string = "(" . substr($filter_string, 0, strlen($filter_string)-1) . ")";
		if($filter_string != "()")
		{
			$filter .= "posareatype_id in " . $filter_string . " ";
		}

		$sql_f = "select posareatype_name from posareatypes where " . $filter;
		$res_f = mysql_query($sql_f) or dberror($sql_f);
		while ($row_f = mysql_fetch_assoc($res_f))
		{
			$filter_array[] = $row_f["posareatype_name"];
		}

		if(count($filter_array) > 0)
		{
			$sheet->write($row_index, 0, "Neighbourhood Areas: " . implode(", ", $filter_array), $f_text_without_border);
			$row_index++;
			$query_has_filter = 1;
		}

	}

	if(count($milestone_filter) > 0)
	{
		$first = true;
		$logical = "";
		foreach($milestone_filter as $milestone_id=>$mfilter_array)
		{
			
			
			if($mfilter_array["mfrom" . $milestone_id] and $mfilter_array["mto" . $milestone_id])
			{
				
				if($first == false and $mfilter_array["logical" . $milestone_id] == 2)
				{
					$logical = "or ";
				}
				elseif($first == false)
				{
					$logical = "and ";
				}

				$sql_f = "select milestone_code from milestones " . 
					     " where milestone_id = " . $milestone_id;
				$res_f = mysql_query($sql_f) or dberror($sql_f);
				if ($row_f = mysql_fetch_assoc($res_f))
				{
					
					$text = $logical . "Milestone " . $row_f["milestone_code"] . ": From " . to_system_date($mfilter_array["mfrom" . $milestone_id]) . " to " . to_system_date($mfilter_array["mto" . $milestone_id]);
					
					
					$sheet->write($row_index, 0, $text, $f_text_without_border);
					$row_index++;
					$query_has_filter = 1;
					$first = false;
				}

			}
			elseif($mfilter_array["mfrom" . $milestone_id])
			{
				if($first == false and $mfilter_array["logical" . $milestone_id] == 2)
				{
					$logical = "or ";
				}
				elseif($first == false)
				{
					$logical = "and ";
				}

				$sql_f = "select milestone_code from milestones " . 
					     " where milestone_id = " . $milestone_id;
				$res_f = mysql_query($sql_f) or dberror($sql_f);
				if ($row_f = mysql_fetch_assoc($res_f))
				{
					$text = $logical . "Milestone " . $row_f["milestone_code"] . ": From " . to_system_date($mfilter_array["mfrom" . $milestone_id]);
					
					
					$sheet->write($row_index, 0, $text, $f_text_without_border);
					$row_index++;
					$query_has_filter = 1;
					$first = false;
				}

			}
			elseif($mfilter_array["mto" . $milestone_id])
			{
				if($first == false and $mfilter_array["logical" . $milestone_id] == 2)
				{
					$logical = "or ";
				}
				elseif($first == false)
				{
					$logical = "and ";
				}

				$sql_f = "select milestone_code from milestones " . 
					     " where milestone_id = " . $milestone_id;
				$res_f = mysql_query($sql_f) or dberror($sql_f);
				if ($row_f = mysql_fetch_assoc($res_f))
				{
					
					$text = $logical . "Milestone " . $row_f["milestone_code"] . ": To " . to_system_date($mfilter_array["mto" . $milestone_id]);
					

					$sheet->write($row_index, 0, $text, $f_text_without_border);
					$row_index++;
					$query_has_filter = 1;
					$first = false;
				}

			}
			
			if($mfilter_array["mhasdate" . $milestone_id])
			{
				if($first == false and $mfilter_array["logical" . $milestone_id] == 2)
				{
					$logical = "or ";
				}
				elseif($first == false)
				{
					$logical = "and ";
				}

				$sql_f = "select milestone_code from milestones " . 
					     " where milestone_id = " . $milestone_id;
				$res_f = mysql_query($sql_f) or dberror($sql_f);
				if ($row_f = mysql_fetch_assoc($res_f))
				{
					
					$text = $logical . "Milestone " . $row_f["milestone_code"] . ": has a date";
					$sheet->write($row_index, 0, $text, $f_text_without_border);
					$row_index++;
					$query_has_filter = 1;
					$first = false;
				}

			}

			
		}
	}

	
	if($query_filter["cmsst"] > 0) // CMS State
	{
		$sheet->write($row_index, 0, "CMS Status: " . $cms_states[$query_filter["cmsst"]], $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}

	if($query_filter["flagship"] > 0) // Flag Ship Option
	{
		$sheet->write($row_index, 0, "Only flag ship projects", $f_text_without_border);
		$row_index++;
		$query_has_filter = 1;
	}
	
	
}

if($query_has_filter == 1)
{
	$row_index++;
}


$sheet->setRow($row_index, 150);

$sheet->write($row_index, $cell_index, "Line Number", $f_caption);
$cell_index++;

foreach($captions as $key=>$caption)
{
	$sheet->write($row_index, $cell_index, $caption, $f_caption);
	$cell_index++;
}



if($show_number_of_projects == 1)
{
	$sheet->write($row_index, $cell_index, "Counter", $f_caption);	
	$cell_index_number_of_projects = $cell_index;
}

//$sheet->writeRow($row_index, $cell_index+1, $captions, $f_caption);
$colum_header_row_index = $row_index;

if($sql)
{
	$zebra_on = false;
	$cell_format_extension = "";

	$counter = 1;
	$counter_group = 1;

	$total_projects = 0;
	$total_projects_per_group01 = 0;
	$total_projects_per_group02 = 0;
	$total_projects_per_group03 = 0;

	$old_group_01 = "";
	$old_group_02 = "";
	$old_group_03 = "";
	
	$group_totals = array();
	$group_totals_cellindex = array();
	
	$group_totals02 = array();
	$group_totals02_cellindex = array();

	$group_totals03 = array();
	$group_totals03cellindex = array();
	
	foreach($totalisation_fields as $field=>$value)
	{
		if(strpos($db_info["attributes"][$field], "IF(") === 0)
		{
			$tmp = explode(" AS ", $field);
			$field_name = $tmp[1];
		}
		else
		{
			$field_name = str_replace(".", "", $field);
		}
		$group_totals[$field] = 0;
		$group_totals_cellindex[$field] = 0;
		$group_totals02[$field] = 0;
		$group_totals02_cellindex[$field] = 0;
		$group_totals03[$field] = 0;
		$group_totals03_cellindex[$field] = 0;
		
		$total_projects_for_avg[$field] = 0;
		$total_projects_per_group_for_avg[$field] = 0;
		$total_projects_per_group02_for_avg[$field] = 0;
		$total_projects_per_group03_for_avg[$field] = 0;
	}


	//group total_fields for data blocks
	if(array_key_exists("item_quantity_block", $data_blocks_captions))
	{
		
		foreach($data_blocks_captions["item_quantity_block"] as $key=>$value)
		{
			$group_totals["item_quantity_block_" . $key] = 0;
			$group_totals02["item_quantity_block_" . $key] = 0;
			$group_totals03["item_quantity_block_" . $key] = 0;
			$item_totals["item_quantity_block_" . $key] = 0;
		}
	}


	
	//print rows

	$subgroup_category_order_state_cell_index = 0;
	$subgroup_category_order_state_row_index = 0;

	$subgroup_category_delivery_state_cell_index = 0;
	$subgroup_category_delivery_state_row_index = 0;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		
		
		
		if($use_zebra == 1 and $zebra_on == true)
		{
			$zebra_on = false;
		}
		elseif($use_zebra == 1 and $zebra_on == false)
		{
			$zebra_on = true;
		}
		
		
		$order_id = $row["order_id"];
		
		$cell_index = 1;
		
		if($subgroup_category_order_state_row_index > 0 and $subgroup_category_order_state_row_index > $subgroup_category_delivery_state_row_index)
		{
			$row_index = $row_index + $subgroup_category_order_state_row_index;
		}
		elseif($subgroup_category_delivery_state_row_index > 0)
		{
			$row_index = $row_index + $subgroup_category_delivery_state_row_index;
		}
		else
		{
			$row_index++;
		}
		$subgroup_category_order_state_row_index = 0;
		$subgroup_category_delivery_state_row_index = 0;

		foreach($selected_field_order as $key=>$field)
		{
			
			
			
			$output_text = 0;

			
			if($db_info["attributes"][$field] == "item_quantity_block")
			{
				
			}
			elseif($db_info["attributes"][$field] == "calculated_content")
			{
				$field_name = str_replace(".", "", $db_info["calculated_content_field"][$field]);

				$output = array();
				$sql_s = $db_info["calculated_content"][$field] . dbquote($row[$db_info["calculated_content_key"][$field]]);
	
				if($db_info["calculated_content_sort_order"][$field])
				{
					$sql_s .= ' order by ' . $db_info["calculated_content_sort_order"][$field];
				}


				$res_s = mysql_query($sql_s) or dberror($sql_s);
				while ($row_s = mysql_fetch_row($res_s))
				{
					$output[] = $row_s[0];
				}
				$output_text = implode(", ", $output);

			}
			elseif($db_info["attributes"][$field] == "content_by_function")
			{

				$field_name = str_replace(".", "", $field);
				$function_name = $db_info["content_by_function"][$field];

				
				$params = array();
				foreach($db_info["content_by_function_params"][$field] as $key=>$value)
				{
					
					if(array_key_exists($value, $row))
					{
						$params[] = $row[$value];
					}
					else
					{
						$params[] = $value;
					}
				}
							
				$function = '$output_text = ' . str_replace('params', implode(', ', $params), $function_name) .';';
				eval($function);
			}
			else
			{
				if(strpos($db_info["attributes"][$field], "IF(") === 0)
				{
					$tmp = explode(' AS ', $db_info["attributes"][$field]);
					$field_name = $tmp[1];
				}
				elseif($db_info["attributes"][$field] == "content_by_subquery")
				{
					$field_name = str_replace(".", "", $db_info["calculated_content_field"][$field]);
				}
				else
				{
					$field_name = str_replace(".", "", $db_info["attributes"][$field]);
				}
			}


			//build totals
			if(array_key_exists($field_name, $totalisation_fields))
			{
				//list totals
				if($db_info["attributes"][$field] == "item_quantity_block")
				{

				}
				elseif($db_info["attributes"][$field] == "calculated_content")
				{
					if(is_numeric($output_text))
					{
						$field_name = str_replace(".", "", $db_info["calculated_content_field"][$field]);
						$totalisation_fields[$field_name] = $totalisation_fields[$field_name] + $output_text;
						$totalisation_fields_column_index[$field_name] = $cell_index;

						$group_totals[$field_name] = $group_totals[$field_name] + $output_text;
						$group_totals_cellindex[$field_name] = $cell_index;

						$group_totals02[$field_name] = $group_totals02[$field_name] + $output_text;
						$group_totals02_cellindex[$field_name] = $cell_index;

						$group_totals03[$field_name] = $group_totals03[$field_name] + $output_text;
						$group_totals03_cellindex[$field_name] = $cell_index;
					}
					else
					{
						$field_name = str_replace(".", "", $db_info["calculated_content_field"][$field]);
						$totalisation_fields[$field_name] = $totalisation_fields[$field_name];
						$totalisation_fields_column_index[$field_name] = $cell_index;

						$group_totals[$field_name] = $group_totals[$field_name];
						$group_totals_cellindex[$field_name] = $cell_index;
						
						$group_totals02[$field_name] = $group_totals02[$field_name];
						$group_totals02_cellindex[$field_name] = $cell_index;
						
						$group_totals03[$field_name] = $group_totals03[$field_name];
						$group_totals03_cellindex[$field_name] = $cell_index;
					}
				}
				elseif($db_info["attributes"][$field] == "content_by_function")
				{
					if(is_numeric($output_text))
					{
						$field_name = str_replace(".", "", $field);

						$totalisation_fields[$field_name] = $totalisation_fields[$field_name] + $output_text;
						$totalisation_fields_column_index[$field_name] = $cell_index;

						$group_totals[$field_name] = $group_totals[$field_name] + $output_text;
						$group_totals_cellindex[$field_name] = $cell_index;
						
						$group_totals02[$field_name] = $group_totals02[$field_name] + $output_text;
						$group_totals02_cellindex[$field_name] = $cell_index;
						
						$group_totals03[$field_name] = $group_totals03[$field_name] + $output_text;
						$group_totals03_cellindex[$field_name] = $cell_index;
					}
					else
					{
						$field_name = str_replace(".", "", $field);
						$totalisation_fields[$field_name] = $totalisation_fields[$field_name];
						$totalisation_fields_column_index[$field_name] = $cell_index;

						$group_totals[$field_name] = $group_totals[$field_name];
						$group_totals_cellindex[$field_name] = $cell_index;
						$group_totals02[$field_name] = $group_totals02[$field_name];
						$group_totals02_cellindex[$field_name] = $cell_index;
						
						$group_totals03[$field_name] = $group_totals03[$field_name];
						$group_totals03_cellindex[$field_name] = $cell_index;
					}
				}
				else
				{
					$totalisation_fields[$field_name] = $totalisation_fields[$field_name] + $row[$field_name];
					$totalisation_fields_column_index[$field_name] = $cell_index;

					$group_totals[$field_name] = $group_totals[$field_name] + $row[$field_name];
					$group_totals_cellindex[$field_name] = $cell_index;
					
					$group_totals02[$field_name] = $group_totals02[$field_name] + $row[$field_name];
					$group_totals02_cellindex[$field_name] = $cell_index;
					
					$group_totals03[$field_name] = $group_totals03[$field_name] + $row[$field_name];
					$group_totals03_cellindex[$field_name] = $cell_index;
				}

				
				if($db_info["attributes"][$field] == "item_quantity_block")
				{
				}

				elseif(is_numeric($output_text) and ($output_text > 0 or $output_text === 0 or $output_text < 0))
				{	
					$total_projects_for_avg[$field_name] = $total_projects_for_avg[$field_name] +1;
					$total_projects_per_group_for_avg[$field_name] = $total_projects_per_group_for_avg[$field_name] +1;
					$total_projects_per_group02_for_avg[$field_name] = $total_projects_per_group02_for_avg[$field_name] +1;
					$total_projects_per_group03_for_avg[$field_name] = $total_projects_per_group03_for_avg[$field_name] +1;
				}
				
			}


			// add group footers
			$empty_lines_comittet = false;
			
			if($query_group03 != '' and array_key_exists($key+2, $selected_field_order ))
			{
				
				$group_printed03 = 0;
				$next_field = $selected_field_order[$key+2];
				
				if(strpos($db_info["attributes"][$next_field], "IF(") === 0)
				{
					$tmp = explode(' AS ', $db_info["attributes"][$next_field]);
					$next_field_name03 = $tmp[1];
				}
				else
				{
					$next_field_name03 = str_replace(".", "", $db_info["attributes"][$next_field]);
				}

				if(strpos($db_info["attributes"][$query_groups[$query_group03]], "IF(") === 0)
				{
					$tmp = explode(' AS ', $db_info["attributes"][$query_groups[$query_group03]]);
					$group_field_name03 =  $tmp[1];
				}
				else
				{
					$group_field_name03 = str_replace(".", "", $db_info["attributes"][$query_groups[$query_group03]]);
				}

				


				if($group_field_name03 == $next_field_name03) // output only if there is grouping of rows
				{
					$concated_old_group03 = $old_group_01 .$old_group_02 .$old_group_03;
					
									
					//$concated_group03 = $row[$group_field_name01] . $row[$group_field_name02] . $row[$next_field_name03];
					$concated_group03 = $row[$next_field_name03];

					
					//if($old_group_03 != $row[$next_field_name03])
					if($concated_old_group03 != $concated_group03)
					{
						if($old_group_03 and $empty_lines_comittet == false)
						{
							if(count($sum_fields) > 0 or $show_number_of_projects == 1)
							{
								$sheet->write($row_index, 0, "Total " . $old_group_01 . " " . $old_group_02 . " " . $old_group_03, $f_group_title);

								//$sheet->write($row_index, $cell_index_number_of_projects+1, $concated_old_group03 ."->" . $concated_group03 . "->" . $next_field_name02, $f_number_text);

								if($show_number_of_projects == 1)
								{
									$sheet->write($row_index, $cell_index_number_of_projects, $total_projects_per_group03, $f_number_bold);
								}

								if(count($sum_fields) > 0)
								{
									foreach($group_totals03 as $key=>$total)
									{
										if(in_array($key, $sum_field_names))
										{
											$sheet->write($row_index, $group_totals03_cellindex[$key], $total, $f_group_decimal2_bold);
										}
										else
										{
											$sheet->write($row_index, $group_totals03_cellindex[$key], "", $f_group_decimal2_bold);
										}
									}
								}
								$group_printed03 = 1;
								$row_index++;
							}
							//if(count($avg_fields) > 0 and in_array($field, $avg_field_names))
							if(count($avg_fields) > 0)
							{
								
								$sheet->write($row_index, 0, "Average " . $old_group_01 . " " . $old_group_02 . " " . $old_group_03 , $f_group_title);

								foreach($group_totals03 as $key=>$total)
								{
									$avg = 0;
									if($total_projects_per_group03_for_avg[$key] > 0)
									{
										//$avg = round($total/$total_projects_per_group03_for_avg[$key], 2);
										$avg = $total/$total_projects_per_group03_for_avg[$key];
									}

									if(in_array($key, $avg_field_names))
									{
										if($totalisation_fields_format[$key] == 'decimal2'
											or $totalisation_fields_format[$key] == 'integer')
										{
											$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_decimal2_bold);
										}
										elseif($totalisation_fields_format[$key] == 'percent2')
										{
											$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_percent2_bold);
										}
										elseif($totalisation_fields_format[$key] == 'integer_overduedays')
										{
											
											$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_decimal2_bold);
										}
										elseif($totalisation_fields_format[$key] == 'cost_difference_percent')
										{
											$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_percent2_bold);
										}
										elseif($totalisation_fields_format[$key] == 'cost_difference_percent2')
										{
											$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_percent2_bold);
										}

										
									}
									else
									{
										$sheet->write($row_index, $totalisation_fields_column_index[$key], "", $f_group_decimal2_bold);
									}
									
								}
								$group_printed03 = 1;
								$row_index++;
							}
						}

						$old_group_03 = $row[$next_field_name03];

					}
				}
				if($group_printed03 == 1)
				{
					$total_projects_per_group03 = 0;
					$counter_group = 1;
					foreach($totalisation_fields as $key=>$value)
					{
						$group_totals03[$key] = 0;
						$group_total03_projects[$key] = 0;
						$total_projects_per_group03_for_avg[$key] = 0;
					}
					$row_index++;
				}
			}
			

			$empty_lines_comittet = false;
			if($query_group02 != '' and array_key_exists($key+1, $selected_field_order ))
			{
				
				$group_printed02 = 0;
				$next_field = $selected_field_order[$key+1];
				
				if(strpos($db_info["attributes"][$next_field], "IF(") === 0)
				{
					$tmp = explode(' AS ', $db_info["attributes"][$next_field]);
					$next_field_name02 = $tmp[1];
				}
				else
				{
					$next_field_name02 = str_replace(".", "", $db_info["attributes"][$next_field]);
				}

				if(strpos($db_info["attributes"][$query_groups[$query_group02]], "IF(") === 0)
				{
					$tmp = explode(' AS ', $db_info["attributes"][$query_groups[$query_group02]]);
					$group_field_name02 =  $tmp[1];
				}
				else
				{
					$group_field_name02 = str_replace(".", "", $db_info["attributes"][$query_groups[$query_group02]]);
				}
				
				if($group_field_name02 == $next_field_name02) // output only if there is grouping of rows
				{
					if($old_group_02 != $row[$next_field_name02])
					{
						if($old_group_02 and $empty_lines_comittet == false)
						{
							if(count($sum_fields) > 0 or $show_number_of_projects == 1)
							{
								$sheet->write($row_index, 0, "Total " . $old_group_01 . " " . $old_group_02, $f_group_title);

								if($show_number_of_projects == 1)
								{
									$sheet->write($row_index, $cell_index_number_of_projects, $total_projects_per_group02, $f_number_bold);
								}

								if(count($sum_fields) > 0)
								{
									foreach($group_totals02 as $key=>$total)
									{
										if(in_array($key, $sum_field_names))
										{
											$sheet->write($row_index, $group_totals02_cellindex[$key], $total, $f_group_decimal2_bold);
										}
										else
										{
											$sheet->write($row_index, $group_totals02_cellindex[$key], "", $f_group_decimal2_bold);
										}
									}
								}
								$group_printed02 = 1;
								$row_index++;
							}
							//if(count($avg_fields) > 0 and in_array($field, $avg_field_names))
							if(count($avg_fields) > 0)
							{
								
								$sheet->write($row_index, 0, "Average " . $old_group_01 . " " . $old_group_02 , $f_group_title);

								foreach($group_totals02 as $key=>$total)
								{
									$avg = 0;
									if($total_projects_per_group02_for_avg[$key] > 0)
									{
										//$avg = round($total/$total_projects_per_group02_for_avg[$key], 2);
										$avg = $total/$total_projects_per_group02_for_avg[$key];
									}

									if(in_array($key, $avg_field_names))
									{
										if($totalisation_fields_format[$key] == 'decimal2'
											or $totalisation_fields_format[$key] == 'integer')
										{
											$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_decimal2_bold);
										}
										elseif($totalisation_fields_format[$key] == 'percent2')
										{
											$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_percent2_bold);
										}
										elseif($totalisation_fields_format[$key] == 'integer_overduedays')
										{
											
											$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_decimal2_bold);
										}
										elseif($totalisation_fields_format[$key] == 'cost_difference_percent')
										{
											$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_percent2_bold);
										}
										elseif($totalisation_fields_format[$key] == 'cost_difference_percent2')
										{
											$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_percent2_bold);
										}

										
									}
									else
									{
										$sheet->write($row_index, $totalisation_fields_column_index[$key], "", $f_group_decimal2_bold);
									}
									
								}
								$group_printed02 = 1;
								$row_index++;
							}
						}

						$old_group_02 = $row[$next_field_name02];

					}
				}
				if($group_printed02 == 1)
				{
					$total_projects_per_group02 = 0;
					$total_projects_per_group03 = 0;
					$counter_group = 1;
					foreach($totalisation_fields as $key=>$value)
					{
						$group_totals02[$key] = 0;
						$group_totals03[$key] = 0;
						$group_total02_projects[$key] = 0;
						$group_total03_projects[$key] = 0;
						$total_projects_per_group02_for_avg[$key] = 0;
						$total_projects_per_group03_for_avg[$key] = 0;
					}
					$row_index++;
				}
			}

		
			if($query_group01 != '')
			{
				$group_printed = 0;	
				
				

				if(strpos($db_info["attributes"][$query_groups[$query_group01]], "IF(") === 0)
				{
					$tmp = explode(' AS ', $db_info["attributes"][$query_groups[$query_group01]]);
					$group_field_name01 =  $tmp[1];
				}
				elseif($db_info["attributes"][$query_groups[$query_group01]] == 'calculated_content')
				{
					
					$tmp = explode('.', $query_groups[$query_group01]);
					$group_field_name01 =  $tmp[1];
				}
				else
				{
					$group_field_name01 = str_replace(".", "", $db_info["attributes"][$query_groups[$query_group01]]);
				}
				

				if($group_field_name01 == $field_name) // output only if there is grouping of rows
				{
					
					if($db_info["attributes"][$field] == "calculated_content")
					{
						$str2compare = $output_text;
					}
					else
					{
						$str2compare = $row[$field_name];
					}
					
					
					if($old_group_01 != $str2compare)
					{
						
						if($old_group_01)
						{
							if(count($sum_fields) > 0 or $show_number_of_projects == 1)
							{
								
								$sheet->write($row_index, 0, "Total " . $old_group_01 , $f_group_title);

								if($show_number_of_projects == 1)
								{
									$sheet->write($row_index, $cell_index_number_of_projects, $total_projects_per_group01, $f_number_bold);
								}
								
								if(count($sum_fields) > 0)
								{
									foreach($group_totals as $key=>$total)
									{
										if(in_array($key, $sum_field_names))
										{
											$sheet->write($row_index, $group_totals_cellindex[$key], $total, $f_group_decimal2_bold);
										}
										else
										{
											//$sheet->write($row_index, $group_totals_cellindex[$key], "", $f_group_decimal2_bold);
										}
									}
								}
								
								//group total_fields for data blocks
								
								if(array_key_exists("item_quantity_block", $data_blocks_captions))
								{
									$tmp_col = $caption_count_without_data_blocks + 1;
									foreach($data_blocks_captions["item_quantity_block"] as $key=>$value)
									{
										$sheet->write($row_index, $tmp_col,$group_totals["item_quantity_block_" . $key], $f_number_bold);
										$tmp_col++;
										
									}
								}
								
								
								$group_printed = 1;
								$row_index++;
							}
							else
							{
								//group total_fields for data blocks
								
								if(array_key_exists("item_quantity_block", $data_blocks_captions))
								{
									$sheet->write($row_index, 0, "Total " . $old_group_01 , $f_group_title);
									$tmp_col = $caption_count_without_data_blocks + 1;
									foreach($data_blocks_captions["item_quantity_block"] as $key=>$value)
									{
										$sheet->write($row_index, $tmp_col,$group_totals["item_quantity_block_" . $key], $f_number_bold);
										$tmp_col++;
										
									}
									$group_printed = 1;
									$row_index++;
								}
							}
						

							if(count($avg_fields) > 0)
							{
								
								$sheet->write($row_index, 0, "Average " . $old_group_01 , $f_group_title);
															
								foreach($group_totals as $key=>$total)
								{
									$avg = 0;
									if($total_projects_per_group_for_avg[$key] > 0)
									{
										//$avg = round($total/$total_projects_per_group_for_avg[$key], 2);
										$avg = $total/$total_projects_per_group_for_avg[$key];
									}
									if(in_array($key, $avg_field_names))
									{
										if($totalisation_fields_format[$key] == 'decimal2'
											or $totalisation_fields_format[$key] == 'integer')
										{
											$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_decimal2_bold);
										}
										elseif($totalisation_fields_format[$key] == 'percent2')
										{
											$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_percent2_bold);
										}
										elseif($totalisation_fields_format[$key] == 'integer_overduedays')
										{
											
											$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_decimal2_bold);
										}
										elseif($totalisation_fields_format[$key] == 'cost_difference_percent')
										{
											$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_percent2_bold);
										}
										elseif($totalisation_fields_format[$key] == 'cost_difference_percent2')
										{
											$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_percent2_bold);
										}
									}
									else
									{
										$sheet->write($row_index, $totalisation_fields_column_index[$key], "", $f_group_percent2_bold);
									}
									
								}
								$group_printed = 1;
								$row_index++;
							}

						}

						if($db_info["attributes"][$field] == "calculated_content")
						{
							$old_group_01 = $output_text;
						}
						else
						{
							$old_group_01 = $row[$field_name];
						}

					}
				}

				if($group_printed == 1)
				{
					$total_projects_per_group01 = 0;
					$total_projects_per_group02 = 0;
					$total_projects_per_group03 = 0;
					$counter_group = 1;
					foreach($totalisation_fields as $key=>$value)
					{
						$group_totals[$key] = 0;
						$group_totals02[$key] = 0;
						$group_totals03[$key] = 0;
						$total_projects_per_group_for_avg[$key] = 0;
						$total_projects_per_group02_for_avg[$key] = 0;
						$total_projects_per_group03_for_avg[$key] = 0;
					}

					if(array_key_exists("item_quantity_block", $data_blocks_captions))
					{
						$tmp_col = $caption_count_without_data_blocks + 1;
						foreach($data_blocks_captions["item_quantity_block"] as $key=>$value)
						{
							$group_totals["item_quantity_block_" . $key] = 0;
						}
					}
					
					$row_index++;
				}
			}
			
			
			
			if($db_info["attributes"][$field] == "calculated_content" 
				or $db_info["attributes"][$field] == "content_by_function")
			{
				if($db_info["datatypes"][$field] == 'text')
				{
					if($zebra_on == true)
					{
						$sheet->write($row_index, $cell_index, $output_text, $f_text_zebra);
					}
					else
					{
						$sheet->write($row_index, $cell_index, $output_text, $f_text);
					}
					
					if($col_widths[$cell_index-1] < strlen($output_text)) {$col_widths[$cell_index-1] = strlen($output_text);}
				}
				elseif($db_info["datatypes"][$field] == 'integer')
				{
					if($zebra_on == true)
					{
						$sheet->write($row_index, $cell_index, $output_text, $f_number_zebra);
					}
					else
					{
						$sheet->write($row_index, $cell_index, $output_text, $f_number);
					}
					if($col_widths[$cell_index-1] < strlen($output_text)){$col_widths[$cell_index-1] = strlen($output_text);}
				}
				elseif($db_info["datatypes"][$field] == 'boolean')
				{

					if(1*$output_text == 1)
					{
						$output_text = "yes";
					}
					else
					{
						$output_text = "no";
					}
					if($zebra_on == true)
					{
						$sheet->write($row_index, $cell_index, $output_text, $f_text_zebra);
					}
					else
					{
						$sheet->write($row_index, $cell_index, $output_text, $f_text);
					}
					
					if($col_widths[$cell_index-1] < strlen($output_text)){$col_widths[$cell_index-1] = strlen($output);}
				}
				elseif($db_info["datatypes"][$field] == 'integer_overduedays')
				{
					if($output_text > $cms_overdue_day_limits['red'])
					{
						$sheet->write($row_index, $cell_index, $output_text, $f_number_red);
					}
					elseif($output_text > $cms_overdue_day_limits['orange'])
					{
						$sheet->write($row_index, $cell_index, $output_text, $f_number_orange);
					}
					elseif($output_text > $cms_overdue_day_limits['yellow'])
					{
						$sheet->write($row_index, $cell_index, $output_text, $f_number_yellow);
					}
					else
					{
						if($zebra_on == true)
						{
							$sheet->write($row_index, $cell_index, $output_text, $f_number_zebra);
						}
						else
						{
							$sheet->write($row_index, $cell_index, $output_text, $f_number);
						}
						
					}
					
					
					if($col_widths[$cell_index-1] < strlen($output_text)){$col_widths[$cell_index-1] = strlen($output_text);}
				}
				elseif($db_info["datatypes"][$field] == 'cost_difference_percent')
				{
					if($output_text > $cost_difference_in_percent['red'])
					{
						$sheet->write($row_index, $cell_index, $output_text, $f_percent2_red);
					}
					elseif($output_text > $cost_difference_in_percent['orange'])
					{
						$sheet->write($row_index, $cell_index, $output_text, $f_percent2_orange);
					}
					else
					{
						$sheet->write($row_index, $cell_index, $output_text, $f_percent2_green);
					}
										
					if($col_widths[$cell_index-1] < strlen($output_text)){$col_widths[$cell_index-1] = strlen($output_text);}
				}
				elseif($db_info["datatypes"][$field] == 'cost_difference_percent2')
				{
					if($output_text > $cost_difference_in_percent2['red'])
					{
						$sheet->write($row_index, $cell_index, $output_text, $f_percent2_red);
					}
					elseif($output_text > $cost_difference_in_percent2['orange'])
					{
						$sheet->write($row_index, $cell_index, $output_text, $f_percent2_orange);
					}
					else
					{
						$sheet->write($row_index, $cell_index, $output_text, $f_percent2_green);
					}
										
					if($col_widths[$cell_index-1] < strlen($output_text)){$col_widths[$cell_index-1] = strlen($output_text);}
				}
				elseif($db_info["datatypes"][$field] == 'decimal2')
				{
					if($zebra_on == true)
					{
						$sheet->write($row_index, $cell_index, $output_text, $f_decimal2_zebra);
					}
					else
					{
						$sheet->write($row_index, $cell_index, $output_text, $f_decimal2);
					}
					
					if($col_widths[$cell_index-1] < strlen($output_text)){$col_widths[$cell_index-1] = strlen($output_text);}
				}
				elseif($db_info["datatypes"][$field] == 'percent2')
				{
					if($zebra_on == true)
					{
						$sheet->write($row_index, $cell_index, $output_text, $f_percent2_zebra);
					}
					else
					{
						$sheet->write($row_index, $cell_index, $output_text, $f_percent2);
					}
					
					if($col_widths[$cell_index-1] < strlen($output_text)){$col_widths[$cell_index-1] = strlen($output_text);}
				}
				elseif($db_info["datatypes"][$field] == 'date')
				{
					if(strtodate($output_text) > 0 or strtodate($output_text) < 0)
					{
						if($zebra_on == true)
						{
							$sheet->write($row_index, $cell_index, mysql_date_to_xls_date($output_text), $f_date_zebra);
						}
						else
						{
							$sheet->write($row_index, $cell_index, mysql_date_to_xls_date($output_text), $f_date);
						}
					}
					else
					{
						if($zebra_on == true)
						{
							$sheet->write($row_index, $cell_index, $output_text, $f_date_zebra);
						}
						else
						{
							$sheet->write($row_index, $cell_index, $output_text, $f_date);
						}
					}
					if($col_widths[$cell_index-1] < strlen($output_text)){$col_widths[$cell_index-1] = strlen($output_text);}
				}

				if($col_widths[$cell_index-1] < strlen($output_text))
				{
					$col_widths[$cell_index-1] = strlen($output_text);
					if($col_widths[$cell_index-1] < strlen($output_text)){$col_widths[$cell_index-1] = strlen($output_text);}
				}
			}
			elseif($db_info["attributes"][$field] == 'subgroup_category_order_state')
			{
				if($subgroup_category_order_state_cell_index == 0)
				{
					$subgroup_category_order_state_cell_index = $cell_index;
				}
				$sql_m = 'select DISTINCT suppliers.address_shortcut as supplier, ' . 
						 'DATE_FORMAT(order_item_ordered, "%d.%m.%Y") as date_ordered, ' .
						 'DATE_FORMAT(order_item_ready_for_pickup, "%d.%m.%Y") as date_ready_for_pickup, ' .
						 'DATE_FORMAT(order_item_pickup, "%d.%m.%Y") as pickup_date, ' .
					     'item_category_name ' . 
						 'from order_items ' .
						 'left join addresses as suppliers on suppliers.address_id = order_item_supplier_address ' .
						 'left join addresses as forwarders on forwarders.address_id = order_item_forwarder_address ' .
						 'left join items on item_id = order_item_item ' . 
						 'left join item_categories on item_category_id = item_category ' .
						 'where order_item_order = ' . $order_id . 
					     ' and order_item_quantity > 0 ' .
						 ' and order_item_supplier_address > 0 ' . 
						 ' and (item_category IN (1, 3, 4, 5, 6, 7, 12, 16, 24) ' . $costmonitoring_group_filter . ')' .
					     $order_item_filter .
						 ' order by order_item_ordered ASC, suppliers.address_shortcut';

				$res_m = mysql_query($sql_m) or dberror($sql_m);
				$suppliers_in_project = false;
				while ($row_m = mysql_fetch_assoc($res_m))
				{
					$cell_index = $subgroup_category_order_state_cell_index;
					if($zebra_on == true)
					{
						$sheet->write($row_index + $subgroup_category_order_state_row_index, $cell_index, $row_m["supplier"], $f_text_zebra);
					}
					else
					{
						$sheet->write($row_index + $subgroup_category_order_state_row_index, $cell_index, $row_m["supplier"], $f_text);
					}
					
					if($col_widths[$cell_index-1] < strlen($row_m["supplier"])){$col_widths[$cell_index-1] = strlen($row_m["supplier"]);}
					$cell_index++;

					if($zebra_on == true)
					{
						$sheet->write($row_index + $subgroup_category_order_state_row_index, $cell_index, $row_m["item_category_name"], $f_text_zebra);
					}
					else
					{
						$sheet->write($row_index + $subgroup_category_order_state_row_index, $cell_index, $row_m["item_category_name"], $f_text);
					}
					
					if($col_widths[$cell_index-1] < strlen($row_m["item_category_name"])){$col_widths[$cell_index-1] = strlen($row_m["item_category_name"]);}
					$cell_index++;

					if($zebra_on == true)
					{
						$sheet->write($row_index + $subgroup_category_order_state_row_index, $cell_index, $row_m["date_ordered"], $f_date_zebra);
					}
					else
					{
						$sheet->write($row_index + $subgroup_category_order_state_row_index, $cell_index, $row_m["date_ordered"], $f_date);
					}
					
					if($col_widths[$cell_index-1] < strlen($row_m["date_ordered"])){$col_widths[$cell_index-1] = strlen($row_m["date_ordered"]);}
					$cell_index++;

					if($zebra_on == true)
					{
						$sheet->write($row_index + $subgroup_category_order_state_row_index, $cell_index, $row_m["pickup_date"], $f_date_zebra);
					}
					else
					{
						$sheet->write($row_index + $subgroup_category_order_state_row_index, $cell_index, $row_m["pickup_date"], $f_date);
					}
					
					if($col_widths[$cell_index-1] < strlen($row_m["pickup_date"])){$col_widths[$cell_index-1] = strlen($row_m["pickup_date"]);}
					$cell_index++;

					if($zebra_on == true)
					{
						$sheet->write($row_index + $subgroup_category_order_state_row_index, $cell_index, $row_m["date_ready_for_pickup"], $f_date_zebra);
					}
					else
					{
						$sheet->write($row_index + $subgroup_category_order_state_row_index, $cell_index, $row_m["date_ready_for_pickup"], $f_date);
					}
					
					if($col_widths[$cell_index-1] < strlen($row_m["date_ready_for_pickup"])){$col_widths[$cell_index-1] = strlen($row_m["date_ready_for_pickup"]);}

					$subgroup_category_order_state_row_index++;
					$suppliers_in_project = true;
				}
				
				if($suppliers_in_project == false)
				{
					$cell_index = $subgroup_category_order_state_cell_index;
					if($zebra_on == true)
					{
						$sheet->write($row_index + $subgroup_category_order_state_row_index, $cell_index, "", $f_text_zebra);
					}
					else
					{
						$sheet->write($row_index + $subgroup_category_order_state_row_index, $cell_index, "", $f_text);
					}
					
					$cell_index++;

					if($zebra_on == true)
					{
						$sheet->write($row_index + $subgroup_category_order_state_row_index, $cell_index, "", $f_text_zebra);
					}
					else
					{
						$sheet->write($row_index + $subgroup_category_order_state_row_index, $cell_index, "", $f_text);
					}
					
					$cell_index++;

					if($zebra_on == true)
					{
						$sheet->write($row_index + $subgroup_category_order_state_row_index, $cell_index, "", $f_date_zebra);
					}
					else
					{
						$sheet->write($row_index + $subgroup_category_order_state_row_index, $cell_index, "", $f_date);
					}
					
					$cell_index++;

					if($zebra_on == true)
					{
						$sheet->write($row_index + $subgroup_category_order_state_row_index, $cell_index, "", $f_date_zebra);
					}
					else
					{
						$sheet->write($row_index + $subgroup_category_order_state_row_index, $cell_index, "", $f_date);
					}
					
					$cell_index++;

					if($zebra_on == true)
					{
						$sheet->write($row_index + $subgroup_category_order_state_row_index, $cell_index, "", $f_date_zebra);
					}
					else
					{
						$sheet->write($row_index + $subgroup_category_order_state_row_index, $cell_index, "", $f_date);
					}
					

					$subgroup_category_order_state_row_index++;
				}
			}
			elseif($db_info["attributes"][$field] == 'subgroup_category_delivery_state')
			{
				if($subgroup_category_delivery_state_cell_index == 0)
				{
					$subgroup_category_delivery_state_cell_index = $cell_index;
				}
				$sql_m = 'select DISTINCT suppliers.address_shortcut, forwarders.address_shortcut as forwarder,' . 
						 'DATE_FORMAT(order_item_expected_arrival, "%d.%m.%Y") as expected_arrival_date, ' .
						 'DATE_FORMAT(order_item_arrival, "%d.%m.%Y") as arrival_date, ' .
						 'item_category_name, order_item_shipment_code ' .
						 'from order_items ' .
						 'left join addresses as suppliers on suppliers.address_id = order_item_supplier_address ' .
						 'left join addresses as forwarders on forwarders.address_id = order_item_forwarder_address ' .
						 'left join items on item_id = order_item_item ' . 
						 'left join item_categories on item_category_id = item_category ' .
						 'where order_item_order = ' . $order_id . 
						 ' and order_item_supplier_address > 0 ' .
					     ' and order_item_quantity > 0 ' .
						 ' and (item_category IN (1, 3, 4, 5, 6, 7, 12, 16, 24) ' . $costmonitoring_group_filter . ')' .
					     $order_item_filter .
						 ' order by order_item_ordered ASC, suppliers.address_shortcut';

				$res_m = mysql_query($sql_m) or dberror($sql_m);
				$forwarders_in_project = false;
				while ($row_m = mysql_fetch_assoc($res_m))
				{
					$cell_index = $subgroup_category_delivery_state_cell_index;
					
					if($zebra_on == true)
					{
						$sheet->write($row_index + $subgroup_category_delivery_state_row_index, $cell_index, $row_m["item_category_name"], $f_text_zebra);
					}
					else
					{
						$sheet->write($row_index + $subgroup_category_delivery_state_row_index, $cell_index, $row_m["item_category_name"], $f_text);
					}
					
					if($col_widths[$cell_index-1] < strlen($row_m["item_category_name"])){$col_widths[$cell_index-1] = strlen($row_m["item_category_name"]);}
					$cell_index++;

					if($zebra_on == true)
					{
						$sheet->write($row_index + $subgroup_category_delivery_state_row_index, $cell_index, $row_m["order_item_shipment_code"], $f_text_zebra);
					}
					else
					{
						$sheet->write($row_index + $subgroup_category_delivery_state_row_index, $cell_index, $row_m["order_item_shipment_code"], $f_text);
					}
					
					if($col_widths[$cell_index-1] < strlen($row_m["order_item_shipment_code"])){$col_widths[$cell_index-1] = strlen($row_m["order_item_shipment_code"]);}
					$cell_index++;

					if($zebra_on == true)
					{
						$sheet->write($row_index + $subgroup_category_delivery_state_row_index, $cell_index, $row_m["forwarder"], $f_text_zebra);
					}
					else
					{
						$sheet->write($row_index + $subgroup_category_delivery_state_row_index, $cell_index, $row_m["forwarder"], $f_text);
					}
					
					if($col_widths[$cell_index-1] < strlen($row_m["forwarder"])){$col_widths[$cell_index-1] = strlen($row_m["forwarder"]);}
					$cell_index++;

					if($zebra_on == true)
					{
						$sheet->write($row_index + $subgroup_category_delivery_state_row_index, $cell_index, $row_m["expected_arrival_date"], $f_date_zebra);
					}
					else
					{
						$sheet->write($row_index + $subgroup_category_delivery_state_row_index, $cell_index, $row_m["expected_arrival_date"], $f_date);
					}
					
					if($col_widths[$cell_index-1] < strlen($row_m["expected_arrival_date"])){$col_widths[$cell_index-1] = strlen($row_m["expected_arrival_date"]);}
					$cell_index++;

					if($zebra_on == true)
					{
						$sheet->write($row_index + $subgroup_category_delivery_state_row_index, $cell_index, $row_m["arrival_date"], $f_date_zebra);
					}
					else
					{
						$sheet->write($row_index + $subgroup_category_delivery_state_row_index, $cell_index, $row_m["arrival_date"], $f_date);
					}
					
					if($col_widths[$cell_index-1] < strlen($row_m["arrival_date"])){$col_widths[$cell_index-1] = strlen($row_m["arrival_date"]);}

					$subgroup_category_delivery_state_row_index++;
					$forwarders_in_project = true;
				}
				
				if($forwarders_in_project == false)
				{
					$cell_index = $subgroup_category_delivery_state_cell_index;
					
					if($zebra_on == true)
					{
						$sheet->write($row_index + $subgroup_category_delivery_state_row_index, $cell_index, "", $f_text_zebra);
					}
					else
					{
						$sheet->write($row_index + $subgroup_category_delivery_state_row_index, $cell_index, "", $f_text);
					}
					
					$cell_index++;
					
					if($zebra_on == true)
					{
						$sheet->write($row_index + $subgroup_category_delivery_state_row_index, $cell_index, "", $f_text_zebra);
					}
					else
					{
						$sheet->write($row_index + $subgroup_category_delivery_state_row_index, $cell_index, "", $f_text);
					}
					
					$cell_index++;

					if($zebra_on == true)
					{
						$sheet->write($row_index + $subgroup_category_delivery_state_row_index, $cell_index, "", $f_text_zebra);
					}
					else
					{
						$sheet->write($row_index + $subgroup_category_delivery_state_row_index, $cell_index, "", $f_text);
					}
					
					$cell_index++;

					if($zebra_on == true)
					{
						$sheet->write($row_index + $subgroup_category_delivery_state_row_index, $cell_index, "", $f_date_zebra);
					}
					else
					{
						$sheet->write($row_index + $subgroup_category_delivery_state_row_index, $cell_index, "", $f_date);
					}
					
					$cell_index++;

					if($zebra_on == true)
					{
						$sheet->write($row_index + $subgroup_category_delivery_state_row_index, $cell_index, "", $f_date_zebra);
					}
					else
					{
						$sheet->write($row_index + $subgroup_category_delivery_state_row_index, $cell_index, "", $f_date);
					}
					

					$subgroup_category_delivery_state_row_index++;
				}
			}
			elseif($db_info["attributes"][$field] == "item_quantity_block"
				and array_key_exists("item_quantity_block", $data_blocks_captions)
				)
			{
				$datablock_cellindex = $caption_count_without_data_blocks+1;
				foreach($data_blocks_captions["item_quantity_block"] as $key=>$value)
				{
					
					$item_quantity = "";
					if(array_key_exists($row['project_id'], $data_blocks_data[$data_block_name]))
					{
						foreach($data_blocks_data["item_quantity_block"][$row['project_id']] as $key2=>$item)
						{
							if($key == $item["item_code"])
							{
								$item_quantity = $item["order_item_quantity"];

								$group_totals["item_quantity_block_" . $key] = $group_totals["item_quantity_block_" . $key] + $item_quantity;
								$item_totals["item_quantity_block_" . $key] = $item_totals["item_quantity_block_" . $key] + $item_quantity;
							}
						}
					}
					
					if($zebra_on == true)
					{
						$sheet->write($row_index, $datablock_cellindex, $item_quantity, $f_number_zebra);
					}
					else
					{
						$sheet->write($row_index, $datablock_cellindex, $item_quantity, $f_number);
					}
					$datablock_cellindex++;
				}
			}
			else
			{
				if($db_info["datatypes"][$field] == 'text')
				{
					if(!$row[$field_name])
					{
						if($zebra_on == true)
						{
								$sheet->write($row_index, $cell_index, "", $f_text_zebra_zebra);
						}
						else
						{
							$sheet->write($row_index, $cell_index, "", $f_text);
						}

					}
					else
					{
						if($zebra_on == true)
						{
							$sheet->write($row_index, $cell_index, $row[$field_name], $f_text_zebra);
						}
						else
						{
							$sheet->write($row_index, $cell_index, $row[$field_name], $f_text);
						}
						
						if($col_widths[$cell_index-1] < strlen($row[$field_name])){$col_widths[$cell_index-1] = strlen($row[$field_name]);}
					}
					
				}
				elseif($db_info["datatypes"][$field] == 'integer')
				{
					if(!$row[$field_name] or $row[$field_name] == 0)
					{
						if($zebra_on == true)
						{
							$sheet->write($row_index, $cell_index, "", $f_number_zebra);
						}
						else
						{
							$sheet->write($row_index, $cell_index, "", $f_number);
						}
						
					}
					else
					{
						if($zebra_on == true)
						{
							$sheet->write($row_index, $cell_index, $row[$field_name], $f_number_zebra);
						}
						else
						{
							$sheet->write($row_index, $cell_index, $row[$field_name], $f_number);
						}
						
						if($col_widths[$cell_index-1] < strlen($row[$field_name])){$col_widths[$cell_index-1] = strlen($row[$field_name]);}
					}
					
				}
				elseif($db_info["datatypes"][$field] == 'decimal2')
				{
					if(!$row[$field_name] or $row[$field_name] == 0)
					{
						if($zebra_on == true)
						{
							$sheet->write($row_index, $cell_index, "", $f_decimal2_zebra);
						}
						else
						{
							$sheet->write($row_index, $cell_index, "", $f_decimal2);
						}
						
					}
					else
					{
						if($zebra_on == true)
						{
							$sheet->write($row_index, $cell_index, $row[$field_name], $f_decimal2_zebra);
						}
						else
						{
							$sheet->write($row_index, $cell_index, $row[$field_name], $f_decimal2);
						}
						
						if($col_widths[$cell_index-1] < strlen($row[$field_name])){$col_widths[$cell_index-1] = strlen($row[$field_name]);}
					}
				}
				elseif($db_info["datatypes"][$field] == 'date')
				{
					
					if($zebra_on == true)
					{
						$sheet->write($row_index, $cell_index, mysql_date_to_xls_date($row[$field_name]), $f_date_zebra);
					}
					else
					{
						$sheet->write($row_index, $cell_index, mysql_date_to_xls_date($row[$field_name]), $f_date);
					}
					
					if($col_widths[$cell_index-1] < strlen($row[$field_name])){$col_widths[$cell_index-1] = strlen($row[$field_name]);}
				}
				elseif($db_info["datatypes"][$field] == 'boolean')
				{
					$output = "no";
					if($row[$field_name] == 1)
					{
						$output = "yes";
					}
					if($zebra_on == true)
					{
						$sheet->write($row_index, $cell_index, $output, $f_text_zebra);
					}
					else
					{
						$sheet->write($row_index, $cell_index, $output, $f_text);
					}
					
					if($col_widths[$cell_index-1] < strlen($output)){$col_widths[$cell_index-1] = strlen($output);}
				}
			}
			

			$cell_index++;
		}

		if($show_number_of_projects == 1)
		{
			$cell_index = $caption_count_without_data_blocks+1;
			
			if(count($data_blocks_captions) > 0)
			{
				foreach($data_blocks_captions["item_quantity_block"] as $key=>$value)
				{
					$cell_index++;
				}
			}			
			if($zebra_on)
			{
				$sheet->write($row_index, $cell_index, $counter_group, $f_number_zebra); // group counter column
			}
			else
			{
				$sheet->write($row_index, $cell_index, $counter_group, $f_number); // group counter column
			}
			$cell_index++;

		}
		

		if($zebra_on)
		{
			$sheet->write($row_index, 0, $counter, $f_number_zebra); // counter column
		}
		else
		{
			$sheet->write($row_index, 0, $counter, $f_number); // counter column
		}
		
		$counter++;
		$counter_group++;
		$total_projects++;
		$total_projects_per_group01++;
		$total_projects_per_group02++;
		$total_projects_per_group03++;
	
	}
}


if($subgroup_category_order_state_row_index > 0 and $subgroup_category_order_state_row_index > $subgroup_category_delivery_state_row_index)
{
	$row_index = $row_index + $subgroup_category_order_state_row_index - 1;
}
elseif($subgroup_category_delivery_state_row_index > 0)
{
	$row_index = $row_index + $subgroup_category_delivery_state_row_index - 1;
}

//print group total of last group

if($query_group03 != '')
{
	$row_index++;
	if(count($sum_fields) > 0  or $show_number_of_projects == 1)
	{
		$sheet->write($row_index, 0,"Total " . $old_group_01 . " " . $old_group_02 ." " . $old_group_03, $f_group_title);
		if($show_number_of_projects == 1)
		{
			$sheet->write($row_index, $cell_index_number_of_projects, $total_projects_per_group03 , $f_number_bold);
		}
		
		if(count($sum_fields) > 0)
		{
			foreach($group_totals03 as $key=>$total)
			{	
				if(in_array($key, $sum_field_names))
				{
					$sheet->write($row_index, $group_totals03_cellindex[$key], $total, $f_group_decimal2_bold);
				}
				else
				{
					$sheet->write($row_index, $group_totals03_cellindex[$key], "", $f_group_decimal2_bold);
				}
			}
		}
		$row_index++;
	}

	
	if(count($avg_fields) > 0)
	{
		$sheet->write($row_index, 0, "Average " . $old_group_01 . " " . $old_group_02 ." " . $old_group_03, $f_group_title);
		foreach($group_totals03 as $key=>$total)
		{	
			$avg = 0;
			if($total_projects_per_group03_for_avg[$key] > 0)
			{
				//$avg = round($total/$total_projects_per_group03_for_avg[$key]);
				$avg = $total/$total_projects_per_group03_for_avg[$key];
			}
			if(in_array($key, $avg_field_names))
			{
				if($totalisation_fields_format[$key] == 'decimal2' 
					or $totalisation_fields_format[$key] == 'integer')
				{
					$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_decimal2_bold);
				}
				elseif($totalisation_fields_format[$key] == 'percent2')
				{
					$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_percent2_bold);
				}
				elseif($totalisation_fields_format[$key] == 'integer_overduedays')
				{
					
					$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_decimal2_bold);
				}
				elseif($totalisation_fields_format[$key] == 'cost_difference_percent')
				{
					$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_percent2_bold);
				}
				elseif($totalisation_fields_format[$key] == 'cost_difference_percent2')
				{
					$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_percent2_bold);
				}
			}
			else
			{
				$sheet->write($row_index, $totalisation_fields_column_index[$key], "", $f_group_decimal2_bold);
			}
		}
		$row_index++;
	}

}

if($query_group02 != '')
{
	$row_index++;
	if(count($sum_fields) > 0  or $show_number_of_projects == 1)
	{
		$sheet->write($row_index, 0,"Total " . $old_group_01 . " " . $old_group_02, $f_group_title);
		if($show_number_of_projects == 1)
		{
			$sheet->write($row_index, $cell_index_number_of_projects, $total_projects_per_group02 , $f_number_bold);
		}
		
		if(count($sum_fields) > 0)
		{
			foreach($group_totals02 as $key=>$total)
			{	
				if(in_array($key, $sum_field_names))
				{
					$sheet->write($row_index, $group_totals02_cellindex[$key], $total, $f_group_decimal2_bold);
				}
				else
				{
					$sheet->write($row_index, $group_totals02_cellindex[$key], "", $f_group_decimal2_bold);
				}
			}
		}
		$row_index++;
	}

	
	if(count($avg_fields) > 0)
	{
		$sheet->write($row_index, 0, "Average " . $old_group_01 . " " . $old_group_02, $f_group_title);
		foreach($group_totals02 as $key=>$total)
		{	
			$avg = 0;
			if($total_projects_per_group02_for_avg[$key] > 0)
			{
				//$avg = round($total/$total_projects_per_group02_for_avg[$key]);
				$avg = $total/$total_projects_per_group02_for_avg[$key];
			}
			if(in_array($key, $avg_field_names))
			{
				if($totalisation_fields_format[$key] == 'decimal2' 
					or $totalisation_fields_format[$key] == 'integer')
				{
					$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_decimal2_bold);
				}
				elseif($totalisation_fields_format[$key] == 'percent2')
				{
					$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_percent2_bold);
				}
				elseif($totalisation_fields_format[$key] == 'integer_overduedays')
				{
					
					$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_decimal2_bold);
				}
				elseif($totalisation_fields_format[$key] == 'cost_difference_percent')
				{
					$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_percent2_bold);
				}
				elseif($totalisation_fields_format[$key] == 'cost_difference_percent2')
				{
					$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_percent2_bold);
				}
			}
			else
			{
				$sheet->write($row_index, $totalisation_fields_column_index[$key], "", $f_group_decimal2_bold);
			}
		}
		$row_index++;
	}

}


if($query_group01 != '')
{
	$row_index++;
	if(count($sum_fields) > 0  or $show_number_of_projects == 1)
	{
				
		$sheet->write($row_index, 0, "Total " . $old_group_01, $f_group_title);

		if($show_number_of_projects == 1)
		{
			$sheet->write($row_index, $cell_index_number_of_projects, $total_projects_per_group01 , $f_number_bold);
		}

		if(count($sum_fields) > 0)
		{
			foreach($group_totals as $key=>$total)
			{	
				if(in_array($key, $sum_field_names))
				{
					$sheet->write($row_index, $group_totals_cellindex[$key], $total, $f_group_decimal2_bold);
				}
				else
				{
					//$sheet->write($row_index, $group_totals_cellindex[$key], "", $f_group_decimal2_bold);
				}
			}
		}


		//group total_fields for data blocks
		if(array_key_exists("item_quantity_block", $data_blocks_captions))
		{
			$tmp_col = $caption_count_without_data_blocks + 1;
			foreach($data_blocks_captions["item_quantity_block"] as $key=>$value)
			{
				$sheet->write($row_index, $tmp_col,$group_totals["item_quantity_block_" . $key], $f_number_bold);
				$tmp_col++;
				
			}
		}

		$row_index++;
	}
	else
	{
		//group total_fields for data blocks
		if(array_key_exists("item_quantity_block", $data_blocks_captions))
		{
			$sheet->write($row_index, 0, "Total " . $old_group_01, $f_group_title);
			$tmp_col = $caption_count_without_data_blocks + 1;
			foreach($data_blocks_captions["item_quantity_block"] as $key=>$value)
			{
				$sheet->write($row_index, $tmp_col,$group_totals["item_quantity_block_" . $key], $f_number_bold);
				$tmp_col++;
				
			}
			$row_index++;
		}

		
	}
	
	
	if(count($avg_fields) > 0)
	{
		$sheet->write($row_index, 0, "Average " . $old_group_01, $f_group_title);
		foreach($group_totals as $key=>$total)
		{	
			$avg = 0;
			if($total_projects_per_group_for_avg[$key] > 0)
			{
				//$avg = round($total/$total_projects_per_group_for_avg[$key], 2);
				$avg = $total/$total_projects_per_group_for_avg[$key];
			}
			
			if(in_array($key, $avg_field_names))
			{
				if($totalisation_fields_format[$key] == 'decimal2' 
					or $totalisation_fields_format[$key] == 'integer')
				{
					$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_decimal2_bold);
				}
				elseif($totalisation_fields_format[$key] == 'percent2')
				{
					$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_percent2_bold);
				}
				elseif($totalisation_fields_format[$key] == 'integer_overduedays')
				{
					
					$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_decimal2_bold);
				}
				elseif($totalisation_fields_format[$key] == 'cost_difference_percent')
				{
					$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_percent2_bold);
				}
				elseif($totalisation_fields_format[$key] == 'cost_difference_percent2')
				{
					$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_percent2_bold);
				}
			}
			else
			{
				$sheet->write($row_index, $totalisation_fields_column_index[$key], "", $f_group_decimal2_bold);
			}
		}
		$row_index++;
	}
}


//print list totals
if(count($sum_fields) > 0 or count($avg_fields) > 0  or $show_number_of_projects == 1)
{
	if($query_group01 != '')
	{
		$row_index++;
	}
	else
	{
		$row_index++;
		$row_index++;
	}
	
	
	

	$cell_index = 1;
	
	if(count($sum_fields) > 0 or $show_number_of_projects == 1)
	{
		$sheet->write($row_index, 0, "Overall Total", $f_group_title);

		if($show_number_of_projects == 1)
		{
			$sheet->write($row_index, $cell_index_number_of_projects, $total_projects , $f_number_bold);
		}

		if(count($sum_fields) > 0)
		{
			foreach($totalisation_fields as $key=>$value)
			{
				if(in_array($key, $sum_field_names))
				{
					$sheet->write($row_index, $totalisation_fields_column_index[$key], $value, $f_group_decimal2_bold);
				}
				else
				{
					$sheet->write($row_index, $totalisation_fields_column_index[$key], "", $f_group_decimal2_bold);
				}
				$cell_index++;
			}
		}

		//list total_fields for data blocks
		if(array_key_exists("item_quantity_block", $data_blocks_captions))
		{
			$tmp_col = $caption_count_without_data_blocks + 1;
			foreach($data_blocks_captions["item_quantity_block"] as $key=>$value)
			{
				$sheet->write($row_index, $tmp_col,$item_totals["item_quantity_block_" . $key], $f_number_bold);
				$tmp_col++;
				
			}
		}
		$row_index++;
	}
	else
	{
		//list total_fields for data blocks
		if(array_key_exists("item_quantity_block", $data_blocks_captions))
		{
			$tmp_col = $caption_count_without_data_blocks + 1;
			foreach($data_blocks_captions["item_quantity_block"] as $key=>$value)
			{
				$sheet->write($row_index, $tmp_col,$item_totals["item_quantity_block_" . $key], $f_number_bold);
				$tmp_col++;
				
			}
			$row_index++;
		}
		
	}


	if(count($avg_fields) > 0)
	{
		$sheet->write($row_index, 0, "Overall Average ", $f_group_title);

		
		foreach($totalisation_fields as $key=>$value)
		{
			if($total_projects_for_avg[$key] > 0)
			{
				//$avg = round($value/$total_projects_for_avg[$key], 2);
				$avg =$value/$total_projects_for_avg[$key];
			}
			
			if(in_array($key, $avg_field_names))
			{
				if($totalisation_fields_format[$key] == 'decimal2'
					or $totalisation_fields_format[$key] == 'integer')
				{
					$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_decimal2_bold);
				}
				elseif($totalisation_fields_format[$key] == 'percent2')
				{
					$sheet->write($row_index, $totalisation_fields_column_index[$key], $avg, $f_group_percent2_bold);
				}
			}
			else
			{
				$sheet->write($row_index, $totalisation_fields_column_index[$key], "", $f_group_decimal2_bold);
			}

			$cell_index++;
		}
	}
}
else
{
	///list total_fields for data blocks
	if(array_key_exists("item_quantity_block", $data_blocks_captions) and $query_group01 != '')
	{
		$sheet->write($row_index, 0, "Overall Total", $f_group_title);
		$tmp_col = $caption_count_without_data_blocks + 1;
		foreach($data_blocks_captions["item_quantity_block"] as $key=>$value)
		{
			$sheet->write($row_index, $tmp_col,$item_totals["item_quantity_block_" . $key], $f_number_bold);
			$tmp_col++;
			
		}
		$row_index++;
	}
	
}


for($i=0;$i<count($captions);$i++)
{
	$reduction = 1;
	
	
	if($col_widths[$i] > 50)
	{
		$reduction = 0.7;
	}
	elseif($col_widths[$i] > 30)
	{
		$reduction = 0.75;
	}
	elseif($col_widths[$i] > 20)
	{
		$reduction = 0.78;
	}
	elseif($col_widths[$i] > 10)
	{
		$reduction = 0.88;
	}

	$sheet->setColumn($i+1, $i+1, $reduction*$col_widths[$i]);
}




$end_time = time();
$diff = $end_time - $start_time;


if(array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
$ips = $_SERVER['HTTP_X_FORWARDED_FOR'];
if(is_array ( $ips )) {
	$i = count($ips);
	$ip = $ips[$i-1];
}
else
{
	$ip = $ips;
}
}
else
{
$ip = $_SERVER['REMOTE_ADDR'];
}

$pageURL = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";;
$sql = "Insert into statistics (" .
   "statistic_user, statistic_ip, statistic_date, " .
   "statistic_url, statistic_duration) " .
   "values ('" .
   user_id() . "', '" .
   $ip . "', '" .
   date("Y-m-d-H-i-s") . "', '" .
   $pageURL . "', " .
   $diff . ")";

$res = mysql_query($sql);



$xls->close(); 

?>