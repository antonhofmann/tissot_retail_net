<?php
/********************************************************************

    consumption.php

    Enter parameters for the query of consumption

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2003-06-22
    Modified by:    Stefan Jaberg (sja@mediaparx.ch)
    Date modified:  2004-06-03
    Version:        1.2.0

    Copyright (c) 2003, Swatch AG, All Rights Reserved.

*********************************************************************/

die;
/*not in use anymore*/

require_once "../include/frame.php";
require_once "include/calculate_functions.php";
require_once("../lib/phpchartdir.php");

check_access("can_perform_queries");

ini_set("max_execution_time", "1800");


/********************************************************************
    prepare all data needed
*********************************************************************/
// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";

$sql_items = "select item_id, item_code, item_name ".
             "from items ";


$sql_order_items = "select item_id, item_code, item_name ".
             "from items ";


$item_filter = "";


$items_consumed = array();
$turn_over = array();
$list_total = 0;


/********************************************************************
    Create Form
*********************************************************************/ 

if(param("mode"))
{
    $mode = $_REQUEST["mode"];
    $from_date = $_REQUEST["from_date"];
    $to_date = $_REQUEST["to_date"];
    $country = $_REQUEST["country"];
    $all_items = $_REQUEST["all_items"];
	$show_charts = $_REQUEST["show_charts"];
}
else
{
    $mode = 0;
}


$form = new form("items", "consumption");


if($mode == 0)
{
    $form->add_hidden("mode", 1);
    $form->add_section("Period");
    $form->add_comment("Please enter the date in the form of dd.mm.yy");
    $form->add_comment("Only dates from 31.10.02 are possible since the system started on the 1st of November 2002!");

    $form->add_edit("from_date", "From Date", NOTNULL , "", TYPE_DATE, 20);
    $form->add_edit("to_date", "To Date", NOTNULL , "", TYPE_DATE, 20);

    $form->add_section("Country");
    $form->add_list("country", "Country*", $sql_countries);

    $form->add_comment("Please check in case you want to have the query performed over all items.");
    $form->add_checkbox("all_items", "Include all Items");
	
	$form->add_comment("Please check in case you want to have a graphical representation.");
    $form->add_checkbox("show_charts", "Show charts");

    $form->add_comment(" \n");
    $form->add_button("execute", "Execute Query");
}
else
{
    $form->add_label("from_date", "From Date", "", $from_date);
    $form->add_label("to_date", "To Date", "", $to_date);
    if (!$country)
    {
        $form->add_label("country", "Country", "", "");
    }
    else
    {
        $form->add_lookup("country", "Country", "countries", "country_name", 0, $country);
    }

    $form->add_comment("");
    $form->add_comment("The Quantities indicated are the ones having an actual arrival date!");
}



if($mode == 0)
{
    // itemlist
    $list = new ListView($sql_items, LIST_HAS_HEADER | LIST_SHOW_COUNT);

    $list->set_entity("items");
    $list->set_filter("item_type < 3");
    $list->set_order("item_code");

    $list->set_checkbox("category_items", "categories", param("category"));

    $list->add_column("item_code", "Code", "", LIST_FILTER_FREE);
    $list->add_column("item_name", "Name", "", LIST_FILTER_FREE);

    $list->process();
}
else
{
    // list of ordered items
    $list2 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

    $list2->set_entity("order_items");
    $list2->set_order("item_code");

}

$nr_items = 0;

if ($form->button("execute"))
{
    if ($form->validate())
    {
        
        if (!$all_items)
        {        
            $check_ids = array();

            foreach (array_keys($_REQUEST) as $key)
            {
                if (preg_match("/__check__(\d+)/", $key, $matches))
                {
                    $check_ids[$matches[1]] = true;
					
					$nr_items++;
				}
            }

            foreach (array_keys($check_ids) as $id)
            {
                if ($item_filter == "")
                {
                    $item_filter = $item_filter . " item_type < 3 and (item_id = " . $id;
                }
                else
                {
                    $item_filter = $item_filter . " or item_id = " . $id;
                }

                $items_delivered = get_item_total_consumption_data($id, $from_date, $to_date, $form->value("country"), "");

                $items_consumed[$id] = $items_delivered["item_consumption"];
                $turn_over[$id] = $items_delivered["turn_over"];
                $list_total = $list_total + $items_delivered["turn_over_1"];
			}

            if ($item_filter == "")
            {
                // user has not choosen any items
                $item_filter = "item_type = 999999999999";
            }
            else
                {
                $item_filter = $item_filter . ")";
            }
        }
        else
        {

            $item_filter = "item_type < 3";
            
            $sql = $sql_items . " where item_type < 3";
            $res = mysql_query($sql) or dberror($sql);
            while ($row = mysql_fetch_assoc($res))
            {
                $items_delivered = get_item_total_consumption_data($row["item_id"], $from_date, $to_date, $form->value("country"), "");
                $items_consumed[$row["item_id"]] = $items_delivered["item_consumption"];
                $turn_over[$row["item_id"]] = $items_delivered["turn_over"];
                $list_total = $list_total + $items_delivered["turn_over_1"];
				
				$nr_items++;
			}
        }

        
        $list2->set_filter($item_filter);
    }
}

if($mode == 1)
{
    $list2->add_column("item_code", "Code", "", LIST_FILTER_FREE);
    $list2->add_column("item_name", "Name", "", LIST_FILTER_FREE);
    $list2->add_text_column("consumption", "Delivered", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $items_consumed);
    $list2->add_text_column("turn_over", "Turn Over", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $turn_over);

    $list2->set_footer("item_code", "Total");
    $list2->set_footer("turn_over", number_format($list_total,2, ".", "'"));

    $list2->process();
	
    $link = "http://" . $_SERVER["HTTP_HOST"] . "/mis/delivered_by_item_xls.php?" . 
            "p1="  . $from_date .
            "&p2=" . $to_date . 
            "&p3=" . $country . 
            "&p4=" . $sql_order_items . 
            "&p5=" . $item_filter;

    $form->add_button("xls", "Export to Excel", $link);
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();



/********************************************************************
    Render Page
*********************************************************************/ 
   
$page = new Page("queries");

require "include/mis_page_actions.php";

$page->header();
$page->title("Consumption of Items");

$form->render();

if($mode == 0)
{
    $list->render();
}
else
{
    $list2->render();
	
	if($show_charts)
	{
		require_once "include/generate_charts.php";
	}
}

$page->footer();

?>