<?php
/********************************************************************

    order_queries_query_sortorder.php

    Sort Order of the query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2016-08-19
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2016-08-19
    Version:        1.0.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if(!has_access("can_perform_all_queries") 
  and !has_access("can_perform_projects_query"))
{
	redirect("noaccess.php");
}
require_once "include/orders_query_check_access.php";

require_once "include/orders_query_get_functions.php";
require_once "order_queries_get_query_params.php";

/********************************************************************
    prepare all data needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("order_queries.php");
}

$query_id = param("query_id");

$orderquery = get_query_name($query_id);
$icon = "../pictures/add_item.gif";
$link = "javascript:open_selector('')";




//get Query Fields
$fields = array();
$selected_field_order = array();
$general_sortorder = 0;

$sql = "select orderquery_fields, orderquery_field_order, orderquery_order, orderquery_order_desc " . 
       "from orderqueries " .
	   "where orderquery_id = " . $query_id;

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$tmp = unserialize($row["orderquery_fields"]);


	
	foreach($tmp as $key=>$field_array)
	{
		foreach($field_array as $field=>$caption)
		{
			$fields[$field] = $caption;
		}
	}
	
	

	$fields_order = $row["orderquery_field_order"];
	$fields_order_initial_value = $row["orderquery_field_order"];


	$fields_order = $row["orderquery_field_order"];
	$query_order = $row["orderquery_order"];
	$query_order_initial_value = $row["orderquery_order"];

	$general_sortorder = $row["orderquery_order_desc"];
}

if($query_order)
{
	$selected_field_order = array();
	$query_order = str_replace("selected_field_order[]=", "", $query_order);
	$selected_field_order = explode   ("&", $query_order);
	$table = "";
	$table .= '<table id="selected_field_order" cellspacing="0" cellpadding="2">';
	foreach($selected_field_order as $key=>$field)
	{
		if(array_key_exists($field,$fields) 
			and $db_info["attributes"][$field] != "calculated_content"
		    and $db_info["attributes"][$field] != "content_by_function"
			and $db_info["attributes"][$field] != "item_quantity_block")
		{
			$table .= '<tr id="' . $field . '"><td>' . str_replace('---- ', '', $fields[$field]) . '</td></tr>';
		}
	}
	$table .= '</table>';
}
elseif($fields_order)
{
	$selected_field_order = array();
	$fields_order = str_replace("selected_field_order[]=", "", $fields_order);
	$selected_field_order = explode("&", $fields_order);
	$table = "";
	$table .= '<table id="selected_field_order" cellspacing="0" cellpadding="2">';
	foreach($selected_field_order as $key=>$field)
	{
		if(array_key_exists($field,$fields) 
			and $db_info["attributes"][$field] != "calculated_content"
		    and $db_info["attributes"][$field] != "content_by_function")
		{
			$table .= '<tr id="' . $field . '"><td>' . str_replace('---- ', '', $fields[$field]) . '</td></tr>';
		}
	}
	$table .= '</table>';
}
elseif($selected_field_order)
{
	$selected_field_order = array();
	$selected_field_order = $fields;
	$table = "";
	$table .= '<table id="selected_field_order" cellspacing="0" cellpadding="2">';
	foreach($selected_field_order as $key=>$caption)
	{
		if(array_key_exists($field,$fields) 
			and $db_info["attributes"][$field] != "calculated_content"
		    and $db_info["attributes"][$field] != "content_by_function")
		{
			$table .= '<tr id="' . $key . '"><td>' . $fields[$key] . '</td></tr>';
		}
	}
	$table .= '</table>';
}
else
{
	$table = "";
}


/********************************************************************
    create form
*********************************************************************/

$form = new Form("orderqueries", "query_generator");

$form->add_hidden("query_id", param("query_id"));
$form->add_hidden("form_save", 1);
$form->add_section($orderquery["name"]);

$form->add_section(" ");
$form->add_comment("Please set the sort order of the output data just by dragging and dropping the lines in the following list of selected fields.");

$form->add_table($table);

$form->add_hidden("orderquery_order", $query_order_initial_value);

$form->add_checkbox("orderquery_order_desc", "Set general sort order to descending", $general_sortorder, 0, "Sort Order");



$form->add_button("submit", "Save Sort Order", 0);
if(check_if_query_has_fields($query_id) == true)
{
	$form->add_button("execute", "Execute Query");
}
$form->add_button("back", "Back to the List of Queries");


/********************************************************************
    process form
*********************************************************************/
$form->populate();
$form->process();

if($form->button("back"))
{
	redirect("order_queries.php");
}
elseif($form->button("submit"))
{
	
	$sql = "Update orderqueries SET " . 
		   "orderquery_order = " . dbquote($form->value("orderquery_order"))  . ", " .
		   "orderquery_order_desc = " . dbquote($form->value("orderquery_order_desc"))  . ", " .
		   "date_modified = " . dbquote(date("Y-m-d H:s:i")) . ", " . 
		    "user_modified = " . dbquote(user_login()) .
		   " where orderquery_id = " . param("query_id");

	$result = mysql_query($sql) or dberror($sql);

	redirect("order_queries_query_sortorder.php?query_id=" . param("query_id"));
}
elseif($form->button("execute"))
{
	redirect("order_queries_query_xls.php?query_id=" . param("query_id"));
}


/********************************************************************
    render
*********************************************************************/

$page = new Page("queries");

require_once("include/mis_page_actions.php");
$page->header();
$page->title("Edit Order Query - Sort Order");

require_once("include/orders_query_tabs.php");

$form->render();


?>

<script type="text/javascript">
$(document).ready(function() {
    // Initialise the table
    $("#selected_field_order").tableDnD();

	$('#selected_field_order').tableDnD({
        onDragClass: "myDragClass",
		onDrop: function(table, row) {
			document.forms[0].orderquery_order.value = $.tableDnD.serialize();
        }
    });
});
</script>

<?php

$page->footer();

?>


