<?php
/********************************************************************

    latest_logins_xls.php

    Enter parameters for the query of traffic

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-28-10
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-28-10
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_queries");
require_once "../include/phpexcel/PHPExcel.php";


/********************************************************************
    prepare all data needed
*********************************************************************/


//get all users
$statistics_data = array();


$sql = "select statistic_user, max(statistic_date) as lastdate from statistics_2012_01_2012_12 where statistic_user > 0 
group by statistic_user";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$statistics_data[$row["statistic_user"]] = $row["lastdate"];
}

$sql = "select statistic_user, max(statistic_date) as lastdate from statistics_2013_2014 where statistic_user > 0 
group by statistic_user";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$statistics_data[$row["statistic_user"]] = $row["lastdate"];
}

$sql = "select statistic_user, max(statistic_date) as lastdate from statistics_2014_2016 where statistic_user > 0 
group by statistic_user";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$statistics_data[$row["statistic_user"]] = $row["lastdate"];
}


$sql = "select statistic_user, max(statistic_date) as lastdate " . 
       "from statistics where statistic_user > 0 " .
       "group by statistic_user";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$statistics_data[$row["statistic_user"]] = $row["lastdate"];
}

asort($statistics_data);



$users = array();
$sql = "select user_id, concat(user_name, ' ', user_firstname) as username, address_company, users.date_created as dc, user_address " . 
       " from users " . 
	   " inner join addresses on address_id = user_address " . 
	   " where user_active = 1 " . 
	   " order by user_name, user_firstname";


$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$users[$row["user_id"]] = $row;
}


/********************************************************************
    XLS Params
*********************************************************************/
$captions['A'] = "Company";
$captions['B'] = "User";
$captions['C'] = "Latest Login";

$colwidth = array();
$colwidth['A'] = "15";
$colwidth['B'] = "15";
$colwidth['C'] = "15";

//output formats
$default_border = array(
    'style' => PHPExcel_Style_Border::BORDER_THIN,
    'color' => array('rgb'=>'1006A3')
);

$style_normal_border = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '1006A3'),
		),
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	)
);


$style_normal_border_right = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '1006A3'),
		),
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	),
);


$style_title = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
    'font' => array(
        'bold' => true,
		'size' => 16
    )
);

$style_header = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'font' => array(
        'bold' => true,
    )
);

$style_header_right = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	),
    'font' => array(
        'bold' => true,
    )
);

$style_bold = array(
	'font' => array(
        'bold' => true
    )
);



/********************************************************************
    Start output
*********************************************************************/
$objPHPExcel = new PHPExcel();

$logo = new PHPExcel_Worksheet_Drawing();
$logo->setName('Logo');
$logo->setDescription('Logo');
$logo->setPath('../pictures/brand_logo.jpg');
$logo->setHeight(36);
$logo->setWidth(113);

$sheet = $objPHPExcel->getActiveSheet();
$objPHPExcel->getActiveSheet()->setTitle('Latest Logins');

$logo->setWorksheet($objPHPExcel->getActiveSheet());


//default styles
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(10); 


// HEADRES ROW 1
$sheet->setCellValue('B1', 'Latest Logins ' . $modules[$selected_module] . ' (' . date("d.m.Y H:i:s") . ')');
$sheet->getStyle('B1')->applyFromArray( $style_title );
$sheet->getRowDimension('1')->setRowHeight(36);


$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(1);


$row_index = 3;



foreach($statistics_data as $user_id=>$last_date)
{
	
	if(array_key_exists($user_id, $users))
	{
		$cell_index = 0;
		
		
		
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $users[$user_id]["address_company"]);
		if($colwidth['A'] < strlen($users[$user_id]["address_company"])){$colwidth['A'] = strlen($users[$user_id]["address_company"]);}
		$cell_index++;


		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $users[$user_id]["username"] . " (" . $user_id . ")" );
		if($colwidth['B'] < strlen($users[$user_id]["username"])){$colwidth['B'] = strlen($users[$user_id]["username"]);}
		$cell_index++;


		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $last_date);
		if($colwidth['C'] < strlen(number_format ( $last_date , 0 , '.' , "'" ))){$colwidth['C'] = strlen(number_format ( $last_date , 0 , '.' , "'" ));}

		$row_index++;
		unset($users[$user_id]);
	}

}


$row_index++;
$row_index++;
$row_index++;
$row_index++;
$cell_index = 0;
$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "Active users that never logged in since 5.2.2012");
$row_index++;

foreach($users as $user_id=>$user_data)
{
	$cell_index = 0;
	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $user_data["address_company"]);
	if($colwidth['A'] < strlen($user_data["address_company"])){$colwidth['A'] = strlen($user_data["address_company"]);}
	$cell_index++;


	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $user_data["username"]. " (" . $user_id . ")" );
	if($colwidth['B'] < strlen($user_data["username"])){$colwidth['B'] = strlen($user_data["username"]);}
	$cell_index++;


	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $users[$user_id]["dc"]);
	if($colwidth['C'] < strlen(number_format ( $users[$user_id]["dc"] , 0 , '.' , "'" ))){$colwidth['C'] = strlen(number_format ( $users[$user_id]["dc"] , 0 , '.' , "'" ));}

	$row_index++;
}


//Format column heights and witdhs
foreach($colwidth as $col=>$width) {
	$sheet->getColumnDimension($col)->setWidth($width);
}


/********************************************************************
    Start output
*********************************************************************/
$filename = 'last_logins_by_user_' . $selected_module . '_'. date('Ymd H:i:s') . '.xls';
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
header('Pragma: ');

//send data
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');


?>