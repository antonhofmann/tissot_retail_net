<?php
include("phpchartdir.php");

#The data for the line chart
$data0 = array(3.42, 3.49, 3.33, 3.38, 3.51, 3.46, 3.29, 3.41, 3.44, 3.57, 3.59, 3.52, 3.37, 3.34, 3.51, 3.56, 3.56, 3.60, 3.70, 3.76, 3.63, 3.67, 3.75, 3.64, 3.50);
$data1 = array(4.42, 4.49, 4.33, 4.38, 4.51, 4.1, 4.29, 4.41, 4.44, 4.57, 4.59, 4.52, 4.37, 4.34, 4.51, 4.56, 4.56, 4.60, 4.70, 4.76, 4.63, 4.67, 4.75, 4.64, 4.50);
$data2 = array(0.36, 0.28, 0.25, 0.33, 0.38, 0.20, 0.22, 0.30, 0.25, 0.33, 0.30, 0.24, 0.28, 0.15, 0.21, 0.26, 0.46, 0.42, 0.48, 0.45, 0.43, 0.52, 0.64, 0.60, 0.54);
$data3 = array(1.36, 1.28, 1.25, 1.33, 1.38, 1.20, 1.22, 1.30, 1.25, 1.33, 1.30, 1.24, 1.28, 1.15, 1.21, 1.26, 1.46, 1.42, 1.48, 1.45, 1.43, 1.52, 1.64, 1.60, 1.54);
$data4 = array(2.36, 2.28, 2.25, 2.33, 2.38, 2.20, 2.22, 2.30, 2.25, 2.33, 2.30, 2.24, 2.28, 2.15, 2.21, 2.26, 2.46, 2.42, 2.48, 2.45, 2.43, 2.52, 2.64, 2.60, 2.54);

#The labels for the line chart
$labels = array("00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24");

#Create a XYChart object of size 580 x 200 pixels, with a pale yellow (0xC4DEBA)
#background, a black border, and 1 pixel 3D border effect
$c = new XYChart(580, 200, 0xC4DEBA, 0x0, 0);

#Set the plotarea at (50, 20) and of size 500 x 134 pixels, with white
#background. Turn on both horizontal and vertical grid lines with light grey
#color (0xc0c0c0)
$c->setPlotArea(50, 20, 500, 134, 0xffffff, -1, -1, 0xc0c0c0, -1);

#Add a legend box at (55, 25) (top of the chart) with horizontal layout. Use 8
#pts Arial font. Set the background and border color to Transparent.
//$legendObj = $c->addLegend(55, 25, false, "", 8);
//$legendObj->setBackground(Transparent);

#Add a title box to the chart using 11 pts Arial Bold Italic font. The text is
#white (0xffffff) on a dark red (0x800000) background, with a 1 pixel 3D border.
//$titleObj = $c->addTitle("Daily Server Load", "arialbi.ttf", 11, 0xffffff);
//$titleObj->setBackground(0x800000, -1, 1);

#Add a title to the y axis
$c->yAxis->setTitle("V/m");

#Maximal Wert bei y-Achse festlegen
$c->yAxis->setLinearScale(0, 10);

#Set the labels on the x axis
$c->xAxis->setLabels($labels);

#Add a title to the x axis
$c->xAxis->setTitle("Zeit");

#Add a line layer to the chart
$layer = $c->addLineLayer2();

#Set the default line width to 2 pixels
$layer->setLineWidth(2);

#Add the three data sets to the line layer. For demo purpose, we use a dash line
#color for the last line
$layer->addDataSet($data0, 0x009900, "Radio");
$layer->addDataSet($data1, 0xFBB12B, "TV");
$layer->addDataSet($data2, 0xCD0126, "GSM900");
$layer->addDataSet($data3, 0x005399, "GSM1800");
$layer->addDataSet($data4, 0xFFFF00, "UMTS");
//$layer->addDataSet($data2, $c->dashLineColor(0x3333ff, DashLine), "Server # 3");

#output the chart
header("Content-type: image/gif");
print($c->makeChart2(GIF));
?>
