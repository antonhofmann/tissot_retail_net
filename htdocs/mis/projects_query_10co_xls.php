<?php
/********************************************************************

    projects_query_10co_xls.php

    Generate Excel-File for Production Order Planning catalogue orders

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-02-07
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-02-07
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_queries");

require_once "../include/xls/Writer.php";
require_once "include/get_functions.php";
require_once("projects_query_filter_strings.php");

/********************************************************************
    prepare Data Needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("projects_queries.php");
}


//check if filter is present
$query_name = "";
$print_query_filter = 0;
$sql = "select mis_query_name, mis_query_filter, mis_print_filter from mis_queries " .
	   "where mis_query_id = " . param("query_id");

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{

	$query_name = $row["mis_query_name"];

	if($row["mis_print_filter"] == 1)
	{
		$print_query_filter = 1;
	}

	$filters = array();
	$filters = unserialize($row["mis_query_filter"]);

	foreach($filters as $key=>$value)
	{

		$fosc = $filters["fst"]; //from order state
		$tosc = $filters["tst"]; // to order state

		$clt = $filters["clt"]; // Client Types
		$gr = $filters["gr"]; // Geografical Regions
		$re = $filters["re"]; // Supplied Regions
		$cnt = $filters["co"]; // Countries

		$fdy = $filters["fdy"]; // preferred delivery Year From
		$fdm = $filters["fdm"]; // preferred delivery  Month From
		$tdy = $filters["tdy"]; // preferred delivery  Year To
		$tdm = $filters["tdm"]; // preferred delivery  Month To


		if(array_key_exists("fdy2", $filters))
		{
			$fdy2 = $filters["fdy2"]; // ready for pickup or pickup date Year From
			$fdm2 = $filters["fdm2"]; // ready for pickup or pickup date  Month From
			$tdy2 = $filters["tdy2"]; // ready for pickup or pickup date  Year To
			$tdm2 = $filters["tdm2"]; // ready for pickup or pickup date Month To
		
		}
		else
		{
			$fdy2 = "";
			$fdm2 = "";
			$tdy2 = "";
			$tdm2 = "";
		}

		if(array_key_exists("items", $filters))
		{
			$items = $filters["items"]; // Items
		
		}
		else
		{
			$items = "";
		}


		if(array_key_exists("icat", $filters))
		{
			$icats = $filters["icat"]; // Item categories
		
		}
		else
		{
			$icats = "";
		}


		$supp = $filters["supp"]; // suppliers
	}
}
else
{
	redirect("projects_queries.php");
}


$header = "Production Order Planning Catalogue Orders - " . $query_name . " for: ";
$header .= " (" . date("d.m.Y G:i") . ")";



/********************************************************************
    prepare Data
*********************************************************************/

$filter = "";

$clt = substr($clt,0, strlen($clt)-1); // remove last comma
$clt = str_replace("-", ",", $clt);
if($clt) // client type
{
    $filter =  " (address_client_type IN (" . $clt . "))";
	$_filter_strings["Client Types"] = get_filter_string("clt", $clt);
}

$gr = substr($gr,0, strlen($gr)-1); // remove last comma
$gr = str_replace("-", ",", $gr);
$re = substr($re,0, strlen($re)-1); // remove last comma
$re = str_replace("-", ",", $re);

if($filter) {

	if($gr and $re) 
	{
		$filter .=  " and (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	}
	elseif($gr) // geografical regions
	{
		$filter .=  " and (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	}
	elseif($re) //supplying
	{
		$filter .=  " and (country_region IN (" . $re . "))";
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	}
}
else
{
	if($gr and $re) 
	{
		$filter .=  "  (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	}
	elseif($gr) // geografical regions
	{
		$filter .=  "  (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	}
	elseif($re) //supplying
	{
		$filter .=  "  (country_region IN (" . $re . "))";
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	}
}

$cnt = substr($cnt,0, strlen($cnt)-1); // remove last comma
$cnt = str_replace("-", ",", $cnt);
if($cnt and $filter) // country
{
    $filter .=  " and (order_shop_address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);

}
elseif($cnt)
{
    $filter =  "  (order_shop_address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);
}



/*
if($filter and $fosc) // from order state
{
    $filter.=  " and order_actual_order_state_code >= '" . $fosc . "' ";
	$_filter_strings["From Order State Code"] = $fosc;
}
elseif($fosc)
{
	 $filter.=  "  order_actual_order_state_code >= '" . $fosc . "' ";
	 $_filter_strings["From Order State Code"] = $fosc;
}

if($filter and $tosc) // to order state
{
    $filter.=  " and order_actual_order_state_code <= '" . $tosc . "' ";
	$_filter_strings["To Order State Code"] = $tosc;
}
elseif($tosc)
{
	$filter.=  "  order_actual_order_state_code <= '" . $tosc . "' ";
	$_filter_strings["To Order State Code"] = $tosc;
}
*/

//get order_state_ids
$order_state_filter = "";
if($fosc and $tosc)
{
	$order_state_filter = "order_state_code >= '" . $fosc . "'";
	$order_state_filter .= " and order_state_code <= '" . $tosc . "'";

}
elseif($fosc)
{
	$order_state_filter = "order_state_code >= '" . $fosc . "'";

}
elseif($tosc)
{
	$order_state_filter = " order_state_code <= '" . $tosc . "'";

}

$order_ids = array();
if($order_state_filter)
{
	
	$sql = "select distinct actual_order_state_order " . 
		   " from actual_order_states " . 
		   " left join orders on order_id = actual_order_state_order " .
		   " left join order_states on order_state_id = actual_order_state_state " . 
		   " left join order_items on order_item_order = order_id " . 
		   " where order_actual_order_state_code < '890' " . 
			" and (order_archive_date is null or order_archive_date = '0000-00-00') " .
		   " and (order_item_pickup is null or order_item_pickup = '0000-00-00' ) " .
		   " and order_item_type = 1 " .
	       " and " . $order_state_filter;

	$res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
		$order_ids[$row["actual_order_state_order"]] = $row["actual_order_state_order"];
	}
	
	if(count($order_ids) > 0)
	{
		if($filter)
		{
			$filter .= " and order_id in (" . implode(',', $order_ids). ") ";
		}
		else
		{
			$filter = " order_id in (" . implode(',', $order_ids). ") ";
		}
	}
}




if($filter) // preferred delivery from
{
	if($fdy > 0 and $fdm > 0)
	{
		$tmp = "" . $fdy . "" . $fdm;
		$filter.=  " and concat(YEAR(order_preferred_delivery_date), DATE_FORMAT(order_preferred_delivery_date,'%m')) >= " . $tmp;
		$_filter_strings["Preferred delivery from"] = $fdy . "-" . $fdm;
	}
	elseif($fdy > 0)
	{
		$filter.=  " and YEAR(order_preferred_delivery_date) >= " . $fdy;
		$_filter_strings["Preferred delivery from"] = $fdy;
	}
	elseif($fdm > 0)
	{
		$filter.=  " and MONTH(order_preferred_delivery_date) >= " . $fdm;
		$_filter_strings["Preferred delivery from"] = $fdm;
	}
}
else
{
	if($fdy > 0 and $fdm > 0)
	{
		$tmp = "" . $fdy . "" . $fdm;
		$filter.=  " concat(YEAR(order_preferred_delivery_date), DATE_FORMAT(order_preferred_delivery_date,'%m')) >= " . $tmp;
		$_filter_strings["Preferred delivery from"] = $fdy . "-" . $fdm;;
	}
	elseif($fdy > 0)
	{
		$filter.=  " YEAR(order_preferred_delivery_date) >= " . $fdy;
		$_filter_strings["Preferred delivery from"] = $fdy;
	}
	elseif($fdm > 0)
	{
		$filter.=  " MONTH(order_preferred_delivery_date) >= " . $fdm;
		$_filter_strings["Preferred delivery from"] = $fdm;
	}
}

if($filter) // preferred delivery to
{
	if($tdy > 0 and $tdm > 0)
	{
		$tmp = "" . $tdy . "" . $tdm;
		$filter.=  " and concat(YEAR(order_preferred_delivery_date), DATE_FORMAT(order_preferred_delivery_date,'%m')) <= " . $tmp;
		$_filter_strings["Preferred delivery to"] = $tdy . "-" . $tdm;
	}
	elseif($tdy > 0)
	{
		$filter.=  " and YEAR(order_preferred_delivery_date) <= " . $tdy;
		$_filter_strings["Preferred delivery to"] = $tdy;
	}
	elseif($tdm > 0)
	{
		$filter.=  " and MONTH(order_preferred_delivery_date) <= " . $tdm;
		$_filter_strings["Preferred delivery to"] = $tdm;
	}
}
else
{
	if($tdy > 0 and $tdm > 0)
	{
		$tmp = "" . $tdy . "" . $tdm;
		$filter.=  "  concat(YEAR(order_preferred_delivery_date), DATE_FORMAT(order_preferred_delivery_date,'%m')) <= " . $tmp;
		$_filter_strings["Preferred delivery to"] = $tdy . "-" . $tdm;
	}
	elseif($tdy > 0)
	{
		$filter.=  "  YEAR(order_preferred_delivery_date) <= " . $tdy;
		$_filter_strings["Preferred delivery to"] = $tdy;
	}
	elseif($tdm > 0)
	{
		$filter.=  "  MONTH(order_preferred_delivery_date) <= " . $tdm;
		$_filter_strings["Preferred delivery to"] = $tdm;
	}
}



$pickup_date_filter_used = false;
$pickup_date_filter = "";

	
if($fdy2 > 0 and $fdm2 > 0)
{
	$tmp = "" . $fdy2 . "" . $fdm2;
	$pickup_date_filter.=  " and concat(YEAR(order_item_ready_for_pickup), DATE_FORMAT(order_item_ready_for_pickup,'%m')) >= " . $tmp;
	$_filter_strings["Ready for Pickup date from"] = $fdy2 . "-" . $fdm2;
	$pickup_date_filter_used = true;
}
elseif($fdy2 > 0)
{
	$pickup_date_filter.=  " and YEAR(order_item_ready_for_pickup) >= " . $fdy2;
	$_filter_strings["Ready for Pickup date from"] = $fdy2;
	$pickup_date_filter_used = true;
}
elseif($fdm2 > 0)
{
	$pickup_date_filter.=  " and MONTH(order_item_ready_for_pickup) >= " . $fdm2;
	$_filter_strings["Ready for Pickup date from"] = $fdm2;
	$pickup_date_filter_used = true;
}


if($tdy2 > 0 and $tdm2 > 0)
{
	$tmp = "" . $tdy2 . "" . $tdm2;
	$pickup_date_filter.=  " and concat(YEAR(order_item_ready_for_pickup), DATE_FORMAT(order_item_ready_for_pickup,'%m')) <= " . $tmp;
	$_filter_strings["Ready for Pickup date to"] = $tdy2 . "-" . $tdm2;
	$pickup_date_filter_used = true;
}
elseif($tdy2 > 0)
{
	$pickup_date_filter.=  " and YEAR(order_item_ready_for_pickup) <= " . $tdy2;
	$_filter_strings["Ready for Pickup date to"] = $tdy2;
	$pickup_date_filter_used = true;
}
elseif($tdm2 > 0)
{
	$pickup_date_filter.=  " and MONTH(order_item_ready_for_pickup) <= " . $tdm2;
	$_filter_strings["Ready for Pickup date to"] = $tdm2;
	$agreed_opening_date_filter_used = true;
}


//suppliers

$suppliers = substr($supp,0, strlen($supp)-1); // remove last comma
$suppliers = str_replace("-", ",", $suppliers);

$supplier_filter = "";
$supplier_filter2 = "";
if($supp)
{
	$supplier_filter = " and supplier_address IN (" . $suppliers . ") ";
	$supplier_filter2 = " and order_item_supplier_address IN (" . $suppliers . ") ";
}

//items
$items = substr($items,0, strlen($items)-1); // remove last comma
$items = str_replace("-", ",", $items);

$icats = substr($icats,0, strlen($icats)-1); // remove last comma
$icats = str_replace("-", ",", $icats);

$item_filter = "";
$order_item_filter = "";

$item_category_filter = "";
$order_item_category_filter = "";

if($items)
{
	$item_filter = " and item_id IN (" . $items . ") ";
	$order_item_filter = " and order_item_item IN (" . $items . ") ";
}
elseif($icats)
{
	$item_category_filter = " and item_category IN (" . $icats . ") ";
	$order_item_category_filter = " and item_category IN (" . $icats . ") ";
}


//get items filtered by ready for pickup or pickup date
$items_filtered_by_pick_up_date = array();
if($pickup_date_filter_used == true)
{
	
	$sql_i = "select DISTINCT item_id " . 
		 "from orders " . 
		 "left join order_items on order_item_order = order_id " .
		 "left join items on item_id = order_item_item " .
		 "left join suppliers on supplier_item = item_id " .
		 "left join countries on country_id = order_shop_address_country " . 
		 "left join addresses on address_id = order_client_address " . 
		 " where order_type = 2 and order_actual_order_state_code < '890' " . 
		 " and (order_archive_date is null or order_archive_date = '0000-00-00') " .
		 " and item_id > 0 " . 
		 $item_filter .
		 $item_categoryfilter .
		 $supplier_filter .
		 $pickup_date_filter;

	$res_i = mysql_query($sql_i) or dberror($sql_i);
	while ($row_i = mysql_fetch_assoc($res_i))
	{
		$items_filtered_by_pick_up_date[] = $row_i["item_id"];
	}
}

if(count($items_filtered_by_pick_up_date) > 0 and $item_filter)
{
	
	$item_filter = " and item_id IN (" . implode(',', $items_filtered_by_pick_up_date). ") ";
	$order_item_filter = " and order_item_item IN (" . implode(',', $items_filtered_by_pick_up_date). ") ";

	
}

/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "production_order_planning_" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename); 
$xls->setVersion(8);
$sheet =& $xls->addWorksheet("Summary");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
$header_row =& $xls->addFormat();
$header_row->setSize(10);
$header_row->setAlign('left');
$header_row->setBold();

$header_row_vertical =& $xls->addFormat();
$header_row_vertical->setSize(10);
$header_row_vertical->setAlign('left');
$header_row_vertical->setBold();
$header_row_vertical->setTextRotation(270);

$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);


$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_decimal =& $xls->addFormat();
$f_decimal->setSize(8);
$f_decimal->setAlign('right');
$f_decimal->setBorder(1);
$f_decimal->setNumFormat('0.00');


$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_decimal =& $xls->addFormat();
$f_decimal->setSize(8);
$f_decimal->setAlign('right');
$f_decimal->setBorder(1);
$f_decimal->setNumFormat('0.00');

$f_date =& $xls->addFormat();
$f_date->setSize(8);
$f_date->setAlign('left');
$f_date->setBorder(1);
$f_date->setNumFormat('DD.MM.YYYY');

$f_used =& $xls->addFormat();
$f_used->setSize(8);
$f_used->setBorder(1);
$f_used->setPattern(2);
$f_used->setBgColor('yellow');


$f_vertical =& $xls->addFormat();
$f_vertical->setSize(9);
$f_vertical->setAlign('left');
$f_vertical->setBorder(1);
$f_vertical->setTextRotation(270);



/********************************************************************
    write all data summary
*********************************************************************/
$sheet->write(0, 0, $header, $header_row);
$row_index = 2;

if($print_query_filter == 1)
{
	foreach($_filter_strings as $key=>$value)
	{
		 $sheet->write($row_index, 0,$key . ": " . $value);
		 $row_index++;
	}
	$row_index++;
}

$row_index_pl_header = $row_index;


$col_index = 0;


$product_lines_with_items = array();


//itemlist
$items_used = array();
$filter_tmp = $filter;
if($filter and substr($filter, 0, 4 !=' and')){$filter_tmp= ' and ' . $filter;}

$itemrowpositions = array();
$itemtotals = array();
$category = '';


$sql_i = 'SELECT DISTINCT item_category_id, item_category_name, item_id, ' . 
		 'item_code, item_name, item_category ' . 
		 'from items ' .
		 "left join suppliers on supplier_item = item_id " .
		 'left join item_categories on item_category_id = item_category ' .
		 'where item_visible_in_production_order = 1 ' . 
		 $item_filter .
		 $item_category_filter .
		 $supplier_filter .
		 ' order by item_category_sortorder, item_code ';


$res_i = mysql_query($sql_i) or dberror($sql_i);
while ($row_i = mysql_fetch_assoc($res_i))
{
	
	if($pickup_date_filter_used == true)
	{
		$item_total = 0;
		
		
		//only consider items not having a request for delivery
		if($tosc <= '700')
		{
			$sql_s = "select sum(order_item_quantity) as amount " . 
					 "from orders " . 
					 "left join order_items on order_item_order = order_id " .
				     "left join items on item_id = order_item_item " .
					 "left join countries on country_id = order_shop_address_country " . 
					 "left join addresses on address_id = order_client_address " . 
					 " where order_type = 2 and order_actual_order_state_code < '890' " . 
					 " and (order_archive_date is null or order_archive_date = '0000-00-00') " .
					 $filter_tmp . 
					 $order_item_filter .
					 $supplier_filter2 .
					 $pickup_date_filter . 
					 " and order_item_item = " . $row_i['item_id'] . 
					 " and order_item_quantity > 0 " .
				     " and (order_item_ready_for_pickup is null or order_item_ready_for_pickup = '0000-00-00' ) " .
					 " and (order_item_pickup is null or order_item_pickup = '0000-00-00' ) ";
		}
		else
		{
			$sql_s = "select sum(order_item_quantity) as amount " . 
					 "from orders " . 
					 "left join order_items on order_item_order = order_id " .
				     "left join items on item_id = order_item_item " .
					 "left join countries on country_id = order_shop_address_country " . 
					 "left join addresses on address_id = order_client_address " . 
					 " where order_type = 2 and order_actual_order_state_code < '890' " . 
					 " and (order_archive_date is null or order_archive_date = '0000-00-00') " .
					 $filter_tmp . 
					 $order_item_filter .
					 $supplier_filter2 .
					 $pickup_date_filter . 
					 " and order_item_item = " . $row_i['item_id'] . 
					 " and order_item_quantity > 0 " . 
					 " and (order_item_pickup is null or order_item_pickup = '0000-00-00' ) ";
		}
		
		
		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$item_total = $item_total + $row_s['amount'];
		}

		if($item_total > 0)
		{
		
			if($category != $row_i["item_category_id"])
			{
				$category = $row_i["item_category_id"];
				$sheet->write($row_index, $col_index, $row_i['item_category_name'], $header_row);
				$row_index++;

			}
			
			$sheet->write($row_index, $col_index, $row_i['item_code'], $f_normal);
			$sheet->setColumn($col_index, $col_index, 20);
			$sheet->write($row_index, $col_index+1, $row_i['item_name'], $f_normal);
			$sheet->setColumn($col_index, $col_index+1, 60);

			$sheet->write($row_index, $col_index+2, $item_total_ready_for_pickup, $f_number);
			$sheet->write($row_index, $col_index+3, $item_total_picked_up, $f_number);
			$sheet->write($row_index, $col_index+4, $item_total, $f_number);
			$items_used[] = $row_i['item_id'];
			
			$row_index++;
		}
		
	}
	else
	{
		$item_total = 0;
		
		
		//only consider items not having a request for delivery
		if($tosc <= '700')
		{
			$sql_s = "select sum(order_item_quantity) as amount " . 
					 "from orders " . 
					 "left join order_items on order_item_order = order_id " .
				     "left join items on item_id = order_item_item " .
					 "left join countries on country_id = order_shop_address_country " . 
					 "left join addresses on address_id = order_client_address " . 
					 " where order_type = 2 and order_actual_order_state_code < '890' " . 
					 " and (order_archive_date is null or order_archive_date = '0000-00-00') " .
				     " and (order_item_ready_for_pickup is null or order_item_ready_for_pickup = '0000-00-00' ) " .
					 " and (order_item_pickup is null or order_item_pickup = '0000-00-00' ) " .
					 $filter_tmp . 
					 $order_item_filter .
					 $supplier_filter2 .
					 $pickup_date_filter . 
					 " and order_item_item = " . $row_i['item_id']; 
		}
		else
		{
			$sql_s = "select sum(order_item_quantity) as amount " . 
					 "from orders " . 
					 "left join order_items on order_item_order = order_id " .
				     "left join items on item_id = order_item_item " .
					 "left join countries on country_id = order_shop_address_country " . 
					 "left join addresses on address_id = order_client_address " . 
					 " where order_type = 2 and order_actual_order_state_code < '890' " . 
					 " and (order_archive_date is null or order_archive_date = '0000-00-00') " .
					 " and (order_item_pickup is null or order_item_pickup = '0000-00-00' ) " .
					 $filter_tmp . 
					 $order_item_filter .
					 $supplier_filter2 .
					 $pickup_date_filter . 
					 " and order_item_item = " . $row_i['item_id']; 
		}

		$res_s = mysql_query($sql_s) or dberror($sql_s);
		if($row_s = mysql_fetch_assoc($res_s))
		{
			$item_total = $row_s['amount'];
		}

		if($item_total > 0)
		{
		
			if($category != $row_i["item_category_id"])
			{
				$category = $row_i["item_category_id"];
				$sheet->write($row_index, $col_index, $row_i['item_category_name'], $header_row);
				$row_index++;

			}
			
			$sheet->write($row_index, $col_index, $row_i['item_code'], $f_normal);
			$sheet->setColumn($col_index, $col_index, 20);
			$sheet->write($row_index, $col_index+1, $row_i['item_name'], $f_normal);
			$sheet->setColumn($col_index, $col_index+1, 60);

			$sheet->write($row_index, $col_index+2, $item_total, $f_number);
			$items_used[] = $row_i['item_id'];
			
			$row_index++;
		}
	}
}


/********************************************************************
    write all data detail for product lines
*********************************************************************/

$sheetnumber = 2;

	
$sheet =& $xls->addWorksheet('Catalogue Orders');
$sheet->setInputEncoding("UTF-8");
$header = "Production Order Planning Catalogue Orders- " . $query_name . " for: ";

$header .= " (" . date("d.m.Y G:i") . ")";

$xls->activesheet = 2;
$sheet->write(0, 0, $header, $header_row);


$row_index = 3;
$col_index = 6;
$num_of_item_columns = 7;



$sheet->write(3, 0, "Counter", $f_vertical);
$sheet->write(3, 2, "Client", $f_vertical);
$sheet->write(3, 3, "Country", $f_vertical);
$sheet->write(3, 4, "Order Number", $f_vertical);
$sheet->write(3, 5, "Order Status", $f_vertical);


//itemlist
$itemcolpositions = array();
$itemtotals = array();
$category = '';





if(count($items_used) > 0)
{
	$item_filter .= ' and item_id IN (' . implode(',', $items_used) . ') ';
}



$sql_i = 'SELECT DISTINCT item_category_id, item_category_name, item_id, ' . 
		 'item_code, item_name, item_category ' . 
		 'from items ' .
		 "left join suppliers on supplier_item = item_id " .
		 'left join item_categories on item_category_id = item_category ' .
		 'where item_visible_in_production_order = 1 ' . 
		 $item_filter .
		 $item_category_filter .
		 $supplier_filter .
		 ' order by item_category_sortorder, item_code ';
		 //' where item_visible_in_production_order = 1 and item_active = 1 ' . $item_filter .


$res_i = mysql_query($sql_i) or dberror($sql_i);

while ($row_i = mysql_fetch_assoc($res_i))
{
	
	if($category != $row_i["item_category_id"])
	{
		$category = $row_i["item_category_id"];
		$sheet->write($row_index-1, $col_index, $row_i['item_category_name'], $header_row_vertical);
	}
	
	
	$sheet->write($row_index, $col_index, $row_i['item_code'],$f_vertical);

	$itemcolpositions[$row_i['item_id']] = $col_index;
	$itemtotals[$row_i['item_id']] = 0;
	$col_index++;
	$num_of_item_columns++;
}

for($i=6;$i<=$num_of_item_columns;$i++)
{
	$sheet->setColumn($i, $i, 3);
}


//order list
$columnnumber = 1;
$row_index = 7;
$col_index = 0;


//compose order filter
$filter_tmp = $filter;
if($filter and substr($filter, 0, 4 !=' and')){$filter_tmp= ' and ' . $filter;}


$selected_orders = array();
$sql_s = "select DISTINCT order_id " . 
		 "from orders " . 
		 "left join order_items on order_item_order = order_id " .
		 "left join items on item_id = order_item_item " . 
		 "left join countries on country_id = order_shop_address_country " . 
		 "left join addresses on address_id = order_client_address " . 
		 " where order_type = 2 and order_actual_order_state_code < '890' " . 
		 " and (order_archive_date is null or order_archive_date = '0000-00-00') " .
		 " and order_item_quantity > 0 and order_item_type = 1 " . 
		 " and (order_item_pickup is null or order_item_pickup = '0000-00-00' ) " .
		 $filter_tmp . 
		 $order_item_filter .
		 $order_item_category_filter . 
		 $supplier_filter2 .
		 $pickup_date_filter;




$res_s = mysql_query($sql_s) or dberror($sql_s);
while($row_s = mysql_fetch_assoc($res_s))
{
	
	//check if all items were requested for delivery to forwarders
	if($tosc <= '700')
	{
		//get the number of items in the order
		$num_items = 0;
		$sql_i ="select count(order_item_id) as num_items " . 
				 "from orders " . 
				 "left join order_items on order_item_order = order_id " .
				 "left join items on item_id = order_item_item " . 
				 "left join countries on country_id = order_shop_address_country " . 
				 "left join addresses on address_id = order_client_address " .
				 " where order_actual_order_state_code < '890' " . 
				 " and (order_archive_date is null or order_archive_date = '0000-00-00') " .
				 " and order_item_quantity > 0 and order_item_type = 1 " . 
				 " and (order_item_pickup is null or order_item_pickup = '0000-00-00' ) " .
				 $filter_tmp . 
				 $order_item_filter .
			     $order_item_category_filter . 
				 $supplier_filter2 .
				 $pickup_date_filter . 
			     " and order_id = " . $row_s["order_id"];

		

		$res_i = mysql_query($sql_i) or dberror($sql_i);
		if ($row_i = mysql_fetch_assoc($res_i))
		{
			$num_items = $row_i["num_items"];
		}

		//get the number of items having a request for delivery
		$num_items2 = 0;
		$sql_i ="select count(DISTINCT order_item_id) as num_items " . 
				 "from orders " . 
				 "left join order_items on order_item_order = order_id " .
				 "left join items on item_id = order_item_item " . 
				 "left join countries on country_id = order_shop_address_country " . 
				 "left join addresses on address_id = order_client_address " .
			     " left join dates on date_order_item = order_item_id " . 
				 " where order_actual_order_state_code < '890' " . 
				 " and (order_archive_date is null or order_archive_date = '0000-00-00') " .
				 " and order_item_quantity > 0 and order_item_type = 1 " . 
				 " and (order_item_pickup is null or order_item_pickup = '0000-00-00' ) " .
				 $filter_tmp . 
				 $order_item_filter .
			     $order_item_category_filter . 
				 $supplier_filter2 .
				 $pickup_date_filter .  
			     " and date_type = 1" . 
			     " and order_id = ". $row_s["order_id"];

		$res_i = mysql_query($sql_i) or dberror($sql_i);
		if ($row_i = mysql_fetch_assoc($res_i))
		{
			$num_items2 = $row_i["num_items"];
		}
		
		if($num_items2 < $num_items)
		{
			$selected_orders[] = $row_s['order_id'];
		}
	}
	else
	{
			$selected_orders[] = $row_s['order_id'];
	}
}

$order_filter = "order_id = 0";
if(count($selected_orders) > 0)
{
	$order_filter = 'order_id IN (' . implode(',', $selected_orders) . ') ';
}


if($pickup_date_filter_used == true)
{
	
	$sheet->write(3, 1, "Ready for Pickup Date", $f_vertical);
	
	$tmp = array();
	$order_items_filter = "";
	foreach($itemcolpositions as $item_id=>$value)
	{
		$tmp[] = $item_id;
	}
	if(count($tmp) > 0)
	{
		$order_items_filter = " and order_item_item IN (" . implode(',', $tmp) . ")";
	}


	$sheet->write($row_index-1, 0, "Ready for Pickup", $header_row);

	//ready for pickup
	$sql_p = "select DISTINCT order_id, address_company, order_number, " . 
			 "order_preferred_delivery_date, order_number, " . 
			 " country_name, order_item_ready_for_pickup, order_actual_order_state_code " . 
			 "from orders " . 
			 "left join addresses on address_id = order_client_address " .
		     "left join countries on country_id = address_country " .
		     "left join order_items on order_item_order = order_id  " .
			 " where " . $order_filter . 
		     " and (order_item_pickup is null or order_item_pickup = '0000-00-00' ) " .
		     " and order_item_quantity > 0 and order_item_type = 1 " .
		     $order_items_filter;
	$sql_p .= " order by order_item_ready_for_pickup, order_number";


	$res = mysql_query($sql_p) or dberror($sql_p);
	while ($row = mysql_fetch_assoc($res))
	{
		$ready_for_pickup_date = mysql_date_to_xls_date($row['order_item_ready_for_pickup']);
		

		$sheet->write($row_index, $col_index, $columnnumber, $f_normal);
		$col_index++;
		$sheet->write($row_index, $col_index, $ready_for_pickup_date, $f_date);
		$col_index++;
		$sheet->write($row_index, $col_index, $row['address_company'], $f_normal);
		$col_index++;
		$sheet->write($row_index, $col_index, $row['country_name'], $f_normal);
		$col_index++;
		$sheet->write($row_index, $col_index, $row['order_number'], $f_normal);
		$col_index++;

		$sheet->write($row_index, $col_index, $row['order_actual_order_state_code'], $f_normal);
		$col_index++;

		
		
		foreach($itemcolpositions as $item_id=>$colposition) {
			
			$sql_s = 'select sum(order_item_quantity) as amount ' . 
					 'from order_items ' . 
					 'where order_item_item = "' . $item_id . '" ' .
					 ' and order_item_order = "' . $row['order_id'] . '"' . 
				      " and (order_item_pickup is null or order_item_pickup = '0000-00-00' ) " .
				     " and order_item_ready_for_pickup = " . dbquote($row['order_item_ready_for_pickup']);

			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$sheet->write($row_index, $col_index, $row_s['amount'], $f_number);
				$itemtotals[$item_id] = $itemtotals[$item_id] + $row_s['amount'];
				$col_index++;
			
			}
			
		}
		
		$col_index = 0;
		$row_index++;
		$columnnumber++;
	}


	
	$col_index = 5;
	$sheet->write(4, $col_index, "Item Totals", $f_normal);
	$col_index++;
	foreach($itemtotals as $item_id=>$total)
	{
		$sheet->write(4, $col_index, $total, $f_number);
		$col_index++;
	}
}
else
{
	
	$sheet->write(3, 1, "Preferred Arrival Date", $f_vertical);
	
	$sql_p = "select DISTINCT order_id, address_company, order_number, " . 
			 "order_preferred_delivery_date, order_number, " . 
			 " country_name, order_actual_order_state_code " . 
			 "from orders " . 
			 "left join addresses on address_id = order_client_address " .
		     "left join countries on country_id = address_country " .
		     "left join order_items on order_item_order = order_id  " .
			 " where " . $order_filter . 
		     " and (order_item_pickup is null or order_item_pickup = '0000-00-00' ) " .
		     " and order_item_quantity > 0 and order_item_type = 1 " .
		     $order_items_filter;
	
	/*
	
	$sql_p = "select DISTINCT order_id, order_address_company, order_number, " . 
			 "order_preferred_delivery_date, order_number, " . 
			 " country_name, place_name " . 
			 "from orders " .
		     "left join order_addresses on order_address_order = order_id " .
			 "left join countries on country_id = order_address_country " .
		     "left join places on place_id = order_address_place_id " .
			 "left join addresses on address_id = order_client_address " .
			 " where order_address_type = 2 and " . $order_filter;
			  
	*/	
	
	if($agreed_opening_date_filter_used == true)
	{
		$sql_p .= " order by order_number";
	}
	else
	{
		$sql_p .= " order by order_preferred_delivery_date, order_number";
	}


	$res = mysql_query($sql_p) or dberror($sql_p);
	while ($row = mysql_fetch_assoc($res))
	{
		$preferred_arrival_date = mysql_date_to_xls_date($row['order_preferred_delivery_date']);
		

		$sheet->write($row_index, $col_index, $columnnumber, $f_normal);
		$col_index++;
		$sheet->write($row_index, $col_index, $preferred_arrival_date, $f_date);
		$col_index++;
		$sheet->write($row_index, $col_index, $row['address_company'], $f_normal);
		$col_index++;
		$sheet->write($row_index, $col_index, $row['country_name'] , $f_normal);
		$col_index++;
		$sheet->write($row_index, $col_index, $row['order_number'], $f_normal);
		$col_index++;

		$sheet->write($row_index, $col_index, $row['order_actual_order_state_code'], $f_normal);
		$col_index++;

		
		
		
		foreach($itemcolpositions as $item_id=>$colposition) {
			
			$sql_s = 'select sum(order_item_quantity) as amount ' . 
					 'from order_items ' . 
					 'where order_item_item = "' . $item_id . '" ' .
					 ' and order_item_order = "' . $row['order_id'] . '"' .
				     " and (order_item_pickup is null or order_item_pickup = '0000-00-00' ) ";

			$res_s = mysql_query($sql_s) or dberror($sql_s);
			if($row_s = mysql_fetch_assoc($res_s))
			{
				$sheet->write($row_index, $col_index, $row_s['amount'], $f_number);
				$itemtotals[$item_id] = $itemtotals[$item_id] + $row_s['amount'];
				$col_index++;
			
			}
			
		}
		
		$col_index = 0;
		$row_index++;
		$columnnumber++;
	}

	$col_index = 5;
	$sheet->write(4, $col_index, "Item Totals", $f_normal);
	$col_index++;
	foreach($itemtotals as $item_id=>$total)
	{
		$sheet->write(4, $col_index, $total, $f_number);
		$col_index++;
	}
}




$sheet->setColumn(0, 0, 3);
$sheet->setColumn(1, 1, 8);
$sheet->setColumn(2, 2, 30);
$sheet->setColumn(3, 3, 20);
$sheet->setColumn(4, 4, 15);
$sheet->setColumn(5, 5, 10);
$sheet->setColumn(6, 6, 8);
$xls->close(); 

?>
