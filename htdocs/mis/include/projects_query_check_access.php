<?php

/********************************************************************

    projects_query_check_access.php

    Defines the possible page actions depending on access rights

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-10-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-10-14
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

$sql = "select * from projectqueries where projectquery_id =" . dbquote(param("query_id"));
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	if($row["projectquery_owner"] != user_id())
	{
		
		$sql = "select projectquery_permission_id " . 
			   " from projectquery_permissions " . 
			   " where projectquery_permission_query =" . dbquote(param("query_id")) . 
			   " and projectquery_permission_user = " . dbquote(user_id());

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			
		}
		else
		{
			redirect("noaccess.php");
		}
	}
}
?>