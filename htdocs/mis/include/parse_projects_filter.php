<?php

/********************************************************************
    prepare all data needed
*********************************************************************/

//get costmonitoring furniture groups
$costmonitoring_groups = array();
$costmonitoring_group_filter = "";
/*
$sql = "select costmonitoringgroup_id from costmonitoringgroups " . 
       "where costmonitoringgroup_include_in_masterplan = 1 ";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
  $costmonitoring_groups[] = $row["costmonitoringgroup_id"];
}


if(count($costmonitoring_groups > 0)) {

  $costmonitoring_group_filter = " or order_items_costmonitoring_group IN (" . implode(",", $costmonitoring_groups) .") ";
  
}
*/

// disable owners, it's no longer needed
$owners = false;

$states = array();
$states[1] = "Projects in progress";
$states[2] = "Projects on hold";
$states[4] = "Operating POS Locations";
$states[6] = "Projects cancelled";


//print query from project list
if(array_key_exists('cfplist', $_GET) and $_GET["cfplist"] == 1 
   and array_key_exists("project_lits_filter_settings", $_SESSION))
{
	$query_title = "Project's masterplan (" . date("d.m.Y H:i:s");
	$print_query_filter = 0;
	$show_detail = 1;

	$show_treatment_state = true;

	$order = " order by country_name, project_number, order_shop_address_company, order_shop_address_place ";
	$order2 = " order by order_actual_order_state_code, country_name, project_number, order_shop_address_company, order_shop_address_place ";

		
	$sql_d = "select project_id, project_order, project_number, project_postype, " .
		   "concat(lpls.user_firstname, ' ', lpls.user_name) as local_project_leader, " .
		   "concat(pls.user_firstname, ' ', pls.user_name) as project_leader, project_cost_CER, project_cost_milestone_turnaround, " .
		   "project_projectkind, project_order, project_costtype_text, " .
		   "DATE_FORMAT(projects.project_construction_startdate, '%d.%m.%Y') as construction_startdate, " .
		   "DATE_FORMAT(projects.date_created, '%d.%m.%Y') as submission_date, " .
		   "DATE_FORMAT(project_planned_opening_date, '%d.%m.%Y') as preferred_opening_date, " .
		   "DATE_FORMAT(project_actual_opening_date, '%d.%m.%Y') as actual_opening_date, " .
		   "DATE_FORMAT(project_real_opening_date, '%d.%m.%Y') as realistic_opening_date, " .
		   "projects.project_construction_startdate as project_construction_startdate, " .
			   "projects.date_created as project_date_created, " .
			   "project_planned_opening_date, project_actual_opening_date, project_real_opening_date, " .
		   "project_cost_type, project_costtype_text, projectkind_code, postype_name, " .
		   "country_name,  project_cost_sqms, project_cost_totalsqms, order_shop_address_company, product_line_name, " .
		   "order_shop_address_place, order_shop_address_address, order_actual_order_state_code, " . 
		   "order_logistic_status, order_development_status, " . 
		   "order_shop_address_country, country_region, possubclass_name, productline_subclass_name, " . 
		   "transportation_types1.transportation_type_name as transportation_type_name_arranged, " . 
		   "transportation_types2.transportation_type_name as transportation_type_name_mode, " . 
		   "region_name, salesregion_name, " .
		   " address_company, project_state_text " .
		   "from projects " .
		   "inner join orders on order_id = project_order " .
		   "inner join project_costs on project_cost_order = project_order " .
		   "inner join project_costtypes on project_costtype_id = project_cost_type " .
		   "left join cer_basicdata on cer_basicdata_project = project_id " . 
		   "inner join projectkinds on projectkind_id = project_projectkind " .
		   "inner join postypes on postype_id = project_postype " . 
		   "left join product_lines on product_line_id = project_product_line " .
		   "left join productline_subclasses on productline_subclass_id = project_product_line_subclass " .
		   "left join possubclasses on possubclass_id = project_pos_subclass " .
		   "inner join addresses on address_id = order_franchisee_address_id " .
		   "inner join countries on country_id = order_shop_address_country " .
		   "inner join salesregions on salesregion_id = country_salesregion " .
		   "inner join regions on region_id = country_region " .
		   "left join users as pls on pls.user_id = project_retail_coordinator " . 
		   "left join users as lpls on lpls.user_id = project_local_retail_coordinator " .
		   "left join transportation_types as transportation_types1 on order_preferred_transportation_arranged = transportation_types1. transportation_type_id ".
		   "left join transportation_types as transportation_types2 on order_preferred_transportation_mode = transportation_types2. transportation_type_id ".
		   "left join project_states on project_state_id = project_state " .
		   "where " . $_SESSION["project_lits_filter_settings"] . 
		   $order;

		$res = mysql_query($sql_d) or dberror($sql_d);
		$projects = array();
		while ($row = mysql_fetch_assoc($res)) {
		  $projects[] = $row;
		}

	  	$sql_d2 = "select project_id, project_order, project_number, project_postype, " .
			 "concat(lpls.user_firstname, ' ', lpls.user_name) as local_project_leader, " .
			 "concat(pls.user_firstname, ' ', pls.user_name) as project_leader, project_cost_CER, project_cost_milestone_turnaround, " .
			 "project_projectkind, project_order, project_costtype_text, " .
			 "DATE_FORMAT(projects.project_construction_startdate, '%d.%m.%Y') as construction_startdate, " .
			 "DATE_FORMAT(projects.date_created, '%d.%m.%Y') as submission_date, " .
			 "DATE_FORMAT(project_planned_opening_date, '%d.%m.%Y') as preferred_opening_date, " .
			 "DATE_FORMAT(project_actual_opening_date, '%d.%m.%Y') as actual_opening_date, " .
			 "DATE_FORMAT(project_real_opening_date, '%d.%m.%Y') as realistic_opening_date, " .
			 "project_cost_type, project_costtype_text, projectkind_code, postype_name, " .
			 "country_name,  project_cost_sqms, project_cost_totalsqms, order_shop_address_company, product_line_name, " .
			 "order_shop_address_place, order_shop_address_address, order_logistic_status, order_development_status, " . 
			 "order_shop_address_country, country_region, possubclass_name, productline_subclass_name, " . 
			 "transportation_types1.transportation_type_name as transportation_type_name_arranged, " . 
			 "transportation_types2.transportation_type_name as transportation_type_name_mode, " . 
			 "region_name, salesregion_name, " .
			 " address_company " .
			 "from projects " .
			 "inner join orders on order_id = project_order " .
			 "left join project_costs on project_cost_order = project_order " .
			 "left join project_costtypes on project_costtype_id = project_cost_type " .
		     "left join cer_basicdata on cer_basicdata_project = project_id " .
			 "left join projectkinds on projectkind_id = project_projectkind " .
			 "left join postypes on postype_id = project_postype " . 
			 "left join product_lines on product_line_id = project_product_line " .
			 "left join productline_subclasses on productline_subclass_id = project_product_line_subclass " .
			 "left join possubclasses on possubclass_id = project_pos_subclass " .
			 "left join addresses on address_id = order_franchisee_address_id " .
			 "left join countries on country_id = order_shop_address_country " .
			 "left join salesregions on salesregion_id = country_salesregion " .
			 "left join regions on region_id = country_region " .
			 "left join users as pls on pls.user_id = project_retail_coordinator " .
			 "left join users as lpls on lpls.user_id = project_local_retail_coordinator " .
			 "left join transportation_types as transportation_types1 on order_preferred_transportation_arranged = transportation_types1. transportation_type_id ".
			"left join transportation_types as transportation_types2 on order_preferred_transportation_mode = transportation_types2. transportation_type_id ".
			  "where " . $_SESSION["project_lits_filter_settings"] . 
		   $order2;				
}
else // print query from MIS Module
{
	$show_treatment_state = false;
	
	// query_id may already be set in including file
	if (!isset($query_id)) {
		$query_id = param('query_id');
	}
	if(!$query_id) {
	  redirect("projects_queries.php");
	}
	
	//check if filter is present
	$query_title = "";
	$print_query_filter = 0;
	$sql = "select mis_query_name, mis_query_filter, mis_print_filter from mis_queries " .
		 "where mis_query_id = " . $query_id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
	  $query_title = $row["mis_query_name"] . ' (' . date("d.m.Y H:i:s"). ')';

	  if($row["mis_print_filter"] == 1)
	  {
		$print_query_filter = 1;
	  }


	  $filters = array();
	  $filters = unserialize($row["mis_query_filter"]);

	  foreach($filters as $key=>$value)
	  {
		
		$st =  $filters["ptst"]; // Project State


		$fosc = $filters["fst"]; //from order state
		$tosc = $filters["tst"]; // to order state

		if(array_key_exists("fst2", $filters))
		{
		  $fosc2 = $filters["fst2"];
		}
		else
		{
			$fosc2 = "";
		}

		if(array_key_exists("tst2", $filters))
		{
		  $tosc2 = $filters["tst2"];
		}
		else
		{
			$tosc2 = "";
		}
		

		$clt = $filters["clt"]; // Client Types
		$pl = $filters["pl"]; // product lines
		$pls = $filters["plsc"]; // product line subclasses
		$pk = $filters["pk"]; // product kinds
		$pt = $filters["pt"]; // project types
		$suc = $filters["ptsc"]; // POS Type Subclass
		$lpt = $filters["pct"]; // project cost type
		$gr = $filters["gr"]; // Geografical Regions
		$re = $filters["re"]; // Supplied Regions
		$cnt = $filters["co"]; // Countries
		$sc = $filters["rtc"]; // Retail Coordinators
		$lsc = $filters["lrtc"]; // Local Retail Coordinators

		$fdy = $filters["fdy"]; // Closing Year From
		$fdm = $filters["fdm"]; // Closing Month From
		$tdy = $filters["tdy"]; // Closing Year To
		$tdm = $filters["tdm"]; // Closing Month To

		$rto = $filters["rto"]; // Retail Operators
		if(array_key_exists("dcontr", $filters))
		{
		  $dcontr = $filters["dcontr"];
		}
		else
		{
			$dcontr = "";
		}

		$show_detail = $filters["detail"]; // Show second Sheet (Details for Suppliers and Forwarders
		//$show_detail = 1;
		
		$idvi = 0;  // Visuals
		if(array_key_exists("idvi", $filters))
		{
		  $idvi = $filters["idvi"];
		}

		if(array_key_exists("areas", $filters))
		{
		  $areas = $filters["areas"]; // Neighbourhood Locations
		
		}
		else
		{
		  $areas = "";
		}

		if(array_key_exists("owners", $filters))
		{
		  $owners = $filters["owners"]; // Owners
		
		}
		else
		{
		  $owners = "";
		}

		if(array_key_exists("dos", $filters))
		{
		  $dos = $filters["dos"]; // Design Objectives
		
		}
		else
		{
		  $dos = "";
		}

		if(array_key_exists("supp", $filters))
		{
		  $supp = $filters["supp"]; // Suppliers
		
		}
		else
		{
		  $supp = "";
		}

		

		

	  }
	}
	else
	{
	  redirect("projects_queries.php");
	}





	$filter = "";

	$owners = substr($owners,0, strlen($owners)-1); // remove last comma
	$owners = str_replace("-", ",", $owners);
	if($owners) // owners
	{
		$filter =  " where (order_franchisee_address_id IN (" . $owners . "))";
	  $_filter_strings["Owners"] = get_filter_string("owners", $owners);
	}


	$clt = substr($clt,0, strlen($clt)-1); // remove last comma
	$clt = str_replace("-", ",", $clt);
	if($clt and $filter) // client type
	{
		$filter .=  " and (address_client_type IN (" . $clt . "))";
	  $_filter_strings["Client Types"] = get_filter_string("clt", $clt);
	}
	elseif($clt)
	{
	  $filter =  " where (address_client_type IN (" . $clt . "))";
	  $_filter_strings["Client Types"] = get_filter_string("clt", $clt);
	}

	$pl = substr($pl,0, strlen($pl)-1); // remove last comma
	$pl = str_replace("-", ",", $pl);
	if($pl and $filter) // product line
	{
	  $filter .=  " and (project_product_line IN (" . $pl . "))";
	  $_filter_strings["Product Lines"] = get_filter_string("pl", $pl);
	}
	elseif($pl) // product line
	{
		$filter =  " where (project_product_line IN (" . $pl . "))";
	  $_filter_strings["Product Lines"] = get_filter_string("pl", $pl);
	}

	$pls = substr($pls,0, strlen($pls)-1); // remove last comma
	$pls = str_replace("-", ",", $pls);
	if($pls and $filter) // product line subclasses
	{
	  $filter .=  " and (project_product_line_subclass IN (" . $pls . "))";
	  $_filter_strings["Product Line Subclasses"] = get_filter_string("pls", $pls);
	}
	elseif($pls) // product line
	{
		$filter =  " where (project_product_line_subclass IN (" . $pls . "))";
	  $_filter_strings["Product Line Subclasses"] = get_filter_string("pls", $pls);
	}

	$pk = substr($pk,0, strlen($pk)-1); // remove last comma
	$pk = str_replace("-", ",", $pk);

	if($pk and $filter) // product line
	{
	  $filter .=  " and (project_projectkind IN (" . $pk . "))";
	  $_filter_strings["Project Types"] = get_filter_string("pk", $pk);
	}
	elseif($pk) // product kind
	{
		$filter =  " where (project_projectkind IN (" . $pk . "))";
	  $_filter_strings["Project Types"] = get_filter_string("pk", $pk);
	}


	$pt = substr($pt,0, strlen($pt)-1); // remove last comma
	$pt = str_replace("-", ",", $pt);
	if($pt and $filter) // project type
	{
		$filter .=  " and (project_postype IN (" . $pt . "))";
	  $_filter_strings["POS Types"] = get_filter_string("pt", $pt);
	}
	elseif($pt)
	{
		$filter =  " where (project_postype IN (" . $pt . "))";
	  $_filter_strings["POS Types"] = get_filter_string("pt", $pt);
	}

	$suc = substr($suc,0, strlen($suc)-1); // remove last comma
	$suc = str_replace("-", ",", $suc);
	if($suc and $filter) // Pos Type Sub classe
	{
		$filter .=  " and (project_pos_subclass IN (" . $suc . "))";
	  $_filter_strings["POS Type Subclasses"] = get_filter_string("suc", $suc);
	}
	elseif($suc)
	{
		$filter =  " where (project_pos_subclass IN (" . $suc . "))";
	  $_filter_strings["POS Type Subclasses"] = get_filter_string("suc", $suc);
	}

	$lpt = substr($lpt,0, strlen($lpt)-1); // remove last comma
	$lpt = str_replace("-", ",", $lpt);
	if($lpt and $filter) // project cost type
	{
		$filter .=  " and (project_cost_type IN (" . $lpt . "))";
	  $_filter_strings["Legal Types"] = get_filter_string("lpt", $lpt);
	}
	elseif($lpt)
	{
	   $filter =  " where (project_cost_type IN (" . $lpt . "))";
	   $_filter_strings["Legal Types"] = get_filter_string("lpt", $lpt);
	}


	$gr = substr($gr,0, strlen($gr)-1); // remove last comma
	$gr = str_replace("-", ",", $gr);
	$re = substr($re,0, strlen($re)-1); // remove last comma
	$re = str_replace("-", ",", $re);

	if($filter) {

	  if($gr and $re) 
	  {
		$filter .=  " and (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	  }
	  elseif($gr) // geografical regions
	  {
		$filter .=  " and (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	  }
	  elseif($re) //supplying
	  {
		$filter .=  " and (country_region IN (" . $re . "))";
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	  }
	}
	else
	{
	  if($gr and $re) 
	  {
		$filter .=  " where (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	  }
	  elseif($gr) // geografical regions
	  {
		$filter .=  " where (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	  }
	  elseif($re) //supplying
	  {
		$filter .=  " where (country_region IN (" . $re . "))";
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	  }
	}

	$cnt = substr($cnt,0, strlen($cnt)-1); // remove last comma
	$cnt = str_replace("-", ",", $cnt);
	if($cnt and $filter) // country
	{
		$filter .=  " and (order_shop_address_country IN (" . $cnt . "))";
	  $_filter_strings["Country"] = get_filter_string("cnt", $cnt);

	}
	elseif($cnt)
	{
		$filter =  " where (order_shop_address_country IN (" . $cnt . "))";
	  $_filter_strings["Country"] = get_filter_string("cnt", $cnt);
	}


	$sc = substr($sc,0, strlen($sc)-1); // remove last comma
	$sc = str_replace("-", ",", $sc);
	if($sc and $filter) // Retail Coordinator
	{
		$filter .=  " and (project_retail_coordinator IN (" . $sc . "))";
	  $_filter_strings["Project Leader"] = get_filter_string("sc", $sc);

	}
	elseif($sc)
	{
		$filter =  " where (project_retail_coordinator IN (" . $sc . "))";
	  $_filter_strings["Project Leader"] = get_filter_string("sc", $sc);
	}


	$lsc = substr($lsc,0, strlen($lsc)-1); // remove last comma
	$lsc = str_replace("-", ",", $lsc);

	if($lsc and $filter) // Local Retail Coordinator
	{
	  $filter .=  " and (project_local_retail_coordinator IN (" . $lsc . "))";
	  $_filter_strings["Local Project Leader"] = get_filter_string("lrtc", $lsc);

	}
	elseif($lsc)
	{
		$filter =  " where (project_local_retail_coordinator IN (" . $lsc . "))";
	  $_filter_strings["Local Project Leader"] = get_filter_string("lrtc", $lsc);

	}

	$rto = substr($rto,0, strlen($rto)-1); // remove last comma
	$rto = str_replace("-", ",", $rto);
	if($rto and $filter) // Retail Operator
	{
	  $filter .=  " and (order_retail_operator IN (" . $rto . "))";
	  $_filter_strings["Logistics Coordinator"] = get_filter_string("rto", $rto);

	}
	elseif($rto)
	{
	  $filter =  " where (order_retail_operator IN (" . $rto . "))";
	  $_filter_strings["Logistics Coordinator"] = get_filter_string("rto", $rto);
	}


	$dcontr = substr($dcontr,0, strlen($dcontr)-1); // remove last comma
	$dcontr = str_replace("-", ",", $dcontr);
	if($dcontr and $filter) // Design Contractor
	{
	  $filter .=  " and (project_design_contractor IN (" . $dcontr . "))";
	  $_filter_strings["Design Contractor"] = get_filter_string("dcontr", $dcontr);

	}
	elseif($dcontr)
	{
	  $filter =  " where (project_design_contractor IN (" . $dcontr . "))";
	  $_filter_strings["Design Contractor"] = get_filter_string("dcontr", $dcontr);
	}


	if($st == 4) // projects operating
	{
	  //only projects with an opening date
	  if($filter) // closed from year
	  {
		$filter.=  " and (project_actual_opening_date <> '0000-00-00' and project_actual_opening_date is not null) ";
	  }
	  else
	  {
		$filter =  "where (project_actual_opening_date <> '0000-00-00' and project_actual_opening_date is not null) ";
	  }

	  //only projects without an closing date

	  if($filter) // closed from year
	  {
		$filter.=  " and order_actual_order_state_code <> '900' and (project_shop_closingdate = '0000-00-00' or project_shop_closingdate is null) ";
	  }
	  else
	  {
		$filter =  "where order_actual_order_state_code <> '900' and project_shop_closingdate = '0000-00-00' or project_shop_closingdate is null) ";
	  }
	  $_filter_strings["Treatment State"] = "operating";
	}
	elseif($st == 1) // projects in progress
	{
	  
	  if($filter) // closed from year
	  {
		$filter.=  " and project_state = 1 ";
	  }
	  else
	  {
		$filter =  "where project_state = 1 ";
	  }

	  if($filter) // closed from year
	  {
		$filter.=  " and (order_archive_date = '0000-00-00' or order_archive_date is null) ";
	  }
	  else
	  {
		$filter =  "where (order_archive_date = '0000-00-00' or order_archive_date is null) ";
	  }
	  $_filter_strings["Treatment State"] = "in progress";
	  
	  /*
	  if($filter) // closed from year
	  {
		$filter.=  " and (project_actual_opening_date = '0000-00-00' or project_actual_opening_date is null) ";
	  }
	  else
	  {
		$filter =  "where (project_actual_opening_date = '0000-00-00' or project_actual_opening_date is null) ";
	  }

	  if($filter) // closed from year
	  {
		$filter.=  " and order_actual_order_state_code <> '900' and (project_shop_closingdate = '0000-00-00' or project_shop_closingdate is null) ";
	  }
	  else
	  {
		$filter =  "where order_actual_order_state_code <> '900' and project_shop_closingdate = '0000-00-00' or project_shop_closingdate is null) ";
	  }
	  */
	}
	elseif($st == 2) // projects on hold
	{
	  
	  if($filter) // closed from year
	  {
		$filter.=  " and project_state = 2 ";
	  }
	  else
	  {
		$filter =  "where project_state = 2 ";
	  }

	  if($filter) // closed from year
	  {
		$filter.=  " and order_actual_order_state_code <> '900' and (project_shop_closingdate = '0000-00-00' or project_shop_closingdate is null) ";
	  }
	  else
	  {
		$filter =  "where order_actual_order_state_code <> '900' and project_shop_closingdate = '0000-00-00' or project_shop_closingdate is null) ";
	  }
	  $_filter_strings["Treatment State"] = "on hold";
	}
	elseif($st == 6) // cancelled
	{
	  
	  if($filter)
	  {
		$filter.=  " and order_actual_order_state_code = '900'";
	  }
	  else
	  {
		$filter =  "where order_actual_order_state_code = '900'";
	  }
	  $_filter_strings["Treatment State"] = "cancelled";
	}


	//developmen status
	if($filter and $fosc) // from order state
	{
		$filter.=  " and order_development_status >= '" . $fosc . "' ";
	    $_filter_strings["From Develompment Status"] = $fosc;
	}
	elseif($fosc)
	{
	   $filter.=  " where order_development_status >= '" . $fosc . "' ";
	   $_filter_strings["From Develompment Status"] = $fosc;
	}

	if($filter and $tosc) // to order state
	{
		$filter.=  " and order_development_status <= '" . $tosc . "' ";
		$_filter_strings["To Develompment Status"] = $tosc;
	}
	elseif($tosc)
	{
	  $filter.=  " where order_development_status <= '" . $tosc . "' ";
	  $_filter_strings["To Develompment Status"] = $tosc;
	}




	//Logistic status
	if($filter and $fosc2) // from order state
	{
		$filter.=  " and order_logistic_status >= '" . $fosc2. "' ";
	    $_filter_strings["From Logistic Statrus"] = $fosc2;
	}
	elseif($fosc2)
	{
	   $filter.=  " where order_logistic_status >= '" . $fosc2. "' ";
	   $_filter_strings["From Logistic Statrus"] = $fosc2;
	}

	if($filter and $tosc2) // to order state
	{
		$filter.=  " and order_logistic_status <= '" . $tosc2. "' ";
		$_filter_strings["To Logistic Statrus"] = $tosc2;
	}
	elseif($tosc2)
	{
	  $filter.=  " where order_logistic_status <= '" . $tosc2. "' ";
	  $_filter_strings["To Logistic Statrus"] = $tosc2;
	}


	if($filter and $idvi) // Visuals
	{
		$filter.=  " and project_uses_icedunes_visuals = 1";
	  $_filter_strings["Only Projects with Visulas"] = "yes";
	}
	elseif($idvi)
	{
	  $filter =  " where project_uses_icedunes_visuals = 1 ";
	  $_filter_strings["Only Projects with Visulas"] = "yes";
	}



	if($gr and $re) 
	{
	  $order = " order by region_name, salesregion_name, country_name, project_number, order_shop_address_company, order_shop_address_place ";

	  $order2 = " order by order_actual_order_state_code, region_name, salesregion_name, country_name, project_number, order_shop_address_company, order_shop_address_place ";
	}
	elseif($gr) 
	{
	  $order = " order by salesregion_name, country_name, project_number, order_shop_address_company, order_shop_address_place ";
	  $order2 = " order by order_actual_order_state_code, salesregion_name, country_name, project_number, order_shop_address_company, order_shop_address_place ";
	}
	elseif($re) 
	{
	  $order = " order by region_name, country_name, project_number, order_shop_address_company, order_shop_address_place ";
	  $order2 = " order by  order_actual_order_state_code, oregion_name, country_name, project_number, order_shop_address_company, order_shop_address_place ";
	}
	else
	{
	  $order = " order by country_name, project_number, order_shop_address_company, order_shop_address_place ";
	  $order2 = " order by order_actual_order_state_code, country_name, project_number, order_shop_address_company, order_shop_address_place ";
	}


	//get projects of the indicated suppliers

	$filter_suppliers = '';
	$supp = substr($supp,0, strlen($supp)-1); // remove last comma
	$supp = str_replace("-", ",", $supp);
	if($supp and $filter)
	{
		$filter_suppliers =  " and (order_item_supplier_address IN (" . $supp . "))";
	  $_filter_strings["Supplier"] = get_filter_string("supp", $supp);
	}
	elseif($supp)
	{
		$filter_suppliers =  " where (order_item_supplier_address IN (" . $supp . "))";
	  $_filter_strings["Supplier"] = get_filter_string("supp", $supp);
	}


	$supplier_projects = array();
	if($filter_suppliers)
	{
	  $sql = "select DISTINCT project_id " . 
			 "from projects " .
		   "inner join orders on order_id = project_order " .
		  "inner join order_items on order_item_order = order_id " .
		   "left join project_costs on project_cost_order = project_order " .
		   "left join project_costtypes on project_costtype_id = project_cost_type " .
		   "left join projectkinds on projectkind_id = project_projectkind " .
		   "left join postypes on postype_id = project_postype " . 
		   "left join product_lines on product_line_id = project_product_line " .
		   "left join productline_subclasses on productline_subclass_id = project_product_line_subclass " .
		   "left join possubclasses on possubclass_id = project_pos_subclass " .
		   "left join addresses on address_id = order_client_address " .
		   "left join countries on country_id = order_shop_address_country " .
		   "left join salesregions on salesregion_id = country_salesregion " .
		   "left join regions on region_id = country_region " .
		   "left join users on user_id = project_retail_coordinator " . 
		   "left join transportation_types as transportation_types1 on order_preferred_transportation_arranged = transportation_types1. transportation_type_id ".
		   "left join transportation_types as transportation_types2 on order_preferred_transportation_mode = transportation_types2. transportation_type_id ".
		   $filter . 
		   $filter_suppliers;

	  $res = mysql_query($sql) or dberror($sql);
	  while ($row = mysql_fetch_assoc($res))
	  {
		$supplier_projects[] = $row["project_id"];
	  }
	}


	if($filter and count($supplier_projects) > 0)
	{
	  $filter .= ' and project_id in (' . implode(',', $supplier_projects) . ')';
	}
	elseif(count($supplier_projects) > 0)
	{
	  $filter = ' where project_id in (' . implode(',', $supplier_projects) . ')';
	}

	//filter posareas get all matching orders
	$areas = substr($areas,0, strlen($areas)-1); // remove last comma
	$areas = str_replace("-", ",", $areas);

	if($areas)
	{
	  
	  $order_ids = array();
	  $sql = "select project_order " . 
			 "from projects " .
		   "inner join orders on order_id = project_order " .
		   "left join project_costs on project_cost_order = project_order " .
		   "left join project_costtypes on project_costtype_id = project_cost_type " .
		   "left join projectkinds on projectkind_id = project_projectkind " .
		   "left join postypes on postype_id = project_postype " . 
		   "left join product_lines on product_line_id = project_product_line " .
		   "left join productline_subclasses on productline_subclass_id = project_product_line_subclass " .
		   "left join possubclasses on possubclass_id = project_pos_subclass " .
		   "left join addresses on address_id = order_client_address " .
		   "left join countries on country_id = order_shop_address_country " .
		   "left join salesregions on salesregion_id = country_salesregion " .
		   "left join regions on region_id = country_region " .
		   "left join users on user_id = project_retail_coordinator " . 
		   "left join transportation_types as transportation_types1 on order_preferred_transportation_arranged = transportation_types1. transportation_type_id ".
		   "left join transportation_types as transportation_types2 on order_preferred_transportation_mode = transportation_types2. transportation_type_id ".
		   $filter;
	  
	  $res = mysql_query($sql) or dberror($sql);
	  while ($row = mysql_fetch_assoc($res))
	  {
		$sql_a = "select DISTINCT posorder_order " . 
			   "from posorders " . 
			   "left join posareas on posorder_posaddress = posarea_posaddress " .
			   "where posorder_order = " . $row["project_order"] . 
			   " and posarea_area IN (" . $areas . ")";

		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
		  $order_ids[$row["project_order"]] = $row["project_order"];
		}

		$sql_a = "select DISTINCT posorder_order " . 
			   "from posorderspipeline " . 
			   "left join posareaspipeline on posorder_posaddress = posarea_posaddress " .
			   "where posorder_order = " . $row["project_order"] . 
			   " and posarea_area IN (" . $areas . ")";

		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
		  $order_ids[$row["project_order"]] = $row["project_order"];
		}
	  
	  }

	  if(count($order_ids) > 0)
	  {
	  
		
		if($filter) // Visuals
		{
		  $filter.=  " and project_order IN (" . implode ( ',' , $order_ids ) . ") ";
		}
		else
		{
		  $filter =  " where project_order IN (" . implode ( ',' , $order_ids ) . ") ";
		}
	  }
	  $_filter_strings["Neighbourhood Locations"] = get_filter_string("areas", $areas);
	}

	//filter design objectives get all matching orders
	$dos = substr($dos,0, strlen($dos)-1); // remove last comma
	$dos = str_replace("-", ",", $dos);

	if($dos)
	{
	  
	  $order_ids2 = array();
	  $sql = "select project_order " . 
			 "from project_items " .
		   "left join projects on project_id = project_item_project " .
		   "where project_item_item IN (" . $dos . ")";
	  
	  $res = mysql_query($sql) or dberror($sql);
	  while ($row = mysql_fetch_assoc($res))
	  {
		$order_ids2[$row["project_order"]] = $row["project_order"];
	  }

	  if(count($order_ids2) > 0)
	  {
		if($filter) // Design Objectives
		{
		  $filter.=  " and project_order IN (" . implode ( ',' , $order_ids2 ) . ") ";
		}
		else
		{
		  $filter =  " where project_order IN (" . implode ( ',' , $order_ids2 ) . ") ";
		}
	  }
	  $_filter_strings["Design Objectives"] = get_filter_string("dos", $dos);
	}


	//evaluate milestone filter
	$from_dates = array();
	$to_dates = array();
	$sql = "select mis_query_milestone_filter " . 
		   "from mis_queries ". 
		 " where mis_query_id = " . dbquote($query_id);

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
	  $tmp = unserialize($row["mis_query_milestone_filter"]);

	  //var_dump($tmp);

	  if(is_array($tmp) and array_key_exists("from_dates",$tmp))
	  {

		foreach($tmp["from_dates"] as $milestone_id=>$date)
		{
		  $from_dates[$milestone_id] = to_system_date($date);
		}
	  }

	  if(is_array($tmp ) and array_key_exists("to_dates",$tmp))
	  {
		foreach($tmp["to_dates"] as $milestone_id=>$date)
		{
		  $to_dates[$milestone_id] = to_system_date($date);
		}
	  }
	}




	//evaluate milestone filter
	$milestone_filter = "";
	$from_dates = array();
	$to_dates = array();
	$mfrom = "";
	$mto = "";

	$sql = "select mis_query_milestone_filter " . 
		   "from mis_queries ". 
		 " where mis_query_id = " . dbquote($query_id);

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
	  $tmp = unserialize($row["mis_query_milestone_filter"]);

	  //var_dump($tmp);

	  if(is_array($tmp) and array_key_exists("from_dates",$tmp))
	  {

		foreach($tmp["from_dates"] as $milestone_id=>$date)
		{
		  $from_dates[$milestone_id] = $date;
		}
	  }

	  if(is_array($tmp ) and array_key_exists("to_dates",$tmp))
	  {
		foreach($tmp["to_dates"] as $milestone_id=>$date)
		{
		  $to_dates[$milestone_id] = $date;
		}
	  }

	  if(is_array($tmp ) and array_key_exists("mfrom",$tmp))
	  {
		$mfrom = $tmp["mfrom"];
		$mto = $tmp["mto"];
	  }
	}

	if(count($from_dates) > 0 or count($to_dates) > 0)
	{
	  $tmp = array();
	  $milesone_dates = array();
	  foreach($from_dates as $milestone_id=>$milestone_date)
	  {
		if($milestone_date != NULL 
		  and $milestone_date != '000000-00-00'
			and $to_dates[$milestone_id] != NULL 
		  and $to_dates[$milestone_id] != '000000-00-00'
		  )
		{
		  $sql_m = "select distinct project_milestone_project " . 
			   " from project_milestones " .
				 " left join milestones on milestone_id = project_milestone_milestone " . 
			   " where project_milestone_milestone = " . $milestone_id . 
			   " and project_milestone_date >= " . dbquote($milestone_date) .
				   " and project_milestone_date <= " . dbquote($to_dates[$milestone_id]);

		  
		  if($mfrom)
		  {
			$sql_m .= " and milestone_code >= " . dbquote($mfrom);
			  
		  }
		  if($mto)
		  {
			$sql_m .=  " and milestone_code <= " . dbquote($mto);
		  }
			
			
		  $res = mysql_query($sql_m) or dberror($sql_m);
		  while ($row = mysql_fetch_assoc($res))
		  {
			$tmp[] = $row["project_milestone_project"];
		  }
		}

	  }
	  
	  
	  
	  if(count($tmp) == 0 and ($mfrom or $mto))
	  {
		
		$sql_m = "select distinct project_milestone_project " . 
			 " from project_milestones " .
			   " left join milestones on milestone_id = project_milestone_milestone " . 
			 " where project_milestone_date is not null " . 
			 " and project_milestone_date <> '0000-00-00' ";

		if($mfrom)
		{
		  $sql_m .= " and milestone_code >= " . dbquote($mfrom);
			
		}
		if($mto)
		{
		  $sql_m .=  " and milestone_code <= " . dbquote($mto);
		}

		


		$res = mysql_query($sql_m) or dberror($sql_m);
		while ($row = mysql_fetch_assoc($res))
		{
		  $tmp[] = $row["project_milestone_project"];
		}
	  }

	  if(count($tmp) > 0)
	  {
		$milestone_filter = " AND project_id IN (" . implode(',', $tmp) . ") ";
	  }
	}


	//prevent from including projects in the archive
	$filter .= ' AND (order_archive_date is null or order_archive_date = "0000-00-00") ';

	$sql_d = "select project_id, project_order, project_number, project_postype, " .
		   "concat(lpls.user_firstname, ' ', lpls.user_name) as local_project_leader, " .
		   "concat(pls.user_firstname, ' ', pls.user_name) as project_leader, project_cost_CER, project_cost_milestone_turnaround, " .
		   "project_projectkind, project_order, project_costtype_text, " .
		   "DATE_FORMAT(projects.project_construction_startdate, '%d.%m.%Y') as construction_startdate, " .
		   "DATE_FORMAT(projects.date_created, '%d.%m.%Y') as submission_date, " .
		   "DATE_FORMAT(project_planned_opening_date, '%d.%m.%Y') as preferred_opening_date, " .
		   "DATE_FORMAT(project_actual_opening_date, '%d.%m.%Y') as actual_opening_date, " .
		   "DATE_FORMAT(project_real_opening_date, '%d.%m.%Y') as realistic_opening_date, " .
		   "projects.project_construction_startdate as project_construction_startdate, " .
			   "projects.date_created as project_date_created, " .
			   "project_planned_opening_date, project_actual_opening_date, project_real_opening_date, " .
		   "project_cost_type, project_costtype_text, projectkind_code, postype_name, " .
		   "country_name,  project_cost_sqms, project_cost_totalsqms, order_shop_address_company, product_line_name, " .
		   "order_shop_address_place, order_shop_address_address, order_actual_order_state_code, " .
		   "order_logistic_status, order_development_status, " . 
		   "order_shop_address_country, country_region, possubclass_name, productline_subclass_name, " . 
		   "transportation_types1.transportation_type_name as transportation_type_name_arranged, " . 
		   "transportation_types2.transportation_type_name as transportation_type_name_mode, " . 
		   "region_name, salesregion_name, " .
		   " address_company, project_state_text " .
		   "from projects " .
		   "inner join orders on order_id = project_order " .
		   "inner join project_costs on project_cost_order = project_order " .
		   "inner join project_costtypes on project_costtype_id = project_cost_type " .
		   "inner join projectkinds on projectkind_id = project_projectkind " .
		   "inner join postypes on postype_id = project_postype " . 
		   "left join product_lines on product_line_id = project_product_line " .
		   "left join productline_subclasses on productline_subclass_id = project_product_line_subclass " .
		   "left join possubclasses on possubclass_id = project_pos_subclass " .
		   "inner join addresses on address_id = order_franchisee_address_id " .
		   "inner join countries on country_id = order_shop_address_country " .
		   "inner join salesregions on salesregion_id = country_salesregion " .
		   "inner join regions on region_id = country_region " .
		   "left join users as pls on pls.user_id = project_retail_coordinator " .
		   "left join users as lpls on lpls.user_id = project_local_retail_coordinator " .
		   "left join transportation_types as transportation_types1 on order_preferred_transportation_arranged = transportation_types1. transportation_type_id ".
		   "left join transportation_types as transportation_types2 on order_preferred_transportation_mode = transportation_types2. transportation_type_id ".
		   "left join project_states on project_state_id = project_state " .
		   $filter .
		   $milestone_filter .
		   $order;

	 $res = mysql_query($sql_d) or dberror($sql_d);
	 $projects = array();
	 while ($row = mysql_fetch_assoc($res)) {
	  $projects[] = $row;
	 }



	 $sql_d2 = "select project_id, project_order, project_number, project_postype, " .
		     "concat(lpls.user_firstname, ' ', lpls.user_name) as local_project_leader, " .
			 "concat(pls.user_firstname, ' ', pls.user_name) as project_leader, project_cost_CER, project_cost_milestone_turnaround, " .
			 "project_projectkind, project_order, project_costtype_text, " .
			 "DATE_FORMAT(projects.project_construction_startdate, '%d.%m.%Y') as construction_startdate, " .
			 "DATE_FORMAT(projects.date_created, '%d.%m.%Y') as submission_date, " .
			 "DATE_FORMAT(project_planned_opening_date, '%d.%m.%Y') as preferred_opening_date, " .
			 "DATE_FORMAT(project_actual_opening_date, '%d.%m.%Y') as actual_opening_date, " .
			 "DATE_FORMAT(project_real_opening_date, '%d.%m.%Y') as realistic_opening_date, " .
			 "project_cost_type, project_costtype_text, projectkind_code, postype_name, " .
			 "country_name,  project_cost_sqms, project_cost_totalsqms, order_shop_address_company, product_line_name, " .
			 "order_shop_address_place, order_shop_address_address, order_logistic_status, order_development_status, " . 
			 "order_shop_address_country, country_region, possubclass_name, productline_subclass_name, " . 
			 "transportation_types1.transportation_type_name as transportation_type_name_arranged, " . 
			 "transportation_types2.transportation_type_name as transportation_type_name_mode, " . 
			 "region_name, salesregion_name, " .
			 " address_company " .
			 "from projects " .
			 "inner join orders on order_id = project_order " .
			 "left join project_costs on project_cost_order = project_order " .
			 "left join project_costtypes on project_costtype_id = project_cost_type " .
			 "left join projectkinds on projectkind_id = project_projectkind " .
			 "left join postypes on postype_id = project_postype " . 
			 "left join product_lines on product_line_id = project_product_line " .
			 "left join productline_subclasses on productline_subclass_id = project_product_line_subclass " .
			 "left join possubclasses on possubclass_id = project_pos_subclass " .
			 "left join addresses on address_id = order_franchisee_address_id " .
			 "left join countries on country_id = order_shop_address_country " .
			 "left join salesregions on salesregion_id = country_salesregion " .
			 "left join regions on region_id = country_region " .
			 "left join users as pls on pls.user_id = project_retail_coordinator " .
		     "left join users as lpls on lpls.user_id = project_local_retail_coordinator " .
			 "left join transportation_types as transportation_types1 on order_preferred_transportation_arranged = transportation_types1. transportation_type_id ".
			"left join transportation_types as transportation_types2 on order_preferred_transportation_mode = transportation_types2. transportation_type_id ".
			 $filter . 
			   " and project_cost_cms_completed != 1 " .
			   $milestone_filter .
			 $order2;
}
