<?php

/********************************************************************

    orders_query_check_access.php

    Defines the possible page actions depending on access rights

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2016-08-19
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2016-08-19
    Version:        1.0.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.

*********************************************************************/

$sql = "select * from orderqueries where orderquery_id =" . dbquote(param("query_id"));
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	if($row["orderquery_owner"] != user_id())
	{
		
		$sql = "select orderquery_permission_id " . 
			   " from orderquery_permissions " . 
			   " where orderquery_permission_query =" . dbquote(param("query_id")) . 
			   " and orderquery_permission_user = " . dbquote(user_id());

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			
		}
		else
		{
			redirect("noaccess.php");
		}
	}
}
?>