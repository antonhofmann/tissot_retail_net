<?php

$showquery_link = "javascript:popup('project_queries_query_xls.php?query_id=" .  param("query_id") . "',800,600);";

$page->add_tab("q_name", "Name", "project_queries_query.php?query_id=" . param("query_id"), $target = "_self", $flags = 0);
$page->add_tab("q_fields", "Fields", "project_queries_query_fields.php?query_id=" . param("query_id"), $target = "_self", $flags = 0);
$page->add_tab("q_fieldorder", "Field Order", "project_queries_query_field_order.php?query_id=" . param("query_id"), $target = "_self", $flags = 0);
$page->add_tab("q_sortorder", "Sort Order", "project_queries_query_sortorder.php?query_id=" . param("query_id"), $target = "_self", $flags = 0);
$page->add_tab("q_filter1", "Filter", "project_queries_query_filter.php?query_id=" . param("query_id"), $target = "_self", $flags = 0);
$page->add_tab("q_filter2", "Milestone Filter", "project_queries_query_milestone_filter.php?query_id=" . param("query_id"), $target = "_self", $flags = 0);
$page->add_tab("q_grouping", "Grouping", "project_queries_grouping.php?query_id=" . param("query_id"), $target = "_self", $flags = 0);
$page->add_tab("q_function", "Functions", "project_queries_functions.php?query_id=" . param("query_id"), $target = "_self", $flags = 0);

$page->add_tab("q_coloring", "Coloring", "project_queries_coloring.php?query_id=" . param("query_id"), $target = "_self", $flags = 0);

$page->tabs();


?>