<?php
/********************************************************************

    mis_page_actions.php

    Defines the possible page actions depending on access rights

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2003-06-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-06-25
    Version:        1.0.0

    Copyright (c) 2003, Swatch AG, All Rights Reserved.

*********************************************************************/
$query_id = param("query_id");
if(has_access("can_query_posindex"))
{
	$page->register_action('pos_queries', 'POS Queries', "/pos/query_generator.php", "_blank");
	$page->register_action('poslocation_queries', 'Status POS Locations', "projects_queries.php?qt=30");
}

if(has_access("can_perform_all_queries") or has_access("can_perform_projects_query"))
{
	$page->register_action('project_queries', 'Project Queries', "project_queries.php");

	//$page->register_action('replacement_orders', 'Replacement Orders', "order_queries.php");
	
	//$page->register_action('dummy', '', "");
}


if(has_access("can_perform_all_queries") or has_access("can_perform_masterplan_query"))
{
	$page->register_action('nothing0', '', "");
	
	$page->register_action('masterplan', 'Projects Masterplan', "projects_queries.php?qt=31");
	//$page->register_action('masterplan', '---OLD Masterplan', "projects_queries.php?qt=6");
	//$page->register_action('masterplano', 'Orders Masterplan', "projects_queries.php?qt=35");

}

if(has_access("can_perform_all_queries") or has_access("can_perform_cmsoverview_query"))
{
	//$page->register_action('nothing1', '', "");
	//$page->register_action('cms_overview', 'CMS Overview', "projects_queries.php?qt=7");
}

if(has_access("can_perform_all_queries") or has_access("can_perform_cmsoverview_query"))
{
	//$page->register_action('cms_overview', 'CMS Completed Projects', "projects_queries.php?qt=16");
}

if(has_access("can_perform_all_queries") or has_access("can_perform_statusreport_query"))
{
	//$page->register_action('cms_progress', 'CMS Statusreport', "projects_queries.php?qt=12");
}

if(has_access("can_perform_all_queries") or has_access("can_perform_klcost_query"))
{
	//$page->register_action('cms_projects', 'KL Approval versus Real Costs', "projects_queries.php?qt=8");
}

if(has_access("can_perform_all_queries") or has_access("can_perform_project_milestones_query"))
{
	$page->register_action('project_milestones', 'Project Milestones', "projects_queries.php?qt=11");
}



if(has_access("can_perform_all_queries") or has_access("can_perform_transportation_cost_query"))
{
	//$page->register_action('logistics_n', '', "");
	//$page->register_action('transportation_cost', 'Transportation Cost', "projects_queries.php?qt=13");
	//$page->register_action('logistics_s', 'Purchase Volume by Supplier', "projects_queries.php?qt=40");
	//$page->register_action('logistics_f', 'Shipments by Forwarder', "projects_queries.php?qt=41");
}

if(has_access("can_perform_all_queries") or has_access("can_perform_project_sheets_query"))
{	
	//$page->register_action('nothing2', '', "");
	//$page->register_action('list_of_project_sheets', 'List of Project Sheets', "projects_queries.php?qt=9");
}

if(has_access("can_perform_all_queries") or has_access("can_perform_production_planning_query"))
{
	//$page->register_action('nothing3', '', "");
	//$page->register_action('production_order_planning', 'Production Order Planning Projects', "projects_queries.php?qt=10");
	//$page->register_action('production_order_planning2', 'Production Order Planning Catalogue Orders', "projects_queries.php?qt=10co");
}

if(has_access("can_perform_all_queries") or has_access("can_perform_delivered_query"))
{
	//$page->register_action('nothing4', '', "");
	//$page->register_action('delivered_by_item', 'Delivered Items', "projects_queries.php?qt=15");
	
	//$page->register_action('delivered_by_item', 'Delivery by Item', "delivered_by_item.php");
	//$page->register_action('delivered_by_country', 'Delivery by Country', "delivered_by_country.php");
}

if(has_access("can_perform_all_queries") or has_access("can_perform_equipment_query"))
{
	//$page->register_action('pos_equipment', 'Equipment of POS Locations', "projects_queries.php?qt=14");
	//$page->register_action('nothing6', '', "");
}

if(has_access("can_perform_all_queries") or has_access("can_perform_items_in_progress_query"))
{
	//$page->register_action('items_in_progress', 'Items in Progress', "items_in_progress.php");
}


if(has_access("can_perform_all_queries"))
{
	//$page->register_action('order_state', 'Order States', "projects_queries.php?qt=22");	
}

if(has_access("can_view_statistics"))
{
	$page->register_action('nothing4', '', "");

	$page->register_action('traffic_by_modules', 'Traffic by Modules', "traffic_by_modules.php");
	$page->register_action('traffic_by_company', 'Traffic by Company', "traffic_by_company.php");
	$page->register_action('traffic_by_user', 'Traffic by User', "traffic_by_user.php");
	$page->register_action('traffic_by_scores', 'Traffic by Top Scorers', "traffic_by_top_scorer.php");

	$page->register_action('latest_logins', 'Latest Logins', "latest_logins.php");
}

$page->register_action('nothing5', '', "");
$page->register_action('home', 'Home', "welcome.php");

?>