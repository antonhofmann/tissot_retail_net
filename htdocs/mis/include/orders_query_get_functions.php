<?php
/********************************************************************

    orders_query_get_functions.php

    Various utility functions to get information from tables.

    
    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2016-08-19
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2016-08-19
    Version:        1.0.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.
*********************************************************************/


/********************************************************************
    get group fields for summing up and function fields
*********************************************************************/
$query_group_fields = array();
$query_function_fields = array();

$db_info_client = query_clientaddress_fields();
$db_info_orders = query_orders_fields();

$db_info["group_fields"] = array_merge($db_info_client["group_fields"], $db_info_orders["group_fields"]);

$db_info["function_fields"] = array_merge($db_info_client["function_fields"], $db_info_orders["function_fields"]);


foreach($db_info["group_fields"] as $key=>$field)
{
	$query_group_fields[] = $field;
}

foreach($db_info["function_fields"] as $key=>$field)
{
	$query_function_fields[] = $field;
}

/********************************************************************
    get query name
*********************************************************************/
function get_query_name($id)
{
    $orderquery = array();
	
	$sql = "select * from orderqueries " .
		   "where orderquery_id = " . $id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$orderquery["name"] = $row["orderquery_name"];
	}
    return $orderquery;
}


/********************************************************************
    decode query filter
*********************************************************************/
function decode_field_array($field_order)
{
	$fields = array();
	
	if($field_order)
	{
		$code = str_replace("selected_field_order[]=", "", $field_order);
		$fields = explode  ( "&"  , $code);
	}
	return $fields;
}

/********************************************************************
    encode query filter
*********************************************************************/
function encode_field_array($field_array)
{
	$string = "";
	
	foreach($field_array as $key=>$fieldname)
	{
		$string .= "selected_field_order[]=";
		$string .= $fieldname;
		$string .= "&";

	}
	$string = substr($string, 0, strlen($string)-1);
	
	return $string;
}


/********************************************************************
    Check if query has fields
*********************************************************************/
function check_if_query_has_fields($query_id)
{
	$has_fields = false;

	$sql = "select orderquery_id, orderquery_name, orderquery_fields, orderquery_filter " . 
		   "from orderqueries " .
		   " where orderquery_id = " . $query_id;
	

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$fields = unserialize($row["orderquery_fields"]);
		$filter = unserialize($row["orderquery_filter"]);

		
		if(count($fields) > 0)
		{
			foreach($filter as $key=>$value)
			{
				if($value)
				{
					$has_fields = true;
				}
			}
			
		}
	}

	return $has_fields;
}

/********************************************************************
    get query filter
*********************************************************************/
function get_query_filter($query_id)
{
	$filter = array();
	
	$sql = "select orderquery_filter from orderqueries " .
	       "where orderquery_id = " . $query_id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{

		$filter = unserialize($row["orderquery_filter"]);

	}
	return $filter;
}

/********************************************************************
    get predefined filter for a user in queries
*********************************************************************/
function user_predefined_filter($user_id)
{
	$predefined_filter["re"] = "";
	$predefined_filter["gr"] = "";
	$predefined_filter["co"] = "";
	$predefined_filter["user_address_id_filter"] = "";
	$countries = array();
	$salesregions = array();
	$regions = array();
	$user_address_ids = array();

	$sql = "select * from users " .
		   "left join addresses on address_id = user_address " .
		   "left join countries on country_id = address_country " . 
		   "left join salesregions on salesregion_id = country_salesregion " .
		   "left join regions on region_id = country_region " .
		   "where user_id = " . $user_id;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$countries[$row["country_id"]] = $row["country_id"];
		$salesregions[$row["salesregion_id"]] = $row["salesregion_id"];
		$regions[$row["region_id"]] = $row["region_id"];
		$user_address_ids[$row["address_id"]] = $row["address_id"];
	
	}

	$predefined_filter["re"] = implode("-",$salesregions) . "-";
	$predefined_filter["gr"] = implode("-",$regions) . "-";
	$predefined_filter["co"] = implode("-",$countries) . "-";
	$predefined_filter["user_address_id_filter"] = "(" . implode(",",$user_address_ids) . ")";

	return $predefined_filter;
}




/********************************************************************
    get DB Info for Client Address Fields
*********************************************************************/
function query_clientaddress_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_sort_order = array();
	$content_by_function = array();
	$content_by_function_params = array();
	$group_fields = array();
	$function_fields = array();

	$fields["clients.address_client_type"] = "Client Type";
	$attributes["clients.address_client_type"] = "clienttypes.client_type_code";
	$datatypes["clients.address_client_type"] = "text";
	
	$fields["clients.address_company"] = "Client Company";
	$attributes["clients.address_company"] = "clients.address_company";
	$datatypes["clients.address_company"] = "text";
	$group_fields[] = "clients.address_company";
	
	$fields["clients.address_company2"] = "Client Company 2";
	$attributes["clients.address_company2"] = "clients.address_company2";
	$datatypes["clients.address_company2"] = "text";
	
	$fields["clients.address_address"] = "Client Street";
	$attributes["clients.address_address"] = "clients.address_address";
	$datatypes["clients.address_address"] = "text";

	$fields["clients.address_address2"] = "Client Additional Address";
	$attributes["clients.address_address2"] = "clients.address_address2";
	$datatypes["clients.address_address2"] = "text";
	
	$fields["clients.address_zip"] = "Client Zip";
	$attributes["clients.address_zip"] = "clients.address_zip";
	$datatypes["clients.address_zip"] = "text";
	
	$fields["clients.address_place"] = "Client City";
	$attributes["clients.address_place"] = "clients.address_place";
	$datatypes["clients.address_place"] = "text";
	
	$fields["clients.address_country"] = "Client Country";
	$attributes["clients.address_country"] = "clientcountries.country_name";
	$datatypes["clients.address_country"] = "text";
	
	$fields["clients.address_phone"] = "Client Phone";
	$attributes["clients.address_phone"] = "clients.address_phone";
	$datatypes["clients.address_phone"] = "text";
	
	$fields["clients.address_mobile_phone"] = "Client Mobile";
	$attributes["clients.address_mobile_phone"] = "clients.address_mobile_phone";
	$datatypes["clients.address_mobile_phone"] = "text";
	
	$fields["clients.address_email"] = "Client Email";
	$attributes["clients.address_email"] = "clients.address_email";
	$datatypes["clients.address_email"] = "text";
	
	$fields["clients.address_website"] = "Client Website";
	$attributes["clients.address_website"] = "clients.address_website";
	$datatypes["clients.address_website"] = "text";
	
	$fields["clients.address_contact_name"] = "Client Contact Name";
	$attributes["clients.address_contact_name"] = "clients.address_contact_name";
	$datatypes["clients.address_contact_name"] = "text";
	
	$fields["clients.address_contact_email"] = "Client Contact Email";
	$attributes["clients.address_contact_email"] = "clients.address_contact_email";
	$datatypes["clients.address_contact_email"] = "text";
	
	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["calculated_content_sort_order"] = $calculated_content_sort_order;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;
	$db_info["group_fields"] = $group_fields;
	$db_info["function_fields"] = $function_fields;

	return $db_info;
}



/********************************************************************
    get DB Info for order fields
*********************************************************************/
function query_orders_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_sort_order = array();
	$content_by_function = array();
	$content_by_function_params = array();
	$group_fields = array();
	$function_fields = array();

	
	$fields["orders.order_number"] = "Order Number";
	$attributes["orders.order_number"] = "orders.order_number";
	$datatypes["orders.order_number"] = "text";

	


	$fields["repleaced_items.order_item_text"] = "Related to Item";
	$attributes["repleaced_items.order_item_text"] = "repleaced_items.order_item_text";
	$datatypes["repleaced_items.order_item_text"] = "text";


	$fields["order_items.order_item_text"] = "Replacement Item";
	$attributes["order_items.order_item_text"] = "order_items.order_item_text";
	$datatypes["order_items.order_item_text"] = "text";


	$fields["suppliers.address_company"] = "Supplier";
	$attributes["suppliers.address_company"] = "suppliers.address_company";
	$datatypes["suppliers.address_company"] = "text";
	$group_fields[] = "suppliers.address_company";

	
	$fields["forwarders.address_company"] = "Forwarder";
	$attributes["forwarders.address_company"] = "forwarders.address_company";
	$datatypes["forwarders.address_company"] = "text";
	$group_fields[] = "forwarders.address_company";


	$fields["order_item_replacement_reason_text"] = "Replacement Reason";
	$attributes["order_item_replacement_reason_text"] = "order_item_replacement_reason_text";
	$datatypes["order_item_replacement_reason_text"] = "text";


	$fields["order_item_warranty_type_text"] = "Warranty";
	$attributes["order_item_warranty_type_text"] = "order_item_warranty_type_text";
	$datatypes["order_item_warranty_type_text"] = "text";


	$fields["replacements_payer_names.replacements_payer_name_name"] = "Item Paid by";
	$attributes["replacements_payer_names.replacements_payer_name_name"] = "replacements_payer_names.replacements_payer_name_name";
	$datatypes["replacements_payer_names.replacements_payer_name_name"] = "text";

	$fields["replacements_payer_names2.replacements_payer_name_name"] = "Freight Paid by";
	$attributes["replacements_payer_names2.replacements_payer_name_name"] = "replacements_payer_names2.replacements_payer_name_name";
	$datatypes["replacements_payer_names2.replacements_payer_name_name"] = "text";

	
	$fields["order_item_replacements.order_item_replacement_remarks"] = "Remarks";
	$attributes["order_item_replacements.order_item_replacement_remarks"] = "order_item_replacements.order_item_replacement_remarks";
	$datatypes["order_item_replacements.order_item_replacement_remarks"] = "text";


	$fields["order_items.item_costs_chf"] = "Item Costs in CHF";
	$attributes["order_items.item_costs_chf"] = "content_by_function";
	$datatypes["order_items.item_costs_chf"] = "decimal2";
	$content_by_function["order_items.item_costs_chf"] = "get_item_costs_chf(params)";
	$content_by_function_params["order_items.item_costs_chf"] = array("order_item_id");
	$function_fields[] = "order_items.item_costs_chf";

	
	$fields["countries.country_name"] = "Project's Country";
	$attributes["countries.country_name"] = "countries.country_name";
	$datatypes["countries.country_name"] = "text";
	$group_fields[] = "countries.country_name";
	
	$fields["projects.order_number"] = "Project Number";
	$attributes["projects.order_number"] = "projects.project_number";
	$datatypes["projects.order_number"] = "text";

	$fields["projects.project_cost_type"] = "Project Legal Type";
	$attributes["projects.project_cost_type"] = "project_costtypes.project_costtype_text";
	$datatypes["projects.project_cost_type"] = "text";
	$group_fields[] = "projects.project_cost_typee";

	$fields["projects.project_projectkind"] = "Project Type";
	$attributes["projects.project_projectkind"] = "projectkinds.projectkind_name";
	$datatypes["projects.project_projectkind"] = "text";
	$group_fields[] = "projects.project_projectkind";

	$fields["projects.project_postype"] = "POS Type";
	$attributes["projects.project_postype"] = "postypes.postype_name";
	$datatypes["projects.project_postype"] = "text";
	$group_fields[] = "projects.project_postype";


	$fields["projects.project_product_line"] = "Product Line";
	$attributes["projects.project_product_line"] = "product_lines.product_line_name";
	$datatypes["projects.project_product_line"] = "text";
	$group_fields[] = "projects.project_product_line";

	$fields["projects.project_product_line_subclass"] = "Product Line Subclass";
	$attributes["projects.project_product_line_subclass"] = "productline_subclasses.productline_subclass_name";
	$datatypes["projects.project_product_line_subclass"] = "text";



	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["calculated_content_sort_order"] = $calculated_content_sort_order;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;
	$db_info["group_fields"] = $group_fields;
	$db_info["function_fields"] = $function_fields;


	return $db_info;
}


/***********************************************************************
    create an  empty column
************************************************************************/
function create_comment_column($order_id = 0)
{
	return "";
}

/***********************************************************************
    get costs of item
************************************************************************/
function get_item_costs_chf($order_item_id = 0)
{
	
	$sql = " select order_item_quantity*order_item_system_price as amount " . 
		   " from order_items where order_item_id = " . dbquote($order_item_id);

	
	
	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		return $row["amount"];
	}
	
	return "";
}
?>