<?php
/********************************************************************

    projects_query_get_functions.php

    Various utility functions to get information from tables.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2014-09-14
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2014-09-14
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.
*********************************************************************/


/********************************************************************
    get group fields for summing up and function fields
*********************************************************************/
$query_group_fields = array();
$query_function_fields = array();

$db_info_pos = query_posaddress_fields();
$db_info_client = query_clientaddress_fields();
$db_info_franchisee = query_franchiseeaddress_fields();
$db_info_distributionchannels = query_distribution_channel_fields();
$db_info_projects = query_projects_fields();
$db_info_cer = query_cer_fields();
$db_info_logistics = query_logistics_fields();
$db_info_rental = query_rental_fields();
$db_info_project_sheets = query_projects_sheet_fields();
$db_info_staff = query_staff_fields();
$db_info_budget = query_budget_fields();
$db_info_investments = query_investment_fields();
$db_info_approved_investments = query_approved_investments_fields();
$db_info_real_costs = query_real_costs_fields();
$db_info_cost_comparison = query_cost_comparison_fields();
$db_info_orderstates = query_orderstate_fields();
$db_info_cms = query_cms_fields();
$db_info_milestones = query_milestone_fields();
$db_info_items = query_item_fields();


$db_info["group_fields"] = array_merge($db_info_pos["group_fields"], $db_info_client["group_fields"], $db_info_franchisee["group_fields"], $db_info_distributionchannels["group_fields"], $db_info_projects["group_fields"], $db_info_cer["group_fields"], $db_info_logistics["group_fields"], $db_info_rental["group_fields"], $db_info_project_sheets["group_fields"], $db_info_cms["group_fields"], $db_info_staff["group_fields"], $db_info_budget["group_fields"], $db_info_investments["group_fields"], $db_info_approved_investments["group_fields"], $db_info_real_costs["group_fields"], $db_info_cost_comparison["group_fields"], $db_info_milestones["group_fields"], $db_info_orderstates["group_fields"], $db_info_items["group_fields"]);

$db_info["function_fields"] = array_merge($db_info_pos["function_fields"], $db_info_client["function_fields"], $db_info_franchisee["function_fields"], $db_info_distributionchannels["function_fields"], $db_info_projects["function_fields"], $db_info_cer["function_fields"], $db_info_logistics["function_fields"], $db_info_rental["function_fields"], $db_info_project_sheets["function_fields"], $db_info_cms["function_fields"], $db_info_staff["function_fields"], $db_info_budget["function_fields"], $db_info_investments["function_fields"], $db_info_approved_investments["function_fields"], $db_info_real_costs["function_fields"], $db_info_cost_comparison["function_fields"], $db_info_milestones["function_fields"], $db_info_orderstates["function_fields"], $db_info_items["function_fields"]);


foreach($db_info["group_fields"] as $key=>$field)
{
	$query_group_fields[] = $field;
}

foreach($db_info["function_fields"] as $key=>$field)
{
	$query_function_fields[] = $field;
}

/********************************************************************
    get query name
*********************************************************************/
function get_query_name($id)
{
    $projectquery = array();
	
	$sql = "select * from projectqueries " .
		   "where projectquery_id = " . $id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$projectquery["name"] = $row["projectquery_name"];
	}
    return $projectquery;
}


/********************************************************************
    decode query filter
*********************************************************************/
function decode_field_array($field_order)
{
	$fields = array();
	
	if($field_order)
	{
		$code = str_replace("selected_field_order[]=", "", $field_order);
		$fields = explode  ( "&"  , $code);
	}
	return $fields;
}

/********************************************************************
    encode query filter
*********************************************************************/
function encode_field_array($field_array)
{
	$string = "";
	
	foreach($field_array as $key=>$fieldname)
	{
		$string .= "selected_field_order[]=";
		$string .= $fieldname;
		$string .= "&";

	}
	$string = substr($string, 0, strlen($string)-1);
	
	return $string;
}


/********************************************************************
    Check if query has fields
*********************************************************************/
function check_if_query_has_fields($query_id)
{
	$has_fields = false;

	$sql = "select projectquery_id, projectquery_name, projectquery_fields, projectquery_filter " . 
		   "from projectqueries " .
		   " where projectquery_id = " . $query_id;
	

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$fields = unserialize($row["projectquery_fields"]);
		$filter = unserialize($row["projectquery_filter"]);

		
		if(count($fields) > 0)
		{
			foreach($filter as $key=>$value)
			{
				if($value)
				{
					$has_fields = true;
				}
			}
			
		}
	}

	return $has_fields;
}

/********************************************************************
    get query filter
*********************************************************************/
function get_query_filter($query_id)
{
	$filter = array();
	
	$sql = "select projectquery_filter from projectqueries " .
	       "where projectquery_id = " . $query_id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{

		$filter = unserialize($row["projectquery_filter"]);

	}
	return $filter;
}

/********************************************************************
    get query milestone filters
*********************************************************************/
function get_query_milestone_filter($query_id)
{
	$filter = array();
	
	$sql = "select projectquery_milestone_filter from projectqueries " .
	       "where projectquery_id = " . $query_id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{

		$filter = unserialize($row["projectquery_milestone_filter"]);

	}

	if($filter == false)
	{
		$filter = array();
	}

	return $filter;
}


/********************************************************************
    get predefined filter for a user in queries
*********************************************************************/
function user_predefined_filter($user_id)
{
	$predefined_filter["re"] = "";
	$predefined_filter["gr"] = "";
	$predefined_filter["co"] = "";
	$predefined_filter["user_address_id_filter"] = "";
	$countries = array();
	$salesregions = array();
	$regions = array();
	$user_address_ids = array();

	$sql = "select * from users " .
		   "left join addresses on address_id = user_address " .
		   "left join countries on country_id = address_country " . 
		   "left join salesregions on salesregion_id = country_salesregion " .
		   "left join regions on region_id = country_region " .
		   "where user_id = " . $user_id;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$countries[$row["country_id"]] = $row["country_id"];
		$salesregions[$row["salesregion_id"]] = $row["salesregion_id"];
		$regions[$row["region_id"]] = $row["region_id"];
		$user_address_ids[$row["address_id"]] = $row["address_id"];
	
	}

	$predefined_filter["re"] = implode("-",$salesregions) . "-";
	$predefined_filter["gr"] = implode("-",$regions) . "-";
	$predefined_filter["co"] = implode("-",$countries) . "-";
	$predefined_filter["user_address_id_filter"] = "(" . implode(",",$user_address_ids) . ")";

	return $predefined_filter;
}



/********************************************************************
    get DB Info for POS Address Fields
*********************************************************************/
function query_posaddress_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_sort_order = array();
	$content_by_function = array();
	$content_by_function_params = array();
	$group_fields = array();
	$function_fields = array();

	
	$fields["posaddresses.posaddress_salesregion"] = "Geographical Region";
	$attributes["posaddresses.posaddress_salesregion"] = "salesregions.salesregion_name";
	$datatypes["posaddresses.posaddress_salesregion"] = "text";
	$group_fields[] = "posaddresses.posaddress_salesregion";
	
	$fields["posaddresses.posaddress_region"] = "Supplied Region";
	$attributes["posaddresses.posaddress_region"] = "regions.region_name";
	$datatypes["posaddresses.posaddress_region"] = "text";
	$group_fields[] = "posaddresses.posaddress_region";

	$fields["posaddresses.posaddress_country"] = "Country";
	$attributes["posaddresses.posaddress_country"] = "countries.country_name";
	$datatypes["posaddresses.posaddress_country"] = "text";
	$group_fields[] = "posaddresses.posaddress_country";

	$fields["posaddresses.posaddress_place"] = "City";
	$attributes["posaddresses.posaddress_place"] = "IF(places.place_name is NULL, orders.order_shop_address_place, places.place_name) AS place_name";
	$datatypes["posaddresses.posaddress_place"] = "text";
	$group_fields[] = "posaddresses.posaddress_place";
	
	$fields["posaddresses.posaddress_name"] = "POS Name";
	$attributes["posaddresses.posaddress_name"] = "IF(posaddresses.posaddress_name is NULL, orders.order_shop_address_company, posaddresses.posaddress_name) AS posaddress_name";
	$datatypes["posaddresses.posaddress_name"] = "text";
	$group_fields[] = "posaddresses.posaddress_name";
	
	$fields["posaddresses.posaddress_address"] = "Address";
	$attributes["posaddresses.posaddress_address"] = "IF(posaddresses.posaddress_address is NULL, orders.order_shop_address_address, posaddresses.posaddress_address) AS place_address";
	$datatypes["posaddresses.posaddress_address"] = "text";


	$fields["posaddresses.posaddress_eprepnr"] = "Enterprise Reporting Number";
	$attributes["posaddresses.posaddress_eprepnr"] = "content_by_function";
	$datatypes["posaddresses.posaddress_eprepnr"] = "text";
	$content_by_function["posaddresses.posaddress_eprepnr"] = "get_pos_epnr(params)";
	$content_by_function_params["posaddresses.posaddress_eprepnr"] = array("order_id");


	$fields["posaddresses.environment"] = "POS Environment";
	$attributes["posaddresses.environment"] = "content_by_function";
	$datatypes["posaddresses.environment"] = "text";
	$content_by_function["posaddresses.environment"] = "get_pos_environment(params)";
	$content_by_function_params["posaddresses.environment"] = array("order_id");


	$fields["posaddresses.images"] = "Info about POS Images";
	$attributes["posaddresses.images"] = "content_by_function";
	$datatypes["posaddresses.images"] = "text";
	$content_by_function["posaddresses.images"] = "get_pos_images(params)";
	$content_by_function_params["posaddresses.images"] = array("order_id");




	
	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["calculated_content_sort_order"] = $calculated_content_sort_order;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;
	$db_info["group_fields"] = $group_fields;
	$db_info["function_fields"] = $function_fields;
	
	return $db_info;
}



/********************************************************************
    get DB Info for Client Address Fields
*********************************************************************/
function query_clientaddress_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_sort_order = array();
	$content_by_function = array();
	$content_by_function_params = array();
	$group_fields = array();
	$function_fields = array();

	$fields["clients.address_client_type"] = "Client Type";
	$attributes["clients.address_client_type"] = "clienttypes.client_type_code";
	$datatypes["clients.address_client_type"] = "text";
	
	$fields["clients.address_company"] = "Client Company";
	$attributes["clients.address_company"] = "clients.address_company";
	$datatypes["clients.address_company"] = "text";
	$group_fields[] = "clients.address_company";
	
	$fields["clients.address_company2"] = "Client Company 2";
	$attributes["clients.address_company2"] = "clients.address_company2";
	$datatypes["clients.address_company2"] = "text";
	
	$fields["clients.address_address"] = "Client Street";
	$attributes["clients.address_address"] = "clients.address_address";
	$datatypes["clients.address_address"] = "text";

	$fields["clients.address_address2"] = "Client Additional Address";
	$attributes["clients.address_address2"] = "clients.address_address2";
	$datatypes["clients.address_address2"] = "text";
	
	$fields["clients.address_zip"] = "Client Zip";
	$attributes["clients.address_zip"] = "clients.address_zip";
	$datatypes["clients.address_zip"] = "text";
	
	$fields["clients.address_place"] = "Client City";
	$attributes["clients.address_place"] = "clients.address_place";
	$datatypes["clients.address_place"] = "text";
	
	$fields["clients.address_country"] = "Client Country";
	$attributes["clients.address_country"] = "clientcountries.country_name";
	$datatypes["clients.address_country"] = "text";
	
	$fields["clients.address_phone"] = "Client Phone";
	$attributes["clients.address_phone"] = "clients.address_phone";
	$datatypes["clients.address_phone"] = "text";
	
	$fields["clients.address_mobile_phone"] = "Client Mobile";
	$attributes["clients.address_mobile_phone"] = "clients.address_mobile_phone";
	$datatypes["clients.address_mobile_phone"] = "text";
	
	$fields["clients.address_email"] = "Client Email";
	$attributes["clients.address_email"] = "clients.address_email";
	$datatypes["clients.address_email"] = "text";
	
	$fields["clients.address_website"] = "Client Website";
	$attributes["clients.address_website"] = "clients.address_website";
	$datatypes["clients.address_website"] = "text";
	
	$fields["clients.address_contact_name"] = "Client Contact Name";
	$attributes["clients.address_contact_name"] = "clients.address_contact_name";
	$datatypes["clients.address_contact_name"] = "text";
	
	$fields["clients.address_contact_email"] = "Client Contact Email";
	$attributes["clients.address_contact_email"] = "clients.address_contact_email";
	$datatypes["clients.address_contact_email"] = "text";
	
	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["calculated_content_sort_order"] = $calculated_content_sort_order;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;
	$db_info["group_fields"] = $group_fields;
	$db_info["function_fields"] = $function_fields;

	return $db_info;
}

/********************************************************************
    get DB Info for franchisee Address Fields
*********************************************************************/
function query_franchiseeaddress_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_sort_order = array();
	$content_by_function = array();
	$content_by_function_params = array();
	$group_fields = array();
	$function_fields = array();

	$fields["franchisees.address_company"] = "Owner Company";
	$attributes["franchisees.address_company"] = "franchisees.address_company";
	$datatypes["franchisees.address_company"] = "text";
	$group_fields[] = "franchisees.address_company";

	$fields["franchisees.address_company2"] = "Owner Company 2";
	$attributes["franchisees.address_company2"] = "franchisees.address_company2";
	$datatypes["franchisees.address_company2"] = "text";

	$fields["franchisees.address_address"] = "Owner Street";
	$attributes["franchisees.address_address"] = "franchisees.address_address";
	$datatypes["franchisees.address_address"] = "text";

	$fields["franchisees.address_address2"] = "Owner Additional Address";
	$attributes["franchisees.address_address2"] = "franchisees.address_address2";
	$datatypes["franchisees.address_address2"] = "text";

	$fields["franchisees.address_zip"] = "Owner Zip";
	$attributes["franchisees.address_zip"] = "franchisees.address_zip";
	$datatypes["franchisees.address_zip"] = "text";

	$fields["franchisees.address_place"] = "Owner City";
	$attributes["franchisees.address_place"] = "franchisees.address_place";
	$datatypes["franchisees.address_place"] = "text";

	$fields["franchisees.address_country"] = "Owner Country";
	$attributes["franchisees.address_country"] = "franchiseecountries.country_name";
	$datatypes["franchisees.address_country"] = "text";

	$fields["franchisees.address_phone"] = "Owner Phone";
	$attributes["franchisees.address_phone"] = "franchisees.address_phone";
	$datatypes["franchisees.address_phone"] = "text";

	$fields["franchisees.address_mobile_phone"] = "Owner Mobile";
	$attributes["franchisees.address_mobile_phone"] = "franchisees.address_mobile_phone";
	$datatypes["franchisees.address_mobile_phone"] = "text";

	$fields["franchisees.address_email"] = "Owner Email";
	$attributes["franchisees.address_email"] = "franchisees.address_email";
	$datatypes["franchisees.address_email"] = "text";

	$fields["franchisees.address_website"] = "Owner Website";
	$attributes["franchisees.address_website"] = "franchisees.address_website";
	$datatypes["franchisees.address_website"] = "text";

	$fields["franchisees.address_contact_name"] = "Owner Contact Name";
	$attributes["franchisees.address_contact_name"] = "franchisees.address_contact_name";
	$datatypes["franchisees.address_contact_name"] = "text";

	$fields["franchisees.address_contact_email"] = "Owner Contact Email";
	$attributes["franchisees.address_contact_email"] = "franchisees.address_contact_email";
	$datatypes["franchisees.address_contact_email"] = "text";

	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["calculated_content_sort_order"] = $calculated_content_sort_order;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;
	$db_info["group_fields"] = $group_fields;
	$db_info["function_fields"] = $function_fields;

	return $db_info;
}




/********************************************************************
    get DB Info for distribution channel fields
*********************************************************************/
function query_distribution_channel_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_sort_order = array();
	$content_by_function = array();
	$content_by_function_params = array();
	$group_fields = array();
	$function_fields = array();

	$fields["mps_distchannel_groups.mps_distchannel_group_name"] = "Distribution Channel Group";
	$attributes["mps_distchannel_groups.mps_distchannel_group_name"] = "mps_distchannel_groups.mps_distchannel_group_name";
	$datatypes["mps_distchannel_groups.mps_distchannel_group_name"] = "text";

	$fields["mps_distchannels.mps_distchannel_code"] = "Distribution Channel Code";
	$attributes["mps_distchannels.mps_distchannel_code"] = "mps_distchannels.mps_distchannel_code";
	$datatypes["mps_distchannels.mps_distchannel_code"] = "text";
	$group_fields[] = "mps_distchannels.mps_distchannel_code";

	$fields["mps_distchannels.mps_distchannel_name"] = "Distribution Channel Name";
	$attributes["mps_distchannels.mps_distchannel_name"] = "mps_distchannels.mps_distchannel_name";
	$datatypes["mps_distchannels.mps_distchannel_name"] = "text";

	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["calculated_content_sort_order"] = $calculated_content_sort_order;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;
	$db_info["group_fields"] = $group_fields;
	$db_info["function_fields"] = $function_fields;

	return $db_info;
}


/********************************************************************
    get DB Info for project fields
*********************************************************************/
function query_projects_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_sort_order = array();
	$content_by_function = array();
	$content_by_function_params = array();
	$group_fields = array();
	$function_fields = array();

	$fields["projects.project_popup_name"] = "PopUp Name";
	$attributes["projects.project_popup_name"] = "projects.project_popup_name";
	$datatypes["projects.project_popup_name"] = "text";

	$fields["projects.order_number"] = "Project Number";
	$attributes["projects.order_number"] = "orders.order_number";
	$datatypes["projects.order_number"] = "text";

	$fields["projects.project_state"] = "Treatment State";
	$attributes["projects.project_state"] = "project_states.project_state_text";
	$datatypes["projects.project_state"] = "text";

	$fields["projects.order_actual_order_state_code"] = "Global Project Status Code";
	$attributes["projects.order_actual_order_state_code"] = "orders.order_actual_order_state_code";
	$datatypes["projects.order_actual_order_state_code"] = "text";

	
    /*
	$fields["projects.order_development_status"] = "Project Status Development Code";
	$attributes["projects.order_development_status"] = "orders.order_development_status";
	$datatypes["projects.order_development_status"] = "text";

	$fields["projects.order_logistic_status"] = "Project Status Logistic Code";
	$attributes["projects.order_logistic_status"] = "orders.order_logistic_status";
	$datatypes["projects.order_logistic_status"] = "text";
	*/
	
	$fields["projects.order_actual_order_state_fulltext"] = "Global Project Status Full Text";
	$attributes["projects.order_actual_order_state_fulltext"] = "content_by_function";
	$datatypes["projects.order_actual_order_state_fulltext"] = "text";
	$content_by_function["projects.order_actual_order_state_fulltext"] = "get_order_state_text(params)";
	$content_by_function_params["projects.order_actual_order_state_fulltext"] = array("order_id", 1);

	/*
	$fields["projects.order_development_status_fulltext"] = "Project Status Development Full Text";
	$attributes["projects.order_development_status_fulltext"] = "content_by_function";
	$datatypes["projects.order_development_status_fulltext"] = "text";
	$content_by_function["projects.order_development_status_fulltext"] = "get_order_state_text(params)";
	$content_by_function_params["projects.order_development_status_fulltext"] = array("order_id", 3);

	
	$fields["projects.order_logistic_status_fulltext"] = "Project Status Logistic Full Text";
	$attributes["projects.order_logistic_status_fulltext"] = "content_by_function";
	$datatypes["projects.order_logistic_status_fulltext"] = "text";
	$content_by_function["projects.order_logistic_status_fulltext"] = "get_order_state_text(params)";
	$content_by_function_params["projects.order_logistic_status_fulltext"] = array("order_id", 2);
	*/
	
	$fields["projects.latest_order_state"] = "Date of Actual Project State";
	$attributes["projects.latest_order_state"] = "content_by_function";
	$datatypes["projects.latest_order_state"] = "date";
	$content_by_function["projects.latest_order_state"] = "get_latest_order_state_date(params)";
	$content_by_function_params["projects.latest_order_state"] = array("order_id");

	$fields["projects.project_cost_type"] = "Project Legal Type";
	$attributes["projects.project_cost_type"] = "project_costtypes.project_costtype_text";
	$datatypes["projects.project_cost_type"] = "text";
	$group_fields[] = "projects.project_cost_typee";

	$fields["projects.project_projectkind"] = "Project Type";
	$attributes["projects.project_projectkind"] = "projectkinds.projectkind_name";
	$datatypes["projects.project_projectkind"] = "text";
	$group_fields[] = "projects.project_projectkind";

	$fields["projects.project_type_subclass_id"] = "Project Type Subclass";
	$attributes["projects.project_type_subclass_id"] = "project_type_subclasses.project_type_subclass_name";
	$datatypes["projects.project_type_subclass_id"] = "boolean";

	$fields["projects.project_postype"] = "POS Type";
	$attributes["projects.project_postype"] = "postypes.postype_name";
	$datatypes["projects.project_postype"] = "text";
	$group_fields[] = "projects.project_postype";

	$fields["projects.project_pos_subclass"] = "POS Type Subclass";
	$attributes["projects.project_pos_subclass"] = "possubclasses.possubclass_name";
	$datatypes["projects.project_pos_subclass"] = "text";

	$fields["projects.project_is_flagship"] = "Flag Ship Option";
	$attributes["projects.project_is_flagship"] = "projects.project_is_flagship";
	$datatypes["projects.project_is_flagship"] = "boolean";


	$fields["projects.location"] = "Location Type";
	$attributes["projects.location"] = "content_by_function";
	$datatypes["projects.location"] = "text";
	$content_by_function["projects.location"] = "get_project_location(params)";
	$content_by_function_params["projects.location"] = array("project_id");

	$fields["projects.project_product_line"] = "Product Line";
	$attributes["projects.project_product_line"] = "product_lines.product_line_name";
	$datatypes["projects.project_product_line"] = "text";
	$group_fields[] = "projects.project_product_line";

	$fields["projects.project_product_line_subclass"] = "Product Line Subclass";
	$attributes["projects.project_product_line_subclass"] = "productline_subclasses.productline_subclass_name";
	$datatypes["projects.project_product_line_subclass"] = "text";

	//$fields["projects.project_furniture_type_store"] = "Uses Store Furniture";
	//$attributes["projects.project_furniture_type_store"] = "projects.project_furniture_type_store";
	//$datatypes["projects.project_furniture_type_store"] = "boolean";

	//$fields["projects.project_furniture_type_sis"] = "Uses SIS Furniture";
	//$attributes["projects.project_furniture_type_sis"] = "projects.project_furniture_type_sis";
	//$datatypes["projects.project_furniture_type_sis"] = "boolean";

	$fields["projects.project_uses_icedunes_visuals"] = "Uses Visuals";
	$attributes["projects.project_uses_icedunes_visuals"] = "projects.project_uses_icedunes_visuals";
	$datatypes["projects.project_uses_icedunes_visuals"] = "boolean";

	//$fields["projects.project_is_local_production"] = "Is locally porduced";
	//$attributes["projects.project_is_local_production"] = "projects.project_is_local_production";
	//$datatypes["projects.project_is_local_production"] = "boolean";

	//$fields["project_costs.project_cost_grosssqms"] = "Gross Surface";
	//$attributes["project_costs.project_cost_grosssqms"] = "project_costs.project_cost_grosssqms";
	//$datatypes["project_costs.project_cost_grosssqms"] = "decimal2";
	//$function_fields[] = "project_costs.project_cost_grosssqms";

	$fields["project_costs.project_cost_totalsqms"] = "Total Surface";
	$attributes["project_costs.project_cost_totalsqms"] = "project_costs.project_cost_totalsqms";
	$datatypes["project_costs.project_cost_totalsqms"] = "decimal2";
	$function_fields[] = "project_costs.project_cost_totalsqms";

	$fields["project_costs.project_cost_sqms"] = "Sales Surface";
	$attributes["project_costs.project_cost_sqms"] = "project_costs.project_cost_sqms";
	$datatypes["project_costs.project_cost_sqms"] = "decimal2";
	$function_fields[] = "project_costs.project_cost_sqms";

	$fields["project_costs.project_cost_backofficesqms"] = "Other Surface";
	$attributes["project_costs.project_cost_backofficesqms"] = "project_costs.project_cost_backofficesqms";
	$datatypes["project_costs.project_cost_backofficesqms"] = "decimal2";
	$function_fields[] = "project_costs.project_cost_backofficesqms";

	$fields["projects.project_cost_numfloors"] = "Num. Floors";
	$attributes["projects.project_cost_numfloors"] = "project_costs.project_cost_numfloors";
	$datatypes["projects.project_cost_numfloors"] = "decimal2";

	$fields["projects.project_watches_displayed"] = "Watches Displayed";
	$attributes["projects.project_watches_displayed"] = "projects.project_watches_displayed";
	$datatypes["projects.project_watches_displayed"] = "integer";

	$fields["projects.project_watches_stored"] = "Watches Stored";
	$attributes["projects.project_watches_stored"] = "projects.project_watches_stored";
	$datatypes["projects.project_watches_stored"] = "integer";

	$fields["projects.project_bijoux_displayed"] = "Watch Straps Displayed";
	$attributes["projects.project_bijoux_displayed"] = "projects.project_bijoux_displayed";
	$datatypes["projects.project_bijoux_displayed"] = "integer";

	$fields["projects.project_bijoux_stored"] = "Watch Straps Stored";
	$attributes["projects.project_bijoux_stored"] = "projects.project_bijoux_stored";
	$datatypes["projects.project_bijoux_stored"] = "integer";

	$fields["projects.submission_date"] = "Project Submission Date";
	$attributes["projects.submission_date"] = "orders.order_date";
	$datatypes["projects.submission_date"] = "date";

	$fields["projects.project_planned_opening_date"] = "Client's Preferred Opening Date";
	$attributes["projects.project_planned_opening_date"] = "projects.project_planned_opening_date";
	$datatypes["projects.project_planned_opening_date"] = "date";

	$fields["projects.project_real_opening_date"] = "Agreed Opening Date";
	$attributes["projects.project_real_opening_date"] = "projects.project_real_opening_date";
	$datatypes["projects.project_real_opening_date"] = "date";

	$fields["projects.project_actual_opening_date"] = "Actual POS Opening Date";
	$attributes["projects.project_actual_opening_date"] = "projects.project_actual_opening_date";
	$datatypes["projects.project_actual_opening_date"] = "date";

	$fields["projects.project_planned_takeover_date"] = "Planned Takeover Date";
	$attributes["projects.project_planned_takeover_date"] = "projects.project_planned_takeover_date";
	$datatypes["projects.project_planned_takeover_date"] = "date";

	$fields["projects.project_actual_takeover_date"] = "Actual Takeover Date";
	$attributes["projects.project_actual_takeover_date"] = "projects.project_actual_takeover_date";
	$datatypes["projects.project_actual_takeover_date"] = "date";

	$fields["projects.project_planned_closing_date"] = "Planned Closing Date";
	$attributes["projects.project_planned_closing_date"] = "projects.project_planned_closing_date";
	$datatypes["projects.project_planned_closing_date"] = "date";

	$fields["projects.project_shop_closingdate"] = "Actual POS Closing Date";
	$attributes["projects.project_shop_closingdate"] = "projects.project_shop_closingdate";
	$datatypes["projects.project_shop_closingdate"] = "date";

	$fields["projects.project_popup_closingdate"] = "PopUp Closing Date";
	$attributes["projects.project_popup_closingdate"] = "projects.project_popup_closingdate";
	$datatypes["projects.project_popup_closingdate"] = "date";

	$fields["projects.project_cancellation_date"] = "Date of Cancellation";
	$attributes["projects.project_cancellation_date"] = "content_by_function";
	$datatypes["projects.project_cancellation_date"] = "date";
	$content_by_function["projects.project_cancellation_date"] = "get_project_cancellation_date(params)";
	$content_by_function_params["projects.project_cancellation_date"] = array("order_id");

	$fields["projects.project_cancellation_state"] = "Cancellation at State";
	$attributes["projects.project_cancellation_state"] = "content_by_function";
	$datatypes["projects.project_cancellation_state"] = "text";
	$content_by_function["projects.project_cancellation_state"] = "get_project_cancellation_state(params)";
	$content_by_function_params["projects.project_cancellation_state"] = array("order_id");


	$fields["projects.project_construction_startdate"] = "Construction Staring Date";
	$attributes["projects.project_construction_startdate"] = "projects.project_construction_startdate";
	$datatypes["projects.project_construction_startdate"] = "date";

	$fields["projects.order_archive_date"] = "Archive Date";
	$attributes["projects.order_archive_date"] = "orders.order_archive_date";
	$datatypes["projects.order_archive_date"] = "date";


	
	$fields["projects.project_fagagreement_type"] = "Agreement Type";
	$attributes["projects.project_fagagreement_type"] = "calculated_content";
	$datatypes["projects.project_fagagreement_type"] = "text";
	$calculated_content["projects.project_fagagreement_type"] = "select agreement_type_name from agreement_types where agreement_type_id = ";
	$calculated_content_key["projects.project_fagagreement_type"] = "project_fagagreement_type";
	$calculated_content_field["projects.project_fagagreement_type"] = "agreement_type_name";
	$calculated_content_sort_order["projects.project_business_unit"] = "";
	
	
	$fields["projects.project_fagrsent"] = "Agreement Sent";
	$attributes["projects.project_fagrsent"] = "projects.project_fagrsent";
	$datatypes["projects.project_fagrsent"] = "boolean";

	$fields["projects.project_fagrsigned"] = "Agreement Signed";
	$attributes["projects.project_fagrsigned"] = "projects.project_fagrsigned";
	$datatypes["projects.project_fagrsigned"] = "boolean";

	$fields["projects.project_fagrstart"] = "Agreement Starting Daet";
	$attributes["projects.project_fagrstart"] = "projects.project_fagrstart";
	$datatypes["projects.project_fagrstart"] = "date";

	$fields["projects.project_fagrend"] = "Agreement Ending Date";
	$attributes["projects.project_fagrend"] = "projects.project_fagrend";
	$datatypes["projects.project_fagrend"] = "date";

	$fields["projects.project_fag_comment"] = "Agreement Comment";
	$attributes["projects.project_fag_comment"] = "projects.project_fag_comment";
	$datatypes["projects.project_fag_comment"] = "text";

	$fields["projects.date_set_on_hold"] = "Date when project was set on hold";
	$attributes["projects.date_set_on_hold"] = "content_by_function";
	$datatypes["projects.date_set_on_hold"] = "date";
	$content_by_function["projects.date_set_on_hold"] = "get_onhold_info_state(params)";
	$content_by_function_params["projects.date_set_on_hold"] = array("project_id", 'date');

	$fields["projects.reason_set_on_hold"] = "Reason why project was set on hold";
	$attributes["projects.reason_set_on_hold"] = "content_by_function";
	$datatypes["projects.reason_set_on_hold"] = "text";
	$content_by_function["projects.reason_set_on_hold"] = "get_onhold_info_state(params)";
	$content_by_function_params["projects.reason_set_on_hold"] = array("project_id", 'reason');


	
	/*
	$fields["projects.duration1"] = "LN approval: (proj.submission - LN approval)";
	$attributes["projects.duration1"] = "content_by_function";
	$datatypes["projects.duration1"] = "integer";
	$content_by_function["projects.duration1"] = "get_project_duration(params)";
	$content_by_function_params["projects.duration1"] = array("order_id", 1);
	$function_fields[] = "projects.duration1";

	$fields["projects.duration2"] = "LAYOUT REQUEST (LN approval - step 210)";
	$attributes["projects.duration2"] = "content_by_function";
	$datatypes["projects.duration2"] = "integer";
	$content_by_function["projects.duration2"] = "get_project_duration(params)";
	$content_by_function_params["projects.duration2"] = array("order_id", 2);
	$function_fields[] = "projects.duration2";

	$fields["projects.duration3"] = "HQ LAYOUT PREPARATION (step 310 - step 350)";
	$attributes["projects.duration3"] = "content_by_function";
	$datatypes["projects.duration3"] = "integer";
	$content_by_function["projects.duration3"] = "get_project_duration(params)";
	$content_by_function_params["projects.duration3"] = array("order_id", 3);
	$function_fields[] = "projects.duration3";

	$fields["projects.duration4"] = "HQ MINI BOOKLET PREPARATION (step 450 - step 417)";
	$attributes["projects.duration4"] = "content_by_function";
	$datatypes["projects.duration4"] = "integer";
	$content_by_function["projects.duration4"] = "get_project_duration(params)";
	$content_by_function_params["projects.duration4"] = array("order_id", 4);
	$function_fields[] = "projects.duration4";


	$fields["projects.duration5"] = "HQ BOOKLET PREPARATION (step 517 - step 551)";
	$attributes["projects.duration5"] = "content_by_function";
	$datatypes["projects.duration5"] = "integer";
	$content_by_function["projects.duration5"] = "get_project_duration(params)";
	$content_by_function_params["projects.duration5"] = array("order_id", 5);
	$function_fields[] = "projects.duration5";

	$fields["projects.duration6"] = "BUDGET PREPARATION HQ SUPPLIED ITEMS (step 441 - step 560)";
	$attributes["projects.duration6"] = "content_by_function";
	$datatypes["projects.duration6"] = "integer";
	$content_by_function["projects.duration6"] = "get_project_duration(params)";
	$content_by_function_params["projects.duration6"] = array("order_id", 6);
	$function_fields[] = "projects.duration6";


	$fields["projects.duration7"] = "BUDGET APPROVAL HQ SUPPLIED ITEMS (step 570 - 620)";
	$attributes["projects.duration7"] = "content_by_function";
	$datatypes["projects.duration7"] = "integer";
	$content_by_function["projects.duration7"] = "get_project_duration(params)";
	$content_by_function_params["projects.duration7"] = array("order_id", 7);
	$function_fields[] = "projects.duration7";



	$fields["projects.duration8"] = "DELIVERIES OF HQ SUPPLIED ITEMS (step 620 - step 800)";
	$attributes["projects.duration8"] = "content_by_function";
	$datatypes["projects.duration8"] = "integer";
	$content_by_function["projects.duration8"] = "get_project_duration(params)";
	$content_by_function_params["projects.duration8"] = array("order_id", 8);
	$function_fields[] = "projects.duration8";


	$fields["projects.duration9"] = "FULL BUDGET APPROVAL (step 620 - step 940)";
	$attributes["projects.duration9"] = "content_by_function";
	$datatypes["projects.duration9"] = "integer";
	$content_by_function["projects.duration9"] = "get_project_duration(params)";
	$content_by_function_params["projects.duration9"] = array("order_id", 9);
	$function_fields[] = "projects.duration9";

	$fields["projects.duration10"] = "CER-APPROVAL (step 840 - milestone CER approved)";
	$attributes["projects.duration10"] = "content_by_function";
	$datatypes["projects.duration10"] = "integer";
	$content_by_function["projects.duration10"] = "get_project_duration(params)";
	$content_by_function_params["projects.duration10"] = array("order_id", 10);
	$function_fields[] = "projects.duration10";


	$fields["projects.duration11"] = "Duration of project (project submission - actual opening date)";
	$attributes["projects.duration11"] = "content_by_function";
	$datatypes["projects.duration11"] = "integer";
	$content_by_function["projects.duration11"] = "get_project_duration(params)";
	$content_by_function_params["projects.duration11"] = array("order_id", 11);
	$function_fields[] = "projects.duration11";

	$fields["projects.duration12"] = "Duration of CMS completion (actual opening date - CMS completion)";
	$attributes["projects.duration12"] = "content_by_function";
	$datatypes["projects.duration12"] = "integer";
	$content_by_function["projects.duration12"] = "get_project_duration(params)";
	$content_by_function_params["projects.duration12"] = array("order_id", 12);
	$function_fields[] = "projects.duration12";

	$fields["projects.duration13"] = "Duration of of CMS approval (actual opening date - CMS approval)";
	$attributes["projects.duration13"] = "content_by_function";
	$datatypes["projects.duration13"] = "integer";
	$content_by_function["projects.duration13"] = "get_project_duration(params)";
	$content_by_function_params["projects.duration13"] = array("order_id", 13);
	$function_fields[] = "projects.duration13";



	$fields["projects.comment"] = "Comments";
	$attributes["projects.comment"] = "content_by_function";
	$datatypes["projects.comment"] = "integer";
	$content_by_function["projects.comment"] = "create_comment_column(params)";
	$content_by_function_params["projects.comment"] = array("order_id");
	$function_fields[] = "projects.comment";
	*/
	
	$fields["projects.project_is_urgent"] = "Urgent Project";
	$attributes["projects.project_is_urgent"] = "projects.project_is_urgent";
	$datatypes["projects.project_is_urgent"] = "boolean";



	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["calculated_content_sort_order"] = $calculated_content_sort_order;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;
	$db_info["group_fields"] = $group_fields;
	$db_info["function_fields"] = $function_fields;


	return $db_info;
}


/********************************************************************
    get DB Info for cer fields
*********************************************************************/
function query_cer_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_sort_order = array();
	$content_by_function = array();
	$content_by_function_params = array();
	$group_fields = array();
	$function_fields = array();


	$fields["ln_basicdata.no_ln_needed"] = "LN needed";
	$attributes["ln_basicdata.no_ln_needed"] = "content_by_function";
	$datatypes["ln_basicdata.no_ln_needed"] = "boolean";
	$content_by_function["ln_basicdata.no_ln_needed"] = "get_ln_needed(params)";
	$content_by_function_params["ln_basicdata.no_ln_needed"] = array("order_id");

	$fields["ln_basicdata.ln_basicdata_submitted"] = "LN Submission Date";
	$attributes["ln_basicdata.ln_basicdata_submitted"] = "ln_basicdata.ln_basicdata_submitted";
	$datatypes["ln_basicdata.ln_basicdata_submitted"] = "date";

	$fields["ln_basicdata.ln_basicdata_submitted_by"] = "LN submitted by";
	$attributes["ln_basicdata.ln_basicdata_submitted_by"] = "calculated_content";
	$datatypes["ln_basicdata.ln_basicdata_submitted_by"] = "date";
	$calculated_content["ln_basicdata.ln_basicdata_submitted_by"] = "select concat(user_name, ' ', user_firstname) from users where user_id = ";
	$calculated_content_key["ln_basicdata.ln_basicdata_submitted_by"] = "ln_basicdata_submitted_by";
	$calculated_content_field["ln_basicdata.ln_basicdata_submitted_by"] = "ln_basicdata_submitted_by";
	$calculated_content_sort_order["ln_basicdata.ln_basicdata_submitted_by"] = "";


	$fields["ln_basicdata.ln_basicdata_resubmitted"] = "LN Resubmission Date";
	$attributes["ln_basicdata.ln_basicdata_resubmitted"] = "ln_basicdata.ln_basicdata_resubmitted";
	$datatypes["ln_basicdata.ln_basicdata_resubmitted"] = "date";

	$fields["ln_basicdata.ln_basicdata_resubmitted_by"] = "LN resubmitted by";
	$attributes["ln_basicdata.ln_basicdata_resubmitted_by"] = "calculated_content";
	$datatypes["ln_basicdata.ln_basicdata_resubmitted_by"] = "date";
	$calculated_content["ln_basicdata.ln_basicdata_resubmitted_by"] = "select concat(user_name, ' ', user_firstname) from users where user_id = ";
	$calculated_content_key["ln_basicdata.ln_basicdata_resubmitted_by"] = "ln_basicdata_resubmitted_by";
	$calculated_content_field["ln_basicdata.ln_basicdata_resubmitted_by"] = "ln_basicdata_resubmitted_by";
	$calculated_content_sort_order["ln_basicdata.ln_basicdata_resubmitted_by"] = "";


	$fields["ln_basicdata.ln_basicdata_rejected"] = "LN Rejection Date";
	$attributes["ln_basicdata.ln_basicdata_rejected"] = "ln_basicdata.ln_basicdata_rejected";
	$datatypes["ln_basicdata.ln_basicdata_rejected"] = "date";

	$fields["ln_basicdata.ln_basicdata_rejected_by"] = "LN rejected by";
	$attributes["ln_basicdata.ln_basicdata_rejected_by"] = "calculated_content";
	$datatypes["ln_basicdata.ln_basicdata_rejected_by"] = "date";
	$calculated_content["ln_basicdata.ln_basicdata_rejected_by"] = "select concat(user_name, ' ', user_firstname) from users where user_id = ";
	$calculated_content_key["ln_basicdata.ln_basicdata_rejected_by"] = "ln_basicdata_rejected_by";
	$calculated_content_field["ln_basicdata.ln_basicdata_rejected_by"] = "ln_basicdata_rejected_by";
	$calculated_content_sort_order["ln_basicdata.ln_basicdata_rejected_by"] = "";


	$fields["ln_basicdata.ln_basicdata_locked"] = "LN locked";
	$attributes["ln_basicdata.ln_basicdata_locked"] = "ln_basicdata.ln_basicdata_locked";
	$datatypes["ln_basicdata.ln_basicdata_locked"] = "boolean";

	
	$fields["cer_basicdata.no_cer_needed"] = "CER/AF needed";
	$attributes["cer_basicdata.no_cer_needed"] = "content_by_function";
	$datatypes["cer_basicdata.no_cer_needed"] = "boolean";
	$content_by_function["cer_basicdata.no_cer_needed"] = "get_cer_needed(params)";
	$content_by_function_params["cer_basicdata.no_cer_needed"] = array("order_id");
	

	$fields["cer_basicdata.cer_basicdata_submitted"] = "CER/AF Submission Date";
	$attributes["cer_basicdata.cer_basicdata_submitted"] = "cer_basicdata.cer_basicdata_submitted";
	$datatypes["cer_basicdata.cer_basicdata_submitted"] = "date";

	$fields["cer_basicdata.cer_basicdata_submitted_by"] = "CER/AF submitted by";
	$attributes["cer_basicdata.cer_basicdata_submitted_by"] = "calculated_content";
	$datatypes["cer_basicdata.cer_basicdata_submitted_by"] = "date";
	$calculated_content["cer_basicdata.cer_basicdata_submitted_by"] = "select concat(user_name, ' ', user_firstname) from users where user_id = ";
	$calculated_content_key["cer_basicdata.cer_basicdata_submitted_by"] = "cer_basicdata_submitted_by";
	$calculated_content_field["cer_basicdata.cer_basicdata_submitted_by"] = "cer_basicdata_submitted_by";
	$calculated_content_sort_order["cer_basicdata.cer_basicdata_submitted_by"] = "";


	$fields["cer_basicdata.cer_basicdata_resubmitted"] = "CER/AF Resubmission Date";
	$attributes["cer_basicdata.cer_basicdata_resubmitted"] = "cer_basicdata.cer_basicdata_resubmitted";
	$datatypes["cer_basicdata.cer_basicdata_resubmitted"] = "date";

	$fields["cer_basicdata.cer_basicdata_resubmitted_by"] = "CER/AF resubmitted by";
	$attributes["cer_basicdata.cer_basicdata_resubmitted_by"] = "calculated_content";
	$datatypes["cer_basicdata.cer_basicdata_resubmitted_by"] = "date";
	$calculated_content["cer_basicdata.cer_basicdata_resubmitted_by"] = "select concat(user_name, ' ', user_firstname) from users where user_id = ";
	$calculated_content_key["cer_basicdata.cer_basicdata_resubmitted_by"] = "cer_basicdata_resubmitted_by";
	$calculated_content_field["cer_basicdata.cer_basicdata_resubmitted_by"] = "cer_basicdata_resubmitted_by";
	$calculated_content_sort_order["cer_basicdata.cer_basicdata_resubmitted_by"] = "";


	$fields["cer_basicdata.cer_basicdata_rejected"] = "CER/AF Rejection Date";
	$attributes["cer_basicdata.cer_basicdata_rejected"] = "cer_basicdata.cer_basicdata_rejected";
	$datatypes["cer_basicdata.cer_basicdata_rejected"] = "date";

	$fields["cer_basicdata.cer_basicdata_rejected_by"] = "CER/AF rejected by";
	$attributes["cer_basicdata.cer_basicdata_rejected_by"] = "calculated_content";
	$datatypes["cer_basicdata.cer_basicdata_rejected_by"] = "date";
	$calculated_content["cer_basicdata.cer_basicdata_rejected_by"] = "select concat(user_name, ' ', user_firstname) from users where user_id = ";
	$calculated_content_key["cer_basicdata.cer_basicdata_rejected_by"] = "cer_basicdata_rejected_by";
	$calculated_content_field["cer_basicdata.cer_basicdata_rejected_by"] = "cer_basicdata_rejected_by";
	$calculated_content_sort_order["cer_basicdata.cer_basicdata_rejected_by"] = "";


	$fields["cer_basicdata.cer_basicdata_cer_locked"] = "CER/AF locked";
	$attributes["cer_basicdata.cer_basicdata_cer_locked"] = "cer_basicdata.cer_basicdata_cer_locked";
	$datatypes["cer_basicdata.cer_basicdata_cer_locked"] = "boolean";





	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["calculated_content_sort_order"] = $calculated_content_sort_order;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;
	$db_info["group_fields"] = $group_fields;
	$db_info["function_fields"] = $function_fields;


	return $db_info;
}

/********************************************************************
    get DB Info for project rental fields
*********************************************************************/
function query_logistics_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_sort_order = array();
	$content_by_function = array();
	$content_by_function_params = array();
	$group_fields = array();
	$function_fields = array();

	$fields["orders.order_preferred_transportation_arranged"] = "Transportation arranged by";
	$attributes["orders.order_preferred_transportation_arranged"] = "calculated_content";
	$datatypes["orders.order_preferred_transportation_arranged"] = "text";
	$calculated_content["orders.order_preferred_transportation_arranged"] = "select transportation_type_name from transportation_types where transportation_type_code = 'arranged' and transportation_type_id = ";
	$calculated_content_key["orders.order_preferred_transportation_arranged"] = "order_preferred_transportation_arranged";
	$calculated_content_field["orders.order_preferred_transportation_arranged"] = "transportation_type_name";
	$calculated_content_sort_order["orders.order_preferred_transportation_arranged"] = "";


	$fields["orders.order_preferred_transportation_mode"] = "Transportation mode";
	$attributes["orders.order_preferred_transportation_mode"] = "calculated_content";
	$datatypes["orders.order_preferred_transportation_mode"] = "text";
	$calculated_content["orders.order_preferred_transportation_mode"] = "select transportation_type_name from transportation_types where transportation_type_code = 'mode' and transportation_type_id = ";
	$calculated_content_key["orders.order_preferred_transportation_mode"] = "order_preferred_transportation_mode";
	$calculated_content_field["orders.order_preferred_transportation_mode"] = "transportation_type_name";
	$calculated_content_sort_order["orders.order_preferred_transportation_mode"] = "";


	$fields["projects.floor_delivery_date"] = "Floor Arival Date";
	$attributes["projects.floor_delivery_date"] = "content_by_function";
	$datatypes["projects.floor_delivery_date"] = "date";
	$content_by_function["projects.floor_delivery_date"] = "get_floor_delivery_date(params)";
	$content_by_function_params["projects.floor_delivery_date"] = array("order_id");

	$fields["projects.frames_delivery_date"] = "Frames Arrival Date";
	$attributes["projects.frames_delivery_date"] = "content_by_function";
	$datatypes["projects.frames_delivery_date"] = "date";
	$content_by_function["projects.frames_delivery_date"] = "get_frames_delivery_date(params)";
	$content_by_function_params["projects.frames_delivery_date"] = array("order_id");

	$fields["projects.visuals_delivery_date"] = "Visuals Arrival Date";
	$attributes["projects.visuals_delivery_date"] = "content_by_function";
	$datatypes["projects.visuals_delivery_date"] = "date";
	$content_by_function["projects.visuals_delivery_date"] = "get_visuals_delivery_date(params)";
	$content_by_function_params["projects.visuals_delivery_date"] = array("order_id");

	$fields["projects.illumination_delivery_date"] = "Illumination Arrival Date";
	$attributes["projects.illumination_delivery_date"] = "content_by_function";
	$datatypes["projects.illumination_delivery_date"] = "date";
	$content_by_function["projects.illumination_delivery_date"] = "get_illumination_delivery_date(params)";
	$content_by_function_params["projects.illumination_delivery_date"] = array("order_id");

	$fields["projects.furniture_delivery_date"] = "Furniture Arrival Date";
	$attributes["projects.furniture_delivery_date"] = "content_by_function";
	$datatypes["projects.furniture_delivery_date"] = "date";
	$content_by_function["projects.furniture_delivery_date"] = "get_furniture_delivery_date(params)";
	$content_by_function_params["projects.furniture_delivery_date"] = array("order_id");


	$fields["projects.category_order_state"] = "Item Category Oder State";
	$attributes["projects.category_order_state"] = "subgroup_category_order_state";

	$fields["projects.category_delivery_state"] = "Item Category Delivery State";
	$attributes["projects.category_delivery_state"] = "subgroup_category_delivery_state";
	

	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["calculated_content_sort_order"] = $calculated_content_sort_order;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;
	$db_info["group_fields"] = $group_fields;
	$db_info["function_fields"] = $function_fields;


	return $db_info;
}

/********************************************************************
    get DB Info for project rental fields
*********************************************************************/
function query_rental_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_sort_order = array();
	$content_by_function = array();
	$content_by_function_params = array();
	$group_fields = array();
	$function_fields = array();

	$fields["posleases.poslease_startdate"] = "Lease Start Date";
	$attributes["posleases.poslease_startdate"] = "content_by_function";
	$datatypes["posleases.poslease_startdate"] = "date";
	$content_by_function["posleases.poslease_startdate"] = "get_rental_detail(params)";
	$content_by_function_params["posleases.poslease_startdate"] = array("order_id", 'poslease_startdate');

	$fields["posleases.poslease_enddate"] = "Lease End Date";
	$attributes["posleases.poslease_enddate"] = "content_by_function";
	$datatypes["posleases.poslease_enddate"] = "date";
	$content_by_function["posleases.poslease_enddate"] = "get_rental_detail(params)";
	$content_by_function_params["posleases.poslease_enddate"] = array("order_id", 'poslease_enddate');

	$fields["posleases.poslease_signature_date"] = "Lease Contract Signed on";
	$attributes["posleases.poslease_signature_date"] = "content_by_function";
	$datatypes["posleases.poslease_signature_date"] = "date";
	$content_by_function["posleases.poslease_signature_date"] = "get_rental_detail(params)";
	$content_by_function_params["posleases.poslease_signature_date"] = array("order_id", 'poslease_signature_date');

	
	$fields["posleases.poslease_hasfixrent"] = "Contract contains fixed rent";
	$attributes["posleases.poslease_hasfixrent"] = "content_by_function";
	$datatypes["posleases.poslease_hasfixrent"] = "boolean";
	$content_by_function["posleases.poslease_hasfixrent"] = "get_rental_detail(params)";
	$content_by_function_params["posleases.poslease_hasfixrent"] = array("order_id", 'poslease_hasfixrent');
	

	$fields["posleases.poslease_isindexed"] = "Contract contains tacit renewal clause";
	$attributes["posleases.poslease_isindexed"] = "content_by_function";
	$datatypes["posleases.poslease_isindexed"] = "boolean";
	$content_by_function["posleases.poslease_isindexed"] = "get_rental_detail(params)";
	$content_by_function_params["posleases.poslease_isindexed"] = array("order_id", 'poslease_isindexed');

	$fields["posleases.poslease_indexclause_in_contract"] = "Index Clause in Contract";
	$attributes["posleases.poslease_indexclause_in_contract"] = "content_by_function";
	$datatypes["posleases.poslease_indexclause_in_contract"] = "boolean";
	$content_by_function["posleases.poslease_indexclause_in_contract"] = "get_rental_detail(params)";
	$content_by_function_params["posleases.poslease_indexclause_in_contract"] = array("order_id", 'poslease_indexclause_in_contract');

	$fields["posleases.poslease_extensionoption"] = "Extension Option to Date";
	$attributes["posleases.poslease_extensionoption"] = "content_by_function";
	$datatypes["posleases.poslease_extensionoption"] = "date";
	$content_by_function["posleases.poslease_extensionoption"] = "get_rental_detail(params)";
	$content_by_function_params["posleases.poslease_extensionoption"] = array("order_id", 'poslease_extensionoption');

	$fields["posleases.poslease_exitoption"] = "Exit Option to Date";
	$attributes["posleases.poslease_exitoption"] = "content_by_function";
	$datatypes["posleases.poslease_exitoption"] = "date";
	$content_by_function["posleases.poslease_exitoption"] = "get_rental_detail(params)";
	$content_by_function_params["posleases.poslease_exitoption"] = array("order_id", 'poslease_exitoption');

	$fields["posleases.poslease_termination_time"] = "Termination Deadline";
	$attributes["posleases.poslease_termination_time"] = "content_by_function";
	$datatypes["posleases.poslease_termination_time"] = "integer";
	$content_by_function["posleases.poslease_termination_time"] = "get_rental_detail(params)";
	$content_by_function_params["posleases.poslease_termination_time"] = array("order_id", 'poslease_termination_time');

	$fields["posleases.cer_basicdata_deadline_property"] = "Deadline for property";
	$attributes["posleases.cer_basicdata_deadline_property"] = "content_by_function";
	$datatypes["posleases.cer_basicdata_deadline_property"] = "date";
	$content_by_function["posleases.cer_basicdata_deadline_property"] = "get_cer_detail(params)";
	$content_by_function_params["posleases.cer_basicdata_deadline_property"] = array("project_id", 'cer_basicdata_deadline_property');

	$fields["posleases.poslease_handoverdate"] = "Handover Date";
	$attributes["posleases.poslease_handoverdate"] = "content_by_function";
	$datatypes["posleases.poslease_handoverdate"] = "date";
	$content_by_function["posleases.poslease_handoverdate"] = "get_rental_detail(params)";
	$content_by_function_params["posleases.poslease_handoverdate"] = array("order_id", 'poslease_handoverdate');

	$fields["posleases.poslease_firstrentpayed"] = "First Rent Paid as from";
	$attributes["posleases.poslease_firstrentpayed"] = "content_by_function";
	$datatypes["posleases.poslease_firstrentpayed"] = "date";
	$content_by_function["posleases.poslease_firstrentpayed"] = "get_rental_detail(params)";
	$content_by_function_params["posleases.poslease_firstrentpayed"] = array("order_id", 'poslease_firstrentpayed');

	$fields["posleases.poslease_freeweeks"] = "Rent Free Period in Weeks";
	$attributes["posleases.poslease_freeweeks"] = "content_by_function";
	$datatypes["posleases.poslease_freeweeks"] = "integer";
	$content_by_function["posleases.poslease_freeweeks"] = "get_rental_detail(params)";
	$content_by_function_params["posleases.poslease_freeweeks"] = array("order_id", 'poslease_freeweeks');




	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["calculated_content_sort_order"] = $calculated_content_sort_order;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;
	$db_info["group_fields"] = $group_fields;
	$db_info["function_fields"] = $function_fields;


	return $db_info;
}

/********************************************************************
    get DB Info for CMS fields
*********************************************************************/
function query_cms_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_sort_order = array();
	$content_by_function = array();
	$content_by_function_params = array();
	$group_fields = array();
	$function_fields = array();

	$fields["project_costs.project_cost_cms_completion_due_date"] = "CMS Due Date";
	$attributes["project_costs.project_cost_cms_completion_due_date"] = "project_costs.project_cost_cms_completion_due_date";
	$datatypes["project_costs.project_cost_cms_completion_due_date"] = "date";
	
	$fields["project_costs.project_cost_cms_completion_date"] = "Logistics CMS completed on";
	$attributes["project_costs.project_cost_cms_completion_date"] = "project_costs.project_cost_cms_completion_date";
	$datatypes["project_costs.project_cost_cms_completion_date"] = "date";

	$fields["project_costs.project_cost_cms_completed_by"] = "Logistics CMS completed by";
	$attributes["project_costs.project_cost_cms_completed_by"] = "calculated_content";
	$datatypes["project_costs.project_cost_cms_completed_by"] = "text";
	$calculated_content["project_costs.project_cost_cms_completed_by"] = "select concat(user_name, ' ', user_firstname) as username from users where user_id =  ";
	$calculated_content_key["project_costs.project_cost_cms_completed_by"] = "project_cost_cms_completed_by";
	$calculated_content_field["project_costs.project_cost_cms_completed_by"] = "username";
	$calculated_content_sort_order["project_costs.project_cost_cms_completed_by"] = "";

	$fields["project_costs.project_cost_cms_approved_date"] = "Development CMS completed on";
	$attributes["project_costs.project_cost_cms_approved_date"] = "project_costs.project_cost_cms_approved_date";
	$datatypes["project_costs.project_cost_cms_approved_date"] = "date";

	$fields["project_costs.project_cost_cms_approved_by"] = "Development CMS completed by";
	$attributes["project_costs.project_cost_cms_approved_by"] = "calculated_content";
	$datatypes["project_costs.project_cost_cms_approved_by"] = "text";
	$calculated_content["project_costs.project_cost_cms_approved_by"] = "select concat(user_name, ' ', user_firstname)  as username from users where user_id =  ";
	$calculated_content_key["project_costs.project_cost_cms_approved_by"] = "project_cost_cms_approved_by";
	$calculated_content_field["project_costs.project_cost_cms_approved_by"] = "username";
	$calculated_content_sort_order["project_costs.project_cost_cms_approved_by"] = "";

	$fields["project_costs.project_cost_cms_controlled_date"] = "CMS approved on";
	$attributes["project_costs.project_cost_cms_controlled_date"] = "project_costs.project_cost_cms_controlled_date";
	$datatypes["project_costs.project_cost_cms_controlled_date"] = "date";

	$fields["project_costs.project_cost_cms_controlled_by"] = "CMS approved by";
	$attributes["project_costs.project_cost_cms_controlled_by"] = "calculated_content";
	$datatypes["project_costs.project_cost_cms_controlled_by"] = "text";
	$calculated_content["project_costs.project_cost_cms_controlled_by"] = "select concat(user_name, ' ', user_firstname)  as username from users where user_id =  ";
	$calculated_content_key["project_costs.project_cost_cms_controlled_by"] = "project_cost_cms_controlled_by";
	$calculated_content_field["project_costs.project_cost_cms_controlled_by"] = "username";
	$calculated_content_sort_order["project_costs.project_cost_cms_controlled_by"] = "";
	
	
	$fields["project_costs.cms_completion_overdue"] = "Logistics CMS completion overdue in days";
	$attributes["project_costs.cms_completion_overdue"] = "content_by_function";
	$datatypes["project_costs.cms_completion_overdue"] = "integer_overduedays";
	$content_by_function["project_costs.cms_completion_overdue"] = "get_cms_overdue_days_date(params)";
	$content_by_function_params["project_costs.cms_completion_overdue"] = array("project_id", 1);
	$function_fields[] = "project_costs.cms_completion_overdue";

	$fields["project_costs.cms_approval_overdue"] = "Development CMS completion overdue in days";
	$attributes["project_costs.cms_approval_overdue"] = "content_by_function";
	$datatypes["project_costs.cms_approval_overdue"] = "integer_overduedays";
	$content_by_function["project_costs.cms_approval_overdue"] = "get_cms_overdue_days_date(params)";
	$content_by_function_params["project_costs.cms_approval_overdue"] = array("project_id", 2);
	$function_fields[] = "project_costs.cms_approval_overdue";

	$fields["project_costs.cms_controlled_overdue"] = "CMS approval overdue in days";
	$attributes["project_costs.cms_controlled_overdue"] = "content_by_function";
	$datatypes["project_costs.cms_controlled_overdue"] = "integer_overduedays";
	$content_by_function["project_costs.cms_controlled_overdue"] = "get_cms_overdue_days_date(params)";
	$content_by_function_params["project_costs.cms_controlled_overdue"] = array("project_id", 3);
	$function_fields[] = "project_costs.cms_controlled_overdue";


	$fields["project_costs.cms_completion_days"] = "Number of Days until Logistic CMS was completed";
	$attributes["project_costs.cms_completion_days"] = "content_by_function";
	$datatypes["project_costs.cms_completion_days"] = "integer_overduedays";
	$content_by_function["project_costs.cms_completion_days"] = "get_cms_completion_days(params)";
	$content_by_function_params["project_costs.cms_completion_days"] = array("project_id", 1);
	$function_fields[] = "project_costs.cms_completion_days";

	$fields["project_costs.cms_approval_days"] = "Number of Days until Development CMS was completed";
	$attributes["project_costs.cms_approval_days"] = "content_by_function";
	$datatypes["project_costs.cms_approval_days"] = "integer_overduedays";
	$content_by_function["project_costs.cms_approval_days"] = "get_cms_completion_days(params)";
	$content_by_function_params["project_costs.cms_approval_days"] = array("project_id", 2);
	$function_fields[] = "project_costs.cms_approval_days";


	$fields["project_costs.cms_controlled_days"] = "Number of Days until CMS was approved";
	$attributes["project_costs.cms_controlled_days"] = "content_by_function";
	$datatypes["project_costs.cms_controlled_days"] = "integer_overduedays";
	$content_by_function["project_costs.cms_controlled_days"] = "get_cms_completion_days(params)";
	$content_by_function_params["project_costs.cms_controlled_days"] = array("project_id", 3);
	$function_fields[] = "project_costs.cms_controlled_days";

	
	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["calculated_content_sort_order"] = $calculated_content_sort_order;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;
	$db_info["group_fields"] = $group_fields;
	$db_info["function_fields"] = $function_fields;

	return $db_info;
}


/********************************************************************
    get DB Info for project sheet fields
*********************************************************************/
function query_projects_sheet_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_sort_order = array();
	$content_by_function = array();
	$content_by_function_params = array();
	$group_fields = array();
	$function_fields = array();

	
	$fields["projectsheet.project_business_unit"] = "Business Unit";
	$attributes["projectsheet.project_business_unit"] = "calculated_content";
	$datatypes["projectsheet.project_business_unit"] = "text";
	$calculated_content["projectsheet.project_business_unit"] = "select business_unit_name from business_units where business_unit_id = ";
	$calculated_content_key["projectsheet.project_business_unit"] = "project_business_unit";
	$calculated_content_field["projectsheet.project_business_unit"] = "business_unit_name";
	$calculated_content_sort_order["projectsheet.project_business_unit"] = "";


	$fields["projectsheet.project_approval_date9"] = "Deadline fro Approval";
	$attributes["projectsheet.project_approval_date9"] = "projects.project_approval_date9";
	$datatypes["projectsheet.project_approval_date9"] = "date";


	$fields["projectsheet.project_cost_center"] = "Cost Center";
	$attributes["projectsheet.project_cost_center"] = "projects.project_cost_center";
	$datatypes["projectsheet.project_cost_center"] = "text";

	$fields["projectsheet.project_account_number"] = "Account Number";
	$attributes["projectsheet.project_account_number"] = "projects.project_account_number";
	$datatypes["projectsheet.project_account_number"] = "text";

	$fields["projectsheet.project_ps_product_line"] = "Product Line";
	$attributes["projectsheet.project_ps_product_line"] = "projects.project_ps_product_line";
	$datatypes["projectsheet.project_ps_product_line"] = "text";

	$fields["projectsheet.project_planned_amount_next_year"] = "Planned Amount Current Year";
	$attributes["projectsheet.project_planned_amount_next_year"] = "projects.project_planned_amount_next_year";
	$datatypes["projectsheet.project_planned_amount_next_year"] = "decimal2";
	$function_fields[] = "projectsheet.project_planned_amount_next_year";

	$fields["projectsheet.project_planned_amount_current_year"] = "Planned Amount Next Year";
	$attributes["projectsheet.project_planned_amount_current_year"] = "projects.project_planned_amount_current_year";
	$datatypes["projectsheet.project_planned_amount_current_year"] = "decimal2";
	$function_fields[] = "projectsheet.project_planned_amount_current_year";

	$fields["projectsheet.project_account_number"] = "Account Number";
	$attributes["projectsheet.project_account_number"] = "projects.project_account_number";
	$datatypes["projectsheet.project_account_number"] = "text";

	$fields["projectsheet.project_opening_date"] = "Project Sheet Opening Date";
	$attributes["projectsheet.project_opening_date"] = "projects.project_opening_date";
	$datatypes["projectsheet.project_opening_date"] = "date";

	$fields["projectsheet.project_closing_date"] = "Project Sheet Closing Date";
	$attributes["projectsheet.project_closing_date"] = "projects.project_closing_date";
	$datatypes["projectsheet.project_closing_date"] = "date";

	$fields["projectsheet.project_share_company"] = "Share Tissot in %";
	$attributes["projectsheet.project_share_company"] = "projects.project_share_company";
	$datatypes["projectsheet.project_share_company"] = "integer";

	$fields["projectsheet.project_share_other"] = "Share Other in %";
	$attributes["projectsheet.project_share_other"] = "projects.project_share_other";
	$datatypes["projectsheet.project_share_other"] = "integer";

	$fields["projectsheet.project_budget_total"] = "Budget Total in CHF";
	$attributes["projectsheet.project_budget_total"] = "projects.project_budget_total";
	$datatypes["projectsheet.project_budget_total"] = "integer";
	$function_fields[] = "projectsheet.project_budget_total";

	$fields["projectsheet.project_budget_committed"] = "Budget Committed in CHF";
	$attributes["projectsheet.project_budget_committed"] = "projects.project_budget_committed";
	$datatypes["projectsheet.project_budget_committed"] = "integer";
	$function_fields[] = "projectsheet.project_budget_committed";

	$fields["projectsheet.project_budget_spent"] = "Already Spent in CHF";
	$attributes["projectsheet.project_budget_spent"] = "projects.project_budget_spent";
	$datatypes["projectsheet.project_budget_spent"] = "integer";
	$function_fields[] = "projectsheet.project_budget_spent";

	$fields["projectsheet.project_budget_after_project"] = "Open to Commit after this Project in CHF";
	$attributes["projectsheet.project_budget_after_project"] = "projects.project_budget_after_project";
	$datatypes["projectsheet.project_budget_after_project"] = "integer";
	$function_fields[] = "projectsheet.project_budget_after_project";

	

	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["calculated_content_sort_order"] = $calculated_content_sort_order;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;
	$db_info["group_fields"] = $group_fields;
	$db_info["function_fields"] = $function_fields;

	return $db_info;
}


/********************************************************************
    get DB Info for staff fields
*********************************************************************/
function query_staff_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_sort_order = array();
	$content_by_function = array();
	$content_by_function_params = array();
	$group_fields = array();
	$function_fields = array();

	$fields["staff.order_user"] = "Project Owner";
	$attributes["staff.order_user"] = "calculated_content";
	$datatypes["staff.order_user"] = "text";
	$calculated_content["staff.order_user"] = "select concat(user_name, ' ', user_firstname) from users where user_id = ";
	$calculated_content_key["staff.order_user"] = "order_user";
	$calculated_content_field["staff.order_user"] = "order_user";
	$calculated_content_sort_order["staff.order_user"] = "";

	$fields["staff.project_retail_coordinator"] = "Project Leader";
	$attributes["staff.project_retail_coordinator"] = "calculated_content";
	$datatypes["staff.project_retail_coordinator"] = "text";
	$calculated_content["staff.project_retail_coordinator"] = "select concat(user_name, ' ', user_firstname) from users where user_id = ";
	$calculated_content_key["staff.project_retail_coordinator"] = "project_retail_coordinator";
	$calculated_content_field["staff.project_retail_coordinator"] = "project_retail_coordinator";
	$calculated_content_sort_order["staff.project_retail_coordinator"] = "";
	$group_fields[] = "staff.project_retail_coordinator";

	$fields["staff.project_local_retail_coordinator"] = "Local Retail Coordinator";
	$attributes["staff.project_local_retail_coordinator"] = "calculated_content";
	$datatypes["staff.project_local_retail_coordinator"] = "text";
	$calculated_content["staff.project_local_retail_coordinator"] = "select concat(user_name, ' ', user_firstname) from users where user_id = ";
	$calculated_content_key["staff.project_local_retail_coordinator"] = "project_local_retail_coordinator";
	$calculated_content_field["staff.project_local_retail_coordinator"] = "project_local_retail_coordinator";
	$calculated_content_sort_order["staff.project_local_retail_coordinator"] = "";
	$group_fields[] = "staff.project_local_retail_coordinator";

	$fields["staff.project_design_contractor"] = "Design Contractor";
	$attributes["staff.project_design_contractor"] = "calculated_content";
	$datatypes["staff.project_design_contractor"] = "text";
	$calculated_content["staff.project_design_contractor"] = "select concat(address_company, ' ', user_name, ' ', user_firstname) from users left join addresses on address_id = user_address where user_id = ";
	$calculated_content_key["staff.project_design_contractor"] = "project_design_contractor";
	$calculated_content_field["staff.project_design_contractor"] = "project_design_contractor";
	$calculated_content_sort_order["staff.project_design_contractor"] = "";

	$fields["staff.project_design_supervisor"] = "Design Supervisor";
	$attributes["staff.project_design_supervisor"] = "calculated_content";
	$datatypes["staff.project_design_supervisor"] = "text";
	$calculated_content["staff.project_design_supervisor"] = "select concat(user_name, ' ', user_firstname) from users where user_id = ";
	$calculated_content_key["staff.project_design_supervisor"] = "project_design_supervisor";
	$calculated_content_field["staff.project_design_supervisor"] = "project_design_supervisor";
	$calculated_content_sort_order["staff.project_design_supervisor"] = "";

	/*
	$fields["staff.project_cms_approver"] = "CMS Approver";
	$attributes["staff.project_cms_approver"] = "calculated_content";
	$datatypes["staff.project_cms_approver"] = "text";
	$calculated_content["staff.project_cms_approver"] = "select concat(user_name, ' ', user_firstname) from users where user_id = ";
	$calculated_content_key["staff.project_cms_approver"] = "project_cms_approver";
	$calculated_content_field["staff.project_cms_approver"] = "project_cms_approver";
	$calculated_content_sort_order["staff.project_cms_approver"] = "";

	$fields["staff.order_retail_operator"] = "Logistics Coordinator";
	$attributes["staff.order_retail_operator"] = "calculated_content";
	$datatypes["staff.order_retail_operator"] = "text";
	$calculated_content["staff.order_retail_operator"] = "select concat(user_name, ' ', user_firstname) from users where user_id = ";
	$calculated_content_key["staff.order_retail_operator"] = "order_retail_operator";
	$calculated_content_field["staff.order_retail_operator"] = "order_retail_operator";
	$calculated_content_sort_order["staff.order_retail_operator"] = "";

	$fields["staff.order_delivery_confirmation_by"] = "Delivery Confirmaton by";
	$attributes["staff.order_delivery_confirmation_by"] = "calculated_content";
	$datatypes["staff.order_delivery_confirmation_by"] = "text";
	$calculated_content["staff.order_delivery_confirmation_by"] = "select concat(user_name, ' ', user_firstname) from users where user_id = ";
	$calculated_content_key["staff.order_delivery_confirmation_by"] = "order_delivery_confirmation_by";
	$calculated_content_field["staff.order_delivery_confirmation_by"] = "order_delivery_confirmation_by";
	$calculated_content_sort_order["staff.order_delivery_confirmation_by"] = "";
	*/

	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["calculated_content_sort_order"] = $calculated_content_sort_order;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;
	$db_info["group_fields"] = $group_fields;
	$db_info["function_fields"] = $function_fields;

	return $db_info;
}


/********************************************************************
    get DB Info for budget fields
*********************************************************************/
function query_budget_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_sort_order = array();
	$content_by_function = array();
	$content_by_function_params = array();
	$group_fields = array();
	$function_fields = array();

	$fields["budget.approved_budget_chf"] = "Approved Budget CHF";
	$attributes["budget.approved_budget_chf"] = "content_by_function";
	$datatypes["budget.approved_budget_chf"] = "decimal2";
	$content_by_function["budget.approved_budget_chf"] = "get_approved_budget_chf(params)";
	$content_by_function_params["budget.approved_budget_chf"] = array("project_id");
	$function_fields[] = "budget.approved_budget_chf";
	
	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["calculated_content_sort_order"] = $calculated_content_sort_order;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;
	$db_info["group_fields"] = $group_fields;
	$db_info["function_fields"] = $function_fields;

	return $db_info;
}



/********************************************************************
    get DB Info for investment fields
*********************************************************************/
function query_investment_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_sort_order = array();
	$content_by_function = array();
	$content_by_function_params = array();
	$group_fields = array();
	$function_fields = array();

	
	//investment amounts
	$fields["investments.requested_amount_chf"] = "Requested Amount Investments CHF";
	$attributes["investments.requested_amount_chf"] = "content_by_function";
	$datatypes["investments.requested_amount_chf"] = "decimal2";
	$content_by_function["investments.requested_amount_chf"] = "get_investment_amount_chf(params)";
	$content_by_function_params["investments.requested_amount_chf"] = array("project_id", 1, 1);
	$function_fields[] = "investments.requested_amount_chf";

	$fields["investments.requested_amount_constuction_chf"] = "---- Requested Amount Local Construction CHF";
	$attributes["investments.requested_amount_constuction_chf"] = "content_by_function";
	$datatypes["investments.requested_amount_constuction_chf"] = "decimal2";
	$content_by_function["investments.requested_amount_constuction_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["investments.requested_amount_constuction_chf"] = array("project_id", 1, 0 , 1);
	$function_fields[] = "investments.requested_amount_constuction_chf";

	$fields["investments.requested_amount_fixturing_chf"] = "---- Requested Amount Store fixturing / Furniture CHF";
	$attributes["investments.requested_amount_fixturing_chf"] = "content_by_function";
	$datatypes["investments.requested_amount_fixturing_chf"] = "decimal2";
	$content_by_function["investments.requested_amount_fixturing_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["investments.requested_amount_fixturing_chf"] = array("project_id", 3, 0 , 1);
	$function_fields[] = "investments.requested_amount_fixturing_chf";

	$fields["investments.requested_amount_architectural_chf"] = "---- Requested Amount Architectural services CHF";
	$attributes["investments.requested_amount_architectural_chf"] = "content_by_function";
	$datatypes["investments.requested_amount_architectural_chf"] = "decimal2";
	$content_by_function["investments.requested_amount_architectural_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["investments.requested_amount_architectural_chf"] = array("project_id", 5, 0 , 1);
	$function_fields[] = "investments.requested_amount_architectural_chf";
	
	$fields["investments.requested_amount_equipment_chf"] = "---- Requested Amount Equipment CHF";
	$attributes["investments.requested_amount_equipment_chf"] = "content_by_function";
	$datatypes["investments.requested_amount_equipment_chf"] = "decimal2";
	$content_by_function["investments.requested_amount_equipment_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["investments.requested_amount_equipment_chf"] = array("project_id", 7, 0 , 1);
	$function_fields[] = "investments.requested_amount_equipment_chf";


	$fields["investments.requested_amount_other_chf"] = "---- Requested Amount Other CHF";
	$attributes["investments.requested_amount_other_chf"] = "content_by_function";
	$datatypes["investments.requested_amount_other_chf"] = "decimal2";
	$content_by_function["investments.requested_amount_other_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["investments.requested_amount_other_chf"] = array("project_id", 11, 0 , 1);
	$function_fields[] = "investments.requested_amount_other_chf";


	
	//investment additional amounts
	$fields["investments.requested_additional_chf"] = "Requested Additional Amount Investments CHF";
	$attributes["investments.requested_additional_chf"] = "content_by_function";
	$datatypes["investments.requested_additional_chf"] = "decimal2";
	$content_by_function["investments.requested_additional_chf"] = "get_investment_additional_chf(params)";
	$content_by_function_params["investments.requested_additional_chf"] = array("project_id", 1, 1);
	$function_fields[] = "investments.requested_additional_chf";

	$fields["investments.requested_additional_constuction_chf"] = "---- Requested Additional Amount Local Construction CHF";
	$attributes["investments.requested_additional_constuction_chf"] = "content_by_function";
	$datatypes["investments.requested_additional_constuction_chf"] = "decimal2";
	$content_by_function["investments.requested_additional_constuction_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["investments.requested_additional_constuction_chf"] = array("project_id", 1, 0 , 2);
	$function_fields[] = "investments.requested_additional_constuction_chf";

	$fields["investments.requested_additional_fixturing_chf"] = "---- Requested Additional Amount Store fixturing / Furniture CHF";
	$attributes["investments.requested_additional_fixturing_chf"] = "content_by_function";
	$datatypes["investments.requested_additional_fixturing_chf"] = "decimal2";
	$content_by_function["investments.requested_additional_fixturing_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["investments.requested_additional_fixturing_chf"] = array("project_id", 3, 0 , 2);
	$function_fields[] = "investments.requested_additional_fixturing_chf";

	$fields["investments.requested_additional_architectural_chf"] = "---- Requested Additional Amount Architectural services CHF";
	$attributes["investments.requested_additional_architectural_chf"] = "content_by_function";
	$datatypes["investments.requested_additional_architectural_chf"] = "decimal2";
	$content_by_function["investments.requested_additional_architectural_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["investments.requested_additional_architectural_chf"] = array("project_id", 5, 0 , 2);
	$function_fields[] = "investments.requested_additional_architectural_chf";

	$fields["investments.requested_additional_equipment_chf"] = "---- Requested Additional Amount Equipment CHF";
	$attributes["investments.requested_additional_equipment_chf"] = "content_by_function";
	$datatypes["investments.requested_additional_equipment_chf"] = "decimal2";
	$content_by_function["investments.requested_additional_equipment_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["investments.requested_additional_equipment_chf"] = array("project_id", 7, 0 , 2);
	$function_fields[] = "investments.requested_additional_equipment_chf";


	$fields["investments.requested_additional_other_chf"] = "---- Requested Additional Amount Other CHF";
	$attributes["investments.requested_additional_other_chf"] = "content_by_function";
	$datatypes["investments.requested_additional_other_chf"] = "decimal2";
	$content_by_function["investments.requested_additional_other_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["investments.requested_additional_other_chf"] = array("project_id", 11, 0 , 2);
	$function_fields[] = "investments.requested_additional_other_chf";
	
	
	//Intagibles amounts
	$fields["investments.requested_amount2_chf"] = "Requested Amount Intagibles CHF";
	$attributes["investments.requested_amount2_chf"] = "content_by_function";
	$datatypes["investments.requested_amount2_chf"] = "decimal2";
	$content_by_function["investments.requested_amount2_chf"] = "get_investment_amount_chf(params)";
	$content_by_function_params["investments.requested_amount2_chf"] = array("project_id", 2, 1);
	$function_fields[] = "investments.requested_amount2_chf";

	$fields["investments.requested_amount_key_mony_chf"] = "---- Requested Amount Keymoney/Godwill CHF";
	$attributes["investments.requested_amount_key_mony_chf"] = "content_by_function";
	$datatypes["investments.requested_amount_key_mony_chf"] = "decimal2";
	$content_by_function["investments.requested_amount_key_mony_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["investments.requested_amount_key_mony_chf"] = array("project_id", 15, 17, 1);
	$function_fields[] = "investments.requested_amount_key_mony_chf";

	$fields["investments.requested_amount_deposit_chf"] = "---- Requested Amount Deposit CHF";
	$attributes["investments.requested_amount_deposit_chf"] = "content_by_function";
	$datatypes["investments.requested_amount_deposit_chf"] = "decimal2";
	$content_by_function["investments.requested_amount_deposit_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["investments.requested_amount_deposit_chf"] = array("project_id", 9, 0 , 1);
	$function_fields[] = "investments.requested_amount_deposit_chf";

	$fields["investments.requested_amount_noncapitalized_chf"] = "---- Requested Amount Other non-capitalized costs";
	$attributes["investments.requested_amount_noncapitalized_chf"] = "content_by_function";
	$datatypes["investments.requested_amount_noncapitalized_chf"] = "decimal2";
	$content_by_function["investments.requested_amount_noncapitalized_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["investments.requested_amount_noncapitalized_chf"] = array("project_id", 13, 0 , 1);
	$function_fields[] = "investments.requested_amount_noncapitalized_chf";

	
	//Intagibles additional amounts
	$fields["investments.requested_additional2_chf"] = "Requested Additional Amount Intagibles CHF";
	$attributes["investments.requested_additional2_chf"] = "content_by_function";
	$datatypes["investments.requested_additional2_chf"] = "decimal2";
	$content_by_function["investments.requested_additional2_chf"] = "get_investment_additional_chf(params)";
	$content_by_function_params["investments.requested_additional2_chf"] = array("project_id", 2, 1);
	$function_fields[] = "investments.requested_additional2_chf";

	$fields["investments.requested_additional_key_mony_chf"] = "---- Requested Additional Amount Keymoney/Godwill CHF";
	$attributes["investments.requested_additional_key_mony_chf"] = "content_by_function";
	$datatypes["investments.requested_additional_key_mony_chf"] = "decimal2";
	$content_by_function["investments.requested_additional_key_mony_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["investments.requested_additional_key_mony_chf"] = array("project_id", 15, 17, 2);

	$fields["investments.requested_additional_deposit_chf"] = "---- Requested Additional Amount Deposit CHF";
	$attributes["investments.requested_additional_deposit_chf"] = "content_by_function";
	$datatypes["investments.requested_additional_deposit_chf"] = "decimal2";
	$content_by_function["investments.requested_additional_deposit_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["investments.requested_additional_deposit_chf"] = array("project_id", 9, 0 , 2);

	$fields["investments.requested_additional_noncapitalized_chf"] = "---- Requested Additional Amount Other non-capitalized costs";
	$attributes["investments.requested_additional_noncapitalized_chf"] = "content_by_function";
	$datatypes["investments.requested_additional_noncapitalized_chf"] = "decimal2";
	$content_by_function["investments.requested_additional_noncapitalized_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["investments.requested_additional_noncapitalized_chf"] = array("project_id", 13, 0 , 2);

	
	//Investment total amounts
	$fields["investments.total_requested_amount_chf"] = "Total Requested Amount Investments CHF";
	$attributes["investments.total_requested_amount_chf"] = "content_by_function";
	$datatypes["investments.total_requested_amount_chf"] = "decimal2";
	$content_by_function["investments.total_requested_amount_chf"] = "get_total_investment_amount_chf(params)";
	$content_by_function_params["investments.total_requested_amount_chf"] = array("project_id", 1, 1);

	$fields["investments.total_requested_amount_constuction_chf"] = "---- Total Requested Amount Local Construction CHF";
	$attributes["investments.total_requested_amount_constuction_chf"] = "content_by_function";
	$datatypes["investments.total_requested_amount_constuction_chf"] = "decimal2";
	$content_by_function["investments.total_requested_amount_constuction_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["investments.total_requested_amount_constuction_chf"] = array("project_id", 1, 0 , 3);
	$function_fields[] = "investments.total_requested_amount_constuction_chf";

	$fields["investments.total_requested_amount_fixturing_chf"] = "---- Total Requested Amount Store fixturing / Furniture CHF";
	$attributes["investments.total_requested_amount_fixturing_chf"] = "content_by_function";
	$datatypes["investments.total_requested_amount_fixturing_chf"] = "decimal2";
	$content_by_function["investments.total_requested_amount_fixturing_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["investments.total_requested_amount_fixturing_chf"] = array("project_id", 3, 0 , 3);
	$function_fields[] = "investments.total_requested_amount_fixturing_chf";

	$fields["investments.total_requested_amount_architectural_chf"] = "---- Total Requested Amount Architectural services CHF";
	$attributes["investments.total_requested_amount_architectural_chf"] = "content_by_function";
	$datatypes["investments.total_requested_amount_architectural_chf"] = "decimal2";
	$content_by_function["investments.total_requested_amount_architectural_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["investments.total_requested_amount_architectural_chf"] = array("project_id", 5, 0 , 3);
	$function_fields[] = "investments.total_requested_amount_architectural_chf";


	$fields["investments.total_requested_amount_equipment_chf"] = "---- Total Requested Amount Equipment CHF";
	$attributes["investments.total_requested_amount_equipment_chf"] = "content_by_function";
	$datatypes["investments.total_requested_amount_equipment_chf"] = "decimal2";
	$content_by_function["investments.total_requested_amount_equipment_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["investments.total_requested_amount_equipment_chf"] = array("project_id", 7, 0 , 3);
	$function_fields[] = "investments.total_requested_amount_equipment_chf";

	$fields["investments.total_requested_amount_other_chf"] = "---- Total Requested Amount Other CHF";
	$attributes["investments.total_requested_amount_other_chf"] = "content_by_function";
	$datatypes["investments.total_requested_amount_other_chf"] = "decimal2";
	$content_by_function["investments.total_requested_amount_other_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["investments.total_requested_amount_other_chf"] = array("project_id", 11, 0 , 3);
	$function_fields[] = "investments.total_requested_amount_other_chf";
	
	
	//Intagibles total amounts
	$fields["investments.total_requested_amount2_chf"] = "Total Requested Amount Intagibles CHF";
	$attributes["investments.total_requested_amount2_chf"] = "content_by_function";
	$datatypes["investments.total_requested_amount2_chf"] = "decimal2";
	$content_by_function["investments.total_requested_amount2_chf"] = "get_total_investment_amount_chf(params)";
	$content_by_function_params["investments.total_requested_amount2_chf"] = array("project_id", 2, 1);
	$function_fields[] = "investments.total_requested_amount2_chf";

	$fields["investments.total_requested_amount_key_mony_chf"] = "---- Total Requested Amount Keymoney/Godwill CHF";
	$attributes["investments.total_requested_amount_key_mony_chf"] = "content_by_function";
	$datatypes["investments.total_requested_amount_key_mony_chf"] = "decimal2";
	$content_by_function["investments.total_requested_amount_key_mony_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["investments.total_requested_amount_key_mony_chf"] = array("project_id", 15, 17, 3);
	$function_fields[] = "investments.total_requested_amount_key_mony_chf";

	$fields["investments.total_requested_amount_deposit_chf"] = "---- Total Requested Amount Deposit CHF";
	$attributes["investments.total_requested_amount_deposit_chf"] = "content_by_function";
	$datatypes["investments.total_requested_amount_deposit_chf"] = "decimal2";
	$content_by_function["investments.total_requested_amount_deposit_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["investments.total_requested_amount_deposit_chf"] = array("project_id", 9, 0 , 3);
	$function_fields[] = "investments.total_requested_amount_deposit_chf";

	$fields["investments.total_requested_amount_noncapitalized_chf"] = "---- Total Requested Amount Other non-capitalized costs";
	$attributes["investments.total_requested_amount_noncapitalized_chf"] = "content_by_function";
	$datatypes["investments.total_requested_amount_noncapitalized_chf"] = "decimal2";
	$content_by_function["investments.total_requested_amount_noncapitalized_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["investments.total_requested_amount_noncapitalized_chf"] = array("project_id", 13, 0 , 3);
	$function_fields[] = "investments.total_requested_amount_noncapitalized_chf";


	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["calculated_content_sort_order"] = $calculated_content_sort_order;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;
	$db_info["group_fields"] = $group_fields;
	$db_info["function_fields"] = $function_fields;

	return $db_info;
}



/********************************************************************
    get DB Info for approved investment fields
*********************************************************************/
function query_approved_investments_fields()
{

	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_sort_order = array();
	$content_by_function = array();
	$content_by_function_params = array();
	$group_fields = array();
	$function_fields = array();


	//investment amounts
	$fields["approved_investments.approved_amount_chf"] = "Approved Amount Investments CHF";
	$attributes["approved_investments.approved_amount_chf"] = "content_by_function";
	$datatypes["approved_investments.approved_amount_chf"] = "decimal2";
	$content_by_function["approved_investments.approved_amount_chf"] = "get_investment_amount_chf(params)";
	$content_by_function_params["approved_investments.approved_amount_chf"] = array("project_id", 1, 2);
	$function_fields[] = "approved_investments.approved_amount_chf";


	$fields["approved_investments.approved_amount_constuction_chf"] = "---- Approved Amount Local Construction CHF";
	$attributes["approved_investments.approved_amount_constuction_chf"] = "content_by_function";
	$datatypes["approved_investments.approved_amount_constuction_chf"] = "decimal2";
	$content_by_function["approved_investments.approved_amount_constuction_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["approved_investments.approved_amount_constuction_chf"] = array("project_id", 1, 0 , 4);
	$function_fields[] = "approved_investments.approved_amount_constuction_chf";

	$fields["approved_investments.approved_amount_fixturing_chf"] = "---- Approved Amount Store fixturing / Furniture CHF";
	$attributes["approved_investments.approved_amount_fixturing_chf"] = "content_by_function";
	$datatypes["approved_investments.approved_amount_fixturing_chf"] = "decimal2";
	$content_by_function["approved_investments.approved_amount_fixturing_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["approved_investments.approved_amount_fixturing_chf"] = array("project_id", 3, 0 , 4);
	$function_fields[] = "approved_investments.approved_amount_fixturing_chf";

	$fields["approved_investments.approved_amount_architectural_chf"] = "---- Approved Amount Architectural services CHF";
	$attributes["approved_investments.approved_amount_architectural_chf"] = "content_by_function";
	$datatypes["approved_investments.approved_amount_architectural_chf"] = "decimal2";
	$content_by_function["approved_investments.approved_amount_architectural_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["approved_investments.approved_amount_architectural_chf"] = array("project_id", 5, 0 , 4);
	$function_fields[] = "approved_investments.approved_amount_architectural_chf";

	$fields["approved_investments.approved_amount_equipment_chf"] = "---- Approved Amount Equipment CHF";
	$attributes["approved_investments.approved_amount_equipment_chf"] = "content_by_function";
	$datatypes["approved_investments.approved_amount_equipment_chf"] = "decimal2";
	$content_by_function["approved_investments.approved_amount_equipment_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["approved_investments.approved_amount_equipment_chf"] = array("project_id", 7, 0 , 4);
	$function_fields[] = "approved_investments.approved_amount_equipment_chf";

	$fields["approved_investments.approved_amount_other_chf"] = "---- Approved Amount Other CHF";
	$attributes["approved_investments.approved_amount_other_chf"] = "content_by_function";
	$datatypes["approved_investments.approved_amount_other_chf"] = "decimal2";
	$content_by_function["approved_investments.approved_amount_other_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["approved_investments.approved_amount_other_chf"] = array("project_id", 11, 0 , 4);
	$function_fields[] = "approved_investments.approved_amount_other_chf";

	
	//Investment additional amounts
	$fields["approved_investments.approved_additional_chf"] = "Approved Additional Amount Investments CHF";
	$attributes["approved_investments.approved_additional_chf"] = "content_by_function";
	$datatypes["approved_investments.approved_additional_chf"] = "decimal2";
	$content_by_function["approved_investments.approved_additional_chf"] = "get_investment_additional_chf(params)";
	$content_by_function_params["approved_investments.approved_additional_chf"] = array("project_id", 1, 2);
	$function_fields[] = "approved_investments.approved_additional_chf";

	$fields["approved_investments.approved_additional_constuction_chf"] = "---- Approved Additional Amount Local Construction CHF";
	$attributes["approved_investments.approved_additional_constuction_chf"] = "content_by_function";
	$datatypes["approved_investments.approved_additional_constuction_chf"] = "decimal2";
	$content_by_function["approved_investments.approved_additional_constuction_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["approved_investments.approved_additional_constuction_chf"] = array("project_id", 1, 0 , 5);
	$function_fields[] = "approved_investments.approved_additional_constuction_chf";

	$fields["approved_investments.approved_additional_fixturing_chf"] = "---- Approved Additional Amount Store fixturing / Furniture CHF";
	$attributes["approved_investments.approved_additional_fixturing_chf"] = "content_by_function";
	$datatypes["approved_investments.approved_additional_fixturing_chf"] = "decimal2";
	$content_by_function["approved_investments.approved_additional_fixturing_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["approved_investments.approved_additional_fixturing_chf"] = array("project_id", 3, 0 , 5);
	$function_fields[] = "approved_investments.approved_additional_fixturing_chf";

	$fields["approved_investments.approved_additional_architectural_chf"] = "---- Approved Additional Amount Architectural services CHF";
	$attributes["approved_investments.approved_additional_architectural_chf"] = "content_by_function";
	$datatypes["approved_investments.approved_additional_architectural_chf"] = "decimal2";
	$content_by_function["approved_investments.approved_additional_architectural_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["approved_investments.approved_additional_architectural_chf"] = array("project_id", 5, 0 , 5);
	$function_fields[] = "approved_investments.approved_additional_architectural_chf";

	$fields["approved_investments.approved_additional_equipment_chf"] = "---- Approved Additional Amount Equipment CHF";
	$attributes["approved_investments.approved_additional_equipment_chf"] = "content_by_function";
	$datatypes["approved_investments.approved_additional_equipment_chf"] = "decimal2";
	$content_by_function["approved_investments.approved_additional_equipment_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["approved_investments.approved_additional_equipment_chf"] = array("project_id", 7, 0 , 5);
	$function_fields[] = "approved_investments.approved_additional_equipment_chf";
	

	$fields["approved_investments.approved_additional_other_chf"] = "---- Approved Additional Amount Other CHF";
	$attributes["approved_investments.approved_additional_other_chf"] = "content_by_function";
	$datatypes["approved_investments.approved_additional_other_chf"] = "decimal2";
	$content_by_function["approved_investments.approved_additional_other_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["approved_investments.approved_additional_other_chf"] = array("project_id", 11, 0 , 5);
	$function_fields[] = "approved_investments.approved_additional_other_chf";
	

	//intangible amounts
	$fields["approved_investments.approved_amount2_chf"] = "Approved Amount Intagibles CHF";
	$attributes["approved_investments.approved_amount2_chf"] = "content_by_function";
	$datatypes["approved_investments.approved_amount2_chf"] = "decimal2";
	$content_by_function["approved_investments.approved_amount2_chf"] = "get_investment_amount_chf(params)";
	$content_by_function_params["approved_investments.approved_amount2_chf"] = array("project_id", 2, 2);
	$function_fields[] = "approved_investments.approved_amount2_chf";

	$fields["approved_investments.approved_amount_key_mony_chf"] = "---- Approved Amount Keymoney/Godwill CHF";
	$attributes["approved_investments.approved_amount_key_mony_chf"] = "content_by_function";
	$datatypes["approved_investments.approved_amount_key_mony_chf"] = "decimal2";
	$content_by_function["approved_investments.approved_amount_key_mony_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["approved_investments.approved_amount_key_mony_chf"] = array("project_id", 15, 17, 4);
	$function_fields[] = "approved_investments.approved_amount_key_mony_chf";

	$fields["approved_investments.approved_amount_deposit_chf"] = "---- Approved Amount Deposit CHF";
	$attributes["approved_investments.approved_amount_deposit_chf"] = "content_by_function";
	$datatypes["approved_investments.approved_amount_deposit_chf"] = "decimal2";
	$content_by_function["approved_investments.approved_amount_deposit_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["approved_investments.approved_amount_deposit_chf"] = array("project_id", 9, 0 , 4);
	$function_fields[] = "approved_investments.approved_amount_deposit_chf";

	$fields["approved_investments.approved_amount_noncapitalized_chf"] = "---- Approved Amount Other non-capitalized costs";
	$attributes["approved_investments.approved_amount_noncapitalized_chf"] = "content_by_function";
	$datatypes["approved_investments.approved_amount_noncapitalized_chf"] = "decimal2";
	$content_by_function["approved_investments.approved_amount_noncapitalized_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["approved_investments.approved_amount_noncapitalized_chf"] = array("project_id", 13, 0 , 4);
	$function_fields[] = "approved_investments.approved_amount_noncapitalized_chf";


	//intangible additional amounts
	$fields["approved_investments.approved_additional2_chf"] = "Approved Additional Amount Intagibles CHF";
	$attributes["approved_investments.approved_additional2_chf"] = "content_by_function";
	$datatypes["approved_investments.approved_additional2_chf"] = "decimal2";
	$content_by_function["approved_investments.approved_additional2_chf"] = "get_investment_additional_chf(params)";
	$content_by_function_params["approved_investments.approved_additional2_chf"] = array("project_id", 2, 2);
	$function_fields[] = "approved_investments.approved_additional2_chf";

	$fields["approved_investments.approved_additional_key_mony_chf"] = "---- Approved Additional Amount Keymoney/Godwill CHF";
	$attributes["approved_investments.approved_additional_key_mony_chf"] = "content_by_function";
	$datatypes["approved_investments.approved_additional_key_mony_chf"] = "decimal2";
	$content_by_function["approved_investments.approved_additional_key_mony_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["approved_investments.approved_additional_key_mony_chf"] = array("project_id", 15, 17, 5);
	$function_fields[] = "approved_investments.approved_additional_key_mony_chf";


	$fields["approved_investments.approved_additional_deposit_chf"] = "---- Approved Additional Amount Deposit CHF";
	$attributes["approved_investments.approved_additional_deposit_chf"] = "content_by_function";
	$datatypes["approved_investments.approved_additional_deposit_chf"] = "decimal2";
	$content_by_function["approved_investments.approved_additional_deposit_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["approved_investments.approved_additional_deposit_chf"] = array("project_id", 9, 0 , 5);
	$function_fields[] = "approved_investments.approved_additional_deposit_chf";

	$fields["approved_investments.approved_additional_noncapitalized_chf"] = "---- Approved Additional Amount Other non-capitalized costs";
	$attributes["approved_investments.approved_additional_noncapitalized_chf"] = "content_by_function";
	$datatypes["approved_investments.approved_additional_noncapitalized_chf"] = "decimal2";
	$content_by_function["approved_investments.approved_additional_noncapitalized_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["approved_investments.approved_additional_noncapitalized_chf"] = array("project_id", 13, 0 , 5);
	$function_fields[] = "approved_investments.approved_additional_noncapitalized_chf";

	
	//total investment amounts
	$fields["approved_investments.total_approved_amount_chf"] = "Total Approved Amount Investments CHF";
	$attributes["approved_investments.total_approved_amount_chf"] = "content_by_function";
	$datatypes["approved_investments.total_approved_amount_chf"] = "decimal2";
	$content_by_function["approved_investments.total_approved_amount_chf"] = "get_total_investment_amount_chf(params)";
	$content_by_function_params["approved_investments.total_approved_amount_chf"] = array("project_id", 1, 2);
	$function_fields[] = "approved_investments.total_approved_amount_chf";

	$fields["approved_investments.total_approved_amount_constuction_chf"] = "---- Total Approved Amount Local Construction CHF";
	$attributes["approved_investments.total_approved_amount_constuction_chf"] = "content_by_function";
	$datatypes["approved_investments.total_approved_amount_constuction_chf"] = "decimal2";
	$content_by_function["approved_investments.total_approved_amount_constuction_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["approved_investments.total_approved_amount_constuction_chf"] = array("project_id", 1, 0 , 6);
	$function_fields[] = "approved_investments.total_approved_amount_constuction_chf";

	$fields["approved_investments.total_approved_amount_fixturing_chf"] = "---- Total Approved Amount Store fixturing / Furniture CHF";
	$attributes["approved_investments.total_approved_amount_fixturing_chf"] = "content_by_function";
	$datatypes["approved_investments.total_approved_amount_fixturing_chf"] = "decimal2";
	$content_by_function["approved_investments.total_approved_amount_fixturing_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["approved_investments.total_approved_amount_fixturing_chf"] = array("project_id", 3, 0 , 6);
	$function_fields[] = "approved_investments.total_approved_amount_fixturing_chf";

	$fields["approved_investments.total_approved_amount_architectural_chf"] = "---- Total Approved Amount Architectural services CHF";
	$attributes["approved_investments.total_approved_amount_architectural_chf"] = "content_by_function";
	$datatypes["approved_investments.total_approved_amount_architectural_chf"] = "decimal2";
	$content_by_function["approved_investments.total_approved_amount_architectural_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["approved_investments.total_approved_amount_architectural_chf"] = array("project_id", 5, 0 , 6);
	$function_fields[] = "approved_investments.total_approved_amount_architectural_chf";

	$fields["approved_investments.total_approved_amount_equipment_chf"] = "---- Total Approved Amount Equipment CHF";
	$attributes["approved_investments.total_approved_amount_equipment_chf"] = "content_by_function";
	$datatypes["approved_investments.total_approved_amount_equipment_chf"] = "decimal2";
	$content_by_function["approved_investments.total_approved_amount_equipment_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["approved_investments.total_approved_amount_equipment_chf"] = array("project_id", 7, 0 , 6);
	$function_fields[] = "approved_investments.total_approved_amount_equipment_chf";

	$fields["approved_investments.total_approved_amount_other_chf"] = "---- Total Approved Amount Other CHF";
	$attributes["approved_investments.total_approved_amount_other_chf"] = "content_by_function";
	$datatypes["approved_investments.total_approved_amount_other_chf"] = "decimal2";
	$content_by_function["approved_investments.total_approved_amount_other_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["approved_investments.total_approved_amount_other_chf"] = array("project_id", 11, 0 , 6);
	$function_fields[] = "approved_investments.total_approved_amount_other_chf";

	
	//total intangible amounts
	$fields["approved_investments.total_approved_amount2_chf"] = "Total Approved Amount Intagibles CHF";
	$attributes["approved_investments.total_approved_amount2_chf"] = "content_by_function";
	$datatypes["approved_investments.total_approved_amount2_chf"] = "decimal2";
	$content_by_function["approved_investments.total_approved_amount2_chf"] = "get_total_investment_amount_chf(params)";
	$content_by_function_params["approved_investments.total_approved_amount2_chf"] = array("project_id", 2, 2);
	$function_fields[] = "approved_investments.total_approved_amount2_chf";

	$fields["approved_investments.total_approved_amount_key_mony_chf"] = "---- Total Approved Amount Keymoney/Godwill CHF";
	$attributes["approved_investments.total_approved_amount_key_mony_chf"] = "content_by_function";
	$datatypes["approved_investments.total_approved_amount_key_mony_chf"] = "decimal2";
	$content_by_function["approved_investments.total_approved_amount_key_mony_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["approved_investments.total_approved_amount_key_mony_chf"] = array("project_id", 15, 17, 6);
	$function_fields[] = "approved_investments.total_approved_amount_key_mony_chf";

	$fields["approved_investments.total_approved_amount_deposit_chf"] = "---- Total Approved Amount Deposit CHF";
	$attributes["approved_investments.total_approved_amount_deposit_chf"] = "content_by_function";
	$datatypes["approved_investments.total_approved_amount_deposit_chf"] = "decimal2";
	$content_by_function["approved_investments.total_approved_amount_deposit_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["approved_investments.total_approved_amount_deposit_chf"] = array("project_id", 9, 0 , 6);
	$function_fields[] = "approved_investments.total_approved_amount_deposit_chf";

	$fields["approved_investments.total_approved_amount_noncapitalized_chf"] = "---- Total Approved Amount Other non-capitalized costs";
	$attributes["approved_investments.total_approved_amount_noncapitalized_chf"] = "content_by_function";
	$datatypes["approved_investments.total_approved_amount_noncapitalized_chf"] = "decimal2";
	$content_by_function["approved_investments.total_approved_amount_noncapitalized_chf"] = "get_investment_amount_per_group_chf(params)";
	$content_by_function_params["approved_investments.total_approved_amount_noncapitalized_chf"] = array("project_id", 13, 0 , 6);
	$function_fields[] = "approved_investments.total_approved_amount_noncapitalized_chf";
	
	
	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["calculated_content_sort_order"] = $calculated_content_sort_order;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;
	$db_info["group_fields"] = $group_fields;
	$db_info["function_fields"] = $function_fields;

	return $db_info;
	
}

/********************************************************************
    get DB Info for distribution channel fields
*********************************************************************/
function query_real_costs_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_sort_order = array();
	$content_by_function = array();
	$content_by_function_params = array();
	$group_fields = array();
	$function_fields = array();
	
	
	//total real cost
	$fields["real_costs.real_cost_chf"] = "Totel Real Cost CHF";
	$attributes["real_costs.real_cost_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_chf"] = "get_real_cost_chf(params)";
	$content_by_function_params["real_costs.real_cost_chf"] = array("project_id");
	$function_fields[] = "real_costs.real_cost_chf";

	$fields["real_costs.real_cost_per_sqms_gross_surface_chf"] = "---- Total Real Cost per sqms gross surface CHF";
	$attributes["real_costs.real_cost_per_sqms_gross_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_per_sqms_gross_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_per_sqms_gross_surface_chf"] = "get_cost_per_sqm(params)";
	$content_by_function_params["real_costs.real_cost_per_sqms_gross_surface_chf"] = array("project_id", "order_id", 1);
	$function_fields[] = "real_costs.real_cost_per_sqms_gross_surface_chf";

	$fields["real_costs.real_cost_per_sqms_total_surface_chf"] = "---- Total Real Cost per sqms total surface CHF";
	$attributes["real_costs.real_cost_per_sqms_total_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_per_sqms_total_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_per_sqms_total_surface_chf"] = "get_cost_per_sqm(params)";
	$content_by_function_params["real_costs.real_cost_per_sqms_total_surface_chf"] = array("project_id", "order_id", 2);
	$function_fields[] = "real_costs.real_cost_per_sqms_total_surface_chf";

	$fields["real_costs.real_cost_per_sqms_sales_surface_chf"] = "---- Total Real Cost per sqms sales surface CHF";
	$attributes["real_costs.real_cost_per_sqms_sales_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_per_sqms_sales_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_per_sqms_sales_surface_chf"] = "get_cost_per_sqm(params)";
	$content_by_function_params["real_costs.real_cost_per_sqms_sales_surface_chf"] = array("project_id", "order_id", 3);
	$function_fields[] = "real_costs.real_cost_per_sqms_sales_surface_chf";

	
	//sum of HQ supplied items and freight charges and local cost	
	$fields["real_costs.real_cost01_chf"] = "Real Cost of Fixturing HQ Supplied Items, Freight Charges and Local Construction CHF";
	$attributes["real_costs.real_cost01_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost01_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost01_chf"] = "get_cost_per_group(params)";
	$content_by_function_params["real_costs.real_cost01_chf"] = array("project_id", 2, 6, 7);
	$function_fields[] = "real_costs.real_cost01_chf";

	$fields["real_costs.real_cost01_per_sqms_gross_surface_chf"] = "---- Real Cost of Fixturing HQ Supplied Items, Freight Charges and Local Construction per sqms gross surface CHF";
	$attributes["real_costs.real_cost01_per_sqms_gross_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost01_per_sqms_gross_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost01_per_sqms_gross_surface_chf"] = "get_cost01_per_sqm(params)";
	$content_by_function_params["real_costs.real_cost01_per_sqms_gross_surface_chf"] = array("project_id", "order_id", 1);
	$function_fields[] = "real_costs.real_cost01_per_sqms_gross_surface_chf";

	$fields["real_costs.real_cost01_per_sqms_total_surface_chf"] = "---- Real Cost of Fixturing HQ Supplied Items, Freight Charges and Local Construction per sqms total surface CHF";
	$attributes["real_costs.real_cost01_per_sqms_total_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost01_per_sqms_total_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost01_per_sqms_total_surface_chf"] = "get_cost01_per_sqm(params)";
	$content_by_function_params["real_costs.real_cost01_per_sqms_total_surface_chf"] = array("project_id", "order_id", 2);
	$function_fields[] = "real_costs.real_cost01_per_sqms_total_surface_chf";

	$fields["real_costs.real_cost01_per_sqms_sales_surface_chf"] = "---- Real Cost of Fixturing HQ Supplied Items, Freight Charges and Local Construction per sqms sales surface CHF";
	$attributes["real_costs.real_cost01_per_sqms_sales_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost01_per_sqms_sales_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost01_per_sqms_sales_surface_chf"] = "get_cost01_per_sqm(params)";
	$content_by_function_params["real_costs.real_cost01_per_sqms_sales_surface_chf"] = array("project_id", "order_id", 3);
	$function_fields[] = "real_costs.real_cost01_per_sqms_sales_surface_chf";


	// sum of HQ Supplied items and freight charges
	$fields["real_costs.real_cost02_chf"] = "Real Cost of Fixturing HQ Supplied Items and Freight Charges CHF";
	$attributes["real_costs.real_cost02_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost02_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost02_chf"] = "get_cost_per_group(params)";
	$content_by_function_params["real_costs.real_cost02_chf"] = array("project_id", 2, 6);
	$function_fields[] = "real_costs.real_cost02_chf";

	$fields["real_costs.real_cost02_per_sqms_gross_surface_chf"] = "---- Real Cost of Fixturing HQ Supplied Items and Freight Charges per sqms gross surface CHF";
	$attributes["real_costs.real_cost02_per_sqms_gross_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost02_per_sqms_gross_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost02_per_sqms_gross_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost02_per_sqms_gross_surface_chf"] = array("project_id", "order_id", 1, 2, 6);
	$function_fields[] = "real_costs.real_cost02_per_sqms_gross_surface_chf";

	$fields["real_costs.real_cost02_per_sqms_total_surface_chf"] = "---- Real Cost of Fixturing HQ Supplied Items andFreight Charges per sqms total surface CHF";
	$attributes["real_costs.real_cost02_per_sqms_total_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost02_per_sqms_total_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost02_per_sqms_total_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost02_per_sqms_total_surface_chf"] = array("project_id", "order_id", 2, 2, 6);
	$function_fields[] = "real_costs.real_cost02_per_sqms_total_surface_chf";

	$fields["real_costs.real_cost02_per_sqms_sales_surface_chf"] = "---- Real Cost of Fixturing HQ Supplied Items and Freight Charges per sqms sales surface CHF";
	$attributes["real_costs.real_cost02_per_sqms_sales_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost02_per_sqms_sales_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost02_per_sqms_sales_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost02_per_sqms_sales_surface_chf"] = array("project_id", "order_id", 3, 2, 6);
	$function_fields[] = "real_costs.real_cost02_per_sqms_sales_surface_chf";



	// sum of HQ supplied items and  fixturing and other and freight charges
	$fields["real_costs.real_cost03_chf"] = "Real Cost of Store fixturing / Furniture CHF";
	$attributes["real_costs.real_cost03_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost03_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost03_chf"] = "get_cost_per_group(params)";
	$content_by_function_params["real_costs.real_cost03_chf"] = array("project_id", 2, 6, 10);
	$function_fields[] = "real_costs.real_cost03_chf";

	$fields["real_costs.real_cost03_per_sqms_gross_surface_chf"] = "---- Real Cost of Store fixturing / Furniture per sqms gross surface CHF";
	$attributes["real_costs.real_cost03_per_sqms_gross_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost03_per_sqms_gross_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost03_per_sqms_gross_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost03_per_sqms_gross_surface_chf"] = array("project_id", "order_id", 1, 2, 6, 10);
	$function_fields[] = "real_costs.real_cost03_per_sqms_gross_surface_chf";

	$fields["real_costs.real_cost03_per_sqms_total_surface_chf"] = "---- Real Cost of Store fixturing / Furniture per sqms total surface CHF";
	$attributes["real_costs.real_cost03_per_sqms_total_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost03_per_sqms_total_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost03_per_sqms_total_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost03_per_sqms_total_surface_chf"] = array("project_id", "order_id", 2, 2, 6, 10);
	$function_fields[] = "real_costs.real_cost03_per_sqms_total_surface_chf";

	$fields["real_costs.real_cost03_per_sqms_sales_surface_chf"] = "---- Real Cost of Store Store fixturing / Furniture per sqms sales surface CHF";
	$attributes["real_costs.real_cost03_per_sqms_sales_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost03_per_sqms_sales_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost03_per_sqms_sales_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost03_per_sqms_sales_surface_chf"] = array("project_id", "order_id", 3, 2, 6, 10);
	$function_fields[] = "real_costs.real_cost03_per_sqms_sales_surface_chf";
	
	
	//local construction
	$fields["real_costs.real_cost_construction_chf"] = "Real Cost of Local Construction Works CHF";
	$attributes["real_costs.real_cost_construction_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_construction_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_construction_chf"] = "get_cost_per_group(params)";
	$content_by_function_params["real_costs.real_cost_construction_chf"] = array("project_id", 7);
	$function_fields[] = "real_costs.real_cost_construction_chf";

	$fields["real_costs.real_cost_construction_per_sqms_gross_surface_chf"] = "---- Real Cost of Local Construction Works per sqms gross surface CHF";
	$attributes["real_costs.real_cost_construction_per_sqms_gross_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_construction_per_sqms_gross_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_construction_per_sqms_gross_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost_construction_per_sqms_gross_surface_chf"] = array("project_id", "order_id", 1, 7);
	$function_fields[] = "real_costs.real_cost_construction_per_sqms_gross_surface_chf";

	$fields["real_costs.real_cost_construction_per_sqms_total_surface_chf"] = "---- Real Cost of Local Construction Works per sqms total surface CHF";
	$attributes["real_costs.real_cost_construction_per_sqms_total_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_construction_per_sqms_total_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_construction_per_sqms_total_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost_construction_per_sqms_total_surface_chf"] = array("project_id", "order_id", 2, 7);
	$function_fields[] = "real_costs.real_cost_construction_per_sqms_total_surface_chf";


	$fields["real_costs.real_cost_construction_per_sqms_sales_surface_chf"] = "---- Real Cost of Local Construction Works per sqms sales surface CHF";
	$attributes["real_costs.real_cost_construction_per_sqms_sales_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_construction_per_sqms_sales_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_construction_per_sqms_sales_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost_construction_per_sqms_sales_surface_chf"] = array("project_id", "order_id", 3, 7);
	$function_fields[] = "real_costs.real_cost_construction_per_sqms_sales_surface_chf";

	
	//store fixturing
	$fields["real_costs.real_cost_fixturing_hq_chf"] = "Real Cost of Fixturing HQ Supplied Items CHF";
	$attributes["real_costs.real_cost_fixturing_hq_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_fixturing_hq_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_fixturing_hq_chf"] = "get_cost_per_group(params)";
	$content_by_function_params["real_costs.real_cost_fixturing_hq_chf"] = array("project_id", 2);
	$function_fields[] = "real_costs.real_cost_fixturing_hq_chf";

	$fields["real_costs.real_cost_fixturing_hq_per_sqms_gross_surface_chf"] = "---- Real Cost of Fixturing HQ Supplied Items per sqms gross surface CHF";
	$attributes["real_costs.real_cost_fixturing_hq_per_sqms_gross_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_fixturing_hq_per_sqms_gross_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_fixturing_hq_per_sqms_gross_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost_fixturing_hq_per_sqms_gross_surface_chf"] = array("project_id", "order_id", 1, 2);
	$function_fields[] = "real_costs.real_cost_fixturing_hq_per_sqms_gross_surface_chf";

	$fields["real_costs.real_cost_fixturing_hq_per_sqms_total_surface_chf"] = "---- Real Cost of Fixturing HQ Supplied Items per sqms total surface CHF";
	$attributes["real_costs.real_cost_fixturing_hq_per_sqms_total_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_fixturing_hq_per_sqms_total_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_fixturing_hq_per_sqms_total_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost_fixturing_hq_per_sqms_total_surface_chf"] = array("project_id", "order_id", 2, 2);
	$function_fields[] = "real_costs.real_cost_fixturing_hq_per_sqms_total_surface_chf";

	$fields["real_costs.real_cost_fixturing_hq_per_sqms_sales_surface_chf"] = "---- Real Cost of Fixturing HQ Supplied Items per sqms sales surface CHF";
	$attributes["real_costs.real_cost_fixturing_hq_per_sqms_sales_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_fixturing_hq_per_sqms_sales_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_fixturing_hq_per_sqms_sales_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost_fixturing_hq_per_sqms_sales_surface_chf"] = array("project_id", "order_id", 3, 2);
	$function_fields[] = "real_costs.real_cost_fixturing_hq_per_sqms_sales_surface_chf";


	//fixturing other cost
	$fields["real_costs.real_cost_fixturing_other_chf"] = "Real Cost of Fixturing Other (local furniture, import taxes etc.) CHF";
	$attributes["real_costs.real_cost_fixturing_other_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_fixturing_other_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_fixturing_other_chf"] = "get_cost_per_group(params)";
	$content_by_function_params["real_costs.real_cost_fixturing_other_chf"] = array("project_id", 10);
	$function_fields[] = "real_costs.real_cost_fixturing_other_chf";

	$fields["real_costs.real_cost_fixturing_other_per_sqms_gross_surface_chf"] = "---- Real Cost of Fixturing Other per sqms gross surface CHF";
	$attributes["real_costs.real_cost_fixturing_other_per_sqms_gross_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_fixturing_other_per_sqms_gross_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_fixturing_other_per_sqms_gross_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost_fixturing_other_per_sqms_gross_surface_chf"] = array("project_id", "order_id", 1, 10);
	$function_fields[] = "real_costs.real_cost_fixturing_other_per_sqms_gross_surface_chf";

	$fields["real_costs.real_cost_fixturing_other_per_sqms_total_surface_chf"] = "---- Real Cost of Fixturing Other per sqms total surface CHF";
	$attributes["real_costs.real_cost_fixturing_other_per_sqms_total_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_fixturing_other_per_sqms_total_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_fixturing_other_per_sqms_total_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost_fixturing_other_per_sqms_total_surface_chf"] = array("project_id", "order_id", 2, 10);
	$function_fields[] = "real_costs.real_cost_fixturing_other_per_sqms_total_surface_chf";

	$fields["real_costs.real_cost_fixturing_other_per_sqms_sales_surface_chf"] = "---- Real Cost of Fixturing Other per sqms sales surface CHF";
	$attributes["real_costs.real_cost_fixturing_other_per_sqms_sales_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_fixturing_other_per_sqms_sales_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_fixturing_other_per_sqms_sales_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost_fixturing_other_per_sqms_sales_surface_chf"] = array("project_id", "order_id", 3, 10);
	$function_fields[] = "real_costs.real_cost_fixturing_other_per_sqms_sales_surface_chf";

	
	//freigth charges
	$fields["real_costs.real_cost_freightcharges_chf"] = "Real Cost of Freight Charges CHF";
	$attributes["real_costs.real_cost_freightcharges_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_freightcharges_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_freightcharges_chf"] = "get_cost_per_group(params)";
	$content_by_function_params["real_costs.real_cost_freightcharges_chf"] = array("project_id", 6);
	$function_fields[] = "real_costs.real_cost_freightcharges_chf";

	$fields["real_costs.real_cost_freightcharges_per_sqms_gross_surface_chf"] = "---- Real Cost of Freight Charges per sqms gross surface CHF";
	$attributes["real_costs.real_cost_freightcharges_per_sqms_gross_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_freightcharges_per_sqms_gross_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_freightcharges_per_sqms_gross_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost_freightcharges_per_sqms_gross_surface_chf"] = array("project_id", "order_id", 1, 6);
	$function_fields[] = "real_costs.real_cost_freightcharges_per_sqms_gross_surface_chf";

	$fields["real_costs.real_cost_freightcharges_per_sqms_total_surface_chf"] = "---- Real Cost of Freight Charges per sqms total surface CHF";
	$attributes["real_costs.real_cost_freightcharges_per_sqms_total_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_freightcharges_per_sqms_total_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_freightcharges_per_sqms_total_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost_freightcharges_per_sqms_total_surface_chf"] = array("project_id", "order_id", 2, 6);
	$function_fields[] = "real_costs.real_cost_freightcharges_per_sqms_total_surface_chf";

	$fields["real_costs.real_cost_freightcharges_per_sqms_sales_surface_chf"] = "---- Real Cost of Freight Charges per sqms sales surface CHF";
	$attributes["real_costs.real_cost_freightcharges_per_sqms_sales_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_freightcharges_per_sqms_sales_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_freightcharges_per_sqms_sales_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost_freightcharges_per_sqms_sales_surface_chf"] = array("project_id", "order_id", 3, 6);
	$function_fields[] = "real_costs.real_cost_freightcharges_per_sqms_sales_surface_chf";

	$fields["real_costs.real_cost_freightcharges_percent01"] = "---- Freight Charges in % of (HQ Supplied Items + Freight Charges)";
	$attributes["real_costs.real_cost_freightcharges_percent01"] = "content_by_function";
	$datatypes["real_costs.real_cost_freightcharges_percent01"] = "percent2";
	$content_by_function["real_costs.real_cost_freightcharges_percent01"] = "get_cost_freightcharges_percent01(params)";
	$content_by_function_params["real_costs.real_cost_freightcharges_percent01"] = array("project_id");
	$function_fields[] = "real_costs.real_cost_freightcharges_percent01";


	//architectural cost
	$fields["real_costs.real_cost_architecture_chf"] = "Real Cost of Architectural Services CHF";
	$attributes["real_costs.real_cost_architecture_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_architecture_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_architecture_chf"] = "get_cost_per_group(params)";
	$content_by_function_params["real_costs.real_cost_architecture_chf"] = array("project_id", 9);
	$function_fields[] = "real_costs.real_cost_architecture_chf";

	$fields["real_costs.real_cost_architecture_per_sqms_gross_surface_chf"] = "---- Real Cost of Architectural Work per sqms gross surface CHF";
	$attributes["real_costs.real_cost_architecture_per_sqms_gross_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_architecture_per_sqms_gross_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_architecture_per_sqms_gross_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost_architecture_per_sqms_gross_surface_chf"] = array("project_id", "order_id", 1, 9);
	$function_fields[] = "real_costs.real_cost_architecture_per_sqms_gross_surface_chf";

	$fields["real_costs.real_cost_architecture_per_sqms_total_surface_chf"] = "---- Real Cost of Architectural Work per sqms total surface CHF";
	$attributes["real_costs.real_cost_architecture_per_sqms_total_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_architecture_per_sqms_total_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_architecture_per_sqms_total_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost_architecture_per_sqms_total_surface_chf"] = array("project_id", "order_id", 2, 9);
	$function_fields[] = "real_costs.real_cost_architecture_per_sqms_total_surface_chf";

	$fields["real_costs.real_cost_architecture_per_sqms_sales_surface_chf"] = "---- Real Cost of Architectural Work per sqms sales surface CHF";
	$attributes["real_costs.real_cost_architecture_per_sqms_sales_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_architecture_per_sqms_sales_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_architecture_per_sqms_sales_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost_architecture_per_sqms_sales_surface_chf"] = array("project_id", "order_id", 3, 9);
	$function_fields[] = "real_costs.real_cost_architecture_per_sqms_sales_surface_chf";
	
	//equipment
	$fields["real_costs.real_cost_equipment_chf"] = "Real Cost of Equipment CHF";
	$attributes["real_costs.real_cost_equipment_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_equipment_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_equipment_chf"] = "get_cost_per_group(params)";
	$content_by_function_params["real_costs.real_cost_equipment_chf"] = array("project_id", 11);
	$function_fields[] = "real_costs.real_cost_equipment_chf";

	$fields["real_costs.real_cost_equipment_per_sqms_gross_surface_chf"] = "---- Real Cost of Equipment per sqms gross surface CHF";
	$attributes["real_costs.real_cost_equipment_per_sqms_gross_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_equipment_per_sqms_gross_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_equipment_per_sqms_gross_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost_equipment_per_sqms_gross_surface_chf"] = array("project_id", "order_id", 1, 11);
	$function_fields[] = "real_costs.real_cost_equipment_per_sqms_gross_surface_chf";

	$fields["real_costs.real_cost_equipment_per_sqms_total_surface_chf"] = "---- Real Cost of Equipment per sqms total surface CHF";
	$attributes["real_costs.real_cost_equipment_per_sqms_total_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_equipment_per_sqms_total_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_equipment_per_sqms_total_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost_equipment_per_sqms_total_surface_chf"] = array("project_id", "order_id", 2, 11);
	$function_fields[] = "real_costs.real_cost_equipment_per_sqms_total_surface_chf";

	$fields["real_costs.real_cost_equipment_per_sqms_sales_surface_chf"] = "---- Real Cost of Equipment per sqms sales surface CHF";
	$attributes["real_costs.real_cost_equipment_per_sqms_sales_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_equipment_per_sqms_sales_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_equipment_per_sqms_sales_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost_equipment_per_sqms_sales_surface_chf"] = array("project_id", "order_id", 3, 11);
	$function_fields[] = "real_costs.real_cost_equipment_per_sqms_sales_surface_chf";

	
	//other cost
	$fields["real_costs.real_cost_other_chf"] = "Real Other CHF";
	$attributes["real_costs.real_cost_other_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_other_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_other_chf"] = "get_cost_per_group(params)";
	$content_by_function_params["real_costs.real_cost_other_chf"] = array("project_id", 8);
	$function_fields[] = "real_costs.real_cost_other_chf";

	$fields["real_costs.real_cost_other_per_sqms_gross_surface_chf"] = "---- Real Other sqms gross surface CHF";
	$attributes["real_costs.real_cost_other_per_sqms_gross_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_other_per_sqms_gross_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_other_per_sqms_gross_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost_other_per_sqms_gross_surface_chf"] = array("project_id", "order_id", 1, 11);
	$function_fields[] = "real_costs.real_cost_other_per_sqms_gross_surface_chf";

	$fields["real_costs.real_cost_other_per_sqms_total_surface_chf"] = "---- Real Other sqms total surface CHF";
	$attributes["real_costs.real_cost_other_per_sqms_total_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_other_per_sqms_total_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_other_per_sqms_total_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost_other_per_sqms_total_surface_chf"] = array("project_id", "order_id", 2, 11);
	$function_fields[] = "real_costs.real_cost_other_per_sqms_total_surface_chf";

	$fields["real_costs.real_cost_other_per_sqms_sales_surface_chf"] = "---- Real Other per sqms sales surface CHF";
	$attributes["real_costs.real_cost_other_per_sqms_sales_surface_chf"] = "content_by_function";
	$datatypes["real_costs.real_cost_other_per_sqms_sales_surface_chf"] = "decimal2";
	$content_by_function["real_costs.real_cost_other_per_sqms_sales_surface_chf"] = "get_cost_per_group_and_sqm(params)";
	$content_by_function_params["real_costs.real_cost_other_per_sqms_sales_surface_chf"] = array("project_id", "order_id", 3, 11);
	$function_fields[] = "real_costs.real_cost_other_per_sqms_sales_surface_chf";




	//percentage values
	$fields["real_costs.real_cost_construction_percent01"] = "Real Cost of Local Construction Works in % of (Fixturing + Freight Charges + Local Construction  Works)";
	$attributes["real_costs.real_cost_construction_percent01"] = "content_by_function";
	$datatypes["real_costs.real_cost_construction_percent01"] = "percent2";
	$content_by_function["real_costs.real_cost_construction_percent01"] = "get_cost_construction_percent01(params)";
	$content_by_function_params["real_costs.real_cost_construction_percent01"] = array("project_id");
	$function_fields[] = "real_costs.real_cost_construction_percent01";


	$fields["real_costs.real_cost_construction_percent"] = "Real Cost of Local Construction Works in % of Total Real Cost";
	$attributes["real_costs.real_cost_construction_percent"] = "content_by_function";
	$datatypes["real_costs.real_cost_construction_percent"] = "percent2";
	$content_by_function["real_costs.real_cost_construction_percent"] = "get_cost_percent_for_group(params)";
	$content_by_function_params["real_costs.real_cost_construction_percent"] = array("project_id", 7);
	$function_fields[] = "real_costs.real_cost_construction_percent";

	$fields["real_costs.real_cost_fixturing_percent"] = "Real Cost of Store fixturing / Furniture in % of Total Real Cost";
	$attributes["real_costs.real_cost_fixturing_percent"] = "content_by_function";
	$datatypes["real_costs.real_cost_fixturing_percent"] = "percent2";
	$content_by_function["real_costs.real_cost_fixturing_percent"] = "get_cost_percent_for_group(params)";
	$content_by_function_params["real_costs.real_cost_fixturing_percent"] = array("project_id", 2);
	$function_fields[] = "real_costs.real_cost_fixturing_percent";

	$fields["real_costs.real_cost_fixturing_other_percent"] = "Real Cost of Fixturing Other (local furniture, import taxes etc.) in % of Total Real Cost";
	$attributes["real_costs.real_cost_fixturing_other_percent"] = "content_by_function";
	$datatypes["real_costs.real_cost_fixturing_other_percent"] = "percent2";
	$content_by_function["real_costs.real_cost_fixturing_other_percent"] = "get_cost_percent_for_group(params)";
	$content_by_function_params["real_costs.real_cost_fixturing_other_percent"] = array("project_id", 10);
	$function_fields[] = "real_costs.real_cost_fixturing_other_percent";

	$fields["real_costs.real_cost_freightcharges_percent"] = "Real Cost of Freight Charges in % of Total Real Cost";
	$attributes["real_costs.real_cost_freightcharges_percent"] = "content_by_function";
	$datatypes["real_costs.real_cost_freightcharges_percent"] = "percent2";
	$content_by_function["real_costs.real_cost_freightcharges_percent"] = "get_cost_percent_for_group(params)";
	$content_by_function_params["real_costs.real_cost_freightcharges_percent"] = array("project_id", 6);
	$function_fields[] = "real_costs.real_cost_freightcharges_percent";

	$fields["real_costs.real_cost_architecture_percent"] = "Real Cost of Architectural Work in % of Total Real Cost";
	$attributes["real_costs.real_cost_architecture_percent"] = "content_by_function";
	$datatypes["real_costs.real_cost_architecture_percent"] = "percent2";
	$content_by_function["real_costs.real_cost_architecture_percent"] = "get_cost_percent_for_group(params)";
	$content_by_function_params["real_costs.real_cost_architecture_percent"] = array("project_id", 9);
	$function_fields[] = "real_costs.real_cost_architecture_percent";

	$fields["real_costs.real_cost_equipment_percent"] = "Real Cost of Equipment in % of Total Real Cost";
	$attributes["real_costs.real_cost_equipment_percent"] = "content_by_function";
	$datatypes["real_costs.real_cost_equipment_percent"] = "percent2";
	$content_by_function["real_costs.real_cost_equipment_percent"] = "get_cost_percent_for_group(params)";
	$content_by_function_params["real_costs.real_cost_equipment_percent"] = array("project_id", 11);
	$function_fields[] = "real_costs.real_cost_equipment_percent";

	$fields["real_costs.real_cost_other_percent"] = "Real Other in % of Total Real Cost";
	$attributes["real_costs.real_cost_other_percent"] = "content_by_function";
	$datatypes["real_costs.real_cost_other_percent"] = "percent2";
	$content_by_function["real_costs.real_cost_other_percent"] = "get_cost_percent_for_group(params)";
	$content_by_function_params["real_costs.real_cost_other_percent"] = array("project_id", 8);
	$function_fields[] = "real_costs.real_cost_other_percent";

	
	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["calculated_content_sort_order"] = $calculated_content_sort_order;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;
	$db_info["group_fields"] = $group_fields;
	$db_info["function_fields"] = $function_fields;

	return $db_info;
}



/********************************************************************
    get DB Info for cost comparision fields
*********************************************************************/
function query_cost_comparison_fields()
{

	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_sort_order = array();
	$content_by_function = array();
	$content_by_function_params = array();
	$group_fields = array();
	$function_fields = array();

	
	//Difference in cost
	$fields["cost_comparison.real_cost_versus_approved_amount_chf"] = "Difference of Real Cost and Total Approved Investment Amount CHF";
	$attributes["cost_comparison.real_cost_versus_approved_amount_chf"] = "content_by_function";
	$datatypes["cost_comparison.real_cost_versus_approved_amount_chf"] = "decimal2";
	$content_by_function["cost_comparison.real_cost_versus_approved_amount_chf"] = "get_real_cost_versus_approved_amount(params)";
	$content_by_function_params["cost_comparison.real_cost_versus_approved_amount_chf"] = array("project_id", "order_id", 1);
	$function_fields[] = "cost_comparison.real_cost_versus_approved_amount_chf";

	$fields["cost_comparison.rc_construction_versus_aa_chf"] = "---- Difference of Real Cost of Local Construction Works and Approved Investment CHF";
	$attributes["cost_comparison.rc_construction_versus_aa_chf"] = "content_by_function";
	$datatypes["cost_comparison.rc_construction_versus_aa_chf"] = "decimal2";
	$content_by_function["cost_comparison.rc_construction_versus_aa_chf"] = "get_real_cost_versus_approved_amount_per_group(params)";
	$content_by_function_params["cost_comparison.rc_construction_versus_aa_chf"] = array("project_id", "order_id", 7, 0, 0, 1, 1);
	$function_fields[] = "cost_comparison.rc_construction_versus_aa_chf";


	$fields["cost_comparison.rc_fixturing_versus_aa_chf"] = "---- Difference of Real Cost of Store fixturing / Furniture and Approved Investment CHF";
	$attributes["cost_comparison.rc_fixturing_versus_aa_chf"] = "content_by_function";
	$datatypes["cost_comparison.rc_fixturing_versus_aa_chf"] = "decimal2";
	$content_by_function["cost_comparison.rc_fixturing_versus_aa_chf"] = "get_real_cost_versus_approved_amount_per_group(params)";
	$content_by_function_params["cost_comparison.rc_fixturing_versus_aa_chf"] = array("project_id", "order_id", 2, 10, 6, 3, 1);
	$function_fields[] = "cost_comparison.rc_fixturing_versus_aa_chf";


	$fields["cost_comparison.rc_architecture_versus_aa_chf"] = "---- Difference of Real Cost of Architectural Works and Approved Investment CHF";
	$attributes["cost_comparison.rc_architecture_versus_aa_chf"] = "content_by_function";
	$datatypes["cost_comparison.rc_architecture_versus_aa_chf"] = "decimal2";
	$content_by_function["cost_comparison.rc_architecture_versus_aa_chf"] = "get_real_cost_versus_approved_amount_per_group(params)";
	$content_by_function_params["cost_comparison.rc_architecture_versus_aa_chf"] = array("project_id", "order_id", 9, 0, 0, 5, 1);
	$function_fields[] = "cost_comparison.rc_architecture_versus_aa_chf";


	$fields["cost_comparison.rc_equipment_versus_aa_chf"] = "---- Difference of Real Cost of Equipmentand and Approved Investment CHF";
	$attributes["cost_comparison.rc_equipment_versus_aa_chf"] = "content_by_function";
	$datatypes["cost_comparison.rc_equipment_versus_aa_chf"] = "decimal2";
	$content_by_function["cost_comparison.rc_equipment_versus_aa_chf"] = "get_real_cost_versus_approved_amount_per_group(params)";
	$content_by_function_params["cost_comparison.rc_equipment_versus_aa_chf"] = array("project_id", "order_id", 11, 0, 0, 7, 1);
	$function_fields[] = "cost_comparison.rc_equipment_versus_aa_chf";


	$fields["cost_comparison.rc_other_versus_aa_chf"] = "---- Difference of Real Other and Approved Investment CHF";
	$attributes["cost_comparison.rc_other_versus_aa_chf"] = "content_by_function";
	$datatypes["cost_comparison.rc_other_versus_aa_chf"] = "decimal2";
	$content_by_function["cost_comparison.rc_other_versus_aa_chf"] = "get_real_cost_versus_approved_amount_per_group(params)";
	$content_by_function_params["cost_comparison.rc_other_versus_aa_chf"] = array("project_id", "order_id", 8, 0, 0, 11, 1);
	$function_fields[] = "cost_comparison.rc_other_versus_aa_chf";


	
	//cos above/below approved in investmenst in %
	$fields["cost_comparison.real_cost_versus_approved_amount_percent"] = "Real Cost above/below Total Approved Investment Amount in %";
	$attributes["cost_comparison.real_cost_versus_approved_amount_percent"] = "content_by_function";
	$datatypes["cost_comparison.real_cost_versus_approved_amount_percent"] = "cost_difference_percent2";
	$content_by_function["cost_comparison.real_cost_versus_approved_amount_percent"] = "get_real_cost_versus_approved_amount(params)";
	$content_by_function_params["cost_comparison.real_cost_versus_approved_amount_percent"] = array("project_id", "order_id", 2);
	$function_fields[] = "cost_comparison.real_cost_versus_approved_amount_percent";

	$fields["cost_comparison.rc_construction_versus_aa_percent"] = "---- Real Cost of Local Construction Works above/below  Approved Investment in %";
	$attributes["cost_comparison.rc_construction_versus_aa_percent"] = "content_by_function";
	$datatypes["cost_comparison.rc_construction_versus_aa_percent"] = "cost_difference_percent2";
	$content_by_function["cost_comparison.rc_construction_versus_aa_percent"] = "get_real_cost_versus_approved_amount_per_group(params)";
	$content_by_function_params["cost_comparison.rc_construction_versus_aa_percent"] = array("project_id", "order_id", 7, 0, 0, 1, 2);
	$function_fields[] = "cost_comparison.rc_construction_versus_aa_percent";


	$fields["cost_comparison.rc_fixturing_versus_aa_percent"] = "---- Real Cost of Store fixturing / Furniture above/below  Approved Investment in %";
	$attributes["cost_comparison.rc_fixturing_versus_aa_percent"] = "content_by_function";
	$datatypes["cost_comparison.rc_fixturing_versus_aa_percent"] = "cost_difference_percent2";
	$content_by_function["cost_comparison.rc_fixturing_versus_aa_percent"] = "get_real_cost_versus_approved_amount_per_group(params)";
	$content_by_function_params["cost_comparison.rc_fixturing_versus_aa_percent"] = array("project_id", "order_id", 2, 10, 6, 3, 2);
	$function_fields[] = "cost_comparison.rc_fixturing_versus_aa_percent";


	$fields["cost_comparison.rc_architecture_versus_aa_percent"] = "---- Real Cost of Architectural Works above/below  Approved Investment in %";
	$attributes["cost_comparison.rc_architecture_versus_aa_percent"] = "content_by_function";
	$datatypes["cost_comparison.rc_architecture_versus_aa_percent"] = "cost_difference_percent2";
	$content_by_function["cost_comparison.rc_architecture_versus_aa_percent"] = "get_real_cost_versus_approved_amount_per_group(params)";
	$content_by_function_params["cost_comparison.rc_architecture_versus_aa_percent"] = array("project_id", "order_id", 9, 0, 0, 5, 2);
	$function_fields[] = "cost_comparison.rc_architecture_versus_aa_percent";


	$fields["cost_comparison.rc_equipment_versus_aa_percent"] = "---- Real Cost of Equipmentand above/below  Approved Investment in %";
	$attributes["cost_comparison.rc_equipment_versus_aa_percent"] = "content_by_function";
	$datatypes["cost_comparison.rc_equipment_versus_aa_percent"] = "cost_difference_percent2";
	$content_by_function["cost_comparison.rc_equipment_versus_aa_percent"] = "get_real_cost_versus_approved_amount_per_group(params)";
	$content_by_function_params["cost_comparison.rc_equipment_versus_aa_percent"] = array("project_id", "order_id", 11, 0, 0, 7, 2);
	$function_fields[] = "cost_comparison.rc_equipment_versus_aa_percent";


	$fields["cost_comparison.rc_other_versus_aa_percent"] = "---- Real Other above/below  Approved Investment in %";
	$attributes["cost_comparison.rc_other_versus_aa_percent"] = "content_by_function";
	$datatypes["cost_comparison.rc_other_versus_aa_percent"] = "cost_difference_percent2";
	$content_by_function["cost_comparison.rc_other_versus_aa_percent"] = "get_real_cost_versus_approved_amount_per_group(params)";
	$content_by_function_params["cost_comparison.rc_other_versus_aa_percent"] = array("project_id", "order_id", 8, 0, 0, 11, 2);
	$function_fields[] = "cost_comparison.rc_other_versus_aa_percent";


	//Percent of real cost above/below approved investments
	$fields["cost_comparison.real_cost_in_percent_of_approved"] = "Real Cost in % of Total Approved Investment Amount";
	$attributes["cost_comparison.real_cost_in_percent_of_approved"] = "content_by_function";
	$datatypes["cost_comparison.real_cost_in_percent_of_approved"] = "cost_difference_percent";
	$content_by_function["cost_comparison.real_cost_in_percent_of_approved"] = "get_real_cost_versus_approved_amount(params)";
	$content_by_function_params["cost_comparison.real_cost_in_percent_of_approved"] = array("project_id", "order_id", 3);
	$function_fields[] = "cost_comparison.real_cost_in_percent_of_approved";


	$fields["cost_comparison.rc_construction_in_percent_aa"] = "---- Real Cost of Local Construction Works in % of  Approved Investment ";
	$attributes["cost_comparison.rc_construction_in_percent_aa"] = "content_by_function";
	$datatypes["cost_comparison.rc_construction_in_percent_aa"] = "cost_difference_percent";
	$content_by_function["cost_comparison.rc_construction_in_percent_aa"] = "get_real_cost_versus_approved_amount_per_group(params)";
	$content_by_function_params["cost_comparison.rc_construction_in_percent_aa"] = array("project_id", "order_id", 7, 0, 0, 1, 3);
	$function_fields[] = "cost_comparison.rc_construction_in_percent_aa";


	$fields["cost_comparison.rc_fixturing_in_percent_aa"] = "---- Real Cost of Store fixturing / Furniture in % of  Approved Investment ";
	$attributes["cost_comparison.rc_fixturing_in_percent_aa"] = "content_by_function";
	$datatypes["cost_comparison.rc_fixturing_in_percent_aa"] = "cost_difference_percent";
	$content_by_function["cost_comparison.rc_fixturing_in_percent_aa"] = "get_real_cost_versus_approved_amount_per_group(params)";
	$content_by_function_params["cost_comparison.rc_fixturing_in_percent_aa"] = array("project_id", "order_id", 2, 10, 6, 3, 3);
	$function_fields[] = "cost_comparison.rc_fixturing_in_percent_aa";


	$fields["cost_comparison.rc_architecture_in_percent_aa"] = "---- Real Cost of Architectural Works in % of  Approved Investment ";
	$attributes["cost_comparison.rc_architecture_in_percent_aa"] = "content_by_function";
	$datatypes["cost_comparison.rc_architecture_in_percent_aa"] = "cost_difference_percent";
	$content_by_function["cost_comparison.rc_architecture_in_percent_aa"] = "get_real_cost_versus_approved_amount_per_group(params)";
	$content_by_function_params["cost_comparison.rc_architecture_in_percent_aa"] = array("project_id", "order_id", 9, 0, 0, 5, 3);
	$function_fields[] = "cost_comparison.rc_architecture_in_percent_aa";


	$fields["cost_comparison.rc_equipment_in_percent_aa"] = "---- Real Cost of Equipmentand in % of  Approved Investment ";
	$attributes["cost_comparison.rc_equipment_in_percent_aa"] = "content_by_function";
	$datatypes["cost_comparison.rc_equipment_in_percent_aa"] = "cost_difference_percent";
	$content_by_function["cost_comparison.rc_equipment_in_percent_aa"] = "get_real_cost_versus_approved_amount_per_group(params)";
	$content_by_function_params["cost_comparison.rc_equipment_in_percent_aa"] = array("project_id", "order_id", 11, 0, 0, 7, 3);
	$function_fields[] = "cost_comparison.rc_equipment_in_percent_aa";


	$fields["cost_comparison.rc_other_in_percent_aa"] = "---- Real Other in % of  Approved Investment ";
	$attributes["cost_comparison.rc_other_in_percent_aa"] = "content_by_function";
	$datatypes["cost_comparison.rc_other_in_percent_aa"] = "cost_difference_percent";
	$content_by_function["cost_comparison.rc_other_in_percent_aa"] = "get_real_cost_versus_approved_amount_per_group(params)";
	$content_by_function_params["cost_comparison.rc_other_in_percent_aa"] = array("project_id", "order_id", 8, 0, 0, 11, 3);
	$function_fields[] = "cost_comparison.rc_other_in_percent_aa";

	
	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["calculated_content_sort_order"] = $calculated_content_sort_order;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;
	$db_info["group_fields"] = $group_fields;
	$db_info["function_fields"] = $function_fields;

	return $db_info;
	
}

/********************************************************************
    get DB Info for milestone fields
*********************************************************************/
function query_milestone_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_sort_order = array();
	$content_by_function = array();
	$content_by_function_params = array();
	$group_fields = array();
	$function_fields = array();

	$fields["milestones.project_cost_milestone_remarks"] = "Milestones Remarks";
	$attributes["milestones.project_cost_milestone_remarks"] = "project_costs.project_cost_milestone_remarks";
	$datatypes["milestones.project_cost_milestone_remarks"] = "text";

	$fields["milestones.project_cost_milestone_remarks2"] = "Milestones Remarks 2 (MIS)";
	$attributes["milestones.project_cost_milestone_remarks2"] = "project_costs.project_cost_milestone_remarks2";
	$datatypes["milestones.project_cost_milestone_remarks2"] = "text";


	
	$sql = "select * from milestones where milestone_active = 1 order by milestone_code";
	$res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
		/*
		$fields["milestone_date_" . $row["milestone_id"]] = $row["milestone_code"] . " ". $row["milestone_text"] . " Date";
		$attributes["milestone_date_" . $row["milestone_id"]] = "content_by_subquery";
		$datatypes["milestone_date_" . $row["milestone_id"]] = "date";
		$calculated_content["milestone_date_" . $row["milestone_id"]] = "select project_milestone_date from project_milestones where project_milestone_project = project_id and project_milestone_milestone = " . $row["milestone_id"];
		$calculated_content_field["milestone_date_" . $row["milestone_id"]] = "milestone_date_" . $row["milestone_id"];
		*/
		
		//$content_by_function["milestones.milestone_id_" . $row["milestone_id"]] = "get_milestone_date(params)";
		//$content_by_function_params["milestones.milestone_id_" . $row["milestone_id"]] = array("project_id", $row["milestone_id"]);
		

		$fields["milestone_date_" . $row["milestone_id"]] = $row["milestone_code"] . " ". $row["milestone_text"] . " Date";
		$attributes["milestone_date_" . $row["milestone_id"]] = "content_by_function";
		$datatypes["milestone_date_" . $row["milestone_id"]] = "date";
		$content_by_function["milestone_date_" . $row["milestone_id"]] = "get_milestone_date(params)";
		$content_by_function_params["milestone_date_" . $row["milestone_id"]] = array("project_id", $row["milestone_id"]);


		$fields["milestones.milestone_comment_" . $row["milestone_id"]] = "---- " . $row["milestone_code"] . " Milestone Comment";
		$attributes["milestones.milestone_comment_" . $row["milestone_id"]] = "content_by_function";
		$datatypes["milestones.milestone_comment_" . $row["milestone_id"]] = "text";
		$content_by_function["milestones.milestone_comment_" . $row["milestone_id"]] = "get_milestone_comment(params)";
		$content_by_function_params["milestones.milestone_comment_" . $row["milestone_id"]] = array("project_id", $row["milestone_id"]);



	}




	$fields["milestones.numdays_14_16"] = "Number of Days from Project Submission to LN posted";
	$attributes["milestones.numdays_14_16"] = "content_by_function";
	$datatypes["milestones.numdays_14_16"] = "integer";
	$content_by_function["milestones.numdays_14_16"] = "get_numdays_between_two_milestones(params)";
	$content_by_function_params["milestones.numdays_14_16"] = array("project_id", 14, 0, 16, 0);


	$fields["milestones.numdays_16_13"] = "Number of days from LN posted to LN approved";
	$attributes["milestones.numdays_16_13"] = "content_by_function";
	$datatypes["milestones.numdays_16_13"] = "integer";
	$content_by_function["milestones.numdays_16_13"] = "get_numdays_between_two_milestones(params)";
	$content_by_function_params["milestones.numdays_16_13"] = array("project_id", 16, 0, 13, 0);


	$fields["milestones.numdays_16_22"] = "Number of days from LN posted to LN rejected";
	$attributes["milestones.numdays_16_22"] = "content_by_function";
	$datatypes["milestones.numdays_16_22"] = "integer";
	$content_by_function["milestones.numdays_16_22"] = "get_numdays_between_two_milestones(params)";
	$content_by_function_params["milestones.numdays_16_22"] = array("project_id", 16, 0, 22, 0);


	$fields["milestones.numdays_13_1"] = "Number of days from LN approved to AF/CER posted/re-submitted";
	$attributes["milestones.numdays_13_1"] = "content_by_function";
	$datatypes["milestones.numdays_13_1"] = "integer";
	$content_by_function["milestones.numdays_13_1"] = "get_numdays_between_two_milestones(params)";
	$content_by_function_params["milestones.numdays_13_1"] = array("project_id", 13, 0, 1, 15);


	$fields["milestones.numdays_1_15"] = "Number of days CER/AF posted/re-submitted to approved by Controlling";
	$attributes["milestones.numdays_1_15"] = "content_by_function";
	$datatypes["milestones.numdays_1_15"] = "integer";
	$content_by_function["milestones.numdays_1_15"] = "get_numdays_between_two_milestones(params)";
	$content_by_function_params["milestones.numdays_1_15"] = array("project_id", 1, 15, 17, 0);



	$fields["milestones.numdays_19_20"] = "Number of days from local signature request to receipt";
	$attributes["milestones.numdays_19_20"] = "content_by_function";
	$datatypes["milestones.numdays_19_20"] = "integer";
	$content_by_function["milestones.numdays_19_20"] = "get_numdays_between_two_milestones(params)";
	$content_by_function_params["milestones.numdays_19_20"] = array("project_id", 19, 0, 20, 0);


	$fields["milestones.numdays_17_10"] = "Number of days approved by Controlling to signed by President";
	$attributes["milestones.numdays_17_10"] = "content_by_function";
	$datatypes["milestones.numdays_17_10"] = "integer";
	$content_by_function["milestones.numdays_17_10"] = "get_numdays_between_two_milestones(params)";
	$content_by_function_params["milestones.numdays_17_10"] = array("project_id", 17, 0, 10, 6);


	$fields["milestones.numdays_17_21"] = "Number of days approved by Controlling to signed by President (<50kFr)";
	$attributes["milestones.numdays_17_21"] = "content_by_function";
	$datatypes["milestones.numdays_17_21"] = "integer";
	$content_by_function["milestones.numdays_17_21"] = "get_numdays_between_two_milestones(params)";
	$content_by_function_params["milestones.numdays_17_21"] = array("project_id", 17, 0, 21, 0);


	$fields["milestones.numdays_1_11"] = "Number of days CER posted/re-submitted to CER to SG KL";
	$attributes["milestones.numdays_1_11"] = "content_by_function";
	$datatypes["milestones.numdays_1_11"] = "integer";
	$content_by_function["milestones.numdays_1_11"] = "get_numdays_between_two_milestones(params)";
	$content_by_function_params["milestones.numdays_1_11"] = array("project_id", 1, 15, 11, 0);


	$fields["milestones.numdays_11_12"] = "Number of days CER to SG KL to KL notice";
	$attributes["milestones.numdays_11_12"] = "content_by_function";
	$datatypes["milestones.numdays_11_12"] = "integer";
	$content_by_function["milestones.numdays_11_12"] = "get_numdays_between_two_milestones(params)";
	$content_by_function_params["milestones.numdays_11_12"] = array("project_id", 11, 0, 12, 0);


	$fields["milestones.numdays_14_12"] = "Number of Days from Projectstart to KL notice";
	$attributes["milestones.numdays_14_12"] = "content_by_function";
	$datatypes["milestones.numdays_14_12"] = "integer";
	$content_by_function["milestones.numdays_14_12"] = "get_numdays_between_two_milestones(params)";
	$content_by_function_params["milestones.numdays_14_12"] = array("project_id", 14, 0, 12, 0);

		
	$fields["milestones.numdays_approval_to_opening"] = "Number of days from KL approval to agreed or actual opening date";
	$attributes["milestones.numdays_approval_to_opening"] = "content_by_function";
	$datatypes["milestones.numdays_approval_to_opening"] = "integer";
	$content_by_function["milestones.numdays_approval_to_opening"] = "get_numdays_approval_to_opening(params)";
	$content_by_function_params["milestones.numdays_approval_to_opening"] = array("project_id");

	
	$fields["milestones.numdays_start_to_opening"] = "Total of days from Projectstart to agreed or actual opening date";
	$attributes["milestones.numdays_start_to_opening"] = "content_by_function";
	$datatypes["milestones.numdays_start_to_opening"] = "integer";
	$content_by_function["milestones.numdays_start_to_opening"] = "get_numdays_start_to_opening(params)";
	$content_by_function_params["milestones.numdays_start_to_opening"] = array("project_id");


	$fields["milestones.latest_milestone"] = "Latest Milestone";
	$attributes["milestones.latest_milestone"] = "content_by_function";
	$datatypes["milestones.latest_milestone"] = "text";
	$content_by_function["milestones.latest_milestone"] = "get_latest_milestone(params)";
	$content_by_function_params["milestones.latest_milestone"] = array("project_id");

	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["calculated_content_sort_order"] = $calculated_content_sort_order;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;
	$db_info["group_fields"] = $group_fields;
	$db_info["function_fields"] = $function_fields;

	return $db_info;
}

/********************************************************************
    get DB Info for Items in Projects
*********************************************************************/
function query_item_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_sort_order = array();
	$content_by_function = array();
	$content_by_function_params = array();
	$group_fields = array();
	$function_fields = array();

	
	/*
	$fields["order_items.item_code"] = "Item Code";
	$attributes["order_items.item_code"] = "items.item_code";
	$datatypes["order_items.item_code"] = "text";

	$fields["order_items.item_name"] = "Item Name";
	$attributes["order_items.item_name"] = "items.item_name";
	$datatypes["order_items.item_name"] = "text";

	$fields["order_items.order_item_quantity"] = "Quantity";
	$attributes["order_items.order_item_quantity"] = "order_items.order_item_quantity";
	$datatypes["order_items.order_item_quantity"] = "decimal2";
	$function_fields[] = "order_items.order_item_quantity";
	*/

	$fields["item_quantity_block"] = "Item Quantity Block";
	$attributes["item_quantity_block"] = "item_quantity_block";
	$datatypes["item_quantity_block"] = "decimal2";
	
	
	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["calculated_content_sort_order"] = $calculated_content_sort_order;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;
	$db_info["group_fields"] = $group_fields;
	$db_info["function_fields"] = $function_fields;
	
	return $db_info;
}

/********************************************************************
    get DB Info for orderstate fields
*********************************************************************/
function query_orderstate_fields()
{
	$db_info = array();
	$fields = array();
	$attributes = array();
	$datatypes = array();
	$calculated_content = array();
	$calculated_content_key = array();
	$calculated_content_field = array();
	$calculated_content_sort_order = array();
	$content_by_function = array();
	$content_by_function_params = array();
	$group_fields = array();
	$function_fields = array();

	
	
	$sql = "select order_state_id, order_state_code,  order_state_name " . 
		   "from order_states " .
		   "    left join order_state_groups on order_states.order_state_group = order_state_group_id " .
		   " where order_state_active and order_state_group_order_type = 1 " . 
	       "order by order_state_code";
	
	$res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
		$fields["order_states.order_state_id_" . $row["order_state_id"]] = $row["order_state_code"] . " ". $row["order_state_name"] . " Date";
		$attributes["order_states.order_state_id_" . $row["order_state_id"]] = "content_by_function";
		$datatypes["order_states.order_state_id_" . $row["order_state_id"]] = "date";
		$content_by_function["order_states.order_state_id_" . $row["order_state_id"]] = "get_order_state_date(params)";
		$content_by_function_params["order_states.order_state_id_" . $row["order_state_id"]] = array("order_id", $row["order_state_id"]);

	}

	
	$fields["order_states.remaining_days"] = "Remaining Days to Agreed Opening Date";
	$attributes["order_states.remaining_days"] = "content_by_function";
	$datatypes["order_states.remaining_days"] = "integer";
	$content_by_function["order_states.remaining_days"] = "get_remaining_days(params)";
	$content_by_function_params["order_states.remaining_days"] = array("project_id");


	$db_info["fields"] = $fields;
	$db_info["attributes"] = $attributes;
	$db_info["datatypes"] = $datatypes;
	$db_info["calculated_content"] = $calculated_content;
	$db_info["calculated_content_key"] = $calculated_content_key;
	$db_info["calculated_content_field"] = $calculated_content_field;
	$db_info["calculated_content_sort_order"] = $calculated_content_sort_order;
	$db_info["content_by_function"] = $content_by_function;
	$db_info["content_by_function_params"] = $content_by_function_params;
	$db_info["group_fields"] = $group_fields;
	$db_info["function_fields"] = $function_fields;

	return $db_info;
}




/********************************************************************
    get project location
*********************************************************************/
function get_pos_data($order_id = 0, $field = 1)
{
	$data = "";

	$posaddress_id = 0;
	$source_table = "posaddresses";

	//check pipeline
	$sql = "select posorder_parent_table, posorder_posaddress " . 
		   " from posorderspipeline " . 
		   " where posorder_order = " . $order_id;


	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$posaddress_id = $row["posorder_posaddress"];
		$source_table = $row["posorder_parent_table"];

	}
	else
	{
		$sql = "select posorder_posaddress " . 
			   " from posorders " . 
			   " where posorder_order = " . $order_id;

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$posaddress_id = $row["posorder_posaddress"];
		}
	}
	
	if($posaddress_id > 0)
	{
		$sql = "select posaddress_name, posaddress_address, place_name " . 
			   " from " . $source_table . 
			   " left join places on place_id = posaddress_place_id " . 
			   " where posaddress_id = " . $posaddress_id;
	
		
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			if($field == 1)
			{
				$data = $row["posaddress_name"];
			}
			elseif($field == 2)
			{
				$data = $row["posaddress_address"];
			}
			elseif($field == 3)
			{
				$data = $row["place_name"];
			}
		}
	}


	return $data;

}


/********************************************************************
    get POS enterprise reporting number
*********************************************************************/
function get_pos_epnr($order_id = 0)
{
	$data = "";

	$posaddress_id = 0;
	$source_table = "posaddresses";

	//check pipeline
	$sql = "select posorder_parent_table, posorder_posaddress " . 
		   " from posorderspipeline " . 
		   " where posorder_order = " . $order_id;


	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$posaddress_id = $row["posorder_posaddress"];
		$source_table = $row["posorder_parent_table"];

	}
	else
	{
		$sql = "select posorder_posaddress " . 
			   " from posorders " . 
			   " where posorder_order = " . $order_id;

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$posaddress_id = $row["posorder_posaddress"];
		}
	}
	
	if($posaddress_id > 0)
	{
		$sql = "select posaddress_eprepnr " . 
			   " from " . $source_table . 
			   " where posaddress_id = " . $posaddress_id;
	
		
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$data = $row["posaddress_eprepnr"];
		}
	}


	return $data;

}


/********************************************************************
    get project location
*********************************************************************/
function get_project_location($project_id = 0)
{

	$location = "";
	$sql = "select location_type_name, project_location " .
	       " from projects " . 
		   " left join location_types on location_type_id = project_location_type " . 
		   " where project_id = " . $project_id;
	
	
	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		if($row["location_type_name"] and $row["project_location"])
		{
			$location = $row["location_type_name"] . " " . $row["project_location"];
		}
		elseif($row["location_type_name"])
		{
			$location = $row["location_type_name"];
		}
		elseif($row["project_location"])
		{
			$location = $row["project_location"];
		}
	}

	return $location;
}

/********************************************************************
    get project cancellation date
*********************************************************************/
function get_project_cancellation_date($order_id = 0)
{

	$date = "";
	$sql = "select left(date_created, 10) as dc from actual_order_states " . 
			" where actual_order_state_order = " . $order_id .
			" and actual_order_state_state = 43 " . 
			" order by actual_order_state_state DESC";
	
	
	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		
		$date = $row["dc"];
	}

	return $date;
}

/********************************************************************
    get project cancellation state
*********************************************************************/
function get_project_cancellation_state($order_id = 0)
{

	$cancelled_from_step = "";
	$sql = "select order_state_code " .
		     " from actual_order_states " .
		     " left join order_states on order_state_id = actual_order_state_state " . 
		     " where actual_order_state_order = " . $order_id . 
		     " and order_state_code < '900' " . 
		     " order by order_state_id DESC " . 
		     " LIMIT 0,1";
	
	
	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		
		$cancelled_from_step = $row["order_state_code"];
	}

	return $cancelled_from_step;
}

/********************************************************************
    get lease detail
*********************************************************************/
function get_rental_detail($order_id = 0, $field = "")
{

	$value = "";
	$record_found = false;
	
	if($field == "")
	{
		return $value;
	}
	$sql = "select  $field " .
		     " from posleases " .
		     " where poslease_order = " . $order_id;	
	
	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		
		$value = $row[$field];
		$record_found = true;
	}

	if($record_found == false)
	{
		$sql = "select  $field " .
				 " from posleasespipeline " .
				 " where poslease_order = " . $order_id;	
		
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			
			$value = $row[$field];
		}
	}

	return $value;
}


/********************************************************************
    get cer detail
*********************************************************************/
function get_cer_detail($project_id = 0, $field = "")
{

	$value = "";
	
	if($field == "")
	{
		return $value;
	}
	$sql = "select  $field " .
		     " from cer_basicdata " .
		     " where cer_basicdata_version = 0 and cer_basicdata_project = " . $project_id;	
	
	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		
		$value = $row[$field];
	}

	
	return $value;
}


/********************************************************************
    get milestone comments
*********************************************************************/
function get_milestone_comment($project_id = 0, $miles_stone_id = 0)
{
	$comment = "";
	$sql = "select project_milestone_comment " . 
		   " from project_milestones " .
		   " where project_milestone_project = " . $project_id . 
		   " and project_milestone_milestone = " . $miles_stone_id;
	
	
	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		
		$comment = $row["project_milestone_comment"];
	}

	return $comment;
}

/********************************************************************
    get latest milestone
*********************************************************************/
function get_latest_milestone($project_id = 0)
{
	$latest_milestone = "";
	
	$no_ln_needed = "";
	$no_cer_needed = "";
	$sql_m = "select ln_no_ln_submission_needed " . 
		     " from ln_basicdata " . 
		     " where ln_basicdata_version = 0 " . 
		     " and ln_basicdata_project = " . $project_id;

	$res_m = mysql_query($sql_m) or dberror($sql_m);
	if ($row_m = mysql_fetch_assoc($res_m))
	{
		if($row_m["ln_no_ln_submission_needed"] == 1)
		{
			$no_ln_needed = "No LN needed";
		}
	}

	$sql_m = "select cer_basicdata_no_cer_submission_needed " . 
		     " from cer_basicdata " . 
		     " where cer_basicdata_version = 0 " . 
		     " and cer_basicdata_project = " . $project_id;

	$res_m = mysql_query($sql_m) or dberror($sql_m);
	if ($row_m = mysql_fetch_assoc($res_m))
	{
		if($row_m["cer_basicdata_no_cer_submission_needed"] == 1)
		{
			$no_cer_needed = "No AF/CER needed";
		}
	}

	// get project's milestones
	$milestones = array();
	$last_milestone = "";
	$sql_m = "select project_milestone_milestone,  " . 
  			 "DATE_FORMAT(project_milestone_date, '%d.%m.%Y') as milestone_date, " .
		     "project_milestone_date, milestone_text " .
		     "from project_milestones " . 
		     "left join milestones on milestone_id = project_milestone_milestone " .
		     "where project_milestone_project = " . $project_id .
		     " and project_milestone_date is not NULL and project_milestone_date <> '0000-00-00' " .
		     " order by milestone_code DESC";

	$res_m = mysql_query($sql_m) or dberror($sql_m);
	if ($row_m = mysql_fetch_assoc($res_m))
	{
		if($no_cer_needed != "" and $no_ln_needed != "")
		{
			$last_milestone = $row_m["milestone_text"] . ": " . $row_m["milestone_date"] . " (" . $no_ln_needed . ", " . $no_cer_needed . ")";
		}
		elseif($no_cer_needed != "")
		{
			$last_milestone = $row_m["milestone_text"] . ": " . $row_m["milestone_date"] . " (" . $no_cer_needed . ")";
		}
		elseif($no_ln_needed != "")
		{
			$last_milestone = $row_m["milestone_text"] . ": " . $row_m["milestone_date"] . " (" . $no_ln_needed . ")";
		}
		else
		{
			$last_milestone = $row_m["milestone_text"] . ": " . $row_m["milestone_date"];
		}
	}

	return $last_milestone;
}

/***********************************************************************
    get milestone date
************************************************************************/
function get_milestone_date($project_id = 0, $milestone_id = 0)
{
	$date = "";
	$sql = "select project_milestone_date " . 
		   "from project_milestones " . 
		   "where project_milestone_project = " . $project_id .
		   " and project_milestone_milestone = " .  $milestone_id;
	
	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$date = $row["project_milestone_date"];
	}
	return $date;

}

/***********************************************************************
    get Number of Days between two milestones
************************************************************************/
function get_numdays_between_two_milestones($project_id = 0, $from1 = 0, $from2 = 0, $to1 = 0, $to2 = 0)
{

	$difference_in_days = "";

	$date1 = "";
	$date2 = "";


	$sql = "select project_milestone_date " . 
		   "from project_milestones " . 
		   " where project_milestone_project = " . dbquote($project_id) .
		   " and project_milestone_milestone = " . dbquote($from1);

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$date1 = $row["project_milestone_date"];
		//echo $date1 . "---";
	}

	if($from2 > 0)
	{
		$sql = "select project_milestone_date " . 
			   "from project_milestones " . 
			   " where project_milestone_project = " . dbquote($project_id) .
			   " and project_milestone_milestone = " . dbquote($from2);

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			if($date1 == NULL or $date1 == "")
			{
				$date1= $row["project_milestone_date"];
			}
			elseif($row["project_milestone_date"] > $date1)
			{
				$date1 = $row["project_milestone_date"];
				//echo $date1 . "@@@";
			}
		}
	}
	
	
	$sql = "select project_milestone_date " . 
		   "from project_milestones " . 
		   " where project_milestone_project = " . dbquote($project_id) .
		   " and project_milestone_milestone = " . dbquote($to1);

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$date2 = $row["project_milestone_date"];
		//echo $date2 . "���";
	}

	if($to2 > 0)
	{
		$sql = "select project_milestone_date " . 
			   "from project_milestones " . 
			   " where project_milestone_project = " . dbquote($project_id) .
			   " and project_milestone_milestone = " . dbquote($to2);

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			if($date2 == NULL or $date2 == "")
			{
				$date2 = $row["project_milestone_date"];
			}
			elseif($row["project_milestone_date"] > $date2)
			{
				$date2 = $row["project_milestone_date"];
			}
			//echo $date2 . "+++";
		}
	}


	if($date1 != '0000-00-00' and $date1 != NULL
		and $date2 != '0000-00-00' and $date2 != NULL) {
		$difference_in_days = ceil((strtotime($date2) - strtotime($date1)) / 86400);
	}
	
	return $difference_in_days;
}




/***********************************************************************
    get Number of days from KL approval to agreed or actual opening date
************************************************************************/
function get_numdays_approval_to_opening($project_id = 0)
{

	$difference_in_days = "";

	$sql = "select project_real_opening_date, project_actual_opening_date, project_milestone_date " . 
		   "from projects " . 
		   "left join project_milestones on project_milestone_project = project_id " . 
		   " where project_id = " . dbquote($project_id) .
		   " and project_milestone_milestone = 12";


	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		if($row['project_milestone_date'] 
			and $row['project_milestone_date'] != '0000-00-00' 
			and $row['project_actual_opening_date'] 
			and $row['project_actual_opening_date'] != '0000-00-00') {
			$difference_in_days = ceil((strtotime($row['project_actual_opening_date']) - strtotime($row['project_milestone_date'])) / 86400);
		}
		elseif($row['project_milestone_date'] 
			and $row['project_milestone_date'] != '0000-00-00' 
			and $row['project_real_opening_date'] 
			and $row['project_real_opening_date'] != '0000-00-00') {
			$difference_in_days = ceil((strtotime($row['project_real_opening_date']) - strtotime($row['project_milestone_date'])) / 86400);
		}
	}
	return $difference_in_days;
}


/***********************************************************************
    get Total of days from Projectstart to agreed or actual opening date
************************************************************************/
function get_numdays_start_to_opening($project_id = 0)
{

	$difference_in_days = "";

	$sql = "select project_real_opening_date, project_actual_opening_date, project_milestone_date " . 
		   "from projects " . 
		   "left join project_milestones on project_milestone_project = project_id " . 
		   " where project_id = " . dbquote($project_id) .
		   " and project_milestone_milestone = 14";


	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		if($row['project_milestone_date'] 
			and $row['project_milestone_date'] != '0000-00-00' 
			and $row['project_actual_opening_date'] 
			and $row['project_actual_opening_date'] != '0000-00-00') {
			$difference_in_days = ceil((strtotime($row['project_actual_opening_date']) - strtotime($row['project_milestone_date'])) / 86400);
		}
		elseif($row['project_milestone_date'] 
			and $row['project_milestone_date'] != '0000-00-00' 
			and $row['project_real_opening_date'] 
			and $row['project_real_opening_date'] != '0000-00-00') {
			$difference_in_days = ceil((strtotime($row['project_real_opening_date']) - strtotime($row['project_milestone_date'])) / 86400);
		}
	}
	return $difference_in_days;
}

/***********************************************************************
    get remaining days to agreed opening date
************************************************************************/
function get_remaining_days($project_id = 0)
{
	$remaining_days = "";

	$sql = "select project_real_opening_date " . 
		   "from projects " . 
		   " where project_id = " . dbquote($project_id) .
		   " and project_real_opening_date >= " . date("Y-m-d");

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$date1 = new DateTime($row["project_real_opening_date"]);
		$date2 = new DateTime(date("Y-m-d"));

		$remaining_days = $date2->diff($date1)->format("%a");
	}
	
	return $remaining_days;
}


/********************************************************************
    get ordestate date
*********************************************************************/
function get_order_state_date($order_id = 0, $order_state_id = 0)
{
	$date = "";
	$sql = "select date_created " . 
		   " from actual_order_states " .
		   " where actual_order_state_order = " . $order_id . 
		   " and actual_order_state_state = " . $order_state_id . 
		   " order by date_created DESC";
	
	
	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		
		$date = substr($row["date_created"], 0, 10);
	}

	return $date;
}

/********************************************************************
    get ordestate date
*********************************************************************/
function get_latest_order_state_date($order_id = 0)
{
	$date = "";
	
	$sql = "select order_actual_order_state_code " . 
		   " from orders " .
		   " where order_id = " . $order_id;

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		
		$sql = "select order_state_id " . 
			   " from order_states " .
			   " LEFT JOIN order_state_groups ON order_states.order_state_group = order_state_group_id " . 
			   " where order_state_group_order_type = 1 " . 
			   " and order_state_code = " . dbquote($row["order_actual_order_state_code"]);

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$sql = "select actual_order_states.date_created AS state_date " . 
				   " from actual_order_states " .
				   " where actual_order_state_order = " . $order_id . 
				   " and actual_order_state_state = " . $row["order_state_id"] . 
				   " order by actual_order_states.date_created DESC";

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$date = substr($row["state_date"], 0, 10);
			}
		}
	}

	return $date;
}


/********************************************************************
    get ordestate date
*********************************************************************/
function get_order_state_text($order_id = 0, $type = 1)
{
	$text = "";
	
	if($type == 1)
	{
		$sql = "select concat(order_state_code, ' ', order_state_name) as text " . 
			   " from orders " .  
			   " left join order_states on order_state_code = order_actual_order_state_code " .
			   " LEFT JOIN order_state_groups ON order_states.order_state_group = order_state_group_id " . 
			   " where order_state_group_order_type = 1 " . 
			   " and order_id = " . dbquote($order_id);
	}
	elseif($type == 2)
	{
		$sql = "select concat(order_state_code, ' ', order_state_name) as text " . 
			   " from orders " .  
			   " left join order_states on order_state_code = order_logistic_status " .
			   " LEFT JOIN order_state_groups ON order_states.order_state_group = order_state_group_id " . 
			   " where order_state_group_order_type = 1 " . 
			   " and order_id = " . dbquote($order_id);
	}
	elseif($type == 3)
	{
		$sql = "select concat(order_state_code, ' ', order_state_name) as text " . 
			   " from orders " .  
			   " left join order_states on order_state_code = order_development_status " .
			   " LEFT JOIN order_state_groups ON order_states.order_state_group = order_state_group_id " . 
			   " where order_state_group_order_type = 1 " . 
			   " and order_id = " . dbquote($order_id);
	}
	
	

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$text = $row["text"];
	}

	return $text;
}


/********************************************************************
    get ordestate date
*********************************************************************/
function get_floor_delivery_date($order_id = 0)
{
	$floor_delivery_date = "";
	$floor_ordered = false;
	$floor_exists_in_the_list_of_materials = false;
	$floor_delivery_date = "";
	$actual_order_state_code = "";

	
	$sql = "select order_actual_order_state_code " . 
		   " from orders " .
		   " where order_id = " . $order_id;

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$actual_order_state_code = $row["order_actual_order_state_code"];
		
		$sql_m = 'select order_item_id, order_item_ordered ' .
				 'from order_items ' . 
				 'left join items on item_id = order_item_item ' . 
				 'where order_item_order = ' . $order_id   .
				 ' and item_category = 3';

		$res_m = mysql_query($sql_m) or dberror($sql_m);
		while ($row_m = mysql_fetch_assoc($res_m))
		{
			$sql_d = 'select DATE_FORMAT(date_date, "%d.%m.%Y") as delivery_date, date_date ' .
					 'from dates ' . 
					 'where date_type = 4 and date_order_item = ' . $row_m['order_item_id'] . 
					 ' order by date_date ASC';

			$res_d = mysql_query($sql_d) or dberror($sql_d);
			while ($row_d = mysql_fetch_assoc($res_d))
			{
				$floor_delivery_date = $row_d["date_date"];
			}

			if($row_m["order_item_ordered"] != '0000-00-00' and $row_m["order_item_ordered"] != NULL) 
			{
				$floor_ordered = true;
			}

			$floor_exists_in_the_list_of_materials = true;
		}

		if($actual_order_state_code >= '600')
		{
			if($floor_exists_in_the_list_of_materials == false) 
			{
				$floor_delivery_date = "local";
			}
			elseif($floor_ordered == false and $floor_delivery_date == '') 
			{
				$floor_delivery_date = "n.a.";
			}
			elseif($floor_ordered == true and $floor_delivery_date == '') 
			{
				$floor_delivery_date = "ordered";
			}
		}
		else
		{
			if($floor_exists_in_the_list_of_materials == false) 
			{
				$floor_delivery_date = "n.a.";
			}
			elseif($floor_ordered == false and $floor_delivery_date == '') 
			{
				$floor_delivery_date = "n.a.";
			}
			elseif($floor_ordered == true and $floor_delivery_date == '') 
			{
				$floor_delivery_date = "ordered";
			}
		}
	}
	
	return $floor_delivery_date;
}

/********************************************************************
    get frames delivery date
*********************************************************************/
function get_frames_delivery_date($order_id = 0)
{
	$frame_delivery_date = "";
	$sql_m = 'select order_item_id ' .
		     'from order_items ' . 
		     'left join items on item_id = order_item_item ' . 
		     'where order_item_order = ' . $order_id  .
		     ' and item_category IN (24)';

	$res_m = mysql_query($sql_m) or dberror($sql_m);
	while ($row_m = mysql_fetch_assoc($res_m))
	{
		$sql_d = 'select DATE_FORMAT(date_date, "%d.%m.%Y") as delivery_date, date_date ' .
			     'from dates ' . 
			     'where date_type = 4 and date_order_item = ' . $row_m['order_item_id'] . 
			     ' order by date_date ASC';

		$res_d = mysql_query($sql_d) or dberror($sql_d);
		while ($row_d = mysql_fetch_assoc($res_d))
		{
			$frame_delivery_date = $row_d["date_date"];
		}
	}

	return $frame_delivery_date;

}


/********************************************************************
    get visuals delivery date
*********************************************************************/
function get_visuals_delivery_date($order_id = 0)
{
	$visuals_delivery_date = "";
	
	$sql_m = 'select order_item_id ' .
		     'from order_items ' . 
		     'left join items on item_id = order_item_item ' . 
		     'where order_item_order = ' . $order_id  .
		     ' and item_category IN (6, 7)';

	$res_m = mysql_query($sql_m) or dberror($sql_m);
	while ($row_m = mysql_fetch_assoc($res_m))
	{
		$sql_d = 'select DATE_FORMAT(date_date, "%d.%m.%Y") as delivery_date, date_date ' .
			     'from dates ' . 
			     'where date_type = 4 and date_order_item = ' . $row_m['order_item_id'] . 
			     ' order by date_date ASC';

		$res_d = mysql_query($sql_d) or dberror($sql_d);
		while ($row_d = mysql_fetch_assoc($res_d))
		{
			$visuals_delivery_date = $row_d["date_date"];
		}
	}

	return $visuals_delivery_date;	

}


/********************************************************************
    get illuminateion delivery date
*********************************************************************/
function get_illumination_delivery_date($order_id = 0)
{
	
	$illumination_delivery_date = "";
	$sql_m = 'select order_item_id ' .
		     'from order_items ' . 
		     'left join items on item_id = order_item_item ' . 
		     'where order_item_order = ' . $order_id  .
		     ' and item_category IN (16)';

	$res_m = mysql_query($sql_m) or dberror($sql_m);
	while ($row_m = mysql_fetch_assoc($res_m))
	{
		$sql_d = 'select DATE_FORMAT(date_date, "%d.%m.%Y") as delivery_date, date_date ' .
			     'from dates ' . 
			     'where date_type = 4 and date_order_item = ' . $row_m['order_item_id'] . 
			     ' order by date_date ASC';

		$res_d = mysql_query($sql_d) or dberror($sql_d);
		while ($row_d = mysql_fetch_assoc($res_d))
		{
			$illumination_delivery_date = $row_d["date_date"];
		}
	}

	return $illumination_delivery_date;

}


/********************************************************************
    get furniture delivery date
*********************************************************************/
function get_furniture_delivery_date($order_id = 0)
{
	$furniture_delivery_date = "";
	
	
	$costmonitoring_groups = array();
	$costmonitoring_group_filter = "";

	/*
	$sql = "select costmonitoringgroup_id from costmonitoringgroups " . 
		   "where costmonitoringgroup_include_in_masterplan = 1 ";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$costmonitoring_groups[] = $row["costmonitoringgroup_id"];
	}

	$costmonitoring_group_filter = "";
	if(count($costmonitoring_groups > 0)) {

		$costmonitoring_group_filter = " or order_items_costmonitoring_group IN (" . implode(",", $costmonitoring_groups) .") ";
		
	}
	*/
	
	$sql_m = 'select order_item_id ' .
		     'from order_items ' . 
		     'left join items on item_id = order_item_item ' . 
		     'where order_item_order = ' . $order_id  .
		     ' and (item_category_include_in_masterplan = 1) '. $costmonitoring_group_filter . ')';

	$res_m = mysql_query($sql_m) or dberror($sql_m);
	while ($row_m = mysql_fetch_assoc($res_m))
	{
		$sql_d = 'select DATE_FORMAT(date_date, "%d.%m.%Y") as delivery_date, date_date ' .
			     'from dates ' . 
			     'where date_type = 4 and date_order_item = ' . $row_m['order_item_id'] . 
			     ' order by date_date ASC';

		$res_d = mysql_query($sql_d) or dberror($sql_d);
		while ($row_d = mysql_fetch_assoc($res_d))
		{
			$furniture_delivery_date = $row_d["date_date"];
		}
	}
	

	return $furniture_delivery_date;

}



/********************************************************************
    get approved budget
*********************************************************************/
function get_approved_budget_chf($project_id = 0)
{
	$amount = 0;
	/*
	$sql = "select sum(if(order_item_quantity_freezed > 0, order_item_quantity_freezed*order_item_system_price_freezed, order_item_system_price_freezed)) as approved_budget_chf " . 
		" from order_items " . 
		" where order_item_order = " . $order_id;
	*/

	$sql = "select sum(costsheet_budget_approved_amount*costsheet_exchangerate/currency_factor) as approved_budget_chf  
		 from costsheets 
		 left join currencies on currency_id = costsheet_currency_id
		 where costsheet_version = 0
			and costsheet_project_id = " . $project_id;
	

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$amount = $row["approved_budget_chf"];
	}
	return $amount;
}



/********************************************************************
    get get requested investment amount
*********************************************************************/
function get_investment_amount_chf($project_id = 0, $type = 0, $base = 1)
{
	$amount = 0;
	
	if($type == 1) // investments
	{
		$filter = " and cer_investment_type in(1, 3, 5, 7, 11, 18, 19, 20) ";
	}
	elseif($type == 2) // intangibles
	{
		$filter = " and cer_investment_type in(9, 13, 15, 17) ";
	}
	else
	{
		$filter = " and cer_investment_type in(0) ";
	}

	
	$sql = "select cer_basicdata_exchangerate, cer_basicdata_factor, " . 
		   "sum(if(cer_investment_type = 19, -1*cer_investment_amount_cer_loc, cer_investment_amount_cer_loc)) as amount, " .
		   "sum(cer_investment_amount_cer_loc_approved) as amount2 " . 
		   " from cer_basicdata " . 
		   " left join cer_investments on cer_investment_project = cer_basicdata_project " . 
		   " where cer_basicdata_version = 0 " . 
		   " and cer_investment_cer_version = 0 " .
		   $filter .  
		   " and cer_basicdata_project = " . $project_id . 
		   " and cer_investment_project = " . $project_id;

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		if($base == 2)
		{
			$amount = $row["amount2"];
		}
		else
		{
			$amount = $row["amount"];
		}

		$exchange_rate = $row["cer_basicdata_exchangerate"];
		$factor = $row["cer_basicdata_factor"];
		
		if($factor > 0)
		{
			$amount = $exchange_rate*$amount/$factor;
		}
		
	}
	return $amount;
}



/********************************************************************
    get requested additional investemen amount
*********************************************************************/
function get_investment_additional_chf($project_id = 0, $type = 0, $base = 1)
{
	$amount = 0;
	
	if($type == 1) // investments
	{
		$filter = " and cer_investment_type in(1, 3, 5, 7, 11, 18, 19, 20) ";
	}
	elseif($type == 2) // intangibles
	{
		$filter = " and cer_investment_type in(9, 13, 15, 17) ";
	}
	else
	{
		$filter = " and cer_investment_type in(0) ";
	}
	
	$sql = "select cer_basicdata_exchangerate, cer_basicdata_factor, " . 
		   "sum(if(cer_investment_type = 19, -1*cer_investment_amount_cer_loc, cer_investment_amount_cer_loc)) as amount, " .
		   "sum(cer_investment_amount_additional_cer_loc_approved) as amount2 " .
		   " from cer_basicdata " . 
		   " left join cer_investments on cer_investment_project = cer_basicdata_project " . 
		   " where cer_basicdata_version = 0 " . 
		   " and cer_investment_cer_version = 0 " .
		   $filter . 
		   " and cer_basicdata_project = " . $project_id . 
		   " and cer_investment_project = " . $project_id;


	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		if($base == 2)
		{
			$amount = $row["amount2"];
		}
		else
		{
			$amount = $row["amount"];
		}
		$exchange_rate = $row["cer_basicdata_exchangerate"];
		$factor = $row["cer_basicdata_factor"];
		
		if($factor > 0)
		{
			$amount = round($exchange_rate*$amount/$factor, 4);
		}
	}
	return $amount;
}



/********************************************************************
    get total requested investemen amount
*********************************************************************/
function get_total_investment_amount_chf($project_id = 0, $type = 0, $content = 1)
{
	$amount = 0;

	if($type == 1) // investments
	{
		$filter = " and cer_investment_type in(1, 3, 5, 7, 11, 18, 19, 20) ";
	}
	elseif($type == 2) // intangibles
	{
		$filter = " and cer_investment_type in(9, 13, 15, 17) ";
	}
	else
	{
		$filter = " and cer_investment_type in(0) ";
	}
	
	$sql = "select cer_basicdata_exchangerate, cer_basicdata_factor, " . 
		   "sum(if(cer_investment_type = 19, -1*cer_investment_amount_cer_loc, cer_investment_amount_cer_loc)) as amount, " .
		   "sum(cer_investment_amount_additional_cer_loc) as additional, " .
		   "sum(cer_investment_amount_cer_loc_approved) as amount2, " .
		   "sum(cer_investment_amount_additional_cer_loc_approved) as additional2 " .
		   " from cer_basicdata " . 
		   " left join cer_investments on cer_investment_project = cer_basicdata_project " . 
		   " where cer_basicdata_version = 0 " . 
		   " and cer_investment_cer_version = 0 " .
		   $filter . 
		   " and cer_basicdata_project = " . $project_id . 
		   " and cer_investment_project = " . $project_id;


	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		if($content == 2)
		{
			$amount = $row["amount2"] + $row["additional2"];
		}
		else
		{
			$amount = $row["amount"] + $row["additional"];
		}
		
		$exchange_rate = $row["cer_basicdata_exchangerate"];
		$factor = $row["cer_basicdata_factor"];
		if($factor > 0)
		{
			$amount = round($exchange_rate*$amount/$factor, 4);
		}
	}
	return $amount;
}


/********************************************************************
    get requested investemen amount per investment type
*********************************************************************/
function get_investment_amount_per_group_chf($project_id = 0, $investment_type1= 0, $investment_type2= 0, $content = 0)
{
	$amount = 0;
	
	$sql = "select cer_investment_amount_cer_loc, cer_investment_amount_cer_loc_approved, " .
		   " cer_investment_amount_additional_cer_loc, cer_investment_amount_additional_cer_loc_approved, " .
		   " cer_basicdata_exchangerate, cer_basicdata_factor " . 
		   " from cer_investments " . 
		   " left join cer_basicdata on cer_investment_project = cer_basicdata_project " . 
		   " where cer_basicdata_version = 0 " . 
		   " and cer_investment_cer_version = 0 " .
		   " and cer_investment_type in (" . $investment_type1 . ", " . $investment_type2 . ") " . 
		   " and cer_basicdata_project = " . $project_id . 
		   " and cer_investment_project = " . $project_id;


	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		
		if($content == 1)
		{
			$amount = $row["cer_investment_amount_cer_loc"];
		}
		elseif($content == 2)
		{
			$amount = $row["cer_investment_amount_additional_cer_loc"];
		}
		elseif($content == 3)
		{
			$amount = $row["cer_investment_amount_cer_loc"] + $row["cer_investment_amount_additional_cer_loc"];
		}
		elseif($content == 4)
		{
			$amount = $row["cer_investment_amount_cer_loc_approved"];
		}
		elseif($content == 5)
		{
			$amount = $row["cer_investment_amount_additional_cer_loc_approved"];
		}
		elseif($content == 6)
		{
			$amount = $row["cer_investment_amount_cer_loc_approved"] + $row["cer_investment_amount_additional_cer_loc_approved"];
		}
		
		$exchange_rate = $row["cer_basicdata_exchangerate"];
		$factor = $row["cer_basicdata_factor"];
		if($factor > 0)
		{
			$amount = round($exchange_rate*$amount/$factor, 4);
		}
	}
	return $amount;
}


/********************************************************************
    get real cost
*********************************************************************/
function get_real_cost_chf($project_id = 0)
{
	$amount = 0;
	/*
	$sql = "select sum(if(order_item_quantity > 0, order_item_quantity*order_item_real_system_price, order_item_real_system_price)) as amount " . 
		" from order_items " . 
		" where order_item_order = " . $order_id;
	*/
	
	$sql = "select sum(costsheet_real_amount*costsheet_exchangerate/currency_factor) as approved_budget_chf  
		 from costsheets 
		 left join currencies on currency_id = costsheet_currency_id
		 where costsheet_version = 0
			and costsheet_project_id = " . $project_id;

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$amount = $row["amount"];
	}


	//get catalogue items from orders assigned to the project
	/*
	$sql = "select TRUNCATE(sum(order_item_quantity * order_items_in_project_real_price), 2) as amount " . 
           " from order_items_in_projects " .
		   " left join order_items on order_item_id = order_items_in_project_order_item_id " .
		   " left join project_cost_groupnames on project_cost_groupname_id =  order_items_in_project_costgroup_id " .
		   " where order_items_in_project_project_order_id = " . $order_id;

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$amount = $amount + $row["amount"];
	}
	*/

	return $amount;
}


/***********************************************************************
    get real cost per sqm
************************************************************************/
function get_cost_per_sqm($project_id = 0, $order_id = 0, $type = 0)
{
	
	$cost_per_sqm = "";

	$real_cost = get_real_cost_chf($project_id);

	if($type == 1)
	{
		$sql = "select project_cost_grosssqms, project_cost_totalsqms " . 
			   " from project_costs " . 
			   " where project_cost_order = " . dbquote($order_id);

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res) and $row["project_cost_totalsqms"] > 0)
		{
			//$cost_per_sqm = round($real_cost/$row["project_cost_grosssqms"], 4);
			$cost_per_sqm = round($real_cost/$row["project_cost_totalsqms"], 4);
		}
	}
	elseif($type == 2)
	{
		$sql = "select project_cost_totalsqms " . 
			   " from project_costs " . 
			   " where project_cost_order = " . dbquote($order_id);

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res) and $row["project_cost_totalsqms"] > 0)
		{
			$cost_per_sqm = round($real_cost/$row["project_cost_totalsqms"], 4);
		}
	}
	elseif($type == 3)
	{
		$sql = "select project_cost_sqms " . 
			   " from project_costs " . 
			   " where project_cost_order = " . dbquote($order_id);

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res) and $row["project_cost_sqms"] > 0)
		{
			$cost_per_sqm = round($real_cost/$row["project_cost_sqms"], 4);
		}
	}

	 

	return $cost_per_sqm;
}




/********************************************************************
    get real cost per cost group
*********************************************************************/
function get_cost_per_group($project_id = 0, $group01 = 0, $group02 = 0, $group03 = 0)
{
	$amount = 0;
	
	/*
	$group_filter = " and order_item_cost_group in (" . $group01 . "," .  $group02 .  "," . $group03 . ") ";
	

	$sql = "select sum(if(order_item_quantity > 0, order_item_quantity*order_item_real_system_price, order_item_real_system_price)) as amount " . 
		" from order_items " . 
		" where order_item_order = " . $order_id  . 
		$group_filter;

	*/
	
	$group_filter = " and costsheet_pcost_group_id in (" . $group01 . "," .  $group02 .  "," . $group03 . ") ";
	

	$sql = "select sum(costsheet_real_amount*costsheet_exchangerate/currency_factor) as amount 
			from costsheets 
			 left join currencies on currency_id = costsheet_currency_id
			where costsheet_version = 0
			and costsheet_project_id = " . $project_id . 
			$group_filter;

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$amount = $row["amount"];
	}

	//get catalogue items from orders assigned to the project
	/*
	$sql = "select TRUNCATE(sum(order_item_quantity * order_items_in_project_real_price), 2) as amount " . 
           " from order_items_in_projects " .
		   " left join order_items on order_item_id = order_items_in_project_order_item_id " .
		   " left join project_cost_groupnames on project_cost_groupname_id =  order_items_in_project_costgroup_id " .
		   " where order_items_in_project_project_order_id = " . $order_id . 
		   " and order_items_in_project_costgroup_id in (" . $group01 . "," .  $group02 .  "," . $group03 . ") ";

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$amount = $amount + $row["amount"];
	}
	*/

	return $amount;
}

/********************************************************************
    get real cost per cost group as percent of total real cost
*********************************************************************/
function get_cost_percent_for_group($project_id = 0, $group01 = 0)
{
	$percentage = "";
	
	$total_real_cost = get_real_cost_chf($project_id);
	
	/*
	$group_filter = " and order_item_cost_group in (" . $group01 . ") ";
	

	$sql = "select sum(if(order_item_quantity > 0, order_item_quantity*order_item_real_system_price, order_item_real_system_price)) as amount " . 
		" from order_items " . 
		" where order_item_order = " . $order_id  . 
		$group_filter;

	*/

	$group_filter = " and costsheet_pcost_group_id in (" . $group01 . ") ";
	
	$sql = "select sum(costsheet_real_amount*costsheet_exchangerate/currency_factor) as amount 
			from costsheets 
			 left join currencies on currency_id = costsheet_currency_id
			where costsheet_version = 0
			and costsheet_project_id = " . $project_id . 
			$group_filter;

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$amount = $row["amount"];
	}

	/*
	//get catalogue items from orders assigned to the project
	$sql = "select TRUNCATE(sum(order_item_quantity * order_items_in_project_real_price), 2) as amount " . 
           " from order_items_in_projects " .
		   " left join order_items on order_item_id = order_items_in_project_order_item_id " .
		   " left join project_cost_groupnames on project_cost_groupname_id =  order_items_in_project_costgroup_id " .
		   " where order_items_in_project_project_order_id = " . $order_id . 
		   $group_filter;

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$amount = $amount + $row["amount"];
	}
	*/

	if($total_real_cost > 0 and $amount)
	{
		$percentage = round($amount/$total_real_cost, 4);
	}


	return $percentage;
}




/***********************************************************************
    get real cost per sqm
************************************************************************/
function get_cost_per_group_and_sqm($project_id = 0, $order_id = 0, $type = 0, $group01 = 0, $group02 = 0, $group03 = 0)
{

	$cost_per_sqm = "";

	$real_cost = get_cost_per_group($project_id, $order_id, $group01, $group02, $group03);


	if($type == 1)
	{
		$sql = "select project_cost_grosssqms, project_cost_totalsqms " . 
			   " from project_costs " . 
			   " where project_cost_order = " . dbquote($order_id);

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res) and $row["project_cost_totalsqms"] > 0)
		{
			//$cost_per_sqm = round($real_cost/$row["project_cost_grosssqms"], 4);
			$cost_per_sqm = round($real_cost/$row["project_cost_totalsqms"], 4);
		}
	}
	elseif($type == 2)
	{
		$sql = "select project_cost_totalsqms " . 
			   " from project_costs " . 
			   " where project_cost_order = " . dbquote($order_id);

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res) and $row["project_cost_totalsqms"] > 0)
		{
			$cost_per_sqm = round($real_cost/$row["project_cost_totalsqms"], 4);
		}
	}
	elseif($type == 3)
	{
		$sql = "select project_cost_sqms " . 
			   " from project_costs " . 
			   " where project_cost_order = " . dbquote($order_id);

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res) and $row["project_cost_sqms"] > 0)
		{
			$cost_per_sqm = round($real_cost/$row["project_cost_sqms"], 4);
		}
	}

	 

	return $cost_per_sqm;
}


/***********************************************************************
    get freight charges in percent of Fixturing+Freightcharges
************************************************************************/
function get_cost_freightcharges_percent01($project_id = 0)
{
	$amount = 0;

	$base_amount = get_cost_per_group($project_id, 2, 6);

	$freight_charges = get_cost_per_group($project_id, 6);

	if($base_amount > 0)
	{
		$amount = round($freight_charges/$base_amount, 4);
	}

	return $amount;
}


	
/***********************************************************************
    get local construction cost in percent of Fixturing+Freightcharges+Local Construction
************************************************************************/
function get_cost_construction_percent01($project_id = 0)
{
	
	$amount = 0;

	$base_amount = get_cost_per_group($project_id, 2, 6, 7);

	$local_construction = get_cost_per_group($project_id, 7);

	if($base_amount > 0)
	{
		$amount = round($local_construction/$base_amount, 4);
	}

	return $amount;
}



/***********************************************************************
    get real cost per sqm fo the total of HQ supplied, Freight Charges and Local Construction
************************************************************************/
function get_cost01_per_sqm($project_id = 0, $order_id = 0, $type = 0)
{
	
	$cost_per_sqm = "";

	$real_cost = get_cost_per_group($project_id, 2, 6, 7);

	if($type == 1)
	{
		$sql = "select project_cost_grosssqms, project_cost_totalsqms " . 
			   " from project_costs " . 
			   " where project_cost_order = " . dbquote($order_id);

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res) and $row["project_cost_totalsqms"] > 0)
		{
			//$cost_per_sqm = round($real_cost/$row["project_cost_grosssqms"], 4);
			$cost_per_sqm = round($real_cost/$row["project_cost_totalsqms"], 4);
		}
	}
	elseif($type == 2)
	{
		$sql = "select project_cost_totalsqms " . 
			   " from project_costs " . 
			   " where project_cost_order = " . dbquote($order_id);

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res) and $row["project_cost_totalsqms"] > 0)
		{
			$cost_per_sqm = round($real_cost/$row["project_cost_totalsqms"], 4);
		}
	}
	elseif($type == 3)
	{
		$sql = "select project_cost_sqms " . 
			   " from project_costs " . 
			   " where project_cost_order = " . dbquote($order_id);

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res) and $row["project_cost_sqms"] > 0)
		{
			$cost_per_sqm = round($real_cost/$row["project_cost_sqms"], 4);
		}
	}

	 

	return $cost_per_sqm;
}


/***********************************************************************
    get real cost versus KL approved amount
************************************************************************/
function get_real_cost_versus_approved_amount($project_id = 0, $order_id = 0, $content = 1)
{
	$amount = 0;

	$real_cost = get_real_cost_chf($project_id);
	$approved_investments = get_total_investment_amount_chf($project_id, 1, 2);

	if($content == 1)
	{
		$amount = $real_cost - $approved_investments;
	}
	elseif($content == 2)
	{
		$amount = "";
		if($approved_investments > 0 and $real_cost > 0)
		{
			$amount = round((-1*($approved_investments - $real_cost))/$approved_investments, 4);
		}
	}
	elseif($content == 3)
	{
		$amount = "";
		if($approved_investments > 0 and $real_cost > 0)
		{
			$amount = round($real_cost/$approved_investments, 4);
		}
	}

	return $amount;
}



/***********************************************************************
    get real cost versus KL approved amount
************************************************************************/
function get_real_cost_versus_approved_amount_per_group($project_id = 0, $order_id = 0, $group01 = 0, $group02 = 0, $group03 = 0, $investment_type = 0, $content = 0)
{
	$amount = 0;

	$real_cost = get_cost_per_group($project_id, $group01, $group02 , $group03);
	$approved_investments = get_investment_amount_per_group_chf($project_id, $investment_type, 0, 6);

	if($content == 1)
	{
		$amount = $real_cost - $approved_investments;
	}
	elseif($content == 2)
	{
		$amount = "";
		if($approved_investments > 0 and $real_cost > 0)
		{
			$amount = round((-1*($approved_investments - $real_cost))/$approved_investments, 4);
		}
	}
	elseif($content == 3)
	{
		$amount = "";
		if($approved_investments > 0 and $real_cost > 0)
		{
			$amount = round($real_cost/$approved_investments, 4);
		}
	}

	return $amount;
}



/***********************************************************************
    get overdue in days for cms completion and cms approval
************************************************************************/
function get_cms_overdue_days_date($project_id = 0, $type = 1)
{
	$number_of_days = "";
	$today = date("Y-m-d");

	
	
	$sql = "select project_actual_opening_date, project_cost_cms_completion_date, " . 
		   " project_cost_cms_approved_date, project_cost_cms_completion_due_date " . 
		   " from projects " . 
		   " left join project_costs on project_cost_order = project_order " . 
		   " where project_id = " . dbquote($project_id);

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		if($type == 1)
		{
			if($row["project_actual_opening_date"] != NULL 
				and  $row["project_actual_opening_date"] != '0000-00-00'
				and ($row["project_cost_cms_completion_date"] == NULL 
					 or $row["project_cost_cms_completion_date"] == '0000-00-00')
			)
			{
				if($row["project_cost_cms_completion_due_date"] != NULL
					and $row["project_cost_cms_completion_due_date"] != '0000-00-00')
				{
					$number_of_days = round((strtotime($today) - strtotime($row["project_cost_cms_completion_due_date"])) / ( 60 * 60 * 24), 0);
				}
				else
				{
					$number_of_days = round((strtotime($today) - strtotime($row["project_actual_opening_date"])) / ( 60 * 60 * 24), 0);
				}
			}
		}
		elseif($type == 2)
		{
			if($row["project_cost_cms_completion_date"] != NULL 
				and  $row["project_cost_cms_completion_date"] != '0000-00-00'
				and ($row["project_cost_cms_approved_date"] == NULL 
					 or $row["project_cost_cms_approved_date"] == '0000-00-00')
			)
			{
				$number_of_days = round((strtotime($today) - strtotime($row["project_cost_cms_completion_date"])) / ( 60 * 60 * 24), 0);
			}
		}
	}

	return $number_of_days;
}


/***********************************************************************
    get overdue in days for cms completion and cms approval
************************************************************************/
function get_cms_completion_days($project_id = 0, $type = 1)
{
	$number_of_days = "";
	
	$sql = "select project_actual_opening_date, project_cost_cms_completion_date, " . 
		   " project_cost_cms_approved_date, project_cost_cms_controlled_date " . 
		   " from projects " . 
		   " left join project_costs on project_cost_order = project_order " . 
		   " where project_id = " . dbquote($project_id);

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		if($type == 1)
		{
			if($row["project_actual_opening_date"] != NULL 
				and  $row["project_actual_opening_date"] != '0000-00-00'
				and $row["project_cost_cms_completion_date"] != NULL 
				and $row["project_cost_cms_completion_date"] != '0000-00-00'
			)
			{
				$number_of_days = round((strtotime($row["project_cost_cms_completion_date"]) - strtotime($row["project_actual_opening_date"])) / ( 60 * 60 * 24), 0);
			}
		}
		elseif($type == 2)
		{
			if($row["project_actual_opening_date"] != NULL 
				and  $row["project_actual_opening_date"] != '0000-00-00'
				and $row["project_cost_cms_approved_date"] != NULL 
				and $row["project_cost_cms_approved_date"] != '0000-00-00'
			)
			{
				$number_of_days = round((strtotime($row["project_cost_cms_approved_date"]) - strtotime($row["project_actual_opening_date"])) / ( 60 * 60 * 24), 0);
			}
		}
		elseif($type == 3)
		{
			if($row["project_cost_cms_completion_date"] != NULL 
				and  $row["project_cost_cms_completion_date"] != '0000-00-00'
				and $row["project_cost_cms_approved_date"] != NULL 
				and $row["project_cost_cms_approved_date"] != '0000-00-00'
				and $row["project_cost_cms_controlled_date"] != NULL 
				and $row["project_cost_cms_controlled_date"] != '0000-00-00'
			)
			{
				
				if($row["project_cost_cms_completion_date"] < $row["project_cost_cms_approved_date"])
				{
					$number_of_days = round((strtotime($row["project_cost_cms_approved_date"]) - strtotime($row["project_actual_opening_date"])) / ( 60 * 60 * 24), 0);
				}
				else
				{
					$number_of_days = round((strtotime($row["project_cost_cms_completion_date"]) - strtotime($row["project_actual_opening_date"])) / ( 60 * 60 * 24), 0);
				}
				
			}
		}

	}

	return $number_of_days;
}



/***********************************************************************
    check if LN is needed
************************************************************************/
function get_ln_needed($order_id = 0)
{
	$ln_needed = 0;

	//get prject data
	$sql = "select project_id, project_postype, project_projectkind, project_cost_type  ". 
		   " from projects " .
		   " left join project_costs on project_cost_order = project_order " . 
		   " where project_order = " . $order_id;
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		
		
		if($row["project_cost_type"] == 1)
		{
			//check cer data
			$sql_c = "select ln_no_ln_submission_needed " . 
				   " from ln_basicdata " . 
				   " where ln_basicdata_version = 0 " . 
				   " and ln_basicdata_project = " . dbquote($row["project_id"]);

			$res_c = mysql_query($sql_c) or dberror($sql_c);
			if ($row_c = mysql_fetch_assoc($res_c))
			{
				
				if($row_c["ln_no_ln_submission_needed"] == 1)
				{
					$ln_needed = 0;
				}
				else
				{
					$ln_needed = 1;
				}

			}
		}
		else
		{
			$ln_needed = 0;
		}
	}

	return $ln_needed;

}

/***********************************************************************
    check if AF/CER is needed
************************************************************************/
function get_cer_needed($order_id = 0)
{
	$cer_needed = 0;

	//get prject data
	$sql = "select project_id, project_postype, project_projectkind, project_cost_type  ". 
		   " from projects " .
		   " left join project_costs on project_cost_order = project_order " . 
		   " where project_order = " . $order_id;
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$sql_c = "select posproject_type_needs_cer, posproject_type_needs_af " . 
			     " from posproject_types " . 
			     " where posproject_type_postype = " . dbquote($row["project_postype"]) . 
			     " and posproject_type_projectcosttype = " . dbquote($row["project_cost_type"]) .
			     " and posproject_type_projectkind = " . dbquote($row["project_projectkind"]);

		$res_c = mysql_query($sql_c) or dberror($sql_c);
		if ($row_c = mysql_fetch_assoc($res_c))
		{
			if($row["project_cost_type"] == 1)
			{
				$cer_needed = $row_c["posproject_type_needs_cer"];
			}
			else
			{
				$cer_needed = $row_c["posproject_type_needs_af"];
			}
		}


		//check cer data
		$sql_c = "select cer_basicdata_no_cer_submission_needed " . 
			   " from cer_basicdata " . 
			   " where cer_basicdata_version = 0 " . 
			   " and cer_basicdata_project = " . dbquote($row["project_id"]);

		$res_c = mysql_query($sql_c) or dberror($sql_c);
		if ($row_c = mysql_fetch_assoc($res_c))
		{
			if($row_c["cer_basicdata_no_cer_submission_needed"] == 1)
			{
				$cer_needed = 0;
			}
			else
			{
				$cer_needed = 1;
			}
		}
	}


	return $cer_needed;

}


/***********************************************************************
    get pos environment
************************************************************************/
function get_pos_environment($order_id = 0)
{
	$data = "";
	$tmp = array();

	$posaddress_id = 0;
	$source_table = "posaddresses";

	//check pipeline
	$sql = "select posorder_parent_table, posorder_posaddress " . 
		   " from posorderspipeline " . 
		   " where posorder_order = " . $order_id;


	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$posaddress_id = $row["posorder_posaddress"];
		$source_table = $row["posorder_parent_table"];

	}
	else
	{
		$sql = "select posorder_posaddress " . 
			   " from posorders " . 
			   " where posorder_order = " . $order_id;

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$posaddress_id = $row["posorder_posaddress"];
		}
	}
	
	if($posaddress_id > 0)
	{
		if($source_table == "posaddresses")
		{
			$sql = "select posareatype_name " . 
				   " from posareas " . 
				   " left join posareatypes on posareatype_id = posarea_area " . 
				   " where posarea_posaddress = " . $posaddress_id;
	
		}
		else
		{
			$sql = "select posareatype_name " . 
				   " from posareaspipeline " . 
				   " left join posareatypes on posareatype_id = posarea_area " . 
				   " where posarea_posaddress = " . $posaddress_id;
		}
		
		
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$tmp[] = $row["posareatype_name"];
		}
	}

	if(count($tmp) > 0)
	{
		$data = implode(', ', $tmp);
	}

	return $data;
}

/***********************************************************************
    get infos for the project treatment state "on hold"
************************************************************************/
function get_onhold_info_state($project_id = 0, $infotype = '')
{
	$data = "";
	

	//check pipeline
	$sql = "select projecttracking_comment, projecttracking_time " . 
		   " from projecttracking " . 
		   " where projecttracking_project_id = " . $project_id . 
		   " and projecttracking_field = 'project_state'" .
		   " and projecttracking_newvalue = 'on hold' " .
	       " order by projecttracking_time DESC";


	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		if($infotype == 'date')
		{
			$data = substr($row["projecttracking_time"], 0, 10);
		}
		elseif($infotype == 'reason')
		{
			$data = $row["projecttracking_comment"];
		}
	}

	return $data;
}


/***********************************************************************
    get project duration
************************************************************************/
function get_project_duration($order_id = 0, $type = "")
{
	$duration = "";
	
	if($type == 1) //LN approval: (proj.submission - LN approval
	{
		$sql = "select order_id, order_date, project_id " . 
               " from orders " .
			   " left join projects on project_order = order_id " . 
			   " where order_id = " . $order_id;

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$startdate = $row["order_date"];

			$enddate = "";
			$sql = "select project_milestone_date " . 
				   " from project_milestones " . 
				   " where project_milestone_project = " . $row["project_id"] .
				   " and project_milestone_milestone in (10, 13)";
			
			$res2 = mysql_query($sql) or dberror($sql);
			while ($row2 = mysql_fetch_assoc($res2))
			{
				if($row2["project_milestone_date"] != NULL 
				and $row2["project_milestone_date"] != '0000-00-00')
				{
					$enddate = $row2["project_milestone_date"];

				}
			}

			if ($startdate != "" and $enddate != ""
			   and $startdate <= $enddate)
			{
				$date1 = new DateTime(substr($startdate, 0, 10));
				$date2 = new DateTime(substr($enddate, 0, 10));

				$duration = $date2->diff($date1)->format("%a");
			}
		}
	}
	elseif($type == 2) //LAYOUT REQUEST (LN approval - step 210)
	{
		$startdate = "";
		$enddate = "";

		$sql = "select order_id, order_date, project_id " . 
               " from orders " .
			   " left join projects on project_order = order_id " . 
			   " where order_id = " . $order_id;

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$sql = "select project_milestone_date " . 
				   " from project_milestones " . 
				   " where project_milestone_project = " . $row["project_id"] .
				   " and project_milestone_milestone in (10, 13)";
			
			$res2 = mysql_query($sql) or dberror($sql);
			while ($row2 = mysql_fetch_assoc($res2))
			{
				if($row2["project_milestone_date"] != NULL 
				and $row2["project_milestone_date"] != '0000-00-00')
				{
					$startdate = $row2["project_milestone_date"];

				}
			}
			
			// get date from step 210
			$sql = "select date_created " .
				   " from actual_order_states " . 
				   " where actual_order_state_state in (4) " . 
				   " and actual_order_state_order = " . $row["order_id"] .
				   " order by date_created asc";
			$res2 = mysql_query($sql) or dberror($sql);
			if ($row2 = mysql_fetch_assoc($res2))
			{
				$enddate = $row2["date_created"];
			}

			if ($startdate != "" and $enddate != ""
			   and $startdate <= $enddate)
			{
				$date1 = new DateTime(substr($startdate, 0, 10));
				$date2 = new DateTime(substr($enddate, 0, 10));

				$duration = $date2->diff($date1)->format("%a");
			}
		}
	}
	elseif($type == 3) //HQ LAAYOUT PREPARATION (step 210 - step 350)
	{
		$startdate = "";
		$enddate = "";

		$sql = "select order_id, order_date, project_id " . 
               " from orders " .
			   " left join projects on project_order = order_id " . 
			   " where order_id = " . $order_id;

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			// get date from step 210
			$sql = "select date_created " .
				   " from actual_order_states " . 
				   " where actual_order_state_state in (4) " . 
				   " and actual_order_state_order = " . $row["order_id"] .
				   " order by date_created asc";
			$res2 = mysql_query($sql) or dberror($sql);
			if ($row2 = mysql_fetch_assoc($res2))
			{
				$startdate = $row2["date_created"];
			}


			// get date from step 350
			$sql = "select date_created " .
				   " from actual_order_states " . 
				   " where actual_order_state_state in (17) " . 
				   " and actual_order_state_order = " . $row["order_id"] .
				   " order by date_created asc";
			$res2 = mysql_query($sql) or dberror($sql);
			if ($row2 = mysql_fetch_assoc($res2))
			{
				$enddate = $row2["date_created"];
			}

			if ($startdate != "" and $enddate != ""
			   and $startdate <= $enddate)
			{
				$date1 = new DateTime(substr($startdate, 0, 10));
				$date2 = new DateTime(substr($enddate, 0, 10));

				$duration = $date2->diff($date1)->format("%a");
			}
		}
	}
	elseif($type == 4) //HQ MINI BOOKLET PREPARATION (step 350 - step 417)
	{
		$startdate = "";
		$enddate = "";

		$sql = "select order_id, order_date, project_id " . 
               " from orders " .
			   " left join projects on project_order = order_id " . 
			   " where order_id = " . $order_id;

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			// get date from step 350
			$sql = "select date_created " .
				   " from actual_order_states " . 
				   " where actual_order_state_state in (17) " . 
				   " and actual_order_state_order = " . $row["order_id"] .
				   " order by date_created asc";
			$res2 = mysql_query($sql) or dberror($sql);
			if ($row2 = mysql_fetch_assoc($res2))
			{
				$startdate = $row2["date_created"];
			}


			// get date from step 417
			$sql = "select date_created " .
				   " from actual_order_states " . 
				   " where actual_order_state_state in (81) " . 
				   " and actual_order_state_order = " . $row["order_id"] .
				   " order by date_created asc";
			$res2 = mysql_query($sql) or dberror($sql);
			if ($row2 = mysql_fetch_assoc($res2))
			{
				$enddate = $row2["date_created"];
			}

			if ($startdate != "" and $enddate != ""
			   and $startdate <= $enddate)
			{
				$date1 = new DateTime(substr($startdate, 0, 10));
				$date2 = new DateTime(substr($enddate, 0, 10));

				$duration = $date2->diff($date1)->format("%a");
			}
		}
	}
	elseif($type == 5) //HQ BOOKLET PREPARATION (step 417 - step 441)
	{
		$startdate = "";
		$enddate = "";

		$sql = "select order_id, order_date, project_id " . 
               " from orders " .
			   " left join projects on project_order = order_id " . 
			   " where order_id = " . $order_id;

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			// get date from step 417
			$sql = "select date_created " .
				   " from actual_order_states " . 
				   " where actual_order_state_state in (81) " . 
				   " and actual_order_state_order = " . $row["order_id"] .
				   " order by date_created asc";
			$res2 = mysql_query($sql) or dberror($sql);
			if ($row2 = mysql_fetch_assoc($res2))
			{
				$startdate = $row2["date_created"];
			}


			// get date from step 441
			$sql = "select date_created " .
				   " from actual_order_states " . 
				   " where actual_order_state_state in (84) " . 
				   " and actual_order_state_order = " . $row["order_id"] .
				   " order by date_created asc";
			$res2 = mysql_query($sql) or dberror($sql);
			if ($row2 = mysql_fetch_assoc($res2))
			{
				$enddate = $row2["date_created"];
			}

			if ($startdate != "" and $enddate != ""
			   and $startdate <= $enddate)
			{
				$date1 = new DateTime(substr($startdate, 0, 10));
				$date2 = new DateTime(substr($enddate, 0, 10));

				$duration = $date2->diff($date1)->format("%a");
			}
		}
	}
	elseif($type == 6) //BUDGET PREPARATION HQ SUPPLIED ITEMS (step 441 - step 560)
	{
		$startdate = "";
		$enddate = "";

		$sql = "select order_id, order_date, project_id " . 
               " from orders " .
			   " left join projects on project_order = order_id " . 
			   " where order_id = " . $order_id;

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			// get date from step 441
			$sql = "select date_created " .
				   " from actual_order_states " . 
				   " where actual_order_state_state in (84) " . 
				   " and actual_order_state_order = " . $row["order_id"] .
				   " order by date_created asc";
			$res2 = mysql_query($sql) or dberror($sql);
			if ($row2 = mysql_fetch_assoc($res2))
			{
				$startdate = $row2["date_created"];
			}


			// get date from step 560
			$sql = "select date_created " .
				   " from actual_order_states " . 
				   " where actual_order_state_state in (31) " . 
				   " and actual_order_state_order = " . $row["order_id"] .
				   " order by date_created asc";
			$res2 = mysql_query($sql) or dberror($sql);
			if ($row2 = mysql_fetch_assoc($res2))
			{
				$enddate = $row2["date_created"];
			}

			if($enddate == NULL or $enddate == '0000-00-00')
			{
				// get date from step 600
				$sql = "select date_created " .
					   " from actual_order_states " . 
					   " where actual_order_state_state in (32) " . 
					   " and actual_order_state_order = " . $row["order_id"] .
					   " order by date_created asc";
				$res2 = mysql_query($sql) or dberror($sql);
				if ($row2 = mysql_fetch_assoc($res2))
				{
					$enddate = $row2["date_created"];
				}
			}

			if ($startdate != "" and $enddate != ""
			   and $startdate <= $enddate)
			{
				$date1 = new DateTime(substr($startdate, 0, 10));
				$date2 = new DateTime(substr($enddate, 0, 10));

				$duration = $date2->diff($date1)->format("%a");
			}
		}
	}
	elseif($type == 7) //BUDGET APPROVAL HQ SUPPLIED ITEMS (step 560 - 620)
	{
		$startdate = "";
		$enddate = "";

		$sql = "select order_id, order_date, project_id " . 
               " from orders " .
			   " left join projects on project_order = order_id " . 
			   " where order_id = " . $order_id;

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			// get date from step 560
			$sql = "select date_created " .
				   " from actual_order_states " . 
				   " where actual_order_state_state in (31) " . 
				   " and actual_order_state_order = " . $row["order_id"] .
				   " order by date_created asc";
			$res2 = mysql_query($sql) or dberror($sql);
			if ($row2 = mysql_fetch_assoc($res2))
			{
				$startdate = $row2["date_created"];
			}


			if($startdate == NULL or $startdate == '0000-00-00')
			{
				// get date from step 600
				$sql = "select date_created " .
					   " from actual_order_states " . 
					   " where actual_order_state_state in (32) " . 
					   " and actual_order_state_order = " . $row["order_id"] .
					   " order by date_created asc";
				$res2 = mysql_query($sql) or dberror($sql);
				if ($row2 = mysql_fetch_assoc($res2))
				{
					$startdate = $row2["date_created"];
				}
			}


			// get date from step 620
			$sql = "select date_created " .
				   " from actual_order_states " . 
				   " where actual_order_state_state in (34) " . 
				   " and actual_order_state_order = " . $row["order_id"] .
				   " order by date_created asc";
			$res2 = mysql_query($sql) or dberror($sql);
			if ($row2 = mysql_fetch_assoc($res2))
			{
				$enddate = $row2["date_created"];
			}

			if ($startdate != "" and $enddate != ""
			   and $startdate <= $enddate)
			{
				$date1 = new DateTime(substr($startdate, 0, 10));
				$date2 = new DateTime(substr($enddate, 0, 10));

				$duration = $date2->diff($date1)->format("%a");
			}
		}
	}
	elseif($type == 8) //DELIVERIES OF HQ SUPPLIED ITEMS (step 620 - step 800)
	{
		$startdate = "";
		$enddate = "";

		$sql = "select order_id, order_date, project_id " . 
               " from orders " .
			   " left join projects on project_order = order_id " . 
			   " where order_id = " . $order_id;

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			// get date from step 620
			$sql = "select date_created " .
				   " from actual_order_states " . 
				   " where actual_order_state_state in (34) " . 
				   " and actual_order_state_order = " . $row["order_id"] .
				   " order by date_created asc";
			$res2 = mysql_query($sql) or dberror($sql);
			if ($row2 = mysql_fetch_assoc($res2))
			{
				$startdate = $row2["date_created"];
			}


			//check if all order items have an arrival date
			$all_items_arrived = true;
			$sql = "select count(order_item_id) as num_recs " .
				   " from order_items " . 
				   " where order_item_order = " . $row["order_id"] .
				   " and (order_item_arrival is null " . 
				   " or order_item_arrival = '0000-00-00') " .
				   " and order_item_type in (1,2) ";
			$res2 = mysql_query($sql) or dberror($sql);
			if ($row2 = mysql_fetch_assoc($res2))
			{
				if($row["num_recs"] > 0)
				{
					$all_items_arrived = false;
				}
			}

			$enddate = "";
			if($all_items_arrived == true) // state 800, delivery confirmed
			{
				$sql = "select date_created " .
					   " from actual_order_states " . 
					   " where actual_order_state_state in (41) " . 
					   " and actual_order_state_order = " . $row["order_id"] .
					   " order by date_created desc";
				$res2 = mysql_query($sql) or dberror($sql);
				if ($row2 = mysql_fetch_assoc($res2))
				{
					$enddate = $row2["date_created"];
				}
			}

			if ($startdate != "" and $enddate != ""
			   and $startdate <= $enddate)
			{
				$date1 = new DateTime(substr($startdate, 0, 10));
				$date2 = new DateTime(substr($enddate, 0, 10));

				$duration = $date2->diff($date1)->format("%a");
			}
		}
	}
	elseif($type == 9) //FULL BUDGET APPROVAL (step 620 - step 840)
	{
		$startdate = "";
		$enddate = "";

		$sql = "select order_id, order_date, project_id " . 
               " from orders " .
			   " left join projects on project_order = order_id " . 
			   " where order_id = " . $order_id;

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			// get date from step 620
			$sql = "select date_created " .
				   " from actual_order_states " . 
				   " where actual_order_state_state in (34) " . 
				   " and actual_order_state_order = " . $row["order_id"] .
				   " order by date_created asc";
			$res2 = mysql_query($sql) or dberror($sql);
			if ($row2 = mysql_fetch_assoc($res2))
			{
				$startdate = $row2["date_created"];
			}


			// get date from step 840
			$sql = "select date_created " .
				   " from actual_order_states " . 
				   " where actual_order_state_state in (87) " . 
				   " and actual_order_state_order = " . $row["order_id"] .
				   " order by date_created asc";
			$res2 = mysql_query($sql) or dberror($sql);
			if ($row2 = mysql_fetch_assoc($res2))
			{
				$enddate = $row2["date_created"];
			}

			if ($startdate != "" and $enddate != ""
			   and $startdate <= $enddate)
			{
				$date1 = new DateTime(substr($startdate, 0, 10));
				$date2 = new DateTime(substr($enddate, 0, 10));

				$duration = $date2->diff($date1)->format("%a");
			}
		}
	}
	elseif($type == 10) //CER-APPROVAL (step 840 - milestone CER approved)
	{
		$startdate = "";
		$enddate = "";

		$sql = "select order_id, order_date, project_id " . 
               " from orders " .
			   " left join projects on project_order = order_id " . 
			   " where order_id = " . $order_id;

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			// get date from step 840
			$sql = "select date_created " .
				   " from actual_order_states " . 
				   " where actual_order_state_state in (87) " . 
				   " and actual_order_state_order = " . $row["order_id"] .
				   " order by date_created asc";
			$res2 = mysql_query($sql) or dberror($sql);
			if ($row2 = mysql_fetch_assoc($res2))
			{
				$startdate = $row2["date_created"];
			}

			//cer aproval above 50K and below 50K
			$sql = "select project_milestone_date " . 
				   " from project_milestones " . 
				   " where project_milestone_project = " . $row["project_id"] .
				   " and project_milestone_milestone in (12, 21)";
			
			$res2 = mysql_query($sql) or dberror($sql);
			while ($row2 = mysql_fetch_assoc($res2))
			{
				if($row2["project_milestone_date"] != NULL 
				and $row2["project_milestone_date"] != '0000-00-00')
				{
					$enddate = $row2["project_milestone_date"];

				}
			}

			if ($startdate != "" and $enddate != ""
			   and $startdate <= $enddate)
			{
				$date1 = new DateTime(substr($startdate, 0, 10));
				$date2 = new DateTime(substr($enddate, 0, 10));

				$duration = $date2->diff($date1)->format("%a");
			}
		}
	}
	elseif($type == 11) //DURATION: project submission - actual opening date 
	{
		$startdate = "";
		$enddate = "";

		$sql = "select order_id, order_date, project_id, project_actual_opening_date " . 
               " from orders " .
			   " left join projects on project_order = order_id " . 
			   " where order_id = " . $order_id;

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$startdate = $row["order_date"];
			$enddate =  $row["project_actual_opening_date"];

			if ($startdate != "" and $enddate != ""
			   and $startdate <= $enddate)
			{
				$date1 = new DateTime(substr($startdate, 0, 10));
				$date2 = new DateTime(substr($enddate, 0, 10));

				$duration = $date2->diff($date1)->format("%a");
			}
		}
	}
	elseif($type == 12) //DURATION: CMS completion (actual opening date - CMS completion)
	{
		$startdate = "";
		$enddate = "";

		$sql = "select order_date, project_actual_opening_date, project_cost_cms_completion_date " . 
               " from orders " . 
			   " left join projects on project_order = order_id " . 
			   " left join project_costs on project_cost_order = order_id " . 
			   " where order_id = " . $order_id;

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			if($row["project_actual_opening_date"] != NULL 
				and $row["project_actual_opening_date"] != '0000-00-00'
			    and $row["project_cost_cms_completion_date"] != NULL 
				and $row["project_cost_cms_completion_date"] != '0000-00-00')
			{
				$date1 = new DateTime($row["project_actual_opening_date"]);
				$date2 = new DateTime($row["project_cost_cms_completion_date"]);

				$duration = $date2->diff($date1)->format("%a");
			}
		}
	}
	elseif($type == 13) //DURATION: CMS approval(actual opening date - CMS approval)
	{
		$startdate = "";
		$enddate = "";

		$sql = "select order_date, project_actual_opening_date, project_cost_cms_approved_date " . 
               " from orders " . 
			   " left join projects on project_order = order_id " . 
			   " left join project_costs on project_cost_order = order_id " . 
			   " where order_id = " . $order_id;

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			if($row["project_actual_opening_date"] != NULL 
				and $row["project_actual_opening_date"] != '0000-00-00'
			    and $row["project_cost_cms_approved_date"] != NULL 
				and $row["project_cost_cms_approved_date"] != '0000-00-00')
			{
				$date1 = new DateTime($row["project_actual_opening_date"]);
				$date2 = new DateTime($row["project_cost_cms_approved_date"]);

				$duration = $date2->diff($date1)->format("%a");
			}
		}
	}
	return $duration;

}

/***********************************************************************
    get information about POS Images
************************************************************************/
function get_pos_images($order_id = 0)
{
	$image_info = array();
	$number_of_images = 0;
	$number_of_invalid_images = 0;
	$number_of_invalid_file_types = 0;

	$sql = "select order_file_path, order_file_type from order_files " . 
		   " where order_file_category = 11 " . 
		   " and order_file_order = " . dbquote($order_id);

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		if(strpos(strtolower($row["order_file_path"]), '.jpg'))
		{
			$number_of_images++;
		}
		elseif(strpos(strtolower($row["order_file_path"]), '.jpeg'))
		{
			$number_of_images++;
		}
		elseif(strpos(strtolower($row["order_file_path"]), '.png'))
		{
			$number_of_images++;
		}
		else
		{
			$number_of_invalid_images++;
		}

		if($row["order_file_type"] != 2)
		{
			$number_of_invalid_file_types++;
		}
	}

	if($number_of_images > 0)
	{
		if($number_of_images == 1)
		{
			$image_info[] = $number_of_images . " image";
		}
		else
		{
			$image_info[] = $number_of_images . " images";
		}
	}

	if($number_of_invalid_images > 0)
	{
		if($number_of_invalid_images == 1)
		{
			$image_info[] = $number_of_invalid_images . " invalid image";
		}
		else
		{
			$image_info[] = $number_of_invalid_images . " invalid images";
		}
	}

	if($number_of_invalid_file_types > 0)
	{
		if($number_of_invalid_file_types == 1)
		{
			$image_info[] = $number_of_invalid_file_types . " invalid type";
		}
		else
		{
			$image_info[] = $number_of_invalid_file_types . " invalid types";
		}
	}

	if(count($image_info) > 0)
	{
		return(implode(', ', $image_info));
	}
	else
	{
		return "no images";
	}

}


/***********************************************************************
    create an  empty column
************************************************************************/
function create_comment_column($order_id = 0)
{
	return "";
}


?>