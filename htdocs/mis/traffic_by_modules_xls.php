<?php
/********************************************************************

    traffic_by_modules_xls.php

    Enter parameters for the query of traffic

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-05-17
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-05-17
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_queries");
require_once "../include/phpexcel/PHPExcel.php";


function cmp($a, $b) {
    if ($a == $b) {
        return 0;
    }
    return ($b < $a) ? -1 : 1;
}

/********************************************************************
    prepare all data needed
*********************************************************************/
$selected_year = date("Y");
if(param("year")) {
	$selected_year = param("year");
}

$selected_module = "user";
if(param("module")) {
	$selected_module = param("module");
}

$number_of_days = 365;
if($selected_year == date("Y"))
{
	$start = strtotime(date("Y") . '-01-01');
	$end = strtotime(date("Y-m-d"));
	$number_of_days = ceil(abs($end - $start) / 86400);

}


$modules = array();
$modules["archive"] = "Archive";
$modules["admin"] = "System Administration";
$modules["cer"] = "CER/AF";
$modules["mis"] = "Management Information";
$modules["pos"] = "POS Index";
$modules["administration"] = "Merchandising Planning System Administration";
$modules["mps"] = "Merchandising Planning Tissot";
$modules["red"] = "Retail Environment Development";
$modules["scpps"] = "Stock Contol and Production Planning";
$modules["sec"] = "Security";
$modules["user"] = "Projects and Catalogue Orders";
$modules["news"] = "Gazette";

$modules["lps"] = "Launch Planning Tissot";
$modules["catalog"] = "Supply Chain";

$stats = array();
$access_totals = 0;

$table_names = array();
//get all statsitic tables
$sql = 'SHOW TABLES FROM db_retailnet where Tables_in_db_retailnet like "statistic%"';
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$table_names[] = $row["Tables_in_db_retailnet"];
}

foreach($table_names as $key=>$table_name)
{

	$sql = "SELECT statistic_id, statistic_url " .
		   "FROM " . $table_name . 
		   " WHERE year(statistic_date) = " . $selected_year .
		   " and statistic_url like '%/" . $selected_module . "/%'" .
		   " and statistic_url not like '%login%'" .
		   " and statistic_url not like '%logoff%'" .
		   " order by statistic_url";
	
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		if(strpos($row["statistic_url"], '?') > 0)
		{
			$tmp = explode('?', $row["statistic_url"]);
			$script = $tmp[0];

		}
		else
		{
			$script = $row["statistic_url"];
		}
		
		$script = str_replace('/data/www/retailnet.tissot.ch/', '', $script);
		$script = str_replace('public/index.php/', '', $script);
		$script = str_replace('public/index.php/', '', $script);


		$script = trim(str_replace(range(0,9),'',$script));
		//$script = str_replace($selected_module . "/", '', $script);
		$script = str_replace('.php', '', $script);
		$script = str_replace('http://retailnet.tissot.ch', '', $script);
		$script = str_replace('https://retailnet.tissot.ch', '', $script);

		if(array_key_exists($script, $stats))
		{
			$stats[$script] = $stats[$script] + 1;
		}
		else
		{
			$stats[$script] = 1;
		}
		$access_totals++;
	}
}


uasort($stats, 'cmp');


/********************************************************************
    XLS Params
*********************************************************************/
$captions['A'] = "Script";
$captions['B'] = "Access Count";


$colwidth = array();
$colwidth['A'] = "15";
$colwidth['B'] = "15";

//output formats
$default_border = array(
    'style' => PHPExcel_Style_Border::BORDER_THIN,
    'color' => array('rgb'=>'1006A3')
);

$style_normal_border = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '1006A3'),
		),
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	)
);


$style_normal_border_right = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '1006A3'),
		),
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	),
);


$style_title = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
    'font' => array(
        'bold' => true,
		'size' => 16
    )
);

$style_header = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'font' => array(
        'bold' => true,
    )
);

$style_header_right = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	),
    'font' => array(
        'bold' => true,
    )
);

$style_bold = array(
	'font' => array(
        'bold' => true
    )
);



/********************************************************************
    Start output
*********************************************************************/
$objPHPExcel = new PHPExcel();

$logo = new PHPExcel_Worksheet_Drawing();
$logo->setName('Logo');
$logo->setDescription('Logo');
$logo->setPath('../pictures/brand_logo.jpg');
$logo->setHeight(36);
$logo->setWidth(113);

$sheet = $objPHPExcel->getActiveSheet();
$objPHPExcel->getActiveSheet()->setTitle('Traffic by Modules');

$logo->setWorksheet($objPHPExcel->getActiveSheet());


//default styles
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(10); 


// HEADRES ROW 1
$sheet->setCellValue('B1', 'Traffic ' . $modules[$selected_module] . ' (' . date("d.m.Y H:i:s") . ')');
$sheet->getStyle('B1')->applyFromArray( $style_title );
$sheet->getRowDimension('1')->setRowHeight(36);


$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(1);


$row_index = 3;


$access_info = "Acess Total " . $selected_year . " = " . 
               number_format ( $access_totals , 0 , '.' , "'" ) . 
			   " ("  . number_format( $access_totals/$number_of_days , 0 , '.' , "'" ) . " per day) ";

$sheet->setCellValueByColumnAndRow(0, $row_index, $access_info);

$row_index = 5;

foreach($stats as $script=>$num_recs)
{
	$cell_index = 0;
	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $script);
	if($colwidth['A'] < strlen($script)){$colwidth['A'] = strlen($script);}
	$cell_index++;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $num_recs);
	if($colwidth['B'] < strlen(number_format ( $num_recs , 0 , '.' , "'" ))){$colwidth['B'] = strlen(number_format ( $num_recs , 0 , '.' , "'" ));}
	

	$row_index++;

}


//Format column heights and witdhs
foreach($colwidth as $col=>$width) {
	$sheet->getColumnDimension($col)->setWidth($width);
}


/********************************************************************
    Start output
*********************************************************************/
$filename = 'traffic_by_module_' . $selected_module . '_'. date('Ymd H:i:s') . '.xls';
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
header('Pragma: ');

//send data
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');


?>