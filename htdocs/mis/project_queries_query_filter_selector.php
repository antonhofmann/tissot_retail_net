<?php
/********************************************************************

    project_queries_query_filter_selector.php

    Enter Filter Criteria for the Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2014-09-14
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2014-09-14
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../include/page_modal.php";

if(!has_access("can_perform_all_queries") 
  and !has_access("can_perform_projects_query"))
{
	redirect("noaccess.php");
}
require_once "include/projects_query_check_access.php";

require_once "include/projects_query_get_functions.php";

/********************************************************************
    prepare all data needed
*********************************************************************/
$query_id = param("query_id");

//get user_country access
$restricted_countries = array();
$sql = "select * from country_access " .
	   "where country_access_user = " . user_id();
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{            
	$restricted_countries[] = $row["country_access_country"];
}



if(count($restricted_countries) > 0)
{

	$sql_salesregion = "select DISTINCT salesregion_id, salesregion_name ".
		               "from countries " .
		               "left join salesregions on salesregion_id = country_salesregion " . 
		               "where country_id IN (" . implode(",", $restricted_countries) . ") " . 
					   "order by salesregion_name";

	$sql_region = "select DISTINCT region_id, region_name ".
		          "from countries " .
		          "left join regions on region_id = country_region " . 
		          "where country_id IN (" . implode(",", $restricted_countries) . ") " . 
				  "order by region_name";
}
else
{
	$sql_salesregion = "select salesregion_id, salesregion_name ".
						"from salesregions order by salesregion_name";

	$sql_region = "select region_id, region_name ".
						"from regions order by region_name";
}






$filter = array();
//get benchmark parameters
$salesregions = array();
$regions = array();


$filter = get_query_filter($query_id);



if(!array_key_exists("fosd", $filter))
{
	$filter["fosd"] = ""; //from project development status
	$filter["tosd"] = ""; //to project development status
	$filter["fosl"] = ""; //from project logistic status
	$filter["tosl"] = ""; //to project logistic status
}


$sql_client_types = "select client_type_id, client_type_code ".
					"from client_types";


$tmp_filter = "";
if(count($filter["ct"]) > 0)
{
	$clienttypes = explode("-", $filter["ct"]);
	foreach($clienttypes as $key=>$clienttype)
	{
		$tmp_filter = $tmp_filter . $clienttype . ",";
	}
	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

	if($tmp_filter)
	{
		$tmp_filter = "where client_type_id in (" . $tmp_filter . ") ";
	}	
	
}


$tmp_filter_re = "";
if(count($filter["re"]) > 0)
{
	$salesregions = explode("-", $filter["re"]);
	foreach($salesregions as $key=>$salesregion)
	{
		$tmp_filter_re = $tmp_filter_re . $salesregion . ",";
	}
	$tmp_filter_re = substr($tmp_filter_re, 0, strlen($tmp_filter_re) - 2);

	if($tmp_filter_re)
	{
		$tmp_filter_re = " country_salesregion in (" . $tmp_filter_re . ") ";
	}	
	
}


$tmp_filter_gr = "";
if(count($filter["gr"]) > 0)
{
	$regions = explode("-", $filter["gr"]);
	foreach($regions as $key=>$region)
	{
		$tmp_filter_gr = $tmp_filter_gr . $region . ",";
	}
	$tmp_filter_gr = substr($tmp_filter_gr, 0, strlen($tmp_filter_gr) - 2);

	if($tmp_filter_gr)
	{
		$tmp_filter_gr = " country_region in (" . $tmp_filter_gr . ") ";
	}
}


$tmp_filter = "";
if($tmp_filter_re and $tmp_filter_gr)
{
	$tmp_filter = "where " . $tmp_filter_re . " and " . $tmp_filter_gr; 
}
elseif($tmp_filter_re)
{
	$tmp_filter = "where " . $tmp_filter_re;
}
elseif($tmp_filter_gr)
{
	$tmp_filter = "where " . $tmp_filter_gr;
}

if($tmp_filter and count($restricted_countries) > 0)
{
	$tmp_filter .= " and country_id IN(" . implode(",", $restricted_countries) . ") ";
}
elseif(count($restricted_countries) > 0)
{
	$tmp_filter = "where country_id IN(" . implode(",", $restricted_countries) . ") ";
}



if($tmp_filter)
{
	$tmp_filter .= " and country_id > 0 ";
}
else
{
	$tmp_filter = " where country_id > 0 ";
}

$sql_country = "select DISTINCT country_id, country_name " .
			   "from orders " .
			   "left join countries on country_id = order_shop_address_country " .
			   $tmp_filter . 
			   " order by country_name";



$tmp_filter = "";
$tmp_filter1 = "";
$provice_country_filter = "";
if(count($filter["co"]) > 0)
{
	$countries = explode("-", $filter["co"]);
	
	foreach($countries as $key=>$country)
	{
		$tmp_filter = $tmp_filter . $country . ",";
	}

	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);
	
	$provice_country_filter = $tmp_filter;

	if($tmp_filter)
	{
		$tmp_filter = "where country_salesregion in (" .  $tmp_filter . ") ";
	}
	elseif(count($filter["re"]) > 0 and count($filter["gr"]) > 0)
	{
		$salesregions = explode("-", $filter["re"]);
		
		foreach($salesregions as $key=>$salesregion)
		{
			$tmp_filter = $tmp_filter . $salesregion . ",";
		}

		$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

		if($tmp_filter)
		{
			$tmp_filter = "where country_salesregion in (" .  $tmp_filter . ") ";
		}

		
		
		$regions = explode("-", $filter["gr"]);
		
		foreach($regions as $key=>$region)
		{
			$tmp_filter1 = $tmp_filter1 . $region . ",";
		}

		$tmp_filter1 = substr($tmp_filter1, 0, strlen($tmp_filter1) - 2);

		if($tmp_filter and $tmp_filter1)
		{
			$tmp_filter .= " and country_region in (" .  $tmp_filter . ") ";
		}
		elseif($tmp_filter1)
		{
			$tmp_filter = "where country_region in (" .  $tmp_filter . ") ";
		}
	}
	elseif(count($filter["re"]) > 0)
	{
		$salesregions = explode("-", $filter["re"]);
		
		foreach($salesregions as $key=>$salesregion)
		{
			$tmp_filter = $tmp_filter . $salesregion . ",";
		}

		$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

		if($tmp_filter)
		{
			$tmp_filter = "where country_salesregion in (" .  $tmp_filter . ") ";
		}
	}
	elseif(count($filter["gr"]) > 0)
	{
		$regions = explode("-", $filter["gr"]);
		
		foreach($regions as $key=>$region)
		{
			$tmp_filter = $tmp_filter . $region . ",";
		}

		$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

		if($tmp_filter)
		{
			$tmp_filter = "where country_region in (" .  $tmp_filter . ") ";
		}
	}
}



$sql_cities = "select DISTINCT place_id, place_name " .
			  "from places " .
			  "left join countries on country_id = place_country " .
			  $tmp_filter . 
			  " order by place_name";


$sql_areas = "select posareatype_id, posareatype_name ".
              "from posareatypes order by posareatype_name";

$sql_project_cost_types = "select project_costtype_id, project_costtype_text ".
						  "from project_costtypes " .
                          "order by project_costtype_text";





$sql_product_lines = "select DISTINCT product_line_id, product_line_name ".
                     "from projects " . 
					 " left join product_lines on product_line_id = project_product_line " .
					 " where product_line_id > 0 " .
                     "   order by product_line_name";




$tmp_filter = "";
if(count($filter["pl"]) > 0)
{
	$product_lines = explode("-", $filter["pl"]);
	$tmp_filter = "";
	foreach($product_lines as $key=>$product_line)
	{
		$tmp_filter = $tmp_filter . $product_line . ",";
	}
	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

	if($tmp_filter)
	{
		$tmp_filter = "where productline_subclass_productline in (" . $tmp_filter . ") ";
	}
}
$sql_furniture_subclasses = "select DISTINCT productline_subclass_id, productline_subclass_name ".
                     "from productline_subclass_productlines " .
								    " left join productline_subclasses on  productline_subclass_id = productline_subclass_productline_class_id " .
					 $tmp_filter .
                     "   order by productline_subclass_name";

$sql_pos_types = "select DISTINCT postype_id, postype_name ".
                     "from postypes " .
                     "order by postype_name";

$sql_possubclasses = "select possubclass_id, possubclass_name ".
                     "from possubclasses " .
                     "order by possubclass_name";

$sql_project_kinds = "select projectkind_id, projectkind_name ".
						 "from projectkinds " .
						 "where projectkind_id > 0 " .
						 "order by projectkind_name";


$sql_project_type_subclasses = "select project_type_subclass_id, project_type_subclass_name ".
						 "from project_type_subclasses " .
						 "order by project_type_subclass_name";

$distribution_channles = array();
$sql_distribution_channels = "select mps_distchannel_id, " . 
                             "concat(mps_distchannel_group_name, ' - ', mps_distchannel_name, ' - ', mps_distchannel_code) as distributionchannel ".
							"from mps_distchannels " .
							"left join mps_distchannel_groups on mps_distchannel_groups.mps_distchannel_group_id = mps_distchannels.mps_distchannel_group_id " .
							"order by mps_distchannel_group_name, mps_distchannel_name, mps_distchannel_code";


$design_objectives = array();
$sql_design_objectives = "select design_objective_item_id, design_objective_item_name, " .
						 "    design_objective_group_name, postype_name " .
						 "from design_objective_items left join design_objective_groups on design_objective_item_group = design_objective_group_id " .
						 "    left join postypes on design_objective_group_postype = postype_id " . 
						 " where postype_name <> '' " . 
						 " order by postype_name, design_objective_group_name, design_objective_item_name ";


$costmonitoring_groups = array();
$sql_costmonitoring_groups = "select costmonitoringgroup_id, costmonitoringgroup_text " . 
                             "from costmonitoringgroups " . 
							 " order by costmonitoringgroup_text";


$sql_rtcs = "select distinct user_id, concat(user_name, ' ', user_firstname) as username " . 
			  "from projects " .
			  " left join users on user_id = project_retail_coordinator " . 
			  " where user_id > 0 " .
			  " order by username";

if(count($filter["co"]) > 0)
{
	$countries = explode("-", $filter["co"]);
	
	foreach($countries as $key=>$country)
	{
		$tmp_filter = $tmp_filter . $country . ",";
	}

	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

	if($tmp_filter) {
		$tmp_filter = " and address_country in ( " . $tmp_filter . ") ";
	}

	
	$sql_lrtcs = "select distinct user_id, concat(user_name, ' ', user_firstname) as username " . 
                          "from projects " .
						  " left join users on user_id = project_local_retail_coordinator " . 
						  " where user_id > 0 " .
		                  $tmp_filter . 
						  " order by username";

}
else
{
	$sql_lrtcs = "select distinct user_id, concat(user_name, ' ', user_firstname) as username " . 
                          "from projects " .
						  " left join users on user_id = project_local_retail_coordinator " . 
		                  " where user_id > 0 " .
						  " order by username";
}

$sql_dsups = "select distinct user_id, concat(user_name, ' ', user_firstname) as username " . 
                          "from projects " .
						  " left join users on user_id = project_design_supervisor " .
						" where user_id > 0 " .
						  " order by username";

$sql_rtos = "select distinct user_id, concat(user_name, ' ', user_firstname) as username " . 
                          "from orders " .
						  " left join users on user_id = order_retail_operator " . 
						  " where order_type = 1 " . 
						  " and user_id > 0 " .
						  " order by username";


$sql_dcontrs = "select distinct user_id, concat(user_name, ' ', user_firstname) as username " . 
                          "from projects " .
						  " left join users on user_id = project_design_contractor " . 
						  " where user_id > 0 " .
						  " order by username";


$filter_items = '';
if(count($filter["cmgr"]))
{
	$tmp_filter = '';
	$tmp = explode("-", $filter["cmgr"]);
	foreach($tmp as $key=>$value)
	{
		if($value)
		{
			$tmp_filter .= $value . ",";
		}
	}
	
	$tmp_filter = "(" . substr($tmp_filter, 0, strlen($tmp_filter)-1) . ")";
	if($tmp_filter != "()")
	{
		$filter_items .= " and item_costmonitoring_group IN " . $tmp_filter . "";
	}
}

if(count($filter["icat"]))
{
	$tmp_filter = '';
	$tmp = explode("-", $filter["icat"]);
	foreach($tmp as $key=>$value)
	{
		if($value)
		{
			$tmp_filter .= $value . ",";
		}
	}
	
	$tmp_filter = "(" . substr($tmp_filter, 0, strlen($tmp_filter)-1) . ")";
	if($tmp_filter != "()")
	{
		$filter_items .= " and item_category IN " . $tmp_filter . "";
	}
}


$productline_filter = '';
if($filter["pl"])
{
	$tmp_filter = '';
	$tmp = explode("-", $filter["pl"]);

	foreach($tmp as $key=>$value)
	{
		if($value)
		{
			$tmp_filter .= $value . ",";
		}
	}


	
	$tmp_filter = "(" . substr($tmp_filter, 0, strlen($tmp_filter)-1) . ")";
	if($tmp_filter != "()")
	{
		$filter_items .= " and project_product_line IN " . $tmp_filter . "";
	}

	$productline_filter = " and project_product_line IN " . $tmp_filter . "";

}



$supplier_filter = '';
if($filter["supp"] > 0)
{
	$filter_items .= " and supplier_address = " . $filter["supp"];

	$supplier_filter = " and supplier_address = " . $filter["supp"];
}



$tmp_filter = "";
if($productline_filter)
{
	$tmp_filter .= $productline_filter;
}
if($supplier_filter)
{
	$tmp_filter .= $supplier_filter;
}

$sql_item_category = "select DISTINCT item_category_id, item_category_name " . 
					 "from item_categories  " .
					 " order by item_category_name";



$item_categories = array();
$sql_item_categories = "select DISTINCT item_category_id, item_category_name " . 
					 "from order_items  " .
					 "left join projects on project_order = order_item_order " . 
					 " left join items on item_id = order_item_item " .
					 "left join suppliers on supplier_item = item_id ".
					 " left join item_categories on item_category_id = item_category " . 
					 "where item_type < 3 and item_category_id > 0 " . 
					 $tmp_filter . 
					 " order by item_category_name";



$filter_items2 = "";
if(!$filter_items)
{
	
	$categories = array();
	$sql_c = "select item_category_id ".
			 "from item_categories " .
	         " order by item_category_name";
	
	$res = mysql_query($sql_c) or dberror($sql_c);
	while ($row = mysql_fetch_assoc($res))
	{
		$categories[$row["item_category_id"]] = $row["item_category_id"];
	}
	
	if(count($categories))
	{
		$filter_items2 .= " and item_category IN (" . implode(',', $categories)  . ") ";
	}

	if(array_key_exists("selected_item_category", $_POST) and $_POST["selected_item_category"] > 0)
	{
		$filter_items2 .= " and item_category IN (" . $_POST["selected_item_category"]  . ") ";
	}
}



$items_filter = $filter["item"]; // Items
$items_filter = substr($items_filter,0, strlen($items_filter)-1); // remove last comma
$items_filter = str_replace("-", ",", $items_filter);


$tmp_filter = '';
if(!$items_filter and !$filter_items and !$filter_items2)
{
	$tmp_filter = " and (item_id IN (" . $items_filter . ")) ";

	$sql_items = "select DISTINCT item_id, concat(item_code, ' ', item_name) as item_name " . 
             " from order_items " .
		     " left join items on item_id = order_item_item " . 
		     " left join projects on project_order = order_item_order " .
			 " left join suppliers on supplier_item = item_id " . 
			 " where item_type = 1 " . 
			 $tmp_filter .
			 " order by item_code";
}
elseif($items_filter)
{
	$sql_items = "select DISTINCT item_id, concat(item_code, ' ', item_name) as item_name " . 
             " from order_items " .
		     " left join items on item_id = order_item_item " .
		     " left join projects on project_order = order_item_order " .
			 " left join suppliers on supplier_item = item_id " . 
			 " where (item_type = 1 " . 
			 $filter_items .
			 $filter_items2 . ") " . 
			 " or item_id in (" . $items_filter . ") " . 
			 " order by item_code";
}
else
{
	$sql_items = "select DISTINCT item_id, concat(item_code, ' ', item_name) as item_name " . 
             " from order_items " .
		     " left join items on item_id = order_item_item " .
		     " left join projects on project_order = order_item_order " . 
			 " left join suppliers on supplier_item = item_id " . 
			 " where item_type = 1 " . 
			 $filter_items .
			 $filter_items2 .
			 " order by item_code";

}


$sql_project_states = "select project_state_id, project_state_text " . 
                        " from project_states " . 
						" order by project_state_text";


/********************************************************************
    save data
*********************************************************************/
if(param("save_form") != '')
{
	$new_filter = array();


	$new_filter["cl"] =  $filter["cl"]; //client
	$new_filter["fr"] =  $filter["fr"]; //owner
	$new_filter["supp"] =  $filter["supp"]; //supplier
	$new_filter["forw"] =  $filter["forw"]; //forwarder
	$new_filter["hs"] =  $filter["hs"]; //handling state
	$new_filter["fos"] =  $filter["fos"]; //from global project status
	$new_filter["tos"] =  $filter["tos"]; //to global project status
	$new_filter["fosd"] =  $filter["fosd"]; //from project development status
	$new_filter["tosd"] =  $filter["tosd"]; //to project development status
	$new_filter["fosl"] =  $filter["fosl"]; //from project logistic status
	$new_filter["tosl"] =  $filter["tosl"]; //to project logistic status
	$new_filter["sufrom"] =  $filter["sufrom"]; // submitted from
	$new_filter["suto"] =  $filter["suto"]; // submitted to
	$new_filter["opfrom"] =  $filter["opfrom"]; // opened from
	$new_filter["opto"] =  $filter["opto"]; // opened to
	$new_filter["agrfrom"] =  $filter["agrfrom"]; // agreed opening from
	$new_filter["agrto"] =  $filter["agrto"]; // agreed opening to
	$new_filter["clfrom"] =  $filter["clfrom"]; // closed from
	$new_filter["clto"] =  $filter["clto"]; // closed to
	$new_filter["cafrom"] =  $filter["cafrom"]; // cancelled from
	$new_filter["cato"] =  $filter["cato"]; // cancelled to
	$new_filter["pshe"] =  $filter["pshe"]; //only project in the list of project sheets
	$new_filter["latp"] =  $filter["latp"]; //only the latest operating project of POS locations
	$new_filter["lopr"] =  $filter["lopr"]; //locally produced
	$new_filter["typb1"] =  $filter["typb1"]; //POS with Store Furniture
	$new_filter["typb2"] =  $filter["typb2"]; //POS with SIS Furniture
	$new_filter["idvs"] =  $filter["idvs"]; //Visuals
	$new_filter["noln"] =  $filter["noln"]; //no ln needed
	$new_filter["lnre"] =  $filter["lnre"]; //LN rejected
	$new_filter["nocer"] =  $filter["nocer"]; //no cer/af needed
	$new_filter["cerre"] =  $filter["cerre"]; //CER/AF rejected
	$new_filter["ct"] =  $filter["ct"]; //client types
	$new_filter["re"] =  $filter["re"]; //geografical region
	$new_filter["gr"] =  $filter["gr"]; // Supplied region
	$new_filter["co"] =  $filter["co"]; // country
	$new_filter["ci"] =  $filter["ci"]; // city
	$new_filter["pct"] =  $filter["pct"]; // Legal Type
	$new_filter["pk"] =  $filter["pk"]; // project Type
	$new_filter["ptsc"] =  $filter["ptsc"]; // project Type subclasses
	$new_filter["pt"] =  $filter["pt"]; // POS Type
	$new_filter["sc"] =  $filter["sc"]; // POS Subclass
	$new_filter["pl"] =  $filter["pl"]; // Product line
	$new_filter["fscs"] =  $filter["fscs"]; // Product line subcalss
	$new_filter["ts"] =  $filter["ts"]; //treatment state
	$new_filter["rtc"] =  $filter["rtc"]; // Project Leader
	$new_filter["lrtc"] =  $filter["lrtc"]; // Local project Leader
	$new_filter["dsup"] =  $filter["dsup"]; // Design supervisor
	$new_filter["rto"] =  $filter["rto"]; // Logistics Coordinator
	$new_filter["dcontr"] =  $filter["dcontr"]; // Logistics Coordinator
	$new_filter["dcs"] =  $filter["dcs"]; // Distribution Channel
	$new_filter["dos"] =  $filter["dos"]; // Design Objectives
	$new_filter["cmgr"] =  $filter["cmgr"]; // Cost Monitoring Groups
	$new_filter["icat"] =  $filter["icat"]; // Item Categories
	$new_filter["item"] =  $filter["item"]; // Items
	$new_filter["ar"] =  $filter["ar"]; // Neighbourhood Areas
	

	//flagship option
	if(array_key_exists("flagship", $filter))
	{
		$new_filter["flagship"] =  $filter["flagship"]; // Only flagship stores
	}
	
	

	if(param("save_form") == "ct") // Client Types
	{
		$CT = "";
		$res = mysql_query($sql_client_types) or dberror($sql_client_types);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["CT_" . $row["client_type_id"]])
			{
				$CT .=$row["client_type_id"] . "-";
			}
		}
		
		$new_filter["ct"] = $CT;
	}	
	elseif(param("save_form") == "re") // Geographical Regions
	{
		$RE = "";

		$res = mysql_query($sql_salesregion) or dberror($sql_salesregion);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["RE_" . $row["salesregion_id"]])
			{
				$RE .=$row["salesregion_id"] . "-";
			}
		}
		
		$new_filter["re"] = $RE;
	}
	elseif(param("save_form") == "gr") //Supplied Regions
	{
		$GR = "";

		$res = mysql_query($sql_region) or dberror($sql_region);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["GR_" . $row["region_id"]])
			{
				$GR .=$row["region_id"] . "-";
			}
		}
		
		$new_filter["gr"] = $GR;
	}
	elseif(param("save_form") == "co") //Countries
	{
		$CO = "";

		$res = mysql_query($sql_country) or dberror($sql_country);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["CO_" . $row["country_id"]])
			{
				$CO .=$row["country_id"] . "-";
			}
		}
		
		$new_filter["co"] = $CO;
	}
	elseif(param("save_form") == "ci") //Cities
	{
		$CI = "";

		$res = mysql_query($sql_cities) or dberror($sql_cities);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["CI_" . md5($row["place_id"])])
			{
				$CI .= trim($row["place_id"]) . "-";
			}
		}
	
		$new_filter["ci"] = $CI;

	}
	elseif(param("save_form") == "pct") //project cost types
	{
		$PCT = "";

		$res = mysql_query($sql_project_cost_types) or dberror($sql_project_cost_types);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PCT_" . $row["project_costtype_id"]])
			{
				$PCT .=$row["project_costtype_id"] . "-";
			}
		}
		$new_filter["pct"] = $PCT;
	}
	elseif(param("save_form") == "pk") //Product Kinds
	{
		$PK = "";

		$res = mysql_query($sql_project_kinds) or dberror($sql_project_kinds);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PK_" . $row["projectkind_id"]])
			{
				$PK .=$row["projectkind_id"] . "-";
			}
		}
		
		$new_filter["pk"] = $PK;
	}
	elseif(param("save_form") == "ptsc") //project type subclasses
	{
		$PTSC = "";

		$res = mysql_query($sql_project_type_subclasses) or dberror($sql_project_type_subclasses);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PTSC_" . $row["project_type_subclass_id"]])
			{
				$PTSC .=$row["project_type_subclass_id"] . "-";
			}
		}
		
		$new_filter["ptsc"] = $PTSC;
	}
	elseif(param("save_form") == "pt") //POS Types
	{
		$PT = "";

		$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PT_" . str_replace(" ", "", $row["postype_id"])])
			{
				$PT .=$row["postype_id"] . "-";
			}
		}
		
		$new_filter["pt"] = $PT;
	}
	elseif(param("save_form") == "sc") //POS Type Subclasses
	{
		$SC = "";

		$res = mysql_query($sql_possubclasses) or dberror($sql_possubclasses);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["SC_" . $row["possubclass_id"]])
			{
				$SC .=$row["possubclass_id"] . "-";
			}
		}
		
		$new_filter["sc"] = $SC;
	}
	elseif(param("save_form") == "pl") //Product Lines
	{
		$PL = "";

		$res = mysql_query($sql_product_lines) or dberror($sql_product_lines);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PL_" . $row["product_line_id"]])
			{
				$PL .=$row["product_line_id"] . "-";
			}
		}
		
		$new_filter["pl"] = $PL;
	}
	elseif(param("save_form") == "fscs") //Furniture Subclasses
	{
		$FSCS = "";

		$res = mysql_query($sql_furniture_subclasses) or dberror($sql_furniture_subclasses);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["FSCS_" . $row["productline_subclass_id"]])
			{
				$FSCS .=$row["productline_subclass_id"] . "-";
			}
		}
		
		$new_filter["fscs"] = $FSCS;
	}
	elseif(param("save_form") == "ts") //Project State
	{
		$TS = "";

		$res = mysql_query($sql_project_states) or dberror($sql_project_states);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["TS_" . $row["project_state_id"]])
			{
				$TS .=$row["project_state_id"] . "-";
			}
		}
		
		$new_filter["ts"] = $TS;
	}
	elseif(param("save_form") == "rtcs") //Project Leaders
	{
		$RTCS = "";

		$res = mysql_query($sql_rtcs) or dberror($sql_rtcs);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["RTCS_" . $row["user_id"]])
			{
				$RTCS .=$row["user_id"] . "-";
			}
		}
		$new_filter["rtc"] = $RTCS;
	}
	elseif(param("save_form") == "lrtcs") //Local Project Leaders
	{
		$LRTCS = "";

		$res = mysql_query($sql_lrtcs) or dberror($sql_lrtcs);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["LRTCS_" . $row["user_id"]])
			{
				$LRTCS .=$row["user_id"] . "-";
			}
		}
		
		$new_filter["lrtc"] = $LRTCS;
	}
	elseif(param("save_form") == "dsups") //Design supervisors
	{
		$DSUPS = "";

		$res = mysql_query($sql_dsups) or dberror($sql_dsups);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["DSUPS_" . $row["user_id"]])
			{
				$DSUPS .=$row["user_id"] . "-";
			}
		}
		
		$new_filter["dsup"] = $DSUPS;
	}
	elseif(param("save_form") == "rtos") //Logistic coordinators
	{
		$RTOS = "";

		$res = mysql_query($sql_rtos) or dberror($sql_rtos);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["RTOS_" . $row["user_id"]])
			{
				$RTOS .=$row["user_id"] . "-";
			}
		}
		
		$new_filter["rto"] = $RTOS;
	}
	elseif(param("save_form") == "dcontrs") //Design Contractros
	{
		$DCONTRS = "";

		$res = mysql_query($sql_dcontrs) or dberror($sql_dcontrs);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["DCONTRS_" . $row["user_id"]])
			{
				$DCONTRS .=$row["user_id"] . "-";
			}
		}
		
		$new_filter["dcontr"] = $DCONTRS;
	}
	elseif(param("save_form") == "dcs") //Distribution Channels
	{
		$DCS = "";

		$res = mysql_query($sql_distribution_channels) or dberror($sql_distribution_channels);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["DCS_" . $row["mps_distchannel_id"]])
			{
				$DCS .=$row["mps_distchannel_id"] . "-";
			}
		}
		
		$new_filter["dcs"] = $DCS;
	}
	elseif(param("save_form") == "dos") //Design Objectives
	{
		$DOS = "";

		$res = mysql_query($sql_design_objectives) or dberror($sql_design_objectives);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["DOS_" . $row["design_objective_item_id"]])
			{
				$DOS .=$row["design_objective_item_id"] . "-";
			}
		}
		
		$new_filter["dos"] = $DOS;
	}
	elseif(param("save_form") == "cmgr") //Cost Monitoring Groups
	{
		$CMGR = "";

		$res = mysql_query($sql_costmonitoring_groups) or dberror($sql_costmonitoring_groups);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["CMGR_" . $row["costmonitoringgroup_id"]])
			{
				$CMGR .=$row["costmonitoringgroup_id"] . "-";
			}
		}
		
		$new_filter["cmgr"] = $CMGR;
	}
	elseif(param("save_form") == "icat") //Item Categories
	{
		$ICAT = "";

		$res = mysql_query($sql_item_categories) or dberror($sql_item_categories);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["ICAT_" . $row["item_category_id"]])
			{
				$ICAT .=$row["item_category_id"] . "-";
			}
		}
		
		$new_filter["icat"] = $ICAT;
	}
	elseif(param("save_form") == "item") //Items
	{
		$ITEM = "";

		$res = mysql_query($sql_items) or dberror($sql_items);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["ITEM_" . $row["item_id"]])
			{
				$ITEM .=$row["item_id"] . "-";
			}
		}
		
		$new_filter["item"] = $ITEM;
	}
	elseif(param("save_form") == "ar") //Areas
	{
		$AR = "";

		$res = mysql_query($sql_areas) or dberror($sql_areas);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["AR_" . $row["posareatype_id"]])
			{
				$AR .=$row["posareatype_id"] . "-";
			}
		}
		
		$new_filter["ar"] = $AR;
	}

	$sql = "update projectqueries " . 
		   "set projectquery_filter = " . dbquote(serialize($new_filter)) . 
		   " where projectquery_id = " . param("query_id");

	$result = mysql_query($sql) or dberror($sql);
	
}

/********************************************************************
    get existant filter
*********************************************************************/
$filter = array();
//get benchmark parameters
$salesregions = array();

$sql = "select projectquery_filter from projectqueries " .
	   "where projectquery_id = " . $query_id;

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{

	$filter = unserialize($row["projectquery_filter"]);
	$clienttypes = explode("-", $filter["ct"]);
	$salesregions = explode("-", $filter["re"]);
	$regions = explode("-", $filter["gr"]);
	$countries = explode("-", $filter["co"]);
	$cities = explode("-", $filter["ci"]);
	$areas = explode("-", $filter["ar"]);
	$customer_services = explode("-", $filter["cs"]);
	$project_cost_types = explode("-", $filter["pct"]);
	$product_lines = explode("-", $filter["pl"]);
	$furniture_subclasses = explode("-", $filter["fscs"]);
	$pos_types = explode("-", $filter["pt"]);
	$subclasses = explode("-", $filter["sc"]);
	$project_kinds = explode("-", $filter["pk"]);
	$project_type_subclasses = explode("-", $filter["ptsc"]);
	$project_states = explode("-", $filter["ts"]);
	$rtcs = explode("-", $filter["rtc"]);
	$lrtcs = explode("-", $filter["lrtc"]);
	$dsups = explode("-", $filter["dsup"]);
	$rtos = explode("-", $filter["rto"]);
	$dcontrs = explode("-", $filter["dcontr"]);
	$distribution_channels = explode("-", $filter["dcs"]);
	$design_objectives = explode("-", $filter["dos"]);
	$costmonitoring_groups = explode("-", $filter["cmgr"]);
	$item_categories = explode("-", $filter["icat"]);
	$items = explode("-", $filter["item"]);
}


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new form("cer_benchmarks", "benchmark_selector");
$form->add_hidden("query_id", $query_id);


if(param("s") == "ct") // Client Types
{
	$page_title = "Client Type Selector";
	$form->add_hidden("save_form", "ct");

	$res = mysql_query($sql_client_types) or dberror($sql_client_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["client_type_id"], $clienttypes))
		{
			$form->add_checkbox("CT_" . $row["client_type_id"], $row["client_type_code"], true);
		}
		else
		{
			$form->add_checkbox("CT_" . $row["client_type_id"], $row["client_type_code"], false);
		}
		
	}
}
elseif(param("s") == "re") // Geographical regions
{
	$page_title = "Geographical Region Selector";
	$form->add_hidden("save_form", "re");

	$res = mysql_query($sql_salesregion) or dberror($sql_salesregion);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["salesregion_id"], $salesregions))
		{
			$form->add_checkbox("RE_" . $row["salesregion_id"], $row["salesregion_name"], true);
		}
		else
		{
			$form->add_checkbox("RE_" . $row["salesregion_id"], $row["salesregion_name"], false);
		}
		
	}
}
elseif(param("s") == "gr") // Supplied regions
{
	$page_title = "Supplied Region Selector";
	$form->add_hidden("save_form", "gr");

	$res = mysql_query($sql_region) or dberror($sql_region);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["region_id"], $regions))
		{
			$form->add_checkbox("GR_" . $row["region_id"], $row["region_name"], true);
		}
		else
		{
			$form->add_checkbox("GR_" . $row["region_id"], $row["region_name"], false);
		}
		
	}
}
elseif(param("s") == "co") // countries
{
	$page_title = "Country Selector";
	$form->add_hidden("save_form", "co");

	$res = mysql_query($sql_country) or dberror($sql_country);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["country_id"], $countries))
		{
			$form->add_checkbox("CO_" . $row["country_id"], $row["country_name"], true);
		}
		else
		{
			$form->add_checkbox("CO_" . $row["country_id"], $row["country_name"], false);
		}
		
	}
}
elseif(param("s") == "ci") // cities
{
	$page_title = "City Selector";
	$form->add_hidden("save_form", "ci");

	$res = mysql_query($sql_cities) or dberror($sql_cities);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array(trim($row["place_id"]), $cities))
		{
			$form->add_checkbox("CI_" . md5($row["place_id"]), $row["place_name"], true);
		}
		else
		{
			$form->add_checkbox("CI_" . md5($row["place_id"]),  $row["place_name"], false);
		}
		
	}
}
elseif(param("s") == "ar") // Neighbourhood Areas
{
	$page_title = "Neighbourhood Area Selector";
	$form->add_hidden("save_form", "ar");

	$res = mysql_query($sql_areas) or dberror($sql_areas);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["posareatype_id"], $areas))
		{
			$form->add_checkbox("AR_" . $row["posareatype_id"], $row["posareatype_name"], true);
		}
		else
		{
			$form->add_checkbox("AR_" . $row["posareatype_id"], $row["posareatype_name"], false);
		}
		
	}
}
elseif(param("s") == "pct") // Project Cost Types
{
	$page_title = "Legal Type Selector";
	$form->add_hidden("save_form", "pct");

	$res = mysql_query($sql_project_cost_types) or dberror($sql_project_cost_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["project_costtype_id"], $project_cost_types))
		{
			$form->add_checkbox("PCT_" . $row["project_costtype_id"], $row["project_costtype_text"], true);
		}
		else
		{
			$form->add_checkbox("PCT_" . $row["project_costtype_id"], $row["project_costtype_text"], false);
		}
		
	}
}
elseif(param("s") == "pl") // Product Lines
{
	$page_title = "Furniture Type Selector";
	$form->add_hidden("save_form", "pl");

	$res = mysql_query($sql_product_lines) or dberror($sql_product_lines);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["product_line_id"], $product_lines))
		{
			$form->add_checkbox("PL_" . $row["product_line_id"], $row["product_line_name"], true);
		}
		else
		{
			$form->add_checkbox("PL_" . $row["product_line_id"], $row["product_line_name"], false);
		}
		
	}
}
elseif(param("s") == "fscs") // Furniture Subclasses
{
	$page_title = "Furniture Subclass Selector";
	$form->add_hidden("save_form", "fscs");

	$res = mysql_query($sql_furniture_subclasses) or dberror($sql_furniture_subclasses);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["productline_subclass_id"], $furniture_subclasses))
		{
			$form->add_checkbox("FSCS_" . $row["productline_subclass_id"], $row["productline_subclass_name"], true);
		}
		else
		{
			$form->add_checkbox("FSCS_" . $row["productline_subclass_id"], $row["productline_subclass_name"], false);
		}
		
	}
}
elseif(param("s") == "pt") // POS Types
{
	$page_title = "POS Type Selector";
	$form->add_hidden("save_form", "pt");

	$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["postype_id"], $pos_types))
		{
			$form->add_checkbox("PT_" . str_replace(" ", "", $row["postype_id"]), $row["postype_name"], true);
		}
		else
		{
			$form->add_checkbox("PT_" . str_replace(" ", "", $row["postype_id"]), $row["postype_name"], false);
		}
	}
}
elseif(param("s") == "sc") // POS Type Subclass
{
	$page_title = "POS Type Subclass Selector";
	$form->add_hidden("save_form", "sc");

	$res = mysql_query($sql_possubclasses) or dberror($sql_possubclasses);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["possubclass_id"], $subclasses))
		{
			$form->add_checkbox("SC_" . $row["possubclass_id"], $row["possubclass_name"], true);
		}
		else
		{
			$form->add_checkbox("SC_" . $row["possubclass_id"], $row["possubclass_name"], false);
		}
		
	}
}
elseif(param("s") == "pk") // Project Types
{
	$page_title = "Project Type Selector";
	$form->add_hidden("save_form", "pk");

	$res = mysql_query($sql_project_kinds) or dberror($sql_project_kinds);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["projectkind_id"], $project_kinds))
		{
			$form->add_checkbox("PK_" . $row["projectkind_id"], $row["projectkind_name"], true);
		}
		else
		{
			$form->add_checkbox("PK_" . $row["projectkind_id"], $row["projectkind_name"], false);
		}
		
	}
}
elseif(param("s") == "ptsc") // Project Type Subclasses
{
	$page_title = "Project Type Subclass Selector";
	$form->add_hidden("save_form", "ptsc");

	$res = mysql_query($sql_project_type_subclasses) or dberror($sql_project_type_subclasses);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["project_type_subclass_id"], $project_type_subclasses))
		{
			$form->add_checkbox("PTSC_" . $row["project_type_subclass_id"], $row["project_type_subclass_name"], true);
		}
		else
		{
			$form->add_checkbox("PTSC_" . $row["project_type_subclass_id"], $row["project_type_subclass_name"], false);
		}
		
	}
}
elseif(param("s") == "ts") // Project States
{
	$page_title = "Treatment State Selector";
	$form->add_hidden("save_form", "ts");

	$res = mysql_query($sql_project_states) or dberror($sql_project_states);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["project_state_id"], $project_states))
		{
			$form->add_checkbox("TS_" . $row["project_state_id"], $row["project_state_text"], true);
		}
		else
		{
			$form->add_checkbox("TS_" . $row["project_state_id"], $row["project_state_text"], false);
		}
		
	}
}
elseif(param("s") == "rtc") // Project Leaders
{
	$page_title = "Project Leader Selector";
	$form->add_hidden("save_form", "rtcs");

	$res = mysql_query($sql_rtcs) or dberror($sql_rtcs);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["user_id"], $rtcs))
		{
			$form->add_checkbox("RTCS_" . $row["user_id"], $row["username"], true);
		}
		else
		{
			$form->add_checkbox("RTCS_" . $row["user_id"], $row["username"], false);
		}
		
	}
}
elseif(param("s") == "lrtc") // Local Project Leaders
{
	$page_title = "Local Project Leader Selector";
	$form->add_hidden("save_form", "lrtcs");

	$res = mysql_query($sql_lrtcs) or dberror($sql_lrtcs);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["user_id"], $lrtcs))
		{
			$form->add_checkbox("LRTCS_" . $row["user_id"], $row["username"], true);
		}
		else
		{
			$form->add_checkbox("LRTCS_" . $row["user_id"], $row["username"], false);
		}
		
	}
}
elseif(param("s") == "dsup") // Design Supervisors
{
	$page_title = "Design Supervisor Selector";
	$form->add_hidden("save_form", "dsups");

	$res = mysql_query($sql_dsups) or dberror($sql_dsups);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["user_id"], $dsups))
		{
			$form->add_checkbox("DSUPS_" . $row["user_id"], $row["username"], true);
		}
		else
		{
			$form->add_checkbox("DSUPS_" . $row["user_id"], $row["username"], false);
		}
		
	}
}
elseif(param("s") == "rto") // Logistics Coordinator
{
	$page_title = "Logistics Coordinator Selector";
	$form->add_hidden("save_form", "rtos");

	$res = mysql_query($sql_rtos) or dberror($sql_rtos);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["user_id"], $rtos))
		{
			$form->add_checkbox("RTOS_" . $row["user_id"], $row["username"], true);
		}
		else
		{
			$form->add_checkbox("RTOS_" . $row["user_id"], $row["username"], false);
		}
		
	}
}
elseif(param("s") == "dcontr") // Design Contractor
{
	$page_title = "Design Contractor Selector";
	$form->add_hidden("save_form", "dcontrs");

	$res = mysql_query($sql_dcontrs) or dberror($sql_dcontrs);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["user_id"], $dcontrs))
		{
			$form->add_checkbox("DCONTRS_" . $row["user_id"], $row["username"], true);
		}
		else
		{
			$form->add_checkbox("DCONTRS_" . $row["user_id"], $row["username"], false);
		}
		
	}
}
elseif(param("s") == "dcs") // Distribution Channels
{
	$page_title = "Distribution Channel Selector";
	$form->add_hidden("save_form", "dcs");

	$res = mysql_query($sql_distribution_channels) or dberror($sql_distribution_channels);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["mps_distchannel_id"], $distribution_channels))
		{
			$form->add_checkbox("DCS_" . $row["mps_distchannel_id"], $row["distributionchannel"], true);
		}
		else
		{
			$form->add_checkbox("DCS_" . $row["mps_distchannel_id"], $row["distributionchannel"], false);
		}
		
	}
}
elseif(param("s") == "dos") // Design Objectives
{
	$page_title = "Design Objectives Selector";
	$form->add_hidden("save_form", "dos");

	$res = mysql_query($sql_design_objectives) or dberror($sql_design_objectives);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["design_objective_item_id"], $design_objectives))
		{
			$form->add_checkbox("DOS_" . $row["design_objective_item_id"], $row["postype_name"] . " - " . $row["design_objective_group_name"] . " - " . $row["design_objective_item_name"], true);
		}
		else
		{
			$form->add_checkbox("DOS_" . $row["design_objective_item_id"], $row["postype_name"] . " - " . $row["design_objective_group_name"] . " - " . $row["design_objective_item_name"], false);
		}
		
	}
}
elseif(param("s") == "cmgr") // Cost Monitoring Groups
{
	$page_title = "Cost Monitoring Group Selector";
	$form->add_hidden("save_form", "cmgr");

	$res = mysql_query($sql_costmonitoring_groups) or dberror($sql_costmonitoring_groups);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["costmonitoringgroup_id"], $costmonitoring_groups))
		{
			$form->add_checkbox("CMGR_" . $row["costmonitoringgroup_id"], $row["costmonitoringgroup_text"], true);
		}
		else
		{
			$form->add_checkbox("CMGR_" . $row["costmonitoringgroup_id"], $row["costmonitoringgroup_text"], false);
		}
		
	}
}
elseif(param("s") == "icat") // Item Categorie
{
	$page_title = "Item Category Selector";
	$form->add_hidden("save_form", "icat");

	$res = mysql_query($sql_item_categories) or dberror($sql_item_categories);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["item_category_id"], $item_categories))
		{
			$form->add_checkbox("ICAT_" . $row["item_category_id"], $row["item_category_name"], true);
		}
		else
		{
			$form->add_checkbox("ICAT_" . $row["item_category_id"], $row["item_category_name"], false);
		}
		
	}
}
elseif(param("s") == "item") // Items
{
	/*
	$page_title = "Item Selector";
	$form->add_hidden("save_form", "item");

	$res = mysql_query($sql_items) or dberror($sql_items);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["item_id"], $items))
		{
			$form->add_checkbox("ITEM_" . $row["item_id"], $row["item_name"], true);
		}
		else
		{
			$form->add_checkbox("ITEM_" . $row["item_id"], $row["item_name"], false);
		}
		
	}
	*/


	$page_title = "Item Selector";

	if($filter_items)
	{
			
		if($items_filter)
		{
			$form->add_section("Selected Items");
			$res = mysql_query($sql_items) or dberror($sql_items);
			while($row = mysql_fetch_assoc($res))
			{
				if(in_array($row["item_id"], $items))
				{
					$form->add_checkbox("ITEM_" . $row["item_id"], $row["item_name"], true);
				}
			}

			
		}
		
		
		$form->add_section("Unselected Items");
		$res = mysql_query($sql_items) or dberror($sql_items);
		while($row = mysql_fetch_assoc($res))
		{
			if(!in_array($row["item_id"], $items))
			{
				$form->add_checkbox("ITEM_" . $row["item_id"], $row["item_name"], false);
			}
		}
		
		$form->add_hidden("save_form", 'item');
	}
	else
	{
	
		$form->add_section("Select");

		if(array_key_exists("selected_product_line", $_POST))
		{
			$form->add_list("selected_product_line", "Product Line", $sql_product_lines, 0, $_POST["selected_product_line"]);
		}
		else
		{
			$form->add_list("selected_product_line", "Product Line", $sql_product_lines, 0);
		}

		if(array_key_exists("selected_item_category", $_POST))
		{
			$form->add_list("selected_item_category", "Item Category", $sql_item_category, 0, $_POST["selected_item_category"]);
		}
		else
		{
			$form->add_list("selected_item_category", "Item Category", $sql_item_category);
		}

		$form->add_hidden("s", "item");

		$tmp = "<input id=\"load_items\" type=\"submit\" value=\"Show Selected Items\" class=\"modalbutton\" onmouseout=\"this.style.color='#006699'\" onmouseover=\"this.style.color='#FF0000';this.style.cursor='pointer'\" style=\"color: rgb(0, 102, 153); cursor: pointer;\">";
		$form->add_label("reload", "", RENDER_HTML, $tmp);

		
		if($items_filter)
		{
			$form->add_section("Selected Items");
			$res = mysql_query($sql_items) or dberror($sql_items);
			while($row = mysql_fetch_assoc($res))
			{
				if(in_array($row["item_id"], $items))
				{
					$form->add_checkbox("ITEM_" . $row["item_id"], $row["item_name"], true);
				}
			}

			
		}
		if($productline_filter or $item_category_filter
			or (array_key_exists("selected_product_line", $_POST) or array_key_exists("selected_item_category", $_POST)
			and  $_POST["selected_product_line"] > 0
			and  $_POST["selected_item_category"] > 0)
			)
		{
			$form->add_section("Unselected Items");
			$res = mysql_query($sql_items) or dberror($sql_items);
			while($row = mysql_fetch_assoc($res))
			{
				if(!in_array($row["item_id"], $items))
				{
					$form->add_checkbox("ITEM_" . $row["item_id"], $row["item_name"], false);
				}
			}
		}

		if(array_key_exists("save_form", $_POST) and $_POST["save_form"] == 'item')
		{
			$form->add_hidden("save_form", 'item');
		}
		else
		{
			$form->add_hidden("save_form", '');
		}
	}
}


if(param("s") == "item") // items
{

	$tmp = "<input id=\"load_items\" type=\"submit\" value=\"Save Item Selection\" class=\"modalbutton\" onmouseout=\"this.style.color='#006699'\" onmouseover=\"this.style.color='#FF0000';this.style.cursor='pointer'\" style=\"color: rgb(0, 102, 153); cursor: pointer;\"  onclick=\"javascript:$('#save_form').val('item');\">";
	$form->add_label("save_items", "", RENDER_HTML, $tmp);
}
else
{
	$form->add_input_submit("submit", "Save Selection", 0);
}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Process buttons
*********************************************************************/ 

/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page_Modal("projects");

$page->header();
$page->title($page_title);

$form->render();

if(param("save_form") != '')
{
?>

<script languege="javascript">
var back_link = "project_queries_query_filter.php?query_id=<?php echo $query_id;?>"; 
$.nyroModalRemove();
</script>

<?php
}
$page->footer();
