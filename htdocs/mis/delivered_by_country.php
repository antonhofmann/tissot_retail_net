<?php
/********************************************************************

    delivered_by_country.php

    Enter parameters for the query of consumption

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2003-06-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-06-27
    Version:        1.0.0

    Copyright (c) 2003, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/calculate_functions.php";

check_access("can_perform_queries");



/********************************************************************
    prepare all data needed
*********************************************************************/
$date_filter1 = "";
$date_filter2 = "";


if(param("mode"))
{
    $mode = $_REQUEST["mode"];
    $from_date = $_REQUEST["from_date"];
    $to_date = $_REQUEST["to_date"];
    $all_countries = $_REQUEST["all_countries"];
    $item_id = $_REQUEST["item_id"];


    if($from_date)
    {
        $from_date = from_system_date($from_date);
        $date_filter1 = "    and order_item_arrival >=  '" . $from_date . "'";
    }
    if($to_date)
    {
        $to_date = from_system_date($to_date);
        $date_filter2 = "    and order_item_arrival <=  '" . $to_date . "'";
    }
}
else
{
    $mode = 0;
    $from_date = "";
    $to_date = "";
    $item_id = "";
}


// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ";


$sql_order_items = "select distinct concat(country_id, ' ', item_id) as list_id, ".
                   "    country_id, country_name, ".
                   "    item_id, item_code, item_name ".
                   "from countries ".
                   "left join orders on order_shop_address_country = country_id ".
                   "left join order_items on order_item_order = order_id ".
                   "left join items on order_item_item = item_id ";

$list_filter = "";


$listfilter_standard =  "item_type < 3 ".
                        "    and order_item_arrival > '0000-00-00' " .
                        "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
                        $date_filter1 . $date_filter2 .
                        "    and order_cancelled is null";                


$items_consumed = array();
$turn_over = array();
$group_totals = array();
$list_total = 0;


$sql_items = "select item_id, item_code ".
             "from items " .
             "where item_type < 3 " .
             "order by item_code ";

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new form("countries", "delivered_by_country");


if($mode == 0)
{
    $form->add_hidden("mode", 1);
    $form->add_section("Period");
    $form->add_comment("Please enter the date in the form of 'dd.mm.yy'. Only dates from 1.11.02 are possible since the system started on the 1st of November 2002!");

    $form->add_edit("from_date", "From Date", 0 , "", TYPE_DATE, 20);
    $form->add_edit("to_date", "To Date", 0 , "", TYPE_DATE, 20);

    
    $form->add_section("Item");
    $form->add_list("item_id", "Item Code", $sql_items);
    
    $form->add_comment("Please check in case you want to have the query performed over all countries.");
    $form->add_checkbox("all_countries", "Include all Countries");

    $form->add_comment(" \n");
    $form->add_button("execute", "Execute Query");


}
else
{
    $form->add_label("from_date", "From Date", "", $from_date);
    $form->add_label("to_date", "To Date", "", $to_date);
    $form->add_comment("");
    $form->add_comment("The Quantities indicated are the ones having an actual arrival date!");
}



// itemlist
$list = new ListView($sql_countries, LIST_HAS_HEADER | LIST_SHOW_COUNT);

$list->set_entity("countries");
$list->set_order("country_name");

$list->set_checkbox("countries", "adresses", "country_id");

$list->add_column("country_name", "Name", "", LIST_FILTER_FREE);

$list->process();


// list of ordered items
$list2 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list2->set_entity("countries");
$list2->set_group("country_name");
$list2->set_order("item_code");


if ($form->button("execute") or $form->button("xls"))
{
    if ($form->validate())
    {
        
        if (!$all_countries)
        {        
            $check_ids = array();

            foreach (array_keys($_REQUEST) as $key)
            {
                if (preg_match("/__check__(\d+)/", $key, $matches))
                {
                    $check_ids[$matches[1]] = true;
                }
            }

            foreach (array_keys($check_ids) as $id)
            {
                if ($list_filter == "")
                {
                    $list_filter = $list_filter . "(country_id = " . $id;
                }
                else
                {
                    $list_filter = $list_filter . " or country_id = " . $id;
                }
            }
            if ($list_filter == "")
            {
                $list_filter = $listfilter_standard;              
            }
            else
            {
                $list_filter = $list_filter . ") and " . $listfilter_standard;
            }
            
            
            if($item_id)
            {
                $list_filter = $list_filter . " and order_item_item = " . $item_id;
            }
            
            $sql = $sql_order_items . " where " . $list_filter;


            $res = mysql_query($sql) or dberror($sql);
            while ($row = mysql_fetch_assoc($res))
            {
                $items_delivered = get_item_total_consumption_data($row["item_id"], $from_date, $to_date, $row["country_id"], "");
                
                $items_consumed[$row["list_id"]] = $items_delivered["item_consumption"];
                $turn_over[$row["list_id"]] = $items_delivered["turn_over"];
                $list_total = $list_total + $items_delivered["turn_over_1"];
                if(array_key_exists($row["country_name"], $group_totals))
                {
                    $group_totals[$row["country_name"]] =  $group_totals[$row["country_name"]] + $items_delivered["turn_over_1"];
                }
                else
                {
                    $group_totals[$row["country_name"]] =  $items_delivered["turn_over_1"];
                }
            }

        }
        else
        {
            $list_filter = $listfilter_standard;              
            
            if($item_id)
            {
                $list_filter = $list_filter . " and order_item_item = " . $item_id;
            }

            $sql = $sql_order_items . " where " . $list_filter;
            
            

            $res = mysql_query($sql) or dberror($sql);
            while ($row = mysql_fetch_assoc($res))
            {
                $items_delivered = get_item_total_consumption_data($row["item_id"], $from_date, $to_date, $row["country_id"], "");
                $items_consumed[$row["list_id"]] = $items_delivered["item_consumption"];
                $turn_over[$row["list_id"]] = $items_delivered["turn_over"];
                $list_total = $list_total + $items_delivered["turn_over_1"];
                if(array_key_exists($row["country_name"], $group_totals))
                {
                    $group_totals[$row["country_name"]] =  $group_totals[$row["country_name"]] + $items_delivered["turn_over_1"];
                }
                else
                {
                    $group_totals[$row["country_name"]] =  $items_delivered["turn_over_1"];
                }
            }
        }

        
        $list2->set_filter($list_filter);

		echo $sql = $sql_order_items . " where " . $list_filter;
    }
}


$list2->add_column("item_code", "Code", "", LIST_FILTER_FREE);
$list2->add_column("item_name", "Name", "", LIST_FILTER_FREE);
$list2->add_text_column("consumption", "Delivered", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $items_consumed);
$list2->add_text_column("turn_over", "Turn Over", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $turn_over);

foreach ($group_totals as $key=>$value)
{
    $list2->set_group_footer("turn_over", $key , number_format($value,2, ".", "'"));
}

$list2->set_footer("item_code", "Total");
$list2->set_footer("turn_over", number_format($list_total,2, ".", "'"));

$list2->process();



if($mode == 1)
{
    
    $link = "http://" . $_SERVER["HTTP_HOST"] . "/mis/delivered_by_country_xls.php?" . 
            "p1="  . $from_date .
            "&p2=" . $to_date . 
            "&p3=" . $sql_order_items . 
            "&p4=" . $list_filter;

    $form->add_button("xls", "Export to Excel", $link);
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page("queries");

require "include/mis_page_actions.php";

$page->header();
$page->title("Consumption of Items");

$form->render();

if($mode == 0)
{
    $list->render();
}
else
{
    $list2->render();
}

$page->footer();

?>