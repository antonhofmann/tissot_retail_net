<?php
/********************************************************************

    order_queries_query_filter.php

    Filter Selection for the Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2016-08-19
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2016-08-19
    Version:        1.0.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";


if(!has_access("can_perform_all_queries") 
  and !has_access("can_perform_projects_query"))
{
	redirect("noaccess.php");
}
require_once "include/orders_query_check_access.php";

require_once "include/orders_query_get_functions.php";

/********************************************************************
    prepare all data needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("order_queries.php");
}

$query_id = param("query_id");

$orderquery = get_query_name($query_id);


$icon = "../pictures/add_item.gif";
$link = "javascript:open_selector('')";


$filter = get_query_filter($query_id);


if(has_access("has_access_to_all_projects"))
{
	$sql_clients = "select DISTINCT order_client_address, concat(country_name, ', ', address_company) as company  " .
				   "from orders " . 
				   "left join addresses on address_id = order_client_address ".
				   "left join countries on country_id = address_country " .
				   "where address_id > 0 ".
				   "order by country_name, address_company";
}
else
{
	$sql_clients = "select DISTINCT order_client_address, concat(country_name, ', ', address_company) as company  " .
				   "from orders " . 
				   "left join addresses on address_id = order_client_address ".
				   "left join countries on country_id = address_country " .
				   "left join users on user_address = address_id " . 
				   "where address_id > 0 " .
		           " and user_id = " . user_id() . 
				   " order by country_name, address_company";
}


$sql_suppliers = "select DISTINCT address_id, " . 
				   "concat(country_name, ', ', address_company) as company  " .
				   "from addresses " .
				   "left join countries on country_id = address_country " .
				   "where address_id > 0 ".
				   " and address_type = 2 " . 
				   "order by country_name, address_company";

$sql_forwarders = "select DISTINCT address_id, " . 
				   "concat(country_name, ', ', address_company) as company  " .
				   "from addresses " .
				   "left join countries on country_id = address_country " .
				   "where address_id > 0 ".
				   " and address_type = 3 " . 
				   "order by country_name, address_company";

$sql_items = "select item_id, concat(item_code, ' ', item_name) as item_name " . 
             " from items " . 
			 " where item_type = 1 " . 
			 " order by item_code";


//get benchmark parameters
$clienttypes = array();
$salesregions = array();
$regions = array();
$countries = array();
$cities = array();
$product_lines = array();
$pos_types = array();
$project_kinds = array();
$project_cost_types = array();
$item_categories = array();

$client = "";
$franchisee = "";
$supplier = "";
$forwarder = "";


$sufrom = "";
$suto = "";

//check if filter is present
$sql = "select orderquery_fields, orderquery_filter, orderquery_area_perception from orderqueries " .
	   "where orderquery_id = " . $query_id;

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$fields_projectaddresses = unserialize($row["orderquery_fields"]);

	$filters = array();
	$filters = unserialize($row["orderquery_filter"]);

	foreach($filters as $key=>$value)
	{
		//client
		if(array_key_exists("cl", $filters))
		{
			$client = $filters["cl"];

		}
		//supplier
		if(array_key_exists("supp", $filters))
		{
			$supplier = $filters["supp"];

		}
		//forwarder
		if(array_key_exists("forw", $filters))
		{
			$forwarder = $filters["forw"];

		}
		
		//submitted from
		if(array_key_exists("sufrom", $filters))
		{
			$sufrom = $filters["sufrom"];

		}
		//submitted to
		if(array_key_exists("suto", $filters))
		{
			$suto = $filters["suto"];

		}

				
		//client type
		if(array_key_exists("ct", $filters))
		{
			$clienttypes = explode("-", $filters["ct"]);
		}
		//geografical region
		if(array_key_exists("re", $filters))
		{
			$salesregions = explode("-", $filters["re"]);
		}
		// sales region
		if(array_key_exists("gr", $filters))
		{
			$regions = explode("-", $filters["gr"]);
		}
		//country
		if(array_key_exists("co", $filters))
		{
			$countries = explode("-", $filters["co"]);
		}

		//legal type
		if(array_key_exists("pct", $filters))
		{
			$project_cost_types = explode("-", $filters["pct"]);
		}

		//project Type
		if(array_key_exists("pk", $filters))
		{
			$project_kinds = explode("-", $filters["pk"]);
		}

		//POS Type
		if(array_key_exists("pt", $filters))
		{
			$pos_types = explode("-", $filters["pt"]);
		}
		

		//item categories
		if(array_key_exists("icat", $filters))
		{
			$item_categories = explode("-", $filters["icat"]);
		}

		
		
	}
}




//SQL for Filters
// create sql for filter criteria

$sql_salesregion = "select salesregion_id, salesregion_name ".
               "from salesregions order by salesregion_name";

$sql_region = "select region_id, region_name ".
			   "from regions order by region_name";


$sql_country = "select DISTINCT country_id, country_name " .
			   "from orders " .
			   "left join countries on country_id = order_shop_address_country " . 
			   " where country_id > 0 " . 
			   "order by country_name";



if(count($countries) > 0)
{
	
	$countries = explode("-", $filter["co"]);
	
	foreach($countries as $key=>$country)
	{
		$tmp_filter = $tmp_filter . $country . ",";
	}

	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);
	
	if($tmp_filter)
	{
		$tmp_filter = " where place_country IN ( " . $tmp_filter . ") ";
	}

	$sql_cities = "select DISTINCT place_id, place_name ".
		          " from places " .
				  $tmp_filter . 
				  "order by place_name";
}
else
{
	$sql_cities = "select DISTINCT place_id, place_name ".
				  "from places order by place_name";
}


$sql_pos_types = "select DISTINCT postype_id, postype_name ".
                     "from postypes " .
                     "order by postype_name";


$sql_project_cost_types = "select project_costtype_id, project_costtype_text ".
						  "from project_costtypes " .
                          "order by project_costtype_text";


$sql_project_kinds = "select projectkind_id, projectkind_name ".
						 "from projectkinds " .
						 "where projectkind_id > 0 " .
						 "order by projectkind_name";


$sql_client_types = "select client_type_id, client_type_code ".
					"from client_types";



$sql_item_categories = "select item_category_id, item_category_name " . 
                             "from item_categories " . 
							 " order by item_category_name";



/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("orderqueries", "query_generator");

$form->add_hidden("query_id", param("query_id"));
$form->add_section($orderquery["name"]);


$form->add_comment("Please select the the filter criteria of your query.");



$form->add_section("Addresses");
$link = "javascript:open_selector('')";



$form->add_list("client_id", "Client",	$sql_clients, 0, $client);
$form->add_list("supplier_id", "Supplier", $sql_suppliers, SUBMIT, $supplier);
$form->add_list("forwarder_id", "Forwarder", $sql_forwarders, 0, $forwarder);


$form->add_Section("Selected Filter Criteria");


$selected_clienttypes = "";
if(count($clienttypes) > 0)
{
	$res = mysql_query($sql_client_types) or dberror($sql_client_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["client_type_id"], $clienttypes))
		{
			$selected_clienttypes .= $row["client_type_code"] . ", ";
		}
	}
	$selected_clienttypes = substr($selected_clienttypes, 0, strlen($selected_clienttypes) - 2);
}

$form->add_label_selector("clienttypes", "Client Types", 0, $selected_clienttypes, $icon, $link);

$selected_salesregions = "";
if(count($salesregions) > 0)
{
	$res = mysql_query($sql_salesregion) or dberror($sql_salesregion);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["salesregion_id"], $salesregions))
		{
			$selected_salesregions .= $row["salesregion_name"] . ", ";
		}
	}
	$selected_salesregions = substr($selected_salesregions, 0, strlen($selected_salesregions) - 2);
}

$form->add_label_selector("salesregions", "Geographical Regions", 0, $selected_salesregions, $icon, $link);

$selected_regions = "";
if(count($regions) > 0)
{
	$res = mysql_query($sql_region) or dberror($sql_region);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["region_id"], $regions))
		{
			$selected_regions .= $row["region_name"] . ", ";
		}
	}
	$selected_regions = substr($selected_regions, 0, strlen($selected_regions) - 2);
}

$form->add_label_selector("regions", "Supplied Regions", 0, $selected_regions, $icon, $link);


$selected_countries = "";
if(count($countries) > 0)
{
	$res = mysql_query($sql_country) or dberror($sql_country);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["country_id"], $countries))
		{
			$selected_countries .= $row["country_name"] . ", ";
		}
	}
	$selected_countries = substr($selected_countries, 0, strlen($selected_countries) - 2);
}
$form->add_label_selector("countries", "Countries", 0, $selected_countries, $icon, $link);


$selected_cities = "";
if(count($cities) > 0)
{
	$res = mysql_query($sql_cities) or dberror($sql_cities);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array(trim($row["place_id"]), $cities))
		{
			$selected_cities .= $row["place_name"] . ", ";
		}
	}
	$selected_cities = substr($selected_cities, 0, strlen($selected_cities) - 2);
}
$form->add_label_selector("cities", "Cities", 0, $selected_cities, $icon, $link);


$selected_pcts = "";
if(count($project_cost_types) > 0)
{
	$res = mysql_query($sql_project_cost_types) or dberror($sql_project_cost_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["project_costtype_id"], $project_cost_types))
		{
			$selected_pcts .= $row["project_costtype_text"] . ", ";
		}
	}
	$selected_pcts = substr($selected_pcts, 0, strlen($selected_pcts) - 2);
}
$form->add_label_selector("pcts", "Legal Types", 0, $selected_pcts, $icon, $link);



$selected_pks = "";
if(count($project_kinds) > 0)
{
	$res = mysql_query($sql_project_kinds) or dberror($sql_project_kinds);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["projectkind_id"], $project_kinds))
		{
			$selected_pks .= $row["projectkind_name"] . ", ";
		}
	}
	$selected_pks = substr($selected_pks, 0, strlen($selected_pks) - 2);
}
$form->add_label_selector("projectkinds", "Project Types", 0, $selected_pks, $icon, $link);



$selected_pts = "";
if(count($pos_types) > 0)
{
	$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["postype_id"], $pos_types))
		{
			$selected_pts .= $row["postype_name"] . ", ";
		}
	}
	$selected_pts = substr($selected_pts, 0, strlen($selected_pts) - 2);
}
$form->add_label_selector("pts", "POS Types", 0, $selected_pts, $icon, $link);


$selected_item_categories = "";
if(count($item_categories) > 0)
{
	$res = mysql_query($sql_item_categories) or dberror($sql_item_categories );
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["item_category_id"], $item_categories))
		{
			$selected_item_categories .= $row["item_category_name"] . ", ";
		}
	}
	$selected_item_categories = substr($selected_item_categories, 0, strlen($selected_item_categories) - 2);
}
$form->add_label_selector("item_categories", "Item Categories", 0, $selected_item_categories, $icon, $link);





$form->add_section("Orders");
$form->add_edit("sufrom", "Submitted from", 0, to_system_date($sufrom), TYPE_DATE, 10);
$form->add_edit("suto", "Submitted to", 0, to_system_date($suto), TYPE_DATE, 10);

$form->add_button("save", "Save Filter");

if(check_if_query_has_fields($query_id) == true)
{
	$form->add_button("execute", "Execute Query");
}
$form->add_button("back", "Back to the List of Queries");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Process buttons
*********************************************************************/ 
if($form->button("save") or $form->button("supplier_id"))
{
	

	if($form->validate())
	{
		$new_filter = array();
		$new_filter["cl"] =  $form->value("client_id"); //client
		$new_filter["supp"] =  $form->value("supplier_id"); //supplier
		$new_filter["forw"] =  $form->value("forwarder_id"); //forwarder
		$new_filter["sufrom"] =  from_system_date($form->value("sufrom")); //submitted from
		$new_filter["suto"] =  from_system_date($form->value("suto")); //submitted to
		$new_filter["ct"] =  $filter["ct"]; //client types
		$new_filter["re"] =  $filter["re"]; //geografical region
		$new_filter["gr"] =  $filter["gr"]; // Supplied region
		$new_filter["co"] =  $filter["co"]; // country
		$new_filter["pct"] =  $filter["pct"]; // Legal Type
		$new_filter["pk"] =  $filter["pk"]; // project Type
		$new_filter["pt"] =  $filter["pt"]; // POS Type
		$new_filter["icat"] =  $filter["icat"]; // Item Categories

		$sql = "update orderqueries " . 
			   "set orderquery_filter = " . dbquote(serialize($new_filter))  . ", " . 
			   "date_modified = " . dbquote(date("Y-m-d H:s:i")) . ", " . 
				"user_modified = " . dbquote(user_login()) .
			   " where orderquery_id = " . param("query_id");

		$result = mysql_query($sql) or dberror($sql);
	}
}
elseif($form->button("back"))
{
	redirect("order_queries.php");
}
elseif($form->button("execute"))
{
	redirect("order_queries_query_xls.php?query_id=" . param("query_id"));
}

/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page("queries");

require_once("include/mis_page_actions.php");


$page->header();
$page->title("Edit Order Query - Filter");

require_once("include/orders_query_tabs.php");

$form->render();

?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#clienttypes_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/order_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=ct'
    });
    return false;
  });
  $('#salesregions_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/order_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=re'
    });
    return false;
  });
  $('#regions_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/order_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=gr'
    });
    return false;
  });
  $('#countries_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/order_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=co'
    });
    return false;
  });
  $('#cities_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/order_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=ci'
    });
    return false;
  });
  $('#pcts_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/order_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=pct'
    });
    return false;
  });
  $('#pls_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/order_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=pl'
    });
    return false;
  });
   $('#fscs_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/order_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=fscs'
    });
    return false;
  });
  $('#pts_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/order_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=pt'
    });
    return false;
  });
  $('#subclasses_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/order_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=sc'
    });
    return false;
  });
  $('#projectkinds_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/order_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=pk'
    });
    return false;
  });
  $('#project_type_subclasses_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/order_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=ptsc'
    });
    return false;
  });
  
  $('#item_categories_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/mis/order_queries_query_filter_selector.php?query_id=<?php echo $query_id;?>&s=icat'
    });
    return false;
  });

});
</script>


<?php
$page->footer();
?>