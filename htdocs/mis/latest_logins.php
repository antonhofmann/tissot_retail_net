<?php
/********************************************************************

    latest_logins.php

    Latest logins of each user

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-10-24
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-10-24
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_queries");


if(count($_POST) > 0)
{
	foreach($_POST as $u=>$value)
	{
		$uid = str_replace('u', '', $u);
		$sql = "update users set user_active = 0, " . 
			   " date_modified = " . dbquote(date("Y-m-d")) . ", " . 
			   " user_modified = " . dbquote(user_login()) . 
			   " where user_id = " . $uid;
		$result = mysql_query($sql) or dberror($sql);

	}
}


$xls_link = "latest_logins_xls.php";

/********************************************************************
    prepare all data needed
*********************************************************************/
$record_content = "";
$record_content .= '<tr>';
$record_content .= '<td style="border-bottom:1px solid #aaaaaa;">';
$record_content .= '<strong>Company</strong>';
$record_content .= '</td>';
$record_content .= '<td style="border-bottom:1px solid #aaaaaa;">';
$record_content .= '<strong>User</strong>';
$record_content .= '</td>';
$record_content .= '<td style="border-bottom:1px solid #aaaaaa;">';
$record_content .= '<strong>Last Login</strong>';
$record_content .= '</td>';
$record_content .= '<td style="border-bottom:1px solid #aaaaaa;">';
$record_content .= '';
$record_content .= '</td>';
$record_content .= '</tr>';



//get all users
$statistics_data = array();


$sql = "select statistic_user, max(statistic_date) as lastdate " . 
       "from statistics where statistic_user > 0 " .
       "group by statistic_user";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$statistics_data[$row["statistic_user"]] = $row["lastdate"];
}

asort($statistics_data);

$users = array();
$sql = "select user_id, concat(user_name, ' ', user_firstname) as username, address_company, users.date_created as dc " . 
       " from users " . 
	   " inner join addresses on address_id = user_address " . 
	   " where user_active = 1 " . 
	   " order by user_name, user_firstname";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$users[$row["user_id"]] = $row;
}


foreach($statistics_data as $user_id=>$last_date)
{			     
	
	if(array_key_exists($user_id, $users))
	{
		$record_content .= '<tr>';
		$record_content .= '<td style="border-bottom:1px solid #aaaaaa;">';
		$record_content .= $users[$user_id]["address_company"] . "&nbsp;&nbsp;&nbsp;";
		$record_content .= '</td>';
		$record_content .= '<td style="border-bottom:1px solid #aaaaaa;">';
		$record_content .= '<a href="/admin/user.php?id=' . $users[$user_id]["user_id"] . '" target="_blank">' . $users[$user_id]["username"] . " (" . $user_id . ")" . "</a>&nbsp;&nbsp;&nbsp;";
		$record_content .= '</td>';
		$record_content .= '<td style="border-bottom:1px solid #aaaaaa;">';
		$record_content .= $last_date;
		$record_content .= '</td>';
		$record_content .= '<td style="border-bottom:1px solid #aaaaaa;">';
		$record_content .= '<input type="checkbox" name="u' . $user_id . '">';
		$record_content .= '</td>';
		$record_content .= '</tr>';
		unset($users[$user_id]);
	}
	

}

$page_content = '<form name="latest_logins" action="latest_logins.php" id="latest_logins" method="POST">';
$page_content .= "<h1>User's last login</h1>";
$page_content .= '<a href="' . $xls_link . '" target="_blank">Export to Excel</a><br /><br />';
$page_content .= '<table>';
$page_content .= $record_content;
$page_content .= '</table>';



//users without activity

$record_content = "";
$record_content .= '<tr>';
$record_content .= '<td style="border-bottom:1px solid #aaaaaa;">';
$record_content .= '<strong>Company</strong>';
$record_content .= '</td>';
$record_content .= '<td style="border-bottom:1px solid #aaaaaa;">';
$record_content .= '<strong>User</strong>';
$record_content .= '</td>';

$record_content .= '<td style="border-bottom:1px solid #aaaaaa;">';
$record_content .= '<strong>Account Created</strong>';
$record_content .= '</td>';
$record_content .= '<td style="border-bottom:1px solid #aaaaaa;">';
$record_content .= '<strong>&nbsp;</strong>';
$record_content .= '</td>';
$record_content .= '</tr>';


foreach($users as $user_id=>$user_data)
{			     
	
	$record_content .= '<tr>';
	$record_content .= '<td style="border-bottom:1px solid #aaaaaa;">';
	$record_content .= $user_data["address_company"] . "&nbsp;&nbsp;&nbsp;";
	$record_content .= '</td>';
	$record_content .= '<td style="border-bottom:1px solid #aaaaaa;">';
	$record_content .= '<a href="/admin/user.php?id=' . $user_data["user_id"] . '" target="_blank">' . $user_data["username"] . " (" . $user_id . ")" . "</a>&nbsp;&nbsp;&nbsp;";
	$record_content .= '</td>';
	
	$record_content .= '<td style="border-bottom:1px solid #aaaaaa;">';
	$record_content .= $user_data["dc"] . "&nbsp;&nbsp;&nbsp;";
	$record_content .= '</td>';
	$record_content .= '<td style="border-bottom:1px solid #aaaaaa;">';
	$record_content .= '<input type="checkbox" name="u' . $user_id . '">';
	$record_content .= '</td>';
	$record_content .= '</tr>';

}

$page_content .= '<p>&nbsp;</p><h1>Active users that never logged in since 5.2.2012</h1>';
$page_content .= '<table>';
$page_content .= $record_content;
$page_content .= '</table>';

$page_content .= '<p>&nbsp;</p>';
$page_content .= "<a href=\"javascript:void(0);\" onclick=\"$('form#latest_logins').submit();\">Set Selected users to inactive</a><br /><br />";
$page_content .= '</form>';


/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page("queries");

require "include/mis_page_actions.php";

$page->header();
$page->title("Latest Logins of Active Users");

echo $page_content;

$page->footer();

?>