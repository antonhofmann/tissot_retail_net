<?php
/********************************************************************

    order_queries_grouping.php

    Set Grouping for the Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2016-08-19
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2016-08-19
    Version:        1.0.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if(!has_access("can_perform_all_queries") 
  and !has_access("can_perform_projects_query"))
{
	redirect("noaccess.php");
}
require_once "include/orders_query_check_access.php";

require_once "include/orders_query_get_functions.php";


/********************************************************************
    prepare all data needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("order_queries.php");
}

$query_id = param("query_id");

$orderquery = get_query_name($query_id);
$icon = "../pictures/add_item.gif";
$link = "javascript:open_selector('')";


//get Query Fields
$fields = array();

$sql = "select orderquery_fields, orderquery_grouping01, orderquery_grouping02, orderquery_grouping03 " .
       "from orderqueries " .
	   "where orderquery_id = " . $query_id;

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$tmp = unserialize($row["orderquery_fields"]);


	
	foreach($tmp as $key=>$field_array)
	{
		foreach($field_array as $field=>$caption)
		{
			$fields[$field] = $caption;
		}
	}

	$grouping01 = $row["orderquery_grouping01"];
	$grouping02 = $row["orderquery_grouping02"];
	$grouping03 = $row["orderquery_grouping03"];
}

$sum_fields = $query_group_fields;

$choices = array();
foreach($sum_fields as $key=>$field)
{
	if(array_key_exists($field, $fields))
	{
		$choices[$key] = $fields[$field];
	}
}



/********************************************************************
    create form
*********************************************************************/

$form = new Form("orderqueries", "query_generator");

$form->add_hidden("query_id", param("query_id"));
$form->add_hidden("form_save", 1);

$form->add_section($orderquery["name"]);

$form->add_section(" ");
$form->add_comment("Please specify the grouping criteria of your query.");

if(count($choices) > 0)
{
	$form->add_list("orderquery_grouping01", "Group 1", $choices, 0, $grouping01);
}

if(count($choices) > 1)
{
	$form->add_list("orderquery_grouping02", "Group 2", $choices, 0, $grouping02);
}
else
{
	$form->add_hidden("orderquery_grouping02", 0);
}

if(count($choices) > 1)
{
	$form->add_list("orderquery_grouping03", "Group 3", $choices, 0, $grouping03);
}
else
{
	$form->add_hidden("orderquery_grouping03", 0);
}

if(count($choices) > 0)
{
	$form->add_button("submit", "Save Grouping", 0);
}
if(check_if_query_has_fields($query_id) == true)
{
	$form->add_button("execute", "Execute Query");
}
$form->add_button("back", "Back to the List of Queries");


/********************************************************************
    process form
*********************************************************************/
$form->populate();
$form->process();

if($form->button("back"))
{
	redirect("order_queries.php");
}
elseif($form->button("submit"))
{
	
	if($form->value("orderquery_grouping01") === '' and $form->value("orderquery_grouping02") > 0)
	{
		$form->error("Group 2 can not be indicated without indicating Group 1.");
	}
	elseif($form->value("orderquery_grouping02") === '' and $form->value("orderquery_grouping03") > 0)
	{
		$form->error("Group 3 can not be indicated without indicating Group 2.");
	}
	else
	{

		$sql = "Update orderqueries SET " . 
			   "orderquery_grouping01 = " . dbquote($form->value("orderquery_grouping01")) . ", " . 
			   "orderquery_grouping02 = " . dbquote($form->value("orderquery_grouping02"))  . ", " .
			   "orderquery_grouping03 = " . dbquote($form->value("orderquery_grouping03"))  . ", " . 
			   "date_modified = " . dbquote(date("Y-m-d H:s:i")) . ", " . 
		       "user_modified = " . dbquote(user_login()) . 
			   " where orderquery_id = " . param("query_id");

		$result = mysql_query($sql) or dberror($sql);

		redirect("order_queries_grouping.php?query_id=" . param("query_id"));
	}
}
elseif($form->button("execute"))
{
	redirect("order_queries_query_xls.php?query_id=" . param("query_id"));
}


/********************************************************************
    render
*********************************************************************/

$page = new Page("queries");

require_once("include/mis_page_actions.php");
$page->header();
$page->title("Edit Order Query - Grouping");

require_once("include/orders_query_tabs.php");

$form->render();

$page->footer();

?>


