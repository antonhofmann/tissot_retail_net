<?php
/********************************************************************

    order_queries_coloring.php

    Set Colors for the Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2016-08-19
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2016-08-19
    Version:        1.0.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if(!has_access("can_perform_all_queries") 
  and !has_access("can_perform_projects_query"))
{
	redirect("noaccess.php");
}


require_once "include/orders_query_check_access.php";
require_once "include/orders_query_get_functions.php";


/********************************************************************
    prepare all data needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("order_queries.php");
}

$query_id = param("query_id");

$orderquery = get_query_name($query_id);
$icon = "../pictures/add_item.gif";
$link = "javascript:open_selector('')";


//get Coloring Fields

$zebra = 0;
$zebra_color = "";
$sql = "select orderquery_zebra, orderquery_zebra_color, orderquery_colors from orderqueries " .
	   "where orderquery_id = " . $query_id;

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$zebra = $row["orderquery_zebra"];
	$zebra_color = $row["orderquery_zebra_color"];
}



/********************************************************************
    create form
*********************************************************************/

$form = new Form("orderqueries", "query_generator");

$form->add_hidden("query_id", param("query_id"));
$form->add_hidden("form_save", 1);

$form->add_section($orderquery["name"]);

$form->add_section(" ");
$form->add_comment("Please specify the coloring options of your query.");
$form->add_section("Zebra");
$form->add_checkbox("orderquery_zebra", "Use zebra for order lines", $zebra, 0, "Zebra");
$form->add_hidden("orderquery_zebra_color", $zebra_color);

$form->add_comment('<span style="cursor:pointer" id="czebra" class="color-box">Click to select the zebra color</span>');
$form->add_section(" ");

$form->add_button("submit", "Save Coloring", 0);


if(check_if_query_has_fields($query_id) == true)
{
	$form->add_button("execute", "Execute Query");
}

$form->add_button("back", "Back to the List of Queries");


/********************************************************************
    process form
*********************************************************************/
$form->populate();
$form->process();

if($form->button("back"))
{
	redirect("order_queries.php");
}
elseif($form->button("submit"))
{
	
	$sql = "Update orderqueries SET " . 
			   "orderquery_zebra = " . dbquote($form->value("orderquery_zebra")) . ", " .
		        "orderquery_zebra_color = " . dbquote($form->value("orderquery_zebra_color")) . ", " .
			   "date_modified = " . dbquote(date("Y-m-d H:s:i")) . ", " . 
		       "user_modified = " . dbquote(user_login()) . 
			   " where orderquery_id = " . param("query_id");

	$result = mysql_query($sql) or dberror($sql);

	redirect("order_queries_coloring.php?query_id=" . param("query_id"));
}
elseif($form->button("execute"))
{
	redirect("order_queries_query_xls.php?query_id=" . param("query_id"));
}


/********************************************************************
    render
*********************************************************************/

$page = new Page("queries");

require_once("include/mis_page_actions.php");
$page->header();
$page->title("Edit Order Query - Coloring");

require_once("include/orders_query_tabs.php");

$form->render();

?>

<script type="text/javascript">
	$('#czebra').colpick({
		colorScheme:'dark',
		layout:'rgbhex',
		color:'<?php echo $zebra_color;?>',
		onSubmit:function(hsb,hex,rgb,el) {
			$(el).css('background-color', '#'+hex);
			$(el).colpickHide();
			$('#orderquery_zebra_color').val(hex);
			
		},
		onChange:function(hsb,hex,rgb,el,bySetColor) {
			
		}
	})
	.css('background-color', '#<?php echo $zebra_color;?>');
</script>

<?php
$page->footer();

?>


