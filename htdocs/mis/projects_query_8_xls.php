<?php
/********************************************************************

   projects_query_8_xls.php

    Generate Excel File for Cost Monitoring Summary of Projects

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2007-07-11
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2007-07-11
    Version:        1.0.0

    Copyright (c) 2007, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_queries");

require_once "../include/xls/Writer.php";
require_once("projects_query_filter_strings.php");

/********************************************************************
    prepare Data Needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("projects_queries.php");
}


//check if filter is present
$query_name = "";
$print_query_filter = 0;
$sql = "select mis_query_name, mis_query_filter, mis_print_filter from mis_queries " .
	   "where mis_query_id = " . param("query_id");

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$query_name = $row["mis_query_name"];
	if($row["mis_print_filter"] == 1)
	{
		$print_query_filter = 1;
	}

	$filters = array();
	$filters = unserialize($row["mis_query_filter"]);

	foreach($filters as $key=>$value)
	{
		$st =  $filters["ptst"]; // Project State
		$fosc = $filters["fst"]; //from order state
		$tosc = $filters["tst"]; // to order state

		$clt = $filters["clt"]; // Client Types
		$pl = $filters["pl"]; // product lines
		$pls = $filters["plsc"]; // product line subclasses
		$pk = $filters["pk"]; // product kinds
		$pt = $filters["pt"]; // project types
		$suc = $filters["ptsc"]; // POS Type Subclass
		$lpt = $filters["pct"]; // project cost type
		$gr = $filters["gr"]; // Geografical Regions
		$re = $filters["re"]; // Supplied Regions
		$cnt = $filters["co"]; // Countries
		$sc = $filters["rtc"]; // Retail Coordinators
		$lrtc = $filters["lrtc"]; // Local Project Leaders

		$fdy = $filters["fdy"]; // Opening Year From
		$fdm = $filters["fdm"]; // Opening Month From
		$tdy = $filters["tdy"]; // Opening Year To
		$tdm = $filters["tdm"]; // Opening Month To

		$fdy1 = $filters["fdy1"]; // Closing Year From
		$fdm1 = $filters["fdm1"]; // Closing Month From
		$tdy1 = $filters["tdy1"]; // Closing Year To
		$tdm1 = $filters["tdm1"]; // Closing Month To
		$idvi = 0;  // Visuals
		if(array_key_exists("idvi", $filters))
		{
			$idvi = $filters["idvi"];
		}

		if(array_key_exists("areas", $filters))
		{
			$areas = $filters["areas"]; // Neighbourhood Locations
		
		}
		else
		{
			$areas = "";
		}

		$archive = $filters["arch"];

	}
}
else
{
	redirect("projects_queries.php");
}



$states = array(1=>"in process (steps 100-730)", 2=>"in deleivery (steps 740-750)", 3=>"operating (steps 800-890)", 4=>"cancelled (step 900)");

$header = $query_name;



$header = "KL Approval versus Real Costs: " . $header . " (" . date("d.m.Y G:i") . ")";

/********************************************************************
    prepare Data
*********************************************************************/
$filter = "";

$clt = substr($clt,0, strlen($clt)-1); // remove last comma
$clt = str_replace("-", ",", $clt);
if($clt) // client type
{
    $filter =  " where (address_client_type IN (" . $clt . "))";
	$_filter_strings["Client Types"] = get_filter_string("clt", $clt);
}

$pl = substr($pl,0, strlen($pl)-1); // remove last comma
$pl = str_replace("-", ",", $pl);
if($pl and $filter) // product line
{
	$filter .=  " and (project_product_line IN (" . $pl . "))";
	$_filter_strings["Product Lines"] = get_filter_string("pl", $pl);
}
elseif($pl) // product line
{
    $filter =  " where (project_product_line IN (" . $pl . "))";
	$_filter_strings["Product Lines"] = get_filter_string("pl", $pl);
}

$pls = substr($pls,0, strlen($pls)-1); // remove last comma
$pls = str_replace("-", ",", $pls);
if($pls and $filter) // product line subclasses
{
    $filter .=  " and project_product_line_subclass IN (" . $pls . ")";
	$_filter_strings["Product Line Subclasses"] = get_filter_string("pls", $pls);
}
elseif($pls)
{
    $filter =  " where project_product_line_subclass IN (" . $pls . ")";
	$_filter_strings["Product Line Subclasses"] = get_filter_string("pls", $pls);
}

$pk = substr($pk,0, strlen($pk)-1); // remove last comma
$pk = str_replace("-", ",", $pk);

if($pk and $filter) // product line
{
	$filter .=  " and (project_projectkind IN (" . $pk . "))";
	$_filter_strings["Project Types"] = get_filter_string("pk", $pk);
}
elseif($pk) // product kind
{
    $filter =  " where (project_projectkind IN (" . $pk . "))";
	$_filter_strings["Project Types"] = get_filter_string("pk", $pk);
}

$pt = substr($pt,0, strlen($pt)-1); // remove last comma
$pt = str_replace("-", ",", $pt);
if($pt and $filter) // project type
{
    $filter .=  " and (project_postype IN (" . $pt . "))";
	$_filter_strings["POS Types"] = get_filter_string("pt", $pt);
}
elseif($pt)
{
    $filter =  " where (project_postype IN (" . $pt . "))";
	$_filter_strings["POS Types"] = get_filter_string("pt", $pt);
}

$suc = substr($suc,0, strlen($suc)-1); // remove last comma
$suc = str_replace("-", ",", $suc);
if($suc and $filter) // POS Type Subclass
{
    $filter .=  " and (project_pos_subclass IN (" . $suc . "))";
	$_filter_strings["POS Type Subclasses"] = get_filter_string("suc", $suc);
}
elseif($suc)
{
   $filter =  " where (project_pos_subclass IN (" . $suc . "))";
   $_filter_strings["POS Type Subclasses"] = get_filter_string("suc", $suc);
}

$lpt = substr($lpt,0, strlen($lpt)-1); // remove last comma
$lpt = str_replace("-", ",", $lpt);
if($lpt and $filter) // project cost type
{
    $filter .=  " and (project_cost_type IN (" . $lpt . "))";
	$_filter_strings["Legal Types"] = get_filter_string("lpt", $lpt);
}
elseif($lpt)
{
   $filter =  " where (project_cost_type IN (" . $lpt . "))";
   $_filter_strings["Legal Types"] = get_filter_string("lpt", $lpt);
}




$gr = substr($gr,0, strlen($gr)-1); // remove last comma
$gr = str_replace("-", ",", $gr);
$re = substr($re,0, strlen($re)-1); // remove last comma
$re = str_replace("-", ",", $re);

if($filter) {

	if($gr and $re) 
	{
		$filter .=  " and (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	}
	elseif($gr) // geografical regions
	{
		$filter .=  " and (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	}
	elseif($re) //supplying
	{
		$filter .=  " and (country_region IN (" . $re . "))";
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	}
}
else
{
	if($gr and $re) 
	{
		$filter .=  " where (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	}
	elseif($gr) // geografical regions
	{
		$filter .=  " where (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	}
	elseif($re) //supplying
	{
		$filter .=  " where (country_region IN (" . $re . "))";
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	}
}

$cnt = substr($cnt,0, strlen($cnt)-1); // remove last comma
$cnt = str_replace("-", ",", $cnt);
if($cnt and $filter) // country
{
    $filter .=  " and (order_shop_address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);

}
elseif($cnt)
{
    $filter =  " where (order_shop_address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);
}


$sc = substr($sc,0, strlen($sc)-1); // remove last comma
$sc = str_replace("-", ",", $sc);
if($sc and $filter) // Retail Coordinator
{
    $filter .=  " and (project_retail_coordinator IN (" . $sc . "))";
	$_filter_strings["Project Leader"] = get_filter_string("sc", $sc);

}
elseif($sc)
{
    $filter =  " where (project_retail_coordinator IN (" . $sc . "))";
	$_filter_strings["Project Leader"] = get_filter_string("sc", $sc);
}

$lrtc = substr($lrtc,0, strlen($lrtc)-1); // remove last comma
$lrtc = str_replace("-", ",", $lrtc);
if($lrtc and $filter) // Retail Coordinator
{
    $filter .=  " and (project_local_retail_coordinator IN (" . $lrtc . "))";
	$_filter_strings["Local Project Leader"] = get_filter_string("lrtc", $lrtc);

}
elseif($lrtc)
{
    $filter =  " where (project_local_retail_coordinator IN (" . $lrtc . "))";
	$_filter_strings["Local Project Leader"] = get_filter_string("lrtc", $lrtc);
}


if($filter) // opened from
{
	if($fdy > 0)
	{
		$filter.=  " and YEAR(project_actual_opening_date) >= " . $fdy;
		$_filter_strings["Opened from"] = $fdy;
	}
}
else
{
	if($fdy > 0)
	{
		$filter.=  " where YEAR(project_actual_opening_date) >= " . $fdy;
		$_filter_strings["Opened from"] = $fdy;
	}
}

if($filter) // opened to
{
	if($tdy > 0)
	{
		$filter.=  " and YEAR(project_actual_opening_date) <= " . $tdy;
		$_filter_strings["Opened to"] = $tdy;
	}
}
else
{
	if($tdy > 0)
	{
		$filter.=  " where YEAR(project_actual_opening_date) <= " . $tdy;
		$_filter_strings["Opened to"] = $tdy;
	}
}

if($filter) // closed from
{
	if($fdy1 > 0)
	{
		$filter.=  " and YEAR(project_shop_closingdate) >= " . $fdy1;
		$_filter_strings["Closed from"] = $fdy1;
	}
}
else
{
	if($fdy1 > 0)
	{
		$filter.=  " where YEAR(project_shop_closingdate) >= " . $fdy1;
		$_filter_strings["Closed from"] = $fdy1;
	}
}

if($filter) // closed to
{
	if($tdy1 > 0)
	{
		$filter.=  " and YEAR(project_shop_closingdate) <= " . $tdy1;
		$_filter_strings["Closed to"] = $tdy1;
	}
}
else
{
	if($tdy1 > 0)
	{
		$filter.=  " where YEAR(project_shop_closingdate) <= " . $tdy1;
		$_filter_strings["Closed to"] = $tdy1;
	}
}

if($filter and $idvi) // Visuals
{
    $filter.=  " and project_uses_icedunes_visuals = 1";
	$_filter_strings["Only Projects with Visulas"] = "yes";
}
elseif($idvi)
{
	$filter =  " where project_uses_icedunes_visuals = 1 ";
	$_filter_strings["Only Projects with Visulas"] = "yes";
}


//filter posareas get all matching orders
$areas = substr($areas,0, strlen($areas)-1); // remove last comma
$areas = str_replace("-", ",", $areas);

if($areas)
{
	
	$order_ids = array();
	$sql = "select project_order " . 
	       "from projects " .
		   "left join orders on order_id = project_order " .
		   "left join project_costs on project_cost_order = project_order " .
		   "left join project_costtypes on project_costtype_id = project_cost_type " .
		   "left join projectkinds on projectkind_id = project_projectkind " .
		   "left join postypes on postype_id = project_postype " . 
		   "left join product_lines on product_line_id = project_product_line " .
		   "left join addresses on address_id = order_client_address " .
		   "left join countries on country_id = order_shop_address_country " . 
		   $filter;
	
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$sql_a = "select DISTINCT posorder_order " . 
			     "from posorders " . 
			     "left join posareas on posorder_posaddress = posarea_posaddress " .
			     "where posorder_order = " . $row["project_order"] . 
			     " and posarea_area IN (" . $areas . ")";

		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			$order_ids[$row["project_order"]] = $row["project_order"];
		}

		$sql_a = "select DISTINCT posorder_order " . 
			     "from posorderspipeline " . 
			     "left join posareaspipeline on posorder_posaddress = posarea_posaddress " .
			     "where posorder_order = " . $row["project_order"] . 
			     " and posarea_area IN (" . $areas . ")";

		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			$order_ids[$row["project_order"]] = $row["project_order"];
		}
	
	}

	if(count($order_ids) > 0)
	{
	
		
		if($filter) // Visuals
		{
			$filter.=  " and project_order IN (" . implode ( ',' , $order_ids ) . ") ";
		}
		else
		{
			$filter =  " where project_order IN (" . implode ( ',' , $order_ids ) . ") ";
		}
	}
	$_filter_strings["Neighbourhood Locations"] = get_filter_string("areas", $areas);
}


//include only projects from Archive
if($archive == 1)
{
	$_filter_strings["Include only archived projects"] = "yes";
}

if($filter and $archive == 1)
{
	$filter.= " and order_archive_date is not null and order_archive_date <> '0000-00-00' ";
	
}
elseif($archive == 1)
{
	$filter = " and order_archive_date is not null and order_archive_date <> '0000-00-00' ";
}


$sql = "select project_id, project_number, project_projectkind, project_order, " .
       "DATE_FORMAT(project_actual_opening_date, '%d.%m.%Y') as openingdate, " .       
       "project_cost_type, project_costtype_text, project_cost_sqms, order_actual_order_state_code, postype_name, product_line_name, " .
       "country_name, project_planned_amount_current_year, order_archive_date, projectkind_code, " .
       "project_actual_opening_date, project_shop_closingdate, order_cancelled, " .
       "order_shop_address_place, order_shop_address_company, order_shop_address_address, order_shop_address_country, country_region " .
       "from projects " .
       "left join orders on order_id = project_order " .
       "left join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join projectkinds on projectkind_id = project_projectkind " .
       "left join postypes on postype_id = project_postype " . 
       "left join product_lines on product_line_id = project_product_line " .
       "left join addresses on address_id = order_client_address " .
       "left join countries on country_id = order_shop_address_country " . 
       $filter . 
       " order by country_name, order_shop_address_place, project_number";


/********************************************************************
    prepare Excel Sheet
*********************************************************************/

$filename = "kl_approval_versus_real_costs.xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename); 
$xls->setVersion(8);
$sheet =& $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();


//formats
$header_row =& $xls->addFormat();
$header_row->setSize(10);
$header_row->setAlign('left');
$header_row->setBold();


$f_cellgroup =& $xls->addFormat();
$f_cellgroup->setSize(8);
$f_cellgroup->setAlign('left');
$f_cellgroup->setBorder(1);
$f_cellgroup->setBold();

$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);


$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_decimal =& $xls->addFormat();
$f_decimal->setSize(8);
$f_decimal->setAlign('right');
$f_decimal->setBorder(1);
$f_decimal->setNumFormat('0.00');

$f_date =& $xls->addFormat();
$f_date->setSize(8);
$f_date->setAlign('left');
$f_date->setBorder(1);
$f_date->setNumFormat('DD.MM.YYYY');


$f_budgetok =& $xls->addFormat();
$f_budgetok->setSize(8);
$f_budgetok->setAlign('right');
$f_budgetok->setBorder(1);
$f_budgetok->setBold();
$f_budgetok->setPattern(18);
$f_budgetok->setColor("white");
$f_budgetok->setBgColor('green');

$f_budget100 =& $xls->addFormat();
$f_budget100->setSize(8);
$f_budget100->setAlign('right');
$f_budget100->setBorder(1);
$f_budget100->setBold();
$f_budget100->setPattern(18);
$f_budget100->setColor("white");
$f_budget100->setBgColor('orange');

$f_budget105 =& $xls->addFormat();
$f_budget105->setSize(8);
$f_budget105->setAlign('right');
$f_budget105->setBorder(1);
$f_budget105->setBold();
$f_budget105->setPattern(18);
$f_budget105->setColor("white");
$f_budget105->setBgColor('red');


$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_notrealized =& $xls->addFormat();
$f_notrealized->setSize(8);
$f_notrealized->setAlign('right');
$f_notrealized->setBorder(1);
$f_notrealized->setpattern(18);
$f_notrealized->setColor("white");
$f_notrealized->setBgColor('red');

$f_inprogress =& $xls->addFormat();
$f_inprogress->setSize(8);
$f_inprogress->setAlign('right');
$f_inprogress->setBorder(1);
$f_inprogress->setpattern(18);
$f_inprogress->setColor("white");
$f_inprogress->setBgColor('navy');

$f_open =& $xls->addFormat();
$f_open->setSize(8);
$f_open->setAlign('right');
$f_open->setBorder(1);
$f_open->setpattern(18);
$f_open->setColor("white");
$f_open->setBgColor('green');

$f_closed =& $xls->addFormat();
$f_closed->setSize(8);
$f_closed->setAlign('right');
$f_closed->setBorder(1);
$f_closed->setpattern(18);
$f_closed->setColor("white");
$f_closed->setBgColor('grey');


$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);


$f_number_bold =& $xls->addFormat();
$f_number_bold->setSize(8);
$f_number_bold->setAlign('right');
$f_number_bold->setBorder(1);
$f_number_bold->setBold();

$f_used =& $xls->addFormat();
$f_used->setSize(8);
$f_used->setBorder(1);
$f_used->setpattern(18);
$f_used->setBgColor('yellow');


//captions
$captions = array();
$captions[] = "Not realized";
$captions[] = "Closed";
$captions[] = "Operating";
$captions[] = "In Progress";
$sheet->setColumn(0, 3, 3);
$captions[] = "Project Number";
$sheet->setColumn(4, 4, 9);
$captions[] = "Country";
$sheet->setColumn(5, 5, 16);
$captions[] = "Project Name";
$sheet->setColumn(6, 6, 50);
$captions[] = "POS Type";
$sheet->setColumn(7, 7, 8);
$captions[] = "Product Line";
$sheet->setColumn(8, 8, 11);
$captions[] = "Opening Date";
$sheet->setColumn(9, 9, 9);

$sheet->setColumn(10, 10, 4);
$captions[] = "C=Corporate, F=Franchise";
$sheet->setColumn(11, 11, 13);
$captions[] = "N=New, R=Renovation, TR=Take Over/Renovation, T=Take Over, L=Lease Renewal, RL=Relocation, ER=Equip Retailer";
$sheet->setColumn(12, 12, 4);
$captions[] = "Sales Surface sqm";

$captions[] = "";
$captions[] = "Local/\nConstruction";
$captions[] = "Store fixturing / Furniture";
$captions[] = "Architectural Services";
$captions[] = "Equipment";
$captions[] = "Others";
$captions[] = "Grand Total in CHF";

$captions[] = "";
$captions[] = "Local/\nConstruction";
$captions[] = "Store fixturing / Furniture";
$captions[] = "Architectural Services";
$captions[] = "Equipment";
$captions[] = "Others";
$captions[] = "Grand Total in CHF";

$captions[] = "";
$captions[] = "Local/\nConstruction";
$captions[] = "Store fixturing / Furniture";
$captions[] = "Architectural Services";
$captions[] = "Equipment";
$captions[] = "Others";
$captions[] = "Grand Total in CHF";

$captions[] = "";
$captions[] = "Local/\nConstruction";
$captions[] = "Store fixturing / Furniture";
$captions[] = "Architectural Services";
$captions[] = "Equipment";
$captions[] = "Others";
$captions[] = "Grand Total in CHF";




/********************************************************************
    write all data
*********************************************************************/


$sheet->write(0, 0, $header, $header_row);
$row_index = 2;

if($print_query_filter == 1)
{
	foreach($_filter_strings as $key=>$value)
	{
		 $sheet->write($row_index, 0,$key . ": " . $value);
		 $row_index++;
	}
	$row_index++;
}


/********************************************************************
    write all captions
*********************************************************************/
$sheet->writeRow($row_index, 0, $captions, $f_caption);
$sheet->setRow($row_index, 110);
$row_index++;

$cellgroup = array();
$cellgroup[] = "Investment as per CER KL-approved in CHF";
$cellgroup[] =  "";
$cellgroup[] =  "";
$cellgroup[] =  "";
$cellgroup[] =  "";
$cellgroup[] =  "";
$sheet->mergeCells($row_index, 14, $row_index, 19);
$sheet->writeRow($row_index,14, $cellgroup, $f_cellgroup);


$cellgroup = array();
$cellgroup[] = "Real Cost in CHF";
$cellgroup[] =  "";
$cellgroup[] =  "";
$cellgroup[] =  "";
$cellgroup[] =  "";
$cellgroup[] =  "";
$sheet->mergeCells($row_index, 21, $row_index, 26);
$sheet->writeRow($row_index,21, $cellgroup, $f_cellgroup);

$cellgroup = array();
$cellgroup[] = "Variance";
$cellgroup[] =  "";
$cellgroup[] =  "";
$cellgroup[] =  "";
$cellgroup[] =  "";
$cellgroup[] =  "";
$sheet->mergeCells($row_index, 28, $row_index, 33);
$sheet->writeRow($row_index,28, $cellgroup, $f_cellgroup);

$cellgroup = array();
$cellgroup[] = "Real Cost in Percentage of CER KL-approved Cost";
$cellgroup[] =  "";
$cellgroup[] =  "";
$cellgroup[] =  "";
$cellgroup[] =  "";
$cellgroup[] =  "";
$sheet->mergeCells($row_index, 35, $row_index, 40);
$sheet->writeRow($row_index,35, $cellgroup, $f_cellgroup);

$sheet->setColumn(13, 13, 2);
$sheet->setColumn(14, 19, 10);
$sheet->setColumn(20, 20, 2);
$sheet->setColumn(21, 26, 10);
$sheet->setColumn(27, 27, 2);
$sheet->setColumn(28, 33, 10);
$sheet->setColumn(34, 34, 2);
$sheet->setColumn(35, 40, 10);


$row_index++;
$row_index++;
$cell_index = 4;

$c01 = 0;
$c02 = 0;
$c03 = 0;
$c04 = 0;

$c = "";
$s = "";
$a = "";
$t = "";

$list_totals = array();
for($i=1;$i<19;$i++)
{
    $list_totals[$i] = 0;
}

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
    
	//check if CER was approved
	$sql_p = "select count(project_milestone_id) as num_recs " . 
		     " from project_milestones " . 
		     " where project_milestone_project = " . dbquote($row["project_id"]) . 
		     " and project_milestone_milestone = 12 " .
		     " and project_milestone_date is not null " . 
		     " and project_milestone_date <> '0000-00-00'";

	$res_p = mysql_query($sql_p) or dberror($sql_p);
	$row_p = mysql_fetch_assoc($res_p);
	
	if($row_p["num_recs"] == 1)
	{
	
		//get investment
		
		$sql_p =  "select * from project_costs " .
				  "where project_cost_order = " . $row["project_order"];

		$res_p = mysql_query($sql_p) or dberror($sql_p);
		$row_p = mysql_fetch_assoc($res_p);

		$project_investment_budget = $row_p["project_cost_budget"];
		$project_investmentt_budget1 = number_format($row_p["project_cost_budget"], 2, ".", "");
		$project_investmentt_sqms = $row_p["project_cost_sqms"];
		
		/*
		$project_investment_construction = $row_p["project_cost_construction"];
		$project_investment_fixturing = $row_p["project_cost_fixturing"];
		$project_investment_architectural = $row_p["project_cost_architectural"];
		$project_investment_equipment = $row_p["project_cost_equipment"];
		$project_investment_other = $row_p["project_cost_other"];
		$project_investment_total = $project_investment_construction + $project_investment_fixturing + $project_investment_architectural + $project_investment_equipment + $project_investment_other;
		*/

		//get cer data
		$cer_investments = array();
		$cer_investments_kl_approved = array();
		$cer_total = 0;
		$cer_present = 0; // old projects do not have a CER since it was introduced only in 2008

		$sql_cer = "select * from cer_basicdata where cer_basicdata_project = " . $row["project_id"];
		$res_cer = mysql_query($sql_cer) or dberror($sql_cer);

		if ($row_cer = mysql_fetch_assoc($res_cer))
		{
			$exchange_rate = $row_cer["cer_basicdata_exchangerate"];
			$factor = $row_cer["cer_basicdata_factor"];
			
				
			$sql_cer = "select cer_investment_type, cer_investment_amount_cer_loc, cer_investment_amount_cer_loc_approved, " . 
						"posinvestment_type_name from cer_investments " .
						"left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
						"where cer_investment_cer_version = 0 " . 
						" and cer_investment_type in (1, 3, 5, 7, 11, 13) and cer_investment_project = " . $row["project_id"];

			$res_cer = mysql_query($sql_cer) or dberror($sql_cer);

			while ($row_cer = mysql_fetch_assoc($res_cer))
			{
				if(!$factor){$factor = 1;}
				$cer_investments[$row_cer["cer_investment_type"]] = $exchange_rate*$row_cer["cer_investment_amount_cer_loc"]/$factor;
				$cer_investments_kl_approved[$row_cer["cer_investment_type"]] = $exchange_rate*$row_cer["cer_investment_amount_cer_loc_approved"]/$factor;
			}
		

		
			if(array_key_exists(1,$cer_investments_kl_approved) and $cer_investments_kl_approved[1] > 0){
				$project_investment_construction = $cer_investments_kl_approved[1];
			}
			else
			{
				//$project_investment_construction = $cer_investments[1];
				$project_investment_construction = 0;
			}

			if(array_key_exists(3,$cer_investments_kl_approved) and $cer_investments_kl_approved[3] > 0){
				$project_investment_fixturing = $cer_investments_kl_approved[3];
			}
			else
			{
				//$project_investment_fixturing = $cer_investments[3];
				$project_investment_fixturing = 0;
			}

			if(array_key_exists(5,$cer_investments_kl_approved) and $cer_investments_kl_approved[5] > 0){
				$project_investment_architectural = $cer_investments_kl_approved[5];
			}
			else
			{
				//$project_investment_architectural = $cer_investments[5];
				$project_investment_architectural = 0;
			}

			if(array_key_exists(7,$cer_investments_kl_approved) and $cer_investments_kl_approved[7] > 0){
				$project_investment_equipment = $cer_investments_kl_approved[7];
			}
			else
			{
				//$project_investment_equipment = $cer_investments[7];
				$project_investment_equipment = 0;
			}

			if(array_key_exists(11,$cer_investments_kl_approved) and array_key_exists(13,$cer_investments_kl_approved) and $cer_investments_kl_approved[11] > 0 and $cer_investments_kl_approved[13] > 0){
				$project_investment_other = $cer_investments_kl_approved[11] + $cer_investments_kl_approved[13];
			}
			elseif(array_key_exists(11,$cer_investments_kl_approved) and $cer_investments_kl_approved[11] > 0) {
				$project_investment_other = $cer_investments_kl_approved[11] + $cer_investments[13];
			}
			elseif(array_key_exists(13,$cer_investments_kl_approved) and $cer_investments_kl_approved[13] > 0) {
				$project_investment_other = $cer_investments_kl_approved[11] + $cer_investments_kl_approved[13];
			}
			else
			{
				//$project_investment_other = $cer_investments_kl_approved[11] + $cer_investments_kl_approved[13];
				$project_investment_other = 0;
			}

			
			$project_investment_total = $project_investment_construction + $project_investment_fixturing + $project_investment_architectural + $project_investment_equipment + $project_investment_other;
		}
		else
		{
			$project_investment_construction = 0;
			$project_investment_fixturing = 0;
			$project_investment_architectural = 0;
			$project_investment_equipment = 0;
			$project_investment_other = 0;
			$project_investment_total = 0;
		}


		//$shop_address = $row["order_shop_address_place"] . ", " . $row["order_shop_address_address"];
		$shop_address = $row["order_shop_address_place"] . ", " . $row["order_shop_address_company"];

		//get real cost
		$typetotals_real = array();
		$typetotals_real[0] = 0;
		$typetotals_real[2] = 0;
		$typetotals_real[6] = 0;
		$typetotals_real[7] = 0;
		$typetotals_real[8] = 0;
		$typetotals_real[9] = 0;
		$typetotals_real[10] = 0;
		$typetotals_real[11] = 0;
		$typetotals_real[31] = 0;

		$typetotals_difference_in_cost1 = array();

		$investment = 0;

		$sql_p = "select order_item_cost_group, order_item_type, ".
				 "order_item_system_price, order_item_quantity, ".
				 "order_item_system_price_freezed, order_item_quantity_freezed, order_item_real_system_price ".
				 "from order_items ".
				 "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
				 "   and order_item_order=" . $row["project_order"] . 
				 "   and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION .
				 "   or order_item_type = " . ITEM_TYPE_SERVICES .
				 "   or order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST .
				 " ) order by order_item_cost_group";

		$res_p = mysql_query($sql_p) or dberror($sql_p);
		while($row_p = mysql_fetch_assoc($res_p))
		{
			$typetotals_real[$row_p["order_item_cost_group"]] =  $typetotals_real[$row_p["order_item_cost_group"]] + $row_p["order_item_real_system_price"] * $row_p["order_item_quantity"];
		}

		//get catalogue items from orders assigned to the project
		$sql_p = "select order_item_quantity * order_items_in_project_real_price as amount,  " . 
			   " order_items_in_project_costgroup_id ". 
			   " from order_items_in_projects " .
			   " left join order_items on order_item_id = order_items_in_project_order_item_id " .
			   " left join project_cost_groupnames on project_cost_groupname_id =  order_items_in_project_costgroup_id " .
			   " where order_items_in_project_project_order_id = " . $row["project_order"] . 
			   " and order_items_in_project_costgroup_id > 0 ";

		
		$res_p = mysql_query($sql_p) or dberror($sql_p);
		while ($row_p = mysql_fetch_assoc($res_p))
		{
			$typetotals_real[$row_p["order_items_in_project_costgroup_id"]] =  $typetotals_real[$row_p["order_items_in_project_costgroup_id"]] + $row_p["amount"];

		}


		$project_cost_real = 0;

		foreach($typetotals_real as $key=>$value)
		{
			 $project_cost_real = $project_cost_real + $value;
		}


		//start printing the output


		//shop not realized
		if($row["order_cancelled"] == 1
			and ($row["project_actual_opening_date"] == NULL
			or $row["project_actual_opening_date"] == '0000-00-00')
		  )
		{
			$c01++;
			$sheet->write($row_index, 0, $c01, $f_notrealized);
		}
		else
		{
			$sheet->write($row_index, 0, "", $f_normal);
		}

		
		// shop closed
		if($row["project_shop_closingdate"] != "0000-00-00" and $row["project_shop_closingdate"])
		{
			$c02++;
			$sheet->write($row_index, 1, $c02, $f_closed);
			$shop_closed = 1;
		}
		else
		{
			$sheet->write($row_index, 1, "", $f_normal);
			$shop_closed = 0;
			
		}

		//shop operating
		if(!$shop_closed and $row["order_actual_order_state_code"] <> '900')
		{
			if($row["project_actual_opening_date"] > '0000-00-00' and $row["project_actual_opening_date"] <= date("Y-m-d"))
			{
				$c03++;
				$sheet->write($row_index, 2, $c03, $f_open);
			}
			else
			{
				$sheet->write($row_index, 2, "", $f_normal);
			}
		}
		else
		{
			$sheet->write($row_index, 2, "", $f_normal);
		}

		
		// in progress
		
	   if($shop_closed != 1) // not closed
		{
			if($row["order_actual_order_state_code"] != '900' and (!$row["order_archive_date"] ))
			{
				$c04++;
				$sheet->write($row_index, 3, $c04, $f_inprogress);
			}
			else
			{
				$sheet->write($row_index, 3, "", $f_normal);
			}
		}
		else
		{
			$sheet->write($row_index, 3, "", $f_normal);
		}

		
		$sheet->write($row_index, $cell_index, $row["project_number"], $f_normal);

		$sheet->write($row_index, $cell_index+1, $row["country_name"], $f_normal);
		$sheet->write($row_index, $cell_index+2, $shop_address, $f_normal);
		$sheet->write($row_index, $cell_index+3, $row["postype_name"], $f_normal);
		$sheet->write($row_index, $cell_index+4, $row["product_line_name"], $f_normal);

		$sheet->write($row_index, $cell_index+5, mysql_date_to_xls_date($row["project_actual_opening_date"]), $f_date);

		$sheet->write($row_index, $cell_index+6, substr($row["project_costtype_text"], 0, 1), $f_center);

		if($row["project_projectkind"] > 0)
		{
			$sheet->write($row_index, $cell_index+7, $row["projectkind_code"], $f_center);
		}
		else
		{
			$sheet->write($row_index, $cell_index+7, "", $f_center);
			   
		}

		if($row["project_cost_sqms"] > 0)
		{
			$sheet->write($row_index, $cell_index+8, number_format($row["project_cost_sqms"], 0, ".", ""), $f_decimal);
		}
		else
		{
			$sheet->write($row_index, $cell_index+8, "", $f_number);
		}

		

		//investment
		$sheet->write($row_index, $cell_index+9, "");
		$sheet->write($row_index, $cell_index+10, number_format($project_investment_construction, 2, ".", ""), $f_number);
		$sheet->write($row_index, $cell_index+11, number_format($project_investment_fixturing, 2, ".", ""), $f_number);
		$sheet->write($row_index, $cell_index+12, number_format($project_investment_architectural, 2, ".", ""), $f_number);
		$sheet->write($row_index, $cell_index+13, number_format($project_investment_equipment, 2, ".", ""), $f_number);
		$sheet->write($row_index, $cell_index+14, number_format($project_investment_other, 2, ".", ""), $f_number);
		$sheet->write($row_index, $cell_index+15, number_format($project_investment_total, 2, ".", ""), $f_number_bold);

		$list_totals[1] = $list_totals[1] + $project_investment_construction;
		$list_totals[2] = $list_totals[2] + $project_investment_fixturing;
		$list_totals[3] = $list_totals[3] + $project_investment_architectural;
		$list_totals[4] = $list_totals[4] + $project_investment_equipment;
		$list_totals[5] = $list_totals[5] + $project_investment_other;
		$list_totals[6] = $list_totals[6] + $project_investment_total;

		
		//real cost
		$sheet->write($row_index, $cell_index+16, "");
		$sheet->write($row_index, $cell_index+17, number_format($typetotals_real[7], 2, ".", ""), $f_number);
		$sheet->write($row_index, $cell_index+18, number_format($typetotals_real[2]+$typetotals_real[6]+$typetotals_real[10], 2, ".", ""), $f_number);
		$sheet->write($row_index, $cell_index+19, number_format($typetotals_real[9], 2, ".", ""), $f_number);
		$sheet->write($row_index, $cell_index+20, number_format($typetotals_real[11], 2, ".", ""), $f_number);
		$sheet->write($row_index, $cell_index+21, number_format($typetotals_real[8], 2, ".", ""), $f_number);
		$sheet->write($row_index, $cell_index+22, number_format($project_cost_real, 2, ".", ""), $f_number_bold);


		$list_totals[7] = $list_totals[7] + $typetotals_real[7];
		$list_totals[8] = $list_totals[8] + $typetotals_real[2] + $typetotals_real[6] + $typetotals_real[10];
		$list_totals[9] = $list_totals[9] + $typetotals_real[9];
		$list_totals[10] = $list_totals[10] + $typetotals_real[11];
		$list_totals[11] = $list_totals[11] + $typetotals_real[8];
		$list_totals[12] = $list_totals[12] + $project_cost_real;

		
		
		
		//variance

		$c = $typetotals_real[7] - $project_investment_construction;
		$s = ($typetotals_real[2]+$typetotals_real[6]+$typetotals_real[10]) - $project_investment_fixturing;
		$a = $typetotals_real[9] - $project_investment_architectural;
		$e = $typetotals_real[11] - $project_investment_equipment;
		$o = $typetotals_real[8] - $project_investment_other;
		$t = $project_cost_real - $project_investment_total;

		$list_totals[13] = $list_totals[13] + $c;
		$list_totals[14] = $list_totals[14] + $s;
		$list_totals[15] = $list_totals[15] + $a;
		$list_totals[16] = $list_totals[16] + $e;
		$list_totals[17] = $list_totals[17] + $o;
		$list_totals[18] = $list_totals[18] + $t;

		$sheet->write($row_index, $cell_index+23, "");
		$sheet->write($row_index, $cell_index+24, number_format($c, 2, ".", ""), $f_number);
		$sheet->write($row_index, $cell_index+25, number_format($s, 2, ".", ""), $f_number);
		$sheet->write($row_index, $cell_index+26, number_format($a, 2, ".", ""), $f_number);
		$sheet->write($row_index, $cell_index+27, number_format($e, 2, ".", ""), $f_number);
		$sheet->write($row_index, $cell_index+28, number_format($o, 2, ".", ""), $f_number);
		$sheet->write($row_index, $cell_index+29, number_format($t, 2, ".", ""), $f_number_bold);
		
		//percentage
		if($project_investment_construction == 0)
		{
			$c = "";
		}
		elseif($project_investment_construction > 0)
		{
			$c = $typetotals_real[7]/$project_investment_construction;
			$c = $c * 100;
		}
		else
		{
			$c = 100;
		}

		if($project_investment_fixturing == 0)
		{
			$s = "";
		}
		elseif($project_investment_fixturing > 0)
		{
			$s = ($typetotals_real[2]+$typetotals_real[6]+$typetotals_real[10])/$project_investment_fixturing;
			$s = $s * 100;
		}
		else
		{
			$s = 100;
		}

		if($project_investment_architectural == 0)
		{
			$a = "";
		}
		elseif($project_investment_architectural > 0)
		{
			$a = $typetotals_real[9]/$project_investment_architectural;
			$a = $a * 100;
		}
		else
		{
			$a = 100;
		}

		if($project_investment_equipment == 0)
		{
			$e = "";
		}
		elseif($project_investment_equipment > 0)
		{
			$e = $typetotals_real[11]/$project_investment_equipment;
			$e = $e * 100;
		}
		else
		{
			$e = 100;
		}

		if($project_investment_other == 0)
		{
			$o = "";
		}
		elseif($project_investment_other > 0)
		{
			$o = $typetotals_real[8]/$project_investment_other;
			$o = $o * 100;
		}
		else
		{
			$o = 100;
		}

		if($project_investment_total == 0)
		{
			$t = "";
		}
		elseif($project_investment_total > 0)
		{
			$t = $project_cost_real/$project_investment_total;
			$t = $t * 100;
		}
		else
		{
			$t = 100;
		}

		if($c > 105.00)
		{
			$f_format = $f_budget105;
			
		}
		elseif($c > 100.00)
		{
			$f_format = $f_budget100;   
		}
		else
		{
			$f_format = $f_budgetok;    
		}
		
		$sheet->write($row_index, $cell_index+30, "");
		if($c != "")
		{
			$sheet->write($row_index, $cell_index+31, number_format($c, 2, ".", "") . "%", $f_format);
		}
		else
		{
			$sheet->write($row_index, $cell_index+31, "", $f_normal);
		}

		if($s > 105.00)
		{
			$f_format = $f_budget105;
			
		}
		elseif($s > 100.00)
		{
			$f_format = $f_budget100;   
		}
		else
		{
			$f_format = $f_budgetok;    
		}

		if($s != "")
		{
			$sheet->write($row_index, $cell_index+32, number_format($s, 2, ".", "") . "%", $f_format);
		}
		else
		{
			$sheet->write($row_index, $cell_index+32, "", $f_normal);
		}

		if($a > 105.00)
		{
			$f_format = $f_budget105;
			
		}
		elseif($a > 100.00)
		{
			$f_format = $f_budget100;   
		}
		else
		{
			$f_format = $f_budgetok;    
		}

		if($a != "")
		{
			$sheet->write($row_index, $cell_index+33, number_format($a, 2, ".", "") . "%", $f_format);
		}
		else
		{
			$sheet->write($row_index, $cell_index+33, "", $f_normal);
		}


		if($e > 105.00)
		{
			$f_format = $f_budget105;
			
		}
		elseif($e > 100.00)
		{
			$f_format = $f_budget100;   
		}
		else
		{
			$f_format = $f_budgetok;    
		}

		if($e != "")
		{
			$sheet->write($row_index, $cell_index+34, number_format($e, 2, ".", "") . "%", $f_format);
		}
		else
		{
			$sheet->write($row_index, $cell_index+34, "", $f_normal);
		}


		if($o > 105.00)
		{
			$f_format = $f_budget105;
			
		}
		elseif($o > 100.00)
		{
			$f_format = $f_budget100;   
		}
		else
		{
			$f_format = $f_budgetok;    
		}

		if($o != "")
		{
			$sheet->write($row_index, $cell_index+35, number_format($o, 2, ".", "") . "%", $f_format);
		}
		else
		{
			$sheet->write($row_index, $cell_index+35, "", $f_normal);
		}
		
		
		if($t > 105.00)
		{
			$f_format = $f_budget105;
			
		}
		elseif($t > 100.00)
		{
			$f_format = $f_budget100;   
		}
		else
		{
			$f_format = $f_budgetok;    
		}

		if($t != "")
		{
			$sheet->write($row_index, $cell_index+36, number_format($t, 2, ".", "") . "%", $f_format);
		}
		else
		{
			$sheet->write($row_index, $cell_index+36, "", $f_normal);
		}


		$row_index++;
	}
}



//write list totals
for($i=0;$i<=13;$i++)
{
    $sheet->write($row_index, $i, "", $f_normal);
}

for($i=10;$i<=15;$i++)
{
    $sheet->write($row_index, $cell_index + $i, number_format($list_totals[$i-9], 2, ".", ""), $f_number_bold);
}

for($i=17;$i<=22;$i++)
{
    $sheet->write($row_index, $cell_index + $i, number_format($list_totals[$i-10], 2, ".", ""), $f_number_bold);
}

for($i=24;$i<=29;$i++)
{
    $sheet->write($row_index, $cell_index + $i, number_format($list_totals[$i-17], 2, ".", ""), $f_number_bold);
}



//percentage
if($list_totals[7] == 0)
{
    $c = "";
}
elseif($list_totals[1] > 0)
{
    $c = $list_totals[7]/$list_totals[1];
    $c = $c * 100;
}
else
{
    $c = 100;
}

if($list_totals[8] == 0)
{
    $s = "";
}
elseif($list_totals[2] > 0)
{
    $s = $list_totals[8]/$list_totals[2];
    $s = $s * 100;
}
else
{
    $s = 100;
}

if($list_totals[9] == 0)
{
    $a = "";
}
elseif($list_totals[3] > 0)
{
    $a = $list_totals[9]/$list_totals[3];
    $a = $a * 100;
}
else
{
    $a = 100;
}

if($list_totals[10] == 0)
{
    $e = "";
}
elseif($list_totals[4] > 0)
{
    $e = $list_totals[10]/$list_totals[4];
    $e = $e * 100;
}
else
{
    $e = 100;
}


if($list_totals[11] == 0)
{
    $o = "";
}
elseif($list_totals[5] > 0)
{
    $o = $list_totals[11]/$list_totals[5];
    $o = $o * 100;
}
else
{
    $o = 100;
}

if($list_totals[12] == 0)
{
    $t = "";
}
elseif($list_totals[6] > 0)
{
    $t = $list_totals[12]/$list_totals[6];
    $t = $t * 100;
}
else
{
    $t = 100;
}


if($c > 105)
{
    $f_format = $f_budget105;
}
elseif($c > 100)
{
    $f_format = $f_budget100;   
}
else
{
    $f_format = $f_budgetok;    
}

if(is_numeric($c))
{
	$sheet->write($row_index, $cell_index+31, number_format($c, 2, ".", "") . "%", $f_format);
}
else
{
	$sheet->write($row_index, $cell_index+31,"", $f_format);
}

if($s > 105)
{
    $f_format = $f_budget105;
}
elseif($s > 100)
{
    $f_format = $f_budget100;   
}
else
{
    $f_format = $f_budgetok;    
}

$sheet->write($row_index, $cell_index+32, number_format($s, 2, ".", "") . "%", $f_format);

if($a > 105)
{
    $f_format = $f_budget105;
}
elseif($a > 100)
{
    $f_format = $f_budget100;   
}
else
{
    $f_format = $f_budgetok;    
}

if($a != "")
{
	$sheet->write($row_index, $cell_index+33, number_format($a, 2, ".", "") . "%", $f_format);
}
else
{
	$sheet->write($row_index, $cell_index+33, "", $f_format);	
}


if($e > 105)
{
    $f_format = $f_budget105;
}
elseif($e > 100)
{
    $f_format = $f_budget100;   
}
else
{
    $f_format = $f_budgetok;    
}

if($e != "")
{
	$sheet->write($row_index, $cell_index+34, number_format($e, 2, ".", "") . "%", $f_format);
}
else
{
		$sheet->write($row_index, $cell_index+34, "", $f_format);
}


if($o > 105)
{
    $f_format = $f_budget105;
}
elseif($o > 100)
{
    $f_format = $f_budget100;   
}
else
{
    $f_format = $f_budgetok;    
}
if(is_numeric($o))
{
	$sheet->write($row_index, $cell_index+35, number_format($o, 2, ".", "") . "%", $f_format);
}
else
{
	$sheet->write($row_index, $cell_index+35, "", $f_format);
}


if($t > 105)
{
    $f_format = $f_budget105;
}
elseif($t > 100)
{
    $f_format = $f_budget100;   
}
else
{
    $f_format = $f_budgetok;    
}

$sheet->write($row_index, $cell_index+36, number_format($t, 2, ".", "") . "%", $f_format);

$xls->close(); 

?>