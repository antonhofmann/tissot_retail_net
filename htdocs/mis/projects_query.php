<?php
/********************************************************************

    project_query.php

    Project Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2011-07-30
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2011-07-30
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";


check_access("can_perform_queries");

require_once "include/query_get_functions.php";

$qt = 1;
if(param("qt"))
{
	$qt = param("qt");
}

if(param("query_id"))
{
	param("id", param("query_id"));
	$query_id = param("query_id");
}
if(id())
{
	param("query_id",id());
	$query_id = id();
}

$user = get_user(user_id());

//update filter in case elements are missing
if(isset($query_id))
{
	$filter = get_query_filter($query_id);

	$new_filter["ptst"] =  $filter["ptst"];
	$new_filter["fst"] =  $filter["fst"];
	$new_filter["tst"] =  $filter["tst"];

	$new_filter["fst2"] =  $filter["fst2"];
	$new_filter["tst2"] =  $filter["tst2"];

	$new_filter["fdy"] =  $filter["fdy"];
	$new_filter["fdm"] =  $filter["fdm"];
	$new_filter["tdy"] =  $filter["tdy"];
	$new_filter["tdm"] =  $filter["tdm"];

	$new_filter["fdy1"] =  $filter["fdy1"];
	$new_filter["fdm1"] =  $filter["fdm1"];
	$new_filter["tdy1"] =  $filter["tdy1"];
	$new_filter["tdm1"] =  $filter["tdm1"];

	$new_filter["fdy2"] =  $filter["fdy2"];
	$new_filter["fdm2"] =  $filter["fdm2"];
	$new_filter["tdy2"] =  $filter["tdy2"];
	$new_filter["tdm2"] =  $filter["tdm2"];

	
	$new_filter["clt"] =  $filter["clt"];
	$new_filter["pl"] =  $filter["pl"];
	$new_filter["plsc"] =  $filter["plsc"];
	$new_filter["pk"] =  $filter["pk"];
	$new_filter["pt"] =  $filter["pt"];
	$new_filter["ptsc"] =  $filter["ptsc"];
	$new_filter["pct"] =  $filter["pct"];
	$new_filter["gr"] =  $filter["gr"];
	$new_filter["re"] =  $filter["re"];
	$new_filter["co"] =  $filter["co"];
	$new_filter["rtc"] =  $filter["rtc"];

	if(array_key_exists("lrtc", $filter))
	{
		$new_filter["lrtc"] =  $filter["lrtc"];
	}
	else
	{
		$new_filter["lrtc"] =  "";
	}
	$new_filter["rto"] =  $filter["rto"];

	if(array_key_exists("dcontr", $filter))
	{
		$new_filter["dcontr"] =  $filter["dcontr"];
	}
	else
	{
		$new_filter["dcontr"] =  "";
	}


	$new_filter["detail"] =  $filter["detail"];

	if(array_key_exists("idvi", $filter))
	{
		$new_filter["idvi"] =  $filter["idvi"];
	}
	else
	{
		$new_filter["idvi"] =  "";
	}

	if(array_key_exists("arch", $filter))
	{
		$new_filter["arch"] =  $filter["arch"];
	}
	else
	{
		$new_filter["arch"] =  "";
	}

	if(array_key_exists("subfy", $filter))
	{
		$new_filter["subfy"] =  $filter["subfy"];
	}
	else
	{
		$new_filter["subfy"] =  "";
	}

	if(array_key_exists("subfm", $filter))
	{
		$new_filter["subfm"] =  $filter["subfm"];
	}
	else
	{
		$new_filter["subfm"] =  "";
	}

	if(array_key_exists("subfd", $filter))
	{
		$new_filter["subfd"] =  $filter["subfd"];
	}
	else
	{
		$new_filter["subfd"] =  "";
	}

	if(array_key_exists("subty", $filter))
	{
		$new_filter["subty"] =  $filter["subty"];
	}
	else
	{
		$new_filter["subty"] =  "";
	}

	if(array_key_exists("subtm", $filter))
	{
		$new_filter["subtm"] =  $filter["subtm"];
	}
	else
	{
		$new_filter["subtm"] =  "";
	}

	if(array_key_exists("subtd", $filter))
	{
		$new_filter["subtd"] =  $filter["subtd"];
	}
	else
	{
		$new_filter["subtd"] =  "";
	}

	if(array_key_exists("cmsst", $filter))
	{
		$new_filter["cmsst"] =  $filter["cmsst"];
	}
	else
	{
		$new_filter["cmsst"] =  "";
	}

	if(array_key_exists("areas", $filter))
	{
		$new_filter["areas"] =  $filter["areas"];
	}
	else
	{
		$new_filter["areas"] =  "";
	}

	if(array_key_exists("items", $filter))
	{
		$new_filter["items"] =  $filter["items"];
	}
	else
	{
		$new_filter["items"] =  "";
	}


	if(array_key_exists("owners", $filter))
	{
		$new_filter["owners"] =  $filter["owners"];
	}
	else
	{
		$new_filter["owners"] =  "";
	}

	if(array_key_exists("supp", $filter))
	{
		$new_filter["supp"] =  $filter["supp"];
	}
	else
	{
		$new_filter["supp"] =  "";
	}

	if(array_key_exists("dos", $filter))
	{
		$new_filter["dos"] =  $filter["dos"];
	}
	else
	{
		$new_filter["dos"] =  "";
	}

	if(array_key_exists("dba", $filter))
	{
		$new_filter["dba"] =  $filter["dba"];
	}
	else
	{
		$new_filter["dba"] =  "";
	}


	if(array_key_exists("icat", $filter))
	{
		$new_filter["icat"] =  $filter["icat"];
	}
	else
	{
		$new_filter["icat"] =  "";
	}


	if(array_key_exists("exclude_e_stores", $filter))
	{
		$new_filter["exclude_e_stores"] =  $filter["exclude_e_stores"];
	}
	else
	{
		$new_filter["exclude_e_stores"] =  "";
	}

		

	$sql = "update mis_queries " . 
			   "set mis_query_filter = " . dbquote(serialize($new_filter)) . 
			   " where mis_query_id = " . param("query_id");

	$result = mysql_query($sql) or dberror($sql);
}

//prepare data


$form = new Form("mis_queries", "Project Query");

$form->add_hidden("query_id", param("query_id"));
$form->add_hidden("qt", $qt);

if($qt == 6 or $qt == 31) {
	$form->add_hidden("mis_query_context", "masterplan");
}
elseif($qt == 7) {
	$form->add_hidden("mis_query_context", "cmsoverview");
}
elseif($qt == 8) {
	$form->add_hidden("mis_query_context", "klapproved");
}
elseif($qt == 9) {
	$form->add_hidden("mis_query_context", "projectsheets");
}
elseif($qt == '10co') {
	$form->add_hidden("mis_query_context", "productionplanning_co");
}
elseif($qt == 10) {
	$form->add_hidden("mis_query_context", "productionplanning");
}
elseif($qt == 11) {
	$form->add_hidden("mis_query_context", "projectmilestones");
}
elseif($qt == 12) {
	$form->add_hidden("mis_query_context", "cmsatatus");
}
elseif($qt == 13) {
	$form->add_hidden("mis_query_context", "transportationcost");
}
elseif($qt == 14) {
	$form->add_hidden("mis_query_context", "posequipment");
}
elseif($qt == 15) {
	$form->add_hidden("mis_query_context", "deliverybyitem");
}
elseif($qt == 16) {
	$form->add_hidden("mis_query_context", "cmscompleted");
}
elseif($qt == 22) {
	$form->add_hidden("mis_query_context", "orderstates");
}
elseif($qt == 30) {
	$form->add_hidden("mis_query_context", "poslocations");
}
elseif($qt == 35) {
	$form->add_hidden("mis_query_context", "masterplano");
}
elseif($qt == 40) {
	$form->add_hidden("mis_query_context", "shipments");
}
elseif($qt == 41) {
	$form->add_hidden("mis_query_context", "shipmentsf");
}




$form->add_edit("mis_query_name", "Name*", NOTNULL);

$form->add_checkbox("mis_print_filter", "Print Query Filter Information in Excel Sheet", "", "", "Filter");

$form->add_hidden("mis_query_owner", user_id());


//prepare data for access
$access_roles = array();
$sql_access_roles = "select role_id from roles where role_include_in_queryselection_mis = 1";
$res = mysql_query($sql_access_roles) or dberror($sql_access_roles);
while ($row = mysql_fetch_assoc($res))
{
	$access_roles[] = $row["role_id"];
}

if(count($access_roles) > 0)
{
	$sql_persons_hq = "select distinct user_id, concat(user_name, ' ' , user_firstname, '(', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " . 
		   "where user_id = 2 " . 
		   "or (address_id = 13 and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and (user_role_role IN (" . implode(",", $access_roles). ") or user_address = " . $user["address"] . "))" .
		   " or (address_type = 7 and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and (user_role_role IN (" . implode(",", $access_roles). ") or user_address = " . $user["address"] . "))" .
		   "order by username";

	$sql_persons_sub = "select distinct user_id, " . 
		   "concat(country_name, ' - ', user_name, ' ' , user_firstname, '(', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " .
		   " left join countries on country_id = address_country " . 
		   "where address_id <> 13 and address_client_type in (2, 3) and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and (user_role_role IN (" . implode(",", $access_roles). ") or user_address = " . $user["address"] . ") " . 
		   "order by country_name, username";

	$sql_persons_ag = "select distinct user_id, " . 
		   "concat(country_name, ' - ', user_name, ' ' , user_firstname, '(', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " .
		    " left join countries on country_id = address_country " .  
		   "where address_id <> 13 and address_client_type = 1 and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and (user_role_role IN (" . implode(",", $access_roles). ") or user_address = " . $user["address"] . ") " . 
		   "order by country_name, username";
}
else
{
	$sql_persons_hq = "select distinct user_id, concat(user_name, ' ' , user_firstname, ' (', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " .
		   "where user_id = 2 or (address_id = 13  and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and user_address = " . $user["address"] . ") " . 
		   "order by username";

	$sql_persons_sub = "select distinct user_id, " . 
		   "concat(country_name, ' - ', user_name, ' ' , user_firstname, ' (', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " .
		   " left join countries on country_id = address_country " . 
		   "where address_id <> 13 and address_client_type in (2, 3) and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and user_address = " . $user["address"] . " " . 
		   "order by country_name, username";

	$sql_persons_ag = "select distinct user_id, " . 
		   "concat(country_name, ' - ', user_name, ' ' , user_firstname, ' (', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " .
		   " left join countries on country_id = address_country " . 
		   "where address_id <> 13 and address_client_type = 1 and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and user_address = " . $user["address"] . " " . 
		   "order by country_name, username";
}

   

$form->add_section("Access Rights");
$form->add_comment("The following persons can have access to my queries");
//$form->add_checklist("Persons", "Persons", "mis_querypermissions", $sql_persons );
$form->add_checklist("Persons0", "Persons HQ", "mis_querypermissions", $sql_persons_hq, 0, "", true );
$form->add_checklist("Persons1", "Subs and Affiliates", "mis_querypermissions", $sql_persons_sub, 0, "", true );
$form->add_checklist("Persons2", "Agents", "mis_querypermissions", $sql_persons_ag, 0, "", true );

$form->add_button("save", "Save Query");
if(param("query_id") > 0)
{
	$form->add_button("delete", "Delete Query");
}
$form->add_button("back", "Back to the List of Queries");

$form->populate();
$form->process();

if($form->button("save"))
{
	if($form->validate())
	{
		$form->save();

		
		if(!param("query_id")) // new record
		{
			
			//create filter
			$filter = array();
			
			$filter["ptst"] =  "";
			
			
			$filter["fst"] =  "";
			$filter["tst"] =  "";

			$filter["fdy"] =  "";
			$filter["fdm"] =  "";
			$filter["tdy"] =  "";
			$filter["tdm"] =  "";
			$filter["fdy1"] =  "";
			$filter["fdm1"] =  "";
			$filter["tdy1"] =  "";
			$filter["tdm1"] =  "";
			$filter["fdy2"] =  "";
			$filter["fdm2"] =  "";
			$filter["tdy2"] =  "";
			$filter["tdm2"] =  "";

		
			$filter["clt"] =  "";
			$filter["pl"] =  "";
			$filter["plsc"] =  "";
			$filter["pk"] =  "";
			$filter["pt"] =  "";
			$filter["ptsc"] =  "";
			$filter["pct"] =  "";
			$filter["gr"] =  "";
			$filter["re"] =  "";
			$filter["co"] =  "";
			$filter["rtc"] =  "";
			$filter["lrtc"] =  "";
			$filter["rto"] =  "";
			$filter["dcontr"] =  "";
			$filter["detail"] =  "";
			$filter["idvi"] =  "";
			$filter["arch"] =  "";
			$filter["subfy"] =  "";
			$filter["subfm"] =  "";
			$filter["subfd"] =  "";
			$filter["subty"] =  "";
			$filter["subtm"] =  "";
			$filter["subtd"] =  "";
			$filter["areas"] =  "";
			$filter["items"] =  "";
			$filter["owners"] =  "";
			$filter["supp"] =  "";
			$filter["dos"] =  "";
			$filter["exclude_e_stores"] =  "";

			
			
			
			$sql_u = "update mis_queries " . 
				     "set mis_query_filter = " . dbquote(serialize($filter)) . 
				     " where mis_query_id = " . id();

			$result = mysql_query($sql_u) or dberror($sql_u);

			
			$result = mysql_query($sql_u) or dberror($sql_u);
		}


		redirect("projects_query.php?query_id=" .  id() . "&qt=" . $qt);
	}
}
elseif($form->button("delete"))
{
	$sql = "delete from mis_queries where mis_query_id = " . id();
	$result = mysql_query($sql) or dberror($sql);

	$sql = "delete from mis_querypermissions where mis_querypermission_query = " . id();
	$result = mysql_query($sql) or dberror($sql);

	redirect("projects_queries.php?qt=" . $qt);

}
elseif($form->button("back"))
{
	redirect("projects_queries.php?qt=" . $qt);

}


$page = new Page("queries");

require "include/mis_page_actions.php";

$page->header();

if($qt == 6 or $qt == 31) {
	$page->title(id() ? "Masterplan: Edit Project Query - Name" : "Masterplan: Add Project Query");
}
elseif($qt == 7) {
	$page->title(id() ? "CMS Overview: Edit Project Query - Name" : "CMS Overview: Add Project Query");
}
elseif($qt == 8) {
	$page->title(id() ? "KL approved versus real cost: Edit Project Query - Name" : "KL approved versus real cost: Add Project Query");
}
elseif($qt == 9) {
	$page->title(id() ? "Project Sheets: Edit Project Query - Name" : "Project Sheets: Add Project Query");
}
elseif($qt == '10co') {
	$page->title(id() ? "Production Order Planning: Edit Catalogue Order Query - Name" : "Production Order Planning Catalogue Orders: Add Project Query");
}
elseif($qt == 10) {
	$page->title(id() ? "Production Order Planning: Edit Project Query - Name" : "Production Order Planning Projects: Add Project Query");
}
elseif($qt == 11) {
	$page->title(id() ? "Project Milestones: Edit Project Query - Name" : "Project Milestones: Add Project Query");
}
elseif($qt == 12) {
	$page->title(id() ? "CMS Statusreport: Edit Project Query - Name" : "CMS Statusreport: Add Project Query");
}
elseif($qt == 13) {
	$page->title(id() ? "Transportation Cost: Edit Project Query - Name" : "Transportation Cost: Add Project Query");
}
elseif($qt == 14) {
	$page->title(id() ? "Equipment of POS Locations: Edit Project Query - Name" : "Equipment of POS Locations: Add Project Query");
}
elseif($qt == 15) {
	$page->title(id() ? "Item Usage: Edit Query - Name" : "Item Usage: Add Query");
}
elseif($qt == 16) {
	$page->title(id() ? "CMS Completed Projects: Edit Project Query - Name" : "CMS Completed Projects: Add Project Query");
}
elseif($qt == 22) {
	$page->title(id() ? "Order States in Catalogue Orders and Projects: Edit Query - Name" : "Order States in Catalogue Orders and Projects: Add Query");
}
elseif($qt == 30) {
	$page->title(id() ? "Status Repor POS Locations: Edit Query - Name" : "Status Repor POS Locations: Add Query");
}
elseif($qt == 35) {
	$page->title(id() ? "Master Plan: Edit Order Query - Name" : "Masterplan: Add Order Query");
}
elseif($qt == 40) {
	$page->title(id() ? "Purchase Volume by Supplier: Edit Query - Name" : "Purchase Volume by Supplier: Add Query");
}
elseif($qt == 41) {
	$page->title(id() ? "Shipments by Forwarder: Edit Query - Name" : "Shipments by Forwarder: Add Query");
}

if(id() > 0 or param("query_id") > 0)
{
	require_once("include/project_query_tabs.php");
}

$form->render();
$page->footer();

?>