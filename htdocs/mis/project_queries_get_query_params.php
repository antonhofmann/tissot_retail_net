<?php
/********************************************************************

    project_queries_get_query_params.php

    Get Query Params

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2014-09-14
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2014-09-14
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/
$start_time = time();
//check if user has access to the query

$captions = array();
$sql = "";
$outputfields = "";
$order_clause = "";
$total_records = 0;
$total_records_per_group = 0;
$data_blocks = array();
$data_blocks_captions = array();

//coloring schema for overdue days
$cms_overdue_day_limits = array();
$cms_overdue_day_limits['red'] = 90;
$cms_overdue_day_limits['orange'] = 60;
$cms_overdue_day_limits['yellow'] = 30;
$cms_overdue_day_limits['green'] = 0;

//coloring schema for cost difference in percent
$cost_difference_in_percent = array();
$cost_difference_in_percent['red'] = 1.05;
$cost_difference_in_percent['orange'] = 1;
$cost_difference_in_percent['yellow'] = 0;
$cost_difference_in_percent['green'] = 0;

$cost_difference_in_percent2 = array();
$cost_difference_in_percent2['red'] = 0.05;
$cost_difference_in_percent2['orange'] = 0;
$cost_difference_in_percent2['yellow'] = 0;
$cost_difference_in_percent2['green'] = 0;


//captions for subgroup_category_order_state
$subgroup_category_order_state_captions = array();
$subgroup_category_order_state_captions[] = "Supplier";
$subgroup_category_order_state_captions[] = "Category";
$subgroup_category_order_state_captions[] = "Order Date";
$subgroup_category_order_state_captions[] = "Ready for Pickup";
$subgroup_category_order_state_captions[] = "Pickup Date";

//captions for subgroup_category_delivery_state
$subgroup_category_delivery_state_captions = array();
$subgroup_category_delivery_state_captions[] = "Category";
$subgroup_category_delivery_state_captions[] = "Shipment Code";
$subgroup_category_delivery_state_captions[] = "Forwarder";
$subgroup_category_delivery_state_captions[] = "Expected Arrival Date";
$subgroup_category_delivery_state_captions[] = "Arrival Date";


//get costmonitoring furniture groups
$costmonitoring_groups = array();
$costmonitoring_group_filter = "";
/*
$sql = "select costmonitoringgroup_id from costmonitoringgroups " . 
       "where costmonitoringgroup_include_in_masterplan = 1 ";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$costmonitoring_groups[] = $row["costmonitoringgroup_id"];
}


if(count($costmonitoring_groups > 0)) {

	$costmonitoring_group_filter = " or order_items_costmonitoring_group IN (" . implode(",", $costmonitoring_groups) .") ";
	
}
*/

$sql = "select * from projectqueries where projectquery_id =" . $query_id;
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$print_query_filter = $row["projectquery_print_filter"];
	$projectquery_field_order = $row["projectquery_field_order"];
	
	$selected_field_order = decode_field_array($projectquery_field_order);
	$projectquery_order = decode_field_array($row["projectquery_order"]);

	$projectquery_area_perception_filter = "";
	$projectquery_area_perception = explode  ("@@" , $row["projectquery_area_perception"]);
	if(array_key_exists(1,$projectquery_area_perception))
	{
		$projectquery_area_perception_filter = $projectquery_area_perception[1];
	}
	
	$db_info_pos = query_posaddress_fields();
	$db_info_client = query_clientaddress_fields();
	$db_info_franchisee = query_franchiseeaddress_fields();
	$db_info_distributionchannels = query_distribution_channel_fields();
	$db_info_projects = query_projects_fields();
	$db_info_cer = query_cer_fields();
	$db_info_logistics = query_logistics_fields();
	$db_info_rental = query_rental_fields();
	$db_info_project_sheets = query_projects_sheet_fields();
	$db_info_cms = query_cms_fields();
	$db_info_staff = query_staff_fields();
	$db_info_budget = query_budget_fields();
	$db_info_investments = query_investment_fields();
	$db_info_approved_investments = query_approved_investments_fields();
	$db_info_real_costs = query_real_costs_fields();
	$db_info_cost_comparison = query_cost_comparison_fields();
	$db_info_milestones = query_milestone_fields();
	$db_info_orderstates = query_orderstate_fields();
	$db_info_items = query_item_fields();


	$db_info["fields"] = array_merge($db_info_pos["fields"], $db_info_client["fields"], $db_info_franchisee["fields"], $db_info_distributionchannels["fields"], $db_info_projects["fields"], $db_info_cer["fields"], $db_info_logistics["fields"], $db_info_rental["fields"], $db_info_project_sheets["fields"], $db_info_cms["fields"], $db_info_staff["fields"], $db_info_budget["fields"], $db_info_investments["fields"], $db_info_approved_investments["fields"], $db_info_real_costs["fields"], $db_info_cost_comparison["fields"], $db_info_milestones["fields"], $db_info_orderstates["fields"], $db_info_items["fields"]);

	$db_info["attributes"] = array_merge($db_info_pos["attributes"], $db_info_client["attributes"], $db_info_franchisee["attributes"], $db_info_distributionchannels["attributes"], $db_info_projects["attributes"], $db_info_cer["attributes"], $db_info_logistics["attributes"], $db_info_rental["attributes"] ,$db_info_project_sheets["attributes"],$db_info_cms["attributes"], $db_info_staff["attributes"], $db_info_budget["attributes"], $db_info_investments["attributes"], $db_info_approved_investments["attributes"], $db_info_real_costs["attributes"], $db_info_cost_comparison["attributes"], $db_info_milestones["attributes"], $db_info_orderstates["attributes"], $db_info_items["attributes"]);
	
	$db_info["datatypes"] = array_merge($db_info_pos["datatypes"], $db_info_client["datatypes"], $db_info_franchisee["datatypes"], $db_info_distributionchannels["datatypes"], $db_info_projects["datatypes"], $db_info_cer["datatypes"], $db_info_logistics["datatypes"], $db_info_project_sheets["datatypes"], $db_info_rental["datatypes"], $db_info_cms["datatypes"],$db_info_staff["datatypes"], $db_info_budget["datatypes"],$db_info_investments["datatypes"],$db_info_approved_investments["datatypes"],$db_info_real_costs["datatypes"], $db_info_cost_comparison["datatypes"], $db_info_milestones["datatypes"], $db_info_orderstates["datatypes"], $db_info_items["datatypes"]);
	
	$db_info["calculated_content"] = array_merge($db_info_pos["calculated_content"], $db_info_client["calculated_content"], $db_info_franchisee["calculated_content"], $db_info_distributionchannels["calculated_content"], $db_info_projects["calculated_content"], $db_info_cer["calculated_content"], $db_info_logistics["calculated_content"], $db_info_rental["calculated_content"], $db_info_project_sheets["calculated_content"], $db_info_cms["calculated_content"], $db_info_staff["calculated_content"], $db_info_budget["calculated_content"], $db_info_investments["calculated_content"], $db_info_approved_investments["calculated_content"], $db_info_real_costs["calculated_content"], $db_info_cost_comparison["calculated_content"], $db_info_milestones["calculated_content"], $db_info_orderstates["calculated_content"], $db_info_items["calculated_content"]);

	$db_info["calculated_content_key"] = array_merge($db_info_pos["calculated_content_key"], $db_info_client["calculated_content_key"], $db_info_franchisee["calculated_content_key"], $db_info_distributionchannels["calculated_content_key"], $db_info_projects["calculated_content_key"], $db_info_cer["calculated_content_key"], $db_info_logistics["calculated_content_key"], $db_info_rental["calculated_content_key"], $db_info_project_sheets["calculated_content_key"], $db_info_cms["calculated_content_key"], $db_info_staff["calculated_content_key"], $db_info_budget["calculated_content_key"], $db_info_investments["calculated_content_key"], $db_info_approved_investments["calculated_content_key"], $db_info_real_costs["calculated_content_key"], $db_info_cost_comparison["calculated_content_key"], $db_info_milestones["calculated_content_key"], $db_info_orderstates["calculated_content_key"], $db_info_items["calculated_content_key"]);

	$db_info["calculated_content_field"] = array_merge($db_info_pos["calculated_content_field"], $db_info_client["calculated_content_field"], $db_info_franchisee["calculated_content_field"], $db_info_distributionchannels["calculated_content_field"], $db_info_projects["calculated_content_field"], $db_info_cer["calculated_content_field"], $db_info_logistics["calculated_content_field"], $db_info_rental["calculated_content_field"], $db_info_project_sheets["calculated_content_field"], $db_info_cms["calculated_content_field"], $db_info_staff["calculated_content_field"], $db_info_budget["calculated_content_field"], $db_info_investments["calculated_content_field"], $db_info_approved_investments["calculated_content_field"], $db_info_real_costs["calculated_content_field"], $db_info_cost_comparison["calculated_content_field"], $db_info_milestones["calculated_content_field"], $db_info_orderstates["calculated_content_field"], $db_info_items["calculated_content_field"]);

	$db_info["calculated_content_sort_order"] = array_merge($db_info_pos["calculated_content_sort_order"], $db_info_client["calculated_content_sort_order"], $db_info_franchisee["calculated_content_sort_order"], $db_info_distributionchannels["calculated_content_sort_order"], $db_info_projects["calculated_content_sort_order"], $db_info_cer["calculated_content_sort_order"], $db_info_logistics["calculated_content_sort_order"], $db_info_rental["calculated_content_sort_order"], $db_info_project_sheets["calculated_content_sort_order"], $db_info_cms["calculated_content_sort_order"], $db_info_staff["calculated_content_sort_order"], $db_info_budget["calculated_content_sort_order"], $db_info_investments["calculated_content_sort_order"], $db_info_approved_investments["calculated_content_sort_order"], $db_info_real_costs["calculated_content_sort_order"], $db_info_cost_comparison["calculated_content_sort_order"], $db_info_milestones["calculated_content_sort_order"], $db_info_orderstates["calculated_content_sort_order"], $db_info_items["calculated_content_sort_order"]);

	$db_info["content_by_function"] = array_merge($db_info_pos["content_by_function"], $db_info_client["content_by_function"], $db_info_franchisee["content_by_function"], $db_info_distributionchannels["content_by_function"], $db_info_projects["content_by_function"], $db_info_cer["content_by_function"], $db_info_logistics["content_by_function"], $db_info_rental["content_by_function"], $db_info_project_sheets["content_by_function"], $db_info_cms["content_by_function"], $db_info_staff["content_by_function"], $db_info_budget["content_by_function"], $db_info_investments["content_by_function"], $db_info_approved_investments["content_by_function"], $db_info_real_costs["content_by_function"], $db_info_cost_comparison["content_by_function"], $db_info_milestones["content_by_function"], $db_info_orderstates["content_by_function"], $db_info_items["content_by_function"]);

	$db_info["content_by_function_params"] = array_merge($db_info_pos["content_by_function_params"], $db_info_client["content_by_function_params"], $db_info_franchisee["content_by_function_params"], $db_info_distributionchannels["content_by_function_params"], $db_info_projects["content_by_function_params"], $db_info_cer["content_by_function_params"], $db_info_logistics["content_by_function_params"], $db_info_rental["content_by_function_params"], $db_info_project_sheets["content_by_function_params"], $db_info_cms["content_by_function_params"], $db_info_staff["content_by_function_params"], $db_info_budget["content_by_function_params"], $db_info_investments["content_by_function_params"], $db_info_approved_investments["content_by_function_params"], $db_info_real_costs["content_by_function_params"], $db_info_cost_comparison["content_by_function_params"], $db_info_milestones["content_by_function_params"], $db_info_orderstates["content_by_function_params"], $db_info_items["content_by_function_params"]);

	
	$query_groups = $query_group_fields;
	$query_group01 = $row["projectquery_grouping01"];
	$query_group02 = $row["projectquery_grouping02"];
	$query_group03 = $row["projectquery_grouping03"];
	$show_number_of_projects = $row["projectquery_show_number_of_projects"];

	$general_sortorder = $row["projectquery_order_desc"];

	$query_filter = unserialize($row["projectquery_filter"]);

	
	//add filters that were added to the system after having gone online
	if(!array_key_exists("noln", $query_filter))
	{
		$query_filter["noln"] =  0; //no ln needed
		$query_filter["lnre"] =  0; //LN rejected
		$query_filter["nocer"] =  0; //no cer/af needed
		$query_filter["cerre"] =  0; //CER/AF rejected
	}


	$query_milestone_filter = unserialize($row["projectquery_milestone_filter"]);

		
	$functionfields = $query_function_fields;
	$sum_fields = unserialize($row["projectquery_sum_fields"]);
	$avg_fields = unserialize($row["projectquery_avg_fields"]);



	if($sum_fields == false)
	{
		$sum_fields = array();
	}

	if($avg_fields == false)
	{
		$avg_fields = array();
	}
	
	$sum_field_names = array();
	foreach($sum_fields as $key=>$fieldindex)
	{
		$sum_field_names[] = str_replace(".", "", $functionfields[$fieldindex]);
	}

	$avg_field_names = array();
	foreach($avg_fields as $key=>$fieldindex)
	{
		$avg_field_names[] = str_replace(".", "", $functionfields[$fieldindex]);
	}


	
	//integrity check
	//check if sumupfields or avg fields are present in the field list
	foreach($sum_fields as $key=>$index)
	{
		if(!in_array($functionfields[$index], $selected_field_order))
		{
			unset($sum_fields[$key]);
		}
	}
	foreach($avg_fields as $key=>$index)
	{
		if(!in_array($functionfields[$index], $selected_field_order))
		{
			unset($avg_fields[$key]);
		}
	}


	$use_zebra = $row["projectquery_zebra"];
	$zebra_color = array(255, 255, 255);
	if($use_zebra == 1 and $row["projectquery_zebra_color"])
	{
		$zebra_color = hex2rgb($row["projectquery_zebra_color"]);
	}



	$totalisation_fields = array();
	$totalisation_fields_format = array();
	$totalisation_fields_column_index = array();

	if($sum_fields)
	{
		foreach($sum_fields as $key=>$index)
		{

			/*
			if(in_array($functionfields[$index], $db_info["item_quantity_block"]))
			{
			}
			*/
			if(in_array($functionfields[$index], $db_info["calculated_content_field"]))
			{
				$new_field_name = str_replace(".", "", $functionfields[$index]);
				$totalisation_fields[$new_field_name] = "0";
				$totalisation_fields_column_index[$new_field_name] = "0";
			}
			elseif(array_key_exists($functionfields[$index], $db_info["content_by_function"]))
			{
				$new_field_name = str_replace(".", "", $functionfields[$index]);
				$totalisation_fields[$new_field_name] = "0";
				$totalisation_fields_column_index[$new_field_name] = "0";
			}
			elseif(in_array($functionfields[$index], $selected_field_order))
			{
				$new_field_name = str_replace(".", "", $db_info["attributes"][$functionfields[$index]]);
				$totalisation_fields[$new_field_name] = "0";
				$totalisation_fields_column_index[$new_field_name] = "0";
				
			}
			$totalisation_fields_format[$new_field_name] = $db_info["datatypes"][$functionfields[$index]];
			
		}
	}
	
	if($avg_fields)
	{
		foreach($avg_fields as $key=>$index)
		{
			if(is_array($db_info["item_quantity_block"]) and in_array($functionfields[$index], $db_info["item_quantity_block"]))
			{

			}
			elseif(in_array($functionfields[$index], $db_info["calculated_content_field"]))
			{
				$new_field_name = str_replace(".", "", $functionfields[$index]);
				$totalisation_fields[$new_field_name] = "0";
				$totalisation_fields_column_index[$new_field_name] = "0";
			}
			elseif(array_key_exists($functionfields[$index], $db_info["content_by_function"]))
			{
				$new_field_name = str_replace(".", "", $functionfields[$index]);
				$totalisation_fields[$new_field_name] = "0";
				$totalisation_fields_column_index[$new_field_name] = "0";
			}
			elseif(in_array($functionfields[$index], $selected_field_order))
			{
				$new_field_name = str_replace(".", "", $db_info["attributes"][$functionfields[$index]]);
				$totalisation_fields[$new_field_name] = "0";
				$totalisation_fields_column_index[$new_field_name] = "0";
			}
			$totalisation_fields_format[$new_field_name] = $db_info["datatypes"][$functionfields[$index]];
		}
	}
	
	//create captions and fields for the sql
	

	
	foreach($selected_field_order as $key=>$field)
	{
		
		if($db_info["attributes"][$field] == "item_quantity_block")
		{
			$data_blocks[] = "item_quantity_block";
		}
		elseif($db_info["attributes"][$field] == "calculated_content")
		{
			$captions[] = str_replace('---- ', "", $db_info["fields"][$field]);
			$outputfields .= $db_info["calculated_content_key"][$field] . ", ";

		}
		elseif($db_info["attributes"][$field] == "content_by_function")
		{
			$captions[] = str_replace('---- ', "", $db_info["fields"][$field]);
		}
		elseif($db_info["attributes"][$field] == "subgroup_category_order_state")
		{
			foreach($subgroup_category_order_state_captions as $key=>$value)
			{
				$captions[] = $value;
			}
		}
		elseif($db_info["attributes"][$field] == "subgroup_category_delivery_state")
		{
			foreach($subgroup_category_delivery_state_captions as $key=>$value)
			{
				$captions[] = $value;
			}
		}
		elseif($db_info["attributes"][$field] == "content_by_subquery")
		{
			$captions[] = str_replace('---- ', "", $db_info["fields"][$field]);

			$subquery = "(" . $db_info["calculated_content"][$field] . ") as " . $field;
			$outputfields .= $subquery . ", ";
		}
		else
		{
			$captions[] = str_replace('---- ', "", $db_info["fields"][$field]);
			//$outputfields .= $db_info["attributes"][$field] . " as " . str_replace(".", "", $db_info["attributes"][$field]) . ", ";

			if(strpos($db_info["attributes"][$field], "IF(") === 0)
			{
				$outputfields .= $db_info["attributes"][$field] . ", ";
			}
			else
			{
				$outputfields .= $db_info["attributes"][$field] . " as " . str_replace(".", "", $db_info["attributes"][$field]) . ", ";
			}

			//echo $db_info["attributes"][$field] . " " . str_replace(".", "", $db_info["attributes"][$field]) . "<br />";
		}
	}

	$outputfields = substr($outputfields, 0, strlen($outputfields)-2);
	
	
	//create filter
		
	$filter = " where order_type = 1"; //onla projects
	$filter_clienttypes = "";
	$filter_salesregions = "";
	$filter_regions = "";
	$filter_countries = "";
	$filter_cities = "";
	$filter_project_costtypes = "";
	$filter_project_kinds = "";
	$filter_project_type_subclasses = "";
	$filter_pos_types = "";
	$filter_possubclasses = "";
	$filter_product_lines = "";
	$filter_furniture_subclasses = "";
	$filter_distribution_channels = "";
	$filter_design_objectives = "";
	$filter_costmonitoring_groups = "";
	$filter_item_categories = "";
	$filter_items = "";
	$filter_areas = "";


	
	if($query_filter["cl"] > 0) // client address
	{
		$filter .= " and order_client_address = " . $query_filter["cl"] . " ";
	}

	if($query_filter["fr"] > 0) // franchisee address
	{
		$filter .= " and order_franchisee_address_id = " . $query_filter["fr"] . " ";
	}
	

	$order_item_filter = "";
	if($query_filter["supp"] > 0) // supplier address
	{
		$filter .= " and order_item_supplier_address = " . $query_filter["supp"] . " ";

		$order_item_filter .= " and order_item_supplier_address = " . $query_filter["supp"] . " ";
	}

	if($query_filter["forw"] > 0) // forwarder address
	{
		$filter .= " and order_item_forwarder_address = " . $query_filter["forw"] . " ";
		$order_item_filter .= " and order_item_forwarder_address = " . $query_filter["forw"] . " ";
	}


	if($query_filter["hs"] > 0) // handling state
	{
		if($query_filter["hs"] == 1) //ongoing projects without an opening date
		{
			 $filter.=  " and (project_actual_opening_date = '0000-00-00' or project_actual_opening_date is null) ";
			 $filter.=  " and order_actual_order_state_code < '890' ";
			 $filter.=  " and (project_shop_closingdate = '0000-00-00' or project_shop_closingdate is null) ";
		}
		elseif($query_filter["hs"] == 2) // ongoing projects with an opening date
		{
			$filter.=  " and (project_actual_opening_date <> '0000-00-00' and project_actual_opening_date is not null) ";
			$filter.=  " and order_actual_order_state_code < '890' ";
			$filter.=  " and (project_shop_closingdate = '0000-00-00' or project_shop_closingdate is null) ";
		}
		elseif($query_filter["hs"] == 3) // projects with a closing date
		{
			$filter.=  " and (project_shop_closingdate <> '0000-00-00' and project_shop_closingdate is not null) ";
			$filter.=  " and (order_actual_order_state_code <> '900') ";
		}
		elseif($query_filter["hs"] == 4) // ongoing projects with a project sheet
		{
			$filter .= " and project_is_on_project_sheet_list = 1";
			$filter .= " and order_actual_order_state_code < '890'";
		}
	}

	if($query_filter["fos"] > 0) // from global order status
	{
		$filter .= " and order_actual_order_state_code >= '" . $query_filter["fos"] . "' ";
	}

	if($query_filter["tos"] > 0) // to global order status
	{
		$filter .= " and order_actual_order_state_code <= '" . $query_filter["tos"] . "' ";
	}


	if($query_filter["fosd"] > 0) // from development status
	{
		$filter .= " and order_development_status >= '" . $query_filter["fosd"] . "' ";
	}

	if($query_filter["tosd"] > 0) // to development status
	{
		$filter .= " and order_development_status <= '" . $query_filter["tosd"] . "' ";
	}

	if($query_filter["fosl"] > 0) // from logitic status
	{
		$filter .= " and order_logistic_status >= '" . $query_filter["fosl"] . "' ";
	}

	if($query_filter["tosl"] > 0) // to logitic status
	{
		$filter .= " and order_logistic_status <= '" . $query_filter["tosl"] . "' ";
	}


	if($query_filter["sufrom"] != '') // submited from
	{
		$filter .= " and orders.order_date >= '" . $query_filter["sufrom"] . "' ";
	}

	if($query_filter["suto"] != '') // submitted to
	{
		$filter .= " and orders.order_date <= '" . $query_filter["suto"] . "' ";
	}

	if($query_filter["opfrom"] != '') // opened from
	{
		$filter .= " and projects.project_actual_opening_date >= '" . $query_filter["opfrom"] . "' ";
	}

	if($query_filter["opto"] != '') // opened to
	{
		$filter .= " and projects.project_actual_opening_date<= '" . $query_filter["opto"] . "' ";
	}
	

	if($query_filter["agrfrom"] != '') // agreed opening date from
	{
		$filter .= " and projects.project_real_opening_date >= '" . $query_filter["agrfrom"] . "' ";
	}

	if($query_filter["agrto"] != '') // agreed opening date to
	{
		$filter .= " and projects.project_real_opening_date<= '" . $query_filter["agrto"] . "' ";
	}


	if($query_filter["clfrom"] != '') // closed from
	{
		$filter .= " and projects.project_shop_closingdate >= '" . $query_filter["clfrom"] . "' ";
	}

	if($query_filter["clto"] != '') // closed to
	{
		$filter .= " and projects.project_shop_closingdate<= '" . $query_filter["clto"] . "' ";
	}

	if($query_filter["cafrom"] != '') // cancelled from
	{
		$filter .= " and left(actual_order_states.date_created, 10) >= '" . $query_filter["cafrom"] . "' ";
		$filter .= " and actual_order_state_state = 43 ";
	}

	if($query_filter["cato"] != '') // cancelled to
	{
		$filter .= " and left(actual_order_states.date_created, 10) <= '" . $query_filter["cato"] . "' ";
		$filter .= " and actual_order_state_state = 43 ";
	}

	

	if($query_filter["lopr"] > 0) // POS with locally produced projects
	{
		$filter .= " and project_is_local_production = 1 ";
	}
	
	
	if($query_filter["typb1"] > 0) // POS with Store Furniture
	{
		$filter .= " and project_furniture_type_store = 1 ";
	}

	if($query_filter["typb2"] > 0) // POS with SIS Furniture
	{
		$filter .= " and project_furniture_type_sis = 1 ";
	}

	if($query_filter["idvs"] > 0) // Visuals
	{
		$filter .= " and project_uses_icedunes_visuals = 1";
	}


	if($query_filter["noln"] > 0) // no LN needed
	{
		$filter .= " and (ln_no_ln_submission_needed = 1 or ln_no_ln_submission_needed is null) ";
	}

	if($query_filter["lnre"] > 0) // LN rejected
	{
		$filter .= " and ln_basicdata_rejected is not null ";
		$filter .= " and ln_basicdata_rejected <> '0000-00-00' ";
		$filter .= " and ln_basicdata_rejected >= ln_basicdata_resubmitted ";
	}

	if($query_filter["nocer"] > 0) // no CER/AF needed
	{
		$filter .= " and (cer_basicdata_no_cer_submission_needed = 1 or cer_basicdata_no_cer_submission_needed is null) ";
	}

	if($query_filter["cerre"] > 0) // CER/AF rejected
	{
		$filter .= " and cer_basicdata_rejected is not null ";
		$filter .= " and cer_basicdata_rejected <> '0000-00-00' ";
		$filter .= " and cer_basicdata_rejected >= cer_basicdata_resubmitted ";
	}
	
		
	if($query_filter["ct"]) //client types
	{
		$tmp = explode("-", $query_filter["ct"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_clienttypes .= $value . ",";
			}
		}
		$filter_clienttypes = "(" . substr($filter_clienttypes, 0, strlen($filter_clienttypes)-1) . ")";
		if($filter_regions != "()")
		{
			
			$filter .= " and clients.address_client_type in " . $filter_clienttypes . " ";
			
			
		}
	}
		
	if($query_filter["re"]) //Geographical region
	{
		$tmp = explode("-", $query_filter["re"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_salesregions .= $value . ",";
			}
		}
		$filter_salesregions = "(" . substr($filter_salesregions, 0, strlen($filter_salesregions)-1) . ")";
		if($filter_regions != "()")
		{
			
			$filter .= " and countries.country_salesregion in " . $filter_salesregions . " ";
			
			
		}
	}

	if($query_filter["gr"]) //Supplied region
	{
		$tmp = explode("-", $query_filter["gr"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_regions .= $value . ",";
			}
		}
		$filter_regions = "(" . substr($filter_regions, 0, strlen($filter_regions)-1) . ")";
		if($filter_regions != "()")
		{
			$filter .= " and countries.country_region in " . $filter_regions . " ";
		}
	}
	
	
	if($query_filter["co"]) // country
	{
		$tmp = explode("-", $query_filter["co"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_countries .= $value . ",";
			}
		}
		$filter_countries = "(" . substr($filter_countries, 0, strlen($filter_countries)-1) . ")";
		if($filter_countries != "()")
		{
			$filter .= " and order_shop_address_country in " . $filter_countries . " ";
		}
	}

	if($query_filter["ci"]) // city
	{
		$tmp = explode("-", $query_filter["ci"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_cities .= "'" . $value . "',";
			}
		}
		$filter_cities = "(" . substr($filter_cities, 0, strlen($filter_cities)-1) . ")";
		if($filter_cities != "()")
		{
			$filter .= " and (posaddresses.posaddress_place_id in " . $filter_cities . " or  posaddressespipeline.posaddress_place_id in " . $filter_cities . ") ";
		}
	}

	
	
	if($query_filter["pct"]) //cost type, legal type
	{
		$tmp = explode("-", $query_filter["pct"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_project_costtypes .= $value . ",";
			}
		}
		$filter_project_costtypes = "(" . substr($filter_project_costtypes, 0, strlen($filter_project_costtypes)-1) . ")";
		if($filter_project_costtypes != "()")
		{
			$filter .= " and project_cost_type in " . $filter_project_costtypes . " ";
		}
	}


	//project Types
	$posaddress_ids_with_project_kinds_indicated = array();
	if($query_filter["pk"]) // project Type
	{
		$tmp = explode("-", $query_filter["pk"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_project_kinds .= $value . ",";
			}
		}

				
		$filter_project_kinds = "(" . substr($filter_project_kinds, 0, strlen($filter_project_kinds)-1) . ")";
		if($filter_project_kinds != "()")
		{
			$filter .= " and project_projectkind in " . $filter_project_kinds;
		}
	}


	//project Types subclasses
	if($query_filter["ptsc"]) // project Type subclasses
	{
		$tmp = explode("-", $query_filter["ptsc"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_project_type_subclasses .= $value . ",";
			}
		}

				
		$filter_project_type_subclasses = "(" . substr($filter_project_type_subclasses, 0, strlen($filter_project_type_subclasses)-1) . ")";
		if($filter_project_type_subclasses != "()")
		{
			$filter .= " and projects.project_type_subclass_id in " . $filter_project_type_subclasses;
		}
	}

	if($query_filter["pt"]) // pos type
	{
		$tmp = explode("-", $query_filter["pt"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_pos_types .= $value . ",";
			}
		}
		$filter_pos_types = "(" . substr($filter_pos_types, 0, strlen($filter_pos_types)-1) . ")";
		if($filter_pos_types != "()")
		{
			$filter .= " and project_postype in " . $filter_pos_types . " ";
		}
	}

	if($query_filter["sc"]) // POS Type Subclass
	{
		$tmp = explode("-", $query_filter["sc"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_possubclasses .= $value . ",";
			}
		}
		$filter_possubclasses = "(" . substr($filter_possubclasses, 0, strlen($filter_possubclasses)-1) . ")";
		if($filter_possubclasses != "()")
		{
			$filter .= " and project_pos_subclass in " . $filter_possubclasses . " ";
		}
	}

	if($query_filter["pl"]) // product line, furniture type
	{
		$tmp = explode("-", $query_filter["pl"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_product_lines .= $value . ",";
			}
		}
		$filter_product_lines = "(" . substr($filter_product_lines, 0, strlen($filter_product_lines)-1) . ")";
		if($filter_product_lines != "()")
		{
			$filter .= " and project_product_line in " . $filter_product_lines . " ";
		}
	}


	if($query_filter["fscs"]) // furniture subclasses
	{
		$tmp = explode("-", $query_filter["fscs"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_furniture_subclasses .= $value . ",";
			}
		}
		
		$filter_furniture_subclasses = "(" . substr($filter_furniture_subclasses, 0, strlen($filter_furniture_subclasses)-1) . ")";
		if($filter_furniture_subclasses != "()")
		{
			$filter .= " and project_product_line_subclass in " . $filter_furniture_subclasses . " ";
		}
		
	}

	
	if($query_filter["ts"]) // project state
	{
		$filter_project_state = "";
		$tmp = explode("-", $query_filter["ts"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_project_state .= $value . ",";
			}
		}
		
		$filter_project_state = "(" . substr($filter_project_state, 0, strlen($filter_project_state)-1) . ")";
		if($filter_project_state != "()")
		{
			$filter .= " and project_state in " . $filter_project_state . " ";
		}
		
	}

	if($query_filter["rtc"]) //project Leader
	{
		$tmp = explode("-", $query_filter["rtc"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_rtcs .= $value . ",";
			}
		}
		$filter_rtcs = "(" . substr($filter_rtcs, 0, strlen($filter_rtcs)-1) . ")";
		
		if($filter_rtcs != "()")
		{
			$filter .= " and project_retail_coordinator in " . $filter_rtcs . " ";
		}
	}

	if($query_filter["lrtc"]) //local project Leader
	{
		$tmp = explode("-", $query_filter["lrtc"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_lrtcs .= $value . ",";
			}
		}
		$filter_lrtcs = "(" . substr($filter_lrtcs, 0, strlen($filter_lrtcs)-1) . ")";
		
		if($filter_lrtcs != "()")
		{
			$filter .= " and project_local_retail_coordinator in " . $filter_lrtcs . " ";
		}
	}

	if($query_filter["dsup"]) //design supervisor
	{
		$tmp = explode("-", $query_filter["dsup"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_dsups .= $value . ",";
			}
		}
		$filter_dsups = "(" . substr($filter_dsups, 0, strlen($filter_dsups)-1) . ")";
		
		if($filter_dsups != "()")
		{
			$filter .= " and project_design_supervisor in " . $filter_dsups . " ";
		}
	}


	if($query_filter["rto"]) //logistics coordinator
	{
		$tmp = explode("-", $query_filter["rto"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_rtos .= $value . ",";
			}
		}
		$filter_rtos = "(" . substr($filter_rtos, 0, strlen($filter_rtos)-1) . ")";
		
		if($filter_rtos != "()")
		{
			$filter .= " and order_retail_operator in " . $filter_rtos . " ";
		}
	}


	if($query_filter["dcontr"]) //Design Contractors
	{
		$tmp = explode("-", $query_filter["dcontr"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_dcontrs .= $value . ",";
			}
		}
		$filter_dcontrs = "(" . substr($filter_dcontrs, 0, strlen($filter_dcontrs)-1) . ")";
		
		if($filter_dcontrs != "()")
		{
			$filter .= " and project_design_contractor in " . $filter_dcontrs . " ";
		}
	}

	if($query_filter["dcs"]) //Distribution Channel
	{
		$tmp = explode("-", $query_filter["dcs"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_distribution_channels .= $value . ",";
			}
		}
		$filter_distribution_channels = "(" . substr($filter_distribution_channels, 0, strlen($filter_distribution_channels)-1) . ")";
		
		if($filter_distribution_channels != "()")
		{
			$filter .= " and project_distribution_channel in " . $filter_distribution_channels . " ";
		}
	}

	
	//design objectives
	$posaddress_ids_with_desigen_objectives_indicated = array();
	if($query_filter["dos"]) //Design Objectives
	{
		
		$tmp = explode("-", $query_filter["dos"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_design_objectives .= $value . ",";
			}
		}
		
		$filter_design_objectives = "(" . substr($filter_design_objectives, 0, strlen($filter_design_objectives)-1) . ")";
		if($filter_design_objectives != "()")
		{
			
			$sql_p = "select DISTINCT project_item_project " . 
				     "from project_items " . 
					 "where project_item_item IN " . $filter_design_objectives; 
			
			$res_p = mysql_query($sql_p) or dberror($sql_p);
			while ($row_p = mysql_fetch_assoc($res_p))
			{
				$project_ids_with_desigen_objectives_indicated[$row_p["project_item_project"]] = $row_p["project_item_project"];
			}
		}


		if(count($project_ids_with_desigen_objectives_indicated) > 0)
		{
			$filter .= " and project_id IN (" . implode(",", $project_ids_with_desigen_objectives_indicated) . ") ";
		}

	}


	//cost monitoring groups
	/*
	$project_with_costmonitoring_group_inicated = array();
	if($query_filter["cmgr"]) //Cost Monitoring Groups
	{
		
		$tmp = explode("-", $query_filter["cmgr"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_costmonitoring_groups .= $value . ",";
			}
		}
		
		$filter_costmonitoring_groups = "(" . substr($filter_costmonitoring_groups, 0, strlen($filter_costmonitoring_groups)-1) . ")";
		if($filter_costmonitoring_groups != "()")
		{
		
			$sql_p = "select DISTINCT order_item_order " . 
					 "from order_items " .
					 "where order_items_costmonitoring_group IN " . $filter_costmonitoring_groups; 
			
			$res_p = mysql_query($sql_p) or dberror($sql_p);
			while ($row_p = mysql_fetch_assoc($res_p))
			{
				$project_with_costmonitoring_group_inicated[$row_p["order_item_order"]] = $row_p["order_item_order"];
			}
		}

		if(count($project_with_costmonitoring_group_inicated) > 0)
		{
			$filter .= " and order_id IN (" . implode(",", $project_with_costmonitoring_group_inicated) . ") ";
		}

	}
	*/


	//item categories
	$project_with_item_category_inicated = array();
	if($query_filter["icat"]) //Cost Monitoring Groups
	{
		
		$tmp = explode("-", $query_filter["icat"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_item_categories .= $value . ",";
			}
		}
		
		$filter_item_categories = "(" . substr($filter_item_categories, 0, strlen($filter_item_categories)-1) . ")";
		if($filter_item_categories != "()")
		{
		
			$sql_p = "select DISTINCT order_item_order " .
					 "from order_items " .
				     " left join items on item_id = order_item_item " . 
					 "where item_category IN " . $filter_item_categories; 
			
			$res_p = mysql_query($sql_p) or dberror($sql_p);
			while ($row_p = mysql_fetch_assoc($res_p))
			{
				$project_with_item_category_inicated[$row_p["order_item_order"]] = $row_p["order_item_order"];
			}
		}

		if(count($project_with_item_category_inicated) > 0)
		{
			$filter .= " and order_id IN (" . implode(",", $project_with_item_category_inicated) . ") ";
		}

	}


	

	//Neighbourhood Area
	$order_ids_with_areas_indicated = array();
	$pos_ids_with_areas_indicated = array();
	$pospipeline_ids_with_areas_indicated = array();
	if($query_filter["ar"]) //POS Areas
	{
		$tmp = explode("-", $query_filter["ar"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_areas .= $value . ",";
			}
		}
		
		$filter_areas = "(" . substr($filter_areas, 0, strlen($filter_areas)-1) . ")";
		if($filter_areas != "()")
		{
			
			$sql_p = "select DISTINCT posarea_posaddress " . 
				     "from posareas " . 
					 "where posarea_area IN " . $filter_areas; 
			
			
			$res_p = mysql_query($sql_p) or dberror($sql_p);
			while ($row_p = mysql_fetch_assoc($res_p))
			{
				$pos_ids_with_areas_indicated[$row_p["posarea_posaddress"]] = $row_p["posarea_posaddress"];
			}
		}


		if(count($pos_ids_with_areas_indicated) > 0)
		{
			$tmp_filter = " where posorder_order > 0 " . 
				          " and posorder_posaddress IN (" . implode(",", $pos_ids_with_areas_indicated) . ") ";
			$sql_a = "select posorder_order " . 
				     " from posorders " . $tmp_filter;

			$res_a = mysql_query($sql_a) or dberror($sql_a);
			while ($row_a = mysql_fetch_assoc($res_a))
			{
				$order_ids_with_areas_indicated[$row_a["posorder_order"]] = $row_a["posorder_order"];
			}

			$tmp_filter = " where posorder_order > 0 " . 
				          " and posorder_posaddress IN (" . implode(",", $pos_ids_with_areas_indicated) . ") ";
			$sql_a = "select posorder_order " . 
				     " from posorderspipeline " . $tmp_filter;
				     
			$res_a = mysql_query($sql_a) or dberror($sql_a);
			while ($row_a = mysql_fetch_assoc($res_a))
			{
				$order_ids_with_areas_indicated[$row_a["posorder_order"]] = $row_a["posorder_order"];
			}
			
		}


		if($filter_areas != "()")
		{
			
			$sql_p = "select DISTINCT posarea_posaddress " . 
				     "from posareaspipeline " . 
					 "where posarea_area IN " . $filter_areas; 
			
			$res_p = mysql_query($sql_p) or dberror($sql_p);
			while ($row_p = mysql_fetch_assoc($res_p))
			{
				$pospipeline_ids_with_areas_indicated[$row_p["posarea_posaddress"]] = $row_p["posarea_posaddress"];
			}
		}


		if(count($pospipeline_ids_with_areas_indicated) > 0)
		{
			$tmp_filter = " where posorder_order > 0 " .
				          "and posorder_posaddress IN (" . implode(",", $pospipeline_ids_with_areas_indicated) . ") ";
			$sql_a = "select posorder_order " . 
				     " from posorderspipeline " . $tmp_filter;
				     
			$res_a = mysql_query($sql_a) or dberror($sql_a);
			while ($row_a = mysql_fetch_assoc($res_a))
			{
				$order_ids_with_areas_indicated[$row_a["posorder_order"]] = $row_a["posorder_order"];
			}
		}
		
		if(count($order_ids_with_areas_indicated) > 0)
		{
			$filter .= " and project_order IN (" . implode(",", $order_ids_with_areas_indicated) . ") ";
		}
	}

	if($query_filter["pshe"] > 0) // Is on project sheet list
	{
		$filter .= " and project_is_on_project_sheet_list = 1";
	}


	if($query_filter["cmsst"] > 0) // CMS States
	{
		
		
		//only open pos locations where CMS is not completed
		//$filter.=  " and project_projectkind IN (1,2,3, 6, 7, 8) ";
		//$filter.=  " and (project_cost_cms_completed = 0  or project_cost_cms_approved = 0) ";
		//$filter.=  " and project_actual_opening_date <> '0000-00-00' and project_actual_opening_date is not null ";

		if($query_filter["cmsst"] == 1)
		{
			$filter .=  " and (project_cost_cms_completion_date is null or project_cost_cms_completion_date = '0000-00-00') "; 
		}
		elseif($query_filter["cmsst"] == 2)
		{
			$filter .=  " and project_cost_cms_completed = 1 and (project_cost_cms_approved_date is null or project_cost_cms_approved_date = '0000-00-00') ";
		}
		elseif($query_filter["cmsst"] == 3)
		{
			$filter .=  " and ((project_cost_cms_completion_date is null or project_cost_cms_completion_date = '0000-00-00') or (project_cost_cms_approved_date is null or project_cost_cms_approved_date = '0000-00-00')) ";
		}
		elseif($query_filter["cmsst"] == 4)
		{
			$filter .=  " and ((project_cost_cms_completion_date is null or project_cost_cms_completion_date = '0000-00-00') and (project_cost_cms_approved_date is null or project_cost_cms_approved_date = '0000-00-00')) ";
		}
		elseif($query_filter["cmsst"] == 5)
		{
			$filter .=  " and project_cost_cms_completion_date is not null and project_cost_cms_completion_date <> '0000-00-00' ";
		}
		elseif($query_filter["cmsst"] == 6)
		{
			$filter .=  " and project_cost_cms_approved_date is not null and project_cost_cms_approved_date <> '0000-00-00' ";
		}
	}

	if($query_filter["flagship"] > 0) // only flag ship projects
	{
		$filter .= " and project_is_flagship = 1";
	}



	//evaluate milestone filter
	$included_project_ids = array();
	$logical_condition = array();
	$tmp = array();
	if(is_array($query_milestone_filter) and count($query_milestone_filter) > 0)
	{
		foreach($query_milestone_filter as $milestone_id=>$mfilter_array)
		{
			

			if($mfilter_array["mfrom" . $milestone_id] and $mfilter_array["mto" . $milestone_id])
			{
				
				$tmp = array();
				$sql_p = "select project_milestone_project " . 
						 " from project_milestones " . 
						 " where project_milestone_milestone = " . $milestone_id . 
						 " and project_milestone_date >= " . dbquote($mfilter_array["mfrom" . $milestone_id]) . 
						  " and project_milestone_date <= " . dbquote($mfilter_array["mto" . $milestone_id]);

				
				$res_p = mysql_query($sql_p) or dberror($sql_p);
				while ($row_p = mysql_fetch_assoc($res_p))
				{
					$tmp[] = $row_p["project_milestone_project"];
				}

				if(count($tmp) > 0)
				{
					$included_project_ids[$milestone_id] = $tmp;
					$logical_condition[$milestone_id] = $mfilter_array["logical" . $milestone_id];
				}

				
			}
			elseif($mfilter_array["mfrom" . $milestone_id])
			{
				$tmp = array();
				$sql_p = "select project_milestone_project " . 
						 " from project_milestones " . 
						 " where project_milestone_milestone = " . $milestone_id . 
						 " and project_milestone_date >= " . dbquote($mfilter_array["mfrom" . $milestone_id]);
				
				$res_p = mysql_query($sql_p) or dberror($sql_p);
				while ($row_p = mysql_fetch_assoc($res_p))
				{
					$tmp[] = $row_p["project_milestone_project"];
				}

				if(count($tmp) > 0)
				{
					$included_project_ids[$milestone_id] = $tmp;
					$logical_condition[$milestone_id] = $mfilter_array["logical" . $milestone_id];
				}

				
			}
			elseif($mfilter_array["mto" . $milestone_id])
			{
				$tmp = array();
				$sql_p = "select project_milestone_project " . 
						 " from project_milestones " . 
						 " where project_milestone_milestone = " . $milestone_id . 
						  " and project_milestone_date <= " . dbquote($mfilter_array["mto" . $milestone_id]);
				
				$res_p = mysql_query($sql_p) or dberror($sql_p);
				while ($row_p = mysql_fetch_assoc($res_p))
				{
					$tmp[] = $row_p["project_milestone_project"];
				}

				if(count($tmp) > 0)
				{
					$included_project_ids[$milestone_id] = $tmp;
					$logical_condition[$milestone_id] = $mfilter_array["logical" . $milestone_id];
				}

				
			}

			

			if(count($included_project_ids) == 0 and $mfilter_array["mhasdate" . $milestone_id])
			{
				$tmp = array();
				$sql_p = "select project_milestone_project " . 
						 " from project_milestones " . 
						 " where project_milestone_milestone = " . $milestone_id . 
						 " and project_milestone_date is not null " . 
					     " and project_milestone_date <> '0000-00-00' ";
				
				$res_p = mysql_query($sql_p) or dberror($sql_p);
				while ($row_p = mysql_fetch_assoc($res_p))
				{
					$tmp[] = $row_p["project_milestone_project"];
				}

				if(count($tmp) > 0)
				{
					$included_project_ids[$milestone_id] = $tmp;
					$logical_condition[$milestone_id] = $mfilter_array["logical" . $milestone_id];
				}

				
			}



			if(count($included_project_ids) == 0 and $mfilter_array["mhasnodate" . $milestone_id])
			{
				$tmp = array();
				$sql_p = "select project_milestone_project " . 
						 " from project_milestones " . 
						 " where project_milestone_milestone = " . $milestone_id . 
						 " and (project_milestone_date is  null " . 
					     " or project_milestone_date = '0000-00-00') ";
				
				$res_p = mysql_query($sql_p) or dberror($sql_p);
				while ($row_p = mysql_fetch_assoc($res_p))
				{
					$tmp[] = $row_p["project_milestone_project"];
				}

				if(count($tmp) > 0)
				{
					$included_project_ids[$milestone_id] = $tmp;
					$logical_condition[$milestone_id] = $mfilter_array["logical" . $milestone_id];
				}

				
			}

		}
	}

	//compoase milestone filter
	$tmp_filter = "";
	if(count($included_project_ids) > 0)
	{
		foreach($included_project_ids as $milestone_id=>$included_projects)
		{
			if(!$tmp_filter)
			{
				$tmp_filter .= " project_id in (" . implode(",",$included_projects) . ")";
			}
			elseif($logical_condition[$milestone_id] == 2)
			{
				$tmp_filter .= " or project_id in (" . implode(",",$included_projects) . ")";
			}
			else
			{
				$tmp_filter = " (" . $tmp_filter . ") and project_id in (" . implode(",",$included_projects) . ")";
			}
		}
		
		if($tmp_filter)
		{
			$filter .= " and (" . $tmp_filter . ")";
		}
	}
	


	//items
	$project_with_items_inicated = array();
	if($query_filter["item"]) //Items
	{
		
		$tmp = explode("-", $query_filter["item"]);
		foreach($tmp as $key=>$value)
		{
			if($value)
			{
				$filter_items .= $value . ",";
			}
		}
		
		$filter_items = "(" . substr($filter_items, 0, strlen($filter_items)-1) . ")";
		if($filter_items != "()")
		{
		
			$sql_p = "select DISTINCT order_item_order " .
					 "from order_items " .
				     " left join orders on order_id = order_item_order " . 
					 "where order_type = 1 and order_item_quantity > 0 " . 
				     "and order_item_item IN " . $filter_items; 
			
			$res_p = mysql_query($sql_p) or dberror($sql_p);
			while ($row_p = mysql_fetch_assoc($res_p))
			{
				$project_with_items_inicated[$row_p["order_item_order"]] = $row_p["order_item_order"];
			}



			//check catalogue orders
			$sql_p = "select DISTINCT order_items_in_project_project_order_id " .
					 "from order_items " .
				     " left join orders on order_id = order_item_order " .
				     " inner join order_items_in_projects on order_items_in_project_order_id = order_id " . 
					 "where order_type = 2 and order_item_quantity > 0 " . 
				     "and order_item_item IN " . $filter_items; 
			
			$res_p = mysql_query($sql_p) or dberror($sql_p);
			while ($row_p = mysql_fetch_assoc($res_p))
			{
				$project_with_items_inicated[$row_p["order_items_in_project_project_order_id"]] = $row_p["order_items_in_project_project_order_id"];
			}

		}

		if(count($project_with_items_inicated) > 0)
		{
			$filter .= " and order_id IN (" . implode(",", $project_with_items_inicated) . ") ";
		}

	}


	//order_items
	$item_filter = "";
	$filter_without_items = "";
	if(strpos($outputfields, "order_items.") === 0 
		or strpos($outputfields, "items.") === 0
		or strpos($outputfields, "order_items.") > 0 
		or strpos($outputfields, "items.") > 0
		or strpos($outputfields, "items.") > 0
		or in_array('item_quantity_block', $data_blocks)
		)
	{

		if($query_filter["item"]) //Items
		{
			
			$filter_items = "";
			$tmp = explode("-", $query_filter["item"]);
			foreach($tmp as $key=>$value)
			{
				if($value)
				{
					$filter_items .= $value . ",";
				}
			}
			
			$filter_items = "(" . substr($filter_items, 0, strlen($filter_items)-1) . ")";
			if($filter_items != "()")
			{
				$item_filter .= " and order_item_item IN " . $filter_items; 
			}
		}
		
		if(array_key_exists("icat", $query_filter))
		{
			$icats = $query_filter["icat"];
			$icats = substr($icats,0, strlen($icats)-1); // remove last comma
			$icats = str_replace("-", ",", $icats);
			if($icats)
			{
				$item_filter .= " and item_category IN (" . $icats . ") ";
			}
		}
	}


	if(strpos($outputfields, "ln_basicdata") === 0 
		or strpos($outputfields, "ln_basicdata") === 0
		or strpos($outputfields, "ln_basicdata") > 0 
		or strpos($outputfields, "ln_basicdata") > 0
		or $query_filter["noln"] == 1
		or $query_filter["lnre"] == 1
		)
	{
		$filter .= " and (ln_basicdata_version = 0 or ln_basicdata_version is null) ";
	}

	if(strpos($outputfields, "cer_basicdata") === 0 
		or strpos($outputfields, "cer_basicdata") === 0
		or strpos($outputfields, "cer_basicdata") > 0 
		or strpos($outputfields, "cer_basicdata") > 0
		or $query_filter["nocer"] == 1
		or $query_filter["cerre"] == 1
		)
	{
		$filter .= " and (cer_basicdata_version = 0 or cer_basicdata_version is null) ";
	}


	$filter_without_items = $filter;
	
	if($item_filter)
	{
		//$filter .= " and item_type = 1 "; // only standard items
		//$filter .= $item_filter;
	}



	// COMPOSE SQL
	
	$outputfields = "project_id, order_id, " . $outputfields;
	$sql = "select DISTINCT " . $outputfields . " from projects ";
	
	$sql_join = "";
	$sql_join .= "left join orders as orders on orders.order_id = projects.project_order ";
	$sql_join .= "left join project_costs as project_costs on project_costs.project_cost_order = projects.project_order ";
	
	
	$sql_join .= "left join postypes as postypes on postypes.postype_id = projects.project_postype ";
	
	$sql_join .= "left join possubclasses as possubclasses on possubclasses.possubclass_id = projects.project_pos_subclass ";


	
	$sql_join .= "left join product_lines as product_lines on product_lines.product_line_id = projects.project_product_line ";
	$sql_join .= "left join productline_subclasses as productline_subclasses on productline_subclasses.productline_subclass_id = projects.project_product_line_subclass ";
	
	$sql_join .= "left join project_costtypes as project_costtypes on project_costtypes.project_costtype_id = project_costs.project_cost_type ";

	$sql_join .= "left join projectkinds as projectkinds on projectkinds.projectkind_id = projects.project_projectkind ";
	$sql_join .= " left join mps_distchannels on mps_distchannel_id = projects.project_distribution_channel ";
	$sql_join .= " left join mps_distchannel_groups on mps_distchannel_groups.mps_distchannel_group_id = mps_distchannels.mps_distchannel_group_id ";

	$sql_join .= "left join project_states as project_states on project_states.project_state_id = projects.project_state ";
	
	$sql_join .= "left join countries as countries on countries.country_id = orders.order_shop_address_country ";
	$sql_join .= "left join salesregions as salesregions on salesregion_id = countries.country_salesregion ";
	$sql_join .= "left join regions on region_id = countries.country_region ";
	$sql_join .= "left join project_type_subclasses on project_type_subclasses.project_type_subclass_id = projects.project_type_subclass_id ";
	
	
	//client
	$sql_join .= "left join addresses as clients on clients.address_id = orders.order_client_address ";
	$sql_join .= "left join client_types as clienttypes on clienttypes.client_type_id = clients.address_client_type ";
	$sql_join .= "left join countries as clientcountries on clientcountries.country_id = clients.address_country ";

	//franchisee
	$sql_join .= "left join addresses as franchisees on franchisees.address_id = orders.order_franchisee_address_id ";
	$sql_join .= "left join countries as franchiseecountries on franchiseecountries.country_id = franchisees.address_country ";
	$sql_join .= "left join agreement_types as posagreements on posagreements.agreement_type_id = projects.project_fagagreement_type "; 


	//posaddresses
	$sql_join .= "LEFT JOIN posorders ON posorders.posorder_order = order_id ";
	$sql_join .= "LEFT JOIN posaddresses ON posaddresses.posaddress_id = posorders.posorder_posaddress ";
	$sql_join .= "LEFT JOIN places ON place_id = posaddresses.posaddress_place_id ";

	
	//posaddressespipeline
	$sql_join .= "LEFT JOIN posorderspipeline ON posorderspipeline.posorder_order = order_id ";
	$sql_join .= "LEFT JOIN posaddressespipeline ON posaddressespipeline.posaddress_id = posorderspipeline.posorder_posaddress ";
	$sql_join .= "LEFT JOIN places as places1 ON places1.place_id = posaddressespipeline.posaddress_place_id ";
	
	//order_items
	



	if($query_filter["cafrom"] != '' or $query_filter["cato"] != '')
	{
		$sql_join .= "left JOIN actual_order_states ON actual_order_state_order = order_id ";
		$sql_join .= "left JOIN order_items ON order_item_order = order_id ";
		$sql_join .= "left join items on item_id = order_item_item ";
	}

	if($query_filter["supp"] > 0 and $query_filter["forw"] > 0) // supplier and forwarder address
	{
		$sql_join .= "left JOIN order_items ON order_item_order = order_id ";
		$sql_join .= "left join items on item_id = order_item_item ";
	}
	elseif($query_filter["supp"] > 0) // supplier address
	{
		$sql_join .= "left JOIN order_items ON order_item_order = order_id ";
		$sql_join .= "left join items on item_id = order_item_item ";
	}
	elseif($query_filter["forw"] > 0) // forwarder address
	{
		$sql_join .= "left JOIN order_items ON order_item_order = order_id ";
		$sql_join .= "left join items on item_id = order_item_item ";
	}
	elseif(strpos($outputfields, "order_items.") === 0 
		or strpos($outputfields, "items.") === 0
		or strpos($outputfields, "order_items.") > 0 
		or strpos($outputfields, "items.") > 0
		or in_array('item_quantity_block', $data_blocks)
		)
	{
		//$sql_join .= "left join order_items on order_item_order = order_id ";
		//$sql_join .= "left join items on item_id = order_item_item ";
	}


	if(strpos($outputfields, "ln_basicdata") === 0 
		or strpos($outputfields, "ln_basicdata") === 0
		or strpos($outputfields, "ln_basicdata") > 0 
		or strpos($outputfields, "ln_basicdata") > 0
		or $query_filter["noln"] == 1
		or $query_filter["lnre"] == 1
		)
	{
		$sql_join .= "left join ln_basicdata on ln_basicdata_project = project_id ";
	}

	if(strpos($outputfields, "cer_basicdata") === 0 
		or strpos($outputfields, "cer_basicdata") === 0
		or strpos($outputfields, "cer_basicdata") > 0 
		or strpos($outputfields, "cer_basicdata") > 0
		or $query_filter["nocer"] == 1
		or $query_filter["cerre"] == 1
		)
	{
		$sql_join .= "left join cer_basicdata on cer_basicdata_project = project_id ";
	}

	
	
	$sql_tmp = $sql;
	$sql .= $sql_join . " " . $filter;

	//echo date("Y-m-d H:i:s") . "<br />";
	if($query_filter["latp"] > 0) // Include only the latest operating project from POS Locations
	{
		$latest_projects = array();
		$sql_p = "select DISTINCT order_id from projects ";
		$sql_p .= $sql_join . " " . $filter;

		$res = mysql_query($sql_p) or dberror($sql_p);
        while ($row = mysql_fetch_assoc($res))
        {
			
			//get posaddress_id
			$sql_o = "select posorder_posaddress " . 
				     " from posorders " . 
				     " where posorder_order = " . $row["order_id"];
			$res_o = mysql_query($sql_o) or dberror($sql_o);
			if ($row_o = mysql_fetch_assoc($res_o))
			{
				//echo date("Y-m-d H:i:s") . "<br />";
				$posaddress_id = $row_o["posorder_posaddress"];
				$sql_o = "select posorder_order " .
					     " from posorders " . 
						 " left join projects on project_order = posorder_order " . 
					     " where posorder_type = 1 " . 
					     " and project_actual_opening_date is not NULL ". 
					     " and project_actual_opening_date <> '0000-00-00' " . 
					     " and (project_shop_closingdate is null or project_shop_closingdate = '0000-00-00') " . 
					     " and posorder_posaddress = " . dbquote($posaddress_id) . 
					     " order by project_actual_opening_date DESC";
				$res_o = mysql_query($sql_o) or dberror($sql_o);
				if ($row_o = mysql_fetch_assoc($res_o))
				{
					$latest_projects[$row_o["posorder_order"]] = $row_o["posorder_order"];
				}
				//echo date("Y-m-d H:i:s") . "<br />";
			}
		}

		if(count($latest_projects) > 0)
		{
			$sql = $sql_tmp . $sql_join . " " . $filter . " and order_id IN (" . implode(',', $latest_projects) . ") ";
		}
	}

	//echo date("Y-m-d H:i:s") . "<br />";
	


	//create captions and fields for the sql
	$query_sortorder = array();
	foreach($projectquery_order as $key=>$value)
	{
		if($db_info["attributes"][$value] == 'item_quantity_block')
		{

		}
		elseif($db_info["attributes"][$value] == 'calculated_content')
		{

		}
		elseif($db_info["attributes"][$value] == 'content_by_function')
		{

		}
		elseif($db_info["attributes"][$value] == 'subgroup_category_order_state')
		{

		}
		elseif($db_info["attributes"][$value] == 'subgroup_category_delivery_state')
		{

		}
		elseif($db_info["attributes"][$value] == 'content_by_subquery')
		{
			$query_sortorder[] = $value;
		}
		else
		{
			if(strpos($db_info["attributes"][$value], "IF(") === 0)
			{
				$tmp = explode(' AS ', $db_info["attributes"][$value]);
				$query_sortorder[] = $tmp[1];
			}
			else
			{
				$query_sortorder[] = $db_info["attributes"][$value];
			}
		}
	}

	
	$order_clause = implode(',', $query_sortorder );

	

	
	//add grouping as primary sort order
	if($query_group03 != NULL and $query_group03 != '')
	{
		
		if($db_info["attributes"][$query_groups[$query_group03]] == "calculated_content")
		{
			$tmp = explode('.', $query_groups[$query_group03]);
			$field_name = $tmp[1];
			$new_order_field =  $tmp[1] . ", ";
			$order_clause = str_replace($new_order_field, "", $order_clause);
			$order_clause = $tmp[1] . ", " . $order_clause;
		}
		elseif(strpos($db_info["attributes"][$query_groups[$query_group03]], "IF(") === 0)
		{
			$tmp = explode(' AS ', $db_info["attributes"][$query_groups[$query_group03]]);
			$field_name = $tmp[1];

			$new_order_field =  $tmp[1] . ", ";
			$order_clause = str_replace($new_order_field, "", $order_clause);
			$order_clause = $tmp[1] . ", " . $order_clause;
		}
		else
		{
			$new_order_field = $db_info["attributes"][$query_groups[$query_group03]] . ", ";
			$order_clause = str_replace($new_order_field, "", $order_clause);
			$order_clause = $db_info["attributes"][$query_groups[$query_group03]] . ", " . $order_clause;
		}

		
		

		//move grouping field to first position of output fields
		$old_position = array_search  ( $query_groups[$query_group03]  , $selected_field_order );
		if($old_position > 0)
		{
			$group_field_caption = $captions[$old_position];
			for($i=$old_position;$i>0;$i--)
			{
				$selected_field_order[$i] = $selected_field_order[$i-1];
				$captions[$i] = $captions[$i-1];
			}
			$selected_field_order[0] = $query_groups[$query_group03];
			$captions[0] = $group_field_caption;
		}
		if(substr($order_clause, strlen($order_clause)-2, 2) == ", ")
		{
			$order_clause = substr($order_clause, 0, strlen($order_clause)-2);
		}
	}
	if($query_group02 != NULL and $query_group02 != '')
	{
		if($db_info["attributes"][$query_groups[$query_group02]] == "calculated_content")
		{
			$tmp = explode('.', $query_groups[$query_group02]);
			$field_name = $tmp[1];
			$new_order_field =  $tmp[1] . ", ";
			$order_clause = str_replace($new_order_field, "", $order_clause);
			$order_clause = $tmp[1] . ", " . $order_clause;
		}
		elseif(strpos($db_info["attributes"][$query_groups[$query_group02]], "IF(") === 0)
		{
			$tmp = explode(' AS ', $db_info["attributes"][$query_groups[$query_group02]]);
			$field_name = $tmp[1];

			$new_order_field =  $tmp[1] . ", ";
			$order_clause = str_replace($new_order_field, "", $order_clause);
			$order_clause = $tmp[1] . ", " . $order_clause;
		}
		else
		{
			$new_order_field = $db_info["attributes"][$query_groups[$query_group02]] . ", ";
			$order_clause = str_replace($new_order_field, "", $order_clause);
			$order_clause = $db_info["attributes"][$query_groups[$query_group02]] . ", " . $order_clause;
		}
		

		//move grouping field to first position of output fields
		$old_position = array_search  ( $query_groups[$query_group02]  , $selected_field_order );
		if($old_position > 0)
		{
			$group_field_caption = $captions[$old_position];
			for($i=$old_position;$i>0;$i--)
			{
				$selected_field_order[$i] = $selected_field_order[$i-1];
				$captions[$i] = $captions[$i-1];
			}
			$selected_field_order[0] = $query_groups[$query_group02];
			$captions[0] = $group_field_caption;
		}
		if(substr($order_clause, strlen($order_clause)-2, 2) == ", ")
		{
			$order_clause = substr($order_clause, 0, strlen($order_clause)-2);
		}
	}
	if($query_group01 != NULL and $query_group01 != '')
	{

		if($db_info["attributes"][$query_groups[$query_group01]] == "calculated_content")
		{
			$tmp = explode('.', $query_groups[$query_group01]);
			$field_name = $tmp[1];
			$new_order_field =  $tmp[1] . ", ";
			$order_clause = str_replace($new_order_field, "", $order_clause);
			$order_clause = $tmp[1] . ", " . $order_clause;
		}
		elseif(strpos($db_info["attributes"][$query_groups[$query_group01]], "IF(") === 0)
		{
			$tmp = explode(' AS ', $db_info["attributes"][$query_groups[$query_group01]]);
			$field_name = $tmp[1];

			$new_order_field =  $tmp[1] . ", ";
			$order_clause = str_replace($new_order_field, "", $order_clause);
			$order_clause = $tmp[1] . ", " . $order_clause;
		}
		else
		{
			$new_order_field = $db_info["attributes"][$query_groups[$query_group01]] . ", ";
			$order_clause = str_replace($new_order_field, "", $order_clause);
			$order_clause = $db_info["attributes"][$query_groups[$query_group01]] . ", " . $order_clause;
		}

		//move grouping field to first position of output fields
		$old_position = array_search  ( $query_groups[$query_group01]  , $selected_field_order );
		if($old_position > 0)
		{
			$group_field_caption = $captions[$old_position];
			for($i=$old_position;$i>0;$i--)
			{
				$selected_field_order[$i] = $selected_field_order[$i-1];
				$captions[$i] = $captions[$i-1];
			}
			$selected_field_order[0] = $query_groups[$query_group01];
			$captions[0] = $group_field_caption;
		}

		if(substr($order_clause, strlen($order_clause)-2, 2) == ", ")
		{
			$order_clause = substr($order_clause, 0, strlen($order_clause)-2);
		}

	}


	if($general_sortorder == 1)
	{
		$order_clause = str_replace(', ', ',', $order_clause);
		$order_clause = str_replace(',', ' DESC,', $order_clause) . ' DESC';
	}

		
	
	if($order_clause)
	{
		$sql .= " order by " . $order_clause;
	}



	//data blocks
	
	$caption_count_without_data_blocks = count($captions);

	if(count($data_blocks) > 0)
	{
		foreach($data_blocks as $key=>$data_block_name)
		{
			$tmp_data = array();

			if($data_block_name == 'item_quantity_block')
			{
				
				foreach($project_with_items_inicated as $key2=>$order_id)
				{
					
					$sql_datablock = 'select project_id, project_order, item_id, ' . 
						             ' item_code, item_name, order_item_quantity ' . 
						             ' from projects ' . 
									 ' left join order_items on order_item_order = project_order ' .
						             ' left join items on item_id = order_item_item ' .
					                 ' where project_order = ' . $order_id . 
						             $item_filter;
					

					$res_data_block = mysql_query($sql_datablock) or dberror($sql_datablock);
					while ($row = mysql_fetch_assoc($res_data_block))
					{
						$tmp = array();
						$tmp["item_code"] = $row['item_code'];
						$tmp["item_name"] = $row['item_name'];
						$tmp["order_item_quantity"] = $row['order_item_quantity'];
						
						
						$data_blocks_data[$data_block_name][$row['project_id']][$row['item_code']] = $tmp;
						
						//if($row['order_item_quantity'] > 0)
						//{
							$data_blocks_captions[$data_block_name][$row['item_code']] = $row['item_code'] . " - " . $row['item_name'];
						//}
					}

					//get catalogue orders
					
					$sql_c = 'select project_id, project_order, item_id, ' . 
							 ' item_code, item_name, order_item_quantity ' . 
							 ' from order_items_in_projects ' .
						     ' LEFT JOIN projects ON project_order = order_items_in_project_project_order_id ' . 
						     ' left join orders on order_id = order_items_in_project_order_id ' . 
							 ' left join order_items on order_item_id = order_items_in_project_order_item_id ' .
							 ' left join items on item_id = order_item_item ' . 
							 ' where order_items_in_project_project_order_id = ' . $order_id . 
							 $item_filter;

					$res_c = mysql_query($sql_c) or dberror($sql_c);
					while ($row_c = mysql_fetch_assoc($res_c))
					{
						$tmp = array();
						$tmp["item_code"] = $row_c['item_code'];
						$tmp["item_name"] = $row_c['item_name'];
						

						if(array_key_exists($row_c['project_id'], $data_blocks_data[$data_block_name])
							and array_key_exists($row_c['item_code'], $data_blocks_data[$data_block_name][$row_c['project_id']])	
						)
						{
							$tmp["order_item_quantity"] = $data_blocks_data[$data_block_name][$row_c['project_id']][$row_c['item_code']]['order_item_quantity'] + $row_c['order_item_quantity'];
							
							$data_blocks_data[$data_block_name][$row_c['project_id']][$row_c['item_code']] = $tmp;
						}
						else
						{
							$tmp["order_item_quantity"] = $row_c['order_item_quantity'];
							$data_blocks_data[$data_block_name][$row_c['project_id']][$row_c['item_code']] = $tmp;
							
							//if($row_c['order_item_quantity'] > 0)
							//{
								$data_blocks_captions[$data_block_name][$row_c['item_code']] = $row_c['item_code'] . " - " . $row_c['item_name'];
							//}
						}
					}
					
					
				}
				if(array_key_exists($data_block_name, $data_blocks_captions))
				{
					foreach($data_blocks_captions[$data_block_name] as $key=>$value)
					{
						$captions[] = $value;
					}
				}
			}
		}
	}
	
	//echo '<pre>';


	//var_dump($data_blocks_data);cde();

	//echo $order_clause . "/";cde();
	
	//echo date("Y-m-d H:i:s") . "<br />";
	
	//echo $sql;abc();

	//var_dump($query_filter);
	//abc();

	//var_dump($selected_field_order);
	//abc();


}
else
{

}

?>