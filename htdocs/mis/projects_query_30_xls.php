<?php
/********************************************************************

    projects_query_30_xls.php

    Print status report POS Locations

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2015-03-24
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2015-03-24
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_queries");

require_once "../include/phpexcel/PHPExcel.php";
require_once "include/get_functions.php";
require_once("projects_query_filter_strings.php");

/********************************************************************
    prepare all data needed
*********************************************************************/

if(!param("query_id"))
{
	redirect("projects_queries.php");
}


$print_query_filter = 0;
$query_name = "";

$sql = "select mis_query_name, mis_query_filter, mis_print_filter from mis_queries " .
	   "where mis_query_id = " . param("query_id");

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$query_name = $row["mis_query_name"];

	if($row["mis_print_filter"] == 1)
	{
		$print_query_filter = 1;
	}


	$filters = array();
	$filters = unserialize($row["mis_query_filter"]);

	foreach($filters as $key=>$value)
	{

		$gr = $filters["gr"]; // Geografical Regions
		$re = $filters["re"]; // Supplied Regions
		$cnt = $filters["co"]; // Countries
		
		$clt = $filters["clt"]; // Client Types
		$pl = $filters["pl"]; // product lines
		$pls = $filters["plsc"]; // product line subclasses
		$pk = $filters["pk"]; // project type
		$pt = $filters["pt"]; // POS types
		$suc = $filters["ptsc"]; // POS Type Subclass
		$lpt = $filters["pct"]; // project cost type
		$from_year = $filters["fdy"]; // from year
		$to_year = $filters["tdy"]; // to year

		$exclude_e_stores = ""; // subclass 20
		if(array_key_exists("exclude_e_stores", $filters))
		{
			$exclude_e_stores = $filters["exclude_e_stores"];
		}

		if(!$from_year){$from_year = date("Y");}
		if(!$to_year){$to_year = date("Y");}

		$agreed_opening_date_must_be_equal_to_the_last_year = $filters["idvi"];
		$do_not_show_country_lines = $filters["arch"];
	}
}
else
{
	redirect("projects_queries.php");
}



$gr = substr($gr,0, strlen($gr)-1); // remove last comma
$gr = str_replace("-", ",", $gr);
$re = substr($re,0, strlen($re)-1); // remove last comma
$re = str_replace("-", ",", $re);


//SALES REGIONS
$geographical_regions = array();
$geographical_region_filter = "";
if($gr) // geografical regions
{
	$geographical_region_filter =  " and (country_salesregion IN (" . $gr . "))";
	$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);

	$sql = "select salesregion_id, salesregion_name from salesregions " . 
		   " where salesregion_id IN (" . $gr . ")" .
		   " order by salesregion_name";
}
else
{
	$sql = "select salesregion_id, salesregion_name from salesregions " . 
		   " order by salesregion_name";
}

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$geographical_regions[$row["salesregion_id"]] = $row["salesregion_name"];
}


//COUNTRIES
$cnt = substr($cnt,0, strlen($cnt)-1); // remove last comma
$cnt = str_replace("-", ",", $cnt);
$country_filter = "";
if($cnt)
{
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);
	$country_filter =  " and country_id IN (" . $cnt . ")";
}


//POS types
$pos_types = array();
$pt = substr($pt,0, strlen($pt)-1); // remove last comma
$pt = str_replace("-", ",", $pt);
$pos_type_filter = "";
if($pt)
{
    $pos_type_filter =  " IN (" . $pt . ")";
	$_filter_strings["POS Types"] = get_filter_string("pt", $pt);

	$sql = "select postype_id, postype_name from postypes " . 
		   " where postype_id IN (" . $pt . ")" .
		   " order by postype_name";
}
else
{
	$sql = "select postype_id, postype_name from postypes " . 
		   " order by postype_name";
}
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$pos_types[$row["postype_id"]] = $row["postype_name"];
}
ksort($pos_types);

//pos type subclasses
$pos_type_subclass_filter = "";
$suc = substr($suc,0, strlen($suc)-1); // remove last comma
$suc = str_replace("-", ",", $suc);


if($suc)
{
   $suc = str_replace("ns", "0", $suc); // pos type without subclass
   $pos_type_subclass_filter =  " IN (" . $suc . ") ";
   $_filter_strings["POS Type Subclasses"] = get_filter_string("suc", $suc);
}

//project kinds
$project_kinds = array();
$pk = substr($pk,0, strlen($pk)-1); // remove last comma
$pk = str_replace("-", ",", $pk);
$project_kind_filter = "";
$_filter_strings["Project Types"] = "";
if($pk) // project_kind
{
    $project_kind_filter =  " IN (" . $pk . ")";
	$sql = "select projectkind_id, projectkind_code, projectkind_name from projectkinds " . 
		   " where projectkind_id IN (" . $pk . ")" .
		   " order by projectkind_name";
}
else
{
	$sql = "select projectkind_id, projectkind_code, projectkind_name from projectkinds " .
		   " where projectkind_id > 0 " . 
		   " order by projectkind_name";
		
}

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$project_kinds[$row["projectkind_id"]] = $row["projectkind_code"];

	$_filter_strings["Project Types"] .= $row["projectkind_code"] . "-" . $row["projectkind_name"] . ", ";
}
$_filter_strings["Project Types"] = substr($_filter_strings["Project Types"], 0, strlen($_filter_strings["Project Types"]) - 2);


//client types
$clt = substr($clt,0, strlen($clt)-1); // remove last comma
$clt = str_replace("-", ",", $clt);
$client_type_filter = "";
if($clt) // client type
{
    $client_type_filter = " IN (" . $clt . ")";
	$_filter_strings["Client Types"] = get_filter_string("clt", $clt);
}

//legal types
$legal_types = array();
$legal_type_filter = "";
$lpt = substr($lpt,0, strlen($lpt)-1); // remove last comma
$lpt = str_replace("-", ",", $lpt);
if($lpt)
{
   $legal_type_filter = " IN (" . $lpt . ")";
   $_filter_strings["Legal Types"] = get_filter_string("lpt", $lpt);
}

$sql = "select project_costtype_id, project_costtype_text " . 
       "from project_costtypes ";
if($legal_type_filter)
{
	$sql .= " where project_costtype_id " . $legal_type_filter;	
}
$sql .= " order by project_costtype_id";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$legal_types[$row["project_costtype_id"]] = $row["project_costtype_text"];
}


//product lines
$product_line_filter = "";
$pl = substr($pl,0, strlen($pl)-1); // remove last comma
$pl = str_replace("-", ",", $pl);
if($pl) // product line
{
    $product_line_filter =  " IN (" . $pl . ")";
	$_filter_strings["Product Lines"] = get_filter_string("pl", $pl);
}

// product line subclass
$product_line_subclass_filter = "";
$pls = substr($pls,0, strlen($pls)-1); // remove last comma
$pls = str_replace("-", ",", $pls);
if($pls)
{
    $product_line_subclass_filter =  " IN (" . $pls . ")";
	$_filter_strings["Product Line Subclasses"] = get_filter_string("pls", $pls);
}

$colwidths = array();
$colwidths[0] = "12";

/********************************************************************
    Start output
*********************************************************************/
$objPHPExcel = new PHPExcel();



//output formats
$default_border = array(
    'style' => PHPExcel_Style_Border::BORDER_THIN,
    'color' => array('rgb'=>'1006A3')
);

$style_normal_border = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '1006A3'),
		),
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
);


$style_normal_border_right = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '1006A3'),
		),
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	),
);


$style_title = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
    'font' => array(
        'bold' => true,
		'size' => 16
    )
);


$style_title2 = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
    'font' => array(
        'bold' => true,
		'size' => 10
    )
);

$style_header = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'font' => array(
        'bold' => true,
    ),
	'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb'=>'EFEFEF')),
);

$style_header_right = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	),
    'font' => array(
        'bold' => true,
    ),
	'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb'=>'EFEFEF')),
);


$year_header = array(
    
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'font' => array(
        'bold' => true,
		'size' => 11
    )
);

$year_header_right = array(
    
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	),
    'font' => array(
        'bold' => true,
		'size' => 11
    )
);

$logo = new PHPExcel_Worksheet_Drawing();
$logo->setName('Logo');
$logo->setDescription('Logo');
$logo->setPath('../pictures/brand_logo.jpg');
$logo->setHeight(36);
$logo->setWidth(113);

$sheet = $objPHPExcel->getActiveSheet();


$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3);

$objPHPExcel->getActiveSheet()->setTitle('Order States');

$logo->setWorksheet($objPHPExcel->getActiveSheet());

//default styles
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(8); 


// HEADRES ROW 1
$sheet->setCellValue('D1', 'Status Report POS Locations: ' . $query_name . ' (' . date("d.m.Y H:i:s") . ')');
$sheet->getStyle('D1')->applyFromArray( $style_title );
$sheet->getRowDimension('1')->setRowHeight(36);




//OUTPUT DATA
$cell_index = 0;
$row_index = 3;

if($print_query_filter == 1)
{
	foreach($_filter_strings as $key=>$value)
	{
		 $sheet->setCellValueByColumnAndRow($cell_index, $row_index, $key . ": " . $value);
		 $row_index++;
	}
	$row_index++;
}
$row_index++;
$row_index++;

//print headers for each year
$cell_index = 1;
$row_index2 = 0;

$list_totals = array();
$actual_year = date("Y");

//header for operating POS locations
foreach($pos_types as $pos_type_id=>$pos_type_name)
{
	for($year=$from_year;$year<=$to_year;$year++)
	{
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $pos_type_name . " openings in " . $year);
		$sheet->getStyleByColumnAndRow($cell_index, $row_index)->applyFromArray( $year_header );
		
		$row_index2 = $row_index + 1;
		
		foreach($project_kinds as $project_kind=>$project_kind_code)
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index2, $project_kind_code);
			$sheet->getStyleByColumnAndRow($cell_index, $row_index2)->applyFromArray( $style_header_right );
			$colwidths[$cell_index] = 5;
			$cell_index++;
		}

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index2, "Total");
		$sheet->getStyleByColumnAndRow($cell_index, $row_index2)->applyFromArray( $style_header_right );
		$colwidths[$cell_index] = 7;
		$cell_index++;


		$sheet->setCellValueByColumnAndRow($cell_index, $row_index2, "Closed");
		$sheet->getStyleByColumnAndRow($cell_index, $row_index2)->applyFromArray( $style_header_right );
		$colwidths[$cell_index] = 8;
		$cell_index++;
		$sheet->getColumnDimensionByColumn($cell_index)->setWidth(4);
		$cell_index++;
	}

	//header for ongoing projects, not operating
	if($to_year == $actual_year)
	{
		
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $pos_type_name . " Projects in process for " . $actual_year);
		$sheet->getStyleByColumnAndRow($cell_index, $row_index)->applyFromArray( $year_header );
		
		$row_index2 = $row_index + 1;
		
		foreach($project_kinds as $project_kind=>$project_kind_code)
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index2, $project_kind_code);
			$sheet->getStyleByColumnAndRow($cell_index, $row_index2)->applyFromArray( $style_header_right );
			$colwidths[$cell_index] = 5;
			$cell_index++;
		}

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index2, "Total");
		$sheet->getStyleByColumnAndRow($cell_index, $row_index2)->applyFromArray( $style_header_right );
		$colwidths[$cell_index] = 7;
		$cell_index++;

		
		if($agreed_opening_date_must_be_equal_to_the_last_year == 0)
		{
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index2, "of which on hold");
			$sheet->getStyleByColumnAndRow($cell_index, $row_index2)->applyFromArray( $style_header_right );
			$sheet->getColumnDimensionByColumn($cell_index)->setWidth(16);
			$cell_index++;
		}

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index2, "of which approved");
		$sheet->getStyleByColumnAndRow($cell_index, $row_index2)->applyFromArray( $style_header_right );
		$sheet->getColumnDimensionByColumn($cell_index)->setWidth(18);
		$cell_index++;

		$sheet->getColumnDimensionByColumn($cell_index)->setWidth(4);
		$cell_index++;

		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $actual_year);
		$sheet->getStyleByColumnAndRow($cell_index, $row_index)->applyFromArray( $year_header_right );
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index2, "Planned Closings");
		$sheet->getStyleByColumnAndRow($cell_index, $row_index2)->applyFromArray( $style_header_right );
		$sheet->getColumnDimensionByColumn($cell_index)->setWidth(18);
		$cell_index++;

		$sheet->getColumnDimensionByColumn($cell_index)->setWidth(4);
		$cell_index++;
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $actual_year);
		$sheet->getStyleByColumnAndRow($cell_index, $row_index)->applyFromArray( $year_header_right );

		
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index2, "Total");
		$sheet->getStyleByColumnAndRow($cell_index, $row_index2)->applyFromArray( $style_header_right );
		$cell_index++;

		$sheet->getColumnDimensionByColumn($cell_index)->setWidth(4);
		$cell_index++;

	}
}

if($to_year == $actual_year)
{

	//add block total pos operating
	if(count($pos_types) == 1)
	{
		$row_index = $row_index-2;
		
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "Total POS operating");
		$sheet->getStyleByColumnAndRow($cell_index, $row_index)->applyFromArray( $year_header );
		$row_index++;
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "as per " . date("Y-m-d"));
		$sheet->getStyleByColumnAndRow($cell_index, $row_index)->applyFromArray( $year_header );
	}
	else
	{
		$row_index = $row_index-1;
		
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "Total POS operating as per " . date("Y-m-d"));
		$sheet->getStyleByColumnAndRow($cell_index, $row_index)->applyFromArray( $year_header );
	}

	$row_index++;
	$tmp_cell_index = $cell_index;
	foreach($pos_types as $pos_type_id=>$pos_type_name)
	{
		$tmp_cell_index2 = $tmp_cell_index;
		$sheet->setCellValueByColumnAndRow($tmp_cell_index, $row_index, $pos_type_name);
		$sheet->getStyleByColumnAndRow($tmp_cell_index, $row_index)->applyFromArray( $year_header );
		$tmp_cell_index = $tmp_cell_index + count($legal_types);

		
		foreach($legal_types as $legal_type_id=>$legal_type_name)
		{
			$sheet->setCellValueByColumnAndRow($tmp_cell_index2, $row_index+1, $legal_type_name);
			$sheet->getStyleByColumnAndRow($tmp_cell_index2, $row_index+1)->applyFromArray( $style_header );
			$sheet->getColumnDimensionByColumn($tmp_cell_index2)->setWidth(strlen($legal_type_name)+ 2);

			$tmp_cell_index2++;
		}

		$sheet->setCellValueByColumnAndRow($tmp_cell_index2, $row_index+1, "Total");
		$sheet->getStyleByColumnAndRow($tmp_cell_index2, $row_index+1)->applyFromArray( $style_header );
		$tmp_cell_index++;
		$sheet->getColumnDimensionByColumn($tmp_cell_index)->setWidth(4);
		$tmp_cell_index++;
	}

	
}


$row_index++;
$row_index++;
$row_index++;

$cell_index = 0;
$group_row = $row_index;


$total_operating_pos_locations = array();

//output data per geografical region
$region_group = 0;
foreach($geographical_regions as $sales_region_id=>$geographical_region)
{
	
	if($client_type_filter)
	{
		$sql = "select country_id, country_name, country_salesregion " . 
			   " from countries " .
			   " left join addresses on address_country = country_id " . 
			   " where country_salesregion = " .  $sales_region_id . " " . 
			   $country_filter;
		$sql .= " and address_client_type " . $client_type_filter;
		$sql .= " order by country_name ";

	}
	else
	{
		$sql = "select country_id, country_name, country_salesregion " . 
			   " from countries " . 
			   " where country_salesregion = " .  $sales_region_id . " " . 
			   $country_filter .
			   " order by country_name ";
	}


	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		
		if($region_group != $sales_region_id)
		{
			$cell_index = 0;
			$totals_per_sales_region = array();
			
			$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $geographical_region);
			if($colwidths[0] < strlen($geographical_region)){$colwidths[0] = strlen($geographical_region)+3;}
			$sheet->getStyleByColumnAndRow($cell_index, $row_index)->applyFromArray( $style_header );
			$row_index++;

			$region_group = $row["country_salesregion"];
		}
		
		
		//caclulate total operating POS locations
		if($to_year == $actual_year)
		{
			
			foreach($pos_types as $pos_type_id=>$pos_type_name)
			{
				$sql_p = "select posaddress_store_postype, posaddress_ownertype, count(posaddress_id) as num_recs from posaddresses " . 
						 " left join addresses on address_id = posaddress_client_id " .
						 " left join countries on country_id = posaddress_country " . 
						 
						 " where (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') ";

				$sql_p .= " and country_id = " . $row["country_id"];
				$sql_p .= " and country_salesregion = " . $sales_region_id;
				$sql_p .= " and posaddress_store_postype = " . $pos_type_id;
				

				if($geographical_region_filter)
				{
					$sql_p .= $geographical_region_filter;
				}

				if($country_filter)
				{
					$sql_p .= $country_filter;
				}
				
				if($client_type_filter)
				{
					$sql_p .= " and address_client_type " . $client_type_filter;
				}
				
				if($pos_type_filter)
				{
					$sql_p .=  " and posaddress_store_postype " . $pos_type_filter;
				}

				if($legal_type_filter)
				{
					$sql_p .=  " and posaddress_ownertype " . $legal_type_filter;
				}

				if($pos_type_subclass_filter)
				{
					$sql_p .=  " and posaddress_store_subclass " . $pos_type_subclass_filter;
				}

				if($product_line_filter)
				{
					$sql_p .=  " and posaddress_store_furniture " . $product_line_filter;
				}

				if($product_line_subclass_filter)
				{
					$sql_p .=  " and posaddress_store_furniture_subclass " . $product_line_subclass_filter;
				}

				
				if($exclude_e_stores)
				{
					$sql_p .=  " and posaddress_store_subclass <> 20 ";
				}
				

				$sql_p .= " group by posaddress_store_postype, posaddress_ownertype " ;
				
				
				
				
				//echo $sql_p . "<br />";abc();
				
			
				$res_p = mysql_query($sql_p) or dberror($sql_p);
				while ($row_p = mysql_fetch_assoc($res_p))
				{
					$total_operating_pos_locations[$sales_region_id][$row["country_id"]][$pos_type_id][$row_p["posaddress_ownertype"]] = $row_p["num_recs"];
				}
			}
			
		}

	
		//echo "<pre>";
		//var_dump($total_operating_pos_locations);cde();
		
		//$total_projects_actual_year = 0;
		$number_of_onging_projects = array();
		
		
		
		//init number array
		$number_of_pos_operating = array();
		foreach($pos_types as $pos_type_id=>$pos_type_name)
		{
			$total_projects_actual_year = 0;
			
			for($year=$from_year;$year<=$to_year;$year++)
			{
				foreach($project_kinds as $project_kind_id=>$project_kind_code)
				{
					$number_of_pos_operating[$pos_type_id][$year][$project_kind_id] = "";
				}
			}
		}
		

		$number_of_pos_closed = array();
		foreach($pos_types as $pos_type_id=>$pos_type_name)
		{
			for($year=$from_year;$year<=$to_year;$year++)
			{
				$number_of_pos_closed[$pos_type_id][$year] = "";
			}
		}

		

		$number_of_total_pos = array();
		foreach($pos_types as $pos_type_id=>$pos_type_name)
		{
			for($year=$from_year;$year<=$to_year;$year++)
			{
				$number_of_total_pos[$pos_type_id][$year] = "";
			}
		}

		
	
		
		
		//calculate operating pos locations
		foreach($pos_types as $pos_type_id=>$pos_type_name)
		{
			
			for($year=$from_year;$year<=$to_year;$year++)
			{
				
				$sql_p = "select posaddress_id, posaddress_store_postype, " . 
					     " year(posaddress_store_closingdate) as closing_year " . 
					     " from posaddresses " .
					     " left join addresses on address_id = posaddress_client_id " . 
					     " left join countries on country_id = posaddress_country " .
						 " where posaddress_country = " . $row["country_id"];

				if($geographical_region_filter)
				{
					$sql_p .= $geographical_region_filter;
				}

				if($country_filter)
				{
					$sql_p .= $country_filter;
				}
				
				if($client_type_filter)
				{
					$sql_p .= " and address_client_type " . $client_type_filter;
				}
				
				
				if($legal_type_filter)
				{
					$sql_p .=  " and posaddress_ownertype " . $legal_type_filter;
				}

				if($pos_type_subclass_filter)
				{
					$sql_p .=  " and posaddress_store_subclass " . $pos_type_subclass_filter;
				}

				if($product_line_filter)
				{
					$sql_p .=  " and posaddress_store_furniture " . $product_line_filter;
				}

				if($product_line_subclass_filter)
				{
					$sql_p .=  " and posaddress_store_furniture_subclass " . $product_line_subclass_filter;
				}

				if($exclude_e_stores)
				{
					$sql_p .=  " and posaddress_store_subclass <> 20 ";
				}

				$sql_p .=  " and posaddress_store_postype = " . $pos_type_id;
				
				$res_p = mysql_query($sql_p) or dberror($sql_p);
				while ($row_p = mysql_fetch_assoc($res_p))
				{
					
					//check if pos was closed in the year
					$pos_closed = false;
					if($row_p["closing_year"] == $year)
					{
						$pos_closed = true;
						
						if(array_key_exists($row_p["posaddress_store_postype"], $number_of_pos_closed))
						{
							$number_of_pos_closed[$row_p["posaddress_store_postype"]][$year] = $number_of_pos_closed[$row_p["posaddress_store_postype"]][$year] + 1;
						}
						else
						{
							$number_of_pos_closed[$row_p["posaddress_store_postype"]][$year] = 1;
						}

					}

					//get latest project of the pos
					$sql_o = "select posorder_order, posorder_postype, posorder_project_kind, " .
						     "posorder_closing_date " . 
						     "from posorders " .
						     " where posorder_type = 1 " . 
						     " and posorder_posaddress = " . $row_p["posaddress_id"] . 
						     " and posorder_postype = " . $pos_type_id .
						     " and (year(posorder_opening_date) = " . $year . 
						     " or year(posorder_closing_date) = " . $year . ")";
						     
					if($project_kind_filter)
					{
						$sql_o .=  " and posorder_project_kind " . $project_kind_filter;
					}

					$sql_o .=  " and posorder_postype = " . $pos_type_id;

					if($legal_type_filter)
					{
						$sql_o .=  " and posorder_legal_type " . $legal_type_filter;
					}

					if($pos_type_subclass_filter)
					{
						$sql_o .=  " and posorder_subclass " . $pos_type_subclass_filter;
					}

					if($product_line_filter)
					{
						$sql_o .=  " and posorder_product_line " . $product_line_filter;
					}

					if($product_line_subclass_filter)
					{
						$sql_o .=  " and posorder_product_line_subclass " . $product_line_subclass_filter;
					}

					
					if($exclude_e_stores)
					{
						$sql_o .=  " and posorder_subclass <> 20 ";
					}


					$res_o = mysql_query($sql_o) or dberror($sql_o);
					while ($row_o = mysql_fetch_assoc($res_o))
					{
					
						if($row_o["posorder_closing_date"] == NULL 
							or $row_o["posorder_closing_date"] == '0000-00-00')
						{
							$number_of_pos_operating[$row_o["posorder_postype"]][$year][$row_o["posorder_project_kind"]] = $number_of_pos_operating[$row_o["posorder_postype"]][$year][$row_o["posorder_project_kind"]] + 1;
	
							$number_of_total_pos[$row_o["posorder_postype"]][$year] = $number_of_total_pos[$row_o["posorder_postype"]][$year] + 1;
						}
						else
						{
							if($pos_closed == false)
							{
								//$number_of_pos_closed[$row_o["posorder_postype"]][$year] = $number_of_pos_closed[$row_o["posorder_postype"]][$year] + 1;
							}
						}
					}
				}
			}
			

		
			if($to_year == $actual_year)
			{

				//calculate projects in process
				$total_projects = "";
				foreach($project_kinds as $project_kind_id=>$project_kind_code)
				{
					$number_of_onging_projects[$pos_type_id][$project_kind_id] = "";
				}
				

				$number_of_projects_approved[$pos_type_id] = 0;
				$sql_o = "select project_id, project_postype, project_projectkind, project_state " . 
						 "from projects " .
						 " left join orders on order_id = project_order " .
						 " left join project_costs on project_cost_order = order_id " . 
						 " left join addresses on address_id = order_client_address " . 
						 " where project_postype = " . $pos_type_id . 
						 " and (project_actual_opening_date is null or project_actual_opening_date = '0000-00-00') " . 
						 " and order_actual_order_state_code < '890' " . 
						 " and (order_archive_date is null or order_archive_date = '0000-00-00') " .
						 " and order_shop_address_country = " . $row["country_id"];

				
				if($agreed_opening_date_must_be_equal_to_the_last_year == 1)
				{
					$sql_o .= " and year(project_real_opening_date) = " . $actual_year;
				}
				
				if($project_kind_filter)
				{
					$sql_o .=  " and project_projectkind " . $project_kind_filter;
				}

				if($pos_type_filter)
				{
					$sql_o .=  " and project_postype " . $pos_type_filter;
				}


				if($client_type_filter)
				{
					$sql_o .= " and address_client_type " . $client_type_filter;
				}

				if($legal_type_filter)
				{
					$sql_o .= " and project_cost_type " . $legal_type_filter;
				}
				
				if($pos_type_subclass_filter)
				{
					$sql_o .=  " and project_pos_subclass " . $pos_type_subclass_filter;
				}

				if($product_line_filter)
				{
					$sql_o .=  " and project_product_line " . $product_line_filter;
				}

				if($product_line_subclass_filter)
				{
					$sql_o .=  " and project_product_line_subclass " . $product_line_subclass_filter;
				}

				if($exclude_e_stores)
				{
					$sql_o .=  " and project_pos_subclass <> 20 ";
				}

				//echo $sql_o;abc();
			
				$res_o = mysql_query($sql_o) or dberror($sql_o);
				while ($row_o = mysql_fetch_assoc($res_o))
				{
					
					$number_of_onging_projects[$row_o["project_postype"]][$row_o["project_projectkind"]] = $number_of_onging_projects[$row_o["project_postype"]][$row_o["project_projectkind"]] + 1;

					if($row_o["project_state"] == 2) // on hold
					{
						$number_of_projects_on_hold++;
					}

					//check if project was approved
					$sql_m = "select project_milestone_id from project_milestones " . 
						     " where project_milestone_project = " . $row_o["project_id"] .
						     " and project_milestone_milestone in (13, 21, 10, 12) " .
						     " and project_milestone_date is not null " . 
						     " and project_milestone_date != '0000-00-00' ";


					$res_m = mysql_query($sql_m) or dberror($sql_m);
					if ($row_m = mysql_fetch_assoc($res_m))
					{
						$number_of_projects_approved[$pos_type_id]++ ;
					}

				}


				//calculate planned closings
				$number_of_planned_closings[$pos_type_id] = 0;
				$sql_o = "select count(posaddress_id) as num_recs " . 
					     "from posaddresses " . 
					     " left join addresses on address_id = posaddress_client_id " . 
					     " where year(posaddress_store_planned_closingdate) = '" . $actual_year . "' " . 
					     " and posaddress_country = " . $row["country_id"] . 
					     " and posaddress_store_postype = " . $pos_type_id . 
					     " and posaddress_store_planned_closingdate > " . dbquote(date("Y-m-d"));


				if($client_type_filter)
				{
					$sql_o .= " and address_client_type " . $client_type_filter;
				}

				if($legal_type_filter)
				{
					$sql_o .= " and posaddress_ownertype " . $legal_type_filter;
				}
				
				if($pos_type_subclass_filter)
				{
					$sql_o .=  " and posaddress_store_subclass " . $pos_type_subclass_filter;
				}

				if($product_line_filter)
				{
					$sql_o .=  " and posaddress_store_furniture " . $product_line_filter;
				}

				if($product_line_subclass_filter)
				{
					$sql_o .=  " and posaddress_store_furniture_subclass " . $product_line_subclass_filter;
				}

				if($exclude_e_stores)
				{
					$sql_o .=  " and posaddress_store_subclass <> 20 ";
				}


				$res_o = mysql_query($sql_o) or dberror($sql_o);
				if ($row_o = mysql_fetch_assoc($res_o))
				{
					$number_of_planned_closings[$pos_type_id] = $row_o["num_recs"];
				}

			}
		}

	
		//check if there is any data for the line in question
		$has_data = false;
		foreach($number_of_pos_operating as $tmp_pos_type_id=>$year_array)
		{
			
			foreach($year_array as $tmp_year=>$project_kind_array)
			{
				foreach($project_kind_array as $tmp_project_kind_id=>$number_of_pos)
				{
					if($number_of_pos > 0)
					{
						$has_data = true;
					}
				}


				if($number_of_total_pos[$tmp_pos_type_id][$tmp_year] > 0)
				{
					$has_data = true;
				}

				if($number_of_pos_closed[$tmp_pos_type_id][$tmp_year] > 0)
				{
					$has_data = true;
				}
			}
		}

		if(is_array($number_of_onging_projects) 
			and array_key_exists($tmp_pos_type_id, $number_of_onging_projects)) {
			foreach($number_of_onging_projects[$tmp_pos_type_id] as $tmp2_project_kind_id=>$number_of_projects)
			{
				if($number_of_projects > 0)
				{
					$has_data = true;
				}
			}
		}


		foreach($pos_types as $key=>$value)
		{
			$pos_type_total = 0;
			foreach($legal_types as $legal_type_id=>$legal_type_name)
			{
				if(array_key_exists($sales_region_id, $total_operating_pos_locations))
				{
					if(array_key_exists($row["country_id"], $total_operating_pos_locations[$sales_region_id]))
					{
						if(array_key_exists($key, $total_operating_pos_locations[$sales_region_id][$row["country_id"]]))
						{
							
							if(array_key_exists($legal_type_id, $total_operating_pos_locations[$sales_region_id][$row["country_id"]][$key]))
							{
								if($total_operating_pos_locations[$sales_region_id][$row["country_id"]][$key][$legal_type_id] > 0)
								{
									$has_data = true;
								}
							}
						}
					}
				}
			}
		}


		//print data
		if($has_data == true)
		{
		
			if($do_not_show_country_lines != 1)
			{
				$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["country_name"]);
				if($colwidths[0] < strlen($row["country_name"])){$colwidths[0] = strlen($row["country_name"])+3;}
				$sheet->getStyleByColumnAndRow($cell_index, $row_index)->applyFromArray( $style_normal_border );
				$cell_index++;
			}
			else
			{
				$cell_index--;
			}


			foreach($number_of_pos_operating as $tmp_pos_type_id=>$year_array)
			{
				
				foreach($year_array as $tmp_year=>$project_kind_array)
				{
					foreach($project_kind_array as $tmp_project_kind_id=>$number_of_pos)
					{
						if($do_not_show_country_lines != 1)
						{
							$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $number_of_pos);
							$sheet->getStyleByColumnAndRow($cell_index, $row_index)->applyFromArray( $style_normal_border_right );
							$cell_index++;
						}
						else
						{
							$cell_index++;
						}
						
						$tmp = "o-" . $tmp_pos_type_id . "-" . $tmp_year . "- " . $tmp_project_kind_id;
						if(array_key_exists($tmp, $totals_per_sales_region))
						{
							$totals_per_sales_region[$tmp] = $totals_per_sales_region[$tmp] + $number_of_pos;
						}
						else
						{
							$totals_per_sales_region[$tmp] = $number_of_pos;
						}
						if(array_key_exists($tmp, $list_totals))
						{
							$list_totals[$tmp] = $list_totals[$tmp] + $number_of_pos;
						}
						else
						{
							$list_totals[$tmp] = $number_of_pos;
						}
					}

					if($do_not_show_country_lines != 1)
					{
						$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $number_of_total_pos[$tmp_pos_type_id][$tmp_year]);
						$sheet->getStyleByColumnAndRow($cell_index, $row_index)->applyFromArray( $style_normal_border_right );
						$cell_index++;
					}
					else
					{
						$cell_index++;
					}

					if($tmp_year == $actual_year)
					{
						$total_projects_actual_year = $number_of_total_pos[$tmp_pos_type_id][$tmp_year];
					}

					$tmp = "o-" . $tmp_pos_type_id . "-" . $tmp_year . "-total";
					if(array_key_exists($tmp, $totals_per_sales_region))
					{
						$totals_per_sales_region[$tmp] = $totals_per_sales_region[$tmp] + $number_of_total_pos[$tmp_pos_type_id][$tmp_year];
					}
					else
					{
						$totals_per_sales_region[$tmp] = $number_of_total_pos[$tmp_pos_type_id][$tmp_year];
					}

					if(array_key_exists($tmp, $list_totals))
					{
						$list_totals[$tmp] = $list_totals[$tmp] + $number_of_total_pos[$tmp_pos_type_id][$tmp_year];
					}
					else
					{
						$list_totals[$tmp] = $number_of_total_pos[$tmp_pos_type_id][$tmp_year];
					}

					if($do_not_show_country_lines != 1)
					{
						$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $number_of_pos_closed[$tmp_pos_type_id][$tmp_year]);
						$sheet->getStyleByColumnAndRow($cell_index, $row_index)->applyFromArray( $style_normal_border_right );
						$cell_index++;
					
						$sheet->getColumnDimensionByColumn($cell_index)->setWidth(4);
						$cell_index++;
					}
					else
					{
						$cell_index++;
						$cell_index++;
					}

					$tmp = "o-" . $tmp_pos_type_id . "-" . $tmp_year . "-closed";
					if(array_key_exists($tmp, $totals_per_sales_region))
					{
						$totals_per_sales_region[$tmp] = $totals_per_sales_region[$tmp] + $number_of_pos_closed[$tmp_pos_type_id][$tmp_year];
					}
					else
					{
						$totals_per_sales_region[$tmp] = $number_of_pos_closed[$tmp_pos_type_id][$tmp_year];
					}

					if(array_key_exists($tmp, $list_totals))
					{
						$list_totals[$tmp] = $list_totals[$tmp] + $number_of_pos_closed[$tmp_pos_type_id][$tmp_year];
					}
					else
					{
						$list_totals[$tmp] = $number_of_pos_closed[$tmp_pos_type_id][$tmp_year];
					}

					$tmp = "o-" . $tmp_pos_type_id . "-" . $tmp_year . "-spacer";
					$totals_per_sales_region[$tmp] = "spacer";

					$tmp = "op-" . $tmp_pos_type_id . "-" . $tmp_year . "-spacer";
					$list_totals[$tmp] = "spacer";
				}

				
				if($to_year == $actual_year)
				{
					//print onging projects
					$total_projects = 0;
					foreach($number_of_onging_projects[$tmp_pos_type_id] as $tmp2_project_kind_id=>$number_of_projects)
					{
						if($do_not_show_country_lines != 1)
						{
							$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $number_of_projects);
							$sheet->getStyleByColumnAndRow($cell_index, $row_index)->applyFromArray( $style_normal_border_right );
							$cell_index++;
						}
						else
						{
							$cell_index++;
						}
						
						$total_projects= $total_projects + $number_of_projects;

						$tmp = "p-" . $tmp_pos_type_id . "-" . $tmp_year . "- " . $tmp2_project_kind_id;
						if(array_key_exists($tmp, $totals_per_sales_region))
						{
							$totals_per_sales_region[$tmp] = $totals_per_sales_region[$tmp] + $number_of_projects;
						}
						else
						{
							$totals_per_sales_region[$tmp] = $number_of_projects;
						}

						if(array_key_exists($tmp, $list_totals))
						{
							$list_totals[$tmp] = $list_totals[$tmp] + $number_of_projects;
						}
						else
						{
							$list_totals[$tmp] = $number_of_projects;
						}
					}
					
					
					if($do_not_show_country_lines != 1)
					{
						if($total_projects == 0)
						{
							$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
						}
						else
						{
							$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_projects);
						}
						$sheet->getStyleByColumnAndRow($cell_index, $row_index)->applyFromArray( $style_normal_border_right );
						$cell_index++;

										
						
						if($agreed_opening_date_must_be_equal_to_the_last_year == 0)
						{
							if($number_of_projects_on_hold == 0)
							{
								$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
							}
							else
							{
								$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $number_of_projects_on_hold);
							}
							$sheet->getStyleByColumnAndRow($cell_index, $row_index)->applyFromArray( $style_normal_border_right );
							$cell_index++;
						}						

						if($number_of_projects_approved[$tmp_pos_type_id] == 0)
						{
							$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
						}
						else
						{
							$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $number_of_projects_approved[$tmp_pos_type_id]);
						}
						$sheet->getStyleByColumnAndRow($cell_index, $row_index)->applyFromArray( $style_normal_border_right );


						$cell_index++;
						$cell_index++;
						
					
						if($number_of_planned_closings[$tmp_pos_type_id] == 0)
						{
							$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
						}
						else
						{
							$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $number_of_planned_closings[$tmp_pos_type_id]);
						}
						$sheet->getStyleByColumnAndRow($cell_index, $row_index)->applyFromArray( $style_normal_border_right );
						$cell_index++;

						$sheet->getColumnDimensionByColumn($cell_index)->setWidth(4);
						$cell_index++;
					}
					else
					{
						$cell_index++;
						$cell_index++;
						$cell_index++;
						$cell_index++;
						$cell_index++;
						$cell_index++;
					}

					
					$total_projects_actual_year = $total_projects_actual_year + $total_projects;
					
					
					if($do_not_show_country_lines != 1)
					{
						if($total_projects_actual_year == 0)
						{
							$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
						}
						else
						{
							$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $total_projects_actual_year);
						}
						$sheet->getStyleByColumnAndRow($cell_index, $row_index)->applyFromArray( $style_normal_border_right );
						$cell_index++;
						$cell_index++;
					}
					else
					{
						$cell_index++;
						$cell_index++;
					}


					$tmp = "p-" . $tmp_pos_type_id . "-" . $tmp_year . "-total";
					if(array_key_exists($tmp, $totals_per_sales_region))
					{
						$totals_per_sales_region[$tmp] = $totals_per_sales_region[$tmp] + $total_projects;
					}
					else
					{
						$totals_per_sales_region[$tmp] = $total_projects;
					}

					if(array_key_exists($tmp, $list_totals))
					{
						$list_totals[$tmp] = $list_totals[$tmp] + $total_projects;
					}
					else
					{
						$list_totals[$tmp] = $total_projects;
					}

					
					//projects on hold
					
					if($agreed_opening_date_must_be_equal_to_the_last_year == 0)
					{
						$tmp = "p-" . $tmp_pos_type_id . "-" . $tmp_year . "-on hold";
						if(array_key_exists($tmp, $totals_per_sales_region))
						{
							$totals_per_sales_region[$tmp] = $totals_per_sales_region[$tmp] + $number_of_projects_on_hold;
						}
						else
						{
							$totals_per_sales_region[$tmp] = $number_of_projects_on_hold;
						}

						if(array_key_exists($tmp, $list_totals))
						{
							$list_totals[$tmp] = $list_totals[$tmp] + $number_of_projects_on_hold;
						}
						else
						{
							$list_totals[$tmp] = $number_of_projects_on_hold;
						}
					}


					//prjects approved
					$tmp = "p-" . $tmp_pos_type_id . "-" . $tmp_year . "-approved";
					if(array_key_exists($tmp, $totals_per_sales_region))
					{
						$totals_per_sales_region[$tmp] = $totals_per_sales_region[$tmp] + $number_of_projects_approved[$tmp_pos_type_id];
					}
					else
					{
						$totals_per_sales_region[$tmp] = $number_of_projects_approved[$tmp_pos_type_id];
					}

					if(array_key_exists($tmp, $list_totals))
					{
						$list_totals[$tmp] = $list_totals[$tmp] + $number_of_projects_approved[$tmp_pos_type_id];
					}
					else
					{
						$list_totals[$tmp] = $number_of_projects_approved[$tmp_pos_type_id];
					}


					//planned closings

					$tmp = "p-" . $tmp_pos_type_id . "-" . $tmp_year . "-spacer";
					$totals_per_sales_region[$tmp] = "spacer";

					$tmp = "p1-" . $tmp_pos_type_id . "-" . $tmp_year . "-spacer";
					$list_totals[$tmp] = "spacer";


					$tmp = "p-" . $tmp_pos_type_id . "-" . $tmp_year . "-planned_closings";
					if(array_key_exists($tmp, $totals_per_sales_region))
					{
						$totals_per_sales_region[$tmp] = $totals_per_sales_region[$tmp] + $number_of_planned_closings[$tmp_pos_type_id];
					}
					else
					{
						$totals_per_sales_region[$tmp] = $number_of_planned_closings[$tmp_pos_type_id];
					}

					if(array_key_exists($tmp, $list_totals))
					{
						$list_totals[$tmp] = $list_totals[$tmp] + $number_of_planned_closings[$tmp_pos_type_id];
					}
					else
					{
						$list_totals[$tmp] = $number_of_planned_closings[$tmp_pos_type_id];
					}
					
					//total projects in actual year

					$tmp = "p1-" . $tmp_pos_type_id . "-" . $tmp_year . "-spacer";
					$totals_per_sales_region[$tmp] = "spacer";

					$tmp = "op1-" . $tmp_pos_type_id . "-" . $tmp_year . "-spacer";
					$list_totals[$tmp] = "spacer";


					
					$tmp = "op-" . $tmp_pos_type_id . "-" . $tmp_year . "-total";
					if(array_key_exists($tmp, $totals_per_sales_region))
					{
						$totals_per_sales_region[$tmp] = $totals_per_sales_region[$tmp] + $total_projects_actual_year;
					}
					else
					{
						$totals_per_sales_region[$tmp] = $total_projects_actual_year;
					}

					$tmp = "op-" . $tmp_pos_type_id . "-" . $tmp_year . "-total";
					if(array_key_exists($tmp, $list_totals))
					{
						$list_totals[$tmp] = $list_totals[$tmp] + $total_projects_actual_year;
					}
					else
					{
						$list_totals[$tmp] = $total_projects_actual_year;
					}
					
					
					$tmp = "p2-" . $tmp_pos_type_id . "-" . $tmp_year . "-spacer";
					$totals_per_sales_region[$tmp] = "spacer";

					$tmp = "op2-" . $tmp_pos_type_id . "-" . $tmp_year . "-spacer";
					$list_totals[$tmp] = "spacer";
				}

			}



			if($to_year == $actual_year)
			{
				
				if($agreed_opening_date_must_be_equal_to_the_last_year == 0)
				{
					$group_cell_for_total_operating_pos_locations = $cell_index;
				}
				else
				{
					$group_cell_for_total_operating_pos_locations = $cell_index-1;
				}
			
				
				foreach($pos_types as $key=>$value)
				{
					$pos_type_total = 0;
					foreach($legal_types as $legal_type_id=>$legal_type_name)
					{
						$show_data = false;
						if(array_key_exists($sales_region_id, $total_operating_pos_locations))
						{
							if(array_key_exists($row["country_id"], $total_operating_pos_locations[$sales_region_id]))
							{
								if(array_key_exists($key, $total_operating_pos_locations[$sales_region_id][$row["country_id"]]))
								{
									
									if(array_key_exists($legal_type_id, $total_operating_pos_locations[$sales_region_id][$row["country_id"]][$key]))
									{
										$show_data = true;
									}
								}
							}
						}
						
						if($show_data == true)
						{
							$value = $total_operating_pos_locations[$sales_region_id][$row["country_id"]][$key][$legal_type_id];
							
						}
						else
						{
							$value = "";
						}
						
						
						if($do_not_show_country_lines != 1)
						{
							$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $value);
							$sheet->getStyleByColumnAndRow($cell_index, $row_index)->applyFromArray( $style_normal_border_right );
							$cell_index++;
						}
						else
						{
							$cell_index++;
						}
						
						$pos_type_total = $pos_type_total + $value;
					}

					
					if($do_not_show_country_lines != 1)
					{
						if($pos_type_total > 0)
						{
							$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $pos_type_total);
						}
						else
						{
							$sheet->setCellValueByColumnAndRow($cell_index, $row_index, "");
						}
						$sheet->getStyleByColumnAndRow($cell_index, $row_index)->applyFromArray( $style_normal_border_right );
						$cell_index++;
						$cell_index++;
					}
					else
					{
						$cell_index++;
						$cell_index++;
					}
				}
				

				
			}
			
			$cell_index = 0;
			
			if($do_not_show_country_lines != 1)
			{
				$row_index++;
			}
		}
	}

	//ouput group totals
	
	$group_cell = 1;
	foreach($totals_per_sales_region as $key=>$value)
	{
		if($value === "spacer")
		{
			//$sheet->setCellValueByColumnAndRow($group_cell, $group_row, "??");
			//$sheet->getStyleByColumnAndRow($group_cell, $group_row)->applyFromArray( $style_header_right );
			$group_cell++;
		}
		else
		{
			$sheet->setCellValueByColumnAndRow($group_cell, $group_row, $value);
			$sheet->getStyleByColumnAndRow($group_cell, $group_row)->applyFromArray( $style_header_right );
			$group_cell++;
		}
	}


	//print region totals for total pos locations operating
	if($to_year == $actual_year)
	{
		
		/*
		if($agreed_opening_date_must_be_equal_to_the_last_year == 1)
		{
		}
		*/
		
		$group_cell = $group_cell_for_total_operating_pos_locations;
		
		if($agreed_opening_date_must_be_equal_to_the_last_year == 1 and $do_not_show_country_lines != 1)
		{
			$group_cell = $group_cell_for_total_operating_pos_locations + 1;
		}
		

		$output_data = array();
		foreach($pos_types as $key=>$value)
		{
			foreach($legal_types as $key2=>$value)
			{
				$output_data[$key][$key2] = "";
			}
		}
		//build totals per geografical region
		if(array_key_exists($sales_region_id, $total_operating_pos_locations))
		{
			foreach($total_operating_pos_locations[$sales_region_id] as $key=>$country_values)
			{
				foreach($country_values as $key2=>$pos_type_values)
				{
					foreach($pos_type_values as $key3=>$legal_type_value)
					{
						$output_data[$key2][$key3] = $output_data[$key2][$key3]  + $legal_type_value;
					}
				}
			}
		}

		foreach($pos_types as $key=>$value)
		{
			
			$pos_type_total = 0;
			foreach($legal_types as $key2=>$value)
			{
				$sheet->setCellValueByColumnAndRow($group_cell, $group_row, $output_data[$key][$key2]);
				$sheet->getStyleByColumnAndRow($group_cell, $group_row)->applyFromArray( $style_header_right );
				$group_cell++;

				$pos_type_total = $pos_type_total + $output_data[$key][$key2];
			}
			$sheet->setCellValueByColumnAndRow($group_cell, $group_row, $pos_type_total);
			$sheet->getStyleByColumnAndRow($group_cell, $group_row)->applyFromArray( $style_header_right );
			$group_cell++;
			$group_cell++;
		}
	
	}
	//$row_index++;
	//$row_index++;
	$group_row = $row_index;



}


//ouput list totals

$sheet->setCellValueByColumnAndRow(0, $row_index, "Total");
$sheet->getStyleByColumnAndRow(0, $row_index)->applyFromArray( $style_header );

$cell_index = 1;


foreach($list_totals as $key=>$value)
{
	if($value === "spacer")
	{
		$cell_index++;
	}
	else
	{
		$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $value);
		$sheet->getStyleByColumnAndRow($cell_index, $row_index)->applyFromArray( $style_header_right );
		$cell_index++;
	}
}



//print world totals for total pos locations operating
if($to_year == $actual_year)
{

	$group_cell = $group_cell_for_total_operating_pos_locations;
	if($agreed_opening_date_must_be_equal_to_the_last_year == 1 and $do_not_show_country_lines != 1)
	{
		$group_cell = $group_cell_for_total_operating_pos_locations + 1;
	}
	
	

	$output_data = array();
	foreach($pos_types as $key=>$value)
	{
		foreach($legal_types as $key2=>$value)
		{
			$output_data[$key][$key2] = "";
		}
	}
	
	//build totals of geografical region
	foreach($total_operating_pos_locations as $region_id=>$region_values)
	{
		foreach($region_values as $key=>$country_values)
		{
			foreach($country_values as $key2=>$pos_type_values)
			{
				foreach($pos_type_values as $key3=>$legal_type_value)
				{
					$output_data[$key2][$key3] = $output_data[$key2][$key3]  + $legal_type_value;
				}
			}
		}
	}


	//$total_operating_pos_locations

	foreach($pos_types as $key=>$value)
	{
		$pos_type_total = 0;
		foreach($legal_types as $key2=>$value)
		{
			$sheet->setCellValueByColumnAndRow($group_cell, $group_row, $output_data[$key][$key2]);
			$sheet->getStyleByColumnAndRow($group_cell, $group_row)->applyFromArray( $style_header_right );
			$group_cell++;

			$pos_type_total = $pos_type_total + $output_data[$key][$key2];
		}
		
		$sheet->setCellValueByColumnAndRow($group_cell, $group_row, $pos_type_total);
		$sheet->getStyleByColumnAndRow($group_cell, $group_row)->applyFromArray( $style_header_right );
		$group_cell++;
		$group_cell++;
	}
}

//Format column heights and witdhs
foreach($colwidths as $col=>$width) {
	$sheet->getColumnDimensionByColumn($col)->setWidth($width);
}


/********************************************************************
    Activate Sheet 1
*********************************************************************/

$objPHPExcel->setActiveSheetIndex(0);
$sheet->setCellValue('A1', "");
$sheet->getStyle('A1')->applyFromArray($style_title);
$objPHPExcel->getActiveSheet()->insertNewRowBefore(1, 1);
$sheet->setCellValue('A1', "");
$sheet->getStyle('A1')->applyFromArray($style_title);


/********************************************************************
    Start output
*********************************************************************/
$filename = 'Status_Report_POS_Locations_' . date('Ymd H:i:s') . '.xls';
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
header('Pragma: ');

//send data
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
?>