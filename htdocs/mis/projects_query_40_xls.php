<?php
/********************************************************************

    projects_query_40_xls.php

    Generate Excel File for Purchase Volume Suppliers

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2017-01-23
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2017-01-23
    Version:        1.0.0

    Copyright (c) 2017, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_perform_queries");

require_once "../include/xls/Writer.php";
require_once("projects_query_filter_strings.php");

/********************************************************************
    prepare Data Needed
*********************************************************************/
if(!param("query_id"))
{
	redirect("projects_queries.php");
}


$months = array();
$months[1] = "Jan";
$months[2] = "Feb";
$months[3] = "Mar";
$months[4] = "Apr";
$months[5] = "Mai";
$months[6] = "Jun";
$months[7] = "Jul";
$months[8] = "Aug";
$months[9] = "Sep";
$months[10] = "Okt";
$months[11] = "Nov";
$months[12] = "Dez";

//check if filter is present
$query_name = "";
$print_query_filter = 0;
$sql = "select mis_query_name, mis_query_filter, mis_print_filter from mis_queries " .
	   "where mis_query_id = " . param("query_id");

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$query_name = $row["mis_query_name"];
	if($row["mis_print_filter"] == 1)
	{
		$print_query_filter = 1;
	}

	$filters = array();
	$filters = unserialize($row["mis_query_filter"]);

	foreach($filters as $key=>$value)
	{
		$supp = $filters["supp"]; // Suppliers
		$forw = $filters["forw"]; // Forwarders
		$clt = $filters["clt"]; // Client Types
		$gr = $filters["gr"]; // Geografical Regions
		$re = $filters["re"]; // Supplied Regions
		$cnt = $filters["co"]; // Countries


		$fdy = $filters["fdy"]; // Delievery Year From
		$fdm = $filters["fdm"]; // Delievery Month From
		$tdy = $filters["tdy"]; // Delievery Year To
		$tdm = $filters["tdm"]; // Delievery Month To


		$from_logsitic_state = 1*$filters["fst2"]; // order state
		$to_logsitic_state = 1*$filters["tst2"]; // order state
	}
}
else
{
	redirect("projects_queries.php");
}

$header = $query_name;

$header = "Purchase Volumes by Supplier: " . $header . " (" . date("d.m.Y G:i") . ")";

/********************************************************************
    prepare Data
*********************************************************************/
$filter = "";


$supp = substr($supp,0, strlen($supp)-1); // remove last comma
$supp = str_replace("-", ",", $supp);
if($supp) // Suppliers
{
    $filter =  " (order_item_supplier_address IN (" . $supp . "))";
	$_filter_strings["supp"] = get_filter_string("supp", $supp);
}

$forw = substr($forw,0, strlen($forw)-1); // remove last comma
$forw = str_replace("-", ",", $forw);
if($forw) // Forwarders
{
    if($filter)
	{
		$filter .=  " and (order_item_forwarder_address IN (" . $forw . "))";
	}
	else
	{
		$filter =  " (order_item_forwarder_address IN (" . $forw . "))";
	}
	$_filter_strings["forw"] = get_filter_string("forw", $forw);
}

$clt = substr($clt,0, strlen($clt)-1); // remove last comma
$clt = str_replace("-", ",", $clt);
if($clt) // client type
{
    if($filter)
	{
		$filter .=  " and (clients.address_client_type IN (" . $clt . "))";
	}
	else
	{
		$filter =  "  (clients.address_client_type IN (" . $clt . "))";
	}
	$_filter_strings["Client Types"] = get_filter_string("clt", $clt);
}

$gr = substr($gr,0, strlen($gr)-1); // remove last comma
$gr = str_replace("-", ",", $gr);
$re = substr($re,0, strlen($re)-1); // remove last comma
$re = str_replace("-", ",", $re);

if($filter) {

	if($gr and $re) 
	{
		$filter .=  " and (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	}
	elseif($gr) // geografical regions
	{
		$filter .=  " and (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	}
	elseif($re) //supplying
	{
		$filter .=  " and (country_region IN (" . $re . "))";
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	}
}
else
{
	if($gr and $re) 
	{
		$filter .=  " (country_salesregion IN (" . $gr . ") OR country_region IN (" . $re . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	}
	elseif($gr) // geografical regions
	{
		$filter .=  " (country_salesregion IN (" . $gr . "))";
		$_filter_strings["Geographical Regions"] = get_filter_string("gr", $gr);
	}
	elseif($re) //supplying
	{
		$filter .=  " (country_region IN (" . $re . "))";
		$_filter_strings["Supplied Regions"] = get_filter_string("re", $re);
	}
}

$cnt = substr($cnt,0, strlen($cnt)-1); // remove last comma
$cnt = str_replace("-", ",", $cnt);
if($cnt and $filter) // country
{
    $filter .=  " and (order_address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);

}
elseif($cnt)
{
    $filter =  " (order_address_country IN (" . $cnt . "))";
	$_filter_strings["Country"] = get_filter_string("cnt", $cnt);
}


if($filter) // ordered from
{
	if($fdy > 0 and $fdm > 0)
	{
		$tmp = "" . $fdy . "" . $fdm;
		$filter.=  " and concat(YEAR(order_item_ordered), DATE_FORMAT(order_item_ordered,'%m')) >= " . $tmp;
		$_filter_strings["Ordered from"] = $fdy . "-" . $fdm;
		$filter_for_preferred_arrival_is_set = true;
	}
	elseif($fdy > 0)
	{
		$filter.=  " and YEAR(order_item_ordered) >= " . $fdy;
		$_filter_strings["Ordered from"] = $fdy;
		$filter_for_preferred_arrival_is_set = true;
	}
	elseif($fdm > 0)
	{
		$filter.=  " and MONTH(order_item_ordered) >= " . $fdm;
		$_filter_strings["Ordered from"] = $fdm;
		$filter_for_preferred_arrival_is_set = true;
	}
}
else
{
	if($fdy > 0 and $fdm > 0)
	{
		$tmp = "" . $fdy . "" . $fdm;
		$filter.=  " concat(YEAR(order_item_ordered), DATE_FORMAT(order_item_ordered,'%m')) >= " . $tmp;
		$_filter_strings["Ordered from"] = $fdy . "-" . $fdm;;
	}
	elseif($fdy > 0)
	{
		$filter.=  " YEAR(order_item_ordered) >= " . $fdy;
		$_filter_strings["Ordered from"] = $fdy;
	}
	elseif($fdm > 0)
	{
		$filter.=  " MONTH(order_item_ordered) >= " . $fdm;
		$_filter_strings["Ordered from"] = $fdm;
	}
}

if($filter) // ordered to
{
	if($tdy > 0 and $tdm > 0)
	{
		$tmp = "" . $tdy . "" . $tdm;
		$filter.=  " and concat(YEAR(order_item_ordered), DATE_FORMAT(order_item_ordered,'%m')) <= " . $tmp;
		$_filter_strings["Ordered to"] = $tdy . "-" . $tdm;
	}
	elseif($tdy > 0)
	{
		$filter.=  " and YEAR(order_item_ordered) <= " . $tdy;
		$_filter_strings["Ordered to"] = $tdy;
	}
	elseif($tdm > 0)
	{
		$filter.=  " and MONTH(order_item_ordered) <= " . $tdm;
		$_filter_strings["Ordered to"] = $tdm;
	}
}
else
{
	if($tdy > 0 and $tdm > 0)
	{
		$tmp = "" . $tdy . "" . $tdm;
		$filter.=  "  concat(YEAR(order_item_ordered), DATE_FORMAT(order_item_ordered,'%m')) <= " . $tmp;
		$_filter_strings["Ordered to"] = $tdy . "-" . $tdm;
	}
	elseif($tdy > 0)
	{
		$filter.=  "  YEAR(order_item_ordered) <= " . $tdy;
		$_filter_strings["Ordered to"] = $tdy;
	}
	elseif($tdm > 0)
	{
		$filter.=  "  MONTH(order_item_ordered) <= " . $tdm;
		$_filter_strings["Ordered to"] = $tdm;
	}
}

if($filters["fst2"])
{
	$_filter_strings["Logistic State to"] = "From Logisitc State: " . $filters["fst2"];
}

if($filters["tst2"])
{
	$_filter_strings["Logistic State from"] = "To Order State: " . $filters["tst2"];
}

if($filter and $from_logsitic_state > 0 and $to_logsitic_state > 0) {
	$filter .= " and order_logistic_status >= " . $from_logsitic_state;
	$filter .= " and order_logistic_status <= " . $to_logsitic_state;
}
elseif($filter and $from_logsitic_state > 0) {
	$filter .= " and order_logistic_status >= " . $from_logsitic_state;
}
elseif($filter and $to_logsitic_state > 0) {
	$filter .= " and order_logistic_status <= " . $to_logsitic_state;
}
else {

	if($from_logsitic_state > 0 and $to_logsitic_state > 0) {
		$filter .= "  order_logistic_status >= " . $from_logsitic_state;
		$filter .= " and order_logistic_status <= " . $to_logsitic_state;
	}
	elseif($from_logsitic_state > 0) {
		$filter .= "  order_logistic_status >= " . $from_logsitic_state;
	}
	elseif($to_logsitic_state > 0) {
		$filter .= "  order_logistic_status <= " . $to_logsitic_state;
	}
}


$filter.= " and order_item_type < 3 ";
$filter.= " and order_address_type = 2 ";
$filter.= " and order_item_supplier_address > 0 ";


//prepare all data by supplier and item and month
$suppliers = array();
$regions = array();
$countries = array();
$purchases = array();
$first_year = 9999;
$last_year = 0;

$sql_d = "select suppliers.address_shortcut as address_shortcut, order_item_quantity, " .
		 " concat(if(item_code <>'', item_code, item_type_name), '@@@', order_item_text, '@@@', unit_name) as item_shortcut, ".
		 " YEAR(order_item_ordered) as dy, MONTH(order_item_ordered) as dm, " .
		 " region_name, country_name, currency_factor, order_item_supplier_exchange_rate, order_item_supplier_price " . 
		 " from order_items " .
		 " left join items on order_item_item = item_id ".
		 " left join item_types on order_item_type = item_type_id " .
		 " left join order_addresses on order_address_order_item = order_item_id " .
		 " left join countries on country_id = order_address_country " .
		 " left join regions on region_id = country_region " .
		 " left join addresses as suppliers on suppliers.address_id = order_item_supplier_address " .
		 " left join currencies on currency_id = order_item_supplier_currency " . 
		 " left join orders on order_id = order_item_order " .
		 " left join addresses as clients on clients.address_id = order_client_address " . 
		 " left join units on unit_id = order_item_unit_id " . 
		 " where " . $filter . 
		 " order by address_shortcut, item_shortcut, order_item_ordered";

$res = mysql_query($sql_d) or dberror($sql_d);
while ($row = mysql_fetch_assoc($res))
{ 
	
	/*list 1 suppliers by month*/
	if(array_key_exists($row['dy'], $suppliers)) {

		if(array_key_exists($row['address_shortcut'], $suppliers[$row['dy']])) {
			
			if(array_key_exists($row['item_shortcut'],$suppliers[$row['dy']][$row['address_shortcut']])) {

				if(array_key_exists($row['dm'],$suppliers[$row['dy']][$row['address_shortcut']][$row['item_shortcut']])) {
					$suppliers[$row['dy']][$row['address_shortcut']][$row['item_shortcut']][$row['dm']] = $suppliers[$row['dy']][$row['address_shortcut']][$row['item_shortcut']][$row['dm']] + $row["order_item_quantity"];
				}
				else {
					$suppliers[$row['dy']][$row['address_shortcut']][$row['item_shortcut']][$row['dm']] = $row["order_item_quantity"];
				}
			
			}
			else {
				$suppliers[$row['dy']][$row['address_shortcut']][$row['item_shortcut']][$row['dm']] = $row["order_item_quantity"];
			}

		}
		else {
			$suppliers[$row['dy']][$row['address_shortcut']][$row['item_shortcut']][$row['dm']] = $row["order_item_quantity"];
		}
		
	}
	else {
		$suppliers[$row['dy']][$row['address_shortcut']][$row['item_shortcut']][$row['dm']] = $row["order_item_quantity"];
	}

	/*list 2 suppliers by region and month*/
	if(array_key_exists($row['dy'], $regions)) {

		if(array_key_exists($row['address_shortcut'], $regions[$row['dy']])) {
			
			if(array_key_exists($row['region_name'],$regions[$row['dy']][$row['address_shortcut']])) {

				if(array_key_exists($row['item_shortcut'],$regions[$row['dy']][$row['address_shortcut']][$row['region_name']])) {
				
					if(array_key_exists($row['dm'],$regions[$row['dy']][$row['address_shortcut']][$row['region_name']][$row['item_shortcut']])) {
						$regions[$row['dy']][$row['address_shortcut']][$row['region_name']][$row['item_shortcut']][$row['dm']] = $regions[$row['dy']][$row['address_shortcut']][$row['region_name']][$row['item_shortcut']][$row['dm']] + $row["order_item_quantity"];
					}
					else {
						$regions[$row['dy']][$row['address_shortcut']][$row['region_name']][$row['item_shortcut']][$row['dm']] = $row["order_item_quantity"];
					}
				}
				else {
					$regions[$row['dy']][$row['address_shortcut']][$row['region_name']][$row['item_shortcut']][$row['dm']] = $row["order_item_quantity"];
				}
			
			}
			else {
				$regions[$row['dy']][$row['address_shortcut']][$row['region_name']][$row['item_shortcut']][$row['dm']] = $row["order_item_quantity"];
			}

		}
		else {
			$regions[$row['dy']][$row['address_shortcut']][$row['region_name']][$row['item_shortcut']][$row['dm']] = $row["order_item_quantity"];
		}
		
	}
	else {
		$regions[$row['dy']][$row['address_shortcut']][$row['region_name']][$row['item_shortcut']][$row['dm']] = $row["order_item_quantity"];
	}

	/*list 3 suppliers by country and month*/
	if(array_key_exists($row['dy'], $countries)) {

		if(array_key_exists($row['address_shortcut'], $countries[$row['dy']])) {
			
			if(array_key_exists($row['country_name'],$countries[$row['dy']][$row['address_shortcut']])) {

				if(array_key_exists($row['item_shortcut'],$countries[$row['dy']][$row['address_shortcut']][$row['country_name']])) {
				
					if(array_key_exists($row['dm'],$countries[$row['dy']][$row['address_shortcut']][$row['country_name']][$row['item_shortcut']])) {
						$countries[$row['dy']][$row['address_shortcut']][$row['country_name']][$row['item_shortcut']][$row['dm']] = $countries[$row['dy']][$row['address_shortcut']][$row['country_name']][$row['item_shortcut']][$row['dm']] + $row["order_item_quantity"];
					}
					else {
						$countries[$row['dy']][$row['address_shortcut']][$row['country_name']][$row['item_shortcut']][$row['dm']] = $row["order_item_quantity"];
					}
				}
				else {
					$countries[$row['dy']][$row['address_shortcut']][$row['country_name']][$row['item_shortcut']][$row['dm']] = $row["order_item_quantity"];
				}
			
			}
			else {
				$countries[$row['dy']][$row['address_shortcut']][$row['country_name']][$row['item_shortcut']][$row['dm']] = $row["order_item_quantity"];
			}

		}
		else {
			$countries[$row['dy']][$row['address_shortcut']][$row['country_name']][$row['item_shortcut']][$row['dm']] = $row["order_item_quantity"];
		}
		
	}
	else {
		$countries[$row['dy']][$row['address_shortcut']][$row['country_name']][$row['item_shortcut']][$row['dm']] = $row["order_item_quantity"];
	}


	/*list 4 suppliers purchases by month*/
	$price_in_chf = $row["order_item_supplier_exchange_rate"] * $row["order_item_supplier_price"] / $row["currency_factor"];
	$amount = $row["order_item_quantity"] * $price_in_chf;

	if(array_key_exists($row['dy'], $purchases)) {
		
		if(array_key_exists($row['address_shortcut'], $purchases[$row['dy']])) {
			if(array_key_exists($row['dm'],$purchases[$row['dy']][$row['address_shortcut']])) {
				$purchases[$row['dy']][$row['address_shortcut']][$row['dm']] = $purchases[$row['dy']][$row['address_shortcut']][$row['dm']] + $amount;
			}
			else {
				$purchases[$row['dy']][$row['address_shortcut']][$row['dm']] = $amount;
			}
		}
		else {
			$purchases[$row['dy']][$row['address_shortcut']][$row['dm']] = $amount;
		}
		
	}
	else {
		$purchases[$row['dy']][$row['address_shortcut']][$row['dm']] = $amount;
	}


	
	
	if($row['dy'] < $first_year) {$first_year = $row['dy'];}
	if($row['dy'] > $last_year) {$last_year = $row['dy'];}

}


foreach($regions as $year=>$company_data) {
	foreach($company_data as $company=>$region_data) {
		 ksort($region_data);
		 $regions[$year][$company] = $region_data;
	}
}

foreach($countries as $year=>$company_data) {
	foreach($company_data as $company=>$country_data) {
		 ksort($country_data);
		 $countries[$year][$company] = $country_data;
	}
}

/*
echo '<pre>';
print_r($purchases);
die;
*/


/********************************************************************
    prepare Excel Sheet
*********************************************************************/

$filename = "purchase_volume_by_suppliers" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename); 
$xls->setVersion(8);
$sheet =& $xls->addWorksheet("By month");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();


//formats
$header_row =& $xls->addFormat();
$header_row->setSize(10);
$header_row->setAlign('left');
$header_row->setBold();


$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);


$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_number_bold =& $xls->addFormat();
$f_number_bold->setSize(8);
$f_number_bold->setAlign('right');
$f_number_bold->setBorder(1);
$f_number_bold->setBold();

$f_decimal =& $xls->addFormat();
$f_decimal->setSize(8);
$f_decimal->setAlign('right');
$f_decimal->setBorder(1);
$f_decimal->setNumFormat(43);

$f_decimal_bold =& $xls->addFormat();
$f_decimal_bold->setSize(8);
$f_decimal_bold->setAlign('right');
$f_decimal_bold->setBorder(1);
$f_decimal_bold->setNumFormat(43);
$f_decimal_bold->setBold();

$f_date =& $xls->addFormat();
$f_date->setSize(8);
$f_date->setAlign('left');
$f_date->setBorder(1);
$f_date->setNumFormat('DD.MM.YYYY');

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
//$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
//$f_caption->setTextRotation(270);
$f_caption->setTextWrap();



/********************************************************************
   supplier data by item and month
*********************************************************************/
$sheet->write(0, 0, $header, $header_row);

$row_index = 2;

if($print_query_filter == 1)
{
	foreach($_filter_strings as $key=>$value)
	{
		 $sheet->write($row_index, 0,$key . ": " . $value);
		 $row_index++;
	}
}

$cell_index = 0;

foreach($suppliers as $year=>$company_data) 
{
	$cell_index = 0;
	$row_index = $row_index+2;
    
	foreach($company_data as $company=>$item_data) {
		
		$supplier_totals = array();
		$sheet->write($row_index, $cell_index, $company, $f_caption);
		$cell_index++;
		$sheet->write($row_index, $cell_index, '', $f_caption);
		$cell_index++;

		$sheet->write($row_index, $cell_index, '', $f_caption);
		$cell_index++;

		foreach($months as $m=>$mname) {
			$sheet->write($row_index, $cell_index, $year . " " . $mname, $f_caption);
			$cell_index++;
		}

		$sheet->write($row_index, $cell_index, "Total " . $year, $f_caption);
		$cell_index++;

		$row_index++;
		$cell_index = 0;

		foreach($item_data as $item=>$monthly_data)
		{
			$tmp = explode('@@@', $item);

			$item_code = $tmp[0];
			$item_name = $tmp[1];
			$unit_name = $tmp[2];
			if(strlen($item_name) > 60) {
				$item_name = substr($item_name, 0, 60) . "...";
			}
			
			
			$item_total = 0;
			$sheet->write($row_index, $cell_index, $item_code, $f_normal);
			$cell_index++;

			$sheet->write($row_index, $cell_index, $item_name, $f_normal);
			$cell_index++;

			$sheet->write($row_index, $cell_index, $unit_name, $f_normal);
			$cell_index++;

			foreach($months as $m=>$mname) {
				if(array_key_exists($m, $monthly_data)) {

					$sheet->write($row_index, $cell_index, $monthly_data[$m], $f_decimal);
					$cell_index++;

					$sk = $year . "-" . $m;
					if(array_key_exists($sk, $supplier_totals)) {
						$supplier_totals[$sk] = $supplier_totals[$sk] +$monthly_data[$m];
					}
					else {
						$supplier_totals[$sk] = $monthly_data[$m];
					}
					$item_total = $item_total + $monthly_data[$m];


				}
				else {
					$sheet->write($row_index, $cell_index, '', $f_decimal);
					$cell_index++;

					$sk = $year . "-" . $m;
					if(!array_key_exists($sk, $supplier_totals)) {
						$supplier_totals[$sk] = 0;
					}
				}
			}
			
			//output item_total
			$sheet->write($row_index, $cell_index, $item_total, $f_decimal);
			$cell_index++;
			$cell_index++;
			$cell_index++;
			$cell_index++;
				
			$cell_index = 0;
			$row_index++;
		
		}

		$cell_index = 0;
		$sheet->write($row_index, $cell_index, 'Totals', $f_caption);
		$cell_index++;
		$sheet->write($row_index, $cell_index, '', $f_caption);
		$cell_index++;
		$sheet->write($row_index, $cell_index, '', $f_caption);
		$cell_index++;
		
		$yearly_total = 0;
		foreach($supplier_totals as $sk=>$supplier_total) {
			if($supplier_total > 0) {
				$sheet->write($row_index, $cell_index, $supplier_total, $f_decimal_bold);

				$yearly_total = $yearly_total + $supplier_total;
			}
			else {
				$sheet->write($row_index, $cell_index,'', $f_decimal);
			}
			$cell_index++;
		}

		$sheet->write($row_index, $cell_index, $yearly_total, $f_decimal);
		
		$cell_index = 0;
		$row_index++;
		$row_index++;
		$row_index++;
	}

	


	

}

$sheet->setColumn(0, 0, 22);
$sheet->setColumn(1, 1, 55);
$sheet->setColumn(14, 14, 10);

/********************************************************************
    supplier data by region item and month
*********************************************************************/
$sheetnumber = 2;
$sheet =& $xls->addWorksheet('By region');

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

$xls->activesheet = 2;

$sheet->write(0, 0, $header, $header_row);

$row_index = 2;

if($print_query_filter == 1)
{
	foreach($_filter_strings as $key=>$value)
	{
		 $sheet->write($row_index, 0,$key . ": " . $value);
		 $row_index++;
	}
}

$cell_index = 0;


foreach($regions as $year=>$company_data) 
{
	$cell_index = 0;
	$row_index = $row_index+2;
    
	foreach($company_data as $company=>$region_data) {
		foreach($region_data as $region=>$item_data) {
		
			$supplier_totals = array();
			$sheet->write($row_index, $cell_index, $company . " " . $region, $f_caption);
			$cell_index++;

			$sheet->write($row_index, $cell_index, "", $f_caption);
			$cell_index++;

			$sheet->write($row_index, $cell_index, "", $f_caption);
			$cell_index++;
				
				
			foreach($months as $m=>$mname) {
				$sheet->write($row_index, $cell_index, $year . " " . $mname, $f_caption);
				$cell_index++;
			}

			$sheet->write($row_index, $cell_index, "Total " . $year, $f_caption);
			$cell_index++;

			$row_index++;
			$cell_index = 0;

			foreach($item_data as $item=>$monthly_data)
			{
				$item_total = 0;
				
				$tmp = explode('@@@', $item);

				$item_code = $tmp[0];
				$item_name = $tmp[1];
				$unit_name = $tmp[2];
				if(strlen($item_name) > 60) {
					$item_name = substr($item_name, 0, 60) . "...";
				}
				
				$sheet->write($row_index, $cell_index, $item_code, $f_normal);
				$cell_index++;

				$sheet->write($row_index, $cell_index, $item_name, $f_normal);
				$cell_index++;

				$sheet->write($row_index, $cell_index, $unit_name, $f_normal);
				$cell_index++;

				foreach($months as $m=>$mname) {
					if(array_key_exists($m, $monthly_data)) {

						$sheet->write($row_index, $cell_index, $monthly_data[$m], $f_decimal);
						$cell_index++;

						$sk = $year . "-" . $m;
						if(array_key_exists($sk, $supplier_totals)) {
							$supplier_totals[$sk] = $supplier_totals[$sk] +$monthly_data[$m];
						}
						else {
							$supplier_totals[$sk] = $monthly_data[$m];
						}
						$item_total = $item_total + $monthly_data[$m];


					}
					else {
						$sheet->write($row_index, $cell_index, '', $f_decimal);
						$cell_index++;

						$sk = $year . "-" . $m;
						if(!array_key_exists($sk, $supplier_totals)) {
							$supplier_totals[$sk] = 0;
						}
					}
				}
				
				//output item_total
				$sheet->write($row_index, $cell_index, $item_total, $f_decimal);
				$cell_index++;
				$cell_index++;
				$cell_index++;
				$cell_index++;
					
				$cell_index = 0;
				$row_index++;
			
			}

			$cell_index = 0;
			$sheet->write($row_index, $cell_index, 'Totals', $f_caption);
			$cell_index++;

			$sheet->write($row_index, $cell_index, '', $f_caption);
			$cell_index++;

			$sheet->write($row_index, $cell_index, '', $f_caption);
			$cell_index++;
			
			$yearly_total = 0;
			foreach($supplier_totals as $sk=>$supplier_total) {
				if($supplier_total > 0) {
					$sheet->write($row_index, $cell_index, $supplier_total, $f_decimal);

					$yearly_total = $yearly_total + $supplier_total;
				}
				else {
					$sheet->write($row_index, $cell_index,'', $f_decimal);
				}
				$cell_index++;
			}

			$sheet->write($row_index, $cell_index, $yearly_total, $f_decimal);
			
			$cell_index = 0;
			$row_index++;
			$row_index++;
			$row_index++;

		}			
	
	}
}

$sheet->setColumn(0, 0, 22);
$sheet->setColumn(1, 1, 55);
$sheet->setColumn(14, 14, 10);

/********************************************************************
    supplier data by country item and month
*********************************************************************/
$sheetnumber = 3;
$sheet =& $xls->addWorksheet('By country');

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

$xls->activesheet = 3;

$sheet->write(0, 0, $header, $header_row);

$row_index = 2;

if($print_query_filter == 1)
{
	foreach($_filter_strings as $key=>$value)
	{
		 $sheet->write($row_index, 0,$key . ": " . $value);
		 $row_index++;
	}
}

$cell_index = 0;


foreach($countries as $year=>$company_data) 
{
	$cell_index = 0;
	$row_index = $row_index+2;
    
	foreach($company_data as $company=>$country_data) {
		foreach($country_data as $country=>$item_data) {
		
			$supplier_totals = array();
			$sheet->write($row_index, $cell_index, $company . " " . $country, $f_caption);
			$cell_index++;

			$sheet->write($row_index, $cell_index, "", $f_caption);
			$cell_index++;

			$sheet->write($row_index, $cell_index, "", $f_caption);
			$cell_index++;
				
				
			foreach($months as $m=>$mname) {
				$sheet->write($row_index, $cell_index, $year . " " . $mname, $f_caption);
				$cell_index++;
			}

			$sheet->write($row_index, $cell_index, "Total " . $year, $f_caption);
			$cell_index++;

			$row_index++;
			$cell_index = 0;

			foreach($item_data as $item=>$monthly_data)
			{
				$item_total = 0;
				$tmp = explode('@@@', $item);

				$item_code = $tmp[0];
				$item_name = $tmp[1];
				$unit_name = $tmp[2];
				if(strlen($item_name) > 60) {
					$item_name = substr($item_name, 0, 60) . "...";
				}

				
				$sheet->write($row_index, $cell_index, $item_code, $f_normal);
				$cell_index++;

				$sheet->write($row_index, $cell_index, $item_name, $f_normal);
				$cell_index++;

				$sheet->write($row_index, $cell_index, $unit_name, $f_normal);
				$cell_index++;

				foreach($months as $m=>$mname) {
					if(array_key_exists($m, $monthly_data)) {

						$sheet->write($row_index, $cell_index, $monthly_data[$m], $f_decimal);
						$cell_index++;

						$sk = $year . "-" . $m;
						if(array_key_exists($sk, $supplier_totals)) {
							$supplier_totals[$sk] = $supplier_totals[$sk] +$monthly_data[$m];
						}
						else {
							$supplier_totals[$sk] = $monthly_data[$m];
						}
						$item_total = $item_total + $monthly_data[$m];


					}
					else {
						$sheet->write($row_index, $cell_index, '', $f_decimal);
						$cell_index++;

						$sk = $year . "-" . $m;
						if(!array_key_exists($sk, $supplier_totals)) {
							$supplier_totals[$sk] = 0;
						}
					}
				}
				
				//output item_total
				$sheet->write($row_index, $cell_index, $item_total, $f_decimal);
				$cell_index++;
				$cell_index++;
				$cell_index++;
				$cell_index++;
					
				$cell_index = 0;
				$row_index++;
			
			}

			$cell_index = 0;
			$sheet->write($row_index, $cell_index, 'Totals', $f_caption);
			$cell_index++;

			$sheet->write($row_index, $cell_index, '', $f_caption);
			$cell_index++;

			$sheet->write($row_index, $cell_index, '', $f_caption);
			$cell_index++;
			
			$yearly_total = 0;
			foreach($supplier_totals as $sk=>$supplier_total) {
				if($supplier_total > 0) {
					$sheet->write($row_index, $cell_index, $supplier_total, $f_decimal);

					$yearly_total = $yearly_total + $supplier_total;
				}
				else {
					$sheet->write($row_index, $cell_index,'', $f_decimal);
				}
				$cell_index++;
			}

			$sheet->write($row_index, $cell_index, $yearly_total, $f_decimal);
			
			$cell_index = 0;
			$row_index++;
			$row_index++;
			$row_index++;

		}			
	
	}
}

$sheet->setColumn(0, 0, 22);
$sheet->setColumn(1, 1, 55);
$sheet->setColumn(14, 14, 10);



/********************************************************************
   supplier purchase volume data by item and month
*********************************************************************/
$sheetnumber = 4;
$sheet =& $xls->addWorksheet('By purchased volume');

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

$xls->activesheet = 4;

$sheet->write(0, 0, $header, $header_row);

$row_index = 2;

if($print_query_filter == 1)
{
	foreach($_filter_strings as $key=>$value)
	{
		 $sheet->write($row_index, 0,$key . ": " . $value);
		 $row_index++;
	}
}

$cell_index = 0;

foreach($purchases as $year=>$company_data) 
{
	$cell_index = 0;
	$row_index = $row_index+2;
	

	$sheet->write($row_index, $cell_index, "Supplier" . $year, $f_caption);
	$cell_index++;

	foreach($months as $m=>$mname) {
		$sheet->write($row_index, $cell_index, $year . " " . $mname, $f_caption);
		$cell_index++;
	}

	$sheet->write($row_index, $cell_index, "Total " . $year, $f_caption);
    $row_index++;
	$cell_index = 0;

	$list_totals = array();
	foreach($company_data as $company=>$monthly_data) {
		$sheet->write($row_index, $cell_index, $company . " (CHF)", $f_normal);
		$cell_index++;
		
		$supplier_total = 0;
		foreach($months as $m=>$mname) {
			if(array_key_exists($m, $monthly_data)) {

				$sheet->write($row_index, $cell_index, $monthly_data[$m], $f_decimal);
				$cell_index++;

				$sk = $year . "-" . $m;
				if(array_key_exists($sk, $list_totals)) {
					$list_totals[$sk] = $list_totals[$sk] +$monthly_data[$m];
				}
				else {
					$list_totals[$sk] = $monthly_data[$m];
				}
				$supplier_total = $supplier_total + $monthly_data[$m];


			}
			else {
				$sheet->write($row_index, $cell_index, '', $f_decimal);
				$cell_index++;

				$sk = $year . "-" . $m;
				if(!array_key_exists($sk, $list_totals)) {
					$list_totals[$sk] = 0;
				}
			}
		}
		
		//output item_total
		$sheet->write($row_index, $cell_index, $supplier_total, $f_decimal);
		
		$cell_index = 0;
		$row_index++;

	}

	$cell_index = 0;
	$sheet->write($row_index, $cell_index, 'Totals CHF', $f_caption);
	$cell_index++;
	
	$yearly_total = 0;
	foreach($list_totals as $sk=>$list_total) {
		if($list_total > 0) {
			$sheet->write($row_index, $cell_index, $list_total, $f_decimal_bold);

			$yearly_total = $yearly_total + $list_total;
		}
		else {
			$sheet->write($row_index, $cell_index,'', $f_decimal_bold);
		}
		$cell_index++;
	}

	$sheet->write($row_index, $cell_index, $yearly_total, $f_decimal_bold);
	
	$cell_index = 0;
	$row_index++;
	$row_index++;
	$row_index++;

	


	

}

$sheet->setColumn(0, 0, 30);
$sheet->setColumn(1, 12, 10);
$sheet->setColumn(13, 13, 10);


$xls->activesheet = 1;

$xls->close(); 

?>