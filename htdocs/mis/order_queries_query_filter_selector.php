<?php
/********************************************************************

    order_queries_query_filter_selector.php

    Enter Filter Criteria for the Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2016-08-19
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2016-08-19
    Version:        1.0.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../include/page_modal.php";

if(!has_access("can_perform_all_queries") 
  and !has_access("can_perform_projects_query"))
{
	redirect("noaccess.php");
}
require_once "include/orders_query_check_access.php";

require_once "include/orders_query_get_functions.php";

/********************************************************************
    prepare all data needed
*********************************************************************/
$query_id = param("query_id");

//get user_country access
$restricted_countries = array();
$sql = "select * from country_access " .
	   "where country_access_user = " . user_id();
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{            
	$restricted_countries[] = $row["country_access_country"];
}



if(count($restricted_countries) > 0)
{

	$sql_salesregion = "select DISTINCT salesregion_id, salesregion_name ".
		               "from countries " .
		               "left join salesregions on salesregion_id = country_salesregion " . 
		               "where country_id IN (" . implode(",", $restricted_countries) . ") " . 
					   "order by salesregion_name";

	$sql_region = "select DISTINCT region_id, region_name ".
		          "from countries " .
		          "left join regions on region_id = country_region " . 
		          "where country_id IN (" . implode(",", $restricted_countries) . ") " . 
				  "order by region_name";
}
else
{
	$sql_salesregion = "select salesregion_id, salesregion_name ".
						"from salesregions order by salesregion_name";

	$sql_region = "select region_id, region_name ".
						"from regions order by region_name";
}






$filter = array();
//get benchmark parameters
$salesregions = array();
$regions = array();


$filter = get_query_filter($query_id);



$sql_client_types = "select client_type_id, client_type_code ".
					"from client_types";


$tmp_filter = "";
if(count($filter["ct"]) > 0)
{
	$clienttypes = explode("-", $filter["ct"]);
	foreach($clienttypes as $key=>$clienttype)
	{
		$tmp_filter = $tmp_filter . $clienttype . ",";
	}
	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

	if($tmp_filter)
	{
		$tmp_filter = "where client_type_id in (" . $tmp_filter . ") ";
	}	
	
}


$tmp_filter_re = "";
if(count($filter["re"]) > 0)
{
	$salesregions = explode("-", $filter["re"]);
	foreach($salesregions as $key=>$salesregion)
	{
		$tmp_filter_re = $tmp_filter_re . $salesregion . ",";
	}
	$tmp_filter_re = substr($tmp_filter_re, 0, strlen($tmp_filter_re) - 2);

	if($tmp_filter_re)
	{
		$tmp_filter_re = " country_salesregion in (" . $tmp_filter_re . ") ";
	}	
	
}


$tmp_filter_gr = "";
if(count($filter["gr"]) > 0)
{
	$regions = explode("-", $filter["gr"]);
	foreach($regions as $key=>$region)
	{
		$tmp_filter_gr = $tmp_filter_gr . $region . ",";
	}
	$tmp_filter_gr = substr($tmp_filter_gr, 0, strlen($tmp_filter_gr) - 2);

	if($tmp_filter_gr)
	{
		$tmp_filter_gr = " country_region in (" . $tmp_filter_gr . ") ";
	}
}


$tmp_filter = "";
if($tmp_filter_re and $tmp_filter_gr)
{
	$tmp_filter = "where " . $tmp_filter_re . " and " . $tmp_filter_gr; 
}
elseif($tmp_filter_re)
{
	$tmp_filter = "where " . $tmp_filter_re;
}
elseif($tmp_filter_gr)
{
	$tmp_filter = "where " . $tmp_filter_gr;
}

if($tmp_filter and count($restricted_countries) > 0)
{
	$tmp_filter .= " and country_id IN(" . implode(",", $restricted_countries) . ") ";
}
elseif(count($restricted_countries) > 0)
{
	$tmp_filter = "where country_id IN(" . implode(",", $restricted_countries) . ") ";
}



if($tmp_filter)
{
	$tmp_filter .= " and country_id > 0 ";
}
else
{
	$tmp_filter = " where country_id > 0 ";
}

$sql_country = "select DISTINCT country_id, country_name " .
			   "from orders " .
			   "left join countries on country_id = order_shop_address_country " .
			   $tmp_filter . 
			   " order by country_name";



$tmp_filter = "";
$tmp_filter1 = "";
$provice_country_filter = "";
if(count($filter["co"]) > 0)
{
	$countries = explode("-", $filter["co"]);
	
	foreach($countries as $key=>$country)
	{
		$tmp_filter = $tmp_filter . $country . ",";
	}

	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);
	
	$provice_country_filter = $tmp_filter;

	if($tmp_filter)
	{
		$tmp_filter = "where country_salesregion in (" .  $tmp_filter . ") ";
	}
	elseif(count($filter["re"]) > 0 and count($filter["gr"]) > 0)
	{
		$salesregions = explode("-", $filter["re"]);
		
		foreach($salesregions as $key=>$salesregion)
		{
			$tmp_filter = $tmp_filter . $salesregion . ",";
		}

		$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

		if($tmp_filter)
		{
			$tmp_filter = "where country_salesregion in (" .  $tmp_filter . ") ";
		}

		
		
		$regions = explode("-", $filter["gr"]);
		
		foreach($regions as $key=>$region)
		{
			$tmp_filter1 = $tmp_filter1 . $region . ",";
		}

		$tmp_filter1 = substr($tmp_filter1, 0, strlen($tmp_filter1) - 2);

		if($tmp_filter and $tmp_filter1)
		{
			$tmp_filter .= " and country_region in (" .  $tmp_filter . ") ";
		}
		elseif($tmp_filter1)
		{
			$tmp_filter = "where country_region in (" .  $tmp_filter . ") ";
		}
	}
	elseif(count($filter["re"]) > 0)
	{
		$salesregions = explode("-", $filter["re"]);
		
		foreach($salesregions as $key=>$salesregion)
		{
			$tmp_filter = $tmp_filter . $salesregion . ",";
		}

		$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

		if($tmp_filter)
		{
			$tmp_filter = "where country_salesregion in (" .  $tmp_filter . ") ";
		}
	}
	elseif(count($filter["gr"]) > 0)
	{
		$regions = explode("-", $filter["gr"]);
		
		foreach($regions as $key=>$region)
		{
			$tmp_filter = $tmp_filter . $region . ",";
		}

		$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

		if($tmp_filter)
		{
			$tmp_filter = "where country_region in (" .  $tmp_filter . ") ";
		}
	}
}



$sql_cities = "select DISTINCT place_id, place_name " .
			  "from places " .
			  "left join countries on country_id = place_country " .
			  $tmp_filter . 
			  " order by place_name";



$sql_project_cost_types = "select project_costtype_id, project_costtype_text ".
						  "from project_costtypes " .
                          "order by project_costtype_text";

$sql_product_lines = "select product_line_id, product_line_name ".
                     "from product_lines " . 
                     "where product_line_posindex = 1 " . 
                     "   order by product_line_name";




$tmp_filter = "";
if(count($filter["pl"]) > 0)
{
	$product_lines = explode("-", $filter["pl"]);
	$tmp_filter = "";
	foreach($product_lines as $key=>$product_line)
	{
		$tmp_filter = $tmp_filter . $product_line . ",";
	}
	$tmp_filter = substr($tmp_filter, 0, strlen($tmp_filter) - 2);

	if($tmp_filter)
	{
		$tmp_filter = "where productline_subclass_productline in (" . $tmp_filter . ") ";
	}
}


$sql_pos_types = "select DISTINCT postype_id, postype_name ".
                     "from postypes " .
                     "order by postype_name";


$sql_project_kinds = "select projectkind_id, projectkind_name ".
						 "from projectkinds " .
						 "where projectkind_id > 0 " .
						 "order by projectkind_name";



$item_categories = array();
$sql_item_categories = "select item_category_id, item_category_name " . 
                             "from item_categories " . 
							 " order by item_category_name";





$supplier_filter = '';
if($filter["supp"] > 0)
{
	$filter_items .= " and supplier_address = " . $filter["supp"];

	$supplier_filter = " and supplier_address = " . $filter["supp"];
}




$tmp_filter = "";
if($supplier_filter)
{
	$tmp_filter .= $supplier_filter;
}

$sql_item_category = "select DISTINCT item_category_id, item_category_name " . 
					 "from items  " . 
					 "left join suppliers on supplier_item = item_id ".
					 " left join item_categories on item_category_id = item_category " . 
					 "where item_type < 3 and item_category_id > 0 " . 
					 $tmp_filter . 
					 " order by item_code";



/********************************************************************
    save data
*********************************************************************/
if(param("save_form") != '')
{
	$new_filter = array();


	$new_filter["cl"] =  $filter["cl"]; //client
	$new_filter["supp"] =  $filter["supp"]; //supplier
	$new_filter["forw"] =  $filter["forw"]; //forwarder
	$new_filter["sufrom"] =  $filter["sufrom"]; // submitted from
	$new_filter["suto"] =  $filter["suto"]; // submitted to
	$new_filter["ct"] =  $filter["ct"]; //client types
	$new_filter["re"] =  $filter["re"]; //geografical region
	$new_filter["gr"] =  $filter["gr"]; // Supplied region
	$new_filter["co"] =  $filter["co"]; // country
	$new_filter["pct"] =  $filter["pct"]; // Legal Type
	$new_filter["pk"] =  $filter["pk"]; // project Type
	$new_filter["pt"] =  $filter["pt"]; // POS Type
	$new_filter["icat"] =  $filter["icat"]; // Item Categories
	

	if(param("save_form") == "ct") // Client Types
	{
		$CT = "";
		$res = mysql_query($sql_client_types) or dberror($sql_client_types);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["CT_" . $row["client_type_id"]])
			{
				$CT .=$row["client_type_id"] . "-";
			}
		}
		
		$new_filter["ct"] = $CT;
	}	
	elseif(param("save_form") == "re") // Geographical Regions
	{
		$RE = "";

		$res = mysql_query($sql_salesregion) or dberror($sql_salesregion);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["RE_" . $row["salesregion_id"]])
			{
				$RE .=$row["salesregion_id"] . "-";
			}
		}
		
		$new_filter["re"] = $RE;
	}
	elseif(param("save_form") == "gr") //Supplied Regions
	{
		$GR = "";

		$res = mysql_query($sql_region) or dberror($sql_region);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["GR_" . $row["region_id"]])
			{
				$GR .=$row["region_id"] . "-";
			}
		}
		
		$new_filter["gr"] = $GR;
	}
	elseif(param("save_form") == "co") //Countries
	{
		$CO = "";

		$res = mysql_query($sql_country) or dberror($sql_country);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["CO_" . $row["country_id"]])
			{
				$CO .=$row["country_id"] . "-";
			}
		}
		
		$new_filter["co"] = $CO;
	}
	elseif(param("save_form") == "pct") //project cost types
	{
		$PCT = "";

		$res = mysql_query($sql_project_cost_types) or dberror($sql_project_cost_types);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PCT_" . $row["project_costtype_id"]])
			{
				$PCT .=$row["project_costtype_id"] . "-";
			}
		}
		$new_filter["pct"] = $PCT;
	}
	elseif(param("save_form") == "pk") //Product Kinds
	{
		$PK = "";

		$res = mysql_query($sql_project_kinds) or dberror($sql_project_kinds);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PK_" . $row["projectkind_id"]])
			{
				$PK .=$row["projectkind_id"] . "-";
			}
		}
		
		$new_filter["pk"] = $PK;
	}
	elseif(param("save_form") == "pt") //POS Types
	{
		$PT = "";

		$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["PT_" . str_replace(" ", "", $row["postype_id"])])
			{
				$PT .=$row["postype_id"] . "-";
			}
		}
		
		$new_filter["pt"] = $PT;
	}
	elseif(param("save_form") == "icat") //Item Categories
	{
		$ICAT = "";

		$res = mysql_query($sql_item_categories) or dberror($sql_item_categories);
		while($row = mysql_fetch_assoc($res))
		{
			if($_POST["ICAT_" . $row["item_category_id"]])
			{
				$ICAT .=$row["item_category_id"] . "-";
			}
		}
		
		$new_filter["icat"] = $ICAT;
	}
	$sql = "update orderqueries " . 
		   "set orderquery_filter = " . dbquote(serialize($new_filter)) . 
		   " where orderquery_id = " . param("query_id");

	$result = mysql_query($sql) or dberror($sql);
	
}

/********************************************************************
    get existant filter
*********************************************************************/
$filter = array();
//get benchmark parameters
$salesregions = array();

$sql = "select orderquery_filter from orderqueries " .
	   "where orderquery_id = " . $query_id;

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{

	$filter = unserialize($row["orderquery_filter"]);
	$clienttypes = explode("-", $filter["ct"]);
	$salesregions = explode("-", $filter["re"]);
	$regions = explode("-", $filter["gr"]);
	$countries = explode("-", $filter["co"]);
	$project_cost_types = explode("-", $filter["pct"]);
	$pos_types = explode("-", $filter["pt"]);
	$project_kinds = explode("-", $filter["pk"]);
	$item_categories = explode("-", $filter["icat"]);
}


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new form("cer_benchmarks", "benchmark_selector");
$form->add_hidden("query_id", $query_id);


if(param("s") == "ct") // Client Types
{
	$page_title = "Client Type Selector";
	$form->add_hidden("save_form", "ct");

	$res = mysql_query($sql_client_types) or dberror($sql_client_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["client_type_id"], $clienttypes))
		{
			$form->add_checkbox("CT_" . $row["client_type_id"], $row["client_type_code"], true);
		}
		else
		{
			$form->add_checkbox("CT_" . $row["client_type_id"], $row["client_type_code"], false);
		}
		
	}
}
elseif(param("s") == "re") // Geographical regions
{
	$page_title = "Geographical Region Selector";
	$form->add_hidden("save_form", "re");

	$res = mysql_query($sql_salesregion) or dberror($sql_salesregion);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["salesregion_id"], $salesregions))
		{
			$form->add_checkbox("RE_" . $row["salesregion_id"], $row["salesregion_name"], true);
		}
		else
		{
			$form->add_checkbox("RE_" . $row["salesregion_id"], $row["salesregion_name"], false);
		}
		
	}
}
elseif(param("s") == "gr") // Supplied regions
{
	$page_title = "Supplied Region Selector";
	$form->add_hidden("save_form", "gr");

	$res = mysql_query($sql_region) or dberror($sql_region);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["region_id"], $regions))
		{
			$form->add_checkbox("GR_" . $row["region_id"], $row["region_name"], true);
		}
		else
		{
			$form->add_checkbox("GR_" . $row["region_id"], $row["region_name"], false);
		}
		
	}
}
elseif(param("s") == "co") // countries
{
	$page_title = "Country Selector";
	$form->add_hidden("save_form", "co");

	$res = mysql_query($sql_country) or dberror($sql_country);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["country_id"], $countries))
		{
			$form->add_checkbox("CO_" . $row["country_id"], $row["country_name"], true);
		}
		else
		{
			$form->add_checkbox("CO_" . $row["country_id"], $row["country_name"], false);
		}
		
	}
}
elseif(param("s") == "pct") // Project Cost Types
{
	$page_title = "Legal Type Selector";
	$form->add_hidden("save_form", "pct");

	$res = mysql_query($sql_project_cost_types) or dberror($sql_project_cost_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["project_costtype_id"], $project_cost_types))
		{
			$form->add_checkbox("PCT_" . $row["project_costtype_id"], $row["project_costtype_text"], true);
		}
		else
		{
			$form->add_checkbox("PCT_" . $row["project_costtype_id"], $row["project_costtype_text"], false);
		}
		
	}
}
elseif(param("s") == "pt") // POS Types
{
	$page_title = "POS Type Selector";
	$form->add_hidden("save_form", "pt");

	$res = mysql_query($sql_pos_types) or dberror($sql_pos_types);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["postype_id"], $pos_types))
		{
			$form->add_checkbox("PT_" . str_replace(" ", "", $row["postype_id"]), $row["postype_name"], true);
		}
		else
		{
			$form->add_checkbox("PT_" . str_replace(" ", "", $row["postype_id"]), $row["postype_name"], false);
		}
	}
}
elseif(param("s") == "pk") // Project Types
{
	$page_title = "Project Type Selector";
	$form->add_hidden("save_form", "pk");

	$res = mysql_query($sql_project_kinds) or dberror($sql_project_kinds);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["projectkind_id"], $project_kinds))
		{
			$form->add_checkbox("PK_" . $row["projectkind_id"], $row["projectkind_name"], true);
		}
		else
		{
			$form->add_checkbox("PK_" . $row["projectkind_id"], $row["projectkind_name"], false);
		}
		
	}
}
elseif(param("s") == "icat") // Item Categorie
{
	$page_title = "Item Category Selector";
	$form->add_hidden("save_form", "icat");

	$res = mysql_query($sql_item_categories) or dberror($sql_item_categories);
	while($row = mysql_fetch_assoc($res))
	{
		if(in_array($row["item_category_id"], $item_categories))
		{
			$form->add_checkbox("ICAT_" . $row["item_category_id"], $row["item_category_name"], true);
		}
		else
		{
			$form->add_checkbox("ICAT_" . $row["item_category_id"], $row["item_category_name"], false);
		}
		
	}
}
$form->add_input_submit("submit", "Save Selection", 0);


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Process buttons
*********************************************************************/ 

/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page_Modal("projects");

$page->header();
$page->title($page_title);

$form->render();

if(param("save_form") != '')
{
?>

<script languege="javascript">
var back_link = "order_queries_query_filter.php?query_id=<?php echo $query_id;?>"; 
$.nyroModalRemove();
</script>

<?php
}
$page->footer();
