<?php
/********************************************************************

    order_queries_query.php

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2016-08-19
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2016-08-19
    Version:        1.0.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

if(!has_access("can_perform_all_queries") 
  and !has_access("can_perform_projects_query"))
{
	redirect("noaccess.php");
}
require_once "include/orders_query_check_access.php";

require_once "include/orders_query_get_functions.php";




if(param("copy_id"))
{
	
	$sql = "select * from orderqueries where orderquery_id = " . param("copy_id");
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		
		$sql = "INSERT INTO orderqueries (". 
			   "orderquery_owner, orderquery_group, orderquery_name, orderquery_print_filter, " . 
			   "orderquery_fields, orderquery_field_order, orderquery_filter, orderquery_order, " . "orderquery_grouping01, orderquery_grouping02, orderquery_show_number_of_orders, " . "orderquery_sum_fields, orderquery_avg_fields, " . 
			   "orderquery_area_perception, orderquery_milestone_filter, " . 
			   "user_created, date_created) ". 
			   "VALUES (" . 
			   dbquote(user_id()) . ", " .
			   dbquote($row["orderquery_group"]) . ", " .
			   dbquote($row["orderquery_name"]. " - copy") . ", " .
			   dbquote($row["orderquery_print_filter"]) . ", " .
			   dbquote($row["orderquery_fields"]) . ", " .
			   dbquote($row["orderquery_field_order"]) . ", " .
			   dbquote($row["orderquery_filter"]) . ", " .
			   dbquote($row["orderquery_order"]) . ", " .
			   dbquote($row["orderquery_grouping01"]) . ", " .
			   dbquote($row["orderquery_grouping02"]) . ", " .
			   dbquote($row["orderquery_show_number_of_projects"]) . ", " .
			   dbquote($row["orderquery_sum_fields"]) . ", " .
			   dbquote($row["orderquery_avg_fields"]) . ", " .
			   dbquote($row["orderquery_area_perception"]) . ", " .
			   dbquote($row["orderquery_milestone_filter"]) . ", " .
			   dbquote(user_login()) . ", " .
			   dbquote(date("Y-m-d")) . ")";

			   
						
		$result = mysql_query($sql) or dberror($sql);
		$new_id = mysql_insert_id();
		redirect("order_queries_query.php?query_id=" . $new_id);

	}
}

			

if(param("query_id"))
{
	param("id", param("query_id"));
	$query_id = param("query_id");
}
if(id())
{
	param("query_id",id());
	$query_id = id();
}

$user = get_user(user_id());

//update filter in case elements are missing
if(isset($query_id))
{
	$filter = get_query_filter($query_id);

	$new_filter["cl"] =  $filter["cl"]; //client
	$new_filter["supp"] =  $filter["supp"]; //supplier
	$new_filter["forw"] =  $filter["forw"]; //forwarder
	$new_filter["sufrom"] =  $filter["sufrom"]; // submitted from
	$new_filter["suto"] =  $filter["suto"]; // subnmitted to
	$new_filter["ct"] =  $filter["ct"]; //client type
	$new_filter["re"] =  $filter["re"]; //geografical region
	$new_filter["gr"] =  $filter["gr"]; // Supplied region
	$new_filter["co"] =  $filter["co"]; // country
	$new_filter["pct"] =  $filter["pct"]; // Legal Type
	$new_filter["pk"] =  $filter["pk"]; // project Type
	$new_filter["pt"] =  $filter["pt"]; // POS Type
	$new_filter["icat"] =  $filter["icat"]; // Item Categories
	
	$sql = "update posqueries " . 
			   "set posquery_filter = " . dbquote(serialize($new_filter)) . 
			   " where posquery_id = " . param("query_id");

	$result = mysql_query($sql) or dberror($sql);
}

//prepare data
$access_roles = array();
$sql_access_roles = "select role_id from roles where role_include_in_queryselection_posindex = 1";
$res = mysql_query($sql_access_roles) or dberror($sql_access_roles);
while ($row = mysql_fetch_assoc($res))
{
	$access_roles[] = $row["role_id"];
}



if(count($access_roles) > 0)
{
	$sql_persons_hq = "select distinct user_id, concat(user_name, ' ' , user_firstname, '(', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " . 
		   "where user_id = 2 " . 
		   "or (address_id = 13 and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and (user_role_role IN (" . implode(",", $access_roles). ") or user_address = " . $user["address"] . "))" .
		   " or (address_type = 7 and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and (user_role_role IN (" . implode(",", $access_roles). ") or user_address = " . $user["address"] . "))" . 
		   "order by username";

	$sql_persons_sub = "select distinct user_id, " . 
		   "concat(country_name, ' - ', user_name, ' ' , user_firstname, '(', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " .
		   " left join countries on country_id = address_country " . 
		   "where address_id <> 13 and address_client_type in (2, 3) and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and (user_role_role IN (" . implode(",", $access_roles). ") or user_address = " . $user["address"] . ") " . 
		   "order by country_name, username";

	$sql_persons_ag = "select distinct user_id, " . 
		   "concat(country_name, ' - ', user_name, ' ' , user_firstname, '(', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " .
		    " left join countries on country_id = address_country " .  
		   "where address_id <> 13 and address_client_type = 1 and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and (user_role_role IN (" . implode(",", $access_roles). ") or user_address = " . $user["address"] . ") " . 
		   "order by country_name, username";
}
else
{
	$sql_persons_hq = "select distinct user_id, concat(user_name, ' ' , user_firstname, ' (', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " .
		   "where user_id = 2 or (address_id = 13  and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and user_address = " . $user["address"] . ") " . 
		   "order by username";

	$sql_persons_sub = "select distinct user_id, " . 
		   "concat(country_name, ' - ', user_name, ' ' , user_firstname, ' (', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " .
		   " left join countries on country_id = address_country " . 
		   "where address_id <> 13 and address_client_type in (2, 3) and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and user_address = " . $user["address"] . " " . 
		   "order by country_name, username";

	$sql_persons_ag = "select distinct user_id, " . 
		   "concat(country_name, ' - ', user_name, ' ' , user_firstname, ' (', address_company, ')') as username " . 
		   "from users " . 
		   "left join user_roles on user_role_user = user_id  " .
		   " left join addresses on address_id = user_address " .
		   " left join countries on country_id = address_country " . 
		   "where address_id <> 13 and address_client_type = 1 and user_id <> " . user_id() . " and user_password <> '' and user_active = 1 " . 
		   " and user_address = " . $user["address"] . " " . 
		   "order by country_name, username";
}


$qgroups = array();
$query_groups = "select DISTINCT orderquery_id, orderquery_group " . 
       "from orderqueries " . 
	   " where orderquery_group <> '' and orderquery_owner = " . user_id() . 
	   " order by orderquery_group";
$res = mysql_query($query_groups) or dberror($query_groups);
while ($row = mysql_fetch_assoc($res))
{
	if(!in_array($row["orderquery_group"], $qgroups))
	{
		$qgroups[$row["orderquery_id"]] = $row["orderquery_group"];
	}
}



$form = new Form("orderqueries", "Query");

$form->add_hidden("query_id", param("query_id"));

$form->add_edit("orderquery_name", "Name*", NOTNULL);

$form->add_checkbox("orderquery_print_filter", "Print Query Filter Information in Excel Sheet", "", "", "Filter");


$form->add_section("Group of Queries");
$form->add_comment("Please select to which group the query belongs or enter a new group.");
$form->add_list("existing_groups", "Existing Groups", $qgroups, SUBMIT, "");
$form->add_edit("orderquery_group", "Group");


$form->add_section("Access Rights");
$form->add_comment("The following persons can have access to my queries");

$form->add_checklist("Persons0", "Persons HQ", "orderquery_permissions",
    $sql_persons_hq, 0, "", true);

$form->add_checklist("Persons1", "Subs and Affiliates", "orderquery_permissions",
    $sql_persons_sub, 0, "", true);

$form->add_checklist("Persons2", "Agents", "orderquery_permissions",
    $sql_persons_ag, 0, "", true);


$form->add_hidden("orderquery_owner", user_id());



$form->add_button("save", "Save Query");
if(param("query_id") > 0)
{
	$form->add_button("copy_query", "Copy Query");
	$form->add_button("delete", "Delete Query");
}
$form->add_button(FORM_BUTTON_BACK, "Back to the List of Queries");

$form->populate();
$form->process();


if($form->button("existing_groups"))
{
	$form->value("orderquery_group", $qgroups[$form->value("existing_groups")]);
	$form->value("existing_groups", "");
}
elseif($form->button("save"))
{
	if($form->validate())
	{
		$form->save();

		

		if(!param("query_id")) // new record
		{
			//create fields
			$fields = array();

			$fields["cl"] = array(); // client
			$fields["ord"] = array(); // Project
			
			$sql_u = "update orderqueries " . 
					 "set orderquery_fields = " . dbquote(serialize($fields)) . 
					 " where orderquery_id = " . id();

			$result = mysql_query($sql_u) or dberror($sql_u);

			//create filter
			$filter = array();
			
			$filter["cl"] =  ""; //client address
			$filter["supp"] =  ""; //client address
			$filter["forw"] =  ""; //client address
			$filter["sufrom"] =  ""; // submitted from
			$filter["suto"] =  ""; // submitted to
			$filter["ct"] =  ""; //Client type
			$filter["re"] =  ""; //Geographical region
			$filter["gr"] =  ""; //Supplied region
			$filter["co"] =  ""; // country
			$filter["pct"] =  ""; //cost type, legal type
			$filter["pk"] =  ""; //project Type
			$filter["pt"] =  ""; // pos type
			$filter["icat"] =  ""; //item categories


			//check if user can select Geographical region and country
			// and set filter
			if(!has_access("has_access_to_all_projects"))
			{
				$predefined_filter = user_predefined_filter(user_id());
				$filter["re"] =  $predefined_filter["re"];
				$filter["gr"] =  $predefined_filter["gr"];
				$filter["co"] =  $predefined_filter["co"];
			}

			$sql_u = "update orderqueries " . 
				     "set orderquery_filter = " . dbquote(serialize($filter)) . 
				     " where orderquery_id = " . id();

			$result = mysql_query($sql_u) or dberror($sql_u);

			$sum_fields = array();
			$sql_u = "update orderqueries " . 
				     "set orderquery_sum_fields = " . dbquote(serialize($sum_fields)) . 
				     " where orderquery_id = " . id();


			$avg_fields = array();
			$sql_u = "update orderqueries " . 
				     "set orderquery_avg_fields = " . dbquote(serialize($avg_fields)) . 
				     " where orderquery_id = " . id();

			$result = mysql_query($sql_u) or dberror($sql_u);
		}


		redirect("order_queries_query.php?query_id=" .  id());
	}
}
elseif($form->button("delete"))
{
	$sql = "delete from orderquery_permissions " . 
		   " where orderquery_permission_query = " . id();
	$result = mysql_query($sql) or dberror($sql);

	$sql = "delete from orderqueries " . 
		   " where orderquery_id = " . id();
	$result = mysql_query($sql) or dberror($sql);

	redirect("order_queries.php");


}

$page = new Page("queries");

require_once("include/mis_page_actions.php");

$page->header();
$page->title(id() ? "Edit Order Query - Name" : "Add Order Query");

if(id() > 0 or param("query_id") > 0)
{
	require_once("include/orders_query_tabs.php");
}

$form->render();
$page->footer();

?>