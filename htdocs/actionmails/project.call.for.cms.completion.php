<?php 
/**
 * This is the actionmail file for mails that call for cms completion
 * It loads the project and order data the mail needs and adds a few
 * roles as (cc) recipients if so configured in the mail template.
 * Sent mails are tracked and tasks created for the recipient.
 */

$auth = User::instance();
$request = Request::instance();
$db = Connector::get();

$_TPL = $this->getTemplateId();
$_PROJECT_ID = $this->getParam('projectID');
$_RECIPIENT = $this->getParam('recipient');
$_RECIPIENT_ROLE = $this->getParam('recipientRole');

// sender
$sender = $this->getSender();

// template roles
$_TEMPLATE_ROLES = $this->getRoles() ?: array();
$_TEMPLATE_CC_ROLES = $this->getRoles(true) ?: array();

// project
$Project = new Project();
$Project->read($_PROJECT_ID);

if (!$Project->id) {
	$this->console('Project $_PROJECT_ID not found');
	return;
}

// project order
$Order = $Project->order();

// project cost
$projectCosts = $Order->getProjectCosts();

// project country
$ShopCountry = $Project->getCountry();

// brand manager
$BrandManager = $Order->getBrandManager();

// regional sales manager
$RegionalSalesManager = $Order->getRegionalSalesManager();

// project leader
$ProjectLeader = $Project->retail_coordinator;

// assemble mail
$this->setDataloader(array(
	'order_number' => $Project->number,
	'order_shop_address_country_name' => $ShopCountry->name,
	'address_company' => $Order->shop_address_company,
	'link' => URLBuilder::editCMS($Project->id),
));

$sth = $db->prepare("
	SELECT DISTINCT order_mail_user AS recipinet
	FROM order_mails
	WHERE order_mail_order = ? AND order_mail_template_id = ?
");

// get tracks fro order mails ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth->execute(array($Order->id, $_TPL));
$result = $sth->fetchAll();
$_ORDER_RECIPIENTS = array();

if ($result) {
	foreach ($result as $row) {
		$_ORDER_RECIPIENTS[] = $row['recipinet'];
	}
}

// add recipients ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_RECIPIENT && $_RECIPIENT_ROLE && $this->checkRole($_RECIPIENT_ROLE) && !in_array($_RECIPIENT, $_ORDER_RECIPIENTS)) {
	$this->addRecipient($_RECIPIENT);
}

if ($BrandManager instanceof User && $this->checkRole(Role::BRAND_MANAGER) && !in_array($BrandManager->id, $_ORDER_RECIPIENTS)) {
	$this->addRecipient($BrandManager->id);
}

if ($ProjectLeader && $this->checkRole(Role::PROJECT_LEADER) && !in_array($ProjectLeader, $_ORDER_RECIPIENTS)) {
	$this->addRecipient($ProjectLeader);
}

if ($RegionalSalesManager instanceof User && $this->checkRole(Role::REGIONAL_SALES_MANAGER) && !in_array($RegionalSalesManager->id, $_ORDER_RECIPIENTS)) {
	$this->addRecipient($RegionalSalesManager->id);
}

// add retail controller for corporate projects
if ($projectCosts['project_cost_type'] == 1) {
	
	$UserMapper = new User_Datamapper();
	$controllers = $UserMapper->getRetailControllers();
	
	foreach ($controllers as $user) {
		if ($user instanceof User && $this->checkRole(Role::RETAIL_CONTROLLER) && !in_array($user->id, $_ORDER_RECIPIENTS)) {
			$this->addRecipient($user->id);
		}
	}
}

// sendmail ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$this->populate(true);
$recipients = $this->getRecipients();


// track mail orders :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
if ($recipients && $Order->id) {
	$ccRecipients = array();

	foreach ($recipients as $id => $recipient) {
		
		if (1== 1 or $recipient->isSendMail()) {
			
			$Order->orderMailTrack(array(
				'order_mail_order'       => $Order->id,
				'order_mail_user'        => $recipient->id,
				'order_mail_from_user'   => $sender->id,
				'order_mail_text'        => $recipient->getBody(true),
				'order_mail_is_cc'       => 0,
				'order_mail_template_id' => $_TPL,
				'user_created'           => $sender->login,
				'date_created'           => date('Y-m-d H:i:s')
			));
			
			$this->console("Order mail track for recipient $recipient->id ");

			$cc = $recipient->getCCrecipients();

			if ($cc) {
				$ccRecipients[$id]['recipients'] = $cc;
				$ccRecipients[$id]['content'] = $recipient->getBody(true);
			}
		}
	}

	if ($ccRecipients) {

		$usr = new User();

		foreach ($ccRecipients as $i => $recipients) {

			foreach ($recipients['recipients'] as $email) {
				
				$usr->getActiveUserFromEmail($email);

				if (!$usr->id) continue;

				$Order->orderMailTrack(array(
					'order_mail_order'       => $Order->id,
					'order_mail_user'        => $usr->id,
					'order_mail_from_user'   => $sender->id,
					'order_mail_text'        => $recipients['content'],
					'order_mail_is_cc'       => 1,
					'order_mail_template_id' => $_TPL,
					'user_created'           => $sender->login,
					'date_created'           => date('Y-m-d H:i:s')
				));

				$this->console("Order CC mail track for recipient $usr->id ");
			}
		}
	}
}
