<?php 
	
$auth = User::instance();
$request = Request::instance();
$protocol = Settings::init()->http_protocol.'://';
$server = $_SERVER['SERVER_NAME'];

$model = new Model();

$_TPL = $this->getTemplateId();
$_PROJECT_ID = $this->getParam('id');

// sender
$sender = $this->getSender();

// Project
$Project = new Project();
$Project->read($_PROJECT_ID);

if (!$Project->id) {
	$this->console('Project $_PROJECT_ID not found');
	return;
}

// project order
$Order = $Project->order();

// involved roles
$_ROLE_PROJECT_MANAGER = 3;
$_ROLE_EXTERNAL_PROJECT_MANAGER = 80;
$_ROLE_CLIENT = 4;
$_ROLE_LOCAL_RETAIL_CORDINATOR = 10;
$_ROLE_DESIGN_CONTRACTOR = 7;
$_ROLE_DESIGN_SUPERVISOR = 8;
//$_ROLE_ORDER_RETAIL_OPERATOR = 2;
$_ROLE_CMS_APPROVAR = array(3,8,10);
$_ROLE_BRAND_MANAGER = 15;	

// Client :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
$_CLIENT = $Order->user;

// Regional Sales Manager :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
$RegionalSalesManager = $Order->getRegionalSalesManager();


// Retail Cordinator :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PROJECT_RETAIL_CORDINATOR = $Project->retail_coordinator;

if ($_PROJECT_RETAIL_CORDINATOR) {

	$paramRetailCordinator = $this->getParam('retail_coordinator');
	
	if ($paramRetailCordinator) {
		$this->addRecipient($_PROJECT_RETAIL_CORDINATOR);
	}
	else
	{
		if ($this->checkRole($_ROLE_PROJECT_MANAGER) || $this->checkRole($_ROLE_PROJECT_MANAGER, true)) {
			$this->setParam('retail_coordinator', true);
			$paramRetailCordinator = true;
		}
		elseif ($this->checkRole($_ROLE_EXTERNAL_PROJECT_MANAGER) || $this->checkRole($_ROLE_EXTERNAL_PROJECT_MANAGER, true)) {
			$this->setParam('retail_coordinator', true);
			$paramRetailCordinator = true;
		}
	}
}

// Local Retail Cordinator :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PROJECT_LOCAL_RETAIL_CORDINATOR = $Project->local_retail_coordinator;

if ($_PROJECT_LOCAL_RETAIL_CORDINATOR && $_ROLE_LOCAL_RETAIL_CORDINATOR) {

	$paramLocalRetailCordinator = $this->getParam('local_retail_coordinator');

	if (!$paramLocalRetailCordinator && ($this->checkRole($_ROLE_LOCAL_RETAIL_CORDINATOR) || $this->checkRole($_ROLE_LOCAL_RETAIL_CORDINATOR, true))) {
		$this->setParam('local_retail_coordinator', true);
		$paramLocalRetailCordinator = true;
	}
	
	if ($paramLocalRetailCordinator) {
		$this->addRecipient($_PROJECT_LOCAL_RETAIL_CORDINATOR);
	}
}
	
// Design Contractor :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PROJECT_DESIGN_CONTRACTOR = $Project->design_contractor;

if ($_PROJECT_DESIGN_CONTRACTOR && $_ROLE_DESIGN_CONTRACTOR) {

	$paramDesignContractor = $this->getParam('design_contractor');

	if (!$paramDesignContractor && ($this->checkRole($_ROLE_DESIGN_CONTRACTOR) || $this->checkRole($_ROLE_DESIGN_CONTRACTOR, true))) {
		$this->setParam('design_contractor', true);
		$paramDesignContractor = true;
	}
	
	if ($paramDesignContractor) {
		$this->addRecipient($_PROJECT_DESIGN_CONTRACTOR);
	}
}
	
// Design Supervisor :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PROJECT_DESIGN_SUPERVISOR = $Project->design_supervisor;

if ($_PROJECT_DESIGN_SUPERVISOR && $_ROLE_DESIGN_SUPERVISOR) {

	$paramDesignSupervisor = $this->getParam('design_supervisor');

	if (!$paramDesignSupervisor && ($this->checkRole($_ROLE_DESIGN_SUPERVISOR) || $this->checkRole($_ROLE_DESIGN_SUPERVISOR, true))) {
		$this->setParam('design_supervisor', true);
		$paramDesignSupervisor = true;
	}
	
	if ($paramDesignSupervisor) {
		$this->addRecipient($_PROJECT_DESIGN_SUPERVISOR);
	}
}
	
// CMS Approver ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PROJECT_CMS_APPROVER = $Project->cms_approver;

if ($_PROJECT_CMS_APPROVER && is_array($_ROLE_CMS_APPROVAR)) {

	$paramCmsApprover = $this->getParam('cms_approver');

	if (!$paramCmsApprover) {
		foreach ($_ROLE_CMS_APPROVAR as $role) {
			if ($this->checkRole($role) || $this->checkRole($role, true) ) {
				$this->setParam('cms_approver', true);
				$paramCmsApprover = true;
				break;
			}
		}
	}	

	if ($paramCmsApprover) {
		$this->addRecipient($_PROJECT_CMS_APPROVER);
	}
}
	
// Order Retail Operator :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
/*
$_ORDER_RETAIL_OPERATOR = $Order->retail_operator;

if ($_ORDER_RETAIL_OPERATOR) {

	$paramRetailOperator = $this->getParam('order_retail_operator');

	if (!$paramRetailOperator && ($this->checkRole($_ROLE_ORDER_RETAIL_OPERATOR) || $this->checkRole($_ROLE_ORDER_RETAIL_OPERATOR, true))) {
		$this->setParam('order_retail_operator', true);
		$paramRetailOperator = true;
	}
	
	if ($paramRetailOperator) {
		$this->addRecipient($_ORDER_RETAIL_OPERATOR);
	}
}
*/

// BrandManager :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
$UserMapper = new User_DataMapper();
$BrandManager = $UserMapper->getBrandManager($Order->client_address);
if ($BrandManager === false) {
	$BrandManager = $UserMapper->getAffiliateBrandManager($Order->client_address);
}



// project.retail.agreed.opening.date.changed ::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
if ($_TPL == 15) {

	$paramOldShopRealOpeningDate = $this->getParam('old_shop_real_opening_date');
	
	// subject and body content
	if ($Project->projectkind == 4 || $Project->projectkind == 5) {
		$this->setSubject("Project's expected ending date was changed  - Project {order_number}, {order_shop_address_country_name}, {order_shop_address_company}");
		$this->setBody("{user_firstname} {user_name} has changed the project's expected ending date {fromto}");
	}

	// old shop real opening date		
	if ($paramOldShopRealOpeningDate) {
		$oldOpeningRealDate = date::system($paramOldShopRealOpeningDate);
	}
	
	// project real opening date
	if ($Project->real_opening_date) {
		$openingRealDate = date::system($Project->real_opening_date);
	}
	
	// content  render fields
	$this->setDataloader(array(
		'fromto' => $oldOpeningRealDate ? "from $oldOpeningRealDate to $openingRealDate" : "to $openingRealDate",
		'order_number' => $Order->number,
		'order_shop_address_country_name' => $Order->data["country_name"],
		'order_shop_address_company' => $Order->shop_address_company,
		'link' => $protocol.$server."/user/project_edit_pos_data.php?pid=".$Project->id
	));
	
	// track fields
	$track = array(
		'projecttracking_user_id' => $auth->id,
		'projecttracking_project_id' => $Project->id,
		'projecttracking_field' => "project_real_opening_date",
		'projecttracking_oldvalue' => $oldOpeningRealDate,
		'projecttracking_newvalue' => $openingRealDate,
		'projecttracking_comment' => $this->getParam('track_comment'),
		'projecttracking_time' => date("Y-m-d:H:i:s")
	);
}
	

// project.retail.productline.changed ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
if ($_TPL == 16) {
	
	$productLine = new Product_Line();
	
	// old product line name
	$paramOldProductLine = $this->getParam('old_product_line');
	$productLine->read($paramOldProductLine);
	$oldProductLineName = $productLine->name ?: 'not yet indicated';
	
	// new product line
	$productLine->read($Project->product_line);
	$newProductLineName = $productLine->name ?: 'not yet indicated';
	
	// content render fields
	$this->setDataloader(array(
		'old_product_line_name' => $oldProductLineName,
		'product_line_name' => $newProductLineName,
		'link' => $protocol.$server."/user/project_view_client_data.php.php?pid=".$Project->id
	));
	
	// track fields
	$track = array(
		'projecttracking_user_id' => $auth->id,
		'projecttracking_project_id' => $Project->id,
		'projecttracking_field' => "product_line",
		'projecttracking_oldvalue' => $oldProductLineName,
		'projecttracking_newvalue' => $newProductLineName,
		'projecttracking_comment' => $this->getParam('track_comment'),
		'projecttracking_time' => date("Y-m-d:H:i:s")
	);
}


// project.retail.state.changed ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_TPL == 17) {
	
	if (!$paramOldProjectState) {
		$this->setBody("The treatment state of the project was set to {project_state_name}");
	}
	
	// new project state
	$paramOldProjectState = $this->getParam('old_project_state');
	$Project->state()->read($paramOldProjectState);
	$oldProjectStateName = $Project->state()->name;
	
	// new project state
	$state = $Project->state;
	$Project->state()->read($state);
	$newProjectStateName = $Project->state()->name;
	
	// content render fields
	$this->setDataloader(array(
		'old_project_state_name' => $oldProjectStateName,
		'project_state_name' => $newProjectStateName,
		'link' => $protocol.$server."/user/project_task_center.php?pid=".$Project->id
	));
	
	// track fields
	$track = array(
		'projecttracking_user_id' => $auth->id,
		'projecttracking_project_id' => $Project->id,
		'projecttracking_field' => "project_state",
		'projecttracking_oldvalue' => $oldProjectStateName,
		'projecttracking_newvalue' => $newProjectStateName,
		'projecttracking_comment' => $this->getParam('track_comment'),
		'projecttracking_time' => date("Y-m-d:H:i:s")
	);
}
	

// project.retail.approval.needed ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
if ($_TPL == 18) {

	$this->setDataloader(array(
		'link' =>  $protocol.$server."/user/project_costs_real_costs.php?pid=".$Project->id
	));
}


// project.projectmanager.changed ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_TPL == 31) {

	$_NEW_RETAIL_CORDINATOR = $this->getParam('new_retail_coordinator');
	$_OLD_RETAIL_CORDINATOR = $this->getParam('old_retail_coordinator');

	$oldRetailCordinator = new User($_OLD_RETAIL_CORDINATOR);
	
	if ($_NEW_RETAIL_CORDINATOR) {
		
		$recipient = $this->addRecipient($_NEW_RETAIL_CORDINATOR);

		if ($oldRetailCordinator->id) {
			$recipient->addCCRecipient($oldRetailCordinator->id);
		}
	}

	$this->setDataloader(array(
		'old_project_manager' => $oldRetailCordinator->firstname . " " . $oldRetailCordinator->name,
		'order_number' => $Order->number,
		'order_shop_address_country_name' => $Order->data["country_name"],
		'address_company' => $Order->shop_address_company,
		'link' => $protocol.$server."/user/project_task_center.php?pid=".$Project->id
	));
}

// project.logisticscoordinator.changed ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
/*
if ($_TPL == 32) {
	
	$_NEW_RETAIL_OPERATOR = $this->getParam('new_retail_operator');
	$_OLD_RETAIL_OPERATOR = $this->getParam('old_retail_operator');

	$oldRetailOperator = new User($_OLD_RETAIL_OPERATOR);

	if ($_NEW_RETAIL_OPERATOR) {
		
		$recipient = $this->addRecipient($_NEW_RETAIL_OPERATOR);

		if ($oldRetailOperator->id) {
			$recipient->addCCRecipient($oldRetailOperator->id);
		}
	}

	$this->setDataloader(array(
		'old_logistics_coordinator' => $oldRetailOperator->firstname . " " . $oldRetailOperator->name,
		'order_number' => $Order->number,
		'order_shop_address_country_name' => $Order->data["country_name"],
		'address_company' => $Order->shop_address_company,
		'link' => $protocol.$server."/user/project_task_center.php?pid=".$Project->id
	));

}
*/

if ($_TPL == 109) {

	$_REASON = $this->getParam('reason');

	$recipient = $this->addRecipient($_CLIENT);


	if ($BrandManager instanceof User) {
		
		// add brand manager as recipient
		if ($this->checkRole($_ROLE_BRAND_MANAGER) and $BrandManager->id > 0) {
			
			$this->addRecipient($BrandManager->id);
		}
		else if ($this->checkRole($_ROLE_BRAND_MANAGER, true) and $BrandManager->id) {
			$recipient->addCCRecipient($BrandManager->id);
		}
	}

	if ($RegionalSalesManager instanceof User and $RegionalSalesManager->id > 0) {
		$recipient->addCCRecipient($RegionalSalesManager->id);
	}

	$this->setDataloader(array(
		'project_number' => $Order->number,
		'order_shop_address_country_name' => $Order->data["country_name"],
		'address_company' => $Order->shop_address_company,
		'link' => $protocol.$server."/user/project_task_center.php?pid=".$Project->id,
		'reason' => $_REASON 
	));
}
	

// sendmail ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$this->populate(true);


// project track :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($this->isSuccess() && $track) {
	$Project->track()->create($track);
}


// track mail orders :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
if ($_TPL == 109) {
	if ($recipient && $Order->id) {
		$ccRecipients = array();
		if (1== 1 or $recipient->isSendMail()) {
			
			$Order->orderMailTrack(array(
				'order_mail_order'       => $Order->id,
				'order_mail_user'        => $recipient->id,
				'order_mail_from_user'   => $sender->id,
				'order_mail_text'        => $recipient->getBody(true),
				'order_mail_is_cc'       => 0,
				'order_mail_template_id' => $_TPL,
				'user_created'           => $sender->login,
				'date_created'           => date('Y-m-d H:i:s')
			));

			
			$this->console("Order mail track for recipient $recipient->id ");

			$cc = $recipient->getCCrecipients();

			if ($cc) {
				$ccRecipients[$id]['recipients'] = $cc;
				$ccRecipients[$id]['content'] = $recipient->getBody(true);
			}
		}

		if ($ccRecipients) {

			$usr = new User();

			foreach ($ccRecipients as $i => $recipients) {

				foreach ($recipients['recipients'] as $email) {
					
					$usr->getActiveUserFromEmail($email);

					if (!$usr->id) continue;

					$Order->orderMailTrack(array(
						'order_mail_order'       => $Order->id,
						'order_mail_user'        => $usr->id,
						'order_mail_from_user'   => $sender->id,
						'order_mail_text'        => $recipients['content'],
						'order_mail_is_cc'       => 1,
						'order_mail_template_id' => $_TPL,
						'user_created'           => $sender->login,
						'date_created'           => date('Y-m-d H:i:s')
					));

					$this->console("Order CC mail track for recipient $usr->id ");
				}
			}
		}
	}
}	