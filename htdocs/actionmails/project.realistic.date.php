<?php
/**
 * This is the actionmail file for mails that call responsible users to enter
 * realistic dates for opening- taking over or starting lease of new or existing POS.
 * It adds the required role recipients and tracks sent mails to order.
 */

$auth = User::instance();
$request = Request::instance();

$_TPL = $this->getTemplateId();
$_PROJECT_ID = $this->getParam('projectID');

// load required data
$Project = new Project();
$Project->read($_PROJECT_ID);

if (!$Project->id) {
	$this->console('Project $_PROJECT_ID not found');
	return;
}

// project order
$Order = $Project->order();

// retail cordinator :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if($_TPL == 83
	or $_TPL == 84
	or $_TPL == 85) {
	
	//actual opening date, lease starting date, actual takeover date missing
	
	if ($Order->user) {
		$recipient = $this->addRecipient($Order->user);
		$recipient->addCCRecipient($Project->retail_coordinator);
	}
	else {
		if ($Project->retail_coordinator) {
			$this->addRecipient($Project->retail_coordinator);
		}
	}	
}
else {
	if ($Project->retail_coordinator) {
		$this->addRecipient($Project->retail_coordinator);
	}
}

// add order retail operator if available :::::::::::::::::::::::::::::::::::::::::::::::::::::::
/*
if ($Order->retail_operator) {
	
	$RetailOperator = new User($Order->retail_operator);
	
	if (!is_null($RetailOperator->email)) {
		$this->addCCRecipients($RetailOperator->email);
	}
}
*/

// datalaoder ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$this->setDataloader(array(
	'project_number' => $Project->number,
	'country' => $Project->getCountry()->name,
	'shop_address' => $Order->shop_address_company,
	'link' => URLBuilder::editPOSData($Project->id)
));


// sendmail ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$this->populate(true);
$recipients = $this->getRecipients();

$sender = $this->getSender();

// track mail orders :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($recipients && $Order->id) {

	$ccRecipients = array();

	foreach ($recipients as $id => $recipient) {
		
		if (1== 1 or $recipient->isSendMail()) {
			
			$Order->orderMailTrack(array(
				'order_mail_order'       => $Order->id,
				'order_mail_user'        => $recipient->id,
				'order_mail_from_user'   => $sender->id,
				'order_mail_text'        => $recipient->getBody(true),
				'order_mail_is_cc'       => 0,
				'order_mail_template_id' => $_TPL,
				'user_created'           => $sender->login,
				'date_created'           => date('Y-m-d H:i:s')
			));
			
			$this->console("Order mail track for recipient $recipient->id ");

			$cc = $recipient->getCCrecipients();

			if ($cc) {
				$ccRecipients[$id]['recipients'] = $cc;
				$ccRecipients[$id]['content'] = $recipient->getBody(true);
			}
		}
	}

	if ($ccRecipients) {

		$usr = new User();

		foreach ($ccRecipients as $i => $recipients) {

			foreach ($recipients['recipients'] as $email) {
				
				$usr->getActiveUserFromEmail($email);

				if (!$usr->id) continue;

				$Order->orderMailTrack(array(
					'order_mail_order'       => $Order->id,
					'order_mail_user'        => $usr->id,
					'order_mail_from_user'   => $sender->id,
					'order_mail_text'        => $recipients['content'],
					'order_mail_is_cc'       => 1,
					'order_mail_template_id' => $_TPL,
					'user_created'           => $sender->login,
					'date_created'           => date('Y-m-d H:i:s')
				));

				$this->console("Order CC mail track for recipient $usr->id ");
			}
		}
	}
}