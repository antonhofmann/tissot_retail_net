<?php 
	
$auth = User::instance();
$request = Request::instance();
$protocol = Settings::init()->http_protocol.'://';
$server = $_SERVER['SERVER_NAME'];

$model = new Model();

$_TPL = $this->getTemplateId();
$_PROJECT_ID = $this->getParam('target_project_id');
$_ORDER_ID = $this->getParam('oid');


// sender
$sender = $this->getSender();

// Project
$Project = new Project();
$Project->read($_PROJECT_ID);

if (!$Project->id) {
	$this->console('Project $_PROJECT_ID not found');
	return;
}

// project order
$Order = $Project->order();

// involved roles
$_ROLE_LOCAL_RETAIL_CORDINATOR = 10;
//$_ROLE_ORDER_RETAIL_OPERATOR = 2;
$_ROLE_BRAND_MANAGER = 15;	

// recipients :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
$recipient = '';
if($this->getParam('pl') and $this->getParam('cl')) {
	$recipient = $this->addRecipient($this->getParam('pl'));
	$this->addCCrecipient($this->getParam('cl'));
}
elseif($this->getParam('pl')) {
	$recipient = $this->addRecipient($this->getParam('pl'));
}
elseif($this->getParam('cl')) {
	$recipient = $this->addRecipient($this->getParam('cl'));
}


// Local Retail Cordinator :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PROJECT_LOCAL_RETAIL_CORDINATOR = $Project->local_retail_coordinator;

if ($_PROJECT_LOCAL_RETAIL_CORDINATOR && $_ROLE_LOCAL_RETAIL_CORDINATOR) {

	$paramLocalRetailCordinator = $this->getParam('local_retail_coordinator');

	if (!$paramLocalRetailCordinator && ($this->checkRole($_ROLE_LOCAL_RETAIL_CORDINATOR) || $this->checkRole($_ROLE_LOCAL_RETAIL_CORDINATOR, true))) {
		$this->setParam('local_retail_coordinator', true);
		$paramLocalRetailCordinator = true;
	}
	
	if ($paramLocalRetailCordinator) {
		$this->addRecipient($_PROJECT_LOCAL_RETAIL_CORDINATOR);
	}
}
	
	
// Order Retail Operator :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
/*
$_ORDER_RETAIL_OPERATOR = $Order->retail_operator;

if ($_ORDER_RETAIL_OPERATOR) {

	$paramRetailOperator = $this->getParam('order_retail_operator');

	if (!$paramRetailOperator && ($this->checkRole($_ROLE_ORDER_RETAIL_OPERATOR) || $this->checkRole($_ROLE_ORDER_RETAIL_OPERATOR, true))) {
		$this->setParam('order_retail_operator', true);
		$paramRetailOperator = true;
	}
	
	if ($paramRetailOperator) {
		$this->addRecipient($_ORDER_RETAIL_OPERATOR);
	}
}
*/


// BrandManager :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
$UserMapper = new User_DataMapper();
$BrandManager = $UserMapper->getBrandManager($Order->client_address);
if ($BrandManager === false) {
	$BrandManager = $UserMapper->getAffiliateBrandManager($Order->client_address);
}

if($recipient > 0) {

	// catalogue or replacement order for a project was posted ::::::::::::::::::::::::::::::::::::::::::::::::::::::
	// templates 149, 150

	// content  render fields
	$this->setDataloader(array(
		'project_number' => $Order->number,
		'country' => $Order->data["country_name"],
		'shop_address' => $Order->shop_address_company,
		'order_link' => $this->getParam('order_link'),
		'project_link' => $this->getParam('project_link')
	));

}	


	
if($recipient > 0) {

	// sendmail ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$this->populate(true);

	$recipients = $this->getRecipients();


	// track mail orders :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	if ($recipient && $Order->id) {
		$ccRecipients = array();
		if (1== 1 or $recipient->isSendMail()) {
			
			$Order->orderMailTrack(array(
				'order_mail_order'       => $Order->id,
				'order_mail_user'        => $recipient->id,
				'order_mail_from_user'   => $sender->id,
				'order_mail_text'        => $recipient->getBody(true),
				'order_mail_is_cc'       => 0,
				'order_mail_template_id' => $_TPL,
				'user_created'           => $sender->login,
				'date_created'           => date('Y-m-d H:i:s')
			));

			
			$this->console("Order mail track for recipient $recipient->id ");

			$cc = $recipient->getCCrecipients();

			if ($cc) {
				$ccRecipients[$id]['recipients'] = $cc;
				$ccRecipients[$id]['content'] = $recipient->getBody(true);
			}
		}

		if ($ccRecipients) {

			$usr = new User();

			foreach ($ccRecipients as $i => $recipients) {

				foreach ($recipients['recipients'] as $email) {
					
					$usr->getActiveUserFromEmail($email);

					if (!$usr->id) continue;

					$Order->orderMailTrack(array(
						'order_mail_order'       => $Order->id,
						'order_mail_user'        => $usr->id,
						'order_mail_from_user'   => $sender->id,
						'order_mail_text'        => $recipients['content'],
						'order_mail_is_cc'       => 1,
						'order_mail_template_id' => $_TPL,
						'user_created'           => $sender->login,
						'date_created'           => date('Y-m-d H:i:s')
					));

					$this->console("Order CC mail track for recipient $usr->id ");
				}
			}
		}

	}
}