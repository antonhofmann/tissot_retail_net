<?php 
	
$auth = User::instance();
$request = Request::instance();
$protocol = Settings::init()->http_protocol.'://';
$server = $_SERVER['SERVER_NAME'];
$model = new Model();

$_TPL = $this->getTemplateId();
$_PROJECT_ID = $this->getParam('id');

// Project
$Project = new Project();
$Project->read($_PROJECT_ID);

if (!$Project->id) {
	$this->console('Project $_PROJECT_ID not found');
	return;
}

// project order
$Order = $Project->order();
	
// involved roles
$_ROLE_PROJECT_MANAGER = 3;
$_ROLE_EXTERNAL_PROJECT_MANAGER = 80;
$_ROLE_RETAIL_CORDINATOR = 10;
$_ROLE_HQ_PROJECT_MANAGER= 19;
$_ROLE_DESIGN_CONTRACTOR = 7;
$_ROLE_DESIGN_SUPERVISOR = 8;
//$_ROLE_ORDER_RETAIL_OPERATOR = 2;
$_ROLE_CMS_APPROVAR = array(3,8,80);

	
// Retail Cordinator :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PROJECT_RETAIL_CORDINATOR = $Project->retail_coordinator;

if ($_PROJECT_RETAIL_CORDINATOR) {

	$paramRetailCordinator = $this->getParam('retail_coordinator');
	
	if ($paramRetailCordinator) {
		$this->addRecipient($_PROJECT_RETAIL_CORDINATOR);
	}
	else
	{
		if ($this->checkRole($_ROLE_PROJECT_MANAGER) || $this->checkRole($_ROLE_PROJECT_MANAGER, true)) {
			$this->setParam('retail_coordinator', true);
			$paramRetailCordinator = true;
		}
		elseif ($this->checkRole($_ROLE_EXTERNAL_PROJECT_MANAGER) || $this->checkRole($_ROLE_EXTERNAL_PROJECT_MANAGER, true)) {
			$this->setParam('retail_coordinator', true);
			$paramRetailCordinator = true;
		}
	}
}
	
	
// Local Retail Cordinator :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PROJECT_LOCAL_RETAIL_CORDINATOR = $Project->local_retail_coordinator;

if ($_PROJECT_LOCAL_RETAIL_CORDINATOR) {

	$paramLocalRetailCordinator = $this->getParam('local_retail_coordinator');

	if (!$paramLocalRetailCordinator && ($this->checkRole($_ROLE_RETAIL_CORDINATOR) || $this->checkRole($_ROLE_RETAIL_CORDINATOR, true))) {
		$this->setParam('local_retail_coordinator', true);
		$paramLocalRetailCordinator = true;
	}
	
	if ($paramLocalRetailCordinator) {
		$this->addRecipient($_PROJECT_LOCAL_RETAIL_CORDINATOR);
	}
}
	
// Design Contractor :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PROJECT_DESIGN_CONTRACTOR = $Project->design_contractor;

if ($_PROJECT_DESIGN_CONTRACTOR) {

	$paramDesignContractor = $this->getParam('design_contractor');

	if (!$paramDesignContractor && ($this->checkRole($_ROLE_DESIGN_CONTRACTOR) || $this->checkRole($_ROLE_DESIGN_CONTRACTOR, true))) {
		$this->setParam('design_contractor', true);
		$paramDesignContractor = true;
	}
	
	if ($paramDesignContractor) {
		$this->addRecipient($_PROJECT_DESIGN_CONTRACTOR);
	}
}
	
	
// Design Supervisor :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PROJECT_DESIGN_SUPERVISOR = $Project->design_supervisor;

if ($_PROJECT_DESIGN_SUPERVISOR) {

	$paramDesignSupervisor = $this->getParam('design_supervisor');

	if (!$paramDesignSupervisor && ($this->checkRole($_ROLE_DESIGN_SUPERVISOR) || $this->checkRole($_ROLE_DESIGN_SUPERVISOR, true))) {
		$this->setParam('design_supervisor', true);
		$paramDesignSupervisor = true;
	}
	
	if ($paramDesignSupervisor) {
		$this->addRecipient($_PROJECT_DESIGN_SUPERVISOR);
	}
}
	
	
// CMS Approver ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_PROJECT_CMS_APPROVER = $Project->cms_approver;

if ($_PROJECT_CMS_APPROVER) {

	$paramCmsApprover = $this->getParam('cms_approver');

	if (!$paramCmsApprover && is_array($_ROLE_CMS_APPROVAR)) {
		
		foreach ($_ROLE_CMS_APPROVAR as $role) {
			if ($this->checkRole($role) || $this->checkRole($role, true) ) {
				$this->setParam('cms_approver', true);
				$paramCmsApprover = true;
				break;
			}
		}
	}	

	if ($paramCmsApprover) {
		$this->addRecipient($_PROJECT_CMS_APPROVER);
	}
}

	
// Order Retail Operator :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
/*
$_ORDER_RETAIL_OPERATOR = $Order->retail_operator;

if ($_ORDER_RETAIL_OPERATOR) {

	$paramRetailOperator = $this->getParam('order_retail_operator');

	if (!$paramRetailOperator && ($this->checkRole($_ROLE_ORDER_RETAIL_OPERATOR) || $this->checkRole($_ROLE_ORDER_RETAIL_OPERATOR, true))) {
		$this->setParam('order_retail_operator', true);
		$paramRetailOperator = true;
	}
	
	if ($paramRetailOperator) {
		$this->addRecipient($_ORDER_RETAIL_OPERATOR);
	}
}
*/


// project.budget.was.unlocked :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_TPL == 34) {
	
	$recipient = $this->addRecipient($_PROJECT_RETAIL_CORDINATOR);
	//$recipient->addCCrecipient($_ORDER_RETAIL_OPERATOR);

	$this->setDataloader(array(
		'order_number' => $Order->number,
		'order_shop_address_country_name' => $Order->shop_address_country_name,
		'address_company' => $Order->shop_address_company,
		'link' => $protocol.$server."/user/project_task_center.php?pid=$Project->id"
	));
}


// project.budget.was.locked :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($this->template == 35) {
	
	$recipient = $this->addRecipient($_PROJECT_RETAIL_CORDINATOR);
	//$recipient->addCCrecipient($_ORDER_RETAIL_OPERATOR);

	$this->setDataloader(array(
		'order_number' => $Order->number,
		'order_shop_address_country_name' => $Order->shop_address_country_name,
		'address_company' => $Order->shop_address_company,
		'link' => $protocol.$server."/user/project_task_center.php?pid=$Project->id"
	));

}
