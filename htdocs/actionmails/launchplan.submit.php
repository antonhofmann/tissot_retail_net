<?php

$_APPLICATION = $this->getParam('application');
$_SELECTED_LAUNCHPLANS = $this->getParam('launchplans');

$protocol = Settings::init()->http_protocol.'://';
$_LINK = $protocol.'://'.$_SERVER['SERVER_NAME']."/$_APPLICATION/launchplans/data";

$db = Connector::get($_APPLICATION);	
$auth = User::instance();

// check in ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$auth->id || !$_SELECTED_LAUNCHPLANS) {
	$this->console("Bad request");
	return;
}

// filters :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_FILTERS = array();

// selected launchplans
$_SELECTED_LAUNCHPLANS = is_array($_SELECTED_LAUNCHPLANS) 
	? join(',', $_SELECTED_LAUNCHPLANS) : 
	$_SELECTED_LAUNCHPLANS;

$_FILTERS[] = "lps_launchplan_id IN ($_SELECTED_LAUNCHPLANS)";

$_QUERY_FILTER = join(' AND ', $_FILTERS);


// launchplans :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_LAUNCHPLANS = array();

$sth = $db->prepare("
	SELECT DISTINCT
		lps_launchplan_id,
		address_id,
		address_company,
		DATE_FORMAT(lps_launchplan_openingdate, '%d.%m.%Y') AS lps_launchplan_openingdate,
		DATE_FORMAT(lps_launchplan_closingdate, '%d.%m.%Y') AS lps_launchplan_closingdate,
		lps_mastersheet_name
	FROM lps_launchplans
	INNER JOIN lps_mastersheets ON lps_mastersheet_id = lps_launchplan_mastersheet_id
	INNER JOIN db_retailnet.addresses ON lps_launchplan_address_id = address_id
	WHERE $_QUERY_FILTER
");

$sth->execute();
$result = $sth->fetchAll();

if (!$result) {
	$this->console("Launch plans not found");
	return;
}

foreach ($result as $row) {
		
	$id = $row['lps_launchplan_id'];
	$company = $row['address_id'];
	
	$_LAUNCHPLANS[$id] = array(
		'company' => $company,
		'address_company' => $row['address_company'],
		'link' => $_LINK.'/'.$id,
		'lps_launchplan_openingdate' => $row['lps_launchplan_openingdate'],
		'lps_launchplan_closingdate' => $row['lps_launchplan_closingdate'],
		'lps_mastersheet_name' => $row['lps_mastersheet_name']
	);
}


// users :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		
$_USERS = array();
$_USER_ROLES = array();

$sth = $db->prepare("
	SELECT DISTINCT
		user_id,
		user_firstname,
		user_name,
		user_email,
		user_email_cc,
		user_email_deputy,
		user_address
	FROM lps_launchplans
	INNER JOIN db_retailnet.addresses ON lps_launchplan_address_id = address_id
	INNER JOIN db_retailnet.users ON user_address = address_id
	WHERE $_QUERY_FILTER AND user_active = 1
");

$sth->execute();
$result = $sth->fetchAll();

if (!$result) {
	$this->console("Users not found");
	return;
}

foreach ($result as $row) {
	
	$user = $row['user_id'];
	$company = $row['user_address'];
	
	$_USERS[$company][$user] = array(
		'user_firstname' => $row['user_firstname'],
		'user_name' => $row['user_name'],
		'user_email' => $row['user_email'],
		'user_email_cc' => $row['user_email_cc'],
		'user_email_deputy' => $row['user_email_deputy']
	);
}

// get user roles
$sth = $db->prepare("
	SELECT
		user_role_user,
		user_role_role
	FROM lps_launchplans
	INNER JOIN db_retailnet.addresses ON lps_launchplan_address_id = address_id
	INNER JOIN db_retailnet.users ON user_address = address_id
	INNER JOIN db_retailnet.user_roles ON user_role_user = user_id
	WHERE $_QUERY_FILTER AND user_active = 1		
");

$sth->execute();
$result = $sth->fetchAll();

if ($result) {
	foreach ($result as $row) {
		$user = $row['user_role_user'];
		$role = $row['user_role_role'];
		$_USER_ROLES[$user][$role] = $role;
	}
}
	

// get system roles ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $db->prepare("
	SELECT role_id, role_name
	FROM application_roles
	INNER JOIN roles ON role_id = application_role_role
	INNER JOIN applications ON application_id = application_role_application
	WHERE application_shortcut = ?
");

// application roles
$sth->execute(array($_APPLICATION));
$result = $sth->fetchAll();
$_APPLICATION_ROLES = _array::extract($result);

// template roles
$_TEMPLATE_ROLES = $this->getRoles() ?: array();
$_TEMPLATE_CC_ROLES = $this->getRoles(true) ?: array();


// recipients ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_RECIPIENTS = array();
$_CC_RECIPIENTS = array();

foreach ($_LAUNCHPLANS as $launchplan => $data) {
		
	$company = $data['company'];

	if (!$_USERS[$company]) {
		$this->console("Company $company has no users");
		continue;
	}
	
	foreach ($_USERS[$company] as $user => $row) {

		$isRecipient = false;
		$isRecipientCC = false;
		
		$roles = $_USER_ROLES[$user] ?: array();

		// check application role
		if ($_APPLICATION_ROLES && $roles) {
			$arr = array_intersect_key($_APPLICATION_ROLES, $roles);
			$isAppRecipient = count($arr) > 0 ? true : false;
		}
		else $isAppRecipient = true;

		// user has not application permitted roles
		if (!$isAppRecipient) {
			$this->console("User $user has not application permitted roles");
			continue;
		}

		// check recipient roles
		if ($_TEMPLATE_ROLES && $roles) {
			$arr = array_intersect_key($_TEMPLATE_ROLES, $roles);
			$isRecipient = count($arr) > 0 ? true : false;
		}
		
		// cc recipient
		if (!$isRecipient && $_TEMPLATE_CC_ROLES && $roles) {
			$arr = array_intersect_key($_TEMPLATE_CC_ROLES, $roles);
			$isRecipientCC = count($arr) > 0 ? true : false;
		}

		if ($isRecipient) {
			$_RECIPIENTS[$launchplan][$user] = $row;
		}
		elseif ($isRecipientCC && $row['user_email']) {			
			$_CC_RECIPIENTS[$launchplan][] = $row['user_email'];
		} 
		
		if (!$isRecipient && !$isRecipientCC) {
			$this->console("User $user has not permitted roles.");
		}
	}
}

if (!$_RECIPIENTS) {
	$this->console("No permitted recipients");
	return;
}


// launchplan files as footer link :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_FILES = array();
$_FOOTER = array();

// get collection category files
$sth = $db->prepare("
	SELECT DISTINCT 
		lps_collection_category_file_id AS id, 
		lps_launchplan_item_launchplan_id AS launchplan,
		lps_collection_category_file_title AS title,
		lps_collection_category_file_path AS path
	FROM lps_collection_category_files 
	INNER JOIN lps_references ON lps_collection_category_file_collection_id = lps_reference_collection_category_id
	INNER JOIN lps_launchplan_items ON lps_reference_id = lps_launchplan_item_reference_id
	WHERE lps_collection_category_file_visible = 1 	
	AND lps_launchplan_item_launchplan_id IN ($_SELECTED_LAUNCHPLANS)
	ORDER BY lps_collection_category_file_title
");	

$sth->execute();
$result = $sth->fetchAll();

if ($result) {
	foreach ($result as $row) {
		$id = $row['id'];
		$launchplan = $row['launchplan'];
		$_FILES[$launchplan][$id] = $row;
	}
}

// get master sheet files
$sth = $db->prepare("
	SELECT DISTINCT 
		lps_mastersheet_file_id AS id,
		lps_launchplan_id AS launchplan,
		lps_mastersheet_file_title AS title,
		lps_mastersheet_file_path AS path
	FROM lps_mastersheet_files 
	INNER JOIN lps_launchplans ON lps_launchplan_mastersheet_id = lps_mastersheet_file_mastersheet_id
	WHERE lps_mastersheet_file_visible = 1 
	AND lps_launchplan_id IN ($_SELECTED_LAUNCHPLANS)
	ORDER BY lps_mastersheet_file_title
");	

$sth->execute();
$result = $sth->fetchAll();

if ($result) {
	foreach ($result as $row) {
		$id = $row['id'];
		$launchplan = $row['launchplan'];
		$_FILES[$launchplan][$id] = $row;
	}
}

if ($_FILES) {

	foreach ($_FILES as $launchplans) {

		$content  = "<br><br>";
		$content .= "<h4>Master Sheet Files</h4>";

		foreach ($launchplans as $launchplan => $data) {
			$link = $protocol.$_SERVER['SERVER_NAME'].$data['path'];
			$content .= "<a href='$link' target='_blank'>{$data[title]}</a><br>";
		}

		$_FOOTER[$launchplan] = $content;
	}
}


// add recipients ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$groupMails = $this->getSetting('group');

foreach ($_RECIPIENTS as $launchplan => $users) {

	$footerContent = $this->getFooter();
	$footerContent .= $_FOOTER[$launchplan];

	if ($groupMails) {
		$group = $this->addGroup($launchplan);
		$group->setDataloader($_LAUNCHPLANS[$launchplan]);
		$group->setFooter($footerContent);
	}
	
	foreach ($users as $user => $data) {
		
		$key = $launchplan.'.'.$user; 
		$recipient = $this->addRecipient($key, $data, false);
		$recipient->setDataloader($_LAUNCHPLANS[$launchplan]);
		
		// cc recipinets
		$recipient->addCCrecipients($_CC_RECIPIENTS[$launchplan], false);

		// add recipient footer
		if ($_FOOTER[$launchplan]) {
			$recipient->setFooter($_FOOTER[$launchplan]);
		}

		if ($groupMails) {
			$group->addRecipient($recipient);
		}
	}
}
