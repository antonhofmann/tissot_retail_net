<?php

$auth = User::instance();
$settings = Settings::init();
$model = new Model(Connector::DB_CORE);

// server vars
$_HTTP_PROTOCOL = $settings->http_protocol.'://';
$_HTTP_SERVER = $_SERVER['SERVER_NAME'];

// get params
$_ID = $this->getParam('id');
$_CC_REQUEST_RECIPIENTS = $this->getParam('cc');
$_INVITATION = $this->getParam('invitation');

// template roles
$_TEMPLATE_ID = $this->getTemplateId();
$_TEMPLATE_ROLES = $this->getRoles() ?: array();
$_TEMPLATE_CC_ROLES = $this->getRoles(true) ?: array();

$_RECIPIENTS = array();
$_CC_RECIPIENTS = array();


// checkin ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
$user = new User($_ID);

if (!$auth->id || !$user->id) {
	$this->console("Bad request");
	return;
}

// get user roles
$_USER_ROLES = $user->roles($_ID);

$this->setDataloader($user->data);


// request to open/close acount ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_TEMPLATE_ID==11 || $_TEMPLATE_ID==12) {


	if ($_INVITATION && $_TEMPLATE_ROLES) {
		$_USER_ROLES = array_keys($_TEMPLATE_ROLES);
	}
	
	// role responsible persons
	$result = $model->query("
		SELECT DISTINCT
			user_id,
			user_firstname,
			user_name,
			user_email,
			user_email_cc,
			user_email_deputy,
			role_id,
			role_application
		FROM roles
		INNER JOIN users ON role_responsible_user_id = user_id
		WHERE user_active = 1	
	")->fetchAll();
	
	// role responsible person not found!
	// get all system administrators
	if (!$result) {
		
		$result = $model->query("
			SELECT DISTINCT
				user_id,
				user_firstname,
				user_name,
				user_email,
				user_email_cc,
				user_email_deputy,
				user_role_role AS role_id
			FROM users
			INNER JOIN user_roles ON user_role_user = user_id
			WHERE user_role_role = 1 AND user_active = 1
		")->fetchAll();
	}
	
	$recipients = array();
	$recipient = false;

	if (!$result) {
		$this->console("Users not found.");
		return;
	}
	
	foreach ($result as $row) {
		
		$userID = $row['user_id'];
		$role = $row['role_id'];
		
		if (!$userID) continue;

		// get first permitted user as recipient
		// all other users are cc recipients
		if (!$recipient &&  $row['user_email'] && in_array($role, $_USER_ROLES)) {
			
			$recipient = $row['user_id'];

			// is system administrator
			$check = $model->query("
				SELECT user_role_user
				FROM user_roles
				INNER JOIN role_permissions ON role_permission_role = user_role_role
				INNER JOIN permissions ON permission_id = role_permission_permission
				WHERE user_role_user = $recipient AND permission_name = 'can_edit_catalog'
			")->fetch();
			
			$row['link']  = $check['user_role_user'] > 0
				? $_HTTP_PROTOCOL.$_HTTP_SERVER."/admin/user.php?id=$user->id"
				: $_HTTP_PROTOCOL.$_HTTP_SERVER."/administration/users/users/".$user->address."/$user->id";
			
			//$_RECIPIENTS[$_ID] = $row;
			$_RECIPIENTS[$recipient] = $row;

			// Janine Schmid is always cc recipient
			if ($_TEMPLATE_ID==12 && $userID<>2094 && $auth->id<>2094 ) {
				$janineSchmid = new User();
				$janineSchmid->read(2094);
				$_CC_RECIPIENTS[$recipient][] = $janineSchmid->email;
			}
			
		} 

		if ($row['user_email']) {
			$_CC_RECIPIENTS[$recipient][] = $row['user_email'];
		}

		if ($row['user_email_cc']) {
			$_CC_RECIPIENTS[$recipient][] = $row['user_email_cc'];
		}

		if ($row['user_email_deputy']) {
			$_CC_RECIPIENTS[$recipient][] = $row['user_email_deputy'];
		}
	}
}


// send login data :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($_TEMPLATE_ID==13) {

	// add recipient
	$_RECIPIENTS[$_ID] = $user->data;

	// role cc recipients
	if (is_array($_USER_ROLES)) {

		$roles = join(',', $_USER_ROLES);
						
		// get user responsible persons
		$result = $model->query("
			SELECT DISTINCT 
				role_responsible_user_id,
				user_email,
				user_email_deputy
			FROM roles 
			INNER JOIN users ON user_id = role_responsible_user_id 
			WHERE role_id IN ($roles)		
		")->fetchAll();
		
		if ($result) {
			
			foreach ($result as $row) {
				
				if ($row['user_email'] <> $auth->email) {
					$_CC_RECIPIENTS[$_ID][] = $row['user_email'];
				}
				
				if ($row['user_email_deputy'] <> $auth->email) {
					$_CC_RECIPIENTS[$_ID][] = $row['user_email_deputy'];
				}
			}
		}
	}
		
	// form cc recipients
	if ($_CC_REQUEST_RECIPIENTS) {

		$_CC_REQUEST_RECIPIENTS = trim($_CC_REQUEST_RECIPIENTS);
		$_CC_REQUEST_RECIPIENTS = explode("\n", $_CC_REQUEST_RECIPIENTS);
		$_CC_REQUEST_RECIPIENTS = array_filter($_CC_REQUEST_RECIPIENTS, 'trim');
		
		foreach ($_CC_REQUEST_RECIPIENTS as $row) {
			
			$comma = strpos($row, ',');
			
			if ($comma === false) {
				if (check::email($row)) {
					$_CC_RECIPIENTS[$_ID][] = trim($row);
				}
			} else {
	
				$arr = explode(',', $row);
				$arr = array_filter($arr, 'trim');
				
				foreach ($arr as $email) {
					if (check::email($email)) {
						$_CC_RECIPIENTS[$_ID][] = $email;
					}
				}
			}
		}
	}
}


// recipients ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
if (!$_RECIPIENTS) {
	$this->console("No permitted recipients");
	return;
}

foreach ($_RECIPIENTS as $id => $data) {
	
	$recipient = $this->addRecipient($key, $data);

	if (!$recipient) {
		$this->console("Error: add recipient $id");
		continue;
	}

	if ($_CC_RECIPIENTS[$id]) {
		$recipient->addCCrecipients($_CC_RECIPIENTS[$id]);
	}

	// add more dataloader
	if ($_TEMPLATE_ID==11 || $_TEMPLATE_ID==12) {
		
		$dataloader = array();

		// recipient dataloader
		foreach ($data as $k => $v) {
			$k = str_replace('user_', 'recipient_', $k);
			$dataloader[$k] = $v;
		}

		$recipient->setDataloader($dataloader);
	}
}