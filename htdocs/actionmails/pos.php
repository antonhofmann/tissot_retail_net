<?php

$_ID = $this->getParam('id');
$_TEMPLATE = $this->getTemplateId();

$protocol = Settings::init()->http_protocol;
$_LINK_NEW_POS = $protocol.'://'.$_SERVER['SERVER_NAME']."/mps/pos/address/$_ID";
$_LINK_OLD_POS = $protocol.'://'.$_SERVER['SERVER_NAME']."/pos/posindex_pos.php?id=$_ID";

$db = Connector::get();	
$auth = User::instance();

// check in ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$auth->id || !$_ID || !$_TEMPLATE) {
	$this->console("Bad request");
	return;
}

// pos
$pos = new Pos();
$pos->read($_ID);

if (!$pos->id) {
	$this->console("POS $_ID not found");
	return;
}

// pos data ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_USERS = array();
$_USER_ROLES = array();
$_USER_APPLICATIONS = array();

// users
$sth = $db->prepare("SELECT * FROM users WHERE user_address = ? AND user_active = 1");
$sth->execute(array($pos->client_id));
$_USERS = $sth->fetchAll();

if (!$_USERS) {
	$this->console("POS users not found.");
	return;
}

// user roles
$sth = $db->prepare("
	SELECT
		user_role_user,	
		user_role_role,
		role_application
	FROM user_roles
	INNER JOIN roles ON role_id = user_role_role
	INNER JOIN users WHERE user_id = user_role_user
	WHERE user_address = ? AND user_active = 1
");

$sth->execute(array($pos->client_id));
$result = $sth->fetchAll();

if ($result) {
	foreach ($result as $row) {
		$user = $row['user_role_user'];
		$role = $row['user_role_role'];
		$app = $row['role_application'];
		$_USER_ROLES[$user][$role] = $role;
		$_USER_APPLICATIONS[$user][$app] = $app;
	}
}

// pos dataloader
$sth = $db->prepare("
	SELECT *
	FROM posaddresses 
	LEFT JOIN countries ON country_id = posaddress_country
	LEFT JOIN posowner_types ON posowner_type_id = posaddress_ownertype
	LEFT JOIN postypes ON postype_id = posaddress_store_postype
	WHERE posaddress_id = ?
");

$sth->execute(array($_ID));
$result = $sth->fetch();
$this->setDataloader($result);


// other recipients ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_OTHER_RECIPIENTS = array();
$_OTHER_CC_RECIPINETS = array();

$sth = $db->prepare("
	SELECT 
		mail_template_other_recipient_name AS name,
		mail_template_other_recipient_value AS value
	FROM mail_template_other_recipients
	WHERE mail_template_other_recipient_template_id = ?
");

$sth->execute(array($_TEMPLATE));
$result = $sth->fetchAll();

// get pos users
$sth = $db->prepare("
	SELECT DISTINCT 
		user_id, 
		user_firstname, 
		user_name, 
		user_email, 
		user_email_cc, 
		user_email_deputy, 
		address_company
	FROM user_company_responsibles
	INNER JOIN users ON user_id = user_company_responsible_user_id
	INNER JOIN addresses ON address_id = user_company_responsible_address_id
	INNER JOIN posaddresses ON posaddress_client_id = address_id
	WHERE posaddress_id = ? AND user_active = 1
	AND IF (
		? IN (1,3,4,5), 
		user_company_responsible_retail = 1, 
		user_company_responsible_wholsale = 1
	)
");

$sth->execute(array($_ID, $pos->ownertype));
$regionalResponsibleUsers = $sth->fetchAll();

if ($result) {
	
	foreach ($result as $row) {
		
		// regional responsible persons
		if ($regionalResponsibleUsers && ($row['name']=='regional_responsible' || $row['name']=='regional_responsible_cc') ) {
			
			foreach ($regionalResponsibleUsers as $user) {
				if ($row['name']=='regional_responsible_cc') $_OTHER_CC_RECIPINETS[] = $user['user_email'];
				else $_OTHER_RECIPIENTS[$user['user_id']] = $user;
			}
		}
	}
}
else $this->console("No other recipients");

// cc decorators :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_CC_DECORATORS = array();

$sth = $db->prepare("
	SELECT project_number, project_uses_icedunes_visuals
	FROM posorders 
	LEFT JOIN orders on order_id = posorder_order  
	LEFT JOIN projects on project_order = order_id 
	WHERE posorder_posaddress = ?
	AND posorder_type = 1 
	AND posorder_project_kind <> 8  
	AND (
		order_actual_order_state_code <> '900' 
		OR order_actual_order_state_code IS NULL
	)
	ORDER BY posorder_year DESC, posorder_opening_date DESC
	LIMIT 0,1;
");

$sth->execute(array($_ID));
$result = $sth->fetch();

if ($result['project_uses_icedunes_visuals']) {

	$sth = $db->prepare("
		SELECT GROUP_CONCAT(DISTINCT user_email) AS cc
		FROM users 
		WHERE user_decoration_responsible = 1 AND user_active = 1
	");

	$sth->execute();
	$result = $sth->fetch();
	$_CC_DECORATORS = $result['cc'] ? explode(',', $result['cc']) : array();
}
else $this->console("No CC decorators");


// set recipients ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_RECIPIENTS = array();
$_CC_RECIPINETS = array();

// get template roles
$_TEMPLATE_ROLES = $this->getRoles();
$_TEMPLATE_CC_ROLES = $this->getRoles(true);

$_CAN_SEND_CC_MAILS = $this->canSendCCmails();

switch ($_TEMPLATE) {
	
	case 19: // Change the closing date of a POS Location
	case 20: // Change the planned closing date of a POS Location
	case 21: // Enter the closing date of a POS Location
	case 22: // Enter the planned closing date of a POS Location
	case 36: // Change the planned closing date of a PopUp Location
	case 37: // Enter the planned closing date of a PopUp Location
			
		foreach ($_USERS as $user) {

			$id = $user['user_id'];
			$roles = $_USER_ROLES[$id] ?: array();
			$applications = $_USER_APPLICATIONS[$id] ?: array();

			if (!$user['user_email']) {
				$this->console("User $id hasn't mail");
				continue;
			}
			
			$isRecipient = false;
			$isCCrecipient = false;
			
			// check recipient roles
			if (!$isRecipient && $_TEMPLATE_ROLES && $roles) {
				$array = array_intersect_key($_TEMPLATE_ROLES, $roles); 
				$isRecipient = count($array) > 0 ? true : false;
			}
			
			// check cc recipeent roles
			if (!$isRecipient && $_TEMPLATE_CC_ROLES && $roles[$id]) {
				$array = array_intersect_key($_TEMPLATE_CC_ROLES, $roles[$id]);
				$isCCrecipient = count($array) > 0 ? true : false;
			}
			
			// recipient
			if ($isRecipient && $user['user_email']) {
				$user['link']  =  $applications['mps'] ? $_LINK_NEW_POS : $_LINK_OLD_POS;				
				$_RECIPIENTS[$id] = $user;
				continue;
			} 
			
			// cc role recipent
			if ($isCCrecipient && $_CAN_SEND_CC_MAILS) {
				$_CC_RECIPINETS[] = $user['user_email'];
			}

			// cc reciepient
			if ($isCCrecipient && $_CAN_SEND_CC_MAILS && $user['user_email_cc']) {
				$_CC_RECIPINETS[] = $user['user_email_cc'];
			}

			// deputy recipient
			if ($isCCrecipient && $_CAN_SEND_CC_MAILS && $user['user_email_deputy']) {
				$_CC_RECIPINETS[] = $user['user_email_deputy'];
			}
		}
		
	break;
}

// add recipinets ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_RECIPIENTS = $_OTHER_RECIPIENTS+$_RECIPIENTS;

if ($_RECIPIENTS) {
	foreach ($_RECIPIENTS as $i => $row) {		
		$reciepient = $this->addRecipient($i);
		$reciepient->setDataloader($row);
	}
}
else $this->console("Recipients not found");

// add cc recipoients ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_CC_RECIPINETS = array_unique(array_filter(array_merge(
	$_OTHER_CC_RECIPINETS, 
	$_CC_DECORATORS, 
	$_CC_RECIPINETS
)));

$this->addCCRecipients($_CC_RECIPINETS);
