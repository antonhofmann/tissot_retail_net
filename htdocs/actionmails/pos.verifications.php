<?php

$_APPLICATION = $this->getParam('application');
$_PERIOD = $this->getParam('period');
$_REGION = $this->getParam('region');
$_ADDRESS_TYPE = $this->getParam('addresstype');
$_CLINET = $this->getParam('client');
$_SELECTED_POS = $this->getParam('selected');

$_TEMPLATE = $this->getTemplateId();

// server vars
$protocol = Settings::init()->http_protocol.'://';
$server = $_SERVER['SERVER_NAME'];
$link = '/'.$_APPLICATION.'/posverification/data';

$db = Connector::get($_APPLICATION);
$auth = User::instance();

// check in ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$auth->id || !$_PERIOD || !$_TEMPLATE) {
	$this->console("Bad request");
	return;
}

// build query filters :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_FILTERS = array();

$_FILTERS[] = "address_type = 1";
$_FILTERS[] = "address_active = 1";
$_FILTERS[] = "(address_involved_in_planning = 1 OR address_involved_in_planning_ff = 1)";

if ($_REGION) {
	$_FILTERS[] = "country_region = $_REGION";
}

if ($_ADDRESS_TYPE && $_ADDRESS_TYPE < 4) {
	$_FILTERS[] = "address_client_type = $_ADDRESS_TYPE";
}

if ($_CLINET) {
	$_FILTERS[] = "address_id = $_CLINET";
}

if ($_SELECTED_POS) {
	$_FILTERS[] = "address_id IN ($_SELECTED_POS)";
}

$_QUERY_FILTERS = join(' AND ', $_FILTERS);


// companies :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_COMPANIES = array();

$sth = $db->prepare("
	SELECT *
	FROM db_retailnet.addresses
	INNER JOIN db_retailnet.countries ON address_country = country_id
	WHERE $_QUERY_FILTERS
");

$sth->execute();
$result = $sth->fetchAll();

if (!$result) {
	$this->console("Companies not found");
	return;
}

foreach ($result as $row) {
	$id = $row['address_id'];
	$_COMPANIES[$id] = $row;
}

// company users :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_USERS = array();
$_USER_ROLES = array();

$sth = $db->prepare("
	SELECT *
	FROM db_retailnet.users
	INNER JOIN db_retailnet.addresses ON address_id = user_address
	INNER JOIN db_retailnet.countries ON address_country = country_id
	WHERE $_QUERY_FILTERS AND user_active = 1
");

$sth->execute();
$result = $sth->fetchAll();

if (!$result) {
	$this->console("Users not found");
	return;
}

foreach ($result as $row) {
	$company = $row['user_address'];
	$user = $row['user_id'];
	$_USERS[$company][$user] = $row;
}


$sth = $db->prepare("
	SELECT
		user_role_user,
		user_role_role
	FROM db_retailnet.user_roles
	INNER JOIN db_retailnet.users on user_id = user_role_user
	INNER JOIN db_retailnet.addresses ON address_id = user_address
	INNER JOIN db_retailnet.countries ON address_country = country_id
	WHERE $_QUERY_FILTERS AND user_active = 1
");

$sth->execute();
$result = $sth->fetchAll();

if ($result) {
	foreach ($result as $row) {
		$user = $row['user_role_user'];
		$role = $row['user_role_role'];
		$_USER_ROLES[$user][$role] = $role;
	}
}

// get all confirmed verifications :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $db->prepare("
	SELECT
		posverification_address_id,
		posverification_id
	FROM posverifications
	WHERE UNIX_TIMESTAMP(posverification_date) = ?
	AND (posverification_confirm_date IS NOT NULL  OR posverification_confirm_date != '')
");

$sth->execute(array($_PERIOD));
$result = $sth->fetchAll();
$_CONFIRMED_VERIFICATIONS = _array::extract($result);


// set recipients ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_RECIPIENTS = array();
$_TEMPLATE_ROLES = $this->getRoles(); 

foreach ($_COMPANIES as $company => $data) {
		
	// ignore confirmed companies
	if ($_CONFIRMED_VERIFICATIONS[$company]) {
		$this->console("Ignore confrimed company $company");
		continue;
	}

	if (!$_USERS[$company]) {
		$this->console("Company $company has no users");
		continue;
	}

	foreach ($_USERS[$company] as $user => $row) {

		$isRecipient = false;

		// is user roles permitted for this mail template
		if ($_TEMPLATE_ROLES && $_USER_ROLES[$user]) {
			$arr = array_intersect_key($_TEMPLATE_ROLES, $_USER_ROLES[$user]); 
			$isRecipient = count($arr) > 0 ? true : false;
		}

		if ($isRecipient) {
			//$key = $company.'.'.$user;
			//$_RECIPIENTS[$key] = $row;		
			$_RECIPIENTS[$company][$user] = $row;		
		} 
		else $this->console("User $user has no permitted roles");
	}
}


// add recipients ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$_RECIPIENTS) {
	$this->console("No permitted recipients");
	return;
}

$groupMails = $this->getSetting('group');

foreach ($_RECIPIENTS as $company => $users) {

	$data = $_COMPANIES[$company];
	$data['link'] = $protocol.$server.$link."/$company/$_PERIOD";
	$data['periode'] = date('F Y', $_PERIOD);

	if ($groupMails) {
		$group = $this->addGroup($company);
		$group->setDataloader($data);
	}

	foreach ($users as $id => $user) {

		$key = $company.$id;

		// recipient
		$recipient = $this->addRecipient($key, $user, false);
		$recipient->setDataloader($data);

		if ($groupMails) {
			$group->addRecipient($recipient);
		}
	}
}
