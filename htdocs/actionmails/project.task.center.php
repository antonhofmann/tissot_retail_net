<?php 
	
$auth = User::instance();
$request = Request::instance();
$protocol = Settings::init()->http_protocol.'://';
$server = $_SERVER['SERVER_NAME'];


$_TPL = $this->getTemplateId();
$_PROJECT_ID = $this->getParam('id');

// Project
$Project = new Project();
$Project->read($_PROJECT_ID);

if (!$Project->id) {
	$this->console('Project $_PROJECT_ID not found');
	return;
}

// project order
$Order = $Project->order();

// involved roles
$roleProjectManager = 3;
$roleExternalProjectManager = 80;
$roleClient = 4;
$roleLocalRetailCordinator = 10;
$roleHqProjectManager = 19;
$roleDesignContractor = 7;
$roleDesignSupervisor = 8;
$roleOrderRetailOperator = 2;
$roleCmsApprovar = array(3,8,10);
		
// Client/order_user :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($Order->user && $roleClient) {

	if ($this->checkRole($roleClient)) {
		$this->addRecipient($Order->user);
	}
	elseif ($this->checkRole($roleClient, true)) {
		$this->addCCrecipient($Order->user);
	}
}

// Project Leader ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($Project->retail_coordinator && $roleProjectManager) {
	
	if ($this->checkRole($roleProjectManager)) {
		$this->addRecipient($Project->retail_coordinator);
	}
	elseif ($this->checkRole($roleProjectManager, true)) {
		$this->addCCrecipient($Project->retail_coordinator);
	}
}
elseif ($Project->retail_coordinator && $roleExternalProjectManager) {
	
	if ($this->checkRole($roleExternalProjectManager)) {
		$this->addRecipient($Project->retail_coordinator);
	}
	elseif ($this->checkRole($roleExternalProjectManager, true)) {
		$this->addCCrecipient($Project->retail_coordinator);
	}
}

// Local Retail Coordinator ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($Project->local_retail_coordinator && $roleLocalRetailCordinator) {
	
	if ($this->checkRole($roleLocalRetailCordinator)) {
		$this->addRecipient($Project->local_retail_coordinator);
	}
	elseif ($this->checkRole($roleLocalRetailCordinator, true)) {
		$this->addCCrecipient($Project->local_retail_coordinator);
	}
}

// Retail Operator :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
/*
if ($Order->retail_operator && $roleOrderRetailOperator) {
	
	if ($this->checkRole($roleOrderRetailOperator)) {
		$this->addRecipient($Order->retail_operator);
	}
	elseif ($this->checkRole($roleOrderRetailOperator, true)) {
		$this->addCCrecipient($Order->retail_operator);
	}
}
*/
// Design Supervisor :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($Project->design_supervisor && $roleDesignSupervisor) {
	
	if ($this->checkRole($roleDesignSupervisor)) {
		$this->addRecipient($Project->design_supervisor);
	}
	elseif ($this->checkRole($roleDesignSupervisor, true)) {
		$this->addCCrecipient($Project->design_supervisor);
	}
}

// Design Contractor :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($Project->design_contractor && $roleDesignContractor) {
	
	if ($this->checkRole($roleDesignContractor)) {
		$this->addRecipient($Project->design_contractor);
	}
	elseif ($this->checkRole($roleDesignContractor, true)) {
		$this->addCCrecipient($Project->design_contractor);
	}
}

// dataloader ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

switch ($_TPL) {
	
	case 55:
	case 56:
	case 57:
	case 58:
	case 59:
	case 60:
		$this->setDataloader(array(
			'project_number' => $Project->number,
			'order_shop_address_country_name' => $Order->shop_address_country_name,
			'address_company' => $Order->shop_address_company,
			'link' => $protocol.$server."/user/project_task_center.php?pid=".$Project->id,
			'attachments' => $this->getParam('attachment_text'),
			'step' => $this->getParam('step')
		));
	break;

	case 65:
		$this->setDataloader(array(
			'project_number' => $Project->number,
			'order_shop_address_country_name' => $Order->shop_address_country_name,
			'address_company' => $Order->shop_address_company,
			'link' => $protocol.$server."/user/project_view_attachments.php?pid=".$Project->id,
			'attachments' => $this->getParam('attachment_text'),
			'step' => $this->getParam('step')
		));
	break;

	case 92:
		$this->setDataloader(array(
			'project_number' => $Project->number,
			'order_shop_address_country_name' => $Order->shop_address_country_name,
			'address_company' => $Order->shop_address_company,
			'link' => $protocol.$server."/user/project_view_traffic_data.php?pid=".$Project->id,
			'attachments' => $this->getParam('attachment_text'),
			'item_list' => $this->getParam('item_list'),
			'step' => $this->getParam('step')
		));
	break;
}


// sendmail ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$this->populate(true);
$recipients = $this->getRecipients();

// track mail orders :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if ($recipients && $Order->id) {

	$ccRecipients = array();

	foreach ($recipients as $id => $recipient) {
		
		if (1== 1 or $recipient->isSendMail()) {
			
			$Order->orderMailTrack(array(
				'order_mail_order'       => $Order->id,
				'order_mail_user'        => $recipient->id,
				'order_mail_from_user'   => $auth->id,
				'order_mail_text'        => $recipient->getBody(true),
				'order_mail_is_cc'       => 0,
				'order_mail_template_id' => $_TPL,
				'user_created'           => $auth->user_login,
				'date_created'           => date('Y-m-d H:i:s')
			));
			
			$this->console("Order mail track for recipient $recipient->id ");

			$cc = $recipient->getCCrecipients();

			if ($cc) {
				$ccRecipients[$id]['recipients'] = $cc;
				$ccRecipients[$id]['content'] = $recipient->getBody(true);
			}
		}
	}

	if ($ccRecipients) {

		$usr = new User();

		foreach ($ccRecipients as $i => $recipients) {

			foreach ($recipients['recipients'] as $email) {
				
				$usr->getActiveUserFromEmail($email);

				if (!$usr->id) continue;

				$Order->orderMailTrack(array(
					'order_mail_order'       => $Order->id,
					'order_mail_user'        => $usr->id,
					'order_mail_from_user'   => $auth->id,
					'order_mail_text'        => $recipients['content'],
					'order_mail_is_cc'       => 1,
					'order_mail_template_id' => $_TPL,
					'user_created'           => $auth->user_login,
					'date_created'           => date('Y-m-d H:i:s')
				));

				$this->console("Order CC mail track for recipient $usr->id ");
			}
		}
	}
}
	