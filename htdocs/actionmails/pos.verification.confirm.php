<?php

$_APPLICATION = $this->getParam('application');
$_PERIOD = $this->getParam('period');
$_ID = $this->getParam('id');

$_TEMPLATE = $this->getTemplateId();
$_TEMPLATE_ROLES = $this->getRoles();

// server vars
$protocol = Settings::init()->http_protocol.'://';
$server = $_SERVER['SERVER_NAME'];
$link = '/'.$_APPLICATION.'/posverification/data';

$db = Connector::get($_APPLICATION);
$auth = User::instance();


// check in ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$auth->id || !$_PERIOD || !$_TEMPLATE) {
	$this->console("Bad request");
	return;
}

// company :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $db->prepare("
	SELECT *
	FROM db_retailnet.addresses
	INNER JOIN db_retailnet.countries ON address_country = country_id
	WHERE address_id = ?
");

$sth->execute(array($_ID));
$_COMPANY = $sth->fetch();

if (!$_COMPANY) {
	$this->console("Company $_ID not found");
	return;
}

// company users :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_USERS = array();
$_USERS_ROLES = array();

$sth = $db->prepare("
	SELECT *
	FROM db_retailnet.users
	INNER JOIN db_retailnet.addresses ON address_id = user_address
	INNER JOIN db_retailnet.countries ON address_country = country_id
	WHERE address_id = ? AND user_active = 1
");

$sth->execute(array($_ID));
$result = $sth->fetchAll();

if (!$result) {
	$this->console("Users not found");
	return;
}

foreach ($result as $row) {
	$user = $row['user_id'];
	$_USERS[$user] = $row;
}

if ($_TEMPLATE_ROLES) {

	$sth = $db->prepare("
		SELECT user_role_user, user_role_role
		FROM db_retailnet.user_roles
		INNER JOIN db_retailnet.users ON user_id = user_role_user
		WHERE user_address = ? AND user_active = 1
	");

	$sth->execute(array($_ID));
	$result = $sth->fetchAll();

	if ($result) {
		foreach ($result as $row) {
			$user = $row['user_role_user'];
			$role = $row['user_role_role'];
			$_USERS_ROLES[$user][$role] = $role;
		}
	}
}


// dataloader ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$data = array_merge($_COMPANY, array(
	'periode' => date('F Y', $_PERIOD),
	'month' => date('F', $_PERIOD),
	'year' => date('Y', $_PERIOD),
	'link' => $protocol.$server.$link."/$_ID/$_PERIOD",
	'type' => $_APPLICATION == 'mpsflikflak' ? "Flik Flak" : "tissot"
));

$this->setDataloader($data);


// company recipients ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

foreach ($_USERS as $user => $row) {

	$isRecipient = false;

	// is user roles permitted for this mail template
	if ($_TEMPLATE_ROLES && $_USERS_ROLES[$user]) {
		$arr = array_intersect_key($_TEMPLATE_ROLES, $_USERS_ROLES[$user]); 
		$isRecipient = count($arr) > 0 ? true : false;
	}

	if ($isRecipient) {
		$recipient = $this->addRecipient($user);	
		$recipient->setDataloader($row);
	} 
	else $this->console("User $user has no permitted roles");
}

if (!$this->hasRecipients()) {
	$this->console("No permitted recipients");
	return;
}


// regional responsibles for the confirmation of the pos verification ::::::::::::::::::::::::::::::

$_OTHER_RECIPIENTS = array();
$_OTHER_CC_RECIPIENTS = array();

$sth = $db->prepare("
	SELECT mail_template_other_recipient_name AS name
	FROM db_retailnet.mail_template_other_recipients
	WHERE mail_template_other_recipient_template_id = ? 
	AND TRIM(mail_template_other_recipient_name) IN ('regional_responsible', 'regional_responsible_cc')
");

$sth->execute(array($_TEMPLATE));
$result = $sth->fetch();

if ($result['name']) {

	$regionalResponsible = $result['name'];

	$sth = $db->prepare("
		SELECT DISTINCT user_id, user_firstname, user_name, user_email, user_email_cc, user_email_deputy, address_company
		FROM db_retailnet.user_company_responsibles
		INNER JOIN db_retailnet.users ON user_id = user_company_responsible_user_id
		INNER JOIN db_retailnet.addresses ON address_id = user_company_responsible_address_id
		WHERE user_active = 1
		AND address_id = ?
		AND (user_company_responsible_retail = 1 or user_company_responsible_wholsale = 1)
	");

	$sth->execute(array($_ID));
	$result = $sth->fetchAll();
	
	if ($result) {
		foreach ($result as $usr) {
			if ($regionalResponsible=='regional_responsible_cc') {
				$this->addCCRecipient($usr['user_id']);
			}
			else {
				$recipient = $this->addRecipient($usr['user_id']);	
				$recipient->setDataloader($usr);
			}
		}
	}
	else $this->console("Users for regional responsible not found");
}
else {
	$this->console("Regional responsible is not permitted");
}
