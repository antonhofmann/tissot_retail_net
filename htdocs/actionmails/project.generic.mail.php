<?php
/**
 * This is the actionmail file for mails that call responsible users to enter
 * realistic dates for opening- taking over or starting lease of new or existing POS.
 * It adds the required role recipients and tracks sent mails to order.
 *
 * @todo  add a mailtemplate flag (or flag in ProjectMail class) to track orders. we shouldn't check that
 * for each mail separately
 */

 // involved roles
$_ROLE_PROJECT_MANAGER = 3;
$_ROLE_EXTERNAL_PROJECT_MANAGER = 80;
$_ROLE_LOCAL_RETAIL_CORDINATOR = 10;

$auth = User::instance();
$request = Request::instance();

$_TPL = $this->getTemplateId();
$_PROJECT_ID = $this->getParam('projectID');

// load required data
$Project = new Project();
$Project->read($_PROJECT_ID);

if (!$Project->id) {
	$this->console('Project $_PROJECT_ID not found');
	return;
}

// project order
$Order = $Project->order();

// set dataloader
$this->setDataloader(array(
	'project_number' => $Project->number,
	'country' => $Project->getCountry()->name . ", " . $Order->shop_address_place,
	'shop_address' => $Order->shop_address_company,
	'link' => $this->getParam('link')
));

// template roles
$_TEMPLATE_ROLES = $this->getRoles() ?: array();
$_TEMPLATE_CC_ROLES = $this->getRoles(true) ?: array();

/**
 * Add all standard project roles. This should be done by ActionMail, not every actionfile on it's own.
 * We will have to refactor this some day.
 * @todo  only fetch the user object if the role is defined as recipient or cc-recipient
 **************************************************************************************************/

if ($_TEMPLATE_ROLES) {
	
	// client
	$Client = $Order->getUser();

	
	if ($Client instanceof User && $this->checkRole(Role::STANDARD_USER)) {
		$recipient = $this->addRecipient($Client);

		if ($this->checkRole($_ROLE_PROJECT_MANAGER) || $this->checkRole($_ROLE_PROJECT_MANAGER, true)) {
			$recipient->addCCRecipient($Project->retail_coordinator);
		}
		elseif ($this->checkRole($_ROLE_EXTERNAL_PROJECT_MANAGER) || $this->checkRole($_ROLE_EXTERNAL_PROJECT_MANAGER, true)) {
			$recipient->addCCRecipient($Project->retail_coordinator);
		}
		if ($this->checkRole($_ROLE_LOCAL_RETAIL_CORDINATOR) || $this->checkRole($_ROLE_LOCAL_RETAIL_CORDINATOR, true)) {
			$recipient->addCCRecipient($Project->local_retail_coordinator);
		}
	}

	// brand manager
	$BrandManager = $Order->getBrandManager();
	
	if ($BrandManager instanceof User && $this->checkRole(Role::BRAND_MANAGER)) {
		$this->addRecipient($BrandManager);
	}
	
	// project leader
	if ($Project->retail_coordinator && $this->checkRole(Role::PROJECT_LEADER)) {
		$this->addRecipient($Project->retail_coordinator);
	}
	
	// regional sales manager
	$RegionalSalesManager = $Order->getRegionalSalesManager();
	
	if ($RegionalSalesManager instanceof User && $this->checkRole(Role::REGIONAL_SALES_MANAGER)) {
		$this->addRecipient($RegionalSalesManager);
	}

	// local retail coordinator
	$RetailCoordinator = $Project->getRetailCoordinator();
	
	if ($RetailCoordinator instanceof User && $this->checkRole(Role::LOCAL_RETAIL_COORDINATOR)) {
		$this->addRecipient($RetailCoordinator);
	}	

	// design contractor
	$DesignContractor = $Project->getDesignContractor();
	
	if ($DesignContractor instanceof User && $this->checkRole(Role::DESIGN_CONTRACTOR)) {
		$this->addRecipient($DesignContractor);
	}

	// design supervisor
	$DesignSupervisor = $Project->getDesignSupervisor();
	
	if ($DesignSupervisor instanceof User && $this->checkRole(Role::DESIGN_SUPERVISOR)) {
		$this->addRecipient($DesignSupervisor);
	}

	// retail operator
	$RetailOperator = $Order->getRetailOperator();
	
	if ($RetailOperator instanceof User && $this->checkRole(Role::LOGISTICS_COORDINATOR)) {
		$this->addRecipient($RetailOperator);
	}
}


// sendmail ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$this->populate(true);
$recipients = $this->getRecipients();

$sender = $this->getSender();

// track mail orders :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
if ($recipients && $Order->id) {

	$ccRecipients = array();


	$state= '';
	if($_TPL == 155) {
		$state= 1;
	}

	foreach ($recipients as $id => $recipient) {
		
		if (1==1 or $recipient->isSendMail()) {
			$Order->orderMailTrack(array(
				'order_mail_order'       => $Order->id,
				'order_mail_user'        => $recipient->id,
				'order_mail_from_user'   => $sender->id,
				'order_mail_text'        => $recipient->getBody(true),
				'order_mail_is_cc'       => 0,
				'order_mail_template_id' => $_TPL,
				'order_mail_order_state' => $state,
				'user_created'           => $sender->login,
				'date_created'           => date('Y-m-d H:i:s')
			));
			
			$this->console("Order mail track for recipient $recipient->id ");

			$cc = $recipient->getCCrecipients();

			if ($cc) {
				$ccRecipients[$id]['recipients'] = $cc;
				$ccRecipients[$id]['content'] = $recipient->getBody(true);
			}
		}
	}

	if ($ccRecipients) {

		$usr = new User();

		foreach ($ccRecipients as $i => $recipients) {

			foreach ($recipients['recipients'] as $email) {
				
				$usr->getActiveUserFromEmail($email);

				if (!$usr->id) continue;

				$Order->orderMailTrack(array(
					'order_mail_order'       => $Order->id,
					'order_mail_user'        => $usr->id,
					'order_mail_from_user'   => $sender->id,
					'order_mail_text'        => $recipients['content'],
					'order_mail_is_cc'       => 1,
					'order_mail_template_id' => $_TPL,
					'user_created'           => $sender->login,
					'date_created'           => date('Y-m-d H:i:s')
				));

				$this->console("Order CC mail track for recipient $usr->id ");
			}
		}
	}
}

