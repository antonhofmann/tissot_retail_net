<?php

$_APPLICATION = $this->getParam('application');
$_ORDERSHEETS = $this->getParam('ordersheets');

$protocol = Settings::init()->http_protocol.'://';
$_LINK = $protocol.'://'.$_SERVER['SERVER_NAME']."/$_APPLICATION/ordersheets/data";

$db = Connector::get($_APPLICATION);	
$auth = User::instance();

// check in ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

if (!$auth->id || !$_ORDERSHEETS) {
	$this->console("Bad request");
	return;
}

// filters :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_FILTERS = array();

$selected = is_array($_ORDERSHEETS) ? join(',', $_ORDERSHEETS) : $_ORDERSHEETS;
$_FILTERS[] = "mps_ordersheet_id IN ($selected)";

$_QUERY_FILTER = join(' AND ', $_FILTERS);


// ordersheets :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_ORDERSHEETS = array();

$sth =$db->prepare("
	SELECT DISTINCT
		mps_ordersheet_id,
		address_id,
		address_company,
		DATE_FORMAT(mps_ordersheet_openingdate, '%d.%m.%Y') AS mps_ordersheet_openingdate,
		DATE_FORMAT(mps_ordersheet_closingdate, '%d.%m.%Y') AS mps_ordersheet_closingdate,
		mps_mastersheet_name
	FROM mps_ordersheets
	INNER JOIN mps_mastersheets ON mps_mastersheet_id = mps_ordersheet_mastersheet_id
	INNER JOIN db_retailnet.addresses ON mps_ordersheet_address_id = address_id
	WHERE $_QUERY_FILTER
");

$sth->execute();
$result = $sth->fetchAll();

if (!$result) {
	$this->console("Order sheets not found");
	return;
}

foreach ($result as $row) {
	$key = $row['mps_ordersheet_id'];
	$row['link'] = $_LINK.'/'.$key;
	$_ORDERSHEETS[$key] = $row;
}


// users :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_USERS = array();
$_USER_ROLES = array();

$sth =$db->prepare("
	SELECT DISTINCT
		user_id,
		user_firstname,
		user_name,
		user_email,
		user_email_cc,
		user_email_deputy,
		user_address
	FROM mps_ordersheets
	INNER JOIN db_retailnet.addresses ON mps_ordersheet_address_id = address_id
	INNER JOIN db_retailnet.users ON user_address = address_id
	WHERE $_QUERY_FILTER AND user_active = 1
");

$sth->execute();
$result = $sth->fetchAll();

if (!$result) {
	$this->console("Users not found");
	return;
}

foreach ($result as $row) {
	$user = $row['user_id'];
	$company = $row['user_address'];
	$_USERS[$company][$user] = $row;
}

// user roels
$sth =$db->prepare("
	SELECT user_role_user, user_role_role
	FROM mps_ordersheets
	INNER JOIN db_retailnet.addresses ON mps_ordersheet_address_id = address_id
	INNER JOIN db_retailnet.users ON user_address = address_id
	INNER JOIN db_retailnet.user_roles ON user_role_user = user_id
	WHERE $_QUERY_FILTER AND user_active = 1
");

$sth->execute();
$result = $sth->fetchAll();

if ($result) {
	foreach ($result as $row) {
		$user = $row['user_role_user'];
		$role = $row['user_role_role'];
		$_USER_ROLES[$user][$role] = $role;
	}
}

// get system roles ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$sth = $db->prepare("
	SELECT role_id, role_name
	FROM application_roles
	INNER JOIN roles ON role_id = application_role_role
	INNER JOIN applications ON application_id = application_role_application
	WHERE application_shortcut = ?
");

// application roles
$sth->execute(array($_APPLICATION));
$result = $sth->fetchAll();
$_APPLICATION_ROLES = _array::extract($result);

// template roles
$_TEMPLATE_ROLES = $this->getRoles() ?: array();
$_TEMPLATE_CC_ROLES = $this->getRoles(true) ?: array();


// recipients ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$_RECIPIENTS = array();
$_CC_RECIPIENTS = array();

foreach ($_ORDERSHEETS as $ordersheet => $data) {
		
	$company = $data['address_id'];
	$masterSheetName = $data['mps_mastersheet_name'];

	if (!$_USERS[$company]) {
		$this->console("Company $company has no users");
		continue;
	}

	foreach ($_USERS[$company] as $user => $row) {

		$isRecipient = false;
		$isRecipientCC = false;

		$userRoles = $_USER_ROLES[$user] ?: array();
			
		// check application role
		if ($_APPLICATION_ROLES && $userRoles) {
			$arr = array_intersect_key($_APPLICATION_ROLES, $userRoles);
			$isAppRecipient = count($arr) > 0 ? true : false;
		}
		else $isAppRecipient = true;

		if (!$isAppRecipient) {
			$this->console("User $user has not application permitted roles");
			continue;
		}
		
		// check recipient roles
		if ($_TEMPLATE_ROLES && $userRoles) {
			$arr = array_intersect_key($_TEMPLATE_ROLES, $userRoles);
			$isRecipient = count($arr) > 0 ? true : false;
		}
		
		// cc recipient
		if (!$isRecipient && $_TEMPLATE_CC_ROLES && $userRoles) {
			$arr = array_intersect_key($_TEMPLATE_CC_ROLES, $userRoles);
			$isRecipientCC = count($arr) > 0 ? true : false;
		}
			
		if ($isRecipient) {
			$_RECIPIENTS[$ordersheet][$user] = $row;
		}
		elseif ($isRecipientCC && $row['user_email']) {			
			$_CC_RECIPIENTS[$ordersheet][] = $row['user_email'];
		} 
		
		if (!$isRecipient && !$isRecipientCC) {
			$this->console("User $user has not permitted roles.");
		}
	}
}

				
// recipients ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
if (!$_RECIPIENTS) {
	$this->console("No permitted recipients");
	return;
}

$groupMails = $this->getSetting('group');

foreach ($_RECIPIENTS as $ordersheet => $users) {
	
	if ($groupMails) {
		$group = $this->addGroup($ordersheet);
		$group->setDataloader($_ORDERSHEETS[$ordersheet]);
	}

	foreach ($users as $user => $data) {
		
		$key = $ordersheet.'.'.$user; 
		$recipient = $this->addRecipient($key, $data, false);
		$recipient->setDataloader($_ORDERSHEETS[$ordersheet]);
		$recipient->addCCrecipients($_CC_RECIPIENTS[$ordersheet], false);

		if ($groupMails) {
			$group->addRecipient($recipient);
		}
	}
}
