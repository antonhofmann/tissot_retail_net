<?php
/**
 * This is the actionmail file for pos lease renewal and resignation notices
 * It sets either the brand- or retail manager as recipient and loads the 
 * appropriate data.
 */

// get params
$posAddressClientID = $this->getParam('posAddressClientID');
$posLocation = $this->getParam('posLocation');
$noRetailManager = $this->getParam('noRetailManager');
$posAddressID = $this->getParam('posAddressID');

$noRetailManager = isset($noRetailManager) ? $noRetailManager : false;
$posAddressID = isset($posAddressID) ? $posAddressID : null;

// template roles
$TemplateRoles = $this->getRoles() ?: array();
$TemplateCCroles = $this->getRoles(true) ?: array();

$UserMapper = new User_DataMapper();

// add recipients defined by role
if (is_array($TemplateRoles) && $TemplateRoles) {
	
	// local retail coordinator
	$LocalRetailCoordinator = $UserMapper->getLocalRetailCoordinator($posAddressClientID);
	
	if ($LocalRetailCoordinator instanceof User && $this->checkRole(Role::LOCAL_RETAIL_COORDINATOR)) {
		$this->addRecipient($LocalRetailCoordinator);
	}

	// get brand & retail manager
	$BrandManager = $UserMapper->getBrandManager($posAddressClientID);
	
	if ($BrandManager === false) {
		$BrandManager = $UserMapper->getAffiliateBrandManager($posAddressClientID);
	}
	
	$retailManagers = $UserMapper->getRetailManager($posAddressClientID);

	// if there's a brand manager and he's defined as recipient or cc
	$recipient = null;
	
	if ($BrandManager instanceof User) {
		
		
		// add brand manager as recipient
		if ($this->checkRole(Role::BRAND_MANAGER)) {
			
			$recipient = $this->addRecipient($BrandManager->id, $BrandManager->data);
			
			// add retail manager(s) as cc
			if ($this->checkRole(Role::RETAIL_MANAGER, true)) {
				foreach ($retailManagers as $RetailManager) {
					$this->addCCRecipient($RetailManager->id, $RetailManager->data);
				}
			}
		}
		// add brand manager as CC
		else if ($this->checkRole(Role::BRAND_MANAGER, true)) {
			$this->addCCRecipient($BrandManager->id, $BrandManager->data);
		}
	}
	// otherwise set retail manager(s) as recipient(s)
	else {
		if (!$noRetailManager && ($this->checkRole(Role::RETAIL_MANAGER) || $this->checkRole(Role::RETAIL_MANAGER, true))) {
			foreach ($retailManagers as $RetailManager) {
				$recipient = $this->addRecipient($RetailManager->id, $RetailManager->data);
			}
		}
	}
}

// if recipient wasn't set by a predefined role, it was set from outside of actionfile
// so we get it here
$recipient = $recipient ?: $this->getFirstRecipient();


if (!$recipient) {
	$this->console("Recipient not found.");
	return;
}

// add project owner if we have a pos address
if ($this->getParam('addProjectOwner') && !is_null($posAddressID)) {
	
	$ProjectMailDatamapper = new Project_Mails_Datamapper;
	
	$ProjectOwner = $ProjectMailDatamapper->getOwnerOfLatestProject(
		$posAddressID,
		$posAddressClientID
	);
	$this->addCCRecipient($ProjectOwner);
}

$existingData = $this->getDataloader();

$data = array(
	'name' => $recipient->getName(),
	'location' => $posLocation,
	'closing_assessment_link' => URLBuilder::closingAssessments(),
);

if (!isset($existingData['link'])) {
	$data['link'] = URLBuilder::mpsPOSAddress($posAddressID);
}

$this->setDataloader($data);
