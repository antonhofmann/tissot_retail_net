<?php 
/**
 * This is the actionmail file for mails that call for cms completion
 * It loads the project and order data the mail needs and adds a few
 * roles as (cc) recipients if so configured in the mail template.
 * Sent mails are tracked and tasks created for the recipient.
 */

$auth = User::instance();
$request = Request::instance();
$db = Connector::get();

$_TPL = $this->getTemplateId();
$_PROJECT_ID = $this->getParam('projectID');
$_RECIPIENT = $this->getParam('recipient');
$_LINK = $this->getParam('link');

// sender
$sender = $this->getSender();

// roles
$roleProjectManager = 3;
$roleExternalProjectManager = 80;
$roleClient = 4;
$roleLocalRetailCordinator = 10;
$roleHqProjectManager = 19;
$roleDesignContractor = 7;
$roleDesignSupervisor = 8;
$roleOrderRetailOperator = 2;


// template roles
$_TEMPLATE_ROLES = $this->getRoles() ?: array();
$_TEMPLATE_CC_ROLES = $this->getRoles(true) ?: array();


// project
$Project = new Project();
$Project->read($_PROJECT_ID);


if (!$Project->id) {
	$this->console('Project $_PROJECT_ID not found');
	return;
}

// project order
$Order = $Project->order();

// project country
$ShopCountry = $Project->getCountry();


// assemble mail
$this->setDataloader(array(
	'project_number' => $Project->number,
	'country' => $ShopCountry->name,
	'shop_address' => $Order->shop_address_company,
	'link' => $_LINK,
));

// add recipients ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
$this->addRecipient($_RECIPIENT);


// Retail Operator :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
/*
if ($Order->retail_operator && $roleOrderRetailOperator) {
	
	if ($this->checkRole($roleOrderRetailOperator)) {
		$this->addRecipient($Order->retail_operator);
	}
	elseif ($this->checkRole($roleOrderRetailOperator, true)) {
		$this->addCCrecipient($Order->retail_operator);
	}
}
*/

// sendmail ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$this->populate(true);
$recipients = $this->getRecipients();


// track mail orders :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
if ($recipients && $Order->id) {
	$ccRecipients = array();

	foreach ($recipients as $id => $recipient) {
		
		if (1== 1 or $recipient->isSendMail()) {
			
			$Order->orderMailTrack(array(
				'order_mail_order'       => $Order->id,
				'order_mail_user'        => $recipient->id,
				'order_mail_from_user'   => $sender->id,
				'order_mail_text'        => $recipient->getBody(true),
				'order_mail_is_cc'       => 0,
				'order_mail_template_id' => $_TPL,
				'user_created'           => $sender->login,
				'date_created'           => date('Y-m-d H:i:s')
			));
			
			$this->console("Order mail track for recipient $recipient->id ");

			$cc = $recipient->getCCrecipients();

			if ($cc) {
				$ccRecipients[$id]['recipients'] = $cc;
				$ccRecipients[$id]['content'] = $recipient->getBody(true);
			}
		}
	}

	if ($ccRecipients) {

		$usr = new User();

		foreach ($ccRecipients as $i => $recipients) {

			foreach ($recipients['recipients'] as $email) {
				
				$usr->getActiveUserFromEmail($email);

				if (!$usr->id) continue;

				$Order->orderMailTrack(array(
					'order_mail_order'       => $Order->id,
					'order_mail_user'        => $usr->id,
					'order_mail_from_user'   => $sender->id,
					'order_mail_text'        => $recipients['content'],
					'order_mail_is_cc'       => 1,
					'order_mail_template_id' => $_TPL,
					'user_created'           => $sender->login,
					'date_created'           => date('Y-m-d H:i:s')
				));

				$this->console("Order CC mail track for recipient $usr->id ");
			}
		}
	}
}
