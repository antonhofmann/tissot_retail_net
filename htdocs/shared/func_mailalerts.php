<?php
/********************************************************************

    func_mailalerts.php

    Various functions for Mail Alerts

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2008-11-16
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2008-11-16
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

$mimetypes = array();
$mimetype['ai']='application/postscript';
$mimetype['aif']='audio/x-aiff';
$mimetype['aifc']='audio/x-aiff';
$mimetype['aiff']='audio/x-aiff';
$mimetype['arj']='application/x-arj-compressed';
$mimetype['asc']='text/plain';
$mimetype['asc txt']='text/plain';
$mimetype['asf']='video/x-ms-asf';
$mimetype['asx']='video/x-ms-asx';
$mimetype['au']='audio/basic';
$mimetype['au']='audio/ulaw';
$mimetype['avi']='video/x-msvideo';
$mimetype['bat']='application/x-msdos-program';
$mimetype['bcpio']='application/x-bcpio';
$mimetype['bin']='application/octet-stream';
$mimetype['c']='text/plain';
$mimetype['cc']='text/plain';
$mimetype['ccad']='application/clariscad';
$mimetype['cdf']='application/x-netcdf';
$mimetype['class']='application/octet-stream';
$mimetype['cod']='application/vnd.rim.cod';
$mimetype['com']='application/x-msdos-program';
$mimetype['cpio']='application/x-cpio';
$mimetype['cpt']='application/mac-compactpro';
$mimetype['csh']='application/x-csh';
$mimetype['css']='text/css';
$mimetype['dcr']='application/x-director';
$mimetype['deb']='application/x-debian-package';
$mimetype['dir']='application/x-director';
$mimetype['dl']='video/dl';
$mimetype['dms']='application/octet-stream';
$mimetype['doc']='application/msword';
$mimetype['drw']='application/drafting';
$mimetype['dvi']='application/x-dvi';
$mimetype['dwg']='application/acad';
$mimetype['dxf']='application/dxf';
$mimetype['dxr']='application/x-director';
$mimetype['eps']='application/postscript';
$mimetype['etx']='text/x-setext';
$mimetype['exe']='application/octet-stream';
$mimetype['exe']='application/x-msdos-program';
$mimetype['ez']='application/andrew-inset';
$mimetype['f']='text/plain';
$mimetype['f90']='text/plain';
$mimetype['fli']='video/fli';
$mimetype['fli']='video/x-fli';
$mimetype['flv']='video/flv';
$mimetype['gif']='image/gif';
$mimetype['gl']='video/gl';
$mimetype['gtar']='application/x-gtar';
$mimetype['gz']='application/x-gunzip';
$mimetype['gz']='application/x-gzip';
$mimetype['h']='text/plain';
$mimetype['hdf']='application/x-hdf';
$mimetype['hh']='text/plain';
$mimetype['hqx']='application/mac-binhex40';
$mimetype['htm']='text/html';
$mimetype['html']='text/html';
$mimetype['html htm']='text/html';
$mimetype['ice']='x-conference/x-cooltalk';
$mimetype['ief']='image/ief';
$mimetype['iges']='model/iges';
$mimetype['igs']='model/iges';
$mimetype['ips']='application/x-ipscript';
$mimetype['ipx']='application/x-ipix';
$mimetype['jad']='text/vnd.sun.j2me.app-descriptor';
$mimetype['jar']='application/java-archive';
$mimetype['jpe']='image/jpeg';
$mimetype['jpeg']='image/jpeg';
$mimetype['jpg']='image/jpeg';
$mimetype['js']='application/x-javascript';
$mimetype['kar']='audio/midi';
$mimetype['latex']='application/x-latex';
$mimetype['lha']='application/octet-stream';
$mimetype['lsp']='application/x-lisp';
$mimetype['lzh']='application/octet-stream';
$mimetype['m']='text/plain';
$mimetype['m3u']='audio/x-mpegurl';
$mimetype['man']='application/x-troff-man';
$mimetype['me']='application/x-troff-me';
$mimetype['mesh']='model/mesh';
$mimetype['mid']='audio/midi';
$mimetype['midi']='audio/midi';
$mimetype['mif']='application/vnd.mif';
$mimetype['mif']='application/x-mif';
$mimetype['mime']='www/mime';
$mimetype['mov']='video/quicktime';
$mimetype['movie']='video/x-sgi-movie';
$mimetype['mp2']='audio/mpeg';
$mimetype['mp2']='video/mpeg';
$mimetype['mp3']='audio/mpeg';
$mimetype['mpe']='video/mpeg';
$mimetype['mpeg']='video/mpeg';
$mimetype['mpg']='video/mpeg';
$mimetype['mpga']='audio/mpeg';
$mimetype['ms']='application/x-troff-ms';
$mimetype['msh']='model/mesh';
$mimetype['nc']='application/x-netcdf';
$mimetype['oda']='application/oda';
$mimetype['ogg']='application/ogg';
$mimetype['ogm']='application/ogg';
$mimetype['pbm']='image/x-portable-bitmap';
$mimetype['pdb']='chemical/x-pdb';
$mimetype['pdf']='application/pdf';
$mimetype['pgm']='image/x-portable-graymap';
$mimetype['pgn']='application/x-chess-pgn';
$mimetype['pgp']='application/pgp';
$mimetype['pl']='application/x-perl';
$mimetype['pm']='application/x-perl';
$mimetype['png']='image/png';
$mimetype['pnm']='image/x-portable-anymap';
$mimetype['pot']='application/mspowerpoint';
$mimetype['ppm']='image/x-portable-pixmap';
$mimetype['pps']='application/mspowerpoint';
$mimetype['ppt']='application/mspowerpoint';
$mimetype['ppz']='application/mspowerpoint';
$mimetype['pre']='application/x-freelance';
$mimetype['prt']='application/pro_eng';
$mimetype['ps']='application/postscript';
$mimetype['qt']='video/quicktime';
$mimetype['ra']='audio/x-realaudio';
$mimetype['ram']='audio/x-pn-realaudio';
$mimetype['rar']='application/x-rar-compressed';
$mimetype['ras']='image/cmu-raster';
$mimetype['ras']='image/x-cmu-raster';
$mimetype['rgb']='image/x-rgb';
$mimetype['rm']='audio/x-pn-realaudio';
$mimetype['roff']='application/x-troff';
$mimetype['rpm']='audio/x-pn-realaudio-plugin';
$mimetype['rtf']='application/rtf';
$mimetype['rtf']='text/rtf';
$mimetype['rtx']='text/richtext';
$mimetype['scm']='application/x-lotusscreencam';
$mimetype['set']='application/set';
$mimetype['sgm']='text/sgml';
$mimetype['sgml']='text/sgml';
$mimetype['sh']='application/x-sh';
$mimetype['shar']='application/x-shar';
$mimetype['silo']='model/mesh';
$mimetype['sit']='application/x-stuffit';
$mimetype['skd']='application/x-koan';
$mimetype['skm']='application/x-koan';
$mimetype['skp']='application/x-koan';
$mimetype['skt']='application/x-koan';
$mimetype['smi']='application/smil';
$mimetype['smil']='application/smil';
$mimetype['snd']='audio/basic';
$mimetype['sol']='application/solids';
$mimetype['spl']='application/x-futuresplash';
$mimetype['src']='application/x-wais-source';
$mimetype['step']='application/STEP';
$mimetype['stl']='application/SLA';
$mimetype['stp']='application/STEP';
$mimetype['sv4cpio']='application/x-sv4cpio';
$mimetype['sv4crc']='application/x-sv4crc';
$mimetype['swf']='application/x-shockwave-flash';
$mimetype['t']='application/x-troff';
$mimetype['tar']='application/x-tar';
$mimetype['tar.gz']='application/x-tar-gz';
$mimetype['tcl']='application/x-tcl';
$mimetype['tex']='application/x-tex';
$mimetype['texi']='application/x-texinfo';
$mimetype['texinfo']='application/x-texinfo';
$mimetype['tgz']='application/x-tar-gz';
$mimetype['tif']='image/tiff';
$mimetype['tif tiff']='image/tiff';
$mimetype['tiff']='image/tiff';
$mimetype['tr']='application/x-troff';
$mimetype['tsi']='audio/TSP-audio';
$mimetype['tsp']='application/dsptype';
$mimetype['tsv']='text/tab-separated-values';
$mimetype['txt']='text/plain';
$mimetype['unv']='application/i-deas';
$mimetype['ustar']='application/x-ustar';
$mimetype['vcd']='application/x-cdlink';
$mimetype['vda']='application/vda';
$mimetype['viv']='video/vnd.vivo';
$mimetype['vivo']='video/vnd.vivo';
$mimetype['vrm']='x-world/x-vrml';
$mimetype['vrml']='model/vrml';
$mimetype['vrml']='x-world/x-vrml';
$mimetype['wav']='audio/x-wav';
$mimetype['wax']='audio/x-ms-wax';
$mimetype['wma']='audio/x-ms-wma';
$mimetype['wmv']='video/x-ms-wmv';
$mimetype['wmx']='video/x-ms-wmx';
$mimetype['wrl']='model/vrml';
$mimetype['wvx']='video/x-ms-wvx';
$mimetype['xbm']='image/x-xbitmap';
$mimetype['xlc']='application/vnd.ms-excel';
$mimetype['xll']='application/vnd.ms-excel';
$mimetype['xlm']='application/vnd.ms-excel';
$mimetype['xls']='application/excel';
$mimetype['xls']='application/vnd.ms-excel';
$mimetype['xlw']='application/vnd.ms-excel';
$mimetype['xml']='text/xml';
$mimetype['xpm']='image/x-xpixmap';
$mimetype['xwd']='image/x-xwindowdump';
$mimetype['xyz']='chemical/x-pdb';
$mimetype['zip']='application/x-zip-compressed';
$mimetype['zip']='application/zip';

/********************************************************************
    append records to table order_mails
*********************************************************************/
function append_mail($order_id, $recepient_id, $user_id, $text, $order_state_code, $type, $order_revision=0)
{

    $text = str_replace("\n\n", "\n", $text);
	$text = str_replace("\n\n", "\n", $text);

	$sql = "select order_state_id " .
           "from order_states ".
           "inner join order_state_groups on order_state_group = order_state_group_id ".
           "where order_state_code = " . dbquote($order_state_code) .
           "    and order_state_group_order_type = " .$type;

    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);

    $order_mail_fields = array();
    $order_mail_values = array();
    
    // insert record into table tasks

    $order_mail_fields[] = "order_mail_order";
    $order_mail_values[] = $order_id;

    if ($recepient_id != "")
    {
        $order_mail_fields[] = "order_mail_user";
        $order_mail_values[] = $recepient_id;
    }

    $order_mail_fields[] = "order_mail_from_user";
    $order_mail_values[] = $user_id;

    $order_mail_fields[] = "order_mail_text";
    $order_mail_values[] = dbquote($text);

    if ($order_state_code != "")
    {
        $order_mail_fields[] = "order_mail_order_state";
        $order_mail_values[] = $row["order_state_id"];
    }

	$order_mail_fields[] = "order_revisioned";
    $order_mail_values[] = $order_revision;

    $order_mail_fields[] = "date_created";
    $order_mail_values[] = "current_timestamp";

    $order_mail_fields[] = "date_modified";
    $order_mail_values[] = "current_timestamp";

    if (isset($_SESSION["user_login"]))
    {
        $order_mail_fields[] = "user_created";
        $order_mail_values[] = dbquote($_SESSION["user_login"]);

        $order_mail_fields[] = "user_modified";
        $order_mail_values[] = dbquote($_SESSION["user_login"]);
    }

    $sql = "insert into order_mails (" . join(", ", $order_mail_fields) . ") values (" . join(", ", $order_mail_values) . ")";
    mysql_query($sql) or dberror($sql);
}


/********************************************************************
    Ask Retail Coordintator/Retail Operator 
	for entering/adapting the agreed shop opening date
*********************************************************************/
function ma_enter_realistic_sopd()
{

	$today = date("Y-m-d");

	//send mails for new projects
	$sql = "select * from mail_alert_types where mail_alert_type_id = 8";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email = $row["mail_alert_type_sender_email"];
	$sender_name = $row["mail_alert_type_sender_name"];
	$mail_text = $row["mail_alert_mail_text"];
    $mail_alert_type_id = 8;

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id = $row["user_id"];


	$sql = "select * from mail_alert_types where mail_alert_type_id = 12";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email2 = $row["mail_alert_type_sender_email"];
	$sender_name2 = $row["mail_alert_type_sender_name"];
	$mail_text2 = $row["mail_alert_mail_text"];
	$mail_alert_type_id2 = 12;

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email2 . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id2 = $row["user_id"];

	$sql = "select * from mail_alert_types where mail_alert_type_id = 14";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email3 = $row["mail_alert_type_sender_email"];
	$sender_name3 = $row["mail_alert_type_sender_name"];
	$mail_text3 = $row["mail_alert_mail_text"];
	$mail_alert_type_id3 = 14;

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email3 . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id3 = $row["user_id"];


	//All projects with an empty project_real_opening_date
	$sql = "select project_id, project_projectkind, project_real_opening_date, project_retail_coordinator, " .
		   "order_id, order_archive_date, order_number, order_retail_operator, " .
		   "order_shop_address_place,order_shop_address_company, order_actual_order_state_code, " .
		   "country_name " . 
		   "from projects " .
		   "inner join orders on order_id = project_order " .
		   "inner join countries on country_id = order_shop_address_country " . 
		   "where (select count(mail_alert_id) from mail_alerts where mail_alert_order = order_id and mail_alert_type IN (8, 12, 14))  = 0 " . 
		   " and (LEFT(order_number, 2) >= 26 or LEFT(order_number, 3) >= 210) " . 
		   " and project_state <> 2 " . 
		   " and order_actual_order_state_code < '800' " . 
		   " and (project_real_opening_date is null " . 
		   "   or project_real_opening_date = '0000-00-00') " . 
		   " and (project_actual_opening_date is null " . 
		   "   or project_actual_opening_date = '0000-00-00') ";

	
	

    $res = mysql_query($sql) or dberror($sql);

    while($row = mysql_fetch_assoc($res))
    {
		if($row["project_retail_coordinator"] > 0)
		{
			
			if($row['project_projectkind'] == 4 ) //take over 
			{
				$subject = "Project's planned take over date is missing - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];

				$sender_email = $sender_email2; 
				$sender_name = $sender_name2;
				$mail_text = $mail_text2;
				$sender_id =  $sender_id2;
				$mail_alert_type_id = $mail_alert_type_id2;

			}
			elseif($row['project_projectkind'] == 5) //lease renewal
			{
				$subject = "Project's lease starting date is missing - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];

				$sender_email = $sender_email3; 
				$sender_name = $sender_name3;
				$mail_text = $mail_text3;
				$sender_id =  $sender_id3;
				$mail_alert_type_id = $mail_alert_type_id3;

			}
			else
			{
				$subject = "Agreed POS opening date is missing - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];
			}
			

			$mail = new Mail();
			$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
			$mail->set_sender($sender_email, $sender_name);


			//add cc and deputee
			$sql = "select user_id, user_firstname, user_email, user_email_cc, user_email_deputy ".
				   "from users ".
				   "where (user_id = " . $row["project_retail_coordinator"] . 
				   "   and user_active = 1)";
			
			$res1 = mysql_query($sql) or dberror($sql);
			$row1 = mysql_fetch_assoc($res1);
			$rtc_id = $row1["user_id"];
			$rtc_name = $row1["user_firstname"];
			$rtc_email = $row1["user_email"];

			$mail->add_recipient($row1["user_email"]);

			if($row1["user_email_cc"])
			{
				$mail->add_cc($row1["user_email_cc"]);
			}
			if($row1["user_email_deputy"])
			{
				$mail->add_cc($row1["user_email_deputy"]);
			}
			$mail->add_cc($sender_email);

			$sql = "select user_id, user_email ".
				   "from users ".
				   "where (user_id = " . $row["order_retail_operator"] . 
				   "   and user_active = 1)";
			$res1 = mysql_query($sql) or dberror($sql);
			$row1 = mysql_fetch_assoc($res1);
			$mail->add_cc($row1["user_email"]);
			$rto_id = $row1["user_id"];


			$bodytext0 = "Dear " . $rtc_name . "\n\n" . $mail_text;
			$link ="project_edit_pos_data.php?pid=" . $row["project_id"];
			$bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
			$bodytext = $bodytext .  APPLICATION_URL ."/user/" . $link . "\n\n";           
			
			$bodytext .= "Thank you very much.\nKind regards, " . $sender_name;

			$mail->add_text($bodytext);

			
			
			//check if mail was sent meanwhile
			$sql_m = 'select count(mail_alert_id) as num_recs ' .
				     'from mail_alerts where mail_alert_type = 8 and mail_alert_order = ' . $row["order_id"];
			$res_m = mysql_query($sql_m) or dberror($sql_m);
			$row_m = mysql_fetch_assoc($res_m);

			if($row_m['num_recs'] == 0) {
			
				$result = $mail->send();

				if($result == 1)
				{
					append_mail($row["order_id"], $rtc_id, $sender_id, $bodytext0, "910", 1);
					append_mail($row["order_id"], $rto_id, $sender_id, $bodytext0, "910", 1);

					//update mail_alerts
					$fields = array();
					$values = array();

					$fields[] = "mail_alert_type";
					$values[] = $mail_alert_type_id;

					$fields[] = "mail_alert_order";
					$values[] = $row["order_id"];

					$fields[] = "mail_alert_date";
					$values[] = dbquote($today);

					$fields[] = "mail_alert_sender";
					$values[] = dbquote($sender_email);

					$fields[] = "mail_alert_recipient";
					$values[] = dbquote($rtc_email);

					$fields[] = "date_created";
					$values[] = "current_timestamp";

					$fields[] = "date_modified";
					$values[] = "current_timestamp";

					$fields[] = "user_created";
					$values[] = dbquote(user_login());

					$fields[] = "user_modified";
					$values[] = dbquote(user_login());

					$sql = "insert into mail_alerts (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);
				}
			}
		}
    }

	
	
	$sql = "select * from mail_alert_types where mail_alert_type_id = 1";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email1 = $row["mail_alert_type_sender_email"];
	$sender_name1 = $row["mail_alert_type_sender_name"];
	$mail_text1 = $row["mail_alert_mail_text"];
	$mail_alert_type_id1 = 1;

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id1 = $row["user_id"];

	$sql = "select * from mail_alert_types where mail_alert_type_id = 13";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email2 = $row["mail_alert_type_sender_email"];
	$sender_name2 = $row["mail_alert_type_sender_name"];
	$mail_text2 = $row["mail_alert_mail_text"];
	$mail_alert_type_id2 = 13;

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email2 . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id2 = $row["user_id"];


	$sql = "select * from mail_alert_types where mail_alert_type_id = 15";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email3 = $row["mail_alert_type_sender_email"];
	$sender_name3 = $row["mail_alert_type_sender_name"];
	$mail_text3 = $row["mail_alert_mail_text"];
	$mail_alert_type_id3 = 15;

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email3 . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id3 = $row["user_id"];


	

	//ALL projects with an non empty project_real_opening_date
	$sql = "select project_id, project_projectkind, project_real_opening_date, project_retail_coordinator, " .
		   "order_id, order_archive_date, order_number, order_retail_operator, " .
		   "order_shop_address_place,order_shop_address_company, order_actual_order_state_code, " .
		   "country_name " . 
		   "from projects " .
		   "inner join orders on order_id = project_order " .
		   "inner join countries on country_id = order_shop_address_country " . 
		   "where (select count(mail_alert_id) from mail_alerts where mail_alert_order = order_id and mail_alert_type IN (1, 13, 15))  = 0 " . 
		   " and (LEFT(order_number, 2) >= 26 or LEFT(order_number, 3) >= 210) " . 
		   " and order_actual_order_state_code < '800' " .
		   " and project_real_opening_date is not null " . 
		   " and project_real_opening_date <> '0000-00-00' " . 
		   " and project_real_opening_date <= '" . $today . "' " .
		   " and project_state <> 2 " .
		   " and (project_actual_opening_date is null " . 
		   "   or project_actual_opening_date = '0000-00-00') ";


    $res = mysql_query($sql) or dberror($sql);

    while($row = mysql_fetch_assoc($res))
    {
		if($row["project_retail_coordinator"] > 0)
		{
			
			if($row['project_projectkind'] == 4) //take over
			{
				$subject = "Project's planned take over date is past due - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];

				$sender_email = $sender_email2; 
				$sender_name = $sender_name2;
				$mail_text = $mail_text2;
				$sender_id =  $sender_id2;
				$mail_alert_type_id = $mail_alert_type_id2;

			}
			elseif($row['project_projectkind'] == 5) //lease renewal
			{
				$subject = "Project's lease starting date is past due - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];

				$sender_email = $sender_email3; 
				$sender_name = $sender_name3;
				$mail_text = $mail_text3;
				$sender_id =  $sender_id3;
				$mail_alert_type_id = $mail_alert_type_id3;

			}
			else
			{
				$subject = "Agreed POS opening date is past due - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];

				$sender_email = $sender_email1; 
				$sender_name = $sender_name1;
				$mail_text = $mail_text1;
				$sender_id =  $sender_id1;
				$mail_alert_type_id = $mail_alert_type_id1;

			}

			$mail = new Mail();
			$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
			$mail->set_sender($sender_email, $sender_name);


			//add cc and deputee
			$sql = "select user_id, user_firstname, user_email, user_email_cc, user_email_deputy ".
				   "from users ".
				   "where (user_id = " . $row["project_retail_coordinator"] . 
				   "   and user_active = 1)";
			
			$res1 = mysql_query($sql) or dberror($sql);
			$row1 = mysql_fetch_assoc($res1);
			$rtc_id = $row1["user_id"];
			$rtc_name = $row1["user_firstname"];
			$rtc_email = $row1["user_email"];

			$mail->add_recipient($row1["user_email"]);

			if($row1["user_email_cc"])
			{
				$mail->add_cc($row1["user_email_cc"]);
			}
			if($row1["user_email_deputy"])
			{
				$mail->add_cc($row1["user_email_deputy"]);
			}
			$mail->add_cc($sender_email);

			$sql = "select user_id, user_email ".
				   "from users ".
				   "where (user_id = " . $row["order_retail_operator"] . 
				   "   and user_active = 1)";
			$res1 = mysql_query($sql) or dberror($sql);
			$row1 = mysql_fetch_assoc($res1);
			$mail->add_cc($row1["user_email"]);
			$rto_id = $row1["user_id"];


			$bodytext0 = "Dear " . $rtc_name . "\n\n" . $mail_text;
			$link ="project_edit_pos_data.php?pid=" . $row["project_id"];
			$bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
			$bodytext = $bodytext .  APPLICATION_URL ."/user/" . $link . "\n\n";           
			
			$bodytext .= "Thank you very much.\nKind regards, " . $sender_name;

			$mail->add_text($bodytext);

			$sql_m = 'select count(mail_alert_id) as num_recs ' .
				     'from mail_alerts where mail_alert_type = 1 and mail_alert_order = ' . $row["order_id"];
			$res_m = mysql_query($sql_m) or dberror($sql_m);
			$row_m = mysql_fetch_assoc($res_m);

			if($rtc_email and $row_m['num_recs'] == 0) {
				$result = $mail->send();

				if($result == 1)
				{
					append_mail($row["order_id"], $rtc_id, $sender_id, $bodytext0, "910", 1);
					append_mail($row["order_id"], $rto_id, $sender_id, $bodytext0, "910", 1);

					//update mail_alerts
					$fields = array();
					$values = array();

					$fields[] = "mail_alert_type";
					$values[] = $mail_alert_type_id;

					$fields[] = "mail_alert_order";
					$values[] = $row["order_id"];

					$fields[] = "mail_alert_date";
					$values[] = dbquote($today);

					$fields[] = "mail_alert_sender";
					$values[] = dbquote($sender_email);

					$fields[] = "mail_alert_recipient";
					$values[] = dbquote($rtc_email);

					$fields[] = "date_created";
					$values[] = "current_timestamp";

					$fields[] = "date_modified";
					$values[] = "current_timestamp";

					$fields[] = "user_created";
					$values[] = dbquote(user_login());

					$fields[] = "user_modified";
					$values[] = dbquote(user_login());

					$sql = "insert into mail_alerts (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);
				}
			}
		}
    }

	return true;
}

/********************************************************************
    Ask Client
	for entering/adapting the actual shop opening date
*********************************************************************/
function ma_enter_actual_sopd()
{
	
	$today = date("Y-m-d");

	$sql = "select * from mail_alert_types where mail_alert_type_id = 2";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email = $row["mail_alert_type_sender_email"];
	$sender_name = $row["mail_alert_type_sender_name"];
	$mail_text = $row["mail_alert_mail_text"];
	$mail_alert_type_id = 2;

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id = $row["user_id"];


	//All projects with full delivery, except lease renewal and take over
	$sql = "select project_id, project_real_opening_date, project_retail_coordinator, " .
		   "order_id, order_archive_date, order_number, order_client_address, " .
		   "order_shop_address_place,order_shop_address_company, order_actual_order_state_code, " .
		   "country_name, order_user " . 
		   "from projects " .
		   "inner join orders on order_id = project_order " .
		   "inner join countries on country_id = order_shop_address_country " . 
		   "where (select count(mail_alert_id) from mail_alerts where mail_alert_order = order_id and mail_alert_type IN (2))  = 0 " . 
		   " and (LEFT(order_number, 2) >= 26 or LEFT(order_number, 3) >= 210) " .
		   " and order_actual_order_state_code >= '800' " . 
		   " and order_actual_order_state_code <= '890' " . 
		   " and project_real_opening_date <= '" . $today . "' " . 
		   " and project_state <> 2 " .
		   " and (project_actual_opening_date is null " . 
		   "   or project_actual_opening_date = '0000-00-00') " . 
		    " and (project_projectkind <> 4 and project_projectkind <> 5)";

    $res = mysql_query($sql) or dberror($sql);

    while($row = mysql_fetch_assoc($res))
    {
		if($row["order_client_address"] > 0)
		{
			$subject = "Actual POS opening date is missing and realisitc POS opening date is past due - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];

			$mail = new Mail();
			$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
			$mail->set_sender($sender_email, $sender_name);

			
			$sql = "select user_id, user_firstname, user_email, user_email_cc, user_email_deputy, user_can_only_see_his_projects ".
				   "from users ".
				   "where  user_id = " . dbquote($row["project_retail_coordinator"]) . 
				   " and user_active = 1";
			
			$res1 = mysql_query($sql) or dberror($sql);
			$row1 = mysql_fetch_assoc($res1);

			$cli_id = $row1["user_id"];
			$cli_name = $row1["user_firstname"];
			$cli_email = $row1["user_email"];

			$mail->add_recipient($cli_email);

			if($row1["user_email_cc"])
			{
				$mail->add_cc($row1["user_email_cc"]);
			}
			if($row1["user_email_deputy"])
			{
				$mail->add_cc($row1["user_email_deputy"]);
			}

			$mail->add_cc($sender_email);

		
			$bodytext0 = "Dear " . $cli_name . "\n\n" . $mail_text;
			$link ="project_edit_pos_data.php?pid=" . $row["project_id"];
			$bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
			$bodytext = $bodytext .  APPLICATION_URL ."/user/" . $link . "\n\n";           
			
			$bodytext .= "Thank you very much.\nKind regards, " . $sender_name;

			$mail->add_text($bodytext);
			
			$sql_m = 'select count(mail_alert_id) as num_recs ' .
				     'from mail_alerts where mail_alert_type = 2 and mail_alert_order = ' . $row["order_id"];
			$res_m = mysql_query($sql_m) or dberror($sql_m);
			$row_m = mysql_fetch_assoc($res_m);

			if($cli_email and $row_m['num_recs'] == 0) {
				$result = $mail->send();

				if($result == 1)
				{
					append_mail($row["order_id"], $cli_id, $sender_id, $bodytext0, "910", 1);

					//update mail_alerts
					$fields = array();
					$values = array();

					$fields[] = "mail_alert_type";
					$values[] = $mail_alert_type_id;

					$fields[] = "mail_alert_order";
					$values[] = $row["order_id"];

					$fields[] = "mail_alert_date";
					$values[] = dbquote($today);

					$fields[] = "mail_alert_sender";
					$values[] = dbquote($sender_email);

					$fields[] = "mail_alert_recipient";
					$values[] = dbquote($cli_email);

					$fields[] = "date_created";
					$values[] = "current_timestamp";

					$fields[] = "date_modified";
					$values[] = "current_timestamp";

					$fields[] = "user_created";
					$values[] = dbquote(user_login());

					$fields[] = "user_modified";
					$values[] = dbquote(user_login());

					$sql = "insert into mail_alerts (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);
				}
			}
		}
    }


	//take over and lease renewal


	$sql = "select * from mail_alert_types where mail_alert_type_id = 17";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email = $row["mail_alert_type_sender_email"];
	$sender_name = $row["mail_alert_type_sender_name"];
	$mail_text = $row["mail_alert_mail_text"];
	$mail_alert_type_id = 17;

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id = $row["user_id"];


	$sql = "select * from mail_alert_types where mail_alert_type_id = 18";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email2 = $row["mail_alert_type_sender_email"];
	$sender_name2 = $row["mail_alert_type_sender_name"];
	$mail_text2 = $row["mail_alert_mail_text"];
	$mail_alert_type_id2 = 18;

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email2 . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id2 = $row["user_id"];


	$sql = "select project_id, project_projectkind, project_real_opening_date, project_retail_coordinator, " .
		   "order_id, order_archive_date, order_number, order_client_address, " .
		   "order_shop_address_place,order_shop_address_company, order_actual_order_state_code, " .
		   "country_name, order_user " . 
		   "from projects " .
		   "inner join orders on order_id = project_order " .
		   "inner join countries on country_id = order_shop_address_country " . 
		   "where (select count(mail_alert_id) from mail_alerts where mail_alert_order = order_id and mail_alert_type IN (17,18))  = 0 " . 
		   " and (LEFT(order_number, 2) >= 26 or LEFT(order_number, 3) >= 210) " . 
		   " and project_real_opening_date is not null " . 
		   " and project_real_opening_date <> '0000-00-00' " . 
		    " and project_real_opening_date <= '" . $today . "' " . 
		   " and project_state <> 2 " .
		   " and order_actual_order_state_code < '890' " .
		   " and (project_actual_opening_date is null " . 
		   "   or project_actual_opening_date = '0000-00-00') " . 
		    " and (project_projectkind = 4 or project_projectkind = 5)";

    $res = mysql_query($sql) or dberror($sql);

    while($row = mysql_fetch_assoc($res))
    {
		if($row["order_client_address"] > 0)
		{
			
			
			if($row['project_projectkind'] == 5) //lease renewal
			{
				$subject = "Actual lease starting date is missing and planned lease starting date is past due - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];

				$sender_email = $sender_email2; 
				$sender_name = $sender_name2;
				$mail_text = $mail_text2;
				$sender_id =  $sender_id2;
				$mail_alert_type_id = $mail_alert_type_id2;

			}
			else // take over
			{
				$subject = "Actual take over date is missing and planned take over date is past due - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];
			}
			
			

			$mail = new Mail();
			$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
			$mail->set_sender($sender_email, $sender_name);

			

			$sql = "select user_id, user_firstname, user_email, user_email_cc, user_email_deputy, user_can_only_see_his_projects ".
				   "from users ".
				   "where  user_id = " . dbquote($row["project_retail_coordinator"]) . 
				   " and user_active = 1";
			
			$res1 = mysql_query($sql) or dberror($sql);
			$row1 = mysql_fetch_assoc($res1);

			$cli_id = $row1["user_id"];
			$cli_name = $row1["user_firstname"];
			$cli_email = $row1["user_email"];

			$mail->add_recipient($cli_email);

			if($row1["user_email_cc"])
			{
				$mail->add_cc($row1["user_email_cc"]);
			}
			if($row1["user_email_deputy"])
			{
				$mail->add_cc($row1["user_email_deputy"]);
			}

			$mail->add_cc($sender_email);

		
			$bodytext0 = "Dear " . $cli_name . "\n\n" . $mail_text;
			$link ="project_edit_pos_data.php?pid=" . $row["project_id"];
			$bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
			$bodytext = $bodytext .  APPLICATION_URL ."/user/" . $link . "\n\n";           
			
			$bodytext .= "Thank you very much.\nKind regards, " . $sender_name;

			$mail->add_text($bodytext);

			
			$sql_m = 'select count(mail_alert_id) as num_recs ' .
				     'from mail_alerts where mail_alert_type = 2 and mail_alert_order = ' . $row["order_id"];
			$res_m = mysql_query($sql_m) or dberror($sql_m);
			$row_m = mysql_fetch_assoc($res_m);

			if($cli_email and $row_m['num_recs'] == 0) {
				$result = $mail->send();

				if($result == 1)
				{
					append_mail($row["order_id"], $cli_id, $sender_id, $bodytext0, "910", 1);

					//update mail_alerts
					$fields = array();
					$values = array();

					$fields[] = "mail_alert_type";
					$values[] = $mail_alert_type_id;

					$fields[] = "mail_alert_order";
					$values[] = $row["order_id"];

					$fields[] = "mail_alert_date";
					$values[] = dbquote($today);

					$fields[] = "mail_alert_sender";
					$values[] = dbquote($sender_email);

					$fields[] = "mail_alert_recipient";
					$values[] = dbquote($cli_email);

					$fields[] = "date_created";
					$values[] = "current_timestamp";

					$fields[] = "date_modified";
					$values[] = "current_timestamp";

					$fields[] = "user_created";
					$values[] = dbquote(user_login());

					$fields[] = "user_modified";
					$values[] = dbquote(user_login());

					$sql = "insert into mail_alerts (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);
				}
			}
		}
    }
	return true;
}

/********************************************************************
    Ask Client
	to resignate lease contract/extend lease contract
*********************************************************************/
function ma_resignation_deadline()
{

	$today = date("Y-m-d");

	
	//extenstion option
	$sql = "select * from mail_alert_types where mail_alert_type_id = 28";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email = $row["mail_alert_type_sender_email"];
	$sender_name = $row["mail_alert_type_sender_name"];
	$mail_text_original = $row["mail_alert_mail_text"];

	$cc1 = $row["mail_alert_type_cc1"];
	$cc2 = $row["mail_alert_type_cc2"];
	$cc3 = $row["mail_alert_type_cc3"];
	$cc4 = $row["mail_alert_type_cc4"];

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id = $row["user_id"];
	
	
	
    $sql = "select * from posleases " . 
			"inner join posaddresses on posaddress_id = poslease_posaddress " . 
			"inner join countries on country_id = posaddress_country " . 
			"where PERIOD_DIFF(DATE_FORMAT(poslease_extensionoption, '%Y%m'), DATE_FORMAT('" . date("Y-m-d") . "', '%Y%m'))  <= (poslease_termination_time+1) " . 
			"and (posaddress_store_closingdate is null " .
            "or posaddress_store_closingdate = '0000-00-00') " . 
            " and poslease_mailalert is not null and poslease_mailalert <> ''  ". 
            "and poslease_termination_time > 0 " . 
            "and poslease_extensionoption >= '" . date("Y-m-d") . "' " . 
		    "and poslease_extensionoption is not null " .
		    "and poslease_extensionoption <> '0000-00-00' " .
		    "and poslease_mailalert is not null " . 
		    "and poslease_mailalert2 is null " .
		    "and poslease_extensionoption is not null " . 
		    "and poslease_extensionoption <> '0000-00-00' " .
		    "and poslease_isindexed <> 1 " . 
		    " order by poslease_posaddress, poslease_enddate DESC";

	
	$res = mysql_query($sql) or dberror($sql);

    while($row = mysql_fetch_assoc($res))
    {

		//check if lease was already renewed
		$send_mail = true;
		$sql_l = "select count(poslease_id) as num_recs " .
			     " from posleases " . 
			     " where poslease_posaddress = " . $row["poslease_posaddress"] . 
			     " and poslease_id <> " . $row["poslease_id"] . 
			     " and year(poslease_enddate) >= year(poslease_startdate) ";

		$res_l = mysql_query($sql_l) or dberror($sql_l);
		$row_l = mysql_fetch_assoc($res_l);
		if($row_l["num_recs"] > 0)
		{
			$send_mail = false;
			//echo "->" . $row["poslease_posaddress"] . "<br />";
		}

		if($send_mail == true)
		{
			//get brand manager
			$bm_email = "";
			$sql_c = "select user_firstname, user_name, user_email from users " .
					 "inner join user_roles on user_role_user = user_id " . 
					 "where user_address = " . dbquote($row["posaddress_client_id"]) . 
					 " and user_role_role = 15 and user_active = 1";

			$res_c = mysql_query($sql_c) or dberror($sql_c);

			if($row_c = mysql_fetch_assoc($res_c))
			{
				$bm_firstname = $row_c["user_firstname"];
				$bm_name = $row_c["user_name"];
				$bm_email = $row_c["user_email"];
			
			}
			else // get brand manager in case of an affiliate
			{
				$sql_c = "select address_client_type, address_parent " . 
						 " from addresses " . 
						 " where address_id = " . dbquote($row["posaddress_client_id"]);
				$res_c = mysql_query($sql_c) or dberror($sql_c);
				if($row_c = mysql_fetch_assoc($res_c))
				{
					if($row_c["address_client_type"] == 3) //Affiliate
					{
						$sql_c = "select user_firstname, user_name, user_email from users " .
								 "inner join user_roles on user_role_user = user_id " . 
								 "where user_address = " . dbquote($row_c["address_parent"]) . 
								 " and user_role_role = 15 and user_active = 1";

						$res_c = mysql_query($sql_c) or dberror($sql_c);

						if($row_c = mysql_fetch_assoc($res_c))
						{
							$bm_firstname = $row_c["user_firstname"];
							$bm_name = $row_c["user_name"];
							$bm_email = $row_c["user_email"];
						
						}
					}
				}
			}


			//get retail manager
			
			$rm_firstname = array();
			$rm_name = array();
			$rm_email = array();
			$sql_c = "select user_firstname, user_name, user_email from users " .
					 "inner join user_roles on user_role_user = user_id " . 
					 "where user_address = " . dbquote($row["posaddress_client_id"]) . 
					 " and user_role_role = 16 and user_active = 1";

			$res_c = mysql_query($sql_c) or dberror($sql_c);

			while($row_c = mysql_fetch_assoc($res_c))
			{
				$rm_firstname[] = $row_c["user_firstname"];
				$rm_name[] = $row_c["user_name"];
				$rm_email[] = $row_c["user_email"];
			
			}

			if($bm_email or count($rm_email) > 0)
			{
				$poslocation = $row["posaddress_name"] . ", " . $row["country_name"];
				$subject = "Lease contract alert - POS Location " . $poslocation;

				$mail_text = str_replace("{pos}", $poslocation, $mail_text_original);

				$mail = new Mail();
				$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
				$mail->set_sender($sender_email, $sender_name);

				if($bm_email)
				{
					$mail->add_recipient($bm_email);
					
					foreach($rm_email as $key=>$email)
					{
						$mail->add_cc($email);
					}
					$mail_text = str_replace("{name}", $bm_firstname, $mail_text);
				}
				else
				{
					foreach($rm_email as $key=>$email)
					{
						$mail->add_recipient($email);
						$mail_text = str_replace("{name}", $rm_firstname[$key], $mail_text);
					}
				}

				if($cc1) {$mail->add_cc($cc1);}
				if($cc2) {$mail->add_cc($cc2);}
				if($cc3) {$mail->add_cc($cc3);}
				if($cc4) {$mail->add_cc($cc4);}
				
				
				$mail->add_text($mail_text);

				//echo $row["poslease_id"] . ' ' . $row["poslease_enddate"] . ' ' . $row['poslease_termination_time'] . ' ' . $mail_text . '<br /><br /><br />';

				$result = $mail->send();
				
				if($result == 1)
				{
					//update mail_alerts
					$mailalert = $mail_text;
					$mailalert .= "\r\n" . "Mail sent " . to_system_date(date("Y-m-d")) . " by " . $sender_name;
					
					if($bm_email and count($rm_email) > 0)
					{
						$mailalert .= "\r\n" . "To: " . $bm_email . " and cc to ";
						foreach($rm_email as $key=>$email)
						{
							$mailalert .= $email . "\r\n";
						}
					}
					elseif($bm_email)
					{
						$mailalert .= "\r\n" . "To: " . $bm_email;
					}
					else
					{
						$mailalert .= "\r\n" . "To: ";
						foreach($rm_email as $key=>$email)
						{
							$mailalert .= $email . "\r\n";
						}
					}
					
					$sql = "update posleases set " . 
						   "poslease_mailalert2 = " . dbquote($mailalert) . 
						   " where poslease_id = " . $row["poslease_id"];

					mysql_query($sql) or dberror($sql);
				}
			}
			//echo  $row["poslease_posaddress"] . "<br />";
		}

		
    }
	
	//resignation deadline
	$sql = "select * from mail_alert_types where mail_alert_type_id = 5";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email = $row["mail_alert_type_sender_email"];
	$sender_name = $row["mail_alert_type_sender_name"];
	$mail_text_original = $row["mail_alert_mail_text"];

	$cc1 = $row["mail_alert_type_cc1"];
	$cc2 = $row["mail_alert_type_cc2"];
	$cc3 = $row["mail_alert_type_cc3"];
	$cc4 = $row["mail_alert_type_cc4"];

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id = $row["user_id"];
	
	
	
    $sql = "select * from posleases " . 
			"inner join posaddresses on posaddress_id = poslease_posaddress " . 
			"inner join countries on country_id = posaddress_country " . 
			"where PERIOD_DIFF(DATE_FORMAT(poslease_enddate, '%Y%m'), DATE_FORMAT('" . date("Y-m-d") . "', '%Y%m'))  <= (poslease_termination_time+1) " . 
			"and (posaddress_store_closingdate is null " .
            "or posaddress_store_closingdate = '0000-00-00') " . 
            " and (poslease_mailalert is null or poslease_mailalert = '' ) ". 
            "and poslease_enddate >= '" . date("Y-m-d") . "' " .
		    " order by poslease_posaddress, poslease_enddate DESC";
	
	//echo $sql . "<br />";
	$res = mysql_query($sql) or dberror($sql);

    while($row = mysql_fetch_assoc($res))
    {
		//check if lease was already renewed
		$send_mail = true;
		$sql_l = "select count(poslease_id) as num_recs " .
			     " from posleases " . 
			     " where poslease_posaddress = " . $row["poslease_posaddress"] . 
			     " and poslease_id <> " . $row["poslease_id"] . 
			     " and year(poslease_enddate) >= year(poslease_startdate) ";

		$res_l = mysql_query($sql_l) or dberror($sql_l);
		$row_l = mysql_fetch_assoc($res_l);
		if($row_l["num_recs"] > 0)
		{
			$send_mail = false;
			//echo "->" . $row["poslease_posaddress"] . ": " . $row["poslease_enddate"] . "(" . $row_l["num_recs"] . ")<br />";
		}

		if($send_mail == true)
		{
		
			//get brand manager
			$bm_email = "";
			$sql_c = "select user_firstname, user_name, user_email from users " .
					 "inner join user_roles on user_role_user = user_id " . 
					 "where user_address = " . dbquote($row["posaddress_client_id"]) . 
					 " and user_role_role = 15 and user_active = 1";

			$res_c = mysql_query($sql_c) or dberror($sql_c);

			if($row_c = mysql_fetch_assoc($res_c))
			{
				$bm_firstname = $row_c["user_firstname"];
				$bm_name = $row_c["user_name"];
				$bm_email = $row_c["user_email"];
			
			}
			else // get brand manager in case of an affiliate
			{
				$sql_c = "select address_client_type, address_parent " . 
						 " from addresses " . 
						 " where address_id = " . dbquote($row["posaddress_client_id"]);
				$res_c = mysql_query($sql_c) or dberror($sql_c);
				if($row_c = mysql_fetch_assoc($res_c))
				{
					if($row_c["address_client_type"] == 3) //Affiliate
					{
						$sql_c = "select user_firstname, user_name, user_email from users " .
								 "inner join user_roles on user_role_user = user_id " . 
								 "where user_address = " . dbquote($row_c["address_parent"]) . 
								 " and user_role_role = 15 and user_active = 1";

						$res_c = mysql_query($sql_c) or dberror($sql_c);

						if($row_c = mysql_fetch_assoc($res_c))
						{
							$bm_firstname = $row_c["user_firstname"];
							$bm_name = $row_c["user_name"];
							$bm_email = $row_c["user_email"];
						
						}
					}
				}
			}

			//get retail manager
			$rm_firstname = array();
			$rm_name = array();
			$rm_email = array();

			$sql_c = "select user_firstname, user_name, user_email from users " .
					 "inner join user_roles on user_role_user = user_id " . 
					 "where user_address = " . dbquote($row["posaddress_client_id"]) . 
					 " and user_role_role = 16 and user_active = 1";

			$res_c = mysql_query($sql_c) or dberror($sql_c);

			if($row_c = mysql_fetch_assoc($res_c))
			{
				$rm_firstname[] = $row_c["user_firstname"];
				$rm_name[] = $row_c["user_name"];
				$rm_email[] = $row_c["user_email"];
			
			}
			

			if($bm_email or count($rm_email) > 0)
			{
				$poslocation = $row["posaddress_name"] . ", " . $row["country_name"];
				$subject = "Lease contract alert - POS Location " . $poslocation;

				$mail_text = str_replace("{pos}", $poslocation, $mail_text_original);

				$mail = new Mail();
				$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
				$mail->set_sender($sender_email, $sender_name);

				if($bm_email)
				{
					$mail->add_recipient($bm_email);
					
					foreach($rm_email as $key=>$email)
					{
						$mail->add_cc($email);
					}

					$mail_text = str_replace("{name}", $bm_firstname, $mail_text);
				}
				else
				{
					foreach($rm_email as $key=>$email)
					{
						$mail->add_recipient($email);
						$mail_text = str_replace("{name}", $rm_firstname[$key], $mail_text);
					}
				}

				if($cc1) {$mail->add_cc($cc1);}
				if($cc2) {$mail->add_cc($cc2);}
				if($cc3) {$mail->add_cc($cc3);}
				if($cc4) {$mail->add_cc($cc4);}
				
				
				$mail->add_text($mail_text);

				//echo $row["poslease_id"] . ' ' . $row["poslease_enddate"] . ' ' . $row['poslease_termination_time'] . ' ' . $mail_text . '<br /><br /><br />';

				$result = $mail->send();

				if($result == 1)
				{
					//update mail_alerts
					$mailalert = $mail_text;
					$mailalert .= "\r\n" . "Mail sent " . to_system_date(date("Y-m-d")) . " by " . $sender_name;
					
					if($bm_email and count($rm_email) > 0)
					{
						$mailalert .= "\r\n" . "To: " . $bm_email . " and cc to ";
						foreach($rm_email as $key=>$email)
						{
							$mailalert .= $email . "\r\n";
						}
					}
					elseif($bm_email)
					{
						$mailalert .= "\r\n" . "To: " . $bm_email;
					}
					else
					{
						$mailalert .= "\r\n" . "To: ";
						foreach($rm_email as $key=>$email)
						{
							$mailalert .= $email . "\r\n";
						}
					}
					
					$sql = "update posleases set " . 
						   "poslease_mailalert = " . dbquote($mailalert) . 
						   " where poslease_id = " . $row["poslease_id"];

					mysql_query($sql) or dberror($sql);
				}
			}
			//echo "send mail: " . $row["poslease_posaddress"] . "<br />";
		}
    }

	return true;
}



/********************************************************************
    Ask Client
	to communicate closing date for temporary POS Locations
*********************************************************************/
function ma_close_temporary_pos_locations()
{

	
	$today = date("Y-m-d");

	//send mails for new projects
	$sql = "select * from mail_alert_types where mail_alert_type_id = 25";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email = $row["mail_alert_type_sender_email"];
	$sender_name = $row["mail_alert_type_sender_name"];
	$mail_text_original = $row["mail_alert_mail_text"];
    $mail_alert_type_id = 25;
	$mail_alert_type_cc1 = $row["mail_alert_type_cc1"];
	$mail_alert_type_cc2 = $row["mail_alert_type_cc2"];
	$mail_alert_type_cc3 = $row["mail_alert_type_cc3"];
	$mail_alert_type_cc4 = $row["mail_alert_type_cc4"];

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id = $row["user_id"];


	
	$sql = "select project_id, project_projectkind, project_real_opening_date, project_retail_coordinator, " .
		   "order_id, order_archive_date, order_number, order_retail_operator, order_client_address, order_user, " .
		   "order_shop_address_place,order_shop_address_company, order_actual_order_state_code, " .
		   "project_local_retail_coordinator, project_planned_closing_date, country_name, posorder_posaddress " . 
		   "from projects " .
		   "inner join orders on order_id = project_order " .
		   "inner join posorders on posorder_order = order_id " .
		   "inner join posaddresses on posaddress_id = posorder_posaddress " .
		   "inner join countries on country_id = order_shop_address_country " . 
		   "where (select count(mail_alert_id) from mail_alerts where mail_alert_order = order_id and mail_alert_type IN (25))  = 0 " . 
		   " and project_projectkind <> 8 " .
		   " and project_planned_closing_date is not null " . 
		   " and project_planned_closing_date <> '0000-00-00' " .
		   " and project_actual_opening_date is not null " . 
		   " and project_actual_opening_date <> '0000-00-00' " . 
		   " and (project_shop_closingdate is  null " . 
		   " or project_shop_closingdate = '0000-00-00') " .
		   " and posaddress_store_subclass = 27 " . 
		   " and posaddress_store_planned_closingdate < " . dbquote(date("Y-m-d"));
		   

    $res = mysql_query($sql) or dberror($sql);

    while($row = mysql_fetch_assoc($res))
    {

		$mail_text = $mail_text_original;
		//get brand manager
		$cc_recipients = array();
		$sql_l = "select user_id, user_email, concat(user_firstname, ' ', user_name) as username,  " .
			     "user_email_cc, user_email_deputy " . 
			     "from users " .
			     "inner join user_roles on user_role_user = user_id " . 
			     "where user_address = " . dbquote($row["order_client_address"]) .
			     " and user_active = 1 " . 
			     " and user_role_role in (15)";

		
	    $res_l = mysql_query($sql_l) or dberror($sql_l);

		if($row_l = mysql_fetch_assoc($res_l))
		{
			$cc_recipients[$row_l["user_id"]] = array("email"=>$row_l["user_email"], "username"=>$row_l["username"],  "email_cc"=>$row_l["user_email_cc"], "email_deputy"=>$row_l["user_email_deputy"]);
		}
		else // get brand manager in case of an affiliate
		{
			$sql_c = "select address_client_type, address_parent " . 
					 " from addresses " . 
					 " where address_id = " . dbquote($row["order_client_address"]);
			$res_c = mysql_query($sql_c) or dberror($sql_c);
			if($row_c = mysql_fetch_assoc($res_c))
			{
				if($row_c["address_client_type"] == 3) //Affiliate
				{
					
					$sql_c ="select user_id, user_email, concat(user_firstname, ' ', user_name) as username,  " .
							 "user_email_cc, user_email_deputy " . 
							 "from users " .
							 "inner join user_roles on user_role_user = user_id " . 
							 "where user_address = " . dbquote($row_c["address_parent"]) .
							 " and user_active = 1 " . 
							 " and user_role_role in (15)";

					$res_c = mysql_query($sql_c) or dberror($sql_c);

					if($row_c = mysql_fetch_assoc($res_c))
					{
						$cc_recipients[$row_c["user_id"]] = array("email"=>$row_c["user_email"], "username"=>$row_c["username"],  "email_cc"=>$row_c["user_email_cc"], "email_deputy"=>$row_c["user_email_deputy"]);
					}
				}
			}
		}

		//get local project Leader 
		$sql_l = "select user_id, user_email, concat(user_firstname, ' ', user_name) as username,  " .
			     "user_email_cc, user_email_deputy " . 
			     "from users " .
			     "where user_id = " . dbquote($row["project_local_retail_coordinator"]);
		
	    $res_l = mysql_query($sql_l) or dberror($sql_l);

		if($row_l = mysql_fetch_assoc($res_l))
		{
			$cc_recipients[$row_l["user_id"]] = array("email"=>$row_l["user_email"], "username"=>$row_l["username"],  "email_cc"=>$row_l["user_email_cc"], "email_deputy"=>$row_l["user_email_deputy"]);
		}



		//get recipeint /client
		$recipients = array();
		$sql_l = "select user_id, user_email, concat(user_firstname, ' ', user_name) as username,  " .
			     "user_email_cc, user_email_deputy " . 
			     "from users " .
			     "where user_id = " . dbquote($row["order_user"]);
		
	    $res_l = mysql_query($sql_l) or dberror($sql_l);

		if($row_l = mysql_fetch_assoc($res_l))
		{
			$recipients[$row_l["user_id"]] = array("email"=>$row_l["user_email"], "username"=>$row_l["username"],  "email_cc"=>$row_l["user_email_cc"], "email_deputy"=>$row_l["user_email_deputy"]);
		}


		
		if(count($recipients) > 0)
		{
			
			$subject = "Closing date is due - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];
			

			$mail = new Mail();
			$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
			$mail->set_sender($sender_email, $sender_name);

			

			foreach($recipients as $user_id=>$user_data)
			{
				$mail->add_recipient($user_data["email"]);
				$recipient_name = $user_data["username"];

				if($user_data["email_cc"])
				{
					$mail->add_cc($user_data["email_cc"]);
				}
				if($user_data["email_deputy"])
				{
					$mail->add_cc($user_data["email_deputy"]);
				}

			}

			foreach($cc_recipients as $user_id=>$user_data)
			{
				if(!array_key_exists($user_id, $recipients))
				{
					$mail->add_cc($user_data["email"]);
					if($user_data["email_cc"])
					{
						$mail->add_cc($user_data["email_cc"]);
					}
					if($user_data["email_deputy"])
					{
						$mail->add_cc($user_data["email_deputy"]);
					}
				}
			}

			if($mail_alert_type_cc1)
			{
				$mail->add_cc($mail_alert_type_cc1);
			}
			if($mail_alert_type_cc2)
			{
				$mail->add_cc($mail_alert_type_cc2);
			}
			if($mail_alert_type_cc3)
			{
				$mail->add_cc($mail_alert_type_cc3);
			}
			if($mail_alert_type_cc4)
			{
				$mail->add_cc($mail_alert_type_cc4);
			}

			
			$link = APPLICATION_URL ."/mps/pos/address/" . $row["posorder_posaddress"];

			$bodytext = str_replace('{link}', $link, $mail_text);
			$bodytext = str_replace('{recipient_name}', $recipient_name, $bodytext);
			$bodytext = str_replace('{sender_name}', $sender_name, $bodytext);
			$bodytext = str_replace('{date}', to_system_date($row["project_planned_closing_date"]), $bodytext);

			$mail->add_text($bodytext);
			
			
			//check if mail was sent meanwhile
			$sql_m = 'select count(mail_alert_id) as num_recs ' .
				     'from mail_alerts where mail_alert_type = 25 and mail_alert_order = ' . $row["order_id"];
			$res_m = mysql_query($sql_m) or dberror($sql_m);
			$row_m = mysql_fetch_assoc($res_m);

			if($row_m['num_recs'] == 0) {
			
				$result = $mail->send();
				//$result = 1;

				if($result == 1)
				{
					
					foreach($recipients as $user_id=>$user_data)
					{
						append_mail($row["order_id"], $user_id, $sender_id, $bodytext, "910", 1);
					}
					foreach($cc_recipients as $user_id=>$user_data)
					{
						if(!array_key_exists($user_id, $recipients))
						{
							append_mail($row["order_id"], $user_id, $sender_id, $bodytext, "910", 1);
						}
					}

					//update mail_alerts
					$fields = array();
					$values = array();

					$fields[] = "mail_alert_type";
					$values[] = $mail_alert_type_id;

					$fields[] = "mail_alert_order";
					$values[] = $row["order_id"];

					$fields[] = "mail_alert_date";
					$values[] = dbquote($today);

					$fields[] = "mail_alert_sender";
					$values[] = dbquote($sender_email);

					$fields[] = "mail_alert_recipient";
					$values[] = dbquote($rtc_email);

					$fields[] = "date_created";
					$values[] = "current_timestamp";

					$fields[] = "date_modified";
					$values[] = "current_timestamp";

					$fields[] = "user_created";
					$values[] = dbquote(user_login());

					$fields[] = "user_modified";
					$values[] = dbquote(user_login());

					$sql = "insert into mail_alerts (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);
				}
			}
		}
    }

	return true;
}


/********************************************************************
    Ask Client
	to enter the opening hours for a POS location
*********************************************************************/
function ma_opening_hours()
{

	$today = date("Y-m-d");

	//send mails for new projects
	$sql = "select * from mail_alert_types where mail_alert_type_id = 30";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email = $row["mail_alert_type_sender_email"];
	$sender_name = $row["mail_alert_type_sender_name"];
	$mail_text_original = $row["mail_alert_mail_text"];
    $mail_alert_type_id = 30;
	$mail_alert_type_cc1 = $row["mail_alert_type_cc1"];
	$mail_alert_type_cc2 = $row["mail_alert_type_cc2"];
	$mail_alert_type_cc3 = $row["mail_alert_type_cc3"];
	$mail_alert_type_cc4 = $row["mail_alert_type_cc4"];

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id = $row["user_id"];


	$sql = "select DISTINCT posaddress_id, posaddress_client_id , posaddress_name, " . 
		   "place_name, country_name, posaddress_ownertype, posaddress_country " . 
		   "from posaddresses " . 
           "left join posopeninghrs on posopeninghr_posaddress_id = posaddress_id " .
           "inner join countries on country_id = posaddress_country " . 
           "inner join places on place_id = posaddress_place_id " . 
           "where posaddress_store_openingdate >= '2014-03-04' " . 
		   " and posopeninghr_id is null " .
		   " and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') " . 
	       "and (select count(posmail_posaddress_id) from posmails where posmail_mail_alert_type = 30 and posmail_posaddress_id = posaddress_id) = 0";

    $res = mysql_query($sql) or dberror($sql);

    while($row = mysql_fetch_assoc($res))
    {
		$mail_text = $mail_text_original;
		//get project owner
		$po_email = "";
		
		$sql_c = "select order_user " . 
			     " from posorders " .
			     " inner join projects on project_order = posorder_order " .
			     " inner join orders on order_id = project_order " . 
			     " where posorder_posaddress = " . $row["posaddress_id"] . 
			     " and project_actual_opening_date is not null " . 
			     " and project_actual_opening_date <> '0000-00-00' " . 
			     " and (project_shop_closingdate is null or project_shop_closingdate = '0000-00-00') " . 
			     " order by posorder_id DESC";

		$res_c = mysql_query($sql_c) or dberror($sql_c);

		if($row_c = mysql_fetch_assoc($res_c))
		{
			$sql_c = "select user_firstname, user_name, user_email from users " .
					 "where user_id = " . dbquote($row_c["order_user"]) . 
					 " and user_active = 1";

			$res_c = mysql_query($sql_c) or dberror($sql_c);
			if($row_c = mysql_fetch_assoc($res_c))
			{
				$po_firstname = $row_c["user_firstname"];
				$po_name = $row_c["user_name"];
				$po_email = $row_c["user_email"];
			}
		}

		//get user having entered the pos in case there is no project
		if($po_email == "")
		{
			$sql = "select user_id, user_email, user_firstname, user_name, " . 
				   "concat(user_name, ' ', user_firstname) as username, ".
				   "user_email_cc, user_email_deputy " . 
				   "from users ".
				   "inner join posaddresses on posaddresses.user_created = user_login ". 
				   "where (posaddress_id = '" . $row["posaddress_id"] . "' " . 
				   "   and user_active = 1)";

			$res_p = mysql_query($sql) or dberror($sql);
			if($row_p = mysql_fetch_assoc($res_p))
			{
				$po_firstname = $row_p["user_firstname"];
				$po_name = $row_p["user_name"];
				$po_email = $row_p["user_email"];
			}

		}
		
		
		//get brand manager
		$bm_email = "";
		$sql_c = "select user_firstname, user_name, user_email from users " .
			     "inner join user_roles on user_role_user = user_id " . 
		         "where user_address = " . dbquote($row["posaddress_client_id"]) . 
			     " and user_role_role = 15 and user_active = 1";

		$res_c = mysql_query($sql_c) or dberror($sql_c);

		if($row_c = mysql_fetch_assoc($res_c))
		{
			$bm_firstname = $row_c["user_firstname"];
			$bm_name = $row_c["user_name"];
			$bm_email = $row_c["user_email"];
		
		}
		else // get brand manager in case of an affiliate
		{
			$sql_c = "select address_client_type, address_parent " . 
					 " from addresses " . 
					 " where address_id = " . dbquote($row["posaddress_client_id"]);
			$res_c = mysql_query($sql_c) or dberror($sql_c);
			if($row_c = mysql_fetch_assoc($res_c))
			{
				if($row_c["address_client_type"] == 3) //Affiliate
				{
					$sql_c = "select user_firstname, user_name, user_email from users " .
							 "inner join user_roles on user_role_user = user_id " . 
							 "where user_address = " . dbquote($row_c["address_parent"]) . 
							 " and user_role_role = 15 and user_active = 1";

					$res_c = mysql_query($sql_c) or dberror($sql_c);

					if($row_c = mysql_fetch_assoc($res_c))
					{
						$bm_firstname = $row_c["user_firstname"];
						$bm_name = $row_c["user_name"];
						$bm_email = $row_c["user_email"];
					
					}
				}
			}
		}


		//get retail manager
		$rm_email = "";
		$sql_c = "select user_firstname, user_name, user_email from users " .
			     "inner join user_roles on user_role_user = user_id " . 
		         "where user_address = " . dbquote($row["posaddress_client_id"]) . 
			     " and user_role_role = 16 and user_active = 1";

		$res_c = mysql_query($sql_c) or dberror($sql_c);

		if($row_c = mysql_fetch_assoc($res_c))
		{
			$rm_firstname = $row_c["user_firstname"];
			$rm_name = $row_c["user_name"];
			$rm_email = $row_c["user_email"];
		
		}
			

		//get local retail coordinator
		$lrc_email = "";
		
		$sql_c = "select project_local_retail_coordinator " . 
			     " from posorders " . 
			     " inner join projects on project_order = posorder_order " . 
			     " where posorder_posaddress = " . $row["posaddress_id"] . 
			     " and project_actual_opening_date is not null " . 
			     " and project_actual_opening_date <> '0000-00-00' " . 
			     " and (project_shop_closingdate is null or project_shop_closingdate = '0000-00-00') " . 
			     " order by posorder_id DESC";
		
		$res_c = mysql_query($sql_c) or dberror($sql_c);

		if($row_c = mysql_fetch_assoc($res_c))
		{
			$sql_c = "select user_firstname, user_name, user_email from users " .
					 "where user_id = " . dbquote($row_c["project_local_retail_coordinator"]) . 
					 " and user_active = 1";

			$res_c = mysql_query($sql_c) or dberror($sql_c);

			if($row_c = mysql_fetch_assoc($res_c))
			{
				$lrc_firstname = $row_c["user_firstname"];
				$lrc_name = $row_c["user_name"];
				$lrc_email = $row_c["user_email"];
			
			}
		}

		
		//if($po_email != "" and ($bm_email or $rm_email))
		if($po_email != "")
		{
			
			$subject = "Opening hours missing - " . $row["country_name"] . ", " . $row["place_name"] . ", " . $row["posaddress_name"];


			
			$bodytext = str_replace('{pos_location}',$row["posaddress_name"], $mail_text);

			$link = APPLICATION_URL ."/pos/posopeninghr.php?country=" . $row["posaddress_country"] . "&ltf=&id=" . $row["posaddress_id"];

			$bodytext = str_replace('{link}',$link, $bodytext);
			

			$mail = new Mail();
			$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
			$mail->set_sender($sender_email, $sender_name);

			
			if($po_email != "")
			{
				$mail->add_recipient($po_email);

				/*
				if($bm_email and $po_email != $bm_email)
				{
					$mail->add_cc($bm_email);
				}
				*/
				if($rm_email and $po_email != $rm_email)
				{
					$mail->add_cc($rm_email);
				}

				if($lrc_email and $po_email != $lrc_email and $lrc_email != $rm_email)
				{
					$mail->add_cc($lrc_email);
				}
				

				$bodytext = str_replace('{name}', $po_firstname, $bodytext);
			}
			/*
			elseif($bm_email and $bm_email != $rm_email)
			{
				$mail->add_recipient($bm_email);
				if($rm_email)
				{
					$mail->add_cc($rm_email);
				}

				$bodytext = str_replace('{name}', $bm_firstname, $bodytext);
			}
			else
			{
				$mail->add_recipient($rm_email);
				$bodytext = str_replace('{name}', $rm_firstname, $bodytext);
			}
			*/

			

			if($mail_alert_type_cc1)
			{
				$mail->add_cc($mail_alert_type_cc1);
			}
			if($mail_alert_type_cc2)
			{
				$mail->add_cc($mail_alert_type_cc2);
			}
			if($mail_alert_type_cc3)
			{
				$mail->add_cc($mail_alert_type_cc3);
			}
			if($mail_alert_type_cc4)
			{
				$mail->add_cc($mail_alert_type_cc4);
			}

			
			$mail->add_text($bodytext);

			
			
			//check if mail was sent meanwhile
			$sql_m = 'select count(posmail_id) as num_recs ' .
				     'from posmails where posmail_mail_alert_type = 30 ' . 
				     ' and posmail_posaddress_id = ' . $row["posaddress_id"];
			$res_m = mysql_query($sql_m) or dberror($sql_m);
			$row_m = mysql_fetch_assoc($res_m);

			if($po_email != "" and $row_m['num_recs'] == 0) {
			
				$result = $mail->send();
				//$result = 1;
				if($result == 1)
				{
					
					if($po_email)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 30;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($po_email);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}

					/*
					if($bm_email and $po_email != $bm_email)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 30;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($bm_email);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}
					*/

					if($rm_email  and $po_email != $rm_email)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 30;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($rm_email);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);

						
					}

					if($lrc_email  and $po_email != $lrc_email  and $rm_email != $lrc_email)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 30;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($lrc_email);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}
					
				}
			}
		}
    }

	return true;
}


/********************************************************************
    Ask Client
	to enter thecustomer services for a POS location
*********************************************************************/
function ma_customer_services()
{

	$today = date("Y-m-d");

	//send mails for new projects
	$sql = "select * from mail_alert_types where mail_alert_type_id = 31";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email = $row["mail_alert_type_sender_email"];
	$sender_name = $row["mail_alert_type_sender_name"];
	$mail_text_original = $row["mail_alert_mail_text"];
    $mail_alert_type_id = 31;
	$mail_alert_type_cc1 = $row["mail_alert_type_cc1"];
	$mail_alert_type_cc2 = $row["mail_alert_type_cc2"];
	$mail_alert_type_cc3 = $row["mail_alert_type_cc3"];
	$mail_alert_type_cc4 = $row["mail_alert_type_cc4"];

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id = $row["user_id"];


	$sql = "SELECT DISTINCT
				posaddress_id,
				posaddress_client_id,
				posaddress_name,
				place_name,
				country_name,
				posaddress_ownertype,
				posaddress_country,posaddress_store_openingdate
			FROM
				posaddresses
			INNER JOIN countries ON country_id = posaddress_country
			INNER JOIN places ON place_id = posaddress_place_id
			WHERE
				posaddress_store_openingdate >= '2014-03-04'
			  and (posaddress_store_closingdate is null or posaddress_store_closingdate >= '0000-00-00')
			AND (
				SELECT
					count(posmail_posaddress_id)
				FROM
					posmails
				WHERE
					posmail_mail_alert_type = 31
				AND posmail_posaddress_id = posaddress_id
			) = 0";

    $res = mysql_query($sql) or dberror($sql);

    while($row = mysql_fetch_assoc($res))
    {

		$mail_text = $mail_text_original;
		//get project owner
		$po_email = "";
		
		
		$sql_c = "select order_user " . 
			     " from posorders " .
			     " inner join projects on project_order = posorder_order " .
			     " inner join orders on order_id = project_order " . 
			     " where posorder_posaddress = " . $row["posaddress_id"] . 
			     " and project_actual_opening_date is not null " . 
			     " and project_actual_opening_date <> '0000-00-00' " . 
			     " and (project_shop_closingdate is null or project_shop_closingdate = '0000-00-00') " . 
			     " order by posorder_id DESC";

		$res_c = mysql_query($sql_c) or dberror($sql_c);

		if($row_c = mysql_fetch_assoc($res_c))
		{
			$sql_c = "select user_firstname, user_name, user_email from users " .
					 "where user_id = " . dbquote($row_c["order_user"]) . 
					 " and user_active = 1";

			$res_c = mysql_query($sql_c) or dberror($sql_c);
			if($row_c = mysql_fetch_assoc($res_c))
			{
				$po_firstname = $row_c["user_firstname"];
				$po_name = $row_c["user_name"];
				$po_email = $row_c["user_email"];
			}
		}


		//get user having entered the pos in case there is no project
		if($po_email == "")
		{
			$sql = "select user_id, user_email, user_firstname, user_name, " . 
				   "concat(user_name, ' ', user_firstname) as username, ".
				   "user_email_cc, user_email_deputy " . 
				   "from users ".
				   "inner join posaddresses on posaddresses.user_created = user_login ". 
				   "where (posaddress_id = '" . $row["posaddress_id"] . "' " . 
				   "   and user_active = 1)";

			$res_p = mysql_query($sql) or dberror($sql);
			if($row_p = mysql_fetch_assoc($res_p))
			{
				$po_firstname = $row_p["user_firstname"];
				$po_name = $row_p["user_name"];
				$po_email = $row_p["user_email"];
			}

		}

		
		
		//get brand manager
		$bm_email = "";
		
		
		$sql_c = "select user_firstname, user_name, user_email from users " .
			     "inner join user_roles on user_role_user = user_id " . 
		         "where user_address = " . dbquote($row["posaddress_client_id"]) . 
			     " and user_role_role = 15 and user_active = 1";

		$res_c = mysql_query($sql_c) or dberror($sql_c);

		if($row_c = mysql_fetch_assoc($res_c))
		{
			$bm_firstname = $row_c["user_firstname"];
			$bm_name = $row_c["user_name"];
			$bm_email = $row_c["user_email"];
		
		}
		else // get brand manager in case of an affiliate
		{
			$sql_c = "select address_client_type, address_parent " . 
					 " from addresses " . 
					 " where address_id = " . dbquote($row["posaddress_client_id"]);
			$res_c = mysql_query($sql_c) or dberror($sql_c);
			if($row_c = mysql_fetch_assoc($res_c))
			{
				if($row_c["address_client_type"] == 3) //Affiliate
				{
					$sql_c = "select user_firstname, user_name, user_email from users " .
							 "inner join user_roles on user_role_user = user_id " . 
							 "where user_address = " . dbquote($row_c["address_parent"]) . 
							 " and user_role_role = 15 and user_active = 1";

					$res_c = mysql_query($sql_c) or dberror($sql_c);

					if($row_c = mysql_fetch_assoc($res_c))
					{
						$bm_firstname = $row_c["user_firstname"];
						$bm_name = $row_c["user_name"];
						$bm_email = $row_c["user_email"];
					
					}
				}
			}
		}
		

		//get retail manager
		$rm_email = "";
		$sql_c = "select user_firstname, user_name, user_email from users " .
			     "inner join user_roles on user_role_user = user_id " . 
		         "where user_address = " . dbquote($row["posaddress_client_id"]) . 
			     " and user_role_role = 16 and user_active = 1";

		$res_c = mysql_query($sql_c) or dberror($sql_c);

		if($row_c = mysql_fetch_assoc($res_c))
		{
			$rm_firstname = $row_c["user_firstname"];
			$rm_name = $row_c["user_name"];
			$rm_email = $row_c["user_email"];
		
		}


		//get local retail coordinator
		$lrc_email = "";
		
		$sql_c = "select project_local_retail_coordinator " . 
			     " from posorders " . 
			     " inner join projects on project_order = posorder_order " . 
			     " where posorder_posaddress = " . $row["posaddress_id"] . 
			     " and project_actual_opening_date is not null " . 
			     " and project_actual_opening_date <> '0000-00-00' " . 
			     " and (project_shop_closingdate is null or project_shop_closingdate = '0000-00-00') " . 
			     " order by posorder_id DESC";
		
		$res_c = mysql_query($sql_c) or dberror($sql_c);

		if($row_c = mysql_fetch_assoc($res_c))
		{
			$sql_c = "select user_firstname, user_name, user_email from users " .
					 "where user_id = " . dbquote($row_c["project_local_retail_coordinator"]) . 
					 " and user_active = 1";

			$res_c = mysql_query($sql_c) or dberror($sql_c);

			if($row_c = mysql_fetch_assoc($res_c))
			{
				$lrc_firstname = $row_c["user_firstname"];
				$lrc_name = $row_c["user_name"];
				$lrc_email = $row_c["user_email"];
			
			}
		}

		//if($po_email != "" and ($bm_email or $rm_email))
		if($po_email != "")
		{
			
			$subject = "Customer services missing - " . $row["country_name"] . ", " . $row["place_name"] . ", " . $row["posaddress_name"];


			
			$bodytext = str_replace('{pos_location}',$row["posaddress_name"], $mail_text);

			$link = APPLICATION_URL ."/pos/posindex_customerservice.php?pos_id=" . $row["posaddress_id"] . "&country=" . $row["posaddress_country"] . "&ltf=all&ostate=&province=";

			

			$bodytext = str_replace('{link}',$link, $bodytext);
			

			$mail = new Mail();
			$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
			$mail->set_sender($sender_email, $sender_name);

			
			


			if($po_email != "")
			{
				$mail->add_recipient($po_email);

				/*
				if($bm_email and $po_email != $bm_email)
				{
					$mail->add_cc($bm_email);
				}
				*/


				if($rm_email and $po_email != $rm_email)
				{
					$mail->add_cc($rm_email);
				}

				if($lrc_email and $po_email != $lrc_email and $lrc_email != $rm_email)
				{
					$mail->add_cc($lrc_email);
				}
				

				$bodytext = str_replace('{name}', $po_firstname, $bodytext);
			}
			
			/*
			elseif($bm_email and $bm_email != $rm_email)
			{
				$mail->add_recipient($bm_email);
				if($rm_email)
				{
					$mail->add_cc($rm_email);
				}

				$bodytext = str_replace('{name}', $bm_firstname, $bodytext);
			}
			else
			{
				$mail->add_recipient($rm_email);
				$bodytext = str_replace('{name}', $rm_firstname, $bodytext);
			}
			*/

			

			if($mail_alert_type_cc1)
			{
				$mail->add_cc($mail_alert_type_cc1);
			}
			if($mail_alert_type_cc2)
			{
				$mail->add_cc($mail_alert_type_cc2);
			}
			if($mail_alert_type_cc3)
			{
				$mail->add_cc($mail_alert_type_cc3);
			}
			if($mail_alert_type_cc4)
			{
				$mail->add_cc($mail_alert_type_cc4);
			}

			

			
			
			$mail->add_text($bodytext);

			
			
			//check if mail was sent meanwhile
			$sql_m = 'select count(posmail_id) as num_recs ' .
				     'from posmails where posmail_mail_alert_type = 31 ' . 
				     ' and posmail_posaddress_id = ' . $row["posaddress_id"];
			$res_m = mysql_query($sql_m) or dberror($sql_m);
			$row_m = mysql_fetch_assoc($res_m);

			if($po_email != "" and $row_m['num_recs'] == 0) {
		
				$result = $mail->send();

				if($result == 1)
				{
					
					if($po_email)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 31;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($po_email);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}

					/*
					if($bm_email and $po_email != $bm_email)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 31;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($bm_email);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}
					*/

					if($rm_email  and $po_email != $rm_email)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 31;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($rm_email);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);

						
					}

					if($lrc_email  and $po_email != $lrc_email  and $rm_email != $lrc_email)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 31;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($lrc_email);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}
				}
			}
		}
    }

	return true;
}


/********************************************************************
    Ask Brand Manager to enter closing date
	in case a planned closing date was entered
*********************************************************************/
function ma_planned_closing_date()
{

	$today = date("Y-m-d");
	$add_days = 2;

	$date = strtotime($today);
	$date = strtotime("-2 day", $date);
	$today = date('Y-m-d', $date);

	//send mails for new projects
	$sql = "select * from mail_alert_types where mail_alert_type_id = 34";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email = $row["mail_alert_type_sender_email"];
	$sender_name = $row["mail_alert_type_sender_name"];
	$mail_text_original = $row["mail_alert_mail_text"];
    $mail_alert_type_id = 34;
	$mail_alert_type_cc1 = $row["mail_alert_type_cc1"];
	$mail_alert_type_cc2 = $row["mail_alert_type_cc2"];
	$mail_alert_type_cc3 = $row["mail_alert_type_cc3"];
	$mail_alert_type_cc4 = $row["mail_alert_type_cc4"];

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id = $row["user_id"];


	$sql = "select DISTINCT posaddress_id, posaddress_client_id , posaddress_name, " . 
		   "place_name, country_name, posaddress_ownertype, posaddress_country, posaddress_ownertype " . 
		   "from posaddresses " . 
           "inner join posaddress_customerservices on posaddress_customerservice_posaddress_id = posaddress_id " .
           "inner join countries on country_id = posaddress_country " . 
           "inner join places on place_id = posaddress_place_id " . 
           "where (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') " .
		   " and posaddress_store_planned_closingdate is not null " . 
		   " and posaddress_store_planned_closingdate <> '0000-00-00' " .
		   " and posaddress_store_subclass <> 27 " .
		   " and posaddress_store_planned_closingdate <= " . dbquote($today);


    $res = mysql_query($sql) or dberror($sql);

    while($row = mysql_fetch_assoc($res))
    {
		
		$legal_type = $row["posaddress_ownertype"];
		
		$mail_text = $mail_text_original;

		//get brand manager
		$bm_email = "";
		$sql_c = "select user_firstname, user_name, user_email, user_email_cc, user_email_deputy from users " .
			     "inner join user_roles on user_role_user = user_id " . 
		         "where user_address = " . dbquote($row["posaddress_client_id"]) . 
			     " and user_role_role = 15 and user_active = 1";

		$res_c = mysql_query($sql_c) or dberror($sql_c);

		if($row_c = mysql_fetch_assoc($res_c))
		{
			$bm_firstname = $row_c["user_firstname"];
			$bm_name = $row_c["user_name"];
			$bm_email = $row_c["user_email"];
			$bm_email_cc = $row_c["user_email_cc"];
			$bm_email_cc2 = $row_c["user_email_deputy"];
		
		}
		else // get brand manager in case of an affiliate
		{
			$sql_c = "select address_client_type, address_parent " . 
					 " from addresses " . 
					 " where address_id = " . dbquote($row["posaddress_client_id"]);
			$res_c = mysql_query($sql_c) or dberror($sql_c);
			if($row_c = mysql_fetch_assoc($res_c))
			{
				if($row_c["address_client_type"] == 3) //Affiliate
				{
					$sql_c = "select user_firstname, user_name, user_email, user_email_cc, user_email_deputy from users " .
							 "inner join user_roles on user_role_user = user_id " . 
							 "where user_address = " . dbquote($row_c["address_parent"]) . 
							 " and user_role_role = 15 and user_active = 1";

					$res_c = mysql_query($sql_c) or dberror($sql_c);

					if($row_c = mysql_fetch_assoc($res_c))
					{
						$bm_firstname = $row_c["user_firstname"];
						$bm_name = $row_c["user_name"];
						$bm_email = $row_c["user_email"];
						$bm_email_cc = $row_c["user_email_cc"];
						$bm_email_cc2 = $row_c["user_email_deputy"];
					}
				}
			}
		}



		//get retail manager
		$rm_email = "";
		$rm_email_cc = "";
		$rm_email_cc2 = "";

		if($legal_type == 1) // corporate
		{
			$sql_c = "select user_firstname, user_name, user_email, user_email_cc, user_email_deputy from users " .
					 "inner join user_roles on user_role_user = user_id " . 
					 "where user_address = " . dbquote($row["posaddress_client_id"]) . 
					 " and user_role_role = 16 and user_active = 1";

			$res_c = mysql_query($sql_c) or dberror($sql_c);

			while($row_c = mysql_fetch_assoc($res_c))
			{
				$rm_firstname = $row_c["user_firstname"];
				$rm_name = $row_c["user_name"];
				$rm_email = $row_c["user_email"];
				$rm_email_cc = $row_c["user_email_cc"];
				$rm_email_cc2 = $row_c["user_email_deputy"];
			
			}
		}


		//get local retail coordinator
		$lrc_email = "";
		$lrc_email_cc = "";
		$lrc_email_cc2 = "";

		$sql_c = "select user_firstname, user_name, user_email, user_email_cc, user_email_deputy from users " .
				 "inner join user_roles on user_role_user = user_id " . 
				 "where user_address = " . dbquote($row["posaddress_client_id"]) . 
				 " and user_role_role = 10 and user_active = 1";

		$res_c = mysql_query($sql_c) or dberror($sql_c);

		while($row_c = mysql_fetch_assoc($res_c))
		{
			$lrc_firstname = $row_c["user_firstname"];
			$lrc_name = $row_c["user_name"];
			$lrc_email = $row_c["user_email"];
			$lrc_email_cc = $row_c["user_email_cc"];
			$lrc_email_cc2 = $row_c["user_email_deputy"];
		
		}



		//get the owner of tha latest project
		$po_firstname = "";
		$po_name = "";
		$po_email = "";
		$po_email_cc = "";
		$po_email_cc2 = "";

		$sql_c = "select user_firstname, user_name, user_email, user_address, user_email_cc, user_email_deputy " . 
			     " from posorders " .
			     " inner join orders on posorder_order = order_id " . 
			     " inner join users on user_id = order_user " . 
			     " where posorder_type = 1 " . 
			     " and posorder_posaddress = " . dbquote($row["posaddress_id"]) .
			     " and user_active = 1 " .
			     " order by posorder_opening_date DESC";

		$res_c = mysql_query($sql_c) or dberror($sql_c);

		if($row_c = mysql_fetch_assoc($res_c))
		{
			$po_firstname = $row_c["user_firstname"];
			$po_name = $row_c["user_name"];
			$po_email = $row_c["user_email"];
			$po_email_cc = $row_c["user_email_cc"];
			$po_email_cc2 = $row_c["user_email_deputy"];
		}

		if($po_email == "") // get first standard user of the address
		{
			$sql_c = "select user_firstname, user_name, user_email, user_email_cc, user_email_deputy from users " .
					 "inner join user_roles on user_role_user = user_id " . 
					 "where user_address = " . dbquote($row["posaddress_client_id"]) . 
					 " and user_role_role = 4 and user_active = 1";

			
			$res_c = mysql_query($sql_c) or dberror($sql_c);

			if($row_c = mysql_fetch_assoc($res_c))
			{
				$po_firstname = $row_c["user_firstname"];
				$po_name = $row_c["user_name"];
				$po_email = $row_c["user_email"];
				$po_email_cc = $row_c["user_email_cc"];
				$po_email_cc2 = $row_c["user_email_deputy"];
			}
		}

		
		if($bm_email)
		{

			$subject = "Closing date is missing - " . $row["country_name"] . ", " . $row["place_name"] . ", " . $row["posaddress_name"];

			$bodytext = str_replace('{pos_location}',$row["posaddress_name"], $mail_text);

			$link = APPLICATION_URL ."/mps/pos/address/" . $row["posaddress_id"];

			

			$bodytext = str_replace('{link}',$link, $bodytext);
			

			$mail = new Mail();
			$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
			$mail->set_sender($sender_email, $sender_name);

			
			$mail->add_recipient($bm_email);

			if($bm_email_cc)
			{
				$mail->add_cc($bm_email_cc);
			}
			if($bm_email_cc2)
			{
				$mail->add_cc($bm_email_cc2);
			}

			if($rm_email and $rm_email != $bm_email and $rm_email != $bm_email_cc and $rm_email != $bm_email_cc2)
			{
				$mail->add_cc($rm_email);
			}

			if($lrc_email 
				and $lrc_email != $bm_email 
				and $lrc_email != $bm_email_cc 
				and $lrc_email != $bm_email_cc2
				and $lrc_email != $rm_email )
			{
				$mail->add_cc($lrc_email);
			}

			if($po_email 
				and $po_email != $bm_email 
				and $po_email != $bm_email_cc 
				and $po_email != $bm_email_cc2
				and $po_email != $rm_email 
				and $po_email != $lrc_email)
			{
				$mail->add_cc($po_email);
			}

			$bodytext = str_replace('{name}', $bm_firstname, $bodytext);
			

			if($mail_alert_type_cc1)
			{
				$mail->add_cc($mail_alert_type_cc1);
			}
			if($mail_alert_type_cc2)
			{
				$mail->add_cc($mail_alert_type_cc2);
			}
			if($mail_alert_type_cc3)
			{
				$mail->add_cc($mail_alert_type_cc3);
			}
			if($mail_alert_type_cc4)
			{
				$mail->add_cc($mail_alert_type_cc4);
			}

			$mail->add_text($bodytext);

			//check if mail was sent meanwhile
			$sql_m = 'select count(posmail_id) as num_recs ' .
				     'from posmails where posmail_mail_alert_type = 34 ' . 
				     ' and posmail_posaddress_id = ' . $row["posaddress_id"];

			$res_m = mysql_query($sql_m) or dberror($sql_m);
			$row_m = mysql_fetch_assoc($res_m);

			if($row_m['num_recs'] == 0) {
			
				$result = $mail->send();

				if($result == 1)
				{
					if($bm_email)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 34;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($bm_email);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}
					if($bm_email_cc)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 34;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($bm_email_cc);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}
					if($bm_email_cc2)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 34;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($bm_email_cc2);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}
					if($rm_email)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 34;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($rm_email);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}

					if($lrc_email)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 34;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($lrc_email);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}

					if($po_email)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 34;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($po_email);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}
				}
			}
		}
		elseif($rm_email)
		{
			
			$subject = "Closing date is missing - " . $row["country_name"] . ", " . $row["place_name"] . ", " . $row["posaddress_name"];

			$bodytext = str_replace('{pos_location}',$row["posaddress_name"], $mail_text);

			$link = APPLICATION_URL ."/mps/pos/address/" . $row["posaddress_id"];

			

			$bodytext = str_replace('{link}',$link, $bodytext);
			

			$mail = new Mail();
			$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
			$mail->set_sender($sender_email, $sender_name);

			
			$mail->add_recipient($rm_email);

			if($rm_email_cc)
			{
				$mail->add_cc($rm_email_cc);
			}
			if($rm_email_cc2)
			{
				$mail->add_cc($rm_email_cc2);
			}

			if($lrc_email 
				and $lrc_email != $rm_email 
				and $lrc_email != $rm_email_cc 
				and $lrc_email != $rm_email_cc2)
			{
				$mail->add_cc($lrc_email);
			}

			if($po_email 
				and $po_email != $rm_email 
				and $po_email != $rm_email_cc 
				and $po_email != $rm_email_cc2
				and $po_email != $lrc_email)
			{
				$mail->add_cc($po_email);
			}

			
			$bodytext = str_replace('{name}', $rm_firstname, $bodytext);
			

			if($mail_alert_type_cc1)
			{
				$mail->add_cc($mail_alert_type_cc1);
			}
			if($mail_alert_type_cc2)
			{
				$mail->add_cc($mail_alert_type_cc2);
			}
			if($mail_alert_type_cc3)
			{
				$mail->add_cc($mail_alert_type_cc3);
			}
			if($mail_alert_type_cc4)
			{
				$mail->add_cc($mail_alert_type_cc4);
			}

			$mail->add_text($bodytext);

			//check if mail was sent meanwhile
			$sql_m = 'select count(posmail_id) as num_recs ' .
				     'from posmails where posmail_mail_alert_type = 34 ' . 
				     ' and posmail_posaddress_id = ' . $row["posaddress_id"];

			$res_m = mysql_query($sql_m) or dberror($sql_m);
			$row_m = mysql_fetch_assoc($res_m);

			if($row_m['num_recs'] == 0) {
			
				$result = $mail->send();

				if($result == 1)
				{
					if($rm_email)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 34;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($rm_email);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}
					if($rm_email_cc)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 34;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($rm_email_cc);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}
					if($rm_email_cc2)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 34;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($rm_email_cc2);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}

					if($lrc_email)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 34;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($lrc_email);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}


					if($po_email)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 34;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($po_email);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}
				}
			}
		}
		elseif($lrc_email)
		{
			
			$subject = "Closing date is missing - " . $row["country_name"] . ", " . $row["place_name"] . ", " . $row["posaddress_name"];

			$bodytext = str_replace('{pos_location}',$row["posaddress_name"], $mail_text);

			$link = APPLICATION_URL ."/mps/pos/address/" . $row["posaddress_id"];

			

			$bodytext = str_replace('{link}',$link, $bodytext);
			

			$mail = new Mail();
			$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
			$mail->set_sender($sender_email, $sender_name);

			
			$mail->add_recipient($lrc_email);

			if($lrc_email_cc)
			{
				$mail->add_cc($lrc_email_cc);
			}
			if($lrc_email_cc2)
			{
				$mail->add_cc($lrc_email_cc2);
			}


			if($po_email 
				and $po_email != $lcr_email 
				and $po_email != $lcr_email_cc 
				and $po_email != $lcr_email_cc2)
			{
				$mail->add_cc($po_email);
			}

			
			$bodytext = str_replace('{name}', $rm_firstname, $bodytext);
			

			if($mail_alert_type_cc1)
			{
				$mail->add_cc($mail_alert_type_cc1);
			}
			if($mail_alert_type_cc2)
			{
				$mail->add_cc($mail_alert_type_cc2);
			}
			if($mail_alert_type_cc3)
			{
				$mail->add_cc($mail_alert_type_cc3);
			}
			if($mail_alert_type_cc4)
			{
				$mail->add_cc($mail_alert_type_cc4);
			}

			$mail->add_text($bodytext);

			//check if mail was sent meanwhile
			$sql_m = 'select count(posmail_id) as num_recs ' .
				     'from posmails where posmail_mail_alert_type = 34 ' . 
				     ' and posmail_posaddress_id = ' . $row["posaddress_id"];

			$res_m = mysql_query($sql_m) or dberror($sql_m);
			$row_m = mysql_fetch_assoc($res_m);

			if($row_m['num_recs'] == 0) {
			
				$result = $mail->send();

				if($result == 1)
				{
					if($lrc_email)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 34;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($lrc_email);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}

					if($lrc_email_cc)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 34;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($lrc_email_cc);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}

					if($lrc_email_cc2)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 34;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($lrc_email_cc2);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}


					if($po_email)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 34;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($po_email);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}
				}
			}
		}
		elseif($po_email)
		{
			
			$subject = "Closing date is missing - " . $row["country_name"] . ", " . $row["place_name"] . ", " . $row["posaddress_name"];

			$bodytext = str_replace('{pos_location}',$row["posaddress_name"], $mail_text);

			$link = APPLICATION_URL ."/mps/pos/address/" . $row["posaddress_id"];

			

			$bodytext = str_replace('{link}',$link, $bodytext);
			

			$mail = new Mail();
			$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
			$mail->set_sender($sender_email, $sender_name);

			
			$mail->add_recipient($po_email);

			if($po_email_cc)
			{
				$mail->add_cc($po_email_cc);
			}
			if($po_email_cc2)
			{
				$mail->add_cc($po_email_cc2);
			}

			
			$bodytext = str_replace('{name}', $rm_firstname, $bodytext);
			

			if($mail_alert_type_cc1)
			{
				$mail->add_cc($mail_alert_type_cc1);
			}
			if($mail_alert_type_cc2)
			{
				$mail->add_cc($mail_alert_type_cc2);
			}
			if($mail_alert_type_cc3)
			{
				$mail->add_cc($mail_alert_type_cc3);
			}
			if($mail_alert_type_cc4)
			{
				$mail->add_cc($mail_alert_type_cc4);
			}

			$mail->add_text($bodytext);

			//check if mail was sent meanwhile
			$sql_m = 'select count(posmail_id) as num_recs ' .
				     'from posmails where posmail_mail_alert_type = 34 ' . 
				     ' and posmail_posaddress_id = ' . $row["posaddress_id"];

			$res_m = mysql_query($sql_m) or dberror($sql_m);
			$row_m = mysql_fetch_assoc($res_m);

			if($row_m['num_recs'] == 0) {
			
				$result = $mail->send();

				if($result == 1)
				{
					if($po_email)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 34;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($po_email);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}

					if($po_email_cc)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 34;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($po_email_cc);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}

					if($po_email_cc2)
					{
						//update mail_alerts
						$fields = array();
						$values = array();

						$fields[] = "posmail_posaddress_id";
						$values[] = $row["posaddress_id"];

						$fields[] = "posmail_mail_alert_type";
						$values[] = 34;

						$fields[] = "posmail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "posmail_recipeint_email";
						$values[] = dbquote($po_email_cc2);

						$fields[] = "posmail_subject";
						$values[] = dbquote($subject);

						$fields[] = "posmail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "date_created";
						$values[] = "current_timestamp";

						$fields[] = "date_modified";
						$values[] = "current_timestamp";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into posmails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						mysql_query($sql) or dberror($sql);
					}
				}
			}
		}
    }

	return true;
}

/********************************************************************
    Ask Supplier to confirm orders
*********************************************************************/
function send_reminder_for_orders_to_suppliers()
{
	
	$today = date("Y-m-d");
	$add_days = 2;


	//get mail body
	$sql = "select mail_alert_mail_text from mail_alert_types where mail_alert_type_id = 23";
	$res = mysql_query($sql) or dberror($sql);

    if($row = mysql_fetch_assoc($res))
    {
		$mail_text_original = $row["mail_alert_mail_text"];


		$sql = "select task_id, task_user, task_from_user, tasks.date_created as dc, " . 
			   "order_id, order_number, order_type, project_id " . 
			   "from tasks " .
			   "inner join orders on order_id = task_order " .
			   "inner join projects on project_order = order_id " .
			   "where task_order_state in (35, 45) " . 
			   " and (task_reminder_sent is null or task_reminder_sent = '0000-00-00') ".
			   " order by task_order";
		
		
		$res = mysql_query($sql) or dberror($sql);

		while($row_t = mysql_fetch_assoc($res))
		{
		
			$mail_text = $mail_text_original;
			//check date
			$date = substr($row_t["dc"], 0, 10);
			$date = date('Y-m-d',strtotime($date) + (24*3600*$add_days));

			if(date('N', strtotime($date)) == 6)
			{
				$date = date('Y-m-d',strtotime($date) + (24*3600*2));
			}
			elseif(date('N', strtotime($date)) == 7)
			{
				$date = date('Y-m-d',strtotime($date) + (24*3600));
			}

			if($today > $date) // send reminder
			{
				if($row_t["order_type"] == 1)
				{
					$subject = MAIL_SUBJECT_PREFIX . ": Reminder to confirm our order - Project " . $row_t["order_number"];
					$link ="project_task_center.php?pid=" . $row_t["project_id"];
					$link = APPLICATION_URL . "/user/" . $link . "\n\n"; 
				}
				else
				{
					$subject = MAIL_SUBJECT_PREFIX . ": Reminder to confirm our order - Order " . $row_t["order_number"];
					$link ="order_task_center.php?oid=" . $row_t["order_id"];
					$link = APPLICATION_URL . "/user/" . $link . "\n\n"; 
				}

				//get sender
				$sender_name = "";
				$sender_mail = "";
				$bodytext = $mail_text;
				
				$sql_u = "select concat(user_firstname, ' ', user_name) as username, user_email " . 
					     "from users " . 
					     "where user_id = " . $row_t["task_from_user"];
				
				$res_u = mysql_query($sql_u) or dberror($sql_u);

				if($row_u = mysql_fetch_assoc($res_u))
				{
					$sender_name = $row_u["username"];
					$sender_mail = $row_u["user_email"];

					$bodytext = str_replace('{sender_name}', $sender_name, $bodytext);
				}

				//get reciepients
				$recipient_name = "";
				$recipient_mail = "";
				$cc_mail = "";
				$deputy_mail = "";
				
				$sql_u = "select concat(user_firstname, ' ', user_name) as username, user_email, " . 
					     "user_email_cc, user_email_deputy " . 
					     "from users " . 
					     "where user_id = " . $row_t["task_user"];

				
				$res_u = mysql_query($sql_u) or dberror($sql_u);

				if($row_u = mysql_fetch_assoc($res_u))
				{
					$recipient_name = $row_u["username"];
					$recipient_mail = $row_u["user_email"];
					$cc_mail =  $row_u["user_email_cc"];
					$deputy_mail =  $row_u["user_email_deputy"];

					$bodytext = str_replace('{recipient_name}', $recipient_name, $bodytext);
				}

				

	
				
				$bodytext = str_replace('{link}', $link, $bodytext);



				$mail = new Mail();
				$mail->set_subject($subject);
				$mail->set_sender($sender_mail, $sender_name);

				$mail->add_recipient($recipient_mail, $recipient_name);
				if($cc_mail) {$mail->add_cc($cc_mail);}
				if($deputy_mail) {$mail->add_cc($deputy_mail);}

				$mail->add_text($bodytext);

				$result = $mail->send();
				if($result == 1)
				{
					append_mail($row_t["order_id"], $row_t["task_user"], $row_t["task_from_user"], $bodytext, "910", 1);
					$sql_u = "update tasks set task_reminder_sent = " . dbquote(date("Y-m-d H:i:s")) ." where task_id = " . $row_t["task_id"];
					mysql_query($sql_u) or dberror($sql_u);
				}
			}
		}
	}
	
	return true;
}



/********************************************************************
    Ask forwarders to enter arrival date
*********************************************************************/
function send_reminder_for_orders_to_forwarders()
{
	
	$today = date("Y-m-d");
	$add_days = 2;


	//get mail body
	$sql = "select mail_alert_mail_text from mail_alert_types where mail_alert_type_id = 24";
	$res = mysql_query($sql) or dberror($sql);

    if($row = mysql_fetch_assoc($res))
    {
		$mail_text_original = $row["mail_alert_mail_text"];
		
		$order_forwarders = array();
		$order_ids = array();

		$sql = "select order_item_id, order_item_forwarder_address, order_item_expected_arrival, order_item_arrival, " . 
			   "order_id, order_number, order_type, project_id " . 
			   "from order_items " .
			   "inner join orders on order_id = order_item_order " .
			   "inner join projects on project_order = order_id " .
			   "where order_item_forwarder_address > 0 " .
			   " and (order_item_arrival is null or order_item_arrival = '0000-00-00') " .
		       " and order_item_expected_arrival is not null and order_item_expected_arrival <> '0000-00-00 ' " .
			   " and (order_item_reminder_sent_to_forwarder is null or order_item_reminder_sent_to_forwarder = '0000-00-00') ".
			   " and order_actual_order_state_code > '720' and  order_actual_order_state_code < '800' " .
			   " order by order_item_forwarder_address, order_id";

		
		$res = mysql_query($sql) or dberror($sql);

		while($row = mysql_fetch_assoc($res))
		{
			$mail_text = $mail_text_original;
			//check date
			$date = substr($row["order_item_expected_arrival"], 0, 10);
			$date = date('Y-m-d',strtotime($date) + (24*3600*$add_days));

			if(date('N', strtotime($date)) == 6)
			{
				$date = date('Y-m-d',strtotime($date) + (24*3600*2));
			}
			elseif(date('N', strtotime($date)) == 7)
			{
				$date = date('Y-m-d',strtotime($date) + (24*3600));
			}

			if($today > $date) // send reminder
			{
				$order_forwarders[$row["order_id"] . "@" . $row["order_item_forwarder_address"]] = $row["order_id"];
				$order_ids[$row["order_id"]] = $row["order_id"];
			}
		}

		
		
		if(count($order_forwarders) > 0)
		{
			foreach($order_forwarders as $key=>$order_id)
			{
				
				$tmp = explode('@', $key);
				$forwarder_id = $tmp[1];

				$sql = "select order_id, order_number, order_type, order_retail_operator, project_id " . 
					   "from orders " .
					   "inner join projects on project_order = order_id " .
					   "where order_id = " . $order_id;

			
			
				$res = mysql_query($sql) or dberror($sql);
				
				if($row_t = mysql_fetch_assoc($res))
				{
					if($row_t["order_type"] == 1)
					{
						$subject = MAIL_SUBJECT_PREFIX . ": Reminder to confirm arrival of items - Project " . $row_t["order_number"];
						$link ="project_edit_traffic_data.php?pid=" . $row_t["project_id"];
						$link = APPLICATION_URL . "/user/" . $link . "\n\n"; 
					}
					else
					{
						$subject = MAIL_SUBJECT_PREFIX . ": Reminder to confirm arrival of items - Order " . $row_t["order_number"];
						$link ="order_edit_traffic_data.php?oid=" . $row_t["order_id"];
						$link = APPLICATION_URL . "/user/" . $link . "\n\n"; 
					}

					//get sender
					$sender_name = "";
					$sender_mail = "";
					$sender_id = "";
					$bodytext = $mail_text;
					
					$sql_u = "select user_id, concat(user_firstname, ' ', user_name) as username, user_email " . 
							 "from users " . 
							 "where user_id = " . $row_t["order_retail_operator"];
					
					$res_u = mysql_query($sql_u) or dberror($sql_u);

					if($row_u = mysql_fetch_assoc($res_u))
					{
						$sender_name = $row_u["username"];
						$sender_mail = $row_u["user_email"];
						$sender_id = $row_u["user_id"];

						$bodytext = str_replace('{sender_name}', $sender_name, $bodytext);
					}

					



					//get reciepients
					$recipient_name = "";
					$recipient_mail = "";
					$recipient_id = "";
					$cc_mail = "";
					$deputy_mail = "";
					
					$sql_u = "select user_id, concat(user_firstname, ' ', user_name) as username, user_email, " . 
							 "user_email_cc, user_email_deputy " . 
							 "from users " . 
							 "where user_active= 1 and user_address = " . $forwarder_id;

					
					$res_u = mysql_query($sql_u) or dberror($sql_u);

					if($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_name = $row_u["username"];
						$recipient_mail = $row_u["user_email"];
						$cc_mail =  $row_u["user_email_cc"];
						$deputy_mail =  $row_u["user_email_deputy"];
						$recipient_id = $row_u["user_id"];

						$bodytext = str_replace('{recipient_name}', $recipient_name, $bodytext);
					}

					

		
					
					$bodytext = str_replace('{link}', $link, $bodytext);


					$mail = new Mail();
					$mail->set_subject($subject);
					$mail->set_sender($sender_mail, $sender_name);

					$mail->add_recipient($recipient_mail, $recipient_name);
					if($cc_mail) {$mail->add_cc($cc_mail);}
					if($deputy_mail) {$mail->add_cc($deputy_mail);}

					$mail->add_text($bodytext);

					$result = $mail->send();
					//$result = 1;
					if($result == 1)
					{
						append_mail($row_t["order_id"], $recipient_id, $sender_id, $bodytext, "910", 1);
						
						$sql_u = "update order_items set order_item_reminder_sent_to_forwarder = " . dbquote(date("Y-m-d H:i:s")) ." where order_item_order = " . $order_id . " and order_item_forwarder_address = " . dbquote($forwarder_id);
						mysql_query($sql_u) or dberror($sql_u);
					}
				}
				
			}
		}
	}
	
	return true;
}



/********************************************************************
   Ask suppliers to enter pickup dates
*********************************************************************/
function ma_reminder_to_enter_pickup_dates()
{
	
	$today = date("Y-m-d");
	$add_days = 3;


	//get mail body
	$sql = "select mail_alert_mail_text from mail_alert_types where mail_alert_type_id = 35";
	$res = mysql_query($sql) or dberror($sql);

    if($row = mysql_fetch_assoc($res))
    {
		$mail_text_original = $row["mail_alert_mail_text"];
		
		$order_suppliers = array();
		$order_ids = array();

		$sql = "select order_item_id, order_item_supplier_address, order_item_expected_arrival, order_item_arrival, " . 
			   "order_id, order_number, order_type, project_id " . 
			   "from order_items " .
			   "inner join orders on order_id = order_item_order " .
			   "inner join projects on project_order = order_id " .
			   "where order_item_supplier_address > 0 " .
			   " and (order_item_pickup is null or order_item_pickup = '0000-00-00') " .
		       " and order_item_expected_arrival is not null and order_item_expected_arrival <> '0000-00-00 ' " .
			   " and (order_item_reminder_sent_to_supplier is null or order_item_reminder_sent_to_supplier = '0000-00-00') ".
			   " and order_actual_order_state_code > '720' and  order_actual_order_state_code < '800' " .
			   " order by order_item_supplier_address, order_id";

		
		$res = mysql_query($sql) or dberror($sql);

		while($row = mysql_fetch_assoc($res))
		{
			//check date
			$date = substr($row["order_item_expected_arrival"], 0, 10);
			$date = date('Y-m-d',strtotime($date) + (24*3600*$add_days));

			if(date('N', strtotime($date)) == 6)
			{
				$date = date('Y-m-d',strtotime($date) + (24*3600*2));
			}
			elseif(date('N', strtotime($date)) == 7)
			{
				$date = date('Y-m-d',strtotime($date) + (24*3600));
			}

			if($today > $date) // send reminder
			{
				$order_suppliers[$row["order_id"] . "@" . $row["order_item_supplier_address"]] = $row["order_id"];
				$order_ids[$row["order_id"]] = $row["order_id"];
			}
		}

		
		
		if(count($order_suppliers) > 0)
		{
			foreach($order_suppliers as $key=>$order_id)
			{
				
				$tmp = explode('@', $key);
				$supplier_id = $tmp[1];

				$sql = "select order_id, order_number, order_type, order_retail_operator, project_id " . 
					   "from orders " .
					   "inner join projects on project_order = order_id " .
					   "where order_id = " . $order_id;

			
			
				$res = mysql_query($sql) or dberror($sql);
				
				if($row_t = mysql_fetch_assoc($res))
				{
					$mail_text = $mail_text_original;
					if($row_t["order_type"] == 1)
					{
						$subject = MAIL_SUBJECT_PREFIX . ": Reminder to enter pick of items - Project " . $row_t["order_number"];
						$link ="project_edit_supplier_data?pid=" . $row_t["project_id"];
						$link = APPLICATION_URL . "/user/" . $link . "\n\n"; 

						$mail_text = str_replace('{project}', "project " . $row_t["order_number"], $mail_text);
					}
					else
					{
						$subject = MAIL_SUBJECT_PREFIX . ": Reminder to enter pick of items - Order " . $row_t["order_number"];
						$link ="order_edit_supplier_data.php?oid=" . $row_t["order_id"];
						$link = APPLICATION_URL . "/user/" . $link . "\n\n"; 

						$mail_text = str_replace('{project}', "catalogue order " . $row_t["order_number"], $mail_text);
					}

					//get sender
					$sender_name = "";
					$sender_mail = "";
					$sender_id = "";
					$bodytext = $mail_text;
					
					$sql_u = "select user_id, concat(user_firstname, ' ', user_name) as username, user_email " . 
							 "from users " . 
							 "where user_id = " . $row_t["order_retail_operator"];
					
					$res_u = mysql_query($sql_u) or dberror($sql_u);

					if($row_u = mysql_fetch_assoc($res_u))
					{
						$sender_name = $row_u["username"];
						$sender_mail = $row_u["user_email"];
						$sender_id = $row_u["user_id"];

						$bodytext = str_replace('{sender_name}', $sender_name, $bodytext);
						$bodytext = str_replace('{link}', $link, $bodytext);

						
					}

					
					//get reciepients
					$recipient_name = "";
					$recipient_mail = "";
					$recipient_id = "";
					$cc_mail = "";
					$deputy_mail = "";
					
					$sql_u = "select user_id, concat(user_firstname, ' ', user_name) as username, user_email, " . 
							 "user_firstname, user_email_cc, user_email_deputy " . 
							 "from users " . 
							 "where user_active= 1 and user_address = " . $supplier_id;

					
					$res_u = mysql_query($sql_u) or dberror($sql_u);

					if($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_name = $row_u["username"];
						$recipient_firstname = $row_u["user_firstname"];
						$recipient_mail = $row_u["user_email"];
						$cc_mail =  $row_u["user_email_cc"];
						$deputy_mail =  $row_u["user_email_deputy"];
						$recipient_id = $row_u["user_id"];

						$bodytext = str_replace('{recipient_name}', $recipient_name, $bodytext);
					}

					

					
					
					$bodytext = str_replace('{name}', $recipient_firstname, $bodytext);


					$mail = new Mail();
					$mail->set_subject($subject);
					$mail->set_sender($sender_mail, $sender_name);

					$mail->add_recipient($recipient_mail, $recipient_name);
					if($cc_mail) {$mail->add_cc($cc_mail);}
					if($deputy_mail) {$mail->add_cc($deputy_mail);}

					$mail->add_text($bodytext);

					$result = $mail->send();
					$result = 1;
					if($result == 1)
					{
						append_mail($row_t["order_id"], $recipient_id, $sender_id, $bodytext, "910", 1);
						
						$sql_u = "update order_items set order_item_reminder_sent_to_supplier = " . dbquote(date("Y-m-d H:i:s")) ." where order_item_order = " . $order_id . " and order_item_supplier_address = " . dbquote($supplier_id);

						mysql_query($sql_u) or dberror($sql_u);
					}

				}
				
			}
		}
	}
	
	return true;
}


/********************************************************************
    Ask Client
	to communicate closing date for popup POS Locations
*********************************************************************/
function ma_close_popup_locations()
{

	
	$today = date("Y-m-d");

	//send mails for new projects
	$sql = "select * from mail_alert_types where mail_alert_type_id = 38";
	$res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
	
	$sender_email = $row["mail_alert_type_sender_email"];
	$sender_name = $row["mail_alert_type_sender_name"];
	$mail_text_original = $row["mail_alert_mail_text"];
    $mail_alert_type_id = 38;
	$mail_alert_type_cc1 = $row["mail_alert_type_cc1"];
	$mail_alert_type_cc2 = $row["mail_alert_type_cc2"];
	$mail_alert_type_cc3 = $row["mail_alert_type_cc3"];
	$mail_alert_type_cc4 = $row["mail_alert_type_cc4"];

	$sql = "select user_id ".
		   "from users ".
		   "where (user_email = '" . $sender_email . "' " . 
		   "   and user_active = 1)";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$sender_id = $row["user_id"];


	
	$sql = "select project_id, project_projectkind, project_real_opening_date, project_retail_coordinator, " .
		   "order_id, order_archive_date, order_number, order_retail_operator, order_client_address, order_user, " .
		   "order_shop_address_place,order_shop_address_company, order_actual_order_state_code, " .
		   "project_local_retail_coordinator, project_planned_closing_date, project_popup_closingdate, country_name, posorder_posaddress " . 
		   "from projects " .
		   "inner join orders on order_id = project_order " .
		   "inner join posorders on posorder_order = order_id " .
		   "inner join posaddresses on posaddress_id = posorder_posaddress " .
		   "inner join countries on country_id = order_shop_address_country " . 
		   "where (select count(mail_alert_id) from mail_alerts where mail_alert_order = order_id and mail_alert_type IN (38))  = 0 " . 
		   " and project_planned_closing_date is not null " . 
		   " and project_planned_closing_date <> '0000-00-00' " .
		   " and project_actual_opening_date is not null " . 
		   " and project_actual_opening_date <> '0000-00-00' " . 
		   " and (project_popup_closingdate is  null " . 
		   " or project_popup_closingdate = '0000-00-00') " .
		   " and project_projectkind = 8 " . 
		   " and project_planned_closing_date < " . dbquote(date("Y-m-d"));

    $res = mysql_query($sql) or dberror($sql);

    while($row = mysql_fetch_assoc($res))
    {
		$project_id = $row["project_id"];
		$mail_text = $mail_text_original;
		$recipients = array();
		$rtc_email = "";

		//get Project Leader
		$sql_l = "select user_id, user_email, concat(user_firstname, ' ', user_name) as username,  " .
			     "user_email_cc, user_email_deputy " . 
			     "from users " .
			     "where user_id = " . dbquote($row["project_retail_coordinator"]);

		
	    $res_l = mysql_query($sql_l) or dberror($sql_l);

		if($row_l = mysql_fetch_assoc($res_l))
		{
			$recipients[$row_l["user_id"]] = array("email"=>$row_l["user_email"], "username"=>$row_l["username"],  "email_cc"=>$row_l["user_email_cc"], "email_deputy"=>$row_l["user_email_deputy"]);

			$rtc_email = $row_l["user_email"];

		}

		if(count($recipients) > 0)
		{
			
			$subject = "Closing date is due - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];
			

			$mail = new Mail();
			$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
			$mail->set_sender($sender_email, $sender_name);

			

			foreach($recipients as $user_id=>$user_data)
			{
				$mail->add_recipient($user_data["email"]);
				$recipient_name = $user_data["username"];

				if($user_data["email_cc"])
				{
					$mail->add_cc($user_data["email_cc"]);
				}
				if($user_data["email_deputy"])
				{
					$mail->add_cc($user_data["email_deputy"]);
				}

			}

			if($mail_alert_type_cc1)
			{
				$mail->add_cc($mail_alert_type_cc1);
			}
			if($mail_alert_type_cc2)
			{
				$mail->add_cc($mail_alert_type_cc2);
			}
			if($mail_alert_type_cc3)
			{
				$mail->add_cc($mail_alert_type_cc3);
			}
			if($mail_alert_type_cc4)
			{
				$mail->add_cc($mail_alert_type_cc4);
			}

			
			$link = APPLICATION_URL ."/user/project_edit_pos_data.php?pid=" . $project_id;



			$bodytext = str_replace('{link}', $link, $mail_text);
			$bodytext = str_replace('{recipient_name}', $recipient_name, $bodytext);
			$bodytext = str_replace('{sender_name}', $sender_name, $bodytext);
			$bodytext = str_replace('{date}', to_system_date($row["project_planned_closing_date"]), $bodytext);

			$mail->add_text($bodytext);
			
			
			//check if mail was sent meanwhile
			$sql_m = 'select count(mail_alert_id) as num_recs ' .
				     'from mail_alerts where mail_alert_type = 38 and mail_alert_order = ' . $row["order_id"];
			$res_m = mysql_query($sql_m) or dberror($sql_m);
			$row_m = mysql_fetch_assoc($res_m);

			if($row_m['num_recs'] == 0) {
			
				$result = $mail->send();
				//$result = 1;

				if($result == 1)
				{
					
					foreach($recipients as $user_id=>$user_data)
					{
						append_mail($row["order_id"], $user_id, $sender_id, $bodytext, "910", 1);
					}
					

					//update mail_alerts
					$fields = array();
					$values = array();

					$fields[] = "mail_alert_type";
					$values[] = $mail_alert_type_id;

					$fields[] = "mail_alert_order";
					$values[] = $row["order_id"];

					$fields[] = "mail_alert_date";
					$values[] = dbquote($today);

					$fields[] = "mail_alert_sender";
					$values[] = dbquote($sender_email);

					$fields[] = "mail_alert_recipient";
					$values[] = dbquote($rtc_email);

					$fields[] = "date_created";
					$values[] = "current_timestamp";

					$fields[] = "date_modified";
					$values[] = "current_timestamp";

					$fields[] = "user_created";
					$values[] = dbquote(user_login());

					$fields[] = "user_modified";
					$values[] = dbquote(user_login());

					$sql = "insert into mail_alerts (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);
				}
			}
		}
    }

	return true;
}

/********************************************************************
    Ask responsible person
	to complete the projects CMS
*********************************************************************/
function ma_cms_due_dates()
{
	//update due dates of all projects

	$sql_p = "select project_number, project_state, project_order, project_actual_opening_date " . 
		   " from projects " .
		   " inner join orders on order_id = project_order " .
		   " inner join project_costs on project_cost_order = order_id " .
		   " where project_cost_cms_completed <> 1 " .
		   " and (project_actual_opening_date is not null and project_actual_opening_date <> '0000-00-00') " .
		   " and (order_archive_date is null or order_archive_date = '0000-00-00') " . 
		   " and (project_cost_cms_completion_due_date is null or project_cost_cms_completion_due_date = '0000-00-00') " .
		   " and project_state in (1, 4, 5) ";
	

	$res_p = mysql_query($sql_p) or dberror($sql_p);
	while($row_p = mysql_fetch_assoc($res_p))
	{
		//check if project has items
		$sql =" select count(order_item_id) as num_recs " . 
			  " from order_items " . 
			  " where order_item_type in (1, 2, 3, 6, 7) and order_item_order = " . dbquote($row_p["project_order"]);
		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);

		if($row['num_recs'] > 0) 
		{
			//due date 4 months from POS opening date
			$pos_opening_date = $row_p["project_actual_opening_date"];
			$due_date = date('Y-m-d', strtotime("+4 months", strtotime($pos_opening_date)));

			//echo $pos_opening_date ." " . $row_p["project_number"] . " " . $due_date;abc();

			$sql_u = "update project_costs " . 
					 " set project_cost_cms_completion_due_date = " . dbquote($due_date) . 
					 " where project_cost_order = " . dbquote($row_p["project_order"]);

			$result = mysql_query($sql_u) or dberror($sql_u);
		}		
	}
	return true;
}

?>