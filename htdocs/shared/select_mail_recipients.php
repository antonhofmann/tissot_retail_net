<?php
/********************************************************************

    select_mail_recipients.php

    Select Mail recipeints for CC Mails

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2010-07-03
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:   2010-07-03
    Version:        1.0.0

    Copyright (c) 2010, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../include/page_modal.php";

//check_access("has_access_to_cer");

/********************************************************************
    prepare all data needed
*********************************************************************/

$names = array();
$email_addresses = array();

$sql = 'Select user_id, user_firstname, user_name, user_email, ' .
       'address_company ' . 
       'from users ' .
	   'left join addresses on address_id = user_address '.
	   'where user_active = 1 ' . 
	   'and user_cer_mailbox_available_for_copy = 1 ' . 
	   'order by address_company, user_name, user_firstname';


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$names[$row['user_id']] = $row['address_company'] . ': ' . $row['user_name'] . ' ' . $row['user_firstname'];
	$email_addresses[$row['user_id']] =  strtolower($row['user_email']);
}

/********************************************************************
    save data
*********************************************************************/
if(param("save_form"))
{
	$ccrecipients = '';
	foreach($email_addresses as $key=>$value) {
		if(array_key_exists($key,$_POST) and $_POST[$key] == 1)
		{
			$ccrecipients .= $value . '\r\n';
		}
	}
}

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new form("cer_mails", "Field cer_mails");
$form->add_hidden("save_form", "1");

$form->add_label("L1", "", "", "Select the users to receive a CC-Mail");

foreach($names as $key=>$name)
{
	if(array_key_exists($key,$_POST) and $_POST[$key] == 1)
	{
		$form->add_checkbox($key, $name, true);
	}
	else
	{
		$form->add_checkbox($key, $name, false);
	}
}

$form->add_input_submit("submit", "Update Recipients", 0);

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$form->populate();
$form->process();

/********************************************************************
    Process buttons
*********************************************************************/ 

/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page_Modal("query_generator");

//require "include/benchmark_page_actions.php";

$page->header();
$page->title("Select CC Recipients");




$form->render();


if(param("save_form"))
{
?>

	<script language="javascript">
		$('#ccmails').val("<?php echo $ccrecipients;?>");
		$.nyroModalRemove();
	</script>

<?php
	
}

?>
<script language="javascript">
	
	function set_checkbox_value()
	{
		var str = '   ' + $('#ccmails').val();
		<?php 
		$script = '';	
		foreach($email_addresses as $key=>$value) {
			
			$script .= "p = str.indexOf('" . $value . "');";
			$script .= "if( p > 0 ) {";
			$script .= "$('#" . $key . "').attr('checked', true);";
			$script .= "}\n";
		}
		echo $script;
		?>
	}

	 set_checkbox_value();

	
</script>
<?php

$page->footer();



