<?php
/********************************************************************

   logistic_functions.php

    Various functions related to shipping data for orders and projects

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2017-03-17
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2017-03-17
    Version:        1.0.0

    Copyright (c) 2017, Swatch AG, All Rights Reserved.

*********************************************************************/

function update_packaging_info_for_items($order_id = 0)
{
	$sql = "select order_item_id, order_item_item from order_items " . 
	$sql .= " where (order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
						  "   and order_item_quantity > 0 " .
						  "   and order_item_type = 1 " . 
						  "   and order_item_order = " . dbquote($order_id);


	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$sql_p = "select * from item_packaging" . 
				 " where item_packaging_item_id = " . dbquote($row["order_item_item"]);

		$res_p = mysql_query($sql_p) or dberror($sql_p);
		$num_of_elements = mysql_num_rows($res_p);
		
		$do_insert = false;
		while ($row_p = mysql_fetch_assoc($res_p))
		{
			$fields = array();
			$values = array();
			
			$sql_o = "select count(order_item_packaging_id) as num_recs " . 
					 " from order_item_packaging " . 
					 " where order_item_packaging_order_item_id = " . $row["order_item_id"];

			$res_o = mysql_query($sql_o) or dberror($sql_o);
			$row_o = mysql_fetch_assoc($res_o);
			
			if($row_o["num_recs"] == 0)
			{
				$do_insert = true;
			}
			elseif($do_insert == false and $row_o["num_recs"] != $num_of_elements)
			{
				$sql_d = "delete from order_item_packaging " . 
						 " where order_item_packaging_order_item_id = " . $row["order_item_id"];
				$result = mysql_query($sql_d) or dberror($sql_d);
				$do_insert = true;
			}

			if($do_insert == true)
			{
				$fields[] = "order_item_packaging_order_item_id";
				$values[] = dbquote($row["order_item_id"]);

				$fields[] = "order_item_packaging_unit_id";
				$values[] = dbquote($row_p["item_packaging_unit_id"]);

				$fields[] = "order_item_packaging_packaging_id";
				$values[] = dbquote($row_p["item_packaging_packaging_id"]);

				$fields[] = "order_item_packaging_number";
				$values[] = dbquote($row_p["item_packaging_number"]);

				$fields[] = "order_item_packaging_length";
				$values[] = dbquote($row_p["item_packaging_length"]);

				$fields[] = "order_item_packaging_width";
				$values[] = dbquote($row_p["item_packaging_width"]);

				$fields[] = "order_item_packaging_height";
				$values[] = dbquote($row_p["item_packaging_height"]);

				$fields[] = "order_item_packaging_weight_net";
				$values[] = dbquote($row_p["item_packaging_weight_net"]);

				$fields[] = "order_item_packaging_weight_gross";
				$values[] = dbquote($row_p["item_packaging_weight_gross"]);

				$fields[] = "date_created";
				$values[] = dbquote(date("Y-m-d H:i:"));

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				 $sql_i = "insert into order_item_packaging (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				 mysql_query($sql_i) or dberror($sql_i);
			}
		}
	}

	return true;
}

?>