<?php
/********************************************************************

    ajx_design_brief_formstaes.php

    Save CSS Display Properties of Forms in file project_design_briefing

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2017-12-19
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2017-12-19
    Version:        1.0.0

    Copyright (c) 2017, OMEGA SA, All Rights Reserved.

*********************************************************************/

session_name("retailnet");
session_start();

$listname = "";
$visibility = "";

if(array_key_exists("formname", $_POST))
{
	$formname = $_POST["formname"];
	$visibility = $_POST["visibility"];
}

$_SESSION["designbrief"][$formname] = $visibility;
echo "success";



?>