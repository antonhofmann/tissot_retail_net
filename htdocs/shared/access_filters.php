<?php
/********************************************************************

   access_filters.php

    Various functions to evaluate a user's access rights on projects

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2014-05-31
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2014-05-31
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.


	Comment: is now used only for the table user_company_responsibles
	can be used later for all access rules

*********************************************************************/

function get_users_regional_access_to_projects($user_id = 0)
{
	$filter = "";
	
	$tmp = array();

	$sql = "select * from user_company_responsibles " . 
		   " where user_company_responsible_user_id = " . $user_id;

	$res = mysql_query($sql) or dberror($sql);
    while($row = mysql_fetch_assoc($res))
	{
		if($row["user_company_responsible_retail"] == 1 and $row["user_company_responsible_wholsale"] == 1)
		{
			$tmp[] = "order_client_address = " . $row["user_company_responsible_address_id"];
		}
		elseif($row["user_company_responsible_retail"] == 1)
		{
			$tmp[] = "(order_client_address = " . $row["user_company_responsible_address_id"] . " and project_cost_type in(1, 3, 4, 5))";
		}
		elseif($row["user_company_responsible_wholsale"] == 1)
		{
			$tmp[] = "(order_client_address = " . $row["user_company_responsible_address_id"] . " and project_cost_type in(2, 6))";
		}
	}

	if(count($tmp) > 0)
	{
		$filter = implode(" or ", $tmp);
	}

	return $filter;
}


function get_users_regional_access_to_countries($type = 0, $user_id = 0, $country_id = 0)
{
	
	if($country_id > 0)
	{
		$filter = array();

		if($country_id > 0)
		{
			$sql = "select posaddress_country from user_company_responsibles " .
				   " left join posaddresses on posaddress_client_id = user_company_responsible_address_id " . 
				   " where user_company_responsible_user_id = " . $user_id . 
				   " and posaddress_country = " . $country_id;
		}
			
		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			$filter[] = $row["posaddress_country"];
		}
		return $filter;
	}
	else
	{
		$filter = "";
		
		$tmp = array();

		$sql = "select DISTINCT user_company_responsible_address_id, " . 
			   " user_company_responsible_retail, user_company_responsible_wholsale, address_country  " . 
			   " from user_company_responsibles " .
			   " left join addresses on address_id = user_company_responsible_address_id " . 
			   " where user_company_responsible_user_id = " . $user_id;

		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			if($type == 1)
			{
				if($row["user_company_responsible_retail"] == 1 and $row["user_company_responsible_wholsale"] == 1)
				{
					$tmp[] = "order_client_address = " . $row["user_company_responsible_address_id"];
				}
				elseif($row["user_company_responsible_retail"] == 1)
				{
					$tmp[] = "(order_client_address = " . $row["user_company_responsible_address_id"] . " and project_cost_type in(1, 3, 4, 5))";;
				}
				elseif($row["user_company_responsible_wholsale"] == 1)
				{
					$tmp[] = "(order_client_address = " . $row["user_company_responsible_address_id"] . " and project_cost_type in(2, 6))";
				}
			}
			else
			{
				$tmp[] = "order_client_address = " . $row["user_company_responsible_address_id"];
			}
		}

		if(count($tmp) > 0)
		{
			$filter = implode(" or ", $tmp);
		}

		return $filter;
	}
}


function get_users_regional_access_to_poslocations($user_id = 0)
{
	$filter = "";
	
	$tmp = array();

	$sql = "select * from user_company_responsibles " . 
		   " where user_company_responsible_user_id = " . $user_id;

	$res = mysql_query($sql) or dberror($sql);
    while($row = mysql_fetch_assoc($res))
	{
		if($row["user_company_responsible_retail"] == 1 and $row["user_company_responsible_wholsale"] == 1)
		{
			$tmp[] = "posaddress_client_id = " . $row["user_company_responsible_address_id"];
		}
		elseif($row["user_company_responsible_retail"] == 1)
		{
			$tmp[] = "(posaddress_client_id = " . $row["user_company_responsible_address_id"] . " and posaddress_ownertype in(1, 3, 4, 5))";
		}
		elseif($row["user_company_responsible_wholsale"] == 1)
		{
			$tmp[] = "(posaddress_client_id = " . $row["user_company_responsible_address_id"] . " and posaddress_ownertype in(2, 6))";
		}
	}

	if(count($tmp) > 0)
	{
		$filter = implode(" or ", $tmp);
	}

	return $filter;
}


function get_users_regional_access_to_companies($user_id = 0, $country_id = 0)
{
	$filter = array();

	if($country_id > 0)
	{
		$sql = "select DISTINCT address_country from user_company_responsibles " .
			   " left join addresses on address_id = user_company_responsible_address_id " . 
			   " where user_company_responsible_user_id = " . $user_id . 
			   " and address_country = " . $country_id;
	}
	else
	{
		$sql = "select DISTINCT address_country from user_company_responsibles " .
			   " left join addresses on address_id = user_company_responsible_address_id " . 
			   " where user_company_responsible_user_id = " . $user_id;
	}
		
	$res = mysql_query($sql) or dberror($sql);
    while($row = mysql_fetch_assoc($res))
	{
		$filter[] = $row["address_country"];
	}
	return $filter;
}


function get_country_access_for_travelling_retail_pos()
{
	$filter = array();
	$sql = "select distinct posaddress_country " .
		   "from posaddresses ".
		   "inner join posareas on posarea_posaddress = posaddress_id " .
		   " where posarea_area IN (4,5) or posaddress_store_subclass IN(17)";

	$res = mysql_query($sql) or dberror($sql);
    while($row = mysql_fetch_assoc($res))
	{
		$filter[] = $row["posaddress_country"];
	}
	return $filter;
}


function get_pos_access_for_travelling_retail_pos($posfilter = '')
{
	$filter = array();

	if($posfilter == '')
	{
		return $filter;
	}

	$sql = "select DISTINCT posaddress_id " .
		   "from posaddresses " .
		   "inner join places on place_id = posaddress_place_id " .
		   "inner join provinces on province_id = place_province " .
		   "inner join countries on posaddress_country = country_id " . 
		   "inner join project_costtypes on project_costtype_id = posaddress_ownertype " . 
		   "inner join postypes on postype_id = posaddress_store_postype " .
		   "inner join posareas on posarea_posaddress = posaddress_id " .
		   " where (posarea_area IN (4,5) or posaddress_store_subclass IN(17)) " . 
		   " and " . $posfilter;

	//echo $sql;cde();

	$res = mysql_query($sql) or dberror($sql);
    while($row = mysql_fetch_assoc($res))
	{
		$filter[] = $row["posaddress_id"];
	}
	return $filter;
}


?>