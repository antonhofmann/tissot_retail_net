<?php
/********************************************************************

    ajx_hide_milestone.php

    hides a milestone in a project

    Created by:     Anton Hofmann (anton.hofmann@codingroom.ch)
    Date created:   2017-04-07
    Modified by:    Anton Hofmann (anton.hofmann@codingroom.ch)
    Date modified:  2017-04-07
    Version:        1.0.0

    Copyright (c) 2017, Swatch AG, All Rights Reserved.

*********************************************************************/

session_name("retailnet");
session_start();

require_once "../include/frame.php";

if(!has_access("can_edit_milestones")) {
	echo 'error';
}
else {
	
	if(array_key_exists('pm', $_POST)) {
	
		$sql = "update project_milestones set project_milestone_visible = 0 ". 
			   "where project_milestone_id = " . dbquote($_POST["pm"]);

		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
	}
	
	echo "success";
}

?>