<?php
/********************************************************************

    ajx_company_access_list_states.php

    Save CSS Display Properties of Lists in user_company_access.php

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-09
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2012, OMEGA SA, All Rights Reserved.

*********************************************************************/

session_name("retailnet");
session_start();

$listname = "";
$visibility = "";

if(array_key_exists("listname", $_POST))
{
	$listname = $_POST["listname"];
	$visibility = $_POST["visibility"];
}

$_SESSION["company_access"][$listname] = $visibility;
echo "success";



?>