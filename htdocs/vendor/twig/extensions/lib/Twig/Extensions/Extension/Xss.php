<?php

/**
 * This file is part of Twig.
 *
 * (c) 2009 Fabien Potencier
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @author Admir Serifi <admir.sherifi@gmail.com>
 */
class Twig_Extensions_Extension_Xss extends Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('xss', array($this, 'xssEscape')),
        );
    }

    public function xssEscape()
    {
        $args = func_get_args();
        $text = array_shift($args);
        $tags = $args ?: array('script', 'object', 'embed', 'applet');
            
        foreach ($tags as $tag) {
            $text = preg_replace("/<".$tag.".*?\/".$tag.">/s", "", $text);
        }

        return $text;
    }

    public function getName()
    {
        return 'Xss';
    }
}