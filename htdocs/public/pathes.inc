<?php

	define('PATH_LIBRARIES', $_SERVER['DOCUMENT_ROOT'].'/libraries/');
	define('PATH_APPLICATIONS', $_SERVER['DOCUMENT_ROOT'].'/applications/');
	define('PATH_CONTROLLERS', $_SERVER['DOCUMENT_ROOT'].'/applications/controllers/');
	define('PATH_MODELS',$_SERVER['DOCUMENT_ROOT'].'/applications/models/');
	define('PATH_VIEWS', $_SERVER['DOCUMENT_ROOT'].'/applications/views/');
	define('PATH_MODULES', $_SERVER['DOCUMENT_ROOT'].'/applications/modules/');
	define('PATH_HELPERS', $_SERVER['DOCUMENT_ROOT'].'/applications/helpers/');
	define('PATH_SETTINGS', $_SERVER['DOCUMENT_ROOT'].'/applications/settings/');
	define('PATH_UTILITIES', $_SERVER['DOCUMENT_ROOT'].'/utilities/');
	define('PATH_THEMES', $_SERVER['DOCUMENT_ROOT'].'/public/themes/');
	define('PATH_DATA', $_SERVER['DOCUMENT_ROOT'].'/public/data/');
	
	define('DIR_CSS', '/public/css/');
	define('DIR_COMPILED', '/public/compiled/');
	define('DIR_IMAGES', '/public/images/');
	define('DIR_SCRIPTS', '/public/scripts/');
	define('DIR_ASSETS', '/public/scripts/');
	define('DIR_THEMES', '/public/themes/');
	define('DIR_JS', '/public/js/'); 
