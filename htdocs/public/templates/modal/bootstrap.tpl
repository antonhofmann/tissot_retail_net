<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header {{classHeader}}">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title">{{title}}</h4>
		</div>
		<div class="modal-body">{{content}}</div>
		<div class="modal-footer {{classFooter}} ">
			{{#buttons}}<button type="button" class="btn btn-{{type}} {{class}}">{{title}}</button>{{/buttons}}
		</div>
	</div>
</div>