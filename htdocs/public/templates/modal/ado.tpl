<div class="ado-modal-header">
	{{lblTitle}}
</div>
<div class="ado-modal-body">
	{{{body}}}
</div>
<div class="ado-modal-footer">
	<div class="ado-row ado-actions">
		<a class="btn btn-default pull-left btn-cancel ado-modal-close">
			<i class="fa fa-ban"></i> {{lblCancel}}
		</a>
		<a class="btn btn-default pull-left btn-save ado-modal-submit">
			<i class="fa fa-check"></i> {{lblSave}}
		</a>
	</div>
</div>