<div class="sk-spinner-container {{theme}} ">
	<div class="sk-spinner-box">
		<div class="sk-spinner sk-spinner-cube-grid">
			<div class="sk-cube"></div>
			<div class="sk-cube"></div>
			<div class="sk-cube"></div>
			<div class="sk-cube"></div>
			<div class="sk-cube"></div>
			<div class="sk-cube"></div>
			<div class="sk-cube"></div>
			<div class="sk-cube"></div>
			<div class="sk-cube"></div>
		</div>
	</div>
</div>