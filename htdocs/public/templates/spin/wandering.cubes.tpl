<div class="sk-spinner-container {{theme}} ">
	<div class="sk-spinner-box">
		<div class="sk-spinner sk-spinner-wandering-cubes">
			<div class="sk-cube1"></div>
			<div class="sk-cube2"></div>
		</div>
	</div>
</div>