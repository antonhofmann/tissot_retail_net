<div class="sk-spinner-container {{theme}} ">
	<div class="sk-spinner-box">
		<div class="sk-spinner sk-spinner-wordpress">
			<span class="sk-inner-circle"></span>
		</div>
	</div>
</div>