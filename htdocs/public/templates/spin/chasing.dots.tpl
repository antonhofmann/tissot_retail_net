<div class="sk-spinner-container {{theme}} ">
	<div class="sk-spinner-box">
		<div class="sk-spinner sk-spinner-chasing-dots">
			<div class="sk-dot1"></div>
			<div class="sk-dot2"></div>
		</div>
	</div>
</div>