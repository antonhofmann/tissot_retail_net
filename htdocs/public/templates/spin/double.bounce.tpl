<div class="sk-spinner-container {{theme}} ">
	<div class="sk-spinner-box">
		<div class="sk-spinner sk-spinner-double-bounce">
			<div class="sk-double-bounce1"></div>
			<div class="sk-double-bounce2"></div>
		</div>
	</div>
</div>