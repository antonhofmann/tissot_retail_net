<span class="drag-handle"/>
<input type="text" name="phase_title" value="{{title}}" class="h3" />
<li class="head hidden">
  <span class="spacer"></span>
  <span class="step_nr">{{stepNr}}</span>
  <span class="step_title">{{stepTitle}}</span>
  <span class="step_type">{{stepType}}</span>
  <span class="step_color">{{stepColor}}</span>
  <span class="step_value">{{stepValue}}</span>
  <span class="step_role">{{stepRole}}</span>
  <span class="step_apply_to">
    {{#projectCostTypes}}<abbr title="{{name}}">{{abbreviation}}</abbr>{{/projectCostTypes}}
    {{#posTypes}}<abbr title="{{name}}">{{abbreviation}}</abbr>{{/posTypes}}
  </span>
  <span class="step_duration">{{stepDuration}}</span>
  <span class="step_active">{{stepActive}}</span>
</li>
<ul class="steps"></ul>
<div class="toolbar"/>
