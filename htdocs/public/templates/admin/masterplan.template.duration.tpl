<p>{{txtNotice}}</p>

<form class="default durations-table">
  {{#regions}}
    <h3 class="caption">
      {{name}}
      <input type="text" placeholder="0" class="duration region-duration-{{id}}">
    </h3>
    <ul class="countries" data-region="{{id}}">
      {{#countries}}
        <li>
          <span>{{name}}</span>
          <input type="text" placeholder="0" class="duration country-duration-{{id}}" data-country="{{id}}">
        </li>
      {{/countries}}
    </ul>
  {{/regions}}
</form>
