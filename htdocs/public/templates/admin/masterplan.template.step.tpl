<span class="drag-handle"></span>
<input type="text" name="step_nr" class="align step_nr" placeholder="{{stepNr}}">
<input type="text" name="title" class="align step_title" placeholder="{{stepTitle}}">
<select name="step_type" class="step_type">
  {{#stepTypes}}
  <option value="{{type}}">{{name}}</option>
  {{/stepTypes}}
</select>
<span class="colorpicker">
  <input type="hidden" class="clr" />
</span>
<select name="value" class="step_value">
  <option value="">--</option>
  {{#values}}
  <option value="{{alias}}">{{name}}</option>
  {{/values}}
</select>
<select name="responsible_role_id" class="step_role">
  <option value="">--</option>
  {{#roles}}
  <option value="{{id}}">{{name}}</option>
  {{/roles}}
</select>
<span class="apply_to">
  {{#projectCostTypes}}
    <input type="checkbox" title="{{name}}" value="{{id}}" name="apply_{{alias}}" checked>
  {{/projectCostTypes}}
  {{#posTypes}}
    <input type="checkbox" title="{{name}}" value="{{id}}" name="apply_{{alias}}" checked>
  {{/posTypes}}
</span>

<span class="duration">
  <span class="icon settings" title="{{stepDuration}}"></span>
</span>

<span class="active">
  <input type="checkbox" value="1" checked name="is_active">
</span>

<span class="icon delete"></span>
