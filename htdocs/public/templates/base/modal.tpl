<div class="ado-modal-header">{{lblTitle}}</div>
<div class="ado-modal-body">
  {{{body}}}
</div>
<div class="ado-modal-footer">
  <div class="ado-row ado-actions">
    <a class="button cancel ado-modal-close">
      <span class="icon cancel"></span>
      <span class="label">{{lblCancel}}</span>
    </a>
    <a class="button save">
      <span class="icon save"></span>
      <span class="label">{{lblSave}}</span>
    </a>
  </div>
</div>
