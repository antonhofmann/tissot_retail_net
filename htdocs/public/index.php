<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/public/header.inc';

Session::init();

// remember last 5 requests
Session::referer();

// request builder
$url = Url::get();
$request = Request::instance($url);

// check user authentification
if (!Session::get('user_id') && ( $request->secure || !$request->isPublicController()) ) {
	Session::set('redirect', $url);	
	Url::redirect('/security/login');
}

// access denied
if (!$request->check()) { 
	Url::redirect("/messages/show/access-denied");
}

// maintenance
$maintenance = Maintenance::getActives();

$user = User::instance();

if ($maintenance && Session::get('user_id')) {
	
	if ($user->address <> 5 && $request->controller <> 'security') {
		url::redirect('/security/maintenance/maintenance');
	}
}

if ($request->controller && $request->action) { 

	$controllerName = $request->controller."_controller";
	$action = $request->action;

	$controller = new $controllerName();
	$params = $request->params ?: array();
	
	// call controller actions	
	call_user_func_array(array($controller, $action), $params);

	// show page
	if ($controller->view) { 
		echo $controller->view->render();
	}
		
	if (!Settings::init()->devmod && Settings::init()->enable_access_statistic) {
		
		$db = Connector::get();
		
		$sth = $db->prepare("
			INSERT INTO statistics (
				statistic_user, 
				statistic_ip, 
				statistic_date, 
				statistic_url, 
				statistic_duration
			) 
			VALUES (?,?,?,?,?) 		
		");


		$sth->execute(array(
			$_SESSION['user_id'], 
			Url::ip(), 
			date('Y-m-d H:i:s'),
			$_SERVER['SCRIPT_FILENAME']."/$url", 
			round(time() - START_TIME))
		);
	}

} else { 
	
	$applications = Application::load(array('application_active = 1'));
	$current = url::application();
	
	if ($applications[$current]) {
		url::redirect('/messages/error/page-not-found');
	} else {
		url::redirect('/page_not_found.php');
	}
}

