<!doctype html>
<!--[if lt IE 7]> <html class="ie6" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en"> <!--<![endif]-->
<head>
	<title><?php echo $this->pagetitle." - ".$this->settings->project_name; ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<link href="/public/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<link href="/public/images/apple-touch-icon.png" rel="apple-touch-icon" type="image/x-icon" />
	
	<script src="/public/scripts/jquery/1.7.2.js" type="text/javascript"></script>
	<script src="/public/scripts/jquery/migrate.min.js" type="text/javascript"></script>
	
	<script src="/public/scripts/master.js" type="text/javascript"></script>
	
	<link rel="stylesheet" type="text/css" href="/public/scripts/fancybox/fancybox.css?v=2.1.5" />
	<script type="text/javascript" src="/public/scripts/fancybox/fancybox.js?v=2.1.5"></script>

	<link href="/public/scripts/adomodal/adomodal.css" rel="stylesheet" type="text/css" />
	<script src="/public/scripts/adomodal/adomodal.js" type="text/javascript"></script> 
	<script type="text/javascript" src="/public/scripts/jquery.actual.min.js"></script>
	<link rel="stylesheet" href="/public/css/spinners.css">
	
	<script type="text/javascript" src="/public/scripts/jgrowl/jgrowl.js"></script>
	<link rel="stylesheet" type="text/css" href="/public/scripts/jgrowl/jgrowl.css" />
	
	<script type="text/javascript" src="/public/scripts/message.js"></script>
	<script type="text/javascript" src="/public/scripts/retailnet.js"></script>

	<link rel="stylesheet" href="/public/scripts/bootstrap/css/bootstrap.compiled.css">
	<link rel="stylesheet" href="/public/css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="/public/themes/default/css/modal.css">
	<script type="text/javascript" src="/public/scripts/message.js"></script>
	<?php 
		echo $this->compiled; 
		$inc = Compiler::export(false);
		echo $inc['css'];
		echo $inc['js'];
	?>

	<script type="text/javascript" src="/public/js/app.js"  ></script>
</head>
<body class="adomat security <?php echo $this->classname; ?>">
	<div class="pagetop-container">
		<div class="pagetop">
			<div class="pagelogo">
				<a href="/mps"><img src="<?php echo $this->settings->path_theme_images; ?>logo.jpg" /></a>
			</div>
			<div class="pagetitle">
				<?php if (url::action() <> "login") echo $this->pagetitle; ?>
			</div>
		</div>
	</div>
	<div class="pagebody-container">
		<div class="pagebody">
			<div class="pageleft">
				<?php echo $this->pageleft(); ?>
			</div>
			<div class="pagecontent">
				<?php echo $this->pagecontent(); ?>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</body>
</html>