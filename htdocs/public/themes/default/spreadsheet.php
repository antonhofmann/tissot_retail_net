<?php 

	$translate = translate::instance();

?>
<!doctype html>
<!--[if lt IE 7]> <html class="ie6" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en"> <!--<![endif]-->
<head>
	<title><?php echo $this->pagetitle." - ".$this->settings->project_name; ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<link href="/public/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<link href="/public/images/apple-touch-icon.png" rel="apple-touch-icon" type="image/x-icon" />
	
	<script src="/public/scripts/jquery/1.7.2.js" type="text/javascript"></script>
	<script src="/public/scripts/jquery/migrate.min.js" type="text/javascript"></script>
	
	<link rel="stylesheet" type="text/css" href="/public/scripts/fancybox/fancybox.css?v=2.1.5" />
	<script type="text/javascript" src="/public/scripts/fancybox/fancybox.js?v=2.1.5"></script>
	
	<link rel="stylesheet" type="text/css" href="/public/scripts/jgrowl/jgrowl.css" />
	<script type="text/javascript" src="/public/scripts/jgrowl/jgrowl.js"  ></script>

	<link href="/public/scripts/adomodal/adomodal.css" rel="stylesheet" type="text/css" />
	<script src="/public/scripts/adomodal/adomodal.js" type="text/javascript"></script> 
	<script type="text/javascript" src="/public/scripts/jquery.actual.min.js"></script>
	<link rel="stylesheet" href="/public/css/spinners.css">
	
	<script type="text/javascript" src="/public/scripts/message.js"></script>
	<script type="text/javascript" src="/public/scripts/retailnet.js"></script>

	<link rel="stylesheet" href="/public/scripts/bootstrap/css/bootstrap.compiled.css">
	<link rel="stylesheet" href="/public/css/font-awesome/css/font-awesome.min.css">
	
	<script type="text/javascript">
		$(document).ready(function() { 

			// ie document mode
			if ($.browser.msie) {
				$('html').removeAttr('class').attr('class','ie ie'+document.documentMode);
			}

			$(window).resize(function() {
				
				var elem = $('#spreadsheet');
				var offset = elem.offset();
				
				var height = $(window).height();
				var scroller = $(document).height() > height ? 24 : 0;
				elem.height(height-offset.top-scroller);

				var width = $(window).width();
				var scroller = $(document).width() > width ? 24 : 0;
				elem.width(width-offset.left-scroller);

			}).trigger('resize');

			$('#close').click(function(event) {
				event.preventDefault();
				window.close();
				return false;
			});

			window.onbeforeunload = function(){
				window.opener.location.reload();
			}
		});
	</script>
	
	<?php 
		echo $this->compiled; 
		$inc = Compiler::export(false);
		echo $inc['css'];
		echo $inc['js'];
	 ?>
	
	<style type="text/css">
		
		body {
			background: #f4f4f4;
			min-width: 400px;
			font-family: arial;
		}
		
		.pagecontent {
			display: block;
			padding: 0 0 0 20px;
			margin: 0;
		}
		
		.pagebody-container {
			margin-top: 20px;
		}
		
		.pagetop .pagetitle {
			padding-left: 20px;
			font-size: 18px;
			overflow: hidden;
			white-space: nowrap;
		}
		
		.actions {
			padding: 0;
			text-align: left !important;
		}
		
		.usermenu .navigation li:first-child {
			border-left: 1px solid silver;
		}
		
		.action-box {
			position: absolute;
			top: 0;
			right: 0;
		}
		
	</style>
</head>
<body class="adomat spreadsheet">
	<div class="pagetop-container">
		<div class="pagetop">			
			<div class="pagetitle">
				<?php echo $this->pagetitle; ?>
			</div>
			<!--  
			<div class='usermenu action-box' >
				<ul class=navigation >
					<li>
						<a id=close >
							<span class='icon icon151' ></span>
							<span class='label' ><?php echo $translate->close; ?></span>		
						</a>
					</li>
				</ul>
			</div>
			-->
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="pagebody-container">
		<div class="pagebody">
			<div class="pagecontent">
				<?php echo $this->pagecontent(); ?>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div id="ajax-loader" class="black"></div>
</body>
</html>