<!doctype html>
<!--[if lt IE 7]> <html class="ie6" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en"> <!--<![endif]-->
<head>
	<title><?php echo $this->pagetitle." - ".$this->settings->project_name; ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="/public/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<link href="/public/images/apple-touch-icon.png" rel="apple-touch-icon" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="<?php echo $this->settings->path_theme_css ?>spreadsheet.css" />
	<script src="/public/scripts/jquery/1.7.2.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="/public/scripts/jgrowl/jgrowl.css" />
	<script type="text/javascript" src="/public/scripts/jgrowl/jgrowl.js"  ></script>
	<?php if (Session::get('messages')) : ?>
	<script type="text/javascript">
		$(document).ready(function() { 
			$.getJSON('/applications/helpers/message.session.php', function(data) {
				if (data) {
					$.each(data, function(key, message) {
						$.jGrowl(message.content, { 
							sticky : message.sticky, 
							life : message.life, 
							theme : message.theme 
						});
					});
				}
			});
		});
	</script>
	<?php  endif; ?>
</head>
<body class="adomat spreadsheet">
	<div class="pagetop-container">
		<div class="pagetop">
			<div class="pagelogo">
				<a href="/mps"><img src="<?php echo $this->settings->path_theme_images; ?>logo.jpg" /></a>
			</div>
			<div class="pagetitle">
				<?php echo $this->pagetitle; ?>
			</div>
			<?php 
				if (isset($this->usermenu)) {
					echo $this->usermenu();
				}
			?>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="pagebody-container">
		<div class="pagebody">
			<div class="pageleft">
				<?php echo $this->pageleft(); ?>
			</div>
			<div class="pagecontent">
				<?php echo $this->pagecontent(); ?>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</body>
</html>