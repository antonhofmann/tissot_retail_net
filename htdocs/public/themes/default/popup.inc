<!doctype html>
<!--[if lt IE 7]> <html class="ie6" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="ie8" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en"> <!--<![endif]-->
<head>
  <title><?php echo $this->pagetitle." - ".$this->settings->project_name; ?></title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  
  <link href="/public/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
  <link href="/public/images/apple-touch-icon.png" rel="apple-touch-icon" type="image/x-icon" />
  
  <?php if ($this->amd === true): ?>
  <script>
    var require = {
      baseUrl: '/public/js'
    };
  </script>
  <script src="/public/scripts/requirejs/require.js"></script>
  <script src="/public/js/main.js"></script>
  <?php endif; ?>

  <!-- jquery 1.10.2 is necessary, do not use lower versions 
  <script src="/public/scripts/jquery/jquery.1.10.2.js"></script>-->
  <?php 
    echo $this->compiled; 
    $inc = Compiler::export(false);
    echo $inc['css'];
    echo $inc['js'];
   ?>
</head>
<body class="adomat popup">
  <div class="pagetop-container">
    <div class="pagetop">     
      <div class="pagetitle">
        <?php echo $this->pagetitle; ?>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
  <div class="pagebody-container">
    <div class="pagebody">
      <div class="pagecontent">
        <?php echo $this->pagecontent(); ?>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</body>
</html>