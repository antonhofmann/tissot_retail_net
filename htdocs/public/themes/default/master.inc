<!doctype html>
<!--[if IE 7]>    <html class="ie ie7" lang="en"> <![endif]-->
<!--[if gt IE 7]>    <html class="ie" lang="en"> <![endif]-->
<!--[if !IE]><!--><html lang="en"> <!--<![endif]--> 
<head>
	<title><?php echo $this->pagetitle." - ".$this->settings->project_name; ?></title>
	<meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<link href="/public/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<link href="/public/images/apple-touch-icon.png" rel="apple-touch-icon" type="image/x-icon" />
	
	<?php if ($this->amd === true): ?>
	<script>
	  var require = {
	    baseUrl: '/public/js'
	  };
	</script>
	<script data-main="main" src="/public/scripts/requirejs/require.js"></script>
	<?php endif; ?>

	<?php if ($this->jquery) : ?>
		<script src="<?php echo $this->jquery ?>" type="text/javascript"></script>
	<?php else : ?>
		<script src="/public/scripts/jquery/1.7.2.js" type="text/javascript"></script>
		<script src="/public/scripts/jquery/migrate.min.js" type="text/javascript"></script>
	<?php endif; ?>
	
	<script src="/public/scripts/master.js" type="text/javascript"></script>
	<script src="/public/scripts/underscore/underscore.min.js" type="text/javascript"></script>
	
	<link rel="stylesheet" type="text/css" href="/public/scripts/fancybox/fancybox.css?v=2.1.5" />
	<script type="text/javascript" src="/public/scripts/fancybox/fancybox.js?v=2.1.5"></script>	

	<link href="/public/scripts/adomodal/adomodal.css" rel="stylesheet" type="text/css" />
	<script src="/public/scripts/adomodal/adomodal.js" type="text/javascript"></script> 
	<script type="text/javascript" src="/public/scripts/jquery.actual.min.js"></script>
	<link rel="stylesheet" href="/public/css/spinners.css">
	
	<script type="text/javascript" src="/public/scripts/jgrowl/jgrowl.js"></script>
	<link rel="stylesheet" type="text/css" href="/public/scripts/jgrowl/jgrowl.css" />
	
	<script type="text/javascript" src="/public/scripts/message.js"></script>
	<script type="text/javascript" src="/public/scripts/retailnet.js"></script>
	<script type="text/javascript" src="/public/scripts/validator.js"></script>

	<link rel="stylesheet" href="/public/scripts/bootstrap/css/bootstrap.compiled.css">
	<link rel="stylesheet" href="/public/css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="/public/themes/default/css/modal.css">

	<?php 
		$inc = Compiler::export(false);
		echo $inc['css'];
		echo $inc['js'];
	?>

	<script type="text/javascript" src="/public/js/app.js"  ></script>
</head>
<body class="adomat">
<div class="page-wrapper">
	<div class="pagetop-container">
		<div class="pagetop">
			<div class="pagelogo">
				<?php 
					$logo_default = $this->settings->path_theme_images.'logo.jpg';
					$logo_application = $this->settings->path_theme_images.'logo_'.url::application().'.jpg';
					$logo = (file_exists($_SERVER['DOCUMENT_ROOT'].$logo_application)) ? $logo_application : $logo_default;
				?>
				<a href="/mps"><img src="<?php echo $logo ?>" /></a>
			</div>
			<div class="pagetitle">
				<?php echo $this->pagetitle; ?>
			</div>
			<?php 
				if (isset($this->usermenu)) {
					echo $this->usermenu();
				}
			?>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="pagebody-container">
		<div class="pagebody">
			<div class="pageleft">
				<?php echo $this->pageleft(); ?>
			</div>
			<div class="pagecontent">
				<?php echo $this->pagecontent(); ?>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<div id="ajax-loader" class="black"></div>
<?php 
	Assistance::show(); 
?>
<script type="text/javascript">
	$(document).ready(function() {

		$("[data-fancybox-type=iframe]").fancybox({
			autoSize	: false,
			fitToView   : true,
		    width       : '800px',
		    height      : '640px',
		    margin 		: 0,
		    padding 	: 0,
			modal 		: true,
	        afterClose	: function() {
	        	if (typeof(modalAfterClose) == "function") { 
	        		modalAfterClose();
	        	}
	        }
		});
		
	});
</script>
</body>
</html>
