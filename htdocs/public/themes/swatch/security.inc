
<!doctype html>
<html lang="en">
<head>
	<title><?php echo $this->pagetitle." - ".$this->settings->project_name; ?></title>
	
	<meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	
	<script src="/public/scripts/modernizr/modernizr.min.js"></script>
	
	<!-- style sheets -->
	<link rel="shortcut icon" type="image/x-icon"  href="/public/images/favicon.ico" />
	<link rel="apple-touch-icon" type="image/x-icon" href="/public/images/apple-touch-icon.png"  />
	<link rel="stylesheet" href="/public/scripts/bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" href="/public/css/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" href="/public/scripts/pnotify/pnotify.custom.min.css" />
	<link rel="stylesheet" href="/public/css/spinners.css" />
  	<link rel="stylesheet" href="/public/scripts/spinner/spinner.css" />
  	<link rel="stylesheet" href="/public/themes/swatch/css/default.css" />
  	<link rel="stylesheet" href="/public/themes/swatch/css/security.css" />
	<?php echo Compiler::get('css') ?>

	<!-- AMD loader -->
	<script> var require = {  baseUrl:'/public/js'} </script>
	<script src="/public/scripts/requirejs/require.js"></script>
	<script src="/public/js/main.js"></script>

</head>

<body class="adomat <?php echo $this->classname ?>">

	<main><?php echo $this->pagecontent(); ?></main>

	<?php echo Compiler::get('js') ?>
</body>
</html>