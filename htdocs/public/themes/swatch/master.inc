
<!doctype html>
<html lang="en">
<head>
	
	<!-- page title -->
	<title><?php echo $this->pagetitle." - ".$this->settings->project_name; ?></title>
	
	<!-- meta data -->
	<meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	
	<!-- page icon -->
	<link href="/public/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<link href="/public/images/apple-touch-icon.png" rel="apple-touch-icon" type="image/x-icon" />
	
	<!-- stylesheets -->
	<link rel="stylesheet" href="/public/scripts/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="/public/css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="/public/scripts/pnotify/pnotify.custom.min.css" />
	<link rel="stylesheet" href="/public/themes/swatch/css/main.css">
	<link rel="stylesheet" href="/public/themes/swatch/css/themes.css">
	<link href="/public/scripts/adomodal/adomodal.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="/public/css/spinners.css">
	<?php echo Compiler::get('css') ?>

	<!-- scripts -->
	<script src="/public/scripts/jquery/jquery.1.10.2.js" ></script>
	<script src="/public/scripts/bootstrap/js/bootstrap.min.js"></script>
	<script src="/public/scripts/jquery.actual.min.js"></script>
	<script src="/public/scripts/tabdrop/bootstrap-tabdrop.js"  ></script>
	<script type="text/javascript" src="/public/scripts/underscore/underscore.min.js"></script>
	<script type="text/javascript" src="/public/scripts/pnotify/pnotify.custom.min.js"></script>
	<script type="text/javascript" src="/public/scripts/datetime/date.js"></script>
	<script type="text/javascript" src="/public/scripts/adomat/ajax.js"></script>
	<script type="text/javascript" src="/public/scripts/adomat/translator.js"></script>
	<script type="text/javascript" src="/public/scripts/adomat/notification.js"></script>
	<script type="text/javascript" src="/public/scripts/adomat/loader.js"></script>
	<script src="/public/scripts/adomodal/adomodal.js"></script> 
	<script type="text/javascript" src="/public/scripts/retailnet.js"></script>
	<script type="text/javascript" src="/public/scripts/adomat/session.message.js"></script>
	<?php echo Compiler::get('js') ?>
	<script src="/public/js/app.js"  ></script>

</head>

<body class="adomat <?php echo $this->pageclass.' '.$this->name ?>">
	
	<!-- nav bar -->
	<nav class="navbar navbar-fixed-top" role="navigation">
		<div class="navbar-main">
			<!-- brand logo -->
			<div class="navbar-header brand-logo">
				<a href="/"><img src="/pictures/brand_logo.jpg" height="36px"></a>
			</div>
			<!-- user menu -->
			<div class="dropdown user-menu pull-right"><?php echo $this->userboxMenu() ?></div>
			<!-- application menu -->
			<div class="dropdown app-menu pull-right">
				<a href="#" class="dropdown-toggle app-menu-toggler" data-toggle="dropdown">
					<i class="fa fa-bars"></i> <strong>Applications</strong> <b class="caret"></b>
				</a>
				<?php echo $this->categories() ?>
			</div>
		</div>
		<!-- application band -->
		<div class="app-band">
			<!-- application title -->
			<div class="app-title">
				<h4><?php echo $this->appName ?: 'Gazette' ?></h4>
				<a href="#" class="sidebar-toggle visible-xs visible-sm" data-toggle="collapse" data-target=".sidebar-left">
					<i class="fa fa-bars"></i>
				</a>
			</div>
			<!-- application pages -->
			<?php echo $this->appTabs() ?>
		</div>
	</nav>
	<?php echo $this->sidebarLeft() ?>

	<!-- page content -->
	<div class="page-main">
		<div class="page-content">
			<?php echo $this->pagecontent(); ?>
		</div>
	</div>

	<div id="ajax-notificator">
		<div class="no-more">No more</div>
		<div class="more"><strong>Loading</strong><span>.</span><span>.</span><span>.</span></div>
	</div>
	<?php Assistance::show(); ?>
</body>
</html>