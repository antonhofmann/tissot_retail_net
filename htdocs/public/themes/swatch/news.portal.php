<!doctype html> 
<html lang="en">
<head>
	<!-- page title -->
	<title><?php echo $this->pagetitle." - ".$this->settings->project_name; ?></title>
	
	<!-- meta data -->
	<meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	
	<!-- page icon -->
	<link href="/public/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<link href="/public/images/apple-touch-icon.png" rel="apple-touch-icon" type="image/x-icon" />
	
	<!-- stylesheets -->
	<link rel="stylesheet" href="/public/scripts/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="/public/css/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="/public/scripts/pnotify/pnotify.custom.min.css" />
	<link rel="stylesheet" href="/public/css/spinners.css">
	<?php if ($this->preview) : ?>
		<link href="/public/scripts/jquery-ui/jquery-ui.min.css" rel="stylesheet"></link>
		<link href="/public/scripts/jquery-ui/jquery-ui.theme.min.css" rel="stylesheet"></link>
	<?php endif; ?>
	<?php echo Compiler::get('css') ?>

</head>
<body class="<?php echo $this->pageclass ?>" <?php echo $this->newsletter['background'] ?> >
	
	<!-- main content -->
	<div class="container main-content">

		<!-- header -->
		<header class="header-top">
			<nav class="navbar navbar-top">
				<div class="navbar-inner">
					<div class="row">	
						<div class="col-xs-12 text-center">
							<a class="brand-logo pull-left" href="<?php echo $this->logoLink ? $this->logoLink : "/gazette"; ?>"></a>
							<?php if (!$this->search && $this->newsletter['number']) : ?><span class="page-title">Gazette <i>No.<?php echo $this->newsletter['number'] ?></i></span><?php endif; ?>
							<span class="navbar-top-actions pull-right">
								<button id="print-newsletter-pdf" class="btn btn-primary btn-xs btn-print">
									<i class="fa fa-file-pdf-o"></i>
									<span class="hidden-xs hidden-sm"> Print PDF</span>
								</button>
								<button id="print-newsletter-doc" class="btn btn-primary btn-xs btn-print">
									<i class="fa fa-file-word-o"></i>
									<span class="hidden-xs hidden-sm"> Print DOC</span>
								</button>
								<button id="sidebar-toggle" class="hidden-md hidden-lg btn-search">
									<i class="fa fa-search" title="Search"></i>
								</button>
							</span>
						</div>
					</div>
				</div>
			</nav>
		</header>
		
		<!-- highlight -->
		<?php if ($this->search) : ?>
			<div class="title-search-results">
				<b id="toalArticles"></b> articles found corresponding to your search criteria
			</div>
		<?php endif; ?>
		<?php if (!$this->search && $this->newsletter['id']) : ?>
			<div class="row">
				<div class="col-xs-12">
					<section>
						<article class="heighlight">
							<div class="heighlight-visual" style="background-image: url(<?php echo $this->newsletter['image'] ?>)">
								<div class="band" >
									<h1 class="pull-left"><?php echo $this->newsletter['title'] ?></h1>
									<span class="date pull-right"><?php echo $this->newsletter['date'] ?></span>
								</div>
							</div>
							<div class="newsletter-content"><?php echo $this->newsletter['content'] ?></div>
							<?php
								if ($this->newsletter['files']) {
									echo "<div class='newsletter-files'>";
									foreach ($this->newsletter['files'] as $row) {
										$title = $row['title'] ?: 'Download PDF';
										$file = $row['file'];
										echo "<a href='$file' target='_blank' class='blocklink'>$title</a>";
									}
									echo "</div>";
								}
							?>
						</article>
					</section>
				</div>
			</div>
		<?php endif; ?>
		
		<!-- newsletters -->
		<div class="row">	
			<!-- newsletter articles -->
			<div class="newsletter col-xs-12 col-sm-12 col-md-8 col-lg-8">
				<div id="articles"></div>
			</div>
			<!-- newsletter sidebar -->
			<div class="sidebar sidebar-right col-xs-12 col-sm-12 col-md-4 col-lg-4 hidden-xs hidden-sm">
				<div id="sidebar" >
					<!-- print basket -->	
					<div class="btn-group btn-block print-basket">
						<button id="print-article-pdf" type="button" class="btn btn-default col-xs-2">
							<i class="fa fa-file-pdf-o"></i> <small>PDF</small>
						</button>
						<button id="print-article-doc" type="button" class="btn btn-default col-xs-2">
							<i class="fa fa-file-word-o"></i> <small>DOC</small>
						</button>
						<button type="button" class="btn btn-default col-xs-7" disabled="disabled">
							<i class="fa fa-print"></i> Print <b id="print-basket-total"></b> articles
						</button>
						<button type="button" class="btn btn-default col-xs-1 dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu btn-block print-items" role="menu"></ul>
						<div class="clearfix"></div>
					</div>
					<!-- form filters -->
					<form id="filters" action="<?php echo $this->searchSubmitAction; ?>" method="post" class="dropdown-box">
						<input type="hidden" name="newsletter" id="newsletter" value="<?php echo $this->newsletter['id'] ?>" />
						<input type="hidden" name="application" id="application" value="news" />
						<input type="hidden" name="controller" id="preview" value="<?php echo $this->controller ?>" />
						<input type="hidden" name="preview" id="preview" value="<?php echo $this->preview ?>" />
						<input type="hidden" name="article" id="article" value="<?php echo $this->article ?>" />
						<?php if (!$this->article) : ?>
						<div class="form-group">
							<div class="icon-addon addon-md dropdown">
								<input name="search" type="text" placeholder="Search" class="form-control full-search input-lg" id="search" value="<?php echo $this->filters['search'] ?>">
								<label for="search" class="glyphicon glyphicon-search" rel="tooltip" title="Search"></label>
								<!--<a class="toggler" href="#" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="true" ><span class="caret"></span></a>-->
								<a class="toggler" href="#" role="button"><span class="caret"></span></a>
								<div class="dropdown-menu dropdown-menu-right">
									<select name="section" id="section" class="form-control filter" value="<?php echo $this->filters['section'] ?>"><option value="">Select Section</option></select>
									<select name="category" id="category" class="form-control filter" value="<?php echo $this->filters['category'] ?>"><option value="">Select Category</option></select>
									<select name="year" id="year" class="form-control filter" value="<?php echo $this->filters['year'] ?>"><option value="">Select Year</option></select>
									<select name="month" id="month" class="form-control filter" value="<?php echo $this->filters['month'] ?>"><option value="">Select Month</option></select>
									<button type="submit" class="btn btn-primary pull-right submit-search-form"><i class="fa fa-search"></i> Search</button>
								</div>
							</div>
						</div>	
						<?php endif; ?>
						<div class="clearfix"></div>
					</form>
					<!-- featured news -->
					<section id="featured" class="featured-news hidden-xs hidden-sm"></section>
					<!-- section categories -->
					<?php if ($this->categoriesMenu) : ?>
					<section class="categories">
						<article>
							<ul class="nav nav-list">
								<?php 
									
									foreach ($this->categoriesMenu as $section => $row) {
										
										echo "<li>";
										echo "<label class='tree-toggler nav-header'>{$row[name]}</label>";
										echo "<ul class='nav nav-list tree'>";
										foreach ($row['categories'] as $category => $val) {
											$active = $this->filters['category']==$category ? 'active' : null;
											$link = $this->preview ? "/news/search/preview/$this->preview/" : "/news/search/";
											echo "<li><a href='#$category' class='$active' data-id='$category' >{$val[name]} ({$val[total]})</a></li>";
										}
										echo "</ul>";
										echo "</li>";
									}
								?>
					        </ul>
						</article>
						<div class="clearfix"></div>
					</section>
					<?php endif; ?>
					<!-- section archive -->
					<?php if ($this->archive) : ?>
					<section class="archive">
						<h5 class="section-title">Archive</h5>
						<div class="btn-group btn-group-justified">
							<button type="button" style="width:100%;" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
								Select Gazette <span class="caret"></span>
							</button>
							<ul class="dropdown-menu" role="menu">
								<?php
									
									$current = $this->newsletter['id'];

									// archived
									if ($this->newsletter['state']==4) {
										echo "<li><a href=\"/gazette\">Current Gazette</a></li>";
										echo "<li role=\"separator\" class=\"divider\"></li>";
									}

									foreach ($this->archive as $id => $name) {
										$active = $id==$current ? 'active' : null;
										echo "<li class=\"btn-group-justified $active\"><a class=\"$active\" href=\"/gazette/$id\" >$name</a></li>";
									}
								?>
							</ul>
						</div>
						<div class="clearfix"></div>
					</section>
					<?php endif; ?>
					<!-- section categories -->
					<?php if ($this->stickers) : ?>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headerStickers">
								<h4 class="panel-title">
									<a class="collapsed" data-toggle="collapse" href="#bodyStickers" aria-expanded="true" aria-controls="bodyStickers">Stickers</a>
								</h4>
							</div>
							<div id="bodyStickers" class="panel-collapse in" role="tabpanel" aria-labelledby="headerStickers">
								<div class="panel-body" id="stickers">
									<div class="stickers height-700">
									<?php 
										foreach ($this->stickers as $row) {
											echo "<div class='item'>";
											echo "<span data-id='{$row[id]}' class='sticker sticker-{$row[id]}' style='background-image: url({$row[image]})'>";
											echo "<span class='selected'><i class='fa fa-check-circle-o'></i></span>";
											if ($row['title']) echo "<p>{$row[title]}</p>";
											echo "</span>";
											echo "</div>";
										}
									?>
									</div>
								</div>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>

		<!-- swatch quick links -->
		<?php if ($this->quicklinks) : ?>
		<nav class="quick-links">
			<h3 class="show-for-medium-up">Quick Links</h3>
			<div class="row">
				<?php 
					$col = ceil(12/count($this->quicklinks));
					foreach ($this->quicklinks as $i => $quicklinks) {
						echo "<div class=\"col-xs-12 col-sm-$col\">";
						echo "<ul class=\"show-for-medium-up\">";
						foreach ($quicklinks as $row) {
							echo "<li><a href=\"{$row[link]}\" target=\"_blank\">{$row[title]}</a></li>";
						}
						echo "</ul>";
						echo "</div>";
					}
				?>
			</div>
			<br /><br />
			<h6>DON'T FORGET THAT I AM AN INTERNAL TOOL WHICH MUST NOT BE SHARED OUTSIDE OF SWATCH OR LEFT BEHIND!</h6>
		</nav>
		<?php endif; ?>

	</div>
	
	<!-- scripts -->
	<script src="/public/scripts/jquery/jquery.1.10.2.js" ></script>
	<script src="/public/scripts/bootstrap/js/bootstrap.min.js"></script>
	<script src="/public/scripts/adomat/notification.js"></script>
	<script src="/public/scripts/adomat/loader.js"></script>
	<script src="/public/js/app.js"  ></script>
	<?php if ($this->preview) : ?>
		<script src="/public/scripts/jquery-ui/jquery-ui.min.js"></script>
	<?php endif; ?>
	<?php echo Compiler::get('js') ?>
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
	<!--[if lt IE 9]>
		<script src="/public/scripts/html/html5shiv.js"></script>
	<![endif]-->
</body>
</html>