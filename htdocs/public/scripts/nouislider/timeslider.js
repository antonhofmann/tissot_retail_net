/**
 * Time Slider Extension
 * Based on noUiSlider
 * https://github.com/leongersen/noUiSlider
 *    	
 * @version 1.0
 * @author Admir Serifi
 * Copyright (c) 2015, MediaParx AG, All Rights Reserved
 */
function TimeSlider(e) {

	var self = $(e),
		targetField = $(self.data('sliderTarget')) || $("<div />"),
		targetIndex = $(e+'-index');

	var STEP = 5;

	/**
	 * Step formater
	 * @param  int a range value
	 * @return {[type]}   [description]
	 */
	var getStepValue = wNumb({
		edit: function( a ) {
			if (a==0)
				return 0;
			else if (a < 60000)
				return a / (1000 * STEP); // seconds
			else if (a >= 60000 && a < 3600000)
				return (a / (1000 * 60)) + 11; // minutes, add 11 steps to jump over the seconds (11 steps: 5s, 10s...55s)
			else if (a >= 3600000 && a < 86400000)
				return (a / (1000 * 60 *  60)) + 70;  // hours, add 70 steps to jump over the seconds + minutes
			else if (a >= 86400000)
				return (a / (1000 * 60 *  60 * 24)) + 93;  // days, sdd 70 steps to jump over the seconds + minutes
		}
	}); 

	/**
	 * Set pip range
	 * @param  int minValue min range vale
	 * @return array
	 */
	var getTimeRange = function(minValue) {

		/**
		 * Range data:
		 * 1-11: Seconds in steps of 5
		 * 12-70: Minutes
		 * 71-94: Hours
		 * 
		 * @return array
		 */
		switch(minValue) {
			case 1:  return [1, 6, 12, 16, 26, 41, 56, 71, 94];
			case 14: return [26, 41, 56, 71, 94, 100];
			case 12:
			default: return [26, 41, 56, 71, 94];
		}
	}

	/**
	 * Get timestamp from rangevalue
	 * @param  int a range value
	 * @return int
	 */
	var getTimeStamp = wNumb({
		edit: function( a ){
			if (a == 1)
				return 0;
			else if (a < 12) 
				return 1000 * STEP*a; // seconds
			else if (a >= 12 && a < 71) 
				return 1000 * 60 * (a-11); // minutes
			else if (a >= 71 && a < 94)
				return 1000 * 60 *  60 * (a-70); // hours
			else if (a >= 94)
				return 1000 * 60 *  60 * 24 * (a-93); // days
		}
	})

	/**
	 * Set filter range for step mode
	 * @param  int range value
	 * @param  int type  mode type (1)
	 * @return int
	 */
	var filterRange = function(value, type) { 
		//return value % 1000 ? 2 : 1;
	}

	var getTimeFormat = wNumb({
		decimals: 0,
		edit: function( a ){
			if (a==1)
				return 0;
			if ( a < 12 )
				return STEP*a + ' sec';
			if ( a >= 12 && a < 71 )
				return (a-11) + ' min';
			if ( a >= 71 && a < 94 )
				return a-70 + ' h';
			if ( a >= 94 )
				return a-93 + ' d';
		}
	})

	var rangeMinValue = parseInt(targetField.attr('min')),
		rangeMaxValue = parseInt(targetField.attr('max')),
		currentValue  = parseInt(targetField.val());

	var startValue = parseInt(getStepValue.to(currentValue)) || 0,
		minValue   = parseInt(getStepValue.to(rangeMinValue)) || 1,
		maxValue   = parseInt(getStepValue.to(rangeMaxValue)) || 94;

	/**
	 * NoUiSlider instance
	 * @type function
	 */
	noUiSlider.create(self.get(0), {
		start: parseInt(startValue),
		step: 1,
		range: {
			'min': minValue,
			'max': maxValue
		},
		pips: {
			mode: 'values',
			values: getTimeRange(minValue), 
			density: 30,
			stepped: true,
			format: getTimeFormat
		}
	});

	/**
	 * Event listener on update
	 * @param  array values all slider values
	 * @param  int handle current changed
	 * @return void
	 */
	self.get(0).noUiSlider.on('update', function( values, handle ) { 
	
		var value = parseInt(values[handle]);
		
		// set time value
		targetField.val(getTimeStamp.to(value));

		// set time index
		targetIndex.text(getTimeFormat.to(value));
	})
}