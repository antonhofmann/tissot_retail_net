/*
 * @version 0.9.3
 * @author Claudio Felber
 * Copyright (c) 2011-2013, Perron2 GmbH, All Rights Reserved
 */

Spreadsheet.options = {
    cssKey: 'spreadsheet', // The prefix used for CSS classes applied to rows and cells of the spreadsheet tables
    readonly: false, // Whether the whole spreadsheet is read-only
    minColumns: 1, // Minimum number of spreadsheet columns
    minRows: 1, // Minimum number of spreadsheet rows
    autoHeight: false, // Automatically determine the height of the spreadsheet container
    fixLeft: 0, // Number of fixed columns to the left of the spreadsheet
    fixTop: 0, // Number of fixed rows at the top of the spreadsheet
    fixedPartsNotSelectable: false, // Whether cells within the fixed area can be selected
    numericFunctions: true, // Whether content functions of individual cells are going to get numeric values instead of raw ones
    hiddenAsNull: false, // Whether a function reads hidden cells as null or 0 (numeric functions)
    undoDepth: 10, // Number of modifications that can be undone by the user
    merge: {}, // Dictionary naming merged cells and their colspan
    onModify: null, // Handler that is called whenever one or multiple cells have been modified
    selectionHandler: null
}

/**
 * Converts the specified HTML element into an Excel-like spreadsheet.
 * @constructor
 * @param {jQuery} $spreadsheet Element ID or jQuery element object
 * @param {object} initialData Dictionary of cell descriptions (@see setData for further details)
 * @param {object} [options] Dictionary of option flags controlling the behavior of the spreadsheet
 *     (@see Spreadsheet.options)
 */
function Spreadsheet($spreadsheet, initialData, options) { 

    /*===== Public API ==========================================================================*/

    /**
     * Removes all cell values within the specified range.
     * @param {string} start The starting cell of the range to be cleared
     * @param {string} end The ending cell of the range to be cleared
     * @param {boolean} [clearClass=false] Whether to remove the CSS class as well
     */
    this.clearValues = function(start, end, clearClass) {
        var range = normalizeRange(start, end);
        if (range) {
            start = getCoordsFromCellName(range.start);
            end = getCoordsFromCellName(range.end);
            var hasChanges;
            var functionCells = {};

            for (var row = start.row; row <= end.row; row++) {
                for (var column = start.column; column <= end.column; column++) {
                    var cellName = getCellNameFromCoords(column, row);
                    var cell = data[cellName];
                    if (updateCellValue(cell, null, clearClass ? null : undefined)) {
                        hasChanges = true;
                        enumerateDependencies(cell, functionCells);
                    }
                }
            }

            if (hasChanges) {
                updateFunctionCells(functionCells);
                setLayout();
            }
        }
    }

    /**
     * Sets the keyboard input focus to the spreadsheet component. The spreadsheet component
     * does only react to keyboard events if it currently has the input focus.
     */
    this.focus = function() {
        $focus.focus();
    }

    /**
     * Returns the value of the specified cell.
     * @param {string} cellName The name of the cell
     * @param {object} options How to read the cell
     * @param {boolean} options.numeric Convert the value to a number
     * @param {boolean} options.hiddenAsNull Return hidden cells as null or 0
     * @return {string|number} The value of the specified cell
     */
    this.getValue = function(cellName, options) {
        options = options || {};
        if (options.hiddenAsNull && isCellHidden(cellName)) {
            return options.numeric ? 0 : null;
        } else {
            var cell = data[cellName];
            var value = cell.value;
            if (cell.html) {
                value = cell.cell.text();
                if (isEmpty(value)) {
                    value = cell.cell.find('img').attr('alt');
                }
            }
            if (options.numeric) {
                value = parseFloat(value);
                return isNaN(value) ? 0.0 : value;
            } else {
                return value;
            }
        }
    }
    
    /**
     * Returns the data of the specified cell.
     * @param {string} cellName The name of the cell
     */
    this.getData = function(cellName) {
    	return data[cellName] || {};
    }

    /**
     * Returns a range of values and associated class names as a dictionary. The dictionary keys
     * are equal to the cell names being referred to. The values are a dictionaries by themselves
     * containing two attributes: value and classValue.
     * @param {string} start The starting cell of the range to be returned
     * @param {string} end The ending cell of the range to be returned
     * @param {object} options How to read the cells
     * @param {boolean} options.numeric Convert the value to a number
     * @param {boolean} options.hiddenAsNull Return hidden cells as null or 0
     * @return {object} A dictionary having the cell name as key and a dictionary with attributes text,
     *     cls and tag as value
     */
    this.getValues = function(start, end, options) {
        options = options || {};
        var range = normalizeRange(start, end, true);
        if (range) {
            start = getCoordsFromCellName(range.start);
            end = getCoordsFromCellName(range.end);

            var values = {};
            for (var column = start.column; column <= end.column; column++) {
                var columnName = columnDefinitions[column].name;
                for (var row = start.row; row <= end.row; row++) {
                    var cellName = columnName + (row + 1);
                    var cell = {
                        text: self.getValue(cellName, options),
                        cls: data[cellName].classValue,
                        tag: data[cellName].tag
                    };
                    values[cellName] = cell;
                }
            }

            return values;
        }
    }

    /**
     * Returns a range of values as an array (or nested arrays if the range is two-dimensional).
     * @param {string} start The starting cell of the range to be returned
     * @param {string} end The ending cell of the range to be returned
     * @param {object} options How to read the cells
     * @param {boolean} [options.numeric=false] Convert the value to a number
     * @param {boolean} [options.hiddenAsNull=false] Return hidden cells as null or 0
     * @param {boolean} [options.compact=false] Return linear selections as a one-dimensional array
     * @return {string[]|number[]} An array (of arrays range is two-dimensional) containing the cell values
     */
    this.getValuesAsArray = function(start, end, options) {
        options = options || {};
        var range = normalizeRange(start, end, true);
        if (range) {
            start = getCoordsFromCellName(range.start);
            end = getCoordsFromCellName(range.end);

            var values = [];
            if (start.column == end.column && options.compact) {
                // Vertical slice
                var columnName = columnDefinitions[start.column].name;
                for (var i = start.row; i <= end.row; i++) {
                    values.push(self.getValue(columnName + (i + 1), options));
                }
            } else if (start.row == end.row && options.compact) {
                // Horizontal slice
                var rowNumber = start.row + 1;
                for (var i = start.column; i <= end.column; i++) {
                    values.push(self.getValue(columnDefinitions[i].name + rowNumber, options));
                }
            } else {
                // two-dimensional block
                var rows = [];
                for (var row = start.row; row <= end.row; row++) {
                    var columns = [];
                    for (var column = start.column; column <= end.column; column++) {
                        columns.push(self.getValue(columnDefinitions[column].name + (row + 1), options));
                    }
                    values.push(columns);
                }
            }

            return values;
        }
    }

    /**
     * Returns the values contained within the specified range as a string. Values on the same
     * line are separated by tab characters. Lines are separated by a newline character.
     * @param {string} start The starting cell of the range to be returned
     * @param {string} end The ending cell of the range to be returned
     * @param {object} options How to read the cells
     * @param {boolean} [options.numeric=false] Convert the value to a number
     * @param {boolean} [options.hiddenAsNull=false] Return hidden cells as null or 0
     * @return {string} The concatenated cell values
     */
    this.getValuesAsString = function(start, end, options) {
        options = options || {};
        var range = normalizeRange(start, end, true);
        if (range) {
            start = getCoordsFromCellName(range.start);
            end = getCoordsFromCellName(range.end);
            var lines = [];
            for (var row = start.row; row <= end.row; row++) {
                var values = [];
                for (var column = start.column; column <= end.column; column++) {
                    var cellName = getCellNameFromCoords(column, row);
                    var value = this.getValue(cellName, options);
                    values.push(isEmpty(value) ? '' : value);
                }
                lines.push(values.join('\t'));
            }
            return lines.join('\n');
        }
    }

    /**
     * Hides the specified columns.
     * @param {string|string[]} columns An array holding the column names to be hidden
     *      or a variable number of column names (vararg parameter)
     */
    this.hideColumns = function(columns) {
        columns = flattenArray(arguments);

        var hasChanges;
        var functionCells = {};
        var length = columns.length;

        for (var i = 0; i < length; i++) {
            var name = columns[i];
            var column = getColumnFromCellName(name);
            var def = columnDefinitions[column];
            if (def.visible) {
                def.visible = false;
                $('.' + options.cssKey + '-' + name, $sections).hide();
                hasChanges = true;
                for (var r = 1; r <= options.dimensions.rows; r++) {
                    var cellName = name + r;
                    enumerateDependencies(data[cellName], functionCells);
                }
            }
        }

        if (hasChanges) {
            ensureHasSelection();
            setLayout();
            updateFunctionCells(functionCells);
        }
    }

    /**
     * Hides the specified rows.
     * @param {string|string[]} rows An array holding the row numbers to be hidden (starting with 1)
     *      or a variable number of row numbers (vararg parameter)
     */
    this.hideRows = function(rows) {
        rows = flattenArray(arguments);

        var hasChanges;
        var functionCells = {};
        var length = rows.length;

        for (var i = 0; i < length; i++) {
            var number = rows[i];
            var def = rowDefinitions[number - 1];
            if (def.visible) {
                def.visible = false;
                $('tr.' + options.cssKey + '-' + number, $sections).hide();
                hasChanges = true;
                for (var c = 0; c < options.dimensions.columns; c++) {
                    var cellName = columnDefinitions[c].name + number;
                    enumerateDependencies(data[cellName], functionCells);
                }
            }
        }

        if (hasChanges) {
            ensureHasSelection();
            setLayout();
            updateFunctionCells(functionCells);
            return true;
        }
    }

    /**
     * Assigns data to the spreadsheet, optionally replacing all current content.
     * The data is specified as a dictionary with the keys being the names of the 
     * individual cells (such as 'a3' or 'g57'). The alphabetic column part ranges from
     * 'a' to 'zz'. The value of each cell description is either a single value to be
     * displayed within the cell or a dictionary if a more verbose description is required.
     * So <code>a1: 'Hello'</code> is a shorthand notation for <code>a1: { text: 'Hello' }</code>.
     * Available attributes:
     * <ul>
     * <li>text: Simple text value to be displayed in the cell or a function that calculates
     *     the cell value based on other cells. The argument names of the function correspond
     *     to the cell names being referenced.
     * <li>html: A piece of HTML to be placed in the cell or a function (analogous to 'text') that
     *     generates the HTML based on other cell values.
     * <li>readonly: Determines whether the cell content can be changed by the user.
     * <li>cls: A CSS class to be assigned to the cell's <td> element
     * <li>numeric: Whether a function expects numeric input values (default specified in options.numericFunctions)
     * <li>hiddenAsNull: Whether a function reads hidden cells as null or 0 (default specified in options.hiddenAsNull)
     * <li>tag: A piece of custom user data
     * </ul>
     * @param {object} newData Dictionary of cell descriptions
     * @param {boolean} [replaceAll=false] Replace all data currently contained in the spreadsheet
     * @example
     * var data = {
     *     a1: 10,
     *     b1: { text: 20, cls: 'important' },
     *     c1: function(a1, b1) { return a1 + b1; }
     * }
     * spreadsheet.setData(data);
     */
    this.setData = function(newData, replaceAll) {
        if (replaceAll) {
            data = {};
        }

        // Copy specified data and add additional required attributes, furthermore
        // determine required dimensions for new data
        var hasFunctions = false;
        var columns = options.minColumns;
        var rows = options.minRows;
        for (var key in newData) {
            var newCell = newData[key];

            // Convert simplified notation to object notation
            if (newCell == null || typeof(newCell) != 'object') {
                newCell = { text: newCell };
            }

            // Access the internal cell object and create it if it does not yet exist
            var cell = data[key];
            if (!cell) {
                data[key] = cell = {};
            } else if (cell.func) {
                var sourceLength = cell.source.length;
                for (var i = 0; i < sourceLength; i++) {
                    var source = cell.source[i];
                    delete data[source].dep[key];
                }
                delete cell.source;
                delete cell.func;
            }

            // Determine the cell's numeric coordinates
            if (!cell.column) {
                var coords = getCoordsFromCellName(key);
                cell.name = key;
                cell.column = coords.column;
                cell.row = coords.row;
            }

            // Update dimensions
            columns = Math.max(cell.column + 1, columns);
            rows = Math.max(cell.row + 1, rows);

            // Assign class property
            var cls = newCell.cls;
            if (typeof(cls) == 'function') {
                cell.classValue = null;
                cell.classFunc = cls;
            } else {
                cell.classValue = cls;
                cell.classFunc = null;
            }

            // Assign remaining properties
            cell.readonly = newCell.readonly;
            cell.numeric = newCell.hasOwnProperty('numeric') ? newCell.numeric : options.numericFunctions;
            cell.hiddenAsNull = newCell.hasOwnProperty('hiddenAsNull') ? newCell.hiddenAsNull : options.hiddenAsNull;
            cell.tag = newCell.tag;

            // Read and analyze text and html attributes
            if (newCell.hasOwnProperty('text')) {
                var text = newCell.text;
                cell.html = false;
                if (typeof(text) == 'function') {
                    prepareFunctionCell(cell, text);
                    hasFunctions = true;
                } else {
                    cell.value = text;
                }
            } else if (newCell.hasOwnProperty('html')) {
                var html = newCell.html;
                cell.html = true;
                if (typeof(html) == 'function') {
                    prepareFunctionCell(cell, html);
                    hasFunctions = true;
                } else {
                    cell.value = html;
                }
            }

            // Update dynamic CSS class
            if (cell.classFunc && !cell.func) {
                if (cell.numeric) {
                    value = parseFloat(cell.value);
                    value = isNaN(value) ? 0.0 : value;
                }
                cell.classValue = cell.classFunc(value);
            }
        }

        // Determine whether the tables have to be rebuilt
        var rebuild = replaceAll
            || !options.dimensions
            || columns > options.dimensions.columns
            || rows > options.dimensions.rows;

        // Store the spreadsheet dimensions
        options.dimensions = {
            columns: replaceAll ? columns : Math.max(columns, options.dimensions.columns),
            rows: replaceAll ? rows : Math.max(rows, options.dimensions.rows)
        };

        // Sort function cells based on their required order of execution
        if (hasFunctions) {
            sortFunctionCells();
        }

        // Calculate function values
        for (var key in functionCellSequence) {
            calculateFunctionCellValue(functionCellSequence[key]);
        }

        // Apply new data
        if (rebuild) {
            $sections.detach();
            buildTables(options.dimensions);

            // Complement missing cell descriptions in data dictionary and assign the corresponding table
            // cell element to the cell description
            var index = options.id.length + 1;
            $('td', $sections).each(function() {
                var $cell = $(this);
                var cellName = this.id.substr(index);
                var cell = data[cellName];
                if (!cell) {
                    var dim = getCoordsFromCellName(cellName);
                    cell = data[cellName] = {
                        name: cellName,
                        column: dim.column,
                        row: dim.row
                    };
                }
                cell.cell = $cell;
                cell.name = cellName;
            });

            // Complement more missing cell descriptions due to merged cells
            var merge = options.merge
            for (var key in merge) {
                var span = merge[key];
                data[key].span = span;
                var coords = getCoordsFromCellName(key);
                var endColumn = coords.column + span;
                for (var column = coords.column + 1; column < endColumn; column++) {
                    var cellName = getCellNameFromCoords(column, coords.row);
                    data[cellName] = {
                        name: cellName,
                        column: column,
                        row: coords.row,
                        master: key
                    };
                }
            }

            // (Re)attach the section tables to the DOM and adjust the layout
            $container.append($sections);
            setLayout();
        } else {
            // If the dimensions have not changed then just update the modified cells (new value cells
            // and all function cells)
            $sections.detach();
            for (var key in newData) {
                var cell = data[key];
                updateCellValue(cell, cell.value, cell.className, true);
            }
            updateFunctionCells();

            // Reattach the section tables to the DOM and adjust the layout
            $container.append($sections);
            setLayout();
        }
    }

    /**
     * Sets the contents of a single cell to the specified value. The value parameter is either a single
     * text value representing the cell content or a dictionary with the attributes text, html, tag and cls
     * according to the method setData(). Missing attributes are ignored, meaning that the according attribute
     * is not altered. If replace is true, missing attributes are considered null. Cells with a content function
     * cannot be altered using this method. Furthermore the CSS class value is ignored if the cell definition
     * features a class function.
     * @param {string} cellName The name of the cell to be modified
     * @param {string|numeric|object} value The value to be assigned to the cell or a dictionary with the attributes
     *    text, html, tag and cls
     * @param {boolean} [replace=false] Whether missing dictionary values such as tag or cls are considered null
     */
    this.setValue = function(cellName, value, replace) {
        var cell = data[cellName];
        if (cell) {
            var params = {};
            params[cellName] = value;
            self.setValues(params, replace);
        }
    }

    /**
     * Displays the specified values. The parameter values is a dictionary associating a cell name with a value.
     * The value is either a single text value representing the cell content or a dictionary with the attributes
     * text, html, tag and cls according to the method setData(). Missing attributes are ignored, meaning that
     * the according attribute is not altered. If replace is true, missing attributes are considered null.
     * Cells with a content function cannot be altered using this method. Furthermore the CSS class value is ignored
     * if the cell definition features a class function.
     * @param values A dictionary with cell names as keys and dictionaries as values.
     *     The allowed dictionary attributes are text, html, tag and cls as defined in (@see setData).
     * @param [replace=false] Whether missing dictionary values such as tag or cls are considered null
     */
    this.setValues = function(values, replace) {
        var hasChanges;
        var functionCells = {};
        for (var cellName in values) {
            var valueCell = values[cellName];
            if (typeof(valueCell) != 'object') {
                valueCell = { text: valueCell };
            }

            var cell = data[cellName];
            if (cell) {
                if (replace || valueCell.hasOwnProperty('tag')) {
                    cell.tag = valueCell.tag;
                }

                var cls;
                if (replace || valueCell.hasOwnProperty('cls')) {
                    cls = valueCell.cls;
                }

                var value;
                if (replace || valueCell.hasOwnProperty('text') || valueCell.hasOwnProperty('html')) {
                    if (valueCell.html) {
                        value = valueCell.html;
                    } else if (valueCell.text) {
                        value = valueCell.text;
                    } else {
                        value = null;
                    }
                }
                
                if (updateCellValue(cell, value, cls)) {
                    hasChanges = true;
                    enumerateDependencies(cell, functionCells);
                }
            }
        }

        if (hasChanges) {
            updateFunctionCells(functionCells);
            setLayout();
        }
    }

    /**
     * Sets the content of the of the cells starting at the specified cell to the passed values.
     * The values are given as as an array of arrays (lines containing column values).
     * @param startCell Origin of the region where the specified values will be put
     * @param values The values to be placed in the spreadsheet (array of arrays)
     */
    this.setValuesFromArray = function(startCell, values) {
        var startCellCoords = getCoordsFromCellName(startCell);
        var maxColumn = options.dimensions.columns;
        var maxRow = options.dimensions.rows;
        var hasChanges;
        var functionCells = {};

        if (startCellCoords && startCellCoords.column < maxColumn && startCellCoords.row < maxRow) {
            var rowsLength = values.length;
            for (var row = 0; row < rowsLength; row++) {
                if (row >= maxRow) {
                    break;
                }

                var rowArray = values[row];
                if (!(rowArray instanceof Array)) {
                    rowArray = [ rowArray ];
                }

                var columnsLength = rowArray.length;
                for (var column = 0; column < columnsLength; column++) {
                    if (column >= maxColumn) {
                        break;
                    }

                    var cellName = getCellNameFromCoords(startCellCoords.column + column, startCellCoords.row + row);
                    var cell = data[cellName];

                    if (updateCellValue(cell, rowArray[column])) {
                        hasChanges = true;
                        enumerateDependencies(cell, functionCells);
                    }
                }
            }

            if (hasChanges) {
                updateFunctionCells(functionCells);
                setLayout();
            }
        }
    }

    /**
     * Sets the content of the of the cells starting at the specified cell to the passed values.
     * The values are given as as string containing the cell values separated by tab characters.
     * Individual rows are separated by a newline character. Not every row is required to contain
     * the same number of cell values. Excel uses the same format when a range of values is copied
     * to or from the clipboard.
     * @param startCell Origin of the region where the specified values will be put
     * @param values The values to be placed in the spreadsheet (Excel clipboard text format)
     */
    this.setValuesFromString = function(startCell, values) {
        var rows = [];
        var lines = values.split('\n');
        var linesLength = lines.length;
        for (var i = 0; i < linesLength; i++) {
            rows.push(lines[i].split('\t'));
        }
        return self.setValuesFromArray(startCell, rows);
    }

    /**
     * Shows the specified columns.
     * @param {string|string[]} columns An array holding the column names to be shown
     *     or a variable number of column names (vararg parameter)
     */
    this.showColumns = function(columns) {
        columns = flattenArray(arguments);

        var hasChanges;
        var functionCells = {};
        var length = columns.length;

        for (var i = 0; i < length; i++) {
            var name = columns[i];
            var column = getColumnFromCellName(name);
            var def = columnDefinitions[column];
            if (!def.visible) {
                def.visible = true;
                $('.' + options.cssKey + '-' + name, $sections).show();
                hasChanges = true;
                for (var r = 1; r <= options.dimensions.rows; r++) {
                    var cellName = name + r;
                    enumerateDependencies(data[cellName], functionCells);
                }
            }
        }

        if (hasChanges) {
            setLayout();
            updateFunctionCells(functionCells);
        }
    }

    /**
     * Shows the specified rows.
     * @param {string|string[]} rows An array holding the row numbers to be shown (starting with 1)
     *      or a variable number of row numbers (vararg parameter)
     */
    this.showRows = function(rows) {
        rows = flattenArray(arguments);

        var hasChanges;
        var functionCells = {};
        var length = rows.length;

        for (var i = 0; i < length; i++) {
            var number = rows[i];
            var def = rowDefinitions[number - 1];
            if (!def.visible) {
                def.visible = true;
                $('tr.' + options.cssKey + '-' + number, $sections).show();
                hasChanges = true;
                for (var c = 0; c < options.dimensions.columns; c++) {
                    var cellName = columnDefinitions[c].name + number;
                    enumerateDependencies(data[cellName], functionCells);
                }
            }
        }

        if (hasChanges) {
            setLayout();
            updateFunctionCells(functionCells);
            return true;
        }
    }

    /*===== Component initialization ============================================================*/

    // Prepare options
    $spreadsheet = $($spreadsheet);
    options = $.extend({}, Spreadsheet.options, options);
    options.minColumns = Math.max(options.minColumns, options.fixLeft + 1);
    options.minRows = Math.max(options.minRows, options.fixTop + 1);
    options.id = $spreadsheet.attr('id') || options.id || 'spreadsheet';

    // Key codes
    var LEFT = 37;
    var RIGHT = 39;
    var UP = 38;
    var DOWN = 40;
    var RETURN = 13;
    var ESCAPE = 27;
    var HOME = 36;
    var END = 35;
    var PAGEUP = 33;
    var PAGEDOWN = 34;
    var DELETE = 46;
    var TAB = 9;
    var F2 = 113;

    // Mouse move actions
    var NONE = 0;
    var SELECT = 1;
    var DRAG = 2;

    var self = this;
    var columnDefinitions = []; // Attributes: name, visible, width, outerWidth
    var rowDefinitions = []; // Attributes: visible, height

    // The complete cell definitions making up the spreadsheet.
    // Attributes:
    // name: Lower case cell name (e.g. a4)
    // column: Numeric column index starting with 0
    // row: Numeric row index starting with 0
    // cell: The jQuery object corresponding to the specific cell
    // value: The cell value; plain text, number or an HTML string (if html attribute is true)
    // func: Function to calculate the cell value (the value is cached in the value attribute)
    // numeric:  Whether a function expects numeric input values
    // html: Whether the value attribute stores HTML data
    // readonly: The cell cannot be modified by the user
    // tag: Custom user data
    // classValue: Class value to be assigned to the cell
    // classFunc: Function used to dynamically calculate a class value --> function(value):string
    // args: List of value function arguments (scalar for single cells, two-element array for cell range)
    // source: Names of the cells the value function is based upon
    // dep: List of dependent function cells
    var data = {};

    var functionCellSequence = {}; // Sequence in which function cell values need to be calculated
    var getArgsPattern = /function\s*\S*\s*\(\s*(.*?)\s*\)/; // Regex pattern to get function arguments
    var splitArgsPattern = /\s*,\s*/; // Regex pattern to split individual function arguments
    var ampPattern = /&/g; // Regex pattern used to encode HTML entity &
    var ltPattern = /</g; // Regex pattern used to encode HTML entity <
    var gtPattern = />/g; // Regex pattern used to encode HTML entity >
    var newlinePattern = /\n/g; // Regex pattern used to replace new line characters with <br>
    var selection; // Current spreadsheet selection
    var cellPadding; // Vertical cell border and padding (for IE7)
    var editMode; // Spreadsheet is in cell edit mode
    var quickEditMode; // Spreadsheet is in quick edit mode (some navigation keys behave differently)
    var editCell; // Cell currently being edited
    var mouseAction; // Mouse move action (NONE, SELECT, DRAG)
    var lastMousePos; // Last mouse position known before current mousemove event
    var draggingCellOffset; // Relative cell offset of mouse to selection start while dragging
    var draggingSelection; // The range of cells occupied by the dragging selection
    var draggingCrossedLeftwards; // True if the mouse crossed the border between the main and left section while dragging
    var draggingCrossedUpwards; // True if the mouse crossed the border between the main and the top section while dragging
    var keepFocus; // Refocus the hidden focus input field if a focus lost is detected (e.g. after a mouse click)
    var autoScrolling; // Automatic scrolling without mouse movement (e.g. while hovering over a scroll bar) is active
    var autoScrollEvent; // Event object to be used for mousemove events generated by auto scrolling
    var undoStack = []; // Stack of undoable actions (dictionary with value and CSS class per cell)
    var redoStack = []; // Stack with redoable actions
    var leftWidth; // Outer width of the left section
    var topHeight; // Outer height of the top section

    // Detect Internet Explorer 7
   // $.browser.msie7 = $.browser.msie && $.browser.version == 7;
    $.browser.msie7 = $.browser.msie && ($.browser.version == 7 || document.documentMode == 7);
    
    // Detect Mac operating system
    $.browser.mac = navigator.userAgent.indexOf('Mac') != -1;

    // Calculate a map to convert column indexes to names and keep account of hidden/visible columns
    columnDefinitions = [];
    for (var i = 0; i < 26; i++) {
        var code = String.fromCharCode(97 + i);
        columnDefinitions[i] = { name: code, visible: true };
    }
    for (var i = 26; i < 702; i++) {
        var code = String.fromCharCode(97 + i / 26 - 1) + String.fromCharCode(97 + i % 26);
        columnDefinitions[i] = { name: code, visible: true };
    }

    // Create basic HTML elements for the spreadsheet component
    var $topleft = $('<div class="topleft"/>');
    var $top = $('<div class="top"/>');
    var $left = $('<div class="left"/>');
    var $main = $('<div class="main"/>');
    var $sections = $('<div class="sections"/>').append($topleft, $top, $left, $main);
    var $layout = $('<div class="layout"/>');
    var $selection = $('<div class="selection"><div class="shade" /><div class="border" unselectable="on" />'
        + '<div class="top" unselectable="on"/><div class="bottom"  unselectable="on"/>'
        + '<div class="left" unselectable="on"/><div class="right" unselectable="on"/></div>');
    var $dragSelection = $('<div class="dragselection"><div class="top"/><div class="bottom"/><div class="left"/><div class="right"/></div>');
    var $focus = $('<textarea class="focus hidden" autofocus />');
    var $measure = $('<div class="measure" />');
    var $container = $('<div/>').addClass(options.cssKey).append($layout, $selection, $dragSelection, $focus, $measure);
    $spreadsheet.append($container);
    if (options.readonly) {
        $selection.addClass('selecting');
    }

    // Determine vertical cell padding and border for IE7
    if ($.browser.msie7) {
        var $table = $('<table cellspacing="" cellpadding=""><tr><td>A&nbsp;</td></tr></table>');
        var $cell = $('td', $table);
        $layout.append($table);
        cellPadding = parseInt($cell.css('padding-top')) + parseInt($cell.css('padding-bottom')) + parseInt($cell.css('border-bottom-width'));
        $layout.empty();
    }

    // Prevent IE from selecting text and Chrome from displaying the caret cursor when the
    // mouse is being dragged
    $sections.add($selection).bind('selectstart', function(e) {
        return false;
    });

    // Determine initial selection
    var cellName = options.fixedPartsNotSelectable ? getCellNameFromCoords(options.fixLeft, options.fixTop) : 'a1';
    selection = { start: cellName, end: cellName };

    // Synchronize top and left sections with the main section when it has been scrolled
    $main.scroll(function(e) {
        leaveEditMode();
        var left = $main.scrollLeft();
        $top.scrollLeft(left);
        var top = $main.scrollTop();
        $left.scrollTop(top);
        setSelection(selection);
    });

    // Make sure that mouse up events are detected outside of the spreadsheet control
    $(window).mousedown(function() {
        leaveEditMode();
    }).mouseup(function() {
        mouseAction = NONE;
    });

    // Handle mouse down events
    $spreadsheet.mousedown(function(e) {
        
        var elem = document.elementFromPoint(e.pageX, e.pageY);

        if (elem == $spreadsheet.get(0) || elem == $main.get(0)) {
            keepFocus = true;
            return;
        }

        var sectionsPos = $sections.offset();
        var x = e.pageX - sectionsPos.left;
        var y = e.pageY - sectionsPos.top;
        var name = getCellNameFromPos(x, y, true);

        if (!canSelectCell(name)) {
            return;
        }
       

        if (name) { 
            
            if (e.shiftKey) { 
                selection.end = name;
                showCell(selection.end);
                setSelection(selection);
            } else {
                selection.start = selection.end = name;
                showCell(selection.start);
                setSelection(selection);
                mouseAction = SELECT;
                $selection.addClass('selecting');
            }

            // ase: event handler after mouse down
            if (typeof options.selectionHandler === "function") { 
                options.selectionHandler(name, $selection);
            }

            leaveEditMode();
            return false;
        }

        lastMousePos = {
            left: x,
            top: y
        }
    }).mouseup(function(e) {
        var cmdKey = e.ctrlKey || e.metaKey;
        if (mouseAction == DRAG) {
            prepareUndo(undoStack);
            redoStack.length = 0;
            var data = self.getValuesAsString(selection.start, selection.end);
            if (!cmdKey) {
                self.clearValues(selection.start, selection.end);
            }
            selection = draggingSelection;
            self.setValuesFromString(selection.start, data);
            $dragSelection.hide();
            setSelection(selection);
            triggerOnModify();
        }

        var top = $(document).scrollTop();
        if (e.target.tagName != 'SELECT' && e.target.tagName != 'INPUT') {
            self.focus();
        }

        mouseAction = NONE;
        if (!options.readonly) {
            $selection.removeClass('selecting');
        }
    }).dblclick(function(e) {
        if (!options.readonly) {
            var elem = document.elementFromPoint(e.pageX, e.pageY);
            if (elem == $spreadsheet.get(0) || elem == $main.get(0)) {
                keepFocus = true;
                return;
            }

            var sectionsPos = $sections.offset();
            var x = e.pageX - sectionsPos.left;
            var y = e.pageY - sectionsPos.top;
            var mouseCellName = getCellNameFromPos(x, y, true);
            if (mouseCellName) {
                enterEditMode();
            }
        }
    }).mousemove(function(e) {
        if (mouseAction) {
            if (!e.hasOwnProperty('pageX')) {
                e = autoScrollEvent;
            } else {
                autoScrollEvent = e;
            }

            var sectionsPos = $sections.offset();
            var x = e.pageX - sectionsPos.left;
            var y = e.pageY - sectionsPos.top;

            var mouseCellName = getCellNameFromPos(x, y);
            var mouseCellCoords = getCoordsFromCellName(mouseCellName);

            if (mouseAction == SELECT) {
                var startCoords = getCoordsFromCellName(selection.start);
                var endCoords = getCoordsFromCellName(selection.end);
                var firstMainCellCoords = getCoordsFromCellName(getCellNameFromPos(leftWidth, topHeight));
                var newEndCoords = {};

                if (startCoords.column < options.fixLeft && mouseCellCoords.column < options.fixLeft
                        || startCoords.column >= options.fixLeft && mouseCellCoords.column >= options.fixLeft) {
                    newEndCoords.column = mouseCellCoords.column;
                } else if (startCoords.column < options.fixLeft && mouseCellCoords.column >= options.fixLeft) {
                    if (firstMainCellCoords.column > options.fixLeft && endCoords.column < options.fixLeft) {
                        newEndCoords.column = options.fixLeft;
                    } else {
                        newEndCoords.column = mouseCellCoords.column;
                    }
                } else if (startCoords.column >= options.fixLeft && mouseCellCoords.column < options.fixLeft) {
                    if (firstMainCellCoords.column == options.fixLeft) {
                        newEndCoords.column = mouseCellCoords.column;
                    } else {
                        newEndCoords.column = firstMainCellCoords.column - 1;
                    }
                }

                if (startCoords.row < options.fixTop && mouseCellCoords.row < options.fixTop
                        || startCoords.row >= options.fixTop && mouseCellCoords.row >= options.fixTop) {
                    newEndCoords.row = mouseCellCoords.row;
                } else if (startCoords.row < options.fixTop && mouseCellCoords.row >= options.fixTop) {
                    if (firstMainCellCoords.row > options.fixTop && endCoords.row < options.fixTop) {
                        newEndCoords.row = options.fixTop;
                    } else {
                        newEndCoords.row = mouseCellCoords.row;
                    }
                } else if (startCoords.row >= options.fixTop && mouseCellCoords.row < options.fixTop) {
                    if (firstMainCellCoords.row == options.fixTop) {
                        newEndCoords.row = mouseCellCoords.row;
                    } else {
                        newEndCoords.row = firstMainCellCoords.row - 1;
                    }
                }

                if (options.fixedPartsNotSelectable) {
                    if (newEndCoords.column < options.fixLeft) {
                        newEndCoords.column = options.fixLeft;
                    }
                    if (newEndCoords.row < options.fixTop) {
                        newEndCoords.row = options.fixTop;
                    }
                }

                selection.end = getCellNameFromCoords(newEndCoords);
                selection = expandSelection(selection);

                setSelection(selection);
                showCell(selection.end);
            } else if (mouseAction == DRAG) {
                if (x < leftWidth && lastMousePos.left >= leftWidth) {
                    draggingCrossedLeftwards = true;
                } else if (x >= leftWidth && lastMousePos.left < leftWidth) {
                    draggingCrossedLeftwards = false;
                }

                if (y < topHeight && lastMousePos.top >= topHeight) {
                    draggingCrossedUpwards = true;
                } else if (y >= topHeight && lastMousePos.top < topHeight) {
                    draggingCrossedUpwards = false;
                }

                var sel = normalizeRange(selection.start, selection.end);
                var startCellCoords = getCoordsFromCellName(sel.start);
                var endCellCoords = getCoordsFromCellName(sel.end);
                var firstMainCellCoords = getCoordsFromCellName(getCellNameFromPos(leftWidth, topHeight));

                if (draggingCrossedLeftwards && mouseCellCoords.column < options.fixLeft && firstMainCellCoords.column >= options.fixLeft) {
                    mouseCellCoords.column = firstMainCellCoords.column - 1;
                }
                if (draggingCrossedUpwards && mouseCellCoords.row < options.fixTop && firstMainCellCoords.row >= options.fixTop) {
                    mouseCellCoords.row = firstMainCellCoords.row - 1;
                }

                mouseCellName = getCellNameFromCoords(mouseCellCoords);

                var startColumn = mouseCellCoords.column - draggingCellOffset.column;
                var endColumn = startColumn + endCellCoords.column - startCellCoords.column;
                var startRow = mouseCellCoords.row - draggingCellOffset.row;
                var endRow = startRow + endCellCoords.row - startCellCoords.row;

                var minColumn = options.fixedPartsNotSelectable ? options.fixLeft : 0;
                var minRow = options.fixedPartsNotSelectable ? options.fixTop : 0;

                if (startColumn < minColumn) {
                    var diff = minColumn - startColumn;
                    startColumn += diff;
                    endColumn += diff;
                } else if (endColumn >= options.dimensions.columns) {
                    var diff = endColumn - options.dimensions.columns + 1;
                    startColumn -= diff;
                    endColumn -= diff;
                }

                if (startRow < minRow) {
                    var diff = minRow - startRow;
                    startRow += diff;
                    endRow += diff;
                } else if (endRow >= options.dimensions.rows) {
                    var diff = endRow - options.dimensions.rows + 1;
                    startRow -= diff;
                    endRow -= diff;
                }

                draggingSelection = {
                    start: getCellNameFromCoords(startColumn, startRow),
                    end: getCellNameFromCoords(endColumn, endRow)
                };

                showCell(mouseCellName);
                setSelection(draggingSelection, $dragSelection);
            }

            if (!autoScrolling) {
                setTimeout(function() {
                    autoScrolling = false;
                    $spreadsheet.mousemove();
                }, 200);
                autoScrolling = true;
            }
        }

        lastMousePos = {
            left: x,
            top: y
        }
    });

    $selection.find('.top, .bottom, .left, .right').mousedown(function(e) {
        if (!options.readonly) {
            mouseAction = DRAG;
            setSelection(selection, $dragSelection);

            var sectionsPos = $sections.offset();
            var x = e.pageX - sectionsPos.left;
            var y = e.pageY - sectionsPos.top;

            var mouseCellName = getCellNameFromPos(x, y);
            var mouseCellCoords = draggingLastMouseCellCoords = getCoordsFromCellName(mouseCellName);
            var sel = normalizeRange(selection.start, selection.end);
            var startCellCoords = getCoordsFromCellName(sel.start);
            var endCellCoords = getCoordsFromCellName(sel.end);

            var column = mouseCellCoords.column - startCellCoords.column;
            var max = endCellCoords.column - startCellCoords.column;
            if (column < 0) {
                column = 0;
            } else if (column > max) {
                column = max;
            }

            var row = mouseCellCoords.row - startCellCoords.row;
            var max = endCellCoords.row - startCellCoords.row;
            if (row < 0) {
                row = 0;
            } else if (row > max) {
                row = max;
            }

            draggingSelection = sel;
            draggingCrossedLeftwards = false;
            draggingCrossedUpwards = false;
            draggingCellOffset = {
                column: column,
                row: row
            };
        }

        lastMousePos = {
            left: x,
            top: y
        }

        return false;
    });
    
    $focus.bind('beforecopy', function(e) {
        return false;
    }).bind('beforecut', function(e) {
        return false;
    }).bind('paste', function(e) {
        if (!options.readonly) {
            var clipboard = e.originalEvent.clipboardData || window.clipboardData;
            if (clipboard) {
                prepareUndo(undoStack);
                redoStack.length = 0;
                var clipboardData = clipboard.getData('Text');
                var startCell = normalizeRange(selection.start, selection.end).start;
                self.setValuesFromString(startCell, clipboardData);
                triggerOnModify();
                return false;
            } else {
                setTimeout(function() {
                    prepareUndo(undoStack);
                    redoStack.length = 0;
                    var data = $focus.val();
                    var startCell = normalizeRange(selection.start, selection.end).start;
                    self.setValuesFromString(startCell, data);
                    triggerOnModify();
                }, 100);
            }
        }
    });

    $focus.keydown(function(e) {
        var cmdKey = e.ctrlKey || e.metaKey;
        if (!editMode) {
            if (e.keyCode == 'C'.charCodeAt(0) && cmdKey) {
                var data = self.getValuesAsString(selection.start, selection.end);
                $focus.val(data).select();
            } else if (e.keyCode == 'X'.charCodeAt(0) && cmdKey) {
                if (!options.readonly) {
                    prepareUndo(undoStack);
                    redoStack.length = 0;
                    var data = self.getValuesAsString(selection.start, selection.end);
                    $focus.val(data).select();
                    self.clearValues(selection.start, selection.end);
                    triggerOnModify();
                }
            } else if (e.keyCode == 'V'.charCodeAt(0) && cmdKey) {
                $focus.val('');
            } else if (e.keyCode == 'Z'.charCodeAt(0) && cmdKey) {
                if (undoStack.length) {
                    prepareUndo(redoStack);
                    executeUndo(undoStack);
                }
            } else if (e.keyCode == 'Y'.charCodeAt(0) && cmdKey || e.keyCode == 'Z'.charCodeAt(0) && cmdKey && e.shiftKey) {
                if (redoStack.length) {
                    prepareUndo(undoStack);
                    executeUndo(redoStack);
                }
            } else if (e.keyCode == DELETE) {
                if (!options.readonly) {
                    prepareUndo(undoStack);
                    redoStack.length = 0;
                    self.clearValues(selection.start, selection.end);
                    triggerOnModify();
                }
            } else if (e.keyCode == F2) {
                if (!options.readonly) {
                    enterEditMode();
                    return false;
                }
            }
        } else if (e.keyCode == ESCAPE) {
            cancelEditMode();
        } else if (e.keyCode == RETURN && cmdKey) {
            insertAtCursor($focus, '\r\n');
            return false;
        }
    }).keyup(function(e) {
        if (editMode) {
            var text = $.trim($focus.val().replace(newlinePattern, '<br>'));
            if (text.substr(text.length - 4) == '<br>' || text.length == 0) {
                text += '&nbsp;';
            }
            $measure.html(text);
            $measure.css('width', $focus.width());
            var $cell = editCell.cell;
            var vertPadding = parseInt($cell.css('padding-top')) + parseInt($cell.css('padding-bottom'));
            var dim = getCellDimensions(editCell.name);
            var mainHeight = $main.get(0).clientHeight;
            var height = Math.min(Math.max($measure.height(), dim.height), mainHeight);
            var top = dim.top;
            var maxHeight = topHeight + mainHeight;
            var bottom = top + height;
            if (bottom > maxHeight) {
                top -= bottom - maxHeight;
            }
            $focus.css({
                top: top,
                height: height - vertPadding
            });
        }
    }).keypress(function(e) {
        var cmdKey = e.ctrlKey || e.metaKey;
        if (!editMode && !cmdKey && e.which >= 32) {
            enterEditMode(String.fromCharCode(e.which));
            return false;
        }
    }).mousedown(function(e) {
        e.stopPropagation();
    });


    // ase: bind focus events
    $focus.on('keydown keyup', function(e) {
        
        // selector hendler
        if (typeof options.selectionHandler === "function") { 
            options.selectionHandler(selection.start, $selection);
        }
    });

    if ($.browser.opera) {
        $focus.keypress(handleNavigationKey);
    } else {
        $focus.keydown(handleNavigationKey);
    }

    $focus.blur(function(e) {
        if (keepFocus) {
            self.focus();
            keepFocus = false;
        }
    });

    this.setData(initialData, true);

    /*===== Private methods =====================================================================*/

    function prepareFunctionCell(cell, func) {
        var parts = getArgsPattern.exec(func)[1].split(splitArgsPattern);
        var args = [];
        var source = [];
        
        var len = parts.length;
        for (var i = 0; i < len; i++) {
            var part = parts[i];
            var index = part.indexOf('_');
            if (index > 0) {
                var arg = [
                    part.substr(0, index),
                    part.substr(index + 1)
                ];
                args.push(arg);
                var start = getCoordsFromCellName(arg[0]);
                var end = getCoordsFromCellName(arg[1]);
                for (var column = start.column; column <= end.column; column++) {
                    for (var row = start.row; row <= end.row; row++) {
                        var name = columnDefinitions[column].name + (row + 1);
                        source.push(name);
                    }
                }
            } else {
                args.push(part);
                source.push(part);
            }
        }

        cell.func = func;
        cell.value = null;
        cell.args = args;
        cell.source = source;
        cell.readonly = true;

        len = source.length;
        for (var i = 0; i < len; i++) {
            var name = source[i];
            sourceCell = data[name];
            if (!sourceCell) {
                sourceCell = data[name] = {};
            }
            var dep = sourceCell.dep;
            if (!dep) {
                dep = sourceCell.dep = {};
            }
            dep[cell.name] = 1;
        }
    }

    function calculateFunctionCellValue(cell) {
        var params = [];
        var args = cell.args;
        var length = args.length;

        var options = {
            numeric: cell.numeric,
            hiddenAsNull: cell.hiddenAsNull,
            compact: true
        }

        for (var i = 0; i < length; i++) {
            var arg = args[i];
            if (arg instanceof Array) {
                params.push(self.getValuesAsArray(arg[0], arg[1], options));
            } else {
                params.push(self.getValue(arg, options));
            }
        }

        var value = cell.func.apply(this, params);
        if (value != cell.value) {
            cell.value = value;
            if (cell.classFunc) {
                if (cell.numeric) {
                    value = parseFloat(cell.value);
                    value = isNaN(value) ? 0.0 : value;
                }
                cell.classValue = cell.classFunc(value);
            }
            return true;
        } else {
            return false;
        }
    }

    function updateFunctionCells(cells) {
        if (cells && $.isEmptyObject(cells)) {
            return;
        } else {
            for (var key in functionCellSequence) {
                if (!cells || cells.hasOwnProperty(key)) {
                    var cell = functionCellSequence[key];
                    if (calculateFunctionCellValue(cell)) {
                        var value = cell.value;
                        if (isEmpty(value)) {
                            cell.cell.html('&nbsp;');
                        } else {
                            cell.cell.text(value);
                        }

                        if (cell.classFunc) {
                            if (cell.numeric) {
                                value = parseFloat(value);
                                value = isNaN(value) ? 0.0 : value;
                            }
                            var cls = cell.classValue = cell.classFunc(value);
                            cell.cell.get(0).className = options.id + '-'
                                + columnDefinitions[cell.column].name
                                + (cell.readonly ? ' readonly' : '')
                                + (cls ? ' ' + cls : '');
                        }
                    }
                }
            }
        }
    }

    function sortFunctionCells() {
        var functionCells = {};
        var functionCount = 0;
        functionCellSequence = {};
        
        for (var key in data) {
            var cell = data[key];
            if (cell.hasOwnProperty('source')) {
                functionCells[key] = cell;
                functionCount++;
            }
        }

        while (functionCount) {
            var currentfunctionCount = functionCount;
            for (var key in functionCells) {
                var cell = functionCells[key];
                var source = cell.source;
                var length = source.length;
                var keep = false;
                for (var i = 0; i < length; i++) {
                    var sourceKey = source[i];
                    if (functionCells.hasOwnProperty(sourceKey)) {
                        keep = true;
                        break;
                    }
                }
                if (!keep) {
                    delete functionCells[key];
                    functionCellSequence[key] = cell;
                    functionCount--;
                }
            }

            if (currentfunctionCount == functionCount) {
                throw "Spreadsheet functions contain recursive dependencies";
            }
        }
    }

    function enumerateDependencies(cell, dict) {
        var dep = cell.dep;
        if (dep) {
            for (key in dep) {
               dict[key] = 1;
               enumerateDependencies(data[key], dict);
            }
        }
    }

    function buildTables(dimensions) {
        var mainColumns = dimensions.columns - options.fixLeft;
        var mainRows = dimensions.rows - options.fixTop;
        var measuredLeft;
        var measuredMain;

        if (options.fixTop) {
            if (options.fixLeft) {
                buildTable($topleft, options.fixLeft, options.fixTop, 0, 0, true);
                measuredLeft = true;
                $topleft.show();
            } else {
                $topleft.hide();
            }
            buildTable($top, mainColumns, options.fixTop, options.fixLeft, 0, true);
            measuredMain = true;
            $top.show();
        } else {
            $top.hide();
            $topleft.hide();
        }

        if (options.fixLeft) {
            buildTable($left, options.fixLeft, mainRows, 0, options.fixTop, !measuredLeft);
            $left.show();
        } else {
            $left.hide();
        }
        buildTable($main, mainColumns, mainRows, options.fixLeft, options.fixTop, !measuredMain);
    }

    function buildTable($div, columns, rows, startColumn, startRow, measure) {
        var width;
        var html;

        if ($.browser.msie7) {
            html = '<div><table cellspacing="" cellpadding="">';
        } else {
            html = '<div><table>';
        }

        var merge = options.merge;
        for (var row = 0; row < rows; row++) {
            var classPrefix = options.cssKey + '-';
            var number = (startRow + row + 1);
            var line = '<tr class="' + classPrefix + number + '">';
            for (var column = 0; column < columns; column++) {
                var index = startColumn + column;
                var name = columnDefinitions[index].name;
                var key = name + number;
                var cls = classPrefix + name;
                var content = '&nbsp;';

                if (data.hasOwnProperty(key)) {
                    var cell = data[key];
                    if (cell) {
                        if (cell.readonly) {
                            cls += ' readonly';
                        }
                        var value = cell.value;
                        if (!isEmpty(value)) {
                            if (cell.html) {
                                content = value;
                            } else {
                                content = value.toString().replace(ampPattern, '&amp;').replace(ltPattern, '&lt;').replace(gtPattern, '&gt;').replace(newlinePattern, '<br>');
                            }
                        }
                        if (cell.classValue) {
                            cls += ' ' + cell.classValue;
                        }
                    }
                }

                var colspan = '';
                var span = merge[key];
                if (span) {
                    colspan = ' colspan="' + span + '"';
                    column += span - 1;
                }

                line += '<td id="' + classPrefix + key + '" class="' + cls + '"' + colspan + ' unselectable="on">' + content + '</td>';
            }
            line += '</tr>';

            if (row == 0) {
                if (measure) {
                    $layout.get(0).innerHTML = html +  line + '</table></div>';
                    var $table = $layout.find('table');
                    $('td', $table).each(function(index) {
                        var col = columnDefinitions[startColumn + index];
                        col.width = this.clientWidth;
                        col.outerWidth = $(this).outerWidth();
                    });
                    $layout.empty();
                }

                html += '<thead><tr>';
                var length = startColumn + columns;
                for (var column = startColumn; column < length; column++) {
                    var cls = classPrefix + columnDefinitions[column].name;
                    var width = columnDefinitions[column].outerWidth;
                    html += '<th class="' + cls + '" style="width: ' + width + 'px"></th>';
                }
                html += '</tr></thead><tbody>';
            }

            html += line;
        }

        html += '</tbody></table></div>';
        $div.get(0).innerHTML = html;
    }

    function enterEditMode(initialValue) {
        var cell = editCell = data[selection.start];
        if (cell.readonly) {
            return;
        }

        var $cell = cell.cell;
        var horzPadding = parseInt($cell.css('padding-left')) + parseInt($cell.css('padding-right'));
        var vertPadding = parseInt($cell.css('padding-top')) + parseInt($cell.css('padding-bottom'));
      
        $focus.hide().show().focus(); // Prevent cursor bug in IE7
        
        var dim = getCellDimensions(selection.start);
        $focus.removeClass('hidden').css({
            left: dim.left,
            top: dim.top,
            width: dim.width - horzPadding,
            height: dim.height - vertPadding
        });
            
        if (initialValue === undefined) {
            $focus.val(cell.value);
        } else {
            $focus.val(initialValue);
            quickEditMode = true;
        }

        editMode = true;
    }

    function leaveEditMode() {
        if (editMode) {
            var value = $focus.val();
            if (value != editCell.value) {
                prepareUndo(undoStack);
                redoStack.length = 0;
                updateCellValue(editCell, value);
                var functionCells = {};
                enumerateDependencies(editCell, functionCells);
                updateFunctionCells(functionCells);
                setLayout();
                triggerOnModify();
            }
            cancelEditMode();
        }
    }

    function cancelEditMode() {
        if (editMode) {
            $focus.addClass('hidden');
            editCell = null;
            editMode = false;
            quickEditMode = false;
            $focus.hide().show().focus(); // Prevent cursor bug in IE7
        }
    }

    function triggerOnModify(list) {
        if (options.onModify) {
            if (list) {
                for (var i = 0; i < list.length; i++) {
                    var cell = data[list[i]];
                    list[i] = {
                        name: cell.name,
                        value: cell.value,
                        tag: cell.tag
                    };
                }
            } else {
                list = [];
                var buffer = undoStack[undoStack.length - 1].data;
                for (var key in data) {
                    var cell = data[key];
                    if (cell.value != buffer[key].value) {
                        var cellData = {
                            name: key,
                            value: cell.value,
                            tag: cell.tag
                        }
                        list.push(cellData);
                    }
                }
            }
            options.onModify(list);
        }
    }



    /**
     * Returns the name of the specified cell.
     * @param {number} column The column number of the cell (starting with 0)
     * @param {number} row The row number of the cell (starting with 0)
     * @return {string} The name of the cell
     */
    function getCellNameFromCoords(column, row) {
        if (typeof(column) == 'object') {
            row = column.row;
            column = column.column;
        }
        return columnDefinitions[column].name + (row + 1);
    }

    /**
     * Returns the name of the cell at the specified coordinates. The coordinates must be
     * given relative to the sections element.
     * @param left The left offset of the cell
     * @param top The top offset of the cell
     * @param [strict=false] Do not map coordinates outside of the cell area to their nearest cell
     * @return The name of the cell or null if no matching cell could be found
     */
    function getCellNameFromPos(left, top, strict) {
        if (typeof(left) == 'object') {
            left = left.left;
            top = left.top;
        }

        if (left >= leftWidth) {
            left += $main.scrollLeft();
        }
        if (top >= topHeight) {
            top += $main.scrollTop();
        }

        var column = 0;
        var maxColumn = options.dimensions.columns;
        while (left >= 0 && column < maxColumn) {
            var def = columnDefinitions[column];
            if (def.visible) {
                left -= def.outerWidth;
            }
            column++;
        }

        var row = 0;
        var maxRow = options.dimensions.rows;
        while (top >= 0 && row < maxRow) {
            var def = rowDefinitions[row];
            if (def.visible) {
                top -= def.height;
            }
            row++;
        }

        if (strict && (left > 0 || top > 0)) {
            return null;
        } else {
            var cellName = columnDefinitions[column - 1].name + (row);
            return data[cellName].master || cellName;
        }
    }

    /**
     * Returns the column number of the specified cell.
     * @param {string} cellName The name of the cell to return the column number for
     * @return {number} The column number of the cell
     */
    function getColumnFromCellName(cellName) {
        var column = cellName.charCodeAt(0) - 97;
        if (column >= 0) {
            var c = cellName.charCodeAt(1) - 97;
            if (c >= 0) {
                column = (column + 1) * 26 + c;
            }
        }
        return column;
    }

    /**
     * Returns the column and row number of the specified cell.
     * @param cellName The name of the cell to calculate the coordinates for
     * @return Dictionary with the attributes "column" and "row"
     */
    function getCoordsFromCellName(cellName) {
        if (cellName != null) {
            var column = cellName.charCodeAt(0) - 97;
            var row;
            if (column >= 0) {
                var c = cellName.charCodeAt(1) - 97;
                if (c >= 0) {
                    column = (column + 1) * 26 + c;
                    row = parseInt(cellName.substr(2)) - 1;
                } else {
                    row = parseInt(cellName.substr(1)) - 1;
                }
                return { column: column, row: row };
            }
        }
        return null;
    }

    /**
     * Returns the relative position, client width and height of the specified
     * cell.
     * @param {string} cellName The name of the cell to be measured
     * @param {jQuery} [parent=$sections] The element relative to which the position is to be indicated
     * @return {object} Dictionary with the attributes "left", "top", "width" and "height"
     */
    function getCellDimensions(cellName, parent) {
        parent = parent ? $(parent) : $sections;

        var cell = data[cellName];
        var mergedCell = false;
        if (cell.master) {
            cell = data[cell.master];
        }

        var $cell = cell.cell;
        if (!$cell) {
            return {
                left: 0,
                top: 0,
                width: 0,
                height: 0
            }
        }

        var cellPos = $cell.offset();
        var cellElem = $cell.get(0);
        var spreadsheetPos = parent.offset();

        return {
            left: cellPos.left - spreadsheetPos.left,
            top: cellPos.top - spreadsheetPos.top,
            width: cellElem.clientWidth,
            height: cellElem.clientHeight
        }
    }

    /**
     * Checks whether the specified cell can be selected.
     * @param {string} cellName The name of the cell to be checked
     * @return {boolean} True if the cell can be selected
     */
    function canSelectCell(cellName) {
        if (options.fixedPartsNotSelectable) {
            var coords = getCoordsFromCellName(cellName);
            return coords && coords.column >= options.fixLeft && coords.row >= options.fixTop;
        } else {
            return true;
        }
    }

    /**
     * Ensures that an arbitrary cell range is always organized in a way that the
     * start specifies the top left corner and the end the bottom right corner.
     * 
     * @param {string} start Starting cell of the range
     * @param {string} end Ending cell of the range
     * @param {boolean} [strict=false] The range is truncated to the area available to the spreadsheet
     * @return {{start:number, end:number}} The normalized range or null if the specified range lies outside
     *     of the area occupied by the spreadsheet
     */
    function normalizeRange(start, end, strict) {
        paramStart = getCoordsFromCellName(start);
        paramEnd = getCoordsFromCellName(end);

        var start = {
            column: Math.min(paramStart.column, paramEnd.column),
            row: Math.min(paramStart.row, paramEnd.row)
        }

        var end = {
            column: Math.max(paramStart.column, paramEnd.column),
            row: Math.max(paramStart.row, paramEnd.row)
        }

        if (strict) {
            if (end.column >= options.dimensions.columns) {
                end.column = options.dimensions.columns - 1;
                if (start.column >= options.dimensions.columns) {
                    start.column = end.column;
                }
            }

            if (end.row >= options.dimensions.rows) {
                end.row = options.dimensions.rows - 1;
                if (start.row >= options.dimensions.rows) {
                    start.row = end.row;
                }
            }
        }

        return {
            start: getCellNameFromCoords(start),
            end: getCellNameFromCoords(end)
        }
    }

    /**
     * Updates the specified cell to a new value and CSS class name. If the information is
     * different from the existing one, the associated DOM object is updated accordingly.
     * @param {object} cell The description object of the cell to be updated
     * @param {string|number} value The new cell value
     * @param {string} classValue The new CSS class
     * @param {boolean} [force=false] Update the cell even if the value or CSS class has not been changed
     * @return {boolean} True if the cell value has changed
     */
    function updateCellValue(cell, value, classValue, force) {
        var isChanged;
        var $cell = cell.cell;
        if (!$cell) {
            return false;
        }

        if (force || !cell.func && !cell.readonly && value !== undefined && value != cell.value) {
            cell.value = value;
            if (isEmpty(value)) {
                $cell.html('&nbsp;');
            } else if (cell.html) {
                $cell.html(value);
            } else {
                $cell.html(value.toString().replace(ampPattern, '&amp;').replace(ltPattern, '&lt;').replace(gtPattern, '&gt;').replace(newlinePattern, '<br>'));
            }
            isChanged = true;
        }

        if (cell.classFunc) {
            if (cell) {
                value = parseFloat(value);
                value = isNaN(value) ? 0.0 : value;
            }
            classValue = cell.classFunc(value);
        }

        if (force || classValue !== undefined && classValue != cell.classValue) {
            cell.classValue = classValue;
            $cell.get(0).className = options.id + '-'
                + columnDefinitions[cell.column].name
                + (cell.readonly ? ' readonly' : '')
                + (classValue ? ' ' + classValue : '');
        }

        return isChanged;
    }

    /**
     * Scrolls the main section in a manner that ensures that the specified cell is visible
     * @param cellName The name of the cell that needs to be visible
     */
    function showCell(cellName) {
        var coords = getCoordsFromCellName(cellName);
        coords.column -= options.fixLeft;
        coords.row -= options.fixTop;

        if (coords.column >= 0 || coords.row >= 0) {
            var dim = getCellDimensions(cellName, $main);
            var main = $main.get(0);

            if (coords.column >= 0) {
                var mainWidth = main.clientWidth;
                if (dim.left < 0) {
                    var offset = $main.scrollLeft() + dim.left;
                    $main.scrollLeft(offset);
                    $top.scrollLeft(offset);
                } else {
                    var diff = dim.left + dim.width - mainWidth;
                    if (diff > 0) {
                        var offset = $main.scrollLeft() + diff;
                        $main.scrollLeft(offset);
                        $top.scrollLeft(offset);
                    }
                }
            }

            if (coords.row >= 0) {
                var mainHeight = main.clientHeight;
                if (dim.top < 0) {
                    var offset = $main.scrollTop() + dim.top;
                    $main.scrollTop(offset);
                    $left.scrollTop(offset);
                } else {
                    var diff = dim.top + dim.height - mainHeight;
                    if (diff > 0) {
                        var offset = $main.scrollTop() + diff;
                        $main.scrollTop(offset);
                        $left.scrollTop(offset);
                    }
                }
            }
        }
    }

    /**
     * Checks whether the specified cell is visible.
     * @param {string} cellName The name of the cell to be checked
     * @param {boolean} [partially=false] Determines whether the cell needs to be entirely or partially visible
     * @return {boolean} True if the cell is visible
     */
    function isCellVisible(cellName, partially) {
        var dim = getCellDimensions(cellName, $main);
        var main = $main.get(0);
        var width = main.clientWidth;
        var height = main.clientHeight;

        var coords = getCoordsFromCellName(cellName);
        var compareHorz = coords.column >= options.fixLeft;
        var compareVert = coords.row >= options.fixTop;

        if (partially) {
            return (!compareHorz || (dim.left + dim.width) >= 0 && dim.left < width)
                && (!compareVert || (dim.top + dim.height) >= 0 && dim.top < height);
        } else {
            return (!compareHorz || dim.left >= 0 && (dim.left + dim.width) < width)
                && (!compareVert ||  dim.top >= 0 && (dim.top + dim.height) < height);
        }
    }

    /**
     * Checks whether the specified cell is hidden because the containing row or column is hidden.
     * @param {string} cellName
     * @return {boolean} True if the cell is hidden
     */
    function isCellHidden(cellName) {
        var cell = data[cellName] || getCoordsFromCellName(cellName);
        var column = columnDefinitions[cell.column];
        var row = rowDefinitions[cell.row];
        return column && !column.visible || row && !row.visible;
    }

    /**
     * Synchronizes the heights of all associated table rows and repositions the table fragments
     * (topleft, top, left, main) according to their contents. Furthermore the selection is
     * positioned according to the values in self.selection.
     */
    function setLayout() {
        // Remove currently assigned height styles from table rows
        $('tr', $sections).css('height', '');

        // Calculate width of div elements surrounding the individual tables
        var width = 0;
        for (var i = 0; i < options.fixLeft; i++) {
            var col = columnDefinitions[i];
            if (col.visible) {
                width += col.outerWidth;
            }
        }
        $topleft.find('div').width(width);
        $left.find('div').width(width);

        width = 0;
        for (var i = options.fixLeft; i < options.dimensions.columns; i++) {
            var col = columnDefinitions[i];
            if (col.visible) {
                width += col.outerWidth;
            }
        }
        $top.find('div').width(width);
        $main.find('div').width(width);

        // Make sure row heights of fixed left and main columns are of equal size
        var totalHeight = 0;
        if (true || options.fixLeft) {
            var $leftRows = $.merge($('tbody tr', $topleft), $('tbody tr', $left));
            var $rightRows = $.merge($('tbody tr', $top), $('tbody tr', $main));
            var length = $leftRows.length || $rightRows.length;

            if (rowDefinitions.length < length) {
                for (var i = rowDefinitions.length; i < length; i++) {
                    rowDefinitions[i] = { visible: true };
                }
            }

            for (var i = 0; i < length; i++) {
                var $leftRow = $leftRows.eq(i);
                var $rightRow = $rightRows.eq(i);
                
                var leftHeight = $leftRow.height();
                var rightHeight = $rightRow.height();

                var height = Math.max(leftHeight, rightHeight);
                if (rowDefinitions[i].visible) {
                    totalHeight += height;
                }

                var realHeight = $.browser.msie7 ? height - cellPadding : height;
                rowDefinitions[i].height = height;

                if (height != leftHeight) {
                    $leftRow.height(realHeight);
                }
                if (height != rightHeight) {
                    $rightRow.height(realHeight);
                }
            }
        }

        // Automatically calculate height of surrounding spreadsheet container if desired
        if (options.autoHeight) {
        	$spreadsheet.css('height', totalHeight + 24); // adoweb: +5px
            var main = $main.get(0);
            var diff = main.offsetHeight - main.clientHeight;
            if (diff) {
                $spreadsheet.css('height', totalHeight + diff + 20); // adoweb: +5px
            }
        }

        // Reposition table fragments
        leftWidth = $left.is(':visible') ? $left.outerWidth() : 0;
        topHeight = $top.is(':visible') ? $top.outerHeight() : 0;

        $main.css('margin-left', leftWidth).css('margin-top', topHeight);
        $left.css('margin-top', topHeight);
        $top.css('margin-left', leftWidth);

        setSelection(selection);
    }

    /**
     * Positions the selection element according to the specified position. The selection
     * element may also be hidden by this method (if the specified position defines a
     * selection that is currently not visible).
     * @param {{start:number,end:number}} selection
     *     A dictionary with two elements start and end specifying the starting
     *     and ending cell of the selection
     * @param {jQuery} [div=self.$selection] The div to be positioned
     */
    function setSelection(selection, div) {
        
        if (!div) {
            div = $selection;
        }
        div.hide();

        var originalStartCoords = getCoordsFromCellName(selection.start);
        var originalEndCoords = getCoordsFromCellName(selection.end);

        var startCoords = {
            column: Math.min(originalStartCoords.column, originalEndCoords.column),
            row: Math.min(originalStartCoords.row, originalEndCoords.row)
        }

        var endCoords = {
            column: Math.max(originalStartCoords.column, originalEndCoords.column),
            row: Math.max(originalStartCoords.row, originalEndCoords.row)
        }

        var start = getCellDimensions(getCellNameFromCoords(startCoords));
        var end = getCellDimensions(getCellNameFromCoords(endCoords));

        var left = start.left;
        var top = start.top;
        var width = end.left + end.width - start.left;
        var height = end.top + end.height - start.top;

        if (startCoords.column >= options.fixLeft) {
            // Completely hide selection if it lies within the main section but is invisible on the left
            if (left + width <= leftWidth) {
                return;
            }

            // Shrink the selection on the left if it lies within the main section and is partially invisible on the left
            var diff = leftWidth - left;
            if (diff > 0) {
                left += diff;
                width -= diff;
            }
        } else if (endCoords.column >= options.fixLeft && left + width < leftWidth) {
            // Ensure that a selection covering the fixed left column and the main section does not shrink into
            // the left column
            width = leftWidth - left;
        }

        if (startCoords.row >= options.fixTop) {
            // Completely hide the selection if it lies within the main section but is invisible on the top
            if (top + height <= topHeight) {
                return;
            }

            // Shrink the selection on the top if it lies within the main section and is partially invisible on the top
            var diff = topHeight - top;
            if (diff > 0) {
                top += diff;
                height -= diff;
            }
        } else if (endCoords.row >= options.fixTop && top + height < topHeight) {
            // Ensure that a selection covering the fixed top row and the main section does not shrink into
            // the top row
            height = topHeight - top;
        }

        var maxWidth = leftWidth + $main.get(0).clientWidth;

        // Completely hide the selection if it is invisible on the right
        if (left >= maxWidth) {
            return;
        }

        // Shrink the selection on the right if it is partially invisible on the right
        var diff = left + width - maxWidth;
        if (diff > 0) {
            width -= diff;
        }

        var maxHeight = topHeight + $main.get(0).clientHeight;

        // Completely hide the selection if it is invisible on the bottom
        if (top >= maxHeight) {
            return;
        }

        // Shrink the selection on the bottom if it is partially invisible on the bottom
        var diff = top + height - maxHeight;
        if (diff > 0) {
            height -= diff;
        }

        div.css('left', left).css('top', top).width(width).height(height).show();

        // ase: event handler after mouse down
        if (selection.start == selection.end && typeof options.selectionHandler === "function") {
            options.selectionHandler(selection.start, $selection);
        }
    }

    function expandSelection(selection) {
        if (selection.start == selection.end) {
            return selection;
        }

        var startCoords = getCoordsFromCellName(selection.start);
        var endCoords = getCoordsFromCellName(selection.end);

        var startColumn = Math.min(startCoords.column, endCoords.column);
        var endColumn = Math.max(startCoords.column, endCoords.column);
        var startRow = Math.min(startCoords.row, endCoords.row);
        var endRow = Math.max(startCoords.row, endCoords.row);

        var restart;
        do {
            restart = false;
            for (var row = startRow; row <= endRow; row++) {
                for (var column = startColumn; column <= endColumn; column++) {
                    var cellName = getCellNameFromCoords(column, row);
                    var cell = data[cellName];
                    if (cell.master) {
                        cell = data[cell.master];
                    }
                    if (cell.span) {
                        var coords = getCoordsFromCellName(cell.name);
                        if (coords.column < startColumn) {
                            startColumn = coords.column;
                            restart = true;
                            break;
                        }
                        var end = coords.column + cell.span - 1;
                        if (end > endColumn) {
                            endColumn = end;
                            restart = true;
                            break;
                        }
                    }
                }
                if (restart) {
                    break;
                }
            }
        } while (restart);

        if (startCoords.column < endCoords.column) {
            startCoords.column = startColumn;
            endCoords.column = endColumn;
        } else {
            startCoords.column = endColumn;
            endCoords.column = startColumn;
        }

        return {
            start: getCellNameFromCoords(startCoords),
            end: getCellNameFromCoords(endCoords)
        }
    }

    /**
     * Modifies the current current selection until it is visible i.e. does not consist of
     * hidden cells.
     */
    function ensureHasSelection() {
        function fixCoord(initialCoord, defs, max) {
            var coord = initialCoord;
            while (!defs[coord].visible) {
                coord++;
                if (coord >= max) {
                    coord == initialCoord - 1;
                    while (!defs[coord].visible) {
                        coord--;
                        if (coord < 0) {
                            return initialCoord;
                        }
                    }
                    break;
                }
            }
            return coord;
        }

        var coords = getCoordsFromCellName(selection.start);
        var column = fixCoord(coords.column, columnDefinitions, options.dimensions.columns);
        var row = fixCoord(coords.row, rowDefinitions, options.dimensions.rows);
        selection.start = getCellNameFromCoords(column, row);

        coords = getCoordsFromCellName(selection.end);
        column = fixCoord(coords.column, columnDefinitions, options.dimensions.columns);
        row = fixCoord(coords.row, rowDefinitions, options.dimensions.rows);
        selection.end = getCellNameFromCoords(column, row);
    }

    /**
     * Checks the specified keyboard event for a navigational key and acts upon it.
     * @param e The event embedding the key action
     */
    function handleNavigationKey(e) {
        var coords = getCoordsFromCellName(selection.end);
        var cmdKey = e.ctrlKey || e.metaKey;
        if (!editMode || quickEditMode || e.keyCode == TAB || e.keyCode == RETURN && !cmdKey) {
            if (e.keyCode == HOME) {
                coords.column = options.fixLeft;
                while (!columnDefinitions[coords.column].visible) {
                    coords.column++;
                }
                if (cmdKey) {
                    coords.row = options.fixTop;
                    while (!rowDefinitions[coords.row].visible) {
                        coords.row++;
                    }
                }
            } else if (e.keyCode == END) {
                coords.column = options.dimensions.columns - 1;
                while (!columnDefinitions[coords.column].visible) {
                    coords.column--;
                }
                if (cmdKey) {
                    coords.row = options.dimensions.rows - 1;
                    while (!rowDefinitions[coords.row].visible) {
                        coords.row--;
                    }
                }
            } else if (e.keyCode == PAGEUP || e.keyCode == PAGEDOWN) {
                // Get relative row number
                var topRow = getCoordsFromCellName(getCellNameFromPos(leftWidth, topHeight));
                var selRow = getCoordsFromCellName(selection.end);
                var rowOffset = selRow.row - topRow.row;
                if (rowOffset < 0) {
                    rowOffset = 0;
                }

                var direction = e.keyCode == PAGEUP ? -1 : 1;
                $main.scrollTop($main.scrollTop() + direction * $main.get(0).clientHeight);
                $left.scrollTop($main.scrollTop());

                var newCoords = getCoordsFromCellName(getCellNameFromPos(leftWidth, topHeight));
                newCoords.row += rowOffset;

                while (true) {
                    selection.end = getCellNameFromCoords(coords.column, newCoords.row);
                    if (isCellVisible(getCellNameFromCoords(topRow.column, newCoords.row), true)) {
                        break;
                    } else {
                        newCoords.row -= direction;
                    }
                }
                coords = null;
            } else if (e.keyCode == LEFT || e.keyCode == TAB && e.shiftKey) {
                if (e.keyCode == TAB) {
                    e.ctrlKey = false;
                    e.metaKey = false;
                    e.shiftKey = false;
                }
                var pos = coords.column;
                var minPos = options.fixedPartsNotSelectable ? options.fixLeft : 0;
                while (pos > minPos) {
                    var cellName = getCellNameFromCoords(--pos, coords.row);
                    pos = getCoordsFromCellName(data[cellName].master || cellName).column;
                    var skipEmpty = cmdKey && isEmpty(data[getCellNameFromCoords(pos, coords.row)].value);
                    if (columnDefinitions[pos].visible && !skipEmpty) {
                        break;
                    }
                }
                if (pos >= minPos) {
                    coords.column = pos;
                } else if (cmdKey) {
                    coords.column = minPos;
                    while (!columnDefinitions[coords.column].visible) {
                        coords.column++;
                    }
                }
            } else if (e.keyCode == RIGHT || e.keyCode == TAB) {
                if (e.keyCode == TAB) {
                    e.ctrlKey = false;
                    e.metaKey = false;
                    e.shiftKey = false;
                }
                var pos = coords.column;
                while (pos < options.dimensions.columns) {
                    var cell = data[getCellNameFromCoords(++pos, coords.row)];
                    if (cell && cell.master) {
                        pos += data[cell.master].span - 1;
                    }
                    var skipEmpty = cmdKey && isEmpty(data[getCellNameFromCoords(pos, coords.row)].value);
                    if (columnDefinitions[pos].visible && !skipEmpty) {
                        break;
                    }
                }
                if (pos < options.dimensions.columns) {
                    coords.column = pos;
                } else if (cmdKey) {
                    coords.column = options.dimensions.columns - 1;
                    while (!columnDefinitions[coords.column].visible) {
                        coords.column--;
                    }
                }
            } else if (e.keyCode == UP || e.keyCode == RETURN && e.shiftKey) {
                if (e.keyCode == RETURN) {
                    e.ctrlKey = false;
                    e.metaKey = false;
                    e.shiftKey = false;
                }
                var pos = coords.row - 1;
                var minPos = options.fixedPartsNotSelectable ? options.fixTop : 0;
                while (pos >= minPos && (!rowDefinitions[pos].visible || cmdKey && isEmpty(data[getCellNameFromCoords(coords.column, pos)].value))) {
                    pos--;
                }
                if (pos >= minPos) {
                    coords.row = pos;
                } else if (cmdKey) {
                    coords.row = minPos;
                    while (!rowDefinitions[coords.row].visible) {
                        coords.row++;
                    }
                }
            } else if (e.keyCode == DOWN || e.keyCode == RETURN) {
                if (e.keyCode == RETURN) {
                    e.ctrlKey = false;
                    e.metaKey = false;
                    e.shiftKey = false;
                }
                var pos = coords.row + 1;
                while (pos < options.dimensions.rows && (!rowDefinitions[pos].visible || cmdKey && isEmpty(data[getCellNameFromCoords(coords.column, pos)].value))) {
                    pos++;
                }
                if (pos < options.dimensions.rows) {
                    coords.row = pos;
                } else {
                    coords.row = options.dimensions.rows - 1;
                    while (!rowDefinitions[coords.row].visible) {
                        coords.row--;
                    }
                }
            } else {
                return;
            }

            leaveEditMode();

            if (coords) {
                var cellName = getCellNameFromCoords(coords);
                selection.end = data[cellName].master || cellName;
            }

            if (!e.shiftKey) {
                selection.start = selection.end;
            }

            selection = expandSelection(selection);
            showCell(selection.end);
            setSelection(selection);
            return false;
        }
    }

    /**
     * Takes a snapshot of the current spreadsheet data and selection and stores it on the
     * specified stack. A maximum of options.undoDepth snapshots is kept.
     * @param stack The stack to store the snapshot on
     */
    function prepareUndo(stack) {
        var cells = {};
        for (var key in data) {
            var cell = data[key];
            cells[key] = {
                value: cell.value,
                classValue: cell.classValue
            }
        }
        var undo = {
            selection: {
                start: selection.start,
                end: selection.end
            },
            data: cells
        };
        stack.push(undo);
        if (stack.length > options.undoDepth) {
            stack.shift();
        }
    }

    /**
     * Restores the spreadsheet to the topmost undo snapshot on the specified stack.
     * @param stack The stack to take the snapshot from
     */
    function executeUndo(stack) {
        if (stack.length) {
            var modified = [];
            var undo = stack.pop();
            var cells = undo.data;
            for (var key in cells) {
                var undoCell = cells[key];
                var cell = data[key];
                var value = undoCell.value;
                if (cell.value != value) {
                    modified.push(key);
                    cell.value = value;
                    if (isEmpty(value)) {
                        cell.cell.html('&nbsp;');
                    } else if (cell.html) {
                        cell.cell.html(value);
                    } else {
                        cell.cell.text(value);
                    }
                }

                var cls = undoCell.classValue;
                if (cell.classValue != cls) {
                    cell.classValue = cls;
                    cell.cell.get(0).className =  options.id + '-'
                        + columnDefinitions[cell.column].name
                        + (cell.readonly ? ' readonly' : '')
                        + (cls ? ' ' + cls : '');
                }
            }
            selection = undo.selection;
            setLayout();

            if (modified.length) {
                triggerOnModify(modified);
            }
        }
    }

    /**
     * Checks whether a value is undefined, null or the empty string.
     * @param value The value to be checked
     * @return True if the valus is empty
     */
    function isEmpty(value) {
        return value == undefined
            || value == null
            || value.toString().length == 0;
    }

    /**
     * Flattens an array with nested array elements.
     * @param array The array to be flattened
     * @param [data] Array to receive the flattened output
     * @return Flattened array
     */
    function flattenArray(array, data) {
        if (!data) {
            data = [];
        }
        var length = array.length;
        for (var i = 0; i < length; i++) {
            var elem = array[i];
            if (elem instanceof Array) {
                flattenArray(elem, data);
            } else {
                data.push(elem);
            }
        }
        return data;
    }

    /**
     * Inserts text at the current position in a textarea or input field.
     * @param field Textarea or input field
     * @param text Text to be inserted
     */
    function insertAtCursor(field, text) {
        field = $(field).get(0);
        field.focus();
        if (document.selection) { // Handle Internet Explorer
            var sel = document.selection.createRange();
            sel.text = text;
            sel.select();
        } else if (field.selectionStart || field.selectionStart == '0') { // Handle Firefox, Chrome, Safari and Opera
            var startPos = field.selectionStart;
            var endPos = field.selectionEnd;
            field.value = field.value.substring(0, startPos) + text + field.value.substring(endPos, field.value.length);
            var length = endPos + text.length - ($.browser.opera ? 0 : 1);
            field.setSelectionRange(length, length);
        } else { // Just append the text in an unsupported browser
            field.value += text;
        }
    }
}

/*===== Debugging functions =====================================================================*/

///#DEBUG
var timer;
function startTimer() {
    timer = new Date().getTime();
}

function endTimer(info) {
    var end = new Date().getTime();
    var time = end - timer;
    if (info) {
        console.log(info + ': ' + time);
    } else {
        console.log(time);
    }
    timer = end;
}
///#ENDDEBUG