(function ($, window, document, undefineid) {
	
	var Methods = {
		
		// initialisation
		init: function(options, elem) {
			
			var self = this;
			self.elem = elem;
			self.$elem = $(elem);
			self.url = ( typeof options === 'string' ) ? options: options.url;
			self.options = $.extend( {}, $.fn.contentLoader.options, options );
			 
			// placeholders
			self.create();
			
			// bind filter events
			if (this.options.filters) {
				$(this.options.filters).delegate('.reloader', 'change', function() {
					$('input.page', self.elem).val(1);
					self.load();
				});
			}

			// dataloader
			return self.load();
		},
		
		// content loader
		load: function(self) { 
			
			var self = this; 
			
			// splash screen
			self.splash(self, true);
			
			// before statment 
			self.before(self);
			
			self.fetch().done(function(response) {
				
				// close splash screen
				self.splash(self);
				
				// display content
				self.display(response);
				
				// before statment 
				self.after(self);
				
				// trigger browser window
				$(window).trigger('resize');
			});
			
			return self;
		},
		
		// fetch data
		fetch: function() {
			
			var self = this;
			
			// include filters vales
			if(self.options.filters) {
				$.each($(self.options.filters).serializeArray(), function(i, field){
					self.options.data[field.name] = field.value;
				});
			}
			
			return $.ajax({
				url: self.url,
				data: $.extend({}, self.options.data, self.options.parameters)
			});
		},
		
		// show content
		display: function(results) {
			
			$('.'+this.options.container,this.elem).html(results)
			
			return self;
		},
		
		// splash screen loader
		splash: function(self, show) {
			
			if (self.options.splash) { 
				
				var splashContainer = $('.'+self.options.splash, self.$elem);
				var splashImage = $('.wait', self.$elem);
				
				splashContainer.hide().removeAttr('stype');
				
				if (show) {
					
					var position = self.$elem.offset();
					var width = self.$elem.width() || 800;
					var height = self.$elem.height() || 902;
					
					splashContainer
					.css({
						top: position.top, 
						left: position.left,
						width: width,
						height: height
					}).show();	
					
					splashImage.removeAttr('stype')
					.css({
						top: (height / 2) - (splashImage.height()/2), 
						left: (width / 2) - (splashImage.width()/2)
					});
				}
			}
		},
		
		// before statment
		before: function(self) {
			
			if (self.options.before) {
				self.options.before(self.elem);
			}
		},
		
		// after statment
		after: function(self) {
			
			// pager
			$('.table-controls li', self.elem).click(function() {
				self.options.parameters.page = $(this).attr('alt') || 1;
				self.load();
			});
			
			// sorter
			$('th strong', self.elem).click(function() {
				
				if ($(this).hasClass("sort")) {
			  		
					var sort = $(this).parent().attr('class');
			  		var direction = ($(this).hasClass("asc")) ? "desc" : "asc";
			  		
			  		$('th strong', self.elem).removeClass("asc").removeClass("desc");
			  		$(this).addClass(direction);
			  		
			  		self.options.parameters.sort = sort;
			  		self.options.parameters.direction = direction;
					self.load();
			  	}
			})
			
			if (self.options.after) {
				self.options.after(self.elem);
			}
		},
		
		// create placeholders
		create: function() {
			
			// content wrapper
			this.$elem.append('<div class="xhr-placeholder '+this.options.container+'"></div>');
			
			// splash screen
			if (this.options.splash) {
				this.$elem.append('<div class="'+this.options.splash+'"><img class="wait" src="'+this.options.splashImage+'" /></div>');
			}
		},
		
		// destroy
		destroy: function() {
			
		}
	}
	
	$.fn.contentLoader = function(method) {
		
		if (Methods[method] ) { 
			return Methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return Methods.init( method, this );
		} else {
			$.error( 'Method ' +  method + ' does not exist' );
		}	
	}
	
	$.fn.contentLoader.options = {
		container : 'table-container',
		filters: '#filters',
		splash : 'ajax-splash',
		url: "ajax.content.php",
		splashImage: "/public/images/loader-quer.gif",
		parameters : {},
		data : {},
		before : null,
		after : null
	};
	
})( jQuery, window, document );