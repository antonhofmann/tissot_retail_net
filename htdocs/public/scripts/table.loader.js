(function( $ ) {
 
	$.fn.tableLoader = function( options ) {
 		
		this.showPage = function(page) { 
			
			var data = {}
			
			if (page) {
				$('input[name=page]').val(page);
			}
			
			if (settings.before != null)  {
				settings.before( self, settings );
			}
				
			if (settings.loader.show) {
				this.loader(true);
			}
			
			if (settings.data) {
				$.each( settings.data, function(i, field) {
					data[field.name] = field.value;
				});
			}
			
			if (settings.filters) {
				$.each(settings.filters.serializeArray(), function(i, field) {
					data[field.name] = field.value;
				});
			}
			
			$.each($('form', self).serializeArray(), function(i, field) {
				data[field.name] = field.value;
			});
			
			$.post(settings.url, data, function(response) { 
				$(container).html(response); 
				self.loader(false);
				self.wrapper();	
				$("body").animate({ scrollTop: 0 }, "fast");			
				if (settings.after != null) {
					settings.after(self, page);
				}
			});
		};
		
		this.wrapper = function() { 
			$(window).trigger('resize');
		}
 
		this.loader = function(show) {
			
			var image = $('img', $(loader));
			$(loader).hide().removeAttr('style');
			image.removeAttr('style');
			
			if (show) {
				var position = $(self).offset();
				var width = $(self).width() || 800;
				var height = $(self).height() || 800;
				$(loader).css({
					top: position.top, 
					left: position.left,
					width: width,
					height: height
				}).show();
				
	  			image.css({
					top: (height / 2) - (image.height()/2), 
					left: (width / 2) - (image.width()/2)
				});	
			}
		};
		
		var settings = jQuery.extend({
			before : null,
			after : null,
			url: "ajax.php",
			parameters : {},
			container : 'table-container',
			loader : {
				show: true,
				container: 'loader-container',
				image: '/public/images/loader-quer.gif'
			}
		}, options );
		
		var self = this;
		var instance, search;
		
		if (!instance) {
			
			instance = self.attr('id');
			
			$(self).wrapInner("<div class='xhr-placeholder' />");
			$('.xhr-placeholder',self).append('<div class="'+settings.container+'"></div>');
			$('.xhr-placeholder',self).append('<div class="'+settings.loader.container+'"><img src="'+settings.loader.image+'" /></div>');
			
			var container = $('.'+settings.container, self).selector;
			var loader = $('.'+settings.loader.container, self).selector;
			self.showPage(1);
			
			var search = $('input[name=search]', self).selector;
		}
		
		$(self).delegate('input.submit', 'click', function() {
			
			var value = $(this).attr('value');
			var parent = $(this).closest('.searchbox');
	  		
	  		if (value=='Search') {
	  			$(this).val('').attr('title', value);
	  		}
	  		
	  		if ($(this).attr('name')=='search') {
	  			$(this).addClass('active');
	  			$('.close', parent).toggle();
	  		}
		});
		
		$(self).delegate('input.submit', 'blur', function(event) {
			
			var value = $(this).val();
			var parent = $(this).closest('.searchbox');
			
			if (value) { 		
		  		if ($(this).attr('name')=='search') {
		  			$('.close', parent).toggle();
		  		}
		  	} else {
		  		$(this).removeClass('active');
		  		$(this).val($(this).attr('title'));
		  	}
		});
		
		$(self).delegate('input.submit', 'keypress', function(event) {
			if (event.keyCode==13) {
				$(this).trigger('change');
				return event.keyCode != 13;
			}
		});
		
		$(self).delegate('input.submit', 'change', function() {
			//var value = $(this).val();
		  	//var caption = $(this).attr('title');
		  	//$(this).val(value);
			self.showPage(1);
		});
		
		
		$(self).delegate('select.submit', 'change', function() {
			self.showPage(1);
		})

		$(self).delegate('select.dropdown-rows', 'change', function() { 
			$('#rows_per_page', self).val($(this).val()).trigger('change');
		})
		
		$(self).delegate('.table-controls li', 'click', function() {
			var page = $(this).attr('alt') || 1;
			self.showPage(page);
		});
		
		$(self).delegate('th strong', 'click', function() {
			if ($(this).hasClass("sort")) {
		  		var field = $(this).parent().attr('class');
		  		var direction = ($(this).hasClass("asc")) ? "desc" : "asc";
		  		$('th strong', self).removeClass("asc").removeClass("desc");
		  		$(this).addClass(direction);
		  		$('#sort', self).val(field);
		  		$('#direction', self).val(direction);
		  		self.showPage(1);
		  	}
		})
		
		$(self).delegate('form', 'submit', function(event) {
			event.preventDefault();
		});
		
		$(document).delegate('.icon.close', 'click', function() {
			$(this).toggle();
			$(search).val('');
			self.showPage(1);
		});
		
		return self;
	};
 
})( jQuery );