<?php

	require_once $_SERVER['DOCUMENT_ROOT'].'/_framework/header.inc.php';
	
	// db connector
	$db = db::getInstance();

	$data = array();

	switch ($_REQUEST['section']) {
		
		case 'provinces':
			
			$provinces = $db->getTableContent("
				SELECT province_id, province_canton
				FROM provinces
				WHERE province_country = ".$_REQUEST['value']."
				ORDER BY province_canton
			");
			
			if ($provinces) {
				
				$data[][0] = "Select";
				
				foreach ($provinces as $row) {
					list($value,$name) = array_values($row);
					$data[][$value] = $name;
				}
			}
			
			$empty = ($_REQUEST['value']) ? "No Provinces" : "Select";
			$data = ($data) ? $data : array(array('0' => $empty));
		
		break;
	
		case 'places':
			
			$places = $db->getTableContent("
				SELECT place_id, place_name
				FROM places
				WHERE place_province = ".$_REQUEST['value']."
				ORDER BY place_name
			");
			
			if ($places) {
				
				$data[][0] = "Select";
				
				foreach ($places as $row) {
					list($value,$name) = array_values($row);
					$data[][$value] = $name;
				}
			}
			
			$empty = ($_REQUEST['value']) ? "No Places" : "Select";
			$data = ($data) ? $data : array(array('0' => $empty));
			
		break;
	}
	
	echo json_encode($data);
