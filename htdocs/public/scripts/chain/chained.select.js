jQuery.fn.chainedSelect= function( target, settings ) {

  	return $(this).change(function() { 
		
		settings = jQuery.extend({
			after : null,
			before : null,
			url: "/applications/helpers/ajax.dropdown.php",
			current : null,
			parameters : {} 
		}, settings );

		if (!$(target).is('select')) {
			return false;
		}

		// set status
		settings.status = true;

		// current selected value
		settings.parameters.value = $('option:selected', this).val();
		
		// loader section
		// if is not defined set section as target name
		settings.parameters.section =  settings.parameters.section || target.substring(1);
		
		// current dropdown value
		// if is not defined set current value from title attribute
		settings.current = $(target).attr('title') || settings.current;
		settings.parameters.current = settings.current;

		// assign before statements
		if (settings.before != null)  {
			settings.before( target, settings );
		}
		
		// loader
		if (settings.status) {
			$.ajax({
				url: settings.url,
				dataType: 'json',
				cache: false,
				data: settings.parameters,
				success: function(json) { 

					// remove options
					$(target).empty();
					
					if (json) {
						
						// parse json object to dropdown options
						for (i = 0; i < json.length; i++) {  
							for ( key in json[i] ) {	
								$(target).append("<option value="+key+">"+json[i][key]+"</option>");
							}
						}
					}			
					
					// set selected value
					if (settings.current != null) {
						$(target).val(settings.current);
					}
			
					// assign afeter statements
					if (settings.after != null) {
						settings.after(target);
					}
			
					// trigger to change
					$(target).change();
	        	}
			});
		}
	});
};