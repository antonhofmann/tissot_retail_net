(function( $ ){
	
	var popover = {
		
		init : function( options ) {
			
			var docWidth = $(document).width(),
				position = 'bottom',
				direction = 'right';
			
			var settings = jQuery.extend($.fn.pop.settings, options);
			
			return this.each(function() {
				
				var offset = $(this).position(),
					width = $(this).outerWidth(true),
					height = $(this).outerHeight(true),
					container = $($(this).attr('data')),
					positions = {};

				if (options && options.position) {
					var data = options.position.split(' ');
					direction = ($.inArray('left', data) > -1) ? 'left' : 'right'; 
					position = ($.inArray('top', data) > -1) ? 'top' : 'bottom';
				}
				
				if (position == 'top') {
					if (direction == 'left') {
						positions.left = offset.left;
						positions.top = offset.top-height-1;
					} else {
						positions.right = offset.left+10;
						positions.top = offset.top-height-1;
					}
				} else {
					if (direction == 'left') {
						positions.left = offset.left-width+12;
						positions.top = offset.top+height-2;
					} else {
						positions.left = offset.left;
						positions.top = offset.top+height-2;
					}
				}
				
				if (positions) {
					container.css(positions)
				}
				
				$(this).addClass('pop'+' pop-'+position+' pop-'+direction);
				container.hide().addClass('popover pop-'+position+' pop-'+direction);
				
				$(this).click(function(event) { 
					event.preventDefault();
					popover.toggler(this);
					return this;
				});
			});
		},
		toggler : function(elem) {
			$(elem).toggleClass('pop-active');
			$($(elem).attr('data')).slideToggle('fast').toggleClass('pop-active');
		},
		show : function(elem) { 
			$(elem).addClass('pop-active');
			$($(elem).attr('data')).slideDown('fast').addClass('pop-active');
		},
		hide : function( ) {
			this.each(function(elem) {
				$(this).removeClass('pop-active');
				$($(this).attr('data')).slideUp('fast').removeClass('pop-active');
				$('.icon',this).removeClass('direction-up');
			});
		},
		destroy : function() {
			return this.each(function(elem){
				$(window).unbind(elem);
			})
		}
	};
	
	$.fn.pop = function( method ) { 
		
		if ( popover[method] ) {
			return popover[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return popover.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on popover plugin' );
		} 
	};
	
	$.fn.pop.settings = {
		showOnlyOnePop: true
	};
	
	$(document).bind('click', function(event) { 
		
		var clicked = $(event.target); 
		
		if (!clicked.hasClass('pop') 
			&& !clicked.parents().hasClass('pop') 
			&& !clicked.hasClass('popover') 
			&& !clicked.parents().hasClass('popover')
		) { 
			$('.pop').each(function(elem) {
				$(this).removeClass('pop-active');
				$($(this).attr('data')).slideUp('fast').removeClass('pop-active');
				$('.icon',this).removeClass('direction-up');
			});
		}
	});
	
})( jQuery );