/**
 * jQuery AdoModal @admirserifi
 *
 * Sample usage:
 * Set the message per instance:
 * $('#elem').plugin({ message: 'Goodbye World!'}); or
 * var p = new Plugin(document.getElementById('elem'),
 * { message: 'Goodbye World!'}).init() or, 
 * set the global default message:
 * Plugin.defaults.message = 'Goodbye World!'
 */

;(function( $, window, document, undefined ){

	// our plugin constructor
	var Modal = function( elem, options ){
		
		this.elem = elem;
		this.$elem = $(elem);

		this.id = this.$elem.length ? this.$elem.get(0).id : '';
		
		// set options
		if ( typeof options === 'object' ) this.options = options;
		else this.options = {button: options};

		// show button
		this.button = $(this.options.button);
		
		// hide button
		this.closeButton = this.options.closeButton ? $(this.options.closeButton) : $('.ado-modal-close', this.$elem);

		// This next line takes advantage of HTML5 data attributes
		// to support customization of the plugin on a per-element basis.
		// For example: <div class=item' data-plugin-options='{"message":"Goodbye World!"}'></div>
		this.metadata = this.$elem.data( "plugin-options" );
	};

	// the plugin prototype
	Modal.prototype = {

		defaults: {
			top: 0,
			left: 0,
			//width: 500,
			height: null,
			closeButton: null,
			clickClose: true,
			overlay: {
				opacity: 0.75,
				background: '#000'
			},
			beforOpen: function() {},
			afterOpen: function() {},
			beforClose: function()  {},
			afterClose: function()  {}
		},

		/**
		 * Modal initialization
		 * @return Modal
		 */
		init: function() { 

			var self = this;

			// Introduce defaults that can be extended either
			// globally or using an object literal.
			this.config = $.extend({}, this.defaults, this.options, this.metadata);

			this.config.width = this.config.width || this.$elem.outerWidth();
			this.config.height = this.config.height || this.$elem.outerHeight();

			// set modal container
			this.$elem.css({
				display: 'none',
				maxHeight: this.config.maxHeight
			});
			
			// set overlay
			$('body').append("<div id=\"overlay-"+this.id+"\" class=\"ado-modal-overlay\"></div>");
			this.zIndex = Number($('.ado-modal-overlay').length)+1100;
			this.$overlay = $('#overlay-'+this.id).css('z-index', this.zIndex);

			// resize container
			$(window).resize(function(e) {
				e.stopPropagation();
				self.resize();
			}).trigger('resize');

			// show button events onclick
			this.button.on('click', function(e) {
				e.preventDefault();
				self.show();
			});

			// close button event onclick
			var closeButton = this.closeButton || $('.ado-modal-close', self);
			closeButton.on('click', function(e) {
				e.preventDefault();
				self.close();
			});

			// click close on overlay
			if (this.config.clickClose) {
				this.$overlay.on('click', function(e) { 
					self.close();                   
				});
			}
			
			return this;
		},

		/**
		* Show Modal
		* @return void
		*/
		show: function() {

			this.config.beforOpen(this.$elem);
			
			this.$overlay.css({ 
				display: 'block', 
				opacity: 0 
			}).fadeTo(200, this.config.overlay.opacity);
			
			this.$elem.show();
			this.config.afterOpen(this.$elem);
			$("body").css({ overflow: 'hidden' })

			return this;
		},

		/**
		 * Modal container resizer
		 * @return void
		 */
		resize: function(opt) {

			var opt = opt || {};
			var width = opt.width || this.config.width;
			var height = opt.height || this.config.height;
			var windowWidth = $(window).width(); 
			var windowHeight = $(window).height();

			width = width>=windowWidth ? windowWidth-40 : width;
			height = height>=windowHeight ? windowHeight-40 : height;

			// container
			this.$elem.css({ 
				position : 'fixed',
				width: width,
				height: height,
				zIndex: this.zIndex+1,
				top: this.config.top ? this.config.top+'px' : '50%',
				marginTop: this.config.top ? 0 : -(height/2)+'px',
				left: this.config.left ? this.config.left+'px' : '50%',
				marginLeft: this.config.left ? 0 : -(width/2)+'px'
			});	

			if (this.$elem.is(':visible')) {
				$("body").css({ overflow: 'hidden' });
			}

			// content
			var headerHeight = $('.ado-modal-header', this.$elem).actual( 'outerHeight' );
			var footerHeight = $('.ado-modal-footer', this.$elem).actual( 'outerHeight' );
			var bodyHeight = height-headerHeight-footerHeight;
			
			bodyHeight = bodyHeight > 0 ? bodyHeight : 'auto';
			
			$('.ado-modal-body', this.$elem).css({
				width: width-2,
				height: bodyHeight
			});

			return this;	
		},

		/**
		* Close Modal
		* @return void
		*/
		close: function() {
			this.config.beforClose(this.$elem);
			this.$overlay.fadeOut(200);
			this.$elem.hide();
			this.config.afterClose(this.$elem);
			$("body").css({ overflow: 'auto' });
			return this;
		}
	}

	Modal.defaults = Modal.prototype.defaults;

	$.fn.adoModal = function(options) {
		return new Modal(this, options).init();
	};

})( jQuery, window , document );
