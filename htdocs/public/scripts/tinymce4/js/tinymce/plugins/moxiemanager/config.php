<?php
	// General
	$moxieManagerConfig['general.license'] = 'F2RC-HNTT-VPNY-AHLT-WXKO-AFUJ-IUSU-OG4N';
	$moxieManagerConfig['general.hidden_tools'] = '';
	$moxieManagerConfig['general.disabled_tools'] = '';
	$moxieManagerConfig['general.plugins'] = 'Favorites,History,Uploaded,AutoRename';
	$moxieManagerConfig['general.plugins'] = '';
	$moxieManagerConfig['general.demo'] = false;
	$moxieManagerConfig['general.debug'] = false;
	$moxieManagerConfig['general.language'] = 'en';
	$moxieManagerConfig['general.temp_dir'] = $_SERVER['DOCUMENT_ROOT'].'/public/data/tmp';
	$moxieManagerConfig['general.allow_override'] = 'hidden_tools,disabled_tools';
	
	// Filesystem
	$moxieManagerConfig['filesystem.rootpath'] = $_SERVER['DOCUMENT_ROOT'].'/public/data/files/help';
	$moxieManagerConfig['filesystem.extensions'] = 'jpg,jpeg,png,gif,html,htm,txt,docx,doc,zip,pdf,xls,xlsx';
	$moxieManagerConfig['filesystem.include_directory_pattern'] = '';
	$moxieManagerConfig['filesystem.exclude_directory_pattern'] = '/^thumbs$/i';
	$moxieManagerConfig['filesystem.include_file_pattern'] = '';
	$moxieManagerConfig['filesystem.exclude_file_pattern'] = '';
	$moxieManagerConfig['filesystem.readable'] = true;
	$moxieManagerConfig['filesystem.writable'] = true;
	$moxieManagerConfig['filesystem.allow_override'] = '*';
	$moxieManagerConfig['autorename.enabled'] = true;
	$moxieManagerConfig['autorename.lowercase'] = true;
	$moxieManagerConfig['autorename.space'] = "_";

	$moxieManagerConfig['thumbnail.folder'] = "thumbs";
	$moxieManagerConfig['thumbnail.enabled'] = true;
	$moxieManagerConfig['thumbnail.auto_generate'] = true;
	$moxieManagerConfig["thumbnail.prefix"] = "";
	$moxieManagerConfig['thumbnail.width'] = 150;
	$moxieManagerConfig['thumbnail.height'] = 150;

	// Upload
	$moxieManagerConfig['upload.include_file_pattern'] = '';
	$moxieManagerConfig['upload.exclude_file_pattern'] = '';
	$moxieManagerConfig['upload.extensions'] = '*';
	$moxieManagerConfig['upload.maxsize'] = '100MB';
	$moxieManagerConfig['upload.overwrite'] = false;
	$moxieManagerConfig['upload.autoresize'] = false;
	$moxieManagerConfig['upload.autoresize_jpeg_quality'] = 90;
	$moxieManagerConfig['upload.chunk_size'] = '5mb';
	$moxieManagerConfig['upload.allow_override'] = '*'; 
?>	