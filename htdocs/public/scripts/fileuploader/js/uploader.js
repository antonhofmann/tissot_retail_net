$(document).ready(function() {

	var UploadForm = $('#fileupload'),
		Upload_Dir = $('.upload_dir', UploadForm).val(),
		extensions = $('#allowed_extensions', UploadForm).val(),
		application = $('.application', UploadForm).val(),
		params = $('.params', UploadForm).val(),
		id = $('.id', UploadForm).val(),
		acceptFileTypes;
	
	
	// loader initialise
	retailnet.loader.init();

/*
	// Initialize the jQuery File Upload widget:
	UploadForm.fileupload({
        url: '/applications/helpers/file.uploader.php',
        sequentialUploads: true,
        autoUpload: true
	});
	
	// file type extensions
	if (extensions) {
		var exts = extensions.split(/[\s,]+/);
		UploadForm.fileupload('option', 'acceptFileTypes', new RegExp('(\.|\/)('+exts.join('|')+')$') );
	}
	
	// extend upload options
	if (params) {
		var options = JSON.parse(params);
		UploadForm.fileupload('option', options);
	}
	
	
	// delete file
	UploadForm.bind('fileuploaddestroy', function (e, data) {
	    
		if (data && data.reload) {
			window.location.reload();
		}
		
		if (data && data.notification) {
			retailnet.notification.show(data.notification.content, eval(data.notification.properties));
		}
	});
    
    // Enable iframe cross-domain access via redirect option:
	UploadForm.fileupload('option', 'redirect', window.location.href.replace(/\/[^\/]*$/, '/public/scripts/fileuploader/cors/result.html?%s'));

    
	UploadForm.addClass('fileupload-processing');

   
	// Load existing files:
    $.ajax({
    	url: UploadForm.attr('action'),
    	dataType: 'json',
    	context: $('#fileupload')[0],
    	data: {
    		section: 'load',
    		application: application,
    		id: id
    	}
    }).always(function () {
        $(this).removeClass('fileupload-processing');
    }).done(function (result) {
        $(this).fileupload('option', 'done').call(this, $.Event('done'), {result: result});        
    });
*/
	
	// show modal screen
	$('.upload-modal').click(function(e) {
		
		e.preventDefault();
		e.stopImmediatePropagation();

		var self = $(this);
		var target = self.attr('href');
		var options = {};
		
		if (self.attr('modal-width') && self.attr('modal-height')) {
			options.autoDimensions = false;
			options.width = Number(self.attr('modal-width'));
			options.height = Number(self.attr('modal-height'));
		}
		
		// reload page on close
		options.onClosed = function() {
			window.location.reload();
		}
		
		if ($('tbody.files tr.template-download', $(target)).length == 0 ) {
			$('button.delete', $(target)).hide();
		}
		
		retailnet.modal.show(target, options);

		return false;

	});
	
	// close modal screen
	$('.modal-close', UploadForm).click(function(e) {
		
		e.preventDefault();
		e.stopImmediatePropagation();
		
		retailnet.modal.hide();
		
		UploadForm[0].reset();
		
		return false;
	});
	
	// start upload files
	$('button.uploader', UploadForm).click(function(e) {
		
		e.preventDefault();
		e.stopImmediatePropagation();
		
		retailnet.notification.hide();
    	
    	var required = $('input.required', UploadForm);
    	var failures = $('input.required[value=]', UploadForm);
    	
    	$('input.required', UploadForm).removeClass('has-error');
   
    	
    	if (required.length > 0 && failures.length > 0) {
    		
    		$('input.required[value=]', UploadForm).addClass('has-error');
    		
    		retailnet.notification.show('Red marked fields are required.', {
    			sticky: true,
    			theme: 'error'
    		});
    		
    	} else {	
    		
    		var data = UploadForm.serializeArray();
			data.push({name: 'section', value: 'save'});
			
			retailnet.loader.show();
    		
    		retailnet.ajax.json(UploadForm.attr('action'), data).done(function(xhr) {
    			
	    		retailnet.loader.hide();
	    		
	    		if (xhr && xhr.reload) {
					window.location.reload();
	        	}
	    		
				if (xhr && xhr.notification) {
					retailnet.notification.show(xhr.notification.content, eval(xhr.notification.properties));
				}
	        });
    	}

		return false;
	});

});