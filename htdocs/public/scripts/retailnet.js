
var retailnet = {
		
	loader: {

		indicators: {
			throbber: 'throbber', 
			snike: 'snike', 
			referesh: 'referesh', 
			spin: 'spin', 
			fullspin: 'fullspin', 
			twist: 'twist', 
			dotter: 'dotter', 
			revolve: 'revolve'
		},

		init: function(name) {
			
			if (!$( "#load-indicator-overlay" ).length) {
				name = name && this.indicators[name] ? name : 'fullspin';
				var $container = $('<div />').appendTo('body');
				$container.attr('id', 'load-indicator-overlay').append('<div id="load-inidcator-container" class="'+ name +'" ><div class="load-indicator">Loading...</div></div><');
			}
		},
		
		show: function(name) {
			
			this.init();

			if (name && this.indicators[name]) {
				$('#load-inidcator-container').removeAttr('class').addClass(name);
			}

			$( "#load-indicator-overlay" ).show();
		},
		
		hide: function() {
			$( "#load-indicator-overlay" ).hide();
		}
	},
	
	modal: {
		
		show: function(target, options) {
			
			var settings = $.extend({ 
				href: target,
				autoCenter: true,
				closeBtn: true,
				padding : 0,
				margin : 0
			}, options);
			
			if (target) {
				$.fancybox(settings);
			}
		},
		
		dialog: function(target, options) {
			
			var settings = $.extend({ 
				href: target,
				autoCenter: true,
				padding : 0,
				margin : 0,
				closeBtn: false,
				modal: true,
				scrolling: 'no',
				onUpdate    : function(){
		            $.fancybox.update();
		        }
			}, options);
			
			if (target) {
				$.fancybox(settings);
			}
		},
		
		hide: function() {
			$.fancybox.close();
		},
		
		close: function() {
			$.fancybox.close();
		},

		box: function(button, options) {

			if (jQuery().leanModal) {
				$(button).leanModal(options);
			}
		},

		boxClose: function(target) {

			if (jQuery().leanModal) {
				$.leanModal.close(target);
			}
		}
	},
	
	notification: {
		
		success: function(message) {
			$.jGrowl(message, { 
				sticky: false, 
				theme: 'message'
			});	
		},
		warning: function(message) {
			$.jGrowl(message, { 
				sticky: false, 
				theme: 'warning'
			});	
		},
		error: function(message) {
			$.jGrowl(message, { 
				sticky: true, 
				theme: 'error'
			});	
		},
		show: function(message, properties) {
			$.jGrowl(message, properties);	
		},
		hide: function() {
			$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
		}
	},
	
	ajax: {
		json: function(url, data) {
			return $.ajax({
				url: url,
				type: 'post',
				dataType: 'json',
				data: data,
				async: true,
				cache: false
		    });
		},
		request: function(url, data) {
			
			var response;
			
			$.ajax({
				url: url,
				type: 'post',
				dataType: 'json',
				data: data,
				async: false,
				cache: false,
		        success: function(xhr) { 
		        	response = xhr;           
		        }
		    });
			
			return response;
		}
	},
	
	tooltip: {
		
		init: function(e, settings) {
			
			e = e || $('.-tooltip');
			
			e.qtip({style: {classes:'qtip qtip-dark' }});
		},
		
		show: function() {
			
		}
	},
	
	is: {
		
		image: function(path) {
			if (path) {
				
				var images = ['jpg','jpeg','gif','png'];
				var extension =  path.split('.').pop();
				
				return (retailnet.map.find(extension, images)) ? true : false;
			}
		},
		
		doc: function(path) {
			
			if (path) {
				
				var images = ['txt','doc','pdf'];
				var extension =  path.split('.').pop();
				
				return (retailnet.map.find(extension, images)) ? true : false;
			}
		},
		
		date: function(s) {
		    
			// format D(D)/M(M)/(YY)YY
		    var dateFormat = /^\d{1,4}[\.|\/|-]\d{1,2}[\.|\/|-]\d{4}$/;
	
		    if (dateFormat.test(s)) {
		        // remove any leading zeros from date values
		        s = s.replace(/0*(\d*)/gi,"$1");
		        var dateArray = s.split(/[\.|\/|-]/);
	
		        // correct month value
		        dateArray[1] = dateArray[1]-1;
	
		        // correct year value
		        if (dateArray[2].length<4) {
		            // correct year value
		            dateArray[2] = (parseInt(dateArray[2]) < 50) ? 2000 + parseInt(dateArray[2]) : 1900 + parseInt(dateArray[2]);
		        }
	
		        var testDate = new Date(dateArray[2], dateArray[1], dateArray[0]);
		        if (testDate.getDate()!=dateArray[0] || testDate.getMonth()!=dateArray[1] || testDate.getFullYear()!=dateArray[2]) {
		            return false;
		        } else {
		            return true;
		        }
		    } else {
		        return false;
		    }
		}
	},
	
	map: {
	
		compare: function(a1, a2) {
		    
			if (a1.length != a2.length) return false;
		    
			var length = a2.length;
		    
		    for (var i = 0; i < length; i++) {
		        if (a1[i] !== a2[i]) return false;
		    }
		    
		    return true;
		},
	
		remove: function(array,item){
		    
			for(var i in array){
		        
				if(array[i]==item){
		            array.splice(i,1);
		            break;
		        }
		    }
		},
	
		find: function(needle, haystack) {
		    
			var length = haystack.length;
		    
		    for(var i = 0; i < length; i++) {
		        
		    	if(typeof haystack[i] == 'object') {
		            if(retailnet.map.compare(haystack[i], needle)) return true;
		        } else {
		           
		        	if(haystack[i] == needle) return true;
		        }
		    }
		    
		    return false;
		}
	},
	
	json: {
		
		normalizer: function(data) {
			
			if (typeof data === 'object') {
				
				var response = {};
				
				for (var key in data) {
					   
					var obj = data[key];
					   
					if (typeof obj === 'object') { 

						for (var j in obj) {
							
							var value = obj[j];
							
							if (/^function\([^)]*\)\s*{/.test(value)) {
								value = Function('return ' + value)();
							}
							
							if (j=='readonly') {
								value = value=='true' ? true : false;
							}
							
							obj[j] = value;
						}
												
					} else {
						
						if (/^function\([^)]*\)\s*{/.test(obj)) {
							obj = Function('return ' + obj)();
						}
						
						if (key=='readonly') {
							obj = obj=='true' ? true : false;
						}
					}

					response[key] = obj;
				}
				
				return response;
			}
		}
	}
}
