if (!Adomat) var Adomat = {};

Adomat.Notification = {
	
	settings: {
		type: 'notice',
		styling: 'bootstrap3', 
		icon: false,
		history: false,
		delay: 5000
	},

	init: function(settings) {
		this.settings = $.extend(this.settings,settings);
	},

	notice: function(text, settings) {
		new PNotify($.extend(this.settings, {
			type: 'notice',
			text: text
		}, settings));
	},

	warning: function(text, settings) {
		new PNotify($.extend(this.settings, {
			type: 'info',
			text: text
		}, settings));
	},

	info: function(text, settings) {
		new PNotify($.extend(this.settings, {
			type: 'info',
			text: text
		}, settings));
	},

	success: function(text, settings) { 
		new PNotify($.extend(this.settings, {
			type: 'success',
			text: text
		}, settings));
	},	

	error: function(text, settings) { 
		new PNotify($.extend(this.settings, {
			type: 'error',
			text: text
		}, settings));
	},

	show: function(settings) {
		new PNotify($.extend(this.settings, settings));
	},
	
	close: function() {
		PNotify.removeAll();
	}
}