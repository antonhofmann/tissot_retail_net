if (!Adomat) var Adomat = {};

Adomat.Spinner = {

	THEME_DARK: 'sk-spinner-theme-dark',

	show: function(el, name, theme, delay) {

		var $this = this;

		name = name || 'fading-circle';
		theme = theme || this.THEME_DARK;

		switch (name) {

			case 'rotating-plane': 
				return $this.rotatingPlane(el, theme, delay);
			break;

			case 'double-bounce':
				return $this.doubleBounce(el, theme, delay);
			break;

			case 'wave':
				return $this.wave(el, theme, delay);
			break;

			case 'wandering-cubes':
				return $this.wanderingCubes(el, theme, delay);
			break;

			case 'pulse':
				return $this.pulse(el, theme, delay);
			break;

			case 'chasing-dots':
				return $this.chasingDots(el, theme, delay);
			break;

			case 'three-bounce':
				return $this.threeBounce(el, theme, delay);
			break;

			case 'circle':
				return $this.circle(el, theme, delay);
			break;

			case 'fading-circle':
				return $this.fadingCircle(el, theme, delay);
			break;

			case 'cube':
				return $this.cube(el, theme, delay);
			break;

			case 'wordpress':
				return $this.wordpress(el, theme, delay);
			break;
		}
	},

	remove: function(el) {
		
		var spinner = el && el.length > 0 ? $('.sk-spinner-container', el) : $('.sk-spinner-container');
		
		spinner.remove();
		spinner.parent().removeClass('sk-spinner-holder');
	},

	rotatingPlane: function(el, theme, delay) {

		var self = this,
			theme = theme || this.THEME_DARK,
			parent = el && el.length ? el : $('body');
		
		var spinner = $("\
			<div class=\"sk-spinner-container "+theme+"\">\
				<div class=\"sk-spinner-box\">\
					<div class=\"sk-spinner sk-spinner-rotating-plane\"></div>\
				</div>\
			</div>\
		");

		spinner.appendTo(parent);

		if (delay > 0) {
			setTimeout(function() { 
				self.remove(parent);
			}, delay);
		}

		return self;
	},

	doubleBounce: function(el, theme, delay) {

		var self = this,
			theme = theme || this.THEME_DARK,
			parent = el && el.length ? el : $('body');
		
		var spinner = $("\
			<div class=\"sk-spinner-container "+theme+"\">\
				<div class=\"sk-spinner-box\">\
					<div class=\"sk-spinner sk-spinner-double-bounce\">\
						<div class=\"sk-double-bounce1\"></div>\
						<div class=\"sk-double-bounce2\"></div>\
					</div>\
				</div>\
			</div>\
		");

		spinner.appendTo(parent);

		if (delay > 0) {
			setTimeout(function() { 
				self.remove(parent);
			}, delay);
		}

		return self;
	},

	wave: function(el, theme, delay) {

		var self = this,
			theme = theme || this.THEME_DARK,
			parent = el && el.length ? el : $('body');
		
		var spinner = $("\
			<div class=\"sk-spinner-container "+theme+" \">\
				<div class=\"sk-spinner-box\">\
					<div class=\"sk-spinner sk-spinner-wave\">\
						<div class=\"sk-rect1\"></div>\
						<div class=\"sk-rect2\"></div>\
						<div class=\"sk-rect3\"></div>\
						<div class=\"sk-rect4\"></div>\
						<div class=\"sk-rect5\"></div>\
					</div>\
				</div>\
			</div>\
		");

		spinner.appendTo(parent);

		if (delay > 0) {
			setTimeout(function() { 
				self.remove(parent);
			}, delay);
		}

		return self;
	},

	wanderingCubes: function(el, theme, delay) {

		var self = this,
			theme = theme || this.THEME_DARK,
			parent = el && el.length ? el : $('body');
		
		var spinner = $("\
			<div class=\"sk-spinner-container "+theme+" \">\
				<div class=\"sk-spinner-box\">\
					<div class=\"sk-spinner sk-spinner-wandering-cubes\">\
						<div class=\"sk-cube1\"></div>\
						<div class=\"sk-cube2\"></div>\
					</div>\
				</div>\
			</div>\
		");

		spinner.appendTo(parent);

		if (delay > 0) {
			setTimeout(function() { 
				self.remove(parent);
			}, delay);
		}

		return self;
	},

	pulse: function(el, theme, delay) {

		var self = this,
			theme = theme || this.THEME_DARK,
			parent = el && el.length ? el : $('body');
		
		var spinner = $("\
			<div class=\"sk-spinner-container "+theme+" \">\
				<div class=\"sk-spinner-box\">\
					<div class=\"sk-spinner sk-spinner-pulse\"></div>\
				</div>\
			</div>\
		");

		spinner.appendTo(parent);

		if (delay > 0) {
			setTimeout(function() { 
				self.remove(parent);
			}, delay);
		}

		return self;
	},

	chasingDots: function(el, theme, delay) {

		var self = this,
			theme = theme || this.THEME_DARK,
			parent = el && el.length ? el : $('body');
		
		var spinner = $("\
			<div class=\"sk-spinner-container "+theme+" \">\
				<div class=\"sk-spinner-box\">\
					<div class=\"sk-spinner sk-spinner-chasing-dots\">\
						<div class=\"sk-dot1\"></div>\
						<div class=\"sk-dot2\"></div>\
					</div>\
				</div>\
			</div>\
		");

		spinner.appendTo(parent);

		if (delay > 0) {
			setTimeout(function() { 
				self.remove(parent);
			}, delay);
		}

		return self;
	},

	threeBounce: function(el, theme, delay) {

		var self = this,
			theme = theme || this.THEME_DARK,
			parent = el && el.length ? el : $('body');
		
		var spinner = $("\
			<div class=\"sk-spinner-container "+theme+" \">\
				<div class=\"sk-spinner-box\">\
					<div class=\"sk-spinner sk-spinner-three-bounce\">\
						<div class=\"sk-bounce1\"></div>\
						<div class=\"sk-bounce2\"></div>\
						<div class=\"sk-bounce3\"></div>\
					</div>\
				</div>\
			</div>\
		");

		spinner.appendTo(parent);

		if (delay > 0) {
			setTimeout(function() { 
				self.remove(parent);
			}, delay);
		}

		return self;
	},

	circle: function(el, theme, delay) {

		var self = this,
			theme = theme || this.THEME_DARK,
			parent = el && el.length ? el : $('body');
		
		var spinner = $("\
			<div class=\"sk-spinner-container "+theme+" \">\
				<div class=\"sk-spinner-box\">\
					<div class=\"sk-spinner sk-spinner-circle\">\
						<div class=\"sk-circle1 sk-circle\"></div>\
						<div class=\"sk-circle2 sk-circle\"></div>\
						<div class=\"sk-circle3 sk-circle\"></div>\
						<div class=\"sk-circle4 sk-circle\"></div>\
						<div class=\"sk-circle5 sk-circle\"></div>\
						<div class=\"sk-circle6 sk-circle\"></div>\
						<div class=\"sk-circle7 sk-circle\"></div>\
						<div class=\"sk-circle8 sk-circle\"></div>\
						<div class=\"sk-circle9 sk-circle\"></div>\
						<div class=\"sk-circle10 sk-circle\"></div>\
						<div class=\"sk-circle11 sk-circle\"></div>\
						<div class=\"sk-circle12 sk-circle\"></div>\
					</div>\
				</div>\
			</div>\
		");

		spinner.appendTo(parent);

		if (delay > 0) {
			setTimeout(function() { 
				self.remove(parent);
			}, delay);
		}

		return self;
	},

	fadingCircle: function(el, theme, delay) {

		var self = this,
			theme = theme || this.THEME_DARK,
			parent = el && el.length ? el : $('body');
		
		var spinner = $("\
			<div class=\"sk-spinner-container "+theme+" \">\
				<div class=\"sk-spinner-box\">\
					<div class=\"sk-spinner sk-spinner-fading-circle\">\
						<div class=\"sk-circle1 sk-circle\"></div>\
						<div class=\"sk-circle2 sk-circle\"></div>\
						<div class=\"sk-circle3 sk-circle\"></div>\
						<div class=\"sk-circle4 sk-circle\"></div>\
						<div class=\"sk-circle5 sk-circle\"></div>\
						<div class=\"sk-circle6 sk-circle\"></div>\
						<div class=\"sk-circle7 sk-circle\"></div>\
						<div class=\"sk-circle8 sk-circle\"></div>\
						<div class=\"sk-circle9 sk-circle\"></div>\
						<div class=\"sk-circle10 sk-circle\"></div>\
						<div class=\"sk-circle11 sk-circle\"></div>\
						<div class=\"sk-circle12 sk-circle\"></div>\
					</div>\
				</div>\
			</div>\
		");

		parent.addClass('sk-spinner-holder');
		spinner.appendTo(parent);

		if (delay > 0) {
			setTimeout(function() { 
				self.remove(parent);
			}, delay);
		}

		return self;
	},

	cube: function(el, theme, delay) {

		var self = this,
			theme = theme || this.THEME_DARK,
			parent = el && el.length ? el : $('body');
		
		var spinner = $("\
			<div class=\"sk-spinner-container "+theme+" \">\
				<div class=\"sk-spinner-box\">\
					<div class=\"sk-spinner sk-spinner-cube-grid\">\
						<div class=\"sk-cube\"></div>\
						<div class=\"sk-cube\"></div>\
						<div class=\"sk-cube\"></div>\
						<div class=\"sk-cube\"></div>\
						<div class=\"sk-cube\"></div>\
						<div class=\"sk-cube\"></div>\
						<div class=\"sk-cube\"></div>\
						<div class=\"sk-cube\"></div>\
						<div class=\"sk-cube\"></div>\
					</div>\
				</div>\
			</div>\
		");

		spinner.appendTo(parent);

		if (delay > 0) {
			setTimeout(function() { 
				self.remove(parent);
			}, delay);
		}

		return self;
	},

	wordpress: function(el, theme, delay) {

		var self = this,
			theme = theme || this.THEME_DARK,
			parent = el && el.length ? el : $('body');
		
		var spinner = $("\
			<div class=\"sk-spinner-container "+theme+" \">\
				<div class=\"sk-spinner-box\">\
					<div class=\"sk-spinner sk-spinner-wordpress\">\
						<span class=\"sk-inner-circle\"></span>\
					</div>\
				</div>\
			</div>\
		");

		spinner.appendTo(parent);

		if (delay > 0) {
			setTimeout(function() { 
				self.remove(parent);
			}, delay);
		}

		return self;
	}
}