if (!Adomat) var Adomat = {};

Adomat.Loader = {


	indicators: {
		throbber: 'throbber', 
		snike: 'snike', 
		referesh: 'referesh', 
		spin: 'spin', 
		fullspin: 'fullspin', 
		twist: 'twist', 
		dotter: 'dotter', 
		revolve: 'revolve'
	},

	init: function(name) {
		
		if (!$( "#load-indicator-overlay" ).length) {
			name = name && this.indicators[name] ? name : 'dotter';
			var $container = $('<div />').appendTo('body');
			$container.attr('id', 'load-indicator-overlay').append('<div id="load-inidcator-container" class="'+ name +'" ><div class="load-indicator">Loading...</div></div><');
		}
	},
	
	show: function(name) {
		
		this.init();

		if (name && this.indicators[name]) {
			$('#load-inidcator-container').removeAttr('class').addClass(name);
		}

		$( "#load-indicator-overlay" ).show();
	},
	
	hide: function() {
		$( "#load-indicator-overlay" ).hide();
	}
}