if (!Adomat) var Adomat = {};

Adomat.Form = {
	
	init: function() {
	
	},

	/**
	 * After submit handler
	 * show notifications or redirect form
	 * @return void
	 */
	afterSubmit: function(data) {

		if (data.redirect) {
			window.location = data.redirect;
		}

		if (data.message) {
			Adomat.Notification.close();
			data.response ? Adomat.Notification.success(data.message) : Adomat.Notification.error(data.message);
		}
	}
}