$(document).ready(function() {

	/**
	 * Message data format
	 * theme: $theme
	 * sticky: $sticky
	 * life: $life
	 * content: $content
	 */
	Adomat.Ajax.post('/applications/helpers/message.session.php').done(function(messages) {
		
		if (messages) {	
			for (var i in messages) {

				var msg = {}
				msg.type = messages[i].theme;
				msg.text = messages[i].content;
				msg.title = messages[i].theme=='error' ? 'Error' : 'Success';

				if (messages[i].sticky) {
					msg.sticker = true;
				}

				Adomat.Notification.show(msg);
			}
		}
	});

});