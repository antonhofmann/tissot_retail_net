if (!Adomat) var Adomat = {};

Adomat.Translator = {

	data: {},

	init: function() {

		if (!_.size(this.data)) {
			this.data = Adomat.Ajax.json('/app/adomat/services/api.translator.php');
		}

		return this;
	},

	get: function(param) {
		return this.data[param];
	}
}