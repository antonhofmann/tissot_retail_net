if (!Adomat) var Adomat = {};

Adomat.Ajax = {

	post: function(url, data) {
		return $.ajax({
			url: url,
			type: 'post',
			dataType: 'json',
			data: data,
			async: true,
			cache: false
	    });
	},
	json: function(url, data) {
		
		var response;
		
		$.ajax({
			url: url,
			type: 'post',
			dataType: 'json',
			data: data,
			async: false,
			cache: false,
	        success: function(xhr) { 
	        	response = xhr;           
	        }
	    });
		
		return response || {};
	}
}