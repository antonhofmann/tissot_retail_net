;(function($) {

   var defaults = {
        text: 'More',
        caret: '<span class="caret"></span>',
        icon: ''
    }

    var TabCollapse = function(el, options) {

        this.element = $(el);
        
        // options
        options = $.extend(true, {}, defaults, $('ul', this.element).data(), options);

        // tablist items
        this.tabList = $('ul', this.element);
        this.tabList.addClass('collapsable-items');
        this.tabListItem  = this.tabList.children("li");

        // wrapper
        this.element.wrapInner( "<div class='tabs-collapsable-container'></div>");
        this.wrapper = $('.tabs-collapsable-container', this.element);

        // toggler container
        var togglerContainer = $('<div />').addClass('toggler-container')
        this.element.append(togglerContainer);
        this.toggler = $('.toggler-container', this.element);

        // dropdown elements
        var ul = $('<ul />').addClass('nav');
        var li = $('<li />').addClass('dropdown');
        var a = $('<a />').prop({
            'href'          : '#',  
            'data-toggle'   : 'dropdown',
            'role'          : 'button', 
            'aria-haspopup' : 'true',
            'aria-expanded' : 'false',
            'class'         : 'dropdown-toggle'
        }).html('<strong>'+options.text+'</strong> '+options.caret);

        // dropdown menu
        li.append(a);
        li.append('<ul class="dropdown-menu"></ul>')
        ul.append(li);
        this.toggler.append(ul);

        // triggers
        this.dropdownToggle = $(".dropdown-toggle", this.toggler);
        this.dropdownList   = $(".dropdown-menu", this.toggler);

        var $this = this;

        $this.options = options;

        $(window).on('load resize', function() {
            $this.collapse();
        });

        $this.tabListItem.on("hover click touchstart",function(e) { 
            if ($(this).is(".hide")) e.preventDefault() 
        });
        
        $this.dropdownList.on("hover click touchstart",function(e) { 
            if ($(this).parent().not(".open")) e.preventDefault() 
        });
        
        // clone list and insert into dropdown
        //$this.tabListItem.clone().appendTo($this.dropdownList);
        $this.build();
        
        $this.dropdownList.click(function(e) { 
            //e.stopPropagation() 
        });

        $this.dropdownToggle.click(function(e) {
            
            e.stopPropagation();

            $('.tabs-collapsable .dropdown-menu').removeClass('open');
            
            if ($this.toggler.is(".show")) {
                $this.dropdownList.toggleClass("open");
            }
        });

        $(document).on("click ", function() {
            if ($this.toggler.is(".show")) {
                $this.dropdownList.removeClass("open");
            }
        });
    };

    TabCollapse.prototype = {

        add: function(item) {

            var $this = this;

            $this.tabList.append(item);
            $this.tabListItem  = $this.tabList.children("li");

            $this.build();
        },

        build: function() {
            
            var $this = this;
            
            $this.dropdownList.empty();
            $this.tabListItem.clone().appendTo($this.dropdownList);

            return $this;
        },

        collapse: function() {

            var $this = this,
                dropdownListItem,
                tabListItemOffset,
                widthTigger = false; 

            var width = 0, elWidth=0;
            var tabbarWidth  = $this.element.width() - this.dropdownToggle.outerWidth(); 

            // for absolute positions
            /*if (tabbarWidth > $(window).width()) {
                tabbarWidth = $(window).width() - this.element.position().left;
                $this.wrapper.width(tabbarWidth);
            }*/

            $this.tabListItem.each(function(index) {

                var el = $(this);

                dropdownListItem  = $this.dropdownList.children("li:eq("+index+")");
                elWidth = !widthTigger ? el.outerWidth() : elWidth;

                if (elWidth==0) {
                    elWidth = el.outerWidth();
                }

                width += elWidth;
                widthTigger = widthTigger || (width >= tabbarWidth);

                el.toggleClass("tab-hide", widthTigger);
                dropdownListItem.toggleClass("tab-show", widthTigger);
            })
                
            $this.dropdownList.children(".tab-show").length != 0
                ? $this.toggler.addClass("show")
                : $this.toggler.removeClass("show")

            $this.dropdownList.css('max-height', $(window).height()-$this.dropdownList.offset().top);
        }
    }


    $.fn.tabCollapse = function ( options ) {

        return this.each(function () {

            var $this = $(this),
                data = $this.data('tabcollapse');

            if (!data)  {
                data = new TabCollapse(this, options);
                $this.data('tabcollapse', data);
            }
        })
    }

})(window.jQuery);

