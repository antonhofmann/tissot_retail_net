var Validator = {
	
	isNumber:function(str) {
        var pattern = /^\d+$/;
        return pattern.test(str);  // returns a boolean
    },

    hasInputMaskError:function(container){
        
		var maskInputs = $('input:text', container).filter('.input-mask'),
			maskInputsEmpty = $('input:text[value=""]', container).filter('.input-mask'),
			requiredEmptyFields = $('input:text[value=""]', container).filter('.required'),
			maskInputsTotal = maskInputs.length,
			maskInputsEmptyTotal = maskInputsEmpty.length;
		
		if (!maskInputsTotal) return;

		// check required fields
		if (requiredEmptyFields.length > 0) {
			requiredEmptyFields.addClass('error');
    		return true;
		}

		if (maskInputsTotal==maskInputsEmptyTotal) {
			maskInputs.removeClass('error');
			return;
		}

		if (container.hasClass('phone-container') && maskInputsEmptyTotal > 0) {
			maskInputsEmpty.addClass('error');
			return true;
		}
    },

    inputMaskResetOnEmpty:function(container){
        
        var maskInputs = $('input:text', container).filter('.input-mask'),
			maskInputsEmpty = $('input:text[value=""]', container).filter('.input-mask'),
			maskInputsTotal = maskInputs.length,
			maskInputsEmptyTotal = maskInputsEmpty.length;

		if (!maskInputsTotal) return true;

		if (maskInputsTotal == maskInputsEmptyTotal) {
			maskInputs.removeClass('error');
		}

		return true;
	},

    checkPhoneNumber: function(e) {

		var $this = this,
    		self = $(e),
			container = self.closest('.input-mask-container'),
			value = self.val().replace(/ /g,''),
			countryCode = $(".input-mask:nth-child(1)", container),
			areaCode = $(".input-mask:nth-child(2)", container),
			phoneNumber = $(".input-mask:nth-child(3)", container),
			maskInputsEmpty = $('input:text[value=""]', container).filter('.input-mask');

		// reset fields
		self.removeClass('error');
		retailnet.notification.hide();

		if (!value) {
			$this.inputMaskResetOnEmpty(container);
			return;
		}

		if (!countryCode.length  || !phoneNumber.length) {
			return;
		}


    	if (self.attr('name')!=countryCode.attr('name') && !$this.isNumber(value)) {
    		self.addClass('error');
    		retailnet.notification.error('The entered value for ' + self.attr('placeholder') + ' is not valid');
    		return;
    	}

    	// check area code zero
    	if (self.attr('name')==areaCode.attr('name') && value!=value.replace(/^0+/g, '')) {
    		self.addClass('error');
    		retailnet.notification.error('Please remove leading zero(s) from area code');
    		return;
    	}

		if (self.attr('name')==countryCode.attr('name')) {

			$.getJSON('/applications/helpers/ajax.country.php', {
				section: 'country_phone_prefix',
				value: value
			}, function(xhr) {
				
				var response = xhr || {}
				
				if (!response.success) {
					self.addClass('error');
	        		retailnet.notification.error('The entered value for ' + self.attr('placeholder') + ' doesn\'t exist');
	        		return;
				}
	        });
		}
    }
}