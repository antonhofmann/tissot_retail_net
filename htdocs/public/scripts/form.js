jQuery.fn.Form = function() {
	
	var form = this;
	var input = $('input:not(:button, :submit, :reset, :hidden)', this);
	var select = $('select:visible', this);
	var textarea = $('textarea:visible', this);
	
	var firstElement = input.get(0);
	
	if (select.get(0) && firstElement) {
		
		var firstSelect = select.get(0);
		
		if (firstSelect.offsetTop < firstElement.offsetTop) {
			firstElement = firstSelect;
		}
	}
	
	if (textarea.get(0) && firstElement) {
		
		var firstTextarea = textarea.get(0);
		
		if (firstTextarea.offsetTop < firstElement.offsetTop) {
			firstElement = textarfirstTextareaea;
		}
	}

	// event: focus
	if (firstElement) {
		firstElement.focus();
		$(firstElement).addClass('onFocus');
	}
	
	// event: keypress
	$(this).delegate("input", "keypress", function(event) {
		
		if (event.keyCode == 13) {
			
			var inputs = $(this).parents("form").eq(0).find(":input").not(':button, :submit, :reset, :hidden');
			var idx = inputs.index(this);
			
			if (inputs.length == idx + 1) {
				form.submit();
			}
			else if (idx == inputs.length - 1) {
				inputs[0].select();
			} else {
				inputs[idx + 1].focus();
				inputs[idx + 1].select();
			}
			
			return false;
       }
	});
	
	// event: click
	jQuery(document).delegate("input, textarea", "click", function() {
		$(this).removeClass('onBlur').addClass('onFocus');
	});
	
	// event: blur
	jQuery(document).delegate("input,select,textarea", "blur", function() {
		
		var tag = $(this).not(':button, :submit, :reset, :hidden, :checkbox, :radio').attr('tag');
		
		if (tag=="required" && $(this).val()) {
			$(this).removeClass('error');
		}
		
		$(this).removeClass('onFocus').addClass('onBlur');
	});
}
