$(document).ready(function() { 
	$.getJSON('/applications/helpers/message.session.php', function(response) {
		if (response) {
			$.each(response, function(i,message) {
				$.jGrowl(message.content, { sticky : message.sticky, life : message.life, theme : message.theme });
			});
		}
	});
});