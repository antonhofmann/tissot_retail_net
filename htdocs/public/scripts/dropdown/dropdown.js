(function( $ ){
	
	var dropbox = {
		init : function() {
			
			var self = this;
			
			this.each(function() { 
				
				self.elem = this;
				
				var parent = $(this).closest('.dropdown-placeholder');
				var width = $(this).outerWidth()+10;
				var id = $(this).attr('id');
				
				var title = ($('option:selected', this).val())
					? $('option:selected',this).text()
					: $('option:first',this).text();
                
                $(this).not('.dropdown-rows').addClass('dropdown')
                .wrap('<span class="dropdown-placeholder dropdown-'+id+'" />')
                .after('<span class=button ><span class=label>' + title + '</span><span class="arrow arrow-down"></span></span>')
                .change(function(){
                	val = $('option:selected',this).text();
                	$('.label', parent).text(val);
                });
                
                $(this).width(width+14);
                $(this).next().width(width);
                
                return self;
            });
		}
	};
	
	$.fn.dropdown = function( method ) { 
		if ( dropbox[method] ) {
			return dropbox[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return dropbox.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on popover plugin' );
		} 
	};
	
})( jQuery );