/*
 	Javascript Master Library

 	Admir Serifi, admir.serifi@mediaparx.com
 	copyright (c) MediaparX AG. All rights reserved.
*/

	function arrayCompare(a1, a2) {
	    if (a1.length != a2.length) return false;
	    var length = a2.length;
	    for (var i = 0; i < length; i++) {
	        if (a1[i] !== a2[i]) return false;
	    }
	    return true;
	}

	function removeFromArray(array,item){
	    for(var i in array){
	        if(array[i]==item){
	            array.splice(i,1);
	            break;
	        }
	    }
	}

	function inArray(needle, haystack) {
	    var length = haystack.length;
	    for(var i = 0; i < length; i++) {
	        if(typeof haystack[i] == 'object') {
	            if(arrayCompare(haystack[i], needle)) return true;
	        } else {
	            if(haystack[i] == needle) return true;
	        }
	    }
	    return false;
	}


	function isImage(path) {
		if (path) {
			var images = ['jpg','jpeg','gif','png'];
			var extension =  path.split('.').pop();
			return (inArray(extension, images)) ? true : false;
		}
	}


	function isValidDate(s) {
	    // format D(D)/M(M)/(YY)YY
	    var dateFormat = /^\d{1,4}[\.|\/|-]\d{1,2}[\.|\/|-]\d{4}$/;

	    if (dateFormat.test(s)) {
	        // remove any leading zeros from date values
	        s = s.replace(/0*(\d*)/gi,"$1");
	        var dateArray = s.split(/[\.|\/|-]/);

	        // correct month value
	        dateArray[1] = dateArray[1]-1;

	        // correct year value
	        if (dateArray[2].length<4) {
	            // correct year value
	            dateArray[2] = (parseInt(dateArray[2]) < 50) ? 2000 + parseInt(dateArray[2]) : 1900 + parseInt(dateArray[2]);
	        }

	        var testDate = new Date(dateArray[2], dateArray[1], dateArray[0]);
	        if (testDate.getDate()!=dateArray[0] || testDate.getMonth()!=dateArray[1] || testDate.getFullYear()!=dateArray[2]) {
	            return false;
	        } else {
	            return true;
	        }
	    } else {
	        return false;
	    }
	}

	
	$(document).ready(function() {
		
		var docMode = document.documentMode;
		var scroller = 0;

		var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
		
		$(window).resize(function() {
			
			$('body').removeAttr('style');
			
			var winwidth = $(window).width();
			var docwidth = $(document).width();
			var winheight = $(window).height();
			var docheight = $(document).height();
			var maxwidth = (winwidth > docwidth) ? winwidth : docwidth;
			
			if (msie > 0 && docheight > winheight) {
				
				scroller = (docheight > winheight) ? 24 : 0;
				
				if (docMode==7) {
					scroller = (winwidth > docwidth) ? scroller : 0;
				}
			}

			$('body').width(maxwidth-scroller);
			
		}).trigger('resize');
		
		// ie document mode
		
		if (msie > 0) {
			$('html').removeAttr('class').attr('class','ie ie'+document.documentMode);
		}
		
		// table rollover
		$(document).delegate(".over-effects tbody tr", "mouseover", function() {
            $(this).addClass("-over");
        });
        
		// table rollout
        $(document).delegate(".over-effects tbody tr", "mouseout", function() {
            $(this).removeClass("-over");
        });
        
        $('a.popup').click(function(event) {
			
			event.preventDefault();

			var width = $(window).width();
			var height = $(window).height();
			var href = $(this).attr('href');
			var name = $(this).attr('name') || 'popupWindow';
			
			width = width - Math.ceil((10/100)*width);
			height = height - Math.ceil((10/100)*height);

			var spreadsheet = window.open(href, name, "width="+width+",height="+height+",resizable=1,scrollbars=1,directories=0,titlebar=0,toolbar=0,location=0,status=0,menubar=0");
			spreadsheet.focus();
			
			return false;
			
		});

	});

