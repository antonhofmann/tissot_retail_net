$(document).ready(function() {
	
	if ($(".assistance-box").length > 0) {
		
		var container = $(".assistance-box"),
			offset = container.offset(),
			width = container.outerWidth(true),
			triggerWidth = $('.trigger', container).outerWidth(),
			triggerHeight = $('.trigger', container).outerHeight();
		
		$('.trigger', container).click(function(e) {

			e.preventDefault();
			e.stopPropagation();
	
			var self = $(this),
				right = parseInt(container.css('right').replace('px', '')),
				active = false;
			
			// right position
			right = (right < 0) ? 0 : -(width - triggerWidth);
			
			// is toggle acive
			activ = (right) ? false : true;
			
			// toggle sections
			$('.assistance-section-content').hide();
			
			// toggle topics with one section
			if ($('.assistance-section-title', container).length == 1) {
				$('.assistance-section-title:first').trigger('click'); 
			}
			
			// slide animation
			container.animate({
				right:right
			}).toggleClass('active', activ);
			
			// change trigger icon
			$(self, container).toggleClass('fa-question-circle').toggleClass('fa-arrow-circle-right');
		});
	
		
		// toggle section content
		$('.assistance-section-title').click(function(e) { 

			e.preventDefault();
			e.stopPropagation();
			
			// togle content
			$(this).next().toggle().promise().done(function() {
				$('.assistance-box').trigger('resized');
			});
			
			// change trigger icon
			$('i', $(this)).toggleClass('fa-caret-right').toggleClass('fa-caret-down');
		});
		
		
		// on container resize
		$(document).on("resized", ".assistance-box", function() {
			
			var containerHeight = container.outerHeight(true),
				contentHeight = $('.topics', container).outerHeight(true) + 10,
				browserHeight = $(window).height();
			
			var totalContainer = offset.top + contentHeight + triggerHeight + 10;
			
			if (totalContainer > browserHeight) {
				containerHeight = browserHeight - offset.top - 40;
				contentHeight = containerHeight - triggerHeight;
			} 
			else {
				containerHeight = contentHeight + triggerHeight;
			}
			
			container.height(containerHeight);
			$('.assistance-box-content', container).height(contentHeight);
		});
		
		// on browser resize
		$(window).resize(function() {
			$('.assistance-box').trigger('resized');
		}).trigger('resize');
	}
	
});