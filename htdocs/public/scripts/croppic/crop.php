<?php

error_reporting(E_ERROR | E_WARNING);

require_once $_SERVER['DOCUMENT_ROOT'].'/utilities/simpleimage.php';

$_ERRORS = array();
$_MESSAGES = array();
$_RESPONSE = array();
$_ROOTPATH = $_SERVER['DOCUMENT_ROOT'];

$_FILENAME = $_REQUEST['filename'];
$_PATH = $_REQUEST['path'];
$_FILE = $_REQUEST['imgUrl'];
$_SRC_WIDTH = $_REQUEST['imgInitW'];
$_SRC_HEIGHT = $_REQUEST['imgInitH'];
$_WIDTH = $_REQUEST['imgW']; 
$_HEIGHT = $_REQUEST['imgH'];
$_Y1 = $_REQUEST['imgY1'];
$_X1 = $_REQUEST['imgX1'];
$_CROP_WIDTH = $_REQUEST['cropW'];
$_CROP_HEIGHT = $_REQUEST['cropH'];

$_QUALITY = 100;
$_SOURCE = $_ROOTPATH.$_FILE;
$_TARGET_PATH = $_ROOTPATH.$_PATH;

// check file extensions
$_PATHINFO = pathinfo($_SOURCE);
$_EXTENSIONS = $_REQUEST['extension'] ? explode(',', $_REQUEST['extension']) : array("gif", "jpeg", "jpg", "png");
$_EXTENSION = strtolower($_PATHINFO['extension']);

if (!file_exists($_SOURCE)) {
	$_ERRORS[] = "File not found.";
	goto BLOCK_RESPONSE;
}

if ($_EXTENSIONS && !in_array($_EXTENSION, $_EXTENSIONS)) {
	$_ERRORS[] = "File extension is not permitted.";
	goto BLOCK_RESPONSE;
}

if (!$_PATH) {
	$_ERRORS[] = "Target path is not defined.";
	goto BLOCK_RESPONSE;
}

if (!$_WIDTH || !$_HEIGHT) {
	$_ERRORS[] = "Bed request.";
	goto BLOCK_RESPONSE;
}

// create target directory
if (!file_exists($_TARGET_PATH)) {
	mkdir($_TARGET_PATH, 0777, true);
	chmod($_TARGET_PATH, 0777);
} 

try {
    
    $img = new SimpleImage();
	$img->load($_SOURCE);

	// resize image 
	$img->resize($_WIDTH, $_HEIGHT);

	$x1 = $_X1;
	$x2 = $x1+$_CROP_WIDTH;
	$y1 = $_Y1;
	$y2 = $_Y1+$_CROP_HEIGHT;

	// check coordinates
	if ($x1>=$x2 || $y1>=$y2) {
		$_ERRORS[] = "Image resize and crop coordinates are wrong";
		goto BLOCK_RESPONSE;
	}

	// crop image
	$img->crop($x1, $y1, $x2, $y2);

	$filename = $_FILENAME ? $_FILENAME.'.croped.'.$_SERVER['REQUEST_TIME'] : $_SERVER['REQUEST_TIME'];
	$_OUTPUT = $_PATH.'/'.$filename.'.'.$_EXTENSION;
	$_OUTPUT_FULLPATH = $_ROOTPATH.$_OUTPUT;

	$img->save($_OUTPUT_FULLPATH);

	if (!file_exists($_OUTPUT_FULLPATH )) {
		$_ERRORS[] = "File processing failure.";
		goto BLOCK_RESPONSE;
	}

	// remove orginal image
	@unlink($_SOURCE);

	// get all entity files
	$files = $_FILENAME ? glob($_ROOTPATH.$_PATH.'/'.$_FILENAME."*", GLOB_BRACE) : null;;
	
	// remove all entity files
	if ($files) {
		foreach ($files as $file) {
			if ($file <> $_OUTPUT_FULLPATH) {
				@unlink($file);
				$_RESPONSE['removed'][] = $file;
			}
		}
	}

	$_RESPONSE['success'] = true;
	$_RESPONSE['url'] = $_OUTPUT;

} catch(Exception $e) {
	$_ERRORS[] = $e->getMessage();
	goto BLOCK_RESPONSE;
}


BLOCK_RESPONSE:

if ($_ERRORS) {

	$_RESPONSE['error'] = join('<br />', $_ERRORS);

	$_RESPONSE['image'] = array(
		'source' => array('width' => $_SRC_WIDTH, 'height' => $_SRC_HEIGHT),
		'zoomed' => array('width' => $_WIDTH, 'height' => $_HEIGHT),
		'cropped' => array('width' => $_CROP_WIDTH, 'height' => $_CROP_HEIGHT),
		'coordinates' => array("$x1,$y1", "$x2,$y2")
	);
}

header('Content-Type: text/json');
print json_encode($_RESPONSE);
