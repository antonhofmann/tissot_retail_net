<?php

error_reporting(E_ERROR | E_WARNING);

require_once $_SERVER['DOCUMENT_ROOT'].'/utilities/simpleimage.php';

$_ERRORS = array();
$_MESSAGES = array();
$_RESPONSE = array();
$_ROOTPATH = $_SERVER['DOCUMENT_ROOT'];

$_FILE = $_FILES["file"];
$_FILENAME = $_REQUEST['filename'];
$_PATH = $_REQUEST['path'];
$_EXTENSIONS = $_REQUEST['extensions'] ? explode(',', $_REQUEST['extensions']) : array("gif", "jpeg", "jpg", "png");
$_PATHINFO = pathinfo($_FILE['name']);
$_EXTENSION = strtolower($_PATHINFO['extension']);

$_MIN_WIDTH = $_REQUEST['minWidth'];
$_MIN_HEIGHT = $_REQUEST['minHeight'];

if ($_EXTENSIONS && !in_array($_EXTENSION, $_EXTENSIONS)) {
	$_ERRORS[] = "File extension is not permitted.";
	goto BLOCK_RESPONSE;
}

if (!$_PATH) {
	$_ERRORS[] = "Error: set file upload directory path.";
	goto BLOCK_RESPONSE;
}

if ($_FILE['error']) {
	$_ERRORS[] = 'Error in file content: '. $_FILE['error'];
	goto BLOCK_RESPONSE;
}

if (!file_exists($_ROOTPATH.$_PATH)) {
	mkdir($_ROOTPATH.$_PATH, 0777, true);
	chmod($_ROOTPATH.$_PATH, 0777);
} 
	
$extension = $_EXTENSION=='jpg' ? 'jpeg' : $_EXTENSION;
$filename = $_FILENAME ?: $_PATHINFO['filename'];
	
$_SOURCE = $_FILE["tmp_name"];
$filename = $_FILENAME ? $_FILENAME.'.'.$_SERVER['REQUEST_TIME'] : $_SERVER['REQUEST_TIME'];
$_TARGET = $_PATH.'/'.$filename.'.'.$extension;
$_TARGET_FULLPATH = $_ROOTPATH.$_TARGET;

// upload file
$upload = move_uploaded_file($_SOURCE, $_TARGET_FULLPATH);

if (!file_exists($_TARGET_FULLPATH)) {
	$_ERRORS[] = "Error by uploading.";
	goto BLOCK_RESPONSE;
}

try {
    
    $img = new SimpleImage();
	$img->load($_TARGET_FULLPATH);

	$_IMG_WIDTH = $img->get_width();
	$_IMG_HEIGHT = $img->get_height();

	// resizing
	if ($_MIN_WIDTH && $_MIN_HEIGHT && ($_IMG_WIDTH < $_MIN_WIDTH || $_IMG_HEIGHT < $_MIN_HEIGHT)) {

		$filename = $_FILENAME ? $_FILENAME.'.'.$_SERVER['REQUEST_TIME'] : $_SERVER['REQUEST_TIME'];
		$_TARGET = $_PATH.'/'.$filename.'.'.$extension;
		$_TARGET_FULLPATH = $_ROOTPATH.$_TARGET;

		$_IMG_WIDTH = $_MIN_WIDTH;
		$_IMG_HEIGHT =  $_MIN_HEIGHT;

		$imgInfo =  $img->get_original_info();
		
		if ($imgInfo['format'] === 'gif') {
			$img->resize($_MIN_WIDTH, $_MIN_HEIGHT);
			$img->save($_TARGET_FULLPATH);
		} else {
			$arr = array(255, 255, 255, 127);
			$bg = new SimpleImage(null, $_MIN_WIDTH, $_MIN_HEIGHT, $arr);
			$bg->overlay($img);
			$bg->save($_TARGET_FULLPATH);
		}
	}

} catch(Exception $e) {
	$_ERRORS[] = $e->getMessage();
	goto BLOCK_RESPONSE;
}

$_RESPONSE = array(
	"success" => $upload,
	"url" => $_TARGET,
	"width" => $_IMG_WIDTH,
	"height" => $_IMG_HEIGHT
);

BLOCK_RESPONSE:

if ($_ERRORS) {
	$_RESPONSE['error'] = join('<br />', $_ERRORS);
}

header('Content-Type: text/json');
print json_encode($_RESPONSE);
