<?php

error_reporting(E_ERROR | E_WARNING);

$_RESPONSE = array();
$_PATH = $_REQUEST['file'];
$_ROOTPATH = $_SERVER['DOCUMENT_ROOT'];

if (file_exists($_ROOTPATH.$_PATH)) {
	$_RESPONSE['success'] = true;
	$_RESPONSE['src'] = $_PATH;
	//$_RESPONSE['class'] = strpos($_PATH, 'croped')!==false ? null : 'tocrop';
}

header('Content-Type: text/json');
print json_encode($_RESPONSE);