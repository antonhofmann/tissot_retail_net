<?php

	error_reporting(E_ERROR);
	
	define('PATH_LIBRARIES', $_SERVER['DOCUMENT_ROOT'].'/libraries/');
	define('PATH_APPLICATIONS', $_SERVER['DOCUMENT_ROOT'].'/applications/');
	define('PATH_CONTROLLERS', $_SERVER['DOCUMENT_ROOT'].'/applications/controllers/');
	define('PATH_MODELS',$_SERVER['DOCUMENT_ROOT'].'/applications/models/');
	define('PATH_DATAMAPPERS', $_SERVER['DOCUMENT_ROOT'].'/applications/datamappers/');
	define('PATH_VIEWS', $_SERVER['DOCUMENT_ROOT'].'/applications/views/');
	define('PATH_MODULES', $_SERVER['DOCUMENT_ROOT'].'/applications/modules/');
	define('PATH_HELPERS', $_SERVER['DOCUMENT_ROOT'].'/applications/helpers/');
	define('PATH_SETTINGS', $_SERVER['DOCUMENT_ROOT'].'/applications/settings/');
	define('PATH_UTILITIES', $_SERVER['DOCUMENT_ROOT'].'/utilities/');
	define('PATH_THEMES', $_SERVER['DOCUMENT_ROOT'].'/public/themes/');
	define('PATH_DATA', $_SERVER['DOCUMENT_ROOT'].'/public/data/');
	
	define('DIR_CSS', '/public/css/');
	define('DIR_COMPILED', '/public/compiled/');
	define('DIR_IMAGES', '/public/images/');
	define('DIR_SCRIPTS', '/public/scripts/');
	define('DIR_ASSETS', '/public/scripts/');
	define('DIR_JS', '/public/js/');
	define('DIR_THEMES', '/public/themes/');

	// manual includes
	require_once PATH_LIBRARIES . 'underscore/underscore.php';	
	require_once PATH_LIBRARIES . 'underscore/mixins.php';	
	
	/**
	 * Autoloader for old retailnet
	 */
	class Retailnet_Legacy_Autoloader {
		/**
		 * Paths within which we search for files
		 * @var array
		 */
		protected static $dir = array(
			PATH_CONTROLLERS,
			PATH_DATAMAPPERS,
			PATH_MODULES,
			PATH_MODELS,
			PATH_UTILITIES
			);

		/**
		 * Register autoloader
		 * @return boolean
		 */
		public static function register() {
			spl_autoload_extensions('.inc, .php, .lib');
			return spl_autoload_register(array('Retailnet_Legacy_Autoloader', 'load'));
		}	//	function Register()

		/**
		 * Do autoloading
		 * @param  string $classname
		 * @return boolean
		 */
		public static function load($classname){
			$file = strtolower($classname).".php";

			foreach (self::$dir as $path) {
				if (file_exists($path.$file)) { 
					$require =  $path.$file;
					break;
				} 
			}

			if (isset($require)) {
				require $require;
			} 
			else {
				return false;
			}
		}
	}

	Retailnet_Legacy_Autoloader::register();


	$settings = Settings::init();
	$path = DIR_THEMES.$settings->theme.'/css';
	
	// compile theme sheets
	CssCompiler::attach(array(
		//"$path/reset.css",
		"$path/layout.css",
		"$path/navigations.css",
		"$path/table.css",
		"$path/form.css",
		"/public/css/gui.css",
		"$path/gui.css",
		"/public/css/retailnet.old.css"
	));	
