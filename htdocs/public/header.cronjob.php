<?php
	
	// stricter error reporting for php unit bad sadly code isn't clean enough for notices :(
	if (isset($_ENV['phpunit']) && $_ENV['phpunit'] === true) {
		error_reporting(E_ALL ^ E_NOTICE ^ E_STRICT);
	}
	else {
		error_reporting(E_ERROR);
	}
	
	define('PATH_LIBRARIES'   , $_SERVER['DOCUMENT_ROOT'].'/libraries/');
	define('PATH_APPLICATIONS', $_SERVER['DOCUMENT_ROOT'].'/applications/');
	define('PATH_CONTROLLERS' , $_SERVER['DOCUMENT_ROOT'].'/applications/controllers/');
	define('PATH_MODELS'      , $_SERVER['DOCUMENT_ROOT'].'/applications/models/');
	define('PATH_VIEWS'       , $_SERVER['DOCUMENT_ROOT'].'/applications/views/');
	define('PATH_TEMPLATES'   , $_SERVER['DOCUMENT_ROOT'].'/applications/templates/');
	define('PATH_MODULES'     , $_SERVER['DOCUMENT_ROOT'].'/applications/modules/');
	define('PATH_DATAMAPPERS' , $_SERVER['DOCUMENT_ROOT'].'/applications/datamappers/');
	define('PATH_ITERATORS'   , $_SERVER['DOCUMENT_ROOT'].'/applications/iterators/');
	define('PATH_HELPERS'     , $_SERVER['DOCUMENT_ROOT'].'/applications/helpers/');
	define('PATH_SETTINGS'    , $_SERVER['DOCUMENT_ROOT'].'/applications/settings/');
	define('PATH_UTILITIES'   , $_SERVER['DOCUMENT_ROOT'].'/utilities/');
	define('PATH_THEMES'      , $_SERVER['DOCUMENT_ROOT'].'/public/themes/');
	define('PATH_DATA'        , $_SERVER['DOCUMENT_ROOT'].'/public/data/');

	// manual includes
	require_once PATH_LIBRARIES . 'underscore/underscore.php';	
	require_once PATH_LIBRARIES . 'underscore/mixins.php';		

	function cronjobAutoloader($classname) { 

		$dir = array(PATH_CONTROLLERS, PATH_MODULES, PATH_MODELS, PATH_UTILITIES, PATH_DATAMAPPERS, PATH_ITERATORS);
		$file = strtolower($classname).".php";

		foreach ($dir as $path) {
			if (file_exists($path.$file)) { 
				$require =  $path.$file;
				break;
			} 
		}

		if (isset($require)) {
			require $require;
		}
	}
	
	spl_autoload_extensions(".php");
	spl_autoload_register('cronjobAutoloader');
