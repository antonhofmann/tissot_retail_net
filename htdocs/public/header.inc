<?php
	
	session_name('retailnet');
	session_start();

	error_reporting(E_ERROR | E_WARNING);
	
	header('Content-type: text/html; charset=utf-8');
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	
	define('START_TIME', time()); 
	
	date_default_timezone_set ('Europe/Zurich');

	// backend paths
	define('PATH_LIBRARIES'   , $_SERVER['DOCUMENT_ROOT'].'/libraries/');
	define('PATH_APPLICATIONS', $_SERVER['DOCUMENT_ROOT'].'/applications/');
	define('PATH_CONTROLLERS' , $_SERVER['DOCUMENT_ROOT'].'/applications/controllers/');
	define('PATH_MODELS'      , $_SERVER['DOCUMENT_ROOT'].'/applications/models/');
	define('PATH_VIEWS'       , $_SERVER['DOCUMENT_ROOT'].'/applications/views/');
	define('PATH_TEMPLATES'   , $_SERVER['DOCUMENT_ROOT'].'/applications/templates/');
	define('PATH_MODULES'     , $_SERVER['DOCUMENT_ROOT'].'/applications/modules/');
	define('PATH_DATAMAPPERS' , $_SERVER['DOCUMENT_ROOT'].'/applications/datamappers/');
	define('PATH_ITERATORS'   , $_SERVER['DOCUMENT_ROOT'].'/applications/iterators/');
	define('PATH_HELPERS'     , $_SERVER['DOCUMENT_ROOT'].'/applications/helpers/');
	define('PATH_SETTINGS'    , $_SERVER['DOCUMENT_ROOT'].'/applications/settings/');
	define('PATH_UTILITIES'   , $_SERVER['DOCUMENT_ROOT'].'/utilities/');
	define('PATH_THEMES'      , $_SERVER['DOCUMENT_ROOT'].'/public/themes/');
	define('PATH_DATA'        , $_SERVER['DOCUMENT_ROOT'].'/public/data/');
	
	// frontend paths
	define('DIR_CSS'     , '/public/css/');
	define('DIR_COMPILED', '/public/compiled/');
	define('DIR_IMAGES', '/public/images/');
	define('DIR_SCRIPTS', '/public/scripts/');
	define('DIR_ASSETS', '/public/scripts/');
	define('DIR_THEMES', '/public/themes/');
	define('DIR_JS', '/public/js/'); 

	// manual includes
	require_once PATH_LIBRARIES . 'underscore/underscore.php';	
	require_once PATH_LIBRARIES . 'underscore/mixins.php';	

	if (!function_exists('retailnetAutoloader')) { 
		
		function retailnetAutoloader($class) { 
			
			$dir = array(PATH_CONTROLLERS, PATH_MODULES, PATH_MODELS, PATH_UTILITIES, PATH_DATAMAPPERS, PATH_ITERATORS);
			$file = trim($class, '/');
			$file = str_replace ('\\', DIRECTORY_SEPARATOR, $file);
			$file = strtolower($file);
			$file = "$file.php";

			if (file_exists($_SERVER['DOCUMENT_ROOT']."/".$file)) {
				$require = $_SERVER['DOCUMENT_ROOT']."/".$file;
			} else {
				foreach ($dir as $path) {
					if (file_exists($path.$file)) { 
						$require =  $path.$file;
						break;
					} 
				}
			}

			if (isset($require)) require $require;
			//elseif(!function_exists('retailnetAutoloader'))  Bootstrap::page_not_found();
		}

		spl_autoload_extensions(".inc, .php, .lib");
		spl_autoload_register('retailnetAutoloader');
	}

