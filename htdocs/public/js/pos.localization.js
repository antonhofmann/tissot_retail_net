// save localized province data

$(document).on('change', 'input[class^="posaddress_name_"]', function(e) {
		
		var self = $(this);

		
		retailnet.ajax.json('/applications/modules/pos/localization/ajax_submit.php', {
			application: $('#application').val(),
			section: 'posaddress_name',
			language_id:self.closest('td').data('language_id'),
			country_id:self.closest('td').data('country_id'),
			posaddress_id: self.attr('index'),
			posaddress_name: self.val()
		})

});


$(document).on('change', 'input[class^="posaddress_name2_"]', function(e) {
		
		var self = $(this);

		
		retailnet.ajax.json('/applications/modules/pos/localization/ajax_submit.php', {
			application: $('#application').val(),
			section: 'posaddress_name2',
			language_id:self.closest('td').data('language_id'),
			country_id:self.closest('td').data('country_id'),
			posaddress_id: self.attr('index'),
			posaddress_name2: self.val()
		})

});


$(document).on('change', 'input[class^="posaddress_address_"]', function(e) {
		
		var self = $(this);

		
		retailnet.ajax.json('/applications/modules/pos/localization/ajax_submit.php', {
			application: $('#application').val(),
			section: 'posaddress_address',
			language_id:self.closest('td').data('language_id'),
			country_id:self.closest('td').data('country_id'),
			posaddress_id: self.attr('index'),
			posaddress_address: self.val()
		})

});


$(document).on('change', 'input[class^="posaddress_address2_"]', function(e) {
		
		var self = $(this);

		
		retailnet.ajax.json('/applications/modules/pos/localization/ajax_submit.php', {
			application: $('#application').val(),
			section: 'posaddress_address2',
			language_id:self.closest('td').data('language_id'),
			country_id:self.closest('td').data('country_id'),
			posaddress_id: self.attr('index'),
			posaddress_address2: self.val()
		})

});


$(document).on('change', 'input[class^="posaddress_zip_"]', function(e) {
		
		var self = $(this);

		
		retailnet.ajax.json('/applications/modules/pos/localization/ajax_submit.php', {
			application: $('#application').val(),
			section: 'posaddress_zip',
			language_id:self.closest('td').data('language_id'),
			country_id:self.closest('td').data('country_id'),
			posaddress_id: self.attr('index'),
			posaddress_zip: self.val()
		})

});


$(document).on('change', 'input[class^="posaddress_phone_"]', function(e) {
		
		var self = $(this);

		
		retailnet.ajax.json('/applications/modules/pos/localization/ajax_submit.php', {
			application: $('#application').val(),
			section: 'posaddress_phone',
			language_id:self.closest('td').data('language_id'),
			country_id:self.closest('td').data('country_id'),
			posaddress_id: self.attr('index'),
			posaddress_phone: self.val()
		})

});

