// save localized province data

$(document).on('change', 'input[class^="openinghr_daytime_"]', function(e) {
		
		var self = $(this);
		
		retailnet.ajax.json('/applications/modules/openinghour/localization/ajax_submit.php', {
			application: $('#application').val(),
			language_id:self.closest('td').data('language_id'),
			openinghr_id: self.attr('index'),
			openinghr_daytime: self.val()
		})

});


$(document).on('change', 'input[class^="openinghr_time_24_"]', function(e) {
		
		var self = $(this);

		
		retailnet.ajax.json('/applications/modules/openinghour/localization/ajax_submit.php', {
			application: $('#application').val(),
			language_id:self.closest('td').data('language_id'),
			openinghr_id: self.attr('index'),
			openinghr_time_24: self.val()
		})

});

$(document).on('change', 'input[class^="openinghr_time_12_"]', function(e) {
		
		var self = $(this);

		
		retailnet.ajax.json('/applications/modules/openinghour/localization/ajax_submit.php', {
			application: $('#application').val(),
			language_id:self.closest('td').data('language_id'),
			openinghr_id: self.attr('index'),
			openinghr_time_12: self.val()
		})

});




