$(document).ready(function() {
	
	$('#sectionForm').formValidation({
		framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        }
	})
    .on('success.field.fv', function(e, data) {
        if (data.fv.getSubmitButton()) {
            data.fv.disableSubmitButtons(false);
        }
    })
	.on('success.form.fv', function(e) {

        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);
        var bv = $form.data('formValidation');

        // Use Ajax to submit form data
        $.post($form.attr('action'), $form.serialize(), function(xhr) {
            
            if (xhr && xhr.success && xhr.redirect) {
            	window.location = xhr.redirect;
            }	

            if (xhr && xhr.success) {
            	Adomat.Notification.success(xhr.success);
            }

            if (xhr && xhr.error) {
            	Adomat.Notification.error(xhr.error);
            }

        }, 'json');
    });

    $('#btnDelete').on('click', function(e) {

    	e.preventDefault();

    	var self = $(this);
		var url = self.prop('href') || self.data('url');
		var msg;

		if (self.data('message')) {
			msg = self.data('message');
			msg = $(msg).length>0 ? $(msg).html() : msg;
		} else msg = 'Are your sure?';

		if (!url) {
			Adomat.Notification.error('Action URL is not defined.');
			return false;
		}
		
		BootstrapDialog.confirm(msg, function(confirmed) {
			
			if (confirmed) {

				Adomat.Ajax.post(url).done(function(xhr) {

					if (xhr && xhr.redirect) {
						window.location=xhr.redirect;
					}

					if (xhr && xhr.error) {
						Adomat.Notification.error(xhr.error);
					}
				})	
			}
		})
    })
})