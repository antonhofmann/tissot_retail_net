$(document).ready(function() {

	// spiner instance	
	retailnet.loader.init();
	
	var ordersheets = $('#ordersheets').tableLoader({
		url: '/applications/modules/launchplan/list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select', self).dropdown();
		}
	});

	$('#ordersheets').on("change", "#actions", function() {

		if ($("option:selected", this).hasClass('request_modal')) {

			var url = $(this).val();

			retailnet.loader.show();

			$.post(url, function(xhr) {

				retailnet.loader.hide();

				if (xhr && xhr.message) {
					
					if (xhr.response) retailnet.notification.success(xhr.message);
					else retailnet.notification.error(xhr.message);
				}

				if (xhr.response) {
					ordersheets.showPage(1);
				}
			});

			$('option:first',this).attr('selected', true);
			
		} else {
			window.location = $(this).val();
		}
	});	
});