$(document).ready(function() {

	var form = $('#passwordform');

	// loader instance
	retailnet.loader.init();

	form.validationEngine({
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		autoHidePrompt: true,
		binded: true,
		scroll: true,
		onValidationComplete: function(form, status){
			if (status === true) {
				retailnet.loader.show();
			} else {
				return false;
			}
		},
		onAjaxFormComplete: function(status, form, json, options) {

			// send mail
			if (json && json.user && json.response) { 

				retailnet.ajax.json('/applications/helpers/security.send.password.confirmation.php', { id:json.user }).done(function(xhr) {

					if (xhr && xhr.response) {
						window.location="/security";
					}
					
					// show notofications
					if (xhr && xhr.message) {
						retailnet.notification.show(xhr.message);
					}
					
				}).complete(function() {

					// show wait screen
					retailnet.loader.hide();
				});	
			
			} else {
				
				retailnet.loader.hide();

				// close all notofications
				if (json && json.message) {
					retailnet.notification.error(json.message);
				}
			}
		
		}
	});

	$("#submit").click(function(e) {
		e.preventDefault();
		retailnet.notification.hide();
		form.submit();
		return false;
	});
});