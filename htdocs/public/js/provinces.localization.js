// save localized province data

$(document).on('change', 'input[class^="canton_"]', function(e) {
		
		var self = $(this);

		retailnet.ajax.json('/applications/modules/province/localization/ajax_submit.php', {
			application: $('#application').val(),
			language_id:self.closest('td').data('language_id'),
			province_id: self.attr('index'),
			province_canton: self.val()
		})

});


$(document).on('change', 'input[class^="region_"]', function(e) {
		
		var self = $(this);

		retailnet.ajax.json('/applications/modules/province/localization/ajax_submit.php', {
			application: $('#application').val(),
			language_id:self.closest('td').data('language_id'),
			province_id: self.attr('index'),
			province_region: self.val()
		})

});


$(document).on('change', 'input[class^="shortcut_"]', function(e) {
		
		var self = $(this);


		retailnet.ajax.json('/applications/modules/province/localization/ajax_submit.php', {
			application: $('#application').val(),
			language_id:self.closest('td').data('language_id'),
			province_id: self.attr('index'),
			province_shortcut: self.val()
		})

});

