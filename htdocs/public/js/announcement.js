jQuery(document).ready(function() {

	var path;
	var application = $('#application');
	var form = $("form.announcement");
	var btnShowFile = $('#showfile');
	var btnUploadFile = $('#upload');
	var filePath = $('#announcement_file');
	var fileTitle = $('#announcement_file_title');
	var hasUpload =  $('#has_upload');

	// spiner instance	
	retailnet.loader.init();
	
	// tooltips
	retailnet.tooltip.init();

	btnShowFile.hide();

	// form validator
	form.validationEngine();

	// form submit
	form.submit(function(e) {

		e.preventDefault();

		var errors = false;
		var id = $('#announcement_id').val();
		var content = tinyMCE.activeEditor.getContent();

		// assign content
		$('#announcement_text').val(content);
	
		// error trigger
		$('.mce-tinymce.mce-container').toggleClass('error', (content) ? false : true)
		
		// check wisiwyg content
		if (!content) errors = true;
		
		// check file validation
		if (fileTitle.val() && !filePath.val() && !id) {
			errors = true;
		}

		// check file title
		if (!fileTitle.val() && filePath.val()) {
			fileTitle.addClass('error');
			errors = true;
		}

		if (!errors && form.validationEngine('validate')) {

			retailnet.loader.show();

			retailnet.ajax.json(form.attr('action'), form.serialize()).done(function(xhr) {

				retailnet.loader.hide();
				retailnet.notification.hide();

				if (xhr) {
					
					if (xhr.response && xhr.redirect) { 
						window.location=xhr.redirect;
					}
					
					if (xhr.message) {
						if (xhr.response) parent.retailnet.notification.success(xhr.message);
						else parent.retailnet.notification.error(xhr.message);
					}

					if (hasUpload.val() && xhr.path) { 
						filePath.val(xhr.path);
						hasUpload.val('');
					}
				}
			});

		} else {
			retailnet.notification.error('Check red selected fields.');
		}

	});

	// wysiwyg
	tinymce.init({
	    selector: "textarea.tinymce",
	    plugins: [
			"advlist autolink lists link visualblocks code nonbreaking contextmenu paste"
	    ],
	    width: 700,
	    height: 200,
	    menubar : false,
	    statusbar : false,
	    toolbar_items_size: 'small',
		autosave_restore_when_empty: false,
	    toolbar: "styleselect | fontsizeselect | bold italic underline | forecolor | alignleft aligncenter alignright alignjustify | removeformat | paste pastetext pasteword"
	});

	// file uploader
	new AjaxUpload( btnUploadFile, {
		action: '/applications/helpers/ajax.file.upload.php',
		name: 'userfile',
		data: {
			application: application.val(),
			checkExtension: true
		},
		onSubmit: function(file, ext) {

			if (!fileTitle.val()) {
				retailnet.notification.error('Please, set file title and select file again');
				fileTitle.addClass('error');
				return false;
			}
			else {
				retailnet.loader.show();
				fileTitle.removeClass('error');
			}
		},
		onComplete: function(file, response) {

			var json = $.parseJSON(response);
			
			retailnet.loader.hide();
			retailnet.notification.hide();
			
			if(json.response) {
				
				var path = $("<div/>").html(json.path).text();
				filePath.val(path);

				// set upload trigger to on
				hasUpload.val(1);
				
				$('#upload').removeClass('error');
				
				if (json.button) {
					retailnet.notification.success(json.button);
				}

				filePath.trigger('change');
				
			} else if (json.message) {
				retailnet.notification.error(json.message);
			}
		}
	});

	
	// submit form
	$("#save").click(function(e) { 
		e.preventDefault();
		retailnet.notification.hide();
		form.submit();
		return false;
	});

	// file button triggers
	filePath.change(function() {

		path = $(this).val(); 

		if (path) {

			btnShowFile.show();

			btnShowFile.removeClass('disabled');

			if (isImage(path)) {
				$('.icon', btnShowFile).removeClass('download').addClass('picture');
				$('.label', btnShowFile).text('Show Picture');
			} else {
				$('.icon', btnShowFile).removeClass('picture').addClass('download');
				$('.label', btnShowFile).text('Download');
			}
			
		} else {
			btnShowFile.hide();
		}
		
	}).trigger('change');
	
	// show uploaded files
	btnShowFile.click(function(event) {

		event.preventDefault();

		if (!$(this).hasClass('disabled')) {

			path = filePath.val();

			if (isImage(path)) {

				retailnet.modal.show(path, { 
					title: fileTitle.val(),
					closeBtn: true
				});
			}
			else {
				var id = new Date().getTime();
				window.open('http://'+location.host+path+'?id='+id);
			}
		}
	});

	// show dialog window
	$('.dialog-trigger').click(function(e) {


		e.preventDefault();
		
		var button = $(this);
		var dialog = '#'+button.attr('id')+'_dialog';

		retailnet.modal.dialog(dialog, { 
			afterLoad: function() {
				$('#apply').attr('href', button.attr('href'))
			}
		});
	});

	// cancel dialog window
	$('#cancel').bind('click', function(event) {
		event.preventDefault();
		retailnet.modal.close();
		return false;
	});	

	// datapicker
	$('.datepicker').datepicker({ 
		dateFormat: 'dd.mm.yy' ,
		showOtherMonths : true,
		firstDay: 1,
		onClose: function(dateText, inst) {
			
			if ($(this).hasClass('required')) {
				$(this).toggleClass('error', (dateText) ? false : true);
			}
		}
	});

});