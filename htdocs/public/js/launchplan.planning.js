
function sum(list) {
	var sum = 0;
	for (var i = 0; i < list.length; i++) sum += list[i];
	return sum;
}

$(document).ready(function() {

	var spreadsheet,
		application = $('#application').val(),
		launchplan = $('#launchplan').val(),
		formFilters = $('#filters'),
		readonly = $('#readonly').val() || 'false',
		checkApprovedQuantities = $('#check_approved_quantities').val(),
		fixedNotSelectable = ($('#fixedNotSelectable').val()) ? false : true,
		newWarehouseContainer = $('#add_warehouse_dialog'),
		removeWarehouseDialogContainer = $('#remove_warehouse_dialog'),
		confirmDialogContainer = $('#confirm_dialog'),
		partiallyDistributionDialogContainer = $('#partially_distribution_dialog'),
		dataindex = [];

	$(window).resize(function() {
		var self = $('#spreadsheet');
		var offset = self.offset() || {};
		var width = $(window).width()-40;
		var height = $(window).height()-offset.top-20;
		self.removeAttr('style');
		self.width(width);
		self.height(height);
	}).trigger('resize');
	
	// show loader
	retailnet.loader.show();
	
	var url = '/applications/modules/launchplan/planning/load.php';
	var data = $('form.request, #filters').serialize();
	var xhr = retailnet.ajax.request(url,data);
	var grid = xhr.data ? retailnet.json.normalizer(xhr.data) : {};
	
	var spreadsheet = new Spreadsheet('#spreadsheet', grid, {
		readonly: false,
		autoHeight: false,
		fixedPartsNotSelectable: true,
		hiddenAsNull: true,
		fixTop: xhr.top,
		fixLeft: xhr.left,
		merge: xhr.merge ? eval(xhr.merge) : {},
		onModify: function(data) {
			retailnet.notification.hide();
			updateSpreadsheet(data);
		}
	});
	
	spreadsheet.focus();
	retailnet.loader.hide();

	// data history indexing
	$(".spreadsheet .main .cel").each(function() {
		var id = $(this).attr('id');
		var index = id.substr(12);
		var value = $(this).text();
		dataindex[index] = (value) ? Number(value) : 0;
	});
	

// spreadsheet events ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	// togle pos type group
	$(document).on('click', '.row-caption', function(e) {

		if ($('.postype-trigger').length > 1) {

			var self = $(this);
			var tr = self.closest('tr');
			var type = $('.type', tr).attr('type');
			var row = $('.type', tr).attr('row');
			var rows = $('.type_'+type);
		
			if (rows.length > 1) {
				
				//self.toggleClass('close');
				$('.spreadsheet-'+row+' td').toggleClass('close');
				$('.arrow_'+type).toggleClass('arrow-down').toggleClass('arrow-right');

				var data = [];

				$.each(rows, function(i,elem) {
					data.push($(elem).data('row'));
				});

				data.splice(0,1);
				
				if (self.hasClass('close')) {
					spreadsheet.hideRows(data);
				} else {
					spreadsheet.showRows(data);
				}
			}
		}
		
	});
	
// tooltip :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$('.infotip').tipsy({
		html: true,
		gravity: 'nw',
		live: true,
		title: function() {
			
			if ($(this).attr('data-title')) {
				return $(this).attr('data-title');
			} else {
			
				var id = $(this).data('id');
				var section = $(this).data('section');
				var selector = section+'-'+id;
				var elem = $('#'+selector);
	
				if (elem.text().length) {
					return elem.html();
				} else {
					$('.image-loader').trigger('load-data', [this]);
					return $('.image-loader').html();
				}
			}
		}
	});

	$('.image-loader').on('load-data', function(event, tooltip) {

		var id = $(tooltip).data('id');
		var section = $(tooltip).data('section');
		var selector = section+'-'+id;

		
		retailnet.ajax.json('/applications/modules/launchplan/planning/tooltip.php',{ 
			application: application,
			section: section,
			id: id 
		}).done(function(xhr) {
			
			if(xhr && xhr.content) {
				
				$('.tooltips-container').append('<div id="'+selector+'" >'+xhr.content+'</div>');
				
				if ($(tooltip).is(':hover')) {
					$(tooltip).tipsy("show");
				}
			}
		});
	});

	
//	new warehouses :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
    var modalWareHouse = newWarehouseContainer.adoModal({
		width: 400,
		clickClose: false,
		button: '#add_warehouse',
		afterClose: function() {
			
			$('.new-warehouse-container', newWarehouseContainer).hide();
			$('#address_warehouse_id').val('');
			$('input', newWarehouseContainer).val('');
			$('.error', newWarehouseContainer).removeClass('error');
		
			modalWareHouse.resize({height:200});
			retailnet.loader.hide();
			retailnet.notification.hide();
		}
	});

	$(document).on('change','#address_warehouse_id', function(e) {
		
		var val = $(this).val();

		$('.new-warehouse-container').toggle(val=='new' ? true : false); 

		var h = $('.sap-fields', newWarehouseContainer).length > 0 ? 360 : 260;
		var height = val=='new' ? h : 200;

		modalWareHouse.resize({height:height});
	});

	$('a.apply', newWarehouseContainer).on('click', function(e) {
	
		e.preventDefault();
		
		retailnet.notification.hide();
		
		var warehouseForm = $('form', newWarehouseContainer);
		var dropdown = $('#address_warehouse_id');
		var container = $('.new-warehouse-container');		
		var required = $('.required:visible', container);
		var failures = $('.required[value=]:visible', container);

		// remove error marks
		$('.error', newWarehouseContainer).removeClass('error');
		
		// dropdown
		if (!dropdown.val()) {
			dropdown.addClass('error');
			retailnet.notification.error('Please select Warehouse from list.');
			return false;
		}
		
		if (required.length > 0 && failures.length > 0) {
    		$('.required[value=]', newWarehouseContainer).addClass('error');
    		retailnet.notification.error('Red marked fields are required.');
		} else {
		
			var data = warehouseForm.serializeArray();
			data.push({name:'application', value: application});
			data.push({name:'launchplan', value: launchplan});
			data.push({name:'section', value: 'add'});
			
			retailnet.loader.show();

			retailnet.ajax.json(warehouseForm.attr('action'), data).done(function(xhr) {
				
				retailnet.loader.hide();
				
				if (xhr && xhr.response) {
					window.location.reload();
				} 
				
				if (xhr && xhr.message) {
					if (xhr.response) retailnet.notification.success(xhr.message);
					else retailnet.notification.error(xhr.message);
				}
				
				retailnet.modal.close();
			});
		}

		return false;
	});


// remove warehose dialog ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var removeWarehouseDialog = removeWarehouseDialogContainer.adoModal({
		width: 400,
		clickClose: false,
		afterClose: function() {
			$('a.apply', removeWarehouseDialogContainer).removeAttr('href');
			retailnet.loader.hide();
			retailnet.notification.hide();
		}
	});

	// remove warehouse dialog
	$(document).on('click', '.remove-warehouse', function(e) {
	
		e.preventDefault();
		
		retailnet.notification.hide();
	
		var self = $(this);
		var warehouse = $(this).data('id');
	
		if (warehouse) {
			$('a.apply', removeWarehouseDialogContainer).attr('data-id', warehouse);
			removeWarehouseDialog.show();
		} else {
			retailnet.notification.error('Warehouse is not defined.');
		}

		return false;
	});

	// add new warehouse
	$('a.apply', removeWarehouseDialogContainer).on('click', function(e) {

		e.preventDefault();

		var self = $(this);
		var warehouse = self.data('id');
		
		if (warehouse) {
			
			retailnet.ajax.json('/applications/modules/launchplan/planning/warehouse.php', {
				application: application,
				launchplan: launchplan,
				warehouse: warehouse,
				section: 'delete'
			}).done(function(xhr) {
				
				if (xhr && xhr.response) {
					window.location.reload();
				}
				
				if (xhr && xhr.message) {
					retailnet.notification.warning(xhr.message);
				}

				removeWarehouseDialog.close();
			});
		}

		return false;
	});


// confirm dialog ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var modalConfirmDialog = confirmDialogContainer.adoModal({
		width: 400,
		clickClose: false,
		button: '#confirm'
	});

	$('a.apply', confirmDialogContainer).on('click', function(e) {

		e.preventDefault();

		retailnet.notification.hide();

		var button = $('#confirm');
		var url = button.attr('href');
		var data = $('form.request').serialize();
		
		retailnet.loader.show();
		
		retailnet.ajax.json(url, data).done(function(xhr) {
			
			retailnet.loader.hide();
			retailnet.modal.close();

			modalConfirmDialog.close();

			// reload page on success
			if (xhr && xhr.response) {

				if (button.hasClass('popup_close')) {
					
					if (window.opener) {
						window.opener.location.reload();
					}
					
					window.close();
					
				} else {
					window.location.reload();
				}
			}

			// show request message
			if (xhr && xhr.message) {
				if (xhr.response) retailnet.notification.success(xhr.message);
				else retailnet.notification.error(xhr.message);
			}
		});
	});


// partially distribution dialog :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var partiallyDistributionDialog = partiallyDistributionDialogContainer.adoModal({
		width: 400,
		clickClose: false,
		button: '#partially_distribution'
	});

	$('a.apply', partiallyDistributionDialogContainer).on('click', function(e) {

		e.preventDefault();

		retailnet.notification.hide();

		var button = $('#partially_distribution');
		var url = button.attr('href');
		var data = $('form.request').serialize();
		
		retailnet.loader.show();
		
		retailnet.ajax.json(url, data).done(function(xhr) {
			
			retailnet.loader.hide();
			retailnet.modal.close();

			// reload page on success
			if (xhr && xhr.response) {

				if (button.hasClass('popup_close')) {
					
					if (window.opener) {
						window.opener.location.reload();
					}
					
					window.close();
					
				} else {
					window.location.reload();
				}

				partiallyDistributionDialog.close();
			}

			// show request message
			if (xhr && xhr.message) {
				if (xhr.response) retailnet.notification.success(xhr.message);
				else retailnet.notification.error(xhr.message);
			}
		});
	});



//	filters ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	// build dropdown UI skins
	$('select', $('#filters')).dropdown();

	// confirmation versions
	$('#version').change(function() {
		var value = $(this).val();
		var url = $('#url').val();
		var link = (value) ? url+'/'+value : url;
		window.location = link;
	});

	// reload page when filters are changed
	$('select', $('#filters')).change(function() {
		$('#filters').submit();
	});

	var popFilter = $('#pop_filter');
	
	popFilter.pop().click(function() {
		$('.icon',this).toggleClass('direction-up');
	});

	popFilter.pop('hide');

	
	
//	spreadsheeet updater :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	var updateSpreadsheet = function(data) {
		
		if (data) {
				
			$.each(data, function(i,cell) {
				
				if (cell && cell.tag != null) {

					var error = '';
					var value = Number(cell.value);
					
					var tag = cell.tag.split(';');
					var section = tag[0];			// section name
					var item = tag[1];				// launchplan item id
					var entity = tag[2];			// pos_id or warehouse_id
					var reference = tag[3];			// reference_id
					var id = tag[4];				// quantity_id

					
					if (!section || !item || !entity || !reference) {
						error = 'Action params wrong. Check cel tag definition.';
					}

					if (!error && isNaN(value)) {
						error = 'Please only integer numbers';
					}

					if (!error && value) {
						error = value%1===0 ? '' : 'Please only integer numbers';
					}

					if (error) {
						spreadsheet.setValue(cell.name, dataindex[cell.name] )
						retailnet.notification.error(error);
						return false;
					} 

					retailnet.ajax.json('/applications/modules/launchplan/planning/quantity.ajax.php', {
						application: application,
						launchplan: launchplan,
						section: section,
						item: item,
						entity: entity,
						reference: reference,
						id: id,
						quantity : value
					}).done(function(xhr) {
						
						if (xhr && xhr.id) { 
							
							// store quantitie in dataindex
							dataindex[cell.name] = value;
							
							// store cell tag in spreadsheet
							// new tag content new inserted ID
							spreadsheet.setValue(cell.name, {
								tag: section+';'+item+';'+entity+';'+reference+';'+xhr.id
							});
						}

						if (xhr && xhr.message) {
							if (xhr.response) retailnet.notification.success(xhr.message);
							else retailnet.notification.error(xhr.message);
						}
					})
				}
			});
		}
	}

	// reset verification
	$('.print-ajax').on('click', function(e) {
		
		e.preventDefault();
		
		retailnet.loader.show();
		
		retailnet.ajax.json($(this).attr('href'), formFilters.serialize()).done(function(xhr) {
			
			xhr = xhr || {}

			if (xhr.errors && xhr.errors.length) {
				
				$.each(xhr.errors, function(i, message) {
					retailnet.notification.error(message);
				})
				
				return;
			}

			if (xhr.success && xhr.file) {
				window.open(xhr.file);
			}
			
		}).complete(function() {
        	retailnet.loader.hide();
		});
		
		return false;
	});
	
});