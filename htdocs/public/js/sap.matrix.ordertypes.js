$(document).ready(function() {

	var $container = $("#matrix");
	var sources = retailnet.ajax.request('/applications/modules/sap/matrix/order.types.php', $('form.request').serializeArray());

	var headers = sources.headers || [];
	var columns = sources.columns || [];
	var data = sources.data || [];

	// show errors on load
	if (sources && sources.errors) {
		$.each(sources.errors, function(i, message) {
			retailnet.notification.error(message);
		})
	}

	var colWidths = [];

	if (columns.length) {
		$.each(columns, function(i,col) {
			colWidths.push(col.width);
		})
	}

	$container.handsontable({
		data: data,
		width: 800,
		startRows: 7,
		startCols: 4,
		colHeaders: headers,
		colWidths: colWidths,
		columnSorting: false,
		columns: columns,
		afterChange: function(changes, source) { 
			
			if (changes) {

				var row = changes[0][0];
				var col = changes[0][1];
				var val = changes[0][3];

				var col = col.split('.');
				var client = handsontable.getDataAtRowProp(row, 'client.id');
				var channel = handsontable.getDataAtRowProp(row, col[0]+'.id');

				retailnet.ajax.json('/applications/modules/sap/matrix/order.type.submit.php', {
					client: client,
					channel: channel,
					value: val
				}).done(function(xhr) {

					if (xhr && xhr.reload) { 
						window.location.reload();
					}

					if (xhr && xhr.notifications) {
						$.each(xhr.notifications, function(i, message) {
							retailnet.notification.success(message);
						})
					}

					if (xhr && xhr.errors) {
						$.each(xhr.errors, function(i, message) {
							retailnet.notification.error(message);
						})
					}
				})
			}
		}
	})

	var handsontable = $container.data('handsontable');

});