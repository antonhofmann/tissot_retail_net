$(document).ready(function() {

	var form = $("form.validator");
	
	// spiner instance	
	retailnet.loader.init();
	
	// tooltip instance	
	retailnet.tooltip.init();

	// validator
	form.validationEngine({
		autoHidePrompt: true,
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		onValidationComplete: function(form, status){
			if (status === true) {
				retailnet.loader.show();
			} else {
				return false;
			}
		},
		onAjaxFormComplete: function(status, form, json, options) {

			retailnet.loader.hide();
			retailnet.notification.hide();

			if (json) {

				if (json.response && json.redirect) { 
					window.location=json.redirect;
				}

				if (json.message) {
					if (json.response) retailnet.notification.success(json.message);
					else retailnet.notification.error(json.message);
				}

				// update fields
				if (json.fields) {
					
					for (var field in json.fields) {
						
						var value = json.fields[field];
						var el = $('[name="'+field+'"]');

						if (el.is(':text') || el.is(':hidden') || el.is('select')) {
							el.val(value);
						}

						if (el.is(':checkbox') || el.is(':radio')) {
							el.prop('checked', value ? true : false);
						}
					}
				}

				if (json.response) {
					form.trigger('afterSuccessSubmit', [json]);
				}
			}
		}
	});
	
	// submit from
	$("#save").click(function(event) { 
		event.preventDefault();
		retailnet.notification.hide();
		form.submit();
		return false;
	});

	// delete from
	$('.dialog').click(function(event) {

		event.preventDefault();

		var button = $(this);
		
		$('#apply, a.apply').attr('href', button.attr('href'));
		
		retailnet.modal.dialog('#'+button.attr('id')+'_dialog');
	});

	// cancel action
	$('#cancel, .cancel').click(function(event) {
		
		event.preventDefault();
		
		retailnet.modal.close();
	
		return false;
	});

});