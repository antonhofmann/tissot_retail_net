var Newsletter = Newsletter || {};

$(document).ready(function() {

	Newsletter.id = $('#newsletter_id').val();

	var URL = '/applications/modules/news/newsletter/newsletter.recipients.php';

	$(function() {

		Adomat.Ajax.post(URL, {
			application: 'news',
			section: 'load',
			id: Newsletter.id
		}).done(function(xhr) {

			xhr = xhr || {};

			if (xhr.data) {
				// form dataloader
				Newsletter.dataloader(xhr.data)
				// breadcrumps builder
				Newsletter.breadcrumps(xhr.data.newsletter_publish_state_id);
			}

			// info block
			if (xhr.info) {
				for(var e in xhr.info) {
					$('#'+e).text(xhr.info[e])
				}
			}

			// alerts
			Newsletter.alerts(xhr.alerts);

			// dialogs
			Newsletter.dialogs(xhr.dialogs);

			$( "#newsletter_send_date" ).datepicker( "option", "minDate", 0 );
			$('#newsletter_send_date').trigger('afterChange');

			// user dependent visibility
			Newsletter.visibility(xhr.disabled, xhr.remove);
		})
	})


	// set cronjob send date :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$('#newsletter_send_date').on('change', function() {

		var self = $(this);
		var val = self.val();

		self.val('');
		self.parent().removeClass('has-error');

		if (!val) return false;

		var parts = val.split('.');
		var today = new Date();
		var date = new Date(parts[2],parts[1]-1,parts[0]);

		// check date format
		if (!date) {
			self.parent().addClass('has-error');
			Adomat.Notification.error('Invalid date', {title: 'Error'});
			return false;
		}	
		
		// chake date in past
		if (today.getTime() > date.getTime()) {
			self.parent().addClass('has-error');
			Adomat.Notification.error('The <b>Newsletter Send Date</b> can not be a date in the past!', {title: 'Error'});
			return false;
		}

		// check recipient list
		if ($('input.recipient:checked').length==0) {
			Adomat.Notification.error('Newsletter hasn\'t recipients.', {title: 'Error'});
			return false;
		}

		var message = "This newsletter is sent on "+val+". Are you sure?";

		BootstrapDialog.confirm(message, function(confirmed) {
			if (confirmed) {
				self.val(val).trigger('save');	
			}
		})

		return false;
	})

	$('#newsletter_send_date').on('afterChange', function(e) { 

		var self = $(this);
		var val = self.val();

		self.next().toggle(val ? true : false);
	})

	$('.remove-terminated-date').on('click', function() {
		BootstrapDialog.confirm("Are you sure?", function(confirmed) {
			if (confirmed) {
				$('#newsletter_send_date').val('').trigger('save');			
			}
		})	
	})

	$('#newsletter_send_date').on('save', function() {

		var self = $(this);
		var val = self.val();

		Adomat.Ajax.post(Newsletter.url, {
			application: 'news',
			section: 'save',
			id: Newsletter.id,
			field: self.attr('name'),
			value: val
		}).done(function(xhr) {

			xhr = xhr || {};

			if (xhr.success) {
				self.val(val);
			}

			if (xhr.success && xhr.redirect) {
				window.location=xhr.redirect;
			}

			if (xhr.notifications) {
				$.each(xhr.notifications, function(i, message) {
					Adomat.Notification.show(message);
				})
			}

			self.trigger('afterChange');
		})	
	})


	// recipents list ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$(document).on('change', 'input.recipient', function(e, chackAllTriggered) {

		var self = $(this);
		var group = self.closest('table');
		var ckecked = self.is(':checked') ? 1 : 0;
		var section = group.data('section');
		var manualTriggered = chackAllTriggered ? false : true;

		Adomat.Ajax.post(URL, {
			application: 'news',
			section: section,
			id: Newsletter.id,
			role: group.data('id'),
			recipient: self.data('id'),
			action: ckecked
		}).done(function(xhr) {
			$('input.group-recipients', group).trigger('changed', [manualTriggered]);
			self.closest('.tab-pane').trigger('changed');
		})
	})

	$(document).on('click', 'input.group-recipients', function(e) {
		
		var self = $(this);
		var group = self.closest('table');
		var checked = self.is(':checked');
		var checkboxes = [];

		$('input.recipient', group).prop('checked', checked)//.trigger('change', [true]);

		if (checked) {
			checkboxes = $('input.recipient', group).map(function() {
				return $(this).data('id');
			}).get();
		}

		Adomat.Loader.show();

		Adomat.Ajax.post(URL, {
			application: 'news',
			section: 'group-'+group.data('section'),
			id: Newsletter.id,
			role: group.data('id'),
			recipients: checkboxes
		}).done(function(xhr) {
			$('input.group-recipients', group).trigger('changed');
			self.closest('.tab-pane').trigger('changed');
			Adomat.Loader.hide();
		})
		
	})

	// total group recipients
	$('input.group-recipients').on('changed', function(e, checkState) {
		
		e.stopImmediatePropagation();

		var self = $(this);
		var group = self.closest('table'); 
		var recipients = $('input.recipient', group).length;
		var recipientsSelected = $('input.recipient:checked', group).length;
		
		if (recipients && checkState) {
			self.prop('checked', recipients==recipientsSelected ? true : false)
		}
		
		// total group recipients
		var label = $('.total-group-recipients', group).data('caption') || 'recipients';

		$('.total-group-recipients', group).toggle(recipientsSelected>0 ? true : false);
		$('.total-group-recipients', group).text(recipientsSelected);

	}).trigger('changed', [true]);

	// total section recipients
	$('.recipients .tab-pane').on('changed', function(e) {
		
		var self = $(this);
		var id = self.attr('id'); 
		var selected = $('input.recipient:checked', self).length;
		
		$('#tab-'+id+' .badge').toggle(selected>0 ? true : false);
		$('#tab-'+id+' .badge').text(selected);
		$('#total-'+id).text(selected);

	}).trigger('changed');


	// test mail :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$(document).on('click', '#action-test-newsletter', function(e) {

		e.stopImmediatePropagation()
		/*
		if ($('input.recipient:checked').length==0) {
			Adomat.Notification.error('Newsletter hasn\'t recipients.', {title: 'Error'});
			return false;
		}
		*/

		TestMailDialog.open();

		return false;
	})

	var TestMailDialog = new BootstrapDialog({
		title: 'Send mail to',
		message: $('#template-test-mail').html().replace(/(\r\n|\n|\r)/gm,""),
		autodestroy: false,
		buttons: [
			{
				label: 'Cancel',
				cssClass: 'btn-default pull-left text-mutted',
				action: function(dialogItself){
					TestMailDialog.close();
				}
			},
			{
				id: 'btnSentTestMail',
				cssClass: 'btn-primary',
				icon: 'fa fa-envelope',
				label: 'Send Mail',
				action: function(dialog) {
					
					var $button = this;
					var $content = dialog.getModalBody();
					var $form = $content.find('form');

					// required controll
					$('.has-error', $form).removeClass('has-error');

					$('input.required', $form).filter(function() { 
						return $(this).val() == ""; 
					}).parent().addClass('has-error'); 

					if ($content.find('.has-error').length) {
						Adomat.Notification.error('Red marked fields are mandatory', {title: 'Error'});
						return false;
					} 

					dialog.enableButtons(false);
					dialog.setClosable(false);
					$button.toggleSpin(true);

					// from data
					var data = $form.serializeArray();
					data.push({name: 'application', value: 'news'});
					
					Adomat.Ajax.post($form.attr('action'), data).done(function(xhr) {

						xhr = xhr || {};

						dialog.setClosable(true);
						dialog.enableButtons(true);
						$button.toggleSpin(false);

						if (xhr && xhr.success) {
							//$form.reset();
							dialog.close();
						}

						if (xhr.message) {
							Adomat.Notification.success(xhr.message);
						}

						if (xhr && xhr.errors) {
							$.each(xhr.errors, function(i, msg) {
								Adomat.Notification.error(msg);
							})
						}
					})
				}
			}
		],
		onhidden: function(dialog){
			dialog.getModalBody().find('form').trigger("reset");
		},
		onshown: function(dialog) {
			dialog.getModalBody().find('form').on('submit', function(e) {
				e.preventDefault();
				return false;
			})
		}
	})


	// send mail :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$(document).on('click', '#action-send-newsletter', function(e) {

		e.preventDefault();

		var self = $(this);
		var url = self.prop('href') || self.data('url');
		var msg;

		if (self.data('message')) {
			msg = self.data('message');
			msg = $(msg).length>0 ? $(msg).html() : msg;
		} 
		else msg = 'Are your sure?';

		if (!url) {
			Adomat.Notification.error('Action URL is not defined.', {title: 'Error'});
			return false;
		}
		
		if ($('input.recipient:checked').length==0) {
			Adomat.Notification.error('Newsletter hasn\'t recipients.', {title: 'Error'});
			return false;
		}
		
		BootstrapDialog.confirm(msg, function(confirmed) {
			if (confirmed) sendNewsletters(url);
		})

		return false;
	})


	var sendNewsletters = function(url) {

		Adomat.Loader.show();

		Adomat.Ajax.post(url).done(function(xhr) {

			xhr = xhr || {}

			Adomat.Loader.hide();

			if (xhr.success && xhr.reload) {
				window.location.reload();
			}

			if (xhr.success) {
				$('#newsletter_send_date').val();
				$('input[type=checkbox]').prop('checked', false);
				$('.flat-tabs .badge').hide();
			}

			if (xhr && xhr.notifications) {
				$.each(xhr.notifications, function(i, message) {
					Adomat.Notification.show(message);
				})
			}
		})	
	}


	// add additional recipient ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	var formAdditionalRecipient = $("#addAdditionalRecipient");

	formAdditionalRecipient.on('submit', function(e) {

		e.preventDefault();

		var self = $(this);
		var fieldsRequired = $('.required', self);

		var fieldsRequiredFailure = $('input.required', self).filter(function() { 
			return $(this).val() == ""; 
		})
		
		self.find('.has-error').removeClass('has-error');

		if (fieldsRequired.length > 0 && fieldsRequiredFailure.length > 0 ) {
			fieldsRequiredFailure.parent().addClass('has-error'); 
			return false;
		}

		var data = self.serializeArray();
		data.push({name: 'application', value: 'news'});
		data.push({name: 'id', value: Newsletter.id });
		data.push({name: 'section', value: 'add-additional-recipient'});

		Adomat.Ajax.post(URL,data).done(function(xhr) {

			var xhr = xhr || {};
			var recipient = xhr.recipient || {};

			if (xhr.success && recipient) {

				var tr = $("<tr />");
				var td = $('<td />').attr('colspan', 3);

				td.append('<div class="checkbox"><label><input type="checkbox" class="recipient additional-recipient" data-id="'+recipient.id+'" > '+recipient.name+'</label></div>');
				tr.addClass('warning').append(td)

				$("html, body").animate({ scrollTop: $(document).height() }, "slow");
				
				setTimeout(function() { 
					tr.removeClass('warning'); 
				}, 5000);


				$('#body-additional').append(tr);
				self.find('input,select').val('');
				$('#btnAddAdditionalRecipient').trigger('click');
			}

			if (xhr.notifications) {
				$.each(xhr.notifications, function(i, message) {
					Adomat.Notification.show(message);
				})
			}
		})

		return false;
	})
})