
function sum(list) {
		
	var sum = 0;
	for (var i = 0; i < list.length; i++) {
		sum += list[i];
	}
	
	return sum;
}

$(document).ready(function() {

	var spreadsheet,
		application = $('#application').val(),
		controller = $('#controller').val(),
		action = $('#action').val(),
		sendmailTemplate = $('#sendmail-template'),
		previewMailTemplate = $('#preview-mail-template'),
		testMailTemplate = $('#test-mail-template'),
		sendmail = $('#sendmail'),
		editMailContent = $('#mail_template_view_modal');
		launchplan = $('#launchplan').val(),
		readonly = $('#readonly').val() || 'false',
		fixedNotSelectable = ($('#fixedNotSelectable').val()) ? false : true,
		dataindex = [];
	
	retailnet.loader.init();

// loader ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	var url = '/applications/modules/launchplan/items.php';
	var data = $('form.request').serialize();
	var xhr = retailnet.ajax.request(url, data);
	var grid = xhr.data ? retailnet.json.normalizer(xhr.data) : {};
	
	var spreadsheet = new Spreadsheet('#spreadsheet', grid, {
		readonly: false,
		autoHeight: true,
		fixedPartsNotSelectable: true,
		hiddenAsNull: true,
		fixTop: xhr.top,
		fixLeft: xhr.left,
		merge: xhr.merge ? eval(xhr.merge) : {},
		onModify: function(data) {
			retailnet.notification.hide();
			updateSpreadsheet(data);
		},
		selectionHandler: function(cel, selection) {
		 	
		 	selection.removeClass('selection-disabled').removeClass('selection-cel selection-editabled');
            
            if (!$('#spreadsheet-'+cel).hasClass('cel')) {
                selection.addClass('selection-disabled');
            }  

            if ($('#spreadsheet-'+cel).hasClass('cel')) {
                selection.addClass('selection-cel');
            }

            if ($('#spreadsheet-'+cel).hasClass('cel-input') || $('#spreadsheet-'+cel).hasClass('cel-revision')) {
                selection.addClass('selection-editabled');
            }
        }
	});

	// hide revision colum
	if (xhr && xhr.hideRevisionColumn) {
		spreadsheet.hideColumns(xhr.revisionColumn);
	}
	
	// add focus
	spreadsheet.focus();

	// hide grid
	retailnet.loader.hide();

	// data history indexing
	$(".spreadsheet .main .cel").each(function() {
		var id = $(this).attr('id');
		var index = id.substr(12);
		var value = $(this).text();
		dataindex[index] = (value) ? Number(value) : 0;
	});


// wysiwyg :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	tinymce.init({
		selector: "#mail_template_text",
		plugins: ["advlist autolink lists textcolor link visualblocks code nonbreaking contextmenu paste preview"],
		width: '100%',
		height: 400,
		menubar : false,
		statusbar : false,
		toolbar_items_size: 'small',
		autosave_restore_when_empty: false,
		paste_as_text: true,
		toolbar: "fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor | removeformat | paste pastetext pasteword",
		setup : function(ed) {
			ed.on('change', function(e) {
				$('#mail_template_text').val(ed.getContent())
			});
		}
	});	


// submitting ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$('a.submit-items').click(function(e) {

		e.preventDefault();
		
		retailnet.notification.hide();

		var error = false;
		var messages = '';
		var self = $(this);
		var url = self.attr('href');
		var data = $('form.request, #spreadsheet :input').serialize();
		var sendmailTrigger = self.hasClass('sendmail') ? 1 : 0;
		var template = self.data('template');

		// sendmail trigger
		sendmail.val(sendmailTrigger);

		if (template && template != $('#mail_template_id').val() ) {
			
			var data = { id: template }
			var sender = retailnet.ajax.request('/applications/helpers/ajax.user.php', {section: 'sender'});
			var dataloader = retailnet.ajax.request('/applications/modules/launchplan/launchplan.ajax.php', {id:launchplan, section: 'data', application: application});
			data.render = $.extend(sender, dataloader);

			var mail = retailnet.ajax.request('/applications/helpers/ajax.mailtemplate.php', data);

			if (mail && mail.mail_template_id) {
				
				$('#mail_template_id').val(mail.mail_template_id);
				$('#mail_template_view_modal').val(mail.mail_template_view_modal);
				$('#mail_template_subject').val(mail.mail_template_subject);
				$('#mail_template_text').val(mail.mail_template_text);

				var editor = tinymce.get('mail_template_text');

				if (editor) {
					editor.setContent(mail.mail_template_text);
					tinyMCE.triggerSave();
				}
			}		
		}

		if (self.attr('id')=='revision') {
			var revisions = $('#spreadsheet :input').length;
			var inRevisions = $('#spreadsheet :input:checked').length;
			error = revisions>0 && inRevisions==0 ? true : false; 
			messages = 'Please, select at least one Item in Revision';
		}
		
		if (!error) {

			if (sendmail.val()==1 && editMailContent.val()==1) {
				$('a.sendmail', sendmailTemplate).attr('href', url);
				modalMailTemplate.resize().show();
			}
			else submitItems(url);

		} else retailnet.notification.error(messages);

		return false;
	});


	var submitItems = function(url) {

		if (url) {
			
			var data = $('form.request, #spreadsheet :input, #sendmail-template form').serialize();

			retailnet.loader.show();

			retailnet.ajax.json(url, data).done(function(xhr) {
					
				var response = xhr || {};
				var reloadPage = false;

				retailnet.loader.hide();

				// show request message
				if (response.message) {
					if (response.response) retailnet.notification.success(response.message);
					else retailnet.notification.error(response.message);
				}

				// required to send mails
				if (response.response && response.sendmail==1) {
					
					var sendMailRespone = sendMails() || {};

					if (sendMailRespone.response) retailnet.notification.success(sendMailRespone.notification);
					else retailnet.notification.error(sendMailRespone.notification);

					reloadPage = sendMailRespone.response ? response.reload : false;
					
				} else {
					reloadPage = response.reload;
				}

				// page reloader / form reset
				if (reloadPage) window.location.reload();
			});

		} else retailnet.notification.error('URL not defined.');
	}


// sendmail ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var modalMailTemplate = sendmailTemplate.adoModal({
		width: 800,
		height: 700,
		clickClose: false
	});

	// button: send mail
	$('.sendmail', sendmailTemplate).on('click', function(e) {

		e.preventDefault();
		
		var self = $(this),
			url = self.attr('href'),
			required = $('.required', sendmailTemplate),
			failures = $('.required[value=]', sendmailTemplate);

    	if (required.length > 0 && failures.length > 0) {
    		$('.required', sendmailTemplate).removeClass('error');
    		$('.required[value=]', sendmailTemplate).addClass('error');
    		retailnet.notification.error('Red marked fields are required.');
    	} else {
    		modalMailTemplate.close();
			submitItems(url); 
    	}
	});
	
	var sendMails = function() {

		var self = $(this);
		var url = $('form', sendmailTemplate).attr('action');
		var data = $('#sendmail-template form').serialize();

		if (launchplan) {
			
			retailnet.loader.show();
			
			var xhr = retailnet.ajax.request(url, data);

			if (xhr) retailnet.loader.hide();

			return xhr;

		} else {
			retailnet.notification.error('Please, select at least one launch plan.');
		}
	}


// preview mail :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var modalMailPreview = previewMailTemplate.adoModal({
		width: 1000,
		height: 600,
		overlay: {
			opacity: 0.75,
			background: '#000'
		}
	});

	$('.preview-mail', sendmailTemplate).on('click', function(e) {

		e.preventDefault();
		
		var self = $(this);
		var url = self.attr('href');
		var data = $('#sendmail-template form').serialize();
		
		if (launchplan) {
			
			retailnet.loader.show();
			
			retailnet.ajax.json(url,data).done(function(xhr) {

				retailnet.loader.hide();

				if (xhr && xhr.response) {

					// assign data from request
					$('.mail-subject', previewMailTemplate).html(xhr.subject);
					$('.mail-address', previewMailTemplate).text(xhr.email);
					$('.mail-cc-address', previewMailTemplate).text(xhr.cc);
					$('.mail-cc-address', previewMailTemplate).parent().toggle(xhr.cc ? true : false);
					$('.mail-date', previewMailTemplate).text(xhr.date);
					$('.mail-content', previewMailTemplate).html(xhr.content);
					$('.mail-footer', previewMailTemplate).html(xhr.footer);
					$('.mail-footer', previewMailTemplate).parent().toggle(xhr.footer ? true : false);

					modalMailPreview.resize().show();

				} else {
					modalMailPreview.close();
				}

				if (xhr && xhr.notification) {
					if (xhr.response) retailnet.notification.success(xhr.notification);
					else retailnet.notification.error(xhr.notification);
				}
			});
			
		} else {
			retailnet.notification.error('Please, select at least one launch plan.');
		}

	});

// test mail :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var modalTestMail = testMailTemplate.adoModal({
		width: 400
	});

	$('.test-mail', sendmailTemplate).on('click', function(e) {
		e.preventDefault();
		modalTestMail.show();
	});

	$('.apply', testMailTemplate).on('click', function(e) {
		
		e.preventDefault();

		var url = $(this).attr('href');
		var data = $('#sendmail-template form').serializeArray();
		var recipient = $('#test-mail-recipient').val();
		
		if (recipient) {

			data.push({
				name: 'test_recipient', 
				value: recipient
			});
			
			retailnet.loader.show();
			
			retailnet.ajax.json(url,data).done(function(xhr) {

				if (xhr && xhr.response) {
					modalTestMail.close();
				}

				if (xhr && xhr.notification) {
					if (xhr.response) retailnet.notification.success(xhr.notification);
					else retailnet.notification.error(xhr.notification);
				}

			}).complete(function() {
				retailnet.loader.hide();
			});
			
		} else {
			retailnet.notification.error('Recipient e-mail is required.');
		}
	});

// spreadsheet updater :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var updateSpreadsheet = function(data) {
		
		if (data) {
				
			$.each(data, function(i,cell) {
				
				if (cell && cell.tag != null) {

					var error = false;
					var message = '';
					var value = Number(cell.value);
					
					var tag = cell.tag.split(';');
					var sectionName = tag[0];
					var item = tag[1];
					var week = tag[2];
					var weekID = tag[3];

					if (!sectionName) {
						error = true;
						message = 'Spreadsheet error: TAG attribute is not defined.';
					}

					if (!error && isNaN(value)) {
						error = true;
						message = 'Please only integer numbers';
					}

					if (!error && value) {
						error = value%1===0 ? false : true;
						message = error ? 'Please only integer numbers' : null;
					}
					
					if (error) {
						spreadsheet.setValue(cell.name, dataindex[cell.name] )
						retailnet.notification.error(message);
						return false;
					} 
					
					retailnet.ajax.json('/applications/modules/launchplan/item.ajax.php', {
						application: application,
						launchplan: launchplan,
						section: sectionName, 
						item: item,
						week: week,
						weekID: weekID,
						value : value
					}).done(function(xhr) {
						
						if (xhr && xhr.id) { 
							
							// store quantitie in dataindex
							dataindex[cell.name] = value;
							
							// store cell tag in spreadsheet
							spreadsheet.setValue(cell.name, {
								tag: sectionName+';'+item+';'+week+';'+xhr.id
							});
						}

						if (xhr.message) {
							if (xhr.response) retailnet.notification.success(xhr.message);
							else retailnet.notification.error(xhr.message);
						}
					});
				}
			});
		}
	}
	
});