$(document).ready(function() {

	var form = $("#staff_form");
	var id = $('#mps_staff_id');
	
	// spiner instance	
	retailnet.loader.init();
	
	// tooltip instance	
	retailnet.tooltip.init();

	form.validationEngine({
		autoHidePrompt: true,
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		onAjaxFormComplete: function(status, form, json, options) {

			retailnet.loader.hide();
			retailnet.notification.hide();

			if (json) {

				if (json.response && json.redirect) { 
					window.location=json.redirect;
				}

				if (json.message) {
					if (json.response) retailnet.notification.success(json.message);
					else retailnet.notification.error(json.message);
				}
			}
		}
	});
	
	$("#save").click(function(event) { 
		
		event.preventDefault();
		
		if (form.validationEngine('validate')) {
			retailnet.loader.show();
			form.submit();
		} else {
			retailnet.notification.error('Please check red marked fields.');
		}
	});

	$('.dialog').click(function(event) {
		
		event.preventDefault();

		var button = $(this);
		
		$('#apply').attr('href', button.attr('href'));
		
		retailnet.modal.dialog('#'+button.attr('id')+'_dialog');
	});

	$('#cancel').bind('click', function(event) {
		event.preventDefault();
		retailnet.modal.close();
		return false;
	});	

});