$(document).ready(function() {

	var $calendar = $('#calendar'),									// calendar instance
	increment = $('#increment'),									// current opening hours auto inrement value
	pos = $('#id').val(),											// pos id
	use24Hour = ( $('#timeformat').val() > 12 ) ? true : false,		// 24 hour time format
	timeFormat = (use24Hour) ? 'H:i' : 'h:i a',						// time format pattern
	readonly = ($('#readonly').val()) ? true : false,				// grid readonly
	copyfrom = $('#copyfrom'),										// popover box
	loaderTrigger = $('#loaderTrigger');							// dropdown loader trigger

	// calendar builder
	$calendar.weekCalendar({
		date : new Date(2012,0,2), // fix calendar on first monday in year 2012
		timeslotsPerHour : 2,
		timeslotHeight : 14,
		businessHours : {
			start : 0,
			end : 24,
			limitDisplay : false
		},
		daysToShow : 7,
		readonly: readonly,
		use24Hour : use24Hour,
		timeFormat: timeFormat,
		dateFormat: false, 
		allowCalEventOverlap: true,
		showHeader: false,
		firstDayOfWeek: 1,
		textSize: 11,
		newEventText: 'New Opening Time',
		height : function($calendar) {
			return 720;
		},
		draggable : function(calEvent, $event) {
			return calEvent.readOnly != true;
		},
		resizable : function(calEvent, $event) { 
			
			return calEvent.readOnly != true;
		},
		eventNew : function(calEvent, $event) {

			var $dialogContent = $("#event_edit_container");

			resetCalenderForm($dialogContent);

			var startField = $dialogContent.find("select[name='start']").val(calEvent.start);
			var endField = $dialogContent.find("select[name='end']").val(calEvent.end);
			
			calenderCheckBoxes(calEvent);
			
			$dialogContent.dialog({
				modal : true,
				width: 320,
				resizable: false,
				title : "New Opening Time",
				close : function() {
					$dialogContent.dialog("destroy");
					$dialogContent.hide();
					$('#calendar').weekCalendar("removeUnsavedEvents");
				},
				buttons : {
					save : function() {
						
						calEvent.id = increment.val();
						calEvent.start = new Date(startField.val()); 
						calEvent.end = new Date(endField.val());

						var id = calEvent.id+1;
						increment.val(id);
						
						//post to data.php
						setCalenderData('save',calEvent);

						$calendar.weekCalendar("removeUnsavedEvents");
						$calendar.weekCalendar("updateEvent",calEvent);
						$dialogContent.dialog("close");
					},
					cancel : function() {
						$dialogContent.dialog("close");
					}
				}
			}).show();

			$dialogContent.find(".date_holder").text($calendar.weekCalendar("formatDate",calEvent.start));
			setupStartAndEndTimeFields(startField,endField, calEvent, $calendar.weekCalendar("getTimeslotTimes",calEvent.start));

		},
		eventDrop : function(calEvent, $event) { 
			$('input[name^=day]').attr('checked',false).attr('disabled',true);
			setCalenderData('save', calEvent);
		},
		eventResize : function(calEvent, $event) { 
			$('input[name^=day]').attr('checked',false).attr('disabled',true);
			setCalenderData('save', calEvent);
		},
		eventClick : function(calEvent, $event) { 

			if (readonly) { 
				return;
			}

			var $dialogContent = $("#event_edit_container");
			
			resetCalenderForm($dialogContent);
			
			var startField = $dialogContent.find("select[name='start']").val(calEvent.start);
			var endField = $dialogContent.find("select[name='end']").val(calEvent.end);

			//$('.dayboxes').hide();
			//$('input[name^=day]').attr('checked',false).attr('disabled',true);
			
			calenderCheckBoxes(calEvent, 'edit');

			$dialogContent.dialog({
				modal : true,
				width: 320,
				resizable: false,
				title : "Edit Opening Time",
				close : function() {
					$dialogContent.dialog("destroy");
					$dialogContent.hide();
					$('#calendar').weekCalendar("removeUnsavedEvents");
				},
				buttons : {
					save : function() {

						calEvent.start = new Date(startField.val());
						calEvent.end = new Date(endField.val());

						setCalenderData('save', calEvent);
						
						$calendar.weekCalendar("updateEvent",calEvent);
						$dialogContent.dialog("close");
					},
					"delete" : function() {
						setCalenderData('delete', calEvent);
						$calendar.weekCalendar("removeEvent",calEvent.id);
						$dialogContent.dialog("close");
					},
					cancel : function() {
						$dialogContent.dialog("close");
					}
				}
			}).show();

			var startField = $dialogContent.find("select[name='start']").val(calEvent.start);
			var endField = $dialogContent.find("select[name='end']").val(calEvent.end);
			
			$dialogContent.find(".date_holder").text($calendar.weekCalendar("formatDate",calEvent.start));
			setupStartAndEndTimeFields(startField,endField, calEvent, $calendar.weekCalendar("getTimeslotTimes",calEvent.start));
			$(window).resize().resize();

		},
		eventMouseover : function(calEvent, $event) {
		},
		eventMouseout : function(calEvent, $event) {
		},
		noEvents : function() {
		}, 
		data : function(start, end, callback) {
			$.getJSON('/applications/helpers/pos.openinghours.php', {action:'load', pos:pos }, function(json) { 
				callback(setCalenderEvents(json));
			});
	    }
	});

	function setCalenderEvents(json) {

		var events = [];
		
		$(json).each(function( i, data ) {

			var startDate = new Date(data.start);
			var endDate = new Date(data.end);
			var obj = {};

			obj.id = data.id;
			obj.start = data.start + startDate.getTimezoneOffset()*60*1000;
			obj.end = data.end + endDate.getTimezoneOffset()*60*1000;
			events.push(obj);
		});

		return events;
	}

	function resetCalenderForm($dialogContent) {
		$dialogContent.find("input").val("");
		$dialogContent.find("textarea").val("");
		$dialogContent.find("select[name='start']").empty();
		$dialogContent.find("select[name='end']").empty();
	}

	function setupStartAndEndTimeFields($startTimeField, $endTimeField, calEvent, timeslotTimes) {

		for ( var i = 0; i < timeslotTimes.length; i++) {
			
			if (timeslotTimes[i]) { 

				var startTime = timeslotTimes[i].start;
				var endTime = timeslotTimes[i].end;
				var startSelected = "";

				if (startTime.getTime() === calEvent.start.getTime()) {
					startSelected = "selected=\"selected\"";
				}
				var endSelected = "";

				if (endTime.getTime() === calEvent.end.getTime()) {
					endSelected = "selected=\"selected\"";
				}

				$startTimeField.append("<option value=\""
					+ startTime + "\" " + startSelected + ">"
					+ timeslotTimes[i].startFormatted
					+ "</option>");

				$endTimeField.append("<option value=\"" + endTime
					+ "\" " + endSelected + ">"
					+ timeslotTimes[i].endFormatted
					+ "</option>");
			}
		}
		
		$endTimeOptions = $endTimeField.find("option");
		$startTimeField.trigger("change");
	}
	
	function setCalenderData(action, calEvent) {
	
		var data = $('input[name^=day]').serializeArray();
		data.push({name: 'action', value: action });
		data.push({name: 'pos', value: pos });

		if (calEvent) {	
			var start = calEvent.start.getTime()/1000 - calEvent.start.getTimezoneOffset()*60;
			var end = calEvent.end.getTime()/1000 - calEvent.end.getTimezoneOffset()*60;
			data.push({name: 'id', value: calEvent.id });
			data.push({name: 'start', value: start  });
			data.push({name: 'end', value: end });
		}
		
		$.ajax({
			dataType: "json",
			url: "/applications/helpers/pos.openinghours.php",
			data: data,
			success: function(json) {
				if (json) {

					if (json.events) { 

						var events = setCalenderEvents(json.events);
						
						$(events).each(function( i, data ) {
							$calendar.weekCalendar("updateEvent", {
								id: data.id,
								start: data.start,
								end: data.end
							});
						});

						$calendar.weekCalendar('refresh');
					}
					
					if (json.last_id) {
						increment.val(json.last_id);
					}
					
					if (json.message) {
						$.jGrowl(json.message, { 
							sticky: false, 
							theme: 'message'
						});
					}
					
					if (json.closing_days) {
						$('#days_closed').val(json.closing_days);
					}
				}
			}
		});
	}

	
	function calenderCheckBoxes(calEvent, mod) { 
		
		var data = {
			action: 'getdays',
			pos: pos,
			mod: mod,
			start: calEvent.start.getTime()/1000 - calEvent.start.getTimezoneOffset()*60,
			end: calEvent.end.getTime()/1000 - calEvent.end.getTimezoneOffset()*60
		}

		$('.dayboxes').show();
		$('input[name^=day]').attr('checked',false).attr('disabled',false);
		
		$.getJSON('/applications/helpers/pos.openinghours.php', data, function(json) { 
			
			if (json) {
				for (var day in json) {
					$(json[day]).each(function( i, obj ) {
						$('input[name="day['+day+']"]')
							.attr('checked', obj.checked)
							.attr('disabled', obj.disabled);
					});
				}
			}
		});
	}

	var $endTimeField = $("select[name='end']");
	var $endTimeOptions = $endTimeField.find("option");

	// reduces the end time options to be only after the start
	// time options.
	$("select[name='start']").change(function() {
	
		var startTime = $(this).find(":selected").val();
		var currentEndTime = $endTimeField.find("option:selected").val();

		var start = new Date(startTime);
		var end = new Date(currentEndTime);
		
		$endTimeField.html($endTimeOptions.filter(function() {
			var newTime = new Date($(this).val());
			return start.getTime() <= newTime.getTime();
		}));

		var endTimeSelected = false;

		$endTimeField.find("option").each(function() { 

			var newTime = new Date($(this).val());

			if (newTime.getTime() === end.getTime()) {
				$(this).attr("selected","selected");
				endTimeSelected = true;
				return false;
			}
		});

		if (!endTimeSelected) {
			// automatically select an end date
			// 2 slots away.
			$endTimeField.find("option:eq(1)").attr("selected","selected");
		}

	});
	
	$('#save').click(function(e) {
		
		var days = $('#days_closed').val();
		var url = $('#form_closing_days').attr('action');
		
		$.getJSON(url, {
			days: days,
			pos: pos
		}, function(json) {
			if (json) { 
				$.jGrowl(json.message, { 
					sticky: (json.response) ? false : true, 
					theme: (json.response) ? 'message' : 'error'
				});
			}
		});
		
		return false;
	});


	// popover box
	copyfrom.pop().click(function() {
		$('.icon',this).toggleClass('direction-up');
	});


	// load dropdown options
	$('.dropdown-loader').change(function() {
		
		var id = $(this).attr('id');
		
		if (id=='pos') {
			
			if ($(this).val()) {
				
				$calendar.weekCalendar("clear");
				$('#days_closed').val('');
				
				$.ajax({
					dataType: 'json',
					url: '/applications/helpers/pos.openinghours.php',
					data: {
						action: 'copy',
						pos: $('#id').val(),
						id: $(this).val()
					},
					success: function(json) { 

						if (json) {
							
							if (json.events) {

								var events = setCalenderEvents(json.events);
								
								$(events).each(function( i, data ) {
									$calendar.weekCalendar("updateEvent", {
										id: data.id,
										start: data.start,
										end: data.end
									});
								});
							}
							
							if (json.last_id) {
								increment.val(json.last_id);
							}
							
							if (json.closing_days) {
								$('#days_closed').val(json.closing_days);
							}
							
							if (json.message) {
								$.jGrowl(json.message, { 
									sticky: false, 
									theme: 'message'
								});
							}
							
							copyfrom.pop('hide');
							$('.dropdown-loader').val('');
							loaderTrigger.val('').trigger('change');
						}
					}
				});
			}
			
		} else {
			loaderTrigger.val(id).trigger('change');
		}
	});


	// for each change by dropdown loaders, 
	// reload all dropdown loaders
	loaderTrigger.change(function(even) {
	
		var self = $(this);
		var maxwidth = 0;
		
		$('.dropdown-loader').each(function(i, elem) {
			
			var id = $(elem).attr('id');
			var selected = $(elem).val();

			if (id != self.val()) {
				
				var data = $('#toolbox').serializeArray()
				data.push({ name: "section", value: id });
				
				$.ajax({
					url: '/applications/helpers/ajax.opening.hours.php',
					dataType: 'json',
					data: data,
					success: function(json) {
						
						$(elem).empty();
						
						if (json) {

							for (i = 0; i < json.length; i++) {  
								for ( key in json[i] ) {	
									$(elem).append("<option value="+key+">"+json[i][key]+"</option>");
								}
							}

							$(elem).val(selected);
						}
					}
				});
			}
		});
		
	}).trigger('change');
	
	$('.dialog').click(function(event) {

		event.preventDefault();
		
		retailnet.modal.dialog('#'+$(this).attr('id')+'_dialog');
	});

	$('#cancel').bind('click', function(event) {
		event.preventDefault();
		retailnet.modal.close();
		return false;
	});	

	$('#apply').on('click', function(event) {

		event.preventDefault();
		
		var xhr = retailnet.ajax.json("/applications/helpers/pos.openinghours.php", {
			action: 'deleteall',
			pos: $('#id').val()
		})
		
		xhr.done(function(json) {
			
			if (json) {
				
				if (json.message) {
					retailnet.notification.success(json.message);
				}
				
				$('#days_closed').val('');
			}
		});
		
		$calendar.weekCalendar("clear");
		
		retailnet.modal.close();
	});


	// close popover box
	$(document).on("click", function(e) {
	    
		var clicked = $(e.target);
		
		if (!clicked.hasClass('pop') && !clicked.hasClass('pop-active') && !clicked.parents().hasClass('pop') && !clicked.parents().hasClass('pop-active')) { 
			copyfrom.pop('hide');
		}
	});
	
	// table rollover
	$(document).on("mouseover", "tbody tr", function() {
		$(this).removeClass("-over");
	});
	
});

