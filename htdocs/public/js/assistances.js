$(document).ready(function() {

	var assistances = $('#assistances').tableLoader({
		url: '/applications/helpers/assistance.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			
			$('select',self).dropdown();

			$('.panel-toggler').click(function(e) {

				e.stopPropagation();
				
				var panel = $(this).closest('.panel');

				$('.content', panel).toggle('slown');
				$('.content ul ul', panel).hide('slown');
				$('.content .toggler', panel).addClass('fa-chevron-circle-down').removeClass('fa-chevron-circle-up');
				$(this).toggleClass('fa-chevron-circle-down').toggleClass('fa-chevron-circle-up');
			});

			$('.panel .title').click(function() {
				var panel = $(this).closest('.panel');
				$('.panel-toggler', panel).trigger('click');
			});

			$( ".section-button, .section-title, .section-edit" ).click(function(e) {

				e.stopPropagation();

				if (!$(this).is('a')) {
					$(this).parent().trigger('click');
				}
			});

			$('.bundle-toggler').click(function(e) {

				e.stopPropagation();
				
				var bundle = $(this).closest('li');

				bundle.next().toggle('slown');
				
				$(this).toggleClass('fa-chevron-circle-down').toggleClass('fa-chevron-circle-up');

				var ul = bundle.next();
				ul.sortable( "enable" );
			});

			$('li.bundle').click(function(e) {

				e.stopPropagation();

				$('.bundle-toggler', $(this)).trigger('click');
			});

			$('.panel', self).each(function(i,panel) {

				if ($(panel).hasClass('selected')) { 

					$(panel).removeClass('selected');

					$('.panel-toggler', $(panel)).trigger('click');

					// panel bndles
					$('.bundel-sections', $(panel)).each(function(j,bundle) {
						
						if ($(bundle).hasClass('selected')) {

							var bundleToggler = $(bundle).prev();

							$(bundle).removeClass('selected');

							bundleToggler.trigger('click');

							$('html, body').animate({
						        scrollTop: bundleToggler.offset().top - $(bundle).outerHeight(true)
						    }, 1000);
						    
						}
						
					});
				}
			})

			$( ".bundel-sections" ).sortable({
				disabled: true,
				connectWith: ".bundel-sections",
				placeholder: "news-article-template-container ui-state-highlight",
				handle: ".orderer",
				items: "li",
				forcePlaceholderSize: true,
				dropOnEmpty: true,
				update: function( event, ui ) { 
					
					var data = $(this).sortable('toArray', {attribute: 'data-id'});
					var id = $(this).data('id');
						
					retailnet.ajax.json('/applications/helpers/assistance.sort.php', {
						id: id,
						data: data
					})
				}

			}).disableSelection();
			
		}
	});
	
})