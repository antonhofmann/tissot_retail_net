$(document).ready(function() {

	var path;
	var application = $('#application');
	var form = $("form.validator");
	var id  = $('input.file_id');
	var btnShowFile = $('#showfile', form);
	var btnUploadFile = $('#upload');
	var filePath = $('input.file_path', form);
	var fileTitle = $('input.file_title', form);
	var hasUpload =  $('#has_upload');
	var iframeUpload = $('body').hasClass('modal-iframe') ? true : false;
	
	// spiner instance	
	retailnet.loader.init();
	
	// tooltips
	retailnet.tooltip.init();
	
	// form validator
	form.validationEngine({
		autoHidePrompt: true,
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		onAjaxFormComplete: function(status, form, json, options) { 
			
			retailnet.loader.hide();
			retailnet.notification.hide();

			if (json) {

				// for modal file uploader
				if (iframeUpload) {

					if (json.message) {
						if (json.response) parent.retailnet.notification.success(json.message);
						else parent.retailnet.notification.error(json.message);
					}

					parent.retailnet.modal.close();
					
				} else {

					if (json.response && json.redirect) { 
						window.location=json.redirect;
					}

					if (json.message) {
						if (json.response) retailnet.notification.success(json.message);
						else retailnet.notification.error(json.message);
					}
				}

				// trigger file path
				if (json.path && hasUpload.val()) {
					filePath.val(json.path);
					hasUpload.val('');
				}

				// update fields
				if (json.response && json.fields) {
					$.each(json.fields, function(field, value) {
						$(field).val(value);
					});
				}
			}
		}
	});

	// ajax file uploader
	new AjaxUpload( btnUploadFile, {
		action: '/applications/helpers/ajax.file.upload.php',
		name: 'userfile',
		data: {
			application: application.val(),
			checkExtension: $('#check_extensions').length > 0 ?  $('#check_extensions').val() : true
		},
		onSubmit: function(file, ext) {

			var error = false;

			if (!fileTitle.val()) {
				
				var message = 'Please, set file title and select file again';

				if (iframeUpload) parent.retailnet.notification.error(message);
				else retailnet.notification.error(message);
				
				fileTitle.addClass('error');
				
				return false;
				
			}

			retailnet.loader.show();	
			fileTitle.removeClass('error');
		},
		onComplete: function(file, response) {
			
			retailnet.loader.hide();
			retailnet.notification.hide();

			var json = $.parseJSON(response);
			
			if (json.response) {
				
				var path = $("<div/>").html(json.path).text();
				filePath.val(path);

				// set upload trigger to on
				hasUpload.val(1);
				
				$('#upload').removeClass('error');
				
				if (iframeUpload) parent.retailnet.notification.success(json.button);
				else retailnet.notification.success(json.button);
				
				filePath.trigger('change');
				
			} else if (json.message) {
				if (iframeUpload) parent.retailnet.notification.error(json.message);
				else retailnet.notification.error(json.message);
			}
		}
	});

	// button show label
	filePath.change(function() {

		path = $(this).val(); 

		if (path) {

			btnShowFile.show();

			if (isImage(path)) {
				$('.icon', btnShowFile).removeClass('download').addClass('picture');
				$('.label', btnShowFile).text('Show Picture');
			} else {
				$('.icon', btnShowFile).removeClass('picture').addClass('download');
				$('.label', btnShowFile).text('Download');
			}
			
		} else {
			btnShowFile.hide();
		}
		
	}).trigger('change');
	
	
	// button show file
	btnShowFile.click(function(event) {

		event.preventDefault();

		if (!$(this).hasClass('disabled')) {

			path = filePath.val();

			if (isImage(path)) {
				retailnet.modal.show(path, {
					title: fileTitle.val()
				});
			}
			else {
				window.open('http://'+location.host+path);
			}
		}
	});
	
	// submit form
	$("#save").click(function(event) { 

		event.preventDefault();
		
		retailnet.notification.hide();

		btnUploadFile.removeClass('error');

		var errors = [];
		/*
		if (fileTitle.val() && !filePath.val() && !id) {
			errors[] = 'Please select one file for upload';
		}
		*/

		if (!fileTitle.val() && filePath.val()) {
			fileTitle.addClass('error');
			errors.push('File title is required');
		}

		if (filePath.hasClass('required') && !filePath.val()) {
			btnUploadFile.addClass('error');
			errors.push('File is required.');
		}
		
		if (form.validationEngine('validate') && errors.length==0) {
			retailnet.loader.show();
			form.submit();
		} else {
			var message = errors.join('<br /><br />');
			if (iframeUpload) parent.retailnet.notification.error(message);
			else retailnet.notification.error(message);
		}
	});

	// delete file
	$('.dialog').click(function(event) {
		
		event.preventDefault();

		var button = $(this);
		
		$('#apply, a.apply').attr('href', button.attr('href'))
		
		retailnet.modal.dialog('#'+button.attr('id')+'_dialog');
	});

	// apply action
	$('#apply').bind('click', function(e) {
		
		e.preventDefault();

		var url = $(this).attr('href');
		
		if (url) {

			if (iframeUpload) {

				retailnet.ajax.json(url).done(function(xhr) {

					if (xhr && xhr.message) {
						if (xhr.response) parent.retailnet.notification.success(xhr.message);
						else parent.retailnet.notification.error(xhr.message);
					}

					if (xhr && xhr.response) {
						parent.retailnet.modal.close();
					}

				});

			} else {
				window.location = url;
			}

		} else {
			if (iframeUpload) parent.retailnet.notification.error('URL is not defined.');
			else retailnet.notification.error('URL is not defined.');
		}

		return false;
	});	

	// cancel action
	$('#cancel, .cancel').bind('click', function(event) {
		event.preventDefault();
		retailnet.modal.hide();
		return false;
	});	



});