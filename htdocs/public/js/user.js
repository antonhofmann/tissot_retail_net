$(document).ready(function() {

	var userForm = $("#userform"),
		addUserContainer = $('#addUserContainer'),
		accountMailContainer = $('#accountMailContainer'),
		accountMailForm = $('form', accountMailContainer),
		tmpSendLoginData = $('#loginMailContainer'),
		buttonSendLoginData = $('#send_login_data'),
		previewMailTemplate = $('#preview-mail-template'),
		testMailTemplate = $('#test-mail-template'),
		iframe = $('body').hasClass('modal-iframe') ? true : false;
	
	
	// loader instance
	retailnet.loader.init();

// wysiwyg :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$('textarea.mail-content').each(function(i,e) {
		
		var editor = $(e);
		
		tinymce.init({
			selector: "#"+editor.attr('id'),
			plugins: ["advlist autolink lists textcolor link visualblocks code nonbreaking contextmenu paste preview"],
			width: '100%',
			height: editor.data('height') || 400,
			menubar : false,
			statusbar : false,
			toolbar_items_size: 'small',
			autosave_restore_when_empty: false,
			toolbar: "fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor | removeformat | paste pastetext pasteword",
			setup : function(ed) {
				ed.on('change', function(e) {
					editor.val(ed.getContent())
				});
			}
		});	
		
	});


// form utilities ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	// tooltips
	$('.-tooltip[title]').qtip({
		position: {corner: {target:'topRight', tooltip:'leftTop'}},
		style:{name:'blue', color:'black', tip:true, width: { min:200, max: 400 } }
	});
	
	// event: focus
	$('input:visible, select:visible, textarea:visible', accountMailForm).click(function() {		
		$(this).addClass('onFocus').removeClass('onBlur');
	});
	
	// event: blur
	$('input:visible,select:visible,textarea:visible', accountMailForm).blur(function() {		
		$(this).removeClass('onFocus').addClass('onBlur');
	});
	
	// event: change
	$('.required:visible', accountMailForm).change(function() {		
		$(this).removeClass('onFocus').addClass('onBlur');
	});


// sendmail login data :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var modalMailLoginData = tmpSendLoginData.adoModal({
		width: 800,
		height: 760,
		clickClose: false
	});

	// button: send login data
	buttonSendLoginData.click(function(e) {
		
		e.preventDefault();
		e.stopPropagation();
		
		if ($('input.modal-trigger', tmpSendLoginData).val()==1) {
			modalMailLoginData.show();
		} else {
			$('form', tmpSendLoginData).submit();
		}
		
		return false;
	});

	// button: send mail
	$('.sendmail', tmpSendLoginData).on('click', function(e) {

		e.preventDefault();
		
		var self = $(this),
			url = self.attr('href'),
			required = $('.required', tmpSendLoginData),
			failures = $('.required[value=]', tmpSendLoginData);

    	if (required.length > 0 && failures.length > 0) {
    		$('.required', tmpSendLoginData).removeClass('error');
    		$('.required[value=]', tmpSendLoginData).addClass('error');
    		retailnet.notification.error('Red marked fields are required.');
    	} else {
    		modalMailLoginData.close();
			$('form', tmpSendLoginData).submit(); 
    	}
	});

	// submit login from
	tmpSendLoginData.on('submit', 'form', function(e) {
		
		e.preventDefault();
		e.stopPropagation();
		
		var self = $(this);
		var data = self.serializeArray();
		
		// show wait screen
		retailnet.loader.show();
				
		retailnet.ajax.json(self.attr('action'), data).done(function(xhr) {
			
			if (xhr && xhr.reload) {
				window.location.reload();
        	}
			
			if (xhr.response && xhr.redirect) {
				window.location = xhr.redirect;
			} 
    		
			if (xhr && xhr.message) {
				if (xhr.message) retailnet.notification.success(xhr.message);
				else retailnet.notification.error(xhr.message);
			}
			
		}).complete(function() {
			retailnet.loader.hide();
		});
		
		return false;
		
	});


	
// user form :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	
	// check user login name as uniques
	$('#user_login').change(function() {
	
		var self = $(this);
		var value = self.val();
	
		// close all notofications
		retailnet.notification.hide();
		
		// remove error marks
		self.removeClass('error');
		
		if (value) {
			
			retailnet.ajax.json('/applications/helpers/user.validator.php', {
				section: 'double_login_name',
				value: value
			}).done(function(xhr) {
				
				// login name exist, add error class
				if (xhr && xhr.error) {
					self.addClass('error');
				}
				
				// notification
				if (xhr && xhr.message) {
					retailnet.notification.show(xhr.message);
				}
			});
		}
	});


	// check user user_email as uniques
	$('#user_email').change(function() {
	
		var self = $(this);
		var value = self.val();
	
		// close all notofications
		retailnet.notification.hide();
		
		// remove error marks
		self.removeClass('error');
		
		if (value) {
			
			retailnet.ajax.json('/applications/helpers/user.validator.php', {
				section: 'double_user_email',
				value: value
			}).done(function(xhr) {
				
				// login name exist, add error class
				if (xhr && xhr.error) {
					self.addClass('error');
				}
				
				// notification
				if (xhr && xhr.message) {
					retailnet.notification.show(xhr.message);
				}
			});
		}
	});
	
	
	// button submit user form
	$("#user_save").on('click', function(e) {
	
		e.preventDefault();
		e.stopPropagation();
		
		// close all notofications
		retailnet.notification.hide();

		$('.roles label').removeClass('error');
		
		// mycompany roles is required
		if (userForm.hasClass('mycompany') && $('input.roles').length>0  && $('input.roles:checked').length==0) {
			$('.roles label').addClass('error');
		}

		var validate = userForm.validationEngine('validate');
		var hasErrors = userForm.find('.error').length > 0 ? true : false;

		$('.input-mask-container').each(function(i,e) {

			var container = $(e);

			// check phone numbers
			if (Validator.hasInputMaskError(container)) {
				hasErrors = true;
			}
		})

		if (hasErrors || !validate) {
			retailnet.notification.error('Please check red marked fields.');
		}
		else if($('#user_phone_number').val() == '' && $('#user_mobile_phone_number').val() == '') {
			retailnet.notification.error('Please indicate the phone number or the mobile phone number!');
			
		}
		else {
			userForm.submit();
		}
		
		return false;
	});
	
	
	// submit user form
	userForm.validationEngine({
		autoHidePrompt: true,
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		onAjaxFormComplete: function(status, form, json, options) {
	
			if (json) {

				modalUserForm.close();

				var sendmail = $("#user_save").hasClass('sendmail') || $('.submit', addUserContainer).hasClass('sendmail') ? true : false;
	
				if (json.response && json.id && sendmail) {
				
					var subtitle = $('#user_firstname').val() + ' ' + $('#user_name').val();
					$('.subtitle', accountMailContainer).text(subtitle);
				
					// set new inserted user id
					$('input.user-id', accountMailContainer).val(json.id).trigger('change');

				} else {
	
					if (json.redirect) {
						window.location=json.redirect;
					}
	
					if (json.message) {
						if (json.response) retailnet.notification.success(json.message);
						else retailnet.notification.error(json.message);
					}
				}
			}
		}
	});

	
// sendmail open/close account :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	var modalMailAccount = accountMailContainer.adoModal({
		width: 800,
		height: 760,
		clickClose: false
	});

	// account mail change user id
	// load user data and parse date to load mail template content
	accountMailContainer.on('change', 'input.user-id', function(e) {

		var self = $(this);
		var data = {};
		var editor = tinymce.get('mailAccountContent');

		if (!self.val()) return false;

		// mail template
		data.id = $('input.mail-id', accountMailContainer).val();
		
		// user dataloader
		var user = retailnet.ajax.request('/applications/helpers/ajax.user.php', {
			id: self.val()
		});
		
		// sender dataloader
		var sender = retailnet.ajax.request('/applications/helpers/ajax.user.php', {
			section: 'sender'
		});
		
		// company dataloader
		var company = retailnet.ajax.request('/applications/helpers/ajax.company.php', {
			id: user.user_address
		});

		// recipient dataloader
		var recipient = retailnet.ajax.request('/applications/helpers/ajax.user.php', {
			id: self.val(), 
			section: 'role-responsibile'
		});
		
		// dataloader
		data.render = $.extend(user, sender, company, recipient);
		
		// mail temlate
		var mail = retailnet.ajax.request('/applications/helpers/ajax.mailtemplate.php', data);
		
		if (mail) {
			
			// modal screen title
			$('.subtitle', accountMailContainer).text(user.user_firstname + ' ' + user.user_name);

			// mail subject
			$('input.mail-subject', accountMailContainer).val(mail.mail_template_subject);
			
			// mail template content
			$('input.mail-content', accountMailContainer).val(mail.mail_template_text);
			
			// update editor content
			if (editor) {
				editor.setContent(mail.mail_template_text);
				tinyMCE.triggerSave();
			}
			
			// mail template content
			$('input.modal-trigger', accountMailContainer).val(mail.mail_template_view_modal);

			// edit mail befor send
			if (mail.mail_template_view_modal) modalMailAccount.resize().show();
			else $('form', accountMailContainer).submit();
			
		} else {
			retailnet.notification.error('Error: E-Mail template not found');
		}
		
		return false;
	});

	// button: send mail
	accountMailContainer.on('click', '.sendmail', function(e) {

		e.preventDefault();

		// save all contents
		tinyMCE.triggerSave();
		
		var self = $(this),
			url = self.attr('href'),
			required = $('.required:visible', accountMailContainer),
			failures = $('.required[value=]:visible', accountMailContainer);

		$('.required', accountMailContainer).removeClass('error');

    	if (required.length > 0 && failures.length > 0) {
    		$('.required[value=]', accountMailContainer).addClass('error');
    		retailnet.notification.error('Red marked fields are required.');
    	} else {
    		modalMailAccount.close();
			$('form', accountMailContainer).submit(); 
    	}
	});

	// form mail submit
	accountMailContainer.on('submit', 'form', function(e) {
		
		e.preventDefault();
		e.stopPropagation();
		
		var self = $(this);
		var data = self.serializeArray();
		
		// show wait screen
		retailnet.loader.show();
		
		retailnet.ajax.json(self.attr('action'), data).done(function(xhr) {
			
			if (xhr && xhr.reload) {
				window.location.reload();
        	}
			
			if (xhr.response && xhr.redirect) {
				window.location = xhr.redirect;
			} 
    		
			if (xhr && xhr.message) {
				if (xhr.message) retailnet.notification.success(xhr.message);
				else retailnet.notification.error(xhr.message);
			}
			
		}).complete(function() {
			retailnet.loader.hide();
		});
		
		return false;
		
	});


// preview mail ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var modalMailPreview = previewMailTemplate.adoModal({
		width: 1000,
		height: 600
	});

	$('.preview-mail').on('click', function(e) {

		e.preventDefault();
		
		var self = $(this);
		var url = self.attr('href');
		var container = $(this).closest('.ado-modal');
		var data = $('form', container).serialize();
		
		retailnet.loader.show();
		
		retailnet.ajax.json(url,data).done(function(xhr) {

			retailnet.loader.hide();

			if (xhr && xhr.response) {

				// assign data from request
				$('.mail-subject', previewMailTemplate).html(xhr.subject);
				$('.mail-address', previewMailTemplate).text(xhr.email);
				$('.mail-cc-address', previewMailTemplate).text(xhr.cc);
				$('.mail-cc-address', previewMailTemplate).parent().toggle(xhr.cc ? true : false);
				$('.mail-date', previewMailTemplate).text(xhr.date);
				$('.mail-content', previewMailTemplate).html(xhr.content);
				$('.mail-footer', previewMailTemplate).html(xhr.footer);
				$('.mail-footer', previewMailTemplate).parent().toggle(xhr.footer ? true : false);

				modalMailPreview.resize().show();

			} else {
				modalMailPreview.close();
			}

			if (xhr && xhr.message) {
				if (xhr.response) retailnet.notification.success(xhr.message);
				else retailnet.notification.error(xhr.message);
			}
		});

	});

// test mail :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var modalTestMail = testMailTemplate.adoModal({
		width: 400,
		afterClose: function()  {
			testMailTemplate.removeAttr('data-container');
		}
	});

	$('.test-mail').on('click', function(e) {
		e.preventDefault();
		var container = $(this).closest('.ado-modal').attr('id');
		testMailTemplate.attr('data-container', '#'+container);
		modalTestMail.show();
	});

	$('.apply', testMailTemplate).on('click', function(e) {
		
		e.preventDefault();

		var url = $(this).attr('href');
		var container = testMailTemplate.data('container');
		var data = $('form', $(container)).serializeArray();
		var recipient = $('#test-mail-recipient').val();
		
		if (recipient) {

			data.push({name: 'test_recipient', value: recipient});
			
			retailnet.loader.show();
			
			retailnet.ajax.json(url,data).done(function(xhr) {

				modalTestMail.close();

				if (xhr && xhr.message) {
					if (xhr.message) retailnet.notification.success(xhr.message);
					else retailnet.notification.error(xhr.message);
				}

			}).complete(function() {
				retailnet.loader.hide();
			});
			
		} else {
			retailnet.notification.error('Recipient e-mail is required.');
		}
	});
	
	
	
// old retailnet sendmail close account ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


	var modalUserForm = addUserContainer.adoModal({
		width: 760,
		height: 720,
		afterClose: function()  {
			
		}
	});

	/*
	addUserContainer.on('click', 'a.submit', function(e) {

		e.preventDefault();
		e.stopPropagation();

		userForm.submit();

		return false;
	});
	*/

	$('.users-list td a').click(function(e) {
		
		e.preventDefault();
		e.stopPropagation();

		var id = $(this).attr('data');
		
		if (id) {
			
			// add close trigger
			// and add template id for close account
			$('.ado-title', accountMailContainer).text('Request to Close Account');
			$('input.close', accountMailContainer).val(1);
			$('.mail-reason', accountMailContainer).show();
			$('input.mail-id', accountMailContainer).val(11);

			// assign user id to form
			// and trigger form for submitting
			$('input.user-id', accountMailContainer).val(id).trigger('change');
				
		} else {
			retailnet.notification.error('Error: User not found.');
		}

		return false;
	});

	$('.modal-userform').click(function(e) {
		
		e.preventDefault();
		e.stopPropagation();
		
		// remove close trigger
		// and add template id for new open account
		$('.ado-title', accountMailContainer).text('Request to Open New Account');
		$('input.close', accountMailContainer).val('');
		$('.mail-reason', accountMailContainer).hide();
		$('#mail_template_id', accountMailContainer).val(12);
		
		modalUserForm.resize().show();
		
		return false;
	});

	// phone number mask
	$('.phone-mask').on('change', function(e) {
		Validator.checkPhoneNumber($(this));
	})

	$('[name="user_phone_country"], [name="user_mobile_phone_country"]').on('focus', function() {

		var self = $(this),
			companyID = Number($('#company').val());

		if (self.val() || !companyID) return;

		// get pos
		$.ajax({
			url: '/applications/helpers/ajax.company.php',
			dataType: 'json',
			data: {
				id: companyID
			},
			success: function(xhr) { 
				if (xhr && xhr.address_country) {
					$.ajax({
						url: '/applications/helpers/ajax.country.php',
						dataType: 'json',
						data: {
							section: 'get_country_phone_prefix',
							value: xhr.address_country
						},
						success: function(response) { 
							self.val(response.country_phone_prefix);
			        	}
					})
				}
        	}
		})
	})
})
