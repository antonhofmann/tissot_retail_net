$(document).ready(function() {

	var IdeaBox = $('#ideabox');
	
	// loader initialise
	retailnet.loader.init();
	
	// close modal screen
	$('.cancel', IdeaBox).click(function(e) {
		
		e.preventDefault();
		e.stopImmediatePropagation();
		
		parent.retailnet.modal.close();
		
		return false;
	});
	
	// start upload files
	$('.button.submit', IdeaBox).click(function(e) {
		
		e.preventDefault();
		e.stopImmediatePropagation();
		
		retailnet.notification.hide();
    	
		var requiredError = false;
    	var inputRequired = $('input.required, textarea.required', IdeaBox);
    	var selectRequired = $('select.required', IdeaBox);
    	
    	// remove all marked errors
    	$('.has-error', IdeaBox).removeClass('has-error');
    	
    	// check input required fields
    	if (inputRequired.length > 0) {
    		
    		var failures = $('.required[value=]', IdeaBox);
    		
    		if (failures.length > 0) {
    			$('.required[value=]', IdeaBox).addClass('has-error');
    			requiredError = true;
    		}
    	}
    	
    	// check select required fields
    	if (selectRequired.length > 0) {
    		
    		var selected = $('select.required option:selected', IdeaBox).filter('[value!=0]');
    		
    		if (selectRequired.length != selected.length) {
    			$('select.required', IdeaBox).filter(':not(:selected)').addClass('has-error');
    			requiredError = true;
    		}
    	}
   
    	if (requiredError) {
    		
    		parent.retailnet.notification.show('Red marked fields are required.', {
    			sticky: true,
    			theme: 'error'
    		});
    		
    	} else {	
    		
    		var data = $('form', IdeaBox).serializeArray();
			
			retailnet.loader.show();
    		
    		retailnet.ajax.json($('form', IdeaBox).attr('action'), data).done(function(xhr) {
    			
	    		retailnet.loader.hide();
	    		
				if (xhr && xhr.notification) {
					parent.retailnet.notification.show(xhr.notification.content, eval(xhr.notification.properties));
				}
				
				parent.location.reload();
	        });
    	}

		return false;
	});

});