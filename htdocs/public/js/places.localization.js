// save localized province data

$(document).on('change', 'input[class^="place_name_"]', function(e) {
		
		var self = $(this);

		retailnet.ajax.json('/applications/modules/place/localization/ajax_submit.php', {
			application: $('#application').val(),
			language_id:self.closest('td').data('language_id'),
			place_id: self.attr('index'),
			place_name: self.val()
		})

});




