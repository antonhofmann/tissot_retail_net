$(document).ready(function() {

	var application = $('#application').val(),
		mastersheet = $('#id').val(),
		popOver = $('#add'),
		group = $('#group'),
		subgroup = $('#subgroup');

	// spiner instance	
	retailnet.loader.init();

	// popover box
	popOver.pop().click(function() {
		$('.icon',this).toggleClass('direction-up');
	});

	// material planning type
	group.chainedSelect('#subgroup', {
		url: '/applications/modules/mastersheet/ajax.php',
		parameters: {
      		section: 'dropdown.items.collection.categories',
      		application: $('#application').val()
      	}
	}).trigger('change');

	// material collection category
	subgroup.change(function() {

		var value = $(this).val();

		if (value) {

			retailnet.loader.show();

			$.ajax({
		        type: "post",
		        url: "/applications/modules/mastersheet/item.save.php",
		        data: {
		        	application: application,
					mastersheet: mastersheet,
					group: group.val(),
					subgroup: value
			   	},
		        success: function(json) { 

		        	retailnet.loader.hide();
					retailnet.notification.hide();

					popOver.pop('hide');
				    group.val('').trigger('change');

		        	if (json) {

						if (json.response) {
							itemsList.showPage(1);
						}

						if (json.message) {
							if (json.response) retailnet.notification.success(json.message);
							else retailnet.notification.error(json.message);
						}
			        }           
		        }
		    });
		}
	});

	var itemsList = $('#mastersheet_items').tableLoader({
		url: '/applications/modules/mastersheet/item.list.php',
		data: $('.request').serializeArray(),
		after: function() {

			var material = $('input.material'),
				checkAll = $('.checkall');

			// material checkbox
			material.change(function() {

				var table = $(this).closest('table');
				var checkboxes = $('.material', table).length;
				var checked = $('.material:checked', table).length;
				var attribute = (checked==checkboxes) ? true : false;
				
				var xhr = saveItem({
					item: $(this).val(),
					checked: ($(this).is(':checked')) ? 1 : 0
				});

				if (xhr.message) {
					if (xhr.response) retailnet.notification.success(xhr.message);
					else retailnet.notification.error(xhr.message);
				}
				
				// checkall controlle
				$('.checkall', table).attr('checked', attribute);

				// if all items are removed from master sheet
				// reload items list
				if (checked==0) {

					// remove delivery date
					saveDeliveryDate({
						group: $('.deliverydate', table.prev('h6')).attr('planning'),
						subgroup: $('.deliverydate', table.prev('h6')).attr('category')
					});

					// reload item list
					itemsList.showPage(1);
				}
			});

			// checkall checkbox
			checkAll.change(function(event) {

				var xhr;
				var table = $(this).closest('table');
				var checkboxes = $('.material', table);
				var checked = ($(this).is(':checked')) ? true : false;

				$.each(checkboxes, function(e) {

					$(this).attr('checked', checked);

					xhr = saveItem({
						item: $(this).val(),
						checked: ($(this).is(':checked')) ? 1 : 0
					});
				});

				if (xhr.message) {
					if (xhr.response) retailnet.notification.success(xhr.message);
					else retailnet.notification.error(xhr.message);
				}

				// if all items are removed from master sheet
				// reload items list
				if ($('.material:checked', table).length == 0) {

					// remove delivery date
					saveDeliveryDate({
						group: $('.deliverydate', table.prev('h6')).attr('planning'),
						subgroup: $('.deliverydate', table.prev('h6')).attr('category')
					});

					// reload item list
					itemsList.showPage(1);
				}
				
			});

			// master sheet delivery date
			$('.delivery input').datepicker({ 
				dateFormat: 'dd.mm.yy',
				showOtherMonths : true,
				firstDay: 1,
				onClose: function(dateText, inst) { 

					var error = (dateText && !isValidDate(dateText)) ? true : false;

					// validate delivery date
					$(this).removeClass('error').toggleClass('error', error);

					if (!error) {
						
						var xhr = saveDeliveryDate({
							group: $(this).attr('planning'),
							subgroup: $(this).attr('category'),
							date: dateText
						});

						if (xhr.message) {
							if (xhr.response) retailnet.notification.success(xhr.message);
							else retailnet.notification.error(xhr.message);
						}
					}
				}
			});
		}
	});

	// save/remove master sheet item
	var saveItem = function(options) {

		var response;

		var data = {
			application: application,
			mastersheet: mastersheet
		};

		$.ajax({
	        type: "post",
	        async: false,
	        url: "/applications/modules/mastersheet/item.save.php",
	        data: jQuery.extend(data, options),
	        success: function(json) { 
	        	response = json;           
	        }
	    });

		return response;
	}
	
	// save/remove master sheet delivery date
	var saveDeliveryDate = function(options) {

		var response;

		var data = {
			application: application,
			mastersheet: mastersheet
		};

		$.ajax({
	        type: "post",
	        async: false,
	        url: "/applications/modules/mastersheet/delivery.date.php",
	        data: jQuery.extend(data, options),
	        success: function(json) { 
	        	response = json;           
	        }
	    });

		return response;
	}
});