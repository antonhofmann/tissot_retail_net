$(document).ready(function() { 

	var form = $('#itemform');
	var mailForm = $('#mailform');
	var data = [];
	var error = false;
	var message,value;


	// loader instance
	retailnet.loader.init();

	// submit action
	$('.submit').click(function(event) {

		event.preventDefault();
		
		retailnet.notification.hide();

		var action = $(this).attr('href');
		var state = $(this).attr('state');
		var template = $(this).attr('template');
		var error, message, value;

		// reset state value
		$('#state').val('');

		// parse button attributes (action, state and template) to mail form 
		if (action) $('#action').val(action);
		if (state) $('#state').val(state);
		if (template) $('#mail_template_id').val(template);

		// set order sheet in revision
		// check is selected least one item from item list
		if (state==8) {
			error = ($('input.item_revision:checked').length > 0) ? false : true;
			message = 'Please, select at least one Item in Revision';
		}

		if (!error) {

			// send mail action
			if ($(this).hasClass('sendmail')) {

				// get mail content
				var xhr = retailnet.ajax.request('/applications/helpers/ajax.mailtemplate.php', {
					id: template
				});

				if (xhr.response) {

					// assign template data
					$('#mail_template_id').val(xhr.mail_template_id);
					$('#mail_template_subject').val(xhr.mail_template_subject);
					$('#mail_template_text').val(xhr.mail_template_text);

					if (xhr.mail_template_view_modal) {

						// edit mail content befor send
						retailnet.modal.show('#modalbox', {
							autoSize	: false,
							closeClick	: false,
							closeBtn	: false,
							fitToView   : true,
						    width       : '90%',
						    height      : '90%',
						    maxWidth	: '800px',
						    maxHeight	: '900px',
							modal 		: true,
							afterShow 	: function() {
								$('.expand').focus();
							}
						}); 	
					}
					else {

						// submit mail form without changes
						mailForm.submit();
					}
				} 
				// template not found
				else if (xhr.message) {

					// show notification
					retailnet.notification.error(xhr.message);
				}
			} 
			// submit items form
			else {
				
				form.submit();
			}
		} 
		// form contain errors
		else if (message) {

			// show notification
			retailnet.notification.error(message);
		}

		return false;
	});	


	// cancel modal screen
	$('#cancel').bind('click', function(e) {
		e.preventDefault();
		retailnet.modal.hide();
		return false;
	});	

	
	// submit modal screen
	$('#sendmail').click(function(e) {

		e.preventDefault();
		
		retailnet.notification.hide();
		
		var self = $(this),
			required = $('.required', mailForm),
			failures = $('.required[value=]', mailForm);

    	if (required.length > 0 && failures.length > 0) {
    		
    		// mark required failures
    		$('.required', mailForm).removeClass('error');
    		$('.required[value=]', mailForm).addClass('error');
    		
    		// show notification
    		retailnet.notification.error('Red marked fields are required.');
    		
    	} else {

    		// hide modal screen
			retailnet.modal.hide();
			
			// submit form
			mailForm.submit();
    	}

		return false;
	})
	
	
	// submit sendmail form
	mailForm.submit(function(e) {
		
		e.preventDefault();
		
		var self = $(this);
		var data = self.serialize();
	
		retailnet.loader.show();
		retailnet.notification.hide();
		
		retailnet.ajax.json(self.attr('action'), data).done(function(xhr) {

			// reload page
			if (xhr && xhr.reload) {
				window.location.reload();
        	}
			
			// reset data
			if (xhr && xhr.response) {

				form.submit();

				$('#mail_template_id').val('');
				$('#mail_template_subject').val('');
				$('#mail_template_text').val('');
			}
    		
			// show notification
			if (xhr && xhr.notification) {
				retailnet.notification.show(xhr.notification.content, eval(xhr.notification.properties));
			}
			
		}).complete(function() {
			retailnet.loader.hide();	
		});
			
		return false;
	});

	

	// calcullate total price
	$('table input[type=text]').change(function() {

		var table = $(this).closest('table'),
			row = $(this).closest('tr'),
			lastcel = $("td.number:last", row),
			totalCels = 'td.'+lastcel.attr('class').split(' ')[0],
			price = parseFloat($('.mps_ordersheet_item_price', row).text()),
			totalprice=0, 
			totalcolumn=0;

		// find last non zero quantites
		$($("input:text", row).get().reverse()).each(function(i,elem) {
			if (!totalprice && $(elem).val() >= 0) {
				totalprice = $(elem).val()*price;
			}
		});

		// assign calcullated total price to last cel in row
		lastcel.text(totalprice.toFixed(2));

		// calculate all total prices for this table
		$($(totalCels, table)).each(function(i,elem) {
			totalcolumn = totalcolumn + parseFloat($(this).text());
		});	

		// assign calcullatet total pricees on table bottom
		$('b.'+table.attr('id')).text(totalcolumn.toFixed(2));
	});

});