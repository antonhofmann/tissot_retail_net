define(function(require, exports, module) {

	var $ = require('jquery');

	/**
	 * Get width of a text
	 * @return {integer}
	 */
	$.fn.textWidth = function(){
	  var html_org = $(this).html();
	  var html_calc = '<span>' + html_org + '</span>';
	  $(this).html(html_calc);
	  var width = $(this).find('span:first').width();
	  $(this).html(html_org);
	  return width;
	};
});