$(document).ready(function() {

	var form = $("#material_standard_form");

	// spiner instance	
	retailnet.loader.init();
	
	// tooltip instance	
	retailnet.tooltip.init();
	
	// validator
	form.validationEngine({
		autoHidePrompt: true,
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		onAjaxFormComplete: function(status, form, json, options) {

			retailnet.loader.hide();
			retailnet.notification.hide();

			if (json) {

				if (json.response && json.redirect) { 
					window.location=json.redirect;
				}

				if (json.message) {
					if (json.response) retailnet.notification.success(json.message);
					else retailnet.notification.error(json.message);
				}
			}
		}
	});
	
	// submit from
	$("#save").click(function(event) { 
		
		event.preventDefault();
		retailnet.notification.hide();
		
		if (form.validationEngine('validate')) {
			retailnet.loader.show();
			form.submit();
		} else {
			retailnet.notification.error('Please check red marked fields.');
		}
	});

	// datapicker
	$('#mps_material_standard_order_date').datepicker({ 
		dateFormat: 'dd.mm.yy',
		showOtherMonths : true,
		firstDay: 1
	});

	$('.selector').click(function(e) { 

		var self = $(this),
			section = $(this).attr('section'),
			data = $(this).attr('data'),
			active = $(this).hasClass('active') ? false : true;

		if (active) {
			$('#mps_material_standard_order').attr('checked', true);
		}
		
		if (data=='all') {
			$('.selector').removeClass('active').toggleClass('active', active);
			$('input.companies').attr('checked', active);
		} else {

			self.toggleClass('active', active);

			// select all controll
			if (!active) {
				$('.selector.all').removeClass('active');
			}
			
			// get companies
			retailnet.ajax.json('/applications/helpers/company.ajax.php', {
				section: section,
				value: data
			}).done(function(xhr) {
				
				if (xhr && xhr['companies']) {

					$('input.companies').each(function(i,e) { 

						var k = $(e).val();
						
						if (xhr['companies'][k]) {
							$(e).attr('checked', active);
						}
					})
				}
			});
		}
	});

	// set material as standard
	$('#mps_material_standard_order').change(function() {

		var checked = $(this).is(':checked');

		if (!checked) {
			$('.selector').removeClass('active');
			$('input.companies').attr('checked', false);
		}
	});

	// client check
	$('input.companies').click(function() {
		
		var boxes = $('input.companies').length,
			checked = $('input.companies:checked').length;

		$('#mps_material_standard_order').attr('checked', checked);
		
		if (checked==boxes) {
			$('.selector').addClass('active');
		}
	});
});