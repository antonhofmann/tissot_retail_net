jQuery(document).ready(function() {

	// layout resize triggers
	$(window).on('resize', function() {

		$('.collapse').removeClass('in');
	});

	if (jQuery().tabdrop) {
		$('.tab-dropper').tabdrop();
	}

	// datapicker
	if (jQuery().datepicker) {
		
		$('input.datepicker').datepicker({ 
			dateFormat: 'dd.mm.yy' ,
			showOtherMonths : true,
			firstDay: 1,
			autoSize: true,
			onClose: function(dateText, inst) {
				
				if ($(this).hasClass('required')) {
					$(this).toggleClass('error', (dateText) ? false : true);
				}
			},
			beforeShow: function(input, inst) { 

			}
		});

		$(document).on('click', 'input.datepicker', function(e) {

			if ($(this).hasClass('datepicker-resize')) {
				var width = $(this).outerWidth(); 
				var inst = $(this).data('datepicker').dpDiv;
				$(inst).css('width', width);
			}
		})
	}

	// app resize events
	$(window).resize(function() {

		var winWidth = $(this).width();
		var winHeight = $(this).height();

		// dynamic modals
		if ($('.modal-dynamic').length > 0 && jQuery().actual) {
			$('.modal-dynamic').each(function(i,el) {
				var el = $(el);
				var headerHeight = $('.modal-header', el).actual('outerHeight', {includeMargin:true}) || 0;
				var footerHeight = $('.modal-footer', el).actual('outerHeight', {includeMargin:true}) || 0;
				var marginTop = $('.modal-dialog').css("marginTop").replace('px', '') || 0;
				var marginBottom = $('.modal-dialog').css("marginBottom").replace('px', '') || 0;
				var maxHeight = winHeight-headerHeight-footerHeight-marginTop-marginBottom-20;
				$('.modal-dynamic .modal-body').css('max-height', maxHeight);
			})
		}

	}).trigger('resize')

	// local storage proccese
	if(typeof(Storage) !== "undefined" && $('a[data-toggle="tab"]').length > 0) {
		
		var url = location.href;
		var tab = localStorage.getItem('selectedTab');
		var ref = localStorage.getItem('selectedTabUrl');

		// save selected tab
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			localStorage.setItem('selectedTab', $(this).attr('href'));
			localStorage.setItem('selectedTabUrl', url);
		});	

		if (tab) {
			if (ref==url) {
				$('a[href='+tab+']').tab('show');
			} else {
				localStorage.setItem('selectedTab', false);
				localStorage.setItem('selectedTabUrl', false);
			}
		}
	}

});