$(document).ready(function() { 

	var form = $('#items'),
		application = $('#application').val(),
		workflowState = $('#state'),
		sendmail = $('#sendmail'),
		sendmailTemplate = $('#sendmail-template'),
		previewMailTemplate = $('#preview-mail-template'),
		testMailTemplate = $('#test-mail-template'),
		editMailContent = $('#mail_template_view_modal'),
		ordersheet = $('#ordersheet').val(),
		ordersheets = $('#ordersheets').val();


	retailnet.loader.init();

// wysiwyg :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	tinymce.init({
		selector: "#mail_template_text",
		plugins: ["advlist autolink lists textcolor link visualblocks code nonbreaking contextmenu paste preview"],
		width: '100%',
		height: 400,
		menubar : false,
		statusbar : false,
		toolbar_items_size: 'small',
		autosave_restore_when_empty: false,
		toolbar: "fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor | removeformat | paste pastetext pasteword",
		setup : function(ed) {
			ed.on('change', function(e) {
				$('#mail_template_text').val(ed.getContent())
			});
		}
	});	


// calcullate total price ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$('table input[type=text]').change(function() {

		var table = $(this).closest('table'),
			row = $(this).closest('tr'),
			lastcel = $("td.number:last", row),
			totalCels = 'td.'+lastcel.attr('class').split(' ')[0],
			price = parseFloat($('.mps_ordersheet_item_price', row).text()),
			totalprice=0, 
			totalcolumn=0;

		// find last non zero quantites
		$($("input:text", row).get().reverse()).each(function(i,elem) {
			if (!totalprice && $(elem).val() >= 0) {
				totalprice = $(elem).val()*price;
			}
		});

		// assign calcullated total price to last cel in row
		lastcel.text(totalprice.toFixed(2));

		// calculate all total prices for this table
		$($(totalCels, table)).each(function(i,elem) {
			totalcolumn = totalcolumn + parseFloat($(this).text());
		});	

		// assign calcullatet total pricees on table bottom
		$('b.'+table.attr('id')).text(totalcolumn.toFixed(2));
	});


// submitting ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$('a.submit-items').on('click', function(e) {

		e.preventDefault();
		
		retailnet.notification.hide();

		var error = false;
		var self = $(this);
		var template = self.data('template');
		var sendmailTrigger = self.hasClass('sendmail') ? 1 : 0;
		var state = self.data('state');
		
		// saction vars
		sendmail.val(sendmailTrigger);
		workflowState.val(state);

		if (self.attr('id')=='revision') {
			var revisions = $('input.item_revision').length ;
			var inRevisions = $('input.item_revision:checked').length;
			error = revisions>0 && inRevisions==0 ? 'Please, select at least one Item in Revision' : '';
		}
		
		if (!error) {

			// set form action
			form.attr('action', self.attr('href'))
			
			// load mail template data
			if (sendmail.val()==1 && template && template != $('#mail_template_id').val()) {

				// get order sheet data
				var render = retailnet.ajax.request('/applications/modules/ordersheet/ajax.php', {
					application: application,
					section: 'ordersheet',
					id: ordersheet
				}) || {};
				
				// get mail template data
				var xhr = retailnet.ajax.request('/applications/helpers/ajax.mailtemplate.php', {
					id: self.data('template'),
					render: render
				}) || {};

				// assign template data
				$('#mail_template_id').val(xhr.mail_template_id);
				$('#mail_template_subject').val(xhr.mail_template_subject);
				$('#mail_template_text').val(xhr.mail_template_text);
				$('#mail_template_view_modal').val(xhr.mail_template_view_modal);

				tinymce.get('mail_template_text').setContent(xhr.mail_template_text);
			}

			// show mail template form
			if (sendmail.val()==1 && editMailContent.val()==1) modalMailTemplate.resize().show();
			else form.submit();
		}
		else retailnet.notification.error(error);

		return false;
	});


// submit items ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	form.submit(function(e) {

		e.preventDefault();
			
		var self = $(this);
		var data = $('#items, form.request').serialize();

		retailnet.loader.show();

		retailnet.ajax.json(self.attr('action'), data).done(function(xhr) {
				
			var response = xhr || {};
			var reloadPage = false;

			retailnet.loader.hide();

			// show request message
			if (response.message) {
				if (response.response) retailnet.notification.success(response.message);
				else retailnet.notification.error(response.message);
			}

			// required to send mails
			if (response.response && response.sendmail==1) {
				
				var sendMailRespone = sendMails() || {};

				if (sendMailRespone.response) retailnet.notification.success(sendMailRespone.notification);
				else retailnet.notification.error(sendMailRespone.notification);

				reloadPage = sendMailRespone.response ? response.reload : false;
				
			} else {
				reloadPage = response.reload;
			}

			// page reloader / form reset
			if (reloadPage) window.location.reload();
		});

		return false;
	});


// sendmail ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var modalMailTemplate = sendmailTemplate.adoModal({
		width: 800,
		height: 700,
		clickClose: false
	});

	// button: send mail
	$('.sendmail', sendmailTemplate).on('click', function(e) {

		e.preventDefault();
		
		var self = $(this),
			url = self.attr('href'),
			required = $('.required', sendmailTemplate),
			failures = $('.required[value=]', sendmailTemplate);

    	if (required.length > 0 && failures.length > 0) {
    		$('.required', sendmailTemplate).removeClass('error');
    		$('.required[value=]', sendmailTemplate).addClass('error');
    		retailnet.notification.error('Red marked fields are required.');
    	} else {
    		modalMailTemplate.close();
			form.submit(); 
    	}
	});
	
	var sendMails = function() {

		var self = $(this);
		var url = $('form', sendmailTemplate).attr('action');
		var data = $('#sendmail-template form, form.request').serialize();

		retailnet.loader.show();
			
		var xhr = retailnet.ajax.request(url, data);

		if (xhr) {
			retailnet.loader.hide();
		}

		return xhr || {};
	}


// preview mail :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var modalMailPreview = previewMailTemplate.adoModal({
		width: 1000,
		height: 600,
		overlay: {
			opacity: 0.75,
			background: '#000'
		}
	});

	$('.preview-mail', sendmailTemplate).on('click', function(e) {

		e.preventDefault();
		
		var self = $(this);
		var url = self.attr('href');
		var data = $('#sendmail-template form, form.request').serialize();
		
		retailnet.loader.show();
		
		retailnet.ajax.json(url,data).done(function(xhr) {

			retailnet.loader.hide();

			if (xhr && xhr.response) {

				// assign data from request
				$('.mail-subject', previewMailTemplate).html(xhr.subject);
				$('.mail-address', previewMailTemplate).text(xhr.email);
				$('.mail-cc-address', previewMailTemplate).text(xhr.cc);
				$('.mail-cc-address', previewMailTemplate).parent().toggle(xhr.cc ? true : false);
				$('.mail-date', previewMailTemplate).text(xhr.date);
				$('.mail-content', previewMailTemplate).html(xhr.content);
				$('.mail-footer', previewMailTemplate).html(xhr.footer);
				$('.mail-footer', previewMailTemplate).parent().toggle(xhr.footer ? true : false);

				modalMailPreview.resize().show();

			} else {
				modalMailPreview.close();
			}

			if (xhr && xhr.notification) {
				if (xhr.response) retailnet.notification.success(xhr.notification);
				else retailnet.notification.error(xhr.notification);
			}
		});

	});

// test mail :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var modalTestMail = testMailTemplate.adoModal({
		width: 400
	});

	$('.test-mail', sendmailTemplate).on('click', function(e) {
		e.preventDefault();
		modalTestMail.show();
	});

	$('.apply', testMailTemplate).on('click', function(e) {
		
		e.preventDefault();

		var url = $(this).attr('href');
		var data = $('#sendmail-template form, form.request').serializeArray();
		var recipient = $('#test-mail-recipient').val();
		
		if (recipient) {

			data.push({
				name: 'test_recipient', 
				value: recipient
			});
			
			retailnet.loader.show();
			
			retailnet.ajax.json(url,data).done(function(xhr) {

				if (xhr && xhr.response) {
					modalTestMail.close();
				}

				if (xhr && xhr.notification) {
					if (xhr.response) retailnet.notification.success(xhr.notification);
					else retailnet.notification.error(xhr.notification);
				}

			}).complete(function() {
				retailnet.loader.hide();
			});
			
		} else {
			retailnet.notification.error('Recipient e-mail is required.');
		}
	});

});