$(document).ready(function() {

	var form = $("#posCustomerServices");
	
	// spiner instance	
	retailnet.loader.init();
	
	// tooltips
	retailnet.tooltip.init();
	
	form.validationEngine({
		autoHidePrompt: true,
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		onAjaxFormComplete: function(status, form, json, options) { 

			retailnet.loader.hide();
			retailnet.notification.hide();

			if (json) {

				if (json.response && json.redirect) { 
					window.location=json.redirect;
				}

				if (json.message) {
					if (json.response) retailnet.notification.success(json.message);
					else retailnet.notification.error(json.message);
				}
			}
		}
	});
	
	$("#save").click(function(event) { 
		
		if (form.validationEngine('validate')) {
			
			retailnet.loader.show();
			
			form.submit();
			
		} else {
			
			retailnet.notification.error('Please check form data');
		}

	});


});