$(document).ready(function() {

	var form = $("#posstore");
	var hints = ['Bad', 'Poor', 'Good', 'Very Good', 'Excellent'];
	var readonly = (form.attr('tag')) ? true : false;
	
	// spiner instance	
	retailnet.loader.init();
	
	// tooltips
	$('.-tooltip').qtip({style: {classes:'qtip qtip-dark' }});
	
	form.validationEngine({
		autoHidePrompt: true,
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		onAjaxFormComplete: function(status, form, json, options) { 

			retailnet.loader.hide();
			retailnet.notification.hide();

			if (json) {

				if (json.response && json.redirect) { 
					window.location=json.redirect;
				}

				if (json.message) {
					if (json.response) retailnet.notification.success(json.message);
					else retailnet.notification.error(json.message);
				}
			}
		}
	});

	
	// omload trigger first location
	$('.locations input:first').trigger('change');
	
	
	$("#save").click(function(event) { 
		
		event.preventDefault();

		retailnet.notification.hide();
		
		if (form.validationEngine('validate')) {
			retailnet.loader.show();
			form.submit();
		} else {
			retailnet.notification.error(message);
		}
	});

	$('#posaddress_store_furniture').chainedSelect('#posaddress_store_furniture_subclass', {
		parameters: {
      		section: 'product_line_subclasses',
      		application: $('#application').val()
      	}
	}).trigger('change'); 

	var v;

	v = $('#posaddress_perc_class').attr('tag');
	v = v ? 6-v : 0;

	$('#posaddress_perc_class').raty({ 
		scoreName: 'posaddress_perc_class', 
		start: v, 
		readOnly: readonly, 
		path: '/public/images', 
		hintList: hints ,
		target : '#posaddress_perc_class_target',
		click: function(score, evt) {
			$('#posaddress_perc_class_target').text(hints[score-1])
		} 
	})

	v = $('#posaddress_perc_tourist').attr('tag');
	v = v ? 6-v : 0;
	
	$('#posaddress_perc_tourist').raty({ 
		scoreName: 'posaddress_perc_tourist',
		start: v, 
		readOnly: readonly, 
		path: '/public/images', 
		hintList: hints,
		target : '#posaddress_perc_tourist_target',
		click: function(score, evt) {
			$('#posaddress_perc_tourist_target').text(hints[score-1])
		} 
	});

	v = $('#posaddress_perc_transport').attr('tag');
	v = v ? 6-v : 0;

	$('#posaddress_perc_transport').raty({ 
		scoreName: 'posaddress_perc_transport', 
		start: v, 
		readOnly: readonly,
		path: '/public/images', 
		hintList: hints,
		target : '#posaddress_perc_transport_target',
		click: function(score, evt) {
			$('#posaddress_perc_transport_target').text(hints[score-1])
		} 
	});

	v = $('#posaddress_perc_people').attr('tag');
	v = v ? 6-v : 0;

	$('#posaddress_perc_people').raty({ 
		scoreName: 'posaddress_perc_people', 
		start: v, 
		readOnly: readonly, 
		path: '/public/images', 
		hintList: hints,
		target : '#posaddress_perc_people_target',
		click: function(score, evt) {
			$('#posaddress_perc_people_target').text(hints[score-1])
		} 
	});

	v = $('#posaddress_perc_parking').attr('tag');
	v = v ? 6-v : 0;

	$('#posaddress_perc_parking').raty({ 
		scoreName: 'posaddress_perc_parking', 
		start: v, 
		readOnly: readonly, 
		path: '/public/images', 
		hintList: hints,
		target : '#posaddress_perc_parking_target',
		click: function(score, evt) {
			$('#posaddress_perc_parking_target').text(hints[score-1])
		} 
	});

	v = $('#posaddress_perc_visibility1').attr('tag');
	v = v ? 6-v : 0;

	$('#posaddress_perc_visibility1').raty({ 
		scoreName: 'posaddress_perc_visibility1', 
		start: v, 
		readOnly: readonly,
		path: '/public/images', 
		hintList: hints,
		target : '#posaddress_perc_visibility1_target',
		click: function(score, evt) {
			$('#posaddress_perc_visibility1_target').text(hints[score-1])
		} 
	});

	v = $('#posaddress_perc_visibility2').attr('tag');
	v = v ? 6-v : 0;

	$('#posaddress_perc_visibility2').raty({ 
		scoreName: 'posaddress_perc_visibility2', 
		start: v, 
		readOnly: readonly, 
		path: '/public/images', 
		hintList: hints,
		target : '#posaddress_perc_visibility2_target',
		click: function(score, evt) {
			$('#posaddress_perc_visibility2_target').text(hints[score-1])
		} 
			
	});

	$('.raty-target').each(function(i,e) {

		var el = $(e);
		var hit = el.prev().attr('tag');

		if (hit) {
			el.text(hints[5-hit])
		}
	});
});