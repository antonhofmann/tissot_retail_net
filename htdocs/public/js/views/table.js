define(function(require, exports, module) {

	var $ = require('jquery'),
		BaseView = require('./view'),
		DataTables = require('datatables'),
		DataTableBootstrap = require('datatablesBootstrap'),
		DataTableResponsive = require('datatablesResponsive'),
		Labels = require('i18n!nls/labels');

	var TableView = BaseView.extend({
		
		el: '.list-databtables',
		
		options: {
			autoRender: true,
			language: {
				search: '',
				searchPlaceholder: Labels.search
			}
		},

		initialize: function(options) {
			
			this.options = $.extend(this.options, options || {});
			
			if (this.options.autoRender) { 
				this.render(this.options);
			}
		},

		render: function(options) {
			this.$el.DataTable(options);
		}

	})

	return TableView;
});