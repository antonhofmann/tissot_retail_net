define(function(require, exports, module) {

	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone'),
		BaseView = require('./view'),
		validator = require('libs/formvalidation/js/formValidation.min'),
		validatorBS = require('libs/formvalidation/js/framework/bootstrap.min'),
		Notify = require('views/notify'),
		Spin = require('views/spin'),
		Labels = require('i18n!nls/labels'); 

	var FormView = BaseView.extend({
		
		options: {
			live: 'disabled',
			icon: {
				valid: 'glyphicon glyphicon-ok',
				invalid: 'glyphicon glyphicon-remove',
				validating: 'glyphicon glyphicon-refresh'
			},
			err: {
				container: 'tooltip'
			}
		},

		initialize: function(options) { 
			
			this.options = $.extend(this.options, options);
		},

		render: function() { 

			// start validator instance
			this.$el.formValidation(this.options);

			// bind events
			this.bindEvents();

			// add form as spin base element
			this.spin = new Spin();
			this.spin.setElement(this.$el);
			this.spin.render();
		},

		getValidator: function() {

			return this.$el.data('formValidation');
		},

		bindEvents: function() { 

			var $this = this;

			// on success
			this.$el.on('success.form.fv', function(e, data) {

				e.preventDefault();

				Notify.close();

				var validator = $this.getValidator();

				$this.$el.trigger('beforeSubmit');

				validator.disableSubmitButtons(true);

				if ($this.$el.data('spinner')) {
					$this.spin.show();
				}

				$this.submit();
	        })

			// on error
	        this.$el.on('err.form.fv', function(e) {
	        	console.log(e);
	        })
		},

		submit: function() { 

			var $this = this;

			Backbone.ajax({
				type: 'POST',
				dataType: "json",
				url: $this.$el.attr('action'),
				data: $this.$el.serialize(),
				success: function(xhr) {  

					var xhr = xhr || {};
					var validator = $this.getValidator();

					validator.disableSubmitButtons(true);

					if (xhr.debugging) {

						$this.$el.trigger('afterSubmit', [xhr, true]);
						
						validator.disableSubmitButtons(false);

						if ($this.$el.data('spinner')) {
							$this.spin.hide();
						}
					}

					// show errors
					if (xhr.errors && xhr.errors.length > 0) {

						$.each(xhr.errors, function(i, message) {
							Notify.bartop({
								type: 'error',
								title: 'Error',
								text: message
							});
						});

						validator.resetForm(); 

						return;
					}

					if (xhr.debugging) return;

					// redirect url
					if (xhr.redirect) {
						window.location = xhr.redirect;
						return;
					} 
					
					// show messages
					if (xhr.messages && xhr.messages.length > 0) {
						$.each(xhr.messages, function(i,message) {
							if (typeof message === 'object') Notify.show(message);
							else Notify.success(message);
						});
					}

					// update fields
					if (xhr.fields && xhr.fields.length > 0) {
						$.each(xhr.fields, function(field,value) {
							$(field).val(value);
						});
					}

					$this.$el.trigger('afterSubmit', [xhr, true]);
					
					validator.disableSubmitButtons(false);

					if ($this.$el.data('spinner')) {
						$this.spin.hide();
					}
				}
			});
		}

	})

	return FormView;
});