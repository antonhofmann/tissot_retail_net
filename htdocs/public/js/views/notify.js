/**
* Represents a generic modal notification view
* 
* It requires that the pnotify/pnotify.custom.min.css file is included.
*/
define(function(require, exports, module) {

	require('pnotify');

	var defaults = {
		type: 'notice',
		styling: 'bootstrap3', 
		icon: false,
		history: false,
		delay: 12000,
		opacity: 0.9,
		buttons: {
			closer: true,
			closer_hover: true,
			sticker: false
		}
	}

	var stack_topleft = {"dir1": "down", "dir2": "right", "push": "top"};
	var stack_bottomleft = {"dir1": "right", "dir2": "up", "push": "top"};
	var stack_bar_top = {"dir1": "down", "dir2": "right", "push": "top", "spacing1": 0, "spacing2": 0};
	var stack_bar_bottom = {"dir1": "up", "dir2": "right", "spacing1": 0, "spacing2": 0};

	return {

		notice: function(text, settings) {
			new PNotify($.extend(defaults, {
				type: 'notice',
				text: text,
				delay: 8000
			}, settings));
		},

		info: function(text, settings) {
			new PNotify($.extend(settings, {
				type: 'info',
				title: 'Info',
				delay: 8000,
				text: text
			}, settings))
		},

		success: function(text, settings) { 
			new PNotify($.extend(settings, {
				type: 'success',
				title: 'Success',
				text: text,
				delay: 8000
			}, settings));
		},	

		error: function(text, settings) { 
			new PNotify($.extend(defaults, {
				type: 'error',
				title: 'Error',
				text: text,
				delay: 20000
			}, settings));
		},

		show: function(settings) {
			new PNotify($.extend(defaults, settings));
		},
		
		close: function() {
			PNotify.removeAll();
		},

		bartop: function(settings) {
			new PNotify($.extend(defaults, {
				cornerclass: "",
				width: "100%",
				stack: stack_bar_top,
				animation: 'slide',
				addclass: "stack-bar-top",
				delay: 20000
			}, settings))
			.get()
			.css({
				top:0,
				buttom:'auto'
			})
		},

		barbottom: function(settings) {
			new PNotify($.extend({}, defaults, {
				cornerclass: "",
				width: "100%",
				stack: stack_bar_bottom,
				animation: 'slide',
				addclass: "stack-bar-bottom",
				delay: 20000
			}, settings))
			.get()
			.css({
				top:'auto',
				bottom:0
			});
		},

		custom: function(settings) {

			if (!settings.custom || !this[settings.custom]) return;

			this[settings.custom](settings);
		}
	}

});