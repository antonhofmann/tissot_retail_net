/**
* Represents a generic modal popup view
* 
* This is a Backbone View wrapper for adomodal
* It requires that the adomodal css file is included.
*/
define(function(require, exports, module) {

	var $ = require('jquery'),
		_ = require('underscore'),
		Bootstrap = require('bootstrap'),
		BaseView = require('./view'),
		mustache = require('mustache'),
		tpl = require('stache!modal/bootstrap');

	var BaseModalView = BaseView.extend({

		className: 'modal fade',

		events: {
			'hidden': 'teardown'
		},

		initialize: function(attributes, options) {

			if (attributes.class) {
				this.className= 'modal fade '+attributes.class;
			}

			this.attributes = $.extend(this.attributes, attributes);
			
			this.$el.modal($.extend({
				show: false
			}, options));

			this.$el.appendTo('body');

			_.bindAll(this, 'show', 'remove', 'render');
		},

		show: function() {
			this.$el.modal('show');
		},

		hide: function() {
			this.$el.modal({show:false});
		},

		remove: function() {
			this.$el.data('modal', null);
			this.remove();
		},

		render: function(data) {

			data = typeof data==='object' ? data : {content: data};
			data.classHeader = data.title ? null : 'hidden';
			data.classFooter = data.buttons ? null : 'hidden';

			var content = tpl(data);

			this.$el.empty().html(content);
		}
	});

	return BaseModalView;

});
