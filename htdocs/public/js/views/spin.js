/**
* Represents a generic modal notification view
* 
* It requires that the /public/assets/spinner/spinner.css file is included.
*/
define([
	'jquery', 
	'underscore', 
	'views/view', 
	'mustache'
], function($, _, BaseView, Mustache) {

	var Spin = BaseView.extend({
		// the element to attach spinner to
		el: 'body',
		// preloaded templates
		templates: {},
		// internal var to know if we have to complete render
		renderIncomplete: false,
		// default options
		options: {
			autoRender: false,
			responsive: true,
			theme: 'sk-spinner-theme-dark',
			template: 'fading.circle'
		},

		/**
		* Spin initialize
		* 
		* @param {object} options
		*/
		initialize: function(param) {

			if (param && typeof param != 'object') {
				this.setElement(param);
			} 
			
			// override any defaults in this.options
			this.options = $.extend(this.options, param);

			// close spinn on esc
			if (this.options.closeOnEsc) {
				$(document).keyup(_.bind(function(e) {
					if (e.keyCode == 27) this.hide();
				}, this));
			}

			this.setTemplate(this.options.template);
		},

		/**
		* Render modal
		* @return {object} Reference to view
		*/
		render: function() {

			var that = this;

			if (!this.$el.length) {
				this.setElement('body');
			}

			this.$el.addClass('sk-spinner-holder');

			// template hasn't loaded yet, mark render as incomplete and let template load-callback handle it
			if (!this.templates[this.selectedTpl]) {
				this.renderIncomplete = true;
				return;
			}

			this._completeRender();
			return this;
		},

		/**
		 * Show spin
		 * 
		 * @return base view
		 */
		show: function(delay) { 

			var that = this;
			var delay = delay || this.options.delay

			this.$el.addClass('spin-show');

			if (delay > 0) { 
				setTimeout(function() { 
					that.hide();
				}, delay);
			}
		},

		/**
		 * Hide spin
		 * 
		 * @return base view
		 */
		hide: function() {
			this.$el.removeClass('spin-show');
		},

		/**
		* Remove spin from dom
		* 
		* @return {object} Reference to view
		*/
		remove: function() {

			var spinner = $('.sk-spinner-container', this.$el)
			
			spinner.remove();
			spinner.parent().removeClass('sk-spinner-holder');
			
			return this;
		},

		/**
		 * Complete render once template is loaded
		 * @return {void}
		 */
		_completeRender: function() {
			var content = this.templates[this.selectedTpl](_.pick(this.options, ['theme']));

			// remove current spinner
			$('.sk-spinner-container', this.$el).remove();
			
			// add spinner markup
			this.$el.append(content);

			// hide spinner container
			//$('.sk-spinner-container', this.$el).hide();
			
			// strach spinner backdrop to browser window
			if (this.$el.prop('tagName').toLowerCase() == 'body') {
				$('.sk-spinner-container', this.$el).css({
					position: 'fixed',
					top: 0,
					bottom: 0,
					left: 0,
					right: 0
				});
			}

			// responsive breakpoints classes
			var that = this;
			$(window).on('resize', function() {

				that.$el.removeClass(function (index, css) {
					return (css.match (/(^|\s)spin-size-\S+/g) || []).join(' ');
				});

				var width = that.$el.width();

				// extra large size
				if (width > 1600) that.$el.addClass('spin-size-xl');
				// large size
				if (width >= 1200 && width <= 1600) that.$el.addClass('spin-size-lg');
				// medium size
				if (width >= 768 && width < 1200) that.$el.addClass('spin-size-md');
				// small size
				if (width >= 480 && width < 768) that.$el.addClass('spin-size-sm');
				// extra small size
				if (width < 480) that.$el.addClass('spin-size-xs');
				
			}).trigger('resize');	

			// automatically show
			if (this.options.autoRender) {
				this.show();
			}

			this.renderIncomplete = false;
		},

		/**
		 * Load the spinner template asyncronously
		 * @param {string} Template name
		 * @return {object} Reference to view
		 */
		setTemplate: function(tpl) { 

			var that = this;
			this.selectedTpl = tpl || this.options.template; 

			if (!this.templates[tpl]) {
				// save loaded template as callback
				var callback = function(tpl) { 
					that.templates[that.selectedTpl] = tpl;
					if (that.renderIncomplete) {
						that._completeRender();
					}
				};

				switch (this.selectedTpl) {

					case 'rotating.plane': 
						require(['stache!spin/rotating.plane'], callback);
						break;

					case 'double.bounce':
						require(['stache!spin/double.bounce'], callback);
						break;

					case 'wave':
						require(['stache!spin/wave'], callback);
						break;

					case 'wandering.cubes':
						require(['stache!spin/wandering.cubes'], callback);
						break;

					case 'pulse':
						require(['stache!spin/pulse'], callback);
						break;

					case 'chasing.dots':
						require(['stache!spin/chasing.dots'], callback);
						break;

					case 'three.bounce':
						require(['stache!spin/three.bounce'], callback);
						break;

					case 'circle':
						require(['stache!spin/circle'], callback);
						break;

					case 'fading.circle':
						require(['stache!spin/fading.circle'], callback);
						break;

					case 'cube':
						require(['stache!spin/cube'], callback);
						break;

					case 'wordpress':
						require(['stache!spin/wordpress'], callback);
						break;
				} 
			}	

			return this;
		}
	});

	return Spin;
})