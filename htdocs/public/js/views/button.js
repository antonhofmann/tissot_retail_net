/**
 * Represents a dynamically built button
 */
define(function(require, exports, module) {

	var $ = require('jquery'),
		BaseView = require('./view');

	var ButtonView = BaseView.extend({
		
		tagName: 'button',
		
		attributes: { class: 'button' },

		/**
		* Create button
		* Pass in any custom attributes in the 'attr' key of options.
		* @param {object} options
		*/
		initialize: function(options) {
			
			var options = options || {};
			
			if (options.tagName) {
				this.tagName = options.tagName;
			}
			
			this.attributes = $.extend(this.attributes, options.attr || {});
		},

		/**
		* Render button
		* @param  {string} text on button
		* @return {object} jquery element
		*/
		render: function(text) {
			
			this.$el.attr(this.attributes);
			
			var $label = $('<span/>').addClass('label').text(text);
		
			this.$el.append($label);
			
			return this.$el;
		}
	});

	return ButtonView;
	
});
