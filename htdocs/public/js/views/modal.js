/**
 * Represents a generic modal popup view
 * This is a Backbone View wrapper for adomodal
 * It requires that the adomodal css file is included.
 */
define(function(require, exports, module) {

  var $ = require('jquery'),
      _ = require('underscore'),
      BaseView = require('./view'),
      modal = require('libs/adomodal/adomodal'),
      mustache = require('mustache'),
      // templates
      tpl = require('stache!base/modal');

  var ModalView = BaseView.extend({
    tagName: 'div',
    attributes: { class: 'ado-modal ado-box' },
    options: {
      // default dimensions
      width: 800,
      height: 700,
      // close modal on click of body?
      clickClose: true,
      // make sure user doesn't have unsaved data when using this
      closeOnEsc: false,
      // override various labels
      lblCancel: 'Cancel',
      lblTitle: '',
      lblSave: 'Save',
      // fired when save button is clicked. modal element is passed as an arg
      onSave: function(modal) {},
      // fired after modal is opened
      afterOpen: function(modal) {},
      // fired after modal is closed
      afterClose: function(modal)  {},
      // pass in a dom node or jQuery element for the body
      body: null
    },
    // will carry instance of modal after initialization
    modal: null,
    // map events
    events: {
      'click .save': function() {
        this.options.onSave(this.$el);
        this.close();
      }
    },

    /**
     * Create modal
     * Options: See this.options
     * @param {object} options
     */
    initialize: function(options) {
      // override any defaults in this.options
      this.options = _.defaults(_.pick(options, _.keys(this.options)), this.options);
      // if body is a jquery object, get it's html
      if (this.options.body instanceof $) {
        this.options.body = this.options.body.prop('outerHTML');
      }
      // if body is a html object, get it's html
      else if (typeof(this.options.body) == 'object' && this.options.body.tagName) {
        this.options.body = this.options.body.outerHTML;
      }
      this.render();

      // close modal on esc
      if (this.options.closeOnEsc) {
        $(document).keyup(_.bind(function(e) {
          if (e.keyCode == 27) {
            this.close();
          }
        }, this));
      }
    },

    /**
     * Render modal
     * @return {object} Reference to view
     */
    render: function() {
      // render tpl and append to body
      this.$el.html(tpl(_.pick(this.options, ['lblTitle', 'lblCancel', 'lblSave', 'body'])))
        .appendTo('body');

      // pick adomodal options from our options
      var options = _.pick(this.options, ['width', 'height', 'afterOpen']);
      options.afterClose = _.bind(function() {
        this.options.afterClose(this.$el);
        this.remove();
        this.undelegateEvents();
      }, this);

      // create modal
      this.modal = this.$el.adoModal(options);
      return this;
    },

    /**
     * Show modal
     * @return {object} Reference to view
     */
    show: function() {
      this.modal.resize().show();
      return this;
    },

    /**
     * Close modal
     * @return {object} Reference to view
     */
    close: function() {
      this.modal.close();
      return this;
    }
  });

  return ModalView;
});
