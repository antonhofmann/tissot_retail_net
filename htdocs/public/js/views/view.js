define(function(require, exports, module) {

	var Backbone = require('backbone');

	/**
	 * Base view. Always extend this view instead of Backbone.View
	 * so we can add common behavior later on if necessary.
	 */
	var BaseView = Backbone.View.extend({});

	return BaseView;

});
