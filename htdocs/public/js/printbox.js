$(document).ready(function() {

	$('.print-box .dd').nestable({
			group: 0,
        	maxDepth: 1
		})

	$('.print-box .dd').on('change', function(e) {
		
		var self   = e.length ? e : $(e.target),
			output = self.data('output'),
			data = self.nestable('serialize'),
			navigaions = [];

		$.each(data, function(i, nav) {
			navigaions.push(nav.name);
		})

		$.post('/applications/helpers/user.ajax.php', {
			section: 'submit.preference',
			user: $('#user_id').val(),
			entity: 'items.print',
			name: 'fields',
			value: navigaions
		}, function(xhr) {

			xhr = xhr || {}

			if (xhr.errors && xhr.errors.length > 0) {

				$.each(xhr.errors, function(i, message) {
					Notify.bartop({
						type: 'error',
						title: 'Error',
						text: message
					});
				});

				return;
			}
		})
	})
})