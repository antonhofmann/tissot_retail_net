$(document).ready(function() {

	var form = $('form.request'),
		application = $('#application').val(),
		sendmailTemplate = $('#sendmail-template'),
		previewMailTemplate = $('#preview-mail-template'),
		testMailTemplate = $('#test-mail-template'),
		ordersheets = $('#ordersheets'),
		mailTemplateID = $('#mail_template_id'),
		editMailContent = $('#mail_template_view_modal');

	// loader instance 
	retailnet.loader.init();
	
// loaders :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$('#reports').tableLoader({
		url: '/applications/modules/ordersheet/reports/list.php',
		filters: $('form.request'),
		after: function(self) {

			// dropdown builders
			$('select',self).dropdown();

			// reset ordersheets
			$('select',self).change(function() {
				ordersheets.val('')
			});

			$('.table-toggler', self).click(function(e) {
				e.preventDefault();
				$(this).closest('tr').next().slideToggle();
				$('.fa', $(this)).toggleClass('fa-chevron-circle-right').toggleClass('fa-chevron-circle-down');
			});
		}
	});

// wysiwyg :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	tinymce.init({
		selector: "#mail_template_text",
		plugins: ["advlist autolink lists textcolor link visualblocks code nonbreaking contextmenu paste preview"],
		width: '100%',
		height: 400,
		menubar : false,
		statusbar : false,
		toolbar_items_size: 'small',
		autosave_restore_when_empty: false,
		toolbar: "fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor | removeformat | paste pastetext pasteword",
		setup : function(ed) {
			ed.on('change', function(e) {
				$('#mail_template_text').val(ed.getContent())
			});
		}
	});	


// sendmail ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var modalMailTemplate = sendmailTemplate.adoModal({
		width: 800,
		height: 700,
		clickClose: false
	});

	$(document).on('click', 'button.send-mail', function(e) {
		
		e.preventDefault();

		retailnet.notification.hide();

		var ordersheet = $(this).data('ordersheet');
		var template = $(this).data('template');
		
		ordersheets.val(ordersheet);
		mailTemplateID.val(template);

		if (!ordersheet) {
			retailnet.notification.error('The order sheet not found.');
			return false;
		}
		
		if (!template) {
			retailnet.notification.error('The mail template not found.');
			return false;
		}

		// get ordersheet data
		var render = retailnet.ajax.request('/applications/modules/ordersheet/ajax.php', {
			id: ordersheet,
			application: application,
			section: 'ordersheet'
		}) || {};

		// mail template popullate	
		var mail = retailnet.ajax.request('/applications/helpers/ajax.mailtemplate.php', {
			id: template,
			render: render
		}) || {};

		$('#mail_template_subject').val(mail.mail_template_subject);
		$('#mail_template_text').val(mail.mail_template_text);
		$('#mail_template_view_modal').val(mail.mail_template_view_modal);
		tinymce.get('mail_template_text').setContent(mail.mail_template_text);
		
		if (editMailContent.val()==1) modalMailTemplate.show();
		else $('form', sendmailTemplate).submit();

		return false;
	});

	// button: send mail
	$('.apply', sendmailTemplate).on('click', function(e) {

		e.preventDefault();

		retailnet.notification.hide();
		
		var self = $(this),
			required = $('.required', sendmailTemplate),
			failures = $('.required[value=]', sendmailTemplate);

    	if (required.length > 0 && failures.length > 0) {
    		$('.required', sendmailTemplate).removeClass('error');
    		$('.required[value=]', sendmailTemplate).addClass('error');
    		retailnet.notification.error('Red marked fields are required.');
    	} else {
    		modalMailTemplate.close();
    		$('form', sendmailTemplate).submit();
    	}
	});

	// submit mail form
	$('form', sendmailTemplate).submit(function(e) {

		e.preventDefault();

		retailnet.notification.hide();
		retailnet.loader.show();

		var self = $(this);
		var data = $('#sendmail-template form, form.request').serialize();

		retailnet.ajax.json(self.attr('action'), data).done(function(xhr) {

			xhr = xhr || {};

			retailnet.loader.hide();

			if (xhr.response) {

				retailnet.loader.show();

				// update shipped raports
				retailnet.ajax.json('/applications/modules/ordersheet/reports/submit.php', {
					id: ordersheets.val(),
					application: application
				}).done(function(xhr) {

					xhr = xhr || {};

					retailnet.loader.hide();

					if (xhr.response) {
						var ordersheet = ordersheets.val();
						var row = $('#row-'+ordersheet);
						$('.order-header h5', row).text('Notified on '+xhr.date);
						$('td.sendmail_date', $('tr.row-'+ordersheet)).text(xhr.date);
						$('.order-actions', row).remove();
						$('.order-container', row).removeClass('unnotified');
					}

					if (xhr.notification) {
						if (xhr.response) retailnet.notification.success(xhr.notification);
						else retailnet.notification.error(xhr.notification);
					}
				});
			}

			if (xhr.notification) {
				if (xhr.response) retailnet.notification.success(xhr.notification);
				else retailnet.notification.error(xhr.notification);
			}
		});

		return false;
	});

// preview mail ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var modalMailPreview = previewMailTemplate.adoModal({
		width: 1000,
		height: 600,
		overlay: {
			opacity: 0.75,
			background: '#000'
		}
	});

	$(document).on('click', 'button.preview-mail', function(e) {

		e.preventDefault();

		retailnet.notification.hide();

		var self = $(this);
		var ordersheet = $(this).data('ordersheet');
		var template = $(this).data('template');

		ordersheets.val(ordersheet);
		mailTemplateID.val(template);

		if (!ordersheet) {
			retailnet.notification.error('The order sheet not found.');
			return false;
		}
		
		if (!template) {
			retailnet.notification.error('The mail template not found.');
			return false;
		}

		// get data
		var data = $('#sendmail-template form, form.request').serialize();

		retailnet.loader.show();
		
		retailnet.ajax.json('/applications/modules/ordersheet/sendmail.preview.php', data).done(function(xhr) {

			retailnet.loader.hide();

			if (xhr && xhr.response) {

				// assign data from request
				$('.mail-subject', previewMailTemplate).html(xhr.subject);
				$('.mail-address', previewMailTemplate).text(xhr.email);
				$('.mail-cc-address', previewMailTemplate).text(xhr.cc);
				$('.mail-cc-address', previewMailTemplate).parent().toggle(xhr.cc ? true : false);
				$('.mail-date', previewMailTemplate).text(xhr.date);
				$('.mail-content', previewMailTemplate).html(xhr.content);
				$('.mail-footer', previewMailTemplate).html(xhr.footer);
				$('.mail-footer', previewMailTemplate).parent().toggle(xhr.footer ? true : false);

				modalMailPreview.resize().show();

			} else {
				modalMailPreview.close();
			}

			if (xhr && xhr.notification) {
				if (xhr.response) retailnet.notification.success(xhr.notification);
				else retailnet.notification.error(xhr.notification);
			}
		});

		return false;
	});

// test mail :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var modalTestMail = testMailTemplate.adoModal({
		width: 400,
		overlay: {
			opacity: 0.75,
			background: '#0000ff'
		}
	});

	$('#reports').on('click', 'button.test-mail', function(e) {
		
		e.preventDefault();

		var ordersheet = $(this).data('ordersheet');
		ordersheets.val(ordersheet);
		
		if (ordersheet) modalTestMail.show();
		else retailnet.notification.error("The order sheet not found");

		return false;
	});

	$('.apply', testMailTemplate).on('click', function(e) {
		
		e.preventDefault();

		retailnet.notification.hide();

		var url = $(this).attr('href');
		var data = $('#sendmail-template form, form.request').serializeArray();
		var recipient = $('#test-mail-recipient').val();
		
		if (recipient) {

			data.push({name: 'test_recipient', value: recipient});
			
			retailnet.loader.show();
			
			retailnet.ajax.json(url,data).done(function(xhr) {

				if (xhr && xhr.response) {
					modalTestMail.close();
				}

				if (xhr && xhr.notification) {
					if (xhr.response) retailnet.notification.success(xhr.notification);
					else retailnet.notification.error(xhr.notification);
				}

			}).complete(function() {
				retailnet.loader.hide();
			});
			
		} else {
			retailnet.notification.error('Recipient e-mail is required.');
		}
	});

});