
$(document).ready(function() {

	$('#btnIdea').fancybox({
		width		: 800,
		height		: 620,
		autoSize	: false,
		closeClick	: false,
		closeBtn	: false,
		margin		: 0,
		padding		: 0,
		iframe		: {
			scrolling : 'auto',
			preload   : true
		}
	});
});