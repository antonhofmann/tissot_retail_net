$(document).ready(function() {
	
	var form = $('#sapForm');
	var currentDistChannel = $('#posaddress_distribution_channel').val();

	
	$('.transfer').click(function(e) {

		var self = $(this);
		var source = self.prev();
		var transfer = self.next();
		var value = source.val();
		
		if (value && !self.hasClass('disabled')) {
			
			transfer.addClass('success');
			
			if (self.hasClass('letter-control')) {
				transfer.val(value.toLowerCase().replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
			} else {
				transfer.val(source.val());
			}	
		}
		
		self.addClass('disabled');
	});
	
	$('#update').click(function(e) {
		
		e.preventDefault();

		retailnet.notification.hide();

		$('#posaddress_id').removeClass('error');

		if (!$('#id').val()) {
			$('#posaddress_id').addClass('error');
			retailnet.notification.error('Select POS location');
			return false;
		}

		$('.input-mask-container').each(function(i,e) {

			var container = $(e);

			// check phone numbers
			if (container.hasClass('phone-container') && Validator.hasInputMaskError(container)) {
				retailnet.notification.error('Please check red marked fields.');
				return false;
			}
		})
		
		retailnet.ajax.json(form.attr('action'), form.serialize()).done(function(xhr) {
			
			if (xhr && xhr.response && xhr.redirect) { 
				window.location=xhr.redirect;
			}

			if (xhr && xhr.message) {
				if (xhr.response) retailnet.notification.success(xhr.message);
				else retailnet.notification.error(xhr.message);
			}
		});
		
		return false;
	});
	
	$('#ignore').click(function(e) {
		
		e.preventDefault();
		
		var import_id = $('#import_id').val();
		
		if (import_id) {
			retailnet.ajax.json(form.attr('action'), {
				ignore: import_id
			}).done(function(xhr) {
				
				if (xhr && xhr.response && xhr.redirect) { 
					window.location=xhr.redirect;
				}
				
				if (xhr && xhr.message) {
					if (xhr.response) retailnet.notification.success(xhr.message);
					else retailnet.notification.error(xhr.message);
				}
			});
		}
		
		return false;
	});
	
	// add new place
	$('#posaddress_place_id').change(function(e) {
		
		var self = $(this);
		
		if (self.val()=='new') {
			retailnet.modal.dialog('#addNewPlace');
		}
	});
	
	// apply action
	$('#placeApply').click(function(e) {
		
		e.preventDefault();
		
		retailnet.notification.hide();
		
		var country = $('#import_country').val();
		var error = false;
		
		$('#provinces, #new_place').removeClass('error');
		
		if (!$('#provinces').val()) {
			error = true;
			$('#provinces').addClass('error');
			retailnet.notification.error('Place province is required.');
		}
		
		if (!$('#new_place').val()) {
			error = true;
			$('#new_place').addClass('error');
			retailnet.notification.error('New place name is required');
		}
		
		if (!error && country) {
			
			// check place name
			retailnet.ajax.json('/applications/helpers/place.ajax.php', {
				section: 'get_from_name',
				province: $('#provinces').val(),
				name: $('#new_place').val()
			}).done(function(xhr) {
				
				if (xhr && xhr.place_id) {
					$('#new_place').addClass('error');
					retailnet.notification.error('This place name already exist.');
				} else {
					
					retailnet.ajax.json('/applications/helpers/place.ajax.php', {
						section: 'add',
						country: country,
						province: $('#provinces').val(),
						name: $('#new_place').val()
					}).done(function(xhr) {
						
						if (xhr && xhr.place_id) {
							
							$('#posaddress_place_id').append($("<option></option>").attr("value", xhr.place_id).text(xhr.place_name)); 
							$('#posaddress_place_id').val(xhr.place_id);
							retailnet.notification.success('New place is inserted.');
							
							$('#provinces').val('');
							$('#new_place').val('')
							retailnet.modal.close();
							
						} else {
							retailnet.notification.error('New place is not inserted.');
						}
					});
				}
				
			});
		}
		
		return false;
	});
	
	// cancel action
	$('#placeCancel').click(function(e) {
		
		e.preventDefault();
		
		$('#posaddress_place_id').val($('#import_posaddress_place_id').attr('data-id')); 
		retailnet.modal.close();
	
		return false;
	});
	
	// change pos
	$('#posaddress_id').change(function(e) {
		
		var self = $(this);
		var val = self.val();
		var successClass = 'success';
		var dangerClass = 'danger';

		$('#posaddress_id').removeClass('error');
		
		// reset transfer fields
		$('.transfer').removeClass('disabled');
		$('input.field-transfer').val('').removeAttr('placeholder');
		$('select.field-transfer').val('');
		$('#id').val('');
		
		$('#posaddress_sapnumber').attr('placeholder', 'SAP Number');
		$('#posaddress_name').attr('placeholder', 'POS Name');
		$('#posaddress_name2').attr('placeholder', 'POS Name 2');
		$('#posaddress_address').attr('placeholder', 'Address');
		$('#posaddress_zip').attr('placeholder', 'ZIP');
		$('#posaddress_phone').attr('placeholder', 'Phone');

		$('#posaddress_phone_country').val('');
		$('#posaddress_phone_area').val('');
		$('#posaddress_phone_number').val('');		

		if (val) {
			
			retailnet.ajax.json('/applications/helpers/pos.ajax.php?section=pos&id='+val).done(function(xhr) {
				
				if (xhr && xhr.posaddress_id) {

					var source, sourceValue, target, targetValue;

					// pos id
					$('#id').val(xhr.posaddress_id);
					
					// sap number
					source = $('#sap_imported_posaddress_sapnr');
					target = $('#posaddress_sapnumber');
					targetValue = xhr.posaddress_sapnumber;
					sourceValue = source.val();

					if (targetValue) {
						
						if (target.is('input')) { 

							target.val(targetValue);
							target.attr('placeholder', targetValue);

							if (sourceValue) {
								source.addClass(sourceValue==targetValue ? successClass : dangerClass);
							} else {
								source.removeClass(successClass).removeClass(dangerClass);
							}

						} else {
							target.text(targetValue);
						}
					}
					
					// pos name
					source = $('#sap_imported_posaddress_name1');
					target = $('#posaddress_name');
					targetValue = xhr.posaddress_name;
					sourceValue = source.val();

					if (targetValue) {
						
						if (target.is('input')) { 

							target.val(targetValue);
							target.attr('placeholder', targetValue);

							if (sourceValue) {
								source.addClass(sourceValue==targetValue ? successClass : dangerClass);
							} else {
								source.removeClass(successClass).removeClass(dangerClass);
							}

						} else {
							target.text(targetValue);
						}
					}
					

					// pos name 2
					source = $('#sap_imported_posaddress_name2');
					target = $('#posaddress_name2');
					targetValue = xhr.posaddress_name2;
					sourceValue = source.val();

					if (targetValue) {
						
						if (target.is('input')) { 

							target.val(targetValue);
							target.attr('placeholder', targetValue);

							if (sourceValue) {
								source.addClass(sourceValue==targetValue ? successClass : dangerClass);
							} else {
								source.removeClass(successClass).removeClass(dangerClass);
							}

						} else  {
							target.text(targetValue);
						}
					}

					// pos address street
					target = $('#posaddress_street');
					targetValue = xhr.posaddress_street;

					if (targetValue) {
						if (target.is('input')) { 
							target.val(targetValue);
							target.attr('placeholder', targetValue);
						} else {
							target.text(targetValue);
						}
					}

					// pos address street number
					target = $('#posaddress_street_number');
					targetValue = xhr.posaddress_street_number;

					if (targetValue) {
						if (target.is('input')) { 
							target.val(targetValue);
							target.attr('placeholder', targetValue);
						} else {
							target.text(targetValue);
						}
					}

					// pos address 2
					source = $('#sap_imported_posaddress_address2');
					target = $('#posaddress_address2');
					targetValue = xhr.posaddress_address2;
					sourceValue = source.val();

					if (targetValue) {
						
						if (target.is('input')) { 

							target.val(targetValue);
							target.attr('placeholder', targetValue);

							if (sourceValue) {
								source.addClass(sourceValue==targetValue ? successClass : dangerClass);
							} else {
								source.removeClass(successClass).removeClass(dangerClass);
							}

						} else {
							target.text(targetValue);
						}
					}

					

					// pos zip 
					source = $('#sap_imported_posaddress_zip');
					target = $('#posaddress_zip');
					targetValue = xhr.posaddress_zip;
					sourceValue = source.val();

					if (targetValue) {
						
						if (target.is('input')) { 

							target.val(targetValue);
							target.attr('placeholder', targetValue);

							if (sourceValue) {
								source.addClass(sourceValue==targetValue ? successClass : dangerClass);
							} else {
								source.removeClass(successClass).removeClass(dangerClass);
							}

						} else {
							target.text(targetValue);
						}
					}

					
					// pos place
					source = $('#sap_imported_posaddress_city');
					target = $('#posaddress_place_id');
					targetValue = xhr.posaddress_place_id;
					sourceValue = source.val();

					if (targetValue) {
						
						if (target.is('select')) { 

							var selected = source.data('id')==targetValue ? targetValue : '';

							target.val(targetValue);

							if (sourceValue) {
								source.addClass(selected ? successClass : dangerClass);
							} else {
								source.removeClass(successClass).removeClass(dangerClass);
							}

						} else {
							target.text(xhr.posaddress_place);
						}
					}
					
					// pos phone
					source = $('#sap_imported_posaddress_phone');
					target = $('#posaddress_phone');
					targetValue = xhr.posaddress_phone;
					sourceValue = source.val();

					if (targetValue) {
						
						if (target.is('input')) { 

							target.val(targetValue);
							target.attr('placeholder', targetValue);

							if (sourceValue) {
								source.addClass(sourceValue==targetValue ? successClass : dangerClass);
							} else {
								source.removeClass(successClass).removeClass(dangerClass);
							}

						} else {
							target.text(targetValue);
						}
					}

										
					// pos distribution channel
					source = $('#sap_imported_posaddress_distribution_channel');
					target = $('#posaddress_distribution_channel');
					targetValue = xhr.posaddress_distribution_channel;
					sourceValue = source.val();

					if (targetValue) {
						
						if (target.is('select')) { 

							target.val(targetValue);

							if (sourceValue) {
								source.addClass(sourceValue==targetValue ? successClass : dangerClass);
							} else {
								source.removeClass(successClass).removeClass(dangerClass);
							}

						} else if (targetValue) {
							retailnet.ajax.json('/applications/helpers/distribution.channel.ajax.php?section=get&id='+targetValue).done(function(xhr) {
								target.text(xhr.distribution_channel_name);
							});
						}
					}

					// pos store closing date
					source = $('#sap_imported_posaddress_deletion_indicator');
					target = $('#posaddress_store_closingdate');
					targetValue = xhr.posaddress_store_closingdate;
					sourceValue = source.val();

					if (targetValue) {
						
						if (target.is('input')) { 

							target.val(targetValue);
							target.attr('placeholder', targetValue);

							if (sourceValue) {
								source.addClass(sourceValue==targetValue ? successClass : dangerClass);
							} else {
								source.removeClass(successClass).removeClass(dangerClass);
							}

						} else if (targetValue != '0000-00-00') {
							target.text(targetValue);
						}
					}
				}
				
			});
		}
	})

	// phone number mask
	$('.phone-mask').on('change', function(e) {
		Validator.checkPhoneNumber($(this));
	})

	$('[name="posaddress_phone_country"]').on('focus', function() {

		var self = $(this),
			posID = Number($('#posaddress_id').val());

		if (self.val() || !posID) return;

		// get pos
		$.ajax({
			url: '/applications/helpers/ajax.pos.php',
			dataType: 'json',
			data: {
				section: 'get',
				value: posID
			},
			success: function(xhr) { 
				if (xhr && xhr.posaddress_country) {
					$.ajax({
						url: '/applications/helpers/ajax.country.php',
						dataType: 'json',
						data: {
							section: 'get_country_phone_prefix',
							value: xhr.posaddress_country
						},
						success: function(response) { 
							self.val(response.country_phone_prefix);
			        	}
					})
				}
        	}
		})
	})
	
});