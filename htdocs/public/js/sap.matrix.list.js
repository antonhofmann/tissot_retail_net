$(document).ready(function() {

	var $container = $("#logistics");
	var sources = retailnet.ajax.request('/applications/modules/sap/planning/logistic.load.php', $('form.request').serializeArray());

	var headers = sources.headers || [];
	var columns = sources.columns || [];
	var groups = sources.groups || [];
	var data = sources.data || [];
	
	window.client = sources.client;

	if (sources && sources.errors) {
		$.each(sources.errors, function(i, message) {
			retailnet.notification.error(message);
		})
	}

	$container.handsontable({
		data: data,
		/*width: 1200,*/
		startRows: 7,
		startCols: 4,
		colHeaders: headers,
		colWidths: [500,80,80,80,80,80,80,80,80,80,80],
		columnSorting: false,
		columns: columns,
		afterChange: function(changes, source) { 
			
			if (changes) {

				var row = changes[0][0];
				var col = changes[0][1];
				var val = changes[0][3];

				var col = col.split('.');
				var channel = handsontable.getDataAtRowProp(row, 'channel.id');
				var type = handsontable.getDataAtRowProp(row, col[0]+'.id');
				var section = handsontable.getDataAtRowProp(row, col[0]+'.section');

				retailnet.ajax.json('/applications/modules/sap/planning/logistic.submit.php', {
					section: section,
					client: window.client,
					channel: channel,
					type: type,
					value: val
				}).done(function(xhr) {

					if (xhr && xhr.reload) { 
						window.location.reload();
					}

					if (xhr && xhr.notifications) {
						$.each(xhr.notifications, function(i, message) {
							retailnet.notification.success(message);
						})
					}

					if (xhr && xhr.errors) {
						$.each(xhr.errors, function(i, message) {
							retailnet.notification.error(message);
						})
					}
				})
			}
		},
		afterRender: function (isForced) {
				
			$('.ht_clone_top thead').hide();

			var group = '';

			$('.header-grouping').remove();

			for (var i in headers) {
				if (i==0) {
					group += '<th class="empty-cel">&nbsp;</th>';
				} else if (groups[i]) { 
					group += '<th colspan="2">'+groups[i]+'</th>';
				}
			}
		
			$container.find('thead').find('tr').before('<tr class="header-grouping">'+group+'</tr>');

			// attach event
			$('#clients').val(window.client);
			
			var td = $('.htCore thead .colHeader:first');
			var offset = td.offset();
			
			$('#clients').css({
				position: 'absolute',
				top: offset.top-10,
				left: offset.left,
				zIndex: 99999999
			})
        },
        beforeRender: function() {
           $('.header-grouping').remove();
        }, 
        modifyColWidth: function (width, col) {
            $('.header-grouping').remove();
        }
	});

	var handsontable = $container.data('handsontable');


	$(document).on('change', '#clients', function(e) {
		updateMatrix();
	})

	var updateMatrix = function() {
		
		var client = $('#clients').val(); console.log(client);
		var url = "/applications/modules/sap/planning/logistic.load.php";
		var data = $('form.request').serializeArray();
		data.push({ name: 'client',  value: client });

		retailnet.ajax.json(url, data).done(function(xhr) {
			handsontable.loadData(xhr.data);
			window.client = client;
			$('#clients').val(client);
		})
	}

});