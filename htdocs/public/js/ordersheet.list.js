$(document).ready(function() {

	// spiner instance	
	retailnet.loader.init();
	
	var ordersheets = $('#ordersheets').tableLoader({
		url: '/applications/modules/ordersheet/list.php',
		data: $('.request').serializeArray(),
		after: function(self) {
			$('select', self).dropdown();
		}
	});

	$('#ordersheets').on("change", "#actions", function() {

		if ($("option:selected", this).hasClass('request_modal')) {

			var url = $(this).val();

			retailnet.loader.show();

			$.post(url, function(xhr) {

				retailnet.loader.hide();

				if (xhr && xhr.message) {
					
					if (xhr.response) retailnet.notification.success(xhr.message);
					else retailnet.notification.error(xhr.message);
				}

				if (xhr.response) {
					ordersheets.showPage(1);
				}
			});

			$('option:first',this).attr('selected', true);
			
		} else {
			window.location = $(this).val();
		}
	});
	
	$('#ordersheets').on("click", "#add", function(event) {

		event.preventDefault();
		
		if ($('input.companies').length > 1) {

			retailnet.modal.show('#modalbox', {
				autoSize	: false,
				closeClick	: false,
				closeBtn	: false,
				fitToView   : true,
			    width       : '90%',
			    height      : '90%',
			    maxWidth	: '720px',
			    maxHeight	: '720px',
				modal 		: true,
				afterLoad: function() {
					$('#standard_ordersheet input').attr('checked', true)
				}
			});
			
		} else {
			$('#standard_ordersheet').submit();
		}
	});

	$('#standard_ordersheet').submit(function(event) {

		event.preventDefault();
		
		retailnet.notification.hide();

		if ($('input.companies:checked').length > 0) {

			retailnet.loader.show();
			
			$.ajax({
				type: 'post',
				url: $(this).attr('action'),
				dataType: 'json',
				data: $(this).serialize(),
				success: function(xhr) {

					retailnet.loader.hide(); 

					if (xhr && xhr.response) {
						if (xhr.redirect) {
							window.location = xhr.redirect;
						} else {
							retailnet.modal.close();
						}
					}

					if (xhr && xhr.message) {
						if (xhr.response) retailnet.notification.success(xhr.message);
						else retailnet.notification.error(xhr.message);
					}
				}
			});
			
		} else {
			retailnet.notification.error('Please, select at least one company.');
		}
		
	});

	$('#apply').click(function(event) { 
		event.preventDefault();
		$('#standard_ordersheet').submit();
		return false;
	});	

	$('#cancel').bind('click', function(event) {
		event.preventDefault();
		retailnet.modal.close();
		return false;
	});	

	$('#select_all').change(function() {
		var self = $(this);
		var caption = self.next('.caption');
		var label = caption.attr('data');
		var checked = (self.is(':checked')) ? true  : false;
		$('input.companies').attr('checked', checked);
		caption.attr('data', caption.text()).text(label);
	});

	$('.checkall .caption').click(function() {
		$('#select_all').trigger('click');
	});

	$('input.companies').click(function() {
		var checked = ($('input.companies').length == $('input.companies:checked').length) ? true : false;
		$('#select_all').attr('checked', checked);
	});	
});