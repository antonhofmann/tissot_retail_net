$(document).ready(function() {

	var launchplanForm = $('#launchplanForm'),
		sendmailTemplate = $('#sendmail-template'),
		previewMailTemplate = $('#preview-mail-template'),
		testMailTemplate = $('#test-mail-template'),
		workflowState = $('#workflowStates'),
		launchplans = $('#launchplans'),
		sendmail = $('#sendmail'),
		editMailContent = $('#mail_template_view_modal');

	// loader instance 
	retailnet.loader.init();
	
// loaders :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$('#list').tableLoader({
		url: '/applications/modules/launchplan/submit/list.php',
		filters: launchplanForm,
		after: function(self) {

			// dropdown builders
			$('select',self).dropdown();

			// reset launchplans
			$('select',self).change(function() {
				launchplans.val('')
			});

			// order sheet box
			$('input.launchplan').click(function() {
				processCheckBoxes();
			});

			// check all order sheets
			$('.checkall').click(function() {
				var checkbox = $('input.launchplan');
				var checked = ($(this).is(':checked')) ? 'checked' : false;
				checkbox.attr('checked', checked);
				processCheckBoxes();
			});
		}
	});

// wysiwyg :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	tinymce.init({
		selector: "#mail_template_text",
		plugins: ["advlist autolink lists textcolor link visualblocks code nonbreaking contextmenu paste preview"],
		width: '100%',
		height: 400,
		menubar : false,
		statusbar : false,
		toolbar_items_size: 'small',
		autosave_restore_when_empty: false,
		toolbar: "fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor | removeformat | paste pastetext pasteword",
		setup : function(ed) {
			ed.on('change', function(e) {
				$('#mail_template_text').val(ed.getContent())
			});
		}
	});	
	
// submitting ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$('.submit-list').click(function(e) {

		e.preventDefault();

		var self = $(this);
		var sendmailTrigger = self.hasClass('sendmail') ? 1 : 0;
		
		// sendmail trigger
		sendmail.val(sendmailTrigger);
		
		if (launchplans.val()) {

			// edit mail content befor sent
			if (sendmail.val()==1 && editMailContent.val()==1) modalMailTemplate.resize().show();
			else launchplanForm.submit();

		} else retailnet.notification.error('Please, select at least one launch plan.');
		
	});	

	launchplanForm.submit(function(e) {
		
		e.preventDefault();
		
		var self = $(this);
		var url = self.attr('action');
		var data = self.serialize();
		
		if (launchplans.val()) {
			
			retailnet.loader.show();
			
			retailnet.ajax.json(url, data).done(function(xhr) {
				
				var submitLaunchplan = xhr || {};
				var reloadPage = false;

				// sshow submit notifications
				if (submitLaunchplan.notification) {
					if (submitLaunchplan.response) retailnet.notification.success(submitLaunchplan.notification);
					else retailnet.notification.error(submitLaunchplan.notification);
				}

				// required to send mails
				if (submitLaunchplan.response && submitLaunchplan.sendmail==1) {
					
					var sendMailRespone = sendMails() || {};

					if (sendMailRespone.response) retailnet.notification.success(sendMailRespone.notification);
					else retailnet.notification.error(sendMailRespone.notification);

					reloadPage = sendMailRespone.response ? submitLaunchplan.reload : false;
					
				} else {
					reloadPage = submitLaunchplan.reload;
				}

				// page reloader / form reset
				if (reloadPage) {
					window.location.reload();
				} else {
					$('table input').attr('checked', false);
					launchplans.val('');
					sendmail.val('');
				}
				
			}).complete(function() {
				retailnet.loader.hide();
			});	
			
		} else {
			retailnet.notification.error('Please, select at least one launch plan.');
		}

	});

// sendmail ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var modalMailTemplate = sendmailTemplate.adoModal({
		width: 800,
		height: 700,
		clickClose: false
	});

	// button: send mail
	$('.sendmail', sendmailTemplate).on('click', function(e) {

		e.preventDefault();
		
		var self = $(this),
			required = $('.required', sendmailTemplate),
			failures = $('.required[value=]', sendmailTemplate);

    	if (required.length > 0 && failures.length > 0) {
    		$('.required', sendmailTemplate).removeClass('error');
    		$('.required[value=]', sendmailTemplate).addClass('error');
    		retailnet.notification.error('Red marked fields are required.');
    	} else {
    		modalMailTemplate.close();
			launchplanForm.submit(); 
    	}
	});
	
	var sendMails = function() {

		var self = $(this);
		var url = $('form', sendmailTemplate).attr('action');
		var data = $('#sendmail-template form, #launchplanForm').serialize();

		if (launchplans.val()) {
			
			retailnet.loader.show();
			
			var xhr = retailnet.ajax.request(url, data);

			if (xhr) retailnet.loader.hide();

			return xhr;

		} else {
			retailnet.notification.error('Please, select at least one launch plan.');
		}
	}

// preview mail ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var modalMailPreview = previewMailTemplate.adoModal({
		width: 1000,
		height: 600,
		overlay: {
			opacity: 0.75,
			background: '#000'
		}
	});

	$('.preview-mail', sendmailTemplate).on('click', function(e) {

		e.preventDefault();
		
		var self = $(this);
		var url = self.attr('href');
		var data = $('#sendmail-template form, #launchplanForm').serialize();
		
		if (launchplans.val()) {
			
			retailnet.loader.show();
			
			retailnet.ajax.json(url,data).done(function(xhr) {

				retailnet.loader.hide();

				if (xhr && xhr.response) {

					// assign data from request
					$('.mail-subject', previewMailTemplate).html(xhr.subject);
					$('.mail-address', previewMailTemplate).text(xhr.email);
					$('.mail-cc-address', previewMailTemplate).text(xhr.cc);
					$('.mail-cc-address', previewMailTemplate).parent().toggle(xhr.cc ? true : false);
					$('.mail-date', previewMailTemplate).text(xhr.date);
					$('.mail-content', previewMailTemplate).html(xhr.content);
					$('.mail-footer', previewMailTemplate).html(xhr.footer);
					$('.mail-footer', previewMailTemplate).parent().toggle(xhr.footer ? true : false);

					modalMailPreview.resize().show();

				} else {
					modalMailPreview.close();
				}

				if (xhr && xhr.notification) {
					if (xhr.response) retailnet.notification.success(xhr.notification);
					else retailnet.notification.error(xhr.notification);
				}
			});
			
		} else {
			retailnet.notification.error('Please, select at least one launch plan.');
		}

	});

// test mail :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var modalTestMail = testMailTemplate.adoModal({
		width: 400,
		overlay: {
			opacity: 0.75,
			background: '#0000ff'
		}
	});

	$('.test-mail', sendmailTemplate).on('click', function(e) {
		e.preventDefault();
		modalTestMail.show();
	});

	$('.apply', testMailTemplate).on('click', function(e) {
		
		e.preventDefault();

		var url = $(this).attr('href');
		var data = $('#sendmail-template form, #launchplanForm').serializeArray();
		var recipient = $('#test-mail-recipient').val();
		
		if (recipient) {

			data.push({
				name: 'test_recipient', 
				value: recipient
			});
			
			retailnet.loader.show();
			
			retailnet.ajax.json(url,data).done(function(xhr) {

				if (xhr && xhr.response) {
					modalTestMail.close();
				}

				if (xhr && xhr.notification) {
					if (xhr.response) retailnet.notification.success(xhr.notification);
					else retailnet.notification.error(xhr.notification);
				}

			}).complete(function() {
				retailnet.loader.hide();
			});
			
		} else {
			retailnet.notification.error('Recipient e-mail is required.');
		}
	});


// processing ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	// store seleted launchplans
	var processCheckBoxes = function() {

		var checkbox = $('input.launchplan'),
			data = launchplans.val().split(',') || [],
			value, index, checked; 
		
		$.each(checkbox, function() {
			value = $(this).attr('value');
			index = $.inArray(value, data);
			checked = $(this).is(':checked');
			if (checked) data.push(value);
			else if (index>-1) data.splice(index, 1);
		});
		
		data = $.grep(data, function(n) { 
			return (n); 
		});
		
		data = $.grep(data, function(v,k){
		    return $.inArray(v,data) === k;
		});
		
		// reset selected
		launchplans.val('');
		
		if (data.length > 0) {
			launchplans.val(data.join(','));
		}

		checked = (checkbox.length == $('input.launchplan:checked').length) ? 'checked' : false;
		$('.checkall').attr('checked', checked);
	}
	
});