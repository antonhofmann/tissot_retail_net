$(document).ready(function() {
	
	// loader initialise
	parent.retailnet.loader.init();
	
	$('.fileuploader').each(function() {
	    
		var self = $(this);
		var extensions = $('input[name=extensions]', self).val();
		var params = $('.params', self).val();
		var form = $('form', self);
		
		var options = self.data();
	
		
		// file type extensions
		if (extensions) {
			var exts = extensions.split(/[\s,]+/);
			options.acceptFileTypes = new RegExp('(\.|\/)('+exts.join('|')+')$');
		}

		// upload instance
		self.fileupload(options);		
		
		self.bind('fileuploaddestroy', function (e, data) {
			
			if (data && data.reload) {
				parent.window.location.reload();
			}
			
			if (data && data.notification) {
				parent.retailnet.notification.show(data.notification.content, eval(data.notification.properties));
			}
		});
		
		// Enable iframe cross-domain access via redirect option:
		self.fileupload('option', 'redirect', window.location.href.replace(/\/[^\/]*$/, '/public/scripts/fileuploader/cors/result.html?%s'));
		
		// get files on start
		if ($('input[name=startload]', self).val()) {
			
			self.addClass('fileupload-processing');
		    
			var data = form.serializeArray();
			data.push({name:'section', value:'load'});
			
			$.ajax({
		    	url: form.attr('action'),
		    	dataType: 'json',
		    	context: form[0],
		    	data: data
		    }).always(function () {
		    	self.removeClass('fileupload-processing');
		    }).done(function (result) {
		    	
				if (result) {
		    		self.fileupload('option', 'done').call(self, $.Event('done'), {result: result} );        
		    	}
			
				$('select').each(function(i,e) {
					$(this).val($(this).data('value'));
				})
				
		    });
		}
		
		// close modal screen
		$('.modal-close', self).on('click', function(e) {
			
			e.preventDefault();
			e.stopImmediatePropagation();
			
			parent.retailnet.modal.close();
			parent.window.location.reload();
			
			return false;
		});
		
		// start upload files
		$('button.uploader', self).click(function(e) {
			
			e.preventDefault();
			e.stopImmediatePropagation();
			
			parent.retailnet.notification.hide();
	    	
	    	var required = $('.required', form);
	    	var failures = $('.required[value=]', form);
	    	
	    	$('.required', form).removeClass('has-error');
	   
	    	
	    	if (required.length > 0 && failures.length > 0) {
	    		
	    		$('.required[value=]', form).addClass('has-error');
	    		
	    		parent.retailnet.notification.error('Red marked fields are required.');
	    		
	    	} else {	
	    		
	    		var data = form.serializeArray();
				data.push({name: 'section', value: 'save'});
				
				parent.retailnet.loader.show();
	    		
	    		retailnet.ajax.json(form.attr('action'), data).done(function(xhr) {
	    			
		    		parent.retailnet.loader.hide();
		    		
		    		if (xhr && xhr.reload) {
						parent.window.location.reload();
		        	}
		    		
					if (xhr && xhr.notification) {
						parent.retailnet.notification.show(xhr.notification.content, eval(xhr.notification.properties));
					}
		        });
	    	}

			return false;
		});
		
		
		// on enter
		self.on('keypress', 'input.file-title', function(e, data) {
	        
			if (e.which === 13) {
	            
	        	e.preventDefault();
	            e.stopPropagation();
	            e.stopImmediatePropagation()
	            
				var textboxes = $("input.file-title", self);
				var currentBoxNumber = textboxes.index(this);
				
				if (textboxes[currentBoxNumber+1] != null) {
					var nextBox = textboxes[currentBoxNumber+1];
					nextBox.focus();
					nextBox.select();
				}

	            return false;
	        }
	    });
		
	});
});