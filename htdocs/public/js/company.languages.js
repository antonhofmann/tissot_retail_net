$(document).ready(function() {

	var table = $('#company_languages').tableLoader({
		url: '/applications/helpers/company.language.list.php',
		data: $('.request').serializeArray()
	});

	$(document).delegate("input.language", "change", function() {

		var id = $('#id').val();
		var language = $(this).val();
		var checked = ($(this).is(':checked')) ? 1 : 0;

		if (!checked) {
			$('.checkall').removeAttr('checked');
		} else {
			var data = [];
			var checkbox = $('input.language');
			var count = 0;
			$.each(checkbox, function(e) {
				if ($(this).is(':checked')) data.push(1);
				count++;
			});
			if (count == data.length ) {
				$('.checkall').attr('checked', 'checked')
			}
		}
		
		var request = $.getJSON('/applications/helpers/company.language.save.php', { 
			id: id,
			language: language,
			checked: checked
		});

		request.success(function(json) {
			if (json.message) {
				if (json.response) retailnet.notification.success(json.message);
				else retailnet.notification.error(json.message);
			}
		});
	});

	$(document).delegate(".checkall", "click", function() {
		
		var checkbox = $('input.language');
		var checked = ($(this).is(':checked')) ? 'checked' : false;

		$.each(checkbox, function(e) {

			if (checked && !$(this).is(':checked')) {
				$(this).attr('checked', 'checked').trigger('change')
			}
			
			if (!checked && $(this).is(':checked')) {
				$(this).attr('checked', false).trigger('change')
			}
		});
	});

});