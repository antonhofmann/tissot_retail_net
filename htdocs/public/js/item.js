$(document).ready(function() {

	var jqv = jQuery("form.validator").data('jqv');
	
	var valide = true;
	var history = {};

	// check item code is unique
	$('#item_code').on('change', function() {

		var item = {};
		item.id = $('#item_id').val();
		item.code = $(this).val();
		item.section = 'code.unique';

		$.getJSON('/applications/helpers/item.ajax.php', item, function(xhr) {
			valide = xhr.success;
			$('#item_code').toggleClass('error', !valide);
        })

	});

	// validate before submit
	jqv.onBeforeAjaxFormValidation = function(form, options) {
		
		if (!valide) {
			retailnet.notification.error('Item Code must be unique.');
			return false;
		}
	}

    // 
    if ($('#item_category').length && $('#item_subcategory').length) {
        
        $('#item_category').chainedSelect('#item_subcategory', {
            url: "/applications/modules/item/category/ajax.php",
            parameters: {
                section: "get.subcategories"
            }
        }).trigger('change');
    }

	$('form.validator').on('afterSuccessSubmit', function(e, xhr) { 

		// clean up dynamic list
		if (xhr && xhr.remove) {
			removeEmptyRows(xhr.remove);
		}
			
		if ($('table.package-informations-list').length) {
			var table = $('table.package-informations-list');
			var data = $('input, textarea, select', table).serializeArray();
			$.post('/applications/helpers/item.logistic.php', data, function(xhr) {

				// clean up dynamic list
				if (xhr && xhr.remove) {
					removeEmptyRows(xhr.remove);
				}

			}, 'json');
		}

		return;
	})

	// validate textboxes onchange
	$(document).on('change', '.form-list input:text', function() {

		var self = $(this);
		var val = self.val();
		var type = self.data('type');
		var row = self.closest('tr');
		var key = self.prop('name')+row.data('id');

		retailnet.notification.hide();
		self.removeClass('error');
		
		switch (type) {

			case 'integer':

				var n = Number(val);
				
				if (isNaN(n) || n%1!=0) {
					retailnet.notification.show('This field should contain only number.', {theme: 'error'});
					self.val(history[key]);
					self.addClass('error');
					$('.btn-list-action', row).hide();
					return;
				}

				break;

			case 'decimal':

				var n = Number(val);
				
				if (isNaN(n)) {
					retailnet.notification.show('This field should contain only number or decimal number.', {theme: 'error'});
					self.val(history[self.prop('name')]);
					self.addClass('error');
					$('.btn-list-action', row).hide();
					return;
				}
		}

		if (val) {
			$('.btn-list-action', row).show();
		}
	
		history[key] = val;
	})
    
    // toggle new row button
    $(document).on('keyup change', '.form-list input, .form-list textarea, .form-list select', function() {
    	
    	var row = $(this).closest('tr');
    	var inputs = $('input:text, textarea, select', row);
    	
    	var inputsEmpty = $('input:text, textarea, select', row).filter(function() { 
    		return this.value == ""; 
    	});
    	
    	$('.btn-add', row).toggle(inputsEmpty.length != inputs.length ? true : false);
    })

    $(document).on('click', '.btn-add', function() {

    	var self = $(this);
    	var row = self.closest('tr');

    	if (!row.is(':last-child')) {
    		return
    	}

    	row.removeClass('newrow');

    	var newRow = row.clone(true)
    	newRow.addClass('newrow');
    	$('input:text, textarea, select', newRow).val('');
    	$('.btn-add', newRow).hide();

    	// reset current row
    	self.toggleClass('btn-add').toggleClass('btn-remove');
    	$('.fa', self).toggleClass('fa-minus-square').toggleClass('fa-plus-square');
    	
    	newRow.insertAfter(row);
    })

    $(document).on('click', '.btn-remove', function() {
    	var self = $(this);
    	var row = self.closest('tr');
    	row.remove();
    })

    // OLD calculate cbm
    $(document).on('change', '#item_width, #item_height, #item_length', function(e) {
    	$('#cbm').trigger('change');
    });

    // OLD calculate cbm 
    $(document).on('change', '#cbm', function(e) { 
    	
    	var self = $(this); 
    	var itemLength = $('#item_width').val();
    	var itemWidth = $('#item_height').val();
    	var itemHeight = $('#item_length').val();
    	
    	var total = itemLength>0 && itemWidth>0 && itemHeight>0 ? (itemLength*itemWidth*itemHeight/1000000).toFixed(4) : '';
    	self.text(total);
    })

    // form list calculate cbm
    $(document).on('change', '.item-length input, .item-width input, .item-height input', function(e) {
    	
    	var self = $(this);
    	var row = self.closest('tr');
    	var itemLength = $('.item-length input', row).val();
    	var itemWidth = $('.item-width input', row).val();
    	var itemHeight = $('.item-height input', row).val();
    	
    	var total = itemLength>0 && itemWidth>0 && itemHeight>0 ? (itemLength*itemWidth*itemHeight/1000000).toFixed(4) : '';
    	$('.item-cbm', row).text(total).trigger('changed');
    })

    // calculate weight net/gross
    $(document).on('change', '.item-weight-net input, .item-weight-gross input', function(e) {
    	
    	var self = $(this);
    	var row = self.closest('tr');
    	var table = row.closest('.package-informations-list');
    	var index = self.parent().index();
    	var sum = 0;

    	$('tbody tr', table).each(function(i,e) {
    		var td = $('td', $(e)).get(index);
    		var val = $('input:text', td).val();
    		sum += val ? Number(val) : 0;
    	})

    	sum = sum ? sum.toFixed(2) : '';

    	var cel = $('tfoot tr:first-child td', table).get(index);
    	$(cel).text(sum);
    })

    $(document).on('changed', '.item-cbm', function() {
    	
    	var sum = 0;

    	$('.item-cbm').each(function(i,e) {
    		var v = $(e).text();
    		sum += v ? Number(v) : 0;
    	})

    	sum = sum ? sum.toFixed(4) : '';
    	$('.total-cbm').text(sum);
    })

    // onload
    $('#cbm').trigger('change');

    $('table.package-informations-list select').each(function() {
    	var self = $(this);
    	self.val(self.data('value'))
    });

    var removeEmptyRows = function(data) {

    	for (var i in data) {
				
			var el = $(i), keys = data[i], rows = [];
			
			for (var key in keys) {
				var row = $('tbody > tr:not(.newrow)', el).eq(keys[key]);
				rows.push(row);
			}

			for (var i in rows) {
				var row = rows[i];
				row.remove();
			}
		}
    }
});