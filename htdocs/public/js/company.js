function closeAll() {
		$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
		retailnet.loader.hide();
	}

$(document).ready(function() {

	var form = $("#companyform"),
		customerNumber = $('#address_mps_customernumber'),
		shiptoNUmber = $('#address_mps_shipto'),
		independentRetailer = $('#address_is_independent_retailer'),
		involvedInPlanning = $('#address_involved_in_planning'),
		involvedInPlanning_ff = $('#address_involved_in_planning_ff'),
		createPos = $('#create_pos_from_company'),
		country = $('#address_country'),
		province = $('#province'),
		newProvince = $('#new_province'),
		place = $('#address_place_id'),
		newPlace = $('#new_place'),
		geodataBox = $('.geobox'),
		geodata = $('.geodata');
	
	retailnet.loader.init();

	// close geobox onload
	geodataBox.hide();
	
	// tooltips
	$('.-tooltip').qtip({style: {classes:'qtip qtip-dark' }});
 
		
	// form validator
	form.validationEngine({
		autoHidePrompt: true,
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		onAjaxFormComplete: function(status, form, json, options) {

			retailnet.loader.hide();
			retailnet.notification.hide();

			if (json) {

				if (json.redirect) {
					window.location=json.redirect;
				}

				if (json.message) {
					if (json.response) retailnet.notification.success(json.message);
					else retailnet.notification.error(json.message);
				}
			}
		}
	});

	
	// submit form
	$("#save").click(function(e) {

		e.preventDefault();
		
		retailnet.notification.hide();
		province.removeClass('error');
	
		var elements = $('input:visible,textarea:visible,select:visible');

		if (province.is(':visible') && !province.val()) {
			province.addClass('error');
		}

		geodata.trigger('blur');

		var validate = form.validationEngine('validate');
		var hasErrors = form.find('.error').length > 0 ? true : false;

		$('.input-mask-container').each(function(i,e) {

			var container = $(e);

			// check phone numbers
			if (Validator.hasInputMaskError(container)) {
				hasErrors = true;
			}
		})

		if (hasErrors || !validate) {
			retailnet.notification.error('Please check red marked fields.');
		}
		else if($('#address_phone_number').val() == '' && $('#address_mobile_phone_number').val() == '') {
			retailnet.notification.error('Please indicate the phone number or the mobile phone number!');
			
		}
		else {
			retailnet.loader.show();
			form.submit();
		}
	});

	
	// check company address for letter cases
	$('#address_address').change(function() {

		var self = $(this);

		var data = {
			section : 'letter_cases',
			value : self.val(),
			application: $('#application').val()
		}

		$.getJSON('/applications/helpers/company.ajax.php', data, function(json) {

			retailnet.notification.hide();
			
			if (json) {
				
				self.toggleClass('error', json.error);
				
				if (json.message) {
					retailnet.notification.error(json.message);
				}
			}
        });
	});

	// chainde counries
	country.chainedSelect('#address_place_id', {
		parameters: {
      		section: "places"
      	}
	});

	//textbox for new options
	country.change(function(e) { 

		geodataBox.hide();
		newProvince.hide();
  		province.empty();

  		var value = Number(country.val());

  		if (!value) return;
	
		$.ajax({
			url: '/applications/helpers/ajax.dropdown.php',
			dataType: 'json',
			data: {
				section: 'provinces',
				value: value
			},
			success: function(json) { 
				if (json) {
					for (i = 0; i < json.length; i++) {  
						for ( key in json[i] ) {	
							province.append("<option value="+key+">"+json[i][key]+"</option>");
						}
					}
					province.val($('#address_province_id').val());
				}
        	}
		});


		$.ajax({
			url: '/applications/helpers/company.ajax.php',
			dataType: 'json',
			data: {
				section: 'get_currency_from_country',
				value: value
			},
			success: function(json) { 
				$('#address_currency').val(json.currency);
        	}
		})
	})

	// trigger country onload
	if (country.is('select')) {
		country.trigger('change');
	}

	
	// textbox for new options
	province.change(function() {
		
		newProvince
			.toggle(country.val() && $(this).val()=="new")
			.removeClass('error')
			.val('');
		
		newPlace.val('')
			.removeClass('error')
			.val('');

		if ($(this).val() == 'new') newProvince.focus();
		else newPlace.focus();
	});
	
	// textbox for new options
	place.change(function() {
		if (country.val()) {
			geodataBox.toggle($(this).val()=="new");
		}
	});

	// check new geodata fields
	geodata.bind({
		blur: function() {

			retailnet.notification.hide();
			
			$(this).removeClass('error');
			
			if ($(this).is(':visible') && $(this).hasClass('required') && !$(this).val()) {

				$(this).addClass('error');
				
				if ($(this).attr('alt')) {
					$.jGrowl($(this).attr('alt')+' is required.', {sticky: false, theme:'error'});
				}
			}
		},
		change: function() {

			retailnet.notification.hide();

			var self = $(this);

			$(this).removeClass('error');

			if ($(this).is(':visible')) {

				var data = {
					section: self.attr('id'),
					country: Number(country.val()),
					province: Number(province.val()),
					place: Number(place.val()),
					new_province: newProvince.val(),
					new_place: newPlace.val(),
					value: self.val()
				}
			
				if (self.val()) {
					$.getJSON('/applications/helpers/ajax.dropdown.php', data, function(json) {
						if (json) {
							self.toggleClass('error', json.error);
							if (json.message) retailnet.notification.error(json.message);
						}
			        });
				} 
			}
		}
	});


	// if company is independent retailr
	// show oportunity to create pos from company data
	independentRetailer.click(function() {

		retailnet.notification.hide();
		
		var checked = $('input.dependent_from_independent_retailer:checked'); 
		var error = false;

		if ($(this).is(':checked')) {
			error = (checked.length > 0) ? true : false;
		}

		if (error) {
			retailnet.notification.error('An independent retailer cannot order merchandising material.');
		}

		$(this).toggleClass('error', error);
		$(this).next().toggleClass('error', error);

		// toggle cretae new pos box
		createPos.trigger('change');
	});


	// create POS location from company data 
	createPos.change(function() {

		retailnet.notification.hide();

		if (independentRetailer.is(':visible')) {
			var checked = independentRetailer.is(':checked') ? true : false;
		} else {
			var checked = true;
		}

		var error = false;

		if ($(this).is(':checked') && !checked) {
			error = true;
			retailnet.notification.error('Please set this Company as Independent Retailer');
		}

		$(this).toggleClass('error', error).next().toggleClass('error', error);
		
	});
	


	// check fields dependet from independent retailers
	// in company is independent retailer
	// then this field can not be null
	$('.required_for_independet_retailer').change(function() {

		retailnet.notification.hide();

		var error = false;
		var value = $(this).val();
		var label = $(this).attr('alt');

		if (!value) {
			error = true;
			retailnet.notification.error('Please indicate the '+label);
		}

		$(this).toggleClass('error', error);
	});


	$('.dependent_from_independent_retailer').bind('change', function() {

		retailnet.notification.hide();

		var error = false;
		var checked = ($(this).is(':checked')) ? true : false;
		var dependet_checked = $('input.dependent_from_independent_retailer:checked'); 
		var error = false;

		if (checked) {
			$('.required_for_independet_retailer').trigger('change');
		}

		if (checked && independentRetailer.is(':checked')) {
			error = true;
			independentRetailer.addClass('error').next().addClass('error');
		}	

		if (error) {
			retailnet.notification.error('An independent retailer cannot order merchandising material.');
		}

		if (dependet_checked.length == 0) {
			independentRetailer.removeClass('error').next().removeClass('error');
			$('.required_for_independet_retailer').removeClass('error');
		}		
	});


	$(".letter-control").click(function() {

		var field = $('input[type=text]:first', $(this).prev())
		var value = field.val();

		if (value) {
			field.val(value.toLowerCase().replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
			field.removeClass('error');
		}
	});

	// address and street mask
	$('.address-mask').on('change', function(e) {

		var $this = $(this),
			container = $this.closest('.input-mask-container'),
			value = $(this).val();

		$this.removeClass('error');
		retailnet.notification.hide();

		if (!$('[name="address_street"]').val()) {
			$('.input-mask', container).val('');
			return;
		}
	})

	// phone number mask
	$('.phone-mask').on('change', function(e) {
		Validator.checkPhoneNumber($(this));
	})

	$('[name="address_phone_country"]').on('focus', function() {

		var self = $(this),
			countryID = Number(country.val());

		if (self.val() || !countryID) return;

		$.ajax({
			url: '/applications/helpers/ajax.country.php',
			dataType: 'json',
			data: {
				section: 'get_country_phone_prefix',
				value: countryID
			},
			success: function(xhr) { 
				self.val(xhr.country_phone_prefix);
        	}
		})
	})

});