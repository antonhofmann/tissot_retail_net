

$(document).ready(function() {

	var $articles = $('#articles'),
		$articleTemplate = $("#article-template").html(),
		onPageLoad = true,
		Filters = {}

	$('body').tooltip({
		selector: '[data-toggle="tooltip"]'
	})

	// filters :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	/*
	$('.list-filters').stickyParent({
		bottoming: false,
		offset_top: $('.navbar-fixed-top').outerHeight()
	})
	*/

	$('.filter-toggle').on('click', function() {
		$('i', $(this)).toggleClass('fa-caret-down').toggleClass('fa-caret-up');
		$('.list-filters').toggleClass('active');
	})


	$('button.sort').on('click', function(e) {

		e.preventDefault();
		e.stopPropagation();

		var $this = $(this);
		var $group = $this.closest('.filter-group')
		
		var sortBy = $this.data('filter');
		var sortAscending = $this.attr('data-ascending') ? $this.data('ascending') : true;

  		$articles.isotope({ 
  			sortBy: sortBy,
  			sortAscending: sortAscending
  		})

  		if ($this.attr('data-ascending')) {
  			$this.data('ascending', !sortAscending)
  		}

  		$('.fa', $this).toggleClass('fa-caret-down').toggleClass('fa-caret-up')

		$('.bnt-group-responsive-caption label', $group).text($this.text());
		$this.closest('.bnt-group-responsive').removeClass('group-open');
		
		return false;
	})

	$('button.filter').on('click', function(e) {

		e.preventDefault();
		e.stopPropagation();

		var $this = $(this);
		var $group = $this.parents('.filter-group');
		var filterGroup = $group.data('filter-group');

		Filters[filterGroup] = $this.attr('data-filter');

		var filterValue = '';
		
		for (var prop in Filters) { filterValue += Filters[prop] }
		$articles.isotope({ filter: filterValue });

		$('.bnt-group-responsive-caption label', $group).text($this.text());
		$this.closest('.bnt-group-responsive').removeClass('group-open');

		return false;
	})

	// view mode
	$('button.view').on('click', function(e) {

		e.preventDefault();
		e.stopPropagation();

		var viewMode = $(this).data('filter');
		$articles.removeAttr('class').addClass('row').addClass(viewMode);

		$(this).siblings().removeClass('active');
		$(this).addClass('active');
		$articles.isotope('layout');

		return false;
	})

	// use value of search field to filter
	var $quicksearch = $('#quicksearch').keyup( debounce( function(e, triggerState) {
		
		var qsRegex = new RegExp( $quicksearch.val(), 'gi' );
		var manuallyTriggered = triggerState ? triggerState : false;
		
		$articles.isotope({ filter: function() {
			return qsRegex ? $(this).text().match( qsRegex ) : true;
    	}})

		if (!manuallyTriggered) {
			saveFilters();
		}

	}, 300 ));

	function debounce( fn, threshold ) {
		
		var timeout;
		
		return function debounced() {
			
			if ( timeout ) {
				clearTimeout( timeout );
			}
			
			function delayed() {
				fn();
				timeout = null;
			}
			
			timeout = setTimeout( delayed, threshold || 100 );
		}
	}


	/* layout utilites :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

	// open responsive collapsed buttons
	$('.list-filters .bnt-group-responsive-caption').on('click', function() {
		$(this).next('.bnt-group-responsive').toggleClass('group-open');	
	})

	// on load, set responsive button caption
	$('.bnt-group-responsive-caption').each(function() {
		var $this= $(this);
		var $group = $this.parents('.filter-group');
		var $caption = $('button.active', $group).text() || 'All';
		$('label', $this).text($caption)
	})


	// filter controlls ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$('.list-filters button:not(.disabled, .bnt-group-responsive-caption)').on('click', function(e, triggerState) { 
		
		e.preventDefault();
		e.stopPropagation();

		var manuallyTriggered = triggerState ? triggerState : false;

		$(this).siblings().removeClass('active');
		$(this).addClass('active');

		if (!manuallyTriggered) {
			saveFilters();
		}

		return false;
	})

	// actions :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


	$(document).on('click', '.btn-state', function(e) {

		e.preventDefault();
		e.stopPropagation();

		var $this = $(this);
		var article = $this.closest('.article-box');
		var id = article.data('id');
		var url = $this.data('url');

		if (!id || !url) {
			 Adomat.Notification.error('The article action is not correctly defined.');
			 return false;
		}

		Adomat.Ajax.post(url, {
			application: 'news',
			id: id
		}).done(function(xhr) {
			
			xhr = xhr || {};

			// notifications
			if (xhr.notifications) {
				$.each(xhr.notifications, function(i, message) {
					Adomat.Notification.show(message);
				})
			}

			if (xhr.stateName) {
				$('.state', article).html(xhr.stateName);
			}

			if (xhr.panel) {
				$('.panel', article).removeClass(function (index, css) {
					return (css.match (/(^|\s)panel-\S+/g) || []).join(' ');
				}).addClass(xhr.panel);
			}

			$this.remove();
			$this.tooltip('hide');
		})

		return false;
	})

	// layout controlls ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

  	$articles.isotope({
		itemSelector: '.article',
		layoutMode: 'fitRows',
		getSortData: {
			title: '.title',
			section: '.section',
			state: '.state',
			created: function( itemElem ) {
				return $(itemElem).find('.created').data('sort');
			},
			expired: function( itemElem ) {
				return $(itemElem).find('.expired').data('sort');
			}
		},
		sortBy: 'title'
	})

  	$('.articles-container').infinitescroll({
		dataType		: 'json',
		navSelector  	: '.article-pagination a.next',
		nextSelector 	: '.article-pagination a.next',
		itemSelector 	: ".article",
		appendCallback	: false,
		validate		: false,
		data: {
			application: 'news',
			archived: $('#archived').val()
		},
		state: {
			currPage: $('a.next').data('page') || 1
		}, 
		path: function (pagenum) {
			var items = onPageLoad ? pagenum*12 : 12;
			var pagenum = onPageLoad ? 1 : pagenum;
			return '/applications/modules/news/article/load.php?page='+pagenum+'&items='+items;
		},
	},  function(result, opts) {

		onPageLoad = false;

		if (result) { 

			if (result) {

				// save qurent filters
				$('.article-pagination a.next').data('page', opts.state.currPage);
				
				Mustache.parse($articleTemplate);
				
				$.each(result, function(i, item) {
					var content = Mustache.render($articleTemplate, item);
					$articles.append(content);
					$articles.isotope('appended', $('#article-'+item.id));
				})

				$articles.isotope('layout');

				$('.list-filters button.active').each(function() { 
					$(this).trigger('click', [true]);
				})

				if ($('#quicksearch').val()) {
					$('#quicksearch').trigger('keyup', [true]);
				}
			}
		}
	})

	$('.articles-container').infinitescroll('retrieve');


	/* layout utilites :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

	function saveFilters() {
		retailnet.ajax.json('/applications/helpers/session.filter.php', {
			application: 'news',
			filter: 'articles',
			section: $('#group-sections button.active').data('id') || 0,
			state: $('#group-states button.active').data('id') || 0,
			sort: $('#group-sort button.active').data('filter'),
			view: $('#group-view button.active').data('filter'),
			page: $('a.next').data('page'),
			search: $('#quicksearch').val()
		})
	}

});