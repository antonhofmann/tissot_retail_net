$(document).ready(function() {

	var form = $('#user-invitation-form'),
		tplTestMail = $('#test-mail-template');

	$('#save').on('click', function(e) {
		e.preventDefault();
		retailnet.notification.hide();
		form.validationEngine('validate');
		form.submit();
		return false;
	});

	// submit user form
	form.validationEngine({
		autoHidePrompt: true,
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		onValidationComplete: function(form, status){ console.log('Complete: ', status);
			if (status==true) { console.log('Show loader: ', status);
				retailnet.loader.show();
			}
		},
		onAjaxFormComplete: function(status, form, json, options) {

			retailnet.loader.hide();
			retailnet.notification.hide();

			xhr = json || {};

			if (xhr.response && xhr.redirect) { 
				window.location=xhr.redirect;
			}

			if (xhr.response) { 
				$('#mail_template_recipients').val('');
			}

			if (xhr.errors) {
				$.each(xhr.errors, function(i,message) {
					retailnet.notification.error(message);
				})
			}

			if (xhr.messages) {
				$.each(xhr.messages, function(i,message) {
					retailnet.notification.success(message);
				})
			}
		}
	})

	var modalTestMail = tplTestMail.adoModal({
		button: $('#test'),
		width: 400,
		overlay: {
			opacity: 0.75,
			background: '#0000ff'
		}
	});

	$('.apply', tplTestMail).on('click', function(e) {
		
		e.preventDefault();

		retailnet.notification.hide();

		var url = $('#test').attr('href'),
			data = $('#sendmail-template form, #user-invitation-form').serializeArray(),
			recipient = $('#test-mail-recipient').val();

		if (!recipient) {
			retailnet.notification.error('Recipient e-mail is required.');
			return;
		}

		data.push({name: 'test', value: recipient});
			
		retailnet.loader.show();
		
		retailnet.ajax.json(url,data).done(function(xhr) {

			if (xhr && xhr.response) {
				modalTestMail.close();
			}

			if (xhr && xhr.errors) {
				$.each(xhr.errors, function(i,message) {
					retailnet.notification.error(message);
				})
			}

			if (xhr && xhr.messages) {
				$.each(xhr.messages, function(i,message) {
					retailnet.notification.success(message);
				})
			}

		}).complete(function() {
			retailnet.loader.hide();
		});	
			
	})

});