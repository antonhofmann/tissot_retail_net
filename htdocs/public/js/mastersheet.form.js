$(document).ready(function() {

	// button: splitting list dialog
	$('.print-options-modal').on('click', function(e) { 

		e.preventDefault();

		var target = $(this).attr('href');

		// reste ch3cked options
		$('input[type=checkbox]', $(target)).prop('checked', false);

		// show modal box
		retailnet.modal.show(target, {
			autoSize	: true,
			closeClick	: false,
			closeBtn	: false,
			fitToView   : true
		});

		return false;
	});

	$('.cancel').on('click', function(e) {
		e.preventDefault();
		retailnet.modal.close();
		retailnet.notification.hide();
		return false;
	});	

	$('.print-box .apply').click(function(e) {

		e.preventDefault();
		retailnet.loader.hide();
		retailnet.notification.hide();

		var modal = $(this).closest('.modalbox');

		var selectedGroups = $('.modal-fieldset.groups input:checked', modal).length ;
		var selectedOptions = $('.modal-fieldset.options input:checked', modal).length;
		
		if (selectedGroups && selectedOptions) {
			$('form', modal).submit();
			retailnet.modal.close();
		} else {
			retailnet.notification.error('Please, select at least one Planning Type / Product Group and one print option.');
		}

		return false;
	});
})