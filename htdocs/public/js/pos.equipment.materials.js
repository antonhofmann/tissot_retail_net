$(document).ready(function() {
	
	// spiner instance	
	retailnet.loader.init();

	var materials = $('#materials').tableLoader({
		url: '/applications/helpers/pos.equipment.material.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {

			$('select',self).dropdown();

			$('.mps_material_name a', self).click(function(event) {
			
				event.preventDefault();

				retailnet.loader.show();

				var modalbox = $('#modalbox');
				var material = $(this);
				var id = material.attr('id');
				var code = $('#mps_material_code-'+id).text();
				var name = material.text();

				// assign material code and name to modal screen
				$('#material').val(id);
				$('.modalbox-header .subtitle strong').text(code+', '+name);
				
				loadContent(modalbox);
				
				retailnet.modal.show('#modalbox', {
					autoSize	: false,
					closeClick	: false,
					closeBtn	: false,
					fitToView   : true,
				    width       : '90%',
				    height      : '90%',
				    maxWidth	: '820px',
				    maxHeight	: '820px',
					modal 		: true,
					afterLoad: function() {
						retailnet.loader.hide();
					},
					afterClose: function() {
						$('#material').val('');
						$('.modalbox-subtitle strong').text('');
						$('.modalbox-content').text('');
					}
				});

				return false;
			});
		}
	});

	var loadContent = function(modalbox) {
		$.ajax({
			url: '/applications/helpers/pos.equipment.material.locations.php',
			data: $('form.request').serialize(),
			success: function(data) {
				$('.modalbox-content').html(data);
			}
		});
	}

	$('#print').click(function(event) {
		event.preventDefault();
		var url = '/applications/exports/excel/pos.equipment.material.php';
		var request = $('form.request').serialize();
		retailnet.modal.close();
		window.location = url+'?'+request;
	});

	$('#cancel').click(function(event) {
		event.preventDefault();
		retailnet.modal.close();
		return false;
	});
	
});