
function sum(list) {
		
	var sum = 0;
	for (var i = 0; i < list.length; i++) {
		sum += list[i];
	}
	
	return sum;
}

$(document).ready(function() {

	var spreadsheet,
		filters = $('#filters'),
		application = $('#application').val(),
		controller = $('#controller').val(),
		action = $('#action').val(),
		mastersheet = $('#mastersheet').val(),
		readonly = $('#readonly').val() || 'false',
		fixedNotSelectable = ($('#fixedNotSelectable').val()) ? false : true,
		splittingForm = $('form.spliting-list');

// filters :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	filters.submit(function(e) {

		e.preventDefault();
		
		// load datagrid
		var data = $('form.request, #filters').serialize();
		var xhr = retailnet.ajax.request('/applications/modules/mastersheet/consolidation/items.php',data);
		var grid = xhr.data ? retailnet.json.normalizer(xhr.data) : {};

		spreadsheet = new Spreadsheet('#spreadsheet', grid, {
			readonly: true,
			autoHeight: true,
			fixedPartsNotSelectable: true,
			hiddenAsNull: true,
			fixTop: xhr.top,
			fixLeft: xhr.left,
			merge: xhr.merge ? eval(xhr.merge) : {}
		});

		// hide revision colum
		if (xhr && xhr.hideRevisionColumn) {
			spreadsheet.hideColumns(xhr.revisionColumn);
		}
		
		return false;

	}).trigger('submit');


	$('#launchplan').change(function(e) {
		e.preventDefault();
		filters.submit();
	});


// splitting list modal ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	// button: splitting list dialog
	$('#btnSplitting').on('click', function(e) {

		e.preventDefault();

		// empty planning types place holder
		$('.planningtypes-placeholder').empty();

		var productGroups = $('.spreadsheet h4');
		
		productGroups.each(function(i,e) {

			// insert input box 
			$('.planningtypes-placeholder').append('\
				<p><input type=checkbox name=group['+$(this).data('id')+'] value=1 />\
				<span>'+$(this).text()+'</span></p>\
			');
		});

		// show modal box
		retailnet.modal.show('#splittingDialog', {
			autoSize	: true,
			closeClick	: false,
			closeBtn	: false,
			fitToView   : true
		});

		return false;
	});

	// submit splitting list
	$('#submitSplittingList').click(function(e) {

		e.preventDefault();
		retailnet.loader.hide();
		retailnet.notification.hide();

		var selectedGroups = $('.modal-fieldset.groups input:checked', splittingForm).length ;
		var selectedOptions = $('.modal-fieldset.options input:checked', splittingForm).length;
		
		if (selectedGroups && selectedOptions) {
			splittingForm.submit();
			$('input:checkbox', splittingForm).attr('checked', false);
			retailnet.modal.close();
		} else {
			retailnet.notification.error('Please, select at least one planning type and one print option.');
		}

		return false;
	});

	// button: create version
	$('#btnVersion').on('click', function(e) {
		e.preventDefault();
		retailnet.modal.dialog('#version_dialog');
		return false;
	});

	// button: apply version
	$('#versionApply').on('click', function(e) {

		e.preventDefault();
		retailnet.loader.hide();
		retailnet.notification.hide();

		var versionTitle = $('#version_name').val();

		retailnet.modal.dialog('#version_dialog');

		if (versionTitle) {

			retailnet.loader.show();

			retailnet.ajax.json($('#btnVersion').attr('href'), {
				id: mastersheet,
				title: versionTitle
			}).done(function(xhr) {

				retailnet.modal.close();

				$('#version_name').val('');

				// reload page
				if (xhr && xhr.reload) {
					window.location.reload();
	        	}

				if (xhr && xhr.message) {
					if (xhr.response) retailnet.notification.success(xhr.message);
					else retailnet.notification.error(xhr.message);
				}

			}).complete(function() {
				retailnet.loader.hide();	
			});

		} else {
			retailnet.notification.error('Please enter Splitting List Version Name.');
		}
		
		return false;
	});

	// close modal screen
	$('.cancel').on('click', function(e) {
		e.preventDefault();
		$('#version_name').val('');
		retailnet.modal.close();
		retailnet.notification.hide();
		return false;
	});	
});