$(document).ready(function() {

	var form = $("#posltm");
	
	// spiner instance	
	retailnet.loader.init();
	
	// tooltips
	$('.-tooltip').qtip({style: {classes:'qtip qtip-dark' }});
	
	form.validationEngine({
		autoHidePrompt: true,
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		onAjaxFormComplete: function(status, form, json, options) { 

			retailnet.loader.hide();
			retailnet.notification.hide();

			if (json) {

				if (json.response && json.redirect) { 
					window.location=json.redirect;
				}

				if (json.message) {
					if (json.response) retailnet.notification.success(json.message);
					else retailnet.notification.error(json.message);
				}
			}
		}
	});

	$('#mps_material_category_id').chainedSelect('#mps_pos_material_material_id', {
		parameters: {
      		section: 'category_materials',
      		application: $('#application').val()
      	}
	}).trigger('change');
	
	$("#save").click(function(event) { 
		
		event.preventDefault();
		
		retailnet.notification.hide();
		
		if (form.validationEngine('validate')) {
			retailnet.loader.show();
			form.submit();
		} else {
			retailnet.notification.error('Please check red marked Fields.');
		}
	});

	$('.dialog').click(function(event) {

		event.preventDefault();
		
		var button = $(this);
		
		$('#apply').attr('href', button.attr('href'))
		
		retailnet.modal.dialog('#'+button.attr('id')+'_dialog');
	});

	$('#cancel').click(function(event) {
		event.preventDefault();
		$.fancybox.close();
		return false;
	});	

});