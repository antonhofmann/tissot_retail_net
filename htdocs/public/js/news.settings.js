$(document).ready(function() {

	var modalPictureContainer = $('#add-picture-modal');

	$(document).on('change', 'table.ajax-submitter input:text', function() {

		var self = $(this);

		Adomat.Ajax.post('/applications/modules/news/settings/ajax.php', {
			application: 'news',
			section: self.closest('.tab-pane').prop('id'),
			id: self.data('id'),
			field: self.prop('name'),
			value: self.val()
		})
	})

	$(document).on('keyup', 'table input:text', function(e) {
		if (e.keyCode==13) {
			var index = $('table input').index(this) + 1;
        	$('table input').eq(index).focus();
		}
	})

	var addPicture = modalPictureContainer.adoModal({
		width: 600,
		height: 400,
		button: '#addImage',
		clickClose: false,
		overlay: {
			opacity: 0.75,
			background: '#000'
		},
		afterClose: function() {
			$('.files', modalPictureContainer).empty();
			$('.fileupload-submit', modalPictureContainer).addClass('disabled');
		}
	});
		

	// file uploader instance ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$('.progress', modalPictureContainer).hide();
	$('.fileupload-submit', modalPictureContainer).addClass('disabled');

	var $uplPath = '/public/data/tmp/';

	$('#fileupload').fileupload({
		url: '/applications/helpers/file.uploader.php',
		dataType: 'json',
		formData: {
			application: 'news',
			path: $uplPath
		},
		submit: function (e) {
			$('.progress', modalPictureContainer).show(500);
		},
		done: function (e, data) {

			$('.files', modalPictureContainer).empty();

			setTimeout(function() { 
				$('.progress', modalPictureContainer).hide(500);
			}, 2000);
			
			$.each(data.result.files, function (index, file) {
				var img = $('<img />').addClass('img-responsive tpl-image').attr('src', $uplPath+file.name);
				$('.files', modalPictureContainer).append(img);
			})

			$('.fileupload-submit', modalPictureContainer).removeClass('disabled');
		},
		progressall: function (e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$('.progress-bar', modalPictureContainer).css('width', progress + '%');
		}
	})
	.prop('disabled', !$.support.fileInput)
	.parent().addClass($.support.fileInput ? undefined : 'disabled');


	// submit upload file
	$('.fileupload-submit', modalPictureContainer).on('click', function(e) {

		e.preventDefault();

		var img = $('.files img:first', modalPictureContainer);
		var src = img.attr('src');
		
		if (!src) {
			Adomat.Notification.error('Image not found');
			return;
		}

		Adomat.Ajax.post('/applications/modules/news/settings/template.picture.php', {
			section: 'add',
			file: src
		}).done(function(xhr) {

			if (xhr.file) {

				addPicture.close();

				Adomat.Notification.info('Your picture is uploaded.');

				var tr = $("<tr />")
			
				tr.append('<td><a href="#" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a></td>');
				tr.append('<td>'+xhr.name+'</td>');
				tr.append('<td><img src="'+xhr.file+'" class="img-responsive" /></td>');

				$('#pictures tbody').append(tr);
			}
		})

		return false;
	});

	// remove file
	$(document).on('click', '#pictures .btn-danger', function(e) {

		e.preventDefault();

		var row = $(this).closest('tr');
		var img = $('img', row);
		var src = img.attr('src');
		
		if (!src) {
			Adomat.Notification.error('Image not found');
			return;
		}

		BootstrapDialog.confirm('Are you sure to remove this picture?', function(confirmed) {
			if (confirmed) {
				Adomat.Ajax.post('/applications/modules/news/settings/template.picture.php', {
					section: 'remove',
					file: src
				}).done(function(xhr) {
					if (xhr.success) {
						addPicture.close();
						Adomat.Notification.info('Your picture is deleted.');
						row.remove();
					}
				})
			}
		})
				
		return false;
	});

});
