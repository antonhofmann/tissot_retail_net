$(document).ready(function() { 

	$('.sticker').on('click', function(e, attributes) {

		e.preventDefault();

		var self = $(this);

		if ($('.selected', self).hasClass('active')) {
  			return;
		}

		$('.article-teaser:first .article-sticker').remove();

		var article = $('.news-article').data('article') || $('.news-article').data('id');
		var teaser = $('.article-teaser:first'); 
		var sticker = self.clone().addClass('article-sticker');

		$('.stickers .selected').removeClass('active');
		$('.selected', self).addClass('active');
		$('.selected', sticker).addClass('active');
		$('.selected i', sticker).removeClass('fa-check-circle-o').addClass('fa-times-circle-o');

		// for manuallay triggering
		// update attributes from db
		if (attributes && typeof attributes === 'object') { 
			sticker.css(attributes);
		}
		
		// add sticker to teaser
		sticker.appendTo(teaser);
		sticker = $('.article-sticker', teaser);
		
		// update db
		if (!attributes) {
			
			var host = window.location.origin.toString();
			var bgImage = sticker.css('background-image');
			var background = bgImage.replace(host, '');

			submitSticker({
				sticker: sticker.data('id'),
				article: article,
				application: 'news',
				section: 'submit',
				left: sticker.position().left,
				top: sticker.position().top,
				width: sticker.width(),
				height: sticker.height(),
				'background-image': background
			})
		}
	
		sticker.draggable({
			containment: 'parent',
			stop: function(e, ui) {
				submitSticker({
					sticker: sticker.data('id'),
					article: article,
					application: 'news',
					section: 'submit',
					left: ui.position.left+'px',
					top: ui.position.top+'px'
				})
			}
		}).resizable({
			containment: 'parent',
			stop: function(e, ui) {
				submitSticker({
					sticker: sticker.data('id'),
					article: article, 
					application: 'news',
					section: 'submit',
					width: ui.size.width,
					height: ui.size.height
				})
			}
		})

		return;
	})

	$(document).on('click', '.article-sticker .selected.active', function(e) {

		e.preventDefault();

		var sticker = $(this).closest('.article-sticker');
		var article = $('.news-article').data('article') || $('.news-article').data('id');

		submitSticker({
			article: article,
			application: 'news',
			section: 'delete'
		}, function(xhr) {
			if (xhr && xhr.success) {
				sticker.remove();
				$('.stickers .selected').removeClass('active');
			}
		})

		return;
	})


	$(function() {

		var article = $('.news-article').data('article') || $('.news-article').data('id');

		/*
		if (article) {
			$.post('/applications/modules/news/article/article.sticker.php', {
				article: article,
				section: 'get'
			}, function(xhr) {
				
				if (xhr.data) {
					var i = xhr.data.id;
					xhr.data.id = '';
					$('.sticker.sticker-'+i).trigger('click', xhr.data);
				}

			}, 'json')
		}
		*/
	})

	var submitSticker = function(data, callback) {
		
		$.post('/applications/modules/news/article/sticker.php', data, function(xhr) { 
			
			if (xhr && xhr.notifications) {
				for (var i in xhr.notifications) {
					//Adomat.Notification.show(xhr.notifications[i]);
				}
			}

			if (typeof callback === 'function') {
				callback(xhr);
			}

		}, 'json')
	}
})