$(document).ready(function() {

	var ITEM = $('#item').val();

	var itemRegionSubmitter = function(data, callback) {

		$.ajax({
			method: "POST",
  			url: "/applications/helpers/item.regions.php",
  			dataType: "json",
			data: data
		}).done(function(xhr) {

			if (xhr && xhr.notifications) {
            	for (var i in xhr.notifications) {
            		Adomat.Notification.show(xhr.notifications[i]);
            	}
            }

            if (typeof callback === 'function') {
            	callback(xhr);
            }
		})
	}

	// region countriess toggler
	$('.table-regions th a').on('click', function(e) {
		e.preventDefault();
		var group = $(this).closest('table');
		$('tbody', group).slideToggle("fast");
		$('.arow-caret', group).toggleClass('fa-caret-down').toggleClass('fa-caret-up')
		return;
	})

	// submit country
	$(document).on('change', 'input.country', function(e, chackAllTriggered) {

		var self = $(this);
		var group = self.closest('table');
		var checked = self.is(':checked') ? 1 : 0;
		var manualTriggered = chackAllTriggered ? false : true;

		if (!self.val() ) {
			Adomat.Notification.error('System error. Please contact customer services.');
		}

		itemRegionSubmitter({
			section: 'country',
			item: ITEM,
			region: group.data('region'),
			country: self.val(),
			action: checked ? 1 : 0
		}, function(xhr) {
			$('input.region', group).trigger('changed', [manualTriggered]);
		})
	})

	// submit countries (region group)
	$(document).on('click', 'input.region', function(e) {
		
		var self = $(this);
		var group = self.closest('table');
		var checked = self.is(':checked');
		var countries = [];

		$('input.country', group).prop('checked', checked);

		// get all countries id
		countries = $('input.country', group).map(function() {
			return $(this).val();
		}).get();
		
		// show countries if is hidden
		/*
		if ((checked || $('input.country:checked', group).length) && $('tbody', group).is(':hidden')) {
			$('tbody', group).slideToggle('fast');
			$('.arow-caret', group).toggleClass('fa-caret-down').toggleClass('fa-caret-up');
		}
		*/

		$('.arow-caret', group).toggleClass('fa-caret-down').toggleClass('fa-caret-up');

		itemRegionSubmitter({
			section: 'countries',
			item: ITEM,
			region: group.data('region'),
			countries: countries,
			action: checked ? 1 : 0
		}, function(xhr) {
			self.trigger('changed');
		})	
	})

	// total group recipients
	$('input.region').on('changed', function(e, checkState) {
		
		e.stopImmediatePropagation();

		var self = $(this);
		var group = self.closest('table'); 
		var countries = $('input.country', group).length;
		var countriesSelected = $('input.country:checked', group).length;
		
		if (countries && checkState) {
			self.prop('checked', countries==countriesSelected ? true : false)
		}
		
		$('.total-countries', group).text(countriesSelected);

	}).trigger('changed', [true]);

	// disabled mode
	if (!$('input.region').length) {
		$('.table-regions').each(function(i,e) {
			var table = $(this);
			var selected = $('img.checked', table).length;
			$('.total-countries', table).text(selected);
		})
	}
})