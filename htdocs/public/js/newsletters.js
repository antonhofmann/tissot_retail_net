$(document).ready(function() {

	var $container = $('#newsletters-container'),
		$newsletters = $('#newsletters'),
		$template = $("#newsletter-template").html(),
		$quicksearch = $('#quicksearch'),
		resetPageNumber = true,
		Filters = {};

	$('.list-filters').stickyParent({
		bottoming: false,
		offset_top: $('.navbar-fixed-top').outerHeight()
	})

	// layout controlls ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	var isoOptions = {
		itemSelector: '.newsletter',
		layoutMode: 'fitRows',
		getSortData: {
			title: '.title',
			state: '.state',
			created: function( itemElem ) {
				return $(itemElem).find('.created').data('sort');
			},
			published: function( itemElem ) {
				return $(itemElem).find('.published').data('sort');
			}
		},
		sortBy: 'created'
	}

	var scrollOptions = {
		dataType		: 'json',
		navSelector  	: '.pagination a.next',
		nextSelector 	: '.pagination a.next',
		itemSelector 	: ".newsletter",
		appendCallback	: false,
		validate		: false,
		data: {
			application: 'news'
		},
		state: {
			currPage: $('a.next').data('page') || 1
		}, 
		path: function (pagenum) {
			var startItems = 60;
			var itemsPerPage = 20;
			var diff = Math.ceil(startItems/itemsPerPage)-1;
			var pagenum = resetPageNumber ? 1 : pagenum+diff;
			var items = pagenum==1 ? startItems : itemsPerPage;
			return '/applications/modules/news/newsletter/list.php?page='+pagenum+'&items='+items;
		}
	}

	// isotope instance
	$newsletters.isotope(isoOptions)

	// scroll load instance
  	$container.infinitescroll(scrollOptions, loadData );
	$container.infinitescroll('retrieve');

	// full text search ::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$quicksearch.keyup( debounce( function(e, manuallyTriggered) {

		var searchTerm = $quicksearch.val();

		if (!manuallyTriggered) {
			resetPageNumber = true;
			resetList({search:searchTerm});
			saveFilters();
		} else {
		
			var qsRegex = new RegExp( searchTerm, 'gi' );
			
			$newsletters.isotope({ filter: function() {
				return qsRegex ? $(this).text().match( qsRegex ) : true;
	    	}})
	    }

	}, 300 ));


	// filter controlls ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$('.list-filters button:not(.disabled, .bnt-group-responsive-caption, .dropdown-toggle)').on('click', function(e, triggerState) {
		
		e.preventDefault();
		e.stopPropagation();

		var manuallyTriggered = triggerState ? triggerState : false;

		$(this).siblings().removeClass('active');
		$(this).addClass('active');
		
		if (!manuallyTriggered) { 
			saveFilters();
		}

		return false;
	})

	// filter dropdowns
	$(".filter-dropdown li a").on('click', function(e, manuallyTriggered) {
		
		e.preventDefault();
		e.stopPropagation();

		var caption = $(this).data('caption');
		var search = $(this).data('search');
		var group = $(this).closest('.filter-dropdown');

		$(this).parents(".btn-group").find('.btn-caption').html(caption);
		group.data('value', search).removeClass('open');

		var data = [];

		$('.filter-dropdown').each(function(i,e) {
			var k = $(e).data('filter-group');
			var v = $(e).data('value');
			data[k] = v;
		});

		resetPageNumber = true;
		
		resetList(data);
	})

	// sort items
	$('button.sort').on('click', function(e) {

		e.preventDefault();
		e.stopPropagation();

		var $this = $(this);
		var $group = $this.closest('.filter-group')
		
		var sortBy = $this.data('filter');
		var sortAscending = $this.data('ascending') ? $this.data('ascending') : false;
		
  		$newsletters.isotope({ 
  			sortBy: sortBy,
  			sortAscending: sortAscending
  		})

  		$this.data('ascending', sortAscending ? 0 : 1)

  		$('.fa', $this).toggleClass('fa-caret-down').toggleClass('fa-caret-up')

		$('.bnt-group-responsive-caption label', $group).text($this.text());
		$this.closest('.bnt-group-responsive').removeClass('group-open');
		
		return false;
	})

	// filter items
	$('button.filter').on('click', function(e) {

		e.preventDefault();
		e.stopPropagation();

		var $this = $(this);
		var $group = $this.parents('.filter-group');
		var filterGroup = $group.data('filter-group');

		Filters[filterGroup] = $this.attr('data-filter');

		var filterValue = '';
		
		for (var prop in Filters) { filterValue += Filters[prop] }
		$newsletters.isotope({ filter: filterValue });

		$('.bnt-group-responsive-caption label', $group).text($this.text());
		$this.closest('.bnt-group-responsive').removeClass('group-open');

		return false;
	})

	// view mode
	$('button.view').on('click', function(e) { 

		e.preventDefault();
		e.stopPropagation();

		var viewMode = $(this).data('filter');
		$newsletters.removeAttr('class').addClass('row').addClass(viewMode);

		$(this).siblings().removeClass('active');
		$(this).addClass('active');
		$newsletters.isotope('layout');

		return false;
	})


	

	// open responsive collapsed buttons
	$('.list-filters .bnt-group-responsive-caption').on('click', function() {
		$(this).next('.bnt-group-responsive').toggleClass('group-open');	
	})

	// on load, set responsive button caption
	$('.bnt-group-responsive-caption').each(function() {
		var $this= $(this);
		var $group = $this.parents('.filter-group');
		var $caption = $('button.active', $group).text() || 'All';
		$('label', $this).text($caption)
	})


	// utilites ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	function saveFilters() {
		retailnet.ajax.json('/applications/helpers/session.filter.php', {
			application: 'news',
			filter: 'newsletters',
			section: $('#group-sections button.active').data('id'),
			state: $('#group-states button.active').data('id'),
			sort: $('#group-sort button.active').data('filter'),
			view: $('#group-view button.active').data('filter'),
			page: $('a.next').data('page'),
			search: $('#quicksearch').val(),
			year: $('.dropdown-years').data('value'),
			newsletter: $('.dropdown-newsletters').data('value')
		})
	}

	function loadData(result, opts) {

		resetPageNumber = false;

		if (result) { 

			$('a.next', $container).data('page', opts.state.currPage);

			if (result) {
				
				Mustache.parse($template);
				
				$.each(result, function(i, item) {
					var content = Mustache.render($template, item);
					$newsletters.append(content);
					$newsletters.isotope('appended', $('#newsletter-'+item.id));
				})

				$newsletters.isotope('layout');

				$('.list-filters button.active').each(function() {
					$(this).trigger('click', [true]);
				})

				/*
				if ($('#quicksearch').val()) {
					$('#quicksearch').trigger('keyup', [true]);
				}
				*/
			}
		}
	}

	function debounce( fn, threshold ) {
		
		var timeout;
		
		return function debounced() {
			
			if ( timeout ) {
				clearTimeout( timeout );
			}
			
			function delayed() {
				fn();
				timeout = null;
			}
			
			timeout = setTimeout( delayed, threshold || 100 );
		}
	} 

	function resetList(data) {

		var searchOptions = scrollOptions;
		
		searchOptions.state.currPage = 1;
		searchOptions.state.isDestroyed = false;
		searchOptions.state.isDone = false;
		searchOptions.data = $.extend(searchOptions.data, data);

		$newsletters.isotope('destroy');
		$container.infinitescroll('destroy');
		$newsletters.empty();

		// new isotope instance
		$newsletters.isotope(isoOptions)
		
		// new scroll load instance
		$container.infinitescroll(searchOptions, loadData );
		$container.infinitescroll('retrieve');
	}

})