// save localized province data

$(document).on('change', 'input[class^="country_name_"]', function(e) {
		
		var self = $(this);

		retailnet.ajax.json('/applications/modules/country/localization/ajax_submit.php', {
			application: $('#application').val(),
			language_id:self.closest('td').data('language_id'),
			store_locator_country_id: self.attr('index'),
			country_name: self.val()
		})
});


