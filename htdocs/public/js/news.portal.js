$(document).ready(function() {

	var $filters = $('#filters'),
		$application = $('#application').val(),
		$newsletter = $('#newsletter').val(),
		$search = $('#search'),
		$articles = [],
		$printBasket = $('.print-basket'),
		$printBasketItems = [],
		$printBasketSelectedArticles = [];

	if(navigator.appVersion.indexOf("MSIE")!=-1 || navigator.appVersion.indexOf("Trident")!=-1) {
		$('html').addClass('ie');
	}

	// track newsletter hit
	if ($newsletter) {
		$.post('/applications/modules/news/newsletter/track.php', {
			application: 'news',
			section: 'open-browser',
			newsletter: $newsletter,
			id: $newsletter
		})
	}
	
	// load articles :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$(function() {

		var spin = $("<div />").addClass('ajax-loader');
		$('#articles').prepend(spin.show());

		// session storage
		Storage.init();

		$.getJSON('/applications/modules/news/portal/articles.php', $filters.serialize(), function(xhr) {
			
			xhr = xhr || {};
			$('#articles').append(xhr.articles);
			$('#featured').append(xhr.featured);
			$('#toalArticles').append(xhr.total);
			spin.remove();

			$("#sidebar").sticky('update');

			if (xhr && xhr.sticker) {
				var stickerData = xhr.sticker;
				var i = stickerData.id;
				delete stickerData['id']; 
				$('.sticker.sticker-'+i).trigger('click', stickerData); 
			}

		}).done(function() {
			
			var urlHash = window.location.hash;
			
			if ($(urlHash).length) { 

				var hashPos = $(urlHash).offset().top;
				$(document).scrollTop(hashPos); 

				window.location.href.split('#')[0];

				// track hash
				$.post('/applications/modules/news/portal/track.php', {
					application: 'news',
					section: 'hash',
					newsletter: $newsletter,
					id: urlHash.substr(9)
				})
			}

			// load print items from session storage
			$printBasketItems = Storage.get('news.print.items', true);
			$printBasketSelectedArticles = Storage.get('news.print.item.titles');
			buildPrintBasket();
		})
	})

	// filters :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	// search form filters
	$(document).on('change', '.filter', function(e, load) {
		
		e.preventDefault();

		var self = $(this);
		var load = load ? 1 : 0;

		$.post('/applications/modules/news/portal/filters.php?load='+load, $filters.serialize(), function(xhr) {

			xhr = xhr || {};

			$('.filter', $filters).each(function(i,e) {

				var $el = $(e);
				var id = $el.prop('id');
				var canLoad = self.prop('id')==id ? load : true;
				var current = xhr.request && xhr.request[id] ? xhr.request[id] : '';

				if (canLoad && xhr[id]) {
					
					// remove all not null options
					$('option[value!=""]', $el).remove();

					// append new options
					$.each(xhr[id], function(i, opt) {
						var selected = opt.value==current ? true : false;
						$el.append($("<option />").val(opt.value).text(opt.name).prop('selected', selected));
					});
				}
			});
		})

		return false;
	})

	// trigger first filter
	$('.filter:first', $filters).trigger('change', [true]);

	$('.dropdown-menu').find('select').click(function(e) {
		e.stopPropagation();
	});

	$('#search').keypress(function(e) {
		if(e.which == 13) {
			$(this).closest('form').submit();
		}
	});

	var dropdown = $('.icon-addon.dropdown');

	$('#search').on('click', function(e) {
		
		if (!dropdown.hasClass('open')) { 
			dropdown.addClass('open');
		}
	})

	$('.toggler', dropdown).on('click', function(e) {
		e.preventDefault();
		dropdown.toggleClass('open');
		return false;
	})

	$(document).on('click', function(e) {
		if($(e.target).closest('.icon-addon.dropdown').length != 0) return false;
   		dropdown.removeClass('open');
	})

	$('.submit-search-form').on('click', function() {
		$filters.submit();
	})


	// submit filters form
	$filters.submit(function(e) {
		
		e.preventDefault();
		
		$.post('/applications/modules/news/portal/filters.php', $filters.serialize()).done(function() {
			window.location=$filters.prop('action');
		})
		
		return false;
	})

	// body content ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$(document).on('click', '.read-more', function(e) {

		e.preventDefault();

		var self = $(this);
		var target = self.data('target');
		var article = self.closest('.news-article').data('id');

		$('.ajax-loader').remove();
		
		// toggle target content
		$(target).slideToggle('fast');

		if (!$articles[article]) {

			var spin = $("<div />").addClass('ajax-loader');
			$(target).prepend(spin.show());

			$.post('/applications/modules/news/portal/article.php', {
				application: $application,
				newsletter: $newsletter,
				article: article
			}, function(xhr) {
				xhr = xhr || {};
				spin.remove();
				$articles[article] = true;
				$(target).append(xhr.content);
			})
		}
		
		// link caption
		var current = self.text();
		var caption = self.data('caption') || current;
		self.text(caption).data('caption', current);

		self.toggleClass('active');

		// track
		if (self.hasClass('active')) {
			$.post('/applications/modules/news/portal/track.php', {
				application: 'news',
				section: 'read-more',
				newsletter: $newsletter,
				id: article
			})
		}
				
		return false;
	})


	// sidebar :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$('#sidebar-toggle').on('click', function (e) { 
		
		if ($('.sidebar-right').hasClass('sidebar-popup')) {
			$('.sidebar-right').removeAttr('style').removeClass('sidebar-popup');
		} else {
			
			var offset = $(this).offset();
			
			$('.sidebar-right').css({
				top: $('.heighlight').offset().top,
				right: $(document).width()-$(this).offset().left-25
			}).toggleClass('sidebar-popup');
		}

		$(this).toggleClass('on');			  
	}); 

	
	$("#sidebar").sticky({
	 	topSpacing: 10,
	 	bottomSpacing: 250,
	 	getWidthFrom: '.sidebar',
	 	responsiveWidth: 1199,
	 	parentControl: true
	 });

	$('.categories label').on('click', function() { 
		$("#sidebar").sticky('update');
	})

	$('label.tree-toggler').click(function() {
		$(this).parent().children('ul.tree').toggle(300);
	});

	$('.categories a.active').closest('ul.tree').toggle(300);

	$('.categories a').on('click', function(e) { 
		
		e.preventDefault();

		var category = $(this).data('id');

		$('select.filter').val('');
		$('#search').val('');
		$('#category').val(category);
		
		$filters.trigger('submit');
		
		return false;
	})

	$(window).resize(function(e) { 

		$('.sidebar-right').removeAttr('style').removeClass('sidebar-popup');
		$('#sidebar-toggle').removeClass('on');

		if ($(window).width() < 991) $("#sidebar").unstick();
		else $("#sidebar").sticky('update');

	}).trigger('resize');



	// print articles ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$(document).on('click', '.add-print-basket', function(e) {

		e.preventDefault();

		var id = $(this).closest('.news-article').data('id');
		
		if ($(this).hasClass('in-basket')) removePrintArticle(id);
		else addPrintArticle(id);

		return false;
	})

	$(document).on('click', '.remove-print-item', function(e) {

		e.preventDefault();

		var item = $(this).closest('li');
		var id = item.data('id');

		removePrintArticle(id);

		return false;
	})

	var addPrintArticle = function(id) {

		var article = $('#article-'+id);
		var title = $('.article-title h5', article).text();

		$printBasketItems.push(id);
		$printBasketSelectedArticles.push(title);

		Storage.set('news.print.items', $printBasketItems);
		Storage.set('news.print.item.titles', $printBasketSelectedArticles);

		buildPrintBasket();

		return true;
	}

	var removePrintArticle = function(id) {

		var index = $printBasketItems.indexOf(id);

		if (index > -1) {
			$printBasketItems.splice(index, 1);
			$printBasketSelectedArticles.splice(index, 1);
		}

		Storage.set('news.print.items', $printBasketItems);
		Storage.set('news.print.item.titles', $printBasketSelectedArticles);
		
		buildPrintBasket();

		return true;
	}

	var buildPrintBasket = function() {

		// reset print nodes
		$('.print-items').empty();
		$('.add-print-basket').removeClass('in-basket');
		$('.add-print-basket .fa').removeClass('fa-times').addClass('fa-book');

		var pBasket = $('.print-items');
		
		$.each($printBasketItems, function(i, id) {

			var article = $('#article-'+id);
			var title = $printBasketSelectedArticles[i];//$('.article-title h5', article).text();

			// popullte print basket
			var item = $('<li />')
				.attr('id', 'print-item-'+id).data('id', id)
				.html('<span class="item"><a href="#"><i class="fa fa-times-circle remove-print-item"></i></a>'+title+'</span>')
				.appendTo(pBasket);

			$('.add-print-basket', article).addClass('in-basket');
			$('.add-print-basket .fa', article).removeClass('fa-book').addClass('fa-times');
		})

		// button print basket toggler
		$printBasket.toggle($printBasketItems.length ? true : false);
		$('#print-basket-total').text($printBasketItems.length);
	}

	$(document).on('click', '#print-newsletter-pdf', function(e) {
		e.preventDefault();
		exportFile('/applications/modules/news/portal/pdf.php', $newsletter);
		return false;
	})

	$(document).on('click', '#print-article-pdf', function(e) {

		e.preventDefault();

		if (!$printBasketItems.length) {
			Adomat.Notification.error('Please select at least one article for print.');
			return false;
		}

		exportFile('/applications/modules/news/portal/pdf.php');

		return false;
	})

	$(document).on('click', '#print-newsletter-doc', function(e) {
		e.preventDefault();
		exportFile('/applications/modules/news/newsletter/print.word.php', $newsletter);
		return false;
	})

	$(document).on('click', '#print-article-doc', function(e) {

		e.preventDefault();

		if (!$printBasketItems.length) {
			Adomat.Notification.error('Please select at least one article for print.');
			return false;
		}

		exportFile('/applications/modules/news/newsletter/print.word.php');

		return false;
	})

	var exportFile = function(url, newsletter) {

		Adomat.Loader.show('dotter');

		$.post(url, {
			application: 'news',
			articles: $printBasketItems,
			newsletter: newsletter,
			controller: $('#controller').val(),
		}, 'json').done(function(xhr) {

			if (xhr && xhr.success) {
				
				if (!newsletter) {
					
					// reset all print utilities
					$('.add-print-basket').removeClass('in-basket');
					$('.add-print-basket .fa').removeClass('fa-times').addClass('fa-book');
					$('.print-items').empty();

					// reset seletec articles
					$printBasketItems = [];
					$printBasketSelectedArticles = [];

					Storage.remove('news.print.items');
					Storage.remove('news.print.item.titles');

					buildPrintBasket();
				}

				// hide loder
				Adomat.Loader.hide();
			}

			if (xhr && xhr.file) {
				//window.open(xhr.file);
				window.location = xhr.file;
			}

			if (xhr && xhr.errors) {
				$.each(xhr.errors, function(i,message) {
					Adomat.Notification.error(message);
				})
			}
		})
	}

	var Storage = {

		engine: sessionStorage,

		init: function() { 
			
			if (typeof(sessionStorage) === undefined) {
				// bind server side storage engine
			}
		},

		get: function(key, isNumber) {
			
			var arr = [];
			var items = this.engine.getItem(key);

			if (items) {
				
				items = items.split(',');
				
				if (isNumber) {
					for (var i in items) {
						arr[i] = parseInt(items[i]);
					}
				} else {
					arr = items;
				}
			}

			return arr;
		},

		set: function(key, items) {
			
			if (items && items.length) {
				this.engine.setItem(key, items.toString());
			} else {
				this.remove(key);
			}
		},

		remove: function(key) {
			this.engine.removeItem(key);
		}
	}

});