$(document).ready(function() {

	var posindex = $('#posindex').tableLoader({
		url: '/applications/helpers/pos.list.php',
		data: $('.request').serializeArray(),
		after: function(self) {

			var advanced = $('#advanced');

			$('.selectbox',self).dropdown();

			advanced.pop().click(function() {
				$('.icon',this).toggleClass('direction-up');
			});

			$('.toggle-content').not('.selected').hide();

			$('.toggle-title').click(function() {
				$('.arrow',this).toggleClass('arrow-right').toggleClass('arrow-down');
				$(this).next().slideToggle('slow');
			});

			$('#cancel').click(function(event) {
				event.preventDefault();
				advanced.pop('hide');
				$('.icon',advanced).toggleClass('direction-up');
			});

			$('#apply').click(function(event) {
				event.preventDefault();
				posindex.showPage(1);
			});

			// print options
			$('#print').change(function() {

				var url = $(this).val();
				var host = location.host;
				
				if (url) {
					if (url=='exportbox') {
						if ($('#posaddresses-modal').val()) {
							
							retailnet.modal.show('#exportbox-posaddresses', {
								autoSize	: false,
								closeClick	: false,
								closeBtn	: false,
								fitToView   : true,
							    width       : '90%',
							    height      : '90%',
							    maxWidth	: '520px',
							    maxHeight	: '520px',
								modal 		: true
							});
							
	
						} else {
							$('#exportbox-posaddresses-apply').trigger('click');
						}
					} else {
						window.location = 'http://'+host+url;
					}
				}
				
				$('option:first',this).attr('selected', true);
			});

			$('#exportbox-posaddresses-apply').click(function(event) { 

				event.preventDefault();

				if ($('#exportbox-posaddresses input[type=checkbox]:checked').length > 0) {
					$('#exportbox-posaddresses form').submit();
					$.fancybox.close();
				} else {
					retailnet.notification.error('Please, select at least one Column for export.');
				}
			
				return false;
			});	

			$('.cancel').click(function(event) {
				event.preventDefault();
				$.fancybox.close();
				return false;
			});

			$('.select_all input').click(function() {
				var form = $(this).closest('form');
				var checked = ($(this).is(':checked')) ? true : false;
				$('input.export_field', form).attr('checked', checked);
				$('.select_all span', form).trigger('click');
			});

			$('.select_all span').click(function() { 
				var form = $(this).closest('form');
				var selectAll = $('.select_all input', form); 
				var checked = ($('input.export_field', form).length == $('input.export_field:checked', form).length) ? true : false;
				var caption = checked ? 'Deselect All' : 'Select All'
				selectAll.attr('checked', checked);
				$(this).text(caption);
			});

			$('input.export_field').click(function() {
				$('.select_all span').trigger('click');
			});	
			

			$('.modal').fancybox({ 
				width		: 720,
				height		: 720,
				autoSize	: false,
				closeClick	: false,
				closeBtn	: true,
				margin		: 0,
				padding		: 0,
				iframe		: {
					scrolling : 'auto',
					preload   : true
				}
			});
		}
	});
});