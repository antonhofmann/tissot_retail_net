var URL = '/applications/modules/news/newsletter/newsletter.articles.php';

Newsletter.addSection = function(section) {

	var container = $('<div />').attr('id', 'section-'+section.id).addClass('section-container').data('id', section.id);
	var header = $('<div />').addClass('section-header');
	var title = $('<h3 />').addClass('title').text(section.name);
	var body = $('<div />').addClass('section-body');
	
	header.append(title);
	container.append(header).append(body);
	$('.ajax-container', '#articles').append(container);

	// article order
	if (jQuery().sortable) {
		
		$('.section-body', container).sortable({
			placeholder: "ui-state-highlight",
			handle: ".article-sort",
			items: "> .article",
			forcePlaceholderSize: true,
			update: function( event, ui ) { 

				var data = $(this).sortable('toArray', {
					attribute: 'data-id'
				});
				
				Adomat.Ajax.post(URL, {
					application: 'news',
					section: 'sort',
					id: Newsletter.id,
					sort: data
				})
			}
		});

		container.disableSelection();
	}
}

$(document).ready(function() {

	var $currentArticles = $('#articles'),
		$unassignedArticles = $('#unassigned'),
		$otherArticles = $('#other'),
		$articleTemplate = $('#article-template').html();
		$teaserTemplate = $('#teaser-template').html();

	Newsletter.id = $('#newsletter_id').val();

	$(function() {

		Adomat.Ajax.post(URL, {
			application: 'news',
			section: 'load',
			id: Newsletter.id
		}).done(function(xhr) {

			xhr = xhr || {};

			if (xhr.data) {
				// form dataloader
				Newsletter.dataloader(xhr.data)
				// breadcrumps builder
				Newsletter.breadcrumps(xhr.data.newsletter_publish_state_id);
			}

			// actions
			Newsletter.actions(xhr.buttons);

			// build sections
			if (xhr.sections) {
				
				$.each(xhr.sections, function(i,section) {
					Newsletter.addSection(section);
				})
			}

			// articles
			if (xhr.articles) { 
				
				var template = $teaserTemplate;

				$.each(xhr.articles, function(i, item) {
					var content = Mustache.render(template, item);
					var section = $('#section-'+item.section);
					$('.section-body', section).append(content);
				})
			}

			// user dependent visibility
			Newsletter.visibility(xhr.disabled, xhr.remove);

			$('.roles.article-restrictions').data('title', 'Role Rstrictions')
			$('.roles.article-restrictions').data('trigger', 'click')
			$('.companies.article-restrictions').data('title', 'Company Rstrictions')
			$('.companies.article-restrictions').data('trigger', 'click')
		})
	})


	// article loader ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$unassignedArticles.infinitescroll({
		dataType		: 'json',
		behavior		: 'local',
		binder			: $('#unassigned'),
		navSelector  	: '#unassigned .pagination a:first',
		nextSelector 	: '#unassigned .pagination a:first',
		itemSelector 	: ".article",
		appendCallback	: false,
		validate		: false,
		data: {
			application: 'news',
			id: Newsletter.id,
			section: 'load-unassigned-articles'
		}
	},  function(result, opts) {

		if (result) { 
			$.each(result, function(i, item) {
				var content = Mustache.render($articleTemplate, item);
				$('.ajax-container', $unassignedArticles).append(content);
			})
		}
	})

	$unassignedArticles.infinitescroll('retrieve');

	$otherArticles.infinitescroll({
		dataType		: 'json',
		behavior		: 'local',
		binder			: $('#other'),
		navSelector  	: '#other .pagination a:first',
		nextSelector 	: '#other .pagination a:first',
		itemSelector 	: ".article",
		appendCallback	: false,
		validate		: false,
		data: {
			application: 'news',
			id: Newsletter.id,
			section: 'load-multiple-articles'
		}
	},  function(result, opts) {

		if (result) { 
			$.each(result, function(i, item) {
				var content = Mustache.render($articleTemplate, item);
				$('.ajax-container', $otherArticles).append(content);
			})
		}
	})

	$otherArticles.infinitescroll('retrieve');

	// flip article details
	$(document).on('click', '.back-toggle', function(e) {
		e.preventDefault();
		var panel = $(this).closest('.panel-box');
		var animate = $(this).data('animate') || 'flip';
		panel.toggleClass(animate);
		return false;
	})


	// add article :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$(document).on('click', '.panel-box .article-add', function(e) {

		e.preventDefault();

		var self = $(this);
		var article = self.closest('.panel-box');

		if (!article.data('id')) {
			Adomat.Notification.error('Article not found', {title: 'Error'});
			return false;
		}

		Adomat.Ajax.post('/applications/modules/news/newsletter/newsletter.article.submit.php', {
			application: 'news',
			section: 'add',
			id: Newsletter.id,
			article: article.data('id')
		}).done(function(xhr) {

			if (xhr && xhr.success) {

				article.fadeOut(1000).remove();
				
				if (xhr.article) {

					var section = $('#section-'+xhr.article.section);
					var content = Mustache.render($teaserTemplate, xhr.article);

					if (!section.length) {
						
						Newsletter.addSection({
							id: xhr.article.section,
							name: xhr.article.section_name
						});
						
						section = $('#section-'+xhr.article.section);
					}

					$('.section-body', section).append(content);
					$('.section-body', section).sortable( "refresh" );
				}
			}

			if (xhr && xhr.notifications) {
				$.each(xhr.notifications, function(i, message) {
					Adomat.Notification.show(message);
				})
			}
		})

		return false;
	})


	// remove article :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$(document).on('click', '.article-remove', function(e) {

		e.preventDefault();

		var self = $(this);
		var article = self.closest('.article');

		if (!article.data('id')) {
			Adomat.Notification.error('Article not found', {title: 'Error'});
			return false;
		}

		BootstrapDialog.confirm('Are you sure to remove this Article?', function(confirmed) {
			
			if (confirmed) {

				Adomat.Ajax.post('/applications/modules/news/newsletter/newsletter.article.submit.php', {
					application: 'news',
					section: 'remove',
					id: Newsletter.id,
					article: article.data('id')
				}).done(function(xhr) {

					if (xhr && xhr.success) {

						article.fadeOut(1000).remove();
						
						if (xhr.article && xhr.entity) {
							var content = Mustache.render($articleTemplate, xhr.article);
							$('.ajax-container', $(xhr.entity)).append(content);
						}
					}

					if (xhr && xhr.notifications) {
						$.each(xhr.notifications, function(i, message) {
							Adomat.Notification.show(message);
						})
					}
				})	
			}
		})

		return false;
	})

});