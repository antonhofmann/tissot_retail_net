$(document).ready(function() {

	var tab, id, checkbox, request, value, url;
	var application = $('#application').val();

	// form fields
	var form = $("#bulkform");
	var year = $('#mps_mastersheet_year');
	var mastersheet = $("#mps_mastersheet_id");
	var openingDate = $('#mps_ordersheet_openingdate');
	var closingDate = $('#mps_ordersheet_closingdate');
	var workflowState =  $('#mps_ordersheet_workflowstate_id');
	var description =  $('#mps_ordersheet_comment');
	var selectedCompanies = $("#selected_companies");
	var selectedItems = $("#selected_items");
	var selectedTab = $("#selected_tab");

	// from actions
	var buttonAdd = $("#add");
	var buttonUpdate = $("#update");
	var buttonDeleteOrdersheets = $("#delete_ordersheets");
	var buttonItems = $("#items");
	var buttonItemsDelete = $('#delete_items');

	// list fields
	var loader = $('#loader');
	var search = $("#search");
	var countries = $("#countries");

	// tabs
	var tabNewOrdersheets = $("#new_ordersheets");
	var tabExistingOrdersheets = $("#existing_ordersheets");
	var tabOrdersheetItems = $("#ordersheet_items");
	
	// spiner instance	
	retailnet.loader.init();
	
	// tooltip instance	
	retailnet.tooltip.init();


	// wysiwyg
	tinymce.init({
	    selector: "textarea.tinymce",
	    plugins: [
			"advlist autolink lists link visualblocks code nonbreaking contextmenu paste"
	    ],
	    width: 580,
	    height: 120,
	    menubar : false,
	    statusbar : false,
	    toolbar_items_size: 'small',
		autosave_restore_when_empty: false,
	    toolbar: "fontsizeselect | bold italic underline | forecolor | alignleft aligncenter alignright alignjustify | removeformat | paste pastetext pasteword"
	});

	// on page load reset tabs
	showTab(tabNewOrdersheets.attr('id'));

	// load tab content
	selectedTab.change(function() {
		loader.empty().tableLoader({
			url: '/applications/modules/ordersheet/manage/list.php',
			filters: form,
			after: function(self) {

				$('select', self).dropdown();

				// reset selected items
				if (selectedTab.val() == tabOrdersheetItems.attr('id') ) {
					selectedItems.val('');
					setSelected();
				}
			}
		});	
	}).trigger('change');	

	// master sheet year chained loader
	year.chainedSelect('#mps_mastersheet_id', {
		url : "/applications/modules/ordersheet/manage/ajax.php",
		parameters: {
      		section: "mastersheets",
      		application: application
      	}
	});

	// master sheet year change
	year.change(function() {

		tab = tabNewOrdersheets.attr('id');
		
		resetForm();
		mastersheet.val('');
		
		showTab(tab);
		selectedTab.val(tab).trigger('change');
	});

	// master sheet change
	mastersheet.change(function() {

		var value = $(this).val();
		resetForm();

		if (value && value != 0) {
			tab = (hasOrdersheets(value)) ? tabExistingOrdersheets.attr('id') : tabNewOrdersheets.attr('id');
			showTab(tab);
			selectedTab.val(tab).trigger('change');
		}
		
	}).trigger('change');


	// datapicker
	$('.datepicker').datepicker({ 
		dateFormat: 'dd.mm.yy' ,
		showOtherMonths : true,
		firstDay: 1,
		onClose: function(dateText, inst) {
			var dateError = (dateText && !isValidDate(dateText)) ? true : false;
			$(this).toggleClass('error', dateError);
		}
	});

	// tab content change
	$('.tabbed a').click(function(event) {

		event.preventDefault();
		tab = $(this).parent().attr('id');

		// reset selected companies
		if (tab == tabNewOrdersheets.attr('id') || tab == tabExistingOrdersheets.attr('id') ) {
			selectedCompanies.val('');
		}

		showTab(tab);	
		selectedTab.val(tab).trigger('change');
		
		return false;
	});

	// form validation
	form.validationEngine({
		autoHidePrompt: true,
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		onAjaxFormComplete: function(status, form, json, options) {

			retailnet.loader.hide();
			retailnet.notification.hide();
			
			// reset form fields
			if (json.response && json.reset) {
				resetForm();
			}

			if (json.response && json.selected_companies) {
				selectedCompanies.val(json.selected_companies)
			}

			if (json.response && json.selected_items) {
				selectedItems.val(json.selected_items)
			}
			
			if (json.response && json.tab) {
				showTab(json.tab);	
				selectedTab.val(json.tab).trigger('change');
			}

			if (!json.tab_items) {
				tabOrdersheetItems.hide();
			}

			if (json.message) { 
				if (json.response) retailnet.notification.success(json.message);
				else retailnet.notification.error(json.message);
			}
		}
	});

	// fetch form actions
	$(".submit").click(function(event) { 

		event.preventDefault();
		retailnet.notification.hide();

		var tab = selectedTab.val();
		var action = $(this).attr('id');
		var url = $(this).attr('href');
		var error = false;
		var messages = [];

		tinyMCE.triggerSave();
		
		switch (action) {

			case 'add' :

				if (!selectedCompanies.val()) {
					error = true;
					messages.push('Please, select at least one Company');
				}

				if (!openingDate.val()) {
					error = true;
					openingDate.addClass('error');
					messages.push('Please, set order sheet opening date');
				}

				if (!closingDate.val()) {
					error = true;
					closingDate.addClass('error');
					messages.push('Please, set order sheet closing date');
				}

			break;

			case 'update' :

				if (!selectedCompanies.val()) {
					error = true;
					messages.push('Please, select at least one order sheet');
				}
				
			break;

			case 'items' :

				$(this).removeClass('actions');

				if (!selectedItems.val()) {
					error = true;
					messages.push('Please, select at least one item');
				}
				
			break;
		}

		if (error && messages) { 
			
			retailnet.notification.error(messages.join('<br />'));

			form.validationEngine('validate');

			messages = [];
			error = false;
			
		} else {
			form.attr('action', url);
			retailnet.loader.show();
			form.submit();
		}

		return false;
	});

	$('.dialog').click(function(event) {

		event.preventDefault();
		
		var button = $(this);
		var dialog = '#'+button.attr('id')+'_dialog';
		
		var message = null;
		var tab = selectedTab.val();

		$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

		if (tab == tabExistingOrdersheets.attr('id') && !selectedCompanies.val()) {
			message = 'Please, select at least one order sheet';
		}

		if (tab == tabOrdersheetItems.attr('id') && !selectedItems.val()) {
			message = 'Please, select at least one item';
		}
		
		if (message) { 
			$.jGrowl( message, {sticky: true, theme: 'error'});
			return false;
		}
		
		retailnet.modal.show(dialog, {
			autoSize: true,
			closeBtn: false,
			afterLoad: function() {
				// assign clicked action link as data for dialog box
				$('.dialog_submit', $(dialog)).attr('data', button.attr('href'))
			}
		});

		return false;
	});

	// cancel dialog box
	$('#cancel, #cancelItem').click(function(event) {
		event.preventDefault();
		retailnet.modal.close();
		return false;
	});	

	// apply dialog box
	// IMPORTANT: the modal box builder must store action link for dialog apply button
	$('#apply, #applyItem').click(function(event) {

		event.preventDefault();
		
		// get clicked action link from data attribute
		var action = $(this).attr('data')
		$(this).removeAttr('data');
		
		// closed dialog
		retailnet.modal.close();

		// set splash screen
		retailnet.loader.show();

		// submit form
		form.attr('action', action).submit();
	})

	loader.delegate(".checkbox", "click", function() { 
		var table = $(this).closest('table');
		setSelected(table);
	});

	loader.delegate(".textbox", "change", function() { 
		var table = $(this).closest('table');
		var tr = $(this).closest('tr');
		var checked = ($(this).val()) ? 'checked' : null;
		$('.checkbox', tr).attr('checked', checked);
		setSelected(table);
	});

	loader.delegate(".checkall", "change", function() {

		var table = $(this).closest('table');
		var checkbox = $('.checkbox', table);
		var checked = ($(this).is(':checked')) ? 'checked' : false;
		checkbox.attr('checked', checked);
		setSelected(table);
	});

	function setSelected(table) {

		var tab = selectedTab.val();
		var count = 0;
		var selected, checkbox;

		if (table) {
			checkbox = $('.checkbox', table);
		} else { 
			checkbox = $('.checkbox');
		}

		if (tab == tabOrdersheetItems.attr('id')) {
			selected = $('#selected_items');
		} else {
			selected = $('#selected_companies');
		}
		
		var data = selected.val().split(','); 
		data = (data.length > 0) ? data : []; 
		
		$.each(checkbox, function() {

			value = $(this).attr('value');
			var index = $.inArray(value, data);

			if ($(this).is(':checked')) {
				data.push(value);
				count++;
			} 
			else if (index>-1) {
				data.splice(index, 1);
			}
		});
		
		selected.val('');
		
		if (data.length>0) {

			data = $.grep(data, function(n) { 
				return (n); 
			});

			data = $.grep(data, function(v,k){
			    return $.inArray(v,data) === k;
			});
			
			selected.val(data.join(','));
		}

		if (data.length > 0 && tab == tabExistingOrdersheets.attr('id')) {
			tabOrdersheetItems.show();
		} else if (tab != tabOrdersheetItems.attr('id'))  {
			tabOrdersheetItems.hide();
		}

		if (table) {
			var checked = (checkbox.length == count) ? 'checked' : false;
			$('.checkall', table).attr('checked', checked);
		}
	}

	
	// reset form
	function resetForm() {
		openingDate.val('');
		closingDate.val('');
		workflowState.val('');
		description.val('');
		selectedCompanies.val('');
		selectedItems.val('');
		search.val('');
		countries.val('');
		$('input[type=checkbox]').attr('checked', false);
		$('table input[type=text]').val('');
		$('input, select').removeClass('error');
	}

	
	// show selected tab
	function showTab(tab) {

		// select tab
		$('.tabbed a').removeClass('selected');
		$('#'+tab+' a').addClass('selected');

		// hide all actions and dialog actions
		$('.actions .button').hide();

		// reset error classes
		$('input, select').removeClass('error');

		switch (tab) {

			case tabNewOrdersheets.attr('id') :

				tabNewOrdersheets.show();

				// for non-selected mastersheet,
				// hide tab existing ordersheets
				if (!mastersheet.val() || mastersheet.val() == 0 ) {
					tabExistingOrdersheets.hide();
				}

				// hide tab items
				tabOrdersheetItems.hide();

				// show companies buttons
				$('.-companies').show();

				// reset selected items
				selectedItems.val(''); 

				// toggle tab dependet fields
				$(".mps_ordersheet_comment, .mps_ordersheet_openingdate, .mps_ordersheet_closingdate").show();
				$(".update_description, .mps_ordersheet_workflowstate_id").hide();
				
			break;

			case tabExistingOrdersheets.attr('id') :

				tabExistingOrdersheets.show();

				// reset selected items
				selectedItems.val(''); 

				// hide tab items
				tabOrdersheetItems.hide();

				// show ordersheets buttons
				$('.-ordersheets').show();

				// toggle tab dependet fields
				$(".mps_ordersheet_comment, .mps_ordersheet_openingdate, .mps_ordersheet_closingdate, .update_description, .mps_ordersheet_workflowstate_id").show();
			
			break;

			case tabOrdersheetItems.attr('id') :

				tabOrdersheetItems.show();

				// show items buttons
				$('.-items').show();

				// toggle tab dependet fields
				$(".mps_ordersheet_comment, .mps_ordersheet_openingdate, .mps_ordersheet_closingdate, .update_description, .mps_ordersheet_workflowstate_id").hide();
			
			break;
		}		
	}

	
	// has master sheet order sheets
	function hasOrdersheets(mastersheet) {

		var request = $.ajax({
			url: '/applications/modules/ordersheet/manage/ajax.php?section=has_ordersheets',
			data: form.serialize(),
			dataType: 'json',
			async: false
		}).responseText;
		
		request = $.parseJSON(request);
		return request.response ? true : false;			
	}

});