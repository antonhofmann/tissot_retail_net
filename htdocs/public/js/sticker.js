$(document).ready(function() {

	var user = $('#user').val(),
		$file = $('#sticker_image'),
		$container = $('.sticker-cropper'),
		$form = $('form#sticker'),
		$btnSubmit = $('button[type=submit]', $form),
		$btnDelete = $('button[type=reset]', $form);

	var Cropper = new Croppic('stickerCropper', {
		uploadData: {
			path: '/data/tmp',
			filename: 'sticker.'+user,
			minWidth: $container.width(),
			minHeight: $container.height()
		},
		cropData: {
			path: '/data/tmp',
			filename: 'sticker.'+user
		},
		onAfterImgUpload: function(self, response) {
			
			response = response || {};
			
			if (response.success && response.url) {
				$file.data('crop', 0);
				$('input[name="hasupload"]').val(1);
				$file.val(response.url).trigger('change');
			}

			if (response.error) {
				Adomat.Notification.error(response.error);
			}
		},
		onAfterImgCrop: function(self, response) {
			
			response = response || {};
			
			if (response.success && response.url) {
				$file.data('crop', 1);
				$file.val(response.url).trigger('change');
			}

			if (response.error) {
				Adomat.Notification.error(response.error);
			}
		}
	})

	// load sticker image
	if ($file.val()) {
		Cropper.loadImage($file.val());
	}
	
	if (!$btnSubmit.length) {
		$(":input, textarea, select").prop("disabled", true);
		$('.cropControls', $container).remove();
	}

    $form.on('submit', function(e) {

    	e.preventDefault();

    	var url = $btnSubmit.data('url');

    	if (!url) {
			Adomat.Notification.error('Action URL is not defined.');
			return false;
		}

		if (!$file.val()) {
			Adomat.Notification.error('Sticker image is not selected.');
			return false;
		}

		if ($('input[name="hasupload"]').val() && !$file.data('crop')) {
			Cropper.crop(function() {
				submitForm(url); 
			});
		} else submitForm(url);

    	return;
    })


    $btnDelete.on('click', function(e) {

    	e.preventDefault();

    	var self = $(this);
		var url = self.data('url');
		var msg = self.data('message') ? self.data('message') : 'Are your sure?';

		if (self.data('messageNode')) {
			var node = self.data('messageNode');
			msg = $(node).length ? $(node).html() : msg;
		}

		if (!url) {
			Adomat.Notification.error('Action URL is not defined.');
			return false;
		}
		
		BootstrapDialog.confirm(msg, function(confirmed) {
			
			if (confirmed) {

				Adomat.Ajax.post(url).done(function(xhr) {

					if (xhr && xhr.redirect) {
						window.location=xhr.redirect;
					}

					if (xhr && xhr.notifications) {
		            	for (var i in xhr.notifications) {
		            		Adomat.Notification.show(xhr.notifications[i]);
		            	}
		            }
				})	
			}
		})

		return;
    })

    var submitForm = function(url) {

    	$.post(url, $form.serialize(), function(xhr) {
            
            if (xhr && xhr.success) {
            	
            	if (xhr.redirect) {
            		window.location = xhr.redirect;
            	}

            	if (xhr.file) {
            		$file.val(xhr.file);
            		Cropper.loadImage(xhr.file);
            	}

            	$('input[name="hasupload"]').val('');
            	$file.data('crop', 0);
            }	

            if (xhr && xhr.notifications) {
            	for (var i in xhr.notifications) {
            		Adomat.Notification.show(xhr.notifications[i]);
            	}
            }

        }, 'json');
    }
})