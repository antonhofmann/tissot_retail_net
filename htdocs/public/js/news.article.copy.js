
var Article = Article || {}


Article.breadcrumps = function(state) {

	var $this = this;
	var container = $('#breadcrumps');

	if (!$this.states) {
		
		this.states = Adomat.Ajax.json('/applications/modules/news/article/article.php', {
			application: 'news',
			section: 'workflow-states'
		});

		$this.states = this.states;
	}
	
	container.empty();

	for (var i in $this.states) { 
		var item = $('<li />');
		item.text($this.states[i]);
		if (i<=state) item.addClass('active');
		container.append(item); 
	}
}


Article.updateField = function(field, value) {

	var $field = $('#'+field);
	var type = $field.prop('tagName');

	if (type) {

		$field.val(value);

		if ($field.is(':checkbox')) {
			$field.attr('checked', value)
		}

		if (jQuery().selectpicker && $field.hasClass('selectpicker')) {
			$field.selectpicker('refresh');
		}

		if (jQuery().datepicker && $field.hasClass('datepicker')) { 
			$field.datepicker( "refresh" );
		}

		if ($field.data('caption')) { 
			$field.trigger('update-caption'); 
		}
	}
}


Article.tracks = function(tracks) {

	if (tracks) {

		var container = $('#articleTracks');

		for (var i in tracks) {

			var track = tracks[i];
			var p = $('<p />');

			p.append("<span>"+track.date+" "+track.name+"</span>"+track.content);

			container.append(p);
		}
	}
}

Article.getAction = function(button) {
	var btn = $('<a />');
	btn.attr(button.attributes);
	btn.data(button.data);
	btn.html(button.title);
	btn.prepend(button.icon);
	return btn;
}

Article.submitAction = function(button, data, callback) {

	var url = button.prop('href');

	if (!url) {
		 Adomat.Notification.error('Action URL is not defined.');
		 return false;
	}

	Adomat.Ajax.post(url, data).done(function(xhr) {
			
		xhr = xhr || {};

		if (xhr.notifications) {
			$.each(xhr.notifications, function(i, message) {
				Adomat.Notification.show(message);
			})
		}

		if (!xhr.success) {
			return false;
		}

		button.remove();

		if (xhr.redirect) {
			window.location=xhr.redirect;
		}

		if (xhr.state) {
			Article.breadcrumps(xhr.state);
		}

		if (xhr.remove || xhr.disable) {
			Article.visibility(xhr.remove, xhr.disable);
		}

		if (xhr.buttons) {
			$.each(xhr.buttons, function(i, button) {
				var btn = Article.getAction(button);
				$('#actions').prepend(btn);
			});
		}

		if (typeof callback === 'function') {
			callback();
		}
	})
}

Article.visibility = function(remove, disable) { 

	if (disable) {
		$.each(disable, function(field, val) {
			$('#'+field).prop('disabled', true);
		})
	}

	if (remove) {
		$.each(remove, function(i,field) {
			$(field).remove();
		})
	}
}

Article.validate = function(self) {

	var validator = self.data('validate')
	var value = self.val();

	if (value) {
		
		switch (validator) {

			case 'url':
				
				var regex = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/gi.test(value);
				
				if (!regex) {
					var field = self.prop('placeholder') || self.prop('id');
					Adomat.Notification.error(field+" is wrong. The URL should be http://www.yourdomain.com (url params are permitted)");
					return false;
				}

			break;
		}
	}

	return true;
}


Article.settings = function(field, settings) {

	if (field.length && typeof settings === 'object') {
		
		// class name
		field.addClass(settings.className);
		delete settings.className;
		
		// data attributes
		field.data(settings.data);
		delete settings.data;

		field.prop(settings);
	}
}


$(document).ready(function() {

	var $article = $('#news_article_id'),
		$articleField = $('.article-field'),
		$sections = $('#news_section_id'),
		$categories = $('#news_article_category_id'),
		$articleAuthor = $('#news_article_author_id'),
		$articleContents = $('#article-contents'),
		$templatesContainer = $("#templates-container"),
		$dropdownList = $('.dropdown-list'),
		$editors = [],
		$croppers = [],
		$tables = [];

	Adomat.Notification.info("Your article will be automatically saved");

	var URL = '/applications/modules/news/article/article.php';

	// popovers
	$('body').popover({
		selector: '.has-popover',
		container: 'body',
		trigger: 'hover'
	})

	// toggle error class for reqired fields
	$(document).on('change', '[required]', function(e) {
		$(this).parent().toggleClass('has-error', !$(this).val() ? true : false);
	});

	// toggle error class for reqired fields
	$(document).on('change', 'input[type=email]', function(e) {
		
		var val = $(this).val();
		
		if (val) {
			var re = /\S+@\S+\.\S+/; 
			$(this).parent().toggleClass('has-error', !re.test(val) ? true : false);
		}
	});

	// on change table headers
	$(document).on('change', 'input.tbl-header', function(e) {
		
		var self = $(this);

		Adomat.Ajax.post(URL, {
			application: 'news',
			section: 'template-table-header',
			id: $article.val(),
			tpl: self.data('tpl'),
			col: self.data('col'),
			value: self.val()
		})
	});

	/*
	$("#accordions").sticky({
	 	topSpacing: 120,
	 	bottomSpacing: 100,
	 	getWidthFrom: '.sidebar',
	 	responsiveWidth: 1199
	 });

	$('#accordions').on('shown.bs.collapse', function () {
		$("#accordions").sticky('update');
	})
	*/


	// wysiwyg editors  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	tinymce.init({
		selector: 'textarea.editor',
		plugins: ["advlist autolink lists link visualblocks code nonbreaking contextmenu paste preview"],
		width: '100%',
		height: 200,
		menubar : false,
		statusbar : false,
		toolbar_items_size: 'small',
		autosave_restore_when_empty: false,
		paste_as_text: true,
		toolbar: "styleselect | bold italic underline | forecolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent  |  removeformat",
		style_formats: [
			/*{title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},*/
			{title: 'Title', block: 'h4', classes: 'news-title'},
			{title: 'Small Text', inline: 'span', classes: 'news-text-sm'},
			{title: 'xSmall Text', inline: 'span', classes: 'news-text-xs'}
		],
		content_css : "/public/css/news.content.css?" + new Date().getTime(),
		setup : function(ed) {
			ed.on('change', function(e) {
				$(ed.getElement()).val(ed.getContent()).trigger('change');
			});
		}
	})

	// dropdown controlls ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	if (jQuery().selectpicker) { 
		$('.selectpicker').selectpicker();
	}

	$articleAuthor.on('change', function() {

		var self = $(this);

		if (self.val()=='new') {
			authorModal.open();
			return false;
		}
	})

	// update caption holder from select picker
	$('#show-confirmators').on('click', function() {
		$('#news_articles_confirmed_user_id').next().find('button.selectpicker').trigger('click');
		$('#news_articles_confirmed_user_id').selectpicker('show');
	});

	// datapicker ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	

	$( "input.datepicker" ).datepicker({
		showOn: "both", 
      	buttonText: '<i class="fa fa-calendar"></i>',
      	dateFormat: 'dd.mm.yy'
	});


	// sort content ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$templatesContainer.sortable({
      placeholder: "news-article-template-container ui-state-highlight",
      handle: ".tpl-order",
      items: "> .news-article-template-container",
      forcePlaceholderSize: true,
      update: function( event, ui ) { 

      		var data = $(this).sortable('toArray', {attribute: 'data-id'});
      		
      		Adomat.Ajax.post(URL, {
				application: 'news',
				section: 'template-sort',
				id: $article.val(),
				sort: data
			}).done(function(xhr) {
				if (xhr && xhr.success) {
					span.remove();
				}
			}) 
      }
    });

    $templatesContainer.disableSelection();


    // add new author ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    var authorModal = new BootstrapDialog({
		title: 'Add New Author',
		message: $('<div />').load('/applications/templates/news/article/author.form.php'),
		autodestroy: false,
		buttons: [
			{
				label: 'Cancel',
				cssClass: 'btn-default pull-left text-mutted',
				action: function(dialogItself){
					authorModal.close();
				}
			},
			{
				id: 'btnAddAuthor',
				cssClass: 'btn-primary',
				icon: 'fa fa-check',
				label: 'Add',
				action: function(dialog) {
					
					var $button = this;
					var $content = dialog.getModalBody();

					// trigger required fields
					$content.find('input[required]').trigger('change');

					if ($content.find('.has-error').length) {
						Adomat.Notification.show({
							type: 'error',
							title: 'Error',
							text: 'Red marked fields are mandatory'
						});
					} else {

						dialog.enableButtons(false);
						dialog.setClosable(false);
						$button.toggleSpin(true);

						var data = $content.find('form').serializeArray();
						data.push({name: 'application', value: 'news'});

						Adomat.Ajax.post('/applications/modules/news/author/submit.php', data).done(function(xhr) {
							
							dialog.setClosable(true);
							dialog.enableButtons(true);
							$button.toggleSpin(false);

							if (xhr && xhr.errors) {
								$.each(xhr.errors, function(i, msg) {
									Adomat.Notification.error(msg);
								})
							}

							if (xhr && xhr.id) {

								var id = 'n'+xhr.id;
								var opt = $('<option />').attr('value', id).text(xhr.name).attr('selected', true);
								var group = $('optgroup.group-news');

								if (!group) {
									var group = $('<optgroup />').addClass('group-news').attr('label', 'News Authors');
									$articleAuthor.append(group);
								}

								group.append(opt).val(id);
								$articleAuthor.trigger('change');
								$articleAuthor.selectpicker('refresh');
								dialog.close();
							}
						})
					}
				}
			}
		],
		onhidden: function(dialog){
			
			dialog.getModalBody().find('form').trigger("reset");
			
			if ($articleAuthor.val()=='new') {
				$articleAuthor.val('').selectpicker('refresh');
			}
		}
	})


	// on page load ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$(function() {

		Adomat.Ajax.post('/applications/modules/news/article/article.load.php', {
			application: 'news',
			section: 'load',
			id: $article.val()
		}).done(function(xhr) {

			// assign article data
			if (xhr && xhr.article) {
				
				for (var field in xhr.article) {
					Article.updateField(field, xhr.article[field]);
				}

				// build breadcrumps
				Article.breadcrumps(xhr.article.news_article_publish_state_id);
			}
			
			// template content initialisation
			if (xhr && xhr.templates) {
				
				// assign article templates
				$.each(xhr.templates, function(ph,conntent) {
					var container = $('#'+ph+'-container');
					container.append(conntent);
				})

				// load template files
				if (xhr.files) {
					for (var id in xhr.files) {
						var files = xhr.files[id];
						var tpl = $('#article-template-'+id);
						$.each(files, function(i, file) { fileRender(tpl, file) })
					}
				}
			}

			// article actions
			if (xhr && xhr.buttons) {
				$.each(xhr.buttons, function(i, button) {
					var btn = Article.getAction(button);
					$('#actions').append(btn);
				});
			}

			// article tracks
			Article.tracks(xhr.tracks);

			// bind template addons
			$('.news-article-template-container').each(function(i,e) {
				var id = $(e).data('id');
				var data = xhr.dataloader ? xhr.dataloader[id] : {};
				iniTemplate($(e), data)
			})

			// checkal controll
			$('.checkbox-list input.check-all').each(function(i, e) {
				$(e).trigger('changed', [true]);
			})

			// trigger subsidiaries
			$('input.check-all-subsidiaries').trigger('changed', [true]);
			$('input.check-all-agents').trigger('changed', [true]);
			$('input.check-all-region').trigger('changed', [true]);

			// trigger regions accrodion
			$('li.chackall', $('ul.checkbox-regions')).trigger('click');

			// settings
			if (xhr.settings) {
				$.each(xhr.settings, function(el,settings) {
					Article.settings($('.'+el), settings)
				})
			}

			$( "#news_article_expiry_date" ).datepicker( "option", "minDate", 0 );

			// visibility
			Article.visibility(xhr.remove, xhr.disabled);
		})
	})


	// article action ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$(document).on('click', '#actions .btn:not(disabled):not(.preview)', function(e) {

		e.preventDefault();

		var $this = $(this);
		var id = $article.val();
		var url = $this.prop('href');

		// reject action
		if ($this.prop('id')=='btnReject') {
			rejectModal.open();
			return false;
		}

		if ($this.hasClass('btn-dialog')) {

			var msg = $this.data('message') || 'Are your sure?';
		
			BootstrapDialog.confirm(msg, function(confirmed) {
				if (confirmed) {
					Article.submitAction($this, {
						application: 'news',
						id: $article.val(),
					});	
				}
			})

			return false;
		}

		Article.submitAction($this, {
			application: 'news',
			id: $article.val(),
		});	

		return false;
	})


	// checkbox list controls ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	// company on change
	$('.checkbox-list input.article-checkbox').on('change', function(e, all, subsidiary, agent, region) {

		e.stopImmediatePropagation();

		var $this = $(this);
		var panel = $this.closest('.panel-body');
		
		var setStateCheckAll = all ? false : true;
		var setStateSubsidiary = subsidiary ? false : true;
		var setStateAgents = agent ? false : true;
		var setStateRegion= region ? false : true;
		//console.log(setStateCheckAll, setStateSubsidiary, setStateAgents, setStateRegion);
		if (!$this.data('id')) {
			Adomat.Notification.error('Action ID not defined.');
			return false;
		}

		if (!panel.data('section')) {
			Adomat.Notification.error('Action section not defined.');
			return false;
		}

		Adomat.Ajax.post(URL, {
			application: 'news',
			section: panel.data('section'),
			id: $article.val(),
			reference: $this.data('id'),
			state: $this.is(':checked') ? 1 : 0
		}).done(function(xhr) {
			$('input.check-all', panel).trigger('changed', [setStateCheckAll]);
			$('input.check-all-subsidiaries', panel).trigger('changed', [setStateSubsidiary]);
			$('input.check-all-agents', panel).trigger('changed', [setStateAgents]);
			$('input.check-all-region', $this.closest('ul')).trigger('changed', [setStateRegion]);
		})
	})

	// check all comanies
	$('.checkbox-list input.check-all').on('click', function(e) {
		var $this = $(this);
		var panel = $this.closest('.panel-body');
		var checked = $this.is(':checked');
		$('input.article-checkbox', panel).prop('checked', checked).trigger('change', [false]);
	})

	// controll statement for check all companies
	$('.checkbox-list input.check-all').on('changed', function(e, checkState) {
		
		//e.stopImmediatePropagation();

		var $this = $(this);
		var panel = $this.closest('.panel-body'); 
		var items = $('input.article-checkbox', panel).length;
		var checked = $('input.article-checkbox:checked', panel).length;
		
		if (checkState) {
			$this.prop('checked', items==checked ? true : false);
		}

		$('.total-group', $this.closest('.panel') ).toggle(checked>0 ? true : false).text(checked);
	})

	// check all subsidiaries
	$('input.check-all-subsidiaries').on('click', function(e) {
		var $this = $(this);
		var panel = $this.closest('.panel-body');
		var checked = $this.is(':checked');
		$('input.subsidiary', panel).prop('checked', checked).trigger('change', [true, false, true]);
	})

	// control statemenet for check all subsidiaries
	$('input.check-all-subsidiaries').on('changed', function(e, checkState) { 
		
		//e.stopImmediatePropagation();

		if (!checkState) return false; 

		var $this = $(this);
		var panel = $this.closest('.panel-body'); 
		var items = $('input.subsidiary', panel).length;
		var checked = $('input.subsidiary:checked', panel).length;

		$('input.check-all-subsidiaries').prop('checked', items==checked ? true : false);
	})

	// checl all agents
	$('input.check-all-agents').on('click', function(e) {
		var $this = $(this);
		var panel = $this.closest('.panel-body');
		var checked = $this.is(':checked');
		$('input.agent', panel).prop('checked', checked).trigger('change', [true, true, false]);
	})

	// controll statement for check all agents
	$('input.check-all-agents').on('changed', function(e, checkState) { 
		
		//e.stopImmediatePropagation();

		if (!checkState) return false;

		var $this = $(this);
		var panel = $this.closest('.panel-body'); 
		var items = $('input.agent', panel).length;
		var checked = $('input.agent:checked', panel).length;
		$('input.check-all-agents').prop('checked', items==checked ? true : false);
	})

	// checl all region
	$('input.check-all-region').on('click', function(e) {
		
		e.stopPropagation();

		var $this = $(this);
		var region = $this.closest('ul');
		var checked = $this.is(':checked');
		$('input.region', region).prop('checked', checked).trigger('change', [false, false, false, true]);
		
		$this.closest('li').trigger('click', [true]);
		$('li.chackall', region).trigger('count.checked');
	})

	// controll statement for check all agents
	$('input.check-all-region').on('changed', function(e, checkState) {
		
		e.stopImmediatePropagation();

		if (!checkState) return false;

		var $this = $(this);
		var region = $this.closest('ul');
		var items = $('input.article-checkbox', region).length;
		var checked = $('input.article-checkbox:checked', region).length;
		$this.prop('checked', items==checked ? true : false);

		$('li.chackall', region).trigger('count.checked');
	})

	$('ul.checkbox-regions li.chackall').on('click', function(e, manuellyClickked) {

		e.stopImmediatePropagation();

		var self = $(this);
		
		if (manuellyClickked) {
			self.siblings().hide().slideToggle();
		} else {
			self.siblings().slideToggle();
			self.trigger('count.checked');
		}
	})

	$('ul.checkbox-regions li.chackall').on('count.checked', function(e) {
		var self = $(this);
		var region = self.closest('ul');
		var checked = $('input.article-checkbox:checked', region).length;
		$('.total-region', self).text('('+checked+')').toggle(checked>0 ? true : false);
	})

	// add template contents :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$('.tmp-add').on('click', function(e) {

		e.preventDefault();

		var self = $(this);

		Adomat.Ajax.post(URL, {
			application: 'news',
			section: 'add-template',
			id: $article.val(),
			template: self.data('id')
		}).done(function(xhr) {

			if (xhr && xhr.template && xhr.container) {
				
				$('#'+xhr.container+'-container').append(xhr.template);
				
				var tpl = $('#article-template-'+xhr.id);
				
				// add focus to first text box
				$('input[type=text]', tpl).focus();

				// init template addons
				iniTemplate(tpl);
			}
		})

		return false;
	})


	// remove template contents ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$(document).on('click', '.tpl-remover', function(e) {

		e.preventDefault();

		var self = $(this);
		var tpl = self.closest('.news-article-template-container');

		Adomat.Ajax.post(URL, {
			application: 'news',
			section: 'remove-template',
			id: $article.val(),
			template: tpl.data('id')
		}).done(function(xhr) {

			if (xhr && xhr.success) {

				$('textarea.editor', tpl).each(function(i,el) {
					var editor = $(el).attr('id');
					if (tinymce.get(editor)) tinymce.remove('#'+editor);
				})

				tpl.remove();
			}
		})

		return false;
	})

	// change article field ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$(document).on('change', '.article-field', function(e) { 

		var self = $(this);
		var value = self.val();

		if (self.is(':checkbox')) {
			value = self.is(':checked') ? 1 : 0;
		}

		if (self.data('validate') && !Article.validate(self) ) {
			return false;
		}

		Adomat.Ajax.post(URL, {
			application: 'news',
			section: 'article-field-change',
			id: $article.val(),
			field: self.attr('name'),
			value: value
		}).done(function(xhr) {
			
			if (xhr && xhr.success) {

				if ( self.data('caption')) {
					self.trigger('update-caption');
				}

				if (self.hasClass('selectpicker')) {
					self.selectpicker('refresh');
				}

				if (xhr.data) {
					for(var field in xhr.data) {
						Article.updateField(field, xhr.data[field]);
					}
				}
			}
		}) 
	})

	// change article template field :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$(document).on('change', '.template-field', function(e) { 

		var self = $(this);
		var tpl = self.closest('.news-article-template-container');
		var value = self.val();

		if (self.is(':checkbox')) {
			value = self.is(':checked') ? 1 : 0;
		}

		if (self.data('validate') && !Article.validate(self) ) {
			return false;
		}

		Adomat.Ajax.post(URL, {
			application: 'news',
			section: 'article-tpl-field-change',
			id: $article.val(),
			template: tpl.data('id'),
			field: self.data('name') || self.attr('name'),
			value: value
		}).done(function(xhr) {
			
			if (xhr && xhr.success) {

				if ( self.data('caption')) {
					self.trigger('update-caption');
				}

				if (self.hasClass('selectpicker')) {
					self.selectpicker('refresh');
				}
			}
		}) 
	})

	// update field placeholder targets ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$('[data-caption]').on('update-caption', function() {

		var self = $(this);
		var field = self.prop('tagName');
		var target = $(self.data('caption'));
		var value = self.val();

		if (field=='INPUT' || field=='TEXTAREA') {
			target.text(value);
		}

		if (field=='SELECT') { 
			var caption = $('option:selected', self).text();
			target.text(caption);
		}
	});


	// remove template file :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$(document).on('click', '.tpl-file-remover', function(e) {

		var self = $(this);
		var tpl = $(this).closest('.news-article-template-container');
		var file = self.data('file');

		if (file) {
			Adomat.Ajax.post(URL, {
				application: 'news',
				section: 'remove-tpl-files',
				id: $article.val(),
				template: tpl.data('id'),
				file: file
			}).done(function(xhr) {
				if (xhr && xhr.success) {
					self.parent().remove();
				}
			})

		} else Adomat.Notification.error('The file is not defined.');
	})



	// article reject ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var rejectModal = new BootstrapDialog({
		title: 'Comment',
		message: $('<div />').load('/applications/templates/news/article/reject.form.php'),
		autodestroy: false,
		buttons: [
			{
				label: 'Cancel',
				cssClass: 'btn-default pull-left text-mutted',
				action: function(dialogItself){
					rejectModal.close();
				}
			},
			{
				id: 'btnAddComment',
				cssClass: 'btn-primary',
				icon: 'fa fa-thumbs-down',
				label: 'Reject',
				action: function(dialog) {
					
					var $button = this;
					var $content = dialog.getModalBody();

					// trigger required fields
					$content.find('textarea[required]').trigger('change');

					if ($content.find('.has-error').length) {
						Adomat.Notification.error('Red marked fields are mandatory');
					} else {

						dialog.enableButtons(false);
						dialog.setClosable(false);
						$button.toggleSpin(true);

						var data = $content.find('form').serializeArray();
						data.push({name: 'application', value: 'news'});
						data.push({name: 'id', value: $article.val()});

						Article.submitAction($('#btnReject'), data, function() {
							dialog.setClosable(true);
							dialog.enableButtons(true);
							$button.toggleSpin(false);
							dialog.close();
						});
					}
				}
			}
		],
		onhidden: function(dialog){
			dialog.getModalBody().find('form').trigger("reset");
		}
	})


	// bind template events ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	function iniTemplate(tpl, data) {

		var id = tpl.prop('id');
		var data = data || {};
		var tplHandsonTable = $('.table-handsontable', tpl);

		// select pickers
		$('select.selectpicker').each(function(i,e) {
			
			$(e).selectpicker('refresh');
			
			if ($(e).is(':disabled')) {
				$(e).closest('.dropdown-selectpicker').addClass('disabled');
			}
		})

		// image cropers
		$('input.field-crop', tpl).each(function(i,e) {
			
			var self = $(e);
			var value = self.val();

			if (self.is(':disabled')) {
				if (value) {
					var img = $('<img />').addClass('img-responsive').attr('src', value);
					$('.image-cropper', tpl).append(img);
				}
			} else {
				var field = self.attr('id');
				$croppers[field] = new Cropper(field);
				$croppers[field].loadImage(value);
			}
		})

		// datapickers
		$('input.datapicker', tpl).each(function(i,e) {
			$(e).unbind('change');
			$(e).bootstrapDP('update', Date.parse(value, "dd.MM.yyyy")); 
			$(e).bind('change');
		})

		// wysiwyg editors
		$('textarea.editor', tpl).each(function(i,e) {
			var editor = $(e).attr('id');
			addEditor(editor);
		})

		// ajax file uploaders
		$('input.tpl-files', tpl).each(function(i,e) { 
			var file = $(e).attr('id');
			addFileUploader(file);
		})

		if (tplHandsonTable.length > 0) {

			//Handsontable.Dom.addClass('table table-hover table-striped');
			
			var settings = $.extend({
				startRows: 2,
				startCols: 2,
				minSpareCols: 0,
				minSpareRows: 1,
				rowHeaders: false,
				colHeaders: true,
				manualColumnResize: true,
				manualRowResize: true,
				contextMenu: true
			}, data.settings);

			// table builders
			$tables[id] = new Handsontable(tplHandsonTable[0], settings);
			
			if (data.sources) { 
				$tables[id].loadData(data.sources);
				$tables[id].render();
			}

			// on change event
			$tables[id].updateSettings({
				afterChange: function(changes, action) {
					if (action=='edit') {
						for (var i in changes) {
							Adomat.Ajax.post(URL, {
								application: 'news',
								section: 'template-table-source',
								id: $article.val(),
								tpl: tpl.data('id'),
								row: changes[i][0],
								col: changes[i][1],
								val: changes[i][3],
							})
						}
					}
				}
			})

			$tables[id].updateSettings({
				afterGetColHeader: function (col, TH) {

					if (col == -1) {
					    return;
					}
					var instance = this;
					// create input element
					var input = document.createElement('input');
					    input.type = 'text';
					    input.className = 'tbl-header form-control';
					    input.setAttribute("data-col", col);
					    input.setAttribute("data-tpl", tpl.data('id'));
					    input.value = TH.firstChild.textContent;

					TH.appendChild(input);

					Handsontable.Dom.addEvent(input, 'change', function (e){
					    var headers = instance.getColHeader();
					        headers[col] = input.value;
					    instance.updateSettings({
					        colHeaders: headers
					    });
					});

					TH.style.position = 'relative';
					TH.firstChild.style.display = 'none';
				}
			})
		}

		// refresh template orders
		$templatesContainer.sortable( "refresh" );
	}


	// croper instance :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	function Cropper(el) {

		var file = $('#'+el);
		var tpl = file.closest('.news-article-template-container')
		var container = $('.image-cropper', tpl);

		return new Croppic(container.attr('id'), {
			uploadData: {
				path: '/data/news/articles/'+$article.val()+'/'+tpl.data('id'),
				filename: file.data('filename') || '',
				minWidth: 320,
				minHeight: 240
			},
			cropData: {
				path: '/data/news/articles/'+$article.val()+'/'+tpl.data('id'),
				filename: file.data('filename') || ''
			},
			onAfterImgUpload: function(self, response) {
				
				response = response || {};
				
				if (response.success && response.url) {
					file.val(response.url).trigger('change');
				}

				if (response.error) {
					Adomat.Notification.error(response.error);
				}
			},
			onAfterImgCrop: function(self, response) {
				
				response = response || {};
				
				if (response.success && response.url) {
					file.val(response.url).trigger('change');
				}

				if (response.error) {
					Adomat.Notification.error(response.error);
				}
			}
		})
	}

	// wysiwyg editor instance :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	
	var addEditor = function(editor, config) {
		if (!tinymce.get(editor)) {
			tinymce.EditorManager.execCommand('mceAddEditor', true, editor);
		}
	}
	

	// file uploader instance ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	var addFileUploader = function(file) {

		var self = $('#'+file);
		var tpl = self.closest('.news-article-template-container')
		var extensions = $('input[name=extensions]', tpl).val();
		var path = '/data/news/articles/'+$article.val()+'/'+tpl.data('id')+'/';
	
		// file type extensions
		if (extensions) {
			var exts = extensions.split(/[\s,]+/);
			extensions = new RegExp('(\.|\/)('+exts.join('|')+')$');
		}

		self.fileupload({
			url: URL,
			dataType: 'json',
			formData: {
				application: 'news',
				section: 'add-tpl-files',
				id: $article.val(),
				template: tpl.data('id'),
				path: path
			},
			acceptFileTypes: extensions,
			submit: function (e) {
				$('.progress', tpl).show(500);
			},
			done: function (e, data) {

				setTimeout(function() { 
					$('.progress', tpl).hide(500);
				}, 2000);
				
				$.each(data.result.files, function (index, file) {
					fileRender(tpl, path+file.name);
				})
			},
			progressall: function (e, data) {
				var progress = parseInt(data.loaded / data.total * 100, 10);
				$('.progress-bar', tpl).css('width', progress + '%');
			}
		})
		.prop('disabled', !$.support.fileInput)
		.parent().addClass($.support.fileInput ? undefined : 'disabled');
	}


	// utilities :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	function fileRender(tpl, file) {

		var self = $('input.tpl-files', tpl);
		var render = self.data('render') || '';
		
		switch (render) {

			case 'images':
				var div = $('<div />').addClass('tpl-file-img');
				var img = $('<img />').addClass('img-responsive').attr('src', file);
				div.append('<a href="#" class="template-control template-control tpl-file-remover" data-file="'+file+'" ><i class="fa fa-times-circle"></i></a>');
				div.append(img);
				$('.files', tpl).append(div);
			break;

			default:
				var span = $('<span />');
				span.addClass('label label-default').html('<a href='+file+' target="_blank" >'+basename(file)+'</a>')
				span.prepend('<i class="fa fa-times-circle tpl-file-remover" data-file="'+file+'" ></i>');
				$('.files', tpl).append(span);
			break;
		}
	}

	function basename(path) {
		return path.split('/').reverse()[0];
	}



	// remove empty articles or set article title on unload ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var myBeforeUnload = window.attachEvent || window.addEventListener;
	var chkevent = window.attachEvent ? 'onbeforeunload' : 'beforeunload';

	myBeforeUnload(chkevent, function(e) {
		
		var newsTitle = $('#news_article_title');

		if (newsTitle.is('input') && !newsTitle.val()) { 			
			Adomat.Ajax.json('/applications/modules/news/article/unload.php', {
				application: 'news',
				id: $article.val()
			})
		}

		if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
			var e = e || window.event;
			if (e) e.returnValue = ''; // For IE and Firefox
			return '';
		}
	})

})