
$(document).ready(function() {
	
	
	
	// user list
	$('.modal-userform').click(function(event) {
		event.preventDefault();
		$.fancybox.showActivity();
		$('#mail_modal').removeClass('close-account');
		modal('#user_modal_form')
		return false;
	});

	// event: focus
	$('input:visible,select:visible,textarea:visible', $('.mail-template-modal')).click(function() {		
		$(this).addClass('onFocus').removeClass('onBlur');
	});
	
	// event: blur
	$('input:visible,select:visible,textarea:visible', $('.mail-template-modal')).blur(function() {		
		$(this).removeClass('onFocus').addClass('onBlur');
	});
	
	// event: change
	$('input:visible,select:visible,textarea:visible', $('.mail-template-modal')).change(function() {		
		$(this).toggleClass('error', ($(this).hasClass('required') && !$(this).val()) ? true : false );
		$(this).removeClass('onFocus').addClass('onBlur');
	});
	
	
	$('#mail_form').submit(function(event) {
		

		
		event.preventDefault();
		//$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
		
		var id = $('#mail_user').val();
		var user = xhr('/applications/helpers/ajax.user.php?id='+id);
		
		if (user) {

			var data = {
				render: true,
				template: $('#mail_template').val(),
				user_firstname: user.user_firstname,
				user_name: user.user_name
			};
			
			// authenticated user data
			var authenticated =  xhr('/applications/helpers/ajax.user.php?id='+$('#authenticated_user').val());

			if (authenticated) {
				data.sender_firstname = authenticated.user_firstname;
				data.sender_name = authenticated.user_name;
			}
			
			// company data
			var company = xhr('/applications/helpers/ajax.company.php?id='+$('#company').val());

			if (company) {
				data.company_name=company.address_company;
			}
			
			// role responsipble data
			var recipient = xhr('/applications/helpers/ajax.user.php', {
				id: id, 
				section: 'role-responsibile'
			});

			if (recipient) {
				data.recipient_firstname = recipient.user_firstname;
				data.recipient_name = recipient.user_name;
			}
	
			
			

			// mail template data
			var mail = xhr('/applications/helpers/ajax.mailtemplate.php', data);

			if (mail) {

				if (mail.mail_template_id) {

					$('.subtitle', $('#mail_modal')).html(user.user_firstname + ', ' + user.user_name);
					$('#mail_subject').val(mail.mail_template_subject);
					$('#mail_content').text(mail.mail_template_text);

					if (mail.mail_template_mode) modal('#mail_modal');
					else $('#mail_apply').trigger('click');
					
				} else {
					$.jGrowl('Error: E-Mail template not found. ', { 
						sticky: true, 
						theme: 'error'
					});
				}
				
			}
			
		} else {
			$.jGrowl('Error: User not found. ', { 
				sticky: false, 
				theme: 'error'
			});
		}
		
		$.fancybox.hideActivity();
		
		return false;
	});

	$('.cancel').click(function(event) {
		event.preventDefault();
		$.fancybox.close();
		return false;
	});	

	$('#mail_apply').click(function(event) {

		event.preventDefault();
		$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');

		$('.required:visible', $('#mail_modal')).trigger('change');

		var errors = $('.error', $('#mail_modal')).length ? true : false;
		
		if (errors) {
			$.jGrowl('Red marked Fields are required.', { 
				sticky: true, 
				theme:'error'
			});
		} else {
			
			retailnet.loader.show();
			
			var action = $('#mail_form').attr('action');
			var data = $('#mail_form').serialize();
			
			request = xhr(action, data);

			if (request) {

				retailnet.loader.hide();

				if (request.response && request.redirect) {
					window.location=request.redirect;
				} 

				if (request.message) {
					$.jGrowl(request.message, {
						life: 5000, 
						sticky: (request.response) ? false : true,
						theme: (request.response) ? 'message' : 'error'
					});
				}
			}

			$.fancybox.close();
		}

		return false;
	});
});