$(document).ready(function() {

	$(document).on('click', 'a.download-zip', function(e) {

		e.preventDefault();

		var self = $(this);
		var row = self.closest('tr');

		retailnet.ajax.json(self.attr('href')).done(function(xhr) {
			
			xhr =  xhr || {};

			if (xhr.success && xhr.file) {
				$('td.downloaded_on', row).text(xhr.date);
				$('td.downloaded_from', row).text(xhr.user);
				window.location=xhr.file;
			}

			if (xhr.message) retailnet.notification.success(xhr.message);
			if (xhr.error) retailnet.notification.error(xhr.error);
		});

		return false;
	});

});