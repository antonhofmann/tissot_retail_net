$(document).ready(function() {
	
	var form = $('form.request'),
		selected = $('#selected'),
		remaindMailTemplate = $('#reminder-mail-template'),
		previewMailTemplate = $('#preview-mail-template'),
		testMailTemplate = $('#test-mail-template');
	
// load clients ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var clientsList = $('#list').tableLoader({
		url: '/applications/modules/pos/verification/consolidation/reminder.list.php',
		filters: form,
		after: function(self) {

			// order sheet box
			$('input.client').click(function() {
				processCheckBoxes();
			});

			// check all order sheets
			$('.checkall').click(function() {
				$('input.client').attr('checked', $(this).is(':checked') ? true : false);
				processCheckBoxes();
			});
		}
	});

// wysiwyg :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	tinymce.init({
		selector: "#mail_template_text",
		plugins: ["advlist autolink lists textcolor link visualblocks code nonbreaking contextmenu paste preview"],
		width: '100%',
		height: 400,
		menubar : false,
		statusbar : false,
		toolbar_items_size: 'small',
		autosave_restore_when_empty: false,
		toolbar: "fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor | removeformat | paste pastetext pasteword",
		setup : function(ed) {
			ed.on('change', function(e) {
				$('#mail_template_text').val(ed.getContent())
			});
		}
	});	
	

// remaind mail ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var modalMailRemaind = remaindMailTemplate.adoModal({
		width: 800,
		height: 700,
		clickClose: false
	});

	// button remind
	$('#remind').click(function(e) {
		
		e.preventDefault();
		
		if ($('#mail_template_view_modal').val()==1) modalMailRemaind.show();
		else $('form', remaindMailTemplate).submit();

		return false;
	});

	// button: send mail
	$('.sendmail', remaindMailTemplate).on('click', function(e) {

		e.preventDefault();
		
		var self = $(this),
			required = $('.required', remaindMailTemplate),
			failures = $('.required[value=]', remaindMailTemplate);

    	if (required.length > 0 && failures.length > 0) {
    		$('.required', remaindMailTemplate).removeClass('error');
    		$('.required[value=]', remaindMailTemplate).addClass('error');
    		retailnet.notification.error('Red marked fields are required.');
    	} else {
    		modalMailRemaind.close();
			$('form', remaindMailTemplate).submit();
    	}

    	return false;
	});
	
	
	// submit mail form
	$('form', remaindMailTemplate).submit(function(e) {

		e.preventDefault();

		var self = $(this);
		var data = $('form.request, #reminder-mail-template form').serialize();
		
		retailnet.loader.show();

    	retailnet.ajax.json(self.attr('action'), data).done(function(xhr) {

    		xhr = xhr || {};

    		if (xhr.reload) {
				window.location.reload();
        	}
    		
			if (xhr.notification) {
				if (xhr.response) retailnet.notification.success(xhr.notification);
				else retailnet.notification.error(xhr.notification);
			}
			
        }).complete(function() {
        	retailnet.loader.hide();
        });


		return false;
	});


// preview mail ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var modalMailPreview = previewMailTemplate.adoModal({
		width: 1000,
		height: 600,
		overlay: {
			opacity: 0.75,
			background: '#000'
		}
	});

	$('.preview-mail', remaindMailTemplate).on('click', function(e) {

		e.preventDefault();
		
		var self = $(this);
		var url = self.attr('href');
		var data = $('form.request, #reminder-mail-template form').serialize();
		
		retailnet.loader.show();
			
		retailnet.ajax.json(url,data).done(function(xhr) {

			retailnet.loader.hide();

			if (xhr && xhr.response) {

				// assign data from request
				$('.mail-subject', previewMailTemplate).html(xhr.subject);
				$('.mail-address', previewMailTemplate).text(xhr.email);
				$('.mail-cc-address', previewMailTemplate).text(xhr.cc);
				$('.mail-cc-address', previewMailTemplate).parent().toggle(xhr.cc ? true : false);
				$('.mail-date', previewMailTemplate).text(xhr.date);
				$('.mail-content', previewMailTemplate).html(xhr.content);
				$('.mail-footer', previewMailTemplate).html(xhr.footer);
				$('.mail-footer', previewMailTemplate).parent().toggle(xhr.footer ? true : false);

				modalMailPreview.resize().show();

			} else {
				modalMailPreview.close();
			}

			if (xhr && xhr.notification) {
				if (xhr.response) retailnet.notification.success(xhr.notification);
				else retailnet.notification.error(xhr.notification);
			}
		});

	});

// test mail :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var modalTestMail = testMailTemplate.adoModal({
		width: 400
	});

	$('.test-mail', remaindMailTemplate).on('click', function(e) {
		e.preventDefault();
		modalTestMail.show();
	});

	$('.apply', testMailTemplate).on('click', function(e) {
		
		e.preventDefault();

		var url = $(this).attr('href');
		var data = $('form.request, #reminder-mail-template form').serializeArray();
		var recipient = $('#test-mail-recipient').val();
		
		if (recipient) {

			data.push({
				name: 'test_recipient', 
				value: recipient
			});
			
			retailnet.loader.show();
			
			retailnet.ajax.json(url,data).done(function(xhr) {

				if (xhr && xhr.response) {
					modalTestMail.close();
				}

				if (xhr && xhr.notification) {
					if (xhr.response) retailnet.notification.success(xhr.notification);
					else retailnet.notification.error(xhr.notification);
				}

			}).complete(function() {
				retailnet.loader.hide();
			});
			
		} else {
			retailnet.notification.error('Recipient e-mail is required.');
		}
	});


// processing ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


	var processCheckBoxes = function() {

		var checkbox = $('input.client'),
			data = selected.val().split(',') || [],
			value, index, checked; 
		
		$.each(checkbox, function() {
			value = $(this).attr('value');
			index = $.inArray(value, data);
			checked = $(this).is(':checked');
			if (checked) data.push(value);
			else if (index>-1) data.splice(index, 1);
		});
		
		data = $.grep(data, function(n) { return (n) });
		data = $.grep(data, function(v,k){ return $.inArray(v,data) === k });
		
		selected.val(data.length ? data.join(',') : '');

		checked = checkbox.length==$('input.client:checked').length ? 'checked' : false;
		$('.checkall').attr('checked', checked);
	}

});