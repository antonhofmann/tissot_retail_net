$(document).ready(function() {

	var buttonExport = $('#export'),
		selectedItems = $('#selected_items'),
		SalesOrdersExportList = $('#sales-orders-export-list'),
		mastersheet = $('#mastersheet').val();


	retailnet.loader.init();

// loaders :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var MastersheetItems = $('#itemlist').tableLoader({
		url: '/applications/modules/mastersheet/salesorders/items.list.php',
		filters: $('form.request'),
		after: function(self) {

			$('select',self).dropdown();

			// items checkboxes
			var items = $('input.item', self);
			
			if (items.length) {

				buttonExport.show();

				// order sheet box
				$('input.checkall', self).on('click', function(e) {
					var table = $(this).closest('table');
					var checked = $(this).is(':checked') ? 'checked' : false;
					$('input[type=checkbox]', table).attr('checked', checked);
				});
				
			} else {
				buttonExport.hide();
			}
		}
	});

// modal export list :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


	var ModalExportItems = $('#export-modal-box').adoModal({
		width: 1400,
		height: 760,
		clickClose: false
	});

	$('.submit-export').on('click', function(e) {

		e.preventDefault();

		// close all notifications
		retailnet.notification.hide();

		$("input.required").filter(function() {
			return !this.value;
		}).addClass("error");

		var errors = SalesOrdersExportList.find('.error').length; 

		if (errors > 0) {
			
			retailnet.notification.error('Check red marked fields.');

			if ($('.date.required.error', SalesOrdersExportList).length > 0) {
				retailnet.notification.error('Enter all Delivery Dates.The Delivery Dates can be also entered  bei Master Sheet Items Tab.');
			}

			return;
		}

		ModalExportItems.close();
		retailnet.loader.show();

		var url = SalesOrdersExportList.attr('action');
		var data = SalesOrdersExportList.serializeArray();

		retailnet.ajax.json(url, data).done(function(xhr) {

			xhr = xhr || {};

			retailnet.loader.hide();

			if (xhr.success) {
				window.location.reload();
			}

			if (xhr.message) {
				if (xhr.response) retailnet.notification.success(xhr.message); 
				else retailnet.notification.error(xhr.message); 
			}
		});
	});
	

// button actions ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	buttonExport.on('click', function(e) {

		e.preventDefault();

		// close all notifications
		retailnet.notification.hide();

		var self = $(this);
		var checkedItems = $('input.item:checked');

		if (checkedItems.length > 0 || $('input.locally_provided').length > 0) {

			retailnet.loader.show();

			var data = $('form.request, input.item:checked').serializeArray();

			// load sales order items
			retailnet.ajax.json('/applications/modules/mastersheet/salesorders/load.items.php', data).done(function(xhr) {

				xhr = xhr || {};

				// assign content to modal box
				SalesOrdersExportList.html(xhr.content);

				// datepicker
				$('input.date').datepicker({ 
					dateFormat: 'dd.mm.yy',
					showOtherMonths : true,
					firstDay: 1,
					onClose: function(dateText, inst) { 
					
						var error = (!dateText || !isValidDate(dateText)) ? true : false;
						$(this).removeClass('error').toggleClass('error', error);
						
						if (!error) {
							
							var emptyDateFields = $('input.date[value=""]');
							
							emptyDateFields.each(function(i,e) {
								if (!$(e).val()) $(e).val(dateText).removeClass('error');
							});
						}
					}
				});

				$('input.switch-input').on('change', function() {
					
					if ($(this).is(':checked')) {
						var sw = $(this).parent();
						var table = sw.closest('h5').next();

						var dropdowns = new Array();

						$('tr td', table).each(function () {
							if($(this).children('select').attr('name')) {
								dropdowns.push($(this).children('select').attr('name'));
							}
							
						});

						
						var counter = 0;	
						$('tr td', table).each(function () {
							
							if($(this).attr("data_address_id")) {
								client = $(this).attr('data_address_id');
								$.ajax({
									type: 'POST',
									headers: {
										"cache-control": "no-cache"
									},
									url: "/applications/helpers/ajax.mps_sap_hq_ordertypes.php",
									async: false,
									cache: false,
									data: {
										address_id: client,
										freegoods: 1
									},
									success: function (data) {

										if(data != '') {
											$.each(data, function(index) {
												var new_cutomernumber = data[index].mps_sap_hq_ordertype_customer_number;
												var new_shiptonumber = data[index].mps_sap_hq_ordertype_shipto_number;
												var new_ordertype = data[index].sap_hq_order_type_name;
												var new_orderreason = data[index].sap_hq_order_reason_name;
												
												var field = 'select[name="' + dropdowns[counter] + '"';
												$(field).val(new_cutomernumber);

												var field = 'select[name="' + dropdowns[counter+1] + '"';
												$(field).val(new_shiptonumber);

												var field = 'select[name="' + dropdowns[counter+2] + '"';
												$(field).val(new_ordertype);

												var field = 'select[name="' + dropdowns[counter+3] + '"';
												$(field).val(new_orderreason);

												counter = counter + 5;
												
											});
										}
										else {
											counter = counter + 5;
										}
									}
								});
							 }
						});
					}
					else{
						var sw = $(this).parent();
						var table = sw.closest('h5').next();

						var dropdowns = new Array();

						$('tr td', table).each(function () {
							if($(this).children('select').attr('name')) {
								dropdowns.push($(this).children('select').attr('name'));
							}
							
						});

						
						var counter = 0;	
						$('tr td', table).each(function () {
							
							if($(this).attr("data_address_id")) {
								client = $(this).attr('data_address_id');
								$.ajax({
									type: 'POST',
									headers: {
										"cache-control": "no-cache"
									},
									url: "/applications/helpers/ajax.mps_sap_hq_ordertypes.php",
									async: false,
									cache: false,
									data: {
										address_id: client,
										default: 1
									},
									success: function (data) {

										if(data != '') {
											$.each(data, function(index) {
												var new_cutomernumber = data[index].mps_sap_hq_ordertype_customer_number;
												var new_shiptonumber = data[index].mps_sap_hq_ordertype_shipto_number;
												var new_ordertype = data[index].sap_hq_order_type_name;
												var new_orderreason = data[index].sap_hq_order_reason_name;
												
												var field = 'select[name="' + dropdowns[counter] + '"';
												$(field).val(new_cutomernumber);

												var field = 'select[name="' + dropdowns[counter+1] + '"';
												$(field).val(new_shiptonumber);

												var field = 'select[name="' + dropdowns[counter+2] + '"';
												$(field).val(new_ordertype);

												var field = 'select[name="' + dropdowns[counter+3] + '"';
												$(field).val(new_orderreason);

												counter = counter + 5;
												
											});
										}
										else {
											counter = counter + 5;
										}
									}
								});
							 }
						});
					}
				});

				if (xhr.notification) {
					if (xhr.response) retailnet.notification.success(xhr.notification); 
					else retailnet.notification.error(xhr.notification); 
				}

				ModalExportItems.resize().show();

				retailnet.loader.hide();
			});

		} else {
			retailnet.notification.error('Please, select at least one item for export.');
		}
	});

});


function change_delivery_details(fieldnumber, client) {

	var field = 'select[name="customernumber[' + fieldnumber + ']"';
	var cutomernumber = $(field).val();

	var new_shiptonumber = '';
	var new_ordertype = '';
	var new_orderreason = '';
	
	$.ajax({
        type: 'POST',
        headers: {
            "cache-control": "no-cache"
        },
        url: "/applications/helpers/ajax.mps_sap_hq_ordertypes.php",
        async: false,
        cache: false,
        data: {
            address_id: client
        },
        success: function (data) {
			if(data != '') {
				$.each(data, function(index) {
					if(cutomernumber == data[index].mps_sap_hq_ordertype_customer_number) {
						var new_shiptonumber = data[index].mps_sap_hq_ordertype_shipto_number;
						var new_ordertype = data[index].sap_hq_order_type_name;
						var new_orderreason = data[index].sap_hq_order_reason_name;

						var field = 'select[name="shiptonumber[' + fieldnumber + ']"';
						$(field).val(new_shiptonumber);

						var field = 'select[name="sap_order_types[' + fieldnumber + ']"';
						$(field).val(new_ordertype);

						var field = 'select[name="sap_order_reasons[' + fieldnumber + ']"';
						$(field).val(new_orderreason);
					}
				});
			}
		}
    });

}		