function sum(list) {
	var sum = 0;
	for (var i = 0; i < list.length; i++) { sum += list[i]; }
	return sum;
}

$(document).ready(function() {
	
	var spreadsheet,
		formFilters = $('form.filters'),
		dialogResetTemplate = $('#reset_verification_dialog');
	
	var options = {
		readonly: true,
		autoHeight: false,
		fixedPartsNotSelectable: true,
		hiddenAsNull: true,
		fixTop: 3,
		fixLeft: 1
	}
	
	// english named months
	Date.prototype.monthNames = [
		"January", "February", "March","April","May", "June",
		"July", "August", "September","October", "November", "December"
	];

	// get month name
	Date.prototype.getMonthName = function() {
		var i = this.getMonth();
		return this.monthNames[i];
	};
	
	// dropdown
	$('.selectbox').dropdown();

	// dropdown on change
	$('select.submit').change(function(event) {
		
		var self = $(this).closest('.dropdown-placeholder');
		var caption = $('option:selected', $(this)).text();
		
		$('span.label', self).text(caption);
		
		formFilters.submit();
	});
	
	// window on resize
	$(window).resize(function() {
		var elem = $('#spreadsheet');
		var elemOffset = elem.offset(); 
		var winHeight = $(window).height();
		var height = winHeight-elemOffset.top;
		elem.height(height);
	}).trigger('resize');


	// spreadsheet :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	formFilters.submit(function(e) {
		
		e.preventDefault();
		
		var state = $('#states').val();
		var data = $('form.request, form.filters').serialize();
		
		// hide remind button for comleted states
		$('#remind').toggleClass('hidden', state==1 ? true : false);
		
		// show wait screen
		retailnet.loader.show();
		
		// load spreadsheet data
		retailnet.ajax.json('/applications/modules/pos/verification/consolidation/data.load.php', data).done(function(xhr) {
			
			var grid = (xhr && xhr.data) ? retailnet.json.normalizer(xhr.data) : {};
			
			if (xhr && xhr.top) {
				options.fixTop = xhr.top;
			}
			
			if (xhr && xhr.left) {
				options.fixLeft = xhr.left;
			}
			
			if (xhr && xhr.merge) {
				options.merge = eval(xhr.merge);
			}
			
			spreadsheet = new Spreadsheet('#spreadsheet', grid, options);
			spreadsheet.focus();
			
		}).complete(function() {
			retailnet.loader.hide();
		});

		
		return false;
	
	}).trigger('submit');
	

	
	// reset verification dialog :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	/*
	var dialogResetVerification = dialogResetTemplate.adoModal({
		width: 400,
		button: '#reset_verification'
	});
	*/

	// button reset verifications
	$('#reset_verification').on('click', function(e) {
		
		e.preventDefault();

		if ($(this).hasClass('dialog')) {
			retailnet.modal.dialog('#reset_verification_dialog');
		} else {
			$('#reset_verification_apply').trigger('click');
		}
		
		return false;
	});
	
	// reset verification
	$('.apply', dialogResetTemplate).on('click', function(e) {
		
		e.preventDefault();
		
		var url = $('#reset_verification').attr('href');
		var data = $('form.request, form.filters').serialize();
		
		// show wait screen
		retailnet.loader.show();

		//dialogResetVerification.close();
		retailnet.modal.close();
		
		retailnet.ajax.json(url, data).done(function(xhr) {
			
			if (xhr && xhr.reload) {
				window.location.reload();
        	}
			
			if (xhr && xhr.notification) {
				retailnet.notification.show(xhr.notification.content, eval(xhr.notification.properties));
			}
			
			formFilters.submit();
			
		}).complete(function() {
        	retailnet.loader.hide();
		});
		
		return false;
	})


	// print dialog ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var printDialogContainer = $('#printDialog');
	
	var modalPrintDialog = printDialogContainer.adoModal({
		width: 400
	});

	$('.print').on('click', function(e) {
		e.preventDefault();
		$('.submit-print', printDialogContainer).prop('href', $(this).prop('href'));
		modalPrintDialog.show();
		return;
	});

	$('.submit-print', printDialogContainer).on('click', function(e) {
		
		e.preventDefault();
		e.stopPropagation();

		retailnet.notification.hide();

		var self = $(this);
		var url = self.prop('href');
		var data = $('form.request, form.filters, #printDialog form').serializeArray();

		if (!$('input.option:checked', printDialogContainer).length) {
			retailnet.notification.error('Please select at least one print option');
			return;
		}

		if (!url) {
			retailnet.notification.error('Action URL is not defined. Contact system administrator');
			return;
		}
		
		retailnet.loader.show();
		modalPrintDialog.close();
		
		retailnet.ajax.json(url, data).done(function(xhr) {

			if (xhr && xhr.success && xhr.file) {
				window.open(xhr.file);
			}

			if (xhr && xhr.errors) {
				$.each(xhr.errors, function(i, message) {
					retailnet.notification.error(message);
				})
			}

		}).complete(function() {
			retailnet.loader.hide();
		})
	})

});