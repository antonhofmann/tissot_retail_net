define(function(require, exports, module) {

var model = require('models/model'),
    PhaseCollection = require('apps/admin/collections/masterplan_templates/phases');

var Masterplan = model.extend({
  url: '/administration/masterplantemplates/sync',
  // use function or object will be copied to other collections
  defaults: {
    phases: null
  },

  /**
   * Setup phases collection when data is passed in
   * @param  {object} options
   */
  initialize: function(options) {
    if (options.phases !== undefined) {
      this.set('phases', new PhaseCollection(options.phases));
    }
  }
});

return Masterplan;
});
