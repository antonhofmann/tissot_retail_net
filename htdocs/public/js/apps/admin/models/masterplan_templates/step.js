define(function(require, exports, module) {

var BaseModel = require('models/model');

var Step = BaseModel.extend({
  urlRoot: '/administration/masterplantemplates/step',

  defaults: {
    apply_corporate: 1,
    apply_franchisee: 1,
    apply_other: 1,
    apply_store: 1,
    apply_sis: 1,
    apply_kiosk: 1,
    apply_independent: 1,
    is_active: 1,
    step_type: 'milestone'
  },

  // the user that should be selected by default when selecting a responsible user
  defaultResponsibleUser: 183,

  /**
   * Setup model with values of last model if it's new
   * @param {object} options
   */
  initialize: function(options) {
    // if this is a new step, get it's values from the model of the last step in collection
    if (this.isNew()) {
      var previousModel = this.collection.last() || false;
      if (previousModel) {
        // we don't want to copy all the values
        this.set(previousModel.omit(['id', 'step_nr', 'title', 'value']));
      }
    }
  },

  /**
   * Get the responsible user
   * Use this instead of step.get('responsible_user_id') because we have a fallback handling
   * @return {integer}
   */
  getResponsibleUser: function() {
    var user = this.get('responsible_user_id');
    // fallback to default user
    if (user == undefined) {
      user = this.defaultResponsibleUser;
    }
    return user;
  }
});

return Step;

});
