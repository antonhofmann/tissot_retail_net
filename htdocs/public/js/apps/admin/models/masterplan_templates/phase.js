define(function(require, exports, module) {

var BaseModel = require('models/model'),
    _ = require('underscore'),
    StepCollection = require('apps/admin/collections/masterplan_templates/steps');

var Phase = BaseModel.extend({
  defaults: {
    steps: undefined,
    parent_id: 0,
    pos: 0
  },

  /**
   * Setup steps collection when data is passed in
   * @param  {object} options
   */
  initialize: function(options) {
    // set steps to new StepCollection
    this.set('steps', new StepCollection(options.steps || null));
    // set parent_id to 0 if it's not defined
    if (options.parent_id !== undefined) {
      if (_.isNull(options.parent_id)) {
        this.set('parent_id', this.defaults.parent_id);
      }
    }
  }
});

return Phase;
});
