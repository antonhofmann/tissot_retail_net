define(function(require, exports, module) {

var BaseCollection = require('collections/collection'),
    Step = require('apps/admin/models/masterplan_templates/step');

var StepCollection = BaseCollection.extend({
  model: Step,
  comparator: 'pos'
});

return StepCollection;

});
