define(function(require, exports, module) {

var BaseCollection = require('collections/collection'),
    Phase = require('apps/admin/models/masterplan_templates/phase');

var PhaseCollection = BaseCollection.extend({
  model: Phase
});

return PhaseCollection;

});
