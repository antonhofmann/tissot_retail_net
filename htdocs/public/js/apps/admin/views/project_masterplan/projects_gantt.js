/**
 * Represents a gantt chart of the project masterplan
 */
define(function(require, exports, module) {

  var $ = require('jquery'),
      helpers = require('jquery.helpers'),
      Backbone = require('backbone'),
      _ = require('underscore'),
      GanttView = require('apps/admin/views/project_masterplan/gantt')
      ;

  /**
   * Handles the masterplan gantt chart over all projects
   * Extends the regular gantt chart with some specific behavior
   */
  var ProjectsMasterplanGanttView = GanttView.extend({
    /**
     * Parse steps
     * options.steps      Array of step data
     * options.projectID  integer Project ID
     * @param {object} options
     */
    initialize: function(options) {
      this.projectID = options.projectID;
      this.options.showFilteredOutSteps = false;

      this.earliestDate = new Date();
      this.latestDate = new Date(0);
      //console.table(options.steps, ['title', 'phase', 'start', 'end', 'type', 'state']);

      this.$el.addClass('view-all-phases');

      _.each(options.steps, _.bind(function(step) {
        // parse start date
        step.start = new Date(step.start);
        // save this date if it's the earliest of all dates
        if (step.start < this.earliestDate) {
          this.earliestDate.setTime(step.start.getTime());
        }
        // parse end date
        step.end = new Date(step.end)

        // save this date if it's the latest of all dates
        if (step.end > this.latestDate) {
          this.latestDate.setTime(step.end.getTime()); 
        }

        // add options for tooltip
        step = _.extend(step, this._getTooltipOptions(step));

        this.steps.push(step);
      }, this));
    },

    /**
     * Init kendo gantt chart
     */
    _initGanttChart: function() {
      var that = this;
      this.$gantt.kendoGantt({
        dataSource: this.dataSource,
        editable: false,
        selectable: false,
        height: '100%',
        pdf: {
          landscape: true,
          fileName: this.projectID,
          proxyURL: '/project/projectmasterplan/export',
          forceProxy: true,
          margin: this.PRINT_MARGIN
        },
        tooltip: { 
          visible: true,
          template: '\
            <div class="gantt-tooltip #= task.type # #= task.hueClass #" style="background-color: \\##= task.color # !important;"> \
              <span class="title">#= task.title #</span class="title"> \
              <span class="date-range">#= task.tooltipDate #</span> \
            </div> \
          '
        },
        views: [
          {
            type: 'month',
            slotSize: 56,
            weekHeaderTemplate: "#='wk '+ kendo.toString(start.getWeekNumber())#", // e.g. wg 43
            monthHeaderTemplate: "#= kendo.toString(start, 'MMM') + ' ' + kendo.toString(start, 'yyyy') #" // eg. AUG 2015
          },
          {
            type: 'year',
            slotSize: 50
          }
        ],
        columns: [
          // @todo translate
          { field: "number", title: "Nr.", width: 60, class: 'number' },
          { field: "title", title: "Title", width: 200, class: 'title' },
        ],
        // custom operations on gantt chart once data is bound
        dataBound: function() {
          var gantt = this;
          
          /**
           * transform project number into a link
           */
          gantt.element.find('.k-grid-content tr[data-uid]').each(function() {
            var dataItem = gantt.dataSource.getByUid($(this).attr('data-uid'));
            if (dataItem.type != 'project') {
              return;
            }
            var $link = $('<a href="/user/project_task_center.php?pid=' + dataItem.id + '" target="_blank" />')
              .text(dataItem.number);
            $(this).children('td:eq(0)').empty().append($link);
          });

          /**
           * style timeline tasks
           */
          gantt.element.find('.k-grid-content .k-gantt-tasks .k-task').each(function(i) {
            var $task = $(this),
                dataItem = gantt.dataSource.getByUid($task.attr('data-uid'));

            if (!dataItem.parentId) {
              $task.addClass('project');
            }

            var taskText = dataItem.title;
            that._enhanceTimelineTask($task, dataItem.title);
          });

          // style tree tasks
          gantt.element.find('.k-grid-content table[role=treegrid] tr').each(function() {
            var dataItem = gantt.dataSource.getByUid($(this).attr('data-uid'));
            if (!dataItem.parentId) {
              $(this).addClass('project');
            }

            $(this).css('color', '#' + dataItem.color);
          });
        }
      });

      // get instance of chart
      this.gantt = this.$gantt.data('kendoGantt');

      // if our dates span several years, we'll start off in year view
      var yearDiff = this.latestDate.getFullYear() - this.earliestDate.getFullYear();
      if (yearDiff > 2) {
        this.setView('year');
      }
    },

    /**
     * Set the view type of the gantt chart
     * @param {string} month|year
     */
    setView: function(view) {
      if (!view) {
        return;
      }
      this.view = view;

      // parent class handles month/year view
      if (_.contains(['month', 'year'], view)) {
        ProjectsMasterplanGanttView.__super__.setView.apply(this, arguments);
      }
      // collapse all projects. the fastest way is to create a new datasource
      else if (_.contains(['projects_only', 'all_phases'], view)) {
        var expanded = (view == 'all_phases');
        var steps = _.map(this.steps, function(step) {
          step.expanded = expanded;
          return step;
        });
        this.dataSource = new kendo.data.GanttDataSource({ data: steps });
        this.gantt.setDataSource(this.dataSource);
        this.$el.toggleClass('view-all-phases view-projects-only')
      }
      else {
        this.filterByPhase(view);
      }
    },

    /**
     * Filter gantt chart by a specific phase
     * Also keeps the projects in the chart or we would have to manipulate the steps quite heavily
     * (a phase by itself does not have any project info)
     * @param {string} phase id
     */
    filterByPhase: function(phaseID) {
      this.dataSource.filter({
        logic: 'or',
        filters: [
          {
            field: 'type',
            operator: 'eq',
            value: 'project'
          },
          {
            field: 'phaseId',
            operator: 'eq',
            value: phaseID
          }
        ]
      });
    }
  });

  return ProjectsMasterplanGanttView;
});