/**
 * Represents a gantt chart of the project masterplan
 */
define(function(require, exports, module) {

  var $ = require('jquery'),
      helpers = require('jquery.helpers'),
      Backbone = require('backbone'),
      _ = require('underscore'),
      BaseView = require('views/view'),
      PdfPrinter = require('pdf_printer'),
      jQRangeSlider = require('jqrange-slider')
      ;

  /**
   * Get the week nr from a date object
   * @return {integer}
   */
   Date.prototype.getWeekNumber = function(){
    var d = new Date(+this);
    d.setHours(0,0,0);
    d.setDate(d.getDate()+4-(d.getDay()||7));
    return Math.ceil((((d-new Date(d.getFullYear(),0,1))/8.64e7)+1)/7);
  };


  /**
   * Handles the project masterplan gantt chart
   *
   * Notice: we don't use mustache templates in this view as for some gantt-related things
   * we have to use the kendo templating function so I figured we'd use the kendo templating
   * for all html snippets in this view for consistency. Also, the templates used herein are
   * quite simple and don't necessarily require a standalone template file.
   */
  var ProjectMasterplanGanttView = BaseView.extend({
    // render into this element on the page
    el: '#gantt-container',
    // map events to methods
    events: {
      'click #btn-pdf-export': 'exportPDF',
      'change .gantt-view': function(e) {
        this.setView($(e.target).val());
      },
      'change #cbx-show-details': function() {
        this.$el.toggleClass('show-details');
      }
    },
    // the step data will be parsed into this array
    steps: [],
    // the gantt chart jQuery object will be set to this
    $gantt: null,
    // the instance of the kendo gant chart will be set to this
    gantt: null,
    // will be send to a kendo dataSource object
    dataSource: null,
    projectID: null,
    view: 'month',
    // flag to ensure data is only rendered once
    dataRendered: false,
    options: {
      dateFormatShort: 'dd.MM.yy',
      dateFormatLong: 'dd.MM.yyyy',
      // show a table with the steps that are filtered out by date filter
      showFilteredOutSteps: true
    },

    /**
     * Parse steps
     * options.steps      Array of step data
     * options.projectID  integer Project ID
     * @param {object} options
     */
    initialize: function(options) {
      this.projectID = options.projectID;

      this.earliestDate = new Date();
      this.latestDate = new Date(0);
      //console.table(options.steps, ['id', 'title', 'type', 'start', 'end']);

      _.each(options.steps, _.bind(function(step) {
        // parse start date
        if (step.start) {
          step.start = new Date(step.start);
          // save this date if it's the earliest of all dates
          if (step.start < this.earliestDate) {
            this.earliestDate.setTime(step.start.getTime());
          }
        }
        else {
          step.start = new Date();
          step.hasNoDate = true;
        }

        // parse end date
        if (step.end) {
          step.end = new Date(step.end);
        }
        // fallback to start date
        else {
          step.end = step.start;
        }
        // save this date if it's the latest of all dates
        if (step.end > this.latestDate) {
          this.latestDate.setTime(step.end.getTime()); 
        }

        // add options for tooltip
        step = _.extend(step, this._getTooltipOptions(step));

        this.steps.push(step);
      }, this));

      // now we know our earliest date: iterates steps again and set steps without a date to 
      // the earliest date. this is necessary because the gantt chart does not show tasks without 
      // dates. these dates will then be hidden when gantt chart is rendered.
      var earliestTime = this.earliestDate.getTime();
      for (var i in this.steps) {
        if (this.steps[i].hasNoDate) {
          this.steps[i].start = this.earliestDate;
          this.steps[i].end = this.latestDate;
        }
        // calculate actual duration for steps that have an end date that is > then start date
        // we use this for displaying the nr of days on the bar in the timeline.
        // we do this here explicitly because nrOfDays is actually on the next step (the step took x days to be completed)
        // but in the timeline it makes sense to have the nrOfDays on the bars also as a visual help
        else if (i > 0) {
          var start = this.steps[i].start.getTime(),
              end = this.steps[i].end.getTime();
          if (end > start) {
            var diff = Math.abs(end - start),
                nrOfDays = Math.floor(diff / (8.64e7));
            this.steps[i].durationDays = nrOfDays > 0 ? nrOfDays : null;
          }
        }
      }
    },

    /**
     * Render gantt chart
     */
    render: function() {
      this.$gantt = this.$('#gantt-chart');
      this.dataSource = new kendo.data.GanttDataSource({});
      this._initGanttChart();

      // get instance of chart
      this.gantt = this.$gantt.data('kendoGantt');

      // show details by default
      this.$el.addClass('show-details');

      // if our dates span several years, we'll start off in year view
      var yearDiff = this.latestDate.getFullYear() - this.earliestDate.getFullYear();
      if (yearDiff > 2) {
        this.setView('year');
      }

      this._initDateRangeSlider();
      // set data of data source only now otherwise the setView above may cause the chart to be drawn
      // twice during initialization which is unnecessary
      this.dataSource.data(this.steps);
    },

    /**
     * Init kendo gantt chart
     */
    _initGanttChart: function() {
      var that = this;
      this.$gantt.kendoGantt({
        dataSource: this.dataSource,
        autoBind: false,
        editable: false,
        selectable: false,
        height: '100%',
        tooltip: { 
          visible: true,
          template: '\
            <div class="gantt-tooltip #= task.hueClass #" style="background-color: \\##= task.color # !important;"> \
              <span class="title">#= task.title #</span class="title"> \
              <span class="date-range">#= task.tooltipDate #</span> \
              # if (task.overdueDays > 0) { # \
                <span class="days-overdue">#= task.overdueDays # days overdue</span> \
              # } # \
            </div> \
          '
        },
        views: [
          {
            type: 'month',
            slotSize: 56,
            weekHeaderTemplate: "#='wk '+ kendo.toString(start.getWeekNumber())#", // e.g. wg 43
            monthHeaderTemplate: "#= kendo.toString(start, 'MMM') + ' ' + kendo.toString(start, 'yyyy') #" // eg. AUG 2015
          },
          {
            type: 'year',
            slotSize: 50
          }
        ],
        columns: [
          // @todo translate
          { field: "step", title: "Step", width: 40, class: 'step-nr' },
          { field: "title", title: "Title", width: 200, class: 'title' },
          { field: "days", title: "N° days", width: 55, field: 'nrOfDays', class: 'nr-of-days' },
          { field: "date", title: "Date", width: 65, field: 'start', class: 'date', format: '{0:' + this.options.dateFormatShort + '}' }
        ],
        // custom operations on gantt chart once data is bound
        dataBound: function() {
          // only execute our code once and when there are actually data items bound
          if (that.dataSource.total() < 1 || that.dataRendered) {
            return;
          }
          var gantt = this;

          /**
           * style tree tasks
           */
          gantt.element.find('.k-grid-content table[role=treegrid] tr').each(function() {
            var dataItem = gantt.dataSource.getByUid($(this).attr('data-uid'));
            $(this).css('background-color', '#' + dataItem.color);
            
            $(this).find('td').each(function(i) {
              var col = gantt.options.columns[i];
              $(this).addClass(col.class + ' column');
              // do not show date field if this step doens't actually have a date
              if (col.class == 'date' && dataItem.hasNoDate) {
                $(this).find('span').remove();
              }
            });
          });

          /**
           * style timeline tasks
           */
          gantt.element.find('.k-grid-content .k-gantt-tasks .k-task').each(function() {
            var $task = $(this),
                dataItem = gantt.dataSource.getByUid($task.attr('data-uid'));

            // mark overdue tasks
            if (dataItem.overdueDays > 0) {
              $task.addClass('overdue');
              $task.find('.k-task-complete').css('background-color', '#' + dataItem.color);
            }

            // hide tasks that don't have an actual date
            if (dataItem.hasNoDate) {
              $task.hide();
            }
            else {
              var taskText = dataItem.durationDays ? dataItem.durationDays + ' days' : '';
              that._enhanceTimelineTask($task, taskText);
            }
          });

          that.adjustHeight();
          that.dataRendered = true;
        }
      });
    },

    /**
     * Adjust height of gantt chart after filtering or before printing
     */
    adjustHeight: function() {
      this.$el
        .add(this.$gantt)
        .add(this.$('.k-gantt-treelist'))
        .css('height', 'auto');

      var actualHeight = this.$('.k-gantt-treelist .k-grid-content > table').height();
      this.$('.k-grid-content').add(this.$('.k-gantt-timeline')).css('height', actualHeight);

      // height of all containers are set to 100% to make sure the space is used, however when the
      // content is actually > 100%, we set the containers to 'auto' or they will be fixed to 100%,
      // causing the filtered-out steps table to be cut off
      //if (actualHeight > $(window).height()) {
      //}
    },

    /**
     * Clear custom task details before they're re-rendered
     * I'm not sure how kendo gantt tracks it's tasks but after filtering a chart, sometimes
     * our custom task details may end up on the wrong tasks or are added twice, so it's best
     * to clean things up before we re-render the timeline.
     */
    clearTaskDetails: function() {
      this.$('.k-timeline .custom-task-detail').remove();
      this.dataRendered = false;
    },

    /**
     * Filter gantt chart between a date range
     * @param {Date} from
     * @param {Date} to
     */
    filterByDate: function(from, to) {
      // create a filter that does this:
      // select dates where
      // (start date is >= from and <= to) OR (end date is >= from and <= to)
      var filterDates = {
        logic: 'and',
        filters: [
          // filter start date
          {
            logic: 'and',
            filters: [
              {
                field: 'start',
                operator: 'gte',
                value: from
              },
              {
                field: 'start',
                operator: 'lte',
                value: to
              }
            ]
          },
          // filter end date
          {
            logic: 'and',
            filters: [
              {
                field: 'end',
                operator: 'gte',
                value: from
              },
              {
                field: 'end',
                operator: 'lte',
                value: to
              }
            ]
          },
        ]
      };
      
      // also filter out the dates for which we created a fake date on initialize
      // because these dates actually have <this.earliestDate> as their date
      var filterFakeDates = {
        field: 'hasNoDate',
        operator: 'neq',
        value: true
      };

      this.dataSource.filter({
        logic: 'and',
        filters: [ filterFakeDates, filterDates ]
      });
    },

    /**
     * Show a list of steps that were filtered out by the current filter settings
     * @return {void}
     */
    showFilteredOutSteps: function() {
      // retrieve filtered data
      var data = this.dataSource.view(),
          // get ids that are currently showing in gantt
          currentIDs = _.pluck(data, 'id'),
          // get all ids from all steps
          allIDs = _.pluck(this.steps, 'id'),
          // find out which ids where filtered out
          filteredIDs = _.difference(allIDs, currentIDs);

      // get all steps that were filtered out and actually have a date
      var filtered = [];
      _.each(filteredIDs, _.bind(function(id) {
        var step = _.findWhere(this.steps, {id: id});
        if (step.hasNoDate) {
          return;
        }
        filtered.push(step);
      }, this));

      // empty table before we start
      var $table = this.$('#filtered-out table').empty();
      // hide table and abort
      if (filtered.length < 1) {
        $table.parent().addClass('hidden');
        return;
      }

      // show table and populate with hidden steps
      $table.parent().removeClass('hidden');
      _.each(filtered, _.bind(function(step) {
        var html = '<tr><td class="step">#=step#</td><td class="title">#=title#</td> \
          <td class="date">#=kendo.format("{0:' + this.options.dateFormatShort + '}", start)#</td></tr>';
        var tpl = kendo.template(html),
            $row = $(tpl(step));
        $table.append($row);
      }, this));
    },

    /**
     * Export gantt chart as pdf
     */
    exportPDF: function(e) {
      e.preventDefault();
      
      // make sure everything all parent containers of timeline are full width
      this.$('.k-timeline .k-grid-header-wrap').parentsUntil('body')
              .css('width', 'auto')
              .css('max-width', 'none');
      this.adjustHeight();

      this._print();
    },

    /**
     * Set the view type of the gantt chart
     * @param {string} month|year
     */
    setView: function(view) {
      this.clearTaskDetails();
      this.view = view;
      // tell the gantt chart
      this.$gantt.data('kendoGantt').view(view);
      // make sure the dropdown is set to the right value
      this.$('#gantt-time-view').val(view);
    },

    /**
     * Enhance a task in the timeline
     * Sets color of task and adds detail information
     * 
     * @param  {object} jQuery object of task element
     * @param  {string} text to display on task bar
     * @return {void}
     */
    _enhanceTimelineTask: function($task, taskText) {
      var $textContainer = $task.find('.k-task-template'),
          $taskContent = $task.find('.k-task-content');
      var dataItem = this.gantt.dataSource.getByUid($task.attr('data-uid'));

      // make sure anything added to the timeline has the class "custom-task-detail" because
      // we have to remove these elements on our own in this.clearTaskDetails()

      // if there's no percentComplete, hide it or it'll show on tasks with short durations
      if (!dataItem.percentComplete) {
        $task.find('.k-task-complete').hide();
      }
      
      // set task background color & calculate if it's a dark color
      if (!dataItem.overdueDays > 0) {
        $task.css('background-color', '#' + dataItem.color);
      }
      $task.addClass(dataItem.hueClass);

      // tasks with start & end on the same day are interpreted by kendo gantt as "milestones" and 
      // have no text content. we want to show content however so we create it ourselves
      if ($task.hasClass('k-task-milestone')) {
        $taskContent = $('<div class="custom-task-detail k-task-content"><div class="k-task-template"></div></div>');
        $taskText = $taskContent.find('.k-task-template');
        $task.parent().append($taskContent);
      }

      $textContainer.text(taskText);
      // move text to the right if it's too wide for the bar
      var textWidth = $textContainer.textWidth(),
          containerWidth = $task.width()
          overflowText = (textWidth > containerWidth);
      if (overflowText) {
        $textContainer.text('(' + taskText + ')')
        $task.addClass('overflow');
        $taskContent.css('left', $task.width());
      }

      // create date markers for start & end date
      var format = '{0:' + this.options.dateFormatShort + '}';
      var tplStart = kendo.template('<div class="custom-task-detail task-date task-date-start">#= kendo.format("' + format + '", start) #</div>');
      $(tplStart({start: dataItem.start})).insertBefore($taskContent);

      // if text already overflowed, add the end date to the overflow text as it's already on the right
      if (overflowText) {
        var tplEnd = kendo.template('<span class="custom-task-detail task-date task-date-end-append">#= kendo.format("' + format + '", end) #</span>');
        $textContainer.append(tplEnd({end: dataItem.end}));
      }
      else {
        if (dataItem.end > dataItem.start) {
          var tplEnd = kendo.template('<div class="custom-task-detail task-date task-date-end">#= kendo.format("' + format + '", end) #</div>');
          $(tplEnd({end: dataItem.end})).insertBefore($taskContent);
        }
      }
    },

    /**
     * Print the chart
     * @private
     */
    _print: function() {
      PdfPrinter.print(this.$el, {
        paperSize: 'A3',
        landscape: true,
        proxyURL: '/project/projectmasterplan/export',
        fileName: this.projectID
      });
    },

    /**
     * Get options for the tooltip
     * @param  {object} step
     * @return {object} Object with relevant properties for tooltip
     */
    _getTooltipOptions: function(step) {
      var options = {};
      // define date displayed in tooltip
      // ranges are like 'dateFrom - dateTo', milestones are just 'date'
      if (step.end === step.start) {
        options.tooltipDate = kendo.toString(step.start, this.options.dateFormatLong);
      }
      else {
        options.tooltipDate = kendo.toString(step.start, this.options.dateFormatLong);
        options.tooltipDate += ' - ' + kendo.toString(step.end, this.options.dateFormatLong);
      }
      
      // a class to determine if it's a dark bg color or not
      options.hueClass = this._isDarkColor(step.color) ? 'dark' : '';
      return options;
    },

    /**
     * Init date-range slider
     */
    _initDateRangeSlider: function() {
      // copy earliest and latest date so we don't chage the origal values
      var from = new Date(this.earliestDate.getTime());
      var to = new Date(this.latestDate.getTime());

      // set "from" to 1rst day of month
      from.setDate(1);
      // set "to" to first day of next month
      to.setMonth(to.getMonth()+1);
      to.setDate(1);

      this.$('#gantt-date-slider').dateRangeSlider({
        step: { months: 1 },
        arrows: false,
        delayOut: 1000,
        formatter: function(val) {
          return kendo.toString(val, 'MMM') + ' ' + kendo.toString(val, 'yyyy');
        },
        range: {
          min: { months: 1 }
        },
        bounds: {
          min: from,
          max: to
        },
        defaultValues: {
          min: from,
          max: to
        }
      })
      .bind('userValuesChanged', _.bind(function(e, data) {
        this.clearTaskDetails();
        this.filterByDate(data.values.min, data.values.max);
        if (this.options.showFilteredOutSteps) {
          this.showFilteredOutSteps();
        }
      }, this));
    },

    /**
     * Calculates if a color is dark
     * @param {string} hex color code without the #
     * @return {Boolean}
     */
    _isDarkColor: function(color) {
      var rgb = parseInt(color, 16); // convert rrggbb to decimal
      var r = (rgb >> 16) & 0xff;    // extract red
      var g = (rgb >>  8) & 0xff;    // extract green
      var b = (rgb >>  0) & 0xff;    // extract blue

      var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709
      return (luma < 150);
    }
  });

  return ProjectMasterplanGanttView;
});