/**
 * Represents the entire masterplan template view
 */
define(function(require, exports, module) {

  var $ = require('jquery'),
      Backbone = require('backbone'),
      _ = require('underscore'),
      BaseView = require('views/view'),
      PhaseView = require('./phase'),
      Phase = require('apps/admin/models/masterplan_templates/phase'),
      ButtonView = require('views/button')
      sortable = require('jquery.sortable'),
      retailnet = require('libs/retailnet')
      ;

  /**
   * Represents the entire masterplan template view
   */
  var MasterplanTemplatesView = BaseView.extend({
    // bind to this element in view
    el: '#masterplan_templates',
    // maps events to methods
    events: {
      'click #addBtn': 'addPhase',
      'click #saveBtn': 'save'
    },
    // set with this.setData()
    data: {},
    phaseViews: {},

    /**
     * Setup masterplan view
     * Pass in a Masterplan model in the 'model' key of options.
     * Pass in view data in the 'data' key of options.
     * @param  {object} options
     */
    initialize: function(options) {
      this.$el.data('masterplan', this);
      this.data = options.data;

      //_.bindAll(this, /*'_onDrop', '_isValidTarget',*/ 'addPhaseView');
      
      // phases must be rendered in the right order, first those without a parent (parent_id = 0),
      // then those with a parent, ordered by pos. so - group phases by parent_id
      var phasesByParent = this.model.get('phases').groupBy('parent_id');
      _.each(phasesByParent, _.bind(function(phases) {
        // sort them by the 'pos' attribute and call addPhaseView for each model
        _.each(_.sortBy(phases, function(phase) { return phase.get('pos') }), _.bind(this.addPhaseView, this));
      }, this));

      // listen for new phases on phase collection
      //this.listenTo(this.model.get('phases'), 'add', this.addPhaseView);

      // make phases sortable
      $('#phases').nestedSortable({ 
        handle: '.drag-handle',
        onDrop: this._onDrop,
        isValidTarget: this._isValidTarget
      });
    },

    /**
     * Add a phase view to collection and render a phase view
     * @param {object} Click event
     */
    addPhase: function(e) {
      e.preventDefault();
      var $btn = $(e.currentTarget),
          $title = $btn.siblings('input[name=phase_title]');

      // get and reset title
      var title = $title.val();
      $title.val('');

      if (!title.length) {
        return;
      }

      // instantiate model
      var phase = this.model.get('phases').add({
        masterplan_id: this.model.get('id'),
        title: title,
        pos: this.model.get('phases').length
      });

      this.addPhaseView(phase);
      this.save();
    },

    /**
     * Sync data to server
     */
    save: function() {
      this.model.bind('sync', function(response) {
        retailnet.notification.show(response.attributes.content, response.attributes.properties);
        setTimeout(function() {
          location.reload();
        }, 1000);
      })
      this.model.save();
    },

    /**
     * Get data
     * You can either get a section of the data or all of it (default) by
     * omitting the section.
     * @param  {string} optional section of data
     * @return {object}
     */
    getData: function(section) {
      if (section && typeof(this.data[section]) !== undefined) {
        return this.data[section];
      }
      else {
        return this.data;
      }
    },

    /**
     * Create a phase view from a phase model
     * @private
     * @param {object} phase model
     */
    addPhaseView: function(phase) {
      // create phase view
      var phaseView = new PhaseView({
        dataProvider: this,
        model: phase
      });
      this.phaseViews[phase.get('id')] = phaseView;
      phaseView.render();
    },

    /**
     * Handle drop
     */
    _onDrop: function($item, container, _super, e) {
      // call drag'n'drop onDrop handler
      _super($item, container);

      // get view of moved item
      var view = $item.data('view');

      // get sibling and self, iterate them and set new position on model
      view.$el.siblings().andSelf().each(function(i) {
        //console.log('setting pos ' + i + ' on phase ' + $(this).data('view').model.get('title'))
        $(this).data('view').model.set('pos', i);
      });

      // when a phase was moved..
      if (view.type == 'phase') {
        var $parentPhase = $item.parents('.phase:first');
        // get id of parent and set on model of moved view
        if ($parentPhase.length) {
          var parent = $parentPhase.data('view');
          view.model.set('parent_id', parent.model.get('id'));
        }
      }
      // when a step was moved..
      else if (view.type == 'step') {
        // remove model from it's current collection and reinsert in new phases collection
        // this is for when a step is dropped on another phase
        var parentPhase = view.$el.closest('.phase').data('view');
        view.model.collection.remove(view.model);
        parentPhase.model.get('steps').add(view.model);
      }
    },

    /**
     * Decide if the hovered element is a valid drop target
     * This is called ondrag
     * @todo  what happens if phase with subphases is dropped on another phase?
     * -> probably just abort in onDrop and rerender the view..
     */
    _isValidTarget: function($current, container) {
      var isSubphase = $current.hasClass('subphase'),
        isPhase = (!isSubphase && $current.hasClass('phase')),
        isStep = $current.hasClass('step'),
        $target = container.el;

      // steps may be dropped on step containers
      if (isStep && $target.hasClass('steps')) {
        return true;
      }
      // phases may be dropped on subphase containers
      else if (isPhase && $target.hasClass('subphases')) {
        return true;
      }
      // phases may be dropped on main phase container
      else if (isPhase && $target.is('#phases')) {
        return true;
      }
      // subphases may be dropped on subphases container
      else if (isSubphase && $target.hasClass('subphases')/* && $current.parent().is($target)*/) {
        return true;
      }

      // everything else is not allowed
      return false;
    }
  });

  return MasterplanTemplatesView;
});
