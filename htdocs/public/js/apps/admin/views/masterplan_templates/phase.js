/**
 * Represents a phase of a masterplan template
 */
define(function(require, exports, module) {

  var $ = require('jquery'),
      Backbone = require('backbone'),
      _ = require('underscore'),
      BaseView = require('views/view'),
      StepView = require('./step'),
      ButtonView = require('views/button'),
      mustache = require('mustache'),
      // templates
      phaseTpl = require('stache!admin/masterplan.template.phase'),
      subPhaseTpl = require('stache!admin/masterplan.template.subphase')
      ;
  
  var PhaseView = BaseView.extend({
    type: 'phase',
    tagName: 'li',
    attributes: { class: 'phase' },
    title: '',
    dataProvider: null,
    events: {
      // save title to model onchange of title field
      'change [name=phase_title]': function(e) {
        // don't let event bubble to parent phase
        e.stopImmediatePropagation();
        this.model.set('title', $(e.target).val());
      },
      // create step on click of button
      'click [name=addStep]': function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        this.addStep(this.model.get('steps').create());
      }
    },

    /**
     * Init phase
     * Supported options:
     *   dataProvider   Instance of dataProvider object, used to get data
     * @param  {object} options
     */
    initialize: function(options) {
      that = this;
      this.dataProvider = options.dataProvider;
      this.$el.data('view', this);

      this.listenTo(this.model, 'change:parent_id', this.render);
    },

    /**
     * Renders the phase
     * This method may be called multiple times depending where a phase is dropped.
     * It only appends the rendered element to the dom on the first time. Succeeding
     * calls just replace the html of the element. This functionality is used to change
     * a phase to a subphase during runtime.
     */
    render: function() {
      var parentID = this.model.get('parent_id'),
          isSubPhase = (parentID > 0);
      
      // @todo translate
      if (isSubPhase) {
        var tpl = subPhaseTpl({
          title: /*this.model.get('pos') + '_' + */this.model.get('title'),
          stepNr: 'Nr.',
          stepTitle: 'Title',
          stepType: 'Type',
          stepColor: 'Color',
          stepValue: 'Value',
          stepRole: 'Responsible role',
          stepActive: 'Active',
          stepDuration: 'Duration',
          projectCostTypes: this.dataProvider.getData('projectCostTypes'),
          posTypes: this.dataProvider.getData('posTypes')
        });
      }
      else {
        var tpl = phaseTpl({
          title: /*this.model.get('pos') + '_' + */this.model.get('title'),
        });
      }

      this.$el.html(tpl);

      /**
       * Subphase
       */
      var appendTo;
      if (isSubPhase) {
        // add "add step" button if it's a subphase
        var addBtn = new ButtonView({attr: {name: 'addStep'}});
        this.$('.toolbar').append(addBtn.render('Add step'));
        this.$el.addClass('subphase');

        // append to parent's subphase container
        var parentView = this.dataProvider.phaseViews[parentID];
        appendTo = parentView.$('.subphases');
      }
      /**
       * Phase
       */
      else {
        this.$el.removeClass('subphase');
        var appendTo = '#phases';
      }

      // only append phase/subphase if not already in dom
      if (!$.contains(document.documentElement, this.el)) {
        $(appendTo).append(this.el);
      }

      // render pre-existing steps. this has to be done after inserting this.el into dom
      if (isSubPhase && this.model.get('steps') !== undefined) {
        this.model.get('steps').forEach(this.addStep, this);
      }
    },

    /**
     * Adds a step to the phase
     * @param {object} Step model
     */
    addStep: function(model) {
      // we only show one "header" per parent phase so
      // go to the parent that is not a subphase
      this.$el.closest('.phase:not(.subphase)')
        // and find the first li.head and show it
        .find('.phase .head:first').removeClass('hidden');

      var stepView = new StepView({
        dataProvider: this.dataProvider,
        model: model
      });
      this.$('> .steps').append(stepView.el);
      stepView.render();
    }
  });

  return PhaseView;
});
