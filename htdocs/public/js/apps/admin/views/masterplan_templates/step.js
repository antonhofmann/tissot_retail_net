/**
 * Represents a step of a masterplan template phase
 */
define(function(require, exports, module) {

  var $ = require('jquery'),
      Backbone = require('backbone'),
      _ = require('underscore'),
      BaseView = require('views/view'),
      colorpicker = require('jquery-ui/colorpicker'),
      mustache = require('mustache'),
      ModalView = require('views/modal'),
      jQueryUI = require('jquery-ui'),
      // templates
      tpl = require('stache!admin/masterplan.template.step'),
      durationTpl = require('stache!admin/masterplan.template.duration')
      ;
  
  /**
   * Step View
   */
  var StepView = BaseView.extend({
    type: 'step',
    tagName: 'li',
    attributes: { class: 'step' },
    dataProvider: null,
    rowBackgroundColor: '#f4f4f4',
    events: {
      'click .icon.delete': 'delete',
      'click .duration .icon': 'openDurationModal',
      'change .step_role': '_responsibleRoleChanged'
    },

    /**
     * Setup view
     * @param {object} options
     */
    initialize: function(options) {
      this.dataProvider = options.dataProvider;
      this.$el.data('view', this);
      _.bindAll(this, '_formChanged', '_initDurationModal', 'saveDurations');
    },

    /**
     * Render the step template
     */
    render: function() {
      // parse template
      this.$el.html(tpl({
        stepNr: 'Nr.',
        stepTitle: 'Title',
        stepDuration: 'Duration',
        projectCostTypes: this.dataProvider.getData('projectCostTypes'),
        posTypes: this.dataProvider.getData('posTypes'),
        roles: this.dataProvider.getData('roles'),
        values: this.dataProvider.getData('values'),
        stepTypes: this.dataProvider.getData('stepTypes')
      }));

      this.renderValues();

      // listen for all form change events
      this.$('input,select').change(this._formChanged);

      // init colopricker on step
      this.$('.colorpicker input').colorpicker({
          showOn: 'button',
          history: false,
          color: '#' + this.model.get('color'),
          displayIndicator: false
        })
        // change color of step on mouseover
        .on('mouseover.color', function(e, color) {
          $(e.currentTarget).closest('.step').css('background-color', color);
        })
        // save color
        .on('change.color', _.bind(function(e, color) {
          $(e.currentTarget).closest('.step')
            .css('background-color', this.rowBackgroundColor);
          this.model.set('color', color.substr(1, color.length-1));
        }, this));

      this.focus();
    },

    /**
     * Focus the appropriate field
     */
    focus: function() {
      if (this.model.isNew()) {
        if (this.model.get('step_nr')) {
          this.$('[name=title]').focus();
        }
        else {
          this.$('[name=step_nr]').focus();
        }
      }
    },

    /**
     * Render values from model to step
     */
    renderValues: function() {
      this.$('[name=step_nr]').val(this.model.get('step_nr'));
      this.$('[name=title]').val(this.model.get('title'));
      this.$('[name=value]').val(this.model.get('value'));
      this.$('[name=is_active]').attr('checked', this.model.get('is_active') == 1);
      this.$('[name=responsible_role_id]').val(this.model.get('responsible_role_id'));
      this.$('[name=step_type]').val(this.model.get('step_type'));

      // concat posTypes and projectCostTypes
      var types = this.dataProvider.getData('projectCostTypes')
        .concat(this.dataProvider.getData('posTypes'));
      // and automatically set if they're checked or not
      _.each(types, function(type) {
        var name = 'apply_' + type.alias;
        this.$('[name=' + name + ']').attr('checked', this.model.get(name) == 1);
      }, this);
    },

    /**
     * Open modal for country durations
     */
    openDurationModal: function() {
      // parse template
      var duration = durationTpl({
        regions: this.dataProvider.getData('salesRegions'),
        txtNotice: "The default duration is in days. Set the duration of a sales region to set the duration of all it's countries."
      });

      // instantiate modal view
      var durationView = new ModalView({
        lblTitle: 'Set default durations',
        body: duration,
        onSave: this.saveDurations,
        afterOpen: this._initDurationModal,
        closeOnEsc: true
      });

      durationView.show();
    },

    /**
     * Save durations from durations modal
     * If this gets more complicated it will make sense to create a specific DurationModalView
     * for handling these things, but right now it's simple enough.
     * @param  {object} modal element
     */
    saveDurations: function(modal) {
      var defaults = {},
          exceptions = [];
      $(modal).find('.countries').each(function() {
        // get default duration of each region
        var regionID = $(this).attr('data-region');
        var regionValue = $(modal).find('.region-duration-' + regionID).val();
        defaults[regionID] = regionValue ? parseInt(regionValue) : null;
        // register exceptions to the default of the corresponding region
        $(this).find('.duration').each(function() {
          if (this.value != regionValue) {
            exceptions.push({
              country_id: $(this).data('country') ? parseInt($(this).data('country')) : null,
              salesregion_id: regionID,
              duration: this.value ? parseInt(this.value) : null
            });
          }
        });
      });

      this.model.set('duration_defaults', defaults);
      this.model.set('duration_exceptions', exceptions);
    },

    /**
     * Delete this step
     * @param  {object} Event object
     */
    delete: function(e) {
      // hide step in gui
      $(e.target).closest('.step').slideUp('fast');
      this.model.destroy();
    },

    /**
     * Update step model on form-change
     * Triggered when any element in step form changes
     * @param {object} Change event
     */
    _formChanged: function(e) {
      var $target = $(e.target);

      // handle value depending on form control type
      var value;
      if ($target.attr('type') == 'checkbox') {
        value = $target.attr('checked') ? 1 : 0;
      }
      else {
        value = $target.val();
      }
      //console.log($target.attr('name'), value);
      this.model.set($target.attr('name'), value);
    },

    /**
     * Responsible role select value changed
     * This is used to allow special actions on certain roles.
     * Currently when choosing the admin role, you must specify an exact user.
     * @param {object} Event
     */
    _responsibleRoleChanged: function(e) {
      if ($(e.target).val() != 1) {
        // get rid of previously saved user_id when changing roles
        this.model.unset('responsible_user_id');
        return;
      }

      // throw together a form with a select. not worth a template file
      var $form = $('<form class="default responsible-user-picker"/>');
      var $select = $('<select name="user_id" size="5" />').appendTo($form);
      var users = this.dataProvider.getData('adminUsers');
      for (var i in users) {
        $select.append(
          $('<option/>')
            .val(users[i].id)
            .text(users[i].firstname + ' ' + users[i].lastname)
            .attr('selected', users[i].id == this.model.getResponsibleUser())
          );
      }

      // init modal
      var userPickerModal = new ModalView({
        // @todo translate
        lblTitle: 'Choose responsible user',
        body: $form,
        width: 300,
        height: 400,
        // save responsible user to modal
        onSave: _.bind(function(modal) {
          this.model.set('responsible_user_id', $(modal).find('select[name=user_id]').val());
        }, this),
        // reset responsisble role if no user was selected
        afterClose: _.bind(function() {
          var userID = this.model.get('responsible_user_id');
          if (!userID) {
            this.$('select[name=responsible_role_id]').val('');
          }
        }, this),
        closeOnEsc: true
      });
      userPickerModal.show();
    },

    /**
     * Called when a sales region duration was changed
     * In this case we want to set all the country values of the changed region
     * to the value if the country value is not already set.
     * Note: "this" is not bound to view object for this method
     * @param  {object} Changed Event
     * @return {void}
     */
    _regionDurationChanged: function(e) {
      var regionDuration = this.value,
          previousValue  = $(this).data('previousValue');

      // if value is not a positive integer, mark field as faulty and abort
      if (regionDuration.search(/[^\d]+/) >= 0
        || parseInt(regionDuration) < 0) {
        $(this).addClass('error');
        return;
      }
      // otherwise remove error class if there is one and remember the value
      else {
        $(this)
          .removeClass('error')
          .data('previousValue', regionDuration);
      }

      $(this).closest('.caption')
        .next('.countries').find('.duration').each(function() {
          // if value is empty, 0 or changed, apply regional value
          if (!this.value || this.value == previousValue) {
            this.value = regionDuration;
          }
      });
    },

    /**
     * Init duration modal after opening
     * Sets up accordion, populates existing values and registers event listeners
     * If this gets any more complex it will be time to extend ModalView with a specific
     * DurationModalView
     * @param {object} Modal HTML element
     */
    _initDurationModal: function(modal) {
      // make regions list an accordion
      $(modal).find('.durations-table').accordion({
        header: '.caption',
        heightStyle: 'content',
        icons: { 'header': 'icon direction-up', 'activeHeader': 'icon direction-down' },
        collapsible: true,
        active: false,
        animate: 250
      });

      // when a region duration changes, change all it's countires that don't yet have a value
      $(modal).find('.caption .duration')
        // stop propagation onclick of text field or the accordion will toggle it's state
        .click(function(e) { e.stopImmediatePropagation(); })
        .change(this._regionDurationChanged);

      // populate default durations
      var defaults = this.model.get('duration_defaults');
      for (var i in defaults) {
        modal.find('.region-duration-' + i)
          .val(defaults[i] || 0)
          // trigger change event to populate all the countries of region
          .change();
      }

      // pouplate country exceptions
      var exceptions = this.model.get('duration_exceptions');
      for (var i in exceptions) {
        modal.find('.country-duration-' + exceptions[i].country_id).val(exceptions[i].duration || 0); 
      }
    }
  });

  return StepView;
});
