$(document).ready(function() {

	// spiner instance
	retailnet.loader.init();
	
	// markup control ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$('.selectbox').dropdown();

	$('input.posverification_data_actual_number').keypress(function(event) {
		
		if (event.keyCode == 13) {
			
			var inputs = $('input.posverification_data_actual_number');
			var idx = inputs.index(this);
			
			if (inputs.length == idx + 1) {
				
			}
			else if (idx == inputs.length - 1) {
				inputs[0].select();
			} else {
				inputs[idx + 1].focus();
				inputs[idx + 1].select();
			}
			
			return false;
       }
	});

	$('#periodes').change(function(event) {
		var value = $(this).val();
		var url = $('#url').val();
		window.location = (value) ? url+'/'+value : url;
	});


	// dialogs :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$('.dialog').click(function(event) {

		event.preventDefault();
		event.stopPropagation();

		// close all notofications
		retailnet.notification.hide();

		


		if ($(this).hasClass('confirm')) {

			var fields = $('input.posverification_data_actual_number').length;
			var emptyfields = $('input.posverification_data_actual_number[value=]').length;

			if (fields==emptyfields) {
				retailnet.notification.error('Please indicate the actaul number of POS locations!');
				return;
			}

			if($('#pos_verification_deleted').val() == 1 
				&& $('#number_of_total_pos_locations').html() != $('#posverification_data_actual_number').html()) {
				retailnet.notification.error('Your actual numbers must match the POS Index!<br />Read the help section on how to proceed.');
				return;
			}
		}
		
		var self = $(this);
		var target = '#'+self.prop('id')+'_dialog';
		
		$('.apply', $(target)).prop('href', self.prop('href'));
		
		retailnet.modal.dialog(target);
	})

	// hide modal dialog
	$('.cancel').click(function(event) {
		event.preventDefault();
		retailnet.modal.close();
		return false;
	});

	// button actions ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$('.apply').on('click', function(event) {

		event.preventDefault();
		event.stopPropagation();

		retailnet.modal.close();

		var self = $(this);
		var url = self.prop('href');
		var data = $('form.request').serializeArray();

		if (!url) {
			retailnet.notification.error('Submit action is not defined');
			return;
		}

		retailnet.loader.show();

		retailnet.ajax.json(url, data).done(function(xhr) {

			retailnet.loader.hide();
			
			if (xhr && xhr.response) {
				window.location.reload();
			}
			
			if (xhr && xhr.notification) {
				retailnet.notification.show(xhr.notification.content, eval(xhr.notification.properties));
			}
		});
	});

	// actual number submit ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$('input.posverification_data_actual_number').on('change', function() {

		var self = $(this);
		var value = self.val();
		var channel = self.attr('index');

		retailnet.notification.hide();

		if (value) {
			
			value = parseInt(value);

			 if (isNaN(value) || value<0 ) {
				 $(this).val('').select();
		         retailnet.notification.error('Invalid Actual Number, this field should contain only Integer number.');
		         return false;
			 }
		}
		
		var data = $('form.request').serializeArray();
    	data.push({name: 'value', value: value});
    	data.push({name: 'channel', value: channel });
    	
    	retailnet.ajax.json('/applications/modules/pos/verification/data/submit.php', data).done(function(xhr) {

    		if (xhr.response) {

        		var table = self.closest('table');
        		
        		// trigger to change for this column
    			$('.total-cell', table).trigger('change');
        	}
    		
			if (xhr && xhr.notification) {
				retailnet.notification.show(xhr.notification.content, eval(xhr.notification.properties));
			}
        });
	})


	// quantitiy calculations ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$('.total-cell').on('change', function() { 

		var self = $(this);
		var table = self.closest('table');
		var target = self.attr('data-target');

		var total = 0;
		var value = 0;

		$('tbody '+target, table).each(function(i, elem) {

			// calcullate delta value
	        if (target=='.delta') {

				var row = $(elem).closest('tr');
				
				var inPosIndex = Number($('td.in_pos_index', row).text());

				var actualNUmber = ($('input.posverification_data_actual_number', row).is('input')) 
					? $('input.posverification_data_actual_number', row).val() 
					: Number($('td.posverification_data_actual_number', row).text());
				
				var delta = actualNUmber - inPosIndex;
				$(elem).text(delta).toggleClass('alert', (delta==0) ? false : true)

				total = total+delta;
				
			} else {

				var value = ($(elem).is('input')) ? $(elem).val() : $(elem).text();

				// sum cell values
		        if (value && !isNaN(value)) {

					value = Number(value);

					if (!isNaN(value)) {
		            	total = total+value;
					}
		        }
			}
		});

		total = total || '&nbsp;';
		self.html(total);
		$('.grand-total-cell').trigger('change');
		
	}).trigger('change');


	$('.grand-total-cell').on('change', function() {

		var self = $(this);
		var target = self.attr('data-target');

		var total = 0;
		
		$('tfoot '+target).each(function(i, elem) {

			value = $(elem).text();

	        if (value && !isNaN(value)) {
	            total = total + Number(value);
	        }
		});

		total = total || '&nbsp;';
		self.html(total);
		
	}).trigger('change');



	// print dialog ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var printDialogContainer = $('#printDialog');
	
	var modalPrintDialog = printDialogContainer.adoModal({
		width: 400
	});

	$('#print').on('click', function(e) {
		e.preventDefault();
		modalPrintDialog.show();
	});

	$('.submit-print', printDialogContainer).on('click', function(e) {
		
		e.preventDefault();
		e.stopPropagation();

		retailnet.notification.hide();

		var url = $('#print').prop('href');
		var data = $('form.request, #printDialog form').serializeArray();
		

		if (!$('input.month:checked', printDialogContainer).length) {
			retailnet.notification.error('Please select at least one month');
			return;
		}

		if (!$('input.option:checked', printDialogContainer).length) {
			retailnet.notification.error('Please select at least one print option');
			return;
		}

		if (!url) {
			retailnet.notification.error('Action URL is not defined. Contact system administrator');
			return;
		}
		
		retailnet.loader.show();
		modalPrintDialog.close();
		
		retailnet.ajax.json(url, data).done(function(xhr) {

			if (xhr && xhr.success && xhr.file) {
				window.open(xhr.file);
			}

			if (xhr && xhr.errors) {
				$.each(xhr.errors, function(i, message) {
					retailnet.notification.error(message);
				})
			}

		}).complete(function() {
			retailnet.loader.hide();
		})
	})
});