
var Article = Article || {} 


Article.breadcrumps = function(state) {

	var $this = this;
	var container = $('#breadcrumps');

	if (!$this.states) {
		
		this.states = Adomat.Ajax.json('/applications/modules/news/article/article.php', {
			application: 'news',
			section: 'workflow-states'
		});

		$this.states = this.states;
	}
	
	container.empty();

	for (var i in $this.states) { 
		var item = $('<li />');
		item.text($this.states[i]);
		if (i<=state) item.addClass('active');
		container.append(item); 
	}
}


Article.updateField = function(field, value) {

	var $field = $('#'+field);
	var type = $field.prop('tagName');

	if (type) {

		$field.val(value);

		if ($field.is(':checkbox')) {
			$field.attr('checked', value)
		}

		if (jQuery().selectpicker && $field.hasClass('selectpicker')) {
			$field.selectpicker('refresh');
		}

		if (jQuery().datepicker && $field.hasClass('datepicker')) { 
			$field.datepicker( "refresh" );
		}

		if ($field.data('caption')) { 
			$field.trigger('update-caption'); 
		}
	}
}


Article.tracks = function(tracks) {

	if (tracks) {

		var container = $('#articleTracks');

		for (var i in tracks) {

			var track = tracks[i];
			var p = $('<p />');

			p.append("<span>"+track.date+"<br />"+track.name+"<br />"+track.company+"</span>"+track.content);

			container.append(p);
		}
	}
}

Article.getAction = function(button) {
	var btn = $('<a />');
	btn.attr(button.attributes);
	btn.data(button.data);
	btn.html(button.title);
	btn.prepend(button.icon);
	return btn;
}

Article.submitAction = function(button, data, callback) {

	var url = button.prop('href');

	if (!url) {
		 Adomat.Notification.error('Action URL is not defined.');
		 return false;
	}

	Adomat.Ajax.post(url, data).done(function(xhr) {
			
		xhr = xhr || {};

		if (xhr.notifications) {
			$.each(xhr.notifications, function(i, message) {
				Adomat.Notification.show(message);
			})
		}

		if (!xhr.success) {
			return false;
		}

		button.remove();

		if (xhr.redirect) {
			window.location=xhr.redirect;
		}

		if (xhr.state) {
			Article.breadcrumps(xhr.state);
		}

		if (xhr.remove || xhr.disable) {
			Article.visibility(xhr.remove, xhr.disable);
		}

		if (xhr.buttons) {
			$.each(xhr.buttons, function(i, button) {
				var btn = Article.getAction(button);
				$('#actions').prepend(btn);
			});
		}

		if (typeof callback === 'function') {
			callback();
		}
	})
}

Article.visibility = function(remove, disable) { 

	if (disable) {
		$.each(disable, function(field, val) {
			$('#'+field).prop('disabled', true);
		})
	}

	if (remove) {
		$.each(remove, function(i,field) {
			
			$(field).remove();
			
			if (field=='.tpl-file-remover') {
				$('.input-group-tpl-file').addClass('disabled');
				$('input', $('.input-group-tpl-file')).prop('readonly', true);
			}
		})
	}
}

Article.validate = function(self) {

	var validator = self.data('validate')
	var value = self.val();

	if (value) {
		
		switch (validator) {

			case 'url':
				
				var regex = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/gi.test(value);
				
				if (!regex) {
					var field = self.prop('placeholder') || self.prop('id');
					Adomat.Notification.error(field+" is wrong. The URL should be http://www.yourdomain.com (url params are permitted)");
					return false;
				}

			break;
		}
	}

	return true;
}


Article.settings = function(field, settings) {

	if (field.length && typeof settings === 'object') {
		
		// class name
		field.addClass(settings.className);
		delete settings.className;
		
		// data attributes
		field.data(settings.data);
		delete settings.data;

		field.prop(settings);
	}
}


$(document).ready(function() {

	var $article = $('#news_article_id'),
		$articleField = $('.article-field'),
		$sections = $('#news_section_id'),
		$categories = $('#news_article_category_id'),
		$articleAuthor = $('#news_article_author_id'),
		$articleContents = $('#article-contents'),
		$templatesContainer = $("#templates-container"),
		$dropdownList = $('.dropdown-list'),
		$editors = [],
		$croppers = [],
		$tables = [];

	Adomat.Notification.info("Your article will be automatically saved");

	var URL = '/applications/modules/news/article/article.php';

	// popovers
	$('body').popover({
		selector: '.has-popover',
		container: 'body',
		trigger: 'hover'
	})

	// toggle error class for reqired fields
	$(document).on('change', '[required]', function(e) {
		$(this).parent().toggleClass('has-error', !$(this).val() ? true : false);
	});

	// toggle error class for reqired fields
	$(document).on('change', 'input[type=email]', function(e) {
		
		var val = $(this).val();
		
		if (val) {
			var re = /\S+@\S+\.\S+/; 
			$(this).parent().toggleClass('has-error', !re.test(val) ? true : false);
		}
	});

	// on change table headers
	$(document).on('change', 'input.tbl-header', function(e) {
		
		var self = $(this);

		Adomat.Ajax.post('/applications/modules/news/article/tmp.table.php', {
			application: 'news',
			section: 'header-save',
			id: $article.val(),
			tpl: self.data('tpl'),
			col: self.data('col'),
			value: self.val()
		})
	});

	/*
	$("#accordions").sticky({
	 	topSpacing: 120,
	 	bottomSpacing: 100,
	 	getWidthFrom: '.sidebar',
	 	responsiveWidth: 1199
	 });

	$('#accordions').on('shown.bs.collapse', function () {
		$("#accordions").sticky('update');
	})
	*/


	// wysiwyg editors  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


	// dropdown controlls ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	if (jQuery().selectpicker) { 
		$('.selectpicker').selectpicker();
	}

	$articleAuthor.on('change', function() {

		var self = $(this);

		if (self.val()=='new') {
			authorModal.open();
			return false;
		}
	})

	// update caption holder from select picker
	$('#show-confirmators').on('click', function() {
		$('#news_articles_confirmed_user_id').next().find('button.selectpicker').trigger('click');
		$('#news_articles_confirmed_user_id').selectpicker('show');
	});

	// datapicker ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	

	$( "input.datepicker" ).datepicker({
		showOn: "both", 
      	buttonText: '<i class="fa fa-calendar"></i>',
      	dateFormat: 'dd.mm.yy'
	});


	// sort content ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$templatesContainer.sortable({
      placeholder: "news-article-template-container ui-state-highlight",
      handle: ".tpl-order",
      items: "> .news-article-template-container",
      forcePlaceholderSize: true,
      update: function( event, ui ) { 

      		var data = $(this).sortable('toArray', {attribute: 'data-id'});
      		
      		Adomat.Ajax.post(URL, {
				application: 'news',
				section: 'template-sort',
				id: $article.val(),
				sort: data
			}).done(function(xhr) {
				if (xhr && xhr.success) {
					span.remove();
				}
			}) 
      }
    });

    $templatesContainer.disableSelection();


    // sticker position ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    

    $('.sticker').on('click', function(e, attributes) {

		e.preventDefault();

		var self = $(this);

		if ($('.selected', self).hasClass('active')) {
			var win = window.open('/news/articles/preview/'+$article.val(), '_blank');
  			win.focus();
  			return;
		}

		$('.stickers .selected').removeClass('active');
		$('.selected', self).addClass('active');
		
		// update db
		if (!attributes) { 
			
			var host = window.location.origin.toString();
			var bgImage = self.css('background-image');
			var background = bgImage.replace(host, '');

			submitSticker({
				sticker: self.data('id'),
				article: $article.val(),
				application: 'news',
				section: 'submit',
				left: "0px",
				top: "0px",
				width: self.width(),
				height: self.height(),
				'background-image': background
			})

		}

		return;
	})

	var submitSticker = function(data, callback) {

		$.post('/applications/modules/news/article/sticker.php', data, function(xhr) {
			
			if (xhr && xhr.notifications) {
				for (var i in xhr.notifications) {
					Adomat.Notification.show(xhr.notifications[i]);
				}
			}

			if (typeof callback === 'function') {
				callback(xhr);
			}

			var win = window.open('/news/articles/preview/'+data.article, '_blank');
  			win.focus();

		}, 'json')
	}

    /* 
	$('.sticker').on('click', function(e, attributes) {

		e.preventDefault();
		$('#teaser-container .article-sticker').remove();

		var self = $(this);
		var teaserContainer = $('#teaser-container');
		var sticker = self.clone().addClass('article-sticker');

		$('.stickers .selected').removeClass('active');
		$('.selected', self).addClass('active');
		$('.selected', sticker).addClass('active');
		$('.selected i', sticker).removeClass('fa-check-circle-o').addClass('fa-times-circle-o');

		// for manuallay triggering
		// update attributes from db
		if (attributes && typeof attributes === 'object') {
			sticker.css(attributes);
		}
		
		// add sticker to teaser
		sticker.appendTo(teaserContainer);
		sticker = $('.article-sticker', teaserContainer);
		
		// update db
		if (!attributes) {
			
			var host = window.location.origin.toString();
			var bgImage = sticker.css('background-image');
			var background = bgImage.replace(host, '');

			submitSticker({
				sticker: sticker.data('id'),
				article: $article.val(),
				application: 'news',
				section: 'submit',
				left: sticker.position().left,
				top: sticker.position().top,
				width: sticker.width(),
				height: sticker.height(),
				'background-image': background
			})
		}
		
		sticker.draggable({
			containment: 'parent',
			stop: function(e, ui) {
				
				
				var left = ui.position.left;
				var top = ui.position.top;

				if (left > 0) {
					var width = parseInt(teaserContainer.width());
					var pos = (parseInt(left)/width)*100;
					left = pos.toFixed(2);
				}

				if (top > 0) {
					var height = teaserContainer.height();
					var pos = (parseInt(top)/height)*100;
					top = pos.toFixed(2);
				}

				submitSticker({
					sticker: sticker.data('id'),
					article: $article.val(),
					application: 'news',
					section: 'submit',
					left: ui.position.left+'px',
					top: ui.position.top+'px',
					oleft: left,
					otop: top
				})
			}
		}).resizable({
			containment: 'parent',
			stop: function(e, ui) {
				submitSticker({
					sticker: sticker.data('id'),
					article: $article.val(), 
					application: 'news',
					section: 'submit',
					width: ui.size.width,
					height: ui.size.height
				})
			}
		})

		return;
	})

	$(document).on('click', '.article-sticker .selected.active', function(e) {

		e.preventDefault();

		var sticker = $(this).closest('.article-sticker');

		submitSticker({
			article: $article.val(),
			application: 'news',
			section: 'delete'
		}, function(xhr) {
			if (xhr && xhr.success) {
				sticker.remove();
				$('.stickers .selected').removeClass('active');
			}
		})

		return;
	})

	var submitSticker = function(data, callback) {

		$.post('/applications/modules/news/article/sticker.php', data, function(xhr) {
			
			if (xhr && xhr.notifications) {
				for (var i in xhr.notifications) {
					Adomat.Notification.show(xhr.notifications[i]);
				}
			}

			if (typeof callback === 'function') {
				callback(xhr);
			}

		}, 'json')
	}
	*/


    // add new author ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    var authorModal = new BootstrapDialog({
		title: 'Add New Author',
		message: $('<div />').load('/applications/templates/news/article/author.form.php'),
		autodestroy: false,
		buttons: [
			{
				label: 'Cancel',
				cssClass: 'btn-default pull-left text-mutted',
				action: function(dialogItself){
					authorModal.close();
				}
			},
			{
				id: 'btnAddAuthor',
				cssClass: 'btn-primary',
				icon: 'fa fa-check',
				label: 'Add',
				action: function(dialog) {
					
					var $button = this;
					var $content = dialog.getModalBody();

					// trigger required fields
					$content.find('input[required]').trigger('change');

					if ($content.find('.has-error').length) {
						Adomat.Notification.show({
							type: 'error',
							title: 'Error',
							text: 'Red marked fields are mandatory'
						});
					} else {

						dialog.enableButtons(false);
						dialog.setClosable(false);
						$button.toggleSpin(true);

						var data = $content.find('form').serializeArray();
						data.push({name: 'application', value: 'news'});

						Adomat.Ajax.post('/applications/modules/news/author/submit.php', data).done(function(xhr) {
							
							dialog.setClosable(true);
							dialog.enableButtons(true);
							$button.toggleSpin(false);

							if (xhr && xhr.errors) {
								$.each(xhr.errors, function(i, msg) {
									Adomat.Notification.error(msg);
								})
							}

							if (xhr && xhr.id) {

								var id = 'n'+xhr.id;
								var opt = $('<option />').attr('value', id).text(xhr.name).attr('selected', true);
								var group = $('optgroup.group-news');

								if (!group) {
									var group = $('<optgroup />').addClass('group-news').attr('label', 'News Authors');
									$articleAuthor.append(group);
								}

								group.append(opt).val(id);
								$articleAuthor.trigger('change');
								$articleAuthor.selectpicker('refresh');
								dialog.close();
							}
						})
					}
				}
			}
		],
		onhidden: function(dialog){
			
			dialog.getModalBody().find('form').trigger("reset");
			
			if ($articleAuthor.val()=='new') {
				$articleAuthor.val('').selectpicker('refresh');
			}
		}
	})


	// on page load ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$(function() {

		Adomat.Ajax.post('/applications/modules/news/article/article.load.php', {
			application: 'news',
			section: 'load',
			id: $article.val()
		}).done(function(xhr) {

			// assign article data
			if (xhr && xhr.article) {
				
				for (var field in xhr.article) {
					Article.updateField(field, xhr.article[field]);
				}

				// build breadcrumps
				Article.breadcrumps(xhr.article.news_article_publish_state_id);
			}
			
			// template content initialisation
			if (xhr && xhr.templates) {
				
				// assign article templates
				$.each(xhr.templates, function(ph,conntent) {
					var container = $('#'+ph+'-container');
					container.append(conntent);
				})

				// load template files
				if (xhr.files) {
					for (var id in xhr.files) {
						var files = xhr.files[id];
						var titles = xhr.fileTitles ? xhr.fileTitles[id] : [];
						var tpl = $('#article-template-'+id);
						$.each(files, function(i, file) { 
							var title = titles ? titles[file] : '';
							fileRender(tpl, file, title) 
						})
					}
				}
			}

			// article actions
			if (xhr && xhr.buttons) {
				$.each(xhr.buttons, function(i, button) {
					var btn = Article.getAction(button);
					$('#actions').append(btn);
				});
			}

			// article tracks
			Article.tracks(xhr.tracks);

			tinymce.init({
				mode : "specific_textareas",
				selector: 'textarea.news_article_teaser_text',
				editor_selector: "news_article_teaser_text",
				plugins: ["autolink link paste"],
				width: '100%',
				height: 200,
				menubar : false,
				statusbar : false,
				toolbar_items_size: 'small',
				paste_as_text: true,
				autosave_restore_when_empty: false,
				toolbar: "bold italic underline | link | outdent indent",
				setup : function(ed) {
					ed.on('change', function(e) {
						$(ed.getElement()).val(ed.getContent()).trigger('change');
					});
				}
			});	

			// bind template addons
			$('.news-article-template-container').each(function(i,e) {
				var id = $(e).data('id');
				var data = xhr.dataloader ? xhr.dataloader[id] : {};
				iniTemplate($(e), data)
			})

			// settings
			if (xhr.settings) {
				$.each(xhr.settings, function(el,settings) {
					Article.settings($('.'+el), settings)
				})
			}

			$( "#news_article_expiry_date" ).datepicker( "option", "minDate", 0 );

			if (xhr.sticker) {
				var stickerData = xhr.sticker;
				var i = xhr.sticker.id;
				delete stickerData['id'];
				$('.sticker.sticker-'+i).trigger('click', stickerData);
			}

			// visibility
			Article.visibility(xhr.remove, xhr.disabled);
		})
	})


	// article action ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$(document).on('click', '#actions .btn:not(disabled):not(.preview)', function(e) {

		e.preventDefault();

		var $this = $(this);
		var id = $article.val();
		var url = $this.prop('href');

		// reject action
		if ($this.prop('id')=='btnReject') {
			rejectModal.open();
			return false;
		}

		if ($this.hasClass('btn-dialog')) {

			var msg = $this.data('message') || 'Are your sure?';
		
			BootstrapDialog.confirm(msg, function(confirmed) {
				if (confirmed) {
					Article.submitAction($this, {
						application: 'news',
						id: $article.val(),
					});	
				}
			})

			return false;
		}

		Article.submitAction($this, {
			application: 'news',
			id: $article.val(),
		});	

		return false;
	})


	// checkbox list controls ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	function updateChackBoxes(entity, callback) {

		if (!entity || !entity.data('section')) {
			Adomat.Notification.error('ERROR: Section not defined.');
			return false;
		}

		var items = $('input.article-checkbox', entity).map(function(){
			return $(this).data('id');
		}).get();

		var selected = $('input.article-checkbox:checked', entity).map(function(){
			return $(this).data('id');
		}).get();

		if (!items) {
			Adomat.Notification.error('Action section not defined.');
			return false;
		}

		Adomat.Ajax.post(URL, {
			application: 'news',
			id: $article.val(),
			section: entity.data('section'),
			items: items,
			selected: selected
		}).done(function(xhr) {

			if (xhr && xhr.error) {
				Adomat.Notification.error(xhr.error);
				return false;
			}

			if (xhr && xhr.success) {
				$('input.check-all', entity).trigger('changed');
			}

			if (callback && typeof callback === 'function') {
				callback(xhr);
			}
		})
	}

	// check controll for singel clicks
	$('.checkbox-list input.article-checkbox').on('click', function(e) {
		
		e.stopImmediatePropagation();

		var panel = $(this).closest('.panel-body');
		
		updateChackBoxes(panel);
	})

	// check controll for all clicks
	$('.checkbox-list input.check-all').on('click', function(e) {
		
		e.stopImmediatePropagation();

		var panel = $(this).closest('.panel-body');
		var checked = $(this).is(':checked');

		$('input.article-checkbox', panel).prop('checked', checked);
		
		updateChackBoxes(panel);
	})

	// check controll for group clicks
	$('.checkbox-list input.check-group').on('click', function(e) {
		
		e.stopImmediatePropagation();
		
		var panel = $(this).closest('.panel-body');
		var entitiy = $(this).data('group');
		var checked = $(this).is(':checked');

		$('input.'+entitiy, panel).prop('checked', checked);

		$(this).closest('.group-container').siblings().filter(':hidden').slideDown();
		
		updateChackBoxes(panel);
	})

	$('.checkbox-list input.check-all').on('changed', function(e) {

		var panel = $(this).closest('.panel-body');

		// controll select all button
		var items = $('input.article-checkbox', panel).map(function() { return $(this).data('id') }).get().length;
		var selected = $('input.article-checkbox:checked', panel).map(function() { return $(this).data('id') }).get().length;
		$('input.check-all', panel).prop('checked', selected>0 && items==selected ? true : false);
		$('.total-entity', panel.closest('.panel')).text(selected || '');

		// check group boxes
		$('input.check-group', panel).each(function(i,e) {
			
			var groupBox = $(e);
			var groupName = groupBox.data('group');
			var items = $('input.'+groupName, panel).map(function() { return $(this).data('id') }).get().length;
			var selected = $('input.'+groupName+':checked', panel).map(function() { return $(this).data('id') }).get().length;
			
			groupBox.prop('checked', selected>0 && items==selected ? true : false);

			var groupContainer = groupBox.closest('.group-container');
			$('.total-group', groupContainer).text(selected ? '('+selected+')' : '');
		})

	}).trigger('changed');

	// hide group items
	$('.checkbox-list .group-container').each(function() {
		$(this).siblings().not('.group-container').slideToggle();
	})

	$('.checkbox-list .group-container').on('click', function(e) { 
		$(this).siblings().not('.group-container').slideToggle();
	})

	// add template contents :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$('.tmp-add').on('click', function(e) {

		e.preventDefault();

		var self = $(this);

		Adomat.Ajax.post(URL, {
			application: 'news',
			section: 'add-template',
			id: $article.val(),
			template: self.data('id')
		}).done(function(xhr) {

			if (xhr && xhr.template && xhr.container) {
				
				$('#'+xhr.container+'-container').append(xhr.template);
				
				var tpl = $('#article-template-'+xhr.id);
				
				// add focus to first text box
				$('input[type=text]', tpl).focus();

				// init template addons
				iniTemplate(tpl);
			}
		})

		return false;
	})


	// remove template contents ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$(document).on('click', '.tpl-remover', function(e) {

		e.preventDefault();

		var self = $(this);
		var tpl = self.closest('.news-article-template-container');

		Adomat.Ajax.post(URL, {
			application: 'news',
			section: 'remove-template',
			id: $article.val(),
			template: tpl.data('id')
		}).done(function(xhr) {

			if (xhr && xhr.success) {

				$('textarea.editor', tpl).each(function(i,el) {
					var editor = $(el).attr('id');
					if (tinymce.get(editor)) tinymce.remove('#'+editor);
				})

				tpl.remove();
			}
		})

		return false;
	})

	// change article field ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$(document).on('change', '.article-field', function(e) { 

		var self = $(this);
		var value = self.val();

		if (self.is(':checkbox')) {
			value = self.is(':checked') ? 1 : 0;
		}

		if (self.data('validate') && !Article.validate(self) ) {
			return false;
		}

		Adomat.Ajax.post(URL, {
			application: 'news',
			section: 'article-field-change',
			id: $article.val(),
			field: self.attr('name'),
			value: value
		}).done(function(xhr) {
			
			if (xhr && xhr.success) {

				if ( self.data('caption')) {
					self.trigger('update-caption');
				}

				if (self.hasClass('selectpicker')) {
					self.selectpicker('refresh');
				}

				if (xhr.data) {
					for(var field in xhr.data) {
						Article.updateField(field, xhr.data[field]);
					}
				}
			}
		}) 
	})

	// change article template field :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$(document).on('change', '.template-field', function(e) { 

		var self = $(this);
		var tpl = self.closest('.news-article-template-container');
		var value = self.val();

		if (self.is(':checkbox')) {
			value = self.is(':checked') ? 1 : 0;
		}

		if (self.data('validate') && !Article.validate(self) ) {
			return false;
		}

		Adomat.Ajax.post(URL, {
			application: 'news',
			section: 'article-tpl-field-change',
			id: $article.val(),
			template: tpl.data('id'),
			field: self.data('name') || self.attr('name'),
			value: value
		}).done(function(xhr) {
			
			if (xhr && xhr.success) {

				if ( self.data('caption')) {
					self.trigger('update-caption');
				}

				if (self.hasClass('selectpicker')) {
					self.selectpicker('refresh');
				}
			}
		}) 
	})

	// update field placeholder targets ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$('[data-caption]').on('update-caption', function() {

		var self = $(this);
		var field = self.prop('tagName');
		var target = $(self.data('caption'));
		var value = self.val();

		if (field=='INPUT' || field=='TEXTAREA') {
			target.text(value);
		}

		if (field=='SELECT') { 
			var caption = $('option:selected', self).text();
			target.text(caption);
		}
	});


	// template file handler :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$(document).on('click', '.tpl-file-download', function(e) {

		var self = $(this);
		var file = self.closest('.input-group-tpl-file').data('file');

		if (file) {
			window.open(file);
		} else Adomat.Notification.error('The file is not defined.');
	})

	$(document).on('click', '.tpl-file-remover', function(e) {

		var self = $(this);
		var tpl = $(this).closest('.news-article-template-container');
		var fileGroup = self.closest('.input-group-tpl-file');
		var file = fileGroup.data('file');

		if (file) {
			Adomat.Ajax.post(URL, {
				application: 'news',
				section: 'remove-tpl-files',
				id: $article.val(),
				template: tpl.data('id'),
				file: file
			}).done(function(xhr) {
				if (xhr && xhr.success) {
					fileGroup.remove();
				}
			})

		} else Adomat.Notification.error('The file is not defined.');
	})

	$(document).on('change', '.tpl-file-title', function(e) {

		var self = $(this);
		var tpl = $(this).closest('.news-article-template-container');
		var fileGroup = self.closest('.input-group-tpl-file');
		var file = fileGroup.data('file');
		var title = self.val();

		if (!title) {
			self.val(basename(file));
			Adomat.Notification.error('The file title is not defined.');
			return;
		}

		Adomat.Ajax.post(URL, {
			application: 'news',
			section: 'title-tpl-files',
			id: $article.val(),
			template: tpl.data('id'),
			file: file,
			title: title
		}).done(function(xhr) {
			if (xhr && xhr.success) {
				
			}
		})
	})



	// article reject ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var rejectModal = new BootstrapDialog({
		title: 'Comment',
		message: $('<div />').load('/applications/templates/news/article/reject.form.php'),
		autodestroy: false,
		buttons: [
			{
				label: 'Cancel',
				cssClass: 'btn-default pull-left text-mutted',
				action: function(dialogItself){
					rejectModal.close();
				}
			},
			{
				id: 'btnAddComment',
				cssClass: 'btn-primary',
				icon: 'fa fa-thumbs-down',
				label: 'Reject',
				action: function(dialog) {
					
					var $button = this;
					var $content = dialog.getModalBody();

					// trigger required fields
					$content.find('textarea[required]').trigger('change');

					if ($content.find('.has-error').length) {
						Adomat.Notification.error('Red marked fields are mandatory');
					} else {

						dialog.enableButtons(false);
						dialog.setClosable(false);
						$button.toggleSpin(true);

						var data = $content.find('form').serializeArray();
						data.push({name: 'application', value: 'news'});
						data.push({name: 'id', value: $article.val()});

						Article.submitAction($('#btnReject'), data, function() {
							dialog.setClosable(true);
							dialog.enableButtons(true);
							$button.toggleSpin(false);
							dialog.close();
						});
					}
				}
			}
		],
		onhidden: function(dialog){
			dialog.getModalBody().find('form').trigger("reset");
		}
	})


	// bind template events ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	function iniTemplate(tpl, data) {

		var id = tpl.prop('id');
		var data = data || {};
		var tplHandsonTable = $('.table-handsontable', tpl);

		// select pickers
		$('select.selectpicker').each(function(i,e) {
			
			$(e).selectpicker('refresh');
			
			if ($(e).is(':disabled')) {
				$(e).closest('.dropdown-selectpicker').addClass('disabled');
			}
		})

		// image cropers
		$('input.field-crop', tpl).each(function(i,e) {
			
			var self = $(e);
			var value = self.val();

			if (self.is(':disabled')) {
				if (value) {
					var img = $('<img />').addClass('img-responsive').attr('src', value);
					$('.image-cropper', tpl).append(img);
				}
			} else {
				var field = self.attr('id');
				$croppers[field] = new Cropper(field);
				$croppers[field].loadImage(value);
			}
		})

		// datapickers
		$('input.datapicker', tpl).each(function(i,e) {
			$(e).unbind('change');
			$(e).bootstrapDP('update', Date.parse(value, "dd.MM.yyyy")); 
			$(e).bind('change');
		})

		// wysiwyg editors
		$('textarea.editor', tpl).each(function(i,e) {
			var editor = $(e).attr('id');
			addEditor(editor);
		})

		// ajax file uploaders
		$('input.tpl-files', tpl).each(function(i,e) { 
			var file = $(e).attr('id');
			addFileUploader(file);
		})

		if (tplHandsonTable.length > 0) {

			//Handsontable.Dom.addClass('table table-hover table-striped');
			
			var settings = $.extend({
				startRows: 2,
				startCols: 2,
				minSpareCols: 0,
				minSpareRows: 1,
				rowHeaders: false,
				colHeaders: true,
				manualColumnResize: true,
				manualRowResize: true,
				contextMenu: ['col_right', "---------", 'remove_row', 'remove_col']
			}, data.settings);

			// table builders
			$tables[id] = new Handsontable(tplHandsonTable[0], settings);
			
			if (data.sources) { 
			
				var array = [];

				for (var i in data.sources) {
					var row = data.sources[i];
					var arr = new Array();
					var length = data.settings ? Object.keys(data.settings.colHeaders).length : arr.length;
					for (var j=0; j< length; j++) arr[j] = row[j];
					array.push(arr);
				}
				
				$tables[id].loadData(array);
				$tables[id].render();
			}

			// update context menu
			$tables[id].updateSettings({
				contextMenu: {
					items: {
						"col_right": {
							disabled: function () {
								var selection = $tables[id].getSelected();
								var totalCols = $tables[id].countCols();
								return selection[1]==totalCols-1 ? false : true;
							}
						},
						"hsep1": "---------",
						"remove_row": {},
						"remove_col": {}
					}
				}
			})

			// events
			$tables[id].updateSettings({
				
				afterChange: function(changes, action) { 
					
					if (action=='edit') {
						for (var i in changes) {
							Adomat.Ajax.post('/applications/modules/news/article/tmp.table.php', {
								application: 'news',
								section: 'data-save',
								id: $article.val(),
								tpl: tpl.data('id'),
								row: changes[i][0],
								col: changes[i][1],
								value: changes[i][3],
							})
						}
					}
				},
				afterCreateCol: function(index, amount) { 

					var instance = this;

					Adomat.Ajax.post('/applications/modules/news/article/tmp.table.php', {
						application: 'news',
						section: 'add-col',
						id: $article.val(),
						tpl: tpl.data('id'),
						col: index,
						value: instance.getColHeader(index)
					})
				},
				afterRemoveCol: function(index, amount) {
					
					Adomat.Ajax.post('/applications/modules/news/article/tmp.table.php', {
						application: 'news',
						section: 'remove-col',
						id: $article.val(),
						tpl: tpl.data('id'),
						col: index
					})
				},
				afterRemoveRow: function(index, amount) {
					
					Adomat.Ajax.post('/applications/modules/news/article/tmp.table.php', {
						application: 'news',
						section: 'remove-row',
						id: $article.val(),
						tpl: tpl.data('id'),
						row: index
					})
				},
				afterGetColHeader: function (col, TH) {

					if (col == -1) {
					    return;
					}
					var instance = this;
					// create input element
					var input = document.createElement('input');
					    input.type = 'text';
					    input.className = 'tbl-header form-control';
					    input.setAttribute("id", 'header-'+col);
					    input.setAttribute("data-col", col);
					    input.setAttribute("data-tpl", tpl.data('id'));
					    input.value = TH.firstChild.textContent;

					TH.appendChild(input);

					Handsontable.Dom.addEvent(input, 'change', function (e){
					    var headers = instance.getColHeader();
					        headers[col] = input.value;
					    instance.updateSettings({
					        colHeaders: headers
					    });
					});

					TH.style.position = 'relative';
					TH.firstChild.style.display = 'none';
				}
			})
		}

		// refresh template orders
		$templatesContainer.sortable( "refresh" );
	}


	// croper instance :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	function Cropper(el) {

		var file = $('#'+el);
		var tpl = file.closest('.news-article-template-container')
		var container = $('.image-cropper', tpl);

		return new Croppic(container.attr('id'), {
			uploadData: {
				path: '/data/news/articles/'+$article.val()+'/'+tpl.data('id'),
				filename: file.data('filename') || '',
				minWidth: 320,
				minHeight: 240
			},
			cropData: {
				path: '/data/news/articles/'+$article.val()+'/'+tpl.data('id'),
				filename: file.data('filename') || ''
			},
			onAfterImgUpload: function(self, response) {
				
				response = response || {};
				
				if (response.success && response.url) {
					file.val(response.url).trigger('change');
				}

				if (response.error) {
					Adomat.Notification.error(response.error);
				}
			},
			onAfterImgCrop: function(self, response) {
				
				response = response || {};
				
				if (response.success && response.url) {
					file.val(response.url).trigger('change');
				}

				if (response.error) {
					Adomat.Notification.error(response.error);
				}
			}
		})
	}

	// wysiwyg editor instance :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	
	var addEditor = function(editor, config) {
		//if (!tinymce.get(editor)) {
			tinymce.init({
				mode : "specific_textareas",
				selector: '#'+editor,
				editor_selector: '#'+editor,
				plugins: ["advlist autolink lists link visualblocks code nonbreaking contextmenu paste preview"],
				width: '100%',
				height: 200,
				menubar : false,
				statusbar : false,
				toolbar_items_size: 'small',
				autosave_restore_when_empty: false,
				paste_as_text: true,
				toolbar: "styleselect | bold italic underline | forecolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent  |  removeformat",
				style_formats: [
					{title: 'Title', block: 'h4', classes: 'news-title'},
					{title: 'Small Text', inline: 'span', classes: 'news-text-sm'},
					{title: 'xSmall Text', inline: 'span', classes: 'news-text-xs'}
				],
				content_css : "/public/css/news.content.css?" + new Date().getTime(),
				setup : function(ed) {
					ed.on('change', function(e) {
						$(ed.getElement()).val(ed.getContent()).trigger('change');
					});
				}
			})
			//tinymce.EditorManager.execCommand('mceAddEditor', true, editor);
		//}
	}
	

	// file uploader instance ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	var addFileUploader = function(file) {

		var self = $('#'+file);
		var tpl = self.closest('.news-article-template-container')
		var extensions = $('input[name=extensions]', tpl).val();
		var path = '/data/news/articles/'+$article.val()+'/'+tpl.data('id')+'/';
	
		// file type extensions
		if (extensions) {
			var exts = extensions.split(/[\s,]+/);
			extensions = new RegExp('(\.|\/)('+exts.join('|')+')$');
		}

		self.fileupload({
			url: URL,
			dataType: 'json',
			formData: {
				application: 'news',
				section: 'add-tpl-files',
				id: $article.val(),
				template: tpl.data('id'),
				path: path
			},
			acceptFileTypes: extensions,
			submit: function (e) {
				$('.progress', tpl).show(500);
			},
			done: function (e, data) {

				setTimeout(function() { 
					$('.progress', tpl).hide(500);
				}, 2000);
				
				$.each(data.result.files, function (index, file) {
					fileRender(tpl, path+file.name);
				})
			},
			progressall: function (e, data) {
				var progress = parseInt(data.loaded / data.total * 100, 10);
				$('.progress-bar', tpl).css('width', progress + '%');
			}
		})
		.prop('disabled', !$.support.fileInput)
		.parent().addClass($.support.fileInput ? undefined : 'disabled');
	}


	// utilities :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	function fileRender(tpl, file, title) {

		var self = $('input.tpl-files', tpl),
			title = title || basename(file),
			render = self.data('render') || '';
		
		switch (render) {

			case 'images':
				var div = $('<div />').addClass('tpl-file-img');
				var img = $('<img />').addClass('img-responsive').attr('src', file);
				div.append('<a href="#" class="template-control template-control tpl-file-remover" data-file="'+file+'" ><i class="fa fa-times-circle"></i></a>');
				div.append(img);
				$('.files', tpl).append(div);
			break;

			/*
			default:
				var span = $('<span />');
				span.addClass('label label-default').html('<a href='+file+' target="_blank" >'+title+'</a>')
				span.prepend('<i class="fa fa-times-circle tpl-file-remover" data-file="'+file+'" ></i>');
				$('.files', tpl).append(span);
			break;
			*/

			default:
				var div = $('<div />').addClass("input-group input-group-sm input-group-tpl-file");
				div.data('file', file);
				div.append('<span class="input-group-addon tpl-file-download"><i class="fa fa-download"></i></span>');
				div.append('<input class="form-control tpl-file-title" value="'+title+'" maxlength="50" />');
				div.append('<span class="input-group-addon tpl-file-remover"><i class="fa fa-times"></i></span>');
				$('.files', tpl).append(div);
			break;
		}
	}

	function basename(path) {
		var str = path.split('/').reverse()[0];
		return str.substr(0,str.lastIndexOf('.'))
	}



	// remove empty articles or set article title on unload ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var myBeforeUnload = window.attachEvent || window.addEventListener;
	var chkevent = window.attachEvent ? 'onbeforeunload' : 'beforeunload';

	myBeforeUnload(chkevent, function(e) {
		
		Adomat.Ajax.json('/applications/modules/news/article/unload.php', {
			application: 'news',
			id: $article.val()
		})

		if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
			var e = e || window.event;
			if (e) e.returnValue = ''; // For IE and Firefox
			return '';
		}
	})

})