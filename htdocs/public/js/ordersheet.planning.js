
function sum(list) {
	var sum = 0;
	for (var i = 0; i < list.length; i++) sum += list[i];
	return sum;
}

$(document).ready(function() {

	var spreadsheet,
		application = $('#application').val(),
		controller = $('#controller').val(),
		action = $('#action').val(),
		ordersheet = $('#ordersheet').val(),
		formFilters = $('#formFilters'),
		readonly = $('#readonly').val() || 'false',
		checkApprovedQuantities = $('#check_approved_quantities').val(),
		fixedNotSelectable = ($('#fixedNotSelectable').val()) ? false : true,
		IS_SAP_RESET = false;
	

	retailnet.loader.init();
	
	
//	spreadsheet loader :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	retailnet.loader.show();
	
	var url = '/applications/modules/ordersheet/planning/load.php';
	var data = $('form.request, #filters').serialize();
	var xhr = retailnet.ajax.request(url,data);
	var grid = xhr.data ? retailnet.json.normalizer(xhr.data) : {};
	
	var spreadsheet = new Spreadsheet('#spreadsheet', grid, {
		readonly: false,
		autoHeight: false,
		fixedPartsNotSelectable: true,
		hiddenAsNull: true,
		fixTop: xhr.top,
		fixLeft: xhr.left,
		merge: xhr.merge ? eval(xhr.merge) : {},
		onModify: function(data) {
			retailnet.notification.hide();
			updateSpreadsheet(data);
		}
	});
	
	// add focus
	spreadsheet.focus();

	// hide grid
	retailnet.loader.hide();
	

//	spreadsheet data history :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	var dataindex = [];

	$(".spreadsheet .main .cel").each(function() {
		var id = $(this).attr('id');
		var index = id.substr(12);
		var value = $(this).text();
		dataindex[index] = (value) ? Number(value) : 0;
	});
	
//	spreadsheet manipulation :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	$(document).on('click', '.row-group.postype', function(e) {

		if ($('.row-group.postype').length > 2) {

			var self = $(this);

			var tr = $(this).closest('tr');
			var type = $('td:first span', tr).data('type');
			var row = $('td:first span', tr).data('row');
			var rows = $('.type_'+type);
			
			if (rows.length > 2) {
				
				$('.spreadsheet-'+row+' td').toggleClass('close');
				$('.arrow_'+type).toggleClass('arrow-down').toggleClass('arrow-right');

				var data = [];

				$.each(rows, function(i,elem) {
					data.push($(elem).data('row'));
				});

				data.splice(0,1);
				
				if (self.hasClass('close')) spreadsheet.hideRows(data);
				else spreadsheet.showRows(data);
			}
		}
		
	});
	
	
//	tooltip ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	$('.infotip').tipsy({
		html: true,
		gravity: 'nw',
		live: true,
		title: function() {
			
			if ($(this).attr('data-title')) {
				return $(this).attr('data-title');
			} else {
			
				var id = $(this).attr('data');
				var section = $(this).attr('rel');
				var selector = section+'-'+id;
				var elem = $('#'+selector);
	
				if (elem.text().length) {
					return elem.html();
				} else {
					$('.image-loader').trigger('load-data', [this]);
					return $('.image-loader').html();
				}
			}
		}
	});

	// load tooltip content from ajax request
	$('.image-loader').on('load-data', function(event, tooltip) {

		var id = $(tooltip).attr('data');
		var section = $(tooltip).attr('rel');
		var selector = section+'-'+id;

		
		retailnet.ajax.json('/applications/helpers/ajax.tooltip.php',{ 
			application: application,
			section: section,
			id: id 
		}).done(function(xhr) {
			
			if(xhr && xhr.content) {
				
				$('.tooltips-container').append('<div id="'+selector+'" >'+xhr.content+'</div>');
				
				if ($(tooltip).is(':hover')) {
					$(tooltip).tipsy("show");
				}
			}
		});
	});


//	new warehouses :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var newWarehouseContainer = $('#add_warehouse_dialog');
	
    var modalWareHouse = newWarehouseContainer.adoModal({
		width: 400,
		clickClose: false,
		button: '#add_warehouse',
		afterClose: function() {
			
			$('.new-warehouse-container', newWarehouseContainer).hide();
			$('#address_warehouse_id').val('');
			$('input', newWarehouseContainer).val('');
			$('.error', newWarehouseContainer).removeClass('error');
		
			modalWareHouse.resize({height:200});
			retailnet.loader.hide();
			retailnet.notification.hide();
		}
	});

	$(document).on('change','#address_warehouse_id', function(e) {
		
		var val = $(this).val();

		$('.new-warehouse-container').toggle(val=='new' ? true : false); 

		var h = $('.sap-fields', newWarehouseContainer).length > 0 ? 360 : 260;
		var height = val=='new' ? h : 200;

		modalWareHouse.resize({height:height});
	});

	$('a.apply', newWarehouseContainer).on('click', function(e) {
	
		e.preventDefault();
		
		retailnet.notification.hide();
		
		var warehouseForm = $('form', newWarehouseContainer);
		var dropdown = $('#address_warehouse_id');
		var container = $('.new-warehouse-container');		
		var required = $('.required:visible', container);
		var failures = $('.required[value=]:visible', container);

		// remove error marks
		$('.error', newWarehouseContainer).removeClass('error');
		
		// dropdown
		if (!dropdown.val()) {
			dropdown.addClass('error');
			retailnet.notification.error('Please select Warehouse from list.');
			return false;
		}
		
		if (required.length > 0 && failures.length > 0) {
    		$('.required[value=]', newWarehouseContainer).addClass('error');
    		retailnet.notification.error('Red marked fields are required.');
		} else {
		
			var data = warehouseForm.serializeArray();
			data.push({name:'application', value: application});
			data.push({name:'ordersheet', value: ordersheet});
			data.push({name:'section', value: 'add'});
			
			retailnet.loader.show();

			retailnet.ajax.json(warehouseForm.attr('action'), data).done(function(xhr) {
				
				retailnet.loader.hide();
				
				if (xhr && xhr.response) {
					window.location.reload();
				} 
				
				if (xhr && xhr.message) {
					if (xhr.response) retailnet.notification.success(xhr.message);
					else retailnet.notification.error(xhr.message);
				}
				
				retailnet.modal.close();
			});
		}

		return false;
	});


// remove warehose dialog ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


	var removeWarehouseDialogContainer = $('#remove_warehouse_dialog');
    
	var removeWarehouseDialog = removeWarehouseDialogContainer.adoModal({
		width: 400,
		clickClose: false,
		afterClose: function() {
			$('a.apply', removeWarehouseDialogContainer).removeAttr('href');
			retailnet.loader.hide();
			retailnet.notification.hide();
		}
	});

	//remove warehouse dialog
	$(document).on('click', '.remove-warehouse', function(e) {
	
		e.preventDefault();
		
		retailnet.notification.hide();
	
		var self = $(this);
		var warehouse = $(this).data('warehouse');
	
		if (warehouse) {
			$('a.apply', removeWarehouseDialogContainer).attr('data-warehouse', warehouse);
			removeWarehouseDialog.show();
		} else {
			retailnet.notification.error('Warehouse is not defined.');
		}

		return false;
	});

	$('a.apply', removeWarehouseDialogContainer).on('click', function(e) {

		e.preventDefault();

		var self = $(this);
		var warehouse = self.data('warehouse');
		
		if (warehouse) {
			
			retailnet.ajax.json('/applications/helpers/ordersheet.warehouse.php', {
				application: application,
				ordersheet: ordersheet,
				warehouse: warehouse,
				section: 'delete'
			}).done(function(xhr) {
				
				if (xhr && xhr.response) {
					window.location.reload();
				}
				
				if (xhr && xhr.message) {
					retailnet.notification.warning(xhr.message);
				}

				removeWarehouseDialog.close();
			});
		}

		return false;
	});


// confirm dialog ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var confirmDialogContainer = $('#confirm_dialog'); 

	var modalConfirmDialog = confirmDialogContainer.adoModal({
		width: 400,
		clickClose: false,
		button: '#confirm'
	});


	$('a.apply', confirmDialogContainer).on('click', function(e) {

		e.preventDefault();

		retailnet.notification.hide();

		var button = $('#confirm');
		var url = button.attr('href');
		var data = $('form.request').serialize();
		
		retailnet.loader.show();
		
		retailnet.ajax.json(url, data).done(function(xhr) {
			
			retailnet.loader.hide();
			retailnet.modal.close();

			modalConfirmDialog.close();

			// reload page on success
			if (xhr && xhr.response) {

				if (xhr.redirect) {
					window.location = xhr.redirect;
				}

				if (xhr.reload) {
					window.location.reload();
				}
			}

			// show request message
			if (xhr && xhr.message) {
				if (xhr.response) retailnet.notification.success(xhr.message);
				else retailnet.notification.error(xhr.message);
			}
		});
	});


// activate ordersheet :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var activateDialogContainer = $('#activateDialog'); 

	var modalActivateDialog = activateDialogContainer.adoModal({
		width: 400,
		clickClose: false,
		button: '#activate'
	});


	$('a.apply', activateDialogContainer).on('click', function(e) {

		e.preventDefault();
		retailnet.notification.hide();

		var button = $('#activate');
		var url = button.attr('href');
		var data = $('form.request').serialize();
		
		retailnet.loader.show();
		
		retailnet.ajax.json(url, data).done(function(xhr) {
			
			xhr = xhr || {};
			
			retailnet.loader.hide();
			retailnet.modal.close();

			modalActivateDialog.close();

			if (xhr.success) {
				if (xhr.redirect) window.location = xhr.redirect;
				if (xhr.reload) window.location.reload();
			}

			if (xhr.errors) {
				$.each(xhr.errors, function(i, message) {
					retailnet.notification.error(message);
				})
			}

			if (xhr.messages) {
				$.each(xhr.messages, function(i, message) {
					retailnet.notification.messages(message);
				})
			}
		})
	});


// partially distribution dialog :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	var partiallyDistributionDialogContainer = $('#partially_distribution_dialog'); 

	var partiallyDistributionDialog = partiallyDistributionDialogContainer.adoModal({
		width: 400,
		clickClose: false,
		button: '#partially_distribution'
	});

	$('a.apply', partiallyDistributionDialogContainer).on('click', function(e) {

		e.preventDefault();

		retailnet.notification.hide();

		var button = $('#partially_distribution');
		var url = button.attr('href');
		var data = $('form.request').serialize();
		
		retailnet.loader.show();
		
		retailnet.ajax.json(url, data).done(function(xhr) {
			
			retailnet.loader.hide();
			retailnet.modal.close();

			// reload page on success
			if (xhr && xhr.response) {

				if (button.hasClass('popup_close')) {
					
					if (window.opener) {
						window.opener.location.reload();
					}
					
					window.close();
					
				} else {
					window.location.reload();
				}

				partiallyDistributionDialog.close();
			}

			// show request message
			if (xhr && xhr.message) {
				if (xhr.response) retailnet.notification.success(xhr.message);
				else retailnet.notification.error(xhr.message);
			}
		});
	});


// reset exported quantities :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


	var exportResetContainer = $('#export-reset'); 

	var exportResetModal = exportResetContainer.adoModal({
		width: 720,
		clickClose: false
	})

	$(document).on('click', '.export-reset', function(e) {

		e.preventDefault();
		e.stopPropagation();

		var self = $(this);
		var item = self.data('item');
		var itemCode = $('.code', self.closest('td')).text();

		IS_SAP_RESET = false;

		$('.ado-subtitle', exportResetContainer).data('item', item);
		$('.ado-subtitle', exportResetContainer).text(itemCode);

		retailnet.notification.hide();

		retailnet.ajax.json('/applications/modules/ordersheet/planning/sap.item.reset.quantities.php', {
			application: application,
			ordersheet: ordersheet,
			item: item
		}).done(function(xhr) {

			xhr = xhr || {};

			if (xhr.errors) {
				
				$.each(xhr.errors, function(i, message) {
					retailnet.notification.error(message);
				})
				
				return false;
			}

			if (!xhr.data) {
				retailnet.notification.warning("Item " + itemCode + " hasn't exported quantities.");
				self.parent().remove();
				return false;
			}

			var table = $("<table />").addClass('table table-hover table-striped');
			var thead = $("<thead />");
			var tbody = $("<tbody />");

			// header
			var tr = $("<tr />");

			// cel pos btn
			var th = $("<th />");
			tr.append(th.html("&nbsp;"));

			// cel pos name
			var th = $("<th />");
			tr.append(th.html("&nbsp;"));

			// thead
			$.each(xhr.headers, function(i, row) {
				
				var th = $("<th />");
				th.addClass('cel-reset sap-reset sap-date col-'+i);
				th.prop('colspan', 3);
				th.prop('title', "Reset all quantities exported on this date")
				th.data('id', i).data('action', 'item');
				th.html(row.caption);
				tr.append(th);

				// separator
				var th = $("<th />").prop('class', 'cel-separator');
				th.html("&nbsp;");
				tr.append(th);
			})

			thead.append(tr);
			
			// thead
			table.append(thead);
	
			// tbody
			$.each(xhr.data, function(pos, row) {
				
				var tr = $("<tr />");

				// btn pos reset
				var btn = $("<span />");
				btn.prop('class', 'btn btn-xs btn-danger btn-reset sap-reset sap-pos');
				btn.data('id', pos);
				btn.data('action', 'pos');
				btn.html('<i class="fa fa-times"></i>');
				
				// cel pos btn
				var td = $("<td />");
				td.append(btn);
				tr.append(td);

				// cel pos caption
				var td = $("<td />");
				tr.append(td.append(row.pos));

				for (var i in xhr.headers) {

					var item = row.quantities[i] || {}

					// cel distributed
					var td = $("<td />").prop('class', 'cel-distributed');
					td.html(item.distributed);
					tr.append(td);

					// cel confirmed
					var td = $("<td />").prop('class', 'cel-confirmed');
					td.html(item.confirmed);
					tr.append(td);

					// cel reset
					var td = $("<td />").prop('class', 'col-'+i);
					td.data({id: item.id, pos: pos, stamp: i, action: 'quantity'})

					if (item.reset) {
						td.addClass('cel-reset sap-reset sap-quantity');
						td.prop('title', "Reset quantity")
					}

					td.html(item.reset);
					tr.append(td);

					// separator
					var td = $("<td />").prop('class', 'cel-separator');
					td.html("&nbsp;");
					tr.append(td);
				}
				
				tbody.append(tr);
			})

			table.append(tbody); 

			$('.ado-modal-body', exportResetContainer).empty().append(table);

			exportResetModal.show();

		})

		return false;
	})

	// reset distribution quantity
	$(document).on('click', '.sap-reset', function(e) {

		e.preventDefault();
		e.stopPropagation();

		var self = $(this);
		var action = self.data('action');
		var item = $('.ado-subtitle', exportResetContainer).data('item');

		retailnet.ajax.json('/applications/modules/ordersheet/planning/sap.item.reset.submit.php', {
			application: application,
			ordersheet: ordersheet,
			item: item,
			action: action,
			id: self.data('id')
		}).done(function(xhr) {
			
			xhr = xhr || {}

			if (xhr.success) {
				IS_SAP_RESET = true;
			}

			if (xhr.errors) {
				
				$.each(xhr.errors, function(i, message) {
					retailnet.notification.error(message);
				})
				
				return false;
			}

			if (xhr.success && xhr.reload) {
				window.location.reload();
			}

			if (xhr.notifications) {
				$.each(xhr.notifications, function(i, message) {
					retailnet.notification.success(message);
				})
			}

			switch (action) {

				case 'quantity':
					self.trigger('after.reset');
				break;

				case 'pos':
					var row = self.closest('tr');
					$('.sap-reset', row).trigger('after.reset');
				break;

				case 'item':
					var stamp = self.data('id');
					$('td.sap-reset.col-'+stamp).trigger('after.reset');
				break;

				case 'all':
					window.location.reload();
				break;
			}

			// remove reset all button
			if (!$('td.sap-quantity', exportResetContainer).length) {
				$('a.sap-reset-all', exportResetContainer).remove();
			}
		})

		return false;
	})

	$(document).on('after.reset', '.sap-reset', function(e) {

		var cel = $(this);
		var row = cel.closest('tr'); 
		var stamp = cel.data('stamp');

		// remove cel reset trigger
		cel.removeClass('cel-reset sap-quantity');
		cel.html("&nbsp;"); 

		// row hasn't reset quantities
		// remove pos reset button
		if (!$('.sap-quantity', row).length) {
			$('.btn-reset', row).remove();
		}

		// col hasn't reset quantities
		// remove col reset trigger
		if (!$('td.sap-quantity.col-'+stamp).length) {
			$('th.col-'+stamp).removeClass('cel-reset');
		}
	})

	// reset distribution quantity
	$(document).on('click', '.sap-reset-close', function(e) {
		
		e.preventDefault();
		e.stopPropagation();

		if (IS_SAP_RESET) window.location.reload();
		else exportResetModal.close();

		retailnet.notification.hide();

		return false;
	})


// update order sheet items ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


	$('a.submit').on('click', function(e) {

		e.preventDefault();

		retailnet.notification.hide();

		var button = $(this);
		var url = button.attr('href');
		var data = $('form.request').serialize();
		
		retailnet.loader.show();
		
		retailnet.ajax.json(url, data).done(function(xhr) {
			
			retailnet.loader.hide();
			retailnet.modal.close();

			// reload page on success
			if (xhr && xhr.response) {

				if (button.hasClass('popup_close')) {
					
					if (window.opener) {
						window.opener.location.reload();
					}
					
					window.close();
					
				} else {
					window.location.reload();
				}
			}

			// show request message
			if (xhr && xhr.message) {
				if (xhr.response) retailnet.notification.success(xhr.message);
				else retailnet.notification.error(xhr.message);
			}
		});
	});

	
//	filters ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	// build dropdown UI skins
	$('select', $('#filters')).dropdown();

	// confirmation versions
	$('#version').change(function() {
		var value = $(this).val();
		var url = $('#url').val();
		var link = (value) ? url+'/'+value : url;
		window.location = link;
	});

	// reload page when filters are changed
	$('select', $('#filters')).change(function() {
		$('#filters').submit();
	});

	var popFilter = $('#pop_filter');
	
	popFilter.pop().click(function() {
		$('.icon',this).toggleClass('direction-up');
	});

	popFilter.pop('hide');

	
//	spreadsheeet updater :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
	var updateSpreadsheet = function(data) {
		
		if (data) {

			var messages = [];
			messages[0] = "Please only integer numbers";
			messages[1] = "The quantity must be greater than zero";
			messages[2] = "Total quantity in planning exeeds aproved quantity.";
			messages[3] = "Total quantity in distribution exeeds shipped quantity.";
			messages[4] = "Total unconfirmed quantity in distribution exeeds adjusted quantity.";
				
			$.each(data, function(i,cell) {
				
				if (cell && cell.tag != null) { 

					var error = '';
					var value = Number(cell.value);
					
					var tag = cell.tag.split(';'); 
					
					var sectionName = tag[0] || '';		// section name
					var itemID 		= tag[1] || '';		// quantity ID
					var refererID 	= tag[2] || '';		// pos/warehouse ID
					var materialID 	= tag[3] || '';		// material ID
					var celReserve 	= tag[4] || '';		// stock reserve

					if (isNaN(value)) {
						error = messages[0];
					}

					if (!error && value) {
						error = value%1===0 ? '' : messages[0];
					}

					if (!error && value) {
						error = value > 0 ? '' : messages[1];
					}
					
					if (!error && celReserve) {
						
						var v = spreadsheet.getValue(celReserve);
						var totalReserve = Number(v);
						var oldStockReserve = dataindex[celReserve];
						var message = $('#spreadsheet').hasClass('planning') ? messages[2] : messages[3];
						
						error = totalReserve < 0  ? message : '';
						/*
						if (oldStockReserve > 0) { 
							error = totalReserve < 0  ? message : '';
						} else {
							error = totalReserve <= 0 ? message : '';
						}
						*/
					}

					// prevent change adjusted quantity
					// if new value exeeds total sum of not confirmed distributions 
					if (sectionName=='adjusted') {
						var totalSum = spreadsheet.getValue(tag[2]);
						error = totalSum < 0  ? messages[4] : '';
					}

					if (error) {
						spreadsheet.setValue(cell.name, dataindex[cell.name]);
						retailnet.notification.error(error);
					} else if (sectionName) { 

						retailnet.ajax.json('/applications/helpers/ordersheet.item.planning.save.php', {
							application: application,
							controller: controller,
							ordersheet: ordersheet,
							quantity : value,
							section: sectionName, 
							id: itemID,
							referer: refererID,
							material: materialID
						}).done(function(xhr) {
							
							if (xhr && xhr.id) { 

								if (sectionName=='adjusted' && tag[2]==1) {
									window.location.reload();
								}
								
								// store quantitie in dataindex
								dataindex[cell.name] = value;

								// store cell tag in spreadsheet
								// new tag content new inserted ID
								spreadsheet.setValue(cell.name, {
									tag: sectionName+';'+xhr.id+';'+refererID+';'+materialID+';'+celReserve
								})
								
							}

							if (xhr && xhr.error) {
								retailnet.notification.error(xhr.error);
								spreadsheet.setValue(cell.name, dataindex[cell.name]);
							}
						})
					}
				}
			})
		}
	}
	
});