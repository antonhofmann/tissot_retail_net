$(document).ready(function() {

	var form = $("#pos_furniture"),
		dropdownProductLines = $('#mps_pos_furniture_product_line_id'),
		dropdownItems = $('#mps_pos_furniture_item_id'),
		dropdownOldItems = $('#old_items');
	
	// spiner instance	
	retailnet.loader.init();
	
	// tooltips
	retailnet.tooltip.init();
	
	form.validationEngine({
		autoHidePrompt: true,
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		onAjaxFormComplete: function(status, form, json, options) { 

			retailnet.loader.hide();
			retailnet.notification.hide();

			if (json) {

				if (json.response && json.redirect) { 
					window.location=json.redirect;
				}

				if (json.message) {
					if (json.response) retailnet.notification.success(json.message);
					else retailnet.notification.error(json.message);
				}
			}
		}
	});

	// dropdown product lines
	dropdownProductLines.chainedSelect('#mps_pos_furniture_item_id', {
		parameters: {
      		section: 'product_line_items',
      		application: $('#application').val()
      	}
	}).trigger('change');

	// dropdown product lines
	dropdownItems.chainedSelect('#old_items', {
		parameters: {
      		section: 'product_line_old_items',
      		application: $('#application').val()
      	},
      	before: function(target, settings) {
      		settings.status = (dropdownItems.val() == 'new') ? true : false;
      	}
	}).trigger('change');

	// dropdown items
	dropdownItems.change(function() {
		
		var val = $(this).val();
		
		if (val=='new') {
			$('.old_items').toggle(true);
			dropdownOldItems.focus();
		} else {
			$('.old_items').toggle(false);
		}
	});
	
	$("#save").click(function(event) { 
		
		event.preventDefault();
		
		retailnet.notification.hide();
		
		if (form.validationEngine('validate')) {
			retailnet.loader.show();
			form.submit();
		} else {
			retailnet.notification.error('Please check red marked Fields.');
		}
	});

	$('.dialog').click(function(event) {

		event.preventDefault();
		
		var button = $(this);
		
		$('#apply').attr('href', button.attr('href'))
		
		retailnet.modal.dialog('#'+button.attr('id')+'_dialog');
	});

	$('#cancel').bind('click', function(event) {
		event.preventDefault();
		$.fancybox.close();
		return false;
	});	

});