define(function(require, exports, module) {

var Backbone = require('backbone');

/**
 * Base model. Always extend this model instead of Backbone.Model
 * so we can add common behavior later on if necessary.
 */
var BaseModel = Backbone.Model.extend({
});

return BaseModel;

});
