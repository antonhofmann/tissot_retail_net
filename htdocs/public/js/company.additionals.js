$(document).ready(function() {

	var form = $("#company_additionals");
	var id = $('#address_additional_id');
	
	retailnet.loader.init();
	
	form.validationEngine({
		autoHidePrompt: true,
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		onAjaxFormComplete: function(status, form, json, options) { 

			retailnet.loader.hide();
			retailnet.notification.hide();

			if (json) {
				
				if (json.response && json.redirect) {
					window.location = json.redirect;
				}

				if (json.message) {
					if (json.response) retailnet.notification.success(json.message);
					else retailnet.notification.error(json.message);
				}
			}
		}
	});
	
	$("#save").click(function(event) { 
		event.preventDefault();
		retailnet.loader.show();
		form.submit();
		return false;
	});
	
});