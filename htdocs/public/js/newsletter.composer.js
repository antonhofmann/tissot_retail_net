var Newsletter = Newsletter || {};

$(document).ready(function() {

	var CropperImage = $('#newsletter_image'),
		CropperContainer = $('#cropper');

	Newsletter.id = $('#newsletter_id').val();

	// wysiwyg editors  

	tinymce.init({
		selector: 'textarea.editor',
		plugins: ["advlist autolink lists link textcolor visualblocks code nonbreaking contextmenu paste preview"],
		width: '100%',
		height: 300,
		menubar : false,
		statusbar : false,
		toolbar_items_size: 'small',
		autosave_restore_when_empty: false,
		paste_as_text: true,
		toolbar: "styleselect | fontsizeselect | bold italic underline | forecolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent  | link | removeformat",
		style_formats: [
			{title: 'Title', block: 'h4', classes: 'news-title'},
			{title: 'Small Text', inline: 'span', classes: 'news-text-sm'},
			{title: 'xSmall Text', inline: 'span', classes: 'news-text-xs'}
		],
		content_css : "/public/css/news.content.css?" + new Date().getTime(),
		setup : function(ed) {
			ed.on('change', function(e) {
				$(ed.getElement()).val(ed.getContent()).trigger('change');
			});
		}
	})


	// image uploader and cropper
	
	var Path = '/data/news/newsletters/'+Newsletter.id;
	var Filename = CropperImage.data('filename') || '';

	var Cropper = new Croppic('cropper', {
		uploadData: {
			path: Path,
			filename: Filename
		},
		cropData: {
			path: Path,
			filename: Filename
		},
		onAfterImgUpload: function(self, response) {
			
			response = response || {};
			
			if (response.success && response.url) {
				CropperImage.val(response.url).trigger('change');
			}

			if (response.error) {
				Adomat.Notification.error(response.error);
			}
		},
		onAfterImgCrop: function(self, response) {
			
			response = response || {};
			
			if (response.success && response.url) {
				CropperImage.val(response.url).trigger('change');
			}

			if (response.error) {
				Adomat.Notification.error(response.error);
			}
		}
	})


	// multiple file uploader

	var FilesContainer = $('#newsletters-files');
	var FilesUploader = $('input[type=file]', FilesContainer);
	var FilesExtensions = ('input[name=extensions]', FilesContainer);
	var FilesProgress = $('.progress', FilesContainer).hide();
	var pathFiles = '/data/news/newsletters/'+Newsletter.id+'/';
	var extensions;

	if (FilesExtensions.val()) {
		var exts = extensions.split(/[\s,]+/);
		extensions = new RegExp('(\.|\/)('+exts.join('|')+')$');
	}

	FilesUploader.fileupload({
		url: Newsletter.url,
		dataType: 'json',
		formData: {
			application: 'news',
			section: 'files',
			id: Newsletter.id,
			path: pathFiles
		},
		acceptFileTypes: extensions,
		submit: function (e) {
			FilesProgress.show(500);
		},
		done: function (e, data) {

			setTimeout(function() { 
				FilesProgress.hide(500);
			}, 2000);
			
			$.each(data.result.files, function (index, file) {
				fileRender($(e.target), pathFiles+file.name);
			})
		},
		progressall: function (e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$('.progress-bar', FilesProgress).css('width', progress + '%');
		}
	})
	.prop('disabled', !$.support.fileInput)
	.parent().addClass($.support.fileInput ? undefined : 'disabled');


	// background file uploader 

	var BackgroundContainer = $('#newsletters-background');
	var FilesUploader = $('input[type=file]', BackgroundContainer);
	var FilesExtensions = ('input[name=extensions]', BackgroundContainer);
	var FilesProgress = $('.progress', BackgroundContainer).hide();
	var pathBG = '/data/news/newsletters/'+Newsletter.id+'/background/';
	var extensions;

	if (FilesExtensions.val()) {
		var exts = extensions.split(/[\s,]+/);
		extensions = new RegExp('(\.|\/)('+exts.join('|')+')$');
	}

	FilesUploader.fileupload({
		url: Newsletter.url,
		dataType: 'json',
		formData: {
			application: 'news',
			section: 'background',
			id: Newsletter.id,
			path: pathBG
		},
		acceptFileTypes: extensions,
		submit: function (e) {
			FilesProgress.show(500);
		},
		done: function (e, data) {

			setTimeout(function() { 
				FilesProgress.hide(500);
			}, 2000);

			$('#newsletters-background .files-list').empty();
			
			$.each(data.result.newsletter_background, function (index, file) {
				fileRender($(e.target), pathBG+file.name);
			})
		},
		progressall: function (e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$('.progress-bar', FilesProgress).css('width', progress + '%');
		}
	})
	.prop('disabled', !$.support.fileInput)
	.parent().addClass($.support.fileInput ? undefined : 'disabled');


	// on page load 
	
	$(function() {

		Adomat.Ajax.post(Newsletter.url, {
			application: 'news',
			section: 'load',
			id: Newsletter.id
		}).done(function(xhr) {

			xhr = xhr || {};

			if (xhr.data) {
				// form dataloader
				Newsletter.dataloader(xhr.data)
				// breadcrumps builder
				Newsletter.breadcrumps(xhr.data.newsletter_publish_state_id);

				if (xhr.data.newsletter_background) {
					$('input[type="file"]', $('#newsletter-background')).val()
					fileRender($('#newsletter_background'), xhr.data.newsletter_background);
				}
			}

			// article actions
			if (xhr.buttons) {
				Newsletter.actions(xhr.buttons);
			} else {
				
				$('.newsletter-title').removeClass (function (index, css) {
				    return (css.match (/(^|\s)col-\S+/g) || []).join(' ');
				}).addClass('col-xs-12');
				
				$('.newsletter-actions').hide();
			}

			// update editors
			$('textarea.editor').each(function(i,e) {
				
				var editor = $(e);
				
				if (tinymce.get( editor.attr('id') )) {
					tinyMCE.get(editor.attr('id')).setContent(editor.val());
				}
			})
			
			// update cropper instance
			if (CropperImage.val()) {

				var image = CropperImage.val();

				if (CropperImage.is(':disabled')) {
					var img = $('<img />').addClass('img-responsive').attr('src', image);
					CropperContainer.append(img);
				} else {
					Cropper.loadImage(image);
				}
			}

			// load template files
			if (xhr.files) {
				for (var el in xhr.files) {
					var files = xhr.files[el];
					$.each(files, function(i, row) { 
						fileRender($('#'+el), row.file, row.title);
					})
				}
			}

			// settings
			if (xhr.settings) {
				$.each(xhr.settings, function(el,settings) {
					Newsletter.settings($('.'+el), settings)
				})
			}


			// user dependent visibility
			Newsletter.visibility(xhr.disabled, xhr.remove);
			
		})
	})
	

	// save field value 
	$(document).on('change', '.field', function(e) { 

		var self = $(this);
		var value = self.val();

		if (self.is(':checkbox')) {
			value = self.is(':checked') ? 1 : 0;
		}

		Adomat.Ajax.post(Newsletter.url, {
			application: 'news',
			section: 'save',
			id: Newsletter.id,
			field: self.attr('name'),
			value: value
		}).done(function(xhr) {
			if (xhr.success && self.prop('id')!='newsletter_title' && !$('#newsletter_title').val()) {
				$('#newsletter_title').val('Untitled Newsletter').trigger('change');
			}
		})
	})

	// remove newsletter file 

	/*
	$(document).on('click', '#newsletters-files .file-remover', function(e) {

		e.preventDefault();

		var self = $(this);
		var file = self.data('file');

		if (file) {
			Adomat.Ajax.post(Newsletter.url, {
				application: 'news',
				section: 'file-remove',
				id: Newsletter.id,
				file: file
			}).done(function(xhr) {
				
				if (xhr && xhr.success) {
					self.parent().remove();
					$('input[type="file"]', $('##newsletters-files')).val()
				}

				if (xhr && xhr.error) {
					Adomat.Notification.error(xhr.error);
				}
			})

		} else Adomat.Notification.error('The file is not defined.');

		return;
	})
	*/

	$(document).on('click', '.tpl-file-download', function(e) {

		e.preventDefault();

		var self = $(this);
		var file = self.closest('.input-group-tpl-file').data('file');

		if (file) {
			window.open(file);
		} 
		else Adomat.Notification.error('The file is not defined.');
	})

	$(document).on('click', '.tpl-file-remover', function(e) {

		e.preventDefault();

		var self = $(this);
		var fileGroup = self.closest('.input-group-tpl-file');
		var file = fileGroup.data('file');

		if (file) {
			Adomat.Ajax.post(Newsletter.url, {
				application: 'news',
				section: 'file-remove',
				id: Newsletter.id,
				file: file
			}).done(function(xhr) {
				if (xhr && xhr.success) {
					fileGroup.remove();
					$('input[type="file"]', $('##newsletters-files')).val()
				}
			})

		} else Adomat.Notification.error('The file is not defined.');
	})

	$(document).on('change', '.tpl-file-title', function(e) {

		var self = $(this);
		var fileGroup = self.closest('.input-group-tpl-file');
		var file = fileGroup.data('file');
		var title = self.val();

		if (!title) {
			self.val(basename(file));
			Adomat.Notification.error('The file title is not defined.');
			return;
		}

		Adomat.Ajax.post(Newsletter.url, {
			application: 'news',
			section: 'file-title',
			id: Newsletter.id,
			file: file,
			title: title
		}).done(function(xhr) {
			if (xhr && xhr.success) {
				
			}
		})
	})

	// remove newsletter file 
	$(document).on('click', '#newsletters-background .file-remover', function(e) {

		e.preventDefault();

		var self = $(this);
		var file = self.data('file');

		if (file) {
			Adomat.Ajax.post(Newsletter.url, {
				application: 'news',
				section: 'background-remove',
				id: Newsletter.id,
				file: file
			}).done(function(xhr) {
				
				if (xhr && xhr.success) {
					self.parent().remove();
				}

				if (xhr && xhr.error) {
					Adomat.Notification.error(xhr.error);
				}
			})

		} else Adomat.Notification.error('The file is not defined.');

		return;
	})


	// file render
	
	function fileRender(el, file, title) {

		var container = el.closest('.ajax-files-container');
		var render = el.data('render') || '';
		var title = title || basename(file);
		var filename = file ? file.split('/').reverse()[0] : 'Undefined file name';
		
		switch (render) {

			case 'images':
				var div = $('<div />').addClass('file-img');
				var img = $('<img />').addClass('img-responsive').attr('src', file);
				div.append('<a href="#" class="file-remover" data-file="'+file+'" ><i class="fa fa-times-circle"></i></a>');
				div.append(img);
				$('.files-list', container).append(div);
			break;

			/*
			default:
				var span = $('<span />');
				span.addClass('label label-default').html('<a href='+file+' target="_blank" >'+filename+'</a>')
				span.prepend('<i class="fa fa-times-circle file-remover" data-file="'+file+'" ></i>');
				$('.files-list', container).append(span);
			break; 
			*/

			default:
				var div = $('<div />').addClass("input-group input-group-sm input-group-tpl-file");
				div.data('file', file);
				div.append('<span class="input-group-addon tpl-file-download"><i class="fa fa-download"></i></span>');
				div.append('<input class="form-control tpl-file-title" value="'+title+'" maxlength="50" />');
				div.append('<span class="input-group-addon tpl-file-remover"><i class="fa fa-times"></i></span>');
				$('.files-list', container).append(div);
			break;
		}
	}

	function basename(path) {
		var str = path.split('/').reverse()[0];
		return str.substr(0,str.lastIndexOf('.'))
	}

	// unload newsletter module
	var myBeforeUnload = window.attachEvent || window.addEventListener;
	var chkevent = window.attachEvent ? 'onbeforeunload' : 'beforeunload';

	myBeforeUnload(chkevent, function(e) {
		
		Adomat.Ajax.json('/applications/modules/news/newsletter/unload.php', {
			application: 'news',
			id: Newsletter.id
		})

		if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
			var e = e || window.event;
			if (e) e.returnValue = ''; // For IE and Firefox
			return '';
		}
	})

})