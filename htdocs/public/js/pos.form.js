$(document).ready(function() {

	// constants
	var application = $('#application').val(),
		pos = $('#posaddress_id').val(),
		syncronCompany = $('#add_from_company').val();
	
	// vars
	var form = $("#posform"),
		country = $('#posaddress_country'),
		province = $('#province'),
		newProvince = $('#new_province'),
		place = $('#posaddress_place_id'),
		newPlace = $('#new_place'),
		client = $('#posaddress_client_id'),
		owner = $('#posaddress_franchisee_id'),
		syncronBox = $('#posaddress_identical_to_address'),
		geodataBox = $('.geobox');
		geodata = $('.geodata');
		
	// spiner instance	
	retailnet.loader.init();
	
	// tooltip instance	
	retailnet.tooltip.init();

	// hide onload
	geodataBox.hide();
	$('.posaddress_identical_to_address').hide();

	
	var validator = form.validationEngine({
		autoHidePrompt: true,
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		onAjaxFormComplete: function(status, form, json, options) { 

			retailnet.loader.hide();
			retailnet.notification.hide();

			if (json) {

				if (json.response && json.redirect) { 
					window.location=json.redirect;
				}

				if (json.message) {
					if (json.response) retailnet.notification.success(json.message);
					else retailnet.notification.error(json.message);
				}
			}
		}
	});
	
	$("#save").click(function(e) { 

		e.preventDefault();
		retailnet.notification.hide();
		province.removeClass('error');
		
		var elements = $('input:visible,textarea:visible,select:visible');

		if (province.is(':visible') && !province.val()) {
			province.addClass('error');
		}

		geodata.trigger('blur');

		var validate = form.validationEngine('validate');
		var hasErrors = form.find('.error').length > 0 ? true : false;

		$('.input-mask-container').each(function(i,e) {

			var container = $(e);

			// check phone numbers
			if (Validator.hasInputMaskError(container)) {
				hasErrors = true;
			}
		})

		if (hasErrors || !validate) {
			retailnet.notification.error('Please check red marked fields.');
		}
		else if($('#posaddress_store_closingdate').val() == '' 
			    && $('#posaddress_phone_number').val() == '' 
			    && $('#posaddress_mobile_phone_number').val() == ''
		       ) {
			retailnet.notification.error('Please indicate the phone number or the mobile phone number!');
			
		}
		else {
			retailnet.loader.show();
			form.submit();
		}

		retailnet.loader.show();
			form.submit();

		return false;
	});

	// pos client
	client.chainedSelect('#posaddress_franchisee_id', {
		parameters: {
      		section: "company_owners",
      		pos : pos,
      		application: application
      	}
	}).trigger('change'); 

	// pos owner
	owner.change(function() {

		var data = {
        	section : 'hasIdentical',
        	application: application,
        	pos : pos,
        	franchisee : owner.val() 
		};

		var row = $('.posaddress_identical_to_address');

		syncronBox.attr('checked', false);

		if (syncronCompany > 0) { 
			row.show();
			syncronBox.attr('checked',true).trigger('change');
		}
		else if (pos && owner.val()) {
			$.getJSON('/applications/helpers/pos.ajax.php', data, function(json) {
				row.toggle(json.response);
				syncronBox.attr('checked',json.checked).trigger('change');
	        });
		}
		else if (owner.val() > 0) {
			row.show();
		}
	});

	
	// chainde counries
	country.chainedSelect('#posaddress_place_id', {
		parameters: {
      		section: "places"
      	}
	});

	// trigger country onload
	if (country.is('select')) {
		country.trigger('change');
	}

	//textbox for new options
	country.change(function(e) {

		geodataBox.hide();
		newProvince.hide();
  		province.empty();
	
		$.ajax({
			url: '/applications/helpers/ajax.dropdown.php',
			dataType: 'json',
			data: {
				section: 'provinces',
				value: Number(country.val())
			},
			success: function(json) { 
				if (json) {
					for (i = 0; i < json.length; i++) {  
						for ( key in json[i] ) {	
							province.append("<option value="+key+">"+json[i][key]+"</option>");
						}
					}
					province.val($('#posaddress_province_id').val());
				}
        	}
		});
	}).trigger('change');

	// textbox for new options
	province.change(function() {
		
		newProvince
			.toggle(country.val() && $(this).val()=="new")
			.removeClass('error')
			.val('');
		
		newPlace.val('')
			.removeClass('error')
			.val('');

		if ($(this).val() == 'new') newProvince.focus();
		else newPlace.focus();
	});
	
	// textbox for new options
	place.change(function() {
		if (country.val()) {
			geodataBox.toggle($(this).val()=="new");
		}
	});

	// check new geodata fields
	geodata.bind({
		blur: function() {

			retailnet.notification.hide();
			$(this).removeClass('error');
			
			if ($(this).is(':visible') && $(this).hasClass('required') && !$(this).val()) {

				$(this).addClass('error');
				
				if ($(this).attr('alt')) {
					retailnet.notification.error($(this).attr('alt')+' is required.');
				}
			}
		},
		change: function() {

			retailnet.notification.hide();

			var self = $(this);

			$(this).removeClass('error');

			if ($(this).is(':visible')) {

				var data = {
					section: self.attr('id'),
					country: Number(country.val()),
					province: Number(province.val()),
					place: Number(place.val()),
					new_province: newProvince.val(),
					new_place: newPlace.val(),
					value: self.val()
				}
			
				if (self.val()) {
					$.getJSON('/applications/helpers/ajax.dropdown.php', data, function(json) {
						if (json) {
							
							self.toggleClass('error', json.error);
							
							if (json.message) {
								retailnet.notification.error(json.message);
							}
						}
			        });
				} 
			}
		}
	});

	// pos identical to company
	$('#posaddress_identical_to_address').change(function() {
		
		var checked = ($(this).is(':checked')) ? true : false;
		var disabled = (checked) ? 'readonly' : false
		var section = (checked) ? 'getcompany' : 'getpos';
		var id = $('#posaddress_id').val();
		var franchisee = Number($('#posaddress_franchisee_id').val());

		var data = {
			franchisee: franchisee,
			section: section,
			application: $('#application').val(),
			id : id 
		}
		
		if(franchisee > 0) {
			
			$.ajax({
				url: '/applications/helpers/pos.ajax.php',
				dataType: 'json',
				cache: false,
				data: data,
				success: function(json) { 

					if (json) {

						$.each(json, function(field, value) {
							$('#'+field).val(value);
						});

						$('#posaddress_province_id').val(json.posaddress_province_id);
						$('#posaddress_place_id').attr('title', json.posaddress_place_id);
						$('#posaddress_country').val(json.posaddress_country).trigger('change');
					}
	        	}
			});
		}
	});


	// check address for letter cases
	$('#posaddress_address').change(function() {

		var self = $(this);

		var data = {
			section : 'letter_cases',
			value : self.val(),
			application: $('#application').val()
		}

		$.getJSON('/applications/helpers/pos.ajax.php', data, function(json) {

			$('div.jGrowl').find('.jGrowl-close').trigger('jGrowl.close');
			
			if (json) {
				
				self.toggleClass('error', json.error);

				if (json.message) {
					retailnet.notification.error(json.message);
				}
			}
        });
	});


	$('input.datepicker').datepicker({ 
		dateFormat: 'dd.mm.yy',
		showOtherMonths : true,
		firstDay: 1
	});

	// pos export in store locator
	$('#posaddress_export_to_web').change(function() {

		var checked = $(this).is(':checked') ? true : false;
		var openingDate = $('input#posaddress_store_openingdate');
		var error = false;

		if (openingDate.length > 0) {
			
			if (checked) {
				error = (openingDate.val()) ? false : true;
			}

			$(this).toggleClass('error', error);
			$(this).next().toggleClass('error', error);
			$(this).next().toggleClass('error', error);

			openingDate.toggleClass('error', error);
		}

		if (error) {
			retailnet.notification.hide();
			retailnet.notification.error('POS can only be shown in store locator if an opening date is entered.');
		}
		
	});

	// pos identical to company
	$('#posaddress_email_on_web').change(function() {

		var checked = $(this).is(':checked') ? true : false;
		var posEmail = $('input#posaddress_email');
		var error = false;

		if (posEmail.length > 0) {

			// check opening date value
			if (checked) {
				error = (posEmail.val()) ? false : true;
			}

			$(this).toggleClass('error', error);
			$(this).next().toggleClass('error', error);

			posEmail.toggleClass('error', error);
		}
		
		if (error) {
			retailnet.notification.hide();
			retailnet.notification.error('Please indicate the POS E-Mail address.');
		}
		
	});

	$('#posaddress_store_openingdate').change(function() {
		
		var self = $(this);
		var val = $(this).val();
		
		// close all notofications
		retailnet.notification.hide();
		
		if (val) {
			
			

			var arr = val.split('.');
			var openingDate = new Date(arr[2]+'/'+arr[1]+'/'+arr[0]);	
			var now = new Date();

			if (openingDate > now) {
				error = true;
				self.addClass('error');
				retailnet.notification.error('The POS opening date must be a date in the past!');
			}
			else if (Date.parse(val, "dd.mm.YYYY")) {
				self.removeClass('error');
				$('#posaddress_export_to_web').removeClass('error').next().removeClass('error');
			} else {
				self.addClass('error');
				retailnet.notification.error('Invalid date.');
			}
			
		} else {
			$('#posaddress_export_to_web').trigger('change');
		}
	});


	// planned date control
	$('#posaddress_store_planned_closingdate').change(function(e) {

		var self = $(this);
		var error = false;
		var val = self.val();

		var openingDate = $('#posaddress_store_openingdate').is('input') ? $('#posaddress_store_openingdate').val() : $('#posaddress_store_openingdate').text();

		// close all notofications
		retailnet.notification.hide();
		self.removeClass('error');

		if (val) {
			
			if (!Date.parse(val, "dd.mm.YYYY")) {
				error = true;
				self.addClass('error');
				retailnet.notification.error('Invalid date.');
			}	

			val = val.split('.');
			var closingDate = new Date(val[2]+'/'+val[1]+'/'+val[0]);	

			
			if (!error && openingDate) {

				var arr = openingDate.split('.');
				var openingDate = new Date(arr[2]+'/'+arr[1]+'/'+arr[0]);
				
				if (closingDate.compareTo(openingDate)==-1) {
					error = true;
					self.addClass('error');
					retailnet.notification.error('The Planned POS closing date can not be a date in the past of the POS opening date!');
				}
				
			}
		}
		
	});

	// closing date control
	$('#posaddress_store_closingdate').change(function(e) {

		var self = $(this);
		var error = false;
		var val = self.val();

		var openingDate = $('#posaddress_store_openingdate').is('input') ? $('#posaddress_store_openingdate').val() : $('#posaddress_store_openingdate').text();

		// close all notofications
		retailnet.notification.hide();
		self.removeClass('error');

		if (val) {
			
			if (!Date.parse(val, "dd.mm.YYYY")) {
				error = true;
				self.addClass('error');
				retailnet.notification.error('Invalid date.');
			}	

			val = val.split('.');
			var closingDate = new Date(val[2]+'/'+val[1]+'/'+val[0]);	

			
			if (!error && openingDate) {

				var arr = openingDate.split('.');
				var openingDate = new Date(arr[2]+'/'+arr[1]+'/'+arr[0]);	
				
				if (closingDate.compareTo(openingDate)==-1) {
					error = true;
					self.addClass('error');
					retailnet.notification.error('The POS closing date can not be a date in the past of the POS opening date!');
				}
				
			}
			
			if (!error) {
				
				var today = new Date.today();
				var yesterday = new Date().add(-1).day();
				var future = new Date().add(2).day();
				
				if (closingDate.compareTo(yesterday)>0 && !closingDate.between(today, future)) {
					self.addClass('error');
					retailnet.notification.error('The POS closing date must be a date in the past or can only be at maximum two days in the future!');
				}
			}
		}
		
	});

	$('#posaddress_email').change(function() {
		if ($(this).val()) {
			$(this).removeClass('error');
			$('#posaddress_email_on_web').removeClass('error').next().removeClass('error');
		} else {
			$('#posaddress_email_on_web').trigger('change');
		}
	});


	$(".letter-control").click(function() {

		var field = $('input[type=text]:first', $(this).prev())
		var value = field.val();

		if (value) {
			field.val(value.toLowerCase().replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
		}
	});

	$('.address-mask').on('change', function(e) {

		var $this = $(this),
			container = $this.closest('.input-mask-container'),
			value = $(this).val();

		$this.removeClass('error');
		retailnet.notification.hide();

		if (!$('[name="posaddress_street"]').val()) {
			$('.input-mask', container).val('');
			return;
		}
	})

	// phone number mask
	$('.phone-mask').on('change', function(e) {
		Validator.checkPhoneNumber($(this));
	})

	$('[name="posaddress_phone_country"]').on('focus', function() {

		var self = $(this),
			countryID = Number(country.val());

		if (self.val() || !countryID) return;

		$.ajax({
			url: '/applications/helpers/ajax.country.php',
			dataType: 'json',
			data: {
				section: 'get_country_phone_prefix',
				value: countryID
			},
			success: function(xhr) { 
				self.val(xhr.country_phone_prefix);
        	}
		})
	})
	
});