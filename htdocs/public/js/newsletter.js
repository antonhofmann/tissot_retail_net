
var Newsletter = {};
var URL = window.location.href;
var PARAMS = URL.match(/\d+\.?\d*/g);

Newsletter.url = '/applications/modules/news/newsletter/newsletter.php';
Newsletter.id = $('#newsletter_id').val();

// newsletter breadcrumps ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

Newsletter.breadcrumps = function(state) {

	var $this = this;
	var container = $('#breadcrumps');

	if (!$this.states) {
		
		this.states = Adomat.Ajax.json($this.url, {
			application: 'news',
			section: 'workflow-states'
		});

		$this.states = this.states;
	}
	
	container.empty();

	for (var i in $this.states) { 
		var item = $('<li />');
		item.text($this.states[i]);
		if (i<=state) item.addClass('active');
		container.append(item); 
	}
}


// newsletter dataloader :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

Newsletter.dataloader = function(data) {

	if (data) {
		
		for (var field in data) { 

			var el = $('#'+field);
			var value = data[field];
			var type = el.prop('tagName');
			var caption = el.data('caption');
		
			if (type) {

				if (el.is(':text') || el.is(':hidden') || el.is('textarea')) {
					el.val(value);
				}

				if (el.is(':checkbox')) {
					el.attr('checked', value)
				}

				if (el.hasClass('datepicker')) { 
					el.datepicker( "refresh" );
				}

				if (caption) { 
					el.trigger('update-caption'); 
				}
			}
		}
	}
}

// actions builder :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

Newsletter.actions = function(buttons) {

	if (buttons) {
		$.each(buttons, function(i, button) {
			var btn = $('<a />').attr(button.attributes).html(button.title)
			btn.data(button.data);
			btn.prepend(button.icon);
			$('#actions').append(btn);
		})
	}
}

// state submit ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

Newsletter.submitState = function(button) {

	var $this = this;
	var state = button.data('state');
	var url = button.attr('href');

	if (!state) {
		 Adomat.Notification.error('The state is not defined.');
		 return false;
	}

	if (!url) {
		 Adomat.Notification.error('The URL is not defined.');
		 return false;
	}

	Adomat.Ajax.post(url, {
		application: 'news',
		id: Newsletter.id,
		state: state
	}).done(function(xhr) {
		
		xhr = xhr || {};

		if (xhr.success) {

			button.remove();

			if (xhr.remove) {
				$.each(xhr.remove, function(i,e) {
					$(e).remove();
				})
			}

			// add new button
			if (xhr.button) { 
				var btn = $('<a />').attr(xhr.button.attributes).html(xhr.button.title);
				btn.data(xhr.button.data)
				btn.prepend(xhr.button.icon);
				$('#actions').prepend(btn);
			}

			if (typeof $this.breadcrumps === 'function' ) {
				$this.breadcrumps(state);
			}
		}

		if (xhr.notifications) {
			$.each(xhr.notifications, function(i, message) {
				Adomat.Notification.show(message);
			})
		}	
	})
}


// visibility ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

Newsletter.visibility = function(disable, remove) { 

	// disable form
	if (disable) {
		$.each(disable, function(i, field) {
			$('#'+field).attr('disabled', true);
		})
	}

	if (remove) {
		$.each(remove, function(i,field) {
			
			$(field).remove();

			if (field=='.tpl-file-remover') {
				$('.input-group-tpl-file').addClass('disabled');
				$('input', $('.input-group-tpl-file')).prop('readonly', true);
			}
		})
	}
}


// alerts ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

Newsletter.alerts = function(alerts) {

	if (alerts) {

		var container = $('#newsletter-alerts');

		$.each(alerts, function(i, alert) {

			var el = $('<div />').attr('role', 'alert').addClass('alert').addClass(alert.type);

			if (alert.dismissible) {
				var button = $('<button />').addClass('close').data('dismiss', 'alert').attr('aria-label', 'Close');
				button.html("<span aria-hidden=\"true\">&times;</span>");
				el.addClass('alert-dismissible').prepend(button);
			}

			el.append(alert.content);
			container.append(el);
		})
	}
}


// dialog ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


Newsletter.dialogs = function(dialogs) {

	if (dialogs) {

		if (!$('#dialogs-container').length) {
			var div = $('<div />').attr('id', 'dialogs-container');
			$('body').append(div);
		}

		var container = $('#dialogs-container').hide();

		$.each(dialogs, function(i, dialog) {

			var button = $(dialog.button);
			
			if (button.length) {
				var name = button.attr('id');
				var el = $('<div />').attr('id', 'dialog-'+name).html(dialog.content);
				button.data('message', '#dialog-'+name);
				container.append(el);
			}
		})
	}
}


// settings ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

Newsletter.settings = function(field, settings) {

	if (field.length && typeof settings === 'object') {
		
		// class name
		field.addClass(settings.className);
		delete settings.className;
		
		// data attributes
		field.data(settings.data);
		delete settings.data;

		field.prop(settings);
	}
}

// newsletter event handlers :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

$(document).ready(function() {

	Newsletter.id = $('#newsletter_id').val();

	// popovers
	$('body').popover({
		selector: '.has-popover',
		container: 'body',
		trigger: 'hover',
		html : true
	})

	// submit state
	$(document).on('click', '.btn-state:not(.btn-dialog)', function(e) {
		e.preventDefault();
		var self = $(this);
		Newsletter.submitState(self);
		return false;
	})

	// dialogs	
	$(document).on('click', '.btn-dialog', function(e) {

		e.preventDefault();

		var self = $(this);
		var url = self.prop('href') || self.data('url');
		var msg;

		if (self.data('message')) {
			msg = self.data('message');
			msg = $(msg).length>0 ? $(msg).html() : msg;
		} 
		else msg = 'Are your sure?';

		if (!url) {
			Adomat.Notification.error('Action URL is not defined.');
			return false;
		}
		
		BootstrapDialog.confirm(msg, function(confirmed) {
			
			if (confirmed) {

				if (self.hasClass('btn-state')) {
					Newsletter.submitState(self);
				} else {

					Adomat.Ajax.post(url).done(function(xhr) {

						if (xhr && xhr.success && xhr.redirect) {
							window.location=xhr.redirect;
						}

						if (xhr && xhr.notifications) {
							$.each(xhr.notifications, function(i, message) {
								Adomat.Notification.show(message);
							})
						}
					})	
				}	
			}
		})

		return false;
	})

});
