var releaseDate = '20151015';
require.config({
	paths: {
		libs: '/public/scripts',
		jquery: '/public/scripts/jquery/jquery-2.1.4.min',
		backbone: '/public/scripts/backbone/backbone.min',
		underscore: '/public/scripts/underscore/underscore.min',
		mustache: '/public/scripts/mustache/mustache.min',
		text: '/public/scripts/requirejs/text',
		stache: '/public/scripts/requirejs/stache',
		'jquery.helpers': '/public/js/jquery.helpers',
		'jquery.sortable': '/public/scripts/jquery-sortable/jquery.sortable.min',
		'jquery-ui': '/public/scripts/jquery-ui/jquery-ui.min',
		'jquery-ui/colorpicker': '/public/scripts/colorpicker/evol.colorpicker',
		'jqrange-slider': '/public/scripts/jqrange-slider/jQDateRangeSlider-min',
		bootstrap: '/public/scripts/bootstrap/js/bootstrap.min',
		datatables: '/public/scripts/datatables/media/js/jquery.dataTables.min',
		datatablesBootstrap: '/public/scripts/datatables/media/js/dataTables.bootstrap.min',
		datatablesResponsive: '/public/scripts/datatables/extensions/Responsive/js/dataTables.responsive.min',
		tabCollabse: '/public/scripts/tapcollapse/tab.collapse.min',
		i18n: "/public/scripts/requirejs/i18n",
		pnotify: '/public/scripts/pnotify/pnotify.custom.min',
		app: 'app.main'
	},
	shim: {
		Backbone: { deps: ['underscore', 'jquery'] },
		backbone: { deps: ['underscore', 'jquery'] },
		underscore: { exports: '_' },
		'jquery-ui/colorpicker': { deps: ['jquery-ui'] },
		'jqrange-slider': { deps: ['libs/jqrange-slider/lib/jquery-ui.min'] },
		'libs/retailnet': { 
			exports: 'retailnet', 
			deps: ['jquery', 'libs/jgrowl/jgrowl'] 
		},
		bootstrap: {deps: ['jquery'] },
		tabCollabse: {deps: ['jquery'] },
		'libs/quickflip/jquery.quickflip.min': { deps: ['jquery'] },
		'libs/formvalidation/js/framework/bootstrap.min': { deps: ['bootstrap'] }
	},

	deps: [
		'jquery', 
		'bootstrap', 
		'tabCollabse'
	],

	// mustache plugin configuration
	stache: {
		path: '/public/templates/',
		extension: '.tpl'
	},

	// bust cache for new releases
	urlArgs: 'rel=' + releaseDate,

	locale: localStorage.getItem('locale') || 'en-us'
});
