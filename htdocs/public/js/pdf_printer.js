/**
 * This module can print a DOM element to pdf
 *
 * It is currently only used for printing masterplan tables and gantts, but could be
 * used for anything. Modern browsers can actually generate and save pdfs right out
 * of the browser, but our friend IE isn't quite that far so as of now, pdf data must
 * be proxied to the server where it can either be further processed or just sent as a
 * download. The pdf data is sent as base64 to the server, just var_dump $_POST to see
 * what you get on the server side. You can then send save the file on the server or
 * send the appropriate headers for the user to download it.
 *
 * Use like:
 * PdfPrinter.print('#my-fancy-element', {
 *   proxyURL: '/some-url',
 *   fileName: 'mypdf.pdf',
 *   landscape: false,
 *   paperSize: 'A4'
 * });
 */
define(function(require, exports, module) {

	var $ = require('jquery'),
			_ = require('underscore'),
			Backbone = require('backbone'),
			kendoWeb = require('libs/kendo-gantt/js/kendo.web.min'),
			kendoPdf = require('libs/kendo-gantt/js/kendo.pdf.min');

	/**
	 * Convert value to mm
	 * @param  {number} val
	 * @return {number}
	 */
	function mm(val) {
	  return val * 2.8347;
	}

	/**
	 * Get dimensions for papersize
	 * @param  {string} size
	 * @return {object}
	 */
	function getPaperDimensions(size) {
	  var dimensions = {
	    'A4': { w: 210, h: 297 },
	    'A3': { w: 297, h: 420 }
	  };
	  return dimensions[size];
	}

	/**
	 * The actual class
	 */
	var PdfPrinter = {
		MARGIN_X: 25,

		/**
		 * Convert value to mm
		 * @param  {number} val
		 * @return {number}
		 */
		mm: function(val) {
		  return val * 2.8347;
		},

		/**
		 * Get dimensions for papersize
		 * @param  {string} size
		 * @return {object}
		 */
		getPaperDimensions: function(size) {
		  var dimensions = {
		    'A4': { w: 210, h: 297 },
		    'A3': { w: 297, h: 420 }
		  };
		  return dimensions[size];
		},

		/**
		 * Print a DOM element as PDF
		 *
		 * options:
		 * paperSize 				A4 (default) or A3
		 * landscape        true|false (sets page layout)
		 * proxyUrl         URL where to send pdf data to
		 * fileName         The filename*
		 * *you can override the filename on server and use this to pass data from
		 * client to server
		 *
		 * @param {object|string} HTML element, jQuery object or string selector
		 * @param {object} options
		 * @return {void}
		 */
		print: function(element, options) {
			var $el = $(element),
					that = this;

			// set some default options
			var defaults = {
				landscape: true,
				paperSize: 'A4'
			};
			options = _.extend(defaults, options);
			var orientation = options.landscape ? 'landscape' : 'portrait';

			// convert element to canvas with kendo tools, returns a promise
			kendo.drawing.drawDOM($el)
				// then resize the object to the expected page size and return as pdf
			  .then(function(group) {
			    var size = getPaperDimensions(options.paperSize);
			    // this might not work for every case. play around with the dimensions
			    // if you're not happy with the result
			    var PAGE_RECT = new kendo.geometry.Rect(
			      [0, 0], [mm(size.w), mm(size.h) - 20]
			    );

			    var content = new kendo.drawing.Group();
			    content.append(group);
			    kendo.drawing.fit(content, PAGE_RECT);

			    // convert the drawing to pdf
			    return kendo.drawing.exportPDF(content, {
			      paperSize: options.paperSize,
			      landscape: options.landscape,
			      margin: {
			        left: that.MARGIN_X,
			        right: that.MARGIN_X
			      }
			    });
			  })
			  // send the pdf data to proxy url
			  // note: modern browsers could actually just download this data
			  .done(function(data) {
			    kendo.saveAs({
			      dataURI: data,
			      // comment this out to enable client-side pdf downloading for browsers who support it
			      forceProxy: true,
			      // this will only work if the url doesn't have a ? in it
			      proxyURL: options.proxyURL + '?orientation=' + orientation,
			      fileName: options.fileName
			    });

			    // we have no way to determine when printing is finsihed (except for playing around with cookies and longpolling, which seems overkill)
			    // so we'll just say we're finished after 1.5s
			    setTimeout(function() { that.trigger('afterPrint') }, 1500);
			  });
		}
	};

	// add backbone's event capabilities
	_.extend(PdfPrinter, Backbone.Events);
	return PdfPrinter;
});