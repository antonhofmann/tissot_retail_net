$(document).ready(function() {

	var form = $("form.launchplan");

	// wysiwyg
	tinymce.init({
	    selector: "textarea.tinymce",
	    plugins: [
			"advlist autolink lists link visualblocks code nonbreaking contextmenu paste preview"
	    ],
	    width: 480,
	    height: 200,
	    menubar : false,
	    statusbar : false,
	    toolbar_items_size: 'small',
		autosave_restore_when_empty: false,
	    toolbar: "fontsizeselect | bold italic underline | forecolor | alignleft aligncenter alignright alignjustify | removeformat | paste pastetext pasteword | preview",
	    setup : function(ed) {
			ed.on('change', function(e) {
				$('#ordersheet_comment').val(ed.getContent())
			});
		}
	});
	
	// spiner instance	
	retailnet.loader.init();
	
	// tooltip instance	
	retailnet.tooltip.init();

	$('#versionCancel').click(function(event) {
		event.preventDefault();
		retailnet.modal.close();
		return false;
	});

	$('#versionApply').click(function(event) {
		if ($(this).hasClass('disabled')) {
			return false;
		}
	})

	// version name box
	$('#version_name').on({
		change: function() {

			var title = $(this).val();
			var url = $('#version_url').val();

			if ($(this).val()) {
				$('#versionApply').removeClass('disabled')
				.attr('href', url+'&title='+title.replace(' ','+'));
			}
		}
	})
	 
	$('#version_name').one('keypress', function(e){
		var value = String.fromCharCode(e.which);
		if (value) {
			$('#versionApply').removeClass('disabled');
		}
	});

});