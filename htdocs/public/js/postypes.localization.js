// save localized province data

$(document).on('change', 'input[class^="postype_name_"]', function(e) {
		
		var self = $(this);

		
		retailnet.ajax.json('/applications/modules/postype/localization/ajax_submit.php', {
			application: $('#application').val(),
			language_id:self.closest('td').data('language_id'),
			postype_id: self.attr('index'),
			postype_name: self.val(),
			store_locator_country_id: $('#store_locator_country_id').val()
		})

});




