$(document).ready(function() {

	var UploadContainer = $('#fileUploader'),
		UploadForm = $('form', UploadContainer),
		application = $('input.application', UploadForm).val(),
		id = $('input.id', UploadForm).val();
	
	
	// loader initialise
	retailnet.loader.init();
	
	// show modal screen
	/*
	$('.upload-modal').click(function(e) {
		
		e.preventDefault();
		e.stopImmediatePropagation();

		var self = $(this);
		var target = self.attr('href');
		var options = {};
		
		if (self.attr('modal-width') && self.attr('modal-height')) {
			options.autoDimensions = false;
			options.width = Number(self.attr('modal-width'));
			options.height = Number(self.attr('modal-height'));
		}
		
		// reload page on close
		options.onClosed = function() {
			window.location.reload();
		}
		
		if ($('tbody.files tr.template-download', $(target)).length == 0 ) {
			$('.btn.delete', $(target)).hide();
		}
		
		retailnet.modal.show(target, options);

		return false;

	});
	*/
	
	
	
	// start upload files
	$('button.uploader', UploadContainer).click(function(e) {
		
		e.preventDefault();
		e.stopImmediatePropagation();
		
		retailnet.notification.hide();
    	
    	var required = $('input.required', UploadForm);
    	var failures = $('input.required[value=]', UploadForm);
    	
    	$('input.required', UploadForm).removeClass('has-error');
   
    	
    	if (required.length > 0 && failures.length > 0) {
    		
    		$('input.required[value=]', UploadForm).addClass('has-error');
    		
    		retailnet.notification.show('Red marked fields are required.', {
    			sticky: true,
    			theme: 'error'
    		});
    		
    	} else {	
    		
    		var data = UploadForm.serializeArray();
			data.push({name: 'section', value: 'save'});
			
			retailnet.loader.show();
    		
    		retailnet.ajax.json(UploadForm.attr('action'), data).done(function(xhr) {
    			
	    		retailnet.loader.hide();
	    		
	    		if (xhr && xhr.reload) {
					window.location.reload();
	        	}
	    		
				if (xhr && xhr.notification) {
					retailnet.notification.show(xhr.notification.content, eval(xhr.notification.properties));
				}
	        });
    	}

		return false;
	});
	

});