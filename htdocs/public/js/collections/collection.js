define(function(require, exports, module) {

var Backbone = require('backbone');

/**
 * Base collection. Always extend this collection instead of Backbone.Collection
 * so we can add common behavior later on if necessary.
 */
var BaseCollection = Backbone.Collection.extend({
  url: '/administration/masterplantemplates',
  
  /**
   * Check if our collection has a model by id or cid
   * @param  {string} id or cid
   * @return {Boolean}
   */
  has: function(id) {
    var model = this.get(id);
    return !(typeof(model) == 'undefined');
  }
});

return BaseCollection;

});
