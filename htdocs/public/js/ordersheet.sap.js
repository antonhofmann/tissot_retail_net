
function sum(list) {
		
	var sum = 0;
	for (var i = 0; i < list.length; i++) {
		sum += list[i];
	}
	
	return sum;
}

$(document).ready(function() {

	var spreadsheet,
		application = $('#application').val(),
		controller = $('#controller').val(),
		action = $('#action').val(),
		ordersheet = $('#ordersheet').val(),
		formFilters = $('#formFilters'),
		readonly = $('#readonly').val() || 'false',
		checkApprovedQuantities = $('#check_approved_quantities').val(),
		fixedNotSelectable = ($('#fixedNotSelectable').val()) ? false : true,
		dataindex = [];
	
	
/** spreadsheet loader ********************************************************************************************************************/
	
	retailnet.loader.show();
	
	var url = '/applications/modules/ordersheet/sap/spreadsheet.php';
	var data = $('form.request, #filters').serialize();
	var xhr = retailnet.ajax.request(url,data);
	var grid = xhr.data ? retailnet.json.normalizer(xhr.data) : {};
	
	var spreadsheet = new Spreadsheet('#spreadsheet', grid, {
		readonly: false,
		autoHeight: false,
		fixedPartsNotSelectable: true,
		hiddenAsNull: true,
		fixTop: xhr.top,
		fixLeft: xhr.left,
		merge: xhr.merge ? eval(xhr.merge) : {},
		onModify: function(data) {
			retailnet.notification.hide();
		}
	});
	
	// add focus
	spreadsheet.focus();

	// hide grid
	retailnet.loader.hide();
	

/** spreadsheet manipulation *************************************************************************************************************/

	
	// Data history indexing
	$(".spreadsheet .main .cel").each(function() {
		var id = $(this).attr('id');
		var index = id.substr(12);
		var value = $(this).text();
		dataindex[index] = (value) ? Number(value) : 0;
	});
	
	// column toggler
	$(document).on('click', '.row-group.postype', function(e) {

		if ($('.row-group.postype').length > 2) {

			var self = $(this);
			var type = $('span', self).attr('type');
			var row = $('span', self).attr('row');
			var rows = $('.type_'+type);
			
			if (rows.length > 2) {
				
				//self.toggleClass('close');
				$('.spreadsheet-'+row+' td').toggleClass('close');
				$('.arrow_'+type).toggleClass('arrow-down').toggleClass('arrow-right');

				var data = [];

				$.each(rows, function(i,elem) {
					data.push($(elem).attr('row'));
				});

				data.splice(0,2);
				
				if (self.hasClass('close')) {
					spreadsheet.hideRows(data);
				} else {
					spreadsheet.showRows(data);
				}
			}
		}
		
	});
	
	
/** tooltip ***********************************************************************************************************************/
	
	
	// pos/material tooltip
	$('.infotip').tipsy({
		html: true,
		gravity: 'nw',
		live: true,
		title: function() {
			
			if ($(this).attr('data-title')) {
				return $(this).attr('data-title');
			} else {
			
				var id = $(this).attr('data');
				var section = $(this).attr('rel');
				var selector = section+'-'+id;
				var elem = $('#'+selector);
	
				if (elem.text().length) {
					return elem.html();
				} else {
					$('.image-loader').trigger('load-data', [this]);
					return $('.image-loader').html();
				}
			}
		}
	});

	// load tooltip content from ajax request
	$('.image-loader').on('load-data', function(event, tooltip) {

		var id = $(tooltip).attr('data');
		var section = $(tooltip).attr('rel');
		var selector = section+'-'+id;

		
		retailnet.ajax.json('/applications/helpers/ajax.tooltip.php',{ 
			application: application,
			section: section,
			id: id 
		}).done(function(xhr) {
			
			if(xhr && xhr.content) {
				
				$('.tooltips-container').append('<div id="'+selector+'" >'+xhr.content+'</div>');
				
				if ($(tooltip).is(':hover')) {
					$(tooltip).tipsy("show");
				}
			}
		});
	});
	
	$('td.warning').tipsy({
		html: true,
		gravity: 'sw',
		live: true,
		title: function() {
			
			var id = $(this).attr('id');
			var celName = id ? id.substring(12) : null;
			
			if (celName) {
				var data = spreadsheet.getData(celName);
				return data.tag;
			}
		}
	});

	
/** filters ***********************************************************************************************************************/

	
	// dropdown company warehouses
	$('#address_warehouse_id').change(function() {
		if ($(this).val()) {
			$('#warehouse_name').val('').trigger('change');
			$('#add_warehouse_apply').removeClass('disabled');
		} else {
			$('#add_warehouse_apply').addClass('disabled');
		}
	});
	
	// build dropdown UI skins
	$('select', $('#filters')).dropdown();

	// confirmation versions
	$('#version').change(function() {
		var value = $(this).val();
		var url = $('#url').val();
		var link = (value) ? url+'/'+value : url;
		window.location = link;
	});

	// reload page when filters are changed
	$('select', $('#filters')).change(function() {
		$('#filters').submit();
	});

	var popFilter = $('#pop_filter');
	
	popFilter.pop().click(function() {
		$('.icon',this).toggleClass('direction-up');
	});

	popFilter.pop('hide');

	$('#failures').on('click', function(e) {

		e.preventDefault();

		var self = $(this);
		var url = self.attr('href');
		
		retailnet.loader.show();
			
		retailnet.ajax.json(url).done(function(xhr) {

			if (xhr && xhr.reload) {
				window.location.reload();
			}

			if (xhr && xhr.message) {
				if (xhr.success) retailnet.notification.success(xhr.message);
				else retailnet.notification.error(xhr.message);
			}

			if (xhr && xhr.success) {
				self.remove();
			}

		}).complete(function() {
			retailnet.loader.hide();
		});

		return false;
	});
	
});