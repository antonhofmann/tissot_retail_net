define([
	'jquery',
	'underscore',
	'backbone',
	'views/notify'
], function($, _, Backbone, Notify) {

	return {
		initialize: function() { //console.log('app init');

			// collapsable tabs
			if (jQuery().tabCollapse) {

				$('.tabs-collapsable').tabCollapse();

				$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

					var nav = $(this).closest('nav');

					if (nav.hasClass('tabs-collapsable')) {
						var ul = $(this).closest('ul').hasClass('collapsable-items') ? $('.dropdown-menu', nav) : $('.collapsable-items', nav);
						var index = $(this).parent().index();
						ul.children('li').removeClass('active').eq(index).addClass('active');
					}
				})
			}

			// track bootstrap tabs
			if(typeof(Storage) !== "undefined" && $('a[data-toggle="tab"]').length > 0) {
				
				var url = location.href;
				var tab = localStorage.getItem('selectedTab');
				var ref = localStorage.getItem('selectedTabUrl');

				// save selected tab
				$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
					localStorage.setItem('selectedTab', $(this).attr('href'));
					localStorage.setItem('selectedTabUrl', url);
				});	

				if (tab) {
					if (ref==url) {
						$('a[href='+tab+']').tab('show');
					} else {
						localStorage.setItem('selectedTab', false);
						localStorage.setItem('selectedTabUrl', false);
					}
				}
			}

			// bootstrap tooltips
			if (jQuery().tooltip) {
				$(function () {
				  $('[data-toggle="tooltip"]').tooltip({ html: true })
				})
			}
		},

		getSessionMessages: function() {

			$.getJSON('/messages/getSession', function(xhr) {

				if (!xhr) return;

				$.each(xhr, function(i, message) { 
					
					if (typeof message==='object') { 
						
						// old retailnet
						if (message.content) message.text = message.content;
						if (message.type == 'message') message.type = 'info';

						(message.custom) ? Notify.custom(message) : Notify.show(message);

					} else {
						Notify.info(message);
					}
				})
			})
		},

		setResponsive: function(breakpoints) {

			var bp = $.extend({
				xl: 1600,
				lg: 1200,
				sm: 768,
				xs: 480
			}, breakpoints);

			// responsive breakpoints classes
			$(window).on('resize', function() {

				$('body').removeClass (function (index, css) {
					return (css.match (/(^|\s)size-\S+/g) || []).join(' ');
				});

				var width = $(window).width();

				// extra large devices
				if (width > bp.xl) $('body').addClass('size-xl');
				
				// large devices
				if (width > bp.lg) {
					$('body').addClass('size-device-xl');
					if (width < 1600) $('body').addClass('size-lg');
				}

				// medium devices
				if (width > bp.sm && width < bp.lg) {
					$('body').addClass('size-md size-device-md');
				}

				// small devices
				if (width <= bp.sm) {
					$('body').addClass('size-device-sm');
					if (width > bp.xs) $('body').addClass('size-sm');
					else $('body').addClass('size-xs');
				}
				
			}).trigger('resize');
		}
	}

});