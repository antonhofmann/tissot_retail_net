$(document).ready(function() {

	var form = $("#posmap");
	var latitude = $('#posaddress_google_lat').val() || $('.posaddress_google_lat .-text').text();
	var longitude = $('#posaddress_google_long').val() || $('.posaddress_google_long .-text').text();
	var draggable = ($('#draggable').val()) ? true : false;
	var mapzoom1 = $('#mapzoom').val(); 
	
	
	// spiner instance	
	retailnet.loader.init();
	
	// tooltips
	retailnet.tooltip.init();
	
	$('.map_content').removeClass('-even');
	
	
	var StartPosition = new google.maps.LatLng(latitude, longitude);
	
	function SetPosition(Location, Viewport) {
	    Marker.setPosition(Location);
	    if(Viewport){
	        Map.fitBounds(Viewport);
	        Map.setZoom(map.getZoom() + 2);
	    }
	    else {
	        Map.panTo(Location);
	    }
	
	    $("#posaddress_google_lat").val(Location.lat().toFixed(15));
	    $("#posaddress_google_long").val(Location.lng().toFixed(15));
	}

	var MapOptions = {
	    zoom: 16,
	    center: StartPosition,
	    mapTypeId: google.maps.MapTypeId.ROADMAP,
	    mapTypeControl: true,
	    disableDoubleClickZoom: false,
	    streetViewControl: true
	};
	
	var MapView = $("#gmap");
	var Map = new google.maps.Map(MapView.get(0), MapOptions);
	
	var Marker = new google.maps.Marker({
	    position: StartPosition, 
	    map: Map, 
	    title: "Drag Me",
	    draggable: draggable
	});
	
	google.maps.event.addListener(Marker, "dragend", function(event) {
	    SetPosition(Marker.position);
	});
	
	SetPosition(Marker.position);

	form.validationEngine({
		autoHidePrompt: true,
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		onAjaxFormComplete: function(status, form, json, options) {
			
			retailnet.loader.hide();
			retailnet.notification.hide();

			if (json) {

				if (json.response) { 
					window.location.reload();
				}

				if (json.message) {
					if (json.response) retailnet.notification.success(json.message);
					else retailnet.notification.error(json.message);
				}
			}
		}
	});
	
	$("#save").click(function(event) { 
		
		if (form.validationEngine('validate')) {
			
			retailnet.loader.show();
			
			form.submit();
			
		} else {
			
			retailnet.notification.error('Please check form data');
		}

	});

});