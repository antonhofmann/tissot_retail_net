$(document).ready(function() {

	$sectionContainer = $('#categories tbody');

	$sectionContainer.sortable({
      placeholder: "ui-state-highlight",
      handle: ".section-order",
      forcePlaceholderSize: true,
      update: function( event, ui ) { 

  		var data = $(this).sortable('toArray', {attribute: 'data-id'});
		
		Adomat.Ajax.post('/applications/modules/news/category/ajax.php', {
			application: 'news',
			section: 'sort',
			data: data
		})
      }
    }).disableSelection();
})