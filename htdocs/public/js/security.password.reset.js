$(document).ready(function() {

	var form = $('#passwordform');

	// loader instance
	retailnet.loader.init();
	
	// tooltips
	retailnet.tooltip.init();

	form.validationEngine({
		ajaxFormValidation: true,
		ajaxFormValidationMethod: 'post',
		autoHidePrompt: true,
		binded: true,
		scroll: true,
		onValidationComplete: function(form, status) {
			if (status === true) {
				retailnet.loader.show();
			} else {
				return false;
			}
		},
		onAjaxFormComplete: function(status, form, json, options) { 

			// send mail
			if (json && json.response) { 
					
				retailnet.ajax.json('/applications/helpers/security.reset.password.php', form.serialize()).done(function(xhr) {

					if (xhr && xhr.response) {
						window.location="/security";
					}
					
					// show notofications
					if (xhr && xhr.message) {
						retailnet.notification.show(xhr.message);
					}
					
				}).complete(function() {

					// show wait screen
					retailnet.loader.hide();
				});	
							
			} else {

				// show wait screen
				retailnet.loader.hide();

				// show notofications
				if (json && json.message) {
					retailnet.notification.show(json.message);
				}
			}
		}
	});

	$("#passwordform :input").blur(function() {
		$(this).toggleClass('error', ($(this).val()) ? false : true);
	});

	$("#submitRequest").click(function(e) {
		e.preventDefault();
		retailnet.notification.hide();
		form.submit();
		return false;
	});
	
});