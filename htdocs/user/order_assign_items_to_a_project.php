<?php
/********************************************************************

    order_assign_items_to_a_project.php

    Assign items of a catalog order to a project

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-11-12
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-11-12
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_assign_orders_to_projects");
register_param("oid");


/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$order = get_order(param('oid'));

// get company's address
$client_address = get_address($order["order_client_address"]);

// get System currency
$system_currency = get_system_currency_fields();


// create sql for order items
$sql_order_items = "select order_item_id, order_item_text, order_item_quantity, ".
                   "    order_item_po_number, order_item_system_price, item_id, ".
                   "    if(order_item_not_in_budget=1 , 'no', 'yes') as order_item_in_budget, order_item_supplier_address, ".
                   "    if(item_code <>'', item_code, item_type_name) as item_shortcut, ".
                   "    addresses.address_shortcut as supplier_company, ".
                   "    addresses_1.address_shortcut as forwarder_company, ".
                   "    item_type_id, item_type_name, ".
                   "    item_type_priority, item_stock_property_of_swatch, unit_name ".
                   "from order_items ".
                   "left join items on order_item_item = item_id ".
                   "left join addresses on order_item_supplier_address = addresses.address_id ".
                   "left join addresses as addresses_1 ".
                   "     on order_item_forwarder_address = addresses_1.address_id ".
                   "left join item_types on order_item_type = item_type_id  " .
				   "left join units on unit_id = item_unit";

$list_filter = "  (order_item_type = " . ITEM_TYPE_STANDARD . 
                " or order_item_type = " . ITEM_TYPE_COST_ESTIMATION . 
                "    or order_item_type = " . ITEM_TYPE_SPECIAL . ") ".
                "    and order_item_order = " . param('oid');



$address_parent = 0;
$sql = "select address_parent from addresses where address_id = " . $order["order_client_address"];
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$address_parent = $row["address_parent"];
}


$sql_projects = "select order_id, concat(order_number, ' ', order_shop_address_company) as orderinfo " . 
                "from orders " . 
				"inner join projects on project_order = order_id " .
				"inner join project_costs on project_cost_order = order_id " . 
				" where ((order_client_address = " . $order["order_client_address"] .
				" or order_client_address = " . $address_parent ." ) " . 
				" and order_type = 1 " . 
				" and (project_shop_closingdate is null or project_shop_closingdate = '0000-00-00') " . 
				" and project_cost_cms_completed <> 1 " .
				" and year(orders.date_created) > 2007 )" . 
				" order by order_number DESC, year(orders.date_created) DESC";

$project_items = array();
$costgroups = array();
$sql_i = "select * from order_items_in_projects " . 
       "where order_items_in_project_order_id = " . param("oid");


$res = mysql_query($sql_i) or dberror($sql_i);
while ($row = mysql_fetch_assoc($res))
{
	$project_items[$row["order_items_in_project_order_item_id"]] = $row["order_items_in_project_project_order_id"];

	$costgroups[$row["order_items_in_project_order_item_id"]] = $row["order_items_in_project_costgroup_id"];
}




$sql_projects = "select order_id, concat(order_number, ' ', order_shop_address_company) as orderinfo " . 
                "from orders " . 
				"inner join projects on project_order = order_id " .
				"inner join project_costs on project_cost_order = order_id " . 
				" where ((order_client_address = " . $order["order_client_address"] .
				" or order_client_address = " . $address_parent ." ) " . 
				" and order_actual_order_state_code < '900' " . 
				" and order_type = 1 " . 
				" and year(orders.date_created) > 2007) "; 

if(count($project_items) > 0)
{
		$sql_projects .= " or order_id in (" . implode(',', $project_items) . ") ";
}
$sql_projects .= " order by order_number DESC, year(orders.date_created) DESC ";


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("orders", "order", 640);

$form->add_section("Order");
$form->add_hidden("oid", param('oid'));
$form->add_hidden("order_file_order", param('oid'));

require_once "include/order_head_small.php";


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Create List for Catalog Items
*********************************************************************/ 
$list = new ListView($sql_order_items, LIST_HAS_HEADER);

$list->set_title("Catalog Items");
$list->set_entity("order_item");
$list->set_filter($list_filter);
$list->set_order("item_type, item_code");

$list->add_column("item_shortcut", "Code");
$list->add_column("order_item_text", "Name");
$list->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);

$list->add_list_column("cost_group", "Cost Group", "select project_cost_groupname_id, project_cost_groupname_name from project_cost_groupnames where project_cost_groupname_active = 1 order by project_cost_groupname_sortorder", 0, $costgroups);

$list->add_list_column("project", "Used in Project", $sql_projects, NOTNULL, $project_items);

$list->add_button("save", "Save");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();

if($list->button("save"))
{
	
	
	//get old values
	$order_ids = array();
	$has_changed = false;
	$old_items = array();
	$items = array();
	$sql = "select * from order_items_in_projects " . 
		   " where order_items_in_project_order_id = " . dbquote(param("oid"));
	
	$res = mysql_query($sql);
	while ($row = mysql_fetch_assoc($res))
    {
		$old_items[$row["order_items_in_project_order_item_id"]] = $row;
	}
	
	
	
	//update table orders_in_projects
	$sql = "delete from order_items_in_projects " . 
		   " where order_items_in_project_order_id = " . dbquote(param("oid"));
	
	$res = mysql_query($sql);

	$projects = $list->values("project");

	foreach($list->values("cost_group") as $key => $value)
    {

		if($key > 0 and $value > 0)
		{
			
			$items[$key] = $key;
			
			
			$fields = array();
			$values = array();

			$fields[] = "order_items_in_project_order_id";
			$values[] = dbquote(param("oid"));

			$fields[] = "order_items_in_project_project_order_id";
			$values[] = dbquote($projects[$key]);

			$fields[] = "order_items_in_project_order_item_id";
			$values[] = dbquote($key);

			$fields[] = "order_items_in_project_costgroup_id";
			$values[] = dbquote($value);

			$fields[] = "user_created";
			$values[] = dbquote(user_login());

			$fields[] = "date_created";
			$values[] = dbquote(date("Y-m-d H:i:s"));


			$sql = "insert into order_items_in_projects (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			mysql_query($sql) or dberror($sql);


			if(!array_key_exists($key, $old_items))
			{
				$sql = "select item_id, item_type, item_name, order_item_quantity " . 
					   " from order_items " .
					   " left join items on item_id = order_item_item " . 
					   " where order_item_id = " . dbquote($key);

				$res = mysql_query($sql);

				if ($row = mysql_fetch_assoc($res))
				{
					$result = track_change_in_order_items('New item assigned from catalogue order', 'Quantity', $projects[$key], "", $row["item_id"],$row["item_type"], $row["item_name"], 0, $row["order_item_quantity"]);
					$has_changed = true;
					$order_ids[$projects[$key]] = $projects[$key];
				}
				
			}
		}
    }


	//add tracking information for removed items
	foreach($old_items as $key=>$data)
	{
		if(!array_key_exists($key, $items ))
		{
			$sql = "select item_id, item_type, item_name, order_item_quantity " . 
				   " from order_items " .
				   " left join items on item_id = order_item_item " . 
				   " where order_item_id = " . dbquote($key);
			
			$res = mysql_query($sql);

			if ($row = mysql_fetch_assoc($res))
			{
				
				$result = track_change_in_order_items('Item from catalogue order was removed', 'Quantity', $data["order_items_in_project_project_order_id"], "", $data["item_id"],$row["item_type"], $row["item_name"], $row["order_item_quantity"], 0);
				$has_changed = true;
				$order_ids[$data["order_items_in_project_project_order_id"]] = $data["order_items_in_project_project_order_id"];
			}
		}
	}

	
	//send mailalert in case project is a corporate project and CMS was already approved by finance controller
	if($has_changed == true)
	{
		foreach($order_ids as $key=>$order_id)
		{
			$sql = "select project_id, project_cost_type, project_cost_cms_approved " .
			       " from projects " .
				   " left join project_costs on project_cost_order = project_order " . 
				   "where project_order = " . dbquote($order_id);

			$res = mysql_query($sql);

			if ($row = mysql_fetch_assoc($res) 
				and $row["project_cost_type"] == 1 
				and $row["project_cost_cms_controlled"] == 1)
			{
				$actionmail = new ActionMail(71);
				$actionmail->setParam('id', $row["project_id"]);
				
				if($senmail_activated == true)
				{
					$actionmail->send();
				}
			}
		}
	}
}


/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("Assign Items to a Project");
$form->render();
echo "<br>";
$list->render();
$page->footer();

?>