<?php
/********************************************************************

    project_add_attachment.php

    add an attachment to a project

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-26
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-18
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

register_param("pid");

check_access("can_add_attachments_in_projects");

/********************************************************************
    prepare all data needed
*********************************************************************/
$user_roles = get_user_roles(user_id());
$user = get_user(user_id());



// read project and order details
$project = get_project(param("pid"));

//design supervisor must get a cce mail in case PM is mot a HQ PM
$cc_to_design_supervisor = false;
$sql = "select user_address " . 
		 " from users " . 
		 " where user_id = " . dbquote($project["project_retail_coordinator"]);


$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);
if($row["user_address"] != 13) //Tissot HQ
{
	if($project["project_design_supervisor"] > 0)
	{
		$cc_to_design_supervisor = true;
	}
}

//get notification reciepients for attachments of the type layout
$notification_reciepients = array();

$sql = "select * from postype_notifications " .
       "where postype_notification_email4 is not null " .
       "   and postype_notification_postype = " . $project["project_postype"] . 
	   "   and postype_notification_prodcut_line = " . $project["project_product_line"];

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
    $notification_reciepients[] =$row["postype_notification_email4"];
}


// get company's address
$client_address = get_address($project["order_client_address"]);

// buld sql for attachment categories

if($project["project_projectkind"] == 8)
{
	$sql_attachment_categories = "select order_file_category_id, concat(order_file_category_priority, ' ', order_file_category_name) as group_name ".
                             "from order_file_categories ".
                             "where order_file_category_type = 2 ".
                             "order by order_file_category_priority";
}
else
{
	$sql_attachment_categories = "select order_file_category_id, concat(order_file_category_priority, ' ', order_file_category_name) as group_name ".
                             "from order_file_categories ".
                             "where order_file_category_id <> 18 and order_file_category_type = 2 ".
                             "order by order_file_category_priority";
}


$sql_suppliers = 'select DISTINCT address_id, concat(address_company, " (" , user_name, user_firstname , ")") as supplier_name ' . 
                 'from addresses ' .
				 'left join users on user_address = address_id '.
				 ' where address_type IN (2,8) and address_active= 1 and user_active = 1 ' .
				 ' order by supplier_name';








// get addresses involved in the project
$companies = get_involved_companies($project["project_order"], 0);

// checkbox names
$check_box_names = array();


$former_attachment = array();
$supp01 = 0;
$supp02 = 0;
$supp03 = 0;
$supp04 = 0;
$supp05 = 0;
$supp06 = 0;
if(array_key_exists("aid", $_GET) and $_GET["aid"] > 0)
{
	
	$sql = "select * " . 
		   "from order_files ". 
		   " where order_file_id = " . $_GET["aid"];

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$former_attachment = $row;

		$allowed_addresses = array();
		$sql = "select * from order_file_addresses " .
			   "where order_file_address_file = " . $_GET["aid"];

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$allowed_addresses[] = $row["order_file_address_address"];
		}
		$former_attachment["allowed_addresses"] = $allowed_addresses;


		$res = mysql_query($sql_suppliers) or dberror($sql_suppliers);

		while ($row = mysql_fetch_assoc($res))
		{
			if(in_array($row["address_id"], $former_attachment["allowed_addresses"]))
			{
				if($supp01 == 0)
				{
					$supp01 = $row["address_id"];
				}
				elseif($supp02 == 0)
				{
					$supp02 = $row["address_id"];
				}
				elseif($supp03 == 0)
				{
					$supp03 = $row["address_id"];
				}
				elseif($supp03 == 0)
				{
					$supp03 = $row["address_id"];
				}
				elseif($supp04 == 0)
				{
					$supp04 = $row["address_id"];
				}
				elseif($supp05 == 0)
				{
					$supp05 = $row["address_id"];
				}
			}
		}

	}
}


//get all involved suppliers
$involved_suppliers = array();

$sql =  "select DISTINCT order_item_supplier_address,  address_company " . 
		"from order_items ".
		"left join addresses on address_id = order_item_supplier_address " . 
		" where (order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
				  "   and order_item_quantity > 0 " .
				  "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
				  "   and order_item_order = " . $project["project_order"] . 
				  "   and order_item_supplier_address > 0 " . 
		"order by address_company";

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
	$involved_suppliers[$row["order_item_supplier_address"]] = $row["address_company"];
}

if(count($involved_suppliers) > 0)
{
	$involved_suppliers['nosupplier'] = "No specific supplier";
}

//check if user is one of the suppliers
$user_is_a_supplier = false;
if(array_key_exists($user["address"], $involved_suppliers))
{
	$user_is_a_supplier = true;
}


//compose invisible div for shipping documents selector
$former_attachment = array();

$sql = "select * " . 
	   "from order_files ". 
	   " where order_file_id = " . dbquote(id());

$res = mysql_query($sql) or dberror($sql);

if ($row = mysql_fetch_assoc($res))
{
	$former_attachment = $row;
}

$sql_shipping_documents = "select standard_shipping_document_id, standard_shipping_document_name " . 
                          "from address_shipping_documents " . 
						  " left join standard_shipping_documents  on standard_shipping_document_id = address_shipping_document_document_id " . 
						  " where address_shipping_document_address_id = " . dbquote($project["order_client_address"]) . 
						  " order by standard_shipping_document_name";

$shipping_document_categories = array();
$res = mysql_query($sql_shipping_documents) or dberror($sql_shipping_documents);
while ($row = mysql_fetch_assoc($res))
{
	$shipping_document_categories[$row["standard_shipping_document_id"]] = $row["standard_shipping_document_name"];
}

if(count($shipping_document_categories) > 0)
{
	$shipping_document_categories['other'] = "Other";
}


//only allow indicate shipping docs if user is supplier, logistics coordinator or project manager
if($user_is_a_supplier == false
   and !in_array(1, $user_roles)
   and !in_array(2, $user_roles)
   and !in_array(3, $user_roles))
{
	$shipping_document_categories = array();
}


if((count($former_attachment) > 0 and $former_attachment['order_file_category'] == 12)
	or param("order_file_category") == 12)
{
	$shipping_document_category_selector = '<div id="shipping_document_category_selector" style="display:block;">';
}
else
{
	$shipping_document_category_selector = '<div id="shipping_document_category_selector" style="display:none;">';
}

$shipping_document_category_selector .= '<br />Please select the type of the shipping document:<br />';
foreach($shipping_document_categories as $id=>$name)
{
	$checked = "";
	if((count($former_attachment) > 0 and $former_attachment['order_file_standard_shipping_document_id'] == $id) or param("order_file_standard_shipping_document_id") == $id)
	{
		$checked = "checked";
	}
	$shipping_document_category_selector .= "<input type=\"radio\" name=\"standard_shipping_document_id\" " . $checked . " value=\"" . $id . "\">" . $name . '<br />';
}

if($user_is_a_supplier == false)
{
	$shipping_document_category_selector .= '<br />Please tell us to which supplier this shipping document belongs:<br />';
	foreach($involved_suppliers as $id=>$name)
	{
		$checked = "";
		if(count($former_attachment) > 0 and $former_attachment['order_file_supplier_id'] == $id or param("order_file_supplier_id") == $id)
		{
			$checked = "checked";
		}
		$shipping_document_category_selector .= "<input type=\"radio\" name=\"standard_shipping_supplier_id\" " . $checked . " value=\"" . $id . "\">" . $name . '<br />';
	}
}

$shipping_document_category_selector .= '<br /></div>';

/********************************************************************
    Create Form
*********************************************************************/ 
$form = new Form("order_files", "file");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("order_file_order", $project["project_order"]);
$form->add_hidden("order_file_owner", user_id());

require_once "include/project_head_small.php";

$form->add_section("Attachment");

if(count($former_attachment) > 0)
{
	$form->add_list("order_file_category", "Category*", $sql_attachment_categories, NOTNULL, $former_attachment["order_file_category"]);
}
else
{
	$form->add_list("order_file_category", "Category*", $sql_attachment_categories, NOTNULL);
}

if(count($shipping_document_categories) > 0)
{
	$form->add_label("shipping_document_category", "", RENDER_HTML, $shipping_document_category_selector);
}
$form->add_hidden("order_file_standard_shipping_document_id",0);
$form->add_hidden("order_file_supplier_id", 0);

$form->add_edit("order_file_title", "Title*", NOTNULL, "", TYPE_CHAR);
$form->add_multiline("order_file_description", "Description*", 4);


if (has_access("can_set_attachment_accessibility_in_projects"))
{
    $form->add_section("Accessibility");
    $form->add_comment("Please indicate who is allowed to view this attachment.");


    $num_checkboxes=1;
    foreach ($companies as $key=>$value_array)
    {
        if(!array_key_exists('role', $value_array)) {
			$value_array["role"] = "";
		}
		
		if(count($former_attachment) > 0 and in_array($value_array["id"], $former_attachment["allowed_addresses"]))
		{
			$form->add_checkbox("A" . $num_checkboxes, $value_array["name"], 1, 0, $value_array["role"]);
		}
		else
		{
			if($value_array["send_default_mail"] == 1)
			{
				$form->add_checkbox("A" . $num_checkboxes, $value_array["name"], 1, 0, $value_array["role"]);
			}
			else
			{
				$form->add_checkbox("A" . $num_checkboxes, $value_array["name"], 0, 0, $value_array["role"]);
			}
		}
		

        $check_box_names["A" . $num_checkboxes] = $value_array["user"];
        $num_checkboxes++;
    }

	
    $form->add_comment("Please indicate accessibility for potential suppliers.");
	$form->add_list("supplier1", "Supplier 1", $sql_suppliers, 0, $supp01);
	$form->add_list("supplier2", "Supplier 2", $sql_suppliers, 0, $supp02);
	$form->add_list("supplier3", "Supplier 3", $sql_suppliers, 0, $supp03);
	$form->add_list("supplier4", "Supplier 4", $sql_suppliers, 0, $supp04);
	$form->add_list("supplier5", "Supplier 5", $sql_suppliers, 0, $supp05);
	$form->add_list("supplier6", "Supplier 6", $sql_suppliers, 0, $supp06);
	


}
else
{
	$form->add_section("Notification");
    $form->add_comment("This attachment is sent to the following persons:");
	//$form->add_label("p1", "Project Leader", 0, $project["project_manager"]);
	//$form->add_label("p2", "Logistics Coordinator", 0, $project["operator"]);


    $num_checkboxes=1;
    foreach ($companies as $key=>$value_array)
    {
        if($value_array["role"] == 'Logistics Coordinator' or $value_array["role"] == 'Project Leader')
		{
			if(!array_key_exists('role', $value_array)) {
				$value_array["role"] = "";
			}
			
			if(count($former_attachment) > 0 and in_array($value_array["id"], $former_attachment["allowed_addresses"]))
			{
				$form->add_checkbox("A" . $num_checkboxes, $value_array["name"], 1, 0, $value_array["role"]);
			}
			else
			{
				if($value_array["send_default_mail"] == 1)
				{
					$form->add_checkbox("A" . $num_checkboxes, $value_array["name"], 1, 0, $value_array["role"]);
				}
				else
				{
					$form->add_checkbox("A" . $num_checkboxes, $value_array["name"], 0, 0, $value_array["role"]);
				}
			}
			

			$check_box_names["A" . $num_checkboxes] = $value_array["user"];
			$num_checkboxes++;
		}
    }

	
	$form->add_hidden("supplier1");
	$form->add_hidden("supplier2");
	$form->add_hidden("supplier3");
	$form->add_hidden("supplier4");
	$form->add_hidden("supplier5");
	$form->add_hidden("supplier6");

}


$order_number = $project["order_number"];


$form->add_section("File");
$form->add_hidden("order_file_type");

$form->add_upload("order_file_path", "File", "/files/orders/$order_number", NOTNULL);


//$form->add_section("Layout Options");
//$form->add_checkbox("order_file_is_final_layout", "Layout signed by Tissot HQ management", "", 0, "Layout");
$form->add_hidden("order_file_is_final_layout", 0);

if (has_access("can_set_attachment_accessibility_in_projects"))
{
	$form->add_section("CER/AF Booklet Options");
	$form->add_checkbox("order_file_attach_to_cer_booklet", "Attach this file to the AF/CER booklet", "", 0, "AF/CER Booklet");

	$form->add_section("CC Recipients");
	$form->add_modal_selector("ccmails", "Selected Recipients", 8);
}
else
{
	$form->add_hidden("order_file_attach_to_cer_booklet", 0);
	$form->add_hidden("ccmails");
}



$form->add_section("Upload Multiple Attachments");
$form->add_checkbox("save_and_addnew", "", 0, 0, "Add another attachment after saving");

$form->add_button("save", "Save Attachment");
$form->add_button(FORM_BUTTON_BACK, "Back");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("save"))
{
	
	//get file_type
	$file_type_is_valid = false;
	$path = $form->value("order_file_path");
	$ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));

	$sql = "select file_type_id from file_types " . 
		   " where file_type_extension like '%" . $ext ."%'";

	
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$form->value("order_file_type", $row["file_type_id"]);
		$file_type_is_valid = true;
	}


	if(count($shipping_document_categories) > 0 
		and $form->value("order_file_category") == 12) // shipping document
	{
		if($form->value("order_file_standard_shipping_document_id") == 'other'
		   or $form->value("order_file_supplier_id") == 'nosupplier')
		{
			$form->add_validation("{order_file_standard_shipping_document_id} = 'other'", "");
			$form->add_validation("{order_file_supplier_id} = 'nosupplier'", "");
		}
		else
		{
			$form->add_validation("{order_file_standard_shipping_document_id} > 0", "You must indicate the type of the shipping document.");
			$form->add_validation("{order_file_supplier_id} > 0", "A shipping document must always be related to a supplier.");
		}
	}


	if($file_type_is_valid == false)
	{
		$form->error("The file type of the file you try to upload has an extension not allowed to be uploaded!");
	}
	elseif($form->validate())
	{
    
		// check if a recipient was selected
		if (has_access("can_set_attachment_accessibility_in_projects"))
		{
			$no_recipient = 1;
		}
		else
		{
			$no_recipient = 0;
		}


		foreach ($form->items as $item)
		{
			if ($item["type"] == "checkbox" and $item["value"] == "1")
			{
				$no_recipient = 0;
			}
		}
		
		$invalid_file_extension = false;
		
		if($form->value("order_file_category") == 11)
		{
			$ext = pathinfo($form->value("order_file_path"), PATHINFO_EXTENSION);
			
			if($ext != 'jpg' and $ext != 'JPG' and $ext != 'jpeg' and $ext != 'JPEG')
			{
				$invalid_file_extension = true;
			}
		}


		if ($form->validate() and $no_recipient == 0 and $invalid_file_extension == false)
		{
			
			if($form->value("order_file_category") != 8) //layout
			{
				$form->value("order_file_is_final_layout", 0);
			}
			
			$form->save();
			$attachment_id = mysql_insert_id();
			
			if($form->value("order_file_category") == 11)
			{
				$sql = "update order_files " . 
					   "set order_file_type = 2 " . 
					   "where order_file_id = " . $attachment_id;
				$res = mysql_query($sql) or dberror($sql);


				//check if project can be archived on uploading POS Images

				//check if the POS has pictures uploaded
				$pos_pictures_uploaded = false;

				$sql = "select count(order_file_id) as num_recs from order_files " . 
					   " where order_file_category = 11 " . 
					   " and order_file_order = " . dbquote($project["project_order"]);

				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				if ($project['project_postype'] == 1 and $row["num_recs"] > 4)
				{
					$pos_pictures_uploaded = true;
				}
				elseif($project['project_postype'] == 2 and  $row["num_recs"] > 2)
				{
					$pos_pictures_uploaded = true;
				}
				elseif($row["num_recs"] > 0) {
					$pos_pictures_uploaded = true;
				}

		
				if($project["project_cost_type"] == 1) //corporate
				{
					if($pos_pictures_uploaded == true 
						and $project["project_cost_cms_completed2"] == 1
						and $project["project_actual_opening_date"] != NULL 
						and $project["project_actual_opening_date"] != '0000-00-00'
						and ($project["order_archive_date"] == NULL 
						or $project["order_archive_date"] == '0000-00-00'))
					{
						if($project["order_actual_order_state_code"] < '890')
						{
							append_order_state($project["project_order"], '890', 1, 1);
							set_archive_date($project["project_order"]);
						}
					}
				}
				else
				{
					if($pos_pictures_uploaded == true 
						and $project["project_cost_cms_completed2"] == 1
						and $project["project_actual_opening_date"] != NULL 
						and $project["project_actual_opening_date"] != '0000-00-00'
						and ($project["order_archive_date"] == NULL 
						or $project["order_archive_date"] == '0000-00-00'))
					{
						if($project["order_actual_order_state_code"] < '890')
						{
							append_order_state($project["project_order"], '890', 1, 1);
							set_archive_date($project["project_order"]);
						}
					}
				}
			}
			
			save_attachment_accessibility_info($form,  $check_box_names);


			

			
			
			// send email notifocation to the retail staff
			$order_id = $project["project_order"];

			$sql = "select order_id, order_number, " .
				   "order_shop_address_company, order_shop_address_place, " .
				   "project_retail_coordinator, country_name, ".
				   "users.user_email as recepient1, users.user_address as address_id1, " .
				   "users2.user_email as recepient2, users2.user_address as address_id2, " .
				   "users1.user_email as sender, concat(users1.user_name, ' ', users1.user_firstname) as user_fullname, ".
				   "users.user_id as user_id1,  " .
				   "users2.user_id as user_id2  " .
				   "from orders ".
				   "left join projects on project_order = " . $order_id . " " .
				   "left join countries on country_id = order_shop_address_country " .
				   "left join users on order_retail_operator = users.user_id ".
				   "left join users as users1 on " . user_id() . "= users1.user_id ".
				   "left join users as users2 on project_retail_coordinator = users2.user_id ".
				   "where order_id = " . $order_id;

			

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$subject = MAIL_SUBJECT_PREFIX . ": New attachment was added - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];

				$sender_email = $row["sender"];
				$sender_name =  $row["user_fullname"];

				$mail = new PHPMailer();
				$mail->Subject = $subject;

				$mail->SetFrom($sender_email, $sender_name);
				$mail->AddReplyTo($sender_email, $sender_name);


				$bodytext0 = "A new attachment has been added by " . $sender_name . ": ";

				$bodytext1 = "A new attachment has been added by " . $sender_name . " for: ";
			
				$reciepients = array();
				$reciepient_ids = array();
				$reciepients_cc = array();
				$reciepients_dp = array();
				$reciepient_ids = array();

				// remove setting of defaul recipients
				/*
				if($row["recepient1"])
				{
					$reciepients[strtolower($row["recepient1"])] = strtolower($row["recepient1"]);
					$reciepient_ids[$row["user_id1"]] =$row["user_id1"];
				
				}
				if($row["recepient2"])
				{
					$reciepients[strtolower($row["recepient2"])] = strtolower($row["recepient2"]);
					$reciepient_ids[$row["user_id2"]] =$row["user_id2"];
				}
				*/
				

				foreach ($check_box_names as $key=>$value)
				{
					if ($form->value($key))
					{

						foreach($companies as $key1=>$company)
						{
							
							if($value == $company["user"])
							{
								$sql = "select user_email, user_email_cc, user_email_deputy ".
									   "from users ".
									   "where (user_id = " . $company["user"] . 
									   "   and user_active = 1)";

								$res1 = mysql_query($sql) or dberror($sql);
								if ($row1 = mysql_fetch_assoc($res1))
								{
									$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
									if($row1["user_email_cc"])
									{
										$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
									}
									if($row1["user_email_deputy"])
									{
										$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
									}

									$reciepient_ids[$company["user"]] = $company["user"];
								}
							}
						}
					}
				}

				

				

				//get all ccmails
				$ccmails =  explode(' ', preg_replace("'\r?\n'"," ",$form->value('ccmails'))); 

				
				foreach($ccmails as $ccmail) {
					if(is_email_address($ccmail)) {
						
						$reciepients_cc[strtolower($ccmail)] = strtolower($ccmail);
					}
				}
				

				//get potential suppliers
				if($form->value("supplier1") > 0) {
					$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
						   "from users ".
						   "where (user_address = " . $form->value("supplier1") . 
						   "   and user_active = 1)";

				
					
					$res1 = mysql_query($sql) or dberror($sql);
					while ($row1 = mysql_fetch_assoc($res1))
					{
						$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
						if($row1["user_email_cc"])
						{
							$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
						}
						if($row1["user_email_deputy"])
						{
							$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
						}

						$reciepient_ids[$row1["user_id"]] = $row1["user_id"];
					}
				}

				if($form->value("supplier2") > 0) {
					$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
						   "from users ".
						   "where (user_address = " . $form->value("supplier2") . 
						   "   and user_active = 1)";
					
					$res1 = mysql_query($sql) or dberror($sql);
					while ($row1 = mysql_fetch_assoc($res1))
					{
						$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
						if($row1["user_email_cc"])
						{
							$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
						}
						if($row1["user_email_deputy"])
						{
							$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
						}

						$reciepient_ids[$row1["user_id"]] = $row1["user_id"];
					}
				}

				if($form->value("supplier3") > 0) {
					$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
						   "from users ".
						   "where (user_address = " . $form->value("supplier3") . 
						   "   and user_active = 1)";
					
					$res1 = mysql_query($sql) or dberror($sql);
					while ($row1 = mysql_fetch_assoc($res1))
					{
						$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
						if($row1["user_email_cc"])
						{
							$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
						}
						if($row1["user_email_deputy"])
						{
							$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
						}

						$reciepient_ids[$row1["user_id"]] = $row1["user_id"];
					}
				}

				if($form->value("supplier4") > 0) {
					$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
						   "from users ".
						   "where (user_address = " . $form->value("supplier4") . 
						   "   and user_active = 1)";
					
					$res1 = mysql_query($sql) or dberror($sql);
					while ($row1 = mysql_fetch_assoc($res1))
					{
						$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
						if($row1["user_email_cc"])
						{
							$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
						}
						if($row1["user_email_deputy"])
						{
							$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
						}

						$reciepient_ids[$row1["user_id"]] = $row1["user_id"];
					}
				}

				if($form->value("supplier5") > 0) {
					$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
						   "from users ".
						   "where (user_address = " . $form->value("supplier5") . 
						   "   and user_active = 1)";
					
					$res1 = mysql_query($sql) or dberror($sql);
					while ($row1 = mysql_fetch_assoc($res1))
					{
						$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
						if($row1["user_email_cc"])
						{
							$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
						}
						if($row1["user_email_deputy"])
						{
							$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
						}

						$reciepient_ids[$row1["user_id"]] = $row1["user_id"];
					}
				}

				if($form->value("supplier6") > 0) {
					$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
						   "from users ".
						   "where (user_address = " . $form->value("supplier6") . 
						   "   and user_active = 1)";
					
					$res1 = mysql_query($sql) or dberror($sql);
					while ($row1 = mysql_fetch_assoc($res1))
					{
						$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
						if($row1["user_email_cc"])
						{
							$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
						}
						if($row1["user_email_deputy"])
						{
							$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
						}

						$reciepient_ids[$row1["user_id"]] = $row1["user_id"];
					}
				}


				//mail to design supervisor in case PM is not a HQ PM
				if($cc_to_design_supervisor == true)
				{
					
					$sql1 = "select user_id, user_email " . 
						   "from users ".
						   "where (user_id = '" .  $project["project_design_supervisor"] . "' " . 
						   "   and user_active = 1)";
					$res1 = mysql_query($sql1) or dberror($sql1);
					$row1 = mysql_fetch_assoc($res1);
					$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
					$reciepient_ids[$row1["user_id"]] =$row1["user_id"];
				
				}
				
				foreach($reciepients as $key=>$email)
				{
					if($email)
					{
						$bodytext1 = $bodytext1 . "\n" . $email;
						$mail->AddAddress($email);
					}
				}


				foreach($reciepients_cc as $key=>$email)
				{
					if($email)
					{
						$bodytext1 = $bodytext1 . "\n" . $email;
						$mail->AddCC($email);
						
					}
				}

				foreach($reciepients_dp as $key=>$email)
				{
					if($email)
					{
						$bodytext1 = $bodytext1 . "\n" . $email;
						$mail->AddCC($email);
					}
				}

				

				//add notification_reciepients for new attachment of type layout
				if($form->value("order_file_category") == 8)
				{
					foreach($notification_reciepients as $key=>$value)
					{
						if($value)
						{
							$mail->AddAddress($value);
							$bodytext1 = $bodytext1 . "\n" . $value;
						}
					}
				}
				
				
				$bodytext0.= "\n\n<--\n";
				$bodytext0.= $form->value("order_file_title") . "\n";
				
				
				if($form->value("order_file_description"))
				{
					$bodytext0.= $form->value("order_file_description") . "\n";
				}
				/*
				$bodytext0.= "download the file here:\n" . APPLICATION_URL . $form->value("order_file_path");
				$bodytext0.= "\n-->";
				*/


				if($form->value("order_file_path"))
				{
					$filepath = $_SERVER["DOCUMENT_ROOT"] . $form->value("order_file_path");

					//7MB plus 30% overhead base64-ecoded = 9.1 MB, 10MB Restriction from Mail server
					if(filesize ( $filepath ) < 7340032 
						and must_attach_file_to_mail($form->value("order_file_category")) == 1)
					{
						$mail->AddAttachment($filepath);
					}
				}
				

				$link ="project_view_attachment.php?pid=" . param("pid") . "&id=" .  $attachment_id;
				$bodytext = $bodytext0 . "\n\nclick below to have direct access to the project:\n";
				$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";

				$mail->Body = $bodytext;
				if($senmail_activated == true)
				{
					$mail->send();
				}


				$bodytext1.= "\n\n<--\n";
				$bodytext1.= $form->value("order_file_title") . "\n";
				$bodytext1.= "-->";

				
				
				$bodytext = $bodytext1 . "\n\nclick below to have direct access to the project:\n";
				$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";  
				

				foreach($reciepient_ids as $key=>$user_id)
				{
					append_mail($project["project_order"], $user_id , user_id(), $bodytext, "", 1, 0, 0, 'order_files', $attachment_id);
				}

							
				foreach($reciepients_cc as $key=>$email)
				{
					$sql = "select user_id from users " . 
						   " where LOWER(user_email) = " . dbquote($email);
					
					$res = mysql_query($sql) or dberror($sql);
					if ($row = mysql_fetch_assoc($res))
					{
						append_mail($project["project_order"], $row["user_id"] , user_id(), $bodytext, "", 1, 0, 1, 'order_files', $attachment_id);
					}
				}

				foreach($reciepients_dp as $key=>$email)
				{
					$sql = "select user_id from users " . 
						   " where LOWER(user_email) = " . dbquote($email);
					
					$res = mysql_query($sql) or dberror($sql);
					if ($row = mysql_fetch_assoc($res))
					{
						append_mail($project["project_order"], $row["user_id"] , user_id(), $bodytext, "", 1, 0, 1, 'order_files', $attachment_id);
					}
				}


				foreach($notification_reciepients as $key=>$email)
				{
					$sql = "select user_id from users " . 
						   " where LOWER(user_email) = " . dbquote($email);
					
					$res = mysql_query($sql) or dberror($sql);
					if ($row = mysql_fetch_assoc($res))
					{
						append_mail($project["project_order"], $row["user_id"] , user_id(), $bodytext, "", 1, 0, 1, 'order_files', $attachment_id);
					}
				}
				
			}

			if($form->value("save_and_addnew") == 1)
			{
				$link = "project_add_attachment.php?pid=" . param("pid") . "&aid=" . id();
				redirect($link);
			}
			else
			{
				$link = "project_view_attachments.php?pid=" . param("pid");
				redirect($link);
			}
		}
		else
		{
			if($form->value("order_file_category") == 999912)
			{
			}
			elseif($invalid_file_extension == true)
			{
				$form->error("Please upload only files of the type 'jpg'.");
			}
			elseif($no_recipient == 0) {
				$form->error("Please fill in all required fields.");
			}
			else
			{
				$form->error("Please fill in all required fields and select a least one person to have access to the attachment.");
			}
		}
	}
}


$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Add an Attachment");
$form->render();


echo '<div id="shipping_document_category_selector" style="display:none;position:absolute;height:200px;width:200px;">';


foreach($shipping_document_categories as $id=>$name)
{
	$checked = "";
	if($former_attachment['order_file_standard_shipping_document_id'] == $id or param("order_file_standard_shipping_document_id") == $id)
	{
		$checked = "checked";
	}
	echo "<input type=\"radio\" onclick=\"javascript:$('#order_file_standard_shipping_document_id').val($(this).val());\" value=\"" . $id . "\" name=\"order_file_standard_shipping_document_id\" " . $checked . ">" . $name . '<br />';
}

?>
</div>



<script language="Javascript">

	$(document).ready(function(){

		$("#order_file_category").change(function(){
		
			if( $('#shipping_document_category_selector').is(':hidden') ) 
			{
				if($("#order_file_category").val() == 12)
				{
					$("#shipping_document_category_selector").show();
				}
			}
			else
			{
				$("#shipping_document_category_selector").hide();
				$('input[name="standard_shipping_document_id"]').attr('checked', false);
				$("#order_file_standard_shipping_document_id").val(0);

				$('input[name="standard_shipping_supplier_id"]').attr('checked', false);
				$("#order_file_supplier_id").val(0);
			}
		});

		$("input[name='standard_shipping_document_id']").change(function(){
			
			var selectedVal = 0;
			var selected = $("input[type='radio'][name='standard_shipping_document_id']:checked");
			if (selected.length > 0) {
				selectedVal = selected.val();
			}
			
			$("#order_file_standard_shipping_document_id").val(selectedVal);
		});

		
		<?php
		if($user_is_a_supplier == false)
		{
		?>
			$("input[name='standard_shipping_supplier_id']").change(function(){
				
				var selectedVal = 0;
				var selected = $("input[type='radio'][name='standard_shipping_supplier_id']:checked");
				if (selected.length > 0) {
					selectedVal = selected.val();
				}
				$("#order_file_supplier_id").val(selectedVal);
			});
		<?php
		}
		else
		{
		?>
			$("#order_file_supplier_id").val(<?php echo $user["address"];?>);
		<?php
		}
		?>

	});

</script>


<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#ccmails_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/shared/select_mail_recipients.php'
    });
    return false;
  });
});
</script>

<?php

require_once "include/project_footer_logistic_state.php";
$page->footer();


?>