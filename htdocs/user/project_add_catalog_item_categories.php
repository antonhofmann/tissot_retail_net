<?php
/********************************************************************

    project_add_catalog_item_categories.php

    Add items to the list of materials
    List all Categories

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-11
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-06-23
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("has_access_to_list_of_materials_in_projects");

register_param("pid");
register_param("oid");
set_referer("project_add_catalog_item_item_list.php");

/********************************************************************
    prepare all data needed
*********************************************************************/


// read project and order details
$project = get_project(param("pid"));

// read project and order details
$order = get_order(param("oid"));

// get company's address
$client_address = get_address($order["order_client_address"]);

$sql = "select DISTINCT concat(product_line_id, item_category_id, item_subcategory) as pcid, " . 
       " product_line_id, item_category_id, item_subcategory_id, " .
       " product_line_name, item_category_name, item_subcategory_name, item_subcategory, " .
	   " item_category_sortorder, item_category_name " . 
       " from product_lines " .
	   "INNER JOIN product_line_supplying_groups ON product_line_supplying_group_line_id = product_line_id
	    INNER JOIN item_supplying_groups ON item_supplying_group_supplying_group_id = product_line_supplying_group_group_id
		INNER JOIN items ON item_id = item_supplying_group_item_id
		INNER JOIN supplying_groups on supplying_group_id = item_supplying_group_supplying_group_id 
		INNER JOIN productline_regions ON productline_region_productline = product_line_id
		INNER JOIN item_categories ON item_category_id = item_category
		LEFT JOIN item_subcategories on item_subcategory_id = item_subcategory
		INNER JOIN item_countries ON item_country_item_id = item_id 
		LEFT join item_pos_types on item_pos_type_item = item_id ";

if(param("type") and param("type") == 2)
{
	$list_filter = " product_line_id = " . dbquote($project["project_product_line"]) .
				   " and item_category_active = 1 
					 and item_active = 1
					 and item_visible_in_projects = 1
					 and item_type IN (2) 
					 and item_country_country_id = " . dbquote($project["order_shop_address_country"]);
}
else
{
	$list_filter = " product_line_id = " . dbquote($project["project_product_line"]) .
				   " and item_category_active = 1 
					 and item_active = 1
					 and item_visible_in_projects = 1
					 and item_type IN (1, 7) 
					 and item_country_country_id = " . dbquote($project["order_shop_address_country"]) . 
					 "    and ((item_type = " . ITEM_TYPE_SERVICES . 
					 " and item_pos_type_pos_type = " . $project["project_postype"] . 
					 " and item_pos_type_product_line = " . $project["project_product_line"] . ") " . 
					 " or (item_type <> " . ITEM_TYPE_SERVICES . " and item_pos_type_pos_type is null))";
}



$links = array();
$sql_tmp = $sql . " where " . $list_filter;
$res = mysql_query($sql_tmp) or dberror($sql_tmp);
while ($row = mysql_fetch_assoc($res))
{

	if(param("type") and param("type") == 2)
	{
		if($row["item_subcategory"] > 0)
		{
			$links[$row["pcid"]] = '<a href="project_add_catalog_item_item_list.php?pid=' . param("pid") . '&oid=' . param("oid") . '&cid=' . $row["item_category_id"] . '&scid=' . $row["item_subcategory"] . '&type=2'. '">' . $row["item_subcategory_name"] . '</a>';
		}
		else
		{
			$links[$row["pcid"]] = '<a href="project_add_catalog_item_item_list.php?pid=' . param("pid") . '&oid=' . param("oid") . '&cid=' . $row["item_category_id"] . '&scid=0' . '&type=2' . '">Items</a>';
		}
	}
	else
	{
		if($row["item_subcategory"] > 0)
		{
			$links[$row["pcid"]] = '<a href="project_add_catalog_item_item_list.php?pid=' . param("pid") . '&oid=' . param("oid") . '&cid=' . $row["item_category_id"] . '&scid=' . $row["item_subcategory"] . '">' . $row["item_subcategory_name"] . '</a>';
		}
		else
		{
			$links[$row["pcid"]] = '<a href="project_add_catalog_item_item_list.php?pid=' . param("pid") . '&oid=' . param("oid") . '&cid=' . $row["item_category_id"] . '&scid=0' . '">Items</a>';
		}
	}
}


/********************************************************************
    Create Form
*********************************************************************/ 
$form = new Form("projects", "project");

require_once "include/project_head_small.php";

/********************************************************************
    Create Lists for Supplying Groups
*********************************************************************/ 


$list = new ListView($sql, LIST_HAS_HEADER);
$list->set_title("");


$list->set_entity("item_categories");
$list->set_filter($list_filter);

$list->set_order("item_category_name, item_subcategory_name");
$list->set_group("item_category_name", "", "item_category_sortorder");

$list->add_hidden("pid", param("pid"));
$list->add_hidden("oid", $project["project_order"]);

$list->add_text_column("category", "Item Category", COLUMN_UNDERSTAND_HTML, $links);

$list->add_button(LIST_BUTTON_BACK, "Back");

$list->process();



/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";


$page->header();


$page->title("Add Catalog Items: Category Selection");
$form->render();


echo "<p>", "Please choose from the following item categories.", "</p>";


$list->render();


require_once "include/project_footer_logistic_state.php";
$page->footer();


?>