<?php
/********************************************************************

    order_add_catalog_item_item_list.php

    Add items to the list of materials from category item list.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-11
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-02-16
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.


*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "include/order_functions.php";


check_access("can_edit_list_of_materials_in_orders");


$cid = param('cid');
$plid = param('plid');


$searchterm = '';
if(array_key_exists('searchterm', $_GET) and $_GET['searchterm'] != '') 
{
	$searchterm = $_GET['searchterm'];
}
else
{
	$searchterm = param('searchterm');
}


/********************************************************************
    prepare all data needed
*********************************************************************/

$order = get_order(param("oid"));
$region = get_user_country_region($order['order_user']);

// get company's address
$client_address = get_address($order["order_client_address"]);

// get category name
if($searchterm)
{
	$category_name = "Item List matching '" . $searchterm . "'";
}
else
{
	$category_name = get_category_name($plid, $cid, param("scid"));
}


$sql = "select DISTINCT item_id, item_category_id, item_category_name, item_code, item_name,item_price, ".
       " concat(address_company, ' - ', item_category_name) as group_head " . 
       " from product_lines " .
	   "INNER JOIN product_line_supplying_groups ON product_line_supplying_group_line_id = product_line_id
	    INNER JOIN item_supplying_groups ON item_supplying_group_supplying_group_id = product_line_supplying_group_group_id
		INNER JOIN items ON item_id = item_supplying_group_item_id
		INNER JOIN supplying_groups on supplying_group_id = item_supplying_group_supplying_group_id 
		INNER JOIN productline_regions ON productline_region_productline = product_line_id
		INNER JOIN item_categories ON item_category_id = item_category
		LEFT JOIN item_subcategories on item_subcategory_id = item_subcategory
		INNER JOIN item_countries ON item_country_item_id = item_id 
		LEFT join item_pos_types on item_pos_type_item = item_id 
		LEFT JOIN suppliers on supplier_item = item_id
	    LEFT JOIN addresses on address_id = supplier_address ";
	
if($searchterm)
{
	$list_filter = "(REPLACE(LOWER(item_code), ' ', '') like '%" . strtolower($searchterm) . "%' OR REPLACE(LOWER(item_name), ' ', '') like '%" .  strtolower($searchterm) . "%' OR REPLACE(LOWER(item_category_name), ' ', '') like '%" .  strtolower($searchterm) . "%' OR REPLACE(LOWER(product_line_name), ' ', '') like '%" .  strtolower($searchterm) . "%' )";


	$list_filter .= " and (product_line_visible = 1 
					 or product_line_budget = 1)
					 and product_line_active = 1
					 and item_category_active = 1 
					 and item_active = 1
					 and item_type = 1 and item_country_country_id = " . dbquote($client_address["country"]) . 
					 " and productline_region_region = " . dbquote($region);
}
else
{
	$list_filter = " item_category = " . dbquote($cid) .
				   " and product_line_id = " . dbquote($plid) .
				   " and (product_line_visible = 1 
					 or product_line_budget = 1)
					 and product_line_active = 1
					 and item_category_active = 1 
					 and item_active = 1
					 and item_type = 1 
					 and item_country_country_id = " . dbquote($client_address["country"]) . 
				     " and productline_region_region = " . dbquote($region);
}

if(param("scid")) {
	$list_filter .= ' and item_subcategory = ' . dbquote(param("scid"));
}
else
{
	$list_filter .= ' and (item_subcategory is null or item_subcategory = 0)';
}

if(param("sg")) {
	$list_filter .= ' and item_supplying_group_supplying_group_id = ' . dbquote(param("sg"));
}


// read all items belonging to the category and beeing conteined in the order
$values = array();

$tmp_sql = $sql . " where " . $list_filter;
$res = mysql_query($tmp_sql) or dberror($tmp_sql);
while ($row = mysql_fetch_assoc($res))
{
    // Check if item is already in table order_items
    $sql_order_item = "select order_item_item, order_item_quantity ".
                      "from order_items ".
                      "where order_item_order = " . param('oid') .
                      "    and order_item_item = " . $row["item_id"];


    $res1 = mysql_query($sql_order_item) or dberror($sql_order_item);
    if ($row1 = mysql_fetch_assoc($res1))
    {
        $values[$row["item_id"]] = $row1["order_item_quantity"];
    }
    else
    {
        $values[$row["item_id"]] = "";
    }
}

/********************************************************************
    Create Form
*********************************************************************/ 
$form = new Form("orders", "order", 640);
$form->add_section("Order");
require_once "include/order_head_small.php";


/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);

$list->set_entity("items");
$list->set_filter($list_filter);
$list->set_order("item_code");

$list->add_hidden("oid", param("oid"));
$list->add_hidden('cid' , $cid);
$list->add_hidden('plid' , $plid);
$list->add_hidden('searchterm' , $searchterm);

$list->add_column("item_code", "Code", "/applications/templates/item.info.modal.php?id={item_id}", LIST_FILTER_FREE, "", "", "", array('class'=>'item_info_box'));
$list->add_column("item_name", "Item", "", LIST_FILTER_FREE);
$list->add_edit_column("item_entry_field", "Quantity", "4", 0, $values);

$list->add_button("add_items", "Add Items");
$list->add_button(LIST_BUTTON_BACK, "Back");



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();


if ($list->button("add_items"))
{
    project_save_order_items($list, ITEM_TYPE_STANDARD);
    $link = "order_edit_material_list.php?oid=" . param("oid"); 
    redirect($link);
}


/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");


require "include/order_page_actions.php";


$page->header();

if($searchterm) {
	$page->title('Add Catalog Items: Search Results for "' . $searchterm . '"');
}
else {
	$page->title("Add Catalog Items: " . $category_name);
}

$form->render();


echo "<p>Please indicate the quantities to add to the list.", "</p>";


$list->render();
$page->footer();


?>