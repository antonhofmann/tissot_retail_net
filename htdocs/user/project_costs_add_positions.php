<?php
/********************************************************************

    project_costs_add_positions.php

    Add new cost positions to costsheet

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-09
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2012, OMEGA SA, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../include/page_modal.php";
require_once "include/get_functions.php";


if (has_access('can_edit_project_costs_budget')
	or has_access('can_edit_project_cost_real_costs')
    or has_access('can_edit_project_costs_bids')
   )
{
}
else {
	redirect("noaccess.php");
}


if(!param("pid") and !param("mode"))
{
	$link = "noaccess.php";
	redirect($link);
}


/********************************************************************
    prepare data
*********************************************************************/

$project = get_project(param("pid"));
$client_address = get_address($project["order_client_address"]);

$order_currency = get_order_currency($project["project_order"]);


/********************************************************************
    save data
*********************************************************************/

if(param("save_form") and $_POST["mode"] == 'bid')
{
	
	$new_subgroups = array();
	foreach($_POST as $key=>$value) {
		if(strpos($key, 'new_subgroup') > 0) {
			$new_subgroups[] = str_replace('__record_new_subgroup_', '', $key);
		}
	}
	
	if(count($new_subgroups) > 0) {
		
		 $last_code = 0;
		$sql = "select pcost_subgroup_id, pcost_subgroup_code, pcost_subgroup_pcostgroup_id
				 from pcost_subgroups 
				 where pcost_subgroup_id in (" . implode(',', $new_subgroups) . ")
				 order by pcost_subgroup_code ";
		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			 $next_code = $last_code+1;
			 if($next_code < 10){$tmp2 = '0' . $next_code;} else {$tmp2 = $next_code;}
			 $code = $row["pcost_subgroup_code"] . '.' . $tmp2;
			 
			 $fields = array();
			 $values = array();

		 
			 $fields[] = "costsheet_bid_position_costsheet_bid_id";
			 $values[] = dbquote($_POST["bid"]);

			 $fields[] = "costsheet_bid_position_project_id";
			 $values[] = dbquote($_POST["pid"]);
			 
			 $fields[] = "costsheet_bid_position_pcost_group_id";
			 $values[] = dbquote($row["pcost_subgroup_pcostgroup_id"]);

			 $fields[] = "costsheet_bid_position_pcost_subgroup_id";
			 $values[] = dbquote($row["pcost_subgroup_id"]);

			 $fields[] = "costsheet_bid_position_code";
			 $values[] = dbquote($code);
			 
			 $fields[] = "date_created";
			 $values[] = dbquote(date("yyyy-mm-dd H:i:s"));

			 $fields[] = "user_created";
			 $values[] = dbquote(user_login());

			 $sql = "insert into costsheet_bid_positions (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";


			 $result = mysql_query($sql) or dberror($sql);
		}
		 
	}
}
elseif(param("save_form"))
{
	 
	$new_subgroups = array();
	foreach($_POST as $key=>$value) {
		if(strpos($key, 'new_subgroup') > 0) {
			$new_subgroups[] = str_replace('__record_new_subgroup_', '', $key);
		}
	}


	if(count($new_subgroups) > 0) {
		
		$last_code = 0;
		$sql = "select pcost_subgroup_id, pcost_subgroup_code, pcost_subgroup_pcostgroup_id
				 from pcost_subgroups 
				 where pcost_subgroup_id in (" . implode(',', $new_subgroups) . ")
				 order by pcost_subgroup_code ";
		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			 
			 $next_code = $last_code+1;
			 if($next_code < 10){$tmp2 = '0' . $next_code;} else {$tmp2 = $next_code;}
			 $code = $row["pcost_subgroup_code"] . '.' . $tmp2;
			 
			 $fields = array();
			 $values = array();

			 $fields[] = "costsheet_project_id";
			 $values[] = dbquote($_POST["pid"]);

			 $fields[] = "costsheet_version";
			 $values[] = 0;
			 
			 $fields[] = "costsheet_pcost_group_id";
			 $values[] = dbquote($row["pcost_subgroup_pcostgroup_id"]);

			 $fields[] = "costsheet_pcost_subgroup_id";
			 $values[] = dbquote($row["pcost_subgroup_id"]);

			 $fields[] = "costsheet_currency_id";
			 $values[] = dbquote($order_currency['id']);

			 $fields[] = "costsheet_exchangerate";
			 $values[] = dbquote($order_currency['exchange_rate']);

			 $fields[] = "costsheet_code";
			 $values[] = dbquote($code);

			 if($_POST["mode"] == 'cms')
			 {
				$fields[] = "costsheet_is_in_cms";
				$values[] = 1;
			 }
			 else
			 {
				$fields[] = "costsheet_is_in_budget";
				$values[] = 1;
			 }
			 
			 $fields[] = "date_created";
			 $values[] = dbquote(date("yyyy-mm-dd H:i:s"));

			 $fields[] = "user_created";
			 $values[] = dbquote(user_login());

			 $sql = "insert into costsheets (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

			 $result = mysql_query($sql) or dberror($sql);
		}

	}
	 
}


/********************************************************************
    render list
*********************************************************************/

//get existing cost subgroups
$exiting_cost_groups = array();
$available_cost_groups = array();
$exiting_cost_sub_groups = array();
$tmp_filter = '';
if(param("bid")) {
	
	$sql = "select distinct costsheet_bid_position_pcost_subgroup_id
			from costsheet_bid_positions
			where costsheet_bid_position_project_id = " . dbquote(param("pid")) . 
			" and costsheet_bid_position_costsheet_bid_id = " . dbquote(param("bid"));

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$exiting_cost_sub_groups[] = $row['costsheet_bid_position_pcost_subgroup_id'];
	}
	if(count($exiting_cost_sub_groups) > 0) {
		$tmp_filter = " pcost_subgroup_id not in (" . implode(',', $exiting_cost_sub_groups) . ") ";
	}
}
else {

	$sql = "select distinct costsheet_pcost_subgroup_id
			from costsheets
			where costsheet_version = 0
			and costsheet_project_id = " . dbquote(param("pid"));

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$exiting_cost_sub_groups[] = $row['costsheet_pcost_subgroup_id'];
	}
	if(count($exiting_cost_sub_groups) > 0) {
		$tmp_filter = " pcost_subgroup_id not in (" . implode(',', $exiting_cost_sub_groups) . ") ";
	}
	
}


//get group
$sql_subgroups = "select pcost_subgroup_id,
				 concat(pcost_group_code, ' ', pcost_group_name) as group_name,
				 concat(pcost_subgroup_code, ' ', pcost_subgroup_name) as sub_group_name 
				 from pcost_subgroups 
				 left join pcost_groups on pcost_group_id = pcost_subgroup_pcostgroup_id ";

$sql = $sql_subgroups . " where " . $tmp_filter;
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$available_cost_groups[] = $row['costsheet_pcost_subgroup_id'];
}

$list = new ListView($sql_subgroups);
$list->set_group("group_name");
$list->set_order("pcost_subgroup_code, pcost_subgroup_code");
$list->set_filter($tmp_filter);

$list->add_checkbox_column('new_subgroup', "", 0);
$list->add_column('sub_group_name', "Position");

$list->populate();
$list->process();


$form = new Form("projects", "projects");
$form->add_hidden("save_form", "1");
$form->add_hidden("costsheet_project_id", param("pid"));
$form->add_hidden("bid", param("bid"));
$form->add_hidden("pid", param("pid"));
$form->add_hidden("gid", param("gid"));
$form->add_hidden("mode", param("mode"));

if(count($available_cost_groups) > 0) { 
	$form->add_input_submit("submit", "Update Cost Sheet", 0);
}
else {
	$form->add_comment("No additional cost groups are available;");
	$form->add_button("close", "Close");
}

$page = new Page_Modal("projects");
$page->header();

$page->title("Add Missing Cost Groups");
if(count($available_cost_groups) > 0) { 
	$list->render();
}
$form->render();

if(param("save_form") and param("mode") == 'cms')
{
?>
	<script languege="javascript">
		var back_link = "project_costs_real_costs.php?pid=<?php echo param("pid")?>";
		$.nyroModalRemove();
	</script>
<?php
}
elseif(param("save_form") and param("mode") == 'bid')
{
?>
	<script languege="javascript">
		var back_link = "project_costs_bid.php?id=<?php echo param("bid");?>&pid=<?php echo param("pid")?>";
		$.nyroModalRemove();
	</script>
<?php
}
elseif(param("save_form"))
{
?>
	<script languege="javascript">
		var back_link = "project_costs_budget.php?pid=<?php echo param("pid")?>";
		$.nyroModalRemove();
	</script>
<?php
}

$page->footer();

?>