<?php
/********************************************************************

    project_forecasts_xls.php

    Generate Excel-File of project forecasts

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-10-01
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-10-01
    Version:        1.1.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

if (has_access("can_view_only_his_project_forecasts")
   or has_access("can_view_project_forecasts")
   or has_access("can_edit_only_his_project_forecasts")
   or has_access("can_edit_project_forecasts"))
{

}
else
{
	redirect("noaccess.php");
}

require_once "../include/xls/Writer.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

$user = get_user(user_id());

/********************************************************************
    prepare Data Needed
*********************************************************************/

$year = param("year"); // year
$country = param("country"); // country

//get product lines


$header = "";
$header = "Project Forecast: " . $year . " (" . date("d.m.Y G:i") . ")";

/********************************************************************
    prepare Data
*********************************************************************/
$user = get_user(user_id());

$sql = "select projectforecast_id, projectforecast_year, projectforecast_quantity, " . 
       "address_company, concat('Q', projectforecast_planned_quartal) as planned_date, " .
       "country_name, project_costtype_text,  projectkind_name, postype_name, projectforecast_comment " . 
       "from projectforecasts " . 
       " left join addresses on address_id = projectforecast_address_id " . 
	   " left join countries on country_id = projectforecast_country_id " .
	   " left join project_costtypes on project_costtype_id = projectforecast_project_costtype_id ". 
	   " left join projectkinds on projectkind_id = projectforecast_projectkind_id " . 
	   " left join postypes on postype_id = projectforecast_postype_id";

$sql = "select projectforecast_id, projectforecast_year, projectforecast_quantity, " . 
       "address_company, concat('Q', projectforecast_planned_quartal) as planned_date, " .
       "country_name, postype_name, projectforecast_comment " . 
       "from projectforecasts " . 
       " left join addresses on address_id = projectforecast_address_id " . 
	   " left join countries on country_id = projectforecast_country_id " .
	   " left join postypes on postype_id = projectforecast_postype_id";

$list_filter = "projectforecast_address_id > 0 and projectforecast_quantity > 0 ";
if (has_access("can_view_only_his_project_forecasts")
   or has_access("can_edit_only_his_project_forecasts"))
{
	$list_filter .= " and projectforecast_address_id = " . $user['address'];
}


if(param("year") == 'all')
{
	$list_filter .= " and projectforecast_year > 0 ";
}
elseif(param("year"))
{
	if(param("year") and param("year") > 0)
	{
		$list_filter .= " and projectforecast_year = " . $year;
	}
}

if(is_numeric($country) and $country > 0)
{
	$list_filter .= " and projectforecast_country_id = " . $country;
}



$sql .= " where " . $list_filter;

/*
$sql .= " order by address_company, country_name, projectforecast_year, project_costtype_text, " . 
        "projectkind_name, planned_date";
*/

$sql .= " order by address_company, country_name, projectforecast_year, planned_date";


/********************************************************************
    prepare Excel Sheet
*********************************************************************/
$filename = "project_forecast_" . date("Ymd") . ".xls";
$xls = new Spreadsheet_Excel_Writer(); 
$xls->send($filename);

$xls->setVersion(8);

$sheet =& $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

//formats
$header_row =& $xls->addFormat();
$header_row->setSize(10);
$header_row->setAlign('left');
$header_row->setBold();


$f_normal =& $xls->addFormat();
$f_normal->setSize(8);
$f_normal->setAlign('left');
$f_normal->setBorder(1);

$f_normal_bold =& $xls->addFormat();
$f_normal_bold->setSize(8);
$f_normal_bold->setAlign('left');
$f_normal_bold->setBorder(1);
$f_normal_bold->setBold();


$f_number =& $xls->addFormat();
$f_number->setSize(8);
$f_number->setAlign('right');
$f_number->setBorder(1);

$f_center =& $xls->addFormat();
$f_center->setSize(8);
$f_center->setAlign('center');
$f_center->setBorder(1);

$f_caption =& $xls->addFormat();
$f_caption->setSize(8);
$f_caption->setAlign('left');
$f_caption->setBorder(1);
$f_caption->setBold();
$f_caption->setTextRotation(270);
$f_caption->setTextWrap();

$f_used =& $xls->addFormat();
$f_used->setSize(8);
$f_used->setBorder(1);
$f_used->setPattern(2);
$f_used->setBgColor('yellow');



//captions
$captions = array();
//$captions[] = "Nr";
//$captions[] = "Geographical Region";
$captions[] = "Company";
$captions[] = "Country";
$captions[] = "Year";
//$captions[] = "Legal Type";
//$captions[] = "Project Type";
$captions[] = "POS Type";
$captions[] = "Planned in";
$captions[] = "Number of POS";
$captions[] = "Remarks";

/********************************************************************
    write all captions
*********************************************************************/
$sheet->write(0, 0, $header, $header_row);


$sheet->writeRow(2, 0, $captions, $f_normal_bold);


$row_index = 3;
$cell_index = 0;
$counter = 0;
$col_widths = array();
for($i=0;$i<count($captions);$i++)
{
	$col_widths[$i] = strlen($captions[$i]);
}



$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{

	$sheet->write($row_index, $cell_index, $row["address_company"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["address_company"]))
	{
		$col_widths[$cell_index] = strlen($row["address_company"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["country_name"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["country_name"]))
	{
		$col_widths[$cell_index] = strlen($row["country_name"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["projectforecast_year"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["projectforecast_year"]))
	{
		$col_widths[$cell_index] = strlen($row["projectforecast_year"]);
	}
	$cell_index++;

	/*
	$sheet->write($row_index, $cell_index, $row["project_costtype_text"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["project_costtype_text"]))
	{
		$col_widths[$cell_index] = strlen($row["project_costtype_text"]);
	}
	$cell_index++;

	$sheet->write($row_index, $cell_index, $row["projectkind_name"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["projectkind_name"]))
	{
		$col_widths[$cell_index] = strlen($row["projectkind_name"]);
	}
	$cell_index++;
	*/

	
	$sheet->write($row_index, $cell_index, $row["postype_name"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["postype_name"]))
	{
		$col_widths[$cell_index] = strlen($row["postype_name"]);
	}
	$cell_index++;
	

	$sheet->write($row_index, $cell_index, $row["planned_date"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["planned_date"]))
	{
		$col_widths[$cell_index] = strlen($row["planned_date"]);
	}
	$cell_index++;

	
	$sheet->write($row_index, $cell_index, $row["projectforecast_quantity"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["projectforecast_quantity"]))
	{
		$col_widths[$cell_index] = strlen($row["projectforecast_quantity"]);
	}
	$cell_index++;

	

	$sheet->write($row_index, $cell_index, $row["projectforecast_comment"], $f_normal);
	if($col_widths[$cell_index] < strlen($row["projectforecast_comment"]))
	{
		$col_widths[$cell_index] = strlen($row["projectforecast_comment"]);
	}
	$cell_index++;

	
	$cell_index = 0;
	$row_index++;
	
}

for($i=0;$i<count($captions);$i++)
{
	$sheet->setColumn($i, $i, $col_widths[$i]);
}


$xls->close(); 

?>