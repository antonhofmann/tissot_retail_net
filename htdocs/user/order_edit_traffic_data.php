<?php
/********************************************************************

    order_edit_traffic_data.php

    Edit Data concerning delivery and traffic

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-18
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-06-27
    Version:        1.1.4

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_functions.php";
require_once "include/save_functions.php";

check_access("can_edit_traffic_data_in_orders");

register_param("oid");
set_referer("order_edit_traffic_data_item.php");
set_referer("order_new.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());

// read order details
$order = get_order(param("oid"));

// get company's address
$client_address = get_address($order["order_client_address"]);

// create sql for order items
$sql_order_items = "select order_item_id, order_item_text, order_item_quantity, order_item_order,".
                   "    order_item_po_number, order_item_shipment_code, ".
                   "    if(order_item_item <>'', item_code, item_type_name) as item_shortcut, ".
                   "    concat(order_item_quantity, ' ', if(order_item_item <>'', item_code, item_type_name), '\n', if(order_item_po_number <>'', order_item_po_number, '')) as item_po, " .
                   
                   "    concat_ws('', '[', order_item_ready_for_pickup_changes, ']', DATE_FORMAT(order_item_ready_for_pickup,'%d.%m.%y'), '\n', '[', order_item_pickup_changes, ']', DATE_FORMAT(order_item_pickup,'%d.%m.%y')) as dates1, ".

                   "    concat_ws('', '[', order_item_expected_arrival_changes, ']', DATE_FORMAT(order_item_expected_arrival,'%d.%m.%y'), '\n', '[', order_item_arrival_changes, ']',  DATE_FORMAT(order_item_arrival,'%d.%m.%y')) as dates2, ".
				   " transportation_type_name, " .
                   "    addresses.address_company as forwarder_company, ".
                   "    addresses_1.address_company as supplier_company, ".
                   "    concat(addresses_1.address_company, '\n', order_address_company) as sup_wh ".
                   "from order_items ".
                   "left join items on order_item_item = item_id ".
				   "left join transportation_types on transportation_type_id = order_item_transportation " . 
                   "left join item_types on order_item_type = item_type_id ".
                   "left join addresses on order_item_forwarder_address = addresses.address_id ".
                   "left join order_addresses on order_address_order_item = order_item_id ".
                   "left join addresses AS addresses_1 on order_item_supplier_address = " .
                   "    addresses_1.address_id ";


if (has_access("has_access_to_all_traffic_data_in_orders"))
{
    $list_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
                      "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . param('oid') .
                      "   and order_item_forwarder_address > 0" .
                      "   and order_address_type = 4 ";

}
else
{
    $list_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
                      "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . param('oid') .
                      "   and order_item_forwarder_address = " . $user_data["address"] .
                      "   and order_address_type = 4 ";
}

// get order_item_dates get_order_dates( param('oid'));
$sql_order_item_dates = $sql_order_items . " " .
                         "where order_item_type <= " . ITEM_TYPE_SPECIAL .
                         "   and order_item_order = " . param('oid') .
                         "   and order_address_type = 4 ".
                         "order by forwarder_company, order_item_po_number, order_item_type," .
                         "item_shortcut";



$replacement_information = array();
$res = mysql_query($sql_order_items . " where " . $list_filter) or dberror($sql_order_items . " where " . $list_filter);
while ($row = mysql_fetch_assoc($res))
{
	$tmp = get_items_replacement_information($row["order_item_id"]);
	if(is_array($tmp))
	{
		$replacement_information[$row["order_item_id"]] = $tmp;
	}
}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("orders", "order");

$form->add_hidden("oid", param("oid"));

require_once "include/order_head_small.php";



$billing_address_province_name = "";
if($order["order_billing_address_place_id"])
{
	$billing_address_province_name = get_province_name($order["order_billing_address_place_id"]);
}

// bill to
$form->add_section("Bill to");
$form->add_label("billing_address_company", "Company", 0, $order["order_billing_address_company"]);

if ($order["order_billing_address_company2"])
{
	$form->add_label("billing_address_company2", "", 0, $order["order_billing_address_company2"]);
}

$form->add_label("billing_address_address", "Street", 0, $order["order_billing_address_address"]);

if ($order["order_billing_address_address2"])
{
	$form->add_label("billing_address_address2", "Additional Address Info", 0, $order["order_billing_address_address2"]);
}

$form->add_label("billing_address_place", "City", 0, $order["order_billing_address_zip"] . " " . $order["order_billing_address_place"]);
$form->add_label("billing_address_province_name", "Province", 0, $billing_address_province_name);

$form->add_label("billing_address_country", "Country", 0, $order["order_billing_address_country_name"]);

$form->add_label("billing_address_phone", "Phone", 0, $order["order_billing_address_phone"]);
$form->add_label("billing_address_mobile_phone", "Mobile Phone", 0, $order["order_billing_address_mobile_phone"]);
$form->add_label("billing_address_email", "Email", 0, $order["order_billing_address_email"]);


// traffic preferences

$form->add_section("Preferences and Traffic Checklist");
$form->add_label("preferred_delivery_date", "Preferred Arrival Date", 0, to_system_date($order["order_preferred_delivery_date"]));


$form->add_lookup("preferred_transportation_arranged", "Transportation arranged by", "transportation_types", "transportation_type_name", 0, $order["order_preferred_transportation_arranged"]);

$form->add_lookup("preferred_transportation_mode", "Transportation mode", "transportation_types", "transportation_type_name", 0, $order["order_preferred_transportation_mode"]);


/*
$value="no";
if ($order["order_packaging_retraction"])
{
    $value="yes";
}
$form->add_label("order_packaging_retraction", "Packaging Retraction Desired", 0, $value);
*/

$value="no";
if ($order["order_pedestrian_mall_approval"])
{
    $value="yes";
}
$form->add_label("pedestrian_mall_approval", "Pedestrian Area Approval Needed", 0, $value);

$value="no";
if ($order["order_full_delivery"])
{
    $value="yes";
}
$form->add_label("full_delivery", "Full Delivery Desired", 0, $value);

$form->add_label("delivery_comments", "Delivery Comments", RENDER_HTML, $order["order_delivery_comments"]);

$form->add_section("Insurance");
if($order["order_insurance"] == 1)
{
	$form->add_label("order_insurance", "Insurance by Tissot/Forwarder", 0, "covered");
}
else
{
	$form->add_label("order_insurance", "Insurance by Tissot/Forwarder", 0, "not covered");
}


if(count($replacement_information) > 0)
{
	$form->add_section("Replacement Information");
	
	foreach($replacement_information as $key=>$tmp)
	{
		$form->add_comment("<strong>" . $tmp["order_item_text"] . "</strong>");
		$form->add_label("original_quantity", "Original Quantity", 0, $tmp["order_item_quantity"]);
		$form->add_label("reason", "Replacement Reason", 0, $tmp["order_item_replacement_reason_text"]);
		$form->add_label("warranty", "Warranty", 0, $tmp["order_item_warranty_type_text"]);
		$form->add_label("furniture_payed_by", "Furniture paid by", 0, $tmp["furniture_payed_by"]);
		$form->add_label("freight_payed_by", "Freight paid by", 0, $tmp["freight_payed_by"]);
		$form->add_label("remarks", "Remark", 0, $tmp["order_item_replacement_remarks"]);
	}
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Create Item List
*********************************************************************/ 
$list = new ListView($sql_order_items, LIST_HAS_SEPARATOR | LIST_HAS_HEADER);

$list->set_title("Items up to Delivery");
$list->set_entity("order_item");
$list->set_group("forwarder_company");

$list->set_filter($list_filter);

$list->set_order("order_item_po_number", "item_code");

$link = "order_edit_traffic_data_item.php?oid=" . param("oid");

$list->add_column("item_po", "Item Code\nP.O. Number", $link, "", "", COLUMN_BREAK);

$list->add_column("transportation_type_name", "Transportation\nType", "", "", "", COLUMN_UNDERSTAND_HTML | COLUMN_BREAK);

$list->add_column("order_item_shipment_code", "Shipment Code", "", "", "", COLUMN_NO_WRAP);

$list->add_column("dates1", "Ready 4 Pickup\nPickup Date", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK);
$list->add_column("dates2", "Expected Arrival\nArrival", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK);

$list->add_column("sup_wh", "Supplier's Warehouse\nPick Up Address", "", "", "", COLUMN_NO_WRAP |COLUMN_BREAK);

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("Edit Traffic Data");
$form->render();
$list->render();

$page->footer();

?>
