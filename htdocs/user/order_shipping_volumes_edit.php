<?php
/********************************************************************

    order_shipping_volumes_edit.php

    Edit item volume and weight data

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2015-01-20
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2015-01-20
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

if(!has_access("can_edit_shipping_details_in_orders") and !has_access("can_edit_only_his_shipping_details_in_orders"))
{
	redirect("noaccess.php");
}

register_param("oid");
register_param("id");
register_param("oiid");

/********************************************************************
    prepare all data needed
*********************************************************************/

// read order and order details
$order = get_order(param("oid"));
$order_state_code = $order["order_actual_order_state_code"];

// get company's address
$client_address = get_address($order["order_client_address"]);

// get order_item's information
$order_item = get_order_item(param("oiid"));

$sql_units = "select unit_id, unit_name " . 
             " from units " . 
			 " order by unit_name";
$sql_packaging_types = "select packaging_type_id, packaging_type_name from packaging_types order by packaging_type_name";


/********************************************************************
    build form
*********************************************************************/
$form = new Form("order_items", "item from list of materials");

$form->add_hidden("oid", param("oid"));
$form->add_hidden("oiid", param("oiid"));

require_once "include/order_head_small.php";

$form->add_section("Item Information");
if ($order_item["type"] == ITEM_TYPE_STANDARD)
{
    $form->add_label("code", "Code", 0, $order_item["code"]);
	
}
elseif ($order_item["type"] == ITEM_TYPE_SPECIAL)
{
	$form->add_label("code", "Code", 0, "Special Item");
}
$form->add_label("order_item_text", "Description", 0, $order_item["text"]);

$form->add_label("order_item_quantity", "Quantity", 0, $order_item["quantity"]);


/********************************************************************
    rende list for a items
*********************************************************************/ 
if($order_item["type"] == 1) //standard items
{
	$sql = "select order_item_packaging_id, order_item_packaging_number, order_item_packaging_unit_id, " . 
		   " order_item_packaging_packaging_id, order_item_packaging_weight_gross, " .
		   "order_item_packaging_length, order_item_packaging_width, order_item_packaging_height, " . 
		   "ROUND((order_item_packaging_number*(order_item_packaging_length*order_item_packaging_width*order_item_packaging_height/1000000)), 2) as cbm " . 
		   " from order_item_packaging ";

	$list_filter = "order_item_packaging_order_item_id = " . dbquote(param("oiid"));

	//fill data arrays
	$quantities = array();
	$units = array();
	$packaging_types = array();
	$gross_weights = array();
	$lengths = array();
	$widths = array();
	$heights = array();

	$sql_u = $sql . " where " . $list_filter;
	$res = mysql_query($sql_u) or dberror($sql_u);
	while ($row = mysql_fetch_assoc($res))
	{
		$quantities[$row["order_item_packaging_id"]] = $row["order_item_packaging_number"];
		$units[$row["order_item_packaging_id"]] = $row["order_item_packaging_unit_id"];
		$packaging_types[$row["order_item_packaging_id"]] = $row["order_item_packaging_packaging_id"];


		$gross_weights[$row["order_item_packaging_id"]] = $row["order_item_packaging_weight_gross"];

		$lengths[$row["order_item_packaging_id"]] = $row["order_item_packaging_length"];
		$widths[$row["order_item_packaging_id"]] = $row["order_item_packaging_width"];
		$heights[$row["order_item_packaging_id"]] = $row["order_item_packaging_height"];

	}

	$list = new ListView($sql, LIST_HAS_SEPARATOR | LIST_HAS_HEADER);
	$list->set_title("Packing Information ");
	$list->set_entity("order_item_packaging");
	$list->set_filter($list_filter);

	$list->add_edit_column("order_item_packaging_number", "Quantity", "4",  COLUMN_UNDERSTAND_HTML, $quantities);

	$list->add_list_column("order_item_packaging_unit_id", "Unit", $sql_units, NOTNULL, $units);
	$list->add_list_column("order_item_packaging_packaging_id", "Packaging", $sql_packaging_types, NOTNULL, $packaging_types);


	$list->add_edit_column("order_item_packaging_weight_gross", "Gross Weight<br />in kg", "12",  COLUMN_UNDERSTAND_HTML, $gross_weights);

	$list->add_edit_column("order_item_packaging_length", "Length in mm", "12",  COLUMN_UNDERSTAND_HTML, $lengths);
	$list->add_edit_column("order_item_packaging_width", "Width in mm", "12",  COLUMN_UNDERSTAND_HTML, $widths);
	$list->add_edit_column("order_item_packaging_height", "Height in mm", "12",  COLUMN_UNDERSTAND_HTML, $heights);

	$list->add_column("cbm", "CBM", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_BREAK);
}
else //special items
{
	$sql = "select order_item_packaging_id, order_item_packaging_number, order_item_packaging_unit_id, " . 
		   " order_item_packaging_packaging_id, order_item_packaging_weight_gross, " .
		   "order_item_packaging_length, order_item_packaging_width, order_item_packaging_height, " . 
		   "ROUND((order_item_packaging_number*(order_item_packaging_length*order_item_packaging_width*order_item_packaging_height/1000000)), 2) as cbm " . 
		   " from order_item_packaging ";

	$list_filter = "order_item_packaging_order_item_id = " . dbquote(param("oiid"));

	//fill data arrays
	$quantities = array();
	$units = array();
	$packaging_types = array();
	$gross_weights = array();
	$lengths = array();
	$widths = array();
	$heights = array();
	$record_ids = array();
	$index = 1;

	$sql_u = $sql . " where " . $list_filter;
	$res = mysql_query($sql_u) or dberror($sql_u);
	while ($row = mysql_fetch_assoc($res))
	{
		$record_ids[$index] = $row["order_item_packaging_id"];
		$quantities[$index] = $row["order_item_packaging_number"];
		$units[$index] = $row["order_item_packaging_unit_id"];
		$packaging_types[$index] = $row["order_item_packaging_packaging_id"];


		$gross_weights[$index] = $row["order_item_packaging_weight_gross"];

		$lengths[$index] = $row["order_item_packaging_length"];
		$widths[$index] = $row["order_item_packaging_width"];
		$heights[$index] = $row["order_item_packaging_height"];
		$index++;

	}

	
	//sql for pipeline
	$sql = "select order_item_packaging_id, order_item_packaging_number, order_item_packaging_unit_id, " . 
		   " order_item_packaging_packaging_id, order_item_packaging_weight_gross, " .
		   "order_item_packaging_length, order_item_packaging_width, order_item_packaging_height, " . 
		   "ROUND((order_item_packaging_number*(order_item_packaging_length*order_item_packaging_width*order_item_packaging_height/1000000)), 2) as cbm " . 
		   " from order_item_packaging_pipeline ";
	
	$list = new ListView($sql, LIST_HAS_SEPARATOR | LIST_HAS_HEADER);
	$list->set_title("Packing Information ");
	$list->set_entity("order_item_packaging");
	

	$list->add_edit_column("order_item_packaging_number", "Quantity", "4",  COLUMN_UNDERSTAND_HTML, $quantities);

	$list->add_list_column("order_item_packaging_unit_id", "Unit", $sql_units, NOTNULL, $units);
	$list->add_list_column("order_item_packaging_packaging_id", "Packaging", $sql_packaging_types, NOTNULL, $packaging_types);


	$list->add_edit_column("order_item_packaging_weight_gross", "Gross Weight<br />in kg", "12",  COLUMN_UNDERSTAND_HTML, $gross_weights);

	$list->add_edit_column("order_item_packaging_length", "Length in mm", "12",  COLUMN_UNDERSTAND_HTML, $lengths);
	$list->add_edit_column("order_item_packaging_width", "Width in mm", "12",  COLUMN_UNDERSTAND_HTML, $widths);
	$list->add_edit_column("order_item_packaging_height", "Height in mm", "12",  COLUMN_UNDERSTAND_HTML, $heights);

	$list->add_column("cbm", "CBM", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_BREAK);
}




$list->add_button("save_data", "Save");
$list->add_button(LIST_BUTTON_BACK, "Back");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();

if ($list->button("save_data"))
{
	$quantities = array();
	$units = array();
	$packaging_types = array();
	$gross_weights = array();
	$lengths = array();
	$widths = array();
	$heights = array();
	
	
	foreach($list->columns as $key=>$column)
	{
		if($column["name"] == "order_item_packaging_number")
		{
			foreach($column["values"] as $key=>$value)
			{
				$quantities[$key] = $value;
			}
		}
		elseif($column["name"] == "order_item_packaging_unit_id")
		{
			foreach($column["values"] as $key=>$value)
			{
				$units[$key] = $value;
			}
		}
		elseif($column["name"] == "order_item_packaging_packaging_id")
		{
			foreach($column["values"] as $key=>$value)
			{
				$packaging_types[$key] = $value;
			}
		}
		elseif($column["name"] == "order_item_packaging_weight_gross")
		{
			foreach($column["values"] as $key=>$value)
			{
				$gross_weights[$key] = $value;
			}
		}
		elseif($column["name"] == "order_item_packaging_length")
		{
			foreach($column["values"] as $key=>$value)
			{
				$lengths[$key] = $value;
			}
		}
		elseif($column["name"] == "order_item_packaging_width")
		{
			foreach($column["values"] as $key=>$value)
			{
				$widths[$key] = $value;
			}
		}
		elseif($column["name"] == "order_item_packaging_height")
		{
			foreach($column["values"] as $key=>$value)
			{
				$heights[$key] = $value;
			}
		}
	}

	if($order_item["type"] == 1) // standard item
	{


		foreach($quantities as $key=>$value)
		{
			 $fields = array();

			 $value = dbquote($units[$key]);
			 $fields[] = "order_item_packaging_unit_id = " . $value;

			 $value = dbquote($packaging_types[$key]);
			 $fields[] = "order_item_packaging_packaging_id = " . $value;

			 $value = dbquote($quantities[$key]);
			 $fields[] = "order_item_packaging_number = " . $value;

			 $value = dbquote($gross_weights[$key]);
			 $fields[] = "order_item_packaging_weight_gross = " . $value;

			 $value = dbquote($lengths[$key]);
			 $fields[] = "order_item_packaging_length = " . $value;

			 $value = dbquote($widths[$key]);
			 $fields[] = "order_item_packaging_width = " . $value;

			 $value = dbquote($heights[$key]);
			 $fields[] = "order_item_packaging_height = " . $value;
			
			 $sql = "update order_item_packaging set " . join(", ", $fields) . " where order_item_packaging_id = " . $key;
			 mysql_query($sql) or dberror($sql);
		}
	}
	elseif($order_item["type"] == 2) //special items
	{
		

		foreach($quantities as $key=>$value)
		{
			
			
			if(array_key_exists($key, $record_ids))
			{
				if($value > 0)
				{
					 $fields = array();
					 $value = dbquote($value);
					 $fields[] = "order_item_packaging_number = " . $value;

					 $value = dbquote($units[$key]);
					 $fields[] = "order_item_packaging_unit_id = " . $value;

					 $value = dbquote($packaging_types[$key]);
					 $fields[] = "order_item_packaging_packaging_id = " . $value;
					 
					 $value = dbquote($quantities[$key]);
					 $fields[] = "order_item_packaging_number = " . $value;

					 $value = dbquote($gross_weights[$key]);
					 $fields[] = "order_item_packaging_weight_gross = " . $value;

					 $value = dbquote($lengths[$key]);
					 $fields[] = "order_item_packaging_length = " . $value;

					 $value = dbquote($widths[$key]);
					 $fields[] = "order_item_packaging_width = " . $value;

					 $value = dbquote($heights[$key]);
					 $fields[] = "order_item_packaging_height = " . $value;
					
					 $sql = "update order_item_packaging set " . join(", ", $fields) . " where order_item_packaging_id = " . $record_ids[$key];
					 mysql_query($sql) or dberror($sql);
				}
				else
				{
					$sql = "delete from  order_item_packaging where order_item_packaging_id = " . $record_ids[$key];
					 mysql_query($sql) or dberror($sql);
				}
			}
			elseif($value > 0)
			{
				$fields = array();
				$values = array();


				$fields[] = "order_item_packaging_order_item_id";
				$values[] = dbquote($form->value("oiid"));

				$fields[] = "order_item_packaging_unit_id";
				$values[] = dbquote($units[$key]);

				$fields[] = "order_item_packaging_packaging_id";
				$values[] = dbquote($packaging_types[$key]);

				$fields[] = "order_item_packaging_number";
				$values[] = dbquote($quantities[$key]);

				$fields[] = "order_item_packaging_length";
				$values[] = dbquote($lengths[$key]);

				$fields[] = "order_item_packaging_width";
				$values[] = dbquote($widths[$key]);

				$fields[] = "order_item_packaging_height";
				$values[] = dbquote($heights[$key]);

				$fields[] = "order_item_packaging_weight_gross";
				$values[] = dbquote($gross_weights[$key]);

				$fields[] = "date_created";
				$values[] = dbquote(date("Y-m-d H:i:"));

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$sql_i = "insert into order_item_packaging (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql_i) or dberror($sql_i);
			
			}
		}
	}
	
	$link = "order_shipping_volumes.php?oid=" . param("oid");
	redirect($link);
    
}



/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("Shipping Details - Edit Weight and Volumes");
$form->render();
$list->render();
$page->footer();

?>