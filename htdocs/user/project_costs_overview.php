<?php
/********************************************************************

    project_costs_overview.php

    View or edit the costs of a project

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-09
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2012, OMEGA SA, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/project_cost_functions.php";


if (has_access("can_view_project_costs") 
   or has_access("can_view_budget_in_projects")
   or has_access('can_edit_project_costs_budget')
   or  has_access('can_edit_project_cost_real_costs')
   or  has_access('can_edit_project_costs_bids')
   )
{  
}
else {
	redirect("noaccess.php");
}

if(!param("pid"))
{
	$link = "welcome.php";
	redirect($link);
}



/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));
// get company's address
$client_address = get_address($project["order_client_address"]);

$currency_symbol = get_currency_symbol($project["order_client_currency"]);

$client_currency =  get_currency($project["order_client_currency"]);

$kl_approved_money = get_kl_apporved_money(param("pid"), 0);

$kl_approved_total = 0;
foreach($kl_approved_money as $key=>$value)
{
	$kl_approved_total = $kl_approved_total + $value;
}
/********************************************************************
    check if project cost positions are present and create if not
*********************************************************************/

$sql = "select count(costsheet_id) as num_recs from costsheets " . 
       " where costsheet_version = 0
			and costsheet_project_id = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);


if($row["num_recs"] == 0) //make the user select a cost template
{
	
	if(has_access("can_edit_project_costs_budget"))
	{
		//temüplates
		$sql_templates = "select pcost_template_id, pcost_template_name " . 
			"from pcost_templates " . 
			"where pcost_template_postype_id = " .dbquote($project["project_postype"]) . 
			"order by pcost_template_name";

		
							
		$form = new Form("projects", "projects");


		$form->add_section("Project");
		$form->add_hidden("pid", param('pid'));
		$form->add_hidden("order_id", $project["project_order"]);


		require_once "include/project_head_small.php";


		$form->add_section("Template Selection");
		$form->add_comment("Please select a template for your cost sheet.");
		$form->add_list("template", "Template",$sql_templates);

		
		//existing projects
		$sql_templates2 = "select DISTINCT project_id, concat(order_shop_address_company, order_shop_address_place) as pname " . 
						  "from costsheets " . 
						  "left join projects on project_id = costsheet_project_id " .
						  "left join orders on order_id = project_order " .
						  "where costsheet_version = 0
							and (costsheet_budget_amount > 0 or costsheet_real_amount > 0)
							and order_shop_address_country = " .dbquote($project["order_shop_address_country"]) . 
						  " order by pname";

		$res = mysql_query($sql_templates2) or dberror($sql_templates2);
		if($row = mysql_fetch_assoc($res))
		{
			if($row["project_id"] > 0)
			{
				$form->add_comment("Or select a template from an existing project for your cost sheet.");
				$form->add_list("template2", "Project",$sql_templates2);
			}
		}
		else
		{
			$form->add_hidden("template2", 0);
		}

		$form->add_button("save", "Create Cost Sheet");


		$form->populate();
		$form->process();

		if($form->button("save"))
		{
			if(!$form->value("template") and $form->value("template2") === 0)
			{
				$form->error("Please select a template.");
			}
			elseif(!$form->value("template") and !$form->value("template2"))
			{
				$form->error("Please select a template or a project.");
			}
			elseif($form->value("template") > 0 and $form->value("template2") > 0)
			{
				$form->error("Please select either a template or a project.");
			}
			else
			{
				$result = create_cost_sheet(param("pid"), $form->value("template"), $form->value("template2"), $client_currency);

				$link = "project_costs_budget.php?pid=" . param("pid");
				redirect($link);
			}
		}

		$page = new Page("projects");
		require "include/project_page_actions.php";
		$page->header();
		$page->title("Project Costs - Template Selection");
		$form->render();
		$page->footer();
	}
	else
	{
						
		$form = new Form("projects", "projects");


		$form->add_section("Project");
		$form->add_hidden("pid", param('pid'));
		$form->add_hidden("order_id", $project["project_order"]);


		require_once "include/project_head_small.php";


		$form->error("No cost sheet has been defined for this project yet.");


		$form->populate();

		$page = new Page("projects");
		require "include/project_page_actions.php";
		$page->header();
		$page->title("Project Costs - Overview");
		$form->render();
		$page->footer();
	}

}
else
{


	/********************************************************************
		get budget data from cost sheet
	*********************************************************************/
	
	$budget = get_project_budget_totals(param("pid"), $client_currency );
	
	$code_data = array();
	$budget_data = array();
	$currency_data = array();
	$text_data = array();
	$comment_data = array();

	$sql = "select costsheet_id, costsheet_code, costsheet_text, " . 
		   "costsheet_budget_amount, costsheet_comment " . 
		   "from costsheets " . 
		   "where costsheet_version = 0
			and (costsheet_budget_amount > 0 or costsheet_real_amount > 0) 
			and costsheet_project_id = " . param("pid");


	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$code_data[$row["costsheet_id"]] = $row["costsheet_code"];
		$budget_data[$row["costsheet_id"]] = $row["costsheet_budget_amount"];
		$currency_data[$row["costsheet_id"]] = $currency_symbol;
		$text_data[$row["costsheet_id"]] = $row["costsheet_text"];
		$comment_data[$row["costsheet_id"]] = $row["costsheet_comment"];
	}

	$amount_difference = array();
	$amount_difference_percent = array();
	
	foreach($budget["group_real_totals"] as $key=>$value) {
		
		if(array_key_exists($key,$kl_approved_money )) {
			$amount_difference[$key] = $value - $kl_approved_money[$key];
			
			if($kl_approved_money[$key] != 0 and $amount_difference[$key] > 0) {
				$amount_difference_percent[$key] = '<span class="error">' . 100*round($amount_difference[$key]/$kl_approved_money[$key], 2) . "%</span>";
			}
			elseif($kl_approved_money[$key] != 0) {
				$amount_difference_percent[$key] = 100*round($amount_difference[$key]/$kl_approved_money[$key], 2) . "%";
			}
			else {
				$amount_difference_percent[$key] = '0.00%';
			}
		}
		else {
			$amount_difference[$key] = '';
		}
		$amount_difference[$key] = number_format($amount_difference[$key], 2, '.', "'");
		$kl_approved_money[$key] = number_format($kl_approved_money[$key], 2, '.', "'");
	}

	
	$difference_total =  $budget["real_total"] - $kl_approved_total;
	

	if($kl_approved_total != 0) {
		$difference_percent_total = 100*round($difference_total/$kl_approved_total, 2) . "%";
	}
	else {
		$difference_percent_total = '0.00%';
	}
	$difference_total = number_format($difference_total, 2, '.', "'");



	/********************************************************************
		Create Form
	*********************************************************************/ 

	$form = new Form("projects", "projects");


	$form->add_section("Project");
	$form->add_hidden("pid", param('pid'));
	$form->add_hidden("order_id", $project["project_order"]);


	require_once "include/project_head_small.php";


	$form->add_section("Currency and Exchange Rate");
	
	$form->add_label("currency", "Currency", 0, $currency_symbol);
	$form->add_label("order_client_exchange_rate", "Exchange Rate", 0, $project["order_client_exchange_rate"]);


	$form->add_section("Business Partner Contribution");
	$form->add_label("project_share_other", "Business Partner Contribution in Percent", 0, $project["project_share_other"] . "%");


	$link = "javascript:popup('/user/project_costs_budget_pdf.php?pid=" . param("pid") . "', 800, 600);";
	$form->add_button("print_budget", "Print Budget in " . $currency_symbol, $link);

	$link = "javascript:popup('/user/project_costs_budget_pdf.php?pid=" . param("pid") . "&sc=1', 800, 600);";
	$form->add_button("print_budget2", "Print Budget CHF", $link);

	$link = "javascript:popup('/user/project_costs_cms_pdf.php?pid=" . param("pid") . "', 800, 600);";
	$form->add_button("print_cms", "Print CMS in " . $currency_symbol, $link);

	$link = "javascript:popup('/user/project_costs_cms_pdf.php?pid=" . param("pid") . "&chf=1', 800, 600);";
	$form->add_button("print_cms2", "Print CMS in CHF", $link);

	
	$link = "project_costs_bid_comparison_pdf.php?pid=" . param("pid");
	$link = "javascript:popup('". $link . "', 800, 600)";
	$form->add_button("print_bid_comparison", "Print Bid Comparison", $link);

	$link = "javascript:popup('/user/project_costs_bids_pdf.php?pid=" . param("pid") .  "', 800, 600);";
	$form->add_button("print_bids", "Print Bids", $link);


	/********************************************************************
		Compose Cost Summary
	*********************************************************************/ 


	$budget_totals_per_sqm = array();
	$real_totals_per_sqm = array();
	$budget_total_per_sqm = 0;
	$real_total_per_sqm = 0;

	foreach($budget["group_totals"] as $key=>$value) {
		if($project['project_cost_totalsqms'] > 0) {
			$budget_totals_per_sqm[$key] = round($value/$project['project_cost_totalsqms']);
			$real_totals_per_sqm[$key] = round($budget["group_real_totals"][$key]/$project['project_cost_totalsqms']);
			
			$budget_total_per_sqm = $budget_total_per_sqm + $budget_totals_per_sqm[$key];
			$real_total_per_sqm = $real_total_per_sqm + $real_totals_per_sqm[$key];
		}
		else {
			$real_totals_per_sqm[$key] = 'n.a.';
			$real_totals_per_sqm[$key] = 'n.a.';
		}
	}
	
	$sql_cost_groups = "select DISTINCT costsheet_pcost_group_id, " . 
		               "concat(pcost_group_code, ' ', pcost_group_name) as costgroup " .
		               "from costsheets " .
					   "left join pcost_groups on pcost_group_id = costsheet_pcost_group_id ";
		               
	$list_filter = "costsheet_version = 0
			and costsheet_project_id = " . param("pid");
	
	$list0 = new ListView($sql_cost_groups, LIST_HAS_HEADER | LIST_HAS_FOOTER);
    $list0->set_title("Cost Groups");
    $list0->set_entity("costsheets");
    $list0->set_order("pcost_group_code");
	$list0->set_filter($list_filter);

    $list0->add_column("costgroup", "Group");
	$list0->add_text_column("budget", "Budget", COLUMN_ALIGN_RIGHT, $budget["group_totals_formated"]);
	$list0->add_text_column("budgetsqm", "Budget<br />per sqm", COLUMN_UNDERSTAND_HTML | COLUMN_ALIGN_RIGHT, $budget_totals_per_sqm);

	$list0->add_text_column("aproved", "Approved", COLUMN_ALIGN_RIGHT, $budget["group_approved_totals_formated"]);
	$list0->add_text_column("aprovedsg", "SG Approved", COLUMN_ALIGN_RIGHT, $kl_approved_money);
	
	
	$list0->add_text_column("real", "Real Cost", COLUMN_ALIGN_RIGHT, $budget["group_real_totals_formated"]);
	$list0->add_text_column("realqm", "Real Cost<br />per sqm", COLUMN_UNDERSTAND_HTML | COLUMN_ALIGN_RIGHT, $real_totals_per_sqm);

	$list0->add_text_column("difference", "Difference", COLUMN_ALIGN_RIGHT, $amount_difference);
	$list0->add_text_column("differencep", "%", COLUMN_UNDERSTAND_HTML | COLUMN_ALIGN_RIGHT, $amount_difference_percent);


	
	$list0->set_footer("costgroup", "Totals");
	$list0->set_footer("budgetsqm", number_format($budget_total_per_sqm, 2));
    $list0->set_footer("budget", number_format($budget["budget_total"], 2));
	$list0->set_footer("aproved", number_format($budget["approved_budget_total"], 2));
	
	if(has_access("has_full_access_to_cer"))
	{
		$list0->set_footer("aprovedsg", number_format($kl_approved_total, 2));
	}
	
	$list0->set_footer("real", number_format($budget["real_total"], 2));
	$list0->set_footer("realsqm", number_format($real_total_per_sqm, 2));
	$list0->set_footer("difference", $difference_total);


	
	if($difference_total > 0)
	{
		$list0->set_footer("differencep", '<span class="error">' . $difference_percent_total . "</span>");
	}
	else
	{
		$list0->set_footer("differencep", $difference_percent_total);
	}



	/********************************************************************
		Compose Cost Sheet
	*********************************************************************/ 
	
	//add all cost groups and cost sub groups
	$list_names = array();
	$group_ids = array();
	$group_titles = array();

	
	$sql2 = "select DISTINCT costsheet_pcost_group_id, pcost_group_code, pcost_group_name " .
		   "from costsheets " .
		   "left join pcost_groups on pcost_group_id = costsheet_pcost_group_id " . 
		   " where costsheet_version = 0
			 and costsheet_project_id = " . param("pid") . " and costsheet_is_in_budget = 1" .
		   " order by pcost_group_code";


	$res = mysql_query($sql2) or dberror($sql2);
	while ($row = mysql_fetch_assoc($res))
	{
		$listname = "list" . $row["pcost_group_code"];
		$list_names[] = $listname;
		$group_ids[] = $row["costsheet_pcost_group_id"];
		$group_titles[] = $row["pcost_group_name"];
		

		$toggler = '<div class="toggler_pointer" id="l' . $row["costsheet_pcost_group_id"] . '_on"><span class="fa fa-minus-square toggler"></span>' .$row["pcost_group_name"] . '</div>';

		$sql = "select costsheet_id, costsheet_pcost_group_id, costsheet_pcost_subgroup_id, " . 
			   "costsheet_budget_amount, costsheet_budget_approved_amount, costsheet_real_amount, " . 
			   "costsheet_code, costsheet_text, costsheet_comment, " .
			   "concat(pcost_subgroup_code, ' ', pcost_subgroup_name) as subgroup  " .
			   "from costsheets " .
			   " left join pcost_subgroups on pcost_subgroup_id = costsheet_pcost_subgroup_id"; 

		$list_filter = "costsheet_version = 0
			and (costsheet_budget_amount > 0 or costsheet_real_amount > 0)
			and costsheet_project_id = " . param("pid") . " and costsheet_pcost_group_id = " . $row["costsheet_pcost_group_id"] . " and costsheet_is_in_budget = 1";
		

		//compose list
		$$listname = new ListView($sql, LIST_HAS_HEADER | LIST_HAS_FOOTER);

		$$listname->set_entity("costsheets");
		$$listname->set_order("LENGTH(costsheet_code), COALESCE(costsheet_code,'Z')");
		$$listname->set_group("subgroup");
		$$listname->set_filter($list_filter);
		$$listname->set_title($toggler);

		$$listname->add_column("costsheet_code", "Code", "", "", "", COLUMN_ALIGN_LEFT);
		$$listname->add_column("costsheet_budget_amount", "Budget", "", "", "", COLUMN_ALIGN_RIGHT);
		$$listname->add_column("costsheet_budget_approved_amount", "Approved", "", "", "", COLUMN_ALIGN_RIGHT);
		$$listname->add_column("costsheet_real_amount", "Real Costs", "", "", "", COLUMN_ALIGN_RIGHT);
		$$listname->add_column("costsheet_text", "Text");

		
		foreach($budget["subgroup_totals"] as $subgroup=>$budget_sub_group_total)
		{
			$$listname->set_group_footer("costsheet_budget_amount",  $subgroup , number_format($budget_sub_group_total, 2));
		}

		foreach($budget["subgroup_approved_totals"] as $subgroup=>$budget_sub_group_total)
		{
			$$listname->set_group_footer("costsheet_budget_approved_amount",  $subgroup , number_format($budget_sub_group_total, 2));
		}

		foreach($budget["subgroup_real_totals"] as $subgroup=>$budget_sub_group_total)
		{
			$$listname->set_group_footer("costsheet_real_amount",  $subgroup , number_format($budget_sub_group_total, 2));
		}
		
		
		$$listname->set_footer("costsheet_budget_amount", number_format($budget["group_totals"][$row["costsheet_pcost_group_id"]], 2));
		$$listname->set_footer("costsheet_budget_approved_amount", number_format($budget["group_approved_totals"][$row["costsheet_pcost_group_id"]], 2));
		
		$$listname->set_footer("costsheet_real_amount", number_format($budget["group_real_totals"][$row["costsheet_pcost_group_id"]], 2));


		$$listname->populate();
	}

	$page = new Page("projects");


	require "include/project_page_actions.php";


	$page->header();
	$page->title("Project Costs - Overview");

	require_once("include/costsheet_tabs.php");
	$form->render();
	
	$list0->render();


	foreach($list_names as $key=>$listname)
	{
		if($budget["group_totals"][$group_ids[$key]] > 0) {
		
			if(array_key_exists("costsheet", $_SESSION) and array_key_exists($listname, $_SESSION["costsheet"]))
			{
				if($_SESSION["costsheet"][$listname] == 0)
				{
					echo '<p>&nbsp;</p>';
					$toggler = '<div class="toggler_pointer" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
					echo $toggler;
					
					echo '<div style="display:none;" id="' . $listname . '">';
					$$listname->render();
					echo '</div>';
				}
				else
				{
					echo '<p>&nbsp;</p>';
					$toggler = '<div class="toggler_pointer toggler_pointer_off" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
					echo $toggler;
					
					echo '<div id="' . $listname . '">';
					$$listname->render();
					echo '</div>';
				}
			}
			else
			{
				echo '<p>&nbsp;</p>';
				$toggler = '<div class="toggler_pointer toggler_pointer_off" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
				echo $toggler;
				
				echo '<div id="' . $listname . '">';
				$$listname->render();
				echo '</div>';
			}
		}
	}


	?>
	<script language="javascript">
	jQuery(document).ready(function($) {
		<?php
		foreach($list_names as $key=>$listname)
		{
		?>
			$('#l<?php echo $group_ids[$key];?>_on').click(function()  {
				$('#<?php echo $listname;?>').css('display', 'none');
				$('#l<?php echo $group_ids[$key];?>_off').css('display', 'block');

				$.ajax({
					type: "POST",
					data: "listname=<?php echo $listname;?>&visibility=0",
					url: "../shared/ajx_costsheet_liststaes.php",
					success: function(msg){
					}
				});
			});
			
			$('#l<?php echo $group_ids[$key];?>_off').click(function()  {
				$('#<?php echo $listname;?>').css('display', 'block');
				$('#l<?php echo $group_ids[$key];?>_off').css('display', 'none');

				$.ajax({
					type: "POST",
					data: "listname=<?php echo $listname;?>&visibility=1",
					url: "../shared/ajx_costsheet_liststaes.php",
					success: function(msg){
					}
				});
			});
		<?php
		}
		?>
	});


	

	</script>

	<?php

	$page->footer();
}

?>