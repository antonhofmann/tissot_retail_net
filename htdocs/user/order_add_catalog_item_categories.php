<?php
/********************************************************************

    order_add_catalog_item_categories.php

    Add items to the list of materials
    List all Categories

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-11
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-07-05
    Version:        1.0.3

    Copyright (c) 2002, Swatch AG, All Rights Reserved.
*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "include/order_functions.php";

check_access("can_edit_list_of_materials_in_orders");

register_param("oid");
set_referer("order_add_catalog_item_item_list.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$order = get_order(param("oid"));
$region = get_user_country_region($order['order_user']);

// get company's address
$client_address = get_address($order["order_client_address"]);


$sql = "select DISTINCT concat(supplying_group_id, product_line_id, item_category_id, item_subcategory) as pcid, " . 
       " supplying_group_id, supplying_group_name, product_line_id, item_category_id, item_subcategory_id, " .
       " product_line_name, item_category_name, item_subcategory_name, item_subcategory, " .
	   " concat(product_line_name, ' - ', supplying_group_name) as group_head " . 
       " from product_lines " .
	   "INNER JOIN product_line_supplying_groups ON product_line_supplying_group_line_id = product_line_id
	    INNER JOIN item_supplying_groups ON item_supplying_group_supplying_group_id = product_line_supplying_group_group_id
		INNER JOIN items ON item_id = item_supplying_group_item_id
		INNER JOIN supplying_groups on supplying_group_id = item_supplying_group_supplying_group_id 
		INNER JOIN productline_regions ON productline_region_productline = product_line_id
		INNER JOIN item_categories ON item_category_id = item_category
		LEFT JOIN item_subcategories on item_subcategory_id = item_subcategory
		INNER JOIN item_countries ON item_country_item_id = item_id";

$list_filter = "(product_line_visible = 1 
				 or product_line_budget = 1)
				 and product_line_active = 1
				 and item_category_active = 1 
				 and item_active = 1
				 and item_type = 1 
				 and item_country_country_id = " . dbquote($client_address["country"]) . 
				 " and productline_region_region = " . dbquote($region);



$links = array();
$sql_tmp = $sql . " where " . $list_filter;
$res = mysql_query($sql_tmp) or dberror($sql_tmp);
while ($row = mysql_fetch_assoc($res))
{
	
	if($row["item_subcategory"] > 0)
	{
		$links[$row["pcid"]] = '<a href="order_add_catalog_item_item_list.php?oid=' . param("oid") . '&plid=' . $row["product_line_id"] . '&cid=' . $row["item_category_id"] . '&scid=' . $row["item_subcategory"] . '&sg=' . $row["supplying_group_id"] . '">' . $row["item_category_name"] . ' - '. $row["item_subcategory_name"] . '</a>';
	}
	else
	{
		$links[$row["pcid"]] = '<a href="order_add_catalog_item_item_list.php?oid=' . param("oid") . '&plid=' . $row["product_line_id"] . '&cid=' . $row["item_category_id"] . '&scid=0' . '&sg=' . $row["supplying_group_id"] . '">' . $row["item_category_name"] . '</a>';
	}

}

/********************************************************************
    Create Form
*********************************************************************/ 


$form = new Form("orders", "order", 640);
$form->add_section("Order");
require_once "include/order_head_small.php";

$form->add_section(" ");

$form->add_comment("Enter a search term or choose from the list of item categories below.<br /><br />");
$form->add_edit("searchterm", "Search Term");


$form->add_button("search", "Search Catalog");
$form->add_button(LIST_BUTTON_BACK, "Back");


/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql, LIST_HAS_HEADER);

$list->set_entity("items");
$list->set_order("product_line_name, supplying_group_name, item_category_name, item_subcategory_name");
$list->set_group("group_head");
$list->set_title("Item Categories");
$list->set_filter($list_filter);

$list->add_text_column("category", "Item Category", COLUMN_UNDERSTAND_HTML, $links);
$list->add_hidden("oid", param("oid"));

/*
$link = "order_add_catalog_item_item_list.php?oid=" . param("oid");
$list->add_column("category_name", "Category", $link, LIST_FILTER_NONE);
*/

/********************************************************************
    Populate page
*********************************************************************/ 
$form->populate();
$form->process();

$list->process();



if ($form->button("search"))
{
	if(!$form->value("searchterm"))
	{
		$form->error("The search term must not be empty.");
	}
	else
	{
		$link = "order_add_catalog_item_item_list.php?oid=" . param('oid') . "&searchterm=" . str_replace(' ', '', $form->value("searchterm"));
		redirect($link);
	}
}




/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");


require "include/order_page_actions.php";


$page->header();


$page->title("Add Catalog Items: Standard Items");
$form->render();


echo "<p>", "Please choose from the following item categories.", "</p>";


$list->render();


?>

<script type="text/javascript">
	$(document).keypress(function(e) {
		if(e.which == 13) {
			
			if($("#searchterm").val())
			{
				button("search");
			}
		}
	});
</script>

<?php

$page->footer();


?>