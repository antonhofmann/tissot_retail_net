<?php
/********************************************************************

    project_orders_in_scooter.php

    View projects's ordered values from scooter

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2018-03-21
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2018-03-21
    Version:        1.0.0

    Copyright (c) 2018, Tissot, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";


check_access("can_view_ordered_values_in_projects");

register_param("pid");

/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());


// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);


// get system currency
$system_currency = get_system_currency_fields();
$order_currency = get_order_currency($project["project_order"]);


$sql_order_items = "select costsheet_id, item_code, costsheet_text, costsheet_budget_amount, 
					currency_symbol, address_company, costsheet_reinvoice_info, costsheet_orderdate
                    from costsheets
                    left join currencies on currency_id = costsheet_currency_id
					left join items on costsheet_item_id = item_id 
					left join suppliers on supplier_item = item_id
					left join addresses on address_id = supplier_address"; 

$list_filter = "costsheet_version = 0 
				and costsheet_item_id > 0 
				and item_stock_property_of_swatch = 1
				and costsheet_project_id = " . $project["project_id"];


$order_dates = array();
$reinvoice_info = array();

$sql = $sql_order_items . " where " . $list_filter;
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$order_dates[$row['costsheet_id']] = to_system_date($row['costsheet_orderdate']);
	$reinvoice_info[$row['costsheet_id']] = $row['costsheet_reinvoice_info'];
}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

$form->add_hidden("pid", param("pid"));

require_once "include/project_head_small.php";

$form->add_comment('<span class="error">All amounts for HQ supplied items converted from CHF into LOC are for reference only as the exchange rate may change.</span>');


$link = "project_orders_in_scooter_pdf.php?pid=" . param("pid");
$link = "javascript:popup('". $link . "', 800, 600)";
$form->add_button("print1", "Print PDF", $link);



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Create Item List
*********************************************************************/ 
$list = new ListView($sql_order_items, LIST_HAS_SEPARATOR | LIST_HAS_HEADER);

$list->set_title("Items to be Ordered in Scooter");
$list->set_entity("costsheets");
$list->set_order("address_company, item_code");
$list->set_filter($list_filter);

$list->add_column("address_company", "Supplier", "", "", "", COLUMN_BREAK);
$list->add_column("item_code", "Item Code", "", "", "", COLUMN_BREAK);
$list->add_column("costsheet_text", "Name", "", "", "", COLUMN_BREAK);
$list->add_column("currency_symbol", "", "", "", "", COLUMN_BREAK | COLUMN_ALIGN_RIGHT);
$list->add_column("costsheet_budget_amount", "Amount", "", "", "", COLUMN_BREAK);
$list->add_date_edit_column("ordered", "Order date", 10, 0, $order_dates);
$list->add_edit_column("reinvoice_info", "Reinvoice Info", 40, 0, $reinvoice_info);

$list->add_button("save", "Save");
/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();


if($list->button("save")) {
	

	foreach($list->values("ordered") as $key => $date)
    {
		$sql = "update costsheets SET " . 
		       "costsheet_orderdate = " . dbquote(from_system_date($date)) .  
		       " where costsheet_id = " . dbquote($key);
	
		$result = mysql_query($sql) or dberror($sql);
	}

	foreach($list->values("reinvoice_info") as $key => $text)
    {
		$sql = "update costsheets SET " . 
		       "costsheet_reinvoice_info = " . dbquote($text) . 
		       " where costsheet_id = " . dbquote($key);
	
		$result = mysql_query($sql) or dberror($sql);
	}


	$form->message("Your data has been saved.");
}


/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Orders to be placed in Scooter");
$form->render();
$list->render();

require_once "include/project_footer_logistic_state.php";
$page->footer();

?>