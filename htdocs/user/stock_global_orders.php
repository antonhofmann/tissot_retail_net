<?php
/********************************************************************

    stock_global_orders.php

    Enter and view global order data

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2004-01-23
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-01-26
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("has_access_to_all_stock_data");


$sid = param("sid");
register_param("sid");
set_referer("stock_global_orders_edit.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
$sql_global_orders = "select store_id,  store_last_global_order, store_last_global_order_date, ".
                     "   item_code, address_shortcut ".
                     "from stores " .
                     "left join items on item_id = store_item " .
                     "left join addresses on address_id = store_address ";


$quantities = array();
$dates = array();
$comments = array();
$sql_q = $sql_global_orders . " where address_shortcut is not null";

$res = mysql_query($sql_q ) or dberror($sql_q );
while ($row = mysql_fetch_assoc($res))
{
    $quantities[$row['store_id']] = '';
    $dates[$row['store_id']] = date("d.m.y");
    $comments[$row['store_id']] = '';
}

/********************************************************************
    Create List
*********************************************************************/ 

$list = new ListView($sql_global_orders);

$list->set_entity("stores");
$list->set_order("item_code");

if($sid)
{
    $list->set_filter("address_shortcut is not null and store_address = " . $sid);
    $list->add_hidden("sid", $sid);
}
else
{
    $list->set_filter("address_shortcut is not null");
}

$list->add_column("item_code", "Code", "stock_global_orders_edit.php");
$list->add_column("address_shortcut", "Supplier");
$list->add_column("store_last_global_order", "Last Order", "", "", "", COLUMN_ALIGN_RIGHT);
$list->add_column("store_last_global_order_date", "Date");
$list->add_edit_column("quantity", "New Order", "6", 0, $quantities);
$list->add_edit_column("date", "Date", "10", 0, $dates);
$list->add_edit_column("comment", "Comment", "50", 0, $comments);

$list->add_button("save", "Save New Global Orders");

if($sid)
{
    $list->add_button(LIST_BUTTON_BACK, "Back");
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();


/********************************************************************
    Save quantities entered
*********************************************************************/ 
if ($list->button("save"))
{
    $quantities = array();
    $dates = array();
    $comments = array();

    foreach($list->values("quantity") as $key => $value)
    {
        if($value)
        {
            if(is_decimal_value($value, 10, 2))
            {
                $quantities[$key] = $value;
                $dates[$key] = from_system_date(date("d.m.y"));
                $comments[$key] = "";
            }
        }
    }

    
    foreach($list->values("date") as $key => $value)
    {
        if(is_datetime_value($value))
        {
            $dates[$key] = from_system_date($value);
        }
    }

    foreach($list->values("comment") as $key => $value)
    {
        if($value)
        {
            $comments[$key] = $value;
        }
    }


    


    foreach($quantities as $key => $value)
    {
        
        // insert record into table store_global_orders
        $fields = array();
        $values = array();
        
        $fields[] = "store_global_order_store";
        $values[] = $key;

        $fields[] = "store_global_order_quantity";
        $values[] = $value;

        $fields[] = "store_global_order_date";
        $values[] = dbquote($dates[$key]);

        $fields[] = "store_global_order_comment";
        $values[] = dbquote($comments[$key]);

        $fields[] = "date_created";
        $values[] = "now()";

        $fields[] = "date_modified";
        $values[] = "now()";

        $fields[] = "user_created";
        $values[] = dbquote(user_login());

        $fields[] = "user_modified";
        $values[] = dbquote(user_login());


        $sql = "insert into store_global_orders (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

        mysql_query($sql) or dberror($sql);

        
        //update global order in table stores
        $sql = "select store_id, store_global_order, store_last_global_order_date ".
               "from stores ".
               "where store_id = " .  $key;

        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {

            $fields = array();

            if($row["store_last_global_order_date"] <= $dates[$key])
            {
                $tmp = $value;
                $fields[] = "store_last_global_order = " . $tmp;
                $tmp = dbquote($dates[$key]);
                $fields[] = "store_last_global_order_date = " . $tmp;
            }

            $tmp = $value + $row["store_global_order"];
            $fields[] = "store_global_order = " . $tmp;

            $sql = "update stores set " . join(", ", $fields) . " where store_id = " . $key;
            mysql_query($sql) or dberror($sql);

        }
    
    }

    if($sid)
    {
        redirect("stock_global_orders.php?sid=" . $sid);
    }
    else
    {
        redirect("stock_global_orders.php");
    }

}


$page = new Page("stock");

$page->register_action('home', 'Home', "welcome.php");
$page->register_action('stock', 'Stock Control Data', "stock.php");

$page->header();
$page->title("Global Orders");

$list->render();
$page->footer();

?>