<?php
/********************************************************************

    project_view_attachments.php

    List of attachments of a project

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-26
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-12-19
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.


*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_attachments_in_projects");


register_param("pid");
set_referer("project_add_attachment.php");
set_referer("project_edit_attachment.php");



/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details

$project = get_project(param("pid"));


// get company's address
$client_address = get_address($project["order_client_address"]);


// get user data
$user_data = get_user(user_id());


// build sql for attachment entries
              


// build filter for the list of attachments
$list1_filter = "order_file_order = " . $project["project_order"];

$file_category_filter = "";

$user_roles = get_user_roles(user_id());
$categroy_restrictions = get_file_category_restirctions($user_roles);

if(count($categroy_restrictions) > 0)
{
	$file_category_filter = " and order_file_category IN (" . implode(",", $categroy_restrictions). ") ";
}

$list1_filter .=  $file_category_filter;


if (has_access("has_access_to_all_attachments_in_projects"))
{
	$sql_attachment = "select distinct order_file_id, order_file_visited, ".
                  "    CONCAT('<span class=\"file-type category-', order_file_category_id, '\" data-type=\"', file_type_extension, '\">', order_file_title, '</span>') AS order_file_title, order_file_description, ".
                  "    order_file_path, file_type_name, order_file_attach_to_cer_booklet, ".
                  "    order_files.date_created, ".
                  "    order_file_category_name, order_file_category_priority, ".
                  "    concat(user_name, ' ', user_firstname) as owner_fullname, ".
		          "    CONCAT(order_file_category_priority, order_file_category_name) as group_name, " . 
		           "   CONCAT('<span class=\"file-category\" data-id=\"', order_file_category_id, '\">', order_file_category_priority, ' ', order_file_category_name, '</span>') as group_name_lookup " . 
                  "from order_files " . 
                  "left join order_file_categories on order_file_category_id = order_file_category ".
                  "left join users on user_id = order_file_owner ".
                  "left join file_types on order_file_type = file_type_id ";



}
else
{
	$file_ids = array();
	$sql_order_addresses = 'select order_file_address_file from order_file_addresses ' .
		                  'where order_file_address_address = ' . $user_data["address"];

	$res = mysql_query($sql_order_addresses) or dberror($sql_oder_addresses);
    while ($row = mysql_fetch_assoc($res)) {
		$file_ids[] = $row['order_file_address_file'];
	}

	$file_filter = "";
	if(count($file_ids) > 0) {
		$file_filter = ' or order_file_id IN (' . implode(',', $file_ids) . ')';
	}
	
	
	$sql_attachment = "select distinct order_file_id, order_file_visited, ".
                  "    CONCAT('<span class=\"file-type category-', order_file_category_id, '\" data-type=\"', file_type_extension, '\">', order_file_title, '</span>') AS order_file_title, order_file_description, ".
                  "    order_file_path, file_type_name, ".
                  "    order_files.date_created, ".
                  "    order_file_category_name, order_file_category_priority, ".
                  "    concat(user_name, ' ', user_firstname) as owner_fullname, ".
		           "   CONCAT(order_file_category_priority, order_file_category_name) as group_name, " . 
		           "   CONCAT('<span class=\"file-category\" data-id=\"', order_file_category_id, '\">', order_file_category_priority, ' ', order_file_category_name, '</span>') as group_name_lookup " . 
                  "from order_files " . 
                  "left join order_file_categories on order_file_category_id = order_file_category ".
                  "left join users on user_id = order_file_owner ".
                  "left join file_types on order_file_type = file_type_id ";
	
	$list1_filter = $list1_filter . " and (order_file_owner = " . user_id() . $file_filter . ")";
}

$list1_filter .= " and order_file_category_type = 2";



//get ln_basic data
$ln_basicdata = array();

$sql = "select *,ln_basicdata.date_created as versiondate  from ln_basicdata " . 
	   "left join projects on project_id = ln_basicdata_project " . 
	   "where ln_basicdata_project = " . dbquote(param("pid")) . 
	   " and ln_basicdata_version = 0";
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$ln_basicdata = $row;
}



// get new comment info pix
$sql_pix = $sql_attachment . " where " . $list1_filter;
$images = set_new_attachment_pictures($sql_pix, $project["project_order"]);

$in_cer_booklet = array();

if (has_access("has_access_to_all_attachments_in_projects"))
{
	$res = mysql_query($sql_pix) or dberror($sql_pix);
	while ($row = mysql_fetch_assoc($res))
	{
		$in_cer_booklet[$row["order_file_id"]] = $row["order_file_attach_to_cer_booklet"];
	}
}

/********************************************************************
    Create Form
*********************************************************************/ 


$form = new Form("order_files", "order_file");


$form->add_hidden("pid", param("pid"));
$form->add_hidden("project_order", $project["project_order"]);


require_once "include/project_head_small.php";


/********************************************************************
    Create List
*********************************************************************/ 
$list1 = new ListView($sql_attachment);
$list1->setAttribute("id", "orderFiles");
$list1->set_title("Project Attachments");

$list1->set_entity("order_files");
$list1->set_filter($list1_filter);
$list1->set_order("order_file_category_priority, order_files.date_created DESC");
$list1->set_group("group_name", 'group_name_lookup');


$link = "project_edit_attachment.php?pid=" . param("pid");



if (has_access("can_edit_attachment_data_in_projects"))
{
    $list1->add_column("date_created", "Date/Time", $link, "", "", COLUMN_NO_WRAP);
}
else
{
    $list1->add_column("date_created", "Date/Time", "", "", "", COLUMN_NO_WRAP);
}

if(count($images)> 0)
{
    $list1->add_image_column("order_file_id", "New", 0, $images);
}

$list1->add_column("owner_fullname", "Made by", "", "", "", COLUMN_NO_WRAP);
$link = "http://" . $_SERVER["HTTP_HOST"] . "/include/openfile.php?id={order_file_id}";
$list1->add_column("order_file_title", "Title", $link, "", "", COLUMN_NO_WRAP | COLUMN_UNDERSTAND_HTML);


if (has_access("has_access_to_all_attachments_in_projects")
	 and $project["project_cost_type"] != 6)
{
	$list1->add_checkbox_column("order_file_attach_to_cer_booklet", "AF/CER<br />Booklet", COLUMN_UNDERSTAND_HTML, $in_cer_booklet);
}

$list1->add_column("file_type_name", "Type", "", "", "", COLUMN_NO_WRAP);
$list1->add_column("order_file_description", "Description");




if (has_access("can_add_attachments_in_projects"))
{
    $list1->add_button("add_attachment", "Add Attachment");
}

if (has_access("has_access_to_all_attachments_in_projects")
   and $project["project_cost_type"] != 6)
{
	$list1->add_button("save", "Save List");
}

$list1->add_button(LIST_BUTTON_LINK, "Download Attachments", array(
	'id' => 'btnDownloadAttachemnts',
	'href' => '/applications/templates/project/download.attachemnts.php?frame=1&pid='.$_REQUEST['pid'],
	'data-fancybox-type' => 'iframe'
));


/********************************************************************
    Create List of LN attachments
*********************************************************************/
$show_ln_documents = false;
if(count($ln_basicdata) > 0 and
	($ln_basicdata["ln_basicdata_floorplan"] 
	or $ln_basicdata["ln_basicdata_pix1"]
	or $ln_basicdata["ln_basicdata_pix2"]
	or $ln_basicdata["ln_basicdata_pix3"]
	or $ln_basicdata["ln_basicdata_draft_aggreement"])
	)
{
	$show_ln_documents = true;
	$floorplan = '<a href="' . $ln_basicdata["ln_basicdata_floorplan"] . '" target="_blank">Download File</a>';
	$location_layout = '<a href="' . $ln_basicdata["ln_basicdata_location_layout"] . '" target="_blank">Download File</a>';
	$pix1 = '<a href="' . $ln_basicdata["ln_basicdata_pix1"] . '" target="_blank">Download File</a>';
	$pix2 = '<a href="' . $ln_basicdata["ln_basicdata_pix2"] . '" target="_blank">Download File</a>';
	$pix3 = '<a href="' . $ln_basicdata["ln_basicdata_pix3"] . '" target="_blank">Download File</a>';
	$lease_agreement = '<a href="' . $ln_basicdata["ln_basicdata_draft_aggreement"] . '" target="_blank">Download File</a>';
	
	$form2 = new Form("ln_basicdata", "ln_basicdata");

	
	$camera = '<i class="fa fa-camera af_ln_docs"></i>';
	
	$form2->add_section("LN/AF Documents " . $camera);
	
	if($ln_basicdata["ln_basicdata_floorplan"])
	{
		$form2->add_label("f1", "Mall Map/Street Map", RENDER_HTML, $floorplan);
	}
	if($ln_basicdata["ln_basicdata_location_layout"])
	{
		$form2->add_label("f16", "Location layout with dimensions", RENDER_HTML, $location_layout);
	}
	if($ln_basicdata["ln_basicdata_pix1"])
	{
		$form2->add_label("f2", "Photo 1", RENDER_HTML, $pix1);
	}
	if($ln_basicdata["ln_basicdata_pix2"])
	{
		$form2->add_label("f3", "Photo 2", RENDER_HTML, $pix2);
	}
	if($ln_basicdata["ln_basicdata_pix2"])
	{
		$form2->add_label("f4", "Photo 3", RENDER_HTML, $pix3);
	}
	if(has_access("has_access_to_all_attachments_in_projects") and $ln_basicdata["ln_basicdata_draft_aggreement"])
	{
		$form2->add_label("f5", "Draft Lease Agreement", RENDER_HTML, $lease_agreement);
	}
}


/********************************************************************
    Create List of CER Mail attachments
*********************************************************************/ 
$sql_cer_mail_attachments = "select * from cer_mails";

$list2 = new ListView($sql_cer_mail_attachments);
$list2->set_title("CER/AF Mail Attachments");
$list2->set_entity("order_files");
$list2->set_filter("cer_mail_attachment1_path <> '' and cer_mail_project = " . param("pid"));
$list2->set_order("cer_mails.date_created DESC");

$list2->add_column("date_created", "Date/Time", "", "", "", COLUMN_NO_WRAP);
$list2->add_column("cer_mail_sender", "Created by", "", "", "", COLUMN_NO_WRAP);
$link = "http://" . $_SERVER["HTTP_HOST"] . "{cer_mail_attachment1_path}";
$list2->add_column("cer_mail_attachment1_title", "Title", $link, "", "", COLUMN_NO_WRAP);


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

$list1->populate();
$list1->process();




$list2->populate();
$list2->process();

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

if ($list1->button("add_attachment"))
{
    $link = "project_add_attachment.php?pid=" . param("pid");
    redirect ($link);
}
elseif ($list1->button("save"))
{

	foreach($list1->columns as $key=>$column)
	{
		 if($column["name"] == 'order_file_attach_to_cer_booklet')
		 {
			 foreach($column["values"] as $order_file_id=>$value)
			 {
				 if(array_key_exists('__order_files_order_file_attach_to_cer_booklet_' . $order_file_id, $_POST))
				 {
					 $sql_u = "update order_files set order_file_attach_to_cer_booklet = 1 " .
							  " where order_file_id = " . dbquote($order_file_id);
				 }
				 else
				 {
					 $sql_u = "update order_files set order_file_attach_to_cer_booklet = 0 "  .
							  " where order_file_id = " . dbquote($order_file_id);
				 }
				 $result = mysql_query($sql_u) or dberror($sql_u);
			 }
		 }
	}

	$link = "project_view_attachments.php?pid=" . param("pid");
    redirect ($link);


}

$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Attachments");
$form->render();

$list1->render();

if(has_access("has_access_to_all_attachments_in_projects") or has_access("has_access_to_cer")) {
	if($show_ln_documents == true)
	{
		echo '<p>&nbsp;</p>';
		$form2->render();
	}
}

if(has_access("has_access_to_all_attachments_in_projects") or has_access("has_access_to_cer")) {
	echo '<p>&nbsp;</p>';
	$list2->render();
}

require_once "include/project_footer_logistic_state.php";

$modalContent = "
<script type=\"text/javascript\">

	function intersect(a, b) {
		var t;
		if (b.length > a.length) t = b, b = a, a = t;
		return a.filter(function (e) {
		    if (b.indexOf(e) !== -1) return true;
		});
	}
	
	$(document).ready(function() {

		$('#btnDownloadAttachemnts').show();
		
		$(\"[data-fancybox-type=iframe]\").click(function(e) {	
			e.preventDefault();
		});

		$(\"[data-fancybox-type=iframe]\").fancybox({
			autoSize	: false,
			fitToView   : true,
		    width       : '800px',
		    height      : '640px',
		    margin 		: 0,
		    padding 	: 0,
			modal 		: true,
	        afterClose	: function() {
	        	if (typeof(modalAfterClose) == \"function\") { 
	        		modalAfterClose();
	        	}
	        },
	        beforeShow: function(){
		        $('body').css({'overflow-y':'hidden'});
		    },
		    afterClose: function(){
		        $('body').css({'overflow-y':'auto'});
		    }
		})

		var images = ['jpg', 'jpeg', 'png', 'gif', 'tif'];

		$('.file-category').each(function(i,e) {
			
			var el = $(e);
			var category = el.data('id');
			var items = $('.file-type.category-'+category).get();
		
			loop_items:
			for (var i in items) { 
				var item = $(items[i]);
				var types = item.data('type');
				var extnsions = types ? types.split(';') : [];
				var arr = intersect(extnsions, images);

				if (arr.length) {
					$('<i />').data('category', category).addClass('fa fa-camera show-gallery').insertAfter(el);
					break loop_items;
				}
			}
		})

		$(document).on('click', '.show-gallery', function(e) {

			e.preventDefault();

			var order = $('#project_order').val();
			var category = $(this).data('category');

			$.fancybox({
				type: 'iframe',
				href: '/applications/templates/image.gallery.php?section=project-order-files&order='+order+'&category='+category,
				closeBtn: true,
				closeClick: true,
				autoSize	: false,
			    width       : '800px',
			    height      : '640px',
			    margin 		: 0,
			    padding 	: 0
			})
		})


		$(document).on('click', '.af_ln_docs', function(e) {

			e.preventDefault();

			var order = $('#project_order').val();
			var category = $(this).data('category');

			$.fancybox({
				type: 'iframe',
				href: '/applications/templates/image.gallery.php?section=af_ln-files&order='+order,
				closeBtn: true,
				closeClick: true,
				autoSize	: false,
			    width       : '800px',
			    height      : '640px',
			    margin 		: 0,
			    padding 	: 0
			})
		})
	})
</script>
";

$page->footer();
?>