<?php
/********************************************************************

    project_edit_cost_monitoring_preview.php

    Edit cost monitoring sheet: preview

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-10-24
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-07
    Version:        1.2.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

if(!has_access("can_view_cost_monitoring") and !has_access("has_access_to_cost_monitoring"))
{
	redirect("noaccess.php");
}

register_param("pid");

set_referer("project_edit_cost_monitoring_group.php");
set_referer("project_edit_cost_monitoring_reinvoice.php");


/********************************************************************
    prepare all data needed
*********************************************************************/


// read project and order details
$project = get_project(param("pid"));

$order_currency = get_order_currency($project["project_order"]);
$system_currency = get_system_currency_fields();


// get company's address
$client_address = get_address($project["order_client_address"]);


/********************************************************************
    Get Cost Monitoring Sheet CER Data
*********************************************************************/ 
$cer_investments = array();
$cer_investments_approved = array();
$cer_investments_kl_additional_approved = array();

$total_from_cer = 0;
$total_from_cer_approved = 0;
$total_from_cer_additional_approved = 0;


$cer_investments_loc = array();
$cer_investments_approved_loc = array();
$cer_investments_kl_additional_approved_loc = array();

$total_from_cer_loc = 0;
$total_from_cer_approved_loc = 0;
$total_from_cer_additional_approved_loc = 0;

$landlord_contribution = 0;
$landlord_contribution_loc = 0;

$sql = "select * from cer_investments " . 
       "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
       "where cer_investment_type in(1, 3, 5, 7, 11, 19) and cer_investment_project = " . param("pid")  .
	   " and cer_investment_cer_version = 0 " . 
	   " order by cer_investment_type";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res)) {

	
	
	//local currency
	
	if($row["cer_investment_type"] == 19)
	{
		$landlord_contribution_loc = $project["project_cost_real_landlord_contribution_loc"];
	}
	else
	{
		$cer_investments_loc[] = $row["cer_investment_amount_cer_loc"];
		$total_from_cer_loc = $total_from_cer_loc + $row["cer_investment_amount_cer_loc"];
		

		if($project["project_cost_type"] == 1) // corporate
		{
			$cer_investments_approved_loc[] = $row["cer_investment_amount_cer_loc_approved"];
			$cer_investments_kl_additional_approved_loc[] = $row["cer_investment_amount_additional_cer_loc_approved"];
			$total_from_cer_approved_loc = $total_from_cer_approved_loc + $row["cer_investment_amount_cer_loc_approved"];
			$total_from_cer_additional_approved_loc = $total_from_cer_additional_approved_loc + $row["cer_investment_amount_additional_cer_loc_approved"];
		}
		else
		{
			$cer_investments_approved_loc[] = '';
			$cer_investments_kl_additional_approved_loc[] = '';
		}
	}
	
	//system currency
	$exchange_rate = $order_currency["exchange_rate"];
	$factor = $order_currency["factor"];

	if($row["cer_investment_type"] == 19)
	{
		$landlord_contribution = $exchange_rate*$project["project_cost_real_landlord_contribution_loc"]/$factor;
	}
	else
	{
		$cer_investments[] = $exchange_rate*$row["cer_investment_amount_cer_loc"]/$factor;
		$total_from_cer = $total_from_cer + ($exchange_rate*$row["cer_investment_amount_cer_loc"]/$factor);


		if($project["project_cost_type"] == 1) // corporate
		{
			$cer_investments_approved[] = $exchange_rate*$row["cer_investment_amount_cer_loc_approved"]/$factor;
			$cer_investments_kl_additional_approved[] = $exchange_rate*$row["cer_investment_amount_additional_cer_loc_approved"]/$factor;
			$total_from_cer_approved = $total_from_cer_approved + ($exchange_rate*$row["cer_investment_amount_cer_loc_approved"]/$factor);
			$total_from_cer_additional_approved = $total_from_cer_additional_approved + ($exchange_rate*$row["cer_investment_amount_additional_cer_loc_approved"]/$factor);
		}
		else
		{
			$cer_investments_approved[] = '';
			$cer_investments_kl_additional_approved[] = '';
		}
	}
}


//local currency
for($i=0;$i<5;$i++) {
	if(!array_key_exists($i, $cer_investments_loc)) {
		$cer_investments_loc[$i] = 0;
	}
	if(!array_key_exists($i, $cer_investments_approved_loc)) {
		$cer_investments_approved_loc[$i] = 0;
	}
	if(!array_key_exists($i, $cer_investments_kl_additional_approved_loc)) {
		$cer_investments_kl_additional_approved_loc[$i] = 0;
	}
}

//system currency
for($i=0;$i<5;$i++) {
	if(!array_key_exists($i, $cer_investments)) {
		$cer_investments[$i] = 0;
	}
	if(!array_key_exists($i, $cer_investments_approved)) {
		$cer_investments_approved[$i] = 0;
	}
	if(!array_key_exists($i, $cer_investments_kl_additional_approved)) {
		$cer_investments_kl_additional_approved[$i] = 0;
	}
}


/********************************************************************
    Get Cost Monitoring Sheet Budget Data from project_costs
*********************************************************************/ 
$project_cost_gross_sqms = $project["project_cost_grosssqms"];
$project_cost_total_sqms = $project["project_cost_totalsqms"];
$project_cost_sqms = $project["project_cost_sqms"];

$project_cost_id = $project["project_cost_id"];


$projectctype = $project["project_cost_type"];

$cms_approved = $project["project_cost_cms_approved"];
$cms_approved_by = $project["project_cost_cms_approved_by"];
$cms_approved_date = $project["project_cost_cms_approved_date"];

$cms_completed = $project["project_cost_cms_completed"];
$cms_completed_by = $project["project_cost_cms_completed_by"];
$cms_completion_date = $project["project_cost_cms_completion_date"];

/********************************************************************
    project items coming from catalogue orders
*********************************************************************/ 
$has_catalogue_orders = false;


$sql_oip = "select order_items_in_project_order_item_id, order_items_in_project_id, order_item_order, " . 
		   " IF(address_shortcut <> '', address_shortcut, order_item_supplier_freetext) as address_shortcut, " .
		   "order_item_text,  order_item_id, order_items_in_project_cms_remark, " .
		   "DATE_FORMAT(order_item_ordered, '%d.%m.%y') as date1, " .
			"DATE_FORMAT(order_item_pickup, '%d.%m.%y') as date2, " .
			"DATE_FORMAT(order_item_arrival, '%d.%m.%y') as date3, " .
			"TRUNCATE(sum(order_item_quantity * order_items_in_project_real_price), 2) as investment, ".
			"TRUNCATE(sum(order_item_quantity * order_items_in_project_real_client_price), 2) as investment_loc, ".
		   " order_item_po_number, order_item_quantity, project_cost_groupname_name, " . 
		   "DATE_FORMAT(order_item_reinvoiced, '%d.%m.%y') as date4, " .
			"order_item_reinvoicenbr, " .
			"DATE_FORMAT(order_item_reinvoiced2, '%d.%m.%y') as date42, " .
			"order_item_reinvoicenbr2, order_item_cms_remark " .
		   " from order_items_in_projects " .
		   " left join order_items on order_item_id = order_items_in_project_order_item_id " .
		   " left join addresses on order_item_supplier_address = address_id ".
		   " left join project_cost_groupnames on project_cost_groupname_id =  order_items_in_project_costgroup_id " .
		   " where order_items_in_project_project_order_id = " . $project["project_order"] . 
		   " group by order_items_in_project_id";

$grouptotals_oip = 0;
$grouptotals_oip_loc = 0;
$res = mysql_query($sql_oip) or dberror($sql_oip);
$replacementinfos = array();

while($row = mysql_fetch_assoc($res))
{
	$grouptotals_oip = $grouptotals_oip  + $row["investment"];
	$grouptotals_oip_loc = $grouptotals_oip_loc  + $row["investment_loc"];
	$has_catalogue_orders = true;


	//check if item is a replacement
	$sql_r = "select count(order_item_replacement_id) as num_recs " . 
		     " from order_item_replacements " . 
		     " where order_item_replacement_catalog_order_order_item_id = " . dbquote($row["order_item_id"]);

	$res_r = mysql_query($sql_r) or dberror($sql_r);
	$row_r = mysql_fetch_assoc($res_r);
	if($row_r["num_recs"] > 0)
	{
		$replacementinfos[$row["order_items_in_project_order_item_id"]] = '<span class="error">Replacement</span>';
	}
}


/********************************************************************
    Calculate difference in percent
*********************************************************************/ 
$project_cost_real = 0;
$project_cost_budget = 0;
$project_cost_hq_budget = 0;


$project_cost_real_loc = 0;
$project_cost_budget_loc = 0;
$project_cost_hq_budget_loc = 0;

$grouptotals_investment = array();
$grouptotals_investment[2] = 0;
$grouptotals_investment[6] = 0;
$grouptotals_investment[7] = 0;
$grouptotals_investment[8] = 0;
$grouptotals_investment[9] = 0;
$grouptotals_investment[10] = 0;
$grouptotals_investment[11] = 0;
$grouptotals_investment[19] = "";

$grouptotals_investment_loc = array();
$grouptotals_investment_loc[2] = 0;
$grouptotals_investment_loc[6] = 0;
$grouptotals_investment_loc[7] = 0;
$grouptotals_investment_loc[8] = 0;
$grouptotals_investment_loc[9] = 0;
$grouptotals_investment_loc[10] = 0;
$grouptotals_investment_loc[11] = 0;
$grouptotals_investment_loc[19] = "";

$grouptotals_hq_investment = array();
$grouptotals_hq_investment[2] = 0;
$grouptotals_hq_investment[6] = 0;

$grouptotals_hq_investment_loc = array();
$grouptotals_hq_investment_loc[2] = 0;
$grouptotals_hq_investment_loc[6] = 0;

$grouptotals_real = array();
$grouptotals_real[2] = 0;
$grouptotals_real[6] = 0;
$grouptotals_real[7] = 0;
$grouptotals_real[8] = 0;
$grouptotals_real[9] = 0;
$grouptotals_real[10] = 0;
$grouptotals_real[11] = 0;
$grouptotals_real[19] = -1*$landlord_contribution;

$grouptotals_real_loc = array();
$grouptotals_real_loc[2] = 0;
$grouptotals_real_loc[6] = 0;
$grouptotals_real_loc[7] = 0;
$grouptotals_real_loc[8] = 0;
$grouptotals_real_loc[9] = 0;
$grouptotals_real_loc[10] = 0;
$grouptotals_real_loc[11] = 0;
$grouptotals_real_loc[19] = -1*$landlord_contribution_loc;

$grouptotals_difference_in_cost = array();
$grouptotals_difference_in_cost[2] = 0;
$grouptotals_difference_in_cost[6] = 0;
$grouptotals_difference_in_cost[7] = 0;
$grouptotals_difference_in_cost[8] = 0;
$grouptotals_difference_in_cost[9] = 0;
$grouptotals_difference_in_cost[10] = 0;
$grouptotals_difference_in_cost[11] = 0;
$grouptotals_difference_in_cost[19] = "";

$grouptotals_difference_in_cost_loc = array();
$grouptotals_difference_in_cost_loc[2] = 0;
$grouptotals_difference_in_cost_loc[6] = 0;
$grouptotals_difference_in_cost_loc[7] = 0;
$grouptotals_difference_in_cost_loc[8] = 0;
$grouptotals_difference_in_cost_loc[9] = 0;
$grouptotals_difference_in_cost_loc[10] = 0;
$grouptotals_difference_in_cost_loc[11] = 0;
$grouptotals_difference_in_cost_loc[19] = "";

$grouptotals_difference_in_cost2 = array();
$grouptotals_difference_in_cost2[2] = 0;
$grouptotals_difference_in_cost2[6] = 0;
$grouptotals_difference_in_cost2[7] = 0;
$grouptotals_difference_in_cost2[8] = 0;
$grouptotals_difference_in_cost2[9] = 0;
$grouptotals_difference_in_cost2[10] = 0;
$grouptotals_difference_in_cost2[11] = 0;
$grouptotals_difference_in_cost2[19] = "";

$num_recs2 = 0;
$num_recs6 = 0;
$num_recs7 = 0;
$num_recs8 = 0;
$num_recs9 = 0;
$num_recs10 = 0;
$num_recs11 = 0;


$sql = "select order_item_cost_group, order_item_type, ".
	   "order_item_client_price, order_item_real_client_price, order_item_quantity,  ".
	   "order_item_client_price_freezed, order_item_quantity_freezed, ".
	   "order_item_client_price_hq_freezed, order_item_quantity_hq_freezed, " . 
	   "order_item_system_price, order_item_real_system_price,  ".
	   "order_item_system_price_freezed, order_item_quantity_freezed, ".
	   "order_item_system_price_hq_freezed, order_item_quantity_hq_freezed " .
	   "from order_items ".
	   "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
	   "   and order_item_order=" . $project["project_order"] . 
	   "   and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION .
	   "   or order_item_type = " . ITEM_TYPE_SERVICES .
	   "   or order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST .
	   " ) " .
	   " and order_item_cost_group > 0 " . 
	   " order by order_item_cost_group";


$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	//local currency
	$grouptotals_investment_loc[$row["order_item_cost_group"]] = $grouptotals_investment_loc[$row["order_item_cost_group"]] + $row["order_item_client_price_freezed"] * $row["order_item_quantity_freezed"];
		
	$project_cost_budget_loc = $project_cost_budget_loc + $row["order_item_client_price_freezed"] * $row["order_item_quantity_freezed"];

	if($row["order_item_cost_group"] == 2 or $row["order_item_cost_group"] == 6)
	{
		$grouptotals_hq_investment_loc[$row["order_item_cost_group"]] = $grouptotals_hq_investment_loc[$row["order_item_cost_group"]] + $row["order_item_client_price_hq_freezed"] * $row["order_item_quantity_hq_freezed"];
	
		$project_cost_hq_budget_loc = $project_cost_hq_budget_loc + $row["order_item_client_price_hq_freezed"] * $row["order_item_quantity_hq_freezed"];
	}

	$grouptotals_real_loc[$row["order_item_cost_group"]] =  $grouptotals_real_loc[$row["order_item_cost_group"]] + ($row["order_item_real_client_price"] * $row["order_item_quantity"]);

	
	$project_cost_real_loc = $project_cost_real_loc + $row["order_item_real_client_price"] * $row["order_item_quantity"];
	
	//system currency
	$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_system_price_freezed"] * $row["order_item_quantity_freezed"];
	
	$project_cost_budget = $project_cost_budget + $row["order_item_system_price_freezed"] * $row["order_item_quantity_freezed"];

	
	if($row["order_item_cost_group"] == 2 or $row["order_item_cost_group"] == 6)
	{
		$grouptotals_hq_investment[$row["order_item_cost_group"]] = $grouptotals_hq_investment[$row["order_item_cost_group"]] + $row["order_item_system_price_hq_freezed"] * $row["order_item_quantity_hq_freezed"];
	
		$project_cost_hq_budget = $project_cost_hq_budget + $row["order_item_system_price_hq_freezed"] * $row["order_item_quantity_hq_freezed"];
	}

	$grouptotals_real[$row["order_item_cost_group"]] =  $grouptotals_real[$row["order_item_cost_group"]] + ($row["order_item_real_system_price"] * $row["order_item_quantity"]);
	
	$project_cost_real = $project_cost_real + $row["order_item_real_system_price"] * $row["order_item_quantity"];

	
	
	if($row["order_item_cost_group"] == 6)  //freight charges
    {
        $num_recs6 = $num_recs6 + 1;
    }
    elseif($row["order_item_cost_group"] == 7)  //local construction cost
    {
        $num_recs7 = $num_recs7 + 1;
    }
	elseif($row["order_item_cost_group"] == 9)  //architectural cost
    {
        $num_recs9 = $num_recs9 + 1;
    }
	elseif($row["order_item_cost_group"] == 10)  //fixturing other cost
    {
        $num_recs10 = $num_recs10 + 1;
    }
	elseif($row["order_item_cost_group"] == 11)  //fixturing other cost
    {
        $num_recs11 = $num_recs11 + 1;
    }
	elseif($row["order_item_cost_group"] == 8) // other cost
    {
        $num_recs8 = $num_recs8 + 1;
    }
    elseif($row["order_item_cost_group"] == 2) // HQ Suplied Items
    {
        $num_recs2 = $num_recs2 + 1;
    }
}

//add catalogue orders
$grouptotals_real[2] = $grouptotals_real[2] + $grouptotals_oip;
$grouptotals_real_oip[2] = number_format($grouptotals_oip, 2, ".", "'");

$grouptotals_investment1 = array();
$grouptotals_hq_investment1 = array();
$grouptotals_real1 = array();
foreach($grouptotals_investment as $key=>$value)
{
	if(!$value){$value = 0;}
	
	$grouptotals_investment1[$key] = number_format($value, 2, ".", "'");
	
	if($row["order_item_cost_group"] == 2 or $row["order_item_cost_group"] == 6)
	{
		$grouptotals_hq_investment1[$key] = number_format($grouptotals_hq_investment[$key], 2, ".", "'");
	}
	$grouptotals_real1[$key] = number_format($grouptotals_real[$key], 2, ".", "'");

}


$grouptotals_real_loc[2] = $grouptotals_real_loc[2] + $grouptotals_oip_loc;
$grouptotals_real_oip_loc[2] = number_format($grouptotals_oip_loc, 2, ".", "'");

$grouptotals_investment1_loc = array();
$grouptotals_hq_investment1_loc = array();
$grouptotals_real1_loc = array();
foreach($grouptotals_investment_loc as $key=>$value)
{
	
	if($value == ''){$value = 0;}
	$grouptotals_investment1_loc[$key] = number_format($value, 2, ".", "'");
	if($row["order_item_cost_group"] == 2 or $row["order_item_cost_group"] == 6)
	{
		$grouptotals_hq_investment1_loc[$key] = number_format($grouptotals_hq_investment_loc[$key], 2, ".", "'");
	}
	$grouptotals_real1_loc[$key] = number_format($grouptotals_real_loc[$key], 2, ".", "'");

}


//calculate difference in percent
$project_cost_real = 0;
$project_cost_real_loc = 0;
foreach($grouptotals_real as $key=>$value)
{
     //system currency
	 $project_cost_real = $project_cost_real + $value;
     $grouptotals_difference_in_cost[$key] =  number_format($grouptotals_real[$key] - $grouptotals_investment[$key], 2, ".", "'");
     
     if($grouptotals_investment[$key] > 0)
     {
        $grouptotals_difference_in_cost2[$key] =  $grouptotals_real[$key] - $grouptotals_investment[$key];
        $grouptotals_difference_in_cost2[$key] = -1 * (1- ($value / $grouptotals_investment[$key]));
     }
     else
     {
        $grouptotals_difference_in_cost2[$key] = 1;
        $grouptotals_investment[$key] = 0;
     }
     
     $grouptotals_difference_in_cost2[$key] = number_format(100*$grouptotals_difference_in_cost2[$key], 2, ".", "'")  . "%";

	 //local currency
	 $project_cost_real_loc = $project_cost_real_loc + $grouptotals_real_loc[$key];
     $grouptotals_difference_in_cost_loc[$key] =  number_format($grouptotals_real_loc[$key] - $grouptotals_investment_loc[$key], 2, ".", "'");
     
     if($grouptotals_investment_loc[$key] > 0)
     {
        $grouptotals_difference_in_cost2_loc[$key] =  $grouptotals_real_loc[$key] - $grouptotals_investment_loc[$key];
        $grouptotals_difference_in_cost2_loc[$key] = -1 * (1- ($grouptotals_real_loc[$key] / $grouptotals_investment_loc[$key]));
     }
     else
     {
        $grouptotals_difference_in_cost2_loc[$key] = 1;
        $grouptotals_investment_loc[$key] = 0;
     }
     
     $grouptotals_difference_in_cost2_loc[$key] = number_format(100*$grouptotals_difference_in_cost2_loc[$key], 2, ".", "'")  . "%";
}




//system currency
$project_cost_diff1 =  $project_cost_real - $project_cost_budget;

if($project_cost_budget == 0 and $project_cost_real == 0)
{
	 $project_cost_diff2 = 0;
}
elseif($project_cost_budget > 0)
{
    $project_cost_diff2 = -1 * ( 1 - ($project_cost_real / $project_cost_budget));
}
else
{
    $project_cost_diff2 = 0;
}

if($total_from_cer_approved > 0 and $project["project_cost_type"] == 1) //corporate
{
	$project_cost_diff3 = -1 * ( 1 - ($project_cost_real / ($total_from_cer_approved + $total_from_cer_additional_approved)));
}
else
{
    $project_cost_diff3 = 0;
}


//local currency
$project_cost_diff1_loc =  $project_cost_real_loc - $project_cost_budget_loc;

if($project_cost_budget_loc == 0 and $project_cost_real_loc == 0)
{
	 $project_cost_diff2_loc = 0;
}
elseif($project_cost_budget_loc > 0)
{
    $project_cost_diff2_loc = -1 * ( 1 - ($project_cost_real_loc / $project_cost_budget_loc));
}
else
{
    $project_cost_diff2_loc = 0;
}

if($total_from_cer_loc == 0 and $project_cost_real_loc == 0)
{
	$project_cost_diff3_loc = 0;
}
elseif($total_from_cer_approved_loc > 0 or $total_from_cer_additional_approved_loc > 0)
{
    if($project["project_cost_type"] == 1) // corporate
	{
		$project_cost_diff3_loc = -1 * ( 1 - ($project_cost_real_loc / ($total_from_cer_approved_loc + $total_from_cer_additional_approved_loc)));
	}
	else
	{
		$project_cost_diff3_loc = -1 * ( 1 - ($project_cost_real_loc / $cer_total_loc));
	}
}
else
{
    $project_cost_diff3_loc = 0;
}



/********************************************************************
    List SQls
*********************************************************************/ 

if($landlord_contribution > 0)
{
	$sql_cost_groups = "select * from project_cost_groupnames";
}
else
{
	$sql_cost_groups = "select * from project_cost_groupnames where project_cost_groupname_active = 1";
}



$sql_base = "select max(order_item_id) as item, " .
			"IF(address_shortcut <> '', address_shortcut, order_item_supplier_freetext) as address_shortcut, " .
			"DATE_FORMAT(order_item_ordered, '%d.%m.%y') as date1, " .
			"DATE_FORMAT(order_item_pickup, '%d.%m.%y') as date2, " .
			"DATE_FORMAT(order_item_arrival, '%d.%m.%y') as date3, " .
			//"IF(order_item_quantity_freezed>0,TRUNCATE(sum(order_item_quantity_freezed * order_item_client_price_freezed), 2), order_item_client_price_freezed) as approved, ". 
			//"TRUNCATE(sum(order_item_quantity * order_item_client_price), 2) as investment, ".
			"TRUNCATE(sum(order_item_quantity_freezed * order_item_client_price_freezed), 2) as approved_loc, ".
			"TRUNCATE(sum(order_item_quantity * order_item_real_client_price), 2) as investment_loc, ".
			"TRUNCATE(sum(order_item_quantity_freezed * order_item_system_price_freezed), 2) as approved, ".
			"TRUNCATE(sum(order_item_quantity * order_item_real_system_price), 2) as investment, ". 
			"if(order_items_monitoring_group is not null, order_items_monitoring_group, order_item_text) as monitoring_group,  " .
			"order_item_po_number, " .
			"DATE_FORMAT(order_item_reinvoiced, '%d.%m.%y') as date4, " .
			"order_item_reinvoicenbr, " .
			"DATE_FORMAT(order_item_reinvoiced2, '%d.%m.%y') as date42, " .
			"order_item_reinvoicenbr2, order_item_cms_remark " .
			"from order_items ".
			"left join items on order_item_item = item_id ".
			"left join addresses on order_item_supplier_address = address_id ".
			"left join item_types on order_item_type = item_type_id " . 
			"left join project_cost_groupnames on project_cost_groupname_id =  order_item_cost_group ";

$sql_grouping = " group by address_shortcut, monitoring_group, " .
             "   date1, date2, date3, order_item_po_number, " .
             "   date4, order_item_reinvoicenbr, order_item_reinvoicenbr2, order_item_cms_remark " .
             "order by address_shortcut ";


$sql_list7 = $sql_base . 
             "where order_item_cost_group = 7 " . 
             " and order_item_order = " . $project["project_order"] . 
             $sql_grouping;


$sql_list2 = $sql_base . 
             "where order_item_cost_group = 2 " .
             "   and order_item_order = " . $project["project_order"] . 
             $sql_grouping;

$sql_list6 = $sql_base . 
             "where order_item_cost_group = 6 " .
             "   and order_item_order = " . $project["project_order"] . 
             $sql_grouping;

$sql_list10 = $sql_base . 
             "where order_item_cost_group = 10 " .
             "   and order_item_order = " . $project["project_order"] . 
             $sql_grouping;

$sql_list9 = $sql_base . 
             "where order_item_cost_group = 9 " .
             "   and order_item_order = " . $project["project_order"] . 
             $sql_grouping;

$sql_list11 = $sql_base . 
             "where order_item_cost_group = 11 " .
             "   and order_item_order = " . $project["project_order"] . 
             $sql_grouping;

$sql_list8 = $sql_base . 
             "where order_item_cost_group = 8 " .
             "   and order_item_order = " . $project["project_order"] . 
             $sql_grouping;






/********************************************************************
	Calucalate Shares
*********************************************************************/ 

//local construction work
$shares_p7 = array();

$sql = $sql_list7;
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($project_cost_real > 0)
	{
		$tmp = 100*$row["investment"]/$project_cost_real;
        $shares_p7[$row["item"]] = number_format(round($tmp,2),2, ".", "'") . "%";
	}
	else
	{
		$shares_p7[$row["item"]] = "0.00%";
	}
}

if($project_cost_real > 0)
{
	$share_t7 = number_format(round(100*$grouptotals_real[7] / $project_cost_real,2),2, ".", "'") . "%";
}
else
{
	$share_t7 = "0.00%";
}

//HQ Supplied Items
$shares_p2 = array();

$sql = $sql_list2;
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($project_cost_real > 0)
	{
		$tmp = 100*$row["investment"]/$project_cost_real;
        $shares_p2[$row["item"]] = number_format(round($tmp,2),2, ".", "'") . "%";
	}
	else
	{
		$shares_p2[$row["item"]] = "0.00%";
	}
}

if($project_cost_real > 0)
{
	$share_t2 = number_format(round(100*$grouptotals_real[2] / $project_cost_real,2),2, ".", "'") . "%";
}
else
{
	$share_t2 = "0.00%";
}

//Freight Charges
$shares_p6 = array();

$sql = $sql_list6;
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($project_cost_real > 0)
	{
		$tmp = 100*$row["investment"]/$project_cost_real;
        $shares_p6[$row["item"]] = number_format(round($tmp,2),2, ".", "'") . "%";
	}
	else
	{
		$shares_p6[$row["item"]] = "0.00%";
	}
}

if($project_cost_real > 0)
{
	$share_t6 = number_format(round(100*$grouptotals_real[6] / $project_cost_real,2),2, ".", "'") . "%";
}
else
{
	$share_t6 = "0.00%";
}

//Fixturing Other Cost
$shares_p10 = array();

$sql = $sql_list10;
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($project_cost_real > 0)
	{
		$tmp = 100*$row["investment"]/$project_cost_real;
        $shares_p10[$row["item"]] = number_format(round($tmp,2),2, ".", "'") . "%";
	}
	else
	{
		$shares_p10[$row["item"]] = "0.00%";
	}
}

if($project_cost_real > 0)
{
	$share_t10 = number_format(round(100*$grouptotals_real[10] / $project_cost_real,2),2, ".", "'") . "%";
}
else
{
	$share_t10 = "0.00%";
}

//Architectural Cost
$shares_p9 = array();

$sql = $sql_list9;
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($project_cost_real > 0)
	{
		$tmp = 100*$row["investment"]/$project_cost_real;
        $shares_p9[$row["item"]] = number_format(round($tmp,2),2, ".", "'") . "%";
	}
	else
	{
		$shares_p9[$row["item"]] = "0.00%";
	}
}

if($project_cost_real > 0)
{
	$share_t9 = number_format(round(100*$grouptotals_real[9] / $project_cost_real,2),2, ".", "'") . "%";
}
else
{
	$share_t9 = "0.00%";
}

//Equipment Cost
$shares_p11 = array();

$sql = $sql_list11;
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($project_cost_real > 0)
	{
		$tmp = 100*$row["investment"]/$project_cost_real;
        $shares_p11[$row["item"]] = number_format(round($tmp,2),2, ".", "'") . "%";
	}
	else
	{
		$shares_p11[$row["item"]] = "0.00%";
	}
}

if($project_cost_real > 0)
{
	$share_t11 = number_format(round(100*$grouptotals_real[11] / $project_cost_real,2),2, ".", "'") . "%";
}
else
{
	$share_t11 = "0.00%";
}

//Other Cost
$shares_p8 = array();

$sql = $sql_list8;
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	if($project_cost_real > 0)
	{
		$tmp = 100*$row["investment"]/$project_cost_real;
        $shares_p8[$row["item"]] = number_format(round($tmp,2),2, ".", "'") . "%";
	}
	else
	{
		$shares_p8[$row["item"]] = "0.00%";
	}
}

if($project_cost_real > 0)
{
	$share_t8 = number_format(round(100*$grouptotals_real[8] / $project_cost_real,2),2, ".", "'") . "%";
}
else
{
	$share_t8 = "0.00%";
}


//Cots of Catalogue items in projects
$shares_oip = array();

$res = mysql_query($sql_oip) or dberror($sql_oip);
while($row = mysql_fetch_assoc($res))
{
	if($project_cost_real > 0)
	{
		$tmp = 100*$row["investment"]/$project_cost_real;
        $shares_oip[$row["order_items_in_project_order_item_id"]] = number_format(round($tmp,2),2, ".", "'") . "%";
	}
	else
	{
		$shares_oip[$row["order_items_in_project_order_item_id"]] = "0.00%";
	}

}

if($project_cost_real > 0)
{
	$share_oip = number_format(round(100*$grouptotals_oip / $project_cost_real,2),2, ".", "'") . "%";
}
else
{
	$share_oip = "0.00%";
}


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid",$project["project_order"]);

require_once "include/project_head_small.php";

$form->add_label("spacer", "", 0, "");

$form->add_label("project_cost_type", "Project Legal Type", 0, $project["project_costtype_text"]);
//$form->add_label("project_cost_gross_sqms", "Gross Surface sqms", 0, $project_cost_gross_sqms);
$form->add_label("project_cost_total_sqms", "Total Surface sqms", 0, $project_cost_total_sqms);
$form->add_label("project_cost_sqms", "Sales Surface sqms", 0, $project_cost_sqms);

$form->add_label("spacer1", "", 0, "");

$form->add_label("cb1", "Budget approved by Client " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($project_cost_budget, 2, ".", "'") . " / " . number_format($project_cost_budget_loc, 2, ".", "'"));
$form->add_label("cb4", "Real Cost (Material, Freight, Local Cost) in " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($project_cost_real, 2, ".", "'") . " / " . number_format($project_cost_real_loc, 2, ".", "'"));

if($project_cost_diff2 > 0)
{
    $output = number_format(100*$project_cost_diff2, 2, ".", "'") . "%";
	$output = "<font color='#FF0000'><strong>" . $output . "</strong></font>";
    $form->add_label("cb5", "Real Cost above approved Budget", RENDER_HTML, $output);
}
else
{
    $output = number_format(100*$project_cost_diff2, 2, ".", "'") . "%";
	$form->add_label("cb5", "Real Cost below approved Budget", 0, $output);
}



if($projectctype == 1) // Corporate
{	    
	
	$form->add_label("spacer2", " ", 0, "");
	$form->add_label("title1_loc", "KL approved investment in " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($total_from_cer_approved + $total_from_cer_additional_approved, 2, ".", "'") . " / " . number_format($total_from_cer_approved_loc + $total_from_cer_additional_approved_loc, 2, ".", "'"));

	$form->add_label("cer1_loc", "- Construction - Buildung " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0 , number_format($cer_investments_approved[0] + $cer_investments_kl_additional_approved[0], 2, ".", "'") . " / " . number_format($cer_investments_approved_loc[0] + $cer_investments_kl_additional_approved_loc[0], 2, ".", "'"));
	$form->add_label("cer2_loc", "- Store fixturing / Furniture " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0 , number_format($cer_investments_approved[1] + $cer_investments_kl_additional_approved[1], 2, ".", "'") . " / " . number_format($cer_investments_approved_loc[1] + $cer_investments_kl_additional_approved_loc[1], 2, ".", "'"));
	$form->add_label("cer3_loc", "- Architectural Services " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0 , number_format($cer_investments_approved[2] + $cer_investments_kl_additional_approved[2], 2, ".", "'") . " / " . number_format($cer_investments_approved_loc[2] + $cer_investments_kl_additional_approved_loc[2], 2, ".", "'"));
	$form->add_label("cer4_loc", "- Equipment " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0 , number_format($cer_investments_approved[3] + $cer_investments_kl_additional_approved[3], 2, ".", "'") . " / " . number_format($cer_investments_approved_loc[3] + $cer_investments_kl_additional_approved_loc[3], 2, ".", "'"));
	$form->add_label("cer5_loc", "- Other " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0 , number_format($cer_investments_approved[4] + $cer_investments_kl_additional_approved[4], 2, ".", "'") . " / " . number_format($cer_investments_approved_loc[4] + $cer_investments_kl_additional_approved_loc[4], 2, ".", "'"));

	
	$form->add_label("cb51_loc", "Real Cost in " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($project_cost_real, 2, ".", "'") . " / " . number_format($project_cost_real_loc, 2, ".", "'"));
	if($project_cost_diff3 >= 0)
	{
		$output = number_format(100*$project_cost_diff3, 2, ".", "'") . "%";
		$output = "<font color='#FF0000'><strong>" . $output . "</strong></font>";
		$form->add_label("cb6_loc", "Real Cost above approved Investment ", RENDER_HTML, $output);
	}
	else
	{
		$output = number_format(100*$project_cost_diff3, 2, ".", "'") . "%";
		$form->add_label("cb6_loc", "Real Cost below approved Investment ", 0, $output);
	}
}

if (strpos($_SERVER["SCRIPT_FILENAME"], 'archive') > 0 and has_access("can_edit_catalog"))
{
	$form->add_button("cost_monitoring", "Back");  
}
else
{
	$form->add_button("cost_monitoring", "Back");  
}

if(has_access("can_print_cost_monitoring_sheet"))
{
	$link = "project_edit_cost_monitoring_preview_pdf.php?pid=" . param("pid") . "&chf=1"; 
	$link = "javascript:popup('". $link . "', 800, 600)";
	$form->add_button("pdf_chf", "Print PDF in CHF", $link);

	if($order_currency["symbol"] != $system_currency["symbol"])
	{
		$link = "project_edit_cost_monitoring_preview_pdf.php?pid=" . param("pid"); 
		$link = "javascript:popup('". $link . "', 800, 600)";
		$form->add_button("pdf_loc", "Print PDF in " . $order_currency["symbol"], $link);
	}
}

if (has_access("can_edit_cost_monitoring_reinvoice_data"))
{
	$form->add_button("reinvoice", "Reinvoice Data");
	$form->add_button("group", "Group Data");
}



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("cost_monitoring"))
{
    $link = "project_edit_cost_monitoring.php?pid=" . param("pid"); 
    redirect($link);
}
elseif ($form->button("reinvoice"))
{
    $link = "project_edit_cost_monitoring_reinvoice.php?pid=" . param("pid"); 
    redirect($link);
}
elseif ($form->button("group"))
{
    $link = "project_edit_cost_monitoring_group.php?pid=" . param("pid"); 
    redirect($link);
}


/********************************************************************
	LIST'S SECTION
*********************************************************************/ 

if($project["project_cost_type"] == 1)  // Corporate
{
    /********************************************************************
        Create List for Cost Types
    *********************************************************************/ 
    $list0 = new ListView($sql_cost_groups, LIST_HAS_HEADER | LIST_HAS_FOOTER);
    $list0->set_title("Cost Groups");
    $list0->set_entity("project_cost_groupnames");
    $list0->set_order("project_cost_groupname_sortorder");



    $list0->add_column("project_cost_groupname_name", "Group");

    $list0->add_text_column("investment_hq", "Approved 620\nin " . $system_currency["symbol"], COLUMN_BG_STRONG | COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $grouptotals_hq_investment1);
	$list0->add_text_column("investment", "Approved 840\nin " . $system_currency["symbol"], COLUMN_BG_STRONG | COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $grouptotals_investment1);

    $list0->add_text_column("real_cost", "Real Cost\nin " . $system_currency["symbol"], COLUMN_BG_STRONG | COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $grouptotals_real1);

	if($grouptotals_oip > 0)
	{
		$list0->add_text_column("real_cost", "CO\nin " . $system_currency["symbol"], COLUMN_BG_STRONG | COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $grouptotals_real_oip);
	}

    $list0->add_text_column("difference_in_cost", "Difference\nin " . $system_currency["symbol"], COLUMN_BG_STRONG | COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $grouptotals_difference_in_cost);


	if($order_currency["symbol"] != $system_currency["symbol"])
	{
		$list0->add_text_column("investment_hq_loc", "Approved 620\nin " . $order_currency["symbol"], COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $grouptotals_hq_investment1_loc);
		$list0->add_text_column("investment_loc", "Approved 840\nin " . $order_currency["symbol"], COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $grouptotals_investment1_loc);

		$list0->add_text_column("real_cost_loc", "Real Cost\nin " . $order_currency["symbol"], COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $grouptotals_real1_loc);

		if($grouptotals_oip_loc > 0)
		{
			$list0->add_text_column("real_cost_loc", "CO\nin " . $order_currency["symbol"], COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $grouptotals_real_oip_loc);
		}

		$list0->add_text_column("difference_in_cost_loc", "Difference\nin " . $order_currency["symbol"], COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $grouptotals_difference_in_cost_loc);
	}

    $list0->add_text_column("difference_in_percent", "Percent", COLUMN_BREAK | COLUMN_ALIGN_RIGHT, $grouptotals_difference_in_cost2);

    $list0->set_footer("project_cost_groupname_name", "Total");
    $list0->set_footer("investment_hq", number_format($project_cost_hq_budget, 2, ".", "'") . "\n " . $system_currency["symbol"]);
	$list0->set_footer("investment", number_format($project_cost_budget, 2, ".", "'") . "\n " . $system_currency["symbol"]);
    $list0->set_footer("real_cost", number_format($project_cost_real, 2, ".", "'") . "\n " . $system_currency["symbol"]);
    $list0->set_footer("difference_in_cost", number_format($project_cost_diff1, 2, ".", "'") . "\n " . $system_currency["symbol"]);
    
	if($order_currency["symbol"] != $system_currency["symbol"])
	{
		$list0->set_footer("investment_hq_loc", number_format($project_cost_hq_budget_loc, 2, ".", "'") . "\n " . $order_currency["symbol"]);
		$list0->set_footer("investment_loc", number_format($project_cost_budget_loc, 2, ".", "'") . "\n " . $order_currency["symbol"]);
		$list0->set_footer("real_cost_loc", number_format($project_cost_real_loc, 2, ".", "'") . "\n " . $order_currency["symbol"]);
		$list0->set_footer("difference_in_cost_loc", number_format($project_cost_diff1_loc, 2, ".", "'") . "\n " . $order_currency["symbol"]);
	}
	
	
	$list0->set_footer("difference_in_percent", number_format(100*$project_cost_diff2, 2, ".", "'") . "%");
}


/********************************************************************
    Create List for Local Works and Other Cost
*********************************************************************/ 
$list7 = new ListView($sql_list7, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list7->set_title("Local Construction");
$list7->set_entity("order_item");

$list7->add_column("address_shortcut", "Supplier", "", "", "", COLUMN_NO_WRAP);
$list7->add_column("monitoring_group", "Description", "", "", "", COLUMN_NO_WRAP);

//$list7->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);
$list7->add_column("approved", "Budget " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list7->add_column("investment", "Cost " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list7->add_column("approved_loc", "Budget " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list7->add_column("investment_loc", "Cost " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
}

//$list7->add_column("date4", "Reinvoiced");
//$list7->add_column("order_item_reinvoicenbr", "Invoice-Nbr.", "", "", "", COLUMN_NO_WRAP);
//$list7->add_column("date42", "Reinvoiced");
//$list7->add_column("order_item_reinvoicenbr2", "Invoice-Nbr.", "", "", "", COLUMN_NO_WRAP);

$list7->add_text_column("p7", "Over All", COLUMN_ALIGN_RIGHT, $shares_p7);

$list7->set_footer("address_shortcut", "Total");
$list7->set_footer("approved", number_format($grouptotals_investment[7],2, ".", "'") . "\n " . $system_currency["symbol"]);
$list7->set_footer("investment", number_format($grouptotals_real[7],2, ".", "'") . "\n " . $system_currency["symbol"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list7->set_footer("approved_loc", number_format($grouptotals_investment_loc[7],2, ".", "'") . "\n " . $order_currency["symbol"]);
	$list7->set_footer("investment_loc", number_format($grouptotals_real_loc[7],2, ".", "'") . "\n " . $order_currency["symbol"]);

}

$list7->set_footer("p7", $share_t7);
$list7->add_column("order_item_cms_remark", "Remark", "", "", "", COLUMN_NO_WRAP);


$list7->populate();
$list7->process();


/********************************************************************
    Create List for HQ Supplied Items
*********************************************************************/ 
$list2 = new ListView($sql_list2, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list2->set_title("Fixturing: HQ Supplied Items");
$list2->set_entity("order_item");

$list2->add_column("address_shortcut", "Supplier", "", "", "", COLUMN_NO_WRAP);
$list2->add_column("monitoring_group", "Description", "", "", "", COLUMN_NO_WRAP);

//$list2->add_column("date1", "Ordered");
//$list2->add_column("date2", "Pickup");
//$list2->add_column("date3", "Arrival");

//$list2->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);
$list2->add_column("approved", "Budget " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list2->add_column("investment", "Cost " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list2->add_column("approved_loc", "Budget " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list2->add_column("investment_loc", "Cost " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
}

//$list2->add_column("date4", "Reinvoiced", "", "", "", COLUMN_BREAK);
//$list2->add_column("order_item_reinvoicenbr", "Invoice-Nbr.", "", "", "", COLUMN_BREAK | COLUMN_NO_WRAP);
//$list2->add_column("date42", "Reinvoiced", "", "", "", COLUMN_BREAK);
//$list2->add_column("order_item_reinvoicenbr2", "Invoice-Nbr.", "", "", "", COLUMN_BREAK | COLUMN_NO_WRAP);

$list2->add_text_column("p2", "Over All", COLUMN_ALIGN_RIGHT, $shares_p2);

$list2->set_footer("address_shortcut", "Total");
$list2->set_footer("approved", number_format($grouptotals_investment[2],2, ".", "'") . "\n " . $system_currency["symbol"]);
$list2->set_footer("investment", number_format($grouptotals_real[2],2, ".", "'") . "\n " . $system_currency["symbol"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list2->set_footer("approved_loc", number_format($grouptotals_investment_loc[2],2, ".", "'") . "\n " . $order_currency["symbol"]);
	$list2->set_footer("investment_loc", number_format($grouptotals_real_loc[2],2, ".", "'") . "\n " . $order_currency["symbol"]);

}

$list2->set_footer("p2", $share_t2);
$list2->add_column("order_item_cms_remark", "Remark", "", "", "", COLUMN_NO_WRAP);



$list2->populate();
$list2->process();



/********************************************************************
    Create List for Freight Charges
*********************************************************************/ 
$list6 = new ListView($sql_list6, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list6->set_title("Fixturing: Freight Charges");
$list6->set_entity("order_item");

$list6->add_column("address_shortcut", "Supplier", "", "", "", COLUMN_NO_WRAP);
$list6->add_column("monitoring_group", "Description", "", "", "", COLUMN_NO_WRAP);

//$list6->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);
$list6->add_column("approved", "Budget " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list6->add_column("investment", "Cost " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list6->add_column("approved_loc", "Budget " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list6->add_column("investment_loc", "Cost " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
}

//$list6->add_column("date4", "Reinvoiced");
//$list6->add_column("order_item_reinvoicenbr", "Invoice-Nbr.", "", "", "", COLUMN_NO_WRAP);
//$list6->add_column("date42", "Reinvoiced");
//$list6->add_column("order_item_reinvoicenbr2", "Invoice-Nbr.", "", "", "", COLUMN_NO_WRAP);

$list6->add_text_column("p6", "Over All", COLUMN_ALIGN_RIGHT, $shares_p6);

$list6->set_footer("address_shortcut", "Total");
$list6->set_footer("approved", number_format($grouptotals_investment[6],2, ".", "'") . "\n " . $system_currency["symbol"]);
$list6->set_footer("investment", number_format($grouptotals_real[6],2, ".", "'") . "\n " . $system_currency["symbol"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list6->set_footer("approved_loc", number_format($grouptotals_investment_loc[6],2, ".", "'") . "\n " . $order_currency["symbol"]);
	$list6->set_footer("investment_loc", number_format($grouptotals_real_loc[6],2, ".", "'") . "\n " . $order_currency["symbol"]);

}
$list6->set_footer("p6", $share_t6);
$list6->add_column("order_item_cms_remark", "Remark", "", "", "", COLUMN_NO_WRAP);

$list6->populate();
$list6->process();


/********************************************************************
    Create List for Other Cost
*********************************************************************/ 
$list10 = new ListView($sql_list10, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list10->set_title("Fixturing: Other");
$list10->set_entity("order_item");

$list10->add_column("address_shortcut", "Supplier", "", "", "", COLUMN_NO_WRAP);
$list10->add_column("monitoring_group", "Description", "", "", "", COLUMN_NO_WRAP);

$list10->add_column("approved", "Budget " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list10->add_column("investment", "Cost " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list10->add_column("approved_loc", "Budget " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list10->add_column("investment_loc", "Cost " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
}

//$list10->add_column("date4", "Reinvoiced");
//$list10->add_column("order_item_reinvoicenbr", "Invoice-Nbr.", "", "", "", COLUMN_NO_WRAP);
//$list10->add_column("date42", "Reinvoiced");
//$list10->add_column("order_item_reinvoicenbr2", "Invoice-Nbr.", "", "", "", COLUMN_NO_WRAP);

$list10->add_text_column("p10", "Over All", COLUMN_ALIGN_RIGHT, $shares_p10);

$list10->set_footer("address_shortcut", "Total");
$list10->set_footer("approved", number_format($grouptotals_investment[10],2, ".", "'") . "\n " . $system_currency["symbol"]);
$list10->set_footer("investment", number_format($grouptotals_real[10],2, ".", "'") . "\n " . $system_currency["symbol"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list10->set_footer("approved_loc", number_format($grouptotals_investment_loc[10],2, ".", "'") . "\n " . $order_currency["symbol"]);
	$list10->set_footer("investment_loc", number_format($grouptotals_real_loc[10],2, ".", "'") . "\n " . $order_currency["symbol"]);

}
$list10->set_footer("p10", $share_t10);
$list10->add_column("order_item_cms_remark", "Remark", "", "", "", COLUMN_NO_WRAP);

$list10->populate();
$list10->process();


/********************************************************************
    Create List for Architectural Cost
*********************************************************************/ 
$list9 = new ListView($sql_list9, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list9->set_title("Architectural Services");
$list9->set_entity("order_item");

$list9->add_column("address_shortcut", "Supplier", "", "", "", COLUMN_NO_WRAP);
$list9->add_column("monitoring_group", "Description", "", "", "", COLUMN_NO_WRAP);

//$list9->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);
$list9->add_column("approved", "Budget " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list9->add_column("investment", "Cost " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list9->add_column("approved_loc", "Budget " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list9->add_column("investment_loc", "Cost " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
}

//$list9->add_column("date4", "Reinvoiced");
//$list9->add_column("order_item_reinvoicenbr", "Invoice-Nbr.", "", "", "", COLUMN_NO_WRAP);
//$list9->add_column("date42", "Reinvoiced");
//$list9->add_column("order_item_reinvoicenbr2", "Invoice-Nbr.", "", "", "", COLUMN_NO_WRAP);

$list9->add_text_column("p9", "Over All", COLUMN_ALIGN_RIGHT, $shares_p9);

$list9->set_footer("address_shortcut", "Total");
$list9->set_footer("approved", number_format($grouptotals_investment[9],2, ".", "'") . "\n " . $system_currency["symbol"]);
$list9->set_footer("investment", number_format($grouptotals_real[9],2, ".", "'") . "\n " . $system_currency["symbol"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list9->set_footer("approved_loc", number_format($grouptotals_investment_loc[9],2, ".", "'") . "\n " . $order_currency["symbol"]);
	$list9->set_footer("investment_loc", number_format($grouptotals_real_loc[9],2, ".", "'") . "\n " . $order_currency["symbol"]);

}

$list9->set_footer("p9", $share_t9);
$list9->add_column("order_item_cms_remark", "Remark", "", "", "", COLUMN_NO_WRAP);

$list9->populate();
$list9->process();


/********************************************************************
    Create List for Equipment
*********************************************************************/ 
$list11 = new ListView($sql_list11, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list11->set_title("Equipment");
$list11->set_entity("order_item");

$list11->add_column("address_shortcut", "Supplier", "", "", "", COLUMN_NO_WRAP);
$list11->add_column("monitoring_group", "Description", "", "", "", COLUMN_NO_WRAP);

//$list11->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);
$list11->add_column("approved", "Budget " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list11->add_column("investment", "Cost " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list11->add_column("approved_loc", "Budget " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list11->add_column("investment_loc", "Cost " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
}

//$list11->add_column("date4", "Reinvoiced");
//$list11->add_column("order_item_reinvoicenbr", "Invoice-Nbr.", "", "", "", COLUMN_NO_WRAP);
//$list11->add_column("date42", "Reinvoiced");
//$list11->add_column("order_item_reinvoicenbr2", "Invoice-Nbr.", "", "", "", COLUMN_NO_WRAP);

$list11->add_text_column("p11", "Over All", COLUMN_ALIGN_RIGHT, $shares_p11);

$list11->set_footer("address_shortcut", "Total");
$list11->set_footer("approved", number_format($grouptotals_investment[11],2, ".", "'") . "\n " . $system_currency["symbol"]);
$list11->set_footer("investment", number_format($grouptotals_real[11],2, ".", "'") . "\n " . $system_currency["symbol"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list11->set_footer("approved_loc", number_format($grouptotals_investment_loc[11],2, ".", "'") . "\n " . $order_currency["symbol"]);
	$list11->set_footer("investment_loc", number_format($grouptotals_real_loc[11],2, ".", "'") . "\n " . $order_currency["symbol"]);

}

$list11->set_footer("p11", $share_t11);
$list11->add_column("order_item_cms_remark", "Remark", "", "", "", COLUMN_NO_WRAP);

$list11->populate();
$list11->process();


/********************************************************************
    Create List for Architectural Cost
*********************************************************************/ 
$list8 = new ListView($sql_list8, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list8->set_title("Other");
$list8->set_entity("order_item");

$list8->add_column("address_shortcut", "Supplier", "", "", "", COLUMN_NO_WRAP);
$list8->add_column("monitoring_group", "Description", "", "", "", COLUMN_NO_WRAP);

//$list8->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);
$list8->add_column("approved", "Budget " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list8->add_column("investment", "Cost " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list8->add_column("approved_loc", "Budget " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list8->add_column("investment_loc", "Cost " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
}

//$list8->add_column("date4", "Reinvoiced");
//$list8->add_column("order_item_reinvoicenbr", "Invoice-Nbr.", "", "", "", COLUMN_NO_WRAP);
//$list8->add_column("date42", "Reinvoiced");
//$list8->add_column("order_item_reinvoicenbr2", "Invoice-Nbr.", "", "", "", COLUMN_NO_WRAP);

$list8->add_text_column("p8", "Over All", COLUMN_ALIGN_RIGHT, $shares_p8);

$list8->set_footer("address_shortcut", "Total");
$list8->set_footer("approved", number_format($grouptotals_investment[8],2, ".", "'") . "\n " . $system_currency["symbol"]);
$list8->set_footer("investment", number_format($grouptotals_real[8],2, ".", "'") . "\n " . $system_currency["symbol"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list8->set_footer("approved_loc", number_format($grouptotals_investment_loc[8],2, ".", "'") . "\n " . $order_currency["symbol"]);
	$list8->set_footer("investment_loc", number_format($grouptotals_real_loc[8],2, ".", "'") . "\n " . $order_currency["symbol"]);

}
$list8->set_footer("p8", $share_t8);
$list8->add_column("order_item_cms_remark", "Remark", "", "", "", COLUMN_NO_WRAP);

$list8->populate();
$list8->process();


/********************************************************************
    Create List for item in projects coming from catalogue orders
*********************************************************************/ 
$link = "/user/order_assign_items_to_a_project.php?oid={order_item_order}";
$list_oip = new ListView($sql_oip, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list_oip->set_title("Catalogue Orders");
$list_oip->set_entity("order_items_in_projects");

$list_oip->add_column("address_shortcut", "Supplier", "", "", "", COLUMN_NO_WRAP);
$list_oip->add_column("project_cost_groupname_name", "Cost Group", "", "", "", COLUMN_NO_WRAP);

$list_oip->add_text_column("replacement", "", COLUMN_NO_WRAP | COLUMN_UNDERSTAND_HTML, $replacementinfos);
if (has_access("can_assign_orders_to_projects"))
{ 
	$list_oip->add_column("order_item_text", "Description", $link, "", "", COLUMN_NO_WRAP);
}
else
{
	$list_oip->add_column("order_item_text", "Description", "", "", "", COLUMN_NO_WRAP);
}

$list_oip->add_column("date1", "Ordered");
$list_oip->add_column("date2", "Pickup");
$list_oip->add_column("date3", "Arrival");
$list_oip->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);
$list_oip->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list_oip->add_column("investment", "Cost " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list_oip->add_column("investment_loc", "Cost " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
}

$list_oip->add_column("date4", "Reinvoiced");
$list_oip->add_column("order_item_reinvoicenbr", "Invoice-Nbr.", "", "", "", COLUMN_NO_WRAP);
$list_oip->add_column("date42", "Reinvoiced");
$list_oip->add_column("order_item_reinvoicenbr2", "Invoice-Nbr.", "", "", "", COLUMN_NO_WRAP);


$list_oip->add_text_column("oip", "Over All", COLUMN_ALIGN_RIGHT, $shares_oip);
$list_oip->add_column("order_items_in_project_cms_remark", "Remark", "", "", "", COLUMN_NO_WRAP);



$list_oip->set_footer("address_shortcut", "Total");
$list_oip->set_footer("investment", number_format($grouptotals_oip,2, ".", "'") . "\n " . $system_currency["symbol"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list_oip->set_footer("investment_loc", number_format($grouptotals_oip_loc,2, ".", "'") . "\n " . $order_currency["symbol"]);

}



$list_oip->set_footer("oip", $share_oip);


$list_oip->populate();
$list_oip->process();



/********************************************************************
    Create List for Remarks
*********************************************************************/ 

$sql_remarks = "select project_cost_remark_id, " .
               "project_cost_remark_remark " .
               "from project_cost_remarks " . 
			   "where project_cost_remark_order = " . $project["project_order"];;

$list_r = "project_cost_remark_order = " . $project["project_order"];

$list_r = new ListView($sql_remarks, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list_r->set_title("Remarks");
$list_r->set_entity("project_cost_remarks");
$list_r->add_column("project_cost_remark_remark", "Remark");
$list_r->process();

/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Cost Monitoring Sheet: Preview");
$form->render();

if($project["project_cost_type"] == 1) //Corporate
{
    echo "<br>";
	$list0->render();
}


if($num_recs7 > 0)
{
    echo "<br>";
    $list7->render();
}

if($num_recs2 > 0)
{
    echo "<br>";
    $list2->render();
}

if($num_recs6 > 0)
{
    echo "<br>";
    $list6->render();
}

if($num_recs10 > 0)
{
    echo "<br>";
    $list10->render();
}

if($num_recs9 > 0)
{
    echo "<br>";
    $list9->render();
}

if($num_recs11 > 0)
{
    echo "<br>";
    $list11->render();
}

if($num_recs8 > 0)
{
    echo "<br>";
    $list8->render();
}

if($has_catalogue_orders == true)
{
	echo "<br>";
	$list_oip->render();
}

$list_r->render();

require_once "include/project_footer_logistic_state.php";
$page->footer();

?>