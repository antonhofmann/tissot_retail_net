<?php
/********************************************************************

    project_edit_cost_monitoring_reinvoice.php

    Edit cost monitoring sheet: reinvoice data

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-10-24
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-07
    Version:        1.1.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_edit_cost_monitoring_reinvoice_data");

register_param("pid");

set_referer("project_edit_cost_monitoring_edit_group.php");

/********************************************************************
    prepare all data needed
*********************************************************************/

// read project and order details
$project = get_project(param("pid"));


// get System currency
$system_currency = get_system_currency_fields();


// get company's address
$client_address = get_address($project["order_client_address"]);

// get System currency
$system_currency = get_system_currency_fields();



/********************************************************************
    Get Item Data
*********************************************************************/ 

$sql_list1 = "select max(order_item_id) as item,  " .
             "order_item_po_number, order_item_supplier_address, " .
             "address_shortcut as supplier_company, " . 
             "project_cost_groupname_name, ".
             " order_item_reinvoiced, order_item_reinvoicenbr, " .
			  " order_item_reinvoiced2, order_item_reinvoicenbr2 " .
             "from order_items ".
             "left join addresses on order_item_supplier_address = addresses.address_id ".
             "left join project_cost_groupnames on project_cost_groupname_id = order_item_cost_group " .
             "where order_item_cost_group = 2 " .  
             " and  order_item_not_in_budget <> 1 " .
             " and order_item_order = " . $project["project_order"] .
             " group by project_cost_groupname_name, " .
             " order_item_po_number, " .
             " supplier_company, order_item_reinvoiced, " . 
             " order_item_supplier_address, " .
             " order_item_reinvoicenbr " .
             " order by order_item_po_number";

$reinvoicedates01_1 = array();
$reinvoicenumbers01_1 = array();
$reinvoicedates01_2 = array();
$reinvoicenumbers01_2 = array();

$res = mysql_query($sql_list1) or dberror($sql_list1);
while ($row = mysql_fetch_assoc($res))
{
    $reinvoicedates01_1[$row["item"]] = to_system_date($row["order_item_reinvoiced"]);
    $reinvoicenumbers01_1[$row["item"]] = $row["order_item_reinvoicenbr"];
	$reinvoicedates01_2[$row["item"]] = to_system_date($row["order_item_reinvoiced2"]);
    $reinvoicenumbers01_2[$row["item"]] = $row["order_item_reinvoicenbr2"];
}


$sql_list = "select order_item_id, order_item_text, " .
		   "order_item_po_number,  order_item_cost_group," .
		   "item_id, if(item_code is null, 'special item', item_code) as item_shortcut, ".
		   "    order_item_supplier_address, " .
		   "address_shortcut as supplier_company, order_item_supplier_freetext, ".
		   "    item_type_id, item_type_name, ".
		   "    item_type_priority, project_cost_groupname_name, ".
		   "    order_item_reinvoiced, order_item_reinvoicenbr, " .
		    "    order_item_reinvoiced2, order_item_reinvoicenbr2 " .
		   "from order_items ".
		   "left join items on order_item_item = item_id ".
		   "left join addresses on order_item_supplier_address = addresses.address_id ".
		   "left join item_types on order_item_type = item_type_id " . 
		   "left join project_cost_groupnames on project_cost_groupname_id = order_item_cost_group";


$list2_filter = "order_item_not_in_budget <> 1  " .
                "and order_item_cost_group in (6, 7, 8, 9, 10, 11) " . 
				"and order_item_order = " . $project["project_order"];

$where_clause = " where order_item_not_in_budget <> 1 " .
                "  and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION . 
                "  or order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST .
				"  or order_item_type = " . ITEM_TYPE_SERVICES .
                "  ) and order_item_order = " . $project["project_order"];

$reinvoicedates02_1 = array();
$reinvoicenumbers02_1 = array();
$reinvoicedates02_2 = array();
$reinvoicenumbers02_2 = array();

$num_recs1 = 0;
$num_recs2 = 0;

$res = mysql_query($sql_list . $where_clause) or dberror($sql_list . $where_clause);
while ($row = mysql_fetch_assoc($res))
{
    $reinvoicedates02_1[$row["order_item_id"]] = to_system_date($row["order_item_reinvoiced"]);
    $reinvoicenumbers02_1[$row["order_item_id"]] = $row["order_item_reinvoicenbr"];
    $suppliers[$row["order_item_id"]] = $row["order_item_supplier_freetext"];

	$reinvoicedates02_2[$row["order_item_id"]] = to_system_date($row["order_item_reinvoiced2"]);
    $reinvoicenumbers02_2[$row["order_item_id"]] = $row["order_item_reinvoicenbr2"];

    if($row["item_type_id"] == ITEM_TYPE_COST_ESTIMATION)
    {
        $num_recs2 = $num_recs2 + 1;
    }
    elseif($row["item_type_id"] == ITEM_TYPE_LOCALCONSTRUCTIONCOST)
    {
        $num_recs2 = $num_recs2 + 1;
    }
    elseif($row["item_type_id"] < ITEM_TYPE_COST_ESTIMATION)
    {
        $num_recs1 = $num_recs1 + 1;
    }
}



/********************************************************************
    project items coming from catalogue orders
*********************************************************************/ 

$reinvoicedates03_1 = array();
$reinvoicenumbers03_1 = array();
$reinvoicedates03_2 = array();
$reinvoicenumbers03_2 = array();

$num_recs3 = 0;


$sql_oip = "select order_item_id, order_item_text, " .
		   "order_item_po_number,  order_item_cost_group," .
		   "item_id, if(item_code is null, 'special item', item_code) as item_shortcut, ".
		   "    order_item_supplier_address, " .
		   "address_shortcut as supplier_company, order_item_supplier_freetext, ".
		   "    item_type_id, item_type_name, ".
		   "    item_type_priority, project_cost_groupname_name, ".
		   "    order_item_reinvoiced, order_item_reinvoicenbr, " .
		    "    order_item_reinvoiced2, order_item_reinvoicenbr2 " .
		   " from order_items_in_projects " .
		   " left join order_items on order_item_id = order_items_in_project_order_item_id " .
		   "left join items on order_item_item = item_id ".
		   "left join addresses on order_item_supplier_address = addresses.address_id ".
		   "left join item_types on order_item_type = item_type_id " . 
		   "left join project_cost_groupnames on project_cost_groupname_id = order_items_in_project_costgroup_id ";

$list3_filter = "order_items_in_project_project_order_id = " . $project["project_order"];


$res = mysql_query($sql_oip . " where " . $list3_filter) or dberror($sql_oip . " where " . $list3_filter);
while ($row = mysql_fetch_assoc($res))
{
    $reinvoicedates03_1[$row["order_item_id"]] = to_system_date($row["order_item_reinvoiced"]);
    $reinvoicenumbers03_1[$row["order_item_id"]] = $row["order_item_reinvoicenbr"];
    $suppliers[$row["order_item_id"]] = $row["order_item_supplier_freetext"];

	$reinvoicedates03_2[$row["order_item_id"]] = to_system_date($row["order_item_reinvoiced2"]);
    $reinvoicenumbers03_2[$row["order_item_id"]] = $row["order_item_reinvoicenbr2"];

    $num_recs3 = $num_recs3 + 1;
    
}

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid",$project["project_order"]);

require_once "include/project_head_small.php";


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Create List for Items
*********************************************************************/ 
$list1 = new ListView($sql_list1);
$list1->set_title("HQ Supplied Items");
$list1->set_entity("order_item");

$list1->add_column("project_cost_groupname_name", "Cost Group", "", "", "", COLUMN_NO_WRAP);
$list1->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);
$list1->add_column("supplier_company", "Supplier", "", "", "", COLUMN_NO_WRAP);
$list1->add_edit_column("order_item_reinvoiced", "Reinvoice Date", "10", COLUMN_BREAK, $reinvoicedates01_1);
$list1->add_edit_column("order_item_reinvoicenbr", "Reinvoice Number", "20", COLUMN_BREAK, $reinvoicenumbers01_1);
$list1->add_edit_column("order_item_reinvoiced2", "Reinvoice Date", "10", COLUMN_BREAK, $reinvoicedates01_2);
$list1->add_edit_column("order_item_reinvoicenbr2", "Reinvoice Number", "20", COLUMN_BREAK, $reinvoicenumbers01_2);

$list1->add_button("save_data_0", "Save");
$list1->add_button("cost_monitoring", "Back");
$list1->add_button("preview", "Preview CMS");

$list1->populate();
$list1->process();


if ($list1->button("save_data_0"))
{
     $error = 0;
	 foreach ($list1->values("order_item_reinvoiced") as $key=>$value)
	 {
		 if(from_system_date($value) == '1970-01-01')
		 {
			$error = 1;
		 }
	 }
	 foreach ($list1->values("order_item_reinvoiced2") as $key=>$value)
	 {
		 if(from_system_date($value) == '1970-01-01')
		 {
			$error = 1;
		 }
	 }
	 if($error == 0)
	 {
		project_update_order_item_reinvoice_data_0($list1); 
	 }
	 else
	 {
		$form->error("There are invalid calendar dates in your list!");
	 }
}
elseif ($list1->button("cost_monitoring"))
{
    $link = "project_edit_cost_monitoring.php?pid=" . param("pid"); 
    redirect($link);
}
elseif ($list1->button("preview"))
{
    $link = "project_edit_cost_monitoring_preview.php?pid=" . param("pid"); 
    redirect($link);
}

/********************************************************************
    Create List for other cost items
*********************************************************************/ 
$list2 = new ListView($sql_list);

$list2->set_title("Other");
$list2->set_entity("order_item");
$list2->set_filter($list2_filter);
$list2->set_order("project_cost_groupname_name, item_shortcut");
$list2->set_group("supplier_company", "supplier_company");


$list2->add_column("project_cost_groupname_name", "Cost Group", "", "", "", COLUMN_NO_WRAP);
$list2->add_column("item_shortcut", "Item Code");

$list2->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);

$list2->add_edit_column("order_item_reinvoiced", "Reinvoice Date", "10", 0, $reinvoicedates02_1);
$list2->add_edit_column("order_item_reinvoicenbr", "Reinvoice Number", "20", 0, $reinvoicenumbers02_1);
$list2->add_edit_column("order_item_reinvoiced2", "Reinvoice Date", "10", 0, $reinvoicedates02_2);
$list2->add_edit_column("order_item_reinvoicenbr2", "Reinvoice Number", "20", 0, $reinvoicenumbers02_2);

$list2->add_column("order_item_text", "Name");


$list2->add_button("save_data", "Save");
$list2->add_button("cost_monitoring", "Back");
$list2->add_button("preview", "Preview CMS");

$list2->populate();
$list2->process();


if ($list2->button("save_data"))
{
     $error = 0;
	 foreach ($list2->values("order_item_reinvoiced") as $key=>$value)
	 {
		 if(from_system_date($value) == '1970-01-01')
		 {
			$error = 1;
		 }
	 }
	 foreach ($list2->values("order_item_reinvoiced2") as $key=>$value)
	 {
		 if(from_system_date($value) == '1970-01-01')
		 {
			$error = 1;
		 }
	 }
	 if($error == 0)
	 {
		project_update_order_item_reinvoice_data($list2); 
	 }
	 else
	 {
		$form->error("There are invalid calendar dates in your list!");
	 }
}
elseif ($list2->button("cost_monitoring"))
{
    $link = "project_edit_cost_monitoring.php?pid=" . param("pid"); 
    redirect($link);
}
elseif ($list2->button("preview"))
{
    $link = "project_edit_cost_monitoring_preview.php?pid=" . param("pid"); 
    redirect($link);
}



/********************************************************************
    Create List for other cost items
*********************************************************************/ 
$list3 = new ListView($sql_oip);

$list3->set_title("Catalogue Orders");
$list3->set_entity("order_item");
$list3->set_filter($list3_filter);
$list3->set_order("project_cost_groupname_name, item_shortcut");
$list3->set_group("supplier_company", "supplier_company");


$list3->add_column("project_cost_groupname_name", "Cost Group", "", "", "", COLUMN_NO_WRAP);
$list3->add_column("item_shortcut", "Item Code");

$list3->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);

$list3->add_edit_column("order_item_reinvoiced", "Reinvoice Date", "10", 0, $reinvoicedates03_1);
$list3->add_edit_column("order_item_reinvoicenbr", "Reinvoice Number", "20", 0, $reinvoicenumbers03_1);
$list3->add_edit_column("order_item_reinvoiced2", "Reinvoice Date", "10", 0, $reinvoicedates03_2);
$list3->add_edit_column("order_item_reinvoicenbr2", "Reinvoice Number", "20", 0, $reinvoicenumbers03_2);

$list3->add_column("order_item_text", "Name");


$list3->add_button("save_data", "Save");
$list3->add_button("cost_monitoring", "Back");
$list3->add_button("preview", "Preview CMS");

$list3->populate();
$list3->process();


if ($list3->button("save_data"))
{
     $error = 0;
	 foreach ($list3->values("order_item_reinvoiced") as $key=>$value)
	 {
		 if(from_system_date($value) == '1970-01-01')
		 {
			$error = 1;
		 }
	 }
	 foreach ($list3->values("order_item_reinvoiced2") as $key=>$value)
	 {
		 if(from_system_date($value) == '1970-01-01')
		 {
			$error = 1;
		 }
	 }
	 if($error == 0)
	 {
		project_update_order_item_reinvoice_data($list3); 
	 }
	 else
	 {
		$form->error("There are invalid calendar dates in your list!");
	 }
}
elseif ($list3->button("cost_monitoring"))
{
    $link = "project_edit_cost_monitoring.php?pid=" . param("pid"); 
    redirect($link);
}
elseif ($list3->button("preview"))
{
    $link = "project_edit_cost_monitoring_preview.php?pid=" . param("pid"); 
    redirect($link);
}



/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Cost Monitoring Sheet: Reinvoice Data");
$form->render();

if($num_recs1 > 0)
{
    $list1->render();
}

if($num_recs2 > 0)
{
    echo "<br />";
	$list2->render();
}

if($num_recs3 > 0)
{
    echo "<br />";
	$list3->render();
}

require_once "include/project_footer_logistic_state.php";
$page->footer();

?>