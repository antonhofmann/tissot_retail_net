<?php
/********************************************************************

    task.php

    Mutation of user records.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-08-21
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2002-10-04
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_view_tasks");

register_param("id");
set_referer("tasks.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
$task_data = get_task_data(id());


/********************************************************************
    build form
*********************************************************************/
$form = new Form("tasks", "task");

$form->add_section("Task Details");
$form->add_hidden("tid", id());

$form->add_label($task_data["type_name"], $task_data["type_name"], 0, $task_data["number"]);
$form->add_label("assigned_by", "Assigned by", 0, $task_data["assigned_by"]);
$form->add_label("text", "Description", 0, $task_data["text"]);
$form->add_label("due_date", "Due Date", 0, $task_data["due_date"]);

if (!$task_data["done_date"])
{
    $form->add_edit("done_date", "Done Date", 0, $task_data["done_date"], TYPE_DATE, 20);
    $form->add_button("save", "Save");
}
else
{
    $form->add_label("done_date", "Done Date", 0, $task_data["done_date"]);
}


$form->add_button(FORM_BUTTON_DELETE, "Delete", "", OPTIONAL);
$form->add_button(FORM_BUTTON_BACK, "Back");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


if ($form->button("save"))
{
    if ($form->validate())
    {
       save_task_data($form);
       $link = "tasks.php";
       redirect ($link);
    }
}



/********************************************************************
    render page
*********************************************************************/
$page = new Page("tasks");

$page->header();
$page->title("Edit Task");
$form->render();
$page->footer();

?>