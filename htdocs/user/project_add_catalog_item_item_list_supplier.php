<?php
/********************************************************************

    project_add_catalog_item_item_list_supplier.php

    Add items to the list of materials from category item list for Supplier's Use Only.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-11
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-02-16
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.


*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";


check_access("can_edit_his_list_of_materials_in_projects");


register_param("pid");


/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));


// get the region of the client's
$client_country = $client_address["country"];


// get user data
$user_data = get_user(user_id());


// get company's address
$client_address = get_address($project["order_client_address"]);
$client_country = $client_address["country"];


// create sql for items belonging to the same supplier
$sql_items = "select DISTINCT item_id, item_code, " .
		   "item_name, item_category_name, supplier_address " . 
		   " from product_lines " .
		   "INNER JOIN product_line_supplying_groups ON product_line_supplying_group_line_id = product_line_id
			INNER JOIN item_supplying_groups ON item_supplying_group_supplying_group_id = product_line_supplying_group_group_id
			INNER JOIN items ON item_id = item_supplying_group_item_id
			INNER JOIN supplying_groups on supplying_group_id = item_supplying_group_supplying_group_id 
			INNER JOIN productline_regions ON productline_region_productline = product_line_id
			INNER JOIN item_categories ON item_category_id = item_category
			LEFT JOIN item_subcategories on item_subcategory_id = item_subcategory
			INNER JOIN item_countries ON item_country_item_id = item_id 
			LEFT join item_pos_types on item_pos_type_item = item_id 
			LEFT JOIN suppliers on supplier_item = item_id
			LEFT JOIN addresses on address_id = supplier_address ";


$list_filter = " (product_line_budget = 1)
				 and product_line_active = 1
				 and item_category_active = 1 
				 and item_active = 1
				 and item_visible_in_projects = 1
				 and item_type = 1 " . 
               "    and supplier_address = " . $user_data["address"] .  
               "    and item_country_country_id = " . dbquote($project["order_shop_address_country"]) .
               "    and product_line_id = " . $project["project_product_line"];




$values = array();
$sql_order_item = "select order_item_item, order_item_quantity ".
                  "from order_items ".
                  "where order_item_order = " . $project["project_order"] .
                  " and order_item_supplier_address = " . $user_data["address"] . 
                  "  and order_item_type = 1";


$res = mysql_query($sql_order_item) or dberror($sql_order_item);
while($row = mysql_fetch_assoc($res))
{
    $values[$row["order_item_item"]] = $row["order_item_quantity"];
}

/********************************************************************
    Create Form
*********************************************************************/ 


$form = new Form("projects", "project");


require_once "include/project_head_small.php";


/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql_items);


$list->set_entity("itmes");
$list->set_filter($list_filter);
$list->set_order("item_code");
$list->set_group("item_category_name");


$list->add_hidden("pid", param("pid"));
$list->add_hidden("oid", param("oid"));
$list->add_hidden("pline", $project["project_product_line"]);

$list->add_column("item_code", "Item Code", "/applications/templates/item.info.modal.php?id={item_id}", "", "", "", "", array('class'=>'item_info_box'));
$list->add_column("item_name", "Item Name");
$list->add_edit_column("item_entry_field", "Quantity", "4", 0, $values);

$list->add_button("add_items", "Add Items");
$list->add_button(LIST_BUTTON_BACK, "Back");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();


if ($list->button("add_items"))
{
	project_save_order_items($list, ITEM_TYPE_STANDARD, 0);
    $link = "project_edit_material_list_supplier.php?pid=" . param("pid"); 
    redirect($link);
}


/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Add Catalog Items: Supplier's Standard Items");
$form->render();


echo "<p>", "Please indicate the quantities to add to the list.", "</p>";


$list->render();
require_once "include/project_footer_logistic_state.php";
$page->footer();

?>