<?php
/********************************************************************
    order_add_attachment.php
    
    add attachment to an order

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-26
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-18
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.


*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "include/order_functions.php";

check_access("can_add_attachments_in_orders");


/********************************************************************
    prepare all data needed
*********************************************************************/
$user_roles = get_user_roles(user_id());
$user = get_user(user_id());

// read project and order details
$order = get_order(param("oid"));

// get company's address
$client_address = get_address($order["order_client_address"]);

// buld sql for attachment categories
$sql_attachment_categories = "select order_file_category_id, concat(order_file_category_priority, ' ', order_file_category_name) as group_name ".
                             "from order_file_categories ".
                             "where order_file_category_type = 1 ".
                             "order by order_file_category_priority";



// get addresses involved in the project
$companies = get_involved_companies(param('oid'), 0);

// checkbox names
$check_box_names = array();


$former_attachment = array();
if(array_key_exists("aid", $_GET) and $_GET["aid"] > 0)
{
	
	$sql = "select * " . 
		   "from order_files ". 
		   " where order_file_id = " . $_GET["aid"];

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$former_attachment = $row;

		$allowed_addresses = array();
		$sql = "select * from order_file_addresses " .
			   "where order_file_address_file = " . $_GET["aid"];

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$allowed_addresses[] = $row["order_file_address_address"];
		}
		$former_attachment["allowed_addresses"] = $allowed_addresses;
	}
}



//get all involved suppliers
$involved_suppliers = array();

$sql =  "select DISTINCT order_item_supplier_address,  address_company " . 
		"from order_items ".
		"left join addresses on address_id = order_item_supplier_address " . 
		" where (order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
				  "   and order_item_quantity > 0 " .
				  "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
				  "   and order_item_order = " . $order["order_id"] . 
				  "   and order_item_supplier_address > 0 " . 
		"order by address_company";

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
	$involved_suppliers[$row["order_item_supplier_address"]] = $row["address_company"];
}

if(count($involved_suppliers) > 0)
{
	$involved_suppliers['nosupplier'] = "No specific supplier";
}

//check if user is one of the suppliers
$user_is_a_supplier = false;
if(array_key_exists($user["address"], $involved_suppliers))
{
	$user_is_a_supplier = true;
}


//compose invisible div for shipping documents selector
$former_attachment = array();

$sql = "select * " . 
	   "from order_files ". 
	   " where order_file_id = " . dbquote(id());

$res = mysql_query($sql) or dberror($sql);

if ($row = mysql_fetch_assoc($res))
{
	$former_attachment = $row;
}

$sql_shipping_documents = "select standard_shipping_document_id, standard_shipping_document_name " . 
                          "from address_shipping_documents " . 
						  " left join standard_shipping_documents  on standard_shipping_document_id = address_shipping_document_document_id " . 
						  " where address_shipping_document_address_id = " . dbquote($order["order_client_address"]) . 
						  " order by standard_shipping_document_name";

$shipping_document_categories = array();
$res = mysql_query($sql_shipping_documents) or dberror($sql_shipping_documents);
while ($row = mysql_fetch_assoc($res))
{
	$shipping_document_categories[$row["standard_shipping_document_id"]] = $row["standard_shipping_document_name"];
}

if(count($shipping_document_categories) > 0)
{
	$shipping_document_categories['other'] = "Other";
}


//only allow indicate shipping docs if user is supplier, logistics coordinator or project manager
if($user_is_a_supplier == false
   and !in_array(1, $user_roles)
   and !in_array(2, $user_roles)
   and !in_array(3, $user_roles))
{
	$shipping_document_categories = array();
}


if((count($former_attachment) > 0 and $former_attachment['order_file_category'] == 15)
	or param("order_file_category") == 15)
{
	$shipping_document_category_selector = '<div id="shipping_document_category_selector" style="display:block;">';
}
else
{
	$shipping_document_category_selector = '<div id="shipping_document_category_selector" style="display:none;">';
}

$shipping_document_category_selector .= '<br />Please select the type of the shipping document:<br />';
foreach($shipping_document_categories as $id=>$name)
{
	$checked = "";
	if((count($former_attachment) > 0 and $former_attachment['order_file_standard_shipping_document_id'] == $id) or param("order_file_standard_shipping_document_id") == $id)
	{
		$checked = "checked";
	}
	$shipping_document_category_selector .= "<input type=\"radio\" name=\"standard_shipping_document_id\" " . $checked . " value=\"" . $id . "\">" . $name . '<br />';
}

if($user_is_a_supplier == false)
{
	$shipping_document_category_selector .= '<br />Please tell us to which supplier this shipping document belongs:<br />';
	foreach($involved_suppliers as $id=>$name)
	{
		$checked = "";
		if(count($former_attachment) > 0 and $former_attachment['order_file_supplier_id'] == $id or param("order_file_supplier_id") == $id)
		{
			$checked = "checked";
		}
		$shipping_document_category_selector .= "<input type=\"radio\" name=\"standard_shipping_supplier_id\" " . $checked . " value=\"" . $id . "\">" . $name . '<br />';
	}
}

$shipping_document_category_selector .= '<br /></div>';



/********************************************************************
    Create Form
********************************************************************/ 
$form = new Form("order_files", "file", 640);
$form->add_section("Order");
$form->add_hidden("oid", param('oid'));
$form->add_hidden("order_file_order", param('oid'));
$form->add_hidden("order_file_owner", user_id());

require_once "include/order_head_small.php";

$form->add_section("Attachment");

if(count($former_attachment) > 0)
{
	$form->add_list("order_file_category", "Category*", $sql_attachment_categories, NOTNULL, $former_attachment["order_file_category"]);
}
else
{
	$form->add_list("order_file_category", "Category*", $sql_attachment_categories, NOTNULL);
}

if(count($shipping_document_categories) > 0)
{
	$form->add_label("shipping_document_category", "", RENDER_HTML, $shipping_document_category_selector);
}
$form->add_hidden("order_file_standard_shipping_document_id",0);
$form->add_hidden("order_file_supplier_id", 0);


$form->add_edit("order_file_title", "Title*", NOTNULL, "", TYPE_CHAR);
$form->add_multiline("order_file_description", "Description*", 4);


$form->add_hidden("supplier1", 0);
$form->add_hidden("supplier2", 0);

if (has_access("can_set_attachment_accessibility_in_orders"))
{
    $form->add_section("Accessibility");
    $form->add_comment("Please indicate who is allowed to view this attachment.");


    $num_checkboxes=1;
    foreach ($companies as $key=>$value_array)
    {
        if(!array_key_exists('role', $value_array)) {
			$value_array["role"] = "";
		}

		if(count($former_attachment) > 0 and in_array($value_array["id"], $former_attachment["allowed_addresses"]))
		{
			$form->add_checkbox("A" . $num_checkboxes, $value_array["name"], 1, 0, $value_array["role"]);
		}
		else
		{
			$form->add_checkbox("A" . $num_checkboxes, $value_array["name"], 0, 0, $value_array["role"]);
		}

        $check_box_names["A" . $num_checkboxes] = $value_array["user"];

        $num_checkboxes++;
    }
}
else
{
	$form->add_section("Notification");
    $form->add_comment("This attachment ist sent to the following person:");
	//$form->add_lookup("p2", "Logistics Coordinator", "users", "concat(user_name, ' ', user_firstname)", 0, $order["order_retail_operator"]);


	$num_checkboxes=1;
    foreach ($companies as $key=>$value_array)
    {
        if($value_array["role"] == 'Logistics Coordinator')
		{
			if(!array_key_exists('role', $value_array)) {
				$value_array["role"] = "";
			}

			if(count($former_attachment) > 0 and in_array($value_array["id"], $former_attachment["allowed_addresses"]))
			{
				$form->add_checkbox("A" . $num_checkboxes, $value_array["name"], 1, 0, $value_array["role"]);
			}
			else
			{
				$form->add_checkbox("A" . $num_checkboxes, $value_array["name"], 0, 0, $value_array["role"]);
			}

			$check_box_names["A" . $num_checkboxes] = $value_array["user"];

			$num_checkboxes++;
		}
    }
}


$order_number = $order["order_number"];


$form->add_section("File");
$form->add_hidden("order_file_type");


$form->add_upload("order_file_path", "File", "/files/orders/$order_number", NOTNULL);

if (has_access("can_set_attachment_accessibility_in_orders"))
{
	$form->add_section("CC Recipients");
	$form->add_modal_selector("ccmails", "Selected Recipients", 8);
}
else
{
	$form->add_hidden("ccmails");
}


$form->add_section("Upload Multiple Attachments");
$form->add_checkbox("save_and_addnew", "", 0, 0, "Add another attachment after saving");

$form->add_button("save", "Save Attachment");
$form->add_button(FORM_BUTTON_BACK, "Back");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("save"))
{
    
    //get file_type
	$file_type_is_valid = false;
	$path = $form->value("order_file_path");
	$ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));

	$sql = "select file_type_id from file_types " . 
		   " where file_type_extension like '%" . $ext ."%'";

	
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$form->value("order_file_type", $row["file_type_id"]);
		$file_type_is_valid = true;
	}


	if(count($shipping_document_categories) > 0 
		and $form->value("order_file_category") == 15) // shipping document
	{
		if($form->value("order_file_standard_shipping_document_id") == 'other'
		  or $form->value("order_file_supplier_id") == 'nosupplier')
		{
			$form->add_validation("{order_file_standard_shipping_document_id} = 'other'", "");
			$form->add_validation("{order_file_supplier_id} = 'nosupplier'", "");
		}
		else
		{
			$form->add_validation("{order_file_standard_shipping_document_id} > 0", "You must indicate the type of the shipping document.");
			$form->add_validation("{order_file_supplier_id} > 0", "A shipping document must always be related to a supplier.");
		}
	}


	if($file_type_is_valid == false)
	{
		$form->error("The file type of the file you try to upload has an extension not allowed to be uploaded!");
	}
	elseif($form->validate())
	{
		$form->save();

	
		$attachment_id = mysql_insert_id();

		// check if a recipient was selected
		if (has_access("can_set_attachment_accessibility_in_orders"))
		{
			$no_recipient = 1;
		}
		else
		{
			$no_recipient = 0;
		}


		foreach ($form->items as $item)
		{
			if ($item["type"] == "checkbox" and $item["value"] == "1")
			{
				$no_recipient = 0;
			}
		}
		
		
		if ($no_recipient == 0)
		{
			save_attachment_accessibility_info($form,  $check_box_names);


			// send email notifocation to the retail staff
			$order_id = param("oid");


			$sql = "select order_id, order_number, ".
				   "users.user_email as recepient, users.user_address as address_id, " .
				   "users1.user_email as sender, concat(users1.user_name, ' ', users1.user_firstname) as user_fullname ".
				   "from orders ".
				   "left join users on order_retail_operator = users.user_id ".
				   "left join users as users1 on " . user_id() . "= users1.user_id ".
				   "where order_id = " . $order_id;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res) and $row["recepient"])
			{
				$subject = MAIL_SUBJECT_PREFIX . ": New attachment was added - Order " . $row["order_number"] . " - " . $client_address["country_name"];
				$sender_email = $row["sender"];
				$sender_name =  $row["user_fullname"];


				$mail = new PHPMailer();
				$mail->Subject = $subject;
				$mail->SetFrom($sender_email, $sender_name);
				$mail->AddReplyTo($sender_email, $sender_name);


				$bodytext0 = "A new attachment has been added by " . $sender_name . ":";
				$bodytext1 = "A new attachment has been added by " . $sender_name . " for:";


				$reciepients = array();
				$reciepients_cc = array();
				$reciepients_dp = array();
				$reciepients[strtolower($row["recepient"])] = strtolower($row["recepient"]);

				foreach ($check_box_names as $key=>$value)
				{
					if ($form->value($key))
					{
						foreach($companies as $key1=>$company)
						{
							
							if($value == $company["user"])
							{
								$sql = "select user_email, user_email_cc, user_email_deputy ".
									   "from users ".
									   "where (user_id = " . $company["user"] . 
									   "   and user_active = 1)";
								
								$res1 = mysql_query($sql) or dberror($sql);
								if ($row1 = mysql_fetch_assoc($res1))
								{
									$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
									$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
									$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
								}
							}
						}
					}
				}

				//get all ccmails
				$ccmails =  explode(' ', preg_replace("'\r?\n'"," ",$form->value('ccmails'))); 
				foreach($ccmails as $ccmail) {
					if(is_email_address($ccmail)) {
						
						$reciepients_cc[strtolower($ccmail)] = strtolower($ccmail);
					}
				
				}

				foreach($reciepients as $key=>$email)
				{
					if($email)
					{	$bodytext1 = $bodytext1 . "\n" . $email;
						$mail->AddAddress($email);
					}
				}


				foreach($reciepients_cc as $key=>$email)
				{
					if($email)
					{
						$bodytext1 = $bodytext1 . "\n" . $email;
						$mail->AddCC($email);
					}
				}

				foreach($reciepients_dp as $key=>$email)
				{
					if($email)
					{
						$bodytext1 = $bodytext1 . "\n" . $email;
						$mail->AddCC($email);
					}
				}

				
				$bodytext0.= "\n\n<--\n";
				$bodytext0.= $form->value("order_file_title") . "\n";
				
				
				if($form->value("order_file_description"))
				{
					$bodytext0.= $form->value("order_file_description") . "\n\n";
				}
				/*
				$bodytext0.= "download the file here:\n" .APPLICATION_URL . $form->value("order_file_path");
				$bodytext0.= "\n-->";
				*/

				if($form->value("order_file_path"))
				{
					$filepath = $_SERVER["DOCUMENT_ROOT"] . $form->value("order_file_path");
					//7MB plus 30% overhead base64-ecoded = 9.1 MB, 10MB Restriction from Mail server
					if(filesize ( $filepath ) < 7340032 
						and must_attach_file_to_mail($form->value("order_file_category")) == 1)
					{
						$mail->AddAttachment($filepath);
					}
				}

				$link ="order_view_attachment.php?oid=" . $order["order_id"] . "&id=" .  $attachment_id;
				$bodytext = $bodytext0 . "\n\nclick below to have direct access to the order:\n";
				$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
				
				$mail->Body = $bodytext;
				
				if($senmail_activated == true)
				{
					$mail->send();
				}

				$bodytext1.= "\n\n<--\n";
				$bodytext1.= $form->value("order_file_title") . "\n";
				
				
				if($form->value("order_file_description"))
				{
					$bodytext1.= $form->value("order_file_description") . "\n\n";
				}
				/*
				$bodytext1.= "download the file here:\n" . APPLICATION_URL . $form->value("order_file_path");
				$bodytext1.= "\n-->";
				*/

				$link ="order_view_attachment.php?oid=" . $order["order_id"] . "&id=" .  $attachment_id;
				$bodytext = $bodytext1 . "\n\nclick below to have direct access to the order:\n";
				$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";   


				foreach($reciepients as $key=>$email)
				{
					$sql = "select user_id from users " . 
						   " where LOWER(user_email) = " . dbquote($email);
					
					$res = mysql_query($sql) or dberror($sql);
					if ($row = mysql_fetch_assoc($res))
					{
						append_mail($order["order_id"], $row["user_id"] , user_id(), $bodytext, "", 2, 0, 0, 'order_files', $attachment_id);
					}
				}

				
							
				foreach($reciepients_cc as $key=>$email)
				{
					$sql = "select user_id from users " . 
						   " where LOWER(user_email) = " . dbquote($email);
					
					$res = mysql_query($sql) or dberror($sql);
					if ($row = mysql_fetch_assoc($res))
					{
						append_mail($order["order_id"], $row["user_id"] , user_id(), $bodytext, "", 2, 0, 1, 'order_files', $attachment_id);
					}
				}

				foreach($reciepients_dp as $key=>$email)
				{
					$sql = "select user_id from users " . 
						   " where LOWER(user_email) = " . dbquote($email);
					
					$res = mysql_query($sql) or dberror($sql);
					if ($row = mysql_fetch_assoc($res))
					{
						append_mail($order["order_id"], $row["user_id"] , user_id(), $bodytext, "", 2, 0, 1, 'order_files', $attachment_id);
					}
				}
			}


			if($form->value("save_and_addnew") == 1)
			{
				$link = "order_add_attachment.php?oid=" . param("oid") . "&aid=" . id();
				redirect($link);
			}
			else
			{
				$link = "order_view_attachments.php?oid=" . param("oid");
				redirect($link);
			}
		}
		else
		{
			if($form->value("order_file_category") == 999915)
			{
			}
			else
			{
				$form->error("Please select a least one person to have access to the attachment.");
			}
		}
	}
}





$page = new Page("orders");


require "include/order_page_actions.php";


$page->header();
$page->title("Add an Attachment");
$form->render();

?>


<script language="Javascript">

	$(document).ready(function(){

		$("#order_file_category").change(function(){
		
			if( $('#shipping_document_category_selector').is(':hidden') ) 
			{
				if($("#order_file_category").val() == 15)
				{
					$("#shipping_document_category_selector").show();
				}
			}
			else
			{
				$("#shipping_document_category_selector").hide();
				$('input[name="standard_shipping_document_id"]').attr('checked', false);
				$("#order_file_standard_shipping_document_id").val(0);

				$('input[name="standard_shipping_supplier_id"]').attr('checked', false);
				$("#order_file_supplier_id").val(0);
			}
		});

		$("input[name='standard_shipping_document_id']").change(function(){
			
			var selectedVal = 0;
			var selected = $("input[type='radio'][name='standard_shipping_document_id']:checked");
			if (selected.length > 0) {
				selectedVal = selected.val();
			}
			
			$("#order_file_standard_shipping_document_id").val(selectedVal);
		});

		
		<?php
		if($user_is_a_supplier == false)
		{
		?>
			$("input[name='standard_shipping_supplier_id']").change(function(){
				
				var selectedVal = 0;
				var selected = $("input[type='radio'][name='standard_shipping_supplier_id']:checked");
				if (selected.length > 0) {
					selectedVal = selected.val();
				}
				$("#order_file_supplier_id").val(selectedVal);
			});
		<?php
		}
		else
		{
		?>
			$("#order_file_supplier_id").val(<?php echo $user["address"];?>);
		<?php
		}
		?>

	});

</script>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#ccmails_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/shared/select_mail_recipients.php'
    });
    return false;
  });
});
</script>

<?php

$page->footer();


?>