<?php
/********************************************************************

    project_view_material_list.php

    View List of Materials

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-09
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-11-13
    Version:        1.2.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

if(!has_access("can_view_budget_in_projects") 
	and !has_access("has_access_to_list_of_materials_in_projects"))
{
	redirect("noaccess.php");
}


register_param("pid");
set_referer("project_add_catalog_item_categories.php");
set_referer("project_edit_material_list_edit_item.php");
set_referer("project_add_catalog_item_special_item_list.php");
set_referer("project_add_special_item_individual.php");
set_referer("project_add_catalog_item_cost_estimation_list.php");
set_referer("project_add_cost_estimation_individual.php");
set_referer("project_add_lc_cost_estimation_individual.php");

/********************************************************************
    prepare all data needed
*********************************************************************/

// read project and order details
$project = get_project(param("pid"));

// get orders's currency
$order_currency = get_order_currency($project["project_order"]);

// get System currency
$system_currency = get_system_currency_fields();


// get company's address
$client_address = get_address($project["order_client_address"]);
$exchange_rate_info = get_order_currency($project["project_order"]);

$currency_list = array();
$sql_currency = "select currency_id, currency_symbol " . 
                " from currencies " .
				" left join countries on country_currency = currency_id " . 
				" where currency_id = 1 " . 
				" or currency_id = " . $project["order_client_currency"] . 
				" or country_id = " . $client_address["country"] .
				" order by currency_symbol";

$res = mysql_query($sql_currency) or dberror($sql_currency);
while ($row = mysql_fetch_assoc($res))
{
	$currency_list[$row["currency_id"]] = get_currency($row["currency_id"]);
}

// create sql for order items

$sql_order_items = "select order_item_id, order_item_text, order_item_quantity, order_item_order_revisions, ".
				   "    order_item_po_number, order_item_system_price, order_item_client_price, item_id, " . 
				   "    order_item_supplier_address, ".
				   "    order_item_forwarder_address, " . 
				   "    if(order_item_not_in_budget=1 , 'no', 'yes') as order_item_in_budget, ".
				   "    TRUNCATE(order_item_quantity * order_item_system_price, 2) as total_price, ".
				   "    TRUNCATE(order_item_quantity * order_item_client_price, 2) as total_price_loc, ".
				   "    if(item_code <>'', item_code, item_type_name) as item_shortcut, ".
				   "    order_item_supplier_freetext, order_item_exclude_from_ps, order_item_discount, ".
				   "    addresses.address_shortcut as supplier_company, ".
				   "    addresses_1.address_shortcut as forwarder_company, ".
				   "    item_type_id, item_type_name, ".
				   "    item_type_priority, " . 
				   "    concat(project_cost_groupname_name, ' - ', item_category_name) as cat_name, " .
				   "    order_item_type,  ".
				   "    item_stock_property_of_swatch, unit_name, order_item_client_price_freezed, " . 
				   " order_item_supplier_exchange_rate, project_cost_groupname_name " .
				   "from order_items ".
				   "left join items on order_item_item = item_id ".
				   "left join item_categories on item_category_id = item_category " .
				   "left join addresses on order_item_supplier_address = addresses.address_id ".
				   "left join addresses as addresses_1 ".
				   "     on order_item_forwarder_address = addresses_1.address_id ".
				   "left join item_types on order_item_type = item_type_id " .
				   "left join project_cost_groupnames on project_cost_groupname_id = order_item_cost_group " .
				   "left join units on unit_id = order_item_unit_id";




$where_clause = " where order_item_cost_group IN (10,6, 2) " . 
                "    and order_item_order = " . $project["project_order"];


//get all list information
$item_suppliers = array();
$item_forwarders = array();

$quantities = array();
$revisions = array();
$exclude_from_ps = array();
$property = array();
$images = array();

$res = mysql_query($sql_order_items . $where_clause) or dberror($sql_order_items . $where_clause);

$num_of_standard_items = 0;
$num_of_special_items = 0;
while ($row = mysql_fetch_assoc($res))
{
    $quantities[$row["order_item_id"]] = $row["order_item_quantity"];
	$revisions[$row["order_item_id"]] = $row["order_item_order_revisions"];

    if($row["order_item_type"] == ITEM_TYPE_STANDARD or $row["order_item_type"] == ITEM_TYPE_SERVICES)
    {
        $num_of_standard_items++;
    }
    elseif($row["order_item_type"] == ITEM_TYPE_SPECIAL)
    {
        $num_of_special_items++;
    }

	$item_suppliers[$row["order_item_id"]] = $row["order_item_supplier_address"];
	$item_forwarders[$row["order_item_id"]] = $row["order_item_forwarder_address"];

	$exclude_from_ps[$row["order_item_id"]] = $row["order_item_exclude_from_ps"];

	if($row["item_stock_property_of_swatch"] == 1)
	{
		$property[$row["order_item_id"]] = "/pictures/stockproperty.gif";
	}

	//set icons for picture indicating column
	$link = "<a class=\"item-files-modal\" data-fancybox-type=\"iframe\" href=\"/applications/templates/item.info.modal.php?id=" . $row["item_id"] . "\"><img border=\"0\" src=\"/pictures/document_view.gif\" /></a>";
	
	$images[$row["order_item_id"]] = $link;

}


//calculate cost group totals
$grouptotals_investment = array();
$grouptotals_investment[2] = 0;
$grouptotals_investment[6] = 0;
$grouptotals_investment[7] = 0;
$grouptotals_investment[8] = 0;
$grouptotals_investment[9] = 0;
$grouptotals_investment[10] = 0;
$grouptotals_investment[11] = 0;

$grouptotals_hq_investment = array();
$grouptotals_hq_investment[2] = 0;
$grouptotals_hq_investment[6] = 0;


$grouptotals_investment_loc = array();
$grouptotals_investment_loc[2] = 0;
$grouptotals_investment_loc[6] = 0;
$grouptotals_investment_loc[7] = 0;
$grouptotals_investment_loc[8] = 0;
$grouptotals_investment_loc[9] = 0;
$grouptotals_investment_loc[10] = 0;
$grouptotals_investment_loc[11] = 0;

$grouptotals_hq_investment_loc = array();
$grouptotals_hq_investment_loc[2] = 0;
$grouptotals_hq_investment_loc[6] = 0;



$sql = "select order_item_cost_group, order_item_type, ".
	   "order_item_system_price, order_item_client_price, order_item_quantity,  ".
	   "order_item_system_price_freezed, order_item_client_price_freezed, order_item_quantity_freezed, ".
	   "order_item_system_price_hq_freezed, order_item_client_price_hq_freezed, order_item_quantity_hq_freezed " .
	   "from order_items ".
	   "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
	   "   and order_item_order=" . $project["project_order"] . 
	   "   and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION .
	   "   or order_item_type = " . ITEM_TYPE_SERVICES .
	   "   or order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST .
	   " ) " .
	   " and order_item_cost_group > 0 " . 
	   " order by order_item_cost_group";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	
	if($row["order_item_cost_group"] == 2 or $row["order_item_cost_group"] == 6)
	{
		if($row["order_item_quantity"] > 0 or $row["order_item_type"] == 1  or $row["order_item_type"] == 2)
		{
			$grouptotals_hq_investment[$row["order_item_cost_group"]] = $grouptotals_hq_investment[$row["order_item_cost_group"]] + $row["order_item_system_price"] * $row["order_item_quantity"];

			$grouptotals_hq_investment_loc[$row["order_item_cost_group"]] = $grouptotals_hq_investment_loc[$row["order_item_cost_group"]] + $row["order_item_client_price"] * $row["order_item_quantity"];

		}
		else
		{
			$grouptotals_hq_investment[$row["order_item_cost_group"]] = $grouptotals_hq_investment[$row["order_item_cost_group"]] + $row["order_item_system_price"];
			$grouptotals_hq_investment_loc[$row["order_item_cost_group"]] = $grouptotals_hq_investment_loc[$row["order_item_cost_group"]] + $row["order_item_client_price"];
		}
	}
	

	
	if($row["order_item_quantity"] > 0 or $row["order_item_type"] == 1  or $row["order_item_type"] == 2)
	{
		$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_system_price"] * $row["order_item_quantity"];

		$grouptotals_investment_loc[$row["order_item_cost_group"]] = $grouptotals_investment_loc[$row["order_item_cost_group"]] + $row["order_item_client_price"] * $row["order_item_quantity"];

	}
	else
	{
		$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_system_price"];

		$grouptotals_investment_loc[$row["order_item_cost_group"]] = $grouptotals_investment_loc[$row["order_item_cost_group"]] + $row["order_item_client_price"];
	}
}


//budget

$project_cost_construction = $grouptotals_investment[7];

$project_cost_fixturing = $grouptotals_investment[10] + $grouptotals_investment[6] + $grouptotals_investment[2];
$project_cost_fixturing_other = $grouptotals_investment[10];
$project_cost_hq_fixturing = $grouptotals_hq_investment[2];
$project_cost_freight_charges = $grouptotals_investment[6];
$project_cost_architectural = $grouptotals_investment[9];
$project_cost_equipment = $grouptotals_investment[11];
$project_cost_other = + $grouptotals_investment[8];
$budget_total = $project_cost_construction + $project_cost_fixturing + $project_cost_architectural + $project_cost_equipment + $project_cost_other;



$project_cost_construction_loc = $grouptotals_investment_loc[7];

$project_cost_fixturing_loc = $grouptotals_investment_loc[10] + $grouptotals_investment_loc[6] + $grouptotals_investment_loc[2];
$project_cost_fixturing_other_loc = $grouptotals_investment_loc[10];
$project_cost_hq_fixturing_loc = $grouptotals_hq_investment_loc[2];
$project_cost_freight_charges_loc = $grouptotals_investment_loc[6];
$project_cost_architectural_loc = $grouptotals_investment_loc[9];
$project_cost_equipment_loc = $grouptotals_investment_loc[11];
$project_cost_other_loc = + $grouptotals_investment_loc[8];
$budget_total_loc = $project_cost_construction_loc + $project_cost_fixturing_loc + $project_cost_architectural_loc + $project_cost_equipment_loc + $project_cost_other_loc;


//total cost
$standard_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_STANDARD);
$special_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_SPECIAL);
$special_item_total_hq = get_order_item_type_total($project["project_order"],  ITEM_TYPE_SPECIAL, 2);
$freight_charges = get_order_item_type_total($project["project_order"],  ITEM_TYPE_COST_ESTIMATION, 6);
$cost_estimation_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_COST_ESTIMATION);
$cost_estimation_item_total_furniture = get_order_item_type_total($project["project_order"],  ITEM_TYPE_COST_ESTIMATION, 2);
$local_construction_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_LOCALCONSTRUCTIONCOST);


$total_cost = $standard_item_total["in_system_currency"] + $special_item_total["in_system_currency"] + $cost_estimation_item_total["in_system_currency"] + $local_construction_item_total["in_system_currency"];

$hq_total_cost = $standard_item_total["in_system_currency"] + $special_item_total_hq["in_system_currency"] + $freight_charges["in_system_currency"];

$total_cost_loc = $standard_item_total["in_order_currency"] + $special_item_total["in_order_currency"] + $cost_estimation_item_total["in_order_currency"] + $local_construction_item_total["in_order_currency"];

$hq_total_cost_loc = $standard_item_total["in_order_currency"] + $special_item_total_hq["in_order_currency"] + $freight_charges["in_order_currency"];


$hq_budget_approval_data = get_hq_budget_approval_data($project["project_order"]);
$budget_approval_data = get_budget_approval_data($project["project_order"]);



//get discount rates
$sql_lc = "select order_item_id, order_item_discount, order_item_localcostgroupcode, lwoffer_id, " .
          " order_item_system_price, order_item_client_price, order_item_supplier_freetext " .
          " from order_items " .
		  " left join lwoffers on lwoffer_order = order_item_order " . 
		  " where order_item_cost_group IN (7) " . 
          "    and order_item_order = " . $project["project_order"];
$res = mysql_query($sql_lc) or dberror($sql_lc);
$discount_rates = array();


while ($row = mysql_fetch_assoc($res))
{
	$discount_rates[$row["order_item_id"]] = $row["order_item_discount"];
}

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid",$project["project_order"]);

require_once "include/project_head_small.php";

$form->add_section("Client Currency Information");
$form->add_label("client_currency", "Currency", 0, $order_currency["symbol"]);
$form->add_label("exchange_rate", "Exchange Rate", 0, $order_currency["exchange_rate"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$form->add_comment('<span class="error">All amounts for HQ supplied items converted from CHF into LOC are for reference only as the exchange rate may change.</span>');
}

$form->add_section("Cost Overview as per " . date("d.m.Y"));

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$form->add_label("l1_lc", "Local Construction in " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($project_cost_construction,2, ".", "'") . " / " . number_format($project_cost_construction_loc,2, ".", "'"));
	
	$form->add_label("l2_lc", "Store fixturing / Furniture in " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($project_cost_fixturing,2, ".", "'"). " / " . number_format($project_cost_fixturing_loc,2, ".", "'"));
	
	
	

	$form->add_label("l9_ow2", "- of which HQ Supplied Items in " .  $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($project_cost_hq_fixturing,2, ".", "'"). " / " . number_format($project_cost_hq_fixturing_loc,2, ".", "'"));

	$form->add_label("l9_ow3", "- of which Freight Charges in " .  $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($project_cost_freight_charges,2, ".", "'"). " / " . number_format($project_cost_freight_charges_loc,2, ".", "'"));

	$form->add_label("l9_ow1", "- of which Fixturing: Other (local furniture, import taxes etc.) in " .  $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($project_cost_fixturing_other,2, ".", "'"). " / " . number_format($project_cost_fixturing_other_loc,2, ".", "'"));

	$form->add_label("l3_lc", "Architectural Services in " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($project_cost_architectural,2, ".", "'"). " / " . number_format($project_cost_architectural_loc,2, ".", "'"));
	$form->add_label("l4_lc", "Equipment in " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($project_cost_equipment,2, ".", "'"). " / " . number_format($project_cost_equipment_loc,2, ".", "'"));
	$form->add_label("l5_lc", "Other in " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($project_cost_other,2, ".", "'"). " / " . number_format($project_cost_other_loc,2, ".", "'"));

	
	$form->add_label("l13_lc", "Total Costs in " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($budget_total,2, ".", "'"). " / " . number_format($budget_total_loc,2, ".", "'"));
}
else
{
	$form->add_label("l1", "Local Construction in " . $system_currency["symbol"], 0, number_format($project_cost_construction,2, ".", "'"));
	
	$form->add_label("l2", "Store fixturing / Furniture in " . $system_currency["symbol"], 0, number_format($project_cost_fixturing,2, ".", "'"));
	
	$form->add_label("l9_ow2", "- of which HQ Supplied Items in " .  $system_currency["symbol"], 0, number_format($project_cost_hq_fixturing,2, ".", "'"));
	
	$form->add_label("l9_ow3", "- of which Freight Charges in " .  $system_currency["symbol"], 0, number_format($project_cost_freight_charges,2, ".", "'"));
	
	$form->add_label("l9_ow1", "- of which Fixturing: Other (local furniture, import taxes etc.) in " .  $system_currency["symbol"], 0, number_format($project_cost_fixturing_other,2, ".", "'"));
	

	$form->add_label("l3", "Architectural Services in " . $system_currency["symbol"], 0, number_format($project_cost_architectural,2, ".", "'"));
	$form->add_label("l4", "Equipment in " . $system_currency["symbol"], 0, number_format($project_cost_equipment,2, ".", "'"));
	$form->add_label("l5", "Other in " . $system_currency["symbol"], 0, number_format($project_cost_other,2, ".", "'"));

	
	$form->add_label("l13", "Total Costs in " . $system_currency["symbol"], 0, number_format($budget_total,2, ".", "'"));
}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();

$link="project_view_material_list_view_item.php?pid=" . param("pid");

/********************************************************************
    Create List for Local Construction Cost Positions
*********************************************************************/ 
$list_co = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list_co->set_entity("order_item");
$list_co->set_filter("order_item_cost_group = 7 and order_item_order = " . $project["project_order"]);
$list_co->set_order("item_code, order_item_id");

$list_co->add_column("item_shortcut", "Item Code", $link, "", "", COLUMN_NO_WRAP);

$list_co->add_column("order_item_text", "Name");
$list_co->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT);
$list_co->set_footer("item_shortcut",  "Total");
$list_co->set_footer("order_item_system_price", number_format($project_cost_construction,2, ".", "'") . "\n" . $system_currency["symbol"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list_co->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_co->set_footer("order_item_client_price", number_format($project_cost_construction_loc,2, ".", "'") . "\n" . $order_currency["symbol"]);
}

$list_co->add_column("order_item_supplier_freetext", "Supplier Info");
$list_co->add_text_column("discount", "Discount in %", 0, $discount_rates);

$list_co->populate();
$list_co->process();

/********************************************************************
    Create List for standard items
*********************************************************************/ 

$list_sf = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list_sf->set_entity("order_item");
$list_sf->set_filter("order_item_cost_group = 2 and order_item_type = 1 and order_item_order = " . $project["project_order"]);
$list_sf->set_order("supplier_company, item_code");
$list_sf->set_group("cat_name");

$list_sf->add_column("item_shortcut", "Item Code", $link, "", "", COLUMN_NO_WRAP);
$list_sf->add_text_column("pix", "Pix", COLUMN_UNDERSTAND_HTML, $images);

$list_sf->add_column("order_item_in_budget", "in Budget", "", "", "", COLUMN_NO_WRAP | COLUMN_ALIGN_CENTER);
$list_sf->add_image_column("property", "", 0, $property);
$list_sf->add_column("order_item_text", "Name");

$list_sf->add_checkbox_column("order_item_exclude_from_ps", "local", 0, $exclude_from_ps);

$list_sf->add_column("order_item_quantity", "Quantity");


$list_sf->add_column("unit_name", "Unit", "", "", "", COLUMN_NO_WRAP);

$list_sf->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT);
$list_sf->add_column("total_price", "Total " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list_sf->set_footer("item_shortcut", "Total");
$list_sf->set_footer("total_price", number_format($standard_item_total["in_system_currency"],2, ".", "'") . "\n" . $system_currency["symbol"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list_sf->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_sf->add_column("total_price_loc", "Total " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list_sf->set_footer("total_price_loc", number_format($standard_item_total["in_order_currency"],2, ".", "'") . "\n" . $order_currency["symbol"]);
}
$list_sf->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);


$list_sf->add_column("supplier_company", "Supplier");
$list_sf->add_column("forwarder_company", "Forwarder");

$entityValue = http_build_query(array(
	'frame' => true,
	'nofilters' => true,
	'entity' => 'project_material_list',
	'entity_values' => array(
		'pid' => param("pid")
	)
));

$list_sf->add_button(LIST_BUTTON_LINK, "Print Product Info Sheets", array(
	'href' => '/applications/templates/item.booklet.php?'.$entityValue,
	'data-fancybox-type' => 'iframe'
));

$list_sf->populate();
$list_sf->process();



/********************************************************************
    Create List for special items
*********************************************************************/ 

$list_si = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list_si->set_entity("order_item");
$list_si->set_filter("order_item_cost_group = 2 and order_item_type = 2 and  order_item_order = " . $project["project_order"]);
$list_si->set_order("supplier_company, item_code");
$list_si->set_group("cat_name");

$list_si->add_column("item_shortcut", "Item Code", $link, "", "", COLUMN_NO_WRAP);
//$list_si->add_text_column("pix", "Pix", COLUMN_UNDERSTAND_HTML, $images);

$list_si->add_column("order_item_in_budget", "in Budget", "", "", "", COLUMN_NO_WRAP | COLUMN_ALIGN_CENTER);
//$list_si->add_image_column("property", "", 0, $property);
$list_si->add_column("order_item_text", "Name");

$list_si->add_checkbox_column("order_item_exclude_from_ps", "local", 0, $exclude_from_ps);

$list_si->add_column("order_item_quantity", "Quantity");


$list_si->add_column("unit_name", "Unit", "", "", "", COLUMN_NO_WRAP);

$list_si->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT);
$list_si->add_column("total_price", "Total " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list_si->set_footer("item_shortcut", "Total");
$list_si->set_footer("total_price", number_format($special_item_total["in_system_currency"],2, ".", "'") . "\n" . $system_currency["symbol"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list_si->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_si->add_column("total_price_loc", "Total " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list_si->set_footer("total_price_loc", number_format($special_item_total["in_order_currency"],2, ".", "'") . "\n" . $order_currency["symbol"]);
}
$list_si->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);


$list_si->add_column("supplier_company", "Supplier");
$list_si->add_column("forwarder_company", "Forwarder");

$list_si->populate();
$list_si->process();



/********************************************************************
    Create List for freight charges
*********************************************************************/ 

$list_fc = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list_fc->set_entity("order_item");
$list_fc->set_filter("order_item_cost_group = 6 and  order_item_order = " . $project["project_order"]);
$list_fc->set_order("supplier_company, item_code");
$list_fc->set_group("cat_name");

$list_fc->add_column("item_shortcut", "Item Code", $link, "", "", COLUMN_NO_WRAP);
//$list_fc->add_text_column("pix", "Pix", COLUMN_UNDERSTAND_HTML, $images);

//$list_fc->add_column("order_item_in_budget", "in Budget", "", "", "", COLUMN_NO_WRAP | COLUMN_ALIGN_CENTER);
//$list_fc->add_image_column("property", "", 0, $property);
$list_fc->add_column("order_item_text", "Name");

//$list_fc->add_checkbox_column("order_item_exclude_from_ps", "local", 0, $exclude_from_ps);
//$list_fc->add_column("order_item_quantity", "Quantity");
//$list_fc->add_column("unit_name", "Unit", "", "", "", COLUMN_NO_WRAP);

$list_fc->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT);
$list_fc->add_column("total_price", "Total " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list_fc->set_footer("item_shortcut", "Total");
$list_fc->set_footer("total_price", number_format($project_cost_freight_charges,2, ".", "'") . "\n" . $system_currency["symbol"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list_fc->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_fc->add_column("total_price_loc", "Total " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list_fc->set_footer("total_price_loc", number_format($project_cost_freight_charges_loc,2, ".", "'") . "\n" . $order_currency["symbol"]);
}
$list_fc->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);

$list_fc->add_column("order_item_supplier_freetext", "Supplier Info");
//$list_fc->add_column("forwarder_company", "Forwarder");

$list_fc->populate();
$list_fc->process();



/********************************************************************
    Create List for other cost
*********************************************************************/ 

$list_of = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list_of->set_entity("order_item");
$list_of->set_filter("(order_item_cost_group = 10 or (order_item_type = 3 and order_item_cost_group = 2)) and  order_item_order = " . $project["project_order"]);
$list_of->set_order("supplier_company, item_code");
$list_of->set_group("cat_name");

$list_of->add_column("item_shortcut", "Item Code", $link, "", "", COLUMN_NO_WRAP);
//$list_of->add_text_column("pix", "Pix", COLUMN_UNDERSTAND_HTML, $images);

//$list_of->add_column("order_item_in_budget", "in Budget", "", "", "", COLUMN_NO_WRAP | COLUMN_ALIGN_CENTER);
//$list_of->add_image_column("property", "", 0, $property);
$list_of->add_column("order_item_text", "Name");

//$list_of->add_checkbox_column("order_item_exclude_from_ps", "local", 0, $exclude_from_ps);
//$list_of->add_column("order_item_quantity", "Quantity");
//$list_of->add_column("unit_name", "Unit", "", "", "", COLUMN_NO_WRAP);

$list_of->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT);
$list_of->add_column("total_price", "Total " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list_of->set_footer("item_shortcut", "Total");
$list_of->set_footer("total_price", number_format($project_cost_fixturing_other + $cost_estimation_item_total_furniture["in_system_currency"],2, ".", "'") . "\n" . $system_currency["symbol"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list_of->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_of->add_column("total_price_loc", "Total " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list_of->set_footer("total_price_loc", number_format($project_cost_fixturing_other_loc + $cost_estimation_item_total_furniture["in_order_currency"],2, ".", "'") . "\n" . $order_currency["symbol"]);
}
$list_of->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);

$list_of->add_column("order_item_supplier_freetext", "Supplier Info");
//$list_of->add_column("forwarder_company", "Forwarder");

$list_of->populate();
$list_of->process();


/********************************************************************
    Create List for Architectural Cost
*********************************************************************/ 
$list_ar = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list_ar->set_entity("order_item");
$list_ar->set_filter("order_item_cost_group = 9 and order_item_order = " . $project["project_order"]);
$list_ar->set_order("item_code, order_item_id");

$list_ar->add_column("item_shortcut", "Item Code", $link, "", "", COLUMN_NO_WRAP);
$list_ar->add_column("order_item_text", "Name");

$list_ar->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT);
$list_ar->set_footer("item_shortcut", "Total");
$list_ar->set_footer("order_item_system_price", number_format($project_cost_architectural,2, ".", "'") . "\n" . $system_currency["symbol"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list_ar->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_ar->set_footer("order_item_client_price", number_format($project_cost_architectural_loc,2, ".", "'") . "\n" . $order_currency["symbol"]);
}
	

$list_ar->add_column("order_item_supplier_freetext", "Supplier Info");

$list_ar->populate();
$list_ar->process();

/********************************************************************
    Create List for Equipment
*********************************************************************/ 
$list_eq = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list_eq->set_entity("order_item");
$list_eq->set_filter("order_item_cost_group = 11 and order_item_order = " . $project["project_order"]);
$list_eq->set_order("item_code, order_item_id");

$list_eq->add_column("item_shortcut", "Item Code", $link, "", "", COLUMN_NO_WRAP);

$list_eq->add_column("order_item_text", "Name");
$list_eq->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT);
$list_eq->set_footer("item_shortcut", "Total");
$list_eq->set_footer("order_item_system_price", number_format($project_cost_equipment,2, ".", "'") . "\n" . $system_currency["symbol"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list_eq->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_eq->set_footer("order_item_client_price", number_format($project_cost_equipment_loc,2, ".", "'") . "\n" . $order_currency["symbol"]);
}

$list_eq->add_column("order_item_supplier_freetext", "Supplier Info");

$list_eq->populate();
$list_eq->process();

/********************************************************************
    Create List for Equipment
*********************************************************************/ 
$list_ot = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list_ot->set_entity("order_item");
$list_ot->set_filter("order_item_cost_group = 8 and order_item_order = " . $project["project_order"]);
$list_ot->set_order("item_code, order_item_id");

$list_ot->add_column("item_shortcut", "Item Code", $link, "", "", COLUMN_NO_WRAP);

$list_ot->add_column("order_item_text", "Name");
$list_ot->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT);
$list_ot->set_footer("item_shortcut", "Total");
$list_ot->set_footer("order_item_system_price", number_format($project_cost_other,2, ".", "'") . "\n" . $system_currency["symbol"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list_ot->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_ot->set_footer("order_item_client_price", number_format($project_cost_other_loc,2, ".", "'") . "\n" . $order_currency["symbol"]);
}

$list_ot->add_column("order_item_supplier_freetext", "Supplier Info");

$list_ot->populate();
$list_ot->process();



/********************************************************************
    Create List for item in projects coming from catalogue orders
*********************************************************************/ 
$has_catalogue_orders = false;
$sql_oip = "select order_items_in_project_id, order_items_in_project_order_item_id, " .
		   "order_items_in_project_id, order_item_order, order_items_in_project_cms_remark, " . 
		   " IF(address_shortcut <> '', address_shortcut, order_item_supplier_freetext) as address_shortcut, " .
		   "order_item_text,  order_item_id, " .
		   "DATE_FORMAT(order_item_ordered, '%d.%m.%y') as date1, " .
			"DATE_FORMAT(order_item_pickup, '%d.%m.%y') as date2, " .
			"DATE_FORMAT(order_item_arrival, '%d.%m.%y') as date3, " .
			"order_item_quantity, order_item_system_price, order_item_client_price, " . 
		   " order_item_po_number, order_item_quantity, project_cost_groupname_name, order_items_in_project_real_price " . 
		   " from order_items_in_projects " .
		   " left join order_items on order_item_id = order_items_in_project_order_item_id " .
		   " left join addresses on order_item_supplier_address = address_id ".
		   " left join project_cost_groupnames on project_cost_groupname_id =  order_items_in_project_costgroup_id " .
		   " where order_items_in_project_project_order_id = " . $project["project_order"];


$replacementinfos = array();
$res = mysql_query($sql_oip) or dberror($sql_oip);
while($row = mysql_fetch_assoc($res))
{
	//check if item is a replacement
	$sql_r = "select count(order_item_replacement_id) as num_recs " . 
		     " from order_item_replacements " . 
		     " where order_item_replacement_catalog_order_order_item_id = " . dbquote($row["order_item_id"]);


	$res_r = mysql_query($sql_r) or dberror($sql_r);
	$row_r = mysql_fetch_assoc($res_r);
	if($row_r["num_recs"] > 0)
	{
		$replacementinfos[$row["order_items_in_project_id"]] = '<span class="error">Replacement</span>';
	}
}

$link = "/user/order_assign_items_to_a_project.php?oid={order_item_order}";

$list_oip = new ListView($sql_oip, LIST_HAS_HEADER | LIST_HAS_FOOTER);


$list_oip->set_entity("order_items_in_projects");
$list_oip->set_title("Catalogue Orders");

$list_oip->add_column("address_shortcut", "Supplier", "", "", "", COLUMN_NO_WRAP);
$list_oip->add_column("project_cost_groupname_name", "Cost Group", "", "", "", COLUMN_NO_WRAP);
$list_oip->add_text_column("replacement", "", COLUMN_NO_WRAP | COLUMN_UNDERSTAND_HTML, $replacementinfos);
$list_oip->add_column("order_item_text", "Description", $link, "", "");
$list_oip->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

$list_oip->add_column("order_item_system_price", "Price CHF", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list_oip->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);


$list_oip->populate();
$list_oip->process();

/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("View List of Materials");
$form->render();
echo "<br />";

echo "<h3>Local Construction</h3><hr />";
$list_co->render();
echo "<br />";
echo "<br />";

echo "<h3>Store fixturing / Furniture - Standard Items</h3><hr />";
$list_sf->render();
echo "<br />";
echo "<br />";

echo "<h3>Store fixturing / Furniture - Special Items</h3><hr />";
$list_si->render();
echo "<br />";
echo "<br />";


echo "<h3>Store fixturing / Furniture - Freight Charges</h3><hr />";
$list_fc->render();
echo "<br />";
echo "<br />";

echo "<h3>Store fixturing / Furniture - Other</h3><hr />";
$list_of->render();
echo "<br />";
echo "<br />";

echo "<h3>Architectural Services</h3><hr />";
$list_ar->render();
echo "<br />";
echo "<br />";

echo "<h3>Equipment</h3><hr />";
$list_eq->render();
echo "<br />";
echo "<br />";

echo "<h3>Other</h3><hr />";
$list_ot->render();
echo "<br />";

echo "<hr />";
$list_oip->render();

require_once "include/project_footer_logistic_state.php";
$page->footer();

?>