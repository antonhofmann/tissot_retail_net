<?php
/********************************************************************

    catalog.php

    Entry page for the catalog section group.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-31
    Modified by:    Anton.Hofmann (aho@mediaparx.ch)
    Date modified:  2003-07-14
    Version:        1.1.4

    Copyright (c) 2002, Swatch AG, All Rights Reserved.


*********************************************************************/
require_once "../include/frame.php";
require_once "include/order_functions.php";
require_once "include/get_functions.php";

set_referer("catalog_category.php");

check_access("can_view_catalog");

/********************************************************************
    prepare all data needed
*********************************************************************/
// get the region of users' address
$user = get_user(user_id());
$user_country = $user["country"];
$user_region = get_user_region(user_id());

$sql = "select DISTINCT concat(product_line_id, item_category_id, item_subcategory) as pcid, " . 
       " product_line_id, item_category_id, item_subcategory_id, " .
       " product_line_name, item_category_name, item_subcategory_name, item_subcategory, " .
	   " item_category_sortorder, item_category_name " . 
       " from product_lines " .
	   "INNER JOIN product_line_supplying_groups ON product_line_supplying_group_line_id = product_line_id
	    INNER JOIN item_supplying_groups ON item_supplying_group_supplying_group_id = product_line_supplying_group_group_id
		INNER JOIN items ON item_id = item_supplying_group_item_id
		INNER JOIN supplying_groups on supplying_group_id = item_supplying_group_supplying_group_id 
		INNER JOIN productline_regions ON productline_region_productline = product_line_id
		INNER JOIN item_categories ON item_category_id = item_category
		LEFT JOIN item_subcategories on item_subcategory_id = item_subcategory
		INNER JOIN item_countries ON item_country_item_id = item_id 
		LEFT join item_pos_types on item_pos_type_item = item_id ";

if(has_access("can_edit_catalog") or has_access("can_enter_orders_for_other_companies"))
{
   $list_filter = "(product_line_clients = 1 
					 or product_line_budget = 1)
					 and product_line_active = 1
					 and item_category_active = 1 
					 and item_active = 1
					 and item_type = 1";
   
   $sql_product_lines = "select product_line_id, product_line_name " . 
	                    " from product_lines "  .
	                    " where (product_line_clients = 1 or product_line_budget = 1) and product_line_active = 1";
   $sql_product_lines .= " order by product_line_name";

   $res = mysql_query($sql_product_lines);
   while($row = mysql_fetch_assoc($res))
   {
		$product_line_filter[$row["product_line_id"]] = $row["product_line_name"];
   }
}
else
{
    $list_filter = "product_line_clients = 1 
					 and product_line_active = 1
					 and item_category_active = 1
					 and (item_visible = 1 or item_visible_in_projects) 
					 and item_active = 1
					 and item_type = 1 
					 and item_country_country_id = " . dbquote($user_country) . 
					 " and productline_region_region = " . $user_region;


 

   $sql_product_lines = "select DISTINCT product_line_id, product_line_name " . 
					    " from product_lines " .
					    "INNER JOIN product_line_supplying_groups ON product_line_supplying_group_line_id = product_line_id
						INNER JOIN item_supplying_groups ON item_supplying_group_supplying_group_id = product_line_supplying_group_group_id
						INNER JOIN items ON item_id = item_supplying_group_item_id
						INNER JOIN supplying_groups on supplying_group_id = item_supplying_group_supplying_group_id 
						INNER JOIN productline_regions ON productline_region_productline = product_line_id
						INNER JOIN item_categories ON item_category_id = item_category
						INNER JOIN item_countries ON item_country_item_id = item_id";
	$sql_product_lines .= " where " . $list_filter;
	$sql_product_lines .= " order by product_line_name";

	$res = mysql_query($sql_product_lines);
    while($row = mysql_fetch_assoc($res))
    {
 		$product_line_filter[$row["product_line_id"]] = $row["product_line_name"];
    }
}


if(param("product_line_filter"))
{
	$list_filter .= " and product_line_id = " . dbquote(param("product_line_filter"));
	$_SESSION["product_line_filter"] = param("product_line_filter");
}
elseif(array_key_exists("product_line_filter", $_SESSION))
{
	$list_filter .= " and product_line_id = " . dbquote($_SESSION["product_line_filter"]);
	param("product_line_filter", $_SESSION["product_line_filter"]);
}

$links = array();
$print_links = array();

$sql_tmp = $sql . " where " . $list_filter;

$res = mysql_query($sql_tmp) or dberror($sql_tmp);
while ($row = mysql_fetch_assoc($res))
{
	
	

	if($row["item_subcategory"] > 0)
	{
		$links[$row["pcid"]] = '<a href="catalog_category.php?plid=' . $row["product_line_id"] . '&cid=' . $row["item_category_id"] . '&scid=' .$row["item_subcategory"] . '">' . $row["item_subcategory_name"] . '</a>';

		//links to print all items
		$entityValue = http_build_query(array(
			'frame' => true,
			'nofilters' => true,
			'entity' => 'catalog_category',
			'entity_values' => array(
				'searchterm' => "",
				'cid' => $row["item_category_id"],
				'plid' => param("product_line_filter"),
				'scid' => $row["item_subcategory"],
				'sg' => ""
			)
		));
	}
	else
	{
		$links[$row["pcid"]] = '<a href="catalog_category.php?plid=' . $row["product_line_id"] . '&cid=' . $row["item_category_id"] . '&scid=0'  . '">GENERAL ITEMS</a>';

		//links to print all items
		$entityValue = http_build_query(array(
			'frame' => true,
			'nofilters' => true,
			'entity' => 'catalog_category',
			'entity_values' => array(
				'searchterm' => "",
				'cid' => $row["item_category_id"],
				'plid' => param("product_line_filter"),
				'scid' => "",
				'sg' => ""
			)
		));
	}

	

	$print_links[$row["pcid"]] = '<a data-fancybox-type="iframe" href="/applications/templates/item.booklet.php?' .  $entityValue. '">Product Info Sheets</a>';

	$_print_link = 'print_price_list_xls.php' .
		 '?plid=' . $row["product_line_id"] . '&cid=' . $row["item_category_id"] . '&scid=' .$row["item_subcategory"];

	$_print_link = "javascript:popup('" . $_print_link . "');";

	$_print_link ='<a href="' . $_print_link . '">Price List</a>';

	
	$print_links2[$row["pcid"]] = $_print_link;


}

/********************************************************************
    Search Form
*********************************************************************/ 
$form = new Form("orders", "order");
$form->add_comment("Enter a search term or choose from the list of item categories below.<br /><br />");
$form->add_list("product_line_filter", "Product Line", $product_line_filter, SUBMIT, param("product_line_filter"));
$form->add_edit("searchterm", "Search Term");



if(has_access("can_edit_catalog") or has_access("can_order_special_items"))
{
	//$form->add_button("special_item", "Order a Special Item");
}
$form->add_button("search", "Search Catalog");


if(param("product_line_filter"))
{
	$_print_link = 'print_price_list_xls.php' .
			 '?plid=' . param("product_line_filter");

	$_print_link = "javascript:popup('" . $_print_link . "');";

	$form->add_button("print_price_list", "Print Price List", $_print_link);
}

/********************************************************************
    Create List of categories
*********************************************************************/ 

$list = new ListView($sql, LIST_HAS_HEADER);
$list->set_title("");

$list->set_entity("item_categories");
$list->set_filter($list_filter);

$list->set_order("item_category_name, item_subcategory_name");
$list->set_group("item_category_name", "", "item_category_sortorder");

$list->add_hidden("pid", param("pid"));
$list->add_hidden("oid", $project["project_order"]);

$list->add_text_column("category", "Item Category", COLUMN_UNDERSTAND_HTML, $links);
$list->add_text_column("print", "", COLUMN_UNDERSTAND_HTML, $print_links);
$list->add_text_column("print2", "", COLUMN_UNDERSTAND_HTML, $print_links2);

/*
if(param("product_line_filter"))
{
	$entityValue = http_build_query(array(
		'frame' => true,
		'nofilters' => true,
		'entity' => 'catalog_category',
		'entity_values' => array(
			'searchterm' => "",
			'cid' => "",
			'plid' => param("product_line_filter"),
			'scid' => "",
			'sg' => ""
		)
	));

	$list->add_button(LIST_BUTTON_LINK, "Print Product Info Sheets", array(
		'href' => '/applications/templates/item.booklet.php?'.$entityValue,
		'data-fancybox-type' => 'iframe'
	));
}
*/




/********************************************************************
    Populate page
*********************************************************************/ 

$form->populate();
$form->process();


$list->process();



if ($form->button("search"))
{
	if(!$form->value("searchterm"))
	{
		$form->error("The search term must not be empty.");
	}
	else
	{
		$link = "catalog_category.php?searchterm=" . str_replace(' ', '', $form->value("searchterm"));
		redirect($link);
	}
}
elseif($form->button("special_item"))
{
	redirect("order_new_special_item.php");
}

$page = new Page("catalog");
$page->register_action('home', 'Home', "welcome.php");


if (has_access("can_create_new_orders"))
{
   if (!basket_is_empty())
    {
        $page->register_action('basket', 'Shopping List', "basket.php");
    }
}

if (has_access("can_create_new_orders"))
{
    //$page->register_action('new', 'New Order', "catalog.php");
    
}

$page->header();
$page->title('Browse Catalog - Place New Catalog Orders');
$form->render();
echo '<br /><br />';

if(param("product_line_filter"))
{
	$list->render();
}


?>

<script type="text/javascript">
	$(document).keypress(function(e) {
		if(e.which == 13) {
			
			if($("#searchterm").val())
			{
				button("search");
			}
		}
	});
</script>

<?php

$page->footer();
?>