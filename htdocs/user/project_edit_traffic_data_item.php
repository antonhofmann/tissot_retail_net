<?php
/********************************************************************

    project_edit_traffic_data_item.php

    List Data concerning delivery and traffic

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-20
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-16
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "include/order_state_constants.php";


check_access("can_edit_traffic_data_in_projects");


/********************************************************************
    prepare all data needed
*********************************************************************/

// read project and order details
$project = get_project(param("pid"));

$development_state = get_project_development_status($project["project_order"]);

/*
if(has_access("can_see_logistic_status"))
{
	$logistic_state = get_logistic_state($project["project_order"], user_id(), $project["order_actual_order_state_code"]);
	$offer_state = get_offer_state($project["project_order"], user_id(), $project["order_actual_order_state_code"]);
}
*/

// read order item details
$order_item = get_order_item(id());

// get supplier address details
$supplier_address = get_address($order_item["supplier"]);
$warehouse_address = get_order_item_address(4, $project["project_order"], id());
$delivery_address = get_order_item_address(2,  $project["project_order"], id());

// get forwarder address details
$forwarder_address = get_address($order_item["forwarder"]);

// read date entries from dates
$exrp = get_last_order_item_date(id(), "EXRP");
//$pick = get_last_order_item_date(id(), "PICK");
$exar = get_last_order_item_date(id(), "EXAR");
$acar = get_last_order_item_date(id(), "ACAR");

//get all supplier addresses
$supplier_addresses = get_order_item_supplier_addresses_of_this_forwarder($project["project_order"], $order_item["forwarder"]);

/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("order_item_order", $project["project_order"]);
$form->add_hidden("order_item_id", id());
$form->add_hidden("order_item_forwarder", $order_item["forwarder"]);

$form->add_lookup("project_id", "Project Number", "projects", "project_number", 0, param("pid"));
$form->add_lookup("product_line", "Product Line", "product_lines", "product_line_name", 0, $project["project_product_line"]);
$form->add_lookup("project_postype", "POS Type", "postypes", "postype_name", 0, $project["project_postype"]);

if(count($user_roles) == 1 
	and (in_array(5, $user_roles) or in_array(6, $user_roles)))
{
	$udata = get_user(user_id());
	$tmp = get_external_losgistics_state($project["project_order"], $udata["address"]);
	if(count($tmp))
	{
		$form->add_label("status_label", "Logistic Status", 0, $tmp["name"]);
	}
	else
	{
		$form->add_label("status_label", "Project Status Development", 0, $development_state["name"]);
	}
}
else
{
	if(isset($offer_state) and $offer_state["data"])
	{
		$form->add_label("status_label", "Project Status Development", 0,  $development_state["name"], 1, "offer_info");
	}
	else
	{
		$form->add_label("status_label", "Project Status Development", 0,  $development_state["name"]);
	}
}

/*
if(has_access("can_see_logistic_status") and isset($logistic_state) and $logistic_state["data"])
{
	$form->add_label("logistic_status_label", "Project Status Logistic", 0, $logistic_state["info"], 1, "logistic_info");
}
elseif(has_access("can_see_logistic_status") and isset($logistic_state) and $logistic_state["info"])
{
	$form->add_label("logistic_status_label", "Project Status Logistic", 0, $logistic_state["info"]);
}
*/

if ($project["project_retail_coordinator"])
{
    $form->add_lookup("retail_coordinator", "Project Leader", "users", "concat(user_name, ' ', user_firstname)", 0, $project["project_retail_coordinator"]);
}
else
{
    $form->add_label("retail_coordinator", "Project Leader");
}


// POS location
$form->add_section("POS Location Address");
$form->add_label("shop_address_company", "Project Name", 0, $project["order_shop_address_company"]);

if ($project["order_shop_address_company2"])
{
    $form->add_label("shop_address_company2", "", 0, $project["order_shop_address_company2"]);
}

$form->add_label("shop_address_address", "Street", 0, $project["order_shop_address_address"]);

if ($project["order_shop_address_address2"])
{
    $form->add_label("shop_address_address2", "Additional Address Info", 0, $project["order_shop_address_address2"]);
}

$form->add_label("shop_address_place", "City", 0, $project["order_shop_address_zip"] . " " . $project["order_shop_address_place"]);

$form->add_lookup("shop_address_country", "", "countries", "country_name", 0, $project["order_shop_address_country"]);


$line = "concat(user_name, ' ', user_firstname)";

$form->add_section("Forwarder");
if ($order_item["forwarder"])
{
    $form->add_label("forwarder_company", "Company", 0, $forwarder_address["company"]);
}
else
{
    $form->add_label("forwarder_company", "Company");
}

$form->add_section("Supplier");
if ($order_item["supplier"])
{
    $form->add_label("supplier_company", "Company", 0, $supplier_address["company"]);
    $form->add_label("supplier_address", "Street", 0, $supplier_address["address"]);
    $form->add_label("supplier_place", "City", 0, $supplier_address["zip"] . " " . $supplier_address["place"]);
    $form->add_lookup("supplier_country", "Country", "countries", "country_name", 0, $supplier_address["country"]);

    if ($supplier_address["contact"])
    {
        $line = "concat(user_name, ' ', user_firstname)";
        $form->add_lookup("supplier_contact_user", "Contact", "users", $line, 0, $supplier_address["contact"]);
        $form->add_lookup("supplier_contact_phone", "Phone", "users", "user_phone", 0, $supplier_address["contact"]);
        $form->add_lookup("supplier_contact_email", "Email", "users", "user_email", 0, $supplier_address["contact"]);
    }
}
else
{
    $form->add_label("supplier_company", "Company");
    $form->add_label("supplier_address", "Street");
    $form->add_label("supplier_place", "City");
    $form->add_label("supplier_country", "Country");
    $form->add_label("supplier_contact", "Contact");
    $form->add_label("supplier_phone", "Phone");
    $form->add_label("supplier_email", "Email");
}


$form->add_section("Supplier's Warehouse Pick Up Address");
if (count($warehouse_address) > 0 )
{
    $form->add_label("warehouse_address_company", "Company", 0, $warehouse_address["company"]);

    if ($warehouse_address["company2"])
    {
        $form->add_label("warehouse_address_company2", "", 0, $warehouse_address["company2"]);
    }

    $form->add_label("warehouse_address_address", "Street", 0, $warehouse_address["address"]);

    if ($warehouse_address["address2"])
    {
        $form->add_label("warehouse_address_address2", "Additional Address Info", 0, $warehouse_address["address2"]);
    }

    $form->add_label("warehouse_address_place", "City", 0, $warehouse_address["zip"] . " " . $warehouse_address["place"]);
    $form->add_lookup("warehouse_address_country", "", "countries", "country_name", 0, $warehouse_address["country"]);
    $form->add_label("warehouse_address_phone", "Phone", 0, $warehouse_address["phone"]);
    $form->add_label("warehouse_address_mobile_phone", "Mobile Phone", 0, $warehouse_address["mobile_phone"]);
    $form->add_label("warehouse_address_email", "Email", 0, $warehouse_address["email"]);
    $form->add_label("warehouse_address_contact", "Contact", 0, $warehouse_address["contact"]);
}
else
{
    $form->add_label("warehouse_address_company", "Company");
    $form->add_label("warehouse_address_address", "Street");
    $form->add_label("warehouse_address_place", "City");
    $form->add_label("warehouse_address_country", "Country");
    $form->add_label("warehouse_address_phone", "Phone");
    $form->add_label("warehouse_address_mobile_phone", "Mobile Phone");
    $form->add_label("warehouse_address_email", "Email");
    $form->add_label("warehouse_address_contact", "Contact");
}


$form->add_section("Ship to ");
if (count($delivery_address) > 0 )
{
    $form->add_label("delivery_address_company", "Company", 0, $delivery_address["company"]);

    if ($delivery_address["company2"])
    {
        $form->add_label("delivery_address_company2", "", 0, $delivery_address["company2"]);
    }

    $form->add_label("delivery_address_address", "Street", 0, $delivery_address["address"]);

    if ($delivery_address["address2"])
{
        $form->add_label("delivery_address_address2", "Additional Address Info", 0, $delivery_address["address2"]);
    }

    $form->add_label("delivery_address_place", "City", 0, $delivery_address["zip"] . " " . $delivery_address["place"]);
    $form->add_lookup("delivery_address_country", "", "countries", "country_name", 0, $delivery_address["country"]);
    $form->add_label("delivery_address_phone", "Phone", 0, $delivery_address["phone"]);
    $form->add_label("delivery_address_mobile_phone", "Mobile Phone", 0, $delivery_address["mobile_phone"]);
    $form->add_label("delivery_address_email", "Email", 0, $delivery_address["email"]);
    $form->add_label("delivery_address_contact", "Contact", 0, $delivery_address["contact"]);
}
else
{
    $form->add_label("delivery_address_company", "Company");
    $form->add_label("delivery_address_address", "Street");
    $form->add_label("delivery_address_place", "City");
    $form->add_label("delivery_address_country", "Country");
    $form->add_label("delivery_address_phone", "Phone");
    $form->add_label("delivery_address_mobile_phone", "Mobile Phone");
    $form->add_label("delivery_address_email", "Email");
    $form->add_label("delivery_address_contact", "Contact");
}


$form->add_section("Item Details");
$form->add_label("order_item_code", "Item Code", 0, $order_item["code"]);
$form->add_label("order_item_text", "", 0, $order_item["text"]);
$form->add_label("order_item_quantity", "Quantity", 0, $order_item["quantity"]);
$form->add_label("order_item_po_number", "P.O. Number", 0, $order_item["po_number"]);

$form->add_section("Traffic Details");

$form->add_lookup("transportation_type", "Transportation Type", "transportation_types", "transportation_type_name", 0, $order_item["transportation"]);


if (has_access("has_access_to_all_traffic_data_in_projects"))
{
	$form->add_edit("order_item_preferred_arrival_date", "Preferred Arrival Date", NOTNULL,  to_system_date($order_item["preferred_arrival_date"]), TYPE_DATE, 10);
}
else
{
	$form->add_label("order_item_preferred_arrival_date", "Preferred Arrival Date", 0, to_system_date($order_item["preferred_arrival_date"]));
}
$form->add_hidden("old_order_item_preferred_arrival_date",  to_system_date($order_item["preferred_arrival_date"]));

$form->add_label("expected_ready_for_pick_up", "Expected Ready for Pickup", 0, $exrp["last_date"]);
$form->add_label("ch1", "Number of changes", 0,  $exrp["changes"]);


//if ($exrp["last_date"] and number_format($project["order_actual_order_state_code"],0) >= REQUEST_FOR_DELIVERY_SUBMITTED)
//if($exrp["last_date"] and number_format($project["order_actual_order_state_code"],0) >= CONFIRM_ORDER_BY_SUPPLIER)
if($exrp["last_date"])
{
     
	
	$form->add_edit("order_item_shipment_code", "Shipment Code", 0,  $order_item["shipment_code"], TYPE_CHAR, 40);
    //$form->add_hidden("pick_up_old_value",  $pick["last_date"]);
    //$form->add_edit("order_item_pickup", "Pick Up", 0, $pick["last_date"], TYPE_DATE, 10);
    //$form->add_label("ch2", "Number of changes", 0,  $pick["changes"]);

    $form->add_hidden("expected_arrival_old_value",  $exar["last_date"]);
    $form->add_edit("expected_arrival", "Expected Arrival", 0, $exar["last_date"], TYPE_DATE, 10);
    $form->add_label("ch3", "Number of changes", 0,  $exar["changes"]);

    $form->add_hidden("arrival_old_value",  $acar["last_date"]);
    $form->add_edit("arrival", "Arrival", 0, $acar["last_date"], TYPE_DATE, 10);
    $form->add_label("ch4", "Number of changes", 0,  $acar["changes"]);

	foreach($supplier_addresses as $key=>$supplier_address)
	{
		$form->add_checkbox("change_all_dates_" . $supplier_address["id"], "apply date entry to every item of " . $supplier_address["company"] . ", " . $supplier_address["place"], 0, 0);
	}
	
    $form->add_button("save", "Save Data");
}

$form->add_button(FORM_BUTTON_BACK, "Back");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


if ($form->button("save"))
{
    
	$form->add_validation("from_system_date({arrival}) <=  " . dbquote(date("Y-m-d")), "The arrival date must not be a future date!");

	
	if ($form->validate())
    {
        update_traffic_data_item($form, 1, $supplier_addresses);

        // send an email to supplier, if pickup date has changed

        /*
		if ($form->value("pick_up_old_value"))
        {
            if ($form->value("pick_up_old_value") != $form->value("order_item_pickup"))
            {
                $mail = new Mail();
                $num_mails = 0;

                $sql = "select order_id, order_number, order_item_po_number, ".
                       "if(order_item_item <>'', item_code, item_type_name) as item_shortcut, ".
                       "order_shop_address_company, order_shop_address_place, country_name, " .
                       "users.user_email as recepient, users.user_email_cc as cc," .
                       "users.user_email_deputy as deputy, " .
                       "users.user_address as address_id, " .
                       "users1.user_email as sender, concat(users1.user_name, ' ', users1.user_firstname) as user_fullname ".
                       "from orders ".
                       "left join countries on country_id = order_shop_address_country " .
                       "left join order_items on order_item_order = order_id " .
                       "left join items on order_item_item = item_id ".
                       "left join item_types on order_item_type = item_type_id ".
                       "left join addresses on address_id = order_item_supplier_address ".
                       "left join users on users.user_address = address_id ".
                       "left join users as users1 on " . user_id() . "= users1.user_id ".
                       "where users.user_active = 1 and order_item_id = " . id();
                
                $mail_sent_to = "\nMail sent to: ";

                $res = mysql_query($sql) or dberror($sql);
                while ($row = mysql_fetch_assoc($res) and $row["recepient"])
                {
                    $sender_email = $row["sender"];
                    $sender_name =  $row["user_fullname"];
                    
                    $subject = MAIL_SUBJECT_PREFIX . ": Pickup date has changed - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];

                    
                    $mail->set_subject($subject);

                    $mail->set_sender($sender_email, $sender_name);

                    $mail->add_recipient($row["recepient"]);
                    if($row["cc"])
                    {
                        $mail->add_cc($row["cc"]);
                    }
                    if($row["deputy"])
                    {
                        $mail->add_cc($row["deputy"]);
                    }

                    $mail_sent_to = $mail_sent_to . "\n" . $row["recepient"];

                    $bodytext0 = "The pickup date has changed for the following Project\n" .
                                 "Project: " . $project["order_number"] . "    Item:  " . $row["item_shortcut"] . "\n".
                                 "from " . $form->value("pick_up_old_value") . " to " . $form->value("pick_up");

                    $num_mails++;
                                                
                }


                $link ="project_view_traffic_data.php?pid=" . param("pid");
                $bodytext = $bodytext0 . "\n\nclick below to have direct access to the project:\n";

                $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
                
                $mail->add_text($bodytext);
                if($num_mails > 0)
                {
                    if($senmail_activated == true)
					{
						$result = $mail->send();
					}
					else
					{
						$result = 1;
					}

                    append_mail($project["project_order"], "" , user_id(), $bodytext0 . " " . $mail_sent_to , "", 1);
                }
            }
        }
		*/

        // send an email to client and retail operatro, 
        //in case expected arrival has changed

        if ($form->value("old_order_item_preferred_arrival_date"))
        {
            if ($form->value("old_order_item_preferred_arrival_date") != $form->value("order_item_preferred_arrival_date"))
            {
				$mail = new ActionMail('preferred.arrival.date.was.changed');
				
                $mail->setParam('projectID', param("pid"));
                $mail->setParam('link', URLBuilder::projectViewTrafficData(param("pid")));
                $mail->setParam('allowResend', true);
				
                $mail->setDataloader(array(
					'from_date' => $form->value("old_order_item_preferred_arrival_date"),
					'to_date'   => $form->value("order_item_preferred_arrival_date"),
				));

				if($senmail_activated == true)
				{
					$mail->send();
                    $result = $mail->isSuccess();
				}
				else
				{
					$result = 1;
				}

				//echo '<pre>';
				//print_r($mail);
			}
		}
		
		
		if ($form->value("expected_arrival_old_value"))
        {
            if ($form->value("expected_arrival_old_value") != $form->value("expected_arrival"))
            {

                
                $mail = new Mail();
                $num_mails = 0;

                $sql = "select order_id, order_number, order_item_po_number, ".
                       "if(order_item_item <>'', item_code, item_type_name) as item_shortcut, ".
                       "order_shop_address_company, order_shop_address_place, country_name, " .
                       "users.user_email as recepient, users.user_email_cc as cc," .
                       "users.user_email_deputy as deputy, " .
                       "users.user_address as address_id, " .
                       "users2.user_email as rto, " . 
                       "users1.user_email as sender, concat(users1.user_name, ' ', users1.user_firstname) as user_fullname ".
                       "from orders ".
                       "left join countries on country_id = order_shop_address_country " .
                       "left join order_items on order_item_order = order_id " .
                       "left join items on order_item_item = item_id ".
                       "left join item_types on order_item_type = item_type_id ".
                       "left join users on users.user_id = order_user ".
                       "left join users as users2 on users2.user_id = order_retail_operator ".
                       "left join users as users1 on " . user_id() . "= users1.user_id ".
                       "where order_item_id = " . id();

               
                $mail_sent_to = "\nMail sent to: ";

                $res = mysql_query($sql) or dberror($sql);
                while ($row = mysql_fetch_assoc($res) and $row["recepient"])
                {
                    $sender_email = $row["sender"];
                    $sender_name =  $row["user_fullname"];
                    
                    $subject = MAIL_SUBJECT_PREFIX . ": Expected arrival has changed - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];
                    
                    $mail->set_subject($subject);

                    $mail->set_sender($sender_email, $sender_name);

                    $mail->add_recipient($row["recepient"]);
                    $mail->add_recipient($row["rto"]);

                    if($row["cc"])
                    {
                        $mail->add_cc($row["cc"]);
                    }
                    if($row["deputy"])
                    {
                        $mail->add_cc($row["deputy"]);
                    }
                    $mail_sent_to = $mail_sent_to . "\n" . $row["recepient"];

                    $bodytext0 = "The expected arrival has changed for the following Project\n" .
                                 "Project: " . $project["order_number"] . "    Item:  " . $row["item_shortcut"] . "\n".
                                 "from " . $form->value("expected_arrival_old_value") . " to " . $form->value("expected_arrival");

                    $num_mails++;
                                                
                }


                $link ="project_view_traffic_data.php?pid=" . param("pid");
                $bodytext = $bodytext0 . "\n\nclick below to have direct access to the project:\n";

                $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
                
                $mail->add_text($bodytext);
                if($num_mails > 0)
                {
                    if($senmail_activated == true)
					{
						$result = $mail->send();
					}
					else
					{
						$result = 1;
					}

                    append_mail($project["project_order"], "" , user_id(), $bodytext0 . " " . $mail_sent_to , "", 1);
                }
            }
        }

        // send an email to client, if arrival date has changed

        if ($form->value("arrival_old_value"))
        {
            if ($form->value("arrival_old_value") != $form->value("arrival"))
            {
                
                $mail = new Mail();
                $num_mails = 0;

                $sql = "select order_id, order_number, order_item_po_number, ".
                       "if(order_item_item <>'', item_code, item_type_name) as item_shortcut, ".
                       "order_shop_address_company, order_shop_address_place, country_name, " .
                       "users.user_email as recepient, users.user_email_cc as cc," .
                       "users.user_email_deputy as deputy, " .
                       "users.user_address as address_id, " .
                       "users1.user_email as sender, concat(users1.user_name, ' ', users1.user_firstname) as user_fullname ".
                       "from orders ".
                       "left join countries on country_id = order_shop_address_country " .
                       "left join order_items on order_item_order = order_id " .
                       "left join items on order_item_item = item_id ".
                       "left join item_types on order_item_type = item_type_id ".
                       "left join users on users.user_id = order_user ".
                       "left join users as users1 on " . user_id() . "= users1.user_id ".
                       "where order_item_id = " . id();
                
                $mail_sent_to = "\nMail sent to: ";

                $res = mysql_query($sql) or dberror($sql);
                while ($row = mysql_fetch_assoc($res) and $row["recepient"])
                {
                    $sender_email = $row["sender"];
                    $sender_name =  $row["user_fullname"];
                    
                  
                    $subject = MAIL_SUBJECT_PREFIX . ": Arrival date has changed - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];
                    
                    $mail->set_subject($subject);

                    $mail->set_sender($sender_email, $sender_name);

                    $mail->add_recipient($row["recepient"]);
                    if($row["cc"])
                    {
                        $mail->add_cc($row["cc"]);
                    }
                    if($row["deputy"])
                    {
                        $mail->add_cc($row["deputy"]);
                    }

                    $mail_sent_to = $mail_sent_to . "\n" . $row["recepient"];

                    $bodytext0 = "The arrival date has changed for the following Project\n" .
                                 "Project: " . $project["order_number"] . "    Item:  " . $row["item_shortcut"] . "\n".
                                 "from " . $form->value("arrival_old_value") . " to " . $form->value("arrival");

                    $num_mails++;
                                                
                }


                $link ="project_view_traffic_data.php?pid=" . param("pid");
                $bodytext = $bodytext0 . "\n\nclick below to have direct access to the project:\n";

                $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
                
                $mail->add_text($bodytext);
                if($num_mails > 0)
                {
                    if($senmail_activated == true)
					{
						$result = $mail->send();
					}
					else
					{
						$result = 1;
					}

                    append_mail($project["project_order"], "" , user_id(), $bodytext0 . " " . $mail_sent_to , "", 1);
                }
            }
        }

        $link = "project_edit_traffic_data.php?pid=" . param("pid");
        redirect ($link);
    }
}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Traffic Data: Edit Item");
$form->render();

require_once "include/project_footer_logistic_state.php";
$page->footer();

?>