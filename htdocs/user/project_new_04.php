<?php
/********************************************************************

    project_new04.php

    Creation of a new project step 04.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-07-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-07-14
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_create_new_projects");


if(!isset($_SESSION["new_project_step_1"]))
{
	$link = "project_new_01.php";
	redirect($link);
}

set_referer("project_new_05.php");


if(count($_POST) == 0 and isset($_SESSION["new_project_step_4"]))
{
	$_SESSION["new_project_step_4"]["action"] = "";
	foreach($_SESSION["new_project_step_4"] as $key=>$value)
	{
		register_param($key);
		param($key, $value);
	}
}
/********************************************************************
    prepare all data needed
*********************************************************************/

// get user data
$user = get_user(user_id());

$poslocation_areas = array();
if(array_key_exists("posaddress_id", $_SESSION["new_project_step_1"]))
{
	
	$sql = "select posarea_area ".
			   "from posareas ".
			   "where posarea_posaddress  = " . dbquote($_SESSION["new_project_step_1"]["posaddress_id"]);


	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{
		$poslocation_areas[] = $row["posarea_area"];
	}
}


$ratings = array();
$ratings[5] = "bad";
$ratings[4] = "poor";
$ratings[3] = "good";
$ratings[2] = "very good";
$ratings[1] = "excellent";

$units = array();
$units[1] = "Square Feet (sqf)";
$units[2] = "Square Meters (sqm)";


//posareas
$posareas = array();
$sql = "select posareatype_id, posareatype_name " . 
	   "from posareatypes " . 
	   " order by posareatype_name";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$posareas[$row["posareatype_id"]]	= $row["posareatype_name"];
}

//get business types and price ranges

$businesstypes = array();
$sql = "select businesstype_id, businesstype_text " . 
	   "from businesstypes " . 
	   " order by businesstype_text";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$businesstypes[$row["businesstype_id"]]	= $row["businesstype_text"];
}
$businesstypes["---"]	= "---------------------------------";
$businesstypes[999999]	= "Other not listed above";

$priceranges = array();
$sql = "select pricerange_id, pricerange_text " . 
	   "from priceranges " . 
	   " order by pricerange_text";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$priceranges[$row["pricerange_id"]]	= $row["pricerange_text"];
}


/********************************************************************
    build form
*********************************************************************/
$form = new Form("", "project");

$form->add_section("POS Location");
$posaddress = $_SESSION["new_project_step_1"]["shop_address_company"] . ", " . $_SESSION["new_project_step_1"]["shop_address_zip"]. " " . $_SESSION["new_project_step_1"]["shop_address_place"];
$form->add_comment($posaddress);

$form->add_section("Environment*");

if($_SESSION["new_project_step_1"]["project_kind"] > 1) // renovation project, takover/renovation or take over
{
	$form->add_comment("Please verify the environment information. You must choose at least one environment. Multiple choice is also possible.");

	
	foreach($posareas as $key=>$name)
	{
		if(array_key_exists("new_project_step_4", $_SESSION) and $_SESSION["new_project_step_4"]["area"] = $key)
		{
			$form->add_checkbox("area". $key, $name, 1, 0, "");
		}
		elseif(in_array($key, $poslocation_areas)) {
			
			$form->add_checkbox("area". $key, $name, 1, 0, "");
		}
		else
		{
			$form->add_checkbox("area". $key, $name, "", 0, "");
		}
	}
	
		
	$form->add_comment("Please indicate on which floor the POS will be situated (Ground floor, Street Level, 1st Floor etc.).");


	$sql = "select * from posorders " . 
		   "where posorder_posaddress = " . dbquote($_SESSION["new_project_step_1"]["posaddress_id"]) . 
		   " and posorder_opening_date <> '0000-00-00' and posorder_opening_date is not null " . 
		   " order by posorder_opening_date DESC";

	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$form->add_edit("project_floor", "Floor*", NOTNULL,$row["posorder_floor"], TYPE_CHAR, 30);
		
		$form->add_hidden("posorder_neighbour_left", $row["posorder_neighbour_left"]);
		$form->add_hidden("posorder_neighbour_left_business_type", $row['posorder_neighbour_left_business_type']);
		$form->add_hidden("posorder_neighbour_left_business_type_new");
		$form->add_hidden("posorder_neighbour_left_price_range", $row['posorder_neighbour_left_price_range']);

		$form->add_hidden("posorder_neighbour_right", $row['posorder_neighbour_right']);
		$form->add_hidden("posorder_neighbour_right_business_type", $row['posorder_neighbour_right_business_type']);
		$form->add_hidden("posorder_neighbour_right_business_type_new");
		$form->add_hidden("posorder_neighbour_right_price_range", $row['posorder_neighbour_right_price_range']);
		
		$form->add_hidden("posorder_neighbour_acrleft", $row['posorder_neighbour_acrleft']);
		$form->add_hidden("posorder_neighbour_acrleft_business_type", $row['posorder_neighbour_acrleft_business_type']);
		$form->add_hidden("posorder_neighbour_acrleft_business_type_new");
		$form->add_hidden("posorder_neighbour_acrleft_price_range", $row['posorder_neighbour_acrleft_price_range']);
		
		$form->add_hidden("posorder_neighbour_acrright", $row['posorder_neighbour_acrright']);
		$form->add_hidden("posorder_neighbour_acrright_business_type", $row['posorder_neighbour_acrright_business_type']);
		$form->add_hidden("posorder_neighbour_acrright_business_type_new");
		$form->add_hidden("posorder_neighbour_acrright_price_range", $row['posorder_neighbour_acrright_price_range']);
		
		$form->add_hidden("posorder_neighbour_brands", $row["posorder_neighbour_brands"]);
	}
	else
	{
		$form->add_edit("project_floor", "Floor*", NOTNULL,"", TYPE_CHAR, 30);
		$form->add_hidden("posorder_neighbour_left");
		$form->add_hidden("posorder_neighbour_left_business_type");
		$form->add_hidden("posorder_neighbour_left_business_type_new");
		$form->add_hidden("posorder_neighbour_left_price_range");

		$form->add_hidden("posorder_neighbour_right");
		$form->add_hidden("posorder_neighbour_right_business_type");
		$form->add_hidden("posorder_neighbour_right_business_type_new");
		$form->add_hidden("posorder_neighbour_right_price_range");
		
		$form->add_hidden("posorder_neighbour_acrleft");
		$form->add_hidden("posorder_neighbour_acrleft_business_type");
		$form->add_hidden("posorder_neighbour_acrleft_business_type_new");
		$form->add_hidden("posorder_neighbour_acrleft_price_range");
		
		$form->add_hidden("posorder_neighbour_acrright");
		$form->add_hidden("posorder_neighbour_acrright_business_type");
		$form->add_hidden("posorder_neighbour_acrright_business_type_new");
		$form->add_hidden("posorder_neighbour_acrright_price_range");
		
		$form->add_hidden("posorder_neighbour_brands");
	}

	$sql = "select * from posaddresses where posaddress_id = " . dbquote($_SESSION["new_project_step_1"]["posaddress_id"]);
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		
		$form->add_section("Area Perception");
		$form->add_comment("Please rate by clicking a star: 5 stars are the best rating.");
		
		
		if(array_key_exists("new_project_step_4", $_SESSION) 
			and array_key_exists("posaddress_perc_class", $_SESSION["new_project_step_4"]))
		{
			$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_class"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_tourist"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_transport"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_people"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_parking"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_visibility1"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from across the Street*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_visibility2"], $flags = 0);
		}
		else
		{
			$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, $row["posaddress_perc_class"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, $row["posaddress_perc_tourist"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, $row["posaddress_perc_transport"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, $row["posaddress_perc_people"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, $row["posaddress_perc_parking"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, $row["posaddress_perc_visibility1"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from across the Street*", $ratings, $row["posaddress_perc_visibility2"], $flags = 0);
		}

		$form->add_section("Inhouse Surfaces");
		$form->add_comment("Please indicate the inhouse surface information. <strong>First read the INFOBUTTONS</strong>!");
		$form->add_list("unit_of_measurement", "Unit of measurement", $units, NOTNULL, 2);
		//$form->add_hidden("unit_of_measurement", 2);

		if($_SESSION["new_project_step_1"]["project_kind"] == 7 or $_SESSION["new_project_step_1"]["project_postype"] > 1)
		{
			//$form->add_edit("posaddress_store_grosssurface", "Gross Surface in <span id='um00'>sqms</span>*", NOTNULL, $row["posaddress_store_grosssurface"], TYPE_DECIMAL, 8, 2, 3, "grosssqm");
			//$form->add_hidden("posaddress_store_grosssurface", $row["posaddress_store_grosssurface"]);
			
			$form->add_edit("posaddress_store_totalsurface", "Total Surface in <span id='um01'>sqms</span>*", NOTNULL, $row["posaddress_store_totalsurface"], TYPE_DECIMAL, 8, 2, 3, "totalsqm");
			//$form->add_hidden("posaddress_store_totalsurface", $row["posaddress_store_totalsurface"]);

			$form->add_edit("posaddress_store_retailarea", "Sales Surface in <span id='um02'>sqms</span>*", NOTNULL, $row["posaddress_store_retailarea"], TYPE_DECIMAL, 8, 2, 3, "retailsqm");
			//$form->add_hidden("posaddress_store_retailarea", $row["posaddress_store_retailarea"]);

			$form->add_edit("posaddress_store_backoffice", "Other Surface in <span id='um03'>sqms</span>", 0, $row["posaddress_store_backoffice"], TYPE_DECIMAL, 8, 2, 3, "backofficesqm");
			//$form->add_hidden("posaddress_store_backoffice", $row["posaddress_store_retailarea"]);
			
			$form->add_hidden("posaddress_store_numfloors");
			$form->add_hidden("posaddress_store_floorsurface1");
			$form->add_hidden("posaddress_store_floorsurface2");
			$form->add_hidden("posaddress_store_floorsurface3");

		}
		else
		{
			//$form->add_edit("posaddress_store_grosssurface", "Gross Surface in <span id='um00'>sqms</span>*", NOTNULL, $row["posaddress_store_grosssurface"], TYPE_DECIMAL, 8, 2, 3, "grosssqm");
			//$form->add_hidden("posaddress_store_grosssurface", $row["posaddress_store_grosssurface"]);

			$form->add_edit("posaddress_store_totalsurface", "Total Surface in <span id='um01'>sqms</span>*", NOTNULL, $row["posaddress_store_totalsurface"], TYPE_DECIMAL, 8, 2, 3, "totalsqm");
			//$form->add_hidden("posaddress_store_totalsurface", $row["posaddress_store_totalsurface"]);

			$form->add_edit("posaddress_store_retailarea", "Sales Surface in <span id='um02'>sqms</span>*", NOTNULL, $row["posaddress_store_retailarea"], TYPE_DECIMAL, 8, 2, 3, "retailsqm");
			//$form->add_hidden("posaddress_store_retailarea", $row["posaddress_store_retailarea"]);


			$form->add_edit("posaddress_store_backoffice", "Other Surface in <span id='um03'>sqms</span>", 0, $row["posaddress_store_backoffice"], TYPE_DECIMAL, 8, 2, 3, "backofficesqm");
			//$form->add_hidden("posaddress_store_backoffice", $row["posaddress_store_backoffice"]);

			/*
			$form->add_edit("posaddress_store_numfloors", "Number of Floors*", NOTNULL, $row["posaddress_store_numfloors"], TYPE_DECIMAL, 1, 0);
			$form->add_edit("posaddress_store_floorsurface1", "Floor 1: Surface  in <span id='um04'>sqms</span>", 0, $row["posaddress_store_floorsurface1"], TYPE_DECIMAL, 8, 2);
			$form->add_edit("posaddress_store_floorsurface2", "Floor 2: Surface  in <span id='um05'>sqms</span>", 0, $row["posaddress_store_floorsurface2"], TYPE_DECIMAL, 8, 2);
			$form->add_edit("posaddress_store_floorsurface3", "Floor 3: Surface  in <span id='um06'>sqms</span>", 0, $row["posaddress_store_floorsurface3"], TYPE_DECIMAL, 8, 2);
			*/

			$form->add_hidden("posaddress_store_numfloors");
			$form->add_hidden("posaddress_store_floorsurface1");
			$form->add_hidden("posaddress_store_floorsurface2");
			$form->add_hidden("posaddress_store_floorsurface3");
		}
	}
	else
	{
		
		$form->add_section("Area Perception");
		$form->add_comment("Please rate by clicking a star: 5 stars are the best rating.");
		
		if(array_key_exists("new_project_step_4", $_SESSION) 
			and array_key_exists("posaddress_perc_class", $_SESSION["new_project_step_4"]))
		{
			$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_class"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_tourist"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_transport"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_people"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_parking"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_visibility1"], NOTNULL);
			$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from across the Street*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_visibility2"], $flags = 0);
		}
		else
		{
			$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, 0, NOTNULL);
			$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, 0, NOTNULL);
			$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, 0, NOTNULL);
			$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, 0, NOTNULL);
			$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, 0, NOTNULL);
			$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, 0, NOTNULL);
			$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from across the Street*", $ratings, 0, NOTNULL);
		}

		$form->add_section("Inhouse Surfaces");
		$form->add_comment("Please indicate the inhouse surface information. <strong>First read the INFOBUTTONS</strong>!");
		$form->add_list("unit_of_measurement", "Unit of measurement", $units, NOTNULL, 2);

		//$form->add_hidden("unit_of_measurement", 2);
		
		if($_SESSION["new_project_step_1"]["project_kind"] == 7 or $_SESSION["new_project_step_1"]["project_postype"] > 1)
		{
			
			//$form->add_edit("posaddress_store_grosssurface", "Gross Surface in <span id='um00'>sqms</span>*", NOTNULL, "", TYPE_DECIMAL, 8, 2, 3, "grosssqm");
			//$form->add_hidden("posaddress_store_grosssurface");

			$form->add_edit("posaddress_store_totalsurface", "Total Surface in <span id='um01'>sqms</span>*", NOTNULL, "", TYPE_DECIMAL, 8, 2, 3, "totalsqm");
			//$form->add_hidden("posaddress_store_totalsurface");

			$form->add_edit("posaddress_store_retailarea", "Sales Surface in <span id='um02'>sqms</span>*", NOTNULL, "", TYPE_DECIMAL, 8, 2, 3, "retailsqm");
			//$form->add_hidden("posaddress_store_retailarea");

			$form->add_edit("posaddress_store_backoffice", "Other Surface in <span id='um03'>sqms</span>", 0, "", TYPE_DECIMAL, 8, 2, 3, "backofficesqm");
			//$form->add_hidden("posaddress_store_backoffice");

			$form->add_hidden("posaddress_store_numfloors");
			$form->add_hidden("posaddress_store_floorsurface1");
			$form->add_hidden("posaddress_store_floorsurface2");
			$form->add_hidden("posaddress_store_floorsurface3");
		}
		else
		{
			//$form->add_edit("posaddress_store_grosssurface", "Gross Surface in <span id='um00'>sqms</span>*", NOTNULL, "", TYPE_DECIMAL, 8, 2, 3, "grosssqm");
			//$form->add_hidden("posaddress_store_grosssurface");

			$form->add_edit("posaddress_store_totalsurface", "Total Surface in <span id='um01'>sqms</span>*", NOTNULL, "", TYPE_DECIMAL, 8, 2, 3, "totalsqm");
			//$form->add_hidden("posaddress_store_totalsurface");


			$form->add_edit("posaddress_store_retailarea", "Sales Surface in <span id='um02'>sqms</span>*", NOTNULL, "", TYPE_DECIMAL, 8, 2, 3, "retailsqm");
			//$form->add_hidden("posaddress_store_retailarea");

			$form->add_edit("posaddress_store_backoffice", "Other Surface in <span id='um03'>sqms</span>", 0, "", TYPE_DECIMAL, 8, 2, 3, "backofficesqm");
			//$form->add_hidden("posaddress_store_backoffice");

			/*
			$form->add_edit("posaddress_store_numfloors", "Number of Floors*", NOTNULL, "", TYPE_DECIMAL, 1, 0);
			$form->add_edit("posaddress_store_floorsurface1", "Floor 1: Surface  in <span id='um04'>sqms</span>", 0, "", TYPE_DECIMAL, 8, 2);
			$form->add_edit("posaddress_store_floorsurface2", "Floor 2: Surface  in <span id='um05'>sqms</span>", 0, "", TYPE_DECIMAL, 8, 2);
			$form->add_edit("posaddress_store_floorsurface3", "Floor 3: Surface  in <span id='um06'>sqms</span>", 0, "", TYPE_DECIMAL, 8, 2);
		    */

			$form->add_hidden("posaddress_store_numfloors");
			$form->add_hidden("posaddress_store_floorsurface1");
			$form->add_hidden("posaddress_store_floorsurface2");
			$form->add_hidden("posaddress_store_floorsurface3");
		}
	}

}
else
{
	$form->add_comment("Please indicate the environment information of the new POS. You must choose at least one environment. Multiple choice is also possible.");

	foreach($posareas as $key=>$name)
	{
		if(array_key_exists("new_project_step_4", $_SESSION) and $_SESSION["new_project_step_4"]["area"] = $key)
		{
			$form->add_checkbox("area". $key, $name, 1, 0, "");
		}
		else
		{
			$form->add_checkbox("area". $key, $name, "", 0, "");
		}

	}

	$form->add_comment("Please indicate on which floor the POS will be situated (Ground floor, Street Level, 1st Floor etc.).");

	$form->add_edit("project_floor", "Floor*", NOTNULL,$row["posorder_floor"], TYPE_CHAR, 30);
	

	$form->add_section("Area Perception");
	$form->add_comment("Please rate by clicking a star: 5 stars are the best rating.");
	
	if(array_key_exists("new_project_step_4", $_SESSION) 
			and array_key_exists("posaddress_perc_class", $_SESSION["new_project_step_4"]))
		{
		$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_class"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_tourist"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_transport"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_people"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_parking"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_visibility1"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from across the Street*", $ratings, $_SESSION["new_project_step_4"]["posaddress_perc_visibility2"], $flags = 0);
	}
	else
	{
		$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from across the Street*", $ratings, 0, NOTNULL);
	}

	
	$form->add_hidden("posorder_neighbour_left");
	$form->add_hidden("posorder_neighbour_left_business_type");
	$form->add_hidden("posorder_neighbour_left_business_type_new");
	$form->add_hidden("posorder_neighbour_left_price_range");

	$form->add_hidden("posorder_neighbour_right");
	$form->add_hidden("posorder_neighbour_right_business_type");
	$form->add_hidden("posorder_neighbour_right_business_type_new");
	$form->add_hidden("posorder_neighbour_right_price_range");
	
	$form->add_hidden("posorder_neighbour_acrleft");
	$form->add_hidden("posorder_neighbour_acrleft_business_type");
	$form->add_hidden("posorder_neighbour_acrleft_business_type_new");
	$form->add_hidden("posorder_neighbour_acrleft_price_range");
	
	$form->add_hidden("posorder_neighbour_acrright");
	$form->add_hidden("posorder_neighbour_acrright_business_type");
	$form->add_hidden("posorder_neighbour_acrright_business_type_new");
	$form->add_hidden("posorder_neighbour_acrright_price_range");
	
	$form->add_hidden("posorder_neighbour_brands");


	$form->add_section("Inhouse Surfaces");
	$form->add_comment("Please indicate the inhouse surface information. <strong>First read the INFOBUTTONS</strong>!");
	$form->add_list("unit_of_measurement", "Unit of measurement", $units, NOTNULL);

	//$form->add_hidden("unit_of_measurement", 2);



	if($_SESSION["new_project_step_1"]["project_kind"] == 7 or $_SESSION["new_project_step_1"]["project_postype"] > 1)
	{
		//$form->add_edit("posaddress_store_grosssurface", "Gross Surface in <span id='um00'>sqms</span>*", NOTNULL, "", TYPE_DECIMAL, 8, 2, 3, "grosssqm");
		//$form->add_hidden("posaddress_store_grosssurface");

		$form->add_edit("posaddress_store_totalsurface", "Total Surface in <span id='um01'></span>*", NOTNULL, "", TYPE_DECIMAL, 8, 2, 3, "totalsqm");
		//$form->add_hidden("posaddress_store_totalsurface");

		$form->add_edit("posaddress_store_retailarea", "Sales Surface in <span id='um02'></span>*", NOTNULL, "", TYPE_DECIMAL, 8, 2, 3, "retailsqm");
		//$form->add_hidden("posaddress_store_retailarea");

		$form->add_edit("posaddress_store_backoffice", "Other Surface in <span id='um03'></span>", 0, "", TYPE_DECIMAL, 8, 2, 3, "backofficesqm");
		//$form->add_hidden("posaddress_store_backoffice");

		$form->add_hidden("posaddress_store_numfloors");
		$form->add_hidden("posaddress_store_floorsurface1");
		$form->add_hidden("posaddress_store_floorsurface2");
		$form->add_hidden("posaddress_store_floorsurface3");
	}
	else
	{
		//$form->add_edit("posaddress_store_grosssurface", "Gross Surface in <span id='um00'>sqms</span>*", NOTNULL, "", TYPE_DECIMAL, 8, 2, 3, "grosssqm");
		//$form->add_hidden("posaddress_store_grosssurface");

		$form->add_edit("posaddress_store_totalsurface", "Total Surface in<span id='um01'></span>*", NOTNULL, "", TYPE_DECIMAL, 8, 2, 3, "totalsqm");
		//$form->add_hidden("posaddress_store_totalsurface");

		$form->add_edit("posaddress_store_retailarea", "Sales Surface in <span id='um02'></span>*", NOTNULL, "", TYPE_DECIMAL, 8, 2, 3, "retailsqm");
		//$form->add_hidden("posaddress_store_retailarea");


		$form->add_edit("posaddress_store_backoffice", "Other Surface in <span id='um03'></span>", 0, "", TYPE_DECIMAL, 8, 2, 3, "backofficesqm");
		//$form->add_hidden("posaddress_store_backoffice");

		/*
		$form->add_edit("posaddress_store_numfloors", "Number of Floors*", NOTNULL, "", TYPE_DECIMAL, 1, 0);
		$form->add_edit("posaddress_store_floorsurface1", "Floor 1: Surface  in <span id='um04'></span>", 0, "", TYPE_DECIMAL, 8, 2);
		$form->add_edit("posaddress_store_floorsurface2", "Floor 2: Surface  in <span id='um05'></span>", 0, "", TYPE_DECIMAL, 8, 2);
		$form->add_edit("posaddress_store_floorsurface3", "Floor 3: Surface  in <span id='um06'></span>", 0, "", TYPE_DECIMAL, 8, 2);
		*/

		$form->add_hidden("posaddress_store_numfloors");
		$form->add_hidden("posaddress_store_floorsurface1");
		$form->add_hidden("posaddress_store_floorsurface2");
		$form->add_hidden("posaddress_store_floorsurface3");
	}

}

$form->add_hidden("posaddress_export_to_web",1);
$form->add_hidden("posaddress_email_on_web", 1);

$form->add_section(" ");

if($_SESSION["new_project_step_1"]["project_kind"] == 4) // Take Over
{
	//$form->add_comment("Delivery (Step 5) is not necessary in case of take over only.");
	$form->add_button("save", "Submit Request");
	$form->add_button("back_to_step_02", "Back");
}
elseif($_SESSION["new_project_step_1"]["project_kind"] == 5) // Lease Reneval
{
	//$form->add_comment("Delivery (Step 5) is not necessary in case of leas renewal.");
	$form->add_button("save", "Submit Request");
	$form->add_button("back_to_step_01", "Back");
}
else
{
	$form->add_button("step5", "Proceed to next step");
	$form->add_button("back_to_step_03", "Back");
}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$form->populate();
$form->process();

//clean fields from apostrophes
foreach($form->items as $key=>$value)
{
	if(array_key_exists('value', $value)) {
		$form->value($key, str_replace('"', '', $form->value($key)));
	}
}

if ($form->button("back_to_step_03"))
{
	$_SESSION["new_project_step_4"] = $_POST;
	
	if($_SESSION["new_project_step_1"]["project_kind"] == 7 or $_SESSION["new_project_step_1"]["project_postype"] > 1)
	{
		$_SESSION["new_project_step_4"]["posaddress_store_numfloors"] = 1;
		$_SESSION["new_project_step_4"]["posaddress_store_floorsurface1"] = $_SESSION["new_project_step_4"]["posaddress_store_totalsurface"];
		$_SESSION["new_project_step_4"]["posaddress_store_floorsurface2"] = 0;
		$_SESSION["new_project_step_4"]["posaddress_store_floorsurface3"] = 0;
	}

	
	$link = "project_new_03.php";
	redirect($link);
}
elseif ($form->button("back_to_step_02"))
{
	$_SESSION["new_project_step_4"] = $_POST;

	if($_SESSION["new_project_step_1"]["project_kind"] == 7 or $_SESSION["new_project_step_1"]["project_postype"] > 1)
	{
		$_SESSION["new_project_step_4"]["posaddress_store_numfloors"] = 1;
		$_SESSION["new_project_step_4"]["posaddress_store_floorsurface1"] = $_SESSION["new_project_step_4"]["posaddress_store_totalsurface"];
		$_SESSION["new_project_step_4"]["posaddress_store_floorsurface2"] = 0;
		$_SESSION["new_project_step_4"]["posaddress_store_floorsurface3"] = 0;
	}
		
	$link = "project_new_02.php";
	redirect($link);
}
elseif ($form->button("back_to_step_01"))
{
	$_SESSION["new_project_step_4"] = $_POST;

	if($_SESSION["new_project_step_1"]["project_kind"] == 7 or $_SESSION["new_project_step_1"]["project_postype"] > 1)
	{
		$_SESSION["new_project_step_4"]["posaddress_store_numfloors"] = 1;
		$_SESSION["new_project_step_4"]["posaddress_store_floorsurface1"] = $_SESSION["new_project_step_4"]["posaddress_store_totalsurface"];
		$_SESSION["new_project_step_4"]["posaddress_store_floorsurface2"] = 0;
		$_SESSION["new_project_step_4"]["posaddress_store_floorsurface3"] = 0;
	}
		
	$link = "project_new_01.php";
	redirect($link);
}
elseif ($form->button("save"))
{
	
	
	$error = 0;

	if(!$form->value("posaddress_perc_class")){$error = 1;}
	if(!$form->value("posaddress_perc_tourist")){$error = 1;}
	if(!$form->value("posaddress_perc_transport")){$error = 1;}
	if(!$form->value("posaddress_perc_people")){$error = 1;}
	if(!$form->value("posaddress_perc_parking")){$error = 1;}
	if(!$form->value("posaddress_perc_visibility1")){$error = 1;}
	if(!$form->value("posaddress_perc_visibility2")){$error = 1;}

	
	if($error == 1)
	{
		$form->error("Please rate all parameters in section 'Area Perception'!");
	}
	elseif($form->validate())
	{
		$_SESSION["new_project_step_4"] = $_POST;

		if($_SESSION["new_project_step_1"]["project_kind"] == 7 or $_SESSION["new_project_step_1"]["project_postype"] > 1)
		{
			$_SESSION["new_project_step_4"]["posaddress_store_numfloors"] = 1;
			$_SESSION["new_project_step_4"]["posaddress_store_floorsurface1"] = $_SESSION["new_project_step_4"]["posaddress_store_totalsurface"];
			$_SESSION["new_project_step_4"]["posaddress_store_floorsurface2"] = 0;
			$_SESSION["new_project_step_4"]["posaddress_store_floorsurface3"] = 0;
		}

		$_SESSION["new_project_step_5"] = "";
		$link = "project_new_06.php";
		redirect($link);
	}
}
elseif ($form->button("save2"))
{
	$validation_rule = "";
	foreach($posareas as $area_id=>$name)
	{
		$validation_rule .= "{area" . $area_id . "} > 0 OR ";
	}
	$validation_rule = substr($validation_rule, 0, strlen($validation_rule)-4);

	$form->add_validation($validation_rule, "The environment must be indicated!");
	
	//$form->add_validation("{posaddress_store_totalsurface} >= {posaddress_store_retailarea}", "The total surface must not be less than the sales surface!");

	//$form->add_validation("{posaddress_store_grosssurface} >= {posaddress_store_totalsurface}", "The Gross Area must not be less than the total surface!");

	$error = 0;

	if(!$form->value("posaddress_perc_class")){$error = 1;}
	if(!$form->value("posaddress_perc_tourist")){$error = 1;}
	if(!$form->value("posaddress_perc_transport")){$error = 1;}
	if(!$form->value("posaddress_perc_people")){$error = 1;}
	if(!$form->value("posaddress_perc_parking")){$error = 1;}
	if(!$form->value("posaddress_perc_visibility1")){$error = 1;}
	if(!$form->value("posaddress_perc_visibility2")){$error = 1;}



	if($form->value("posorder_neighbour_left_business_type") == 999999)
	{
		$form->add_validation("{posorder_neighbour_left_business_type_new} != ''", "Please indicate the business type for the shop on the left side.");
	}

	if($form->value("posorder_neighbour_right_business_type") == 999999)
	{
		$form->add_validation("{posorder_neighbour_right_business_type_new} != ''", "Please indicate the business type for the shop on the right side.");
	}

	if($form->value("posorder_neighbour_acrleft_business_type") == 999999)
	{
		$form->add_validation("{posorder_neighbour_acrleft_business_type_new} != ''", "Please indicate the business type for the shop across left.");
	}

	if($form->value("posorder_neighbour_acrright_business_type") == 999999)
	{
		$form->add_validation("{posorder_neighbour_acrright_business_type_new} != ''", "Please indicate the business type for the shop across right.");
	}



	if($error == 1)
	{
		$form->error("Please rate all parameters in section 'Area Perception'!");
	}
	elseif($form->validate())
	{
		$_SESSION["new_project_step_4"] = $_POST;
		if($_SESSION["new_project_step_1"]["project_kind"] == 7 or $_SESSION["new_project_step_1"]["project_postype"] > 1)
		{
			$_SESSION["new_project_step_4"]["posaddress_store_numfloors"] = 1;
			$_SESSION["new_project_step_4"]["posaddress_store_floorsurface1"] = $_SESSION["new_project_step_4"]["posaddress_store_totalsurface"];
			$_SESSION["new_project_step_4"]["posaddress_store_floorsurface2"] = 0;
			$_SESSION["new_project_step_4"]["posaddress_store_floorsurface3"] = 0;
		}

		$_SESSION["new_project_step_5"] = "";
		$link = "project_new_06.php";
		redirect($link);

	}
}
elseif ($form->button("step5"))
{
	$validation_rule = "";
	foreach($posareas as $area_id=>$name)
	{
		$validation_rule .= "{area" . $area_id . "} > 0 OR ";
	}
	$validation_rule = substr($validation_rule, 0, strlen($validation_rule)-4);

	$form->add_validation($validation_rule, "The environment must be indicated!");
	
	//$form->add_validation("{posaddress_store_totalsurface} >= {posaddress_store_retailarea}", "The total surface must not be less than the sales surface!");

	//$form->add_validation("{posaddress_store_grosssurface} >= {posaddress_store_totalsurface}", "The Gross Area must not be less than the total surface!");


	//$form->add_validation("{posaddress_store_grosssurface} >0", "Gross Surface must be greater than zero!");
	//$form->add_validation("{posaddress_store_totalsurface} >0", "Total Surface must be greater than zero!");
	//$form->add_validation("{posaddress_store_retailarea} >0", "Sales Surface must be greater than zero!");

	$error = 0;

	if(!$form->value("posaddress_perc_class")){$error = 1;}
	if(!$form->value("posaddress_perc_tourist")){$error = 1;}
	if(!$form->value("posaddress_perc_transport")){$error = 1;}
	if(!$form->value("posaddress_perc_people")){$error = 1;}
	if(!$form->value("posaddress_perc_parking")){$error = 1;}
	if(!$form->value("posaddress_perc_visibility1")){$error = 1;}
	if(!$form->value("posaddress_perc_visibility2")){$error = 1;}



	if($form->value("posorder_neighbour_left_business_type") == 999999)
	{
		$form->add_validation("{posorder_neighbour_left_business_type_new} != ''", "Please indicate the business type for the shop on the left side.");
	}

	if($form->value("posorder_neighbour_right_business_type") == 999999)
	{
		$form->add_validation("{posorder_neighbour_right_business_type_new} != ''", "Please indicate the business type for the shop on the right side.");
	}

	if($form->value("posorder_neighbour_acrleft_business_type") == 999999)
	{
		$form->add_validation("{posorder_neighbour_acrleft_business_type_new} != ''", "Please indicate the business type for the shop across left.");
	}

	if($form->value("posorder_neighbour_acrright_business_type") == 999999)
	{
		$form->add_validation("{posorder_neighbour_acrright_business_type_new} != ''", "Please indicate the business type for the shop across right.");
	}

	if($error == 1)
	{
		$form->error("Please rate all parameters in section 'Area Perception'!");
	}
	elseif($form->validate())
	{
		$_SESSION["new_project_step_4"] = $_POST;
		if($_SESSION["new_project_step_1"]["project_kind"] == 7 or $_SESSION["new_project_step_1"]["project_postype"] > 1)
		{
			$_SESSION["new_project_step_4"]["posaddress_store_numfloors"] = 1;
			$_SESSION["new_project_step_4"]["posaddress_store_floorsurface1"] = $_SESSION["new_project_step_4"]["posaddress_store_totalsurface"];
			$_SESSION["new_project_step_4"]["posaddress_store_floorsurface2"] = 0;
			$_SESSION["new_project_step_4"]["posaddress_store_floorsurface3"] = 0;
		}

		$link = "project_new_05.php";
		redirect($link);
	}
}


  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

$page->register_action('home', 'Home', "welcome.php");

$page->header();
$page->title("Project Request");

echo "<div>";
echo "<img class='stepcounter' src='/pictures/numbers/01_off.gif' alt=\"\" />";
echo "<span class='step_inactive'>Basic Project Data</span>";

if($_SESSION["new_project_step_1"]["project_kind"] == 4) // Take over
{
	echo "<img class='stepcounter' src='/pictures/numbers/02_off.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Addresses</span>";
	echo "<img class='stepcounter' src='/pictures/numbers/03_on.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Environment</span>";
}
elseif($_SESSION["new_project_step_1"]["project_kind"] == 5) // lease renewal
{
	echo "<img class='stepcounter' src='/pictures/numbers/02_on.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Environment</span>";
}
elseif($_SESSION["new_project_step_1"]["project_kind"] == 8) // popup
{
	echo "<img class='stepcounter' src='/pictures/numbers/02_off.gif' alt=\"\" />";
	echo "<span class='step_inactive'>PopUp Information</span>";
	echo "<img class='stepcounter' src='/pictures/numbers/03_on.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Comments</span>";
}
else
{
	echo "<img class='stepcounter' src='/pictures/numbers/02_off.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Addresses</span>";
	echo "<img class='stepcounter' src='/pictures/numbers/03_off.gif' alt=\"\" />";
	echo "<span class='step_inactive'>POS Information</span>";
	echo "<img class='stepcounter' src='/pictures/numbers/04_on.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Environment</span>";
	echo "<img class='stepcounter' src='/pictures/numbers/05_off.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Comments</span>";
}
echo "<br /><br /><br /></div>";

$form->render();
?>
<!--
<div id="grosssqm" style="display:none;">
    Please indicate the Gross Surface in units of measurement <strong>rented by Tissot</strong><br />incl. corridors and other public areas, back office, toilets etc.
</div>
-->
<div id="totalsqm" style="display:none;">
    Please indicate the total surface in units of measurement <strong>occupied by Tissot</strong><br />incl. back office, toilets etc.
</div> 
<div id="retailsqm" style="display:none;">
    Please indicate the surface in units of measurement <strong>occupied by Tissot</strong><br />used for sales purposes (without back office, toiletsetc.).
</div> 
<div id="backofficesqm" style="display:none;">
    Please indicate the total other surface in units of measurement <strong>occupied by Tissot</strong><br />like back office, toilets, stock areas, etc.
</div> 


<script language="Javascript">

	function round_decimal(num,decimals){
		return Math.round(num*Math.pow(10,decimals))/Math.pow(10,decimals);
	}
	
	$(document).ready(function(){
		  $("#unit_of_measurement" ).change(function() {
				
				if($("#unit_of_measurement" ).val() == 1)
				{
					//$("#um00").html("<strong> sqf</strong>");
					$("#um01").html("<strong> sqf</strong>");
					$("#um02").html("<strong> sqf</strong>");
					$("#um03").html("<strong> sqf</strong>");
					$("#um04").html("<strong> sqf</strong>");
					$("#um05").html("<strong> sqf</strong>");
					$("#um06").html("<strong> sqf</strong>");
					
					//$("#posaddress_store_grosssurface").val(round_decimal(10.7639104*$("#posaddress_store_grosssurface").val(), 2));
					$("#posaddress_store_totalsurface").val(round_decimal(10.7639104*$("#posaddress_store_totalsurface").val(), 2));
					$("#posaddress_store_retailarea").val(round_decimal(10.7639104*$("#posaddress_store_retailarea").val(), 2));
					$("#posaddress_store_backoffice").val(round_decimal(10.7639104*$("#posaddress_store_backoffice").val(), 2));

					$("#posaddress_store_floorsurface1").val(round_decimal(10.7639104*$("#posaddress_store_floorsurface1").val(), 2));
					$("#posaddress_store_floorsurface2").val(round_decimal(10.7639104*$("#posaddress_store_floorsurface2").val(), 2));
					$("#posaddress_store_floorsurface3").val(round_decimal(10.7639104*$("#posaddress_store_floorsurface3").val(), 2));
				}
				else
			    {
					//$("#um00").html("<strong> sqms</strong>");
					$("#um01").html("<strong> sqms</strong>");
					$("#um02").html("<strong> sqms</strong>");
					$("#um03").html("<strong> sqms</strong>");
					$("#um04").html("<strong> sqms</strong>");
					$("#um05").html("<strong> sqms</strong>");
					$("#um06").html("<strong> sqms</strong>");

					//$("#posaddress_store_grosssurface").val(round_decimal(0.09290304*$("#posaddress_store_grosssurface").val(), 2));
					$("#posaddress_store_totalsurface").val(round_decimal(0.09290304*$("#posaddress_store_totalsurface").val(), 2));
					$("#posaddress_store_retailarea").val(round_decimal(0.09290304*$("#posaddress_store_retailarea").val(), 2));
					$("#posaddress_store_backoffice").val(round_decimal(0.09290304*$("#posaddress_store_backoffice").val(), 2));

					$("#posaddress_store_floorsurface1").val(round_decimal(0.09290304*$("#posaddress_store_floorsurface1").val(), 2));
					$("#posaddress_store_floorsurface2").val(round_decimal(0.09290304*$("#posaddress_store_floorsurface2").val(), 2));
					$("#posaddress_store_floorsurface3").val(round_decimal(0.09290304*$("#posaddress_store_floorsurface3").val(), 2));

				}
		  });

	});

</script>

<?php
$page->footer();


?>