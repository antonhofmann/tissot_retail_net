<?php
/********************************************************************

    project_costs_bid_comparison_pdf.php

    Print empty Offer form for Local Construction Work

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-09
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2014, OMEGA SA, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/project_cost_functions.php";

if (has_access("can_view_project_costs") or has_access("can_view_budget_in_projects"))
{ 
}
else {
	redirect("noaccess.php");
}



/********************************************************************
    prepare pdf
*********************************************************************/

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');


//Instanciation of inherited class
$pdf = new TCPDF("L", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 23, 11);
$pdf->setPrintHeader(false);

$pdf->AddFont('arialn');
$pdf->AddFont('arialn', 'B');

$pdf->Open();

include("project_costs_bid_comparison_pdf_main.php");



if(array_key_exists('cerversion', $_GET) and array_key_exists('pid', $_GET))
{
	$file_path = $_SERVER['DOCUMENT_ROOT'] . '/files/ln/' . $project["project_number"] . '/cer_offer_comparison_' . $_GET['pid'] . '_version_' . $_GET['cerversion'] . '.pdf';
	$data = $pdf->Output("", "S");
	
	file_put_contents($file_path, $data);
	echo "success";
}
else
{
	$filename = "offer_comparison_" . $project["project_number"] . "_" . date("Y-m-d") . ".pdf";
	$pdf->Output($filename);
}


?>