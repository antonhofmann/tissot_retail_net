<?php
/********************************************************************

    project_opening_briefing_pdf.php

    Print Opening Briefing Briefing

    Created by:     Anton Hofmann (anton.hofmann@codingroom.ch)
    Date created:   2019-03-26
    Modified by:    Anton Hofmann (anton.hofmann@codingroom.ch)
    Date modified:  2019-03-26
    Version:        1.0.0

    Copyright (c) 2019, Tissot, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "../shared/func_posindex.php";
require_once "include/get_functions.php";


if (has_access("can_view_client_data_in_projects")
   or has_access("has_access_to_all_projects")
   or has_access("can_view_budget_in_projects")
   or has_access("can_view_delivery_schedule_in_projects")
   or has_access("can_edit_project_sheet")
   or has_access("has_access_to_cost_monitoring")
   or has_access("can_view_design_briefing") 
   or has_access("can_edit_design_briefing")
   or has_access("can_view_project_costs"))
{
}
else
{
	redirect("noaccess.php");
}

if(!array_key_exists('pid', $_GET)) {
	redirect("noaccess.php");
}

$project_id = $_GET['pid'];

/********************************************************************
    prepare all data needed
*********************************************************************/

$project = get_project($project_id);
$client_address = get_address($project["order_client_address"]);

$client = $client_address["company"] . ", " . $client_address["country_name"];


if($project["pipeline"] == 0)
{
	$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");

	$sql = "select posareatype_name " . 
		   "from posareas " .
		   "left join posareatypes on posareatype_id = posarea_area " .
		   "where posarea_posaddress = " . dbquote($pos_data["posaddress_id"]) . 
		   " order by posarea_area";
}
elseif($project["pipeline"] == 1)
{
	$pos_data = get_poslocation_from_pipeline($project["posaddress_id"], $project["order_id"]);

	$sql = "select posareatype_name  
		   from posareaspipeline
		   left join posareatypes on posareatype_id = posarea_area 
		   where posarea_posaddress = " . dbquote($pos_data["posaddress_id"]) . 
		   " order by posarea_area";
}

//POS Basic Data
$poslocation = $pos_data["posaddress_name"];
$poslocation .= ", " . $pos_data["place_name"];


$address = $pos_data["country_name"] . ", " . $pos_data["posaddress_zip"] ." " . $pos_data["place_name"] . ", " . $pos_data["posaddress_address"];

if($pos_data["posaddress_address2"]) {
	$address .= ", " . $pos_data["posaddress_address2"];
}

$poslegaltype = $project["project_costtype_text"];
	
if($project["project_popup_name"])
{
	$projectkind = $project["projectkind_name"] . ", " . $project["project_popup_name"];
}
else
{
	$projectkind = $project["projectkind_name"];
}

$projectkind_id = $project["project_projectkind"];


if(($project["project_projectkind"] == 6 or $project["project_projectkind"] == 9)
	and $project["project_relocated_posaddress_id"] > 0) // relocation
{
	$relocated_pos = get_relocated_pos_info($project["project_relocated_posaddress_id"]);
	$projectkind .= " of " . $relocated_pos["posaddress_name"];

}

//surfaces

$sql_m =  "select * from project_costs " .
        "where project_cost_order = " . dbquote($project["project_order"]);

$res_m = mysql_query($sql_m) or dberror($sql_m);
$row_m = mysql_fetch_assoc($res_m);

$posgrosssqm = $row_m["project_cost_grosssqms"] ? number_format($row_m["project_cost_grosssqms"], 2, ".", "'") : "";
$postotalsqm = $row_m["project_cost_totalsqms"] ? number_format($row_m["project_cost_totalsqms"], 2, ".", "'") : "";
$posretailsqm = $row_m["project_cost_sqms"] ? number_format($row_m["project_cost_sqms"], 2, ".", "'") : "";
$posofficesqm = $row_m["project_cost_backofficesqms"] ? number_format($row_m["project_cost_backofficesqms"], 2, ".", "'") : "";

$square_meters = "Total: " . $postotalsqm . "m2 / Sales: " . $posretailsqm . "m2 / Other: " . $posofficesqm . "m2";

$actual_opening_date = to_system_date($project["project_actual_opening_date"]);

$postype = $project["postype_name"];
$possubclass = $project["possubclass_name"];

if($project["productline_subclass_name"])
{
	$posfurniture = $project["product_line_name"] . " / " . $project["productline_subclass_name"];
}
else
{
	$posfurniture = $project["product_line_name"];
}



$project_info = $poslegaltype . " / " . $projectkind . " / " . $postype;

if($possubclass) {
	$project_info .= " / " . $possubclass;
}

$project_info .= " / " . $posfurniture;


//get POS pictures
$files = array();
$sql = "select order_file_id, order_file_path, order_file_type
        from order_files 
        where order_file_category = 11 and order_file_type in (2)
		and order_file_order = " . dbquote($project['project_order']);


$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res)) {
	$files[$row['order_file_id']] =$row['order_file_path'];
}

/********************************************************************
	prepare pdf
*********************************************************************/
global $page_title;
$page_title = "OPENING BRIEF: Project " . $project['project_number'];


require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');
require_once('../include/SetaPDF/Autoload.php');


class MYPDF extends TCPDF
{
	//Page header
	function Header()
	{
		global $page_title;
		//Logo
		$this->Image('../pictures/brand_logo.jpg',10,8,33);
		//arialn bold 15
		$this->SetFont('arialn','B',12);
		//Move to the right
		$this->Cell(80);
		//Title
		$this->Cell(0,33,$page_title,0,0,'R');
		//Line break
		$this->Ln(20);

	}

	//Page footer
	function Footer()
	{
		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		//arialn italic 8
		$this->SetFont('arialn','I',8);
		//Page number
		$this->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$this->PageNo().'/'.$this->getAliasNbPages(),0,0,'R');
	}

	function keepTogether($height) {
		$this->checkPageBreak($height);
	}

}

//Instanciation of inherited class
$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);

$pdf->SetAutoPageBreak(false);

$pdf->SetMargins(10, 23, 10);

$pdf->Open();

$pdf->SetFillColor(244, 244, 244);
$pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(120, 120, 120)));

$pdf->AddFont('arialn');
$pdf->AddFont('arialn', 'B');

$pdf->AddPage();
$new_page = 0;


$pdf->SetFont('arialn','B',8);
$pdf->Cell(8, 5,"Date:",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(14, 5,date("d.m.y"),1, 0, 'L', 0);


$pdf->SetFont('arialn','B',8);
$pdf->Cell(8, 5,"POS:",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(70, 5, $poslocation ,1, 0, 'L', 0);


$pdf->SetFont('arialn','B',8);
$pdf->Cell(12, 5,"Done by:",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(78, 5, $client,1, 0, 'L', 0);
$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('arialn','B',10);
$pdf->Cell(190,7,'POJECT INFORMATION',1, 0, 'L', 1);
$pdf->Ln();

$pdf->SetFont('arialn','B',8);
$pdf->Cell(18, 5,"Project:",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(172, 5,$project_info,1, 0, 'L', 0);
$pdf->Ln();

$pdf->SetFont('arialn','B',8);
$pdf->Cell(18, 5,"Opening Date:",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(16, 5,$actual_opening_date,1, 0, 'L', 0);

$pdf->SetFont('arialn','B',8);
$pdf->Cell(19, 5,"Square Meters:",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(137, 5,$square_meters,1, 0, 'L', 0);
$pdf->Ln();

$pdf->SetFont('arialn','B',8);
$pdf->Cell(18, 5,"Address:",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(172, 5,$address,1, 0, 'L', 0);
$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('arialn','B',10);
$pdf->Cell(190,7,'IMAGES AFTER OPENING',1, 0, 'L', 1);
$pdf->Ln();


$y = $pdf->GetY()+2;
$box_height= 62;
$image_height = 60;
$y_image = $pdf->GetY()+3;

$i=0;

foreach($files as $id=>$source_file) {
	
	$source_file = str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'] . $source_file);

	if(file_exists($source_file))
	{
		$y = $pdf->SetY($y);
		$pdf->Cell(190,$box_height,'',1, 0, 'L', 0);
		$pdf->Ln();
		
		$pdf->Image($source_file,11,$y_image, '', $image_height, "", "", "", true, 300, "", false, false, 0, false, false,true);
		
		$i++;
		$y = $pdf->GetY()+2;
		$y_image = $y+1;
		
		if($i==3 or $i==7 or $i==11 or $i==15 or $i==19) {
			$pdf->AddPage('P','A4');
			$y = $pdf->GetY()+2;
			$y_image = $y+1;
		}
	}
}





$file_name = "Project_opening_briefing_" . $project["order_number"];
$pdf->Output($file_name);



?>