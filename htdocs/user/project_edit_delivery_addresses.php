<?php
/********************************************************************

    project_edit_delivery_addresses.php

    Edit item delivery addresses

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-11-19
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006.07.05
    Version:        1.1.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_edit_delivery_addresses_in_projects");

register_param("pid");
set_referer("project_edit_delivery_address.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));

// read project and order details
$order = get_order($project["project_order"]);

// get company's address
$client_address = get_address($project["order_client_address"]);

// create sql for order items
$sql_order_items = "select order_item_id, order_item_text, order_item_quantity, ".
                   "    order_item_po_number, order_item_system_price, ".
                   "    if(item_code <>'', item_code, item_type_name) as item_shortcut, ".
                   "    item_category_name, " .
                   "    concat_ws(', ', order_address_company, order_address_place) as delivery_address " .
                   "from order_items ".
                   "left join items on order_item_item = item_id ".
                   "left join item_categories on item_category_id = item_category ".
                   "left join order_addresses on order_address_order_item = order_item_id ".
                   "left join item_types on order_item_type = item_type_id";


/********************************************************************
    Create Form
*********************************************************************/ 
$form = new Form("projects", "project");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid",$project["project_order"]);

require_once "include/project_head_small.php";

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Create List for Catalog Items
*********************************************************************/ 
$list = new ListView($sql_order_items);

$list->set_title("Items up to Delivery");
$list->set_entity("order_item");
$list->set_filter("order_item_type <= " . ITEM_TYPE_SPECIAL . 
                  "   and order_item_order = " . $project["project_order"] .
                  "   and order_address_type = 2 ");

$list->set_order("item_category_name, item_code");
$list->set_group("item_category_name");

$link="project_edit_delivery_address.php?pid=" . param("pid");

$list->add_column("item_shortcut", "Item Code", $link);
$list->add_column("order_item_text", "Name");
$list->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
$list->add_column("delivery_address", "Ship to", "", "", "", COLUMN_NO_WRAP);


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();

/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Edit Ship To Information");
$form->render();
$list->render();
require_once "include/project_footer_logistic_state.php";
$page->footer();


?>