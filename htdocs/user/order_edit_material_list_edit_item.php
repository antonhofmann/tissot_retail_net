<?php
/********************************************************************

    order_edit_material_list_edit_item.php

    Edit item position in material list.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-12
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-12-19
    Version:        1.2.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "include/order_functions.php";

check_access("can_edit_list_of_materials_in_orders");

register_param("oid");
register_param("id");
set_referer("order_edit_material_list.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$order = get_order(param("oid"));



// get order_item's information
$order_item = get_order_item(id());

// get replacement information
$replacement_information = get_items_replacement_information(id());


// get company's address
$client_address = get_address($order["order_client_address"]);

// get system currency
$system_currency = get_system_currency_fields();

// get orders's currency
$order_currency = get_order_currency(param("oid"));


$currencies = array();
$currencies[1] = "CHF";
$currencies[2] = $order_currency["symbol"];

// create sql for the supplier listbox
if ($order_item["type"] == ITEM_TYPE_STANDARD)
{
    $sql_suppliers = "select address_id, address_company ".
                     "from items ".
                     "left join suppliers on item_id = supplier_item ".
                     "left join addresses on supplier_address = address_id ".
                     "where address_active = 1 and item_id = " . $order_item["item"] . " ".
                     "order by address_company";;
}
else
{
    $sql_suppliers = "select address_id, address_company ".
                     "from addresses ".
                     "where address_type = 2 and address_active = 1 " . " ".
                     "order by address_company";;
}


// get supplier's currency
$supplier_currency_symbol = "";
if($order_item["item"]  and $order_item["supplier"])
{
    $supplier_currency_symbol = get_item_currency_symbol($order_item["supplier"], $order_item["item"]);
}
if (!$supplier_currency_symbol)
{
    $supplier_currency_symbol = get_currency_symbol($order_item["supplier_currency"]);
}
$supplier_currencies = get_suplier_currencies($order_item["order_item_supplier_address"]);

if(param("order_item_supplier_address"))
{
	$supplier_currency = get_address_currency(param("order_item_supplier_address"));
	$supplier_currency_symbol = $supplier_currency["symbol"];
	$supplier_currencies = get_suplier_currencies(param("order_item_supplier_address"));
}

$sql_supplier_currencies = "select currency_id, currency_symbol " . 
                           " from currencies " . 
						   " where currency_id in ( " . implode(',', $supplier_currencies) . ") " . 
						   " order by currency_symbol";





$sql_forwarders = "select DISTINCT address_id, address_company ".
                  "from addresses ".
				  " left join users on user_address = address_id " .
                  "where (address_type = 3 or address_id = " . $order["order_client_address"] . ") " . 
				  " and address_active = 1 ".
				  " and user_active = 1 " .
                  "order by address_company";
    


// create sql for the transportation type listbox
$sql_transportation_types = "select transportation_type_id, transportation_type_name ".
                           "from transportation_types ".
                           "order by transportation_type_name";


// get pick up date
$item_pick_up_date = get_last_order_item_date(id(), "PICK");



$sql_units = "select unit_id, unit_name " . 
             " from units " . 
			 " order by unit_name";

$sql_packaging_types = "select packaging_type_id, packaging_type_name " . 
             " from packaging_types " . 
			 " order by packaging_type_name";



$sql_item_categories = "select item_category_id, item_category_name	" . 
                       " from item_categories " . 
					   " where item_category_active = 1 or item_category_id = " . dbquote($order_item["category"]) .
					   " order by item_category_name";

/********************************************************************
    build form
*********************************************************************/
$form = new Form("order_items", "item from list of materials");

$form->add_section("Order");
$form->add_hidden("oid",$order["order_id"]);
$form->add_hidden("order_item_item", $order_item["item"]);
$form->add_hidden("order_item_id", id());
$form->add_hidden("order_item_type", $order_item["type"]);
$form->add_hidden("order_item_currency", $order_item["supplier_currency"]);

require_once "include/order_head_small.php";


if(is_array($replacement_information))
{
	$form->add_section("Replacement Information");
	$form->add_label("original_item", "Original Item", 0, $replacement_information["order_item_text"]);
	$form->add_label("original_quantity", "Original Quantity", 0, $replacement_information["order_item_quantity"]);
	$form->add_label("reason", "Replacement Reason", 0, $replacement_information["order_item_replacement_reason_text"]);
	$form->add_label("warranty", "Warranty", 0, $replacement_information["order_item_warranty_type_text"]);
	$form->add_label("furniture_payed_by", "Furniture paid by", 0, $replacement_information["furniture_payed_by"]);
	$form->add_label("freight_payed_by", "Freight paid by", 0, $replacement_information["freight_payed_by"]);
	$form->add_label("remarks", "Remark", 0, $replacement_information["order_item_replacement_remarks"]);



}

if ($order_item["type"] == ITEM_TYPE_STANDARD or $order_item["type"] == ITEM_TYPE_SPECIAL)
{
	$form->add_section("Supplier");
	if ($order["order_budget_is_locked"] == 1)
	{
		$form->add_lookup("order_item_supplier_address", "Supplier", "addresses", "address_company", $flags = 0, $order_item["supplier"]);
	}
	elseif(!$item_pick_up_date["last_date"])
	{
		$form->add_list("order_item_supplier_address", "Supplier*", $sql_suppliers, NOTNULL | SUBMIT, $order_item["supplier"]);
	}
	else
	{
		$form->add_lookup("order_item_supplier_address", "Supplier", "addresses", "address_company", $flags = 0, $order_item["supplier"]);
	}
}


$form->add_section("Item Information");
$form->add_label("order_item_code", "Code", 0, $order_item["code"]);

if ($order_item["type"] == ITEM_TYPE_SPECIAL)
{
	$form->add_list("order_item_category", "Item Category*", $sql_item_categories, NOTNULL, $order_item["category"]);
}
else
{
	$form->add_hidden("order_item_category", $order_item["category"]);
}

$form->add_multiline("order_item_text", "Description*", 4, NOTNULL);
if ($order_item["type"] == ITEM_TYPE_STANDARD or $order_item["type"] == ITEM_TYPE_SPECIAL)
{
    if ($order["order_budget_is_locked"] == 1)
    {
        $form->add_section("Volumes and Weight");
		$form->add_lookup("order_item_unit_id", "Unit", "units", "unit_name", 0, $order_item["order_item_unit_id"]);
		$form->add_label("order_item_width", "Width in cm", 0);
		$form->add_label("order_item_height", "Height in cm", 0);
		$form->add_label("order_item_length", "length in cm", 0);
		$form->add_label("order_item_gross_weight", "Gross Weight in kg", 0);
		$form->add_lookup("order_item_packaging_type_id", "Packagin", "packaging_types", "packaging_type_name", 0, $order_item["order_item_packaging_type_id"]);
		$form->add_checkbox("order_item_stackable", "", $order_item["order_item_stackable"], 0, "Stackable");
    }
    else
    {
        $form->add_section("Volumes and Weight");
		$form->add_list("order_item_unit_id", "Unit",$sql_units, 0, $order_item["order_item_unit_id"]);
		$form->add_edit("order_item_width", "Width in cm", 0, "" , TYPE_DECIMAL, 10, 2);
		$form->add_edit("order_item_height", "Height in cm", 0, "" , TYPE_DECIMAL, 10, 2);
		$form->add_edit("order_item_length", "length in cm", 0, "" , TYPE_DECIMAL, 10, 2);
		$form->add_edit("order_item_gross_weight", "Gross Weight in kg", 0, "" , TYPE_DECIMAL, 10, 2);
		$form->add_list("order_item_packaging_type_id", "Packaging",$sql_packaging_types, 0, $order_item["order_item_packaging_type_id"]);
		$form->add_checkbox("order_item_stackable", "", $order_item["order_item_stackable"], 0, "Stackable");
    }
}

$form->add_section("Quantity and Price");

if ($order["order_budget_is_locked"] == 1)
{
	$form->add_label("order_item_quantity", "Quantity");
}
else
{
	$form->add_edit("order_item_quantity", "Quantity*", NOTNULL);
}


if ($order_item["type"] == ITEM_TYPE_STANDARD 
or $order_item["type"] == ITEM_TYPE_SERVICES)
{
	
	if(is_array($replacement_information) and $order["order_budget_is_locked"] != 1)
	{

		$form->add_edit("order_item_client_price", "Client's Price in " . $order_currency["symbol"] . "*");;

		$form->add_label("order_item_system_price", "Selling Price in " . $system_currency["symbol"] . "*");
		$form->add_label("order_item_supplier_system_price", "Supplier's Price in " . $system_currency["symbol"], 0, $order_item["supplier_system_price"]);

		$form->add_label("order_item_supplier_price", "Supplier's Price in " . $supplier_currency_symbol);
		$form->add_hidden("is_replacement", 1);
	}
	else
	{
		$form->add_label("order_item_system_price", "Selling Price in " . $system_currency["symbol"] . "*");
		$form->add_label("order_item_client_price", "Client's Price in " . $order_currency["symbol"] . "*");

		$form->add_label("order_item_supplier_system_price", "Supplier's Price in " . $system_currency["symbol"], 0, $order_item["supplier_system_price"]);

		$form->add_label("order_item_supplier_price", "Supplier's Price in " . $supplier_currency_symbol);
		$form->add_hidden("is_replacement", 0);
	}
}
else if ($order_item["type"] == ITEM_TYPE_SPECIAL)
{
	if ($order["order_budget_is_locked"] == 1)
	{
		$form->add_label("order_item_supplier_price", "Supplier's Price in " . $supplier_currency_symbol);
		$form->add_hidden("suppliers_currency", $order_item["supplier_currency"]);
		$form->add_hidden("order_item_supplier_exchange_rate", $order_item["supplier_exchange_rate"]);
		$form->add_label("exchange_rate", "Exchange Rate", 0, $order_item["supplier_exchange_rate"]);
	}
	else
	{

		$form->add_list("suppliers_currency", "Currency*",$sql_supplier_currencies, NOTNULL, $order_item["supplier_currency"]);
		$form->add_edit("order_item_supplier_exchange_rate", "Exchange Rate*", NOTNULL, $order_item["supplier_exchange_rate"]);
		$form->add_edit("order_item_supplier_price", "Supplier's Price");
	}

	$form->add_label("order_item_supplier_system_price", "Supplier's Price in " . $system_currency["symbol"], 0, $order_item["supplier_system_price"]);

	$form->add_hidden("order_item_system_price");
	$form->add_label("order_item_client_price", "Client's Price in " . $order_currency["symbol"]);
}
elseif($order_item["type"] == ITEM_TYPE_COST_ESTIMATION)
{
	
	$form->add_list("currency", "Currency*",$currencies, NOTNULL | SUBMIT, 1);
	$form->add_edit("order_item_system_price", "Price*");
}


if ($order_item["type"] == ITEM_TYPE_STANDARD or $order_item["type"] == ITEM_TYPE_SPECIAL)
{
    // accounting informatin

    $form->add_section("Accounting Information");
    $form->add_edit("order_item_cost_unit_number", "Cost Unit Number");
    $form->add_edit("order_item_po_number", "P.O. Number");
	$form->add_hidden("order_item_old_po_number", $order_item["po_number"]);
	$form->add_checkbox("change_all_ponumbers", "apply change of P.O. number to all items of the same supplier having identical P.O. numbers");

    // Order date

    $form->add_section("Ordersate");
	if($order_item["order_item_ordered"] != NULL 
		and $order_item["order_item_ordered"] != '0000-00-00'
	    and ($order_item["ready_for_pickup"] == NULL
			 or $order_item["ready_for_pickup"] == '0000-00-00')
	   )
	{
		$form->add_checkbox("delete_order_date", "delete order date (allows to order the item again)");
		$form->add_checkbox("change_all_suppliers", "apply deletion to all items of the same supplier");

		
	}
	else
	{
		$form->add_hidden("delete_order_date", 0);
		$form->add_hidden("change_all_suppliers", 0);
	}



    //$form->add_label("order_item_production_time", "Production Time");
    //$form->add_label("order_item_supplier_item_code", "Supplier's Item Code");
    //$form->add_label("order_item_offer_number", "Supplier's Offer Number");
    
	$form->add_checkbox("order_item_no_offer_required", "does not require a supplier's offer", $order_item["no_offer_required"], 0, "Options");
	
	if ($order_item["type"] == ITEM_TYPE_SPECIAL)
	{
		$form->add_checkbox("order_item_only_quantity_proposal", "does only need a supplier's quantity proposal", $order_item["only_quantity_proposal"]);
	}
	else
	{
		$form->add_hidden("order_item_only_quantity_proposal",0);
	}
	

    // Traffic

    $form->add_section("Traffic Information");

    $form->add_hidden("old_order_item_forwarder_address", $order_item["forwarder"]);


    $form->add_list("order_item_transportation", "Transportation Type", $sql_transportation_types, 0, $order_item["transportation"]);
    $form->add_list("order_item_forwarder_address", "Forwarder", $sql_forwarders, 0, $order_item["forwarder"]);
	//$form->add_checkbox("change_all_forwarders", "apply change of transportation type and forwarder to all items of the same forwarder");
    
	
	$choices = array();
	$choices[1] = "apply change of transportation type and forwarder to all items of the same forwarder";
	$choices[2] = "apply change of transportation type and forwarder to all items of the same supplier";
	$captions = array();
	$captions[1] = '';
	$captions[2] = '';

	$form->add_radiolist("change_all_forwarders", $captions, $choices, VERTICAL,0);
	
	$form->add_edit("order_item_staff_for_discharge", "Staff for Discharge", 0, "", TYPE_INT, 1);

    $form->add_hidden("order_item_forwarder_address_old_value",  $order_item["forwarder"]);

    
    /*
    if(!$item_pick_up_date["last_date"])
    {

        $form->add_list("order_item_transportation", "Transportation Type", $sql_transportation_types, 0, $order_item["transportation"]);
        $form->add_list("order_item_forwarder_address", "Forwarder", $sql_forwarders, 0, $order_item["forwarder"]);
        $form->add_edit("order_item_staff_for_discharge", "Staff for Discharge", 0, "", TYPE_INT, 1);
    }
    else
    {
        $form->add_lookup("order_item_transportation", "Transportation Type", "transportation_types", "transportation_type_name", 0, $order_item["transportation"]);
        $form->add_lookup("order_item_forwarder_address", "Forwarder", "addresses", "address_company", 0, $order_item["forwarder"]);
        $form->add_label("order_item_staff_for_discharge", "Staff for Discharge");
    }
    */


    $form->add_section("Budgeting Information");
    $form->add_checkbox("order_item_not_in_budget", "does not appear in budget");

}

$form->add_button("save_data", "Save");

if ($order["order_budget_is_locked"] == 1)
{
    $form->add_button(FORM_BUTTON_BACK, "Back");
}
else
{
    $form->add_button(FORM_BUTTON_DELETE, "Delete");
    $form->add_button(FORM_BUTTON_BACK, "Back");
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("save_data"))
{
    // validate form
    
    if ($form->validate())
    {
         $link = "order_edit_material_list.php?oid=" . param("oid");
         project_edit_order_item_save($form);

         
         //update order_item supplier's currency
		if ($order_item["type"] == ITEM_TYPE_SPECIAL)
		{
			$supplier_currency = get_currency($form->value("suppliers_currency"));
			$sql = "update order_items set " .
				   "order_item_supplier_currency = " . $form->value("suppliers_currency") . ", " .
				   "order_item_supplier_exchange_rate = " . $form->value("order_item_supplier_exchange_rate") . 
				   " where order_item_id = " . id();

			 $res = mysql_query($sql) or dberror($sql);


			 //update clinent's price
			 
			 
			 if($supplier_currency["factor"] > 0)
			 {
				$system_price = $form->value("order_item_supplier_price") * $form->value("order_item_supplier_exchange_rate") / $supplier_currency["factor"];
			 }
			 else
			 {
				 $system_price = 0;
			 }

			 if($order_currency["exchange_rate"] > 0)
			 {
				$client_price = $system_price / $order_currency["exchange_rate"] * $order_currency["factor"];
			 }
			 else
			 {
				 $client_price = 0;
			 }
			
			
			$sql = "update order_items set " .
				   "order_item_system_price = " . $system_price . ", " .
				   "order_item_client_price = " . $client_price . 
				   " where order_item_id = " . id();

			 $res = mysql_query($sql) or dberror($sql);


			
		}
		elseif($order_item["type"] == ITEM_TYPE_COST_ESTIMATION)
		{
			 if($form->value("currency") !=1)
			 {
				  $loc = 1;
				  $client_price = $form->value("order_item_system_price");
				  $system_price = $form->value("order_item_system_price") * $order_currency["exchange_rate"] / $order_currency["factor"];
			 }
			 else
			 {
			     $loc = 0;
				 $system_price = $form->value("order_item_system_price");
				 $client_price = $form->value("order_item_system_price") / $order_currency["exchange_rate"] * $order_currency["factor"];
			 }
			
			 $sql = "update order_items set " .
				    "order_item_system_price = " . $system_price . ", " .
				    "order_item_client_price = " . $client_price . ", " . 
				    "order_item_price_entered_in_loc = " . $loc .
				    " where order_item_id = " . id();

			 $res = mysql_query($sql) or dberror($sql);
		}
		
		 if ($order_item["type"] == ITEM_TYPE_STANDARD or $order_item["type"] == ITEM_TYPE_SPECIAL)
         {
             
			 //check if P.O. number has changed
			 if($form->value("order_item_po_number") and $form->value("order_item_po_number") != $form->value("order_item_old_po_number"))
			 {
				 $supplier = $form->value("order_item_supplier_address");
				 if($supplier and $form->value("change_all_ponumbers") == 1) //apply change to all items of the same supplier
				 {
					$sql = "update order_items set " .
						   " order_item_po_number = " . dbquote($form->value("order_item_po_number")) . 
						   " where order_item_order = " . dbquote(param('oid')) . 
						   " and order_item_supplier_address = " . dbquote($supplier) . 
					       " and order_item_po_number = " . dbquote($form->value("order_item_old_po_number"));
					$res = mysql_query($sql) or dberror($sql);
				 }
			 }



			 //delete order date
			 if($form->value("delete_order_date") == 1)
			 {
				if($form->value("change_all_suppliers") == 1) // items with same supplier
				{

					$sql = "select order_item_id, if(order_item_item <>'', item_code, item_type_name) as item_shortcut " .
						   "from order_items " . 
						   "left join items on item_id = order_item_item " .
						   "left join item_types on order_item_type = item_type_id ".
						   "where order_item_order = " . dbquote(param('oid')) . 
						   " and order_item_supplier_address = " . dbquote($form->value('order_item_supplier_address')) . 
						   " and (order_item_ready_for_pickup is null or order_item_ready_for_pickup = '0000-00-00')";
				
					$res = mysql_query($sql) or dberror($sql);

					while ($row = mysql_fetch_assoc($res))
					{
						$sql_u = "update order_items set " .
							   "order_item_ordered = NULL " .
							   " where order_item_id = " . $row["order_item_id"];

						$result = mysql_query($sql_u) or dberror($sql_u);
					}
				 }
				 else
				 {
					 $sql = "update order_items set " .
							   "order_item_ordered = NULL " .
							   " where order_item_id = " . id();

						 $res = mysql_query($sql) or dberror($sql);

				 }
			}
			 
			 //check if forwarder has changed
             $f = $form->value("order_item_forwarder_address");
             $fo = $form->value("order_item_forwarder_address_old_value");

             if($fo and $f and $f != $fo)
             {
                
				
				if($form->value("change_all_forwarders") > 0) //apply change to all items of the same forwarder or supplier
				{
						
						if($form->value("change_all_forwarders") == 1) //items with same forwarder
						{
							$sql = "select order_item_id, if(order_item_item <>'', item_code, item_type_name) as item_shortcut " .
							   "from order_items " . 
							   "left join items on item_id = order_item_item " .
							   "left join item_types on order_item_type = item_type_id ".
							   "where order_item_order = " .dbquote(param('oid')) . 
							   " and order_item_forwarder_address = " . dbquote($fo);
						}
						elseif($form->value("change_all_forwarders") == 2) // items with same supplier
						{
							$sql = "select order_item_id, if(order_item_item <>'', item_code, item_type_name) as item_shortcut " .
							   "from order_items " . 
							   "left join items on item_id = order_item_item " .
							   "left join item_types on order_item_type = item_type_id ".
							   "where order_item_order = " .dbquote(param('oid')) . 
							   " and order_item_supplier_address = " . dbquote($form->value('order_item_supplier_address'));
						}

						

						$res = mysql_query($sql) or dberror($sql);

						while ($row = mysql_fetch_assoc($res))
						{
							$order_items_concerned[$row["order_item_id"]] = $row["item_shortcut"];
							$sql = "delete from dates " .
								   "where date_order_item = " . $row["order_item_id"] . 
								   "  and (date_type = 2 or date_type = 3) ";

							$res_d = mysql_query($sql) or dberror($sql);

							$sql = "update order_items set " .
								   "order_item_transportation = " . dbquote($form->value("order_item_transportation")) . ", " .
								   "order_item_forwarder_address = " . dbquote($f) . ", " . 
								   "order_item_pickup = Null, " .
								   "order_item_pickup_changes = 0, " .
								   "order_item_expected_arrival = Null, " .
								   "order_item_expected_arrival_changes = 0 " .
								   "where order_item_id = " . $row["order_item_id"];

							$res_u = mysql_query($sql) or dberror($sql);

						}
					}
					else
				    {
						$sql = "delete from dates " .
							   "where date_order_item = " . id() . 
							   "  and (date_type = 2 or date_type = 3) ";

						$res = mysql_query($sql) or dberror($sql);

						$sql = "update order_items set " .
							   "order_item_pickup = Null, " .
							   "order_item_pickup_changes = 0, " .
							   "order_item_expected_arrival = Null, " .
							   "order_item_expected_arrival_changes = 0 " .
							   "where order_item_id = " . id();
					}

                $res = mysql_query($sql) or dberror($sql);

                //get forwarde's names
                $f_a = get_address($f);
                $fo_a = get_address($fo);

                // send an email to supplier, if forwarder has changed

                $mail = new Mail();
                $num_mails = 0;

                $sql = "select order_id, order_number, order_item_po_number, ".
                       "if(order_item_item <>'', item_code, item_type_name) as item_shortcut, ".
                       "users.user_email as recepient, users.user_email_cc as cc," .
                       "users.user_email_deputy as deputy, " .
                       "users.user_address as address_id, " .
                       "users1.user_email as sender, concat(users1.user_name, ' ', users1.user_firstname) as user_fullname ".
                       "from orders ".
                       "left join order_items on order_item_order = order_id " .
                       "left join items on order_item_item = item_id ".
                       "left join item_types on order_item_type = item_type_id ".
                       "left join addresses on address_id = order_item_supplier_address ".
                       "left join users on users.user_address = address_id ".
                       "left join users as users1 on " . user_id() . "= users1.user_id ".
                       "where users.user_active = 1 and order_item_id = " . id();

                $mail_sent_to = "\nMail sent to: ";

                $res = mysql_query($sql) or dberror($sql);

                while ($row = mysql_fetch_assoc($res) and $row["recepient"])
                {
                    $sender_email = $row["sender"];
                    $sender_name =  $row["user_fullname"];
                    $subject = MAIL_SUBJECT_PREFIX . ": Forwarder has changed - Order " . $row["order_number"];
                    $mail->set_subject($subject);

                    $mail->set_sender($sender_email, $sender_name);

                    $mail->add_recipient($row["recepient"]);

                    if($row["cc"])
                    {
                        $mail->add_cc($row["cc"]);
                    }
                    if($row["deputy"])
                    {
                        $mail->add_cc($row["deputy"]);
                    }
                    
                    $mail_sent_to = $mail_sent_to . "\n" . $row["recepient"];

                    $bodytext0 = "The forwarder has changed for the following order\n" .
                                 "Order: " . $row["order_number"] . "    Item:  " . $row["item_shortcut"] . "\n".
                                 "from " . $fo_a["company"] . " to " . $f_a["company"];

                    $num_mails++;
                                                
                }


                $link ="order_view_traffic_data.php?oid=" . $order["order_id"];
                $bodytext = $bodytext0 . "\n\nclick below to have direct access to the order:\n";

                $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
                
                $mail->add_text($bodytext);
                if($num_mails > 0)
                {
                    if($senmail_activated == true)
					{
						$mail->send();
					}


                    append_mail($order["order_id"], "" , user_id(), $bodytext0 . " " . $mail_sent_to , "", 2);
                }


                // send an email to old forwarder, if forwarder has changed
				// first check if forwarder is still in the project
				// only send mail if forwarder still is in the project

				$sql = "select count(order_item_id) as num_recs " . 
					   " from order_items " . 
					   " where order_item_forwarder_address = " . dbquote($fo) . 
					   " and order_item_order = " . dbquote($project["project_order"]);

				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);

				if($row["num_recs"] > 0) {

					$mail = new Mail();
					$num_mails = 0;

					$sql = "select order_id, order_number, order_item_po_number, ".
						   "if(order_item_item <>'', item_code, item_type_name) as item_shortcut, ".
						   "users.user_email as recepient, users.user_email_cc as cc," .
						   "users.user_email_deputy as deputy, " .
						   "users.user_address as address_id, " .
						   "users1.user_email as sender, concat(users1.user_name, ' ', users1.user_firstname) as user_fullname ".
						   "from orders ".
						   "left join order_items on order_item_order = order_id " .
						   "left join items on order_item_item = item_id ".
						   "left join item_types on order_item_type = item_type_id ".
						   "left join addresses on address_id = " . $fo .
						   " left join users on users.user_address = address_id ".
						   "left join users as users1 on " . user_id() . "= users1.user_id ".
						   "where users.user_active = 1 and order_item_id = " . id();

					$mail_sent_to = "\nMail sent to: ";

					$res = mysql_query($sql) or dberror($sql);

					while ($row = mysql_fetch_assoc($res) and $row["recepient"])
					{
						$sender_email = $row["sender"];
						$sender_name =  $row["user_fullname"];
						$subject = MAIL_SUBJECT_PREFIX . ": Forwarder has changed - Order " . $row["order_number"];
						$mail->set_subject($subject);

						$mail->set_sender($sender_email, $sender_name);

						$mail->add_recipient($row["recepient"]);

						if($row["cc"])
						{
							$mail->add_cc($row["cc"]);
						}
						if($row["deputy"])
						{
							$mail->add_cc($row["deputy"]);
						}
						
						$mail_sent_to = $mail_sent_to . "\n" . $row["recepient"];

						$bodytext0 = "Please note that the transportation situation has changed for the following order\n".
									 "Order: " . $row["order_number"] . "\n" .
									 "The following Item will be forwarded by another company:  " . $row["item_shortcut"];

						$num_mails++;
													
					}


					$link ="order_view_traffic_data.php?oid=" . $order["order_id"];
					$bodytext = $bodytext0 . "\n\nclick below to have direct access to the order:\n";

					$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
					
					$mail->add_text($bodytext);
					if($num_mails > 0)
					{
						if($senmail_activated == true)
						{
							$mail->send();
						}


						append_mail($order["order_id"], "" , user_id(), $bodytext0 . " " . $mail_sent_to , "", 2);
					}
				}
             }

         }

		 $link = "order_edit_material_list.php?oid=" . param("oid");

         redirect($link);
    }
}


   
/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("List of Materials: Edit Item Position");
$form->render();
$page->footer();

?>