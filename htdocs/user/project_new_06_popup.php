<?php
/********************************************************************

    project_new_06_popup.php

    Creation of a new project of type popup step 06.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-06-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-06-06
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/



require_once "../include/frame.php";

check_access("can_create_new_projects");

require_once "include/get_functions.php";
require_once "include/save_functions.php";

$user = get_user(user_id());



//save project and send notifications
if((isset($_SESSION["new_project_step_1"])
   and isset($_SESSION["new_project_step_3a"])
   and isset($_SESSION["new_project_step_5"]))
   )
{
	$latest_project = array();
	$pos_id = $_SESSION["new_project_step_1"]["posaddress_id"];
	
	$franchisee_id = 0;
	$client_id = 0;
	$pos_data = array();
	$sql = "select *  
			from posaddresses 
			where posaddress_id = " . dbquote($pos_id);
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$client_id = $row["posaddress_client_id"];
		$franchisee_id = $row["posaddress_franchisee_id"];

		$pos_data = $row;
	}
	
	//get latest project data for the indicated POS
	$sql =	"select * " .  
			"from posorders " .
			"left join orders on order_id = posorder_order " .
		    "left join projects on project_order = order_id " .
		    "left join project_costs on project_cost_order = order_id " .
		    "left join posaddresses on posaddress_id = posorder_posaddress " . 
			"where (order_actual_order_state_code <> '900' or order_actual_order_state_code is null) " . 
			" and posorder_type = 1 and posorder_posaddress = " . dbquote($pos_id) .
			" and posorder_opening_date is not null and posorder_opening_date <> '0000-00-00' " .  
			" order by posorder_year DESC, posorder_opening_date DESC";

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{

		$latest_project = $row;
		$client_id = $row["posaddress_client_id"];
		$franchisee_id = $row["posaddress_franchisee_id"];
	}
	
	if($client_id > 0 and $franchisee_id > 0) {
		//order
		$order_fields = array();
		$order_values = array();
		
		$delivery_address_fields = array();
		$delivery_address_values = array();

		$project_fields = array();
		$project_values = array();

		$project_item_fields = array();
		$project_item_values = array();
		
		// create project number project_kind

		$project_number = project_create_project_number($user["address"], $_SESSION["new_project_step_1"]["project_postype"], $_SESSION["new_project_step_1"]["project_kind"]);

		// get currency details
		$currency = get_address_currency($user["address"]);
		$system_currency = get_system_currency_fields();

		//get standard roles
		$standardroles = get_project_standard_roles($user["address"], $_SESSION["new_project_step_1"]["project_postype"], $_SESSION["new_project_step_1"]["project_cost_type"]);
		
		if($_SESSION["new_project_step_1"]["project_kind"] == 4 or $_SESSION["new_project_step_1"]["project_kind"] == 5) 
		{
			$standard_design_supervisor = 0;
			$standard_retail_coordinator = 0;
			$standard_retail_operator = 0;
			$standard_cms_approver = 0;
			$standard_local_retail_coordinator = user_id();
		}
		elseif($_SESSION["new_project_step_1"]["project_kind"] == 8)
		{
			$standard_design_supervisor = $standardroles['dsup'];
			$standard_cms_approver = $standardroles['cms_approver'];
			$standard_local_retail_coordinator = user_id();


			$standardroles = get_project_standard_roles_popup();
			$standard_retail_coordinator = $standardroles['rtco'];
			$standard_retail_operator = $standardroles['rtop'];
		} 
		else
		{
			$standard_design_supervisor = $standardroles['dsup'];
			$standard_retail_coordinator = $standardroles['rtco'];
			$standard_retail_operator = $standardroles['rtop'];
			$standard_cms_approver = $standardroles['cms_approver'];
			$standard_local_retail_coordinator = user_id();
		}

		// insert record into table orders

		$order_fields[] = "order_number";
		$order_values[] = dbquote($project_number);

		$order_fields[] = "order_date";
		$order_values[] = "current_timestamp";

		$order_fields[] = "order_type";
		$order_values[] = 1;

		if($standard_retail_operator)
		{
			$order_fields[] = "order_retail_operator";
			$order_values[] = $standard_retail_operator;
		}

		$order_fields[] = "order_client_address";
		$order_values[] = $user["address"];


		$order_fields[] = "order_client_currency";
		$order_values[] = $currency["id"];

		$order_fields[] = "order_system_currency";
		$order_values[] = $system_currency["id"];

		$order_fields[] = "order_client_exchange_rate";
		$order_values[] = $currency["exchange_rate"];

		$order_fields[] = "order_system_exchange_rate";
		$order_values[] =  $system_currency["exchange_rate"];

		$order_fields[] = "order_user";
		$order_values[] = user_id();
		
		
		$order_fields[] = "order_preferred_transportation_arranged";
		$order_values[] = trim($_SESSION["new_project_step_5"]["preferred_transportation_arranged"]) == "" ? "null" : dbquote($_SESSION["new_project_step_5"]["preferred_transportation_arranged"]);

		$order_fields[] = "order_preferred_transportation_mode";
		$order_values[] = trim($_SESSION["new_project_step_5"]["preferred_transportation_mode"]) == "" ? "null" : dbquote($_SESSION["new_project_step_5"]["preferred_transportation_mode"]);
		
		
		if(count($latest_project) > 0) {
			$order_fields[] = "order_voltage";
			$order_values[] = dbquote($latest_project["order_voltage"]);
		}

			
		
		// franchisee address
		$order_fields[] = "order_franchisee_address_id";
		$order_values[] = dbquote($franchisee_id);
		
		$sql_a = "select * from addresses where address_id = " . dbquote($franchisee_id);
		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			$order_fields[] = "order_franchisee_address_company";
			$order_values[] = dbquote($row_a["address_company"]);

			$order_fields[] = "order_franchisee_address_company2";
			$order_values[] = dbquote($row_a["address_company2"]);

			$order_fields[] = "order_franchisee_address_address";
			$order_values[] = dbquote($row_a["address_address"]);

			$order_fields[] = "order_franchisee_address_street";
			$order_values[] = dbquote($row_a["address_address_street"]);

			$order_fields[] = "order_franchisee_address_streetnumber";
			$order_values[] = dbquote($row_a["address_address_streetnumber"]);

			$order_fields[] = "order_franchisee_address_address2";
			$order_values[] = dbquote($row_a["address_address2"]);

			$order_fields[] = "order_franchisee_address_zip";
			$order_values[] = dbquote($row_a["address_zip"]);

			$order_fields[] = "order_franchisee_address_place";
			$order_values[] = dbquote(trim($row_a["address_place"]));

			$order_fields[] = "order_franchisee_address_country";
			$order_values[] = dbquote($row_a["address_country"]);

			$order_fields[] = "order_franchisee_address_phone";
			$order_values[] = dbquote($row_a["address_phone"]);

			$order_fields[] = "order_franchisee_address_phone_country";
			$order_values[] = dbquote($row_a["address_phone_country"]);

			$order_fields[] = "order_franchisee_address_phone_area";
			$order_values[] = dbquote($row_a["address_phone_area"]);

			$order_fields[] = "order_franchisee_address_phone_number";
			$order_values[] = dbquote($row_a["address_phone_number"]);


			$order_fields[] = "order_franchisee_address_mobile_phone";
			$order_values[] = dbquote($row_a["address_mobile_phone"]);

			$order_fields[] = "order_franchisee_address_mobile_phone_country";
			$order_values[] = dbquote($row_a["address_mobile_phone_country"]);

			$order_fields[] = "order_franchisee_address_mobile_phone_area";
			$order_values[] = dbquote($row_a["address_mobile_phone_area"]);

			$order_fields[] = "order_franchisee_address_mobile_phone_number";
			$order_values[] = dbquote($row_a["address_mobile_phone_number"]);

			$order_fields[] = "order_franchisee_address_email";
			$order_values[] = dbquote($row_a["address_email"]);

			$order_fields[] = "order_franchisee_address_contact";
			$order_values[] = dbquote($row_a["address_contact_name"]);
		}
		
		$sql_a = "select * from addresses where address_id = " . $client_id;
		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			if($row_a["address_invoice_recipient"] > 0)
			{
				$sql_a = "select * from addresses where address_id = " . $row_a["address_invoice_recipient"];
			}
		}
		else
		{
			$sql_a = "select * from posaddresses " .
					 "left join addresses on address_id = posaddress_client_id " . 
					 "where posaddress_id = " . dbquote($pos_id);
		}
		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			$order_fields[] = "order_billing_address_company";
			$order_values[] = dbquote($row_a["address_company"]);

			$order_fields[] = "order_billing_address_company2";
			$order_values[] = dbquote($row_a["address_company2"]);

			$order_fields[] = "order_billing_address_address";
			$order_values[] = dbquote($row_a["address_address"]);

			$order_fields[] = "order_billing_address_street";
			$order_values[] = dbquote($row_a["address_street"]);

			$order_fields[] = "order_billing_address_streetnumber";
			$order_values[] = dbquote($row_a["address_streetnumber"]);

			$order_fields[] = "order_billing_address_address2";
			$order_values[] = dbquote($row_a["address_address2"]);

			$order_fields[] = "order_billing_address_zip";
			$order_values[] = dbquote($row_a["address_zip"]);

			
			$order_fields[] = "order_billing_address_place_id";
			$order_values[] = dbquote(trim($row_a["address_place_id"]));

			$order_fields[] = "order_billing_address_place";
			$order_values[] = dbquote(trim($row_a["address_place"]));

			$order_fields[] = "order_billing_address_country";
			$order_values[] = dbquote($row_a["address_country"]);

			$order_fields[] = "order_billing_address_phone";
			$order_values[] = dbquote($row_a["address_phone"]);

			$order_fields[] = "order_billing_address_phone_country";
			$order_values[] = dbquote($row_a["address_phone_country"]);

			$order_fields[] = "order_billing_address_phone_area";
			$order_values[] = dbquote($row_a["address_phone_area"]);

			$order_fields[] = "order_billing_address_phone_number";
			$order_values[] = dbquote($row_a["address_phone_number"]);

			$order_fields[] = "order_billing_address_mobile_phone";
			$order_values[] = dbquote($row_a["address_mobile_phone"]);

			$order_fields[] = "order_billing_address_mobile_phone_country";
			$order_values[] = dbquote($row_a["address_mobile_phone_country"]);

			$order_fields[] = "order_billing_address_mobile_phone_area";
			$order_values[] = dbquote($row_a["address_mobile_phone_area"]);

			$order_fields[] = "order_billing_address_mobile_phone_number";
			$order_values[] = dbquote($row_a["address_mobile_phone_number"]);

			$order_fields[] = "order_billing_address_email";
			$order_values[] = dbquote($row_a["address_email"]);

			
			$sql_a = "select concat(user_name, ' ', user_firstname) as username from users where user_id = " . user_id();
			$res_a = mysql_query($sql_a) or dberror($sql_a);
			if ($row_a = mysql_fetch_assoc($res_a))
			{
				$order_fields[] = "order_billing_address_contact";
				$order_values[] = dbquote($row_a["username"]);
			}
		}


		// POS address
		$order_fields[] = "order_shop_address_company";
		$order_values[] = trim($_SESSION["new_project_step_1"]["shop_address_company"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_company"]);

		$order_fields[] = "order_shop_address_company2";
		$order_values[] = trim($_SESSION["new_project_step_1"]["shop_address_company2"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_company2"]);

		$order_fields[] = "order_shop_address_address";
		$order_values[] = trim($_SESSION["new_project_step_1"]["shop_address_address"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_address"]);

		if(array_key_exists('shop_street', $_SESSION["new_project_step_1"]))
		{
			$order_fields[] = "order_shop_address_street";
			$order_values[] = trim($_SESSION["new_project_step_1"]["shop_street"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_street"]);

			$order_fields[] = "order_shop_address_streetnumber";
			$order_values[] = trim($_SESSION["new_project_step_1"]["shop_streetnumber"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_streetnumber"]);
		}


		$order_fields[] = "order_shop_address_address2";
		$order_values[] = trim($_SESSION["new_project_step_1"]["shop_address_address2"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_address2"]);

		$order_fields[] = "order_shop_address_zip";
		$order_values[] = trim($_SESSION["new_project_step_1"]["shop_address_zip"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_zip"]);

		$order_fields[] = "order_shop_address_place";
		$order_values[] = trim($_SESSION["new_project_step_1"]["shop_address_place"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_place"]);

		$order_fields[] = "order_shop_address_country";
		$order_values[] = trim($_SESSION["new_project_step_1"]["shop_address_country"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_country"]);

		
		if(array_key_exists('shop_phone_country', $_SESSION["new_project_step_1"]))
		{
			if(trim($_SESSION["new_project_step_1"]["shop_phone_number"])) {
				$order_fields[] = "order_shop_address_phone";
				$order_values[] = trim($_SESSION["new_project_step_1"]["shop_address_phone"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_phone"]);


				$order_fields[] = "order_shop_address_phone_country";
				$order_values[] = trim($_SESSION["new_project_step_1"]["shop_phone_country"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_phone_country"]);

				$order_fields[] = "order_shop_address_phone_area";
				$order_values[] = trim($_SESSION["new_project_step_1"]["shop_phone_area"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_phone_area"]);

				$order_fields[] = "order_shop_address_phone_number";
				$order_values[] = trim($_SESSION["new_project_step_1"]["shop_phone_number"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_phone_number"]);
			}
		}

		

		if(array_key_exists('shop_mobile_phone_country', $_SESSION["new_project_step_1"]))
		{
			if(trim($_SESSION["new_project_step_1"]["shop_mobile_phone_number"])) {
				$order_fields[] = "order_shop_address_mobile_phone";
				$order_values[] = trim($_SESSION["new_project_step_1"]["shop_address_mobile_phone"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_mobile_phone"]);

				$order_fields[] = "order_shop_address_mobile_phone_country";
				$order_values[] = trim($_SESSION["new_project_step_1"]["shop_mobile_phone_country"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_mobile_phone_country"]);

				$order_fields[] = "order_shop_address_mobile_phone_area";
				$order_values[] = trim($_SESSION["new_project_step_1"]["shop_mobile_phone_area"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_mobile_phone_area"]);

				$order_fields[] = "order_shop_address_mobile_phone_number";
				$order_values[] = trim($_SESSION["new_project_step_1"]["shop_mobile_phone_number"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_mobile_phone_number"]);
			}
		}

		$order_fields[] = "order_shop_address_email";
		$order_values[] = trim($_SESSION["new_project_step_1"]["shop_address_email"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_email"]);

		//$order_fields[] = "order_shop_address_contact";
		//$order_values[] = trim($_SESSION["new_project_step_1"]["shop_address_contact_name"]) == "" ? "null" : dbquote($_SESSION["new_project_step_1"]["shop_address_contact_name"]);

		$order_fields[] = "date_created";
		$order_values[] = "current_timestamp";

		$order_fields[] = "date_modified";
		$order_values[] = "current_timestamp";

		$order_fields[] = "user_created";
		$order_values[] = dbquote(user_login());

		$order_fields[] = "user_modified";
		$order_values[] = dbquote(user_login());

		$sql = "insert into orders (" . join(", ", $order_fields) . ") values (" . join(", ", $order_values) . ")";
		mysql_query($sql) or dberror($sql);

		$order_id= mysql_insert_id();

		append_order_state($order_id, "100", 1, 1);
		save_project_development_status($order_id, "100");
		
		/*
		if($standard_retail_coordinator > 0)
		{
			append_order_state($order_id, "110", 1, 1);
			save_project_development_status($order_id, "110");
		}
		if($standard_retail_operator > 0)
		{
			append_order_state($order_id, "120", 1, 1);
			save_project_development_status($order_id, "120");
		}
		*/

		add_standard_budget_positions(ITEM_TYPE_EXCLUSION, $order_id, $_SESSION["new_project_step_1"]["project_cost_type"]);
		add_standard_budget_positions(ITEM_TYPE_NOTIFICATION, $order_id, $_SESSION["new_project_step_1"]["project_cost_type"]);

				
		// insert record into table projects
		//convert budget into CHF
		$approximate_budget = trim($_SESSION["new_project_step_3a"]["approximate_budget"]);
		
		if($_SESSION["new_project_step_3a"]["currency_factor"] > 0)
		{
			$approximate_budget = $approximate_budget * $_SESSION["new_project_step_3a"]["currency_exchange_rate"] / $_SESSION["new_project_step_3a"]["currency_factor"];
		}
				
		$project_fields[] = "project_number";
		$project_values[] = dbquote($project_number);

		if($standard_design_supervisor)
		{
			$project_fields[] = "project_design_supervisor";
			$project_values[] = $standard_design_supervisor;
		}
		
		if($standard_retail_coordinator)
		{
			$project_fields[] = "project_retail_coordinator";
			$project_values[] = $standard_retail_coordinator;
		}
		
		if($standard_cms_approver)
		{
			$project_fields[] = "project_cms_approver";
			$project_values[] = $standard_cms_approver;
		}

		if($standard_local_retail_coordinator)
		{
			$project_fields[] = "project_local_retail_coordinator";
			$project_values[] = $standard_local_retail_coordinator;
		}

		$project_fields[] = "project_postype";
		$project_values[] = trim($_SESSION["new_project_step_1"]["project_postype"]) == "" ? "0" : dbquote($_SESSION["new_project_step_1"]["project_postype"]);

		$project_fields[] = "project_pos_subclass";
		$project_values[] = dbquote($pos_data["posaddress_store_subclass"]);

		$project_fields[] = "project_projectkind";
		$project_values[] = trim($_SESSION["new_project_step_1"]["project_kind"]) == "" ? "0" : dbquote($_SESSION["new_project_step_1"]["project_kind"]);

		$project_fields[] = "project_order";
		$project_values[] = $order_id;

		
		if(count($latest_project) > 0) {
			$project_fields[] = "project_location_type";
			$project_values[] = dbquote($latest_project["project_location_type"]);

			$project_fields[] = "project_location";
			$project_values[] = dbquote($latest_project["project_location"]);
		}
		
		$project_fields[] = "project_approximate_budget";
		$project_values[] =  $approximate_budget == "" ? "null" : dbquote($approximate_budget);

		$project_fields[] = "project_planned_opening_date";
		$project_values[] = dbquote(from_system_date($_SESSION["new_project_step_3a"]["project_planned_opening_date"]), true);

		$project_fields[] = "project_planned_closing_date";
		$project_values[] = dbquote(from_system_date($_SESSION["new_project_step_3a"]["project_planned_closing_date"]), true);
		
		$project_fields[] = "project_popup_name";
		$project_values[] = dbquote($_SESSION["new_project_step_3a"]["project_popup_name"], true);

		if(count($latest_project) > 0) {
			$project_fields[] = "project_watches_displayed";
			$project_values[] = dbquote($latest_project["project_watches_displayed"]);

			$project_fields[] = "project_watches_stored";
			$project_values[] = dbquote($latest_project["project_watches_stored"]);

			$project_fields[] = "project_bijoux_displayed";
			$project_values[] = dbquote($latest_project["project_bijoux_displayed"]);

			$project_fields[] = "project_bijoux_stored";
			$project_values[] = dbquote($latest_project["project_bijoux_stored"]);

			$project_fields[] = "project_tissot_shop_around";
			$project_values[] = dbquote($latest_project["project_tissot_shop_around"]);
			
			$project_fields[] = "project_comments";
			$project_values[] = trim($_SESSION["new_project_step_5"]["comments"]) == "" ? "null" : dbquote($_SESSION["new_project_step_5"]["comments"]);
		}
		
		$project_fields[] = "project_floor";
		$project_values[] = dbquote($pos_data["posaddress_store_floor"]);


		$project_fields[] = "project_distribution_channel";
		$project_values[] = dbquote($pos_data["posaddress_distribution_channel"]);




		$project_fields[] = "date_created";
		$project_values[] = "current_timestamp";

		$project_fields[] = "date_modified";
		$project_values[] = "current_timestamp";
					
		$project_fields[] = "user_created";
		$project_values[] = dbquote($_SESSION["user_login"]);

		$project_fields[] = "user_modified";
		$project_values[] = dbquote($_SESSION["user_login"]);


		$sql = "insert into projects (" . join(", ", $project_fields) . ") values (" . join(", ", $project_values) . ")";
		mysql_query($sql) or dberror($sql);
		
		$project_id= mysql_insert_id();

		$_REQUEST["id"] = mysql_insert_id();
		
		
		if(count($latest_project) > 0) {
			$sql_a = "select * from project_items " . 
					 " where project_item_project = " . dbquote($latest_project["project_id"]);
			
			// insert records into table project_items
			$res_a = mysql_query($sql_a) or dberror($sql_a);
			if ($row_a = mysql_fetch_assoc($res_a))
			{
				
				$project_item_fields[0] = "project_item_project";
				$project_item_values[0] = $row_a["project_item_project"];

				$project_item_fields[1] = "project_item_item";
				$project_item_values[1] = $row_a["project_item_item"];
				
				$project_item_fields[2] = "date_created";
				$project_item_values[2] = "current_timestamp";

				$project_item_fields[3] = "date_modified";
				$project_item_values[3] = "current_timestamp";

						
				$project_item_fields[4] = "user_created";
				$project_item_values[4] = dbquote(user_login());

				$project_item_fields[5] = "user_modified";
				$project_item_values[5] = dbquote(user_login());
				
				$sql = "insert into project_items (" . join(", ", $project_item_fields) . ") values (" . join(", ", $project_item_values) . ")";
				mysql_query($sql) or dberror($sql);
			}
		}
		
		
		//create cost monitoring sheet
		$fields = array();
		$values = array();

		$fields[] = "project_cost_order";
		$values[] = $order_id;

		
		$fields[] = "project_cost_sqms";
		$values[] = dbquote($pos_data["posaddress_store_retailarea"]);

		$fields[] = "project_cost_original_sqms";
		$values[] = dbquote($pos_data["posaddress_store_retailarea"]);

		//$fields[] = "project_cost_grosssqms";
		//$values[] = dbquote($pos_data["posaddress_store_grosssurface"]);

		$fields[] = "project_cost_totalsqms";
		$values[] = dbquote($pos_data["posaddress_store_totalsurface"]);

		$fields[] = "project_cost_backofficesqms";
		$values[] = dbquote($pos_data["posaddress_store_backoffice"]);

		$fields[] = "project_cost_numfloors";
		$values[] = dbquote($pos_data["posaddress_store_numfloors"]);

		$fields[] = "project_cost_floorsurface1";
		$values[] = dbquote($pos_data["posaddress_store_floorsurface1"]);

		$fields[] = "project_cost_floorsurface2";
		$values[] = dbquote($pos_data["posaddress_store_floorsurface2"]);

		$fields[] = "project_cost_floorsurface3";
		$values[] = dbquote($pos_data["posaddress_store_floorsurface3"]);
		

		$fields[] = "project_cost_type";
		$values[] = trim($_SESSION["new_project_step_1"]["project_cost_type"]) == "''" ? "" : dbquote($_SESSION["new_project_step_1"]["project_cost_type"]);
		
		$fields[] = "date_created";
		$values[] = dbquote(date("Y-m-d"));

		$fields[] = "date_modified";
		$values[] = dbquote(date("Y-m-d"));

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());

		$sql = "insert into project_costs (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
		mysql_query($sql) or dberror($sql);


		//Create Project Milestones
			
		$sql = "select * from milestones " .
			   "where milestone_active = 1 " .
			   "order by milestone_code ";

		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			$sql_m = "select count(project_milestone_id) as num_recs from project_milestones " .
					 "where project_milestone_project = " . $project_id . 
					 "   and project_milestone_milestone = " . $row["milestone_id"];
			
			
			$res_m = mysql_query($sql_m) or dberror($sql_m);
			$row_m = mysql_fetch_assoc($res_m);

			if($row_m["num_recs"] == 0)
			{
				$fields = array();
				$values = array();

				$fields[] = "project_milestone_project";
				$values[] = $project_id;

				$fields[] = "project_milestone_milestone";
				$values[] = $row["milestone_id"];

				if($row["milestone_in_new_project"] == 1)
				{
					$fields[] = "project_milestone_date";
					$values[] = dbquote(date("Y-m-d"));

					$fields[] = "project_milestone_date_comment";
					$values[] = dbquote("On project submission");
				}
				
				$fields[] = "date_created";
				$values[] = dbquote(date("Y-m-d"));

				$fields[] = "date_modified";
				$values[] = dbquote(date("Y-m-d"));

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$fields[] = "user_modified";
				$values[] = dbquote(user_login());

				if($row["milestone_is_inr03_milestone"] == 1 and $_SESSION["new_project_step_1"]["project_cost_type"] == 1)
				{
					$sql = "insert into project_milestones (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);
				}
				elseif($row["milestone_is_inr03_milestone"] == 0)
				{
					$sql = "insert into project_milestones (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					mysql_query($sql) or dberror($sql);
				}
			}
		}

		
		//insert record into pos orders or posorderpipeline
		$fields = array();
		$values = array();

		$fields[] = "posorder_posaddress";
		$values[] = dbquote($_SESSION["new_project_step_1"]["posaddress_id"]);

		$fields[] = "posorder_order";
		$values[] = dbquote($order_id);

		$fields[] = "posorder_type";
		$values[] = 1;

		$fields[] = "posorder_ordernumber";
		$values[] = dbquote($project_number);

		$year_of_order = substr($project_number,0,2);
		if($year_of_order > 30 and $year_of_order <= 99)
		{
			$year_of_order = "19" . $year_of_order;
		}
		else
		{
			$year_of_order = substr($year_of_order, 0, 1) . '00' . substr($year_of_order, 1, 1);
		}

		
		if(substr($project_number,3,1) == '.')
		{
			$year_of_order = substr($project_number,0,1) . '0' . substr($project_number,1,2);
		}

		$fields[] = "posorder_year";
		$values[] = dbquote($year_of_order);

		$fields[] = "posorder_postype";
		$values[] = dbquote($pos_data["posaddress_store_postype"]);

		$fields[] = "posorder_subclass";
		$values[] = dbquote($pos_data["posaddress_store_subclass"]);

		$fields[] = "posorder_project_kind";
		$values[] = dbquote($_SESSION["new_project_step_1"]["project_kind"]);

		$fields[] = "posorder_legal_type";
		$values[] = dbquote($pos_data["posaddress_ownertype"]);

		$fields[] = "posorder_floor";
		$values[] = dbquote($pos_data["posaddress_store_floor"]);

		$fields[] = "posorder_neighbour_left";
		$values[] = dbquote($pos_data["posaddress_neighbour_left"]);

		$fields[] = "posorder_neighbour_left_business_type";
		$values[] = dbquote($pos_data["posaddress_neighbour_left_business_type"]);

		$fields[] = "posorder_neighbour_left_price_range";
		$values[] = dbquote($pos_data["posaddress_neighbour_left_price_range"]);

		$fields[] = "posorder_neighbour_right";
		$values[] = dbquote($pos_data["posaddress_neighbour_right"]);

		$fields[] = "posorder_neighbour_right_business_type";
		$values[] = dbquote($pos_data["posaddress_neighbour_right_business_type"]);

		$fields[] = "posorder_neighbour_right_price_range";
		$values[] = dbquote($pos_data["posaddress_neighbour_right_price_range"]);


		$fields[] = "posorder_neighbour_acrleft";
		$values[] = dbquote($pos_data["posaddress_neighbour_acrleft"]);

		$fields[] = "posorder_neighbour_acrleft_business_type";
		$values[] = dbquote($pos_data["posaddress_neighbour_acrleft_business_type"]);

		$fields[] = "posorder_neighbour_acrleft_price_range";
		$values[] = dbquote($pos_data["posaddress_neighbour_acrleft_price_range"]);


		$fields[] = "posorder_neighbour_acrright";
		$values[] = dbquote($pos_data["posaddress_neighbour_acrright"]);

		$fields[] = "posorder_neighbour_acrright_business_type";
		$values[] = dbquote($pos_data["posaddress_neighbour_acrright_business_type"]);

		$fields[] = "posorder_neighbour_acrright_price_range";
		$values[] = dbquote($pos_data["posaddress_neighbour_acrright_price_range"]);

		$fields[] = "posorder_neighbour_brands";
		$values[] = dbquote($pos_data["posaddress_neighbour_brands"]);

		$fields[] = "date_created";
		$values[] = dbquote(date("Y-m-d"));

		$fields[] = "date_modified";
		$values[] = dbquote(date("Y-m-d"));

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());
		
		
		$sql = "insert into posorders (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
		mysql_query($sql) or dberror($sql);
		

		
		//update posaddress
		$fields = array();

		$value = dbquote($_SESSION["new_project_step_1"]["shop_address_company"]);
		$fields[] = "posaddress_name = " . $value;

		$value = dbquote($_SESSION["new_project_step_1"]["shop_address_company2"]);
		$fields[] = "posaddress_name2 = " . $value;

		$value = dbquote($_SESSION["new_project_step_1"]["shop_address_address"]);
		$fields[] = "posaddress_address = " . $value;

		if(array_key_exists('shop_street', $_SESSION["new_project_step_1"]))
		{
			$value = dbquote($_SESSION["new_project_step_1"]["shop_street"]);
			$fields[] = "posaddress_street = " . $value;

			$value = dbquote($_SESSION["new_project_step_1"]["shop_streetnumber"]);
			$fields[] = "posaddress_street_number = " . trim($value);
		}

		$value = dbquote($_SESSION["new_project_step_1"]["shop_address_address2"]);
		$fields[] = "posaddress_address2 = " . $value;

		$value = dbquote($_SESSION["new_project_step_1"]["shop_address_zip"]);
		$fields[] = "posaddress_zip = " . $value;

		
		

		if(array_key_exists('shop_phone_country', $_SESSION["new_project_step_1"]))
		{
			if(trim($_SESSION["new_project_step_1"]["shop_phone_number"])) {
				$value = dbquote($_SESSION["new_project_step_1"]["shop_address_phone"]);
				$fields[] = "posaddress_phone = " . $value;
				$value = dbquote($_SESSION["new_project_step_1"]["shop_phone_country"]);
				$fields[] = "posaddress_phone_country = " . $value;

				$value = dbquote($_SESSION["new_project_step_1"]["shop_phone_area"]);
				$fields[] = "posaddress_phone_area = " . $value;

				$value = dbquote($_SESSION["new_project_step_1"]["shop_phone_number"]);
				$fields[] = "posaddress_phone_number = " . $value;
			}
		}

		

		if(array_key_exists('shop_mobile_phone_country', $_SESSION["new_project_step_1"]))
		{
			if(trim($_SESSION["new_project_step_1"]["shop_mobile_phone_number"])) {
				$value = dbquote($_SESSION["new_project_step_1"]["shop_address_mobile_phone"]);
				$fields[] = "posaddress_mobile_phone = " . $value;

				$value = dbquote($_SESSION["new_project_step_1"]["shop_mobile_phone_country"]);
				$fields[] = "posaddress_mobile_phone_country = " . $value;

				$value = dbquote($_SESSION["new_project_step_1"]["shop_mobile_phone_area"]);
				$fields[] = "posaddress_mobile_phone_area = " . $value;

				$value = dbquote($_SESSION["new_project_step_1"]["shop_mobile_phone_number"]);
				$fields[] = "posaddress_mobile_phone_number = " . $value;
			}
		}

		$value = dbquote($_SESSION["new_project_step_1"]["shop_address_email"]);
		$fields[] = "posaddress_email = " . $value;
		
		
		$value = dbquote($_SESSION["new_project_step_1"]["posaddress_google_lat"]);
		$fields[] = "posaddress_google_lat = " . $value;

		$value = dbquote($_SESSION["new_project_step_1"]["posaddress_google_long"]);
		$fields[] = "posaddress_google_long = " . $value;

		$value = dbquote($_SESSION["new_project_step_1"]["posaddress_google_precision"]);
		$fields[] = "posaddress_google_precision = 1";

		$value = dbquote(date("Y-m-d"));
		$fields[] = "date_modified = " . $value;

		$value = dbquote(user_login());
		$fields[] = "user_modified = " . $value;

		
		$sql = "update posaddresses set " . join(", ", $fields) . " where posaddress_id = " . $_SESSION["new_project_step_1"]["posaddress_id"];
		mysql_query($sql) or dberror($sql);

		
		//send mail notification
		project_send_mail_notifications($project_id);
	}

}
else
{
	$link = "project_new_01.php";
	redirect($link);
}

unset($_SESSION["new_project_step_0"]);
unset($_SESSION["new_project_step_1"]);
unset($_SESSION["new_project_step_2"]);
unset($_SESSION["new_project_step_3a"]);
unset($_SESSION["new_project_step_4"]);
unset($_SESSION["new_project_step_5"]);


$page = new Page("projects");

$page->register_action('home', 'Home', "welcome.php");

$page->header();
$page->title("New Project");

echo "<p>", "Your request has been submitted.", "</p>";
echo "<p>", "Go directly to the list of your <a href='projects.php'>projects</a>.", "</p>";
echo "<p>", "Print a PDF of your request: <a href='project_print_project_data.php?pid=" . $project_id . "' target='_blank'>Print</a>.", "</p>";

$page->footer();

?>