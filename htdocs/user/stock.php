<?php
/********************************************************************

    stock.php

    List stock data by suppliers

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-29
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-01-26
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_stock");

register_param("id");
set_referer("stock_items.php");


/********************************************************************
    prepare all data needed
*********************************************************************/
$sql_suppliers = "select address_id, address_company ".
                 "from addresses ".
                 "left join address_types on address_type = address_type_id ";


/********************************************************************
    Create List
*********************************************************************/ 

$list = new ListView($sql_suppliers);

$list->set_entity("stores");
$list->set_order("address_company");

if (has_access("has_access_to_all_stock_data"))
{
    $list->set_filter("address_active = 1 and address_type_code IN ('SUPP', 'WAHO')");
}
else
{
    $user_data = get_user(user_id());
    $list->set_filter("address_id = " . $user_data["address"] . " and address_type_code IN ('SUPP', 'WAHO')");    
}

$list->add_column("address_company", "Code", "stock_items.php");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->process();


$page = new Page("stock");

$page->register_action('home', 'Home', "welcome.php");
if (has_access("can_enter_global_order"))
{
    $page->register_action('globalorder', 'Global Orders', "stock_global_orders.php");
}

$page->header();
$page->title("Stock Control Data");

$list->render();
$page->footer();

?>