<?php

if(param("pid") > 0)
{
	$page->add_tab("general", "Items up to Delivery", "project_shipping_details.php?pid=" . param("pid"), $target = "_self", $flags = 0);
	$page->add_tab("volumes", "Weight and Volumes", "project_shipping_volumes.php?pid=" . param("pid"), $target = "_self", $flags = 0);
	$page->add_tab("documents", "Mandatory Shipping Documents", "project_shipping_documents.php?pid=" . param("pid"), $target = "_self", $flags = 0);

	$page->add_tab("client_info", "Client Info", "project_shipping_documents_client_info.php?pid=" . param("pid"), $target = "_self", $flags = 0);

	//$page->add_tab("assign", "Assign Existing Documents", "project_shipping_documents_assign.php?pid=" . param("pid"), $target = "_self", $flags = 0);
}
elseif(param("oid") > 0)
{
	$page->add_tab("general", "Items up to Delivery", "order_shipping_details.php?oid=" . param("oid"), $target = "_self", $flags = 0);
	$page->add_tab("volumes", "Weight and Volumes", "order_shipping_volumes.php?oid=" . param("oid"), $target = "_self", $flags = 0);
	$page->add_tab("documents", "Mandatory Shipping Documents", "order_shipping_documents.php?oid=" . param("oid"), $target = "_self", $flags = 0);
	
	$page->add_tab("client_info", "Client Info", "order_shipping_documents_client_info.php?oid=" . param("oid"), $target = "_self", $flags = 0);

	
	//$page->add_tab("assign", "Assign Existing Documents", "order_shipping_documents_assign.php?oid=" . param("oid"), $target = "_self", $flags = 0);

}
$page->tabs();


?>