<?php
/********************************************************************

    order_edit_request.php

    Edit project's  data as entered by the client.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2003-02-05
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-02-04
    Version:        1.0.3

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "include/order_functions.php";

check_access("can_edit_client_data_in_orders");
set_referer("order_new.php");

/********************************************************************
    prepare all data needed
*********************************************************************/

// Vars
$error = "unspecified";


// read order details
$order = get_order(param("oid"));

// get users' company address
$address = get_address($order["order_client_address"]);

// get orders's currency
$currency = get_order_currency(param("oid"));

// read information from order_addresses
$delivery_address = get_order_address(2, param('oid'));

// create sql for the client listbox
$sql_address = "select address_id, address_company ".
               "from addresses ".
               "where address_type = 1 or address_type = 4 ".
               "order by address_company";

// create sql for the client's contact listbox
if (!param("client_address_id"))
{
    $sql_address_user = "select user_id, concat(user_name, ' ', user_firstname) ".
                        "from users ".
                        "where (user_active = 1 and user_address = ". $order["order_client_address"] . ") " . 
		                " or (user_id = " . $order["order_user"] . " ) " . 
                        " order by user_name";
}
else
{
    $sql_address_user = "select user_id, concat(user_name, ' ', user_firstname) ".
                        "from users ".
                        "where (user_active = 1  and user_address = ". param("client_address_id") . ") ".
                        "order by user_name";
}

// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";

// create sql for the location type listbox
$sql_location_types = "select location_type_id, location_type_name ".
                      "from location_types ".
                      "order by location_type_name";

// create sql for the voltage listbox
$sql_voltages = "select voltage_id, voltage_name ".
                "from voltages";

// create sql for the transportation type listbox
$sql_transportation_modes = "select transportation_type_id, transportation_type_name ".
                            "from transportation_types ".
                            "where transportation_type_visible = 1 " .
							" and transportation_type_code = 'mode' " . 
                            "order by transportation_type_name";


$sql_transportation_arranged = "select transportation_type_id, transportation_type_name ".
								"from transportation_types ".
								"where transportation_type_visible = 1 " .
								" and transportation_type_code = 'arranged' " . 
								"order by transportation_type_name";


$order_state_name = get_actual_order_state_name($order["order_actual_order_state_code"], 2);

//get addresses from pos index
$sql_posaddresses = "select posaddress_id, concat(posaddress_place, ', ', posaddress_name) as posaddress " .
                    "from posaddresses " . 
					"where (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') " . 
					" and posaddress_client_id = '" . $order["order_client_address"] . "' " .
					" order by posaddress";


//create sql for places
if(param("invoice_recipient")) {
	
	$invoice_address = get_address(param("invoice_recipient"));
	$sql_billing_places = "select place_id, place_name from places " . 
		          "where place_country = " .  dbquote($invoice_address["country"]) . 
		          " order by place_name";

}
elseif(param("billing_address_country")) {
	$sql_billing_places = "select place_id, place_name from places " . 
		          "where place_country = " .  param("billing_address_country") . 
		          " order by place_name";
}
elseif($order["order_billing_address_country"]) {
	$sql_billing_places = "select place_id, place_name from places " . 
		          "where place_country = " .  $order["order_billing_address_country"] . 
		          " order by place_name";
}

if(param("deliveryg_address_country")) {
	$sql_delivery_places = "select place_id, place_name from places " . 
		          "where place_country = " .  param("deliveryg_address_country") . 
		          " order by place_name";
}
elseif($delivery_address['country']) {
	$sql_delivery_places = "select place_id, place_name from places " . 
		          "where place_country = " .  $delivery_address['country'] . 
		          " order by place_name";
}
else {
	$sql_delivery_places = "select place_id, place_name from places " . 
		          "where place_country =  " . $address["country"] .
		          " order by place_name";
}


//get province
$billing_address_province_name = "";
if(param("billing_address_place_id"))
{
	$billing_address_province_name = get_province_name(param("billing_address_place_id"));
}
elseif($order["order_billing_address_place_id"])
{
	$billing_address_province_name = get_province_name($order["order_billing_address_place_id"]);
}


$delivery_address_province_name = "";
if(param("delivery_address_place_id"))
{
	$delivery_address_province_name = get_province_name(param("delivery_address_place_id"));
}
elseif($delivery_address["place_id"])
{
	$delivery_address_province_name = get_province_name($delivery_address["place_id"]);
}


//get invoice_addresses
$invoice_addresses = array();

$sql_inv = "select invoice_address_id, concat(invoice_address_company, ', ' ,place_name, ', ', country_name) as company " .
    "from invoice_addresses " .
	"left join places on place_id = invoice_address_place_id " . 
	"left join countries on country_id = invoice_address_country_id " . 
	"where invoice_address_active = 1 and invoice_address_address_id = " . $order["order_client_address"] . 
    " order by invoice_address_company";

$res = mysql_query($sql_inv) or dberror($sql_inv);
while ($row = mysql_fetch_assoc($res))
{
	$invoice_addresses[$row["invoice_address_id"]] = $row["company"];
}

// create array for the delivery address listbox

$delivery_addressesp['01'] = 'POS Location';
$delivery_addresses = get_delivery_addresses($order["order_client_address"]);


$delivery_addresses = $delivery_addressesp + $delivery_addresses;



/********************************************************************
    build form
*********************************************************************/
$form = new Form("orders", "order");

$form->add_section("Order");
$form->add_hidden("oid", param("oid"));
$form->add_label("order_number", "Order Number*", 0, $order["order_number"]);

if (has_access("can_edit_status_in_orders"))
{
    $sql = "select distinct order_state_code " .
           "from order_states " .
		   " where order_state_code < '890' " .
           "order by order_state_code";
    $form->add_list("status", "Status", $sql, NOTNULL, $order["order_actual_order_state_code"]);
}
else
{
    $form->add_hidden("status", $order["order_actual_order_state_code"]);

    $form->add_label("status1", "Status", 0, $order["order_actual_order_state_code"]  . " " . $order_state_name);
}

$form->add_section("Client");
$form->add_list("client_address_id", "Client*", $sql_address, SUBMIT | NOTNULL, $order["order_client_address"]);
$form->add_list("client_address_user_id", "Contact*", $sql_address_user, NOTNULL, $order["order_user"]);

$form->add_section("Bill to");

if($address["invoice_recipient"] > 0)
{
	$sql_invoice_addresses = "select address_id, concat(country_name, ': ', address_company) as company from addresses " .
			 "left join countries on country_id = address_country " .
			 "where address_id = " . $address["id"] . " or address_id = " . $address["invoice_recipient"] . 
			 " order by country_name, address_company";
	$form->add_list("invoice_recipient", "Change Billing Address", $sql_invoice_addresses, SUBMIT);
}
//$form->add_comment("Please indicate the billing address. \nYou can either select an existing address or enter a new address.");
//$form->add_list("billing_address_id", "Bill to", $billing_addresses, SUBMIT);
$form->add_edit("billing_address_company", "Company*", NOTNULL, $order["order_billing_address_company"], TYPE_CHAR);
$form->add_edit("billing_address_company2", "", 0, $order["order_billing_address_company2"], TYPE_CHAR);

$form->add_hidden("billing_address_address", $order["order_billing_address_address"]);
$form->add_multi_edit("bill_to_street", array("order_billing_address_street", "order_billing_address_streetnumber"), "Street/Street number", array(NOTNULL, ''), array($order["order_billing_address_street"], $order["order_billing_address_streetnumber"]), array('', ''), array(200, 6), array(), 0, '', '', array(40, 5), array(), 0, '', '', array(40, 5));


$form->add_edit("billing_address_address2", "Additional Address Info", 0, $order["order_billing_address_address2"], TYPE_CHAR);
$form->add_edit("billing_address_zip", "ZIP", 0, $order["order_billing_address_zip"], TYPE_CHAR, 20);
$form->add_list("billing_address_place_id", "City*", $sql_billing_places, NOTNULL |SUBMIT, $order["order_billing_address_place_id"]);
$form->add_label("billing_address_place", "", 0,$order["order_billing_address_place"]);
$form->add_label("billing_address_province_name", "Province", 0, $billing_address_province_name);
$form->add_list("billing_address_country", "Country*", $sql_countries, SUBMIT | NOTNULL, $order["order_billing_address_country"]);

$form->add_hidden("billing_address_phone", $order["order_billing_address_phone"]);
$form->add_multi_edit("bill_to_phone_number", array("order_billing_address_phone_country", "order_billing_address_phone_area", "order_billing_address_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array($order["order_billing_address_phone_country"], $order["order_billing_address_phone_area"], $order["order_billing_address_phone_number"]), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));


$form->add_hidden("billing_address_mobile_phone", $order["order_billing_address_mobile_phone"]);
$form->add_multi_edit("bill_to_mobile_phone_number", array("order_billing_address_mobile_phone_country", "order_billing_address_mobile_phone_area", "order_billing_address_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array($order["order_billing_address_mobile_phone_country"], $order["order_billing_address_mobile_phone_area"], $order["order_billing_address_mobile_phone_number"]), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));


$form->add_edit("billing_address_email", "Email", 0, $order["order_billing_address_email"], TYPE_CHAR);
$form->add_edit("billing_address_contact", "Contact*", NOTNULL, $order["order_billing_address_contact"], TYPE_CHAR);


if(count($invoice_addresses) > 0)
{
	$form->add_section("Bill to for direct Invoicing");
	$form->add_comment("Please indicate the billing address in case suppliers do send invoices to a different than the above address.");
	$form->add_list("order_direct_invoice_address_id", "Bill to", $invoice_addresses, 0, $order["order_direct_invoice_address_id"]);
}
else
{
	$form->add_hidden("order_direct_invoice_address_id", 0);
}



$form->add_section("POS Location Address");
$form->add_comment("Assignment to POS Index.");
$form->add_list("posaddress_id", "POS Index POS Name", $sql_posaddresses, SUBMIT, $order["posaddress_id"]);

$form->add_comment("Please indicate the POS address in case it is not identical to the delivery address.");
$form->add_edit("shop_address_company", "Company*", 0, $order["order_shop_address_company"], TYPE_CHAR);
$form->add_edit("shop_address_company2", "", 0, $order["order_shop_address_company2"], TYPE_CHAR);

$form->add_hidden("shop_address_address", $order["order_shop_address_address"]);
$form->add_multi_edit("shop_street", array("order_shop_address_street", "order_shop_address_streetnumber"), "Street/Street number", array('', ''), array($order["order_shop_address_street"], $order["order_shop_address_streetnumber"]), array('', ''), array(200, 6), array(), 0, '', '', array(40, 5));


$form->add_edit("shop_address_address2", "Additional Address Info", 0, $order["order_shop_address_address2"], TYPE_CHAR);
$form->add_edit("shop_address_zip", "ZIP", 0, $order["order_shop_address_zip"], TYPE_CHAR, 20);
$form->add_edit("shop_address_place", "City*", 0, $order["order_shop_address_place"], TYPE_CHAR, 20);
$form->add_list("shop_address_country", "Country*", $sql_countries, 0, $order["order_shop_address_country"]);

$form->add_hidden("shop_address_phone", $order["order_shop_address_phone"]);
$form->add_multi_edit("shop_phone_number", array("order_shop_address_phone_country", "order_shop_address_phone_area", "order_shop_address_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array($order["order_shop_address_phone_country"], $order["order_shop_address_phone_area"], $order["order_shop_address_phone_number"]), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

$form->add_hidden("shop_address_mobile_phone", $order["order_shop_address_mobile_phone"]);
$form->add_multi_edit("shop_mobile_phone_number", array("order_shop_address_mobile_phone_country", "order_shop_address_mobile_phone_area", "order_shop_address_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array($order["order_shop_address_mobile_phone_country"], $order["order_shop_address_mobile_phone_area"], $order["order_shop_address_mobile_phone_number"]), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));


$form->add_edit("shop_address_email", "Email", 0, $order["order_shop_address_email"], TYPE_CHAR, 50);


$form->add_section("Ship to");

$tmp_text1="Please indicate the ship to information.\n";
$tmp_text2="You can either select an existing address or enter a new address.";

if (count($delivery_addresses) > 0 )
{
    $form->add_comment($tmp_text1 . $tmp_text2);
    
	$form->add_list("delivery_address_id", "Ship to", $delivery_addresses, SUBMIT);	
	
}
else
{
    $form->add_comment($tmp_text1);
}


$form->add_edit("delivery_address_company", "Company*", NOTNULL, $delivery_address["company"], TYPE_CHAR);
$form->add_edit("delivery_address_company2", "", 0, $delivery_address["company2"], TYPE_CHAR);

$form->add_hidden("delivery_address_address", $delivery_address["address"]);
$form->add_multi_edit("delivery_street", array("order_address_street", "order_address_street_number"), "Street/Street number", array(NOTNULL, ''), array($delivery_address["street"], $delivery_address["streetnumber"]), array('', ''), array(200, 6), array(), 0, '', '', array(40, 5));



$form->add_edit("delivery_address_address2", "Additional Address Info", 0, $delivery_address["address2"], TYPE_CHAR);
$form->add_edit("delivery_address_zip", "ZIP", 0, $delivery_address["zip"], TYPE_CHAR, 20);

$form->add_list("delivery_address_place_id", "City*", $sql_delivery_places, NOTNULL |SUBMIT, $delivery_address['place_id']);
$form->add_label("delivery_address_place", "", 0, $delivery_address['place']);
$form->add_label("delivery_address_province_name", "Province", 0, $delivery_address_province_name);


$form->add_list("delivery_address_country", "Country*", $sql_countries, NOTNULL, $delivery_address["country"]);

$form->add_hidden("delivery_address_phone", $delivery_address["phone"]);
$form->add_multi_edit("delivery_phone_number", array("order_address_phone_country", "order_address_phone_area", "order_address_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array($delivery_address["phone_country"], $delivery_address["phone_area"], $delivery_address["phone_number"]), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

$form->add_hidden("delivery_address_mobile_phone", $delivery_address["mobile_phone"]);
$form->add_multi_edit("delivery_mobile_phone_number", array("order_address_mobile_phone_country", "order_address_mobile_phone_area", "order_address_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array($delivery_address["mobile_phone_country"], $delivery_address["mobile_phone_area"], $delivery_address["mobile_phone_number"]), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

$form->add_edit("delivery_address_email", "Email", 0, $delivery_address["email"], TYPE_CHAR);
$form->add_edit("delivery_address_contact", "Contact*", NOTNULL, $delivery_address["contact"], TYPE_CHAR);


$form->add_section("Location Info");
$form->add_list("voltage", "Voltage Choice*", $sql_voltages, NOTNULL, $order["order_voltage"]);

$form->add_section("Preferences and Traffic Checklist");
$form->add_comment("Please enter the date in the form of dd.mm.yy");
$form->add_edit("preferred_delivery_date", "Preferred Arrival Date*", NOTNULL, to_system_date($order["order_preferred_delivery_date"]), TYPE_DATE, 20);

$form->add_list("preferred_transportation_arranged", "Transportation arranged by*", $sql_transportation_arranged, NOTNULL, $order["order_preferred_transportation_arranged"]);

$form->add_list("preferred_transportation_mode", "Transportation mode*", $sql_transportation_modes, NOTNULL, $order["order_preferred_transportation_mode"]);

//$form->add_radiolist( "packaging_retraction", "Packaging Retraction Desired", array(0 => "no", 1 => "yes"), 0, $order["order_packaging_retraction"]);

$form->add_comment("Please indicate if there is a special approval needed for delivery into ".
                   "a pedestrian area."); 
$form->add_radiolist( "pedestrian_mall_approval", "Pedestrian Area Approval Needed",
    array(0 => "no", 1 => "yes"), 0, $order["order_pedestrian_mall_approval"]);
$form->add_comment("Please indicate if partial delivery is possible or full delivery is required."); 
$form->add_radiolist( "full_delivery", "Full Delivery",
    array(0 => "no", 1 => "yes"), 0, $order["order_full_delivery"]);
$form->add_comment("Please indicate any other circumstances/restrictions concerning delivery and traffic."); 
$form->add_multiline("delivery_comments", "Delivery Comments", 4, 0, $order["order_delivery_comments"]);

$form->add_section("Insurance");
$form->add_radiolist("order_insurance", array(1=>"Insurance by Tissot/Forwarder", 0=>""), array(1=>"covered",0=>"not covered"), VERTICAL, $order["order_insurance"]);

$form->add_section("General Comments");
$form->add_multiline("comments", "Comments", 4, 0, $order["order_special_item_request"]);

$form->add_button("save", "Save Data");



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("client_address_id"))
{
}
elseif($form->button("invoice_recipient"))
{
	if ($form->value("invoice_recipient"))
    {
        $invoice_address = get_address($form->value("invoice_recipient"));

        
		$form->value("billing_address_company", $invoice_address["company"]);
		$form->value("billing_address_company2",  $invoice_address["company2"]);
		$form->value("billing_address_address",  $invoice_address["address"]);

		if(array_key_exists('bill_to_street', $form->items))
		{
			$form->items['bill_to_street']['values'][0] = $invoice_address["street"];
			$form->items['bill_to_street']['values'][1] = $invoice_address["street_number"];
		}

		$form->value("billing_address_address2",  $invoice_address["address2"]);
		$form->value("billing_address_zip",  $invoice_address["zip"]);
		$form->value("billing_address_place",  $invoice_address["place"]);
		$form->value("billing_address_place_id",  $invoice_address["place_id"]);

		$form->value("billing_address_province_name",  $invoice_address["province_name"]);

		$form->value("billing_address_country",  $invoice_address["country"]);
		$form->value("billing_address_phone",  $invoice_address["phone"]);
		
		if(array_key_exists('bill_to_phone', $form->items))
		{
			$form->items['bill_to_phone']['values'][0] = $invoice_address["phone_country"];
			$form->items['bill_to_phone']['values'][1] = $invoice_address["phone_area"];
			$form->items['bill_to_phone']['values'][1] = $invoice_address["phone_number"];
		}

		$form->value("billing_address_mobile_phone",  $invoice_address["mobile_phone"]);

		if(array_key_exists('bill_to_mobile_phone', $form->items))
		{
			$form->items['bill_to_mobile_phone']['values'][0] = $invoice_address["mobile_phone_country"];
			$form->items['bill_to_mobile_phone']['values'][1] = $invoice_address["mobile_phone_area"];
			$form->items['bill_to_mobile_phone']['values'][1] = $invoice_address["mobile_phone_number"];
		}

		$form->value("billing_address_email",  $invoice_address["email"]);
		$form->value("billing_address_contact",  $invoice_address["contact_name"]);


    }
}
else if ($form->button("delivery_address_id"))
{
    // set new delivery address
    $form->value("delivery_address_company", "");
    $form->value("delivery_address_company2",  "");
    $form->value("delivery_address_address",  "");

	if(array_key_exists('delivery_street', $form->items))
	{
		$form->items['delivery_street']['values'][0] = "";
		$form->items['delivery_street']['values'][1] = "";
	}

    $form->value("delivery_address_address2",  "");
    $form->value("delivery_address_zip",  "");
    $form->value("delivery_address_place",  "");
	$form->value("delivery_address_place_id",  0);
    $form->value("delivery_address_country",  0);
    $form->value("delivery_address_phone",  "");

	if(array_key_exists('delivery_phone_number', $form->items))
	{
		$form->items['delivery_phone_number']['values'][0] = "";
		$form->items['delivery_phone_number']['values'][1] = "";
		$form->items['delivery_phone_number']['values'][2] = "";
	}

    $form->value("delivery_address_mobile_phone",  "");

	if(array_key_exists('delivery_mobile_phone_number', $form->items))
	{
		$form->items['delivery_mobile_phone_number']['values'][0] = "";
		$form->items['delivery_mobile_phone_number']['values'][1] = "";
		$form->items['delivery_mobile_phone_number']['values'][2] = "";
	}


    $form->value("delivery_address_email",  "");
    $form->value("delivery_address_contact",  "");

    if ($form->value("delivery_address_id") == '01')
    {
		$sql = "select * from posaddresses " . 
			   "where posaddress_id = " . dbquote($order["posaddress_id"]);

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
				
			$form->value("delivery_address_id", 'pos_address');
			$form->value("delivery_address_company", $row["posaddress_name"]);
			$form->value("delivery_address_company2",  $row["posaddress_name2"]);
			$form->value("delivery_address_address",  $row["posaddress_address"]);
			if(array_key_exists('delivery_street', $form->items))
			{
				$form->items['delivery_street']['values'][0] = $row["posaddress_street"];
				$form->items['delivery_street']['values'][1] = $row["posaddress_street_number"];
			}

			$form->value("delivery_address_address2",  $row["posaddress_address2"]);
			$form->value("delivery_address_zip",  $row["posaddress_zip"]);
			$form->value("delivery_address_place",  $row["posaddress_place"]);
			$form->value("delivery_address_place_id",  $row["posaddress_place_id"]);
			$form->value("delivery_address_country",  $row["posaddress_country"]);
			$form->value("delivery_address_phone",  $row["posaddress_phone"]);

			if(array_key_exists('delivery_phone_number', $form->items))
			{
				$form->items['delivery_phone_number']['values'][0] = $row["posaddress_phone_country"];
				$form->items['delivery_phone_number']['values'][1] = $row["posaddress_phone_area"];
				$form->items['delivery_phone_number']['values'][2] = $row["posaddress_phone_number"];
			}


			$form->value("delivery_address_mobile_phone",  $row["posaddress_mobile_phone"]);

			if(array_key_exists('delivery_mobile_phone_number', $form->items))
			{
				$form->items['delivery_mobile_phone_number']['values'][0] = $row["posaddress_mobile_phone_country"];
				$form->items['delivery_mobile_phone_number']['values'][1] = $row["posaddress_mobile_phone_area"];
				$form->items['delivery_mobile_phone_number']['values'][2] = $row["posaddress_mobile_phone_number"];
			}


			$form->value("delivery_address_email",  $row["posaddress_email"]);
			
			
			$sql = "select province_canton from places left join provinces on province_id = place_province " . 
				   "where place_id = " . dbquote($row["posaddress_place_id"]);
			
			$res = mysql_query($sql) or dberror($sql);

			if ($row = mysql_fetch_assoc($res))
			{
				$form->value("delivery_address_province_name", $row["province_canton"]);
			}
		}
	}
	else
    {

		$sql = "select * from order_addresses where order_address_id = " . $form->value("delivery_address_id");
        $res = mysql_query($sql) or dberror($sql);

        if ($row = mysql_fetch_assoc($res))
        {
            	
			$form->value("delivery_address_company", $row["order_address_company"]);
            $form->value("delivery_address_company2",  $row["order_address_company2"]);
            $form->value("delivery_address_address",  $row["order_address_address"]);

			if(array_key_exists('delivery_street', $form->items))
			{
				$form->items['delivery_street']['values'][0] = $row["order_address_street"];
				$form->items['delivery_street']['values'][1] = $row["order_address_street_number"];
			}

            $form->value("delivery_address_address2",  $row["order_address_address2"]);
            $form->value("delivery_address_zip",  $row["order_address_zip"]);
            $form->value("delivery_address_place",  $row["order_address_place"]);
			$form->value("delivery_address_place_id",  $row["order_address_place_id"]);
            $form->value("delivery_address_country",  $row["order_address_country"]);
            $form->value("delivery_address_phone",  $row["order_address_phone"]);

			if(array_key_exists('delivery_phone_number', $form->items))
			{
				$form->items['delivery_phone_number']['values'][0] = $row["order_address_phone_country"];
				$form->items['delivery_phone_number']['values'][1] = $row["order_address_phone_area"];
				$form->items['delivery_phone_number']['values'][2] = $row["order_address_phone_number"];
			}


            $form->value("delivery_address_mobile_phone",  $row["order_address_mobile_phone"]);

			if(array_key_exists('delivery_mobile_phone_number', $form->items))
			{
				$form->items['delivery_mobile_phone_number']['values'][0] = $row["order_address_mobile_phone_country"];
				$form->items['delivery_mobile_phone_number']['values'][1] = $row["order_address_mobile_phone_area"];
				$form->items['delivery_mobile_phone_number']['values'][2] = $row["order_address_mobile_phone_number"];
			}

            $form->value("delivery_address_email",  $row["order_address_email"]);
            $form->value("delivery_address_contact",  $row["order_address_contact"]);

			$sql = "select province_canton from places left join provinces on province_id = place_province " . 
				   "where place_id = " . dbquote($row["order_address_place_id"]);

			
			$res = mysql_query($sql) or dberror($sql);

			if ($row = mysql_fetch_assoc($res))
			{
				$form->value("delivery_address_province_name", $row["province_canton"]);
			}
        }
    }
}
elseif($form->button("billing_address_place_id"))
{
	$sql= "select place_name, province_canton ".
		  "from places " .
		  "left join provinces on province_id = place_province " .
		  "where place_id = " . dbquote($form->value("billing_address_place_id"));

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$form->value('billing_address_place', $row['place_name']);
		$form->value("billing_address_province_name", $row["province_canton"]);
	}
}
elseif($form->button("delivery_address_place_id"))
{
	$sql= "select place_name, province_canton ".
		  "from places " .
		  "left join provinces on province_id = place_province " .
		  "where place_id = " . dbquote($form->value("delivery_address_place_id"));

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$form->value('delivery_address_place', $row['place_name']);
		$form->value("delivery_address_province_name", $row["province_canton"]);
	}
}
else if($form->button("posaddress_id"))
{
	$sql = "select * from posaddresses ".
		   "where posaddress_id = " . dbquote($form->value("posaddress_id"));

        $res = mysql_query($sql) or dberror($sql);
        
        if ($row = mysql_fetch_assoc($res))
        {

            $form->value("shop_address_company", $row["posaddress_name"]);
            $form->value("shop_address_company2",  $row["posaddress_name2"]);
            $form->value("shop_address_address",  $row["posaddress_address"]);
            
			if(array_key_exists('shop_street', $form->items))
			{
				$form->items['shop_street']['values'][0] = $row["posaddress_street"];
				$form->items['shop_street']['values'][1] = $row["posaddress_street_number"];
			}
			
			$form->value("shop_address_address2",  $row["posaddress_address2"]);
            $form->value("shop_address_zip",  $row["posaddress_zip"]);
            $form->value("shop_address_place",  $row["posaddress_place"]);
            $form->value("shop_address_country",  $row["posaddress_country"]);
            $form->value("shop_address_phone",  $row["posaddress_phone"]);
            
			if(array_key_exists('shop_phone_number', $form->items))
			{
				$form->items['shop_phone_number']['values'][0] = $row["posaddress_phone_country"];
				$form->items['shop_phone_number']['values'][1] = $row["posaddress_phone_area"];
				$form->items['shop_phone_number']['values'][2] = $row["posaddress_phone_number"];
			}
			
			$form->value("shop_address_mobile_phone",  $row["posaddress_mobile_phone"]);

			if(array_key_exists('shop_mobile_phone_number', $form->items))
			{
				$form->items['shop_mobile_phone_number']['values'][0] = $row["posaddress_mobile_phone_country"];
				$form->items['shop_mobile_phone_number']['values'][1] = $row["posaddress_mobile_phone_area"];
				$form->items['shop_mobile_phone_number']['values'][2] = $row["posaddress_mobile_phone_number"];
			}


            $form->value("shop_address_email",  $row["posaddress_email"]);
		}
		else
		{
			$form->value("shop_address_company", "");
            $form->value("shop_address_company2",  "");
            $form->value("shop_address_address",  "");
			if(array_key_exists('shop_street', $form->items))
			{
				$form->items['shop_street']['values'][0] = "";
				$form->items['shop_street']['values'][1] = "";
			}
            $form->value("shop_address_address2",  "");
            $form->value("shop_address_zip",  "");
            $form->value("shop_address_place",  "");
            $form->value("shop_address_country",  "");
            $form->value("shop_address_phone",  "");

			if(array_key_exists('shop_phone_number', $form->items))
			{
				$form->items['shop_phone_number']['values'][0] = "";
				$form->items['shop_phone_number']['values'][1] = "";
				$form->items['shop_phone_number']['values'][2] = "";
			}


            $form->value("shop_address_mobile_phone",  "");

			if(array_key_exists('shop_mobile_phone_number', $form->items))
			{
				$form->items['shop_mobile_phone_number']['values'][0] = "";
				$form->items['shop_mobile_phone_number']['values'][1] = "";
				$form->items['shop_mobile_phone_number']['values'][2] = "";
			}


            $form->value("shop_address_email",  "");
		}


}
else if ($form->button("save"))
{
    if(array_key_exists('bill_to_phone_number', $form->items))
	{
		$form->value("billing_address_phone", $form->unify_multi_edit_field($form->items["bill_to_phone_number"]));
	}
			
	
	if(array_key_exists('bill_to_mobile_phone_number', $form->items))
	{
		$form->value("billing_address_mobile_phone", $form->unify_multi_edit_field($form->items["bill_to_mobile_phone_number"]));

		$form->add_validation("{billing_address_phone} != '' or {billing_address_mobile_phone} != ''", "Invoice address: Please indcate either the phone number or the mobile phone number!");
	}
	
	if($form->value("posaddress_id") > 0) {
		
		$form->value("shop_address_phone", $form->unify_multi_edit_field($form->items["shop_phone_number"]));
		$form->value("shop_address_mobile_phone", $form->unify_multi_edit_field($form->items["shop_mobile_phone_number"]));
		
		//$form->add_validation("{shop_address_phone} != '' or {shop_address_mobile_phone} != ''", "POS address: Please indcate either the phone number or the mobile phone number!");
	}


	if(array_key_exists('delivery_phone_number', $form->items))
	{
		$form->value("delivery_address_phone", $form->unify_multi_edit_field($form->items["delivery_phone_number"]));
	}
	
	if(array_key_exists('delivery_mobile_phone_number', $form->items))
	{
		$form->value("delivery_address_mobile_phone", $form->unify_multi_edit_field($form->items["delivery_mobile_phone_number"]));

		$form->add_validation("{delivery_address_phone} != '' or {delivery_address_mobile_phone} != ''", "Delivery address: Please indcate either the phone number or the mobile phone number!");
	}
	
	
	
	if ($form->validate())
    {
        
		
				
		if(array_key_exists('bill_to_street', $form->items))
		{
			$form->value("billing_address_address", $form->unify_multi_edit_field($form->items["bill_to_street"], get_country_street_number_rule($form->value("billing_address_country"))));
		}
		
		
		
		if(array_key_exists('shop_street', $form->items))
		{
			$form->value("shop_address_address", $form->unify_multi_edit_field($form->items["shop_street"], get_country_street_number_rule($form->value("shop_address_country"))));
		}

		
		
		if(array_key_exists('delivery_street', $form->items))
		{
			$form->value("delivery_address_address", $form->unify_multi_edit_field($form->items["delivery_street"], get_country_street_number_rule($form->value("delivery_address_country"))));
		}

		order_update_client_data($form);
        $form->message("Your changes have been saved.");
    }
}


    
/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("Edit Client Data");
$form->render();
$page->footer();
?>