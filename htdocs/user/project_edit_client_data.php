<?php
/********************************************************************

    project_edit_client_data.php

    Edit project's  data as entered by the client.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-07
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-19
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_edit_client_data_in_projects");


/********************************************************************
    prepare all data needed
*********************************************************************/
$units = array();
$units[1] = "Square Feet (sqf)";
$units[2] = "Square Meters (sqm)";


// Vars
$error = "unspecified";
$design_objectives_listbox_names = array();
$design_objectives_checklist_names = array();

// read project and order details
$project = get_project(param("pid"));

$old_legal_type = $project["project_cost_type"];
$old_project_number = $project["project_number"];
$old_pos_type = $project["project_postype"];
$old_order_state = $project["order_actual_order_state_code"];
$old_development_state = $project["order_development_status"];
$old_logistic_state = $project["order_logistic_status"];

$old_client_user = $project["order_user"];

//check if the cost monitoring sheet exists already
$sql = "select count(project_cost_id) as num_recs " .
       "from project_costs " .
       "where project_cost_order = "  . $project["project_order"]; 
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

if($row["num_recs"] == 0)
{
    $fields = array();
    $values = array();

    $fields[] = "project_cost_order";
    $values[] = $project["project_order"];
    
    $fields[] = "date_created";
    $values[] = dbquote(date("Y-m-d"));

    $fields[] = "date_modified";
    $values[] = dbquote(date("Y-m-d"));

    $fields[] = "user_created";
    $values[] = dbquote(user_login());

    $fields[] = "user_modified";
    $values[] = dbquote(user_login());

    $sql = "insert into project_costs (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

    mysql_query($sql) or dberror($sql);
}

//update order items with predefined cost_groups
$sql = "select order_item_id, order_item_item, item_cost_group, item_costmonitoring_group " . 
       "from order_items " . 
	   "left join items on item_id = order_item_item " .
	   "where order_item_order = " . $project["project_order"] . 
	   "  and order_item_not_in_budget <> 1 " .
	   "  and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION . " or order_item_type >= " . ITEM_TYPE_LOCALCONSTRUCTIONCOST . ") ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$sql_u = "update order_items Set order_item_cost_group =  " . dbquote($row["item_cost_group"]) . " " . 
		     " where (order_item_cost_group is NULL or order_item_cost_group = 0) " .
		     "  and order_item_id = " . $row["order_item_id"];

	mysql_query($sql_u) or dberror($sql_u);

	/* cost monitoring group was removed by Katrin Eckert on January 26th 2016
	$sql_u = "update order_items Set order_items_costmonitoring_group =  " . dbquote($row["item_costmonitoring_group"]) . " " . 
		     " where (order_items_costmonitoring_group is NULL or order_items_costmonitoring_group = 0) " .
		     "  and order_item_id = " . $row["order_item_id"];

	mysql_query($sql_u) or dberror($sql_u);
	*/
}

//get project_projectkind
$sql = "select project_projectkind from projects where project_id = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$old_projectkind = $row["project_projectkind"];
}

// get users' company address
if(param("client_address_id")) {
	$address = get_address(param("client_address_id"));
}
else
{
	$address = get_address($project["order_client_address"]);
}

// get client user data
$user = get_user($project["order_user"]);

//project legal types
if($address["client_type"] == 1) // client is agent, only franchisee as option
{
    $sql_project_cost_types = "select * from project_costtypes where project_costtype_id IN (2,6)";
}
else
{
    $sql_project_cost_types = "select * from project_costtypes where project_costtype_id IN (1, 2, 6)";    
}


$development_state = get_project_development_status($project["project_order"]);

/*
if(has_access("can_see_logistic_status"))
{
	$logistic_state = get_logistic_state($project["project_order"], user_id(), $project["order_actual_order_state_code"]);
	$offer_state = get_offer_state($project["project_order"], user_id(), $project["order_actual_order_state_code"]);
}
*/

$project_state_name = get_project_state_name($project["project_state"]);

// get orders's currency
$currency = get_order_currency($project["project_order"]);

// read design_item_ids from project items
$project_design_objective_item_ids = get_project_design_objective_item_ids(param("pid"));

$delivery_address = get_order_address(2, $project["project_order"]);


// create sql for the client listbox
$sql_address = "select address_id, address_company ".
               "from addresses ".
               "where address_type = 1 or address_type = 4 ".
               "order by address_company";


// create array for the franchisee address listbox
//$franchisee_addresses = get_franchisee_addresses($project["order_client_address"]);

// create array for the billing address listbox
//$billing_addresses = get_billing_addresses($project["order_client_address"]);

// create array for the delivery address listbox
//$delivery_addresses = get_delivery_addresses($project["order_client_address"]);

// create sql for the client's contact listbox
if (!param("client_address_id"))
{
    $sql_address_user = "select user_id, concat(user_name, ' ', user_firstname) ".
                        "from users ".
                        "where user_id = " . $project["order_user"] . " or (user_active = 1 and user_address = ". $project["order_client_address"] . ") ".
                        "order by user_name";
}
else
{
    $sql_address_user = "select user_id, concat(user_name, ' ', user_firstname) ".
                        "from users ".
                        "where  (user_active = 1 and user_address = ". param("client_address_id") . ") ".
                        "order by user_name";
}

// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";

// create sql for the location type listbox
$sql_location_types = "select location_type_id, location_type_name ".
                      "from location_types ".
                      "order by location_type_name";

// create sql for the voltage listbox
$sql_voltages = "select voltage_id, voltage_name ".
                "from voltages";

// create sql for the transportation type listbox
$sql_transportation_modes = "select transportation_type_id, transportation_type_name ".
                            "from transportation_types ".
                            "where transportation_type_visible = 1 " .
							" and transportation_type_code = 'mode' " . 
                            "order by transportation_type_name";


$sql_transportation_arranged = "select transportation_type_id, transportation_type_name ".
								"from transportation_types ".
								"where transportation_type_visible = 1 " .
								" and transportation_type_code = 'arranged' " . 
								"order by transportation_type_name";

// create sql for the project type listbox
$sql_pos_types = "select postype_id, postype_name ".
                     "from postypes ".
                     "order by postype_name";


//pos subclasses
if(param("project_postype"))
{
	$sql_pos_subclasses = "select possubclass_id, possubclass_name " .
		              "from postype_subclasses " .
		              "left join possubclasses on possubclass_id = postype_subclass_subclass " .
		              "left join product_line_pos_types on  product_line_pos_type_id = postype_subclass_pl_pos_type " . 
		              "where  product_line_pos_type_pos_type = " . param("project_postype") .
		              " and product_line_pos_type_product_line is null " .
	                  " order by possubclass_name";
}
elseif($project["project_postype"])
{
	$sql_pos_subclasses = "select possubclass_id, possubclass_name " .
		              "from postype_subclasses " .
		              "left join possubclasses on possubclass_id = postype_subclass_subclass " .
		              "left join product_line_pos_types on  product_line_pos_type_id = postype_subclass_pl_pos_type " . 
		              "where  product_line_pos_type_pos_type = " . $project["project_postype"] .
		              " and product_line_pos_type_product_line is null " .
	                  " order by possubclass_name";
}
else
{
	$sql_pos_subclasses = "select possubclass_id, possubclass_name " .
		              "from postype_subclasses " .
		              "left join possubclasses on possubclass_id = postype_subclass_subclass " .
		              "left join product_line_pos_types on  product_line_pos_type_id = postype_subclass_pl_pos_type " . 
		              "where  product_line_pos_type_pos_type = 0 " .
		              " and product_line_pos_type_product_line =  0" .
	                  " order by possubclass_name";
}

//get addresses from pos index
if(param("project_projectkind") == 7)
{
	$sql_posaddresses = "select posaddress_id, concat(posaddress_place, ', ', posaddress_name, ', ', posaddress_address) as posaddress " .
						"from posaddresses " . 
						"where posaddress_store_postype = 4 " . 
		                " and posaddress_country = " . $project["order_shop_address_country"] . 
						" order by posaddress_place, posaddress_name";
}
else
{

	$sql_posaddresses = "select posaddress_id, concat(posaddress_place, ', ', posaddress_name, ', ', posaddress_address) as posaddress " .
						"from posaddresses " . 
						"where posaddress_country = " . $project["order_shop_address_country"] . 
						" order by posaddress_place, posaddress_name";
}

//project legal types
if(param("project_cost_type"))
{
	if(param("project_cost_type") == 1) // Corporate
	{
		$sql_project_kinds = "select projectkind_id, projectkind_name " . 
			                 "from projectkinds " . 
			                 "where projectkind_id > 0 ";
	}
	else
	{
		if($old_projectkind == 1)
		{
			$sql_project_kinds = "select projectkind_id, projectkind_name " . 
								 "from projectkinds " . 
								 "where projectkind_id in (1,2,6, 7, 9)";
		}
		else
		{
			$sql_project_kinds = "select projectkind_id, projectkind_name " . 
								 "from projectkinds " . 
								 "where projectkind_id in (1,2,6, 9) or projectkind_id = " . $project["project_projectkind"];

		}
	}
}
else
{
	if($project["project_cost_type"] == 1) // Corporate
	{
		$sql_project_kinds = "select projectkind_id, projectkind_name " . 
			                 "from projectkinds " . 
			                 "where projectkind_id > 0 ";
	}
	else
	{
		if($old_projectkind == 1)
		{
			$sql_project_kinds = "select projectkind_id, projectkind_name " . 
								 "from projectkinds " . 
								 "where projectkind_id in (1,2,6, 7, 9)";
		}
		else
		{
			$sql_project_kinds = "select projectkind_id, projectkind_name " . 
								 "from projectkinds " . 
								 "where projectkind_id in (1,2,6, 9) or projectkind_id = " . $project["project_projectkind"];
		}
	}
}

// create sql for the furniture height listbox
$sql_ceiling_heights = "select ceiling_height_id, ceiling_height_height ".
                "from ceiling_heights";

//get pos data
if($project["pipeline"] == 0)
{
	$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");
}
elseif($project["pipeline"] == 1)
{
	$pos_data = get_poslocation_from_pipeline($project["posaddress_id"], $project["order_id"]);
}

$projectkind_has_changed = false;

if($old_projectkind == 1 and param("project_projectkind") > 1) // changed from NEW to another type
{
	$projectkind_has_changed = true;
}


//create sql for places
if(param("client_address_id")) {
	$sql_billing_places = "select place_id, place_name from places " . 
		          "where place_country = " .  dbquote($address["country"]) . 
		          " order by place_name";

}
elseif(param("invoice_recipient")) {
	
	$invoice_address = get_address(param("invoice_recipient"));
	$sql_billing_places = "select place_id, place_name from places " . 
		          "where place_country = " .  dbquote($invoice_address["country"]) . 
		          " order by place_name";

}
elseif(param("billing_address_country")) {
	$sql_billing_places = "select place_id, place_name from places " . 
		          "where place_country = " .  dbquote(param("billing_address_country")) . 
		          " order by place_name";
}
elseif($project["order_billing_address_country"]) {
	$sql_billing_places = "select place_id, place_name from places " . 
		          "where place_country = " .  dbquote($project["order_billing_address_country"]) . 
		          " order by place_name";
}

if(param("delivery_address_country")) {
	$sql_delivery_places = "select place_id, place_name from places " . 
		          "where place_country = " .   dbquote(param("delivery_address_country")) . 
		          " order by place_name";

}
elseif($delivery_address['country']) {
	$sql_delivery_places = "select place_id, place_name from places " . 
		          "where place_country = " .   dbquote($delivery_address['country']) . 
		          " order by place_name";
}
else
{
	$sql_delivery_places = "select place_id, place_name from places " . 
		          "where place_country = " .  dbquote($address['country']) . 
		          " order by place_name";
}


$billing_address_province_name = "";
if(param("billing_address_place_id"))
{
	$billing_address_province_name = get_province_name(param("billing_address_place_id"));
}
elseif($project["order_billing_address_place_id"])
{
	$billing_address_province_name = get_province_name($project["order_billing_address_place_id"]);
}



$delivery_address_province_name = "";
if(param("delivery_address_place_id"))
{
	$delivery_address_province_name = get_province_name(param("delivery_address_place_id"));
}
elseif($delivery_address["place_id"])
{
	$delivery_address_province_name = get_province_name($delivery_address["place_id"]);
}

//get invoice_addresses
$invoice_addresses = array();

$sql_inv = "select invoice_address_id, concat(invoice_address_company, ', ' ,place_name, ', ', country_name) as company " .
    "from invoice_addresses " .
	"left join places on place_id = invoice_address_place_id " . 
	"left join countries on country_id = invoice_address_country_id " . 
	"where invoice_address_active = 1 and invoice_address_address_id = " . $address["id"] . 
    " order by invoice_address_company";

$res = mysql_query($sql_inv) or dberror($sql_inv);
while ($row = mysql_fetch_assoc($res))
{
	$invoice_addresses[$row["invoice_address_id"]] = $row["company"];
}


/********************************************************************
    Get Cost Monitoring Sheet Budget Data
*********************************************************************/ 
$sql =  "select * from project_costs " .
        "where project_cost_order = " . $project["project_order"];

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

$project_cost_grosssqms = $row["project_cost_grosssqms"];
$project_cost_totalsqms = $row["project_cost_totalsqms"];
$project_cost_original_sqms = $row["project_cost_original_sqms"];
$project_cost_sqms = $row["project_cost_sqms"];
$project_cost_backofficesqms = $row["project_cost_backofficesqms"];
$project_cost_floorsurface1 = $row["project_cost_floorsurface1"];
$project_cost_floorsurface2 = $row["project_cost_floorsurface2"];
$project_cost_floorsurface3 = $row["project_cost_floorsurface3"];
$project_cost_numfloors = $row["project_cost_numfloors"];

$project_cost_grosssqft = sqm_to_feet($row["project_cost_grosssqms"]);
$project_cost_totalsqft = sqm_to_feet($row["project_cost_totalsqms"]);
$project_cost_sqft = sqm_to_feet($row["project_cost_sqms"]);
$project_cost_backofficesqft = sqm_to_feet($row["project_cost_backofficesqms"]);
$project_cost_floorsurface1sqft = sqm_to_feet($row["project_cost_floorsurface1"]);
$project_cost_floorsurface2sqft = sqm_to_feet($row["project_cost_floorsurface2"]);
$project_cost_floorsurface3sqft = sqm_to_feet($row["project_cost_floorsurface3"]);



/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid", $project["project_order"]);

if (has_access("can_edit_product_line"))
{
    
    $form->add_list("project_cost_type", "Project Legal Type", $sql_project_cost_types, 0, $project["project_cost_type"]);
	
	if($project["project_projectkind"] != 8) // not a Popup
	{
		$form->add_list("project_projectkind", "Project Type*", $sql_project_kinds,SUBMIT | NOTNULL, $project["project_projectkind"]);
	}
	else
	{
		$form->add_label("type3", "Project Type", 0, $project["projectkind_name"]);
		$form->add_hidden("project_projectkind", $project["project_projectkind"]);
	}

	$form->add_list("project_postype", "POS Type*", $sql_pos_types, NOTNULL | SUBMIT, $project["project_postype"]);
	$form->add_list("project_pos_subclass", "POS Type Subclass", $sql_pos_subclasses, 0, $project["project_pos_subclass"]);

	
    $form->add_label("product_line_name", "Product Line / Subclass", 0, $project["product_line_name"] . " / " . $project["productline_subclass_name"]);
	$form->add_hidden("product_line",$project["project_product_line"]);
	
	if (has_access("can_edit_project_number"))
    {
        $form->add_edit("project_number", "Project Number*", NOTNULL, $project["project_number"], TYPE_CHAR);
    }
    else
    {
        $form->add_label("project_number", "Project Number*", 0, $project["project_number"]);
    }
	$form->add_label("treatment_state", "Treatment State", 0, $project["project_state_text"]);
	
}
else
{
    $form->add_label("type3", "Project Legal Type / Project Type", 0, $project["project_costtype_text"] . " / " . $project["projectkind_name"]);

	$form->add_label("project_postype", "POS Type / Subclass", 0, $project["postype_name"] . " / " . $project["possubclass_name"]);

	$form->add_label("product_line_name", "Product Line / Subclass", 0, $project["product_line_name"] . " / " . $project["productline_subclass_name"]);
	$form->add_hidden("product_line",$project["project_product_line"]);

	$form->add_label("project_number_label", "Project Number  /Treatment State", 0, $project["project_number"] . " / " . $project["project_state_text"]);
}


//$form->add_label("staff", "Project Leader / Logistics Coordinator", 0, $project["project_manager"] . " / " . $project["operator"]);
$form->add_label("staff", "Project Leader", 0, $project["project_manager"]);

if (has_access("can_edit_status_in_projects"))
{
    $sql = "select distinct order_state_code " .
           "from order_states " .
		   " where order_state_code < '890' " . 
           "order by order_state_code";

    $form->add_list("status", "Project Status", $sql, NOTNULL, $project["order_actual_order_state_code"]);
	
	$form->add_hidden("dstatus", $project["order_development_status"]);
	$form->add_hidden("lstatus", $project["order_logistic_status"]);
	
	/*
	$form->add_list("dstatus", "Project Status Development", $sql, NOTNULL, $project["order_development_status"]);
	$form->add_list("lstatus", "Project Status Logistic", $sql, 0, $project["order_logistic_status"]);
	*/

	$form->add_hidden("project_state", $project["project_state"]);
	$form->add_hidden("order_development_status", $project["order_development_status"]);
	$form->add_hidden("order_logistic_status", $project["order_logistic_status"]);

}
else
{
    
	
    $form->add_hidden("status", $project["order_actual_order_state_code"]);
	$form->add_hidden("dstatus", $project["order_development_status"]);
	$form->add_hidden("lstatus", $project["order_logistic_status"]);
	
	$form->add_hidden("project_state", $project["project_state"]);
	$form->add_hidden("order_development_status", $project["order_development_status"]);
	$form->add_hidden("order_logistic_status", $project["order_logistic_status"]);

    

}

/*
if(isset($offer_state) and $offer_state["data"])
{
	$form->add_label("status_label", "Project Status Development", 0,  $development_state["name"], 1, "offer_info");
}
else
{
	$form->add_label("status_label", "Project Status Development", 0,  $development_state["name"]);
}
if(has_access("can_see_logistic_status") and isset($logistic_state) and $logistic_state["data"])
{
	$form->add_label("logistic_status_label", "Project Status Logistic", 0, $logistic_state["info"], 1, "logistic_info");
}
elseif(has_access("can_see_logistic_status") and isset($logistic_state) and $logistic_state["info"])
{
	$form->add_label("logistic_status_label", "Project Status Logistic", 0, $logistic_state["info"]);
}
*/

$form->add_section("Client");
$form->add_list("client_address_id", "Client*", $sql_address, SUBMIT | NOTNULL, $project["order_client_address"]);
$form->add_list("client_address_user_id", "Contact*", $sql_address_user, NOTNULL, $project["order_user"]);

if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) // take over, lease renewal
{
	$form->add_hidden("order_direct_invoice_address_id", $project["order_direct_invoice_address_id"]);


	$form->add_hidden("billing_address_company", $project["order_billing_address_company"]);
	$form->add_hidden("billing_address_company2", $project["order_billing_address_company2"]);
	$form->add_hidden("billing_address_address", $project["order_billing_address_address"]);
	$form->add_hidden("billing_address_street", $project["order_billing_address_street"]);
	$form->add_hidden("billing_address_streetnumber", $project["order_billing_address_streetnumber"]);
	$form->add_hidden("billing_address_address2",$project["order_billing_address_address2"]);
	$form->add_hidden("billing_address_zip", $project["order_billing_address_zip"]);
	$form->add_hidden("billing_address_place", $project["order_billing_address_place"]);
	$form->add_hidden("billing_address_place_id", $project["order_billing_address_place_id"]);
	$form->add_hidden("billing_address_country", $project["order_billing_address_country"]);
	$form->add_hidden("billing_address_phone", $project["order_billing_address_phone"]);
	$form->add_hidden("billing_address_phone_country", $project["order_billing_address_phone_country"]);
	$form->add_hidden("billing_address_phone_area", $project["order_billing_address_phone_area"]);
	$form->add_hidden("billing_address_phone_number", $project["order_billing_address_phone_number"]);
	$form->add_hidden("billing_address_mobile_phone", $project["order_billing_address_mobile_phone"]);
	$form->add_hidden("billing_address_mobile_phone_country", $project["order_billing_address_mobile_phone_country"]);
	$form->add_hidden("billing_address_mobile_phone_area", $project["order_billing_address_mobile_phone_area"]);
	$form->add_hidden("billing_address_mobile_phone_number", $project["order_billing_address_mobile_phone_number"]);
	$form->add_hidden("billing_address_email", $project["order_billing_address_email"]);
	$form->add_hidden("billing_address_contact", $project["order_billing_address_contact"]);


	$form->add_hidden("delivery_address_company", $delivery_address["company"]);
	$form->add_hidden("delivery_address_company2", $delivery_address["company2"]);
	$form->add_hidden("delivery_address_address", $delivery_address["address"]);
	$form->add_hidden("delivery_address_street", $delivery_address["street"]);
	$form->add_hidden("delivery_address_streetnumber", $delivery_address["streetnumber"]);
	$form->add_hidden("delivery_address_address2", $delivery_address["address2"]);
	$form->add_hidden("delivery_address_zip", $delivery_address["zip"]);
	$form->add_hidden("delivery_address_place", $delivery_address["place"]);
	$form->add_hidden("delivery_address_place_id", $delivery_address['place_id']);
	$form->add_hidden("delivery_address_country", $delivery_address["country"]);
	$form->add_hidden("delivery_address_phone", $delivery_address["phone"]);
	$form->add_hidden("delivery_address_mobile_phone", $delivery_address["mobile_phone"]);
	$form->add_hidden("delivery_address_email", $delivery_address["email"]);
	$form->add_hidden("delivery_address_contact", $delivery_address["contact"]);
}
else
{
	$form->add_section("Bill to");

	if($address["invoice_recipient"] > 0)
	{
		$sql_invoice_addresses = "select address_id, concat(country_name, ': ', address_company) as company from addresses " .
                 "left join countries on country_id = address_country " .
				 "where address_id = " . $address["id"] . " or address_id = " . $address["invoice_recipient"] . 
			     " order by country_name, address_company";
		$form->add_list("invoice_recipient", "Change Billing Address", $sql_invoice_addresses, SUBMIT);
	}

	$form->add_edit("billing_address_company", "Company*", NOTNULL, $project["order_billing_address_company"], TYPE_CHAR);
	$form->add_edit("billing_address_company2", "", 0, $project["order_billing_address_company2"], TYPE_CHAR);

	$form->add_hidden("billing_address_address", $project["order_billing_address_address"]);
	$form->add_multi_edit("bill_to_street", array("order_billing_address_street", "order_billing_address_streetnumber"), "Street/Street number", array(NOTNULL, ''), array($project["order_billing_address_street"], $project["order_billing_address_streetnumber"]), array('', ''), array(200, 6), array(), 0, '', '', array(40, 5));


	$form->add_edit("billing_address_address2", "Additional Address Info", 0, $project["order_billing_address_address2"], TYPE_CHAR);
	$form->add_edit("billing_address_zip", "ZIP", 0, $project["order_billing_address_zip"], TYPE_CHAR, 20);
	$form->add_list("billing_address_place_id", "City*", $sql_billing_places, NOTNULL |SUBMIT, $project["order_billing_address_place_id"]);
	$form->add_label("billing_address_place", "", 0,$project["order_billing_address_place"]);
	$form->add_label("billing_address_province_name", "Province", 0, $billing_address_province_name);
	$form->add_list("billing_address_country", "Country*", $sql_countries, SUBMIT | NOTNULL, $project["order_billing_address_country"]);
	
	$form->add_hidden("billing_address_phone", $project["order_billing_address_phone"]);
	$form->add_multi_edit("bill_to_phone_number", array("order_billing_address_phone_country", "order_billing_address_phone_area", "order_billing_address_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array($project["order_billing_address_phone_country"], $project["order_billing_address_phone_area"], $project["order_billing_address_phone_number"]), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));


	$form->add_hidden("billing_address_mobile_phone", $project["order_billing_address_mobile_phone"]);
	$form->add_multi_edit("bill_to_mobile_phone_number", array("order_billing_address_mobile_phone_country", "order_billing_address_mobile_phone_area", "order_billing_address_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array($project["order_billing_address_mobile_phone_country"], $project["order_billing_address_mobile_phone_area"], $project["order_billing_address_mobile_phone_number"]), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

	$form->add_edit("billing_address_email", "Email", 0, $project["order_billing_address_email"], TYPE_CHAR);
	$form->add_edit("billing_address_contact", "Contact*", NOTNULL, $project["order_billing_address_contact"], TYPE_CHAR);


	if(count($invoice_addresses) > 0)
	{
		$form->add_section("Billing Address (invoice address) for direct Invoicing");
		$form->add_comment("Please indicate the billing address (invoice address) in case suppliers do send invoices to a different than the above address.");
		$form->add_list("order_direct_invoice_address_id", "Bill to", $invoice_addresses, 0, $project["order_direct_invoice_address_id"]);
	}
	else
	{
		$form->add_hidden("order_direct_invoice_address_id", 0);
	}


	/*
	$form->add_section("Ship to");
	
	
	$form->add_label("delivery_address_company", "Company", 0, $delivery_address["company"]);
	if($delivery_address["company2"])
	{
		$form->add_label("delivery_address_company2", "", 0, $delivery_address["company2"]);
	}
	$form->add_label("delivery_address_address", "Street", 0, $delivery_address["address"]);
	if($delivery_address["address2"])
	{
		$form->add_label("delivery_address_address2", "Additional Address Info", 0, $delivery_address["address2"]);
	}
	$form->add_label("delivery_address_zip", "ZIP", 0, $delivery_address["zip"]);
	$form->add_lookup("delivery_address_place", "City", "places", "place_name", 0, $delivery_address['place_id']);
	$form->add_label("delivery_address_province_name", "Province", 0, $delivery_address_province_name);
	$form->add_lookup("delivery_address_country", "Country", "countries", "country_name", 0, $delivery_address['country']);
	if($delivery_address["phone"])
	{
		$form->add_label("delivery_address_phone", "Phone", 0, $delivery_address["phone"]);
	}
	if($delivery_address["mobile_phone"])
	{
		$form->add_label("delivery_address_mobile_phone", "Mobile Phone", 0, $delivery_address["mobile_phone"]);
	}
	if($delivery_address["email"])
	{
		$form->add_label("delivery_address_email", "Email", 0, $delivery_address["email"]);
	}
	$form->add_label("delivery_address_contact", "Contact", 0, $delivery_address["contact"]);

	*/

}

if(count($pos_data) > 0 and $old_projectkind == 1 
	and (param("project_projectkind") == 6 or param("project_projectkind") == 9))
{
	$form->add_section("POS Location Address");
	$form->add_hidden("posaddress_id", $pos_data["posaddress_id"]);
	$form->add_label("shop_address_company", "Project Name", 0, $pos_data["posaddress_name"]);
	$form->add_label("shop_address_company2", "", 0, $pos_data["posaddress_name2"]);
	$form->add_label("shop_address_address", "Street", 0, $pos_data["posaddress_address"]);
	$form->add_hidden("shop_address_street", $pos_data["posaddress_street"]);
	$form->add_hidden("shop_address_streetnumber", $pos_data["posaddress_street_number"]);
	$form->add_label("shop_address_address2", "Additional Address Info", 0, $pos_data["posaddress_address2"]);
	$form->add_label("shop_address_zip", "ZIP", 0, $pos_data["posaddress_zip"]);
	$form->add_label("shop_address_place", "City", 0, $pos_data["posaddress_place"]);
	$form->add_label("shop_address_country_name", "Country", 0, $pos_data["country_name"]);
	$form->add_label("shop_address_phone", "Phone", 0, $pos_data["posaddress_phone"]);
	$form->add_hidden("shop_address_phone_country", $pos_data["posaddress_phone_country"]);
	$form->add_hidden("shop_address_phone_area", $pos_data["posaddress_phone_area"]);
	$form->add_hidden("shop_address_phone_number", $pos_data["posaddress_phone_number"]);
	$form->add_label("shop_address_mobile_phone", "Mobile Phone", 0, $pos_data["posaddress_mobile_phone"]);
	$form->add_hidden("shop_address_mobile_phone_country", $pos_data["posaddress_mobile_phone_country"]);
	$form->add_hidden("shop_address_mobile_phone_area", $pos_data["posaddress_mobile_phone_area"]);
	$form->add_hidden("shop_address_mobile_phone_number", $pos_data["posaddress_mobile_phone_number"]);
	$form->add_label("shop_address_email", "Email", 0, $pos_data["posaddress_email"]);
	$form->add_hidden("shop_address_country", $pos_data["posaddress_country"]);
	$form->add_hidden("shop_address_place_id", $pos_data["posaddress_place_id"]);
}
elseif(count($pos_data) > 0 and $projectkind_has_changed == false)
{
	$form->add_section("POS Location Address");
	$form->add_hidden("posaddress_id", $pos_data["posaddress_id"]);
	$form->add_label("shop_address_company", "Project Name", 0, $pos_data["posaddress_name"]);
	$form->add_label("shop_address_company2", "", 0, $pos_data["posaddress_name2"]);
	$form->add_label("shop_address_address", "Street", 0, $pos_data["posaddress_address"]);
	$form->add_hidden("shop_address_street", $pos_data["posaddress_street"]);
	$form->add_hidden("shop_address_streetnumber", $pos_data["posaddress_street_number"]);
	$form->add_label("shop_address_address2", "Additional Address Info", 0, $pos_data["posaddress_address2"]);
	$form->add_label("shop_address_zip", "ZIP", 0, $pos_data["posaddress_zip"]);
	$form->add_label("shop_address_place", "City", 0, $pos_data["posaddress_place"]);
	$form->add_label("shop_address_country_name", "Country", 0, $pos_data["country_name"]);
	$form->add_label("shop_address_phone", "Phone", 0, $pos_data["posaddress_phone"]);
	$form->add_hidden("shop_address_phone_country", $pos_data["posaddress_phone_country"]);
	$form->add_hidden("shop_address_phone_area", $pos_data["posaddress_phone_area"]);
	$form->add_hidden("shop_address_phone_number", $pos_data["posaddress_phone_number"]);
	$form->add_label("shop_address_mobile_phone", "Mobile Phone", 0, $pos_data["posaddress_mobile_phone"]);
	$form->add_hidden("shop_address_mobile_phone_country", $pos_data["posaddress_mobile_phone_country"]);
	$form->add_hidden("shop_address_mobile_phone_area", $pos_data["posaddress_mobile_phone_area"]);
	$form->add_hidden("shop_address_mobile_phone_number", $pos_data["posaddress_mobile_phone_number"]);
	$form->add_label("shop_address_email", "Email", 0, $pos_data["posaddress_email"]);
	$form->add_hidden("shop_address_country", $pos_data["posaddress_country"]);
	$form->add_hidden("shop_address_place_id", $pos_data["posaddress_place_id"]);
}
else
{
	$form->add_section("POS Location Address");
	$form->add_comment("Assignment to POS Index.");
	$form->add_list("posaddress_id", "POS Index POS Name", $sql_posaddresses, SUBMIT, $project["posaddress_id"]);
	$form->add_edit("shop_address_company", "Project Name*", NOTNULL, $project["order_shop_address_company"], TYPE_CHAR);
	$form->add_edit("shop_address_company2", "", 0, $project["order_shop_address_company2"], TYPE_CHAR);
	$form->add_hidden("shop_address_address", $project["order_shop_address_address"]);
	$form->add_multi_edit("shop_street", array("order_shop_address_street", "order_shop_address_streetnumber"), "Street/Street number", array(NOTNULL, ''), array($project["order_shop_address_street"], $project["order_shop_address_streetnumber"]), array('', ''), array(200, 6), array(), 0, '', '', array(40, 5));

	$form->add_edit("shop_address_address2", "Additional Address Info", 0, $project["order_shop_address_address2"], TYPE_CHAR);
	$form->add_edit("shop_address_zip", "ZIP", 0, $project["order_shop_address_zip"], TYPE_CHAR, 20);
	$form->add_edit("shop_address_place", "City*", NOTNULL, $project["order_shop_address_place"], TYPE_CHAR, 20);
	$form->add_list("shop_address_country", "Country*", $sql_countries, NOTNULL, $project["order_shop_address_country"]);
	
	$form->add_hidden("shop_address_phone", $project["order_shop_address_phone"]);
	$form->add_multi_edit("shop_phone_number", array("order_shop_address_phone_country", "order_shop_address_phone_area", "order_shop_address_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array($project["order_shop_address_phone_country"], $project["order_shop_address_phone_area"], $project["order_shop_address_phone_number"]), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

	$form->add_hidden("shop_address_mobile_phone", $project["order_shop_address_mobile_phone"]);
	$form->add_multi_edit("shop_mobile_phone_number", array("order_shop_address_mobile_phone_country", "order_shop_address_mobile_phone_area", "order_shop_address_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array($project["order_shop_address_mobile_phone_country"], $project["order_shop_address_mobile_phone_area"], $project["order_shop_address_mobile_phone_number"]), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

	$form->add_edit("shop_address_email", "Email", 0, $project["order_shop_address_email"], TYPE_CHAR, 50);
}

if($project["project_projectkind"] == 6 or $project["project_projectkind"] == 9) // Relocation Project
{
	$form->add_section("Relocation Info");
	$form->add_comment("Please indicate the POS to be relocated.");
	$form->add_list("project_relocated_posaddress_id", "POS being relocated", $sql_posaddresses, 0, $project["project_relocated_posaddress_id"]);
}
else
{
	$form->add_hidden("project_relocated_posaddress_id", 0);
}

$tmp = true;
if(param("project_projectkind") == 1 
   or param("project_projectkind") == 2
   or param("project_projectkind") == 3
   or param("project_projectkind") == 6
   or param("project_projectkind") == 7
   or param("project_projectkind") == 9)
{
	$tmp = false;
}

if($tmp == true and ($project["project_projectkind"] == 4 
	or $project["project_projectkind"] == 5)) // take over, lease renewal
{
	$form->add_hidden("store_grosssurface", $project_cost_grosssqms);
	$form->add_hidden("store_totalsurface", $project_cost_totalsqms);
	$form->add_hidden("store_retailarea", $project_cost_sqms);
	$form->add_hidden("store_backoffice", $project_cost_backofficesqms);
	$form->add_hidden("store_numfloors", $project_cost_numfloors);
	$form->add_hidden("store_floorsurface1", $project_cost_floorsurface1);
	$form->add_hidden("store_floorsurface2", $project_cost_floorsurface2);
	$form->add_hidden("store_floorsurface3", $project_cost_floorsurface3);

	$form->add_hidden("location_type", $project["project_location_type"]);
	$form->add_hidden("location_type_other", $project["project_location"]);
	$form->add_hidden("voltage", $project["order_voltage"]);

	$form->add_hidden("tissot_shop_around", $project["project_tissot_shop_around"]);


	$form->add_hidden("watches_displayed",$project["project_watches_displayed"]);
	$form->add_hidden("watches_stored", $project["project_watches_stored"]);
	$form->add_hidden("bijoux_displayed",$project["project_bijoux_displayed"]);
	$form->add_hidden("bijoux_stored", $project["project_bijoux_stored"]);

	//$form->add_hidden("preferred_delivery_date", to_system_date($project["order_preferred_delivery_date"]));
	
	$form->add_hidden( "preferred_transportation_arranged");
	$form->add_hidden( "preferred_transportation_mode");
	$form->add_hidden( "packaging_retraction");

	$form->add_hidden( "pedestrian_mall_approval", $project["order_pedestrian_mall_approval"]);
	$form->add_hidden( "full_delivery", $project["order_full_delivery"]);
	$form->add_hidden("delivery_comments", $project["order_delivery_comments"]);

	$form->add_hidden("unit_of_measurement", 2);

	$form->add_hidden("order_insurance", $project["order_insurance"]);

	$form->add_hidden("approximate_budget", $project["project_approximate_budget"]);
	$form->add_hidden("planned_opening_date", to_system_Date($project["project_planned_opening_date"]));
	$form->add_hidden("planned_closing_date");

	if($project["project_projectkind"] == 4 ) // take over
	{
		$form->add_edit("planned_takeover_date", "Client's Planned Takeover Date*", NOTNULL, to_system_Date($project["project_real_opening_date"]), TYPE_DATE, 20);
	}
	else
	{
		$form->add_hidden("planned_takeover_date");
	}


	$form->add_hidden("comments", $project["project_comments"]);

	$form->add_hidden("requested_store_retailarea", $project_cost_original_sqms);
}
else
{
	$form->add_section("Surfaces");

	
	$form->add_list("unit_of_measurement", "Unit of measurement", $units, NOTNULL, 2);

	//$form->add_edit("store_grosssurface", "Gross Surface in <span id='um00'>sqms</span>*", NOTNULL, $project_cost_grosssqms, TYPE_DECIMAL, 10,2);
	$form->add_hidden("store_grosssurface", $project_cost_grosssqms);

	$form->add_edit("store_totalsurface", "Total Surface in <span id='um01'>sqms</span>*", NOTNULL, $project_cost_totalsqms, TYPE_DECIMAL, 10,2);
	
	if(has_access("can_edit_catalog"))
	{
		$form->add_edit("requested_store_retailarea", "Sales Surface upon project entry in <span id='um07'>sqms</span>", NOTNULL, $project_cost_original_sqms, TYPE_DECIMAL, 10,2);
	}
	else
	{
		$form->add_label("requested_store_retailarea", "Sales Surface upon project entry in <span id='um07'>sqms</span>", 0, $project_cost_original_sqms);
	}

	//$form->add_edit("store_retailarea", "Sales Surface according to layout in <span id='um02'>sqms</span>*", NOTNULL, $project_cost_sqms, TYPE_DECIMAL, 10,2);
	
	$form->add_hidden("store_retailarea", $project_cost_sqms);


	$form->add_edit("store_backoffice", "Other Surface in <span id='um03'>sqms</span>", 0, $project_cost_backofficesqms, TYPE_DECIMAL, 10,2);
	
	/*
	$form->add_edit("store_numfloors", "Number of Floors*", NOTNULL, $project_cost_numfloors, TYPE_DECIMAL, 10,2);
	$form->add_edit("store_floorsurface1", "Floor 1: Surface in <span id='um04'>sqms</span>", 0, $project_cost_floorsurface1, TYPE_DECIMAL, 10,2);
	$form->add_edit("store_floorsurface2", "Floor 2: Surface in <span id='um05'>sqms</span>", 0, $project_cost_floorsurface2, TYPE_DECIMAL, 10,2);
	$form->add_edit("store_floorsurface3", "Floor 3: Surface in <span id='um06'>sqms</span>", 0, $project_cost_floorsurface3, TYPE_DECIMAL, 10,2);
    */

	$form->add_hidden("store_numfloors", $project_cost_numfloors);
	$form->add_hidden("store_floorsurface1", $project_cost_floorsurface1);
	$form->add_hidden("store_floorsurface2", $project_cost_floorsurface2);
	$form->add_hidden("store_floorsurface3", $project_cost_floorsurface3);




	$form->add_section("Location Info");
	$form->add_list("location_type", "Location Type*", $sql_location_types, 0, $project["project_location_type"]);
	$form->add_edit("location_type_other", "Other Location Type*", 0, $project["project_location"], TYPE_CHAR, 20);
	//$form->add_list("voltage", "Voltage Choice*", $sql_voltages, NOTNULL, $project["order_voltage"]);
	$form->add_hidden("voltage");

	$form->add_list("tissot_shop_around", "Is there a POS selling Tissot in 1km around the location*", array(2=>'yes', 1=>'no' ), NOTNULL, $project["project_tissot_shop_around"]+1);
	
	// count design_objective_groups new version, design objectives depending on the postype
	$sql_design_objective_group = "select design_objective_group_id, design_objective_group_name, " .
								  "    design_objective_group_multiple " .
								  "from design_objective_groups " .
								  "where design_objective_group_postype =  " . $project["project_postype"] . " " .
								  "order by design_objective_group_priority";

	$res = mysql_query($sql_design_objective_group) or dberror($sql_design_objective_group);
	$number_of_design_objective_groups = mysql_num_rows($res);


	if ($number_of_design_objective_groups > 0)
	{
		$form->add_section("Design Objectives");
		$form->add_comment("Please indicate your basic needs and requirements to assist out design process. ".
		"\n A Project Leader will ensure that the design is consistent with the ".
		"Tissot Retail objectives."); 

		$i = 1;
		
		while ($row = mysql_fetch_assoc($res))
		{
			$sql_design_objective_item = "select design_objective_item_id, design_objective_item_name ".
										 "from design_objective_items ".
										 "where design_objective_item_group=" .    $row["design_objective_group_id"] . " ".
										 "order by design_objective_item_priority";

			if ($row["design_objective_group_multiple"] == 0) 
			{
				$value = "";
				foreach ($project_design_objective_item_ids as $key=>$element)
				{
				  if ($element == $row["design_objective_group_name"])
				  {
					  $value = $key;
				  }
				}

				$form->add_list("design_objective_items" . $i, $row["design_objective_group_name"] . "*", $sql_design_objective_item, NOTNULL, $value);
				$design_objectives_listbox_names[] = "design_objective_items" . $i;
			}
			else 
			{
				$values = array();
				foreach ($project_design_objective_item_ids as $key=>$element)
				{
					if ($element == $row["design_objective_group_name"])
					{
						$values[] = $key;
					}
					next($project_design_objective_item_ids);
				}
				$form->add_checklist("design_objective_items" . $i, $row["design_objective_group_name"] . "*", "project_items", $sql_design_objective_item, NOTNULL, $values);
				$design_objectives_checklist_names[] = "design_objective_items" . $i;
			}
			
			$i++;   
		}
	}

	//$form->add_section("Capacity Request by Client");
	//$form->add_edit("watches_displayed", "Watches Displayed", 0, $project["project_watches_displayed"], TYPE_INT, 20);
	$form->add_hidden("watches_displayed");
	//$form->add_edit("watches_stored", "Watches Stored", 0, $project["project_watches_stored"], TYPE_INT, 20);
	$form->add_hidden("watches_stored");
	//$form->add_edit("bijoux_displayed", "Watch Straps Displayed", 0, $project["project_bijoux_displayed"], TYPE_INT, 20);
	$form->add_hidden("bijoux_displayed");
	//$form->add_edit("bijoux_stored", "Watch Straps Stored", 0, $project["project_bijoux_stored"], TYPE_INT, 20);
	$form->add_hidden("bijoux_stored");

	
	
	//$form->add_section("Preferences and Traffic Checklist");
	//$form->add_comment("Please enter the date in the form of dd.mm.yyyy.");
	//$form->add_edit("preferred_delivery_date", "Preferred Arrival Date", 0, to_system_date($project["order_preferred_delivery_date"]), TYPE_DATE, 20);
	
	/*
	$form->add_list("preferred_transportation_arranged", "Transportation arranged by*", $sql_transportation_arranged, NOTNULL, $project["order_preferred_transportation_arranged"]);

	$form->add_list("preferred_transportation_mode", "Transportation mode*", $sql_transportation_modes, NOTNULL, $project["order_preferred_transportation_mode"]);

	//$form->add_radiolist( "packaging_retraction", "Packaging Retraction Desired", array(0 => "no", 1 => "yes"), 0, $project["order_packaging_retraction"]);

	$form->add_comment("Please indicate if there is a special approval needed for delivery into ".
					   "a pedestrian area."); 
	$form->add_radiolist( "pedestrian_mall_approval", "Pedestrian Area Approval Needed",
		array(0 => "no", 1 => "yes"), 0, $project["order_pedestrian_mall_approval"]);
	$form->add_comment("Please indicate if partial delivery is possible or full delivery is required."); 
	$form->add_radiolist( "full_delivery", "Full Delivery",
		array(0 => "no", 1 => "yes"), 0, $project["order_full_delivery"]);
	$form->add_comment("Please indicate any other circumstances/restrictions concerning delivery and traffic."); 
	$form->add_multiline("delivery_comments", "Delivery Comments", 4, 0, $project["order_delivery_comments"]);

	$form->add_section("Insurance");
	$form->add_radiolist("order_insurance", array(1=>"Insurance by Tissot/Forwarder", 0=>""), array(1=>"covered",0=>"not covered"), VERTICAL, $project["order_insurance"]);

	*/

	$form->add_hidden( "preferred_transportation_arranged");
	$form->add_hidden( "preferred_transportation_mode");

	$form->add_hidden( "packaging_retraction");
	$form->add_hidden( "pedestrian_mall_approval", $project["order_pedestrian_mall_approval"]);
	$form->add_hidden( "full_delivery", $project["order_full_delivery"]);
	$form->add_hidden("delivery_comments", $project["order_delivery_comments"]);
	$form->add_hidden("order_insurance", $project["order_insurance"]);


	$form->add_section("Other Information");
	$form->add_comment("Please use only figures to indicate the budget ".
					   "and enter the date in the form of dd.mm.yyyy.");
	$form->add_edit("approximate_budget", "Approximate Budget in " . $currency["symbol"] . "*", NOTNULL, $project["project_approximate_budget"], TYPE_DECIMAL, 20, 2);
	
	
	if($project["project_projectkind"] == 3 or param("project_projectkind") == 3) // takover renovation
	{
		$form->add_edit("planned_takeover_date", "Client's Planned Takeover Date*", NOTNULL, to_system_Date($project["project_planned_takeover_date"]), TYPE_DATE, 20);

	}
	elseif($project["project_projectkind"] == 9 or param("project_projectkind") == 9) // takover relocation
	{
		$form->add_edit("planned_takeover_date", "Client's Planned Takeover Date*", NOTNULL, to_system_Date($project["project_planned_takeover_date"]), TYPE_DATE, 20);

	}
	else
	{
		$form->add_hidden("planned_takeover_date");
	}
	$form->add_edit("planned_opening_date", "Client's Preferred Opening Date*", NOTNULL, to_system_Date($project["project_planned_opening_date"]), TYPE_DATE, 20);

	if($project["project_pos_subclass"] == 27 or $project["project_projectkind"] == 8) //temporary POS, PopUp
	{
		$form->add_edit("planned_closing_date", "Client's Planned Closing Date*", NOTNULL, to_system_Date($project["project_planned_closing_date"]), TYPE_DATE, 20);
	}
	else
	{
		$form->add_hidden("planned_closing_date");	
	}


	$form->add_section("General Comments");
	$form->add_multiline("comments", "Comments", 4, 0, $project["project_comments"]);
}

$form->add_button("save", "Save Data");



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


if ($form->button("project_projectkind"))
{
	$form->value('unit_of_measurement', 2);
}
elseif ($form->button("product_line"))
{
    delete_design_objective_items(param("pid"));
    project_update_project_product_line(param("pid"), $form->value("product_line"));
    $link = "project_edit_client_data.php?pid=" . param("pid") . "&change_of_product_line=1";
	redirect($link);
}
elseif ($form->button("client_address_id"))
{
	$form->value("billing_address_company", "");
    $form->value("billing_address_company2",  "");
    $form->value("billing_address_address",  "");
	if(array_key_exists('bill_to_street', $form->items))
	{
		$form->items['bill_to_street']['values'][0] = "";
		$form->items['bill_to_street']['values'][1] = "";
	}
	elseif(array_key_exists('billing_address_street', $form->items))
	{
		$form->value("billing_address_street",  "");
		$form->value("billing_address_streetnumber", "");
	}
    $form->value("billing_address_address2",  "");
    $form->value("billing_address_zip",  "");
    $form->value("billing_address_place",  "");
    $form->value("billing_address_country",  0);
    $form->value("billing_address_phone",  "");
	if(array_key_exists('bill_to_phone_number', $form->items))
	{
		$form->items['bill_to_phone_number']['values'][0] = "";
		$form->items['bill_to_phone_number']['values'][1] = "";
		$form->items['bill_to_phone_number']['values'][2] = "";
	}
	elseif(array_key_exists('billing_address_street', $form->items))
	{
		$form->value("billing_address_phone_country",  "");
		$form->value("billing_address_phone_area",  "");
		$form->value("billing_address_phone_number",  "");
	}

    $form->value("billing_address_mobile_phone",  "");
	if(array_key_exists('bill_to_mobile_phone_number', $form->items))
	{
		$form->items['bill_to_mobile_phone_number']['values'][0] = "";
		$form->items['bill_to_mobile_phone_number']['values'][1] = "";
		$form->items['bill_to_mobile_phone_number']['values'][2] = "";
	}
	elseif(array_key_exists('billing_address_street', $form->items))
	{
		$form->value("billing_address_mobile_phone_country",  "");
		$form->value("billing_address_mobile_phone_area",  "");
		$form->value("billing_address_mobile_phone_number",  "");
	}
    $form->value("billing_address_email",  "");
    $form->value("billing_address_contact",  "");

    if ($form->value("client_address_id"))
    {
        $sql = "select * from addresses where address_id = " . $form->value("client_address_id");
        $res = mysql_query($sql) or dberror($sql);

        if ($row = mysql_fetch_assoc($res))
        {
            $form->value("billing_address_company", $row["address_company"]);
            $form->value("billing_address_company2",  $row["address_company2"]);
            $form->value("billing_address_address",  $row["address_address"]);
			
			if(array_key_exists('bill_to_street', $form->items))
			{
				$form->items['bill_to_street']['values'][0] = $row["address_street"];
				$form->items['bill_to_street']['values'][1] = $row["address_streetnumber"];
			}
			elseif(array_key_exists('billing_address_street', $form->items))
			{
				$form->value("billing_address_street",  $row["address_street"]);
				$form->value("billing_address_streetnumber",  $row["address_streetnumber"]);
			}
			$form->value("billing_address_address2",  $row["address_address2"]);
            $form->value("billing_address_zip",  $row["address_zip"]);
            $form->value("billing_address_place",  $row["address_place"]);
			$form->value("billing_address_place_id",  $row["address_place_id"]);
            $form->value("billing_address_country",  $row["address_country"]);
            $form->value("billing_address_phone",  $row["address_phone"]);
			if(array_key_exists('bill_to_phone_number', $form->items))
			{
				$form->items['bill_to_phone_number']['values'][0] = $row["address_phone_country"];
				$form->items['bill_to_phone_number']['values'][1] = $row["address_phone_area"];
				$form->items['bill_to_phone_number']['values'][2] = $row["address_phone_number"];
			}
			elseif(array_key_exists('billing_address_street', $form->items))
			{
				$form->value("billing_address_phone_country",  $row["address_phone_country"]);
				$form->value("billing_address_phone_area",  $row["address_phone_area"]);
				$form->value("billing_address_phone_number",  $row["address_phone_number"]);
			}

            $form->value("billing_address_mobile_phone",  $row["address_mobile_phone"]);

			if(array_key_exists('bill_to_mobile_phone_number', $form->items))
			{
				$form->items['bill_to_mobile_phone_number']['values'][0] = $row["address_mobile_phone_country"];
				$form->items['bill_to_mobile_phone_number']['values'][1] = $row["address_mobile_phone_area"];
				$form->items['bill_to_mobile_phone_number']['values'][2] = $row["address_mobile_phone_number"];
			}
			elseif(array_key_exists('billing_address_street', $form->items))
			{
				$form->value("billing_address_mobile_phone_country",  $row["address_mobile_phone_country"]);
				$form->value("billing_address_mobile_phone_area",  $row["address_mobile_phone_area"]);
				$form->value("billing_address_mobile_phone_number",  $row["address_mobile_phone_number"]);
			}

            $form->value("billing_address_email",  $row["address_contact_email"]);
            $form->value("billing_address_contact",  $row["address_contact_name"]);
        }
    }
}
elseif($form->button("billing_address_place_id"))
{
	$sql= "select place_name, province_canton ".
		  "from places " .
		  "left join provinces on province_id = place_province " .
		  "where place_id = " . dbquote($form->value("billing_address_place_id"));

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$form->value('billing_address_place', $row['place_name']);
		$form->value("billing_address_province_name", $row["province_canton"]);
	}
}
elseif($form->button("delivery_address_place_id"))
{
	$sql= "select place_name, province_canton ".
		  "from places " .
		  "left join provinces on province_id = place_province " .
		  "where place_id = " . dbquote($form->value("delivery_address_place_id"));

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$form->value('delivery_address_place', $row['place_name']);
		$form->value("delivery_address_province_name", $row["province_canton"]);
	}
}
elseif($form->button("invoice_recipient"))
{
	if ($form->value("invoice_recipient"))
    {
        $invoice_address = get_address($form->value("invoice_recipient"));

        
		$form->value("billing_address_company", $invoice_address["company"]);
		$form->value("billing_address_company2",  $invoice_address["company2"]);
		$form->value("billing_address_address",  $invoice_address["address"]);
		if(array_key_exists('bill_to_street', $form->items))
		{
			$form->items['bill_to_street']['values'][0] = $invoice_address["street"];
			$form->items['bill_to_street']['values'][1] = $invoice_address["streetnumber"];
		}
		elseif(array_key_exists('billing_address_street', $form->items))
		{
			$form->value("billing_address_street",  $invoice_address["street"]);
			$form->value("billing_address_streetnumber",  $invoice_address["streetnumber"]);
		}
		$form->value("billing_address_address2",  $invoice_address["address2"]);
		$form->value("billing_address_zip",  $invoice_address["zip"]);
		$form->value("billing_address_place",  $invoice_address["place"]);
		$form->value("billing_address_place_id",  $invoice_address["place_id"]);

		$form->value("billing_address_province_name",  $invoice_address["province_name"]);

		$form->value("billing_address_country",  $invoice_address["country"]);
		$form->value("billing_address_phone",  $invoice_address["phone"]);

		if(array_key_exists('bill_to_phone_number', $form->items))
		{
			$form->items['bill_to_phone_number']['values'][0] = $invoice_address["phone_country"];
			$form->items['bill_to_phone_number']['values'][1] = $invoice_address["phone_area"];
			$form->items['bill_to_phone_number']['values'][2] = $invoice_address["phone_number"];
		}
		elseif(array_key_exists('billing_address_street', $form->items))
		{
			$form->value("billing_address_phone_country",  $invoice_address["phone_country"]);
			$form->value("billing_address_phone_area",  $invoice_address["phone_area"]);
			$form->value("billing_address_phone_number",  $invoice_address["phone_number"]);
		}

		$form->value("billing_address_mobile_phone",  $invoice_address["mobile_phone"]);

		if(array_key_exists('bill_to_mobile_phone_number', $form->items))
		{
			$form->items['bill_to_mobile_phone_number']['values'][0] = $invoice_address["mobile_phone_country"];
			$form->items['bill_to_mobile_phone_number']['values'][1] = $invoice_address["mobile_phone_area"];
			$form->items['bill_to_mobile_phone_number']['values'][2] = $invoice_address["mobile_phone_number"];
		}
		elseif(array_key_exists('billing_address_street', $form->items))
		{
			$form->value("billing_address_mobile_phone_country",  $invoice_address["mobile_phone_country"]);
			$form->value("billing_address_mobile_phone_area",  $invoice_address["mobile_phone_area"]);
			$form->value("billing_address_mobile_phone_number",  $invoice_address["mobile_phone_number"]);
		}

		$form->value("billing_address_email",  $invoice_address["email"]);
		$form->value("billing_address_contact",  $invoice_address["contact_name"]);


    }
}
else if ($form->button("posaddress_id"))
{
	$sql = "select * from posaddresses where posaddress_id = " . $form->value("posaddress_id");
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$form->value("shop_address_company", $row["posaddress_name"]);
		$form->value("shop_address_address",  $row["posaddress_address"]);
		if(array_key_exists('shop_street', $form->items))
		{
			$form->items['shop_street']['values'][0] = $row["posaddress_street"];
			$form->items['shop_street']['values'][1] = $row["posaddress_street_number"];
		}
		elseif(array_key_exists('shop_street', $form->items))
		{
			$form->value("shop_address_street",  $row["posaddress_street"]);
			$form->value("shop_address_streetnumber",  $row["posaddress_street_number"]);
		}
		$form->value("shop_address_address2",  $row["posaddress_address2"]);
		$form->value("shop_address_zip",  $row["posaddress_zip"]);
		$form->value("shop_address_place",  $row["posaddress_place"]);
		$form->value("shop_address_country",  $row["posaddress_country"]);
		$form->value("shop_address_phone",  $row["posaddress_phone"]);

		if(array_key_exists('shop_phone_number', $form->items))
		{
			$form->items['shop_phone_number']['values'][0] = $row["posaddress_phone_country"];
			$form->items['shop_phone_number']['values'][1] = $row["posaddress_phone_area"];
			$form->items['shop_phone_number']['values'][2] = $row["posaddress_phone_number"];
		}
		elseif(array_key_exists('shop_phone_number', $form->items))
		{
			$form->value("shop_address_phone_country",  $row["posaddress_phone_country"]);
			$form->value("shop_address_phone_area",  $row["posaddress_phone_area"]);
			$form->value("shop_address_phone_number",  $row["posaddress_phone_number"]);

		}

		$form->value("shop_address_mobile_phone",  $row["posaddress_mobile_phone"]);

		if(array_key_exists('shop_mobile_phone_number', $form->items))
		{
			$form->items['shop_mobile_phone_number']['values'][0] = $row["posaddress_mobile_phone_country"];
			$form->items['shop_mobile_phone_number']['values'][1] = $row["posaddress_mobile_phone_area"];
			$form->items['shop_mobile_phone_number']['values'][2] = $row["posaddress_mobile_phone_number"];
		}
		elseif(array_key_exists('shop_mobile_phone_number', $form->items))
		{
			$form->value("shop_address_mobile_phone_country",  $row["posaddress_mobile_phone_country"]);
			$form->value("shop_address_mobile_phone_area",  $row["posaddress_mobile_phone_area"]);
			$form->value("shop_address_mobile_phone_number",  $row["posaddress_mobile_phone_number"]);

		}


		$form->value("shop_address_email",  $row["posaddress_email"]);
	}
}
else if ($form->button("save"))
{
	// add validation ruels

    if($project["project_projectkind"] != 4 and $project["project_projectkind"] != 5) // take over, lease renewal
	{
		if (!$form->value("location_type") and !$form->value("location_type_other"))
		{
			$form->add_validation("{location_type}", "The location type must not be empty.");
		}
	}


	if(array_key_exists('bill_to_phone_number', $form->items))
	{
		$form->value("billing_address_phone", $form->unify_multi_edit_field($form->items["bill_to_phone_number"]));

	}
			
	
	if(array_key_exists('bill_to_mobile_phone_number', $form->items))
	{
		$form->value("billing_address_mobile_phone", $form->unify_multi_edit_field($form->items["bill_to_mobile_phone_number"]));

		$form->add_validation("{billing_address_phone} != '' or {billing_address_mobile_phone} != ''", "Invoice address: Please indcate either the phone number or the mobile phone number!");
	}



	if(array_key_exists('shop_phone_number', $form->items))
	{
		$form->value("shop_address_phone", $form->unify_multi_edit_field($form->items["shop_phone_number"]));
	}

	if(array_key_exists('shop_mobile_phone_number', $form->items))
	{
		$form->value("shop_address_mobile_phone", $form->unify_multi_edit_field($form->items["shop_mobile_phone_number"]));
		//$form->add_validation("{shop_address_phone} != '' or {shop_address_mobile_phone} != ''", "POS address: Please indcate either the phone number or the mobile phone number!");
	}

	
	if(array_key_exists('delivery_phone_number', $form->items))
	{
		$form->value("delivery_address_phone", $form->unify_multi_edit_field($form->items["delivery_phone_number"]));
	}
	
	if(array_key_exists('delivery_mobile_phone_number', $form->items))
	{
		$form->value("delivery_address_mobile_phone", $form->unify_multi_edit_field($form->items["delivery_mobile_phone_number"]));

		$form->add_validation("{delivery_address_phone} != '' or {delivery_address_mobile_phone} != ''", "POS address: Please indcate either the phone number or the mobile phone number!");
	}


	if($form->value("project_projectkind") != 6 
		and $form->value("project_projectkind") != 9
		and $form->value("project_projectkind") > 1 
		and $form->value("posaddress_id") == 0 
		and $old_projectkind == 1)
	{

		if($form->value("project_projectkind") == 2)
		{
			$form->error("You have changed the project type from NEW to RENOVATION. You must assign an existing POS Location to the project.");
		}
		elseif($form->value("project_projectkind") == 7)
		{
			$form->error("You have changed the project type from NEW to Equip Retailer. You must assign an existing Independent Retailer to the project.");
		}
		elseif($form->value("project_projectkind") == 3)
		{
			$form->error("You have changed the project type from NEW to TAKE OVER/RENOVATION. You must assign an existing POS Location to the project.");
		}
		elseif($form->value("project_projectkind") == 4)
		{
			$form->error("You have changed the project type from NEW to TAKE OVER. You must assign an existing POS Location to the project.");
		}
	}
    elseif ($form->validate())
    {

		
				
		if(array_key_exists('bill_to_street', $form->items))
		{
			$form->value("billing_address_address", $form->unify_multi_edit_field($form->items["bill_to_street"], get_country_street_number_rule($form->value("billing_address_country"))));
		}
		
		
		
		if(array_key_exists('shop_street', $form->items))
		{
			$form->value("shop_address_address", $form->unify_multi_edit_field($form->items["shop_street"], get_country_street_number_rule($form->value("shop_address_country"))));
		}

				
		if(array_key_exists('delivery_street', $form->items))
		{
			$form->value("delivery_address_address", $form->unify_multi_edit_field($form->items["delivery_street"], get_country_street_number_rule($form->value("delivery_address_country"))));
		}


	   
	   if($form->value("unit_of_measurement") == 1)
	   {
			$form->value("unit_of_measurement", 2);
			$form->value("requested_store_retailarea", feet_to_sqm($form->value("requested_store_retailarea")));
			//$form->value("store_grosssurface", feet_to_sqm($form->value("store_grosssurface")));
			$form->value("store_totalsurface", feet_to_sqm($form->value("store_totalsurface")));
			$form->value("store_retailarea", feet_to_sqm($form->value("store_retailarea")));
			$form->value("store_backoffice", feet_to_sqm($form->value("store_backoffice")));
			$form->value("store_floorsurface1", feet_to_sqm($form->value("store_floorsurface1")));
			$form->value("store_floorsurface2", feet_to_sqm($form->value("store_floorsurface2")));
			$form->value("store_floorsurface3", feet_to_sqm($form->value("store_floorsurface3")));
			
	   }
		
		if($old_project_number != $form->value('project_number')) {

				
			//project tracking
			$field = "project_number";
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote("project_number") . ", " . 
				   dbquote($old_project_number ) . ", " . 
				   dbquote($form->value('project_number')) . ", " . 
				   dbquote('changed') . ", " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
				   
			$result = mysql_query($sql) or dberror($sql);
		}

		if($old_projectkind != $form->value('project_projectkind')) {

				
			//project tracking
			$field = "project_real_opening_date";
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote("project_projectkind") . ", " . 
				   dbquote($old_projectkind ) . ", " . 
				   dbquote($form->value('project_projectkind')) . ", " . 
				   dbquote('changed') . ", " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
				   
			$result = mysql_query($sql) or dberror($sql);
		}

		if($old_pos_type != $form->value('project_postype')) {

				
			//project tracking
			$field = "project_real_opening_date";
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote("project_postype") . ", " . 
				   dbquote($old_pos_type ) . ", " . 
				   dbquote($form->value('project_postype')) . ", " . 
				   dbquote('changed') . ", " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
				   
			$result = mysql_query($sql) or dberror($sql);
		}

		if($old_legal_type != $form->value('project_cost_type')) {

				
			//project tracking
			$field = "project_real_opening_date";
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote("project_cost_type") . ", " . 
				   dbquote($old_legal_type ) . ", " . 
				   dbquote($form->value('project_cost_type')) . ", " . 
				   dbquote('changed') . ", " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
				   
			$result = mysql_query($sql) or dberror($sql);
		}


		if($old_order_state != $form->value('status')) {

				
			//project tracking
			$field = "project_real_opening_date";
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote("order_actual_order_state_code") . ", " . 
				   dbquote($old_order_state ) . ", " . 
				   dbquote($form->value('status')) . ", " . 
				   dbquote('manually changed in edit request') . ", " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
				   
			$result = mysql_query($sql) or dberror($sql);
		}

		if($old_development_state != $form->value('dstatus')) {

				
			//project tracking
			$field = "project_real_opening_date";
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote("order_actual_order_state_code") . ", " . 
				   dbquote($old_development_state ) . ", " . 
				   dbquote($form->value('dstatus')) . ", " . 
				   dbquote('manually changed in edit request') . ", " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
				   
			$result = mysql_query($sql) or dberror($sql);
		}


		if($old_logistic_state != $form->value('lstatus')) {

				
			//project tracking
			$field = "project_real_opening_date";
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote("order_actual_order_state_code") . ", " . 
				   dbquote($old_logistic_state ) . ", " . 
				   dbquote($form->value('lstatus')) . ", " . 
				   dbquote('manually changed in edit request') . ", " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
				   
			$result = mysql_query($sql) or dberror($sql);
		}

		if($old_client_user > 0 
			and $old_client_user != $form->value('client_address_user_id')) {
			$field = "order_user";
			$old_tmp = get_user($old_client_user );
			$new_tmp = get_user($form->value('client_address_user_id'));
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote($field) . ", " . 
				   dbquote($old_tmp["name"] . ' ' . $old_tmp["firstname"]) . ", " . 
				   dbquote($new_tmp["name"] . ' ' . $new_tmp["firstname"]) . ", " .  
				   "'Projectowner changed', " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
				   
			$result = mysql_query($sql) or dberror($sql);
		}


		
		if(($old_projectkind == 2 and $form->value("project_projectkind") == 1)
			or ($old_projectkind == 2 and $form->value("project_projectkind") == 6)
			or ($old_projectkind == 2 and $form->value("project_projectkind") == 9)
			) // project type from renovation to new or from renovation to relocation or from renovation to takover/relocation
		{
			$old_posaddress_id = $form->value("posaddress_id");

			//insert into posaddress pipeline
			$fields = array();
			$values = array();

			$fields[] = "posaddress_client_id";
			$values[] = dbquote($project["order_client_address"]);

			$fields[] = "posaddress_ownertype";
			$values[] = dbquote($project["project_cost_type"]);

			$fields[] = "posaddress_franchisor_id";
			$values[] = dbquote($pos_data["posaddress_franchisor_id"]);

			$fields[] = "posaddress_franchisee_id";
			$values[] = dbquote($project["order_franchisee_address_id"]);

			$fields[] = "posaddress_name";
			$values[] = dbquote($project["order_shop_address_company"]);

			$fields[] = "posaddress_name2";
			$values[] = dbquote($project["order_shop_address_company2"]);

			$fields[] = "posaddress_address";
			$values[] = dbquote($project["order_shop_address_address"]);

			$fields[] = "posaddress_street";
			$values[] = dbquote($project["order_shop_address_street"]);

			$fields[] = "posaddress_street_number";
			$values[] = dbquote($project["order_shop_address_streetnumber"]);

			$fields[] = "posaddress_address2";
			$values[] = dbquote($project["order_shop_address_address2"]);

			$fields[] = "posaddress_zip";
			$values[] = dbquote($project["order_shop_address_zip"]);

			$fields[] = "posaddress_place";
			$values[] = dbquote($project["order_shop_address_place"]);

			$fields[] = "posaddress_place_id";
			$values[] = dbquote($pos_data["posaddress_place_id"]);

			$fields[] = "posaddress_country";
			$values[] = dbquote($project["order_shop_address_country"]);
			
			$fields[] = "posaddress_phone";
			$values[] = dbquote($project["order_shop_address_phone"]);

			$fields[] = "posaddress_phone_country";
			$values[] = dbquote($project["order_shop_address_phone_country"]);

			$fields[] = "posaddress_phone_area";
			$values[] = dbquote($project["order_shop_address_phone_area"]);

			$fields[] = "posaddress_phone_number";
			$values[] = dbquote($project["order_shop_address_phone_number"]);

			$fields[] = "posaddress_mobile_phone";
			$values[] = dbquote($project["order_shop_address_mobile_phone"]);

			$fields[] = "posaddress_mobile_phone_country";
			$values[] = dbquote($project["order_shop_address_mobile_phone_country"]);

			$fields[] = "posaddress_mobile_phone_area";
			$values[] = dbquote($project["order_shop_address_mobile_phone_area"]);

			$fields[] = "posaddress_mobile_phone_number";
			$values[] = dbquote($project["order_shop_address_mobile_phone_number"]);

			$fields[] = "posaddress_email";
			$values[] = dbquote($project["order_shop_address_email"]);

			$fields[] = "posaddress_store_postype";
			$values[] = dbquote($project["project_postype"]);

			$fields[] = "posaddress_store_subclass";
			$values[] = dbquote($project["project_pos_subclass"]);

			$fields[] = "posaddress_store_furniture";
			$values[] = dbquote($project["project_product_line"]);

			//$fields[] = "posaddress_store_grosssurface";
			//$values[] = dbquote($project["project_cost_grosssqms"]);

			$fields[] = "posaddress_store_totalsurface";
			$values[] = dbquote($project["project_cost_totalsqms"]);

			$fields[] = "posaddress_store_retailarea";
			$values[] = dbquote($project["project_cost_sqms"]);

			$fields[] = "posaddress_store_backoffice";
			$values[] = dbquote($project["project_cost_backofficesqms"]);

			$fields[] = "posaddress_store_numfloors";
			$values[] = dbquote($project["project_cost_numfloors"]);

			$fields[] = "posaddress_store_floorsurface1";
			$values[] = dbquote($project["project_cost_floorsurface1"]);

			$fields[] = "posaddress_store_floorsurface2";
			$values[] = dbquote($project["project_cost_floorsurface2"]);

			$fields[] = "posaddress_store_floorsurface3";
			$values[] = dbquote($project["project_cost_floorsurface3"]);

			$fields[] = "posaddress_fagagreement_type";
			$values[] = dbquote($project["project_fagagreement_type"]);

			$fields[] = "posaddress_fagrsent";
			$values[] = dbquote($project["project_fagrsent"]);

			$fields[] = "posaddress_fagrsigned";
			$values[] = dbquote($project["project_fagrsigned"]);

			$fields[] = "posaddress_fagrstart";
			$values[] = dbquote($project["project_fagrstart"]);

			$fields[] = "posaddress_fagrend";
			$values[] = dbquote($project["project_fagrend"]);

			$fields[] = "posaddress_fag_comment";
			$values[] = dbquote($project["project_fag_comment"]);


			$fields[] = "posaddress_google_lat";
			$values[] = dbquote($pos_data["posaddress_google_lat"]);
			
			$fields[] = "posaddress_google_long";
			$values[] = dbquote($pos_data["posaddress_google_long"]);

			$fields[] = "posaddress_google_precision";
			$values[] = 1;

			$fields[] = "posaddress_perc_class";
			$values[] = dbquote($pos_data["posaddress_perc_class"]);

			$fields[] = "posaddress_perc_tourist";
			$values[] = dbquote($pos_data["posaddress_perc_tourist"]);

			$fields[] = "posaddress_perc_transport";
			$values[] = dbquote($pos_data["posaddress_perc_transport"]);

			$fields[] = "posaddress_perc_people";
			$values[] = dbquote($pos_data["posaddress_perc_people"]);

			$fields[] = "posaddress_perc_parking";
			$values[] = dbquote($pos_data["posaddress_perc_parking"]);

			$fields[] = "posaddress_perc_visibility1";
			$values[] = dbquote($pos_data["posaddress_perc_visibility1"]);

			$fields[] = "posaddress_perc_visibility2";
			$values[] = dbquote($pos_data["posaddress_perc_visibility2"]);

			$fields[] = "posaddress_neighbour_left";
			$values[] = dbquote($pos_data["posaddress_neighbour_left"]);

			$fields[] = "posaddress_neighbour_right";
			$values[] = dbquote($pos_data["posaddress_neighbour_right"]);

			$fields[] = "posaddress_neighbour_acrleft";
			$values[] = dbquote($pos_data["posaddress_neighbour_acrleft"]);

			$fields[] = "posaddress_neighbour_acrright";
			$values[] = dbquote($pos_data["posaddress_neighbour_acrright"]);

			$fields[] = "posaddress_neighbour_brands";
			$values[] = dbquote($pos_data["posaddress_neighbour_brands"]);

			$fields[] = "posaddress_neighbour_comment";
			$values[] = dbquote($pos_data["posaddress_neighbour_comment"]);

			$fields[] = "user_created";
			$values[] = dbquote(user_login());

			$fields[] = "date_created";
			$values[] = dbquote(date("y-m-d H:i:s"));

			$sql = "insert into posaddressespipeline (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			mysql_query($sql) or dberror($sql);

			$new_posaddress_id = mysql_insert_id();
			
			
			//insert into posorderspipeline

			$sql = "select * from posorders where posorder_order = " . $project["project_order"];
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{

				$old_posorder_id = $row["posorder_id"];
				$fields = array();
				$values = array();

				$fields[] = "posorder_parent_table";
				$values[] = dbquote("posaddressespipeline");

				$fields[] = "posorder_posaddress";
				$values[] = dbquote($new_posaddress_id);

				$fields[] = "posorder_order";
				$values[] = dbquote($row["posorder_order"]);

				$fields[] = "posorder_type";
				$values[] = dbquote($row["posorder_type"]);

				$fields[] = "posorder_ordernumber";
				$values[] = dbquote($row["posorder_ordernumber"]);

				$fields[] = "posorder_year";
				$values[] = dbquote($row["posorder_year"]);

				$fields[] = "posorder_product_line";
				$values[] = dbquote($row["posorder_product_line"]);

				$fields[] = "posorder_product_line_subclass";
				$values[] = dbquote($row["posorder_product_line_subclass"]);

				$fields[] = "posorder_postype";
				$values[] = dbquote($row["posorder_postype"]);

				$fields[] = "posorder_subclass";
				$values[] = dbquote($row["posorder_subclass"]);

				$fields[] = "posorder_project_kind";
				$values[] = dbquote($row["posorder_project_kind"]);

				$fields[] = "posorder_legal_type";
				$values[] = dbquote($row["posorder_legal_type"]);

				$fields[] = "posorder_system_currency";
				$values[] = dbquote($row["posorder_system_currency"]);

				$fields[] = "posorder_client_currency";
				$values[] = dbquote($row["posorder_client_currency"]);

				$fields[] = "posorder_neighbour_left";
				$values[] = dbquote($row["posorder_neighbour_left"]);

				$fields[] = "posorder_neighbour_right";
				$values[] = dbquote($row["posorder_neighbour_right"]);

				$fields[] = "posorder_neighbour_acrleft";
				$values[] = dbquote($row["posorder_neighbour_acrleft"]);

				$fields[] = "posorder_neighbour_acrright";
				$values[] = dbquote($row["posorder_neighbour_acrright"]);

				$fields[] = "posorder_neighbour_brands";
				$values[] = dbquote($row["posorder_neighbour_brands"]);

				$fields[] = "posorder_neighbour_comment";
				$values[] = dbquote($row["posorder_neighbour_comment"]);


				$fields[] = "posorder_currency_symbol";
				$values[] = dbquote($row["posorder_currency_symbol"]);

				$fields[] = "posorder_exchangerate";
				$values[] = dbquote($row["posorder_exchangerate"]);

				$fields[] = "posorder_remark";
				$values[] = dbquote($row["posorder_remark"]);

				$fields[] = "posorder_project_locally_produced";
				$values[] = dbquote($row["posorder_project_locally_produced"]);

				$fields[] = "posorder_project_type_subclass_id";
				$values[] = dbquote($row["posorder_project_type_subclass_id"]);

				$fields[] = "posorder_furniture_type_store";
				$values[] = dbquote($row["posorder_furniture_type_store"]);

				$fields[] = "posorder_furniture_type_sis";
				$values[] = dbquote($row["posorder_furniture_type_sis"]);

				$fields[] = "posorder_uses_icedunes_visuals";
				$values[] = dbquote($row["posorder_uses_icedunes_visuals"]);

				$fields[] = "date_created";
				$values[] = dbquote(date("Y-m-d H:i:s"));

				$fields[] = "date_modified";
				$values[] = dbquote(date("Y-m-d H:i:s"));

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$fields[] = "user_modified";
				$values[] = dbquote(user_login());
				
				$sql = "insert into posorderspipeline (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);

				$new_posorder_id = mysql_insert_id();
			
				//insert into posareaspipeline
				$sql_t = "select * from posareas where posarea_posaddress = " . $old_posaddress_id;
				$res_t = mysql_query($sql_t) or dberror($sql_t);
				while ($row_t = mysql_fetch_assoc($res_t))
				{
					$sql_i = "Insert into posareaspipeline (" .
							 "posarea_posaddress, posarea_area, user_created, date_created, user_modified, date_modified " . 
							 ") values (" .
							 $new_posaddress_id . ", " . 
							 $row_t["posarea_area"]  . ", " .
							 dbquote($row_t["user_created"])  . ", " .
							 dbquote($row_t["date_created"])  . ", " .
							 dbquote(user_login())  . ", " .
							 "current_timestamp)";

					$result = mysql_query($sql_i) or dberror($sql_i);
				
				}

				//insert to posleasepipeline
				$sql_t = "select * from posleases where poslease_posaddress = " . $old_posaddress_id . 
					     " and poslease_order = " . $project["project_order"];
				$res_t = mysql_query($sql_t) or dberror($sql_t);
				if ($row_t = mysql_fetch_assoc($res_t))
				{
					$sql_i = "Insert into posleasespipeline (" .
							 "poslease_posaddress, poslease_order, poslease_lease_type, poslease_landlord_name, " . 
							 "poslease_negotiator, poslease_negotiated_conditions, " . 
							 "poslease_estate_agent, poslease_estate_address, poslease_startdate, poslease_enddate, " . 
							 "poslease_extensionoption, poslease_exitoption, poslease_isindexed, " . 
						     "poslease_tacit_renewal_duration_years, poslease_tacit_renewal_duration_months, " . 
						     "poslease_indexrate, poslease_average_increase, poslease_realestate_fee, " . 
						     "poslease_annual_charges, poslease_other_fees, " . 
					         "poslease_resignation_deadline, poslease_nexttermin_date, poslease_termin_penalty, " . 
							 "poslease_anual_rent, poslease_addcharges, poslease_currency, poslease_salespercent, " . 
						     " poslease_indexclause_in_contract, poslease_termination_time, poslease_mailalert, " .
						     "poslease_handoverdate,  poslease_firstrentpayed, poslease_freeweeks, " . 
							 "user_created, date_created " . 
							 ") values (" .
							 $new_posaddress_id . ", " .
						     $project["project_order"] . ", " . 
							 dbquote($row_t["poslease_lease_type"])  . ", " .
							 dbquote($row_t["poslease_landlord_name"])  . ", " .
							 dbquote($row_t["poslease_negotiator"])  . ", " .
							 dbquote($row_t["poslease_negotiated_conditions"])  . ", " .
							 dbquote($row_t["poslease_estate_agent"])  . ", " .
							 dbquote($row_t["poslease_estate_address"])  . ", " .
							 dbquote($row_t["poslease_startdate"])  . ", " .
							 dbquote($row_t["poslease_enddate"])  . ", " .
							 dbquote($row_t["poslease_extensionoption"])  . ", " .
							 dbquote($row_t["poslease_exitoption"])  . ", " .
						     dbquote($row_t["poslease_isindexed"])  . ", " .
						     dbquote($row_t["poslease_tacit_renewal_duration_years"])  . ", " .
						     dbquote($row_t["poslease_tacit_renewal_duration_months"])  . ", " .
						     dbquote($row_t["poslease_indexrate"])  . ", " .
						     dbquote($row_t["poslease_average_increase"])  . ", " .
						     dbquote($row_t["poslease_realestate_fee"])  . ", " .
						     dbquote($row_t["poslease_annual_charges"])  . ", " .
						     dbquote($row_t["poslease_other_fees"])  . ", " .
						     dbquote($row_t["poslease_resignation_deadline"])  . ", " .
							 dbquote($row_t["poslease_nexttermin_date"])  . ", " .
							 dbquote($row_t["poslease_termin_penalty"])  . ", " .
							 dbquote($row_t["poslease_anual_rent"])  . ", " .
						     dbquote($row_t["poslease_addcharges"])  . ", " .
							 dbquote($row_t["poslease_currency"])  . ", " .
							 dbquote($row_t["poslease_salespercent"])  . ", " .
							 dbquote($row_t["poslease_indexclause_in_contract"])  . ", " .
						     dbquote($row_t["poslease_termination_time"])  . ", " .
						     dbquote($row_t["poslease_mailalert"])  . ", " .
						     dbquote($row_t["poslease_handoverdate"])  . ", " .
						     dbquote($row_t["poslease_firstrentpayed"])  . ", " .
						     dbquote($row_t["poslease_freeweeks"])  . ", " .
							 dbquote(user_login())  . ", " .
							 "current_timestamp)";

					$result = mysql_query($sql_i) or dberror($sql_i);
				
				}

				//delete from posorders
				$sql_d = "delete from posorders where posorder_posaddress = " . $old_posaddress_id . 
					     " and posorder_order = " . $project["project_order"];
				$result = mysql_query($sql_d) or dberror($sql_d);

				
				//delete from posleases
				$sql_d = "delete from posleases where poslease_posaddress = " . $old_posaddress_id . 
					     " and poslease_order = " . $project["project_order"];
				$result = mysql_query($sql_d) or dberror($sql_d);

			}
			
		}
		elseif($form->value("project_projectkind") > 1 
			and $old_projectkind == 1 
			and $form->value("project_projectkind") !=6
			and $form->value("project_projectkind") !=9) // project type from new to renovation
		{
			
			//remove project from pipeline
			$sql = "select * from posorderspipeline where posorder_order = " . param("oid");
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$posorder_posaddress_id = $row["posorder_posaddress"];
				$posorder_id = $row["posorder_id"];

				//transfer posareas
				$sql_d = "delete from posareas where posarea_posaddress = " . $form->value("posaddress_id");
				$result = mysql_query($sql_d) or dberror($sql_d);

				$sql_t = "select * from posareaspipeline where posarea_posaddress = " . $posorder_posaddress_id;
				$res_t = mysql_query($sql_t) or dberror($sql_t);
				while ($row_t = mysql_fetch_assoc($res_t))
				{
					$sql_i = "Insert into posareas (" .
							 "posarea_posaddress, posarea_area, user_created, date_created, user_modified, date_modified " . 
							 ") values (" .
							 $form->value("posaddress_id") . ", " . 
							 $row_t["posarea_area"]  . ", " .
							 dbquote($row_t["user_created"])  . ", " .
							 dbquote($row_t["date_created"])  . ", " .
							 dbquote(user_login())  . ", " .
							 "current_timestamp)";

					$result = mysql_query($sql_i) or dberror($sql_i);
				
				}

				$sql_d = "delete from posareaspipeline where posarea_posaddress = " . $posorder_posaddress_id;
				$result = mysql_query($sql_d) or dberror($sql_d);


				//transfer lease data
				$sql_t = "select * from posleasespipeline where poslease_order = " . $project["project_order"];
				$res_t = mysql_query($sql_t) or dberror($sql_t);
				if ($row_t = mysql_fetch_assoc($res_t))
				{
					$sql_i = "Insert into posleases (" .
							 "poslease_posaddress, poslease_order, poslease_lease_type, poslease_landlord_name, " . 
							 "poslease_negotiator, poslease_negotiated_conditions, " . 
							 "poslease_estate_agent, poslease_estate_address, poslease_startdate, poslease_enddate, " . 
							 "poslease_extensionoption, poslease_exitoption, poslease_isindexed, " .
						     "poslease_tacit_renewal_duration_years, poslease_tacit_renewal_duration_months, " .
						     "poslease_indexrate, poslease_average_increase, poslease_realestate_fee, " . 
						     "poslease_annual_charges, poslease_other_fees, " . 
							 "poslease_resignation_deadline, poslease_nexttermin_date, poslease_termin_penalty, " . 
							 "poslease_anual_rent, poslease_addcharges, poslease_currency, poslease_salespercent, " . 
						     " poslease_indexclause_in_contract, poslease_termination_time, poslease_mailalert, " .
						     "poslease_handoverdate,  poslease_firstrentpayed, poslease_freeweeks, " . 
							 "user_created, date_created " . 
							 ") values (" .
							 $form->value("posaddress_id") . ", " . 
						     $project["project_order"] . ", " . 
							 dbquote($row_t["poslease_lease_type"])  . ", " .
							 dbquote($row_t["poslease_landlord_name"])  . ", " .
							 dbquote($row_t["poslease_negotiator"])  . ", " .
							 dbquote($row_t["poslease_negotiated_conditions"])  . ", " .
							 dbquote($row_t["poslease_estate_agent"])  . ", " .
							 dbquote($row_t["poslease_estate_address"])  . ", " .
							 dbquote($row_t["poslease_startdate"])  . ", " .
							 dbquote($row_t["poslease_enddate"])  . ", " .
							 dbquote($row_t["poslease_extensionoption"])  . ", " .
							 dbquote($row_t["poslease_exitoption"])  . ", " .
						     dbquote($row_t["poslease_isindexed"])  . ", " .
						     dbquote($row_t["poslease_tacit_renewal_duration_years"])  . ", " .
						     dbquote($row_t["poslease_tacit_renewal_duration_months"])  . ", " .
						     dbquote($row_t["poslease_indexrate"])  . ", " .
						     dbquote($row_t["poslease_average_increase"])  . ", " .
						     dbquote($row_t["poslease_realestate_fee"])  . ", " .
						     dbquote($row_t["poslease_annual_charges"])  . ", " .
						     dbquote($row_t["poslease_other_fees"])  . ", " .
						     dbquote($row_t["poslease_resignation_deadline"])  . ", " .
							 dbquote($row_t["poslease_nexttermin_date"])  . ", " .
							 dbquote($row_t["poslease_termin_penalty"])  . ", " .
							 dbquote($row_t["poslease_anual_rent"])  . ", " .
						     dbquote($row_t["poslease_addcharges"])  . ", " .
							 dbquote($row_t["poslease_currency"])  . ", " .
							 dbquote($row_t["poslease_salespercent"])  . ", " .
							 dbquote($row_t["poslease_indexclause_in_contract"])  . ", " .
						     dbquote($row_t["poslease_termination_time"])  . ", " .
						     dbquote($row_t["poslease_mailalert"])  . ", " .
						     dbquote($row_t["poslease_handoverdate"])  . ", " .
						     dbquote($row_t["poslease_firstrentpayed"])  . ", " .
						     dbquote($row_t["poslease_freeweeks"])  . ", " .
							 dbquote(user_login())  . ", " .
							 "current_timestamp)";
					
					$result = mysql_query($sql_i) or dberror($sql_i);
					
					
				
				}

							

				$sql_d = "delete from posleasespipeline where poslease_order = " . $project["project_order"];
				$result = mysql_query($sql_d) or dberror($sql_d);

				//create pos order
				$sql_p = "select * from posorderspipeline where posorder_id = " . $posorder_id;
				$res_p = mysql_query($sql_p) or dberror($sql_p);
				if ($row_p = mysql_fetch_assoc($res_p))
				{
				
					$posorder_fields = array();
					$posorder_values = array();
					
					$posorder_fields[] = "posorder_posaddress";
					$posorder_values[] = $form->value("posaddress_id");

					$posorder_fields[] = "posorder_order";
					$posorder_values[] = $form->value("oid");

					$posorder_fields[] = "posorder_type";
					$posorder_values[] = 1;

					$posorder_fields[] = "posorder_ordernumber";
					$posorder_values[] = dbquote($form->value("project_number"));

					$posorder_fields[] = "posorder_system_currency";
					$posorder_values[] = dbquote($row_p["posorder_system_currency"]);

					$posorder_fields[] = "posorder_budget_approved_sc";
					$posorder_values[] = dbquote($row_p["posorder_budget_approved_sc"]);

					$posorder_fields[] = "posorder_real_cost_sc";
					$posorder_values[] = dbquote($row_p["posorder_real_cost_sc"]);

					$posorder_fields[] = "posorder_client_currency";
					$posorder_values[] = dbquote($row_p["posorder_client_currency"]);

					$posorder_fields[] = "posorder_budget_approved_cc";
					$posorder_values[] = dbquote($row_p["posorder_budget_approved_cc"]);

					$posorder_fields[] = "posorder_real_cost_cc";
					$posorder_values[] = dbquote($row_p["posorder_real_cost_cc"]);

					$posorder_fields[] = "posorder_opening_date";
					$posorder_values[] = dbquote($row_p["posorder_opening_date"]);

					$posorder_fields[] = "posorder_closing_date";
					$posorder_values[] = dbquote($row_p["posorder_closing_date"]);

					$posorder_fields[] = "posorder_neighbour_left";
					$posorder_values[] = dbquote($row_p["posorder_neighbour_left"]);

					$posorder_fields[] = "posorder_neighbour_right";
					$posorder_values[] = dbquote($row_p["posorder_neighbour_right"]);

					$posorder_fields[] = "posorder_neighbour_acrleft";
					$posorder_values[] = dbquote($row_p["posorder_neighbour_acrleft"]);

					$posorder_fields[] = "posorder_neighbour_acrright";
					$posorder_values[] = dbquote($row_p["posorder_neighbour_acrright"]);

					$posorder_fields[] = "posorder_neighbour_brands";
					$posorder_values[] = dbquote($row_p["posorder_neighbour_brands"]);

					$posorder_fields[] = "posorder_neighbour_comment";
					$posorder_values[] = dbquote($row_p["posorder_neighbour_comment"]);

					$posorder_fields[] = "posorder_currency_symbol";
					$posorder_values[] = dbquote($row_p["posorder_currency_symbol"]);

					$posorder_fields[] = "posorder_exchangerate";
					$posorder_values[] = dbquote($row_p["posorder_exchangerate"]);

					$posorder_fields[] = "posorder_remark";
					$posorder_values[] = dbquote($row_p["posorder_remark"]);

					$posorder_fields[] = "posorder_project_locally_produced";
					$posorder_values[] = dbquote($row_p["posorder_project_locally_produced"]);

					$posorder_fields[] = "posorder_project_type_subclass_id";
					$posorder_values[] = dbquote($row_p["posorder_project_type_subclass_id"]);

					$posorder_fields[] = "user_created";
					$posorder_values[] = dbquote($row_p["user_created"]);

					$posorder_fields[] = "date_created";
					$posorder_values[] = dbquote($row_p["date_created"]);

					$posorder_fields[] = "date_modified";
					$posorder_values[] = "current_timestamp";

					$posorder_fields[] = "user_modified";
					$posorder_values[] = dbquote(user_login());

					$sql = "insert into posorders (" . join(", ", $posorder_fields) . ") values (" . join(", ", $posorder_values) . ")";
					$result = mysql_query($sql) or dberror($sql);

					$sql_d = "delete from posorderspipeline where posorder_posaddress = " . $posorder_posaddress_id;
					$result = mysql_query($sql_d) or dberror($sql_d);
				}

				
				//update posaddress
				$posaddress_fields = array();

				$sql_p = "select * from posaddressespipeline where posaddress_id = " . $posorder_posaddress_id;
				$res_p = mysql_query($sql_p) or dberror($sql_p);
				if ($row_p = mysql_fetch_assoc($res_p))
				{

					$value = dbquote($row_p["posaddress_perc_class"]);
					$posaddress_fields[] = "posaddress_perc_class = " . $value;

					$value = dbquote($row_p["posaddress_perc_tourist"]);
					$posaddress_fields[] = "posaddress_perc_tourist = " . $value;

					$value = dbquote($row_p["posaddress_perc_transport"]);
					$posaddress_fields[] = "posaddress_perc_transport = " . $value;

					$value = dbquote($row_p["posaddress_perc_people"]);
					$posaddress_fields[] = "posaddress_perc_people = " . $value;

					$value = dbquote($row_p["posaddress_perc_parking"]);
					$posaddress_fields[] = "posaddress_perc_parking = " . $value;

					$value = dbquote($row_p["posaddress_perc_visibility1"]);
					$posaddress_fields[] = "posaddress_perc_visibility1 = " . $value;

					$value = dbquote($row_p["posaddress_perc_visibility2"]);
					$posaddress_fields[] = "posaddress_perc_visibility2 = " . $value;

					$value = dbquote($row_p["posaddress_google_lat"]);
					$posaddress_fields[] = "posaddress_google_lat = " . $value;

					$value = dbquote($row_p["posaddress_google_long"]);
					$posaddress_fields[] = "posaddress_google_long = " . $value;

					$value = dbquote($row_p["posaddress_google_precision"]);
					$posaddress_fields[] = "posaddress_google_precision = " . $value;

					$value = "current_timestamp";
					$posaddress_fields[] = "date_modified = " . $value;

					if (isset($_SESSION["user_login"]))
					{
						$value = dbquote($_SESSION["user_login"]);
						$posaddress_fields[] = "user_modified = " . $value;
					}

					$sql = "update posaddresses set " . join(", ", $posaddress_fields) . " where posaddress_id = " . $form->value("posaddress_id");
					mysql_query($sql) or dberror($sql);

					$sql_d = "delete from posaddressespipeline where posaddress_id = " . $posorder_posaddress_id;
					$result = mysql_query($sql_d) or dberror($sql_d);
				}

				//update shop address in order
				$order_fields = array();

				$value = trim($form->value("shop_address_company")) == "" ? "null" : dbquote($form->value("shop_address_company"));
				$order_fields[] = "order_shop_address_company = " . $value;

				$value = trim($form->value("shop_address_address")) == "" ? "null" : dbquote($form->value("shop_address_address"));
				$order_fields[] = "order_shop_address_address = " . $value;

				
				if(array_key_exists('shop_street', $form->items))
				{
					$value = dbquote(trim($form->items['shop_street']['values'][0]));
					$order_fields[] = "order_shop_address_street = " . $value;

					$value = dbquote(trim($form->items['shop_street']['values'][1]));
					$order_fields[] = "order_shop_address_streetnumber = " . $value;
				}
				else
				{
					$value = trim($form->value("shop_address_address")) == "" ? "null" : dbquote($form->value("shop_address_address"));
					$order_fields[] = "order_shop_address_address = " . $value;

					$value = trim($form->value("shop_address_address")) == "" ? "null" : dbquote($form->value("shop_address_address"));
					$order_fields[] = "order_shop_address_address = " . $value;
				}

				$value = trim($form->value("shop_address_address2")) == "" ? "null" : dbquote($form->value("shop_address_address2"));
				$order_fields[] = "order_shop_address_address2 = " . $value;

				$value = trim($form->value("shop_address_zip")) == "" ? "null" : dbquote($form->value("shop_address_zip"));
				$order_fields[] = "order_shop_address_zip = " . $value;

				$value = trim($form->value("shop_address_place")) == "" ? "null" : dbquote($form->value("shop_address_place"));
				$order_fields[] = "order_shop_address_place = " . $value;

				$value = trim($form->value("shop_address_country")) == "" ? "null" : dbquote($form->value("shop_address_country"));
				$order_fields[] = "order_shop_address_country = " . $value;
				
				$value = trim($form->value("shop_address_phone")) == "" ? "null" : dbquote($form->value("shop_address_phone"));
				$order_fields[] = "order_shop_address_phone = " . $value;

				if(array_key_exists('shop_phone_number', $form->items))
				{
					$value = dbquote(trim($form->items['shop_phone_number']['values'][0]));
					$order_fields[] = "order_shop_address_phone_country = " . $value;

					$value = dbquote(trim($form->items['shop_phone_number']['values'][1]));
					$order_fields[] = "order_shop_address_phone_area = " . $value;

					$value = dbquote(trim($form->items['shop_phone_number']['values'][2]));
					$order_fields[] = "order_shop_address_phone_number = " . $value;
				}
				else
				{
					$value = trim($form->value("shop_address_phone_country")) == "" ? "null" : dbquote($form->value("shop_address_phone_country"));
					$order_fields[] = "order_shop_address_phone_country = " . $value;

					$value = trim($form->value("shop_address_phone_area")) == "" ? "null" : dbquote($form->value("shop_address_phone_area"));
					$order_fields[] = "order_shop_address_phone_area = " . $value;

					$value = trim($form->value("shop_address_phone_number")) == "" ? "null" : dbquote($form->value("shop_address_phone_number"));
					$order_fields[] = "order_shop_address_phone_number = " . $value;
				}

				
				$value = trim($form->value("shop_address_mobile_phone")) == "" ? "null" : dbquote($form->value("shop_address_mobile_phone"));
				$order_fields[] = "order_shop_address_mobile_phone = " . $value;


				if(array_key_exists('shop_mobile_phone_number', $form->items))
				{
					$value = dbquote(trim($form->items['shop_mobile_phone_number']['values'][0]));
					$order_fields[] = "order_shop_address_mobile_phone_country = " . $value;

					$value = dbquote(trim($form->items['shop_mobile_phone_number']['values'][1]));
					$order_fields[] = "order_shop_address_mobile_phone_area = " . $value;

					$value = dbquote(trim($form->items['shop_mobile_phone_number']['values'][2]));
					$order_fields[] = "order_shop_address_mobile_phone_number = " . $value;
				}
				else
				{
					$value = trim($form->value("shop_address_mobile_phone_country")) == "" ? "null" : dbquote($form->value("shop_address_mobile_phone_country"));
					$order_fields[] = "order_shop_address_mobile_phone_country = " . $value;

					$value = trim($form->value("shop_address_mobile_phone_area")) == "" ? "null" : dbquote($form->value("shop_address_mobile_phone_area"));
					$order_fields[] = "order_shop_address_mobile_phone_area = " . $value;

					$value = trim($form->value("shop_address_mobile_phone_number")) == "" ? "null" : dbquote($form->value("shop_address_mobile_phone_number"));
					$order_fields[] = "order_shop_address_mobile_phone_number = " . $value;
				}
				
				$value = trim($form->value("shop_address_email")) == "" ? "null" : dbquote($form->value("shop_address_email"));
				$order_fields[] = "order_shop_address_email = " . $value;

				$value = "current_timestamp";
				$order_fields[] = "date_modified = " . $value;

				if (isset($_SESSION["user_login"]))
				{
					$value = dbquote($_SESSION["user_login"]);
					$order_fields[] = "user_modified = " . $value;
				}

				$sql = "update orders set " . join(", ", $order_fields) . " where order_id = " . $form->value("oid");
				mysql_query($sql) or dberror($sql);


			}
		}
		elseif($form->value("project_projectkind") > 1 
			and ($old_projectkind == 6 or $old_projectkind == 9) 
			and $form->value("project_projectkind") ==2) // project type from relocation to renovation
		{
			
			//remove project from pipeline
			$pos_id = $project["project_relocated_posaddress_id"];
			$sql = "select * from posorderspipeline where posorder_order = " . param("oid");
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$posorder_posaddress_id = $row["posorder_posaddress"];
				$posorder_id = $row["posorder_id"];

				//transfer posareas
				$sql_d = "delete from posareas where posarea_posaddress = " . $form->value("posaddress_id");
				$result = mysql_query($sql_d) or dberror($sql_d);

				$sql_t = "select * from posareaspipeline where posarea_posaddress = " . $posorder_posaddress_id;
				$res_t = mysql_query($sql_t) or dberror($sql_t);
				while ($row_t = mysql_fetch_assoc($res_t))
				{
					$sql_i = "Insert into posareas (" .
							 "posarea_posaddress, posarea_area, user_created, date_created, user_modified, date_modified " . 
							 ") values (" .
							 $pos_id . ", " . 
							 $row_t["posarea_area"]  . ", " .
							 dbquote($row_t["user_created"])  . ", " .
							 dbquote($row_t["date_created"])  . ", " .
							 dbquote(user_login())  . ", " .
							 "current_timestamp)";

					$result = mysql_query($sql_i) or dberror($sql_i);
				
				}

				$sql_d = "delete from posareaspipeline where posarea_posaddress = " . $posorder_posaddress_id;
				$result = mysql_query($sql_d) or dberror($sql_d);


				//transfer lease data
				$sql_t = "select * from posleasespipeline where poslease_order = " . $project["project_order"];
				$res_t = mysql_query($sql_t) or dberror($sql_t);
				if ($row_t = mysql_fetch_assoc($res_t))
				{
					$sql_i = "Insert into posleases (" .
							 "poslease_posaddress, poslease_order, poslease_lease_type, poslease_landlord_name, " . 
							 "poslease_negotiator, poslease_negotiated_conditions, " . 
							 "poslease_estate_agent, poslease_estate_address, poslease_startdate, poslease_enddate, " . 
							 "poslease_extensionoption, poslease_exitoption, poslease_isindexed, " .
						     "poslease_tacit_renewal_duration_years, poslease_tacit_renewal_duration_months, " .
							 "poslease_indexrate, poslease_average_increase, poslease_realestate_fee, " . 
							 "poslease_annual_charges, poslease_other_fees, " . 
							 "poslease_resignation_deadline, poslease_nexttermin_date, poslease_termin_penalty, " . 
							 "poslease_anual_rent, poslease_addcharges, poslease_currency, poslease_salespercent, " . 
							 " poslease_indexclause_in_contract, poslease_termination_time, poslease_mailalert, " .
							 "poslease_handoverdate,  poslease_firstrentpayed, poslease_freeweeks, " . 
							 "user_created, date_created " . 
							 ") values (" .
							 $pos_id . ", " . 
							 $project["project_order"] . ", " . 
							 dbquote($row_t["poslease_lease_type"])  . ", " .
							 dbquote($row_t["poslease_landlord_name"])  . ", " .
							 dbquote($row_t["poslease_negotiator"])  . ", " .
							 dbquote($row_t["poslease_negotiated_conditions"])  . ", " .
							 dbquote($row_t["poslease_estate_agent"])  . ", " .
							 dbquote($row_t["poslease_estate_address"])  . ", " .
							 dbquote($row_t["poslease_startdate"])  . ", " .
							 dbquote($row_t["poslease_enddate"])  . ", " .
							 dbquote($row_t["poslease_extensionoption"])  . ", " .
							 dbquote($row_t["poslease_exitoption"])  . ", " .
							 dbquote($row_t["poslease_isindexed"])  . ", " .
						     dbquote($row_t["poslease_tacit_renewal_duration_years"])  . ", " .
						     dbquote($row_t["poslease_tacit_renewal_duration_months"])  . ", " .
							 dbquote($row_t["poslease_indexrate"])  . ", " .
							 dbquote($row_t["poslease_average_increase"])  . ", " .
							 dbquote($row_t["poslease_realestate_fee"])  . ", " .
							 dbquote($row_t["poslease_annual_charges"])  . ", " .
							 dbquote($row_t["poslease_other_fees"])  . ", " .
							 dbquote($row_t["poslease_resignation_deadline"])  . ", " .
							 dbquote($row_t["poslease_nexttermin_date"])  . ", " .
							 dbquote($row_t["poslease_termin_penalty"])  . ", " .
							 dbquote($row_t["poslease_anual_rent"])  . ", " .
							 dbquote($row_t["poslease_addcharges"])  . ", " .
							 dbquote($row_t["poslease_currency"])  . ", " .
							 dbquote($row_t["poslease_salespercent"])  . ", " .
							 dbquote($row_t["poslease_indexclause_in_contract"])  . ", " .
							 dbquote($row_t["poslease_termination_time"])  . ", " .
							 dbquote($row_t["poslease_mailalert"])  . ", " .
							 dbquote($row_t["poslease_handoverdate"])  . ", " .
							 dbquote($row_t["poslease_firstrentpayed"])  . ", " .
							 dbquote($row_t["poslease_freeweeks"])  . ", " .
							 dbquote(user_login())  . ", " .
							 "current_timestamp)";
					
					$result = mysql_query($sql_i) or dberror($sql_i);
					
					
				
				}

							

				$sql_d = "delete from posleasespipeline where poslease_order = " . $project["project_order"];
				$result = mysql_query($sql_d) or dberror($sql_d);

				//create pos order
				$sql_p = "select * from posorderspipeline where posorder_id = " . $posorder_id;
				$res_p = mysql_query($sql_p) or dberror($sql_p);
				if ($row_p = mysql_fetch_assoc($res_p))
				{
				
					$posorder_fields = array();
					$posorder_values = array();
					
					$posorder_fields[] = "posorder_posaddress";
					$posorder_values[] = $pos_id;

					$posorder_fields[] = "posorder_order";
					$posorder_values[] = $form->value("oid");

					$posorder_fields[] = "posorder_type";
					$posorder_values[] = 1;

					$posorder_fields[] = "posorder_ordernumber";
					$posorder_values[] = dbquote($form->value("project_number"));

					$posorder_fields[] = "posorder_system_currency";
					$posorder_values[] = dbquote($row_p["posorder_system_currency"]);

					$posorder_fields[] = "posorder_budget_approved_sc";
					$posorder_values[] = dbquote($row_p["posorder_budget_approved_sc"]);

					$posorder_fields[] = "posorder_real_cost_sc";
					$posorder_values[] = dbquote($row_p["posorder_real_cost_sc"]);

					$posorder_fields[] = "posorder_client_currency";
					$posorder_values[] = dbquote($row_p["posorder_client_currency"]);

					$posorder_fields[] = "posorder_budget_approved_cc";
					$posorder_values[] = dbquote($row_p["posorder_budget_approved_cc"]);

					$posorder_fields[] = "posorder_real_cost_cc";
					$posorder_values[] = dbquote($row_p["posorder_real_cost_cc"]);

					$posorder_fields[] = "posorder_opening_date";
					$posorder_values[] = dbquote($row_p["posorder_opening_date"]);

					$posorder_fields[] = "posorder_closing_date";
					$posorder_values[] = dbquote($row_p["posorder_closing_date"]);

					$posorder_fields[] = "posorder_neighbour_left";
					$posorder_values[] = dbquote($row_p["posorder_neighbour_left"]);

					$posorder_fields[] = "posorder_neighbour_right";
					$posorder_values[] = dbquote($row_p["posorder_neighbour_right"]);

					$posorder_fields[] = "posorder_neighbour_acrleft";
					$posorder_values[] = dbquote($row_p["posorder_neighbour_acrleft"]);

					$posorder_fields[] = "posorder_neighbour_acrright";
					$posorder_values[] = dbquote($row_p["posorder_neighbour_acrright"]);

					$posorder_fields[] = "posorder_neighbour_brands";
					$posorder_values[] = dbquote($row_p["posorder_neighbour_brands"]);

					$posorder_fields[] = "posorder_neighbour_comment";
					$posorder_values[] = dbquote($row_p["posorder_neighbour_comment"]);

					$posorder_fields[] = "posorder_currency_symbol";
					$posorder_values[] = dbquote($row_p["posorder_currency_symbol"]);

					$posorder_fields[] = "posorder_exchangerate";
					$posorder_values[] = dbquote($row_p["posorder_exchangerate"]);

					$posorder_fields[] = "posorder_remark";
					$posorder_values[] = dbquote($row_p["posorder_remark"]);

					$posorder_fields[] = "posorder_project_locally_produced";
					$posorder_values[] = dbquote($row_p["posorder_project_locally_produced"]);

					$posorder_fields[] = "posorder_project_type_subclass_id";
					$posorder_values[] = dbquote($row_p["posorder_project_type_subclass_id"]);

					$posorder_fields[] = "user_created";
					$posorder_values[] = dbquote($row_p["user_created"]);

					$posorder_fields[] = "date_created";
					$posorder_values[] = dbquote($row_p["date_created"]);

					$posorder_fields[] = "date_modified";
					$posorder_values[] = "current_timestamp";

					$posorder_fields[] = "user_modified";
					$posorder_values[] = dbquote(user_login());

					$sql = "insert into posorders (" . join(", ", $posorder_fields) . ") values (" . join(", ", $posorder_values) . ")";
					$result = mysql_query($sql) or dberror($sql);

					$sql_d = "delete from posorderspipeline where posorder_posaddress = " . $posorder_posaddress_id;
					$result = mysql_query($sql_d) or dberror($sql_d);
				}

				
				//update posaddress
				$posaddress_fields = array();

				$sql_p = "select * from posaddressespipeline where posaddress_id = " . $posorder_posaddress_id;
				$res_p = mysql_query($sql_p) or dberror($sql_p);
				if ($row_p = mysql_fetch_assoc($res_p))
				{

					$value = dbquote($row_p["posaddress_perc_class"]);
					$posaddress_fields[] = "posaddress_perc_class = " . $value;

					$value = dbquote($row_p["posaddress_perc_tourist"]);
					$posaddress_fields[] = "posaddress_perc_tourist = " . $value;

					$value = dbquote($row_p["posaddress_perc_transport"]);
					$posaddress_fields[] = "posaddress_perc_transport = " . $value;

					$value = dbquote($row_p["posaddress_perc_people"]);
					$posaddress_fields[] = "posaddress_perc_people = " . $value;

					$value = dbquote($row_p["posaddress_perc_parking"]);
					$posaddress_fields[] = "posaddress_perc_parking = " . $value;

					$value = dbquote($row_p["posaddress_perc_visibility1"]);
					$posaddress_fields[] = "posaddress_perc_visibility1 = " . $value;

					$value = dbquote($row_p["posaddress_perc_visibility2"]);
					$posaddress_fields[] = "posaddress_perc_visibility2 = " . $value;

					$value = dbquote($row_p["posaddress_google_lat"]);
					$posaddress_fields[] = "posaddress_google_lat = " . $value;

					$value = dbquote($row_p["posaddress_google_long"]);
					$posaddress_fields[] = "posaddress_google_long = " . $value;

					$value = dbquote($row_p["posaddress_google_precision"]);
					$posaddress_fields[] = "posaddress_google_precision = " . $value;

					$value = "current_timestamp";
					$posaddress_fields[] = "date_modified = " . $value;

					if (isset($_SESSION["user_login"]))
					{
						$value = dbquote($_SESSION["user_login"]);
						$posaddress_fields[] = "user_modified = " . $value;
					}

					$sql = "update posaddresses set " . join(", ", $posaddress_fields) . " where posaddress_id = " . $pos_id;
					mysql_query($sql) or dberror($sql);

					$sql_d = "delete from posaddressespipeline where posaddress_id = " . $posorder_posaddress_id;
					$result = mysql_query($sql_d) or dberror($sql_d);
				}

				//update shop address in order
				$order_fields = array();

				$value = trim($form->value("shop_address_company")) == "" ? "null" : dbquote($form->value("shop_address_company"));
				$order_fields[] = "order_shop_address_company = " . $value;

				$value = trim($form->value("shop_address_address")) == "" ? "null" : dbquote($form->value("shop_address_address"));
				$order_fields[] = "order_shop_address_address = " . $value;

				if(array_key_exists('shop_street', $form->items))
				{
					$value = dbquote(trim($form->items['shop_street']['values'][0]));
					$order_fields[] = "order_shop_address_street = " . $value;

					$value = dbquote(trim($form->items['shop_street']['values'][1]));
					$order_fields[] = "order_shop_address_streetnumber = " . $value;
				}
				else
				{
					$value = trim($form->value("shop_address_address")) == "" ? "null" : dbquote($form->value("shop_address_address"));
					$order_fields[] = "order_shop_address_address = " . $value;

					$value = trim($form->value("shop_address_address")) == "" ? "null" : dbquote($form->value("shop_address_address"));
					$order_fields[] = "order_shop_address_address = " . $value;
				}

				$value = trim($form->value("shop_address_address2")) == "" ? "null" : dbquote($form->value("shop_address_address2"));
				$order_fields[] = "order_shop_address_address2 = " . $value;

				$value = trim($form->value("shop_address_zip")) == "" ? "null" : dbquote($form->value("shop_address_zip"));
				$order_fields[] = "order_shop_address_zip = " . $value;

				$value = trim($form->value("shop_address_place")) == "" ? "null" : dbquote($form->value("shop_address_place"));
				$order_fields[] = "order_shop_address_place = " . $value;

				$value = trim($form->value("shop_address_country")) == "" ? "null" : dbquote($form->value("shop_address_country"));
				$order_fields[] = "order_shop_address_country = " . $value;
				
				$value = trim($form->value("shop_address_phone")) == "" ? "null" : dbquote($form->value("shop_address_phone"));
				$order_fields[] = "order_shop_address_phone = " . $value;

				if(array_key_exists('shop_phone_number', $form->items))
				{
					$value = dbquote(trim($form->items['shop_phone_number']['values'][0]));
					$order_fields[] = "order_shop_address_phone_country = " . $value;

					$value = dbquote(trim($form->items['shop_phone_number']['values'][1]));
					$order_fields[] = "order_shop_address_phone_area = " . $value;

					$value = dbquote(trim($form->items['shop_phone_number']['values'][2]));
					$order_fields[] = "order_shop_address_phone_number = " . $value;
				}
				else
				{
					$value = trim($form->value("shop_address_phone_country")) == "" ? "null" : dbquote($form->value("shop_address_phone_country"));
					$order_fields[] = "order_shop_address_phone_country = " . $value;

					$value = trim($form->value("shop_address_phone_area")) == "" ? "null" : dbquote($form->value("shop_address_phone_area"));
					$order_fields[] = "order_shop_address_phone_area = " . $value;

					$value = trim($form->value("shop_address_phone_number")) == "" ? "null" : dbquote($form->value("shop_address_phone_number"));
					$order_fields[] = "order_shop_address_phone_number = " . $value;
				}
				
				$value = trim($form->value("shop_address_mobile_phone")) == "" ? "null" : dbquote($form->value("shop_address_mobile_phone"));
				$order_fields[] = "order_shop_address_mobile_phone = " . $value;

				if(array_key_exists('shop_mobile_phone_number', $form->items))
				{
					$value = dbquote(trim($form->items['shop_mobile_phone_number']['values'][0]));
					$order_fields[] = "order_shop_address_mobile_phone_country = " . $value;

					$value = dbquote(trim($form->items['shop_mobile_phone_number']['values'][1]));
					$order_fields[] = "order_shop_address_mobile_phone_area = " . $value;

					$value = dbquote(trim($form->items['shop_mobile_phone_number']['values'][2]));
					$order_fields[] = "order_shop_address_mobile_phone_number = " . $value;
				}
				else
				{
					$value = trim($form->value("shop_address_mobile_phone_country")) == "" ? "null" : dbquote($form->value("shop_address_mobile_phone_country"));
					$order_fields[] = "order_shop_address_mobile_phone_country = " . $value;

					$value = trim($form->value("shop_address_mobile_phone_area")) == "" ? "null" : dbquote($form->value("shop_address_mobile_phone_area"));
					$order_fields[] = "order_shop_address_mobile_phone_area = " . $value;

					$value = trim($form->value("shop_address_mobile_phone_number")) == "" ? "null" : dbquote($form->value("shop_address_mobile_phone_number"));
					$order_fields[] = "order_shop_address_mobile_phone_number = " . $value;
				}
				
				$value = trim($form->value("shop_address_email")) == "" ? "null" : dbquote($form->value("shop_address_email"));
				$order_fields[] = "order_shop_address_email = " . $value;

				$value = "current_timestamp";
				$order_fields[] = "date_modified = " . $value;

				if (isset($_SESSION["user_login"]))
				{
					$value = dbquote($_SESSION["user_login"]);
					$order_fields[] = "user_modified = " . $value;
				}

				$sql = "update orders set " . join(", ", $order_fields) . " where order_id = " . $form->value("oid");
				mysql_query($sql) or dberror($sql);


			}
		}

		/*
		elseif($form->value("project_projectkind") > 1 
			and $old_projectkind == 1 
			and $form->value("project_projectkind") == 7) // project type from new to equip retailer
		{
			
			//update posorderpipeline
			$fields = array();
			
			
			$value = trim($form->value("posaddress_id")) == "" ? "null" : dbquote($form->value("posaddress_id"));
			$fields[] = "posorder_posaddress = " . $value;

			$value = trim($form->value("project_projectkind")) == "" ? "null" : dbquote($form->value("project_projectkind"));
			$fields[] = "posorder_project_kind = " . $value;

			$value = trim($form->value("project_number")) == "" ? "null" : dbquote($form->value("project_number"));
			$fields[] = "posorder_ordernumber = " . $value;

			$value = trim($form->value("project_postype")) == "" ? "null" : dbquote($form->value("project_postype"));
			$fields[] = "posorder_postype = " . $value;

			$value = trim($form->value("project_pos_subclass")) == "" ? "null" : dbquote($form->value("project_pos_subclass"));
			$fields[] = "posorder_subclass = " . $value;

			$value = trim($form->value("project_projectkind")) == "" ? "null" : dbquote($form->value("project_projectkind"));
			$fields[] = "posorder_project_kind = " . $value;

			$value = trim($form->value("project_cost_type")) == "" ? "null" : dbquote($form->value("project_cost_type"));
			$fields[] = "posorder_legal_type = " . $value;

			$sql = "update posorderspipeline set " . join(", ", $fields) . " where posorder_order = " . $form->value("oid");
			mysql_query($sql) or dberror($sql);


			

			//update shop address in order
			$order_fields = array();

			$value = trim($form->value("shop_address_company")) == "" ? "null" : dbquote($form->value("shop_address_company"));
			$order_fields[] = "order_shop_address_company = " . $value;

			$value = trim($form->value("shop_address_address")) == "" ? "null" : dbquote($form->value("shop_address_address"));
			$order_fields[] = "order_shop_address_address = " . $value;

			$value = trim($form->value("shop_address_address2")) == "" ? "null" : dbquote($form->value("shop_address_address2"));
			$order_fields[] = "order_shop_address_address2 = " . $value;

			$value = trim($form->value("shop_address_zip")) == "" ? "null" : dbquote($form->value("shop_address_zip"));
			$order_fields[] = "order_shop_address_zip = " . $value;

			$value = trim($form->value("shop_address_place")) == "" ? "null" : dbquote($form->value("shop_address_place"));
			$order_fields[] = "order_shop_address_place = " . $value;

			$value = trim($form->value("shop_address_country")) == "" ? "null" : dbquote($form->value("shop_address_country"));
			$order_fields[] = "order_shop_address_country = " . $value;
			
			$value = trim($form->value("shop_address_phone")) == "" ? "null" : dbquote($form->value("shop_address_phone"));
			$order_fields[] = "order_shop_address_phone = " . $value;
			
			$value = trim($form->value("shop_address_mobile_phone")) == "" ? "null" : dbquote($form->value("shop_address_mobile_phone"));
			$order_fields[] = "order_shop_address_mobile_phone = " . $value;
			
			$value = trim($form->value("shop_address_email")) == "" ? "null" : dbquote($form->value("shop_address_email"));
			$order_fields[] = "order_shop_address_email = " . $value;

			$value = "current_timestamp";
			$order_fields[] = "date_modified = " . $value;

			if (isset($_SESSION["user_login"]))
			{
				$value = dbquote($_SESSION["user_login"]);
				$order_fields[] = "user_modified = " . $value;
			}

			$sql = "update orders set " . join(", ", $order_fields) . " where order_id = " . $form->value("oid");
			mysql_query($sql) or dberror($sql);


			
		}
		*/
		
		project_update_client_data($form,$design_objectives_listbox_names,$design_objectives_checklist_names);

		
		//send email notifications on change of project number
		if($old_project_number != $form->value('project_number')) {

				//update table old project numbers
				$sql = "Insert into oldproject_numbers (".
					   "oldproject_number_project_id, oldproject_number_user_id, " . 
					   "oldproject_number_old_number, oldproject_number_new_number, " . 
					   "user_created, date_created) VALUES (" .
					   $project["project_id"] . ', ' .
					   user_id() . ', ' .
					   dbquote($old_project_number) . ', ' .
					   dbquote($form->value('project_number')) . ', ' .
					   dbquote(user_login()) . ', ' .
					   dbquote(date("Y-m-d H:i:s")) . ')';
				$res = mysql_query($sql) or dberror($sql);   
				
				
				$sql = "select user_id, user_email, concat(user_name, ' ', user_firstname) as username ".
					   "from users ".
					   "where (user_id = '" . user_id() . "' " . 
					   "   and user_active = 1)";
				
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				
				$sender_id = $row["user_id"];
				$sender_email = $row["user_email"];
				$sender_name = $row["username"];

				//recipients
				$subject = "Project number was changed - Project " . $old_project_number . '/' . $form->value('project_number') . ", " . $project["order_shop_address_country_name"] . ", " . $project["order_shop_address_company"];
				

				if($old_pos_type != $form->value("project_postype")) {
					
					$sql = "select postype_name from postypes where postype_id = " . $old_pos_type;
					$res = mysql_query($sql) or dberror($sql);
					$row = mysql_fetch_assoc($res);
					$old_pos_type_name = $row["postype_name"];


					$sql = "select postype_name from postypes where postype_id = " . $form->value("project_postype");
					$res = mysql_query($sql) or dberror($sql);
					$row = mysql_fetch_assoc($res);
					$new_pos_type_name = $row["postype_name"];
					
					$bodytext0 = $sender_name . " has changed the project number from " . $old_project_number . " to " . $form->value('project_number') . ". The POS Type has changed from " . $old_pos_type_name . " to " . $new_pos_type_name . ".";
				}
				else
				{
					$bodytext0 = $sender_name . " has changed the project number from " . $old_project_number . " to " . $form->value('project_number') . ".";
				}

				$link ="project_view_client_data.php?pid=" . param("pid");
				$bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
				$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";   

				//project Leader
				$reciepients = array();
				$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
					   "from users ".
					   "where (user_id = " . dbquote($project['project_retail_coordinator']) . 
					   "   and user_active = 1)";
				
				$res1 = mysql_query($sql) or dberror($sql);
				if ($row1 = mysql_fetch_assoc($res1))
				{
					$reciepients[$row1["user_id"]] = strtolower($row1["user_email"]);
					$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
					$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
				}


				//retail operator
				/*
				$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
					   "from users ".
					   "where (user_id = " . dbquote($project['order_retail_operator']) . 
					   "   and user_active = 1)";
				
				$res1 = mysql_query($sql) or dberror($sql);
				if ($row1 = mysql_fetch_assoc($res1))
				{
					$reciepients[$row1["user_id"]] = strtolower($row1["user_email"]);
					$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
					$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
				}
				*/


				//design_contractor
				if($project['project_design_contractor'] and $project['order_actual_order_state_code'] < '620') {
					$send_mail = true;
					$project_state_restrictions = get_project_state_restrictions($project['project_design_contractor']);

				
					if($project_state_restrictions['from_state'] or $project_state_restrictions['to_state'])
					{
						if($project_state_restrictions['from_state'] and $project['order_actual_order_state_code'] < $project_state_restrictions['from_state'])
						{
							$send_mail = false;
						}
						if($project_state_restrictions['to_state'] and $project['order_actual_order_state_code'] > $project_state_restrictions['to_state'])
						{
							$send_mail = false;
						}
					}
					

					if($send_mail == true)
					{
						$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
							   "from users ".
							   "where (user_id = " . dbquote($project['project_design_contractor']) . 
							   "   and user_active = 1)";
						
						$res1 = mysql_query($sql) or dberror($sql);
						if ($row1 = mysql_fetch_assoc($res1))
						{
							$reciepients[$row1["user_id"]] = strtolower($row1["user_email"]);
							$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
							$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
						}
					}
				}


				//design_supervisor
				if($project['project_design_supervisor']) {
					$send_mail = true;
					$project_state_restrictions = get_project_state_restrictions($project['project_design_supervisor']);

				
					if($project_state_restrictions['from_state'] or $project_state_restrictions['to_state'])
					{
						if($project_state_restrictions['from_state'] and $project['order_actual_order_state_code'] < $project_state_restrictions['from_state'])
						{
							$send_mail = false;
						}
						if($project_state_restrictions['to_state'] and $project['order_actual_order_state_code'] > $project_state_restrictions['to_state'])
						{
							$send_mail = false;
						}
					}
					

					if($send_mail == true)
					{
						$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
							   "from users ".
							   "where (user_id = " . dbquote($project['project_design_supervisor']) . 
							   "   and user_active = 1)";
						
						$res1 = mysql_query($sql) or dberror($sql);
						if ($row1 = mysql_fetch_assoc($res1))
						{
							$reciepients[$row1["user_id"]] = strtolower($row1["user_email"]);
							$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
							$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
						}
					}
				}
				

				//client
				$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
					   "from users ".
					   "where (user_id = " . dbquote($project['order_user']) . 
					   "   and user_active = 1)";
				
				$res1 = mysql_query($sql) or dberror($sql);
				if ($row1 = mysql_fetch_assoc($res1))
				{
					$reciepients[$row1["user_id"]] = strtolower($row1["user_email"]);
					$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
					$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
				}


				//retail supervising team
				$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
					   "from supervisingteam " . 
					   "left join users on user_id = supervisingteam_user ".
					   "where user_active = 1";
				
				$res1 = mysql_query($sql) or dberror($sql);
				while ($row1 = mysql_fetch_assoc($res1))
				{
					$reciepients[$row1["user_id"]] = strtolower($row1["user_email"]);
					$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
					$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
				}

				
				$result = 0;
				foreach($reciepients as $user_id=>$user_email) {
					
					$mail = new Mail();
					$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
					$mail->set_sender($sender_email, $sender_name);
					$mail->add_text($bodytext);
					$mail->add_recipient($user_email);

					
					if($senmail_activated == true)
					{
						$result = $mail->send();
					}
					else
					{
						$result = 1;
					}
				}

				if($result == 1)
				{
					foreach($reciepients as $user_id=>$user_email) {
						append_mail($project["project_order"], $user_id, $sender_id, $bodytext0, "", 1);
					}
				}
		}


        $form->message("Your changes have been saved.");
    }
}

/*

else if ($form->button("delivery_address_id"))
{
    // set delivery address

    $form->value("delivery_address_company", "");
    $form->value("delivery_address_company2",  "");
    $form->value("delivery_address_address",  "");
    $form->value("delivery_address_address2",  "");
    $form->value("delivery_address_zip",  "");
    $form->value("delivery_address_place",  "");
    $form->value("delivery_address_country",  0);
    $form->value("delivery_address_phone",  "");
    $form->value("delivery_address_mobile_phone",  "");
    $form->value("delivery_address_email",  "");
    $form->value("delivery_address_contact",  "");


    if ($form->value("delivery_address_id"))
    {
       $sql = "select * from order_addresses where order_address_id = " . $form->value("delivery_address_id");
        $res = mysql_query($sql) or dberror($sql);

        if ($row = mysql_fetch_assoc($res))
        {
            $form->value("delivery_address_company", $row["order_address_company"]);
            $form->value("delivery_address_company2",  $row["order_address_company2"]);
            $form->value("delivery_address_address",  $row["order_address_address"]);
            $form->value("delivery_address_address2",  $row["order_address_address2"]);
            $form->value("delivery_address_zip",  $row["order_address_zip"]);
            $form->value("delivery_address_place",  $row["order_address_place"]);
            $form->value("delivery_address_country",  $row["order_address_country"]);
            $form->value("delivery_address_phone",  $row["order_address_phone"]);
            $form->value("delivery_address_mobile_phone",  $row["order_address_mobile_phone"]);
            $form->value("delivery_address_email",  $row["order_address_email"]);
            $form->value("delivery_address_contact",  $row["order_address_contact"]);
        }
    }   
}

else if ($form->button("project_cost_type"))
{
	if($form->value("project_cost_type") == 2) // Franchisee Store
	{
		$form->value("franchisee_address_company", "");
		$form->value("franchisee_address_company2",  "");
		$form->value("franchisee_address_address",  "");
		$form->value("franchisee_address_address2",  "");
		$form->value("franchisee_address_zip",  "");
		$form->value("franchisee_address_place",  "");
		$form->value("franchisee_address_country",  0);
		$form->value("franchisee_address_phone",  "");
		$form->value("franchisee_address_mobile_phone",  "");
		$form->value("franchisee_address_email",  "");
		$form->value("franchisee_address_contact",  "");
	}
}
else if ($form->button("franchisee_address_id"))
{
    
	if($form->value("project_cost_type") == 2) // Franchisee Store
	{
		// set new franchisee address
		$form->value("franchisee_address_company", "");
		$form->value("franchisee_address_company2",  "");
		$form->value("franchisee_address_address",  "");
		$form->value("franchisee_address_address2",  "");
		$form->value("franchisee_address_zip",  "");
		$form->value("franchisee_address_place",  "");
		$form->value("franchisee_address_country",  0);
		$form->value("franchisee_address_phone",  "");
		$form->value("franchisee_address_mobile_phone",  "");
		$form->value("franchisee_address_email",  "");
		$form->value("franchisee_address_contact",  "");

		if ($form->value("franchisee_address_id"))
		{
			$sql = "select * from orders where order_id = " . $form->value("franchisee_address_id");
			$res = mysql_query($sql) or dberror($sql);

			if ($row = mysql_fetch_assoc($res))
			{
				$form->value("franchisee_address_company", $row["order_franchisee_address_company"]);
				$form->value("franchisee_address_company2",  $row["order_franchisee_address_company2"]);
				$form->value("franchisee_address_address",  $row["order_franchisee_address_address"]);
				$form->value("franchisee_address_address2",  $row["order_franchisee_address_address2"]);
				$form->value("franchisee_address_zip",  $row["order_franchisee_address_zip"]);
				$form->value("franchisee_address_place",  $row["order_franchisee_address_place"]);
				$form->value("franchisee_address_country",  $row["order_franchisee_address_country"]);
				$form->value("franchisee_address_phone",  $row["order_franchisee_address_phone"]);
				$form->value("franchisee_address_mobile_phone",  $row["order_franchisee_address_mobile_phone"]);
				$form->value("franchisee_address_email",  $row["order_franchisee_address_email"]);
				$form->value("franchisee_address_contact",  $row["order_franchisee_address_contact"]);
			}
		}
	}
}
elseif ($form->button("billing_address_id"))
{
    // set new billing address

    $form->value("billing_address_company", "");
    $form->value("billing_address_company2",  "");
    $form->value("billing_address_address",  "");
    $form->value("billing_address_address2",  "");
    $form->value("billing_address_zip",  "");
    $form->value("billing_address_place",  "");
    $form->value("billing_address_country",  0);
    $form->value("billing_address_phone",  "");
    $form->value("billing_address_mobile_phone",  "");
    $form->value("billing_address_email",  "");
    $form->value("billing_address_contact",  "");

    if ($form->value("billing_address_id"))
    {
        $sql = "select * from orders where order_id = " . $form->value("billing_address_id");
        $res = mysql_query($sql) or dberror($sql);

        if ($row = mysql_fetch_assoc($res))
        {
            $form->value("billing_address_company", $row["order_billing_address_company"]);
            $form->value("billing_address_company2",  $row["order_billing_address_company2"]);
            $form->value("billing_address_address",  $row["order_billing_address_address"]);
            $form->value("billing_address_address2",  $row["order_billing_address_address2"]);
            $form->value("billing_address_zip",  $row["order_billing_address_zip"]);
            $form->value("billing_address_place",  $row["order_billing_address_place"]);
            $form->value("billing_address_country",  $row["order_billing_address_country"]);
            $form->value("billing_address_phone",  $row["order_billing_address_phone"]);
            $form->value("billing_address_mobile_phone",  $row["order_billing_address_mobile_phone"]);
            $form->value("billing_address_email",  $row["order_billing_address_email"]);
            $form->value("billing_address_contact",  $row["order_billing_address_contact"]);
        }
    }
}
*/

    
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Edit Request");
$form->render();
?>

<script language="Javascript">

	function round_decimal(num,decimals){
		return Math.round(num*Math.pow(10,decimals))/Math.pow(10,decimals);
	}


	$(document).ready(function(){
		  $("#unit_of_measurement" ).change(function() {
				
				if($("#unit_of_measurement" ).val() == 1)
				{
					$("#um00").html("<strong> sqf</strong>");
					//$("#um01").html("<strong> sqf</strong>");
					$("#um02").html("<strong> sqf</strong>");
					$("#um03").html("<strong> sqf</strong>");
					$("#um04").html("<strong> sqf</strong>");
					$("#um05").html("<strong> sqf</strong>");
					$("#um06").html("<strong> sqf</strong>");
					$("#um07").html("<strong> sqf</strong>");
					
					
					
					$("#requested_store_retailarea").val(round_decimal(10.7639104*$("#requested_store_retailarea").val(), 2));
					//$("#store_grosssurface").val(round_decimal(10.7639104*$("#store_grosssurface").val(), 2));
					$("#store_totalsurface").val(round_decimal(10.7639104*$("#store_totalsurface").val(), 2));
					$("#store_retailarea").val(round_decimal(10.7639104*$("#store_retailarea").val(), 2));
					$("#store_backoffice").val(round_decimal(10.7639104*$("#store_backoffice").val(), 2));

					$("#store_floorsurface1").val(round_decimal(10.7639104*$("#store_floorsurface1").val(), 2));
					$("#store_floorsurface2").val(round_decimal(10.7639104*$("#store_floorsurface2").val(), 2));
					$("#store_floorsurface3").val(round_decimal(10.7639104*$("#store_floorsurface3").val(), 2));
				}
				else
			    {
					$("#um00").html("<strong> sqms</strong>");
					//$("#um01").html("<strong> sqms</strong>");
					$("#um02").html("<strong> sqms</strong>");
					$("#um03").html("<strong> sqms</strong>");
					$("#um04").html("<strong> sqms</strong>");
					$("#um05").html("<strong> sqms</strong>");
					$("#um06").html("<strong> sqms</strong>");
					$("#um07").html("<strong> sqms</strong>");

					$("#requested_store_retailarea").val(round_decimal(0.09290304*$("#requested_store_retailarea").val(), 2));
					//$("#store_grosssurface").val(round_decimal(0.09290304*$("#store_grosssurface").val(), 2));
					$("#store_totalsurface").val(round_decimal(0.09290304*$("#store_totalsurface").val(), 2));
					$("#store_retailarea").val(round_decimal(0.09290304*$("#store_retailarea").val(), 2));
					$("#store_backoffice").val(round_decimal(0.09290304*$("#store_backoffice").val(), 2));

					$("#store_floorsurface1").val(round_decimal(0.09290304*$("#store_floorsurface1").val(), 2));
					$("#store_floorsurface2").val(round_decimal(0.09290304*$("#store_floorsurface2").val(), 2));
					$("#store_floorsurface3").val(round_decimal(0.09290304*$("#store_floorsurface3").val(), 2));

				}
		  });


	});

</script>


<?php
require_once "include/project_footer_logistic_state.php";
$page->footer();

?>