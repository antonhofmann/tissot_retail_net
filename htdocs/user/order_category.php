<?php

/********************************************************************

    order_category.php

    "New order" catalog browser.

    Created by:     Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2002-08-20
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-08-04
    Version:        1.0.8

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/order_functions.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_view_orders");

register_param('cid');

$cid = param('cid');
$plid = param('plid');
$item_id = param("item_id");


$searchterm = '';
if(array_key_exists('searchterm', $_GET) and $_GET['searchterm'] != '') 
{
	$searchterm = $_GET['searchterm'];
}
else
{
	$searchterm = param('searchterm');
}





/********************************************************************
    prepare all data needed
*********************************************************************/

// get the region of users' address
$user_region = get_user_region(user_id());
$roles = get_user_roles(user_id());

$user = get_user(user_id());
$user_country = $user["country"];

// get category name
if($searchterm)
{
	$category_name = "Item List matching '" . $searchterm . "'";
}
else
{
	$category_name = get_category_name($plid, $cid, param("scid"));
}


$basket_values = array();
$sql_basket = "select basket_item_item, basket_item_quantity " .
			  "from basket_items " .
			  "where basket_item_basket= " . get_users_basket();


$res = mysql_query($sql_basket);
while ($row = mysql_fetch_assoc($res))
{
	$basket_values[$row['basket_item_item']] = $row['basket_item_quantity'];
}


$sql = "select DISTINCT item_id, item_category_id, item_category_name, " . 
       " item_code, item_name,item_price, supplying_group_name ".
       " from items " .
	    " INNER JOIN item_supplying_groups ON item_id = item_supplying_group_item_id " . 
	   " INNER JOIN supplying_groups on supplying_group_id = item_supplying_group_supplying_group_id  " .
	   " INNER JOIN product_line_supplying_groups ON product_line_supplying_group_group_id = supplying_group_id " . 
	   " INNER JOIN product_lines on product_line_id = product_line_supplying_group_line_id " . 
	   " INNER JOIN productline_regions ON productline_region_productline = product_line_id ".
       " INNER join item_categories on item_category_id = item_category " .
       " INNER join item_countries on item_country_item_id = item_id ";


$sql = "select DISTINCT item_id, item_category_id, item_category_name, item_code, item_name,item_price, ".
       " concat(address_company, ' - ', item_category_name) as group_head " . 
       " from product_lines " .
	   "INNER JOIN product_line_supplying_groups ON product_line_supplying_group_line_id = product_line_id
	    INNER JOIN item_supplying_groups ON item_supplying_group_supplying_group_id = product_line_supplying_group_group_id
		INNER JOIN items ON item_id = item_supplying_group_item_id
		INNER JOIN supplying_groups on supplying_group_id = item_supplying_group_supplying_group_id 
		INNER JOIN productline_regions ON productline_region_productline = product_line_id
		INNER JOIN item_categories ON item_category_id = item_category
		LEFT JOIN item_subcategories on item_subcategory_id = item_subcategory
		INNER JOIN item_countries ON item_country_item_id = item_id 
		LEFT join item_pos_types on item_pos_type_item = item_id 
		LEFT JOIN suppliers on supplier_item = item_id
	    LEFT JOIN addresses on address_id = supplier_address ";

if(has_access("can_edit_catalog") or has_access("can_enter_orders_for_other_companies"))
{
	
    if($searchterm)
	{
		$list_filter = "(REPLACE(LOWER(item_code), ' ', '') like '%" . strtolower($searchterm) . "%' OR REPLACE(LOWER(item_name), ' ', '') like '%" .  strtolower($searchterm) . "%' OR REPLACE(LOWER(item_category_name), ' ', '') like '%" .  strtolower($searchterm) . "%' OR REPLACE(LOWER(product_line_name), ' ', '') like '%" .  strtolower($searchterm) . "%' )";


		$list_filter .= " and (product_line_visible = 1 
						 or product_line_budget = 1)
						 and product_line_active = 1
						 and item_category_active = 1 
						 and item_active = 1
						  and item_type = 1";
	}
	else
	{
		$list_filter = " item_category = " . dbquote($cid) .
					   " and product_line_id = " . dbquote($plid) .
					   " and (product_line_visible = 1 
						 or product_line_budget = 1)
						 and product_line_active = 1
						 and item_category_active = 1 
						 and item_active = 1
						  and item_type = 1";
	}
}
else
{
	
	if($searchterm)
	{
		
		$list_filter = "(REPLACE(LOWER(item_code), ' ', '') like '%" . strtolower($searchterm) . "%' OR REPLACE(LOWER(item_name), ' ', '') like '%" .  strtolower($searchterm) . "%' OR REPLACE(LOWER(item_category_name), ' ', '') like '%" .  strtolower($searchterm) . "%' OR REPLACE(LOWER(product_line_name), ' ', '') like '%" .  strtolower($searchterm) . "%' )";

		$list_filter .= " and product_line_visible = 1 
						 and product_line_active = 1
						 and item_category_active = 1
						 and item_visible = 1 
						 and item_active = 1
						 and item_type = 1 
						 and item_country_country_id = " . dbquote($user_country);
	}
	else
	{
		$list_filter = " item_category = " . dbquote($cid) .
					    " and product_line_id = " . dbquote($plid) .
					    " and product_line_visible = 1 
						 and product_line_active = 1
						 and item_category_active = 1
						 and item_visible = 1 
						 and item_active = 1
						 and item_type = 1 
						 and item_country_country_id = " . dbquote($user_country);
	}
}


if(param("scid")) {
	$list_filter .= ' and item_subcategory = ' . dbquote(param("scid"));
}
elseif(!$searchterm)
{
	//$list_filter .= ' and (item_subcategory is null or item_subcategory = 0)';
}

if(param("sg")) {
	$list_filter .= ' and item_supplying_group_supplying_group_id = ' . dbquote(param("sg"));
}


/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);

$list->set_entity("items");
$list->set_filter($list_filter);
$list->set_order("supplying_group_name, item_code");
$list->set_group("supplying_group_name");

$list->add_hidden('cid' , $cid);
$list->add_hidden('plid' , $plid);
$list->add_hidden('searchterm' , $searchterm);

$list->add_column("item_code", "Code", "/applications/templates/item.info.modal.php?id={item_id}", LIST_FILTER_FREE, "", "", "", array('class'=>'item_info_box'));
$list->add_column("item_name", "Item", "", LIST_FILTER_FREE);

$list->add_edit_column("item_quantity", "Quantity", "6", 0, $basket_values);
$list->add_button(LIST_BUTTON_BACK, "Back");
$list->add_button("basket_add", "Add to Shopping List");
$list->add_button("cancel_order", "Cancel Ordering");


$list->populate();
$list->process();


if ($list->button("basket"))
{
    redirect("basket.php");
}


if ($list->button("basket_add"))
{
	foreach($list->values("item_quantity") as $key => $value)
    {
        if ($value)
		{
			$add_this_items[$key] = $value;
		}
    }

	if($item_id > 0)
	{
		$add_this_items[$item_id] = 1;
	}
	
	if(param('searchterm') and param('searchterm') != '')
	{
		update_shopping_list($add_this_items, get_users_basket(), 'search_was_performed');
	}
	else
	{
		update_shopping_list($add_this_items, get_users_basket(), $cid);
	}




	// check, if item has addons


    $addon_items = array();
    $parent_items = $add_this_items;


    foreach($parent_items as $key => $value)
    {
        if ($value > 0)
        {
            $addon_items[] = $key;
        }
    }


    if (!empty($addon_items))
    {
        $sql_addons = "select * " .
                      "from addons " .
                      "where addon_parent in (" . join (',' , $addon_items) . ") " .
                      "group by addon_parent"; 


        unset($addon_items);
        $res = mysql_query($sql_addons);
        while ($row = mysql_fetch_assoc($res))
        {
            $addon_items[] = $row['addon_parent'];
        }
    }


    if (!empty($addon_items))
    {


        // add addons to basket


		$sql_a_items = "select DISTINCT addon_id, addon_package_quantity, item_category, " .
					   "    addon_min_packages, addon_max_packages, addon_parent, addon_child " . 
					   "from addons " .
					   "left join items on item_id = addon_parent ";


        foreach ($addon_items as $addon_item_key => $addon_item_value)
        {
            $sql = $sql_a_items . "where addon_parent = " . $addon_item_value . " " .
                   "    and item_category = " . $cid;


			$res = mysql_query($sql) or dberror($sql);
            while ($row = mysql_fetch_assoc($res))
            {
                $addon_quantity = (isset($parent_items[$row['addon_parent']])) ? 
                                  $parent_items[$row['addon_parent']]: 0 ;


                update_basket_item(get_users_basket(), $row['addon_child'], 
                                   $addon_quantity, 
                                   $addon_item_value,
                                   $row['addon_parent']);
            }
         }
    }


    
    redirect("basket.php");
}


if ($list->button("cancel_order"))
{
	redirect("orders.php");
}




$page = new Page("orders");
if (!basket_is_empty())
{
    $page->register_action('basket', 'Shopping List');
}


$page->register_action('home', 'Home', "welcome.php");


$page->header();
$page->title($category_name);
$list->render();
$page->footer();


?>