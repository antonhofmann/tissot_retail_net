<?php
/********************************************************************

    project_add_material_replacement.php

    Add item position in material replacements.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2015-06-30
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2015-06-30
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

if(!has_access("can_view_material_replacements") and !has_access("can_edit_material_replacements"))
{
	redirect("noaccess.php");
}


/********************************************************************
    prepare all data needed
*********************************************************************/
$possible_replacements = array();
$original_quantity = "";
$item_name = "";
$item_type = "";
$item_id = "";
$replacement_type = "";
$supplier_id = "";




// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);
$selected_replacement = '';
$item_was_not_ordered = true;



if(!param("order_item_replacement_order_item_id") and param("id")) //edit mode
{
	$sql = "select * from order_item_replacements " . 
		   " where order_item_replacement_id = " . dbquote(param("id"));

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		param("order_item_replacement_order_item_id", $row["order_item_replacement_order_item_id"]);
		param("replacement_item", $row["order_item_replacement_item_id"]);
		$selected_replacement = $row["order_item_replacement_item_id"];
		$replacement_type = $row["order_item_replacement_type"];

		if($row["order_item_replacement_catalog_order_id"] > 0)
		{
			$item_was_not_ordered = false;
		}
		$supplier_currency = get_address_currency($row["order_item_replacement_supplier_address"]);
	}
}



$sql_payer_name = "select replacements_payer_name_id, replacements_payer_name_name " . 
                  " from replacements_payer_names " . 
				  " order by replacements_payer_name_name";


/*
$sql_order_items = "select order_item_id, concat(order_item_text, ' - ', if(item_id > 0, item_code, 'Special Item')) as item_name " . 
                   " from order_items " .
				   " left join items on item_id = order_item_item " . 
				   " where order_item_type <= 2 " .
				   " and order_item_quantity > 0 " .
				   " and order_item_arrival is not null and order_item_arrival <> '0000-00-00' " . 
				   " and order_item_order = " . dbquote($project["project_order"]) . 
				   " order by order_item_text";

*/
$sql_order_items = "select order_item_id, concat(address_company, ': ', order_item_text, ' - ', if(item_id > 0, item_code, 'Special Item')) as item_name " . 
                   " from order_items " .
				   " left join items on item_id = order_item_item " .
				   " left join addresses on address_id = order_item_supplier_address " . 
				   " where order_item_type <= 2 " .
				   " and order_item_quantity > 0 " .
				   " and order_item_order = " . dbquote($project["project_order"]) . 
				   " order by address_company, order_item_text";


$sql_suppliers = "select address_id, address_company ".
				 "from addresses ".
				 "where address_type in (2, 8) and address_active = 1 " . " ".
				 "order by address_company";;


$sql_replacement_reasons = "select order_item_replacement_reason_id, order_item_replacement_reason_text " . 
                           " from order_item_replacement_reasons " . 
						   " order by order_item_replacement_reason_text";


$sql_warranty_types = "select order_item_warranty_type_id, order_item_warranty_type_text " . 
                           " from order_item_warranty_types " . 
						   " order by order_item_warranty_type_text";


if(param("order_item_replacement_order_item_id"))
{
	//get Item
	$sql = "select item_id, order_item_quantity, order_item_supplier_address, " . 
		   " if(item_id > 0, concat(order_item_text, ' - ', item_code), concat(order_item_text, ' - Special Item')) as item_name " . 
		   " from order_items " .
		   " left join items on item_id = order_item_item " . 
		   " where order_item_id = " . dbquote(param("order_item_replacement_order_item_id"));

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$supplier_id = $row["order_item_supplier_address"];

		$supplier_currency = get_address_currency($supplier_id);
		
		$possible_replacements['01'] = 'Full replacement of ' . $row["item_name"];
		$original_quantity = $row["order_item_quantity"];
		$possible_replacements['02'] = 'Other individual special position';


		//get spare parts
		$sql = "select item_id, concat(item_name, ' - ', item_code) as item_name " . 
			   " from parts " . 
			   " left join items on item_id = part_child " .
			   " left join suppliers on supplier_item = item_id " .
			   " left join addresses on address_id = supplier_address " . 
			   " where address_active = 1 and item_active = 1 and part_parent = " . dbquote($row["item_id"]) . 
			   " order by item_name";

		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$possible_replacements[$row["item_id"]] = 'Spare part replacement of ' . $row["item_name"];
		}


		if($replacement_type == 'full')
		{
			param("replacement_item", '01');
			$selected_replacement = '01';
		}
		elseif($replacement_type == 'other')
		{
			param("replacement_item", '02');
			$selected_replacement = '02';
		}

	}

}


if(param("order_item_replacement_supplier_address"))
{
	$supplier_currency = get_address_currency(param("order_item_replacement_supplier_address"));
}


if(param("replacement_item") and param("replacement_item") == '01') // full replacement
{
	$item_type = 2; //special item

	if(param("order_item_replacement_order_item_id"))
	{
		$sql = "select order_item_item, order_item_type, order_item_text, order_item_supplier_address " . 
				" from order_items " .
			   " left join suppliers on supplier_item = order_item_item " .
			   " where order_item_id = " . dbquote(param("order_item_replacement_order_item_id"));

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$item_name = $row["order_item_text"];
			$item_type = $row["order_item_type"];
			$item_id = $row["order_item_item"];
			$supplier_id = $row["order_item_supplier_address"];
		
		}
	}

	$replacement_type = 'full';
}
elseif(param("replacement_item") and param("replacement_item") == '02') // other replacement
{
	$item_type = 2; //special items
	if(param("order_item_replacement_order_item_id"))
	{
		$sql = "select order_item_item, order_item_type, order_item_text " . 
				" from order_items " . 
			   " where order_item_id = " . dbquote(param("order_item_replacement_order_item_id"));

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$item_name = $row["order_item_text"];
			$item_id = "";
			$supplier_id = "";
		}
		$replacement_type = 'other';
	}
}
elseif(param("replacement_item"))
{
	$item_type = 2; //special items
	$sql = "select item_id, item_name, item_type, supplier_address " . 
		   " from items " .
		   " left join suppliers on supplier_item = item_id " . 
		   " where item_id = " . dbquote(param("replacement_item"));

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$item_name = $row["item_name"];
		$item_type = $row["item_type"];
		$item_id = $row["item_id"];
		$supplier_id = $row["supplier_address"];
	}

	$replacement_type = 'sparepart';
}
else
{
	$item_type = 2; //special items
	$replacement_type = 'other';
	$supplier_id = "";
}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("order_item_replacements", "replacement of materials");

require_once "include/project_head_small.php";

$form->add_section("Related Item from the List of Materials");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("order_item_replacement_order_id", $project["project_order"]);
$form->add_hidden("order_item_replacement_item_id", $item_id);
$form->add_hidden("order_item_replacement_item_type", $item_type);
$form->add_hidden("order_item_replacement_type", $replacement_type);


if(has_access("can_edit_material_replacements") and $item_was_not_ordered == true)
{
	$form->add_list("order_item_replacement_order_item_id", "Related to Item*",
			$sql_order_items, NOTNULL | SUBMIT);


	if(param("order_item_replacement_order_item_id") or param("id") > 0)
	{
		$form->add_label("original_quantity", "Original Quantity", 0, $original_quantity);
		
		$form->add_section("Replacement Information");
		
		$form->add_list("replacement_item", "Replacement item*", $possible_replacements, SUBMIT, $selected_replacement);
		
		if(param("replacement_item") or param("id") > 0)
		{
			$form->add_list("order_item_replacement_supplier_address", "Supplier*", $sql_suppliers, NOTNULL, $supplier_id);
			
			$form->add_multiline("order_item_replacement_item_text", "Item Description*", 4, NOTNULL);
					
			$form->add_edit("order_item_replacement_quantity", "Replacement Quantity*", NOTNULL);
			
			
			$form->add_list("order_item_replacement_reason_id", "Reason for the Replacement*", $sql_replacement_reasons, NOTNULL);


			$form->add_section("Warranty Information");
			$form->add_list("order_item_replacement_warranty_type_id", "Warranty Type", $sql_warranty_types, 0);


			$form->add_section("Payment Information");

			$form->add_checkbox("order_item_replacement_item_to_be_payed", "Item has to be paid", "", 0, "Item");
			
			$form->add_list("order_item_replacement_item_to_be_payed_by", "Payment of Item by",
				$sql_payer_name, 0, "");

			$form->add_checkbox("order_item_replacement_freight_to_be_payed", "Freight charges have to be paid", "", 0, "Freight Charges");
			
			$form->add_list("order_item_replacement_ferigeht_to_be_payed_by", "Payment of Freight Charges by",
				$sql_payer_name, 0, "");

			$form->add_multiline("order_item_replacement_remarks", "Remarks", 6, 0);

			if(param("id"))
			{
				$form->add_button("delete_item", "Delete");
			}
			$form->add_button(FORM_BUTTON_BACK, "Back");
			$form->add_button(FORM_BUTTON_SAVE, "Save");
		}
	}
}
elseif(id() > 0)
{
	$form->add_lookup("order_item_replacement_order_item_id", "Related to Item", "order_items", 
                  "order_item_text",HIDEEMPTY ,id());
	
	$form->add_label("original_quantity", "Original Quantity", 0, $original_quantity);
	
	$form->add_section("Replacement Information");
	
	
	$form->add_label("replacement_item", "Replacement item", 0, $possible_replacements[$selected_replacement]);

	$form->add_lookup("order_item_replacement_supplier_address", "Supplier", "addresses", 
                  "address_company",HIDEEMPTY ,id());
	
	
	$form->add_label("order_item_replacement_item_text", "Item Description");
			
	$form->add_label("order_item_replacement_quantity", "Replacement Quantity");

	
	$form->add_lookup("order_item_replacement_reason_id", "Reason for the Replacement", "order_item_replacement_reasons", 
                  "order_item_replacement_reason_text",HIDEEMPTY ,id());


	$form->add_section("Warranty Information");
	$form->add_lookup("order_item_replacement_warranty_type_id", "Warranty Type", "order_item_warranty_types", 
                  "order_item_warranty_type_text",HIDEEMPTY ,id());

	$form->add_section("Payment Information");

	$form->add_checkbox("order_item_replacement_item_to_be_payed", "Item has to be paid", "", 0, "Item");
	
	$form->add_lookup("order_item_replacement_item_to_be_payed_by", "Payment of Item by", "replacements_payer_names", 
                  "replacements_payer_name_name",HIDEEMPTY ,id());

	$form->add_checkbox("order_item_replacement_freight_to_be_payed", "Freight charges have to be paid", "", 0, "Freight Charges");
	
	$form->add_lookup("order_item_replacement_ferigeht_to_be_payed_by", "Payment of Freight Charges by", "replacements_payer_names", 
                  "replacements_payer_name_name",HIDEEMPTY ,id());

	
	$form->add_label("order_item_replacement_remarks", "Remarks");
	$form->add_button(FORM_BUTTON_BACK, "Back");
	
}




/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


if ($form->button("replacement_item"))
{
	$form->value("order_item_replacement_item_id", $item_id);
	$form->value("order_item_replacement_item_type", $item_type);
	$form->value("order_item_replacement_item_text", $item_name);
	$form->value("order_item_replacement_type", $replacement_type);
	$form->value("order_item_replacement_supplier_address", $supplier_id);

}
elseif ($form->button(FORM_BUTTON_SAVE))
{
	if($form->value("order_item_replacement_item_to_be_payed") == 1)
	{
		$form->add_validation("{order_item_replacement_item_to_be_payed_by} != ''", "Please indicate who is to pay the replacement.");
	}

	if($form->value("order_item_replacement_freight_to_be_payed") == 1)
	{
		$form->add_validation("{order_item_replacement_ferigeht_to_be_payed_by} != ''", "Please indicate who is to pay the freight charges.");
	}
	
	if($form->validate())
	{
		$link ="project_edit_material_replacements.php?pid=" . $project["project_id"];
		redirect($link);
	}
}
elseif ($form->button("delete_item"))
{
	$sql = "delete from order_item_replacements where order_item_replacement_id = " . dbquote(id());
	$result = mysql_query($sql) or dberror($sql);
	$link ="project_edit_material_replacements.php?pid=" . $project["project_id"];
	redirect($link);
}
elseif(param("id") and $form->button("order_item_replacement_order_item_id"))
{
	$form->value("order_item_replacement_item_id", "");
	$form->value("order_item_replacement_item_type", "");
	$form->value("order_item_replacement_item_text", "");
	$form->value("order_item_replacement_type", "");
	$form->value("replacement_item", "");
	$form->value("order_item_replacement_supplier_address", "");
	$form->value("order_item_replacement_quantity", "");
	$form->value("order_item_replacement_reason", "");
	
}


   
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Replacement of Materials: Add Item");
$form->render();

require_once "include/project_footer_logistic_state.php";
$page->footer();

?>