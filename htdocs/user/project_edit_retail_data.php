<?php
/********************************************************************

    project_edit_retail_data.php

    Edit Retail Assignements.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-08
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-16
    Version:        1.0.4

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "include/order_state_constants.php";

check_access("can_edit_retail_data");

register_param("pid");
set_referer("project_confirm_to_client.php");


/********************************************************************
    prepare all data needed
*********************************************************************/

// read project and order details
$project = get_project(param("pid"));

$development_state = get_project_development_status($project["project_order"]);

/*
if(has_access("can_see_logistic_status"))
{
	$logistic_state = get_logistic_state($project["project_order"], user_id(), $project["order_actual_order_state_code"]);
	$offer_state = get_offer_state($project["project_order"], user_id(), $project["order_actual_order_state_code"]);
}
*/

if(param('project_state')) {
	$sql = "select project_state_id, project_state_text from " .
				   " project_states  where project_state_id = " . param('project_state');

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$treatment_state_text = $row["project_state_text"];
	}
}
else
{
	$treatment_state_text = $project["project_state_text"];
}

$tracking_info = array();
$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
	   "concat(user_name, ' ', user_firstname) as user_name " . 
	   "from projecttracking " . 
       "left join users on user_id = projecttracking_user_id " . 
       "where projecttracking_project_id = " . param("pid") . 
	   " and projecttracking_field = 'project_real_opening_date' " . 
	   " order by projecttracking_time";


$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$tracking_info[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
		"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
		"projecttracking_comment"=>$row["projecttracking_comment"],
		"projecttracking_time"=>$row["projecttracking_time"],
		"user_name"=>$row["user_name"]
		);
}

$tracking_info2 = array();
$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
	   "concat(user_name, ' ', user_firstname) as user_name " . 
	   "from projecttracking " . 
       "left join users on user_id = projecttracking_user_id " . 
       "where projecttracking_project_id = " . param("pid") . 
	   " and projecttracking_field = 'project_state' " . 
	   " order by projecttracking_time";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$tracking_info2[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
		"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
		"projecttracking_comment"=>$row["projecttracking_comment"],
		"projecttracking_time"=>$row["projecttracking_time"],
		"user_name"=>$row["user_name"]
		);
}


$old_product_line = $project["project_product_line"];
$old_shop_real_opening_date = $project["project_real_opening_date"];
$old_project_state = $project["project_state"];
$old_order_state = $project["order_actual_order_state_code"];
$old_budget_covered_by = $project["project_budget_covered_by"];
$old_retail_coordinator = $project["project_retail_coordinator"];
//$old_retail_operator = $project["order_retail_operator"];

$old_project_type_subclass_id = $project["project_type_subclass_id"];

// get Action parameter
//$action_parameter_rto = get_action_parameter(RETAIL_OPERATOR_ASSIGNED, 1);
$action_parameter_rtc = get_action_parameter(RETAIL_COORDINATOR_ASSIGNED, 1);

// get company's address
$client_address = get_address($project["order_client_address"]);



// create sql for the retail_coordinator listbox
$sql_retail_coordinators = "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as user_fullname ".
                           "from users ".
                           "left join user_roles on user_id = user_role_user ".
                           "where (user_role_role = 3 or user_role_role = 80) and user_active = 1 " . 
                           "order by user_name, user_firstname";


// create sql for the local retail_coordinator listbox
$sql_local_retail_coordinators = "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as user_fullname ".
                           "from users ".
                           "left join user_roles on user_id = user_role_user ".
                           "where user_address = " . $project["order_client_address"] .
						   " and ((user_role_role = 15 or user_role_role = 4 or user_role_role = 16 or user_role_role = 10) " . 
						   " and user_active = 1) or user_id = '" . $project["project_local_retail_coordinator"] . "' " .
                           "order by user_name, user_firstname";



// create sql for the retail_operator listbox
$sql_retail_operators = "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as user_fullname ".
                        "from users ".
                        "left join user_roles on user_id = user_role_user ".
                        "where user_role_role = 2 and user_active = 1 " . 
						"order by user_name, user_firstname";

// create sql for the design contractor listbox
$sql_contractors = "select DISTINCT user_id, ".
                   "    concat(address_company, ', ', user_name, ' ', user_firstname) as user_fullname ".
                   "from addresses ".
                   "left join users on user_address = address_id ".
                   "where address_type = 5 ".
                   "    and address_active = 1 ".
                   "    and user_active = 1 ".
                   "order by address_company, user_name, user_firstname";


$sql_contractors = "select DISTINCT user_id, ".
                   "    concat(address_company, ', ', user_name, ' ', user_firstname) as user_fullname ".
                   "from addresses ".
                   "left join users on user_address = address_id ".
				   "left join user_roles on user_role_user = user_id ".
                   "where user_role_role = 7 ".
                   "    and address_active = 1 ".
                   "    and user_active = 1 ".
                   "order by address_company, user_name, user_firstname";

// create sql for the design supervisor listbox
$sql_supervisors = "select DISTINCT user_id, ".
                   "    concat(user_name, ' ', user_firstname) as user_fullname ".
                   "from users ".
                   "left join user_roles on user_role_user = user_id ".
                   "where user_role_role = 8 " .
                   "    and user_active = 1 ".
                   "order by user_name";


// create sql for the client's contact listbox
$sql_address_user = "select DISTINCT user_id, concat(address_company, ', ', user_name, ' ', user_firstname) ".
                    "from users ".
                    "left join addresses on address_id = " . $project["order_client_address"] . " " .
                    "where user_active = 1 and user_address = ". $project["order_client_address"] . " ".
                    "order by address_company, user_name";

//get RRMA: HQ Project Leaders
$sql_rrmas = "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as username " . 
             "from user_roles " .
	         "left join users on user_id = user_role_user " . 
	         "where user_role_role = 19 " .
			 "order by user_name, user_firstname";


// create sql for the cms approvers listbox
$sql_cms_approvers = "select DISTINCT user_id, concat(user_name, ' ', user_firstname) as user_fullname ".
					 "from users ".
					 "left join user_roles on user_id = user_role_user ".
					 "where user_role_role in (3, 8, 10, 80) and user_active = 1 " . 
					 "order by user_name, user_firstname";



// create sql for the product line listbox
if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
{
	$sql_product_line = "select product_line_id, product_line_name ".
						"from product_lines ".
						"where (product_line_budget = 1 and product_line_active = 1) ".
		                " or product_line_id = " . dbquote($project["project_product_line"]) . 
						" order by product_line_name";
}
else
{
	$sql_product_line = "select DISTINCT product_line_id, product_line_name ".
						"from product_lines ".
		                "left join productline_regions on productline_region_productline = product_line_id " .
						"where (product_line_budget = 1  and product_line_active = 1 ".
		                " and productline_region_region = " . $client_address["country_region"] . ") " .
		                " or product_line_id = " . dbquote($project["project_product_line"]) . 
						" order by product_line_name";


}

//echo $sql_product_line;

//create sql for product line subclasses
$num_or_product_line_subclasses = 0;
if(param("product_line"))
{
	$sql_product_line_sub_classes = "select productline_subclass_id, productline_subclass_name " . 
									"from productline_subclass_productlines " .
		                            " left join productline_subclasses on  productline_subclass_id = productline_subclass_productline_class_id " . 
									"where (productline_subclass_active = 1 or productline_subclass_id = " . dbquote($project["project_product_line_subclass"]) . ") ".
		                            "and productline_subclass_productline_line_id = " . dbquote(param("product_line")) . 
		                            " order by productline_subclass_name";

	//count subclasses
	$sql_product_line_sub_classes_count = "select count(productline_subclass_id) as num_recs " . 
									"from productline_subclass_productlines " .
		                            " left join productline_subclasses on  productline_subclass_id = productline_subclass_productline_class_id " .
									"where (productline_subclass_active = 1 or productline_subclass_id = " . dbquote($project["project_product_line_subclass"]) . ") ".
		                            "and productline_subclass_productline_line_id = " . dbquote(param("product_line"));

	$res = mysql_query($sql_product_line_sub_classes_count) or dberror($sql_product_line_sub_classes_count);
    $row = mysql_fetch_assoc($res);
	$num_or_product_line_subclasses = $row["num_recs"];
}
elseif($project["project_product_line"] > 0) 
{
	$sql_product_line_sub_classes = "select productline_subclass_id, productline_subclass_name " . 
									"from productline_subclass_productlines " .
								    " left join productline_subclasses on  productline_subclass_id = productline_subclass_productline_class_id " . 
									"where (productline_subclass_active = 1 or productline_subclass_id = " . dbquote($project["project_product_line_subclass"]) . ") ".
		                            "and productline_subclass_productline_line_id = " . dbquote($project["project_product_line"]) . 
		                            " order by productline_subclass_name";

	//count subclasses
	$sql_product_line_sub_classes_count = "select count(productline_subclass_id) as num_recs " . 
									"from productline_subclass_productlines " .
								    " left join productline_subclasses on  productline_subclass_id = productline_subclass_productline_class_id " . 
									"where (productline_subclass_active = 1 or productline_subclass_id = " . dbquote($project["project_product_line_subclass"]) . ") ".
		                            "and productline_subclass_productline_line_id = " . dbquote($project["project_product_line"]);

	$res = mysql_query($sql_product_line_sub_classes_count) or dberror($sql_product_line_sub_classes_count);
    $row = mysql_fetch_assoc($res);
	$num_or_product_line_subclasses = $row["num_recs"];
}

//get addresses from pos index
$sql_posaddresses = "select posaddress_id, concat(posaddress_place, ', ', posaddress_name) as posaddress " .
                    "from posaddresses " . 
					"where posaddress_country = " . $project["order_shop_address_country"] . 
					" order by posaddress_place, posaddress_name";


$sql_project_type_subclasses = "select project_type_subclass_id, project_type_subclass_name " . 
                               " from project_type_subclasses order by project_type_subclass_name";

/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");


$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid", $project["project_order"]);

//show project information
if($project["project_projectkind"] == 2 
	or $project["project_projectkind"] == 3) // renovation and takeover/renovation
{
	$renovated_pos_id = get_renovated_pos_info($project["project_order"]);
	if ($renovated_pos_id > 0)
	{
		if (has_access("can_edit_pos_data") or has_access("can_view_pos_data"))
		{ 
			$shop_address = $project["order_shop_address_zip"] . " " .
							$project["order_shop_address_place"] . ", " .
							$project["order_shop_address_country_name"];
			
			$tmp = '<a href="../pos/posindex_pos.php?country=' . $project["order_shop_address_country"] . '&ltf=all&ostate=&province=&id=' . $renovated_pos_id . '" target="_blank">' . $project["order_shop_address_company"] . '</a><br />' . $shop_address;
		}
		$form->add_label("shop_address", "POS Location Address", RENDER_HTML, $tmp);
	}
	else
	{	
		$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];
		$form->add_label("shop_address", "POS Location Address", 0, $shop);
	}
}
else
{
	$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];
	$form->add_label("shop_address", "POS Location Address", 0, $shop);
}

if(($project["project_projectkind"] == 6
	or $project["project_projectkind"] == 9) 
	and $project["project_relocated_posaddress_id"] > 0) // relocation
{
	$relocated_pos = get_relocated_pos_info($project["project_relocated_posaddress_id"]);

	if (count($relocated_pos) > 0)
	{
		if (has_access("can_edit_pos_data") or has_access("can_view_pos_data"))
		{ 
			$tmp = '<a href="../pos/posindex_pos.php?country=' . $project["order_shop_address_country"] . '&ltf=all&ostate=&province=&id=' . $project["project_relocated_posaddress_id"] . '" target="_blank">' . $relocated_pos["posaddress_name"] . ", " .$relocated_pos["place_name"] . '</a>';
			$form->add_label("relocated_pos", "Relocated POS", RENDER_HTML, $tmp );
		}
		else
		{
			$form->add_label("relocated_pos", "Relocated POS", 0, $relocated_pos["posaddress_name"] . ", " .$relocated_pos["place_name"] );
		}
	}
}


$client = $client_address["company"] . ", " .
          $client_address["zip"] . " " . $client_address["place"] . ", " .
          $client_address["country_name"];




$form->add_label("client_address", "Client", 0, $client);


if($project["order_franchisee_address_id"] > 0)
{
	$tmp = get_address($project["order_franchisee_address_id"]);
	$franchisee = $tmp["company"] . ", " .
        $tmp["zip"] . " " .
        $tmp["place_name"] . ", " .
        $tmp["country_name"];
}
else
{
	$franchisee = $project["order_franchisee_address_company"] . ", " .
        $project["order_franchisee_address_zip"] . " " .
        $project["order_franchisee_address_place"] . ", " .
        $project["order_franchisee_address_country_name"];

}

if($project["project_cost_type"] != 6 and $project["project_postype"] != 2)
{
	$form->add_label("franchisee_address", "Owner Company", 0, $franchisee);		
}
else
{
	$form->add_label("franchisee_address", "Owner Company", 0, $franchisee);		
}

$form->add_label("type3", "Project Legal Type / Project Type", 0, $project["project_costtype_text"] . " / " . $project["projectkind_name"]);

$form->add_list("project_type_subclass_id", "Project Type Subclass", $sql_project_type_subclasses, SUBMIT, $project["project_type_subclass_id"]);
$form->add_hidden("old_project_type_subclass_id", $old_project_type_subclass_id);

$form->add_checkbox("posaddress_is_flagship", "the POS is a flag ship POS", $project["project_is_flagship"], "", "Flag Ship Option");

$form->add_label("project_postype", "POS Type / Subclass", 0, $project["postype_name"] . " / " . $project["possubclass_name"]);


$form->add_list("product_line", "Product Line*", $sql_product_line, NOTNULL | SUBMIT, $project["project_product_line"]);

if($num_or_product_line_subclasses > 0)
{
	$form->add_list("product_line_subclass", "Product Line Subclass", $sql_product_line_sub_classes,0, $project["project_product_line_subclass"]);
}
else
{
	$form->add_hidden("product_line_subclass",0);
}

$form->add_label("project_number_label", "Project Number  /Treatment State", 0, $project["project_number"] . " / " . $project["project_state_text"]);


if (has_access("can_edit_status_in_projects") and
       ($project["order_actual_order_state_code"] == '120'
	    or $project["order_actual_order_state_code"] == '210')
    )
{
    $sql = "select distinct order_state_code " .
           "from order_states " .
		   " where order_state_code IN (800, " . $project["order_actual_order_state_code"] . ") " .
           "order by order_state_code";
    $form->add_list("status", "Global Project Status", $sql, NOTNULL, $project["order_actual_order_state_code"]);

}

/*
if(isset($offer_state) and $offer_state["data"])
{
	$form->add_label("status1", "Project Status Development", 0, $development_state["name"], 1, "offer_info");
	$form->add_hidden("status", $project["order_actual_order_state_code"]);
}
else
{
	$form->add_label("status1", "Project Status Development", 0, $development_state["name"]);
	$form->add_hidden("status", $project["order_actual_order_state_code"]);
}
*/

$form->add_label("status1", "Project Status Development", 0, $development_state["name"]);
$form->add_hidden("status", $project["order_actual_order_state_code"]);

/*
if(has_access("can_see_logistic_status") and is_array($logistic_state) and $logistic_state["data"])
{
	$form->add_label("logistic_status_label", "Project Status Logistic", 0, $logistic_state["info"], 1, "logistic_info");
}
elseif(has_access("can_see_logistic_status") and is_array($logistic_state) and $logistic_state["info"])
{
	$form->add_label("logistic_status_label", "Project Status Logistic", 0, $logistic_state["info"]);
}
*/


if($project["project_projectkind"] != 4 and $project["project_projectkind"] != 5) // take over, lease renewal
{
	$form->add_section("Production Type");
	$form->add_list("project_production_type", "Production Type",
		"select production_type_id, production_type_name from production_types " .
		"order by production_type_name", NOTNULL, $project["project_production_type"]);
}
else {
	$form->add_hidden("project_production_type", 1);
}

//add fields for editing
$form->add_section("Project Management");
$form->add_list("retail_coordinator", "Project Leader*", $sql_retail_coordinators, NOTNULL, $project["project_retail_coordinator"]);
$form->add_hidden("old_retail_coordinator", $old_retail_coordinator);

$form->add_list("local_retail_coordinator", "Local Project Leader", $sql_local_retail_coordinators, 0, $project["project_local_retail_coordinator"]);


//$form->add_list("hq_project_manager", "HQ Project Leader", $sql_rrmas, 0, $project["project_hq_project_manager"]);
//$form->add_hidden("old_retail_operator", $old_retail_operator);

if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) // take over, lease renewal
{

	$form->add_hidden("retail_operator", 0);
	$form->add_hidden("contractor_user_id", 0);
	$form->add_hidden("supervisor_user_id", 0);
	$form->add_hidden("cms_approver_user_id", 0);
	$form->add_hidden("delivery_confirmation_by", 0);
	
	
	if(count($tracking_info) > 0) {
		$form->add_edit("project_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20, 0, 1, "changehistory");
	}
	else
	{
		$form->add_edit("project_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20);
	}

	if($project["project_real_opening_date"] != NULL and $project["project_real_opening_date"] != '0000-00-00')
	{
		if($project["project_projectkind"] == 5)
		{
			$form->add_edit("change_comment", "Reason for changing lease starting date*", 0);
		}
		else
		{
			$form->add_edit("change_comment", "Reason for changing agreed opening date*", 0);
		}
	}
	else
	{
		$form->add_hidden("change_comment");
	}

	$form->add_hidden("old_shop_real_opening_date", to_system_date($project["project_real_opening_date"]));
	$form->add_hidden("project_no_planning", 0);
	$form->add_hidden("project_is_local_production", 0);
	$form->add_hidden("project_furniture_type_store", 0);
	$form->add_hidden("project_furniture_type_sis", 0);
	$form->add_hidden("project_uses_icedunes_visuals", 0);

	$form->add_hidden("old_project_state", $project["project_state"]);
	//$form->add_hidden("project_state", $project["project_state"]);


	if (has_access("can_edit_treatment_state"))
	{
		$form->add_section("Treatment State");
			
		if(count($tracking_info2) > 0) {
			$sql = "select project_state_id, project_state_text from " .
				   " project_states  where project_state_selectable = 1 or project_state_id = " . dbquote($project["project_state"]);
			$form->add_list("project_state", "Treatment State", $sql, 0, $project["project_state"], 1, "changehistory2");

		}
		else
		{ 
			$sql = "select project_state_id, project_state_text from " .
				   " project_states  where project_state_selectable = 1 or project_state_id = " . dbquote($project["project_state"]);
			$form->add_list("project_state", "Treatment State", $sql, 0, $project["project_state"]);
		}

		
		$form->add_edit("change_comment2", "Reason for changing the treatment state", 0);

	}
	$form->add_hidden("project_budget_covered_by");
}
else
{
	//$form->add_list("retail_operator", "Logistics Coordinator", $sql_retail_operators, NOTNULL, $project["order_retail_operator"]);
	$form->add_hidden("retail_operator", 0);
	$form->add_section("Design Staff");
	$form->add_list("contractor_user_id", "Design Contractor", $sql_contractors, 0, $project["project_design_contractor"]);
	$form->add_list("supervisor_user_id", "Design Supervisor", $sql_supervisors, 0, $project["project_design_supervisor"]);

	//$form->add_section("Controller");
	//$form->add_list("cms_approver_user_id", "CMS Approval", $sql_cms_approvers, 0, $project["project_cms_approver"]);
	$form->add_hidden("cms_approver_user_id", 0);

	/*
	$form->add_section("Confirmation of Delivery by");
	$form->add_list("delivery_confirmation_by", "Person", $sql_address_user, 0, $project["order_delivery_confirmation_by"]);
	*/
	$form->add_hidden("delivery_confirmation_by", 0);
	
	if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
	{
		$form->add_section("Project Date");
	}
	elseif($project["project_projectkind"] == 3
		or $project["project_projectkind"] == 9) //Take Over and renovation
	{
		$form->add_section("POS Opening Dates");
		$form->add_label("project_planned_takeover_date", "Client's Preferred Takover Date", 0, to_system_date($project["project_planned_takeover_date"]));
	}
	else
	{
		$form->add_section("POS Opening Dates");
	}

	$form->add_label("project_planned_opening_date", "Client's Preferred Opening Date", 0, to_system_date($project["project_planned_opening_date"]));
	
	if(count($tracking_info) > 0) {
		$form->add_edit("project_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20, 0, 1, "changehistory");
	}
	else
	{
		$form->add_edit("project_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20);
	}

	if($project["project_real_opening_date"] != NULL and $project["project_real_opening_date"] != '0000-00-00')
	{
		$form->add_edit("change_comment", "Reason for changing agreed opening date*", 0);
	}
	else
	{
		$form->add_hidden("change_comment");
	}

	
	$form->add_hidden("old_shop_real_opening_date", to_system_date($project["project_real_opening_date"]));

	if (has_access("can_edit_treatment_state"))
	{
		$form->add_section("Treatment State");

		/*
		if ($project['project_state'] == 5) {
			$form->add_lookup('project_state', "Treatment State", 'project_states', 'project_state_text', 0, 5);
		} else {
			
			if(count($tracking_info2) > 0) {
				$sql = "select project_state_id, project_state_text from " .
					   " project_states  where project_state_selectable = 1";
				$form->add_list("project_state", "Treatment State", $sql, 0, $project["project_state"], 1, "changehistory2");

			}
			else
			{
				$sql = "select project_state_id, project_state_text from " .
					   " project_states  where project_state_selectable = 1";
				$form->add_list("project_state", "Treatment State", $sql, 0, $project["project_state"]); 
			}

			$form->add_edit("change_comment2", "Reason for changing the treatment state", 0);
		}
		*/

		if(count($tracking_info2) > 0) {
			$sql = "select project_state_id, project_state_text from " .
				   " project_states  where project_state_selectable = 1 or project_state_id = " . dbquote($project["project_state"]);
			$form->add_list("project_state", "Treatment State", $sql, 0, $project["project_state"], 1, "changehistory2");

		}
		else
		{
			$sql = "select project_state_id, project_state_text from " .
				   " project_states  where project_state_selectable = 1 or project_state_id = " . dbquote($project["project_state"]);;
			$form->add_list("project_state", "Treatment State", $sql, 0, $project["project_state"]); 
		}

		$form->add_edit("change_comment2", "Reason for changing the treatment state", 0);


	}
	else
	{
		$form->add_hidden("project_state", $project["project_state"]);
	}

	$form->add_hidden("old_project_state", $project["project_state"]);

	$form->add_hidden("old_project_budget_covered_by", $project["project_budget_covered_by"]);


	if($project["project_projectkind"] == 8)
	{
		$form->add_section("Budget Coverage");
		$form->add_list("project_budget_covered_by", "Budget covered by*", "select budget_covering_unit_id, budget_covering_unit_unitname from budget_covering_units order by budget_covering_unit_unitname", NOTNULL, $project["project_budget_covered_by"]);
	}
	else
	{
		$form->add_hidden("project_budget_covered_by");
	}

	
	$form->add_hidden("project_no_planning", 0);
	$form->add_hidden("project_is_local_production", 0);
	$form->add_hidden("project_furniture_type_store", 0);
	$form->add_hidden("project_furniture_type_sis", 0);
	
	/*
	$form->add_section("Miscellanous");
	$form->add_checkbox("project_no_planning", "project does not need architectural planning", $project["project_no_planning"], 0, "Planning");
	$form->add_checkbox("project_is_local_production", "project is locally realized (local production)", $project["project_is_local_production"], 0, "Local Production");

	
	if($project["project_postype"] == 1) // Store
	{
		$form->add_hidden("project_furniture_type_store", 0);
		$form->add_checkbox("project_furniture_type_sis", "STORE project to be realized in SIS furniture", $project["project_furniture_type_sis"], 0, "Furniture");
		
	}
	elseif($project["project_postype"] == 2) // SIS
	{
		$form->add_checkbox("project_furniture_type_store", "SIS project to be realized in STORE furniture", $project["project_furniture_type_store"], 0, "Furniture");
		$form->add_hidden("project_furniture_type_sis", 0);
	}
	else
	{
		$form->add_hidden("project_furniture_type_store", 0);
		$form->add_hidden("project_furniture_type_sis", 0);
	}
	*/

	$form->add_section("Visuals");
	$form->add_checkbox("project_uses_icedunes_visuals", "The project uses Visuals", $project["project_uses_icedunes_visuals"], 0, "Visuals");
}


if($project["project_projectkind"] == 6 or $project["project_projectkind"] == 9) // Relocation Project
{
	$form->add_section("Relocation Info");
	$form->add_comment("Please indicate the POS to be relocated.");
	$form->add_list("project_relocated_posaddress_id", "POS being relocated", $sql_posaddresses, 0, $project["project_relocated_posaddress_id"]);
}
else
{
	$form->add_hidden("project_relocated_posaddress_id", 0);
}

$form->add_hidden("project_use_ps2004", $project["project_use_ps2004"]);
//$form->add_checkbox("project_use_ps2004", "use project sheet 2004", $project["project_use_ps2004"]);


$form->add_button("save", "Save Data");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("product_line"))
{
	//get standard design contractor
	$sql = 'select standarddesigncontractor_dcon_user ' . 
		   'from standarddesigncontractors ' . 
	       'where standarddesigncontractor_productline = ' . dbquote($form->value("product_line")) . 
		   ' and standarddesigncontractor_postype = ' . dbquote($form->value("project_postype"));

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$form->value("contractor_user_id", $row["standarddesigncontractor_dcon_user"]);
	}

	$form->value("product_line_subclass", 0);
	

	$form->value("project_uses_icedunes_visuals", 0);
}
elseif($form->button("project_type_subclass_id"))
{
	$sql = "select * from project_type_subclasses " . 
		   " where project_type_subclass_id = " . dbquote($form->value("project_type_subclass_id"));
	
	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$form->value("retail_coordinator", $row["project_type_subclass_project_manager"]);
		//$form->value("retail_operator", $row["project_type_subclass_logistics_coordinator"]);
	}
	else
	{
		$form->value("retail_coordinator", "");
		//$form->value("retail_operator", "");

	}

}
elseif ($form->button("save"))
{
	
	$error = 0;
	if($form->value("old_shop_real_opening_date") and $form->value("old_shop_real_opening_date") != $form->value("project_real_opening_date"))
	{
		$form->add_validation("{change_comment} != ''", "Please indicate the reason for changing the agreed opening date!");
	}


	if($form->value("old_project_state") != $form->value("project_state"))
	{
		$form->add_validation("{change_comment2} != ''", "Please indicate the reason for changing the treatment state!");
	}

	if($form->value("project_state") != 2) // on hold
	{
		$form->add_validation("{project_real_opening_date} != ''", "The agreed opening date must be indicated.");
	}

	if($project["project_projectkind"] !=5 
		and $form->value("project_real_opening_date")
		and (!$project["project_actual_opening_date"])) {
		
		if(from_system_date($form->value("project_real_opening_date")) <  date("Y-m-d")) {
			$form->error("The agreed opening date must be a future date.");
			$error = 1;
		}
	}
	
	if ($error == 0 and $form->validate())
    {
		
		//set cms approver in case of PL change
		/*
		if($old_retail_coordinator > 0 
			and $form->value('retail_coordinator') > 0
			and  $old_retail_coordinator != $form->value('retail_coordinator')) 
		{
			$form->value('cms_approver_user_id', $form->value('retail_coordinator'));
		}
		*/
		
		
		project_update_retail_data($form);

		//update flag ship option
		if($project["posaddress_id"] > 0 and $project["pipeline"] == 1) // project is in pipeline (new, relocation)
		{
			$sql = "update posaddressespipeline set posaddress_is_flagship = " . dbquote($form->value("posaddress_is_flagship")) . 
				   " where posaddress_id = " . $project["posaddress_id"];
			$result = mysql_query($sql) or dberror($sql);
		}
		elseif($project["posaddress_id"] > 0 
			and $project["project_actual_opening_date"] != NULL
			and $project["project_actual_opening_date"] != '0000-00-00'
			)
		{
			$sql = "update posaddresses set posaddress_is_flagship = " . dbquote($form->value("posaddress_is_flagship")) . 
				   " where posaddress_id = " . $project["posaddress_id"];
			$result = mysql_query($sql) or dberror($sql);
		}

		$sql = "update projects set project_is_flagship = " . dbquote($form->value("posaddress_is_flagship")) . 
			   " where project_id = " . dbquote(param("pid"));
		
		$result = mysql_query($sql) or dberror($sql);

		//$form->value("treatment_state", $treatment_state_text);

		if($old_retail_coordinator > 0 
			and $form->value('retail_coordinator') > 0
			and  $old_retail_coordinator != $form->value('retail_coordinator')) 
		{

			$field = "retail_coordinator";
			$old_tmp = get_user($old_retail_coordinator);
			$new_tmp = get_user($form->value('retail_coordinator'));
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote($field) . ", " . 
				   dbquote($old_tmp["name"] . ' ' . $old_tmp["firstname"]) . ", " . 
				   dbquote($new_tmp["name"] . ' ' . $new_tmp["firstname"]) . ", " . 
				   "'Project Leader changed', " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
				   
			$result = mysql_query($sql) or dberror($sql);
			
			
			$actionmail = new ActionMail(31);
			$actionmail->setParam('id', param("pid"));
			$actionmail->setParam('old_retail_coordinator', $old_retail_coordinator);
			$actionmail->setParam('new_retail_coordinator', $form->value('retail_coordinator'));
			
			if($senmail_activated == true)
			{
				$actionmail->send();
			}

			$recipient = $actionmail->getRecipient($form->value('retail_coordinator'));
			
			if ($actionmail->isSuccess() && $recipient->isSendMail()) {
				$bodytext = $recipient->getContent();
				append_mail($project["project_order"], $form->value('retail_coordinator'), user_id(), $bodytext, "910", 1);
				append_mail($project["project_order"], $old_retail_coordinator, user_id(), $bodytext, "910", 1, 0, 1);
			}

			
			//echo "<pre>";
			//print_r($actionmail);
			//die;
		}

		
		/*
		if($old_retail_operator > 0 
			and $form->value('retail_operator') > 0
			and  $old_retail_operator != $form->value('retail_operator')) 
		{

			
			$field = "retail_operator";
			$old_tmp = get_user($old_retail_operator);
			$new_tmp = get_user($form->value('retail_operator'));
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote($field) . ", " . 
				   dbquote($old_tmp["name"] . ' ' . $old_tmp["firstname"]) . ", " . 
				   dbquote($new_tmp["name"] . ' ' . $new_tmp["firstname"]) . ", " .  
				   "'Logistics Coordinator changed', " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
				   
			$result = mysql_query($sql) or dberror($sql);

			
			$actionmail = new ActionMail(32);
			$actionmail->setParam('id', param("pid"));
			$actionmail->setParam('old_retail_operator', $old_retail_operator);
			$actionmail->setParam('new_retail_operator', $form->value('retail_operator'));
			if($senmail_activated == true)
			{
				$actionmail->send();
			}

			$recipient = $actionmail->getRecipient($form->value('retail_operator'));

			if ($actionmail->isSuccess() && $recipient->isSendMail()) {
				$bodytext = $recipient->getContent();
				append_mail($project["project_order"], $form->value('retail_operator'), user_id(), $bodytext, "910", 1);
				append_mail($project["project_order"], $old_retail_operator, user_id(), $bodytext, "910", 1, 0, 1);
			}
			
		}
		*/
		
		if($old_project_type_subclass_id != $form->value('project_type_subclass_id'))
		{
			//project tracking
			$field = "project_type_subclass";
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote("project_type_subclass_id") . ", " . 
				   dbquote($old_project_type_subclass_id ) . ", " . 
				   dbquote($form->value('project_type_subclass_id')) . ", " . 
				   dbquote('manually changed in edit retail data') . ", " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
			$result = mysql_query($sql) or dberror($sql);
		}
		
		if($old_order_state != $form->value('status')) {

				
			//project tracking
			$field = "project_real_opening_date";
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote("order_actual_order_state_code") . ", " . 
				   dbquote($old_order_state ) . ", " . 
				   dbquote($form->value('status')) . ", " . 
				   dbquote('manually changed in edit request') . ", " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
				   
			$result = mysql_query($sql) or dberror($sql);
		}

		
		if($form->value("project_no_planning") == 1) {
			$form->value("contractor_user_id", 0);
		}

        if ($project["order_actual_order_state_code"] + 0 > RETAIL_OPERATOR_ASSIGNED_P)
        {
            $do_update = 0;
        }
        else
        {
            $do_update = 1;
        }



        // append record to table actual_order_states and append task
        if ((!$old_retail_coordinator and $form->value("retail_coordinator"))
			or ($form->value("retail_coordinator") and $project["order_actual_order_state_code"] == '100'))
        {
            append_order_state($project["project_order"], RETAIL_COORDINATOR_ASSIGNED, 1, $do_update);
			save_project_development_status($project["project_order"], RETAIL_COORDINATOR_ASSIGNED);
            $messagetext = get_string("project_" . RETAIL_COORDINATOR_ASSIGNED);
            $date = date("Y-m-d");
            $link = "project_task_center.php?pid=" . param('pid');
            if ($action_parameter_rtc["append_task"] == 1)
            {
                append_task($project["project_order"], $form->value("retail_coordinator"), $messagetext, $link, $date, user_id());
            }

        }

        /*
		if ((!$old_retail_operator and $form->value("retail_operator"))
			or ($form->value("retail_operator") and $project["order_actual_order_state_code"] == '100'))
        {
            append_order_state($project["project_order"], RETAIL_OPERATOR_ASSIGNED_P, 1, $do_update);
			save_project_development_status($project["project_order"], RETAIL_OPERATOR_ASSIGNED_P);
            $messagetext = get_string("project_" . RETAIL_OPERATOR_ASSIGNED_P);
            $date = date("Y-m-d");
            $link = "project_task_center.php?pid=" . param('pid');
            if ($action_parameter_rto["append_task"] == 1)
            {
                append_task($project["project_order"], $form->value("retail_operator"), $messagetext, $link, $date, user_id());
            }
        }
		


		if($form->value("cms_approver_user_id") > 0 and $form->value("cms_approver_user_id") != $project["project_cms_approver"])
        {
	
			if($project["project_cost_cms_completed"] == 1 and $project["project_cost_cms_approved"] == 0) //CMS was completed but not approved
			{
				//send mail to cms approval person if actual opening date was entered
				$sql = "select * from users " .
					   "where user_id = " . dbquote($form->value("cms_approver_user_id"));
				
				$res = mysql_query($sql) or dberror($sql);
				if($row = mysql_fetch_assoc($res))
				{
					$recipient = $row["user_email"];
					$reciepient_user_id = $row["user_id"];

					$sql = "select user_id, user_email, concat(user_name, ' ', user_firstname) as username ".
						   "from users ".
						   "where (user_id = '" . user_id() . "' " . 
						   "   and user_active = 1)";
					$res = mysql_query($sql) or dberror($sql);
					$row = mysql_fetch_assoc($res);
					$sender_id = $row["user_id"];
					$sender_email = $row["user_email"];
					$sender_name = $row["username"];
					

					$subject = "CMS approval needed - Project " . $project["order_number"] . ", " . $project["order_shop_address_country_name"] . ", " . $project["order_shop_address_company"];

					$mail = new Mail();
					$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
					$mail->set_sender($sender_email, $sender_name);
					$mail->add_recipient($recipient);

					$bodytext0 = $sender_name . " has confirmed the completion of the cost monitoring sheet for the project. Please check and approve the Cost Monitoring Sheet.";
					$link ="project_costs_real_costs.php?pid=" . param("pid");
					$bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
					$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
					$mail->add_text($bodytext);
					
					if($senmail_activated == true)
					{
						$result = $mail->send();
					}
					else
					{
						$result = 1;
					}
					append_mail($project["project_order"], $reciepient_user_id, user_id(), $bodytext0, "910", 1);
					append_task($project["project_order"], $reciepient_user_id, $bodytext0, $link, "",  user_id(), '910', 1);

				}
			}
        }
		*/


		//send email notifications on change of product line
		if($old_product_line != $form->value('product_line')) 
		{

				//project tracking
				$field = "product_line";
				$sql = "Insert into projecttracking (" . 
					   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
					   user_id() . ", " . 
					   $project["project_id"] . ", " . 
					   dbquote($field) . ", " . 
					   dbquote($old_product_line) . ", " . 
					   dbquote($form->value('product_line')) . ", " . 
					   dbquote("User " . user_login() . " has chanded the product line") . ", " . 
					   dbquote(date("Y-m-d:H:i:s")) . ")"; 
					   
				$result = mysql_query($sql) or dberror($sql);
				
				$old_product_line_name = "";
				$new_product_line_name = "";

				if($old_product_line > 0) 
				{
					$sql = "select product_line_name ".
							"from product_lines ".
							"where product_line_id = " . dbquote($old_product_line);


					$res = mysql_query($sql) or dberror($sql);
					$row = mysql_fetch_assoc($res);
					$old_product_line_name = $row["product_line_name"];

				}
				else
				{
					$old_product_line_name = 'not yet indicated';
				
				}

				if($form->value('product_line') > 0) 
				{
					$sql = "select product_line_name ".
										"from product_lines ".
										"where product_line_id = " . dbquote($form->value('product_line'));


					$res = mysql_query($sql) or dberror($sql);
					$row = mysql_fetch_assoc($res);
					$new_product_line_name = $row["product_line_name"];

				}
				
				
				$sql = "select user_id, user_email, concat(user_name, ' ', user_firstname) as username ".
					   "from users ".
					   "where (user_id = '" . user_id() . "' " . 
					   "   and user_active = 1)";
				
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				
				$sender_id = $row["user_id"];
				$sender_email = $row["user_email"];
				$sender_name = $row["username"];

				//recipients
				$subject = "Product line was changed - Project " . $project["order_number"] . ", " . $project["order_shop_address_country_name"] . ", " . $project["order_shop_address_company"];


				$bodytext0 = $sender_name . " has changed the product line of the project " . $project["order_number"] . " from " . $old_product_line_name . " to " . $new_product_line_name . ".";
				$link ="project_view_client_data.php?pid=" . param("pid");
				$bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
				$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";   

				//project Leader
				$reciepients = array();
				$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
					   "from users ".
					   "where (user_id = " . dbquote($project['project_retail_coordinator']) . 
					   "   and user_active = 1)";
				
				$res1 = mysql_query($sql) or dberror($sql);
				if ($row1 = mysql_fetch_assoc($res1))
				{
					$reciepients[$row1["user_id"]] = strtolower($row1["user_email"]);
					$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
					$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
				}
				

				//client
				$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
					   "from users ".
					   "where (user_id = " . dbquote($project['order_user']) . 
					   "   and user_active = 1)";
				
				$res1 = mysql_query($sql) or dberror($sql);
				if ($row1 = mysql_fetch_assoc($res1))
				{
					$reciepients[$row1["user_id"]] = strtolower($row1["user_email"]);
					$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
					$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
				}


				//retail supervising team
				$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
					   "from supervisingteam " . 
					   "left join users on user_id = supervisingteam_user ".
					   "where user_active = 1";
				
				$res1 = mysql_query($sql) or dberror($sql);
				while ($row1 = mysql_fetch_assoc($res1))
				{
					$reciepients[$row1["user_id"]] = strtolower($row1["user_email"]);
					$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
					$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
				}
					
				$result = 0;
				foreach($reciepients as $user_id=>$user_email) {
					
					$mail = new Mail();
					$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
					$mail->set_sender($sender_email, $sender_name);
					$mail->add_text($bodytext);
					$mail->add_recipient($user_email);

					if($senmail_activated == true)
					{
						$result = $mail->send();
					}
					else
					{
						$result = 1;
					}
				}

				if($result == 1)
				{
					foreach($reciepients as $user_id=>$user_email) {
						append_mail($project["project_order"], $user_id, $sender_id, $bodytext0, "", 1);
					}
				}
		}

		if($form->value("old_shop_real_opening_date") != $form->value("project_real_opening_date"))
		{
			
			
			//project tracking
			$field = "project_real_opening_date";
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote($field) . ", " . 
				   dbquote(to_system_date($form->value("old_shop_real_opening_date"))) . ", " . 
				   dbquote(to_system_date($form->value("project_real_opening_date"))) . ", " . 
				   dbquote($form->value("change_comment")) . ", " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
				   
			$result = mysql_query($sql) or dberror($sql);

			
			
			
			
			$sql = "select * from mail_alert_types " . 
					   "where mail_alert_type_id = 1";

			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);
			
			$recipient = $row["mail_alert_type_sender_email"];

			$sql = "select user_id ".
				   "from users ".
				   "where user_email = '" . $recipient . "' ";
			
			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);
			$reciepient_user_id = $row["user_id"];


			//project Leader
			$reciepient_user_id_rtc = $project["project_retail_coordinator"];
			$sql = "select user_email, concat(user_name, ' ', user_firstname) as username ".
				   "from users ".
				   "where user_id = '" . $reciepient_user_id_rtc . "' ";
			
			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);
			$recipient_rtc = $row["user_email"];
			$recipient_rtc_name = $row["username"];


			//client
			$reciepient_user_id_client = $project["order_user"];
			$sql = "select user_email, concat(user_name, ' ', user_firstname) as username ".
				   "from users ".
				   "where user_id = '" . $reciepient_user_id_client . "' ";
			
			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);
			$recipient_client = $row["user_email"];
			$recipient_client_name = $row["username"];
			


			//retail operator
			$reciepient_user_id_rto = $project["order_retail_operator"];
			$sql = "select user_email, concat(user_name, ' ', user_firstname) as username ".
				   "from users ".
				   "where user_id = '" . $reciepient_user_id_rto . "' ";
			
			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);
			$recipient_rto = $row["user_email"];
			$recipient_rto_name = $row["username"];



			//travel retail projects
			//get areas

			$recipients_travel_emails = array();
			$posareas = array();
			
			if($project["project_projectkind"] == 1 
				or $project["project_projectkind"] == 6
				or $project["project_projectkind"] == 9) //new or relocation project
			{
				$sql_p = "select posarea_area from posorderspipeline " .
					   "left join posareaspipeline on posarea_posaddress = posorder_posaddress " . 
					   "where posorder_order = " . dbquote($project["project_order"]);
			}
			elseif($project["project_projectkind"] > 1) //renovation, takeover/renovation and takeover project
			{
				$sql_p = "select posarea_area from posorders " .
					   "left join posareas on posarea_posaddress = posorder_posaddress " . 
					   "where posorder_order = " . dbquote($project["project_order"]);
			}


			$res_p = mysql_query($sql_p) or dberror($sql_p);
			while ($row_p = mysql_fetch_assoc($res_p))
			{
				$posareas[$row_p["posarea_area"]] = $row_p["posarea_area"];
			}
			
			
			if(count($posareas) > 0)
			{
				if(count($posareas) > 0)
				{
					$filter = " IN (" . implode(',', $posareas ) . ')';
				}

				$sql_r = 'select * from posareatypes ' . 
					   'where posareatype_id ' . $filter;


				$res_r = mysql_query($sql_r) or dberror($sql_r);
				while ($row_r = mysql_fetch_assoc($res_r))
				{
					
					if($row_r["posareatype_email1"])
					{

						$sql = 'select user_id from users ' . 
								   'where user_active = 1 and user_email = ' . dbquote($row_r["posareatype_email1"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipients_travel_emails[$row_u["user_id"]] = $row_r["posareatype_email1"];
						}
						
					}

					
					if($row_r["posareatype_email2"])
					{
						
						$sql = 'select user_id from users ' . 
								   'where user_active = 1 and user_email = ' . dbquote($row_r["posareatype_email2"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipients_travel_emails[$row_u["user_id"]] = $row_r["posareatype_email2"];
						}
						
					}

					if($row_r["posareatype_email3"])
					{
						
						$sql = 'select user_id from users ' . 
								   'where user_active = 1 and user_email = ' . dbquote($row_r["posareatype_email3"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipients_travel_emails[$row_u["user_id"]] = $row_r["posareatype_email3"];
						}
						
					}

					if($row_r["posareatype_email4"])
					{
						
						$sql = 'select user_id from users ' . 
								   'where user_active = 1 and user_email = ' . dbquote($row_r["posareatype_email4"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipients_travel_emails[$row_u["user_id"]] = $row_r["posareatype_email4"];
						}
						
					}

					if($row_r["posareatype_email5"])
					{
						
						$sql = 'select user_id from users ' . 
								   'where user_active = 1 and user_email = ' . dbquote($row_r["posareatype_email5"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipients_travel_emails[$row_u["user_id"]] = $row_r["posareatype_email5"];
						}
						
					}

					if($row_r["posareatype_email6"])
					{
						
						$sql = 'select user_id from users ' . 
								   'where user_active = 1 and user_email = ' . dbquote($row_r["posareatype_email6"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipients_travel_emails[$row_u["user_id"]] = $row_r["posareatype_email6"];
						}
						
					}
					
				}
			}

			if($project["project_pos_subclass"] == 17) // duty free
			{
				$sql_r = 'select * from possubclasses ' . 
					   'where possubclass_id = 17 ';


				$res_r = mysql_query($sql_r) or dberror($sql_r);
				while ($row_r = mysql_fetch_assoc($res_r))
				{
					if($row_r["possubclass_email1"])
					{

						$sql = 'select user_id from users ' . 
								   'where user_active = 1 and user_email = ' . dbquote($row_r["possubclass_email1"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipients_travel_emails[$row_u["user_id"]] = $row_r["possubclass_email1"];
						}
						
					}

					if($row_r["possubclass_email2"])
					{

						$sql = 'select user_id from users ' . 
								   'where user_active = 1 and user_email = ' . dbquote($row_r["possubclass_email2"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipients_travel_emails[$row_u["user_id"]] = $row_r["possubclass_email2"];
						}
						
					}

					if($row_r["possubclass_email3"])
					{

						$sql = 'select user_id from users ' . 
								   'where user_active = 1 and user_email = ' . dbquote($row_r["possubclass_email3"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipients_travel_emails[$row_u["user_id"]] = $row_r["possubclass_email3"];
						}
						
					}

					if($row_r["possubclass_email4"])
					{

						$sql = 'select user_id from users ' . 
								   'where user_active = 1 and user_email = ' . dbquote($row_r["possubclass_email4"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipients_travel_emails[$row_u["user_id"]] = $row_r["possubclass_email4"];
						}
						
					}

					if($row_r["possubclass_email5"])
					{

						$sql = 'select user_id from users ' . 
								   'where user_active = 1 and user_email = ' . dbquote($row_r["possubclass_email5"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipients_travel_emails[$row_u["user_id"]] = $row_r["possubclass_email5"];
						}
						
					}

					if($row_r["possubclass_email6"])
					{

						$sql = 'select user_id from users ' . 
								   'where user_active = 1 and user_email = ' . dbquote($row_r["possubclass_email6"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipients_travel_emails[$row_u["user_id"]] = $row_r["possubclass_email6"];
						}
					}
				}
			}


			//users for regional responsabilities
			$regional_responsable_reciepients = array();
			
			//get users responsible for the country
			
			//retail 
			if($project["project_cost_type"] == 1
				or $project["project_cost_type"] == 3
				or $project["project_cost_type"] == 4
				or $project["project_cost_type"] == 5)
			{
				$sql = "select user_company_responsible_user_id, user_email " . 
					   "from user_company_responsibles " . 
					   "left join users on user_id = user_company_responsible_user_id " . 
					   "where user_company_responsible_address_id = " . $project["order_client_address"] . 
					   " and user_company_responsible_retail = 1";
			}
			else // wholesale
			{
				$sql = "select user_company_responsible_user_id " . 
					   "from user_company_responsibles " . 
					   "left join users on user_id = user_company_responsible_user_id " . 
					   "where user_company_responsible_address_id = " . $project["order_client_address"] . 
					   " and user_company_responsible_wholsale = 1";
			}

			$res = mysql_query($sql) or dberror($sql);
			while($row = mysql_fetch_assoc($res))
			{
				$regional_responsable_reciepients[$row["user_company_responsible_user_id"]]  = $row["user_email"];
			}

			
			
			$sql = "select user_id, user_email, concat(user_name, ' ', user_firstname) as username ".
				   "from users ".
				   "where (user_id = '" . user_id() . "' " . 
				   "   and user_active = 1)";
			
			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);
			
			$sender_id = $row["user_id"];
			$sender_email = $row["user_email"];
			$sender_name = $row["username"];
			
			if($form->value("old_shop_real_opening_date"))
			{
				$fromto = "from " . $form->value("old_shop_real_opening_date") . " to " .  $form->value("project_real_opening_date");
			}
			else
			{
				$fromto = " to " .  $form->value("project_real_opening_date");
			}
			
			if($project["project_projectkind"] == 4) //Take Over 
			{
				$subject = "Project's planned takeover was changed  - Project " . $project["order_number"] . ", " . $project["order_shop_address_country_name"] . ", " . $project["order_shop_address_company"];

				$bodytext0 = $sender_name . " has changed the project's expected ending date " . $fromto ;
			}
			elseif($project["project_projectkind"] == 5) //lease renewal
			{
				$subject = "Project's lease starting date was changed  - Project " . $project["order_number"] . ", " . $project["order_shop_address_country_name"] . ", " . $project["order_shop_address_company"];

				$bodytext0 = $sender_name . " has changed the project's expected ending date " . $fromto ;
			}
			else
			{
				$subject = "Agreed POS opening Date was changed - Project " . $project["order_number"] . ", " . $project["order_shop_address_country_name"] . ", " . $project["order_shop_address_company"]. ": .";

				$bodytext0 = $sender_name . " has changed the agreed POS Opening Date of the project " . $fromto;
			}

			if($form->value("change_comment"))
			{
				$bodytext0 .= "\n" . "The reason is : " . $form->value("change_comment");
			}
			

			$mail = new Mail();
			$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
			$mail->set_sender($sender_email, $sender_name);
			$mail->add_recipient($recipient);


			if($recipient_rtc_name) 
			{
				$mail->add_cc($recipient_rtc, $recipient_rtc_name);
			}
			if($recipient_rto_name) 
			{
				$mail->add_cc($recipient_rto, $recipient_rto_name);
			}


			if($recipient_client_name) 
			{
				$mail->add_cc($recipient_client, $recipient_client_name);
			}

			foreach($recipients_travel_emails as $user_id=>$email)
			{
				$mail->add_cc($email);
			}


			foreach($regional_responsable_reciepients as $user_id=>$email)
			{
				$mail->add_cc($email);
			}

					
			
			$link ="project_task_center.php?pid=" . param("pid");
			$bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
			$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";
			
			           
			$mail->add_text($bodytext);
			
			if($senmail_activated == true)
			{
				$result = $mail->send();
			}
			else
			{
				$result = 1;
			}

			$sql = "delete from mail_alerts where mail_alert_type = 1 " .
				   "and mail_alert_order = " . $project["project_order"];
			mysql_query($sql) or dberror($sql);

			append_mail($project["project_order"], $reciepient_user_id, user_id(), $bodytext0, "910", 1);

			if($recipient_rtc_name) 
			{
				append_mail($project["project_order"], $reciepient_user_id_rtc, user_id(), $bodytext0, "910", 1, 0, 1);
			}

			if($recipient_rto_name) 
			{
				append_mail($project["project_order"], $reciepient_user_id_rto, user_id(), $bodytext0, "910", 1, 0, 1);
			}

			if($recipient_client_name) 
			{
				append_mail($project["project_order"], $reciepient_user_id_client, user_id(), $bodytext0, "910", 1, 0, 1);
			}

			foreach($recipients_travel_emails as $user_id=>$email)
			{
				append_mail($project["project_order"], $user_id, user_id(), $bodytext0, "910", 1, 0, 1);
			}


			foreach($regional_responsable_reciepients as $user_id=>$email)
			{
				append_mail($project["project_order"], $user_id, user_id(), $bodytext0, "910", 1, 0, 1);
			}


			$form->value("old_shop_real_opening_date", $form->value("project_real_opening_date"));

		}


		if($form->value("old_project_state") != $form->value("project_state"))
		{
			
			//project tracking
			$field = "project_state";
			$project_state_name_old = get_project_state_name($form->value("old_project_state"));
			$project_state_name_new = get_project_state_name($form->value("project_state"));
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote($field) . ", " . 
				   dbquote($project_state_name_old) . ", " . 
				   dbquote($project_state_name_new) . ", " . 
				   dbquote($form->value("change_comment2")) . ", " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
				   
			$result = mysql_query($sql) or dberror($sql);


			//send mail notification in case order state is greater than 700
			if($project["order_actual_order_state_code"] > 699)
			{
				$sql = "select * from mail_alert_types " . 
						   "where mail_alert_type_id = 40";

				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				
				$cc1 = $row["mail_alert_type_cc1"];
				$cc2 = $row["mail_alert_type_cc2"];
				$cc3 = $row["mail_alert_type_cc3"];
				$cc4 = $row["mail_alert_type_cc4"];

				
				//get RTC
				$sql = "select user_id, user_email, concat(user_name, ' ', user_firstname) as username ".
					   "from users ".
					   "where (user_id = '" . $project["project_retail_coordinator"] . "' " . 
					   "   and user_active = 1)";


				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				
				$rtc_id = $row["user_id"];
				$rtc_email = $row["user_email"];
				$rtc_name = $row["username"];


				//get RTO
				$sql = "select user_id, user_email, concat(user_name, ' ', user_firstname) as username ".
					   "from users ".
					   "where (user_id = '" . $project["order_retail_operator"] . "' " . 
					   "   and user_active = 1)";


				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				
				$rto_id = $row["user_id"];
				$rto_email = $row["user_email"];
				$rto_name = $row["username"];


				//get Design Supervisor
				$sql = "select user_id, user_email, concat(user_name, ' ', user_firstname) as username ".
					   "from users ".
					   "where (user_id = '" . $project["project_design_supervisor"] . "' " . 
					   "   and user_active = 1)";


				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				
				$dsup_id = $row["user_id"];
				$dsup_email = $row["user_email"];
				$dsup_name = $row["username"];


				

				//get sender
				$sql = "select user_id, user_email, concat(user_name, ' ', user_firstname) as username ".
					   "from users ".
					   "where (user_id = '" . user_id() . "' " . 
					   "   and user_active = 1)";
				
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				
				$sender_id = $row["user_id"];
				$sender_email = $row["user_email"];
				$sender_name = $row["username"];

				
				$subject = "Project was set " . $project_state_name_new . "  - Project " . $project["order_number"] . ", " . $project["order_shop_address_country_name"] . ", " . $project["order_shop_address_company"];

				
				if($project_state_name_old)
				{
					$mailtext = "The treatment state of the project was set from '" . $project_state_name_old . "' to '" . $project_state_name_new . "'\n\n";
				}
				else
				{
					$mailtext = "The treatment state of the project was set to " . $project_state_name_new . "'\n\n";
				}

				if( $form->value("change_comment2"))
				{
					$mailtext .= "\n" . "The reason is : " . $form->value("change_comment2");
				}

				
				$mail = new Mail();
				$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
				$mail->set_sender($sender_email, $sender_name);
				$mail->add_recipient($rtc_email, $rtc_name);
				$mail->add_recipient($rto_email, $rto_name);
				
				if($dsup_email){
					$mail->add_cc($dsup_email);
				}
				if($cc1){
					$mail->add_cc($cc1);
				}
				if($cc2){
					$mail->add_cc($cc2);
				}
				if($cc3){
					$mail->add_cc($cc3);
				}
				if($cc4){
					$mail->add_cc($cc4);
				}

				$link ="project_task_center.php?pid=" . param("pid");
				$bodytext = $mailtext . "\Click below to have direct access to the project:\n";
				$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n"; 
				
				

				$mail->add_text($bodytext);
				
				if($senmail_activated == true)
				{
					$result = $mail->send();
				}
				else
				{
					$result = 1;
				}

				
				append_mail($project["project_order"], $rtc_id, user_id(), $mailtext, "910", 1);
				append_mail($project["project_order"], $rto_id, user_id(), $mailtext, "910", 1);

				if($dsup_id)
				{
					append_mail($project["project_order"], $dsup_id, user_id(), $mailtext, "910", 1, 0, 1);
				}
			
			}


			//send actionmail
			if($form->value("project_state") == 2) // on hold
			{

				$actionmail = new ActionMail(109);
				$actionmail->setParam('id', param("pid"));
				$actionmail->setParam('reason', $form->value("change_comment2"));

				
				if(1==1 or $senmail_activated == true)
				{
					$actionmail->send();
				}

				$recipient = $actionmail->getRecipient($project["order_user"]);
			}
		}


		$form->add_hidden("old_project_budget_covered_by", $project["project_budget_covered_by"]);


		if($project["project_projectkind"] == 8)
		{
			if($form->value("project_budget_covered_by") == 3 
				and ($form->value("old_project_budget_covered_by") == 0 or $form->value("old_project_budget_covered_by") != 3))
			{

				$result = project_send_mail_notification_for_popups($project["project_id"]);
			}
		}


		

		redirect("project_edit_retail_data.php?pid=" . param("pid"));


		if($form->value("project_state") == 2) // on hold
		{
			$form->value("project_real_opening_date", "");
		}

		

        $form->message("Your changes have been saved.");
    }   
}


    
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Edit Retail Data");
$form->render();


?>


<div id="changehistory" style="display:none;">
    <strong>Changes of the agreed opening date</strong>
	<table class="table_tracking">
	<tr>
	<td class="label">User</td>
	<td class="label">Time</td>
	<td class="label">Old Value</td>
	<td class="label">New Value</td>
	<td class="label">Comment</td>
	</tr>

	<?php
		foreach($tracking_info as $key=>$values)
		{
			echo '<tr class="tr_tracking"><td class="td_tracking_nobr">' . $values['user_name'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_time'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_oldvalue'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_newvalue'] . '</td>';
			echo '<td class="td_tracking">' . $values['projecttracking_comment'] . '</td></tr>';
		}
	?>
	
	</table>
</div> 


<div id="changehistory2" style="display:none;">
    <strong>Changes of the treatment state</strong>
	<table class="table_tracking">
	<tr>
	<td class="label">User</td>
	<td class="label">Time</td>
	<td class="label">Old Value</td>
	<td class="label">New Value</td>
	<td class="label">Comment</td>
	</tr>

	<?php
		foreach($tracking_info2 as $key=>$values)
		{
			echo '<tr class="tr_tracking"><td class="td_tracking_nobr">' . $values['user_name'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_time'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_oldvalue'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_newvalue'] . '</td>';
			echo '<td class="td_tracking">' . $values['projecttracking_comment'] . '</td></tr>';
		}
	?>
	
	</table>
</div> 

<?php

echo "<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>";

require_once "include/project_footer_logistic_state.php";
$page->footer();

?>