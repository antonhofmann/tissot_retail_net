<?php
/********************************************************************

    projects_cms_completed.php

    Entry page for the projects section.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2012-03-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2012-03-06
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_state_constants.php";

check_access("can_view_projects");

register_param("showall");

/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());
$project_state_restrictions = get_project_state_restrictions(user_id());

// create sql
$sql = "select distinct project_id, project_order, project_number, project_projectkind, project_cost_type, " .
       "project_actual_opening_date, product_line_name, postype_name, project_postype, " . 
       "concat(order_shop_address_place,', ', order_shop_address_company), country_name, ".
       "    rtcs.user_name as project_retail_coordinator, rtos.user_name as order_retail_operator,".
       "    order_id, order_actual_order_state_code, project_costtype_text,  ".
	   " project_cost_cms_completed2, project_cost_cms_approved, project_cost_cms_controlled, project_state_text, " .
	   "if(project_state IN (2,4,5), " . 
	   "if(project_state = 2, concat('<span class=\"error\">', project_state_text, '</span>'), concat('<strong>', project_state_text, '</strong>')), project_real_opening_date) as real_opening_date ".
       "from projects ".
       "left join orders on project_order = order_id ".
	   "inner join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
       "left join product_lines on project_product_line = product_line_id ".
       "left join postypes on postype_id = project_postype ".
	   "left join project_states on project_state_id = project_state ".
       "left join countries on order_shop_address_country = countries.country_id ".
       "left join users as rtcs on project_retail_coordinator = rtcs.user_id " . 
	   "left join users as rtos on order_retail_operator = rtos.user_id ";

$sql_count = "select count(project_id) as num_recs " . 
       "from projects ".
       "left join orders on project_order = order_id ".
	   "inner join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join project_states on project_state_id = project_state ".
       "left join product_lines on project_product_line = product_line_id ".
       "left join postypes on postype_id = project_postype ".
       "left join countries on order_shop_address_country = countries.country_id ".
       "left join users as rtcs on project_retail_coordinator = rtcs.user_id " . 
	   "left join users as rtos on order_retail_operator = rtos.user_id ";


if (has_access("has_access_to_all_projects"))
{
	$list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '890' ";
}
else
{

	$condition = get_user_specific_order_list(user_id(), 1, $user_roles);

	if ($condition == "")
	{
		//$list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '890'  " .
		//			   "   and order_retail_operator is null";
		
		$list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '890'  ";
	}
	else
	{
		//$list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '890'  " .
		//			   "   and (" . $condition . " or order_retail_operator is null)";

		$list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '890'  " .
					   "   and (" . $condition . ")";
	}
}


//check if there is only one project

if(param('search_term')) {

	$search_term = trim(param('search_term'));
	$list_filter .= ' and (project_number like "%' . $search_term . '%" 
	                  or postype_name like "%' . $search_term . '%" 
					  or project_costtype_text like "%' . $search_term . '%"
					  or project_state_text like "%' . $search_term . '%"
					  or product_line_name like "%' . $search_term . '%"
					  or postype_name like "%' . $search_term . '%"
					  or order_actual_order_state_code like "%' . $search_term . '%"
					  or order_shop_address_place like "%' . $search_term . '%"
					  or order_shop_address_company like "%' . $search_term . '%"
					  or country_name like "%' . $search_term . '%")';

	$sql_count = $sql_count . ' where ' . $list_filter;
	$res = mysql_query($sql_count) or dberror($sql_count);

	if ($row = mysql_fetch_assoc($res))
	{
		if ($row['num_recs'] == 1)
		{
			$sql = $sql . ' where ' . $list_filter;
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$link = "project_task_center.php?pid=" . $row['project_id'];
				redirect($link);
			}
		}
	}
}


$images = array();
$cms_states = array();
$aods = array();

if(param('search_term')) {

	$sql_images = "select project_id from tasks " .
		   "left join orders on order_id = task_order " .
		   "left join projects on project_order = order_id " .
		   "left join project_costs on project_cost_order = order_id " .
		   "left join project_costtypes on project_costtype_id = project_cost_type " .
		   "left join project_states on project_state_id = project_state ".
		   "left join product_lines on project_product_line = product_line_id ".
		   "left join postypes on postype_id = project_postype ".
		   "left join countries on order_shop_address_country = countries.country_id ".
		   "where task_user = " . user_id() .
		   "   and task_done_date is null " .
		   "   and " . $list_filter;
}
else
{
	$sql_images = "select project_id from tasks " .
		   "left join orders on order_id = task_order " .
		   "left join projects on project_order = order_id " .
		   "where task_user = " . user_id() .
		   "   and task_done_date is null " .
		   "   and " . $list_filter;
}

$res = mysql_query($sql_images) or dberror($sql_images);

while ($row = mysql_fetch_assoc($res))
{
    $images[$row["project_id"]] = "/pictures/todo.gif";
}

if (has_access("can_edit_retail_data"))
{
    
	if(param('search_term')) {
		
		$sql_images = "select project_id from projects " .
					  "left join orders on order_id = project_order " .
			          "left join project_costs on project_cost_order = order_id " .
					   "left join project_costtypes on project_costtype_id = project_cost_type " .
					   "left join product_lines on project_product_line = product_line_id ".
					   "left join postypes on postype_id = project_postype ".
					   "left join countries on order_shop_address_country = countries.country_id ".
						"left join project_states on project_state_id = project_state ".
					  "where (order_retail_operator is null " .
					  "or project_retail_coordinator is null " .
					  "or project_real_opening_date is null " . 
					  "or project_real_opening_date = '0000-00-00') " .
					  "and order_actual_order_state_code = '100' ".
					  "   and " . $list_filter;
	}
	else
	{
		$sql_images = "select project_id from projects " .
					  "left join orders on order_id = project_order " .
					  "where (order_retail_operator is null " .
					  "or project_retail_coordinator is null " .
					  "or project_real_opening_date is null " . 
					  "or project_real_opening_date = '0000-00-00') " .
					  "and order_actual_order_state_code = '100' ".
					  "   and " . $list_filter;
	}

    $res = mysql_query($sql_images) or dberror($sql_images);

    while ($row = mysql_fetch_assoc($res))
    {
        $images[$row["project_id"]] = "/pictures/bullet_ball_glass_red.gif";
    }
}


// check if all 1:n relations in the process are fully processed
$fully_processed = array();
$cms_approved_states = array();
$cms_controlled_states = array();
$pos_pictures = array();
$missing_pos_pictures = array();
if (has_access("has_access_to_all_projects"))
{
    $sql_tmp = $sql . " where " . $list_filter;
    $res = mysql_query($sql_tmp);

    while($row = mysql_fetch_assoc($res))
    {
        $att = "";

        if($row['order_actual_order_state_code'] >= ORDER_TO_SUPPLIER_SUBMITTED)
        {
            //check if all items were ordered
            $ordered = check_if_all_items_hav_order_date($row['order_id'], "");
            if ($ordered == 0)
            {
                $fully_processed[$row['project_id']] = "/pictures/wf_warning.gif";
            }
        }
        elseif($row['order_actual_order_state_code'] >= REQUEST_FOR_DELIVERY_SUBMITTED)
        {
            //check if a request for delivery was made for all items
            $delivered = check_requast_for_deleivery_for_all_items($row['order_id'], REQUEST_FOR_DELIVERY_SUBMITTED);
            if ($delivered == 0)
            {
                $fully_processed[$row['project_id']] = "/pictures/wf_warning.gif";
            }
        }

		if($row['project_actual_opening_date'] != NULL and $row['project_actual_opening_date'] != '0000-00-00')
		{
			$aods[$row["project_id"]] = "/pictures/ok.gif";
		}
		else
		{
			$aods[$row["project_id"]] = "/pictures/not_ok.gif";
		}

		if($row['project_cost_cms_completed2'] == 1)
		{
			$cms_states[$row["project_id"]] = "/pictures/ok.gif";
		}
		else
		{
			$cms_states[$row["project_id"]] = "/pictures/not_ok.gif";
		}


		if($row['project_cost_cms_approved'] == 1)
		{
			$cms_approved_states[$row["project_id"]] = "/pictures/ok.gif";
		}
		else
		{
			$cms_approved_states[$row["project_id"]] = "/pictures/not_ok.gif";
		}
		
		
		if($row["project_cost_type"] == 1)
		{
			if($row['project_cost_cms_controlled'] == 1)
			{
				$cms_controlled_states[$row["project_id"]] = "/pictures/ok.gif";
			}
			else
			{
				$cms_controlled_states[$row["project_id"]] = "/pictures/not_ok.gif";
			}
		}
	
		
		$sql_p = "select count(order_file_id) as num_recs from order_files " . 
				 " where order_file_category = 11 " . 
			     " and order_file_order = " . dbquote($row["project_order"]);

		$res_p = mysql_query($sql_p) or dberror($sql_p);
		$row_p = mysql_fetch_assoc($res_p);

		if ($row['project_postype'] == 1 and $row_p["num_recs"] > 4)
		{
			$pos_pictures[$row["project_id"]] = "/pictures/ok.gif";die;
		}
		elseif($row['project_postype'] == 2 and  $row_p["num_recs"] > 2)
		{
			$pos_pictures[$row["project_id"]] = "/pictures/ok.gif";
		}
		elseif($row['project_postype'] > 2 and $row_p["num_recs"] > 0) {
			$pos_pictures[$row["project_id"]] = "/pictures/ok.gif";
		}
		else
		{
			$pos_pictures[$row["project_id"]] = "/pictures/not_ok.gif";
			$missing_pos_pictures[] =  $row["project_id"];
		}
		


    }
}


//$list_filter .= " and project_cost_cms_completed2 = 1 and order_actual_order_state_code >= 800 ";
$list_filter .= " and project_cost_cms_completed2 = 1";

if(param("statusfilter") == 2 and count($missing_pos_pictures) > 0)
{
	$list_filter .= " and project_id in (" . implode(', ', $missing_pos_pictures). ") ";
}
elseif(param("statusfilter") == 3)
{
	$list_filter .= " and project_cost_cms_approved <> 1";
}
elseif(param("statusfilter") == 4)
{
	$list_filter .= " and project_cost_cms_controlled <> 1 and project_cost_type = 1";
}
elseif(param("statusfilter") == 5)
{
	$list_filter .= " and (project_actual_opening_date is null or project_actual_opening_date = '0000-00-00' )";
}



/********************************************************************
    Create List
*********************************************************************/ 
$status_filter = array();
$status_filter[1] = "Show all";
$status_filter[2] = "Missing POS Images";
//$status_filter[3] = "Development CMS not completed";
//$status_filter[4] = "Not approved by controller";
$status_filter[5] = "Missing POS Opening Date";
asort($status_filter);


$list = new ListView($sql);

$list->set_entity("projects");
$list->set_order("left(projects.date_created, 10) desc");
$list->set_filter($list_filter);   


$list->add_listfilters("statusfilter", "Status", 'select', $status_filter, param("statusfilter"));
$list->add_listfilters("search_term", "Search Term", 'input', "", param("search_term"));


$list->add_image_column("todo", "Job", 0, $images);


if (preg_match("/" . APPLICATION_URLPART . "/", $_SERVER["HTTP_HOST"]))
{
	$link = APPLICATION_URL . "/user";
}
else
{
	$link = "/user";
}

$list->add_column("project_number", "Project No.", $link . "/project_task_center.php?pid={project_id}", "", "", COLUMN_NO_WRAP);


if (has_access("has_access_to_all_projects"))
{
	/*
	$list->add_image_column("cms1", "CMS\nLogistic", COLUMN_BREAK, $cms_states);
	$list->add_image_column("cms2", "CMS\nDevelopment", COLUMN_BREAK, $cms_approved_states);
	$list->add_image_column("cms3", "CMS\nController", COLUMN_BREAK, $cms_controlled_states);
	*/
	
	$list->add_image_column("aod", "Opening\nDate", COLUMN_BREAK, $aods);
	$list->add_image_column("aod", "POS\nImages", COLUMN_BREAK, $pos_pictures);

	
}


$list->add_column("project_costtype_text", "Legal Type", "", 0, "", COLUMN_NO_WRAP);
//$list->add_column("product_line_name", "Product Line", "", 0, "", COLUMN_NO_WRAP);
$list->add_column("postype_name", "POS Type", "", 0, "", COLUMN_NO_WRAP);



$list->add_image_column("order_state", "", 0, $fully_processed);

if(has_access("has_access_to_order_status_report_in_projects"))
{
    $popup_link = "popup1:project_status_report.php?pid=";
    $list->add_column("order_actual_order_state_code", "Status", $popup_link . "{project_id}");
}
else
{
    $list->add_column("order_actual_order_state_code", "Status");
}

$list->add_column("real_opening_date", "Agreed \nOpening Date", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK | COLUMN_UNDERSTAND_HTML);


$list->add_column("country_name", "Country", "", 0, "");
$list->add_column("concat(order_shop_address_place,', ', order_shop_address_company)", "POS Address");

$list->add_column("project_retail_coordinator", "PL" , "", 0, "");
//$list->add_column("order_retail_operator", "RTLC", "", 0, "");

$link = "projects_cms_completed_xls.php?search_term=" . param('search_term') ."&statusfilter=" . param("statusfilter");
$link = "javascript:popup('" . $link . "');";
$list->add_button("print_list", "Print List", $link);


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$list->process();


if ($list->button("new"))
{
    redirect("project_new_01.php");
}
if ($list->button("show_all"))
{
    $link = "projects.php?showall=1";
    redirect($link);
}
else if ($list->button("show_my_list"))
{
    $link = "projects.php";
    redirect($link);
}
else if ($list->button("extend"))
{
    if (param("showall"))
    {
        $link = "projects_extended_view.php?showall=1";
    }
    else
    {
        $link = "projects_extended_view.php";
    }
    redirect($link);
}
else if ($list->button("archive"))
{
    $link = "../archive/projects_archive.php";
    redirect($link);
}

$page = new Page("projects");

require_once("include/projects_list_page_actions.php");

$page->header();
$page->title("Projects with completed CMS");
$list->render();

?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#dashboard').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/user/dashboard_projects.php'
    });
    return false;
  }); 
});
</script>

<?php

$page->footer();
?>