<?php
/********************************************************************

    project_add_special_item_individual.php

    Add special item positions to an order only for supplier's use

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2003-01-23
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-02-16
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";


check_access("can_edit_his_list_of_materials_in_projects");


/********************************************************************
    prepare all data needed
*********************************************************************/

// read project and order details
$project = get_project(param("pid"));


// get company's address
$client_address = get_address($project["order_client_address"]);

// get user data
$user_data = get_user(user_id());


// get Supplier currency
$supplier_currency = get_address_currency($user_data["address"]);

// create sql for the supplier listbox
$sql_suppliers = "select address_id, address_company ".
                 "from addresses ".
                 "where address_type = 2 and address_active = 1 " .
                 "order by address_company";


$supplier_currencies = get_suplier_currencies($user_data["address"]);

$sql_supplier_currencies = "select currency_id, currency_symbol " . 
						   " from currencies " . 
						   " where currency_id in ( " . implode(',', $supplier_currencies) . ") " . 
						   " order by currency_symbol";



$sql_units = "select unit_id, unit_name " . 
             " from units " . 
			 " order by unit_name";

$sql_packaging_types = "select packaging_type_id, packaging_type_name " . 
             " from packaging_types " . 
			 " order by packaging_type_name";


//get supplying options
$supplying_options["address_only_quantity_proposal"] = 0;
$supplying_options["address_cost_group"] = 0;
$supplying_options["address_costmonitoring_group"] = 0;
$supplying_options["address_currency"] = 1;

$sql = "select address_only_quantity_proposal, address_cost_group, address_costmonitoring_group, address_currency " . 
       " from addresses " . 
	   " where address_id = " . $user_data["address"];

$res = mysql_query($sql) or dberror($sql);
$supplying_options = mysql_fetch_assoc($res);


/********************************************************************
    build form
*********************************************************************/
$form = new Form("order_items", "order_item");


$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid",$project["project_order"]);
$form->add_hidden("order_item_id", id());
$form->add_hidden("order_item_order", $project["project_order"]);
$form->add_hidden("order_item_type", ITEM_TYPE_SPECIAL);


require_once "include/project_head_small.php";


$form->add_section("Item Information");
$form->add_multiline("order_item_text", "Description*", 4, NOTNULL);

$form->add_section("Volumes and Weight");
$form->add_list("order_item_unit_id", "Unit",$sql_units, 0);
$form->add_edit("order_item_width", "Width in cm", 0, "" , TYPE_DECIMAL, 10, 2);
$form->add_edit("order_item_height", "Height in cm", 0, "" , TYPE_DECIMAL, 10, 2);
$form->add_edit("order_item_length", "length in cm", 0, "" , TYPE_DECIMAL, 10, 2);
$form->add_edit("order_item_gross_weight", "Gross Weight in kg", 0, "" , TYPE_DECIMAL, 10, 2);
$form->add_list("order_item_packaging_type_id", "Packaging",$sql_packaging_types, 0);
$form->add_checkbox("order_item_stackable", "", "", 0, "Stackable");

if($supplying_options["address_only_quantity_proposal"] == 1)
{
	
	$form->add_section("Quantity");
	$form->add_edit("order_item_quantity", "Quantity*", NOTNULL);
	$form->add_hidden("order_item_system_price", 0);
	$form->add_hidden("order_item_supplier_address", $user_data["address"]);
	
	$form->add_hidden("suppliers_currency", $supplying_options["address_currency"]);
	$form->add_hidden("order_item_supplier_price", 0);
}
else
{
	$form->add_section("Quantity and Price");
	$form->add_edit("order_item_quantity", "Quantity*", NOTNULL);
	$form->add_hidden("order_item_system_price", 0);
	$form->add_hidden("order_item_supplier_address", $user_data["address"]);
	$form->add_list("suppliers_currency", "Currency*",$sql_supplier_currencies, NOTNULL);
	$form->add_edit("order_item_supplier_price", "Supplier's Price", 0, "", TYPE_DECIMAL, 20,2);
}


$form->add_section("Other Information");
$form->add_edit("order_item_supplier_item_code", "Item Code");
$form->add_edit("order_item_offer_number", "Offer Number");
$form->add_edit("order_item_production_time", "Production Time");


$form->add_hidden("order_item_only_quantity_proposal", $supplying_options["address_only_quantity_proposal"]);
$form->add_hidden("order_item_cost_group", $supplying_options["address_cost_group"]);
//$form->add_hidden("order_items_costmonitoring_group", $supplying_options["address_costmonitoring_group"]);


$form->add_button("save_data", "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


if ($form->button("save_data"))
{
    if ($form->validate())
    {
		project_add_special_item_save_supplier_data($form);
        $link = "project_edit_material_list_supplier.php?pid=" . param("pid"); 
        redirect($link);
    }
}


   
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Add Supplier's Special Item");
$form->render();
require_once "include/project_footer_logistic_state.php";

$page->footer();


?>