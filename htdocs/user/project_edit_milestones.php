<?php
/********************************************************************

    project_edit_milestones.php

    Edit MileStones of the Projecs

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-10-24
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-16
    Version:        1.0.1

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_edit_milestones");

register_param("pid");

/********************************************************************
    prepare all data needed
*********************************************************************/
$user_roles = get_user_roles(user_id());

// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

// get System currency
$order_currency = get_order_currency($project["project_order"]);


/********************************************************************
    Create Cost Monitoring Sheet
*********************************************************************/ 
//check if the cost monitoring sheet exists already
$sql = "select count(project_cost_id) as num_recs " .
       "from project_costs " .
       "where project_cost_order = "  . $project["project_order"]; 

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

if($row["num_recs"] == 0)
{
    $fields = array();
    $values = array();

    $fields[] = "project_cost_order";
    $values[] = $project["project_order"];
    
    $fields[] = "date_created";
    $values[] = dbquote(date("Y-m-d"));

    $fields[] = "date_modified";
    $values[] = dbquote(date("Y-m-d"));

    $fields[] = "user_created";
    $values[] = dbquote(user_login());

    $fields[] = "user_modified";
    $values[] = dbquote(user_login());

    $sql = "insert into project_costs (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

    mysql_query($sql) or dberror($sql);
}


/********************************************************************
    Create Project Milestones
*********************************************************************/ 
$new_cer_version = 0;

$sql = "select * from milestones " .
	   "where milestone_active = 1 " .
	   "order by milestone_code ";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$sql_m = "select count(project_milestone_id) as num_recs from project_milestones " .
		     "where project_milestone_project = " . $project["project_id"] . 
	         "   and project_milestone_milestone = " . $row["milestone_id"];
	
	
	$res_m = mysql_query($sql_m) or dberror($sql_m);
	$row_m = mysql_fetch_assoc($res_m);

	if($row_m["num_recs"] == 0)
	{
		$fields = array();
		$values = array();

		$fields[] = "project_milestone_project";
		$values[] = $project["project_id"];

		$fields[] = "project_milestone_milestone";
		$values[] = $row["milestone_id"];

		$fields[] = "project_milestone_date_enterd_by_user";
		$values[] = user_id();

		$fields[] = "date_created";
		$values[] = dbquote(date("Y-m-d"));

		$fields[] = "date_modified";
		$values[] = dbquote(date("Y-m-d"));

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());

		if($row["milestone_is_inr03_milestone"] == 1 and $project["project_cost_type"] == 1)
		{
			$sql = "insert into project_milestones (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			mysql_query($sql) or dberror($sql);
		}
		elseif($row["milestone_is_inr03_milestone"] == 0)
		{
			$sql = "insert into project_milestones (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			mysql_query($sql) or dberror($sql);
		}
	}
}

/********************************************************************
    Get Cost Monitoring Sheet Budget Data
*********************************************************************/ 
$sql =  "select * from project_costs " .
        "where project_cost_order = " . $project["project_order"];

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

$project_cost_sqms = $row["project_cost_sqms"];

$project_cost_CER = $row["project_cost_CER"];
$project_cost_milestone_remarks = $row["project_cost_milestone_remarks"];
$project_cost_milestone_remarks2 = $row["project_cost_milestone_remarks2"];
$project_cost_milestone_turnaround = $row["project_cost_milestone_turnaround"];

/********************************************************************
    Caclucalte dates and diffeneces in dates
*********************************************************************/
$dates = array();
$comments = array();
$datecomments = array();
$daysconsumed = array();
$daysaccumulated = array();
$lastdate = "";
$milestone_turnaround = "";
$milestones_ids = array();


//get trunaround
$date_min = "0000-00-00";
$sql_m = "select min(project_milestone_date) as date_min " .
         "from project_milestones " .
         " where project_milestone_milestone<> 33 " . 
		 " and project_milestone_date <> '0000-00-00' " . 
		 " and project_milestone_date is not NULL " . 
		 " and project_milestone_project = " . $project["project_id"];
$res = mysql_query($sql_m) or dberror($sql_m);
if($row = mysql_fetch_assoc($res))
{
	$date_min = $row["date_min"];
}

$date_max = "0000-00-00";
$sql_m = "select max(project_milestone_date) as date_max " . 
         "from project_milestones " .
         " where project_milestone_milestone<> 33 " .
		 " and project_milestone_date <> '0000-00-00' " . 
		 " and project_milestone_date is not NULL " . 
		 " and project_milestone_project = " . $project["project_id"];
$res = mysql_query($sql_m) or dberror($sql_m);
if($row = mysql_fetch_assoc($res))
{
	$date_max = $row["date_max"];
}

$milestone_turnaround = floor((strtotime($date_max) - strtotime($date_min)) / 86400);


$sql = "select project_milestone_id, project_milestone_date, project_milestone_date_comment, " .
       "project_milestone_comment, milestone_id, milestone_code, milestone_text " . 
       "from project_milestones " .
       " left join milestones on milestone_id = project_milestone_milestone ";


//check if project has an IN03 - request for additional funding
$has_in03 = false;
$sql_r = "select count(cer_refunding_id) as num_recs " . 
         "from cer_refundings ". 
		 " where cer_refunding_project = ". $project["project_id"];

$res_r = mysql_query($sql_r) or dberror($sql_r);
$row_r = mysql_fetch_assoc($res_r);

if($row_r["num_recs"] > 0)
{
	$has_in03 = true;	
}

if($has_in03 == true)
{
	$list_filter = "project_milestone_visible = 1 and project_milestone_project = " . $project["project_id"];
}
else
{
	$list_filter = "milestone_is_inr03_milestone = 0 and " . 
		           "project_milestone_visible = 1 and project_milestone_project = " . $project["project_id"];
}


if($project["project_projectkind"] == 5) //Lease Renewal
{
	$list_filter .= " and milestone_code <= '0030' ";	
}
elseif($project["project_projectkind"] == 4) //Takeover
{
	$list_filter .= " and milestone_code < '0100' ";	
}


if($project["project_cost_type"] == 2) //Franchisee
{
	//$list_filter .= " and milestone_code < '1250' ";	
}



$sql_m = $sql . " where " . $list_filter . " order by milestone_code ";

$res = mysql_query($sql_m) or dberror($sql_m);
$num_of_visible_milestones = 0;
$removers = array();
while($row = mysql_fetch_assoc($res))
{
	$dates[$row["project_milestone_id"]] = to_system_date($row["project_milestone_date"]);
	$datecomments[$row["project_milestone_id"]] = $row["project_milestone_date_comment"];
    $comments[$row["project_milestone_id"]] = $row["project_milestone_comment"];
	$milestones_ids[$row["project_milestone_id"]] = $row["milestone_id"];

	if($row["project_milestone_date"] == NULL
		or $row["project_milestone_date"] == '0000-00-00') {
		$removers[$row["project_milestone_id"]] = '<img id="m'.$row["project_milestone_id"].'"style="cursor:pointer;" src="../pictures/closed.gif" />';
	}
	

	if($lastdate != "0000-00-00" and $lastdate != NULL and $row["project_milestone_date"] != "0000-00-00" and $row["project_milestone_date"] != NULL)
	{
		$thisdate = $row["project_milestone_date"];
		$difference_in_days = floor((strtotime($thisdate) - strtotime($date_min)) / 86400);
		
		
		
		$daysconsumed[$row["project_milestone_id"]] = $difference_in_days;
		
		
		//$milestone_turnaround = $milestone_turnaround + $difference_in_days;
		//$daysaccumulated[$row["project_milestone_id"]] = $milestone_turnaround;
	}

	if($row["project_milestone_date"] != "0000-00-00" and $row["project_milestone_date"] != NULL)
	{
		$lastdate = $row["project_milestone_date"];
	}

	$num_of_visible_milestones++;
}

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid",$project["project_order"]);

require_once "include/project_head_small.php";

$form->add_label("project_cost_sqms", "Sales Surface sqms", 0, $project_cost_sqms);

if($project["project_cost_type"])
{
    $form->add_label("project_cost_type", "Project Legal Type", $project["project_cost_type"]);
}
else
{
    $form->add_label("dummy", "Project Legal Type");

}


$form->add_edit("project_cost_CER", "CER Amount " . $order_currency["symbol"], 0 , $project_cost_CER, TYPE_DECIMAL, 12,2);

$form->add_edit("project_cost_milestone_turnaround", "Turn Around " , 0 , $milestone_turnaround, TYPE_DECIMAL, 12,2);

$form->add_multiline("project_cost_milestone_remarks", "Remarks", 6, 0, $project_cost_milestone_remarks);
$form->add_multiline("project_cost_milestone_remarks2", "Remarks 2 (MIS)", 6, 0, $project_cost_milestone_remarks2);

$form->add_section("Project Milestone Selector");
$icon = "../pictures/add_item.gif";
$link = "javascript:open_selector('')";
$form->add_label_selector("milestones", "Milestone Selector", 0, "Please select the milestones for this project.", $icon, $link);


if(param("s") == 1)
{
    $form->message = "Your changes have been saved.";
}

if($num_of_visible_milestones > 0)
{
	$form->add_button("remove_milestones", "Remove all Milestones");
}
else
{
	$form->add_button("show_milestones", "Add all Milestones");
}

$form->add_button("save_form", "Save Data");



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Create List for Milestones
*********************************************************************/ 
$list = new ListView($sql);

$list->set_title("Milestones");
$list->set_entity("project_milestones");
$list->set_filter($list_filter);
$list->set_order("milestone_code");


$list->add_text_column("removers", "", COLUMN_UNDERSTAND_HTML, $removers);
$list->add_column("milestone_code", "Code"); 
$list->add_column("milestone_text", "Description");

$list->add_date_edit_column("project_milestone_date", "Date", "8", 0, $dates);
$list->add_text_column("daysconsumed", "Days", COLUMN_ALIGN_RIGHT, $daysconsumed);
//$list->add_text_column("daysaccumulated", "Sum", COLUMN_ALIGN_RIGHT, $daysaccumulated);
$list->add_edit_column("project_milestone_date_comment", "Date Comment", "30", 0, $datecomments);
$list->add_edit_column("project_milestone_comment", "General Comment", "55", 0, $comments);


$list->populate();
$list->process();


/********************************************************************
    save data
*********************************************************************/

if ($form->button("save_form") or $form->button("project_cost_type"))
{
    
    if ($form->validate())
    {
        $sql = "update project_costs set " .
               "project_cost_CER = '" . $form->value("project_cost_CER") . "', " .
               "project_cost_milestone_turnaround = " . dbquote($form->value("project_cost_milestone_turnaround")) . ", " .
               "project_cost_milestone_remarks = " . dbquote($form->value("project_cost_milestone_remarks")) . ", " .
			   "project_cost_milestone_remarks2 = " . dbquote($form->value("project_cost_milestone_remarks2")) .
               " where project_cost_order = " . $project["project_order"];

        $result = mysql_query($sql) or dberror($sql);

        $dates = $list->values("project_milestone_date");

        $comments = $list->values("project_milestone_comment");

		$datecomments = $list->values("project_milestone_date_comment");

        foreach ($dates as $key=>$value)
        {
            
			//get old date value of milestone
			$old_date_value_of_milestone = '';
			$sql = "select project_milestone_date from project_milestones where project_milestone_id = " . $key;
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res) and $value)
			{
				$old_date_value_of_milestone = $row["project_milestone_date"];
			}

			
			//update milestones in project
			$project_milestone_fields = array();
			
			if($value and is_date_value($value))
            {
                $project_milestone_fields[] = "project_milestone_date = " . dbquote(from_system_date($value));
			}
			else {
				$project_milestone_fields[] = "project_milestone_date = NULL";
			}


			if($comments[$key])
            {
                $project_milestone_fields[] = "project_milestone_comment = " . dbquote($comments[$key]);
            }
			else
			{
				$project_milestone_fields[] = "project_milestone_comment = ''";
			}
			if($datecomments[$key])
            {
                $project_milestone_fields[] = "project_milestone_date_comment = " . dbquote($datecomments[$key]);
            }
			else
			{
				$project_milestone_fields[] = "project_milestone_date_comment = ''";
			}
            

            $value1 = "current_timestamp";
            $project_milestone_fields[] = "date_modified = " . $value1;

            if (isset($_SESSION["user_login"]))
            {
                $value1 = dbquote($_SESSION["user_login"]);
                $project_milestone_fields[] = "user_modified = " . $value1;
            }
       
            $sql = "update project_milestones set " . join(", ", $project_milestone_fields) . " where project_milestone_id = " . $key;
            mysql_query($sql) or dberror($sql);


			//LN/AF approval
			$ln_was_approved = false;
			if(($milestones_ids[$key] == 13 or $milestones_ids[$key] == 39)
				and $value 
				and ($old_date_value_of_milestone == NULL or $old_date_value_of_milestone == '0000-00-00'))
			{
				$ln_was_approved = true;
			}


			//update ln_basicdata and lock ln
			if($ln_was_approved == true) {
				$fields = array();

				$fields[] = "ln_basicdata_locked = 1";

				$value1 = "current_timestamp";
				$fields[] = "date_modified = " . $value1;

				if (isset($_SESSION["user_login"]))
				{
					$value1 = $_SESSION["user_login"];
					$fields[] = "user_modified = " . dbquote($value1);
				}

				$sql = "update ln_basicdata set " . join(", ", $fields) . " where ln_basicdata_version = 0 and ln_basicdata_project = " . param("pid");
				mysql_query($sql) or dberror($sql);

				$new_cer_version = create_cer_version(param("pid"), $context = "ln", $project["pipeline"]);
			}


			//CER/INR03 approval
			if(($milestones_ids[$key] == 12 or $milestones_ids[$key] == 21) 
				and $value
				and ($old_date_value_of_milestone == NULL or $old_date_value_of_milestone == '0000-00-00'))
			{
				$fields = array();

				$fields[] = "cer_basicdata_cer_locked = 1";

				$value1 = "current_timestamp";
				$fields[] = "date_modified = " . $value1;

				if (isset($_SESSION["user_login"]))
				{
					$value1 = $_SESSION["user_login"];
					$fields[] = "user_modified = " . dbquote($value1);
				}

				$sql = "update cer_basicdata set " . join(", ", $fields) . " where cer_basicdata_project = " . param("pid");
				mysql_query($sql) or dberror($sql);
				
				$new_cer_version = create_cer_version(param("pid"), $context = "cer", $project["pipeline"]);
			}



			//update KL approved money

			if(($milestones_ids[$key] == 12 or $milestones_ids[$key] == 29) 
				and $value
				and ($old_date_value_of_milestone == NULL or $old_date_value_of_milestone == '0000-00-00'))
			{
				$sql_i = "select cer_investment_id, cer_investment_amount_cer_loc from cer_investments
				       where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote(param("pid"));
				

				$result_i = mysql_query($sql_i) or dberror($sql_i);
				while ($row_i = mysql_fetch_assoc($result_i))
				{
					$sql_u = "update cer_investments
					          set cer_investment_amount_cer_loc_approved = " . dbquote($row_i['cer_investment_amount_cer_loc']) .
							  " where cer_investment_id = " . dbquote($row_i['cer_investment_id']);
					
					$result_u = mysql_query($sql_u) or dberror($sql_u);
				}
				
			}
			elseif($milestones_ids[$key] == 13 
				and $project['project_cost_type'] == 2
				and $value
				and ($old_date_value_of_milestone == NULL or $old_date_value_of_milestone == '0000-00-00'))
			{
				$sql_i = "select cer_investment_id, cer_investment_amount_cer_loc from cer_investments
				       where cer_investment_cer_version = 0 and cer_investment_project = " . dbquote(param("pid"));
				

				$result_i = mysql_query($sql_i) or dberror($sql_i);
				while ($row_i = mysql_fetch_assoc($result_i))
				{
					$sql_u = "update cer_investments
					          set cer_investment_amount_cer_loc_approved = " . dbquote($row_i['cer_investment_amount_cer_loc']) .
							  " where cer_investment_id = " . dbquote($row_i['cer_investment_id']);
					
					$result_u = mysql_query($sql_u) or dberror($sql_u);
				}
				
			}


			//send email notifications
			$subject = '';
			$sql_mail_alert = '';
			if($ln_was_approved == true and $milestones_ids[$key] == 39 //
				and $project["project_cost_type"] == 1
				and $project["project_projectkind"] != 4
				and $project["project_projectkind"] != 5
				and $project['project_production_type'] == 4)
			{
				$sql_mail_alert = "select * from mail_alert_types " . 
									"where mail_alert_type_id = 46";

				$subject = "LN approval - Project " . $project["order_number"] . ", " .	$project["order_shop_address_country_name"] . ", " . $project["order_shop_address_place"] . ", " . $project["order_shop_address_company"];
			}
			elseif($ln_was_approved == true
				and $project["project_cost_type"] == 1
				and $project["project_projectkind"] != 4
				and $project["project_projectkind"] != 5
				and ($project['project_production_type'] == 1 or $project['project_production_type'] == 2))
			{
				
				$sql_mail_alert = "select * from mail_alert_types " . 
									"where mail_alert_type_id = 44";

				$subject = "LN approval - Project " . $project["order_number"] . ", " .	$project["order_shop_address_country_name"] . ", " . $project["order_shop_address_place"] . ", " . $project["order_shop_address_company"];
			}
			elseif($ln_was_approved == true 
				and $project["project_cost_type"] == 2
				and $project["project_projectkind"] != 4
				and $project["project_projectkind"] != 5
				and ($project['project_production_type'] == 1 or $project['project_production_type'] == 2))
			{
				
				$sql_mail_alert = "select * from mail_alert_types " . 
									"where mail_alert_type_id = 45";

				$subject = "AF approval - Project " . $project["order_number"] . ", " .	$project["order_shop_address_country_name"] . ", " . $project["order_shop_address_place"] . ", " . $project["order_shop_address_company"];
			}
			elseif($ln_was_approved == true 
				and $project["project_cost_type"] == 1
				and $project["project_projectkind"] != 4
				and $project["project_projectkind"] != 5
				and $project['project_production_type'] == 4)
			{
				$sql_mail_alert = "select * from mail_alert_types " . 
									"where mail_alert_type_id = 46";

				$subject = "LN approval - Project " . $project["order_number"] . ", " .	$project["order_shop_address_country_name"] . ", " . $project["order_shop_address_place"] . ", " . $project["order_shop_address_company"];
			}
			elseif($ln_was_approved == true 
				and $project["project_cost_type"] == 2
				and $project["project_projectkind"] != 4
				and $project["project_projectkind"] != 5
				and $project['project_production_type'] == 4)
			{
				
				$sql_mail_alert = "select * from mail_alert_types " . 
									"where mail_alert_type_id = 47";

				$subject = "AF approval - Project " . $project["order_number"] . ", " .	$project["order_shop_address_country_name"] . ", " . $project["order_shop_address_place"] . ", " . $project["order_shop_address_company"];
			}
			elseif($ln_was_approved == true 
				and $project["project_cost_type"] == 1
				and ($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5))
			{
				$sql_mail_alert = "select * from mail_alert_types " . 
									"where mail_alert_type_id = 10";

				$subject = "LN approval - Project " . $project["order_number"] . ", " .	$project["order_shop_address_country_name"] . ", " . $project["order_shop_address_place"] . ", " . $project["order_shop_address_company"];
			}

			if($subject and $sql_mail_alert) {
				
				$cc_recipients = array();
				$cc_recipient_ids = array();
				
				//sender
				$user_data = get_user(user_id());
				$sender_email = $user_data["email"];
				$sender_name = $user_data["name"] . " " . $user_data["firstname"];
				
				//repipient
				$user_data = get_user($project["order_user"]);
				$recipient_email = $user_data["email"];
				$recipient_id = $project["order_user"];
				$recipient_firstname = $user_data["firstname"];
				$recipient_name = $user_data["name"] . " " . $user_data["firstname"];

				if($user_data["cc"])
				{
					$cc_recipients[$user_data["cc"]] = $user_data["cc"];

					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($user_data["cc"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$cc_recipient_ids[$user_data["cc"]] = $row_u["user_id"];
					}
				}

				if($user_data["deputy"])
				{
					$cc_recipients[$user_data["deputy"]] = $user_data["deputy"];

					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($user_data["deputy"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$cc_recipient_ids[$user_data["cc"]] = $row_u["user_id"];
					}
				}


				//get milstone notifications
				$sql_mail_milestones = "select * from project_milestones " . 
									   "left join milestones on milestone_id = project_milestone_milestone " . 
									   "where milestone_on_check_cer_lease = 1 and project_milestone_id = " . $key;

				$cc_fields = array();
				$cc_fields[] = 'milestone_email_' . $project['project_postype'];
				$cc_fields[] = 'milestone_ccmail_' . $project['project_postype'];
				$cc_fields[] = 'milestone_ccmail_' . $project['project_postype']. '_2';
				$cc_fields[] = 'milestone_ccmail_' . $project['project_postype']. '_3';
				$cc_fields[] = 'milestone_ccmail_' . $project['project_postype']. '_4';


				$res = mysql_query($sql_mail_milestones) or dberror($sql_mail_milestones);
				if ($row = mysql_fetch_assoc($res))
				{
					foreach($cc_fields as $key=>$field_name) {
						
						if($row[$field_name] 
							and $recipient_email != $row[$field_name]
							and !in_array($row[$field_name], $cc_recipients)) {
							$cc_recipients[$row[$field_name]] = $row[$field_name];

							$sql = 'select user_id from users ' . 
								   'where user_email = ' . dbquote($row[$field_name]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$cc_recipient_ids[$row[$field_name]] = $row_u["user_id"];
							}
						}
					}
				}

				$res = mysql_query($sql_mail_alert) or dberror($sql_mail_alert);
				if ($row = mysql_fetch_assoc($res))
				{
					$link = APPLICATION_URL . "/user/project_design_briefing.php?pid=" . $project['project_id'];

					$link2 = APPLICATION_URL . "/cer/cer_application_lease.php?pid=" . $project['project_id'];

					$link3 = APPLICATION_URL . "/user/project_task_center.php?pid=" . $project['project_id'];

					$mail_text = $row["mail_alert_mail_text"];
					$mail_text = str_replace('{name}', $recipient_firstname, $mail_text);
					$mail_text = str_replace('{link}', $link, $mail_text);
					$mail_text = str_replace('{link2}', $link2, $mail_text);
					$mail_text = str_replace('{link3}', $link3, $mail_text);


					$mail_text = str_replace('{sender}', $sender_name, $mail_text);

					$mail = new Mail();
					$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
					$mail->set_sender($sender_email, $sender_name);
					
					$mail->add_recipient($recipient_email);


					foreach($cc_recipients as $key=>$cc_email)
					{
						if($recipient_email != $cc_email)
						{
							$mail->add_cc($cc_email);
						}
					}

					$mail->add_text($mail_text);

					if($senmail_activated == true)
					{
						$result = $mail->send();
					}
					else
					{
						$result = 1;
					}

					if($result == 1)
					{
						
						if($project['project_production_type'] == 1 
							or $project['project_production_type'] == 2)
						{
							$link = "project_design_briefing.php?pid=" . $project['project_id'];
							append_task($project["project_order"], $project["order_user"], $mail_text,$link, "", user_id(), '200', 1, 0);
						}
						
						append_mail($project["project_order"], $project["order_user"],  user_id(), $mail_text, "200", 1,0, 0);

						
						foreach($cc_recipient_ids as $key=>$user_id)
						{
							if($user_id)
							{
								append_mail($project["project_order"], $user_id,  user_id(), $mail_text, "200", 1,0, 1);
							}
						}

					}
				}
			}
			elseif($milestones_ids[$key] == 40 
				and $value
				and ($old_date_value_of_milestone == NULL or $old_date_value_of_milestone == '0000-00-00'))
			{
				//send mail to client
				$link = APPLICATION_URL . "/user/project_task_center.php?pid=" . param("pid");
				$actionmail = new ActionMail(156);
				$actionmail->setParam('projectID', param("pid"));
				$actionmail->setParam('link', $link);
				$actionmail->send();
			}
			elseif($value
				and ($old_date_value_of_milestone == NULL or $old_date_value_of_milestone == '0000-00-00'))
			{
				
				$milestone_standard_text = '';
				$sql_m = "select milestone_mailtext from milestones " . 
									"where milestone_id = " . dbquote($milestones_ids[$key]);

				$res = mysql_query($sql_m) or dberror($sql_m);
				if ($row = mysql_fetch_assoc($res))
				{
					$milestone_standard_text = $row['milestone_mailtext'];
				}
				
				
				$sql_mail_alert = "select * from mail_alert_types " . 
									"where mail_alert_type_id = 49";
				
				
				//send mail alerts for all other milestones
				$subject = "Milestone - Project " . $project["order_number"] . ", " .	$project["order_shop_address_country_name"] . ", " . $project["order_shop_address_place"] . ", " . $project["order_shop_address_company"];


				$cc_recipients = array();
				$cc_recipient_ids = array();
				
				//sender
				$user_data = get_user(user_id());
				$sender_email = $user_data["email"];
				$sender_name = $user_data["name"] . " " . $user_data["firstname"];
				
				//repipient
				$user_data = get_user($project["order_user"]);
				$recipient_email = $user_data["email"];
				$recipient_id = $project["order_user"];
				$recipient_firstname = $user_data["firstname"];
				$recipient_name = $user_data["name"] . " " . $user_data["firstname"];

				if($user_data["cc"])
				{
					$cc_recipients[$user_data["cc"]] = $user_data["cc"];

					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($user_data["cc"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$cc_recipient_ids[$user_data["cc"]] = $row_u["user_id"];
					}
				}

				if($user_data["deputy"])
				{
					$cc_recipients[$user_data["deputy"]] = $user_data["deputy"];

					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($user_data["deputy"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$cc_recipient_ids[$user_data["cc"]] = $row_u["user_id"];
					}
				}


				//get milstone notifications
				$milestone_name = '';
				$sql_mail_milestones = "select * from project_milestones " . 
									   "left join milestones on milestone_id = project_milestone_milestone " . 
									   "where project_milestone_id = " . $key;
				$cc_fields = array();
				$cc_fields[] = 'milestone_email_' . $project['project_postype'];
				$cc_fields[] = 'milestone_ccmail_' . $project['project_postype'];
				$cc_fields[] = 'milestone_ccmail_' . $project['project_postype']. '_2';
				$cc_fields[] = 'milestone_ccmail_' . $project['project_postype']. '_3';
				$cc_fields[] = 'milestone_ccmail_' . $project['project_postype']. '_4';

				$res = mysql_query($sql_mail_milestones) or dberror($sql_mail_milestones);
				if ($row = mysql_fetch_assoc($res))
				{
					$milestone_name = $row['milestone_text'];
					foreach($cc_fields as $key=>$field_name) {
						
						if($row[$field_name] 
							and $recipient_email != $row[$field_name]
							and !in_array($row[$field_name], $cc_recipients)) {
							$cc_recipients[$row[$field_name]] = $row[$field_name];

							$sql = 'select user_id from users ' . 
								   'where user_email = ' . dbquote($row[$field_name]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$cc_recipient_ids[$row[$field_name]] = $row_u["user_id"];
							}
						}
					}
				}


				$res = mysql_query($sql_mail_alert) or dberror($sql_mail_alert);
				if ($row = mysql_fetch_assoc($res))
				{
					$link = APPLICATION_URL . "/user/project_view_milestones.php?pid=" . $project['project_id'];

					
					if($milestone_standard_text) {

						$mail_text = $milestone_standard_text;
						$mail_text = str_replace('{name}', $recipient_firstname, $mail_text);
						$mail_text = str_replace('{milestone_name}', $milestone_name, $mail_text);
						$mail_text = str_replace('{date}', $value, $mail_text);
						$mail_text = str_replace('{link}', $link, $mail_text);
						$mail_text = str_replace('{sender}', $sender_name, $mail_text);
					}
					else {
						$mail_text = $row["mail_alert_mail_text"];
						$mail_text = str_replace('{name}', $recipient_firstname, $mail_text);
						$mail_text = str_replace('{milestone_name}', $milestone_name, $mail_text);
						$mail_text = str_replace('{date}', $value, $mail_text);
						$mail_text = str_replace('{link}', $link, $mail_text);
						$mail_text = str_replace('{sender}', $sender_name, $mail_text);
					}

					$mail = new Mail();
					$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
					$mail->set_sender($sender_email, $sender_name);
					
					$mail->add_recipient($recipient_email);


					foreach($cc_recipients as $key=>$cc_email)
					{
						if($recipient_email != $cc_email)
						{
							$mail->add_cc($cc_email);
						}
					}

					$mail->add_text($mail_text);

					if($senmail_activated == true)
					{
						$result = $mail->send();
					}
					else
					{
						$result = 1;
					}


					if($result == 1)
					{
						
						append_mail($project["project_order"], $project["order_user"],  user_id(), $mail_text, "200", 1,0, 0);

						
						foreach($cc_recipient_ids as $key=>$user_id)
						{
							if($user_id)
							{
								append_mail($project["project_order"], $user_id,  user_id(), $mail_text, "200", 1,0, 1);
							}
						}

					}
				}
			}
		}
          
		$link = "project_edit_milestones.php?pid=" . param("pid") . "&s=1"; 
		redirect($link);

    }
}
elseif ($form->button("remove_milestones"))
{
	$sql_u = "update project_milestones " . 
			   "set project_milestone_visible = 0 " . 
			   " where project_milestone_project = " . param("pid");

	$result = mysql_query($sql_u) or dberror($sql_u);

	$link = "project_edit_milestones.php?pid=" . param("pid") . "&s=1"; 
        redirect($link);
}
elseif ($form->button("show_milestones"))
{
	$sql_u = "update project_milestones " . 
			   "set project_milestone_visible = 1 " . 
			   " where project_milestone_project = " . param("pid");

	$result = mysql_query($sql_u) or dberror($sql_u);

	$link = "project_edit_milestones.php?pid=" . param("pid") . "&s=1"; 
        redirect($link);
}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Project Milestones");
$form->render();

echo "<br>";
$list->render();


?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  
 
  $('#milestones_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/user/project_edit_milestone_selector.php?pid=<?php echo $project["project_id"]?>'
    });
    return false;
  });
});
</script>

<script type="text/javascript">
<?php
foreach($milestones_ids as $pmid=>$mid) {
?>
	var m = "<?php echo '#m' . $pmid;?>";

	$(m).click(function(e) {
		 $(this).closest("tr").hide();
		 $.ajax({
			type: "POST",
			data: 'pm=<?php echo $pmid;?>',
			url: "../shared/ajx_hide_milestone.php",
			success: function(msg){
				
			}
		});
	});
    

<?php
}
?>
</script>

<?php
if($new_cer_version > 0)
{
	$link = '/user/project_view_project_budget_pdf.php?pid=' . param("pid") . '&cerversion=' . $new_cer_version;
	$link2 = '/user/project_offer_comparison_pdf.php?pid=' . param("pid") . '&cerversion=' . $new_cer_version;

	?>
	<script language="javascript">

		$.ajax({
			type: "GET",
			data: '',
			url: "<?php echo $link;?>",
			success: function(msg){
			}
		});

		$.ajax({
			type: "GET",
			data: '',
			url: "<?php echo $link2;?>",
			success: function(msg){
			}
		});
	
	</script>
	<?php

	$link = "project_edit_milestones.php?pid=" . param("pid") . "&s=1"; 
	redirect($link);
}



require_once "include/project_footer_logistic_state.php";
$page->footer();

?>