<?php
/********************************************************************

    project_new03.php

    Creation of a new project step 03.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-07-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-07-14
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_create_new_projects");

if(!isset($_SESSION["new_project_step_1"]))
{
	$link = "project_new_01.php";
	redirect($link);
}
if(!isset($_SESSION["new_project_step_3"]) and $_SESSION["new_project_step_1"]["project_kind"] != 8)
{
	$_SESSION["new_project_step_3"] = array();
}
elseif(!isset($_SESSION["new_project_step_3a"]))
{
	$_SESSION["new_project_step_3a"] = array();
}

set_referer("project_new_04.php");

if(count($_POST) == 0 and isset($_SESSION["new_project_step_3"]))
{
	$_SESSION["new_project_step_3"]["action"] = "";
	foreach($_SESSION["new_project_step_3"] as $key=>$value)
	{
		register_param($key);
		param($key, $value);
	}
}
elseif(count($_POST) == 0 and isset($_SESSION["new_project_step_3a"]))
{
	$_SESSION["new_project_step_3a"]["action"] = "";
	foreach($_SESSION["new_project_step_3a"] as $key=>$value)
	{
		register_param($key);
		param($key, $value);
	}
}
/********************************************************************
    prepare all data needed
*********************************************************************/

// Vars
$design_objectives_listbox_names = array();
$design_objectives_checklist_names = array();

$roles = get_user_roles(user_id());

// get the region of users' address
$user_region = get_user_region(user_id());
// get user data
$user = get_user(user_id());

// get users' company address
$address = get_address($user["address"]);

// get client's currency
$currency = get_address_currency($user["address"]);

$sql = "select currency_id, currency_symbol, currency_exchange_rate, currency_factor " . 
       "from countries " .
	   " left join currencies on currency_id = country_currency " . 
	   " where country_id = " . dbquote($_SESSION["new_project_step_1"]["shop_address_country"]);

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$currency["symbol"] = $row["currency_symbol"];
	$currency["id"] = $row["currency_id"];
	$currency["exchange_rate"] = $row["currency_exchange_rate"];
    $currency["factor"] = $row["currency_factor"];
}

// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";


// create sql for the location type listbox
$sql_location_types = "select location_type_id, location_type_name ".
                      "from location_types ".
                      "order by location_type_name";

// create sql for the voltage listbox
$sql_voltages = "select voltage_id, voltage_name ".
                "from voltages";

// create sql design_objective_groups
$sql_design_objective_groups = "select design_objective_group_id, design_objective_group_name, " .
                               "    design_objective_group_multiple " .
                               "from design_objective_groups " .
                               "where design_objective_group_active = 1 and " . 
							   "design_objective_group_postype = " . $_SESSION["new_project_step_1"]["project_postype"] . " " .
                               "order by design_objective_group_priority";


$sql_distribution_channels = "select mps_distchannel_id, " .
                             "concat(mps_distchannel_group_name, ' - ', mps_distchannel_name , ' - ', mps_distchannel_code) as channel " . "from mps_distchannels " . 
							 "left join mps_distchannel_groups on mps_distchannel_groups.mps_distchannel_group_id = mps_distchannels.mps_distchannel_group_id " .
							 "where mps_distchannel_selectable_in_new_projects = 1 " . 
							 "order by mps_distchannel_group_name, mps_distchannel_name, mps_distchannel_code ";


//apply condition if client is owner company and client is an agent
$use_condition_for_agents = 0;
if(array_key_exists("new_project_step_2", $_SESSION)
   and array_key_exists("franchisee_address_id", $_SESSION["new_project_step_2"]))
{
	
	if($address["client_type"] == 1 
		and $user["address"] == $_SESSION["new_project_step_2"]["franchisee_address_id"])
	{
		$use_condition_for_agents = 1;
	}

	$standard_dsitribution_channel = get_standard_distribution_channel($_SESSION["new_project_step_1"]["project_cost_type"], $_SESSION["new_project_step_1"]["project_postype"], $_SESSION["new_project_step_1"]["project_pos_subclass"], $use_condition_for_agents);
}

$pos_data = array();
if(array_key_exists("posaddress_id", $_SESSION["new_project_step_1"]) 
	and $_SESSION["new_project_step_1"]["project_kind"] == 2) //renovation
{
	$pos_data = get_poslocation($_SESSION["new_project_step_1"]["posaddress_id"]);
}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("", "project");


if(isset($_SESSION["new_project_step_1"]) and $_SESSION["new_project_step_1"]["project_kind"] == 8)
{
	$form->add_section("PopUP for an existing POS Location");
}
else
{
	$form->add_section("POS Location");
}
$posaddress = $_SESSION["new_project_step_1"]["shop_address_company"] . ", " . $_SESSION["new_project_step_1"]["shop_address_zip"]. " " . $_SESSION["new_project_step_1"]["shop_address_place"];
$form->add_comment($posaddress);

if($_SESSION["new_project_step_1"]["project_kind"] == 8)
{
	
	$form->add_edit("project_popup_name", "PopUp Description*", NOTNULL, "", TYPE_CHAR, 0, 0, 1, "popup");


	$form->add_edit("project_planned_opening_date", "Planned Starting Date*", NOTNULL, "", TYPE_DATE, 20);
	$form->add_edit("project_planned_closing_date", "Planned Ending Date*", NOTNULL, "", TYPE_DATE, 20);


	$form->add_section("Other Information");
	$form->add_comment("Please use only figures to indicate the budget ".
					   "and enter the date in the form of dd.mm.yy");
	$form->add_edit("approximate_budget", "Approximate Budget in " . $currency["symbol"] . "*", NOTNULL, "", TYPE_DECIMAL, 20);
	$form->add_hidden("currency_exchange_rate", $currency["exchange_rate"]);
	$form->add_hidden("currency_factor", $currency["factor"]);

	$form->add_section(" ");
	$form->add_button("step5", "Proceed to next step");
	$form->add_button("back", "Back");
}
else
{
	if($standard_dsitribution_channel > 0)
	{
		$form->add_hidden("distribution_channel", $standard_dsitribution_channel);
	}
	elseif(($_SESSION["new_project_step_1"]["project_cost_type"] == 2 or $_SESSION["new_project_step_1"]["project_cost_type"] == 6) and $_SESSION["new_project_step_1"]["project_postype"] == 2) //other SIS
	{
		
		if($_SESSION["new_project_step_1"]["project_kind"] == 2 and count($pos_data) > 0 and $pos_data["posaddress_distribution_channel"] > 0) //renovation
		{
			$form->add_list("distribution_channel", "Distribution Channel*", $sql_distribution_channels, NOTNULL, $pos_data["posaddress_distribution_channel"]);

		}
		else
		{
			$form->add_list("distribution_channel", "Distribution Channel*", $sql_distribution_channels, NOTNULL);
		}
	}
	else
	{
		$form->add_hidden("distribution_channel", 0);
	}


	$form->add_section("Location Info");
	//$form->add_list("voltage", "Voltage Choice*", $sql_voltages, NOTNULL);
	$form->add_hidden("voltage");
	$form->add_list("location_type", "Location Type*", $sql_location_types);
	$form->add_edit("location_type_other", "Or Other Location Type*", 0, "", TYPE_CHAR, 20);

	/*
	if($_SESSION["new_project_step_1"]["project_postype"] == 2
	   or $_SESSION["new_project_step_1"]["project_postype"] == 3) // SIS or Kiosk
	{
	*/
		// count design_objective_groups
		$res = mysql_query($sql_design_objective_groups) or dberror($sql_design_objective_groups);
		$number_of_design_objective_groups = mysql_num_rows($res);

		if ($number_of_design_objective_groups > 0)
		{
			$form->add_section("Design Objectives");
			$form->add_comment("Please indicate your basic needs and requirements to assist out design process. ".
							   "A Project Leader will ensure that the design is consistent with the ".
							   "Tissot Retail objectives."); 

			$i = 1;
			
			while ($row = mysql_fetch_assoc($res))
			{
				$sql_design_objective_item = "select design_objective_item_id, design_objective_item_name ".
											 "from design_objective_items ".
											 "where design_objective_item_active = 1 " . 
											 "   and design_objective_item_group=" . $row["design_objective_group_id"] . " ".
											 "order by design_objective_item_priority";

				if ($row["design_objective_group_multiple"] == 0) 
				{
					$form->add_list("design_objective_items" . $i, $row["design_objective_group_name"] . "*", $sql_design_objective_item, NOTNULL);
					$design_objectives_listbox_names[] = "design_objective_items" . $i;
				}
				else 
				{
					$form->add_checklist("design_objective_items" . $i, $row["design_objective_group_name"], "project_items", $sql_design_objective_item, NOTNULL);
					$design_objectives_checklist_names[] = "design_objective_items" . $i;
				}
				
				$i++;   
			}
		}
	/*
	}
	*/

	//$form->add_section("Capacity Request by Client");
	//$form->add_edit("watches_displayed", "Watches Displayed", 0, "", TYPE_INT, 20);
	//$form->add_edit("watches_stored", "Watches Stored", 0, "", TYPE_INT, 20);

	$form->add_hidden("watches_displayed", 0);
	$form->add_hidden("watches_stored", 0);

	//$form->add_edit("bijoux_displayed", "Watch Straps Displayed", 0, "", TYPE_INT, 20);
	//$form->add_edit("bijoux_stored", "Watch Straps Stored", 0, "", TYPE_INT, 20);

	$form->add_hidden("bijoux_displayed", 0);
	$form->add_hidden("bijoux_stored", 0);

	$form->add_list("tissot_shop_around", "Is there a POS selling Tissot in 1km around the location*",array(2=>'yes', 1=>'no'), NOTNULL);

	$form->add_section("Other Information");
	$form->add_comment("Please use only figures to indicate the budget ".
					   "and enter the date in the form of dd.mm.yy");
	$form->add_edit("approximate_budget", "Approximate Budget in " . $currency["symbol"] . "*", NOTNULL, "", TYPE_DECIMAL, 20);
	$form->add_hidden("currency_exchange_rate", $currency["exchange_rate"]);
	$form->add_hidden("currency_factor", $currency["factor"]);


	if($_SESSION["new_project_step_1"]["project_kind"] == 3
		or $_SESSION["new_project_step_1"]["project_kind"] == 9) // takeover/renovation project
	{
		$form->add_edit("planned_takeover_date", "Planned Takeover Date*", NOTNULL, "", TYPE_DATE, 20);
	}

	$form->add_edit("planned_opening_date", "Preferred Opening Date*", NOTNULL, "", TYPE_DATE, 20);

	if($_SESSION["new_project_step_1"]["project_pos_subclass"] == 27) // temporary pos
	{
		$form->add_edit("planned_closing_date", "Planned Closing Date*", NOTNULL, "", TYPE_DATE, 20);
	}
	else
	{
		$form->add_hidden("planned_closing_date");
	}

	$form->add_section(" ");
	$form->add_button("step4", "Proceed to next step");
	$form->add_button("back", "Back");
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$form->populate();
$form->process();

//clean fields from apostrophes
foreach($form->items as $key=>$value)
{
	if(array_key_exists('value', $value)) {
		$form->value($key, str_replace('"', '', $form->value($key)));
	}
}

if ($form->button("back"))
{
	
	
	if($_SESSION["new_project_step_1"]["project_kind"] == 8)
	{
		$_SESSION["new_project_step_3a"] = $_POST;
		redirect("project_new_01.php");
	}
	else
	{
		$_SESSION["new_project_step_3"] = $_POST;
		redirect("project_new_02.php");
	}
}
elseif ($form->button("step4"))
{
	$form->add_validation("from_system_date({planned_opening_date}) >  " . dbquote(date("Y-m-d")), "The preferred opening date must be a future date!");
	
	if (!$form->value("location_type") and !$form->value("location_type_other"))
    {
        $form->add_validation("{location_type} or {location_type_other}", "The location type must not be empty.");
    }

	$form->add_validation("{approximate_budget}", "Approximate budget cannot be a zero value.");

	$error = 0;
	if($_SESSION["new_project_step_1"]["project_pos_subclass"] == 27) // temporary pos
	{
		if(from_system_date($form->value("planned_closing_date")) <= from_system_date($form->value("planned_opening_date")))
		{
			$error = 1;
		}
	}
	if($_SESSION["new_project_step_1"]["project_kind"] == 8) // PopUp
	{
		if(from_system_date($form->value("project_planned_closing_date")) <= from_system_date($form->value("project_planned_opening_date")))
		{
			$error = 1;
		}
	}

	if($error == 1)
	{
		$form->error("The planned closing date must be in the future of the planned opening date!");
	}
	elseif($form->validate())
	{
		
		$_SESSION["new_project_step_3"] = $_POST;
		$_SESSION["new_project_step_3"]["design_objectives_listbox_names"] = $design_objectives_listbox_names;
		$_SESSION["new_project_step_3"]["design_objectives_checklist_names"] = $design_objectives_checklist_names;
		

		$link = "project_new_04.php";
		redirect($link);
	}
}
elseif ($form->button("step5"))
{
	//$form->add_validation("from_system_date({project_planned_opening_date}) >  " . dbquote(date("Y-m-d")), "The planned starting date must be a future date!");
	
	//$form->add_validation("from_system_date({project_planned_closing_date}) >  " . dbquote(date("Y-m-d")), "The planned ending date must be a future date!");

	$error = 0;
	if($_SESSION["new_project_step_1"]["project_pos_subclass"] == 27) // temporary pos
	{
		if(from_system_date($form->value("planned_closing_date")) <= from_system_date($form->value("planned_opening_date")))
		{
			$error = 1;
		}
	}
	if($_SESSION["new_project_step_1"]["project_kind"] == 8) // PopUp
	{
		if(from_system_date($form->value("project_planned_closing_date")) <= from_system_date($form->value("project_planned_opening_date")))
		{
			$error = 1;
		}
	}

	if($error == 1)
	{
		$form->error("The planned closing date must be in the future of the planned opening date!");
	}
	elseif($form->validate())
	{
		
		$_SESSION["new_project_step_3a"] = $_POST;

		$link = "project_new_05.php";
		redirect($link);
	}
}
  
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

$page->register_action('home', 'Home', "welcome.php");

$page->header();
$page->title("Project Request");

echo "<div>";
echo "<img class='stepcounter' src='/pictures/numbers/01_off.gif' alt=\"\" />";
echo "<span class='step_inactive'>Basic Project Data</span>";

if($_SESSION["new_project_step_1"]["project_kind"] == 4) // Take over
{
	echo "<img class='stepcounter' src='/pictures/numbers/02_off.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Addresses</span>";
	echo "<img class='stepcounter' src='/pictures/numbers/03_on.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Environment</span>";
}
elseif($_SESSION["new_project_step_1"]["project_kind"] == 5) // lease renewal
{
	echo "<img class='stepcounter' src='/pictures/numbers/02_on.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Environment</span>";
}
elseif($_SESSION["new_project_step_1"]["project_kind"] == 8) // popup
{
	echo "<img class='stepcounter' src='/pictures/numbers/02_on.gif' alt=\"\" />";
	echo "<span class='step_inactive'>PopUp Information</span>";
	echo "<img class='stepcounter' src='/pictures/numbers/03_off.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Comments</span>";
}
else
{
	echo "<img class='stepcounter' src='/pictures/numbers/02_off.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Addresses</span>";
	echo "<img class='stepcounter' src='/pictures/numbers/03_on.gif' alt=\"\" />";
	echo "<span class='step_inactive'>POS Information</span>";
	echo "<img class='stepcounter' src='/pictures/numbers/04_off.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Environment</span>";
	echo "<img class='stepcounter' src='/pictures/numbers/05_off.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Comments</span>";
}
echo "<br /><br /><br /></div>";

$form->render();


?>

<div id="popup" style="display:none;">
    Please add a short description of the PopUplike e.g. <br />the name of the campaign (SISTEM51) or any other reason
</div> 

<?php
$page->footer();


?>