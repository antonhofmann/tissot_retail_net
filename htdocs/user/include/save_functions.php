<?php
/********************************************************************

    save_functions.php

    Various utility functions to save information into tables

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-08
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-02-23
    Version:        1.2.4

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "order_state_constants.php";


/********************************************************************
    get cc and deputy mail addresses by user_email
*********************************************************************/
function get_cc_deputy_by_email($email)
{
	$emails = array();
	$sql = "select user_email_cc, user_email_deputy " . 
		 "from users " . 
		 "where user_email = " . dbquote($email);

	$res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
		if(is_email_address($row["user_email_cc"]))
		{
			$emails[$row["user_email_cc"]] = $row["user_email_cc"];
		}
		if(is_email_address($row["user_email_deputy"]))
		{
			$emails[$row["user_email_deputy"]] = $row["user_email_deputy"];
		}
	}

	return $emails;
}

/********************************************************************
    get cc and deputy mail addresses by user_id
*********************************************************************/
function get_cc_deputy_by_user_id($user_id)
{
	$emails = array();
	$sql = "select user_email_cc, user_email_deputy " . 
		 "from users " . 
		 "where user_id = " . dbquote($user_id);

	$res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
		if(is_email_address($row["user_email_cc"]))
		{
			$emails[$row["user_email_cc"]] = $row["user_email_cc"];
		}
		if(is_email_address($row["user_email_deputy"]))
		{
			$emails[$row["user_email_deputy"]] = $row["user_email_deputy"];
		}
	}

	return $emails;
}


/********************************************************************
    add fbi_info to the list of fields
*********************************************************************/
function add_fbi_info(&$fields, &$values)
{
     $fields[] = "date_created";
     $values[] = "now()";

     $fields[] = "date_modified";
     $values[] = "now()";

     $fields[] = "user_created";
     $values[] = dbquote(user_login());

     $fields[] = "user_modified";
     $values[] = dbquote(user_login());
}

/********************************************************************
    save an address record
*********************************************************************/
function save_address($form, $address_type = BILLING_ADDRESS, $order_id = "NULL",
                            $order_item_id = "NULL", $address_parent = "NULL", 
                            $prefix = "billing_address")
{
    
	
	//insert new place into places

	
	if(array_key_exists('delivery_address_place_id', $form->items))
	{
		if($form->items['delivery_address_place_id']['value'] == 999999999) {
			$country_id = $form->items['delivery_address_country']['value'];
			$province_id = $form->items['delivery_address_province_id']['value'];
			$place_name = $form->items['delivery_address_place']['value'];

			$sql = "insert into places (place_country, place_province, place_name, date_created, user_created) Values (" . 
				   $country_id .  "," . $province_id . "," . dbquote($place_name) . "," . dbquote(date('Y-m-d')) . "," . dbquote(user_login()) . ")";

			$result = mysql_query($sql) or dberror($sql);

			$form->items['delivery_address_place_id']['value'] = mysql_insert_id();

		}
	};



	$sql = "show columns ".
           "from order_addresses " . 
           "like 'order_address_%'";

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        if (empty($row['Key']))
        {
            if($row['Field'] != "order_address_inactive" 
				and $row['Field'] != "order_address_place_new" 
				and $row['Field'] != "order_address_place_tmp"
			    and $row['Field'] != "order_address_fax"
				and $row['Field'] != "order_address_fax_country"
				and $row['Field'] != "order_address_fax_area"
				and $row['Field'] != "order_address_fax_number")
			{
				$table_fields[] = $row['Field'];
			}
        }
    }

    $address_fields[] = "order_address_order";
    $address_values[] = dbquote($order_id);

    $address_fields[] = "order_address_order_item";
    $address_values[] = dbquote($order_item_id);
    
    $address_fields[] = "order_address_type";
    $address_values[] = dbquote($address_type);

    $address_fields[] = "order_address_parent";
    $address_values[] = dbquote($address_parent);

    foreach ($table_fields as $field)
    {
        if (($field != "order_address_id")
           && ($field != "order_address_order")
           && ($field != "order_address_order_item")
           && ($field != "order_address_type")
           && ($field != "order_address_parent"))
        {

            if (($prefix == "shop_address") 
                && (($field == "order_address_phone")
                    || ($field == "order_address_fax")
                    || ($field == "order_address_email")
                    || ($field == "order_address_contact")
				    || ($field == "order_address_fax")))
            {
                continue;
            }

            $address_fields[] = $field;
			
			if ((($prefix == "delivery_address") 
                || ($prefix == "shop_address"))
                 )
            {
                
				if(!array_key_exists(str_replace("order_address", "delivery_address", $field), $form->items))
				{
					foreach($form->items as $fieldname=>$field_array)
					{
						if($field_array['type'] == 'multi_edit')
						{
							foreach($field_array['fields'] as $key=>$db_field_name)
							{
								if($field == $db_field_name)
								{
									$address_values[] = dbquote(trim($field_array['values'][$key]));
								}
							}
						}
					}
				}
				else
				{
					$address_values[] = trim($form->value(str_replace("order_address", "delivery_address", $field))) == "" ? "null" : dbquote($form->value(str_replace("order_address", "delivery_address", $field)));
				}
            } else {
                
				if(!array_key_exists(str_replace("order_address", $prefix, $field), $form->items))
				{
					foreach($form->items as $fieldname=>$field_array)
					{
						if($field_array['type'] == 'multi_edit')
						{
							foreach($field_array['fields'] as $key=>$db_field_name)
							{
								if($field == $db_field_name)
								{
									$address_values[] = dbquote(trim($field_array['values'][$key]));
								}
							}
						}
					}
				}
				else
				{
					$address_values[] = trim($form->value(str_replace("order_address", $prefix, $field))) == "" ? "null" : dbquote($form->value(str_replace("order_address", $prefix, $field)));
				}
            }
        }
    }

    add_fbi_info($address_fields, $address_values);

    $sql = "insert into order_addresses (" . join(", ", $address_fields) . ") values (" . join(", ", $address_values) . ")";

    mysql_query($sql) or dberror($sql);

    return mysql_insert_id();
}


/********************************************************************
    create a new project number
*********************************************************************/
function project_create_project_number_check_number($project_number)
{
	$sql = "select count(project_id) as num_recs " . 
		   "from projects " . 
		   "where project_number = " . dbquote($project_number);

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);

    if($row["num_recs"] > 0)
	{
		return true;
	}
	else
	{
		return false;
	}

}

function project_create_project_number($client_id, $pos_type, $project_kind)
{
    // build prefix (first and last figure of year)

    $res = getdate(time());
    $yyyy = $res["year"];
	$project_number = substr($yyyy, 0, 1) . substr($yyyy, 2, 2) . ".";
	
    // get country code
    $sql = "select countries.country_code ".
           "from addresses LEFT JOIN countries ON address_country = countries.country_id ".
           "where address_id = " . $client_id;

    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
    $project_number = $project_number . $row["country_code"] . ".";

    // get project type's starting number
	if($project_kind == 4 
		or $project_kind == 5
		or $project_kind == 8) // Take Over and Lease Renewalprojects or PopUp get separate number
	{
		// get latest project number
		if($project_kind == 4) {
			$project_number .= 'T';
		}
		elseif($project_kind == 5) {
			$project_number .= 'L';
		}
		else
		{
			$project_number .= 'P';
		}
		
		$sql = "select project_number ".
			   "from projects ".
			   "where project_number Like '" . $project_number . "%' ".
			   "order by project_number desc ".
			   "limit 0,1";

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$last = substr($row["project_number"],8,2);
			$next = intval($last) + 1;

			if ($next < 10)
			{
				$next = "0" . $next;
			}
			$project_number=$project_number . $next;
		}
		else
		{
			$project_number= $project_number ."01";
		}

		//check if project number is occupied by another product line
		$occupied = project_create_project_number_check_number($project_number);

		while($occupied == true)
		{
			$last = substr($project_number,9,2);
			$next = intval($last) + 1;
			if ($next < 10)
			{
				$next = "0" . $next;
			}
			$project_number = substr($project_number,0,9) . $next;
			$occupied = project_create_project_number_check_number($project_number);
		}
		
	}
	else
	{
		$sql = "select * ".
			   "from product_line_pos_types ".
			   "where product_line_pos_type_pos_type = " . $pos_type . 
			   " and product_line_pos_type_product_line is NULL";

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$first = $row["product_line_pos_type_starting_number"];
		}

		if ($first < 10)
		{
			$first = "00" . $first;
		}
		elseif ($first < 100)
		{
			$first = "0" . $first;
		}

		// get latest project number of non take over project
		
		$sql = "select project_number ".
			   "from projects ".
			   "where project_number Like '" . $project_number . "%' ".
			   "    and project_postype = " . $pos_type . " " .
			   "    and project_projectkind <> 3 " .
		       "    and project_projectkind <> 4 " . 
			   "    and project_projectkind <> 5 " .
			   "    and project_projectkind <> 8 " .
			   "order by project_number desc ".
			   "limit 0,1";

		$res = mysql_query($sql) or dberror($sql);

		//if ($pos_type <> 1 && $row = mysql_fetch_assoc($res))
		if ($row = mysql_fetch_assoc($res))
		{
			$last = substr($row["project_number"],8,3);
			$next = intval($last) + 1;

			if ($next < 10)
			{
				$next = "00" . $next;
			}
			elseif ($next < 100)
			{
				$next = "0" . $next;
			}

			$project_number=$project_number . $next;
		}
		else
		{
			$project_number=$project_number . $first;
		}

		//check if project number is occupied by another product line
		$occupied = project_create_project_number_check_number($project_number);

		while($occupied == true)
		{
			$last = substr($project_number,8,3);
			$next = intval($last) + 1;
			if ($next < 10)
			{
				$next = "0" . $next;
			}
			elseif ($next < 100)
			{
				$next = "0" . $next;
			}
			$project_number = substr($project_number,0,8) . $next;
			$occupied = project_create_project_number_check_number($project_number);
		}
	}


    return $project_number;
}



/********************************************************************
   update project client data
*********************************************************************/
function  project_update_client_data($form,$design_objectives_listbox_names,$design_objectives_checklist_names)
{

    $order_fields = array();

    $delivery_address_fields = array();

    $project_fields = array();

    $project_item_fields = array();
    $project_item_values = array();


	//update table posorders or posorderspipeline
	$sql = "select order_number, project_projectkind from orders " . 
		   "left join projects on project_order = order_id " . 
		   "where order_id = " . $form->value("oid");

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$old_project_number = $row["order_number"];
		$old_projectkind = $row["project_projectkind"];

		$year_of_order = substr($form->value("project_number"),0,2);
		if($year_of_order > 30 and $year_of_order <= 99)
		{
			$year_of_order = "19" . $year_of_order;
		}
		else
		{
			$year_of_order = substr($year_of_order, 0, 1) . '00' . substr($year_of_order, 1, 1);
		}

		if(substr($form->value("project_number"),3,1) == '.')
		{
			$year_of_order = substr($form->value("project_number"),0,1) . '0' . substr($form->value("project_number"),1,2);
		}

		$sql = "update posorders set " .
			   "posorder_year = " . dbquote($year_of_order) . ", " . 
			   "posorder_product_line = " . dbquote($form->value("product_line")) . ", " . 
			   "posorder_postype = " . dbquote($form->value("project_postype")) . ", " . 
			   "posorder_subclass = " . dbquote($form->value("project_pos_subclass")) . ", " . 
			   "posorder_project_kind = " . dbquote($form->value("project_projectkind")) . ", " . 
			   "posorder_legal_type = " . dbquote($form->value("project_cost_type")) . ", " .
			   "posorder_ordernumber = " . dbquote($form->value("project_number")) .
			   " where posorder_ordernumber = " . dbquote($old_project_number);


		$result = mysql_query($sql) or dberror($sql);

		$sql = "update posorderspipeline set " .
			   "posorder_year = " . dbquote($year_of_order) . ", " . 
			   "posorder_product_line = " . dbquote($form->value("product_line")) . ", " . 
			   "posorder_postype = " . dbquote($form->value("project_postype")) . ", " . 
			   "posorder_subclass = " . dbquote($form->value("project_pos_subclass")) . ", " . 
			   "posorder_project_kind = " . dbquote($form->value("project_projectkind")) . ", " . 
			   "posorder_legal_type = " . dbquote($form->value("project_cost_type")) . ", " .
			   "posorder_ordernumber = " . dbquote($form->value("project_number")) . 
			   " where posorder_ordernumber = " . dbquote($old_project_number);

		$result = mysql_query($sql) or dberror($sql);
	}

    // update record in table orders
    $value = trim($form->value("project_number")) == "" ? "null" : dbquote($form->value("project_number"));
    $order_fields[] = "order_number = " . $value;

    $value = dbquote($form->value("status"));
    $order_fields[] = "order_actual_order_state_code = " . $value;

	$value = dbquote($form->value("dstatus"));
    $order_fields[] = "order_development_status = " . $value;

	$value = dbquote($form->value("lstatus"));
    $order_fields[] = "order_logistic_status = " . $value;

    $value = $form->value("client_address_id");
    $order_fields[] = "order_client_address = " . $value;

    $value = $form->value("client_address_user_id");
    $order_fields[] = "order_user = " . $value;

    $value = trim($form->value("preferred_transportation_arranged")) == "" ? "null" : dbquote($form->value("preferred_transportation_arranged"));
    $order_fields[] = "order_preferred_transportation_arranged = " . $value;

	$value = trim($form->value("preferred_transportation_mode")) == "" ? "null" : dbquote($form->value("preferred_transportation_mode"));
    $order_fields[] = "order_preferred_transportation_mode = " . $value;

    $value = trim($form->value("voltage")) == "" ? "null" : dbquote($form->value("voltage"));
    $order_fields[] = "order_voltage = " . $value;

    //$value = dbquote(from_system_date($form->value("preferred_delivery_date")), true);
    //$order_fields[] = "order_preferred_delivery_date = " . $value;


    $value = trim($form->value("full_delivery")) == "" ? "null" : dbquote($form->value("full_delivery"));
    $order_fields[] = "order_full_delivery = " . $value;

    //$value = trim($form->value("packaging_retraction")) == "" ? "null" : dbquote($form->value("packaging_retraction"));
    //$order_fields[] = "order_packaging_retraction = " . $value;

    $value = trim($form->value("pedestrian_mall_approval")) == "" ? "null" : dbquote($form->value("pedestrian_mall_approval"));
    $order_fields[] = "order_pedestrian_mall_approval = " . $value;

    $value = trim($form->value("delivery_comments")) == "" ? "null" : dbquote($form->value("delivery_comments"));
    $order_fields[] = "order_delivery_comments = " . $value;

	$value = dbquote($form->value("order_insurance"));
    $order_fields[] = "order_insurance = " . $value;

    
	// franchisee address
    /*
	$value = trim($form->value("franchisee_address_company")) == "" ? "null" : dbquote($form->value("franchisee_address_company"));
    $order_fields[] = "order_franchisee_address_company = " . $value;

     $value = trim($form->value("franchisee_address_company2")) == "" ? "null" : dbquote($form->value("franchisee_address_company2"));
    $order_fields[] = "order_franchisee_address_company2 = " . $value;

    $value = trim($form->value("franchisee_address_address")) == "" ? "null" : dbquote($form->value("franchisee_address_address"));
    $order_fields[] = "order_franchisee_address_address = " . $value;

    $value = trim($form->value("franchisee_address_address2")) == "" ? "null" : dbquote($form->value("franchisee_address_address2"));
    $order_fields[] = "order_franchisee_address_address2 = " . $value;

    $value = trim($form->value("franchisee_address_zip")) == "" ? "null" : dbquote($form->value("franchisee_address_zip"));
    $order_fields[] = "order_franchisee_address_zip = " . $value;

     $value = trim($form->value("franchisee_address_place")) == "" ? "null" : dbquote($form->value("franchisee_address_place"));
    $order_fields[] = "order_franchisee_address_place = " . $value;

    $value = trim($form->value("franchisee_address_country")) == "" ? "null" : dbquote($form->value("franchisee_address_country"));
    $order_fields[] = "order_franchisee_address_country = " . $value;

    $value = trim($form->value("franchisee_address_phone")) == "" ? "null" : dbquote($form->value("franchisee_address_phone"));
    $order_fields[] = "order_franchisee_address_phone = " . $value;

    $value = trim($form->value("franchisee_address_mobile_phone")) == "" ? "null" : dbquote($form->value("franchisee_address_mobile_phone"));
    $order_fields[] = "order_franchisee_address_mobile_phone = " . $value;

    $value = trim($form->value("franchisee_address_email")) == "" ? "null" : dbquote($form->value("franchisee_address_email"));
    $order_fields[] = "order_franchisee_address_email = " . $value;

    $value = trim($form->value("franchisee_address_contact")) == "" ? "null" : dbquote($form->value("franchisee_address_contact"));
    $order_fields[] = "order_franchisee_address_contact = " . $value;

	*/
	
	// billing address

	
	$value = trim($form->value("order_direct_invoice_address_id")) == "" ? "null" : dbquote($form->value("order_direct_invoice_address_id"));
    $order_fields[] = "order_direct_invoice_address_id = " . $value;



    $value = trim($form->value("billing_address_company")) == "" ? "null" : dbquote($form->value("billing_address_company"));
    $order_fields[] = "order_billing_address_company = " . $value;

     $value = trim($form->value("billing_address_company2")) == "" ? "null" : dbquote($form->value("billing_address_company2"));
    $order_fields[] = "order_billing_address_company2 = " . $value;

    $value = trim($form->value("billing_address_address")) == "" ? "null" : dbquote($form->value("billing_address_address"));
    $order_fields[] = "order_billing_address_address = " . $value;

	if(array_key_exists('bill_to_street', $form->items))
	{
		foreach($form->items['bill_to_street']['fields'] as $key=>$fieldname)
		{
			$value = dbquote($form->items['bill_to_street']['values'][$key]);
			$order_fields[] = $fieldname . " = " . $value;

		}
	}
	elseif(array_key_exists('billing_address_street', $form->items))
	{
		$value = trim($form->value("billing_address_street")) == "" ? "null" : dbquote($form->value("billing_address_street"));
	    $order_fields[] = "order_billing_address_street = " . $value;

		$value = trim($form->value("billing_address_streetnumber")) == "" ? "null" : dbquote($form->value("billing_address_streetnumber"));
	    $order_fields[] = "order_billing_address_streetnumber = " . $value;
	}

    $value = trim($form->value("billing_address_address2")) == "" ? "null" : dbquote($form->value("billing_address_address2"));
    $order_fields[] = "order_billing_address_address2 = " . $value;

    $value = trim($form->value("billing_address_zip")) == "" ? "null" : dbquote($form->value("billing_address_zip"));
    $order_fields[] = "order_billing_address_zip = " . $value;

    $value = dbquote($form->value("billing_address_place_id"));
    $order_fields[] = "order_billing_address_place_id = " . $value;

	$value = trim($form->value("billing_address_place")) == "" ? "null" : dbquote($form->value("billing_address_place"));
    $order_fields[] = "order_billing_address_place = " . $value;

    $value = trim($form->value("billing_address_country")) == "" ? "null" : dbquote($form->value("billing_address_country"));
    $order_fields[] = "order_billing_address_country = " . $value;

    $value = trim($form->value("billing_address_phone")) == "" ? "null" : dbquote($form->value("billing_address_phone"));
    $order_fields[] = "order_billing_address_phone = " . $value;

	if(array_key_exists('bill_to_phone_number', $form->items))
	{
		foreach($form->items['bill_to_phone_number']['fields'] as $key=>$fieldname)
		{
			$value = dbquote($form->items['bill_to_phone_number']['values'][$key]);
			$order_fields[] = $fieldname . " = " . $value;

		}
	}
	elseif(array_key_exists('billing_address_phone_country', $form->items))
	{
		$value = trim($form->value("billing_address_phone_country")) == "" ? "null" : dbquote($form->value("billing_address_phone_country"));
	    $order_fields[] = "order_billing_address_phone_country = " . $value;

		$value = trim($form->value("billing_address_phone_area")) == "" ? "null" : dbquote($form->value("billing_address_phone_area"));
	    $order_fields[] = "order_billing_address_phone_area = " . $value;

		$value = trim($form->value("billing_address_phone_number")) == "" ? "null" : dbquote($form->value("billing_address_phone_number"));
	    $order_fields[] = "order_billing_address_phone_number = " . $value;
	}

    $value = trim($form->value("billing_address_mobile_phone")) == "" ? "null" : dbquote($form->value("billing_address_mobile_phone"));
    $order_fields[] = "order_billing_address_mobile_phone = " . $value;

	if(array_key_exists('bill_to_mobile_phone_number', $form->items))
	{
		foreach($form->items['bill_to_mobile_phone_number']['fields'] as $key=>$fieldname)
		{
			$value = dbquote($form->items['bill_to_mobile_phone_number']['values'][$key]);
			$order_fields[] = $fieldname . " = " . $value;

		}
	}
	elseif(array_key_exists('billing_address_mobile_phone_country', $form->items))
	{
		$value = trim($form->value("billing_address_mobile_phone_country")) == "" ? "null" : dbquote($form->value("billing_address_mobile_phone_country"));
	    $order_fields[] = "order_billing_address_mobile_phone_country = " . $value;

		$value = trim($form->value("billing_address_mobile_phone_area")) == "" ? "null" : dbquote($form->value("billing_address_mobile_phone_area"));
	    $order_fields[] = "order_billing_address_mobile_phone_area = " . $value;

		$value = trim($form->value("billing_address_mobile_phone_number")) == "" ? "null" : dbquote($form->value("billing_address_mobile_phone_number"));
	    $order_fields[] = "order_billing_address_mobile_phone_number = " . $value;
	}

    $value = trim($form->value("billing_address_email")) == "" ? "null" : dbquote($form->value("billing_address_email"));
    $order_fields[] = "order_billing_address_email = " . $value;

    $value = trim($form->value("billing_address_contact")) == "" ? "null" : dbquote($form->value("billing_address_contact"));
    $order_fields[] = "order_billing_address_contact = " . $value;


    $value = "current_timestamp";
    $order_fields[] = "date_modified = " . $value;

    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $order_fields[] = "user_modified = " . $value;
    }

    $sql = "update orders set " . join(", ", $order_fields) . " where order_id = " . $form->value("oid");

    mysql_query($sql) or dberror($sql);

    
   
    // update delivery address in table order_addresses
    /*
	$value = trim($form->value("delivery_address_company")) == "" ? "null" : dbquote($form->value("delivery_address_company"));
    $delivery_address_fields[] = "order_address_company = " . $value;

     $value = trim($form->value("delivery_address_company2")) == "" ? "null" : dbquote($form->value("delivery_address_company2"));
    $delivery_address_fields[] = "order_address_company2 = " . $value;

    $value = trim($form->value("delivery_address_address")) == "" ? "null" : dbquote($form->value("delivery_address_address"));
    $delivery_address_fields[] = "order_address_address = " . $value;

    $value = trim($form->value("delivery_address_address2")) == "" ? "null" : dbquote($form->value("delivery_address_address2"));
    $delivery_address_fields[] = "order_address_address2 = " . $value;

    $value = trim($form->value("delivery_address_zip")) == "" ? "null" : dbquote($form->value("delivery_address_zip"));
    $delivery_address_fields[] = "order_address_zip = " . $value;

    $value = trim($form->value("delivery_address_place")) == "" ? "null" : dbquote($form->value("delivery_address_place"));
    $delivery_address_fields[] = "order_address_place = " . $value;

	$value = dbquote($form->value("delivery_address_place_id"));
    $delivery_address_fields[] = "order_address_place_id = " . $value;

    $value = trim($form->value("delivery_address_country")) == "" ? "null" : dbquote($form->value("delivery_address_country"));
    $delivery_address_fields[] = "order_address_country = " . $value;

    $value = trim($form->value("delivery_address_phone")) == "" ? "null" : dbquote($form->value("delivery_address_phone"));
    $delivery_address_fields[] = "order_address_phone = " . $value;

    $value = trim($form->value("delivery_address_mobile_phone")) == "" ? "null" : dbquote($form->value("delivery_address_mobile_phone"));
    $delivery_address_fields[] = "order_address_mobile_phone = " . $value;

    $value = trim($form->value("delivery_address_email")) == "" ? "null" : dbquote($form->value("delivery_address_email"));
    $delivery_address_fields[] = "order_address_email = " . $value;

    $value = $form->value("client_address_id");
    $delivery_address_fields[] = "order_address_parent = " . $value;

    $value = trim($form->value("delivery_address_contact")) == "" ? "null" : dbquote($form->value("delivery_address_contact"));
    $delivery_address_fields[] = "order_address_contact = " . $value;

    $value = "current_timestamp";
    $delivery_address_fields[] = "date_modified = " . $value;

    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $delivery_address_fields[] = "user_modified = " . $value;
    }

    $sql = "update order_addresses set " . join(", ", $delivery_address_fields) . " where order_address_type = 2 and order_address_order = " . $form->value("oid");
    mysql_query($sql) or dberror($sql);
	*/

    
    // update record in table projects
    $value = dbquote($form->value("project_number"));
    $project_fields[] = "project_number = " . $value;

    $value = trim($form->value("project_postype")) == "" ? "0" : dbquote($form->value("project_postype"));
    $project_fields[] = "project_postype = " . $value;


	$value = trim($form->value("project_pos_subclass")) == "" ? "0" : dbquote($form->value("project_pos_subclass"));
    $project_fields[] = "project_pos_subclass = " . $value;


    $value = trim($form->value("project_state")) == "" ? "1" : dbquote($form->value("project_state"));
    $project_fields[] = "project_state = " . $value;

    $value = trim($form->value("project_projectkind")) == "" ? "null" :
    dbquote($form->value("project_projectkind"));
    $project_fields[] = "project_projectkind = " . $value;

    $value = trim($form->value("product_line")) == "" ? "null" : dbquote($form->value("product_line"));
    $project_fields[] = "project_product_line = " . $value;

    $value = dbquote($form->value("location_type"));
    $project_fields[] = "project_location_type = " . $value;

    $value = trim($form->value("location_type_other")) == "" ? "null" : dbquote($form->value("location_type_other"));
    $project_fields[] = "project_location = " . $value;

    $value = trim($form->value("approximate_budget")) == "" ? "null" : dbquote($form->value("approximate_budget"));
    $project_fields[] = "project_approximate_budget = " . $value;

    $value = dbquote(from_system_date($form->value("planned_opening_date")), true);
    $project_fields[] = "project_planned_opening_date = " . $value;


	$value = dbquote(from_system_date($form->value("planned_closing_date")), true);
    $project_fields[] = "project_planned_closing_date = " . $value;

	$value = dbquote(from_system_date($form->value("planned_takeover_date")), true);
    $project_fields[] = "project_planned_takeover_date = " . $value;
	
	
    $value = trim($form->value("watches_displayed")) == "" ? "null" : dbquote($form->value("watches_displayed"));
    $project_fields[] = "project_watches_displayed = " . $value;

    $value = trim($form->value("watches_stored")) == "" ? "null" : dbquote($form->value("watches_stored"));
    $project_fields[] = "project_watches_stored = " . $value;
	
	/*
	$value = trim($form->value("bijoux_displayed")) == "" ? "null" : dbquote($form->value("bijoux_displayed"));
    $project_fields[] = "project_bijoux_displayed = " . $value;

    $value = trim($form->value("bijoux_stored")) == "" ? "null" : dbquote($form->value("bijoux_stored"));
    $project_fields[] = "project_bijoux_stored = " . $value;
	*/


	$value = $form->value("tissot_shop_around") - 1;
    $project_fields[] = "project_tissot_shop_around = " . $value;

    $value = trim($form->value("comments")) == "" ? "null" : dbquote($form->value("comments"));
    $project_fields[] = "project_comments = " . $value;

	$value = 0;
	foreach($design_objectives_listbox_names as $key=>$field)
	{
		if($form->value($field) == 151 or $form->value($field) == 157 or $form->value($field) == 159)
		{
			$value = 1;
		}
	
	}
	
	$value = trim($form->value("project_relocated_posaddress_id")) == "" ? "0" : dbquote($form->value("project_relocated_posaddress_id"));
    $project_fields[] = "project_relocated_posaddress_id = " . $value;



    $value = "current_timestamp";
    $project_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $project_fields[] = "user_modified = " . $value;
    }

    $sql = "update projects set " . join(", ", $project_fields) . " where project_id = " . $form->value("pid");
    mysql_query($sql) or dberror($sql);


    $_REQUEST["id"] = $form->value("pid");

    // delete all records form project_items belonign to the project
    $sql = "delete from project_items where project_item_project = " . $form->value("pid");
    mysql_query($sql) or dberror($sql);

    // insert records into table project_items
    foreach ($design_objectives_listbox_names as $element)
    {
        $project_item_fields[0] = "project_item_project";
        $project_item_values[0] = $form->value("pid");

        $project_item_fields[1] = "project_item_item";
        $project_item_values[1] = $form->value($element);
        
        $project_item_fields[2] = "date_created";
        $project_item_values[2] = "current_timestamp";

        $project_item_fields[3] = "date_modified";
        $project_item_values[3] = "current_timestamp";
                
        if (isset($_SESSION["user_login"]))
        {
            $project_item_fields[4] = "user_created";
            $project_item_values[4] = dbquote($_SESSION["user_login"]);

            $project_item_fields[5] = "user_modified";
            $project_item_values[5] = dbquote($_SESSION["user_login"]);
        }
        $sql = "insert into project_items (" . join(", ", $project_item_fields) . ") values (" . join(", ", $project_item_values) . ")";
        mysql_query($sql) or dberror($sql);
    }
    

    foreach ($design_objectives_checklist_names as $name)
    {
        $tmp = $form->value($name);
        foreach ($tmp as $value)
        {
        $project_item_fields[0] = "project_item_project";
        $project_item_values[0] =$form->value("pid");

        $project_item_fields[1] = "project_item_item";
        $project_item_values[1] = $value;
        
        $project_item_fields[2] = "date_created";
        $project_item_values[2] = "current_timestamp";

        $project_item_fields[3] = "date_modified";
        $project_item_values[3] = "current_timestamp";
                
        if (isset($_SESSION["user_login"]))
        {
            $project_item_fields[4] = "user_created";
            $project_item_values[4] = dbquote($_SESSION["user_login"]);

            $project_item_fields[5] = "user_modified";
            $project_item_values[5] = dbquote($_SESSION["user_login"]);
        }
        $sql = "insert into project_items (" . join(", ", $project_item_fields) . ") values (" . join(", ", $project_item_values) . ")";
        mysql_query($sql) or dberror($sql);
        }
    }


    // update record in table project_costs
	$value = dbquote($form->value("store_retailarea"));
    $project_cost_fields[] = "project_cost_sqms = " . $value;

	$value = dbquote($form->value("requested_store_retailarea"));
    $project_cost_fields[] = "project_cost_original_sqms = " . $value;
	
	$value = dbquote($form->value("store_totalsurface"));
    $project_cost_fields[] = "project_cost_totalsqms = " . $value;

	$value = dbquote($form->value("store_backoffice"));
    $project_cost_fields[] = "project_cost_backofficesqms = " . $value;

	/*
	$value = dbquote($form->value("store_floorsurface1"));
    $project_cost_fields[] = "project_cost_floorsurface1 = " . $value;

	$value = dbquote($form->value("store_floorsurface2"));
    $project_cost_fields[] = "project_cost_floorsurface2 = " . $value;

	$value = dbquote($form->value("store_floorsurface3"));
    $project_cost_fields[] = "project_cost_floorsurface3 = " . $value;

	$value = dbquote($form->value("store_numfloors"));
    $project_cost_fields[] = "project_cost_numfloors = " . $value;
	*/
	
	$value = dbquote($form->value("project_cost_type"));
    $project_cost_fields[] = "project_cost_type = " . $value;

    $value = "current_timestamp";
    $project_cost_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $project_cost_fields[] = "user_modified = " . $value;
    }

    $sql = "update project_costs set " . join(", ", $project_cost_fields) . " where project_cost_order = " . $form->value("oid");
    mysql_query($sql) or dberror($sql);


	return true;

}


/********************************************************************
   update project retail data
*********************************************************************/
function  project_update_retail_data($form)
{

    $project_fields = array();
    $order_fields = array();

    // update record in table projects


	$value = dbquote($form->value("project_production_type"));
    $project_fields[] = "project_production_type = " . $value;

	$value = dbquote($form->value("product_line"));
    $project_fields[] = "project_product_line = " . $value;

	$value = dbquote($form->value("project_type_subclass_id"));
    $project_fields[] = "project_type_subclass_id = " . $value;

	$value = dbquote($form->value("product_line_subclass"));
    $project_fields[] = "project_product_line_subclass = " . $value;


    $value = dbquote($form->value("retail_coordinator"));
    $project_fields[] = "project_retail_coordinator = " . $value;

	//$value = dbquote($form->value("hq_project_manager"));
    //$project_fields[] = "project_hq_project_manager = " . $value;

	$value = dbquote($form->value("local_retail_coordinator"));
    $project_fields[] = "project_local_retail_coordinator = " . $value;
    
    
	if($form->value("project_no_planning") == 1) {
		$project_fields[] = "project_design_contractor = 0";
	}
	else
	{
		$value = dbquote($form->value("contractor_user_id"));
		$project_fields[] = "project_design_contractor = " . $value;
	}
    
    
	$value = dbquote($form->value("supervisor_user_id"));
	$project_fields[] = "project_design_supervisor = " . $value;

	$value = dbquote($form->value("cms_approver_user_id"));
    $project_fields[] = "project_cms_approver = " . $value;

    
	if($form->value("project_state") == 2) // on hold
	{
		$project_fields[] = "project_real_opening_date = NULL";
	}
	else
	{
		$value = dbquote(from_system_date($form->value("project_real_opening_date")));
		$project_fields[] = "project_real_opening_date = " . $value;
	}

	$value = dbquote($form->value("project_state"));
    $project_fields[] = "project_state = " . $value;
	
	$value = dbquote($form->value("project_is_local_production"));
    $project_fields[] = "project_is_local_production = " . $value;

	$value = dbquote($form->value("project_type_subclass_id"));
    $project_fields[] = "project_type_subclass_id = " . $value;

	$value = dbquote($form->value("project_furniture_type_sis"));
    $project_fields[] = "project_furniture_type_sis = " . $value;

	$value = dbquote($form->value("project_furniture_type_store"));
    $project_fields[] = "project_furniture_type_store = " . $value;

	$value = dbquote($form->value("project_uses_icedunes_visuals"));
    $project_fields[] = "project_uses_icedunes_visuals = " . $value;

	$value = dbquote($form->value("project_relocated_posaddress_id"));
    $project_fields[] = "project_relocated_posaddress_id = " . $value;

	$value = dbquote($form->value("project_budget_covered_by"));
    $project_fields[] = "project_budget_covered_by = " . $value;


    $value = dbquote($form->value("project_use_ps2004"));
    $project_fields[] = "project_use_ps2004 = " . $value;

    $value = "current_timestamp";
    $project_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $project_fields[] = "user_modified = " . $value;
    }

    $sql = "update projects set " . join(", ", $project_fields) . " where project_id = " . $form->value("pid");
    mysql_query($sql) or dberror($sql);

    $_REQUEST["id"] = $form->value("pid");


    // update record in table orders

	$value = dbquote($form->value("status"));
    $order_fields[] = "order_actual_order_state_code = " . $value;
    
    $value = dbquote($form->value("retail_operator"));
    $order_fields[] = "order_retail_operator = " . $value;

    $value = dbquote($form->value("delivery_confirmation_by"));
    $order_fields[] = "order_delivery_confirmation_by = " . $value;

    $value = "current_timestamp";
    $order_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $order_fields[] = "user_modified = " . $value;
    }

    $sql = "update orders set " . join(", ", $order_fields) . " where order_id = " . $form->value("oid");
    mysql_query($sql) or dberror($sql);


	//update posorders
	$sql = "update posorders set " . 
		   "posorder_product_line = " . dbquote($form->value("product_line")) . ", " .
		   "posorder_product_line_subclass = " . dbquote($form->value("product_line_subclass")) . ", " .
	       "posorder_project_locally_produced = " . dbquote($form->value("project_is_local_production")) . ", " . 
		   "posorder_project_type_subclass_id = " . dbquote($form->value("project_type_subclass_id")) . ", " . 
		   "posorder_furniture_type_store = " . dbquote($form->value("project_furniture_type_store")) . ", " . 
		   "posorder_furniture_type_sis = " . dbquote($form->value("project_furniture_type_sis")) . ", " .
		   "posorder_uses_icedunes_visuals = " . dbquote($form->value("project_uses_icedunes_visuals")) . 
		   " where posorder_order = " . dbquote($form->value("oid"));
	$result = mysql_query($sql) or dberror($sql);
	
	
	$sql = "update posorderspipeline set " . 
		   "posorder_product_line = " . dbquote($form->value("product_line")) . ", " .
		   "posorder_product_line_subclass = " . dbquote($form->value("product_line_subclass")) . ", " .
		   "posorder_project_locally_produced = " . dbquote($form->value("project_is_local_production")) . ", " . 
		   "posorder_project_type_subclass_id = " . dbquote($form->value("project_type_subclass_id")) . ", " . 
		   "posorder_furniture_type_store = " . dbquote($form->value("project_furniture_type_store")) . ", " . 
		   "posorder_furniture_type_sis = " . dbquote($form->value("project_furniture_type_sis")) . ", " .
		   "posorder_uses_icedunes_visuals = " . dbquote($form->value("project_uses_icedunes_visuals")) . 
		   " where posorder_order = " . dbquote($form->value("oid"));
	$result = mysql_query($sql) or dberror($sql);


	//update posaddress
	$sql = 'select project_real_opening_date from projects where project_id = ' . $form->value("pid");
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{

	
	   if($row["project_real_opening_date"] != NULL or $row["project_real_opening_date"] != '0000-00-00')
	   {
			$realistic_opening_date = $row["project_real_opening_date"];
		
			//get the posaddress
			$sql = "select posaddress_id, posaddress_fagrstart, posaddress_store_postype " . 
				   "from posorderspipeline " . 
				   "left join posaddressespipeline on posaddress_id = posorder_posaddress " .
				   " where posorder_order = " . dbquote($form->value("oid"));
			
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$pos_id = $row["posaddress_id"];
				if($row["posaddress_store_postype"] == 1 or $row["posaddress_store_postype"] == 3)
				{
					

					//if($row["posaddress_fagrstart"] == NULL or $row["posaddress_fagrstart"] == '0000-00-00')
					//{
						$start = to_system_date($realistic_opening_date);
						$start = "01." . substr($start, 3, strlen($start)-1);


						$months = 13 - substr($realistic_opening_date, 5,2);
						$duration = "3 years and " . $months . " months";


						$end = to_system_date($realistic_opening_date);
						if(substr($end, 6, strlen($end)-1) < 10) {
							$end = "31.12.0" . (substr($end, 6, strlen($end)-1) + 3);
						}
						else
						{
							$end = "31.12." . (substr($end, 6, strlen($end)-1) + 3);
						}

						
						$sql = "update posaddressespipeline set " . 
							   "posaddress_fagrstart = " . dbquote(from_system_date($start)) . ', ' .
							   "posaddress_fagrend = " . dbquote(from_system_date($end)) . ', ' .
							   "posaddress_fargrduration = " . dbquote($duration) . 
							   " where posaddress_id = " . $pos_id;

						$result = mysql_query($sql) or dberror($sql);

						
						$sql = "update projects set " . 
							   "project_fagrstart = " . dbquote(from_system_date($start)) . ', ' .
							   "project_fagrend = " . dbquote(from_system_date($end)) .  
							   " where project_id = " . param("pid");

						

						$result = mysql_query($sql) or dberror($sql);

					//}
				}
				else
				{
					$sql = "update posaddressespipeline set " . 
							   "posaddress_fagrstart = NULL, " . 
							   "posaddress_fagrend = NULL, " . 
							   "posaddress_fargrduration = 0 " . 
							   " where posaddress_id = " . $pos_id;

						$result = mysql_query($sql) or dberror($sql);

						
						$sql = "update projects set " . 
							   "project_fagrstart = NULL, " . 
							   "project_fagrend = NULL " . 
							   " where project_id = " . param("pid");

						

						$result = mysql_query($sql) or dberror($sql);
				}
			}
	   }
	}


}


/********************************************************************
   update project delivery confirmation
*********************************************************************/
function  update_delivery_confirmation($form, $type)
{

    $order_fields = array();

    // update record in table orders
    
    $value = dbquote($form->value("quality_ok"));
    $order_fields[] = "order_quality_ok = " . $value;

    $value = dbquote($form->value("quantity_ok"));
    $order_fields[] = "order_quantity_ok = " . $value;

    $value = dbquote($form->value("packing_list_ok"));
    $order_fields[] = "order_packing_list_ok = " . $value;

    $value = "current_timestamp";
    $order_fields[] = "order_confirmation_date = " . $value;

    $order_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $order_fields[] = "user_modified = " . $value;
    }

    $sql = "update orders set " . join(", ", $order_fields) . " where order_id = " . $form->value("oid");
    mysql_query($sql) or dberror($sql);

    append_order_state($form->value("oid"), "800", $type, 1);

}

/********************************************************************
   insert, update or delete project order items (material list)
*********************************************************************/
function  project_save_order_items($list, $type, $no_offer_required = 1)
{
	
	$list_of_materials_was_changed = false;
	$is_hq_supplied_item = false;
	foreach($list as $key=>$value)
    {
        if($key == "hidden" and isset($value["plid"]))
        {
            $pline = $value["plid"];
        }
        if($key == "hidden" and isset($value["cid"]))
        {
            $cid = $value["cid"];
            
        }
		
    }


	



    foreach ($list->values("item_entry_field") as $key=>$value)
    {
		if($key > 0)
		{
			
			
			$sql_i = "select item_category from items where item_id = " . $key;
			$res = mysql_query($sql_i) or dberror($sql_i);
			if ($row = mysql_fetch_assoc($res))
			{
				$cid = $row["item_category"];
			}
			
			// chek if item is new or already in order_items
			$sql_order_item = "select order_item_id, order_item_item, " . 
						  "order_item_type, order_item_text, order_item_quantity, ".
						  "order_item_ready_for_pickup, order_item_po_number, order_item_cost_group " .  
						  "from order_items ".
						  "where order_item_item = " . $key .
						  "    and order_item_order = " . param("oid");


			$res = mysql_query($sql_order_item) or dberror($sql_order_item);
			if ($row = mysql_fetch_assoc($res))
			{
				$order_item_id = $row["order_item_id"];
				$item_id = $row["order_item_item"];
				$type = $row["order_item_type"];
				$item_name = $row["order_item_text"];
				$old_quantity = $row["order_item_quantity"];

				//check if item is a hq supplied item
				if($row["order_item_cost_group"] == 2 
				   or $row["order_item_cost_group"] == 6)
				{
					$is_hq_supplied_item = true;
				}
				
				// update record in table order_item
				if ($value >0)
				{
					$order_item_fields = array();

					if ($type == ITEM_TYPE_COST_ESTIMATION)
					{
						$order_item_fields[] = "order_item_system_price = " . $value;
						$order_item_fields[] = "order_item_quantity = 1";
						$new_quantity = 1;
					}
					elseif ($type == ITEM_TYPE_LOCALCONSTRUCTIONCOST)
					{
						$order_item_fields[] = "order_item_system_price = " . $value;
						$order_item_fields[] = "order_item_quantity = 1";
						$new_quantity = 1;
					}
					else
					{
						
						$order_item_fields[] = "order_item_no_offer_required = " . dbquote($no_offer_required);
						$order_item_fields[] = "order_item_quantity = " . str_replace(",", ".", $value);
						$new_quantity = $value;
					}

					$value1 = "current_timestamp";
					$project_fields[] = "date_modified = " . $value1;
		
					if (isset($_SESSION["user_login"]))
					{
						$value1 = dbquote($_SESSION["user_login"]);
						$project_fields[] = "user_modified = " . $value1;
					}

					$order_item_fields[] = "order_item_category = " . $cid;

		   
					$sql = "update order_items set " . join(", ", $order_item_fields) . " " .
						   "where order_item_item = " . $key .
						   "    and order_item_order = " . param("oid");

					mysql_query($sql) or dberror($sql);


					//add tracking information to table order_item_trackings
					if($old_quantity != $new_quantity)
					{
						$result = track_change_in_order_items('Update item in list of materials', 'Quantity', param("oid"), $order_item_id, $item_id, $type, $item_name, $old_quantity, $new_quantity);
						$list_of_materials_was_changed = true;
					}
				}

				// delete record in table order_item
				if ($value == 0 and !$row["order_item_po_number"] and ($row["order_item_ready_for_pickup"] == NULL or $row["order_item_ready_for_pickup"] == '0000-00-00'))
				{
					$sql = "delete from order_items " .
						   "where order_item_category = " . $cid .
						   "   and order_item_item = " . $key .
						   "    and order_item_order = " . param("oid");
					$result = mysql_query($sql) or dberror($sql);

					
					//add tracking information to table order_item_trackings
					if($old_quantity > 0)
					{
						
						$result = track_change_in_order_items('Delete item in list of materials', 'Quantity', param("oid"), $order_item_id, $item_id, $type, $item_name, $old_quantity, 0);
						$list_of_materials_was_changed = true;
					}
				}
			}
			else
			{
				
				// insert new records into table order_items
				if ($value >0)
				{
					//get item_type
					$sql = "select item_type, item_category from items where item_id = " . $key;
					$res1 = mysql_query($sql) or dberror($sql);

					if ($row = mysql_fetch_assoc($res1))
					{
						$type = $row["item_type"];
						$cid = $row["item_category"];
					}

					if(!$type){$type =1;}

					
					$order_item_fields = array();
					$order_item_values = array();
		
					$order_item_fields[] = "order_item_order";
					$order_item_values[] = param("oid");

					$order_item_fields[] = "order_item_item";
					$order_item_values[] = $key;

					$order_item_fields[] = "order_item_type";
					$order_item_values[] = $type;


					if ($type == ITEM_TYPE_COST_ESTIMATION)
					{
						$order_item_fields[] = "order_item_quantity";
						$order_item_values[] = 1;
						$new_quantity = 1;
					}

					if ($type == ITEM_TYPE_LOCALCONSTRUCTIONCOST)
					{
						$order_item_fields[] = "order_item_quantity";
						$order_item_values[] = 1;
						$new_quantity = 1;
					}

					if ($type == ITEM_TYPE_STANDARD or $type == ITEM_TYPE_SPECIAL or $type == ITEM_TYPE_SERVICES)
					{
						
						$order_item_fields[] = "order_item_quantity";
						$order_item_values[] = str_replace(",", ".", $value);
						$new_quantity = $value;

						$order_item_fields[] = "order_item_category";
						$order_item_values[] = $cid;
					

						// get supplier's address id (first record in case of several)
						$sql = "select address_id, address_company, supplier_item_price, supplier_item_currency ".
							   "from items ".
							   "left join suppliers on item_id = supplier_item ".
							   "left join addresses on supplier_address = address_id ".
							   "where item_id = " . $key;
					
						$res1 = mysql_query($sql) or dberror($sql);

						if ($row1 = mysql_fetch_assoc($res1))
						{
							if ($row1["address_id"])
							{
								
								if ($row1["supplier_item_currency"])
								{
									$currency = get_currency($row1["supplier_item_currency"]);
								}
								else
								{
									$currency = get_address_currency($row1["address_id"]);
								}
								
								$order_item_fields[] = "order_item_supplier_address";
								$order_item_values[] = $row1["address_id"];

								// get suppliers's currency information
								$supplier_currency = get_address_currency($row1["address_id"]);
		
								$order_item_fields[] = "order_item_supplier_currency";
								$order_item_values[] = $currency["id"];
		
								$order_item_fields[] = "order_item_supplier_exchange_rate";
								$order_item_values[] = $currency["exchange_rate"];

								$order_item_fields[] = "order_item_supplier_price";
								$order_item_values[] = dbquote($row1["supplier_item_price"]);

							}
						}
					}

					// get orders's currency
					$currency = get_order_currency(param("oid"));

					// get item data
					$item_name = "";

					$sql = "select * ".
						   "from items ".
						   "where item_id = " . $key;

					$res1 = mysql_query($sql) or dberror($sql);

					if ($row = mysql_fetch_assoc($res1))
					{
						$item_name = $row["item_name"];
						
						$order_item_fields[] = "order_item_text";
						$order_item_values[] = trim($row["item_name"]) == "" ? "null" : dbquote($row["item_name"]);

						$order_item_fields[] = "order_item_system_price";
						if ($type == ITEM_TYPE_COST_ESTIMATION)
						{
							$order_item_values[] = $value;
							$client_price = $value / $currency["exchange_rate"] * $currency["factor"];
						}
						elseif ($type == ITEM_TYPE_LOCALCONSTRUCTIONCOST)
						{
							$order_item_values[] = $value;
							$client_price = $value / $currency["exchange_rate"] * $currency["factor"];
						}
						else
						{
							$order_item_values[] = trim($row["item_price"]) == "" ? "null" : dbquote($row["item_price"]);
							$client_price = $row["item_price"] / $currency["exchange_rate"] * $currency["factor"];
						}


						$order_item_fields[] = "order_item_client_price";
						$order_item_values[] = trim($client_price) == "" ? "null" : dbquote($client_price);

						if (!param("pid"))
						{
							$order_item_fields[] = "order_item_cost_unit_number";
							$order_item_values[] = trim($row["category_cost_unit_number"]) == "" ? "null" : dbquote($row["category_cost_unit_number"]);
						}

						/* cost monitoring group was removed by Katrin Eckert on January 26th 2016
						$order_item_fields[] = "order_items_costmonitoring_group";
						$order_item_values[] = dbquote($row["item_costmonitoring_group"]);
						*/


						//logistic fields
						$order_item_fields[] = "order_item_unit_id";
						$order_item_values[] = dbquote($row["item_unit"]);

						$order_item_fields[] = "order_item_width";
						$order_item_values[] = dbquote($row["item_width"]);

						$order_item_fields[] = "order_item_length";
						$order_item_values[] = dbquote($row["item_length"]);

						$order_item_fields[] = "order_item_height";
						$order_item_values[] = dbquote($row["item_height"]);

						$order_item_fields[] = "order_item_gross_weight";
						$order_item_values[] = dbquote($row["item_gross_weight"]);

						$order_item_fields[] = "order_item_packaging_type_id";
						$order_item_values[] = dbquote($row["item_packaging_type"]);

						$order_item_fields[] = "order_item_stackable";
						$order_item_values[] = dbquote($row["item_stackable"]);

					}

					$order_item_fields[] = "order_item_no_offer_required";
					$order_item_values[] = $no_offer_required;


					if ($type > 0)
					{
						$order_item_fields[] = "order_item_cost_group";
						$order_item_values[] = dbquote($row["item_cost_group"]);


						//check if item is a hq supplied item
						if($row["item_cost_group"] == 2 
						   or $row["item_cost_group"] == 6)
						{
							$is_hq_supplied_item = true;
						}
					}
					

					$order_item_fields[] = "date_created";
					$order_item_values[] = "current_timestamp";

					$order_item_fields[] = "date_modified";
					$order_item_values[] = "current_timestamp";
					
					if (isset($_SESSION["user_login"]))
					{
						$order_item_fields[] = "user_created";
						$order_item_values[] = dbquote($_SESSION["user_login"]);

						$order_item_fields[] = "user_modified";
						$order_item_values[] = dbquote($_SESSION["user_login"]);
					}

					$sql = "insert into order_items (" . join(", ", $order_item_fields) . ") values (" . join(", ", $order_item_values) . ")";

					mysql_query($sql) or dberror($sql);

					$item_id = mysql_insert_id();

					if ($type == ITEM_TYPE_STANDARD or $type == ITEM_TYPE_SPECIAL)
					{
						// copy delivery address for new item in table order_addresses
						$sql = "select * from order_addresses ".
							   "where order_address_type = 2 ".
							   "    and order_address_order = ". param("oid") .
							   "    and (order_address_order_item = 0 ".
							   "    or order_address_order_item is null) ";

						$res2 = mysql_query($sql) or dberror($sql);
						$row2 = mysql_fetch_assoc($res2);

						$delivery_address_fields = array();
						$delivery_address_values = array();

						$delivery_address_fields[] = "order_address_order";
						$delivery_address_values[] = param("oid");

						$delivery_address_fields[] = "order_address_order_item";
						$delivery_address_values[] = $item_id;

						$delivery_address_fields[] = "order_address_type";
						$delivery_address_values[] = 2;

						$delivery_address_fields[] = "order_address_company";
						$delivery_address_values[] = dbquote($row2["order_address_company"]);

						$delivery_address_fields[] = "order_address_company2";
						$delivery_address_values[] = dbquote($row2["order_address_company2"]);

						$delivery_address_fields[] = "order_address_address";
						$delivery_address_values[] = dbquote($row2["order_address_address"]);

						$delivery_address_fields[] = "order_address_street";
						$delivery_address_values[] = dbquote($row2["order_address_street"]);

						$delivery_address_fields[] = "order_address_street_number";
						$delivery_address_values[] = dbquote($row2["order_address_street_number"]);

						$delivery_address_fields[] = "order_address_address2";
						$delivery_address_values[] = dbquote($row2["order_address_address2"]);

						$delivery_address_fields[] = "order_address_zip";
						$delivery_address_values[] = dbquote($row2["order_address_zip"]);

						$delivery_address_fields[] = "order_address_place";
						$delivery_address_values[] = dbquote($row2["order_address_place"]);

						$delivery_address_fields[] = "order_address_place_id";
						$delivery_address_values[] = dbquote($row2["order_address_place_id"]);

						$delivery_address_fields[] = "order_address_country";
						$delivery_address_values[] = dbquote($row2["order_address_country"]);

						$delivery_address_fields[] = "order_address_phone";
						$delivery_address_values[] = dbquote($row2["order_address_phone"]);

						$delivery_address_fields[] = "order_address_phone_country";
						$delivery_address_values[] = dbquote($row2["order_address_phone_country"]);

						$delivery_address_fields[] = "order_address_phone_area";
						$delivery_address_values[] = dbquote($row2["order_address_phone_area"]);

						$delivery_address_fields[] = "order_address_phone_number";
						$delivery_address_values[] = dbquote($row2["order_address_phone_number"]);

						$delivery_address_fields[] = "order_address_mobile_phone";
						$delivery_address_values[] = dbquote($row2["order_address_mobile_phone"]);

						$delivery_address_fields[] = "order_address_mobile_phone_country";
						$delivery_address_values[] = dbquote($row2["order_address_mobile_phone_country"]);

						$delivery_address_fields[] = "order_address_mobile_phone_area";
						$delivery_address_values[] = dbquote($row2["order_address_mobile_phone_area"]);

						$delivery_address_fields[] = "order_address_mobile_phone_number";
						$delivery_address_values[] = dbquote($row2["order_address_mobile_phone_number"]);

						$delivery_address_fields[] = "order_address_email";
						$delivery_address_values[] = dbquote($row2["order_address_email"]);

						$delivery_address_fields[] = "order_address_parent";
						$delivery_address_values[] = dbquote($row2["order_address_parent"]);

						$delivery_address_fields[] = "order_address_contact";
						$delivery_address_values[] = dbquote($row2["order_address_contact"]);

						$delivery_address_fields[] = "date_created";
						$delivery_address_values[] = "current_timestamp";

						$delivery_address_fields[] = "date_modified";
						$delivery_address_values[] = "current_timestamp";

						if (isset($_SESSION["user_login"]))
						{
							$delivery_address_fields[] = "user_created";
							$delivery_address_values[] = dbquote($_SESSION["user_login"]);

							$delivery_address_fields[] = "user_modified";
							$delivery_address_values[] = dbquote($_SESSION["user_login"]);
						}

						$sql = "insert into order_addresses (" . join(", ", $delivery_address_fields) . ") values (" . join(", ", $delivery_address_values) . ")";
						mysql_query($sql) or dberror($sql);
					
					}


					//add tracking information to table order_item_trackings
					$result = track_change_in_order_items('Add item in list of materials', 'Quantity', param("oid"), $item_id, $key,  $type, $item_name, 0, $new_quantity);
					$list_of_materials_was_changed = true;
					
				}
			}
		}
    }


	//send mails on change of the list of materials
	if(param("pid") and $list_of_materials_was_changed == true)
	{
		$result = track_change_in_order_items_send_mail(param("pid"), $is_hq_supplied_item);
	}
	
}

/********************************************************************
   update or delete project order items (material list)
*********************************************************************/
function  project_update_order_items($list)
{
	
	$list_of_materials_was_changed = false;
	$is_hq_supplied_item = false;
	$suppliers = array();
	foreach($list->columns as $key=>$column)
	{
		if($column["name"] == "item_suppliers")
		{
			$suppliers = $list->values("item_suppliers");
		}
	}

	

	foreach ($list->values("item_entry_field") as $key=>$value)
    {
		// update record
        //if ($value >0)
        //{
            
			
			//add tracking information to table order_item_trackings
			$new_quantity = $value;
			$sql = "select order_item_item, order_item_order, order_item_quantity, " . 
				   " order_item_type, order_item_text, order_item_cost_group " . 
				   " from order_items " . 
				   " where order_item_id = " . dbquote($key);

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{

				if($row["order_item_quantity"] != $new_quantity)
				{
					$result = track_change_in_order_items('Update item in list of materials', 'Quantity', $row["order_item_order"], $key, $row["order_item_item"], $row["order_item_type"],  $row["order_item_text"], $row["order_item_quantity"], $new_quantity);
					$list_of_materials_was_changed = true;


					//check if item is a hq supplied item
					if($row["order_item_cost_group"] == 2 
					   or $row["order_item_cost_group"] == 6)
					{
						$is_hq_supplied_item = true;
					}
				}
			}
			
			$order_item_fields = array();
    
            $value = $value ?: 'NULL';
            $order_item_fields[] = "order_item_quantity = " . str_replace(",", ".", $value);;

			

			if(count($suppliers) > 0)
			{
				$order_item_fields[] = "order_item_supplier_address = " . dbquote($suppliers[$key]);
			}

            $value1 = "current_timestamp";
            $project_fields[] = "date_modified = " . $value1;
    
            if (isset($_SESSION["user_login"]))
            {
                $value1 = dbquote($_SESSION["user_login"]);
                $project_fields[] = "user_modified = " . $value1;
            }
       
            $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
            mysql_query($sql) or dberror($sql);


			

        //}

        // delete record in table order_item
        if ($value == 0)
        {
            //delete_record("order_items", $key, true);
        }
    }


	//send mails on change of the list of materials
	if(param("pid") and $list_of_materials_was_changed == true)
	{
		$result = track_change_in_order_items_send_mail(param("pid"), $is_hq_supplied_item);
	}
	return true;

}



/********************************************************************
   update cost monitoring information for items
*********************************************************************/
function  project_update_order_item_cost1($list, $nocom = true, $order_currency = array(), $show_budget_in_loc = false)
{
		
	//get cost group names
	$costgroup_names = array();
	$sql = "select project_cost_groupname_id, project_cost_groupname_name " . 
		   " from project_cost_groupnames ";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$costgroup_names[$row["project_cost_groupname_id"]] = $row["project_cost_groupname_name"];
	}

	//get cost monitoring groups
	$costmonitoringgroups = array();
	$sql = "select costmonitoringgroup_id, costmonitoringgroup_text " . 
		   " from costmonitoringgroups ";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$costmonitoringgroups[$row["costmonitoringgroup_id"]] = $row["costmonitoringgroup_text"];
	}


	//get old values of order_items
	$old_values = array();
	
	if($show_budget_in_loc == true)
	{
		foreach ($list->values("order_item_cost_group") as $key=>$value)
		{
			$sql = "select order_item_order, order_item_type, order_item_cost_group, " .
				   "order_item_item, order_item_text, order_item_quantity, order_item_real_client_price, " .
				   " order_item_cms_remark, order_items_costmonitoring_group " . 
				   " from order_items " .
				   " where order_item_id = " . $key;
			
			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$old_values[$key] = $row;
			}
		}
	}
	else
	{
		foreach ($list->values("order_item_cost_group") as $key=>$value)
		{
			$sql = "select order_item_order, order_item_type, order_item_cost_group, " .
				   "order_item_item, order_item_text, order_item_quantity, order_item_real_system_price, " .
				   " order_item_cms_remark, order_items_costmonitoring_group " . 
				   " from order_items " .
				   " where order_item_id = " . $key;
			
			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$old_values[$key] = $row;
			}
		}
	}

	foreach ($list->values("order_item_cost_group") as $key=>$value)
    {
		// update record
        if(!$value) {$value = 0;}
        $order_item_fields = array();

        $order_item_fields[] = "order_item_cost_group = " . $value;

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);

		//add tracking information
		if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_cost_group"])
		{
			 $old_costgroup_name = "";
			 $new_costgroup_name = "";
			 if(array_key_exists($old_values[$key]["order_item_cost_group"], $costgroup_names))
			 {
				$old_costgroup_name = $costgroup_names[$old_values[$key]["order_item_cost_group"]];
			 }
			 if(array_key_exists($value, $costgroup_names))
			 {
				$new_costgroup_name = $costgroup_names[$value];
			 }
			 
			 $result = track_change_in_order_items('Change cost group in CMS', 'Cost Group', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_costgroup_name, $new_costgroup_name);
		}
    }


    foreach ($list->values("order_item_quantity") as $key=>$value)
    {
        // update record
        if(!$value) {$value = 0;}
        $order_item_fields = array();

        $order_item_fields[] = "order_item_quantity = " . str_replace(",", ".", $value);

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);


		//add tracking information
		if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_quantity"])
		{
			 $result = track_change_in_order_items('Change cost group in CMS', 'Quantity', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_values[$key]["order_item_quantity"], $value);
		}

    }

	if($show_budget_in_loc == true)
	{
		foreach ($list->values("order_item_real_client_price") as $key=>$value)
		{
			// update record
			if(!$value) {$value = 0;}
		
			$order_item_fields = array();

			$order_item_fields[] = "order_item_real_client_price = " . $value;
			
			if(count($order_currency) > 0)
			{
				$client_price = $value;
				if($order_currency["exchange_rate"] > 0)
				{
					$system_price = $client_price * $order_currency["exchange_rate"] / $order_currency["factor"];
				}
				else
				{
					$system_price = 0;
				}
				$order_item_fields[] = "order_item_real_system_price = " . $system_price;
			}

			$value1 = "current_timestamp";
			$project_fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = dbquote($_SESSION["user_login"]);
				$project_fields[] = "user_modified = " . $value1;
			}
	   
			$sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
			mysql_query($sql) or dberror($sql);

			//add tracking information
			if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_real_client_price"])
			{
				 $result = track_change_in_order_items('Change cost group in CMS', 'Real Cost', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $order_currency["symbol"] . " " . $old_values[$key]["order_item_real_client_price"], $order_currency["symbol"] . " " . $value);
			}
		}
	}
	else
	{
		foreach ($list->values("order_item_real_system_price") as $key=>$value)
		{
			// update record
			if(!$value) {$value = 0;}
		
			$order_item_fields = array();

			$order_item_fields[] = "order_item_real_system_price = " . $value;
			
			if(count($order_currency) > 0)
			{
				$system_price = $value;
				if($order_currency["exchange_rate"] > 0)
				{
					$client_price = $system_price / $order_currency["exchange_rate"] * $order_currency["factor"];
				}
				else
				{
					$client_price = 0;
				}
				$order_item_fields[] = "order_item_real_client_price = " . $client_price;
			}

			$value1 = "current_timestamp";
			$project_fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = dbquote($_SESSION["user_login"]);
				$project_fields[] = "user_modified = " . $value1;
			}
	   
			$sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
			mysql_query($sql) or dberror($sql);

			//add tracking information
			if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_real_system_price"])
			{
				 $result = track_change_in_order_items('Change cost group in CMS', 'Real Cost', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], "CHF " . $old_values[$key]["order_item_real_system_price"], "CHF " . $value);
			}
		}
	}

	foreach ($list->values("order_item_cms_remark") as $key=>$value)
    {
        // update record
    
        $order_item_fields = array();

        
		$order_item_fields[] = "order_item_cms_remark = " . dbquote($value);

        $value1 = "current_timestamp";
        $project_fields[] = "date_modified = " . $value1;

        if (isset($_SESSION["user_login"]))
        {
            $value1 = dbquote($_SESSION["user_login"]);
            $project_fields[] = "user_modified = " . $value1;
        }
   
        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);


		//add tracking information
		if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_cms_remark"])
		{
			 $result = track_change_in_order_items('Change cost group in CMS', 'Remark', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_values[$key]["order_item_cms_remark"], $value);
		}
    }

    /*
    if($project["order_date"] > "2005-10-31")
    {
        //update budget approved
        if(isset($list["order_item_quantity_freezed"]))
        {
            foreach ($list->values("order_item_quantity_freezed") as $key=>$value)
            {
                // update record
                if(!$value) {$value = 0;}
                $order_item_fields = array();

                $order_item_fields[] = "order_item_quantity_freezed = " . $value;

                $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
                mysql_query($sql) or dberror($sql);
            }

            foreach ($list->values("order_item_system_price_freezed") as $key=>$value)
            {
                // update record
                if(!$value) {$value = 0;}
            
                $order_item_fields = array();

                $order_item_fields[] = "order_item_system_price_freezed = " . $value;

                $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
                mysql_query($sql) or dberror($sql);
            }
        }
    }
	*/
	
	/* cost monitoring group was removed by Katrin Eckert on January 26th 2016
	if($nocom == true)
	{
		foreach ($list->values("order_items_costmonitoring_group") as $key=>$value)
		{
			// update record
			if(!$value) {$value = 0;}
		
			$order_item_fields = array();

			$order_item_fields[] = "order_items_costmonitoring_group = " . $value;

			$value1 = "current_timestamp";
			$project_fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = dbquote($_SESSION["user_login"]);
				$project_fields[] = "user_modified = " . $value1;
			}
	   
			$sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
			mysql_query($sql) or dberror($sql);

			//add tracking information
			if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_items_costmonitoring_group"])
			{
				 $old_costmonitoring_group_name = "";
				 $new_costmonitoring__group_name = "";
				 if(array_key_exists($old_values[$key]["order_items_costmonitoring_group"], $costmonitoringgroups))
				 {
					$old_costmonitoring_group_name = $costmonitoringgroups[$old_values[$key]["order_items_costmonitoring_group"]];
				 }
				 if(array_key_exists($value, $costmonitoringgroups))
				 {
					$new_costmonitoring__group_name = $costmonitoringgroups[$value];
				 }
				 
				 $result = track_change_in_order_items('Change cost monitoring group in CMS', 'Benchmark Item Group', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_costmonitoring_group_name, $new_costmonitoring__group_name);
			}
		}
	}
	*/
}


/********************************************************************
   update cost monitoring information for other cost
*********************************************************************/
function  project_update_order_item_cost2($list, $order_currency = array(), $show_budget_in_loc = false)
{

	//get cost group names
	$costgroup_names = array();
	$sql = "select project_cost_groupname_id, project_cost_groupname_name " . 
		   " from project_cost_groupnames ";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$costgroup_names[$row["project_cost_groupname_id"]] = $row["project_cost_groupname_name"];
	}

	
	//get old values of order_items
	$old_values = array();
	if($show_budget_in_loc == true)
	{
		foreach ($list->values("order_item_cost_group") as $key=>$value)
		{
			$sql = "select order_item_order, order_item_type, order_item_cost_group, " .
				   "order_item_item, order_item_text, order_item_quantity, order_item_real_client_price, " .
				   " order_item_cms_remark, order_items_costmonitoring_group " . 
				   " from order_items " .
				   " where order_item_id = " . $key;
			
			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$old_values[$key] = $row;
			}
		}
	}
	else
	{
		foreach ($list->values("order_item_cost_group") as $key=>$value)
		{
			$sql = "select order_item_order, order_item_type, order_item_cost_group, " .
				   "order_item_item, order_item_text, order_item_quantity, order_item_real_system_price, " .
				   " order_item_cms_remark, order_items_costmonitoring_group " . 
				   " from order_items " .
				   " where order_item_id = " . $key;
			
			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$old_values[$key] = $row;
			}
		}
	}

	foreach ($list->values("order_item_cost_group") as $key=>$value)
    {
        // update record
        if(!$value) {$value = 0;}
        $order_item_fields = array();

        $order_item_fields[] = "order_item_cost_group = " . $value;

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);


		//add tracking information
		if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_cost_group"])
		{
			 $old_costgroup_name = "";
			 $new_costgroup_name = "";
			 if(array_key_exists($old_values[$key]["order_item_cost_group"], $costgroup_names))
			 {
				$old_costgroup_name = $costgroup_names[$old_values[$key]["order_item_cost_group"]];
			 }
			 if(array_key_exists($value, $costgroup_names))
			 {
				$new_costgroup_name = $costgroup_names[$value];
			 }
			 
			 $result = track_change_in_order_items('Change cost group in CMS', 'Cost Group', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_costgroup_name, $new_costgroup_name);
		}
    }

    /*
	foreach ($list->values("order_item_supplier_freetext") as $key=>$value)
    {
        // update record

        $order_item_fields = array();

        $order_item_fields[] = "order_item_supplier_freetext = " . dbquote($value);

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);

    }
	*/

    if($show_budget_in_loc == true)
	{
		foreach ($list->values("order_item_real_client_price") as $key=>$value)
		{
			// update record
			if(!$value) {$value = 0;}

			$order_item_fields = array();

			$order_item_fields[] = "order_item_real_client_price = " . $value;

			if(count($order_currency) > 0)
			{
				$client_price = $value;
				if($order_currency["exchange_rate"] > 0)
				{
					$system_price = $client_price * $order_currency["exchange_rate"] / $order_currency["factor"];
				}
				else
				{
					$system_price = 0;
				}
				$order_item_fields[] = "order_item_real_system_price = " . $system_price;
			}


			$value1 = "current_timestamp";
			$project_fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = dbquote($_SESSION["user_login"]);
				$project_fields[] = "user_modified = " . $value1;
			}
	   
			$sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
			mysql_query($sql) or dberror($sql);

			//echo $sql . "<br />";

			//add tracking information
			if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_real_client_price"])
			{
				 $result = track_change_in_order_items('Change cost group in CMS', 'Real Cost', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $order_currency["symbol"] . " " . $old_values[$key]["order_item_real_client_price"], $order_currency["symbol"] . " " . $value);
			}

		}
	}
	else
	{
		foreach ($list->values("order_item_real_system_price") as $key=>$value)
		{
			// update record
			if(!$value) {$value = 0;}

			$order_item_fields = array();


			$order_item_fields[] = "order_item_real_system_price = " . $value;

			if(count($order_currency) > 0)
			{
				$system_price = $value;
				if($order_currency["exchange_rate"] > 0)
				{
					$client_price = $system_price / $order_currency["exchange_rate"] * $order_currency["factor"];
				}
				else
				{
					$client_price = 0;
				}
				$order_item_fields[] = "order_item_real_client_price = " . $client_price;
			}


			$value1 = "current_timestamp";
			$project_fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = dbquote($_SESSION["user_login"]);
				$project_fields[] = "user_modified = " . $value1;
			}
	   
			$sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
			mysql_query($sql) or dberror($sql);

			//add tracking information
			if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_real_system_price"])
			{
				 $result = track_change_in_order_items('Change cost group in CMS', 'Real Cost', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], "CHF " . $old_values[$key]["order_item_real_system_price"], "CHF " . $value);
			}

		}
	}

	foreach ($list->values("order_item_cms_remark") as $key=>$value)
    {
        // update record
    
        $order_item_fields = array();

        
		$order_item_fields[] = "order_item_cms_remark = " . dbquote($value);

        $value1 = "current_timestamp";
        $project_fields[] = "date_modified = " . $value1;

        if (isset($_SESSION["user_login"]))
        {
            $value1 = dbquote($_SESSION["user_login"]);
            $project_fields[] = "user_modified = " . $value1;
        }
   
        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);

		//add tracking information
		if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_cms_remark"])
		{
			 $result = track_change_in_order_items('Change cost group in CMS', 'Remark', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_values[$key]["order_item_cms_remark"], $value);
		}
    }


	/*
	foreach ($list->values("order_item_system_price_freezed") as $key=>$value)
    {
        // update record
        if(!$value) {$value = 0;}

        $order_item_fields = array();

		$order_item_fields[] = "order_item_system_price_freezed = " . $value;
		$order_item_fields[] = "order_item_client_price_freezed = " . $value;

        $value1 = "current_timestamp";
        $project_fields[] = "date_modified = " . $value1;

        if (isset($_SESSION["user_login"]))
        {
            $value1 = dbquote($_SESSION["user_login"]);
            $project_fields[] = "user_modified = " . $value1;
        }
   
        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);

    }
	*/

    /*
	if($project["order_date"] > "2005-10-31")
    {
        //budget approved
        foreach ($list->values("order_item_system_price_freezed") as $key=>$value)
        {
            // update record
            if(!$value) {$value = 0;}

            $order_item_fields = array();

            $order_item_fields[] = "order_item_system_price_freezed = " . $value;

            $value1 = "current_timestamp";
            $project_fields[] = "date_modified = " . $value1;

            if (isset($_SESSION["user_login"]))
            {
                $value1 = dbquote($_SESSION["user_login"]);
                $project_fields[] = "user_modified = " . $value1;
            }
       
            $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
            mysql_query($sql) or dberror($sql);
        }
    }
	*/
}



/********************************************************************
   update cost monitoring information for items
*********************************************************************/
function  project_update_order_item_cost3($list, $order_currency = array(), $show_budget_in_loc = false)
{
    
	

	if($show_budget_in_loc == true)
	{
		//get old values of order_items
		$old_values = array();
		foreach ($list->values("order_items_in_project_real_client_price") as $key=>$value)
		{
			$sql = "select order_items_in_project_project_order_id, order_item_type, order_item_cost_group, " .
				   "order_item_item, order_item_text, order_item_quantity, order_items_in_project_real_client_price, " .
				   " order_item_cms_remark, order_items_costmonitoring_group " . 
				   " from order_items " .
				   " left join order_items_in_projects on order_items_in_project_order_item_id = order_item_id " .
				   " where order_items_in_project_id = " . $key;

			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$old_values[$key] = $row;
			}
		}
		
		foreach ($list->values("order_items_in_project_real_client_price") as $key=>$value)
		{
			// update record
			if(!$value) {$value = 0;}
			$order_item_fields = array();

			$order_item_fields[] = "order_items_in_project_real_client_price = " . $value;

			if(count($order_currency) > 0)
			{
				$client_price = $value;
				if($order_currency["exchange_rate"] > 0)
				{
					$system_price = $client_price * $order_currency["exchange_rate"] / $order_currency["factor"];
				}
				else
				{
					$system_price = 0;
				}
				$order_item_fields[] = "order_items_in_project_real_price = " . $system_price;
			}

			$sql = "update order_items_in_projects set " . join(", ", $order_item_fields) . " where order_items_in_project_id = " . $key;
			mysql_query($sql) or dberror($sql);

			//add tracking information
			if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_items_in_project_real_client_price"])
			{
				 $result = track_change_in_order_items('Change cost group in CMS', 'Real Cost', $old_values[$key]["order_items_in_project_project_order_id"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $order_currency["symbol"] . " " . $old_values[$key]["order_items_in_project_real_client_price"], $order_currency["symbol"] . " " . $value);
			}
		}
	}
	else
	{
		//get old values of order_items
		$old_values = array();
		foreach ($list->values("order_items_in_project_real_price") as $key=>$value)
		{
			$sql = "select order_items_in_project_project_order_id, order_item_type, order_item_cost_group, " .
				   "order_item_item, order_item_text, order_item_quantity, order_items_in_project_real_price, " .
				   " order_item_cms_remark, order_items_costmonitoring_group " . 
				   " from order_items " .
				   " left join order_items_in_projects on order_items_in_project_order_item_id = order_item_id " .
				   " where order_items_in_project_id = " . $key;
			
			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$old_values[$key] = $row;
			}
		}
		
		foreach ($list->values("order_items_in_project_real_price") as $key=>$value)
		{
			// update record
			if(!$value) {$value = 0;}
			$order_item_fields = array();

			$order_item_fields[] = "order_items_in_project_real_price = " . $value;

			if(count($order_currency) > 0)
			{
				$system_price = $value;
				if($order_currency["exchange_rate"] > 0)
				{
					$client_price = $system_price / $order_currency["exchange_rate"] * $order_currency["factor"];
				}
				else
				{
					$client_price = 0;
				}
				$order_item_fields[] = "order_items_in_project_real_client_price = " . $client_price;
			}

			$sql = "update order_items_in_projects set " . join(", ", $order_item_fields) . " where order_items_in_project_id = " . $key;
			mysql_query($sql) or dberror($sql);

			echo $sql . "<br />";


			//add tracking information
			if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_items_in_project_real_price"])
			{
				 $result = track_change_in_order_items('Change cost group in CMS', 'Real Cost', $old_values[$key]["order_items_in_project_project_order_id"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], "CHF " . $old_values[$key]["order_items_in_project_real_price"], "CHF " . $value);
			}
		}
	}

	
	foreach ($list->values("order_items_in_project_cms_remark") as $key=>$value)
	{
		// update record
		$order_item_fields = array();

		$order_item_fields[] = "order_items_in_project_cms_remark = " . dbquote($value);
		
		$sql = "update order_items_in_projects set " . join(", ", $order_item_fields) . " where order_items_in_project_id = " . $key;
		mysql_query($sql) or dberror($sql);

	}
}


/********************************************************************
   update cost monitoring information for items: reinvoice data
*********************************************************************/
function  project_update_order_item_reinvoice_data_0($list)
{
    
    //get old values of order_items
	$old_values = array();
	foreach ($list->values("order_item_reinvoiced") as $key=>$value)
    {
		$sql = "select order_item_order, order_item_type, order_item_item, " .
			   " order_item_text, order_item_reinvoiced, order_item_reinvoicenbr, " .
			   " order_item_reinvoiced2, order_item_reinvoicenbr2 " . 
			   " from order_items " .
			   " where order_item_id = " . $key;
		
		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$old_values[$key] = $row;
		}
	}
	
	$pos = array();
    $sups = array();

    $sql = $list->sql;

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $pos[] = $row["order_item_po_number"];
        $sups[] = $row["order_item_supplier_address"];

    }
    
    $i = 0;
    foreach ($list->values("order_item_reinvoiced") as $key=>$value)
    {
           
           if($i < count($pos))
           {
               // update record
                $order_item_fields = array();
                if($value)
                {
                    $order_item_fields[] = "order_item_reinvoiced = " . dbquote(from_system_date($value));
				}
				else
			    {
					$order_item_fields[] = "order_item_reinvoiced = NULL";
			    }

                $sql = "update order_items set " . join(", ", $order_item_fields) . 
					" where order_item_po_number = " . dbquote($pos[$i]) . 
					" and order_item_supplier_address = " . dbquote($sups[$i]);
                mysql_query($sql) or dberror($sql);


				//add tracking information
				if(array_key_exists($key, $old_values) and from_system_date($value) != $old_values[$key]["order_item_reinvoiced"])
				{
					 $result = track_change_in_order_items('Change reinvoice date in CMS', 'Reinvoice Date', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_values[$key]["order_item_reinvoiced"], from_system_date($value));
				}

           }
            $i++;
    }

    $i = 0;
    foreach ($list->values("order_item_reinvoicenbr") as $key=>$value)
    {
           
           if($i < count($pos))
           {
               // update record
                $order_item_fields = array();

                $order_item_fields[] = "order_item_reinvoicenbr = " . dbquote(trim($value));

                $sql = "update order_items set " . join(", ", $order_item_fields) . 
                " where order_item_po_number = " . dbquote($pos[$i]) . 
                " and order_item_supplier_address = " . dbquote($sups[$i]);
                mysql_query($sql) or dberror($sql);


				//add tracking information
				if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_reinvoicenbr"])
				{
					 $result = track_change_in_order_items('Change reinvoice number in CMS', 'Reinvoice Number', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_values[$key]["order_item_reinvoicenbr"], $value);
				}
           }
            $i++;
    }


	$i = 0;
    foreach ($list->values("order_item_reinvoiced2") as $key=>$value)
    {
           
           if($i < count($pos))
           {
               // update record
                $order_item_fields = array();
                if($value)
                {
                    $order_item_fields[] = "order_item_reinvoiced2 = " . dbquote(from_system_date($value));
				}
				else
			    {
					$order_item_fields[] = "order_item_reinvoiced2 = NULL";
			    }

                $sql = "update order_items set " . join(", ", $order_item_fields) . 
					" where order_item_po_number = " . dbquote($pos[$i]) . 
					" and order_item_supplier_address = " . dbquote($sups[$i]);
			    mysql_query($sql) or dberror($sql);

				//add tracking information
				if(array_key_exists($key, $old_values) and from_system_date($value) != $old_values[$key]["order_item_reinvoiced2"])
				{
					 $result = track_change_in_order_items('Change reinvoice date in CMS', 'Reinvoice Date', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_values[$key]["order_item_reinvoiced2"], from_system_date($value));
				}

           }
            $i++;
    }

    $i = 0;
    foreach ($list->values("order_item_reinvoicenbr2") as $key=>$value)
    {
           
           if($i < count($pos))
           {
               // update record
                $order_item_fields = array();

                $order_item_fields[] = "order_item_reinvoicenbr2 = " . dbquote(trim($value));

                $sql = "update order_items set " . join(", ", $order_item_fields) . 
                " where order_item_po_number = " . dbquote($pos[$i]) . 
                " and order_item_supplier_address = " . dbquote($sups[$i]);
                mysql_query($sql) or dberror($sql);

				//add tracking information
				if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_reinvoicenbr2"])
				{
					 $result = track_change_in_order_items('Change reinvoice number in CMS', 'Reinvoice Number', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_values[$key]["order_item_reinvoicenbr2"], $value);
				}
           }
            $i++;
    }
}


/********************************************************************
   update cost monitoring information for items: reinvoice data
*********************************************************************/
function  project_update_order_item_reinvoice_data($list)
{
    
    //get old values of order_items
	$old_values = array();
	foreach ($list->values("order_item_reinvoiced") as $key=>$value)
    {
		$sql = "select order_item_order, order_item_type, order_item_item, " .
			   " order_item_text, order_item_reinvoiced, order_item_reinvoicenbr, " .
			   " order_item_reinvoiced2, order_item_reinvoicenbr2 " . 
			   " from order_items " .
			   " where order_item_id = " . $key;
		
		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$old_values[$key] = $row;
		}
	}
	
	foreach ($list->values("order_item_reinvoiced") as $key=>$value)
    {
        // update record
        
            $order_item_fields = array();

            if($value)
			{
				$order_item_fields[] = "order_item_reinvoiced = " . dbquote(from_system_date($value));
			}
			else
			{
				$order_item_fields[] = "order_item_reinvoiced = NULL";
			}

			$sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
			mysql_query($sql) or dberror($sql);

			//add tracking information
			if(array_key_exists($key, $old_values) and from_system_date($value) != $old_values[$key]["order_item_reinvoiced"])
			{
				 $result = track_change_in_order_items('Change reinvoice date in CMS', 'Reinvoice Date', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_values[$key]["order_item_reinvoiced"], from_system_date($value));
			}

    }

    foreach ($list->values("order_item_reinvoicenbr") as $key=>$value)
    {
        // update record
        $order_item_fields = array();

        $order_item_fields[] = "order_item_reinvoicenbr = " . dbquote(trim($value));

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);

		//add tracking information
		if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_reinvoicenbr"])
		{
			 $result = track_change_in_order_items('Change reinvoice number in CMS', 'Reinvoice Number', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_values[$key]["order_item_reinvoicenbr"], $value);
		}
    }

	foreach ($list->values("order_item_reinvoiced2") as $key=>$value)
    {
        // update record
        
            $order_item_fields = array();

            if($value)
			{
				$order_item_fields[] = "order_item_reinvoiced2 = " . dbquote(from_system_date($value));
			}
			else
			{
				$order_item_fields[] = "order_item_reinvoiced2 = NULL";
			}

            $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
            mysql_query($sql) or dberror($sql);

			//add tracking information
			if(array_key_exists($key, $old_values) and from_system_date($value) != $old_values[$key]["order_item_reinvoiced2"])
			{
				 $result = track_change_in_order_items('Change reinvoice date in CMS', 'Reinvoice Date', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_values[$key]["order_item_reinvoiced2"], from_system_date($value));
			}
    }

    foreach ($list->values("order_item_reinvoicenbr2") as $key=>$value)
    {
        // update record
        $order_item_fields = array();

        $order_item_fields[] = "order_item_reinvoicenbr2 = " . dbquote(trim($value));

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);

		//add tracking information
		if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_item_reinvoicenbr2"])
		{
			 $result = track_change_in_order_items('Change reinvoice number in CMS', 'Reinvoice Number', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_values[$key]["order_item_reinvoicenbr2"], $value);
		}
    }

}

/********************************************************************
   update cost monitoring information for items: cost group data
*********************************************************************/
function  project_update_order_item_group_data($list, $order_id)
{
	//get old values of order_items
	$old_values = array();
	foreach ($list->values("order_items_monitoring_group") as $key=>$value)
    {
		$sql = "select order_item_order, order_item_type, order_item_item, " .
			   " order_item_text, order_items_monitoring_group " . 
			   " from order_items " .
			   " where order_item_id = " . $key;
		
		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$old_values[$key] = $row;
		}
	}
    
	foreach ($list->values("order_items_monitoring_group") as $key=>$value)
    {

        // update record
        $order_item_fields = array();

        if($value)
		{
			$order_item_fields[] = "order_items_monitoring_group = " . dbquote($value);
		}
		else
		{
			$order_item_fields[] = "order_items_monitoring_group = NULL";
		}

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $key;
        mysql_query($sql) or dberror($sql);

		//add tracking information
		if(array_key_exists($key, $old_values) and $value != $old_values[$key]["order_items_monitoring_group"])
		{
			 $result = track_change_in_order_items('Change grouping in CMS', 'Group Name', $old_values[$key]["order_item_order"], $key, $old_values[$key]["order_item_item"],  $old_values[$key]["order_item_type"], $old_values[$key]["order_item_text"], $old_values[$key]["order_items_monitoring_group"], $value);
		}
    }

    // update all empty fields with th entry the user made for the sam supplier

    $sql = "select distinct order_item_supplier_address, " .
           "order_items_monitoring_group  " .
           "from order_items " .
           "where order_item_order = " . $order_id .
           "   and order_items_monitoring_group <>'' ";

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $monitoring_group = $row["order_items_monitoring_group"];
        $supplier = $row["order_item_supplier_address"];

        $order_item_fields = array();

        $order_item_fields[] = "order_items_monitoring_group = " . dbquote($monitoring_group);

        $sql = "update order_items set " . join(", ", $order_item_fields) . 
        "  where order_item_order = " . $order_id .
        "   and order_item_supplier_address = " . $supplier . 
        "   and (order_items_monitoring_group  = '' or order_items_monitoring_group is null)";


        mysql_query($sql) or dberror($sql);
    }

}

/***********************************************************************
   insert special item or cost estimation record
************************************************************************/
function  project_add_special_item_save($form)
{
	$list_of_materials_was_changed = false;
	$is_hq_supplied_item = false;

	$order_item_fields = array();
	$order_item_values = array();

	$order_item_fields[] = "order_item_order";
	$order_item_values[] = trim($form->value("order_item_order")) == "" ? "null" : dbquote($form->value("order_item_order"));

	$order_item_fields[] = "order_item_type";
	$order_item_values[] = trim($form->value("order_item_type")) == "" ? "null" : dbquote($form->value("order_item_type"));

	$order_item_fields[] = "order_item_text";
	$order_item_values[] = trim($form->value("order_item_text")) == "" ? "null" : dbquote($form->value("order_item_text"));

	
	if(array_key_exists("suppliers_currency", $form->items) and $form->value("suppliers_currency") !=1)
	{
		$order_item_fields[] = "order_item_price_entered_in_loc";
		$order_item_values[] = 1;
	}
	elseif(array_key_exists("currency", $form->items) and $form->value("currency") !=1)
	{
		 $order_item_fields[] = "order_item_price_entered_in_loc";
		 $order_item_values[] = 1;
	}

	if ($form->value("order_item_type") == ITEM_TYPE_SPECIAL)
	{
		
		if(array_key_exists("order_item_category", $form->items))
		{
			$order_item_fields[] = "order_item_category";
			$order_item_values[] = dbquote($form->value("order_item_category"));
		}
		
		
		//logistic fields
		$order_item_fields[] = "order_item_unit_id";
		$order_item_values[] = trim($form->value("order_item_unit_id")) == "" ? "null" : dbquote($form->value("order_item_unit_id"));

		$order_item_fields[] = "order_item_width";
		$order_item_values[] = trim($form->value("order_item_width")) == "" ? "null" : dbquote($form->value("order_item_width"));

		$order_item_fields[] = "order_item_length";
		$order_item_values[] = trim($form->value("order_item_length")) == "" ? "null" : dbquote($form->value("order_item_length"));

		$order_item_fields[] = "order_item_height";
		$order_item_values[] = trim($form->value("order_item_height")) == "" ? "null" : dbquote($form->value("order_item_height"));

		$order_item_fields[] = "order_item_gross_weight";
		$order_item_values[] = trim($form->value("order_item_gross_weight")) == "" ? "null" : dbquote($form->value("order_item_gross_weight"));

		$order_item_fields[] = "order_item_packaging_type_id";
		$order_item_values[] = trim($form->value("order_item_packaging_type_id")) == "" ? "null" : dbquote($form->value("order_item_packaging_type_id"));

		$order_item_fields[] = "order_item_stackable";
		$order_item_values[] = trim($form->value("order_item_stackable")) == "" ? "null" : dbquote($form->value("order_item_stackable"));
		
		
		$order_item_fields[] = "order_item_quantity";
		$order_item_values[] = trim($form->value("order_item_quantity")) == "" ? "null" : dbquote($form->value("order_item_quantity"));


		$order_item_fields[] = "order_item_supplier_price";
		$order_item_values[] = trim($form->value("order_item_supplier_price")) == "" ? 0 : dbquote($form->value("order_item_supplier_price"));


		$order_item_fields[] = "order_item_supplier_address";
		$order_item_values[] = trim($form->value("order_item_supplier_address")) == "" ? "null" : dbquote($form->value("order_item_supplier_address"));

		
		$order_item_fields[] = "order_item_no_offer_required";
		$order_item_values[] = trim($form->value("order_item_no_offer_required")) == "" ? "null" : dbquote($form->value("order_item_no_offer_required"));

		$order_item_fields[] = "order_item_only_quantity_proposal";
		$order_item_values[] = trim($form->value("order_item_only_quantity_proposal")) == "" ? "null" : dbquote($form->value("order_item_only_quantity_proposal"));
		
		
		// get suppliers's currency information
		//$supplier_currency = get_address_currency($form->value("order_item_supplier_address"));
		$supplier_currency = get_currency($form->value("suppliers_currency"));

		$order_item_fields[] = "order_item_supplier_currency";
		$order_item_values[] = $supplier_currency["id"];


		$order_item_fields[] = "order_item_supplier_exchange_rate";
		$order_item_values[] = $form->value("order_item_supplier_exchange_rate");

		if(isset($form->items["order_item_cost_group"]))
		{
			$order_item_fields[] = "order_item_cost_group";
			$order_item_values[] = dbquote($form->value("order_item_cost_group"));

			/*
			$order_item_fields[] = "order_items_costmonitoring_group";
			$order_item_values[] = dbquote($form->value("order_items_costmonitoring_group"));
			*/

			if(isset($form->items["order_item_exclude_from_ps"]))
			{
				$order_item_fields[] = "order_item_exclude_from_ps";
				$order_item_values[] = dbquote($form->value("order_item_exclude_from_ps"));
			}


			//check if item is a hq supplied item
			if($form->value("order_item_cost_group") == 2 
			   or $form->value("order_item_cost_group") == 6)
			{
				$is_hq_supplied_item = true;
			}
		}

	}

	if ($form->value("order_item_type") == ITEM_TYPE_COST_ESTIMATION)
	{
		if(isset($form->items["order_item_supplier_freetext"]))
		{
			$order_item_fields[] = "order_item_supplier_freetext";
			$order_item_values[] = dbquote($form->value("order_item_supplier_freetext"));
		}

		if(isset($form->items["order_item_cost_group"]))
		{
			$order_item_fields[] = "order_item_cost_group";
			$order_item_values[] =$form->value("order_item_cost_group");

			//check if item is a hq supplied item
			if($form->value("order_item_cost_group") == 2 
			   or $form->value("order_item_cost_group") == 6)
			{
				$is_hq_supplied_item = true;
			}

		}
		$order_item_fields[] = "order_item_system_price";
		$order_item_values[] =$form->value("order_item_system_price");

		
		$order_item_fields[] = "order_item_quantity";
		$order_item_values[] = 1;

	}

	if ($form->value("order_item_type") == ITEM_TYPE_LOCALCONSTRUCTIONCOST)
	{
		if(isset($form->items["order_item_supplier_freetext"]))
		{
			$order_item_fields[] = "order_item_supplier_freetext";
			$order_item_values[] = dbquote($form->value("order_item_supplier_freetext"));
		}

		$order_item_fields[] = "order_item_system_price";
		$order_item_values[] =$form->value("order_item_system_price");

		if(isset($form->items["order_item_cost_group"]))
		{
			$order_item_fields[] = "order_item_cost_group";
			$order_item_values[] = $form->value("order_item_cost_group");

			//check if item is a hq supplied item
			if($form->value("order_item_cost_group") == 2 
			   or $form->value("order_item_cost_group") == 6)
			{
				$is_hq_supplied_item = true;
			}

			$order_item_fields[] = "order_item_quantity";
			$order_item_values[] = 1;
		}

	}

	
	if ($form->value("order_item_type") == ITEM_TYPE_SPECIAL)
	{

		$system_price = $form->value("order_item_supplier_price") * $form->value("order_item_supplier_exchange_rate") / $supplier_currency["factor"];

		$order_item_fields[] = "order_item_system_price";
		$order_item_values[] = dbquote($system_price);
		
		// get orders's currency
		$currency = get_order_currency($form->value("order_item_order"));
		$client_price = $system_price / $currency["exchange_rate"] * $currency["factor"];

		$order_item_fields[] = "order_item_client_price";
		$order_item_values[] = dbquote($client_price);
		
	}
	else
	{
		// get orders's currency
		$currency = get_order_currency($form->value("order_item_order"));

		$client_price = $form->value("order_item_system_price") / $currency["exchange_rate"] * $currency["factor"];

		$order_item_fields[] = "order_item_client_price";
		$order_item_values[] = trim($client_price) == "" ? "null" : dbquote($client_price);
	}

	$order_item_fields[] = "date_created";
	$order_item_values[] = "current_timestamp";

	$order_item_fields[] = "date_modified";
	$order_item_values[] = "current_timestamp";
			
	if (isset($_SESSION["user_login"]))
	{
		$order_item_fields[] = "user_created";
		$order_item_values[] = dbquote($_SESSION["user_login"]);

		$order_item_fields[] = "user_modified";
		$order_item_values[] = dbquote($_SESSION["user_login"]);
	}
	$sql = "insert into order_items (" . join(", ", $order_item_fields) . ") values (" . join(", ", $order_item_values) . ")";

	mysql_query($sql) or dberror($sql);

	$item_id = mysql_insert_id();



	//add tracking information to table order_item_trackings
	if ($form->value("order_item_type") == ITEM_TYPE_SPECIAL)
	{
		$result = track_change_in_order_items('Add item in list of materials', 'Quantity', $form->value("order_item_order"), $item_id, "",  $form->value("order_item_type"), $form->value("order_item_text"), 0, $form->value("order_item_quantity"));
		$list_of_materials_was_changed = true;

		$result = track_change_in_order_items('Add item in list of materials', 'Supplier Price', $form->value("order_item_order"), $item_id, "",  $form->value("order_item_type"), $form->value("order_item_text"), 0, $form->value("order_item_supplier_price"));
		$list_of_materials_was_changed = true;


		
	}
	elseif ($form->value("order_item_type") == ITEM_TYPE_COST_ESTIMATION
		or $form->value("order_item_type") == ITEM_TYPE_LOCALCONSTRUCTIONCOST)
	{
		$result = track_change_in_order_items('Add item in list of materials', 'Price', $form->value("order_item_order"), $item_id, "",  $form->value("order_item_type"), $form->value("order_item_text"), 0, $form->value("order_item_system_price"));
		$list_of_materials_was_changed = true;
	}
	
	
	if ($form->value("order_item_type") == ITEM_TYPE_SPECIAL)
	{
		// copy delivery address for new item in table order_addresses
		$sql = "select * from order_addresses ".
			   "where order_address_type = 2 ".
			   "    and order_address_order = ". param("oid") .
			   "    and (order_address_order_item = 0 ".
			   "    or order_address_order_item is null) ";

		$res2 = mysql_query($sql) or dberror($sql);
		$row2 = mysql_fetch_assoc($res2);

		$delivery_address_fields = array();
		$delivery_address_values = array();

		$delivery_address_fields[] = "order_address_order";
		$delivery_address_values[] = $form->value("order_item_order");

		$delivery_address_fields[] = "order_address_order_item";
		$delivery_address_values[] = $item_id;

		$delivery_address_fields[] = "order_address_type";
		$delivery_address_values[] = 2;

		$delivery_address_fields[] = "order_address_company";
		$delivery_address_values[] = dbquote($row2["order_address_company"]);

		$delivery_address_fields[] = "order_address_company2";
		$delivery_address_values[] = dbquote($row2["order_address_company2"]);

		$delivery_address_fields[] = "order_address_address";
		$delivery_address_values[] = dbquote($row2["order_address_address"]);

		$delivery_address_fields[] = "order_address_street";
		$delivery_address_values[] = dbquote($row2["order_address_street"]);

		$delivery_address_fields[] = "order_address_street_number";
		$delivery_address_values[] = dbquote($row2["order_address_street_number"]);

		$delivery_address_fields[] = "order_address_address2";
		$delivery_address_values[] = dbquote($row2["order_address_address2"]);

		$delivery_address_fields[] = "order_address_zip";
		$delivery_address_values[] = dbquote($row2["order_address_zip"]);

		$delivery_address_fields[] = "order_address_place";
		$delivery_address_values[] = dbquote($row2["order_address_place"]);

		$delivery_address_fields[] = "order_address_country";
		$delivery_address_values[] = dbquote($row2["order_address_country"]);

		$delivery_address_fields[] = "order_address_phone";
		$delivery_address_values[] = dbquote($row2["order_address_phone"]);

		$delivery_address_fields[] = "order_address_phone_country";
		$delivery_address_values[] = dbquote($row2["order_address_phone_country"]);

		$delivery_address_fields[] = "order_address_phone_area";
		$delivery_address_values[] = dbquote($row2["order_address_phone_area"]);

		$delivery_address_fields[] = "order_address_phone_number";
		$delivery_address_values[] = dbquote($row2["order_address_phone_number"]);

		$delivery_address_fields[] = "order_address_mobile_phone";
		$delivery_address_values[] = dbquote($row2["order_address_mobile_phone"]);

		$delivery_address_fields[] = "order_address_mobile_phone_country";
		$delivery_address_values[] = dbquote($row2["order_address_mobile_phone_country"]);

		$delivery_address_fields[] = "order_address_mobile_phone_area";
		$delivery_address_values[] = dbquote($row2["order_address_mobile_phone_area"]);

		$delivery_address_fields[] = "order_address_mobile_phone_number";
		$delivery_address_values[] = dbquote($row2["order_address_mobile_phone_number"]);

		$delivery_address_fields[] = "order_address_email";
		$delivery_address_values[] = dbquote($row2["order_address_email"]);

		$delivery_address_fields[] = "order_address_parent";
		$delivery_address_values[] = dbquote($row2["order_address_parent"]);

		$delivery_address_fields[] = "order_address_contact";
		$delivery_address_values[] = dbquote($row2["order_address_contact"]);

		$delivery_address_fields[] = "date_created";
		$delivery_address_values[] = "current_timestamp";

		$delivery_address_fields[] = "date_modified";
		$delivery_address_values[] = "current_timestamp";

		if (isset($_SESSION["user_login"]))
		{
			$delivery_address_fields[] = "user_created";
			$delivery_address_values[] = dbquote($_SESSION["user_login"]);

			$delivery_address_fields[] = "user_modified";
			$delivery_address_values[] = dbquote($_SESSION["user_login"]);
		}

		$sql = "insert into order_addresses (" . join(", ", $delivery_address_fields) . ") values (" . join(", ", $delivery_address_values) . ")";
		mysql_query($sql) or dberror($sql);


		
		//update order_item
		
		if(isset($form->items["order_item_cost_unit_number"]))
		{
			$order_item_fields = array();
			$value =  trim($form->value("order_item_cost_unit_number")) == "" ? "null" : dbquote($form->value("order_item_cost_unit_number"));
			$order_item_fields[] = "order_item_cost_unit_number = " . $value;

			$value =  trim($form->value("order_item_po_number")) == "" ? "null" : dbquote($form->value("order_item_po_number"));
			$order_item_fields[] = "order_item_po_number = " . $value;


			$value =  trim($form->value("order_item_transportation")) == "" ? "null" : dbquote($form->value("order_item_transportation"));
			$order_item_fields[] = "order_item_transportation = " . $value;

			$value =  trim($form->value("order_item_forwarder_address")) == "" ? "null" : dbquote($form->value("order_item_forwarder_address"));
			$order_item_fields[] = "order_item_forwarder_address = " . $value;

			$value =  trim($form->value("order_item_staff_for_discharge")) == "" ? "null" :     dbquote($form->value("order_item_staff_for_discharge"));
			$order_item_fields[] = "order_item_staff_for_discharge = " . $value;

			$value =  trim($form->value("order_item_no_offer_required")) == "" ? "null" : dbquote($form->value("order_item_no_offer_required"));
			$order_item_fields[] = "order_item_no_offer_required = " . $value;

			$value =  trim($form->value("order_item_not_in_budget")) == "" ? "null" : dbquote($form->value("order_item_not_in_budget"));
			$order_item_fields[] = "order_item_not_in_budget = " . $value;
			
			$value = "current_timestamp";
			$order_item_fields[] = "date_modified = " . $value;
		
			if (isset($_SESSION["user_login"]))
			{
				$value = dbquote($_SESSION["user_login"]);
				$order_item_fields[] = "user_modified = " . $value;
			}

			$sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $item_id;
			mysql_query($sql) or dberror($sql);
		}
	}

	//send mails on change of the list of materials
	if(param("pid") and $list_of_materials_was_changed == true)
	{
		$result = track_change_in_order_items_send_mail(param("pid"), $is_hq_supplied_item);
	}
	
	return $item_id;
}



/***********************************************************************
   insert special item entered by a supplier
************************************************************************/
function  project_add_special_item_save_supplier_data($form)
{
	$list_of_materials_was_changed = false;
	$is_hq_supplied_item = false;

	$order_item_fields = array();
	$order_item_values = array();

	$order_item_fields[] = "order_item_order";
	$order_item_values[] = trim($form->value("order_item_order")) == "" ? "null" : dbquote($form->value("order_item_order"));

	$order_item_fields[] = "order_item_type";
	$order_item_values[] = trim($form->value("order_item_type")) == "" ? "null" : dbquote($form->value("order_item_type"));

	$order_item_fields[] = "order_item_text";
	$order_item_values[] = trim($form->value("order_item_text")) == "" ? "null" : dbquote($form->value("order_item_text"));

	$order_item_fields[] = "order_item_quantity";
	$order_item_values[] = trim($form->value("order_item_quantity")) == "" ? "null" : dbquote($form->value("order_item_quantity"));

	$order_item_fields[] = "order_item_supplier_address";
	$order_item_values[] = trim($form->value("order_item_supplier_address")) == "" ? "null" : dbquote($form->value("order_item_supplier_address"));

	$order_item_fields[] = "order_item_supplier_price";
	$order_item_values[] =  trim($form->value("order_item_supplier_price")) == "" ? "null" : dbquote($form->value("order_item_supplier_price"));


	if(array_key_exists("suppliers_currency", $form->items) and $form->value("suppliers_currency") !=1)
	{
		$order_item_fields[] = "order_item_price_entered_in_loc";
		$order_item_values[] = 1;
	}
	
	
	// get suppliers currency
	$supplier_currency_id = null;
	$supplier_currency_exchangerate = null;


	$supplier_currency = get_currency($form->value("suppliers_currency"));
	$supplier_currency_id = $supplier_currency["id"];
	
	if($supplier_currency["exchange_rate_furniture"] > 0)
	{
		$supplier_currency_exchangerate = $supplier_currency["exchange_rate_furniture"];
	}
	else
	{
		$supplier_currency_exchangerate = $supplier_currency["exchange_rate"];
	}
	
	
	$supplier_currency_factor = $supplier_currency["factor"];

	$order_item_fields[] = "order_item_supplier_currency";
	$order_item_values[] = $supplier_currency_id;
	$order_item_fields[] = "order_item_supplier_exchange_rate";
	$order_item_values[] = $supplier_currency_exchangerate;

	// get orders currencies (system and client)
	$oid = 0;
	if ($form->value("oid"))
	{
		$oid = $form->value("oid");
	}

	$sql = "select order_client_address, order_client_currency, order_system_currency, " .
		   "    order_client_exchange_rate, order_system_exchange_rate " .
		   "    from orders " .
		   "    where order_id = " . $oid;

	 $res = mysql_query($sql) or dberror($sql);
	 if ($row = mysql_fetch_assoc($res))
	 {
		 $supplier_price = $form->value("order_item_supplier_price");

		 // set sytsem price
		 $system_price = $supplier_price * $supplier_currency_exchangerate / $supplier_currency_factor;
		 $order_item_fields[] = "order_item_system_price";
		 $order_item_values[] = $system_price;

		 // set client's price
		 $client_currency = get_order_currency($form->value("oid"));
		 $value = $system_price / $row["order_client_exchange_rate"] * $client_currency["factor"];
		 $order_item_fields[] = "order_item_client_price";
		 $order_item_values[] = $value;
	 }

	$order_item_fields[] = "order_item_supplier_item_code";
	$order_item_values[] =  trim($form->value("order_item_supplier_item_code")) == "" ? "null" : dbquote($form->value("order_item_supplier_item_code"));
	
	$order_item_fields[] = "order_item_offer_number";
	$order_item_values[] =  trim($form->value("order_item_offer_number")) == "" ? "null" : dbquote($form->value("order_item_offer_number"));

	$order_item_fields[] = "order_item_production_time";
	$order_item_values[] =  trim($form->value("order_item_production_time")) == "" ? "null" : dbquote($form->value("order_item_production_time"));



	//logistic fields
	$order_item_fields[] = "order_item_unit_id";
	$order_item_values[] =  trim($form->value("order_item_unit_id")) == "" ? "null" : dbquote($form->value("order_item_unit_id"));

	$order_item_fields[] = "order_item_width";
	$order_item_values[] =  trim($form->value("order_item_width")) == "" ? "null" : dbquote($form->value("order_item_width"));

	$order_item_fields[] = "order_item_length";
	$order_item_values[] =  trim($form->value("order_item_length")) == "" ? "null" : dbquote($form->value("order_item_length"));

	$order_item_fields[] = "order_item_height";
	$order_item_values[] =  trim($form->value("order_item_height")) == "" ? "null" : dbquote($form->value("order_item_height"));

	$order_item_fields[] = "order_item_gross_weight";
	$order_item_values[] =  trim($form->value("order_item_gross_weight")) == "" ? "null" : dbquote($form->value("order_item_gross_weight"));

	$order_item_fields[] = "order_item_packaging_type_id";
	$order_item_values[] =  trim($form->value("order_item_packaging_type_id")) == "" ? "null" : dbquote($form->value("order_item_packaging_type_id"));

	$order_item_fields[] = "order_item_stackable";
	$order_item_values[] =  trim($form->value("order_item_stackable")) == "" ? "null" : dbquote($form->value("order_item_stackable"));


	//supplying options
	$order_item_fields[] = "order_item_only_quantity_proposal";
	$order_item_values[] = dbquote($form->value("order_item_only_quantity_proposal"));

	$order_item_fields[] = "order_item_cost_group";
	$order_item_values[] = dbquote($form->value("order_item_cost_group"));


	//check if item is a hq supplied item
	if($form->value("order_item_cost_group") == 2 
	   or $form->value("order_item_cost_group") == 6)
	{
		$is_hq_supplied_item = true;
	}

	/*
	$order_item_fields[] = "order_items_costmonitoring_group";
	$order_item_values[] = dbquote($form->value("order_items_costmonitoring_group"));
	*/

	
	$order_item_fields[] = "date_created";
	$order_item_values[] = "current_timestamp";

	$order_item_fields[] = "date_modified";
	$order_item_values[] = "current_timestamp";
			
	if (isset($_SESSION["user_login"]))
	{
		$order_item_fields[] = "user_created";
		$order_item_values[] = dbquote($_SESSION["user_login"]);

		$order_item_fields[] = "user_modified";
		$order_item_values[] = dbquote($_SESSION["user_login"]);
	}
	$sql = "insert into order_items (" . join(", ", $order_item_fields) . ") values (" . join(", ", $order_item_values) . ")";
	mysql_query($sql) or dberror($sql);

	$item_id = mysql_insert_id();


	//add tracking information to table order_item_trackings
	if ($form->value("order_item_type") == ITEM_TYPE_SPECIAL)
	{
		$result = track_change_in_order_items('Add item in list of materials', 'Quantity', $form->value("order_item_order"), $item_id, "",  $form->value("order_item_type"), $form->value("order_item_text"), 0, $form->value("order_item_quantity"));
		$list_of_materials_was_changed = true;

		$result = track_change_in_order_items('Add item in list of materials', 'Supplier Price', $form->value("order_item_order"), $item_id, "",  $form->value("order_item_type"), $form->value("order_item_text"), 0, $form->value("order_item_supplier_price"));
		$list_of_materials_was_changed = true;


		
	}
	elseif ($form->value("order_item_type") == ITEM_TYPE_COST_ESTIMATION
		or $form->value("order_item_type") == ITEM_TYPE_LOCALCONSTRUCTIONCOST)
	{
		$result = track_change_in_order_items('Add item in list of materials', 'Price', $form->value("order_item_order"), $item_id, "",  $form->value("order_item_type"), $form->value("order_item_text"), 0, $form->value("order_item_system_price"));
		$list_of_materials_was_changed = true;
	}

	// copy delivery address for new item in table order_addresses
	$sql = "select * from order_addresses ".
		   "where order_address_type = 2 ".
		   "    and order_address_order = ". param("oid") .
		   "    and (order_address_order_item = 0 ".
		   "    or order_address_order_item is null) ";

	$res2 = mysql_query($sql) or dberror($sql);
	$row2 = mysql_fetch_assoc($res2);

	$delivery_address_fields = array();
	$delivery_address_values = array();

	$delivery_address_fields[] = "order_address_order";
	$delivery_address_values[] = $form->value("order_item_order");

	$delivery_address_fields[] = "order_address_order_item";
	$delivery_address_values[] = $item_id;

	$delivery_address_fields[] = "order_address_type";
	$delivery_address_values[] = 2;

	$delivery_address_fields[] = "order_address_company";
	$delivery_address_values[] = dbquote($row2["order_address_company"]);

	$delivery_address_fields[] = "order_address_company2";
	$delivery_address_values[] = dbquote($row2["order_address_company2"]);

	$delivery_address_fields[] = "order_address_address";
	$delivery_address_values[] = dbquote($row2["order_address_address"]);

	$delivery_address_fields[] = "order_address_street";
	$delivery_address_values[] = dbquote($row2["order_address_street"]);

	$delivery_address_fields[] = "order_address_street_number";
	$delivery_address_values[] = dbquote($row2["order_address_street_number"]);

	$delivery_address_fields[] = "order_address_address2";
	$delivery_address_values[] = dbquote($row2["order_address_address2"]);

	$delivery_address_fields[] = "order_address_zip";
	$delivery_address_values[] = dbquote($row2["order_address_zip"]);

	$delivery_address_fields[] = "order_address_place";
	$delivery_address_values[] = dbquote($row2["order_address_place"]);

	$delivery_address_fields[] = "order_address_country";
	$delivery_address_values[] = dbquote($row2["order_address_country"]);

	$delivery_address_fields[] = "order_address_phone";
	$delivery_address_values[] = dbquote($row2["order_address_phone"]);

	$delivery_address_fields[] = "order_address_phone_country";
	$delivery_address_values[] = dbquote($row2["order_address_phone_country"]);

	$delivery_address_fields[] = "order_address_phone_area";
	$delivery_address_values[] = dbquote($row2["order_address_phone_area"]);

	$delivery_address_fields[] = "order_address_phone_number";
	$delivery_address_values[] = dbquote($row2["order_address_phone_number"]);

	$delivery_address_fields[] = "order_address_mobile_phone";
	$delivery_address_values[] = dbquote($row2["order_address_mobile_phone"]);

	$delivery_address_fields[] = "order_address_email";
	$delivery_address_values[] = dbquote($row2["order_address_email"]);

	$delivery_address_fields[] = "order_address_parent";
	$delivery_address_values[] = dbquote($row2["order_address_parent"]);

	$delivery_address_fields[] = "order_address_contact";
	$delivery_address_values[] = dbquote($row2["order_address_contact"]);

	$delivery_address_fields[] = "date_created";
	$delivery_address_values[] = "current_timestamp";

	$delivery_address_fields[] = "date_modified";
	$delivery_address_values[] = "current_timestamp";

	if (isset($_SESSION["user_login"]))
	{
		$delivery_address_fields[] = "user_created";
		$delivery_address_values[] = dbquote($_SESSION["user_login"]);

		$delivery_address_fields[] = "user_modified";
		$delivery_address_values[] = dbquote($_SESSION["user_login"]);
	}

	$sql = "insert into order_addresses (" . join(", ", $delivery_address_fields) . ") values (" . join(", ", $delivery_address_values) . ")";
	mysql_query($sql) or dberror($sql);

	//send mails on change of the list of materials
	if(param("pid") and $list_of_materials_was_changed == true)
	{
		$result = track_change_in_order_items_send_mail(param("pid"), $is_hq_supplied_item);
	}
}

/***********************************************************************
   save suppliers currency and pricing data and
   copy item information into all other item records of the same order
   belonging to the same supplier
************************************************************************/
function  project_edit_order_item_save($form)
{
	$list_of_materials_was_changed = false;
	$is_hq_supplied_item = false;
		
	//order_item_tracking
	if ($form->value("order_item_type") == ITEM_TYPE_STANDARD 
		or $form->value("order_item_type") == ITEM_TYPE_SERVICES)
	{
		$new_value = $form->value("order_item_quantity");
					
		$sql = "select order_item_item, order_item_order, order_item_quantity, " . 
			   " order_item_type, order_item_text, order_item_cost_group, order_item_client_price_freezed " . 
			   " from order_items " . 
			   " where order_item_id = " . dbquote($form->value("order_item_id"));

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{

			if($row["order_item_client_price_freezed"] > 0 and $row["order_item_quantity"] != $new_value)
			{
				$result = track_change_in_order_items('Update item in list of materials', 'Quantity', $row["order_item_order"], $form->value("order_item_id"), $row["order_item_item"], $row["order_item_type"],  $row["order_item_text"], $row["order_item_quantity"], $new_value);
				$list_of_materials_was_changed = true;

				//check if item is a hq supplied item
				if($row["order_item_cost_group"] == 2 
				   or $row["order_item_cost_group"] == 6)
				{
					$is_hq_supplied_item = true;
				}

			}
		}
	}
	elseif ($form->value("order_item_type") == ITEM_TYPE_SPECIAL)
	{
		$new_value = $form->value("order_item_quantity");
		$new_value2 = $form->value("order_item_supplier_price");
					
		$sql = "select order_item_item, order_item_order, order_item_quantity, order_item_supplier_price, " . 
			   " order_item_type, order_item_text, order_item_cost_group, order_item_client_price_freezed " . 
			   " from order_items " . 
			   " where order_item_id = " . dbquote($form->value("order_item_id"));

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{

			if($row["order_item_client_price_freezed"] > 0 and $row["order_item_quantity"] != $new_value)
			{
				$result = track_change_in_order_items('Update item in list of materials', 'Quantity', $row["order_item_order"], $form->value("order_item_id"), $row["order_item_item"], $row["order_item_type"],  $row["order_item_text"], $row["order_item_quantity"], $new_value);
				$list_of_materials_was_changed = true;

				//check if item is a hq supplied item
				if($row["order_item_cost_group"] == 2 
				   or $row["order_item_cost_group"] == 6)
				{
					$is_hq_supplied_item = true;
				}

			}

			if($row["order_item_supplier_price"] != $new_value2)
			{
				$result = track_change_in_order_items('Update item in list of materials', 'Supplier Price', $row["order_item_order"], $form->value("order_item_id"), $row["order_item_item"], $row["order_item_type"],  $row["order_item_text"], $row["order_item_supplier_price"], $new_value2);
				$list_of_materials_was_changed = true;

			}
		}
	}
	else if ($form->value("order_item_type") == ITEM_TYPE_COST_ESTIMATION 
		OR $form->value("order_item_type") == ITEM_TYPE_LOCALCONSTRUCTIONCOST)
	{
		
		if(isset($form->items["order_item_system_price"]))
		{
			$new_value = $form->value("order_item_system_price");
					
			$sql = "select order_item_item, order_item_order, order_item_system_price, " . 
				   " order_item_type, order_item_text, order_item_cost_group, order_item_client_price_freezed " . 
				   " from order_items " . 
				   " where order_item_id = " . dbquote($form->value("order_item_id"));

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{

				if($row["order_item_cost_group"] == 6 
					and $row["order_item_client_price_freezed"] > 0 
					and $row["order_item_system_price"] != $new_value)
				{
					$result = track_change_in_order_items('Update item in list of materials', 'Price', $row["order_item_order"], $form->value("order_item_id"), "", $row["order_item_type"],  $row["order_item_text"], $row["order_item_system_price"], $new_value);
					$list_of_materials_was_changed = true;

					//check if item is a hq supplied item
					if($row["order_item_cost_group"] == 2 
					   or $row["order_item_cost_group"] == 6)
					{
						$is_hq_supplied_item = true;
					}

				}
			}
		}
		elseif(isset($form->items["order_item_client_price"]))
		{

			$new_value = $form->value("order_item_client_price");
					
			$sql = "select order_item_item, order_item_order, order_item_client_price, " . 
				   " order_item_type, order_item_text, order_item_cost_group " . 
				   " from order_items " . 
				   " where order_item_id = " . dbquote($form->value("order_item_id"));

			$res = mysql_query($sql) or dberror($sql);
			if (($row["order_item_cost_group"] == 2 or $row["order_item_cost_group"] == 6) and
				$row = mysql_fetch_assoc($res))
			{

				if($row["order_item_client_price"] != $new_value)
				{
					$result = track_change_in_order_items('Update item in list of materials', 'Price', $row["order_item_order"], $form->value("order_item_id"), "", $row["order_item_type"],  $row["order_item_text"], $row["order_item_client_price"], $new_value);
					$list_of_materials_was_changed = true;

					//check if item is a hq supplied item
					if($row["order_item_cost_group"] == 2 
					   or $row["order_item_cost_group"] == 6)
					{
						$is_hq_supplied_item = true;
					}

				}
			}
		}
	}	



	//update order_item
	
	$order_item_fields = array();

	$value =  trim($form->value("order_item_text")) == "" ? "null" : dbquote($form->value("order_item_text"));
	$order_item_fields[] = "order_item_text = " . $value;

	if(array_key_exists("currency", $form->items) and $form->value("currency") !=1)
	{
		$order_item_fields[] = "order_item_price_entered_in_loc = 1";
	}
	elseif(array_key_exists("suppliers_currency", $form->items) and $form->value("suppliers_currency") !=1)
	{
		$order_item_fields[] = "order_item_price_entered_in_loc = 1";
	}
	else
	{
		$order_item_fields[] = "order_item_price_entered_in_loc = 0";
	}
	
	
	if ($form->value("order_item_type") == ITEM_TYPE_STANDARD 
		or $form->value("order_item_type") == ITEM_TYPE_SERVICES 
		or $form->value("order_item_type") == ITEM_TYPE_SPECIAL)
	{

		$value =  trim($form->value("order_item_quantity")) == "" ? "null" : dbquote($form->value("order_item_quantity"));
		$order_item_fields[] = "order_item_quantity = " . str_replace(",", ".", $value);

		// get suppliers' currency data
		$supplier_currency_id = null;
		$supplier_currency_exchangerate = null;

		
		if ($form->value("order_item_currency"))
		{
			$currency = get_currency($form->value("order_item_currency"));
			$supplier_currency_id = $currency["id"];
			$supplier_currency_exchangerate = $currency["exchange_rate"];
		}
		else
		{
			$currency = get_address_currency($form->value("order_item_supplier_address"));
			$supplier_currency_id = $currency["id"];
			$supplier_currency_exchangerate = $currency["exchange_rate"];
		}
		
		$order_item_fields[] = "order_item_category = " . dbquote($form->value("order_item_category"));
		$order_item_fields[] = "order_item_supplier_currency = " . $supplier_currency_id;

		$order_item_fields[] = "order_item_supplier_exchange_rate = " . $supplier_currency_exchangerate;

		if ($form->value("order_item_type") == ITEM_TYPE_STANDARD or $form->value("order_item_type") == ITEM_TYPE_SERVICES)
		{
			$value =  trim($form->value("order_item_client_price")) == "" ? "null" : dbquote($form->value("order_item_client_price"));
			$order_item_fields[] = "order_item_client_price = " . $value;


			// update supplier's price
			$sql = "select * ".
				   "from suppliers ".
				   "where supplier_address = " . $form->value("order_item_supplier_address") .
				   "    and supplier_item = " . $form->value("order_item_item");

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$order_item_fields[] = "order_item_supplier_price = " . $row["supplier_item_price"];
			}

			
			if(array_key_exists("is_replacement", $form->items) 
				and $form->value("is_replacement") == 1) //standard item is a replacment order item
			{
				$value =  trim($form->value("order_item_client_price")) == "" ? "null" : dbquote($form->value("order_item_client_price"));
				$order_item_fields[] = "order_item_client_price = " . $value;
				
				
				$order_currency = get_order_currency($form->value("oid"));
				$value = $form->value("order_item_client_price") * $order_currency["exchange_rate"] / $order_currency["factor"];
				$order_item_fields[] = "order_item_system_price = " . $value;
			
			}
		}
		else if ($form->value("order_item_type") == ITEM_TYPE_SPECIAL)
		{
			
			if(isset($form->items["order_item_cost_group"]))
			{
				$value = $form->value("order_item_cost_group");
				$order_item_fields[] = "order_item_cost_group = " . $value;

				/*
				$value = $form->value("order_items_costmonitoring_group");
				$order_item_fields[] = "order_items_costmonitoring_group = " . $value;
				*/

				//check if item is a hq supplied item
				if($form->value("order_item_cost_group") == 2 
				   or $form->value("order_item_cost_group") == 6)
				{
					$is_hq_supplied_item = true;
				}
			}
			$value =  trim($form->value("order_item_system_price")) == "" ? "null" : dbquote($form->value("order_item_system_price"));
			$order_item_fields[] = "order_item_system_price = " . $value;

			$order_currency = get_order_currency($form->value("oid"));
			$value = $form->value("order_item_system_price") / $order_currency["exchange_rate"] * $order_currency["factor"];
			$order_item_fields[] = "order_item_client_price = " . $value;

			$value =  trim($form->value("order_item_supplier_price")) == "" ? "null" : dbquote($form->value("order_item_supplier_price"));
			$order_item_fields[] = "order_item_supplier_price = " . $value;


			/*
			$value =  trim($form->value("order_item_supplier_item_code")) == "" ? "null" : dbquote($form->value("order_item_supplier_item_code"));
			$order_item_fields[] = "order_item_supplier_item_code = " . $value;

			$value =  trim($form->value("order_item_offer_number")) == "" ? "null" : dbquote($form->value("order_item_offer_number"));
			$order_item_fields[] = "order_item_offer_number = " . $value;

			$value =  trim($form->value("order_item_production_time")) == "" ? "null" : dbquote($form->value("order_item_production_time"));
			$order_item_fields[] = "order_item_production_time = " . $value;
			*/

		}

		$value =  trim($form->value("order_item_cost_unit_number")) == "" ? "null" : dbquote($form->value("order_item_cost_unit_number"));
		$order_item_fields[] = "order_item_cost_unit_number = " . $value;

		$value =  trim($form->value("order_item_po_number")) == "" ? "null" : dbquote($form->value("order_item_po_number"));
		$order_item_fields[] = "order_item_po_number = " . $value;

		$value =  trim($form->value("order_item_supplier_address")) == "" ? "null" : dbquote($form->value("order_item_supplier_address"));
		$order_item_fields[] = "order_item_supplier_address = " . $value;


		if ($form->value("order_item_type") == ITEM_TYPE_STANDARD 
			or $form->value("order_item_type") == ITEM_TYPE_SPECIAL)
		{
			if(array_key_exists("order_item_preferred_arrival_date", $form->items))
			{
				$value =  trim(from_system_date($form->value("order_item_preferred_arrival_date"))) == "" ? "null" : dbquote(from_system_date($form->value("order_item_preferred_arrival_date")));
				$order_item_fields[] = "order_item_preferred_arrival_date = " . $value;
			}
			
			$value =  trim($form->value("order_item_transportation")) == "" ? "null" : dbquote($form->value("order_item_transportation"));
			$order_item_fields[] = "order_item_transportation = " . $value;

			$value =  trim($form->value("order_item_forwarder_address")) == "" ? "null" : dbquote($form->value("order_item_forwarder_address"));
			$order_item_fields[] = "order_item_forwarder_address = " . $value;

			$value =  trim($form->value("order_item_staff_for_discharge")) == "" ? "null" :     dbquote($form->value("order_item_staff_for_discharge"));
			$order_item_fields[] = "order_item_staff_for_discharge = " . $value;

			$value =  trim($form->value("order_item_no_offer_required")) == "" ? "null" : dbquote($form->value("order_item_no_offer_required"));
			$order_item_fields[] = "order_item_no_offer_required = " . $value;

			$value =  trim($form->value("order_item_only_quantity_proposal")) == "" ? "null" : dbquote($form->value("order_item_only_quantity_proposal"));
			$order_item_fields[] = "order_item_only_quantity_proposal = " . $value;

			$value =  trim($form->value("order_item_not_in_budget")) == "" ? "null" : dbquote($form->value("order_item_not_in_budget"));
			$order_item_fields[] = "order_item_not_in_budget = " . $value;

			if(isset($form->items["order_item_exclude_from_ps"]))
			{
				$value =  trim($form->value("order_item_exclude_from_ps")) == "" ? "null" : dbquote($form->value("order_item_exclude_from_ps"));
				$order_item_fields[] = "order_item_exclude_from_ps = " . $value;
			}


			//logistic fields
			$value =  trim($form->value("order_item_unit_id")) == "" ? "null" : dbquote($form->value("order_item_unit_id"));
			$order_item_fields[] = "order_item_unit_id = " . $value;

			$value =  trim($form->value("order_item_width")) == "" ? "null" : dbquote($form->value("order_item_width"));
			$order_item_fields[] = "order_item_width = " . $value;

			$value =  trim($form->value("order_item_length")) == "" ? "null" : dbquote($form->value("order_item_length"));
			$order_item_fields[] = "order_item_length = " . $value;

			$value =  trim($form->value("order_item_height")) == "" ? "null" : dbquote($form->value("order_item_height"));
			$order_item_fields[] = "order_item_height = " . $value;

			$value =  trim($form->value("order_item_gross_weight")) == "" ? "null" : dbquote($form->value("order_item_gross_weight"));
			$order_item_fields[] = "order_item_gross_weight = " . $value;

			$value =  trim($form->value("order_item_packaging_type_id")) == "" ? "null" : dbquote($form->value("order_item_packaging_type_id"));
			$order_item_fields[] = "order_item_packaging_type_id = " . $value;

			$value =  trim($form->value("order_item_stackable")) == "" ? "null" : dbquote($form->value("order_item_stackable"));
			$order_item_fields[] = "order_item_stackable = " . $value;
		}

	}
	else if ($form->value("order_item_type") == ITEM_TYPE_COST_ESTIMATION 
		OR $form->value("order_item_type") == ITEM_TYPE_LOCALCONSTRUCTIONCOST)
	{

		if(isset($form->items["order_item_cost_group"]))
		{
			$value = $form->value("order_item_cost_group");
			$order_item_fields[] = "order_item_cost_group = " . $value;

			//check if item is a hq supplied item
			if($form->value("order_item_cost_group") == 2 
			   or $form->value("order_item_cost_group") == 6)
			{
				$is_hq_supplied_item = true;
			}

		}
		
		if(isset($form->items["order_item_supplier_freetext"]))
		{
			$value =  trim($form->value("order_item_supplier_freetext")) == "" ? "null" : dbquote($form->value("order_item_supplier_freetext"));
			$order_item_fields[] = "order_item_supplier_freetext = " . $value;
		}

		if(isset($form->items["order_item_system_price"]))
		{
			$value =  trim($form->value("order_item_system_price")) == "" ? "null" : dbquote($form->value("order_item_system_price"));
			$order_item_fields[] = "order_item_system_price = " . $value;

			$order_currency = get_order_currency($form->value("oid"));
			$value = $form->value("order_item_system_price") / $order_currency["exchange_rate"] * $order_currency["factor"];
			$order_item_fields[] = "order_item_client_price = " . $value;
		}
		elseif(isset($form->items["order_item_client_price"]))
		{
			$value =  trim($form->value("order_item_client_price")) == "" ? "null" : dbquote($form->value("order_item_client_price"));
			$order_item_fields[] = "order_item_client_price = " . $value;

			$order_currency = get_order_currency($form->value("oid"));
			$value = $order_currency["exchange_rate"] * $form->value("order_item_client_price") / $order_currency["factor"];
			$order_item_fields[] = "order_item_system_price = " . $value;
		}

	}
	
	$value = "current_timestamp";
	$order_item_fields[] = "date_modified = " . $value;

	if (isset($_SESSION["user_login"]))
	{
		$value = dbquote($_SESSION["user_login"]);
		$order_item_fields[] = "user_modified = " . $value;
	}

	$sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $form->value("order_item_id");

	mysql_query($sql) or dberror($sql);

	if ($form->value("order_item_type") == ITEM_TYPE_STANDARD 
		or $form->value("order_item_type") == ITEM_TYPE_SPECIAL 
		or $form->value("order_item_type") == ITEM_TYPE_SERVICES)
	{
		// copy values into all aother order_items
		$sql_order_item = "select * ".
						  "from order_items ".
						  "where (order_item_not_in_budget is null or order_item_not_in_budget = 0) ".
						  "    and order_item_order = " . param("oid") . " " .
						  "    and order_item_supplier_address = ". $form->value("order_item_supplier_address").
						  "    and (order_item_type = " . ITEM_TYPE_STANDARD . " ".
						  "    or order_item_type = " . ITEM_TYPE_SERVICES . " ".
						  "    or order_item_type = " . ITEM_TYPE_SPECIAL . ")";

		$res = mysql_query($sql_order_item) or dberror($sql_order_item);
		while ($row = mysql_fetch_assoc($res))
		{
			// update all empty P.O. Numbers
			$value = trim($form->value("order_item_po_number")) == "" ? "null" : dbquote($form->value("order_item_po_number"));

			if (!$row["order_item_po_number"])
			{
				$sql = "update order_items set order_item_po_number =" . $value . " ".
				   "where order_item_id = " . $row["order_item_id"];
				mysql_query($sql) or dberror($sql);
			}


			// update all empty cost unit numbers
			$value = trim($form->value("order_item_cost_unit_number")) == "" ? "null" : dbquote($form->value("order_item_cost_unit_number"));

			if (!$row["order_item_cost_unit_number"])
			{
				$sql = "update order_items set order_item_cost_unit_number =" . $value . " ".
				   "where order_item_id = " . $row["order_item_id"];
				mysql_query($sql) or dberror($sql);
			}

			if ($form->value("order_item_type") == ITEM_TYPE_STANDARD 
				or $form->value("order_item_type") == ITEM_TYPE_SPECIAL)
			{
				// preferred arrival date
				if(array_key_exists("order_item_preferred_arrival_date", $form->items))
				{
					$value = trim(from_system_date($form->value("order_item_preferred_arrival_date"))) == "" ? "null" :     dbquote(from_system_date($form->value("order_item_preferred_arrival_date")));

					$sql = "update order_items set order_item_preferred_arrival_date =" . $value . " ".
							   "where order_item_id = " . $row["order_item_id"];
						mysql_query($sql) or dberror($sql);
				}


				if (!$row["order_item_forwarder_address"])
				{
					$sql = "update order_items set order_item_forwarder_address =" . $value . " ".
						   "where order_item_id = " . $row["order_item_id"];
					mysql_query($sql) or dberror($sql);
				}
				
				// update all empty forwarder addresses
				$value = trim($form->value("order_item_forwarder_address")) == "" ? "null" :     dbquote($form->value("order_item_forwarder_address"));

				if (!$row["order_item_forwarder_address"])
				{
					$sql = "update order_items set order_item_forwarder_address =" . $value . " ".
						   "where order_item_id = " . $row["order_item_id"];
					mysql_query($sql) or dberror($sql);
				}
	
				// update all transportation codes
				$value = trim($form->value("order_item_transportation")) == "" ? "null" : dbquote($form->value("order_item_transportation"));

				if (!$row["order_item_transportation"])
				{
					$sql = "update order_items set order_item_transportation =" . $value . " ".
					   "where order_item_id = " . $row["order_item_id"];
					mysql_query($sql) or dberror($sql);
				}
			}
		}
	}


	//send mails on change of the list of materials
	if(param("pid") and $list_of_materials_was_changed == true)
	{
		$result = track_change_in_order_items_send_mail(param("pid"), $is_hq_supplied_item);
	}

}


/***********************************************************************
   save suppliers entries for an item in the list of materials
************************************************************************/
function  project_edit_order_item_save_supplier_data($form)
{

        $order_item_fields = array();

        $value =  trim($form->value("order_item_text")) == "" ? "null" : dbquote($form->value("order_item_text"));
        $order_item_fields[] = "order_item_text = " . $value;

        $value =  trim($form->value("order_item_quantity")) == "" ? "null" : dbquote($form->value("order_item_quantity"));
        $order_item_fields[] = "order_item_quantity = " . str_replace(",", ".", $value);


        if ($form->value("order_item_type") == ITEM_TYPE_STANDARD or $form->value("order_item_type") == ITEM_TYPE_SERVICES)
        {
            $value =  trim($form->value("order_item_client_price")) == "" ? "null" : dbquote($form->value("order_item_client_price"));
            $order_item_fields[] = "order_item_client_price = " . $value;

            // update supplier's price
            $sql = "select * ".
                   "from suppliers ".
                   "where supplier_address = " . $form->value("order_item_supplier_address") .
                   "    and supplier_item = " . $form->value("order_item_item");

            $res = mysql_query($sql) or dberror($sql);
            if ($row = mysql_fetch_assoc($res))
            {
                $order_item_fields[] = "order_item_supplier_price = " . dbquote($row["supplier_item_price"]);
            }
        }
        else if ($form->value("order_item_type") == ITEM_TYPE_SPECIAL)
        {

            $value = $form->value("order_item_supplier_price");
            $order_item_fields[] = "order_item_supplier_price = " . dbquote($value);

            $supplier_currency_id = null;
            $supplier_currency_exchangerate = null;


            if ($form->value("order_item_currency"))
            {
                $currency = get_currency($form->value("order_item_currency"));
                $supplier_currency_id = $currency["id"];
                $supplier_currency_exchangerate = $currency["exchange_rate"];
                $supplier_currency_factor = $currency["factor"];
            }
            else
            {
				$client_currency = get_order_currency($form->value("oid"));
                $supplier_currency_id = $currency["id"];
                $supplier_currency_exchangerate = $currency["exchange_rate"];
                $supplier_currency_factor = $currency["factor"];
            }

            $order_item_fields[] = "order_item_supplier_currency = " . $supplier_currency_id;
            $order_item_fields[] = "order_item_supplier_exchange_rate = " . $supplier_currency_exchangerate;

            // get orders currencies (system and client)
            $oid = 0;
            if ($form->value("oid"))
            {
                $oid = $form->value("oid");
            }

            $sql = "select order_client_address, order_client_currency, order_system_currency, " .
                  "    order_client_exchange_rate, order_system_exchange_rate " .
                  "    from orders " .
                  "    where order_id = " . $oid;

            $res = mysql_query($sql) or dberror($sql);
            if ($row = mysql_fetch_assoc($res))
            {
                $supplier_price = $form->value("order_item_supplier_price");

                // set sytsem price
                $system_price = $supplier_price * $supplier_currency_exchangerate / $supplier_currency_factor;
                $order_item_fields[] = "order_item_system_price = " . $system_price;
       
				$client_currency = get_order_currency($form->value("oid"));
                // set client's price
                $value = $system_price / $row["order_client_exchange_rate"] * $client_currency["factor"];
                $order_item_fields[] = "order_item_client_price = " . $value;

            }


			//logistic information
			$value =  trim($form->value("order_item_unit_id")) == "" ? "null" : dbquote($form->value("order_item_unit_id"));
            $order_item_fields[] = "order_item_unit_id = " . $value;

			$value =  trim($form->value("order_item_width")) == "" ? "null" : dbquote($form->value("order_item_width"));
            $order_item_fields[] = "order_item_width = " . $value;

			$value =  trim($form->value("order_item_length")) == "" ? "null" : dbquote($form->value("order_item_length"));
            $order_item_fields[] = "order_item_length = " . $value;

			$value =  trim($form->value("order_item_height")) == "" ? "null" : dbquote($form->value("order_item_height"));
            $order_item_fields[] = "order_item_height = " . $value;

			$value =  trim($form->value("order_item_gross_weight")) == "" ? "null" : dbquote($form->value("order_item_gross_weight"));
            $order_item_fields[] = "order_item_gross_weight = " . $value;

			$value =  trim($form->value("order_item_packaging_type_id")) == "" ? "null" : dbquote($form->value("order_item_packaging_type_id"));
            $order_item_fields[] = "order_item_packaging_type_id = " . $value;

			$value =  trim($form->value("order_item_stackable")) == "" ? "null" : dbquote($form->value("order_item_stackable"));
            $order_item_fields[] = "order_item_stackable = " . $value;

            
            $value =  trim($form->value("order_item_supplier_item_code")) == "" ? "null" : dbquote($form->value("order_item_supplier_item_code"));
            $order_item_fields[] = "order_item_supplier_item_code = " . $value;

            $value =  trim($form->value("order_item_offer_number")) == "" ? "null" : dbquote($form->value("order_item_offer_number"));
            $order_item_fields[] = "order_item_offer_number = " . $value;

            $value =  trim($form->value("order_item_production_time")) == "" ? "null" : dbquote($form->value("order_item_production_time"));
            $order_item_fields[] = "order_item_production_time = " . $value;
        }


        $value = "current_timestamp";
        $order_item_fields[] = "date_modified = " . $value;
    
        if (isset($_SESSION["user_login"]))
        {
            $value = dbquote($_SESSION["user_login"]);
            $order_item_fields[] = "user_modified = " . $value;
        }

        $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $form->value("order_item_id");
        mysql_query($sql) or dberror($sql);


}


/********************************************************************
    add standard budget positions
*********************************************************************/
function add_standard_budget_positions($type, $order_id, $legal_type = 0)
{

    if($legal_type == 0)
	{
		$sql = "select exclusion_notification_id, exclusion_notification_description ".
			   "from exclusion_notifications ".
			   "where exclusion_notification_type_id = " . $type . " " .
			   "order by exclusion_notification_code";

	}
	elseif($legal_type == 1)
	{
		$sql = "select exclusion_notification_id, exclusion_notification_description ".
			   "from exclusion_notifications ".
			   "where exclusion_notification_corporate = 1 and exclusion_notification_type_id = " . $type . " " .
			   "order by exclusion_notification_code";

	}
	else
	{
		$sql = "select exclusion_notification_id, exclusion_notification_description ".
			   "from exclusion_notifications ".
			   "where exclusion_notification_non_corporate = 1 and exclusion_notification_type_id = " . $type . " " .
			   "order by exclusion_notification_code";
	}

  
    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $order_item_fields = array();
        $order_item_values = array();

        $order_item_fields[] = "order_item_order";
        $order_item_values[] = $order_id;

        $order_item_fields[] = "order_item_item";
        $order_item_values[] = 0;

        $order_item_fields[] = "order_item_text";
        $order_item_values[] = dbquote($row["exclusion_notification_description"]);

        $order_item_fields[] = "order_item_type";
        $order_item_values[] = $type;

        $order_item_fields[] = "date_created";
        $order_item_values[] = "current_timestamp";

        $order_item_fields[] = "date_modified";
        $order_item_values[] = "current_timestamp";
                
        if (isset($_SESSION["user_login"]))
        {
            $order_item_fields[] = "user_created";
            $order_item_values[] = dbquote($_SESSION["user_login"]);

            $order_item_fields[] = "user_modified";
            $order_item_values[] = dbquote($_SESSION["user_login"]);
        }
        $sql = "insert into order_items (" . join(", ", $order_item_fields) . ") values (" . join(", ", $order_item_values) . ")";
        mysql_query($sql) or dberror($sql);
    }
}


/********************************************************************
    delete all budget positions
*********************************************************************/
function delete_all_budget_positions($type, $order_id)
{

    $sql = "delete ".
           "from order_items ".
           "where order_item_type = " . $type . 
           "   and order_item_order = " . $order_id;
    mysql_query($sql) or dberror($sql);
}

/***********************************************************************
   insert dates from order to supplier
************************************************************************/
function  append_order_dates($order_id, $address_id)
{
    $sql = "select order_item_id ".
           "from order_items ".
           "where order_item_order = " . $order_id .
           "    and order_item_supplier_address = " . $address_id .
           "    and order_item_po_number <> ''";

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        insert_date_item($row["order_item_id"], date("Y-m-d"), 'ORSU');
    }
    
}


/***********************************************************************
   insert dates from request for delivery to forwarder
************************************************************************/
function  append_request_for_delivery_dates($order_id, $address_id)
{
    $sql = "select order_item_id ".
           "from order_items ".
           "where order_item_order = " . $order_id .
           "    and order_item_forwarder_address = " . $address_id .
           "    and order_item_po_number <>''";

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        // check if ready for pick up date was entered
        $ready4pickup = 0;

        $sql_dates = "select date_id ".
                     "from dates ".
                     "left join date_types on date_type = date_type_id ".
                     "where date_order_item = ". $row["order_item_id"] .
                     "    and date_type_code = 'EXRP'";

        $res2= mysql_query($sql_dates) or dberror($sql_dates);
  
        if ($row2 = mysql_fetch_assoc($res2))
        {
            $ready4pickup = 1;
        }

        if ($ready4pickup == 1)
        {
            insert_date_item($row["order_item_id"], date("Y-m-d"), 'ORFW');
        }
    }
    
}

/***********************************************************************
   update item's delivery address
************************************************************************/
function  project_update_delivery_address($form)
{
    // insert or update delivery addresses
    $sql = "select order_address_id ".
           "from order_addresses ".
           "where order_address_order = " . $form->value("order_item_order") .
           "    and order_address_order_item = " . $form->value("order_item_id").
           "    and order_address_type = 2";

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res)) 
    {
        // update delivery address in table order_addresses
        $delivery_address_fields = array();

        $value = trim($form->value("delivery_address_company")) == "" ? "null" : dbquote($form->value("delivery_address_company"));
        $delivery_address_fields[] = "order_address_company = " . $value;

         $value = trim($form->value("delivery_address_company2")) == "" ? "null" : dbquote($form->value("delivery_address_company2"));
        $delivery_address_fields[] = "order_address_company2 = " . $value;

        $value = trim($form->value("delivery_address_address")) == "" ? "null" : dbquote($form->value("delivery_address_address"));
        $delivery_address_fields[] = "order_address_address = " . $value;


		if(array_key_exists('delivery_street', $form->items))
		{
			foreach($form->items['delivery_street']['fields'] as $key=>$fieldname)
			{
				$value = dbquote($form->items['delivery_street']['values'][$key]);
				$delivery_address_fields[] = $fieldname . " = " . $value;

			}
		}

        $value = trim($form->value("delivery_address_address2")) == "" ? "null" : dbquote($form->value("delivery_address_address2"));
        $delivery_address_fields[] = "order_address_address2 = " . $value;

        $value = trim($form->value("delivery_address_zip")) == "" ? "null" : dbquote($form->value("delivery_address_zip"));
        $delivery_address_fields[] = "order_address_zip = " . $value;

        $value = dbquote($form->value("delivery_address_place_id"));
        $delivery_address_fields[] = "order_address_place_id = " . $value;

		$value = trim($form->value("delivery_address_place")) == "" ? "null" : dbquote($form->value("delivery_address_place"));
        $delivery_address_fields[] = "order_address_place = " . $value;

        $value = trim($form->value("delivery_address_country")) == "" ? "null" : dbquote($form->value("delivery_address_country"));
        $delivery_address_fields[] = "order_address_country = " . $value;

        $value = trim($form->value("delivery_address_phone")) == "" ? "null" : dbquote($form->value("delivery_address_phone"));
        $delivery_address_fields[] = "order_address_phone = " . $value;

		if(array_key_exists('delivery_phone_number', $form->items))
		{
			foreach($form->items['delivery_phone_number']['fields'] as $key=>$fieldname)
			{
				$value = dbquote($form->items['delivery_phone_number']['values'][$key]);
				$delivery_address_fields[] = $fieldname . " = " . $value;

			}
		}

        $value = trim($form->value("delivery_address_mobile_phone")) == "" ? "null" : dbquote($form->value("delivery_address_mobile_phone"));
        $delivery_address_fields[] = "order_address_mobile_phone = " . $value;

		if(array_key_exists('delivery_mobile_phone_number', $form->items))
		{
			foreach($form->items['delivery_mobile_phone_number']['fields'] as $key=>$fieldname)
			{
				$value = dbquote($form->items['delivery_mobile_phone_number']['values'][$key]);
				$delivery_address_fields[] = $fieldname . " = " . $value;

			}
		}

        $value = trim($form->value("delivery_address_email")) == "" ? "null" : dbquote($form->value("delivery_address_email"));
        $delivery_address_fields[] = "order_address_email = " . $value;

        $value = $form->value("order_client_address");
        $delivery_address_fields[] = "order_address_parent = " . $value;

        $value = trim($form->value("delivery_address_contact")) == "" ? "null" : dbquote($form->value("delivery_address_contact"));
        $delivery_address_fields[] = "order_address_contact = " . $value;

        $value = "current_timestamp";
        $delivery_address_fields[] = "date_modified = " . $value;

        if (isset($_SESSION["user_login"]))
        {
            $value = dbquote($_SESSION["user_login"]);
            $delivery_address_fields[] = "user_modified = " . $value;
        }

        $item_filter = "";
		if($form->value("same_supplier") == 1) {
			//get items of the same supplier
			$item_ids = array();
			$sql = "select DISTINCT order_item_id from order_items " . 
				   "where order_item_order = " .  $form->value("order_item_order") . 
				   " and order_item_supplier_address = " . dbquote($form->value("order_item_supplier_address"));

			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{
				$item_ids[$row["order_item_id"]] = $row["order_item_id"];
			}

			if(count($item_ids) > 0) {
				$item_filter = " or order_address_order_item IN (" . implode(',', $item_ids) . ") ";
			}

	
		}
		if($form->value("same_forwarder") == 1) {
			$item_ids = array();
			$sql = "select DISTINCT order_item_id from order_items " . 
				   "where order_item_order = " .  $form->value("order_item_order") . 
				   " and order_item_forwarder_address = " . dbquote($form->value("order_item_forwarder_address"));

			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{
				$item_ids[$row["order_item_id"]] = $row["order_item_id"];
			}

			if(count($item_ids) > 0) {
				$item_filter = " or order_address_order_item IN (" . implode(',', $item_ids) . ") ";
			}
		}
		
		$sql = "update order_addresses set " . join(", ", $delivery_address_fields) . 
			   " where order_address_type = 2 ".
			   "    and order_address_order = " . $form->value("order_item_order") .
			   "    and (order_address_order_item = " . $form->value("order_item_id") . $item_filter . ")";
		
		mysql_query($sql) or dberror($sql);
    }
    else // insert new
    {
        // insert delivery addresses into table order_addresses for all items
        // not having a delivery address and beloging to the same order

        $sql_order_items = "select order_item_id, order_item_order ".
                           "from order_items ".
                           "where order_item_order = " . $form->value("order_item_order") .
                           "    and order_item_type <= ". ITEM_TYPE_SPECIAL;


        $res = mysql_query($sql_order_items) or dberror($sql_order_items);
        while ($row = mysql_fetch_assoc($res)) 
        {
            // check if record is already there
            $sql = "select order_address_id ".
                   "from order_addresses ".
                   "where order_address_order = " . $row["order_item_order"] .
                   "    and order_address_order_item = " . $row["order_item_id"].
                   "    and order_address_type = 2";

            $res1 = mysql_query($sql) or dberror($sql);
            if ($row1 = mysql_fetch_assoc($res1)) 
            {
                //nothing
            }
            else
            {
                // insert new record
        
                $delivery_address_fields = array();
                $delivery_address_values = array();

        
                $delivery_address_fields[] = "order_address_order";
                $delivery_address_values[] = $row["order_item_order"];

                $delivery_address_fields[] = "order_address_order_item";
                $delivery_address_values[] = $row["order_item_id"];

                $delivery_address_fields[] = "order_address_type";
                $delivery_address_values[] = 2;

                $delivery_address_fields[] = "order_address_company";
                $delivery_address_values[] = trim($form->value("delivery_address_company")) == "" ? "null" : dbquote($form->value("delivery_address_company"));

                $delivery_address_fields[] = "order_address_company2";
                $delivery_address_values[] = trim($form->value("delivery_address_company2")) == "" ? "null" : dbquote($form->value("delivery_address_company2"));

                $delivery_address_fields[] = "order_address_address";
                $delivery_address_values[] = trim($form->value("delivery_address_address")) == "" ? "null" : dbquote($form->value("delivery_address_address"));


				if(array_key_exists('delivery_street', $form->items))
				{
					foreach($form->items['delivery_street']['fields'] as $key=>$fieldname)
					{
						$value = dbquote($form->items['delivery_street']['values'][$key]);

						$delivery_address_fields[] = $fieldname;
						$delivery_address_values[] = $value;

					}
				}

                $delivery_address_fields[] = "order_address_address2";
                $delivery_address_values[] = trim($form->value("delivery_address_address2")) == "" ? "null" : dbquote($form->value("delivery_address_address2"));

                $delivery_address_fields[] = "order_address_zip";
                $delivery_address_values[] = trim($form->value("delivery_address_zip")) == "" ? "null" : dbquote($form->value("delivery_address_zip"));

                $delivery_address_fields[] = "order_address_place";
                $delivery_address_values[] = trim($form->value("delivery_address_place")) == "" ? "null" : dbquote($form->value("delivery_address_place"));

                $delivery_address_fields[] = "order_address_country";
                $delivery_address_values[] = trim($form->value("delivery_address_country")) == "" ? "null" : dbquote($form->value("delivery_address_country"));

                $delivery_address_fields[] = "order_address_phone";
                $delivery_address_values[] = trim($form->value("delivery_address_phone")) == "" ? "null" : dbquote($form->value("delivery_address_phone"));

				if(array_key_exists('delivery_phone_number', $form->items))
				{
					foreach($form->items['delivery_phone_number']['fields'] as $key=>$fieldname)
					{
						$value = dbquote($form->items['delivery_phone_number']['values'][$key]);

						$delivery_address_fields[] = $fieldname;
						$delivery_address_values[] = $value;

					}
				}

                $delivery_address_fields[] = "order_address_mobile_phone";
                $delivery_address_values[] = trim($form->value("delivery_address_mobile_phone")) == "" ? "null" : dbquote($form->value("delivery_address_mobile_phone"));

				if(array_key_exists('delivery_mobile_phone_number', $form->items))
				{
					foreach($form->items['delivery_mobile_phone_number']['fields'] as $key=>$fieldname)
					{
						$value = dbquote($form->items['delivery_mobile_phone_number']['values'][$key]);

						$delivery_address_fields[] = $fieldname;
						$delivery_address_values[] = $value;

					}
				}

                $delivery_address_fields[] = "order_address_email";
                $delivery_address_values[] = trim($form->value("delivery_address_email")) == "" ? "null" : dbquote($form->value("delivery_address_email"));

                $delivery_address_fields[] = "order_address_parent";
                $delivery_address_values[] = dbquote($form->value("order_client_address"));

                $delivery_address_fields[] = "order_address_contact";
                $delivery_address_values[] = trim($form->value("delivery_address_contact")) == "" ? "null" : dbquote($form->value("delivery_address_contact"));

                $delivery_address_fields[] = "date_created";
                $delivery_address_values[] = "current_timestamp";

                $delivery_address_fields[] = "date_modified";
                $delivery_address_values[] = "current_timestamp";

                if (isset($_SESSION["user_login"]))
                {
                    $delivery_address_fields[] = "user_created";
                    $delivery_address_values[] = dbquote($_SESSION["user_login"]);

                    $delivery_address_fields[] = "user_modified";
                    $delivery_address_values[] = dbquote($_SESSION["user_login"]);
                }

                $sql = "insert into order_addresses (" . join(", ", $delivery_address_fields) . ") values (" . join(", ", $delivery_address_values) . ")";
                mysql_query($sql) or dberror($sql);
            }
        }
    }
}


/***********************************************************************
   update supplier's data into order_items and dates
************************************************************************/
function  project_update_supplier_data_item($form, $supplier_addresses)
{
    $update_all_warehouse_addresses = $form->value("change_all_warehouse_addresses");
    $order_item_fields = array();

    $value = "current_timestamp";
    $order_item_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $order_item_fields[] = "user_modified = " . $value;
    }

    $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $form->value("order_item_id");
    mysql_query($sql) or dberror($sql);
    
    // expected ready for pick up: insert new entry if new or date has changed
    
    if ($form->value("expected_ready_for_pick_up") != $form->value("expected_ready_for_pick_up_old_value"))
    {
        if ($form->value("expected_ready_for_pick_up"))
        {
            $update_alldates = 0;
            $suppliers_ids = array();
            
            foreach($supplier_addresses as $key=>$supplier_address)
            {
            	if($form->value("change_all_dates_" . $supplier_address["id"]) == 1)
	        	{
	        		$suppliers_ids[] = $supplier_address["id"];
	        		$update_alldates = 1;
	        	}
	            
            }
            
            if($update_alldates == 0)
	        {
	            insert_date_item($form->value("order_item_id"), $form->value("expected_ready_for_pick_up"), 'EXRP');
	        }
	
	        // insert the same date for all order item records belonging to the
	        // same P.O. Number and the same supplier
	
	        foreach($suppliers_ids as $key=>$supplier_id)
	        {
		        append_date_items($form->value("order_item_order"), 
		                          "empty", 
		                          $supplier_id, 
		                          $form->value("expected_ready_for_pick_up"),
		                          "EXRP",
		                          "SUPP",
		                          $update_alldates, 
		                          $supplier_id);
	        }
        }
		elseif(has_access("can_delete_project_pickup_dates") or has_access("can_delete_order_pickup_dates"))
		{
			insert_date_item($form->value("order_item_id"), $form->value("expected_ready_for_pick_up"), 'EXRP');
		}
    }



	// pick up: insert new entry if new or date has changed
	if ($form->value("order_item_pickup") != $form->value("pick_up_old_value"))
    {

		if ($form->value("order_item_pickup"))
        {
			$update_alldates = 0;
            $suppliers_ids = array();
            
            foreach($supplier_addresses as $key=>$supplier_address)
            {
            	if($form->value("change_all_dates_" . $supplier_address["id"]) == 1)
	        	{
	        		$suppliers_ids[] = $supplier_address["id"];
	        		$update_alldates = 1;
	        	}
	            
            }
            
            if($update_alldates == 0)
	        {
	            insert_date_item($form->value("order_item_id"), $form->value("order_item_pickup"), 'PICK');
	        }
	
	        // insert the same date for all order item records belonging to the
	        // same P.O. Number and the same supplier
	
	        foreach($suppliers_ids as $key=>$supplier_id)
	        {
		        append_date_items($form->value("order_item_order"), 
		                          "empty", 
		                          $supplier_id, 
		                          $form->value("order_item_pickup"),
		                          "PICK",
		                          "SUPP",
		                          $update_alldates, 
		                          $supplier_id);
	        }
        }
		elseif(has_access("can_delete_project_pickup_dates") or has_access("can_delete_order_pickup_dates"))
		{
			insert_date_item($form->value("order_item_id"), $form->value("order_item_pickup"), 'PICK');
		}
    }
    

    // insert or update warehose addresses
	$new_place_id = 0;
	if($form->value("warehouse_address_place_id") == 999999999) //insert new city
	{
		$sql = "insert into places (place_country, place_province, place_name, date_created, user_created) VALUES (" .
			   dbquote($form->value("warehouse_address_country")) . ", " . 
			   dbquote($form->value("province")) . ", " . 
			   dbquote($form->value("warehouse_address_place")) . ", " .
			   dbquote(date("Y-m-d")) . ", " . 
			    dbquote(user_login()) . ")";
		
		mysql_query($sql) or dberror($sql);
		$new_place_id =  mysql_insert_id();
	
	}


    $sql = "select order_address_id ".
           "from order_addresses ".
           "where order_address_order = " . $form->value("order_item_order") .
           "    and order_address_order_item = " . $form->value("order_item_id").
           "    and order_address_type = 4";

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res)) 
    {
        // update warehous address in table order_addresses
        $warehouse_address_fields = array();

        $value = trim($form->value("warehouse_address_company")) == "" ? "null" : dbquote($form->value("warehouse_address_company"));
        $warehouse_address_fields[] = "order_address_company = " . $value;

         $value = trim($form->value("warehouse_address_company2")) == "" ? "null" : dbquote($form->value("warehouse_address_company2"));
        $warehouse_address_fields[] = "order_address_company2 = " . $value;

        $value = trim($form->value("warehouse_address_address")) == "" ? "null" : dbquote($form->value("warehouse_address_address"));
        $warehouse_address_fields[] = "order_address_address = " . $value;

		if(array_key_exists('warehouse_street', $form->items))
		{
			foreach($form->items['warehouse_street']['fields'] as $key=>$fieldname)
			{
				$value = dbquote($form->items['warehouse_street']['values'][$key]);
				$warehouse_address_fields[] = $fieldname . " = " . $value;

			}
		}


        $value = trim($form->value("warehouse_address_address2")) == "" ? "null" : dbquote($form->value("warehouse_address_address2"));
        $warehouse_address_fields[] = "order_address_address2 = " . $value;

        $value = trim($form->value("warehouse_address_zip")) == "" ? "null" : dbquote($form->value("warehouse_address_zip"));
        $warehouse_address_fields[] = "order_address_zip = " . $value;

        $value = trim($form->value("warehouse_address_place")) == "" ? "null" : dbquote($form->value("warehouse_address_place"));
        $warehouse_address_fields[] = "order_address_place = " . $value;

		
		if($new_place_id > 0)
		{
			$value = $new_place_id;
		}
		else
		{
			$value = trim($form->value("warehouse_address_place_id")) == "" ? "null" : dbquote($form->value("warehouse_address_place_id"));
		}
        $warehouse_address_fields[] = "order_address_place_id = " . $value;

        $value = trim($form->value("warehouse_address_country")) == "" ? "null" : dbquote($form->value("warehouse_address_country"));
        $warehouse_address_fields[] = "order_address_country = " . $value;

        $value = trim($form->value("warehouse_address_phone")) == "" ? "null" : dbquote($form->value("warehouse_address_phone"));
        $warehouse_address_fields[] = "order_address_phone = " . $value;

		if(array_key_exists('warehouse_phone_number', $form->items))
		{
			foreach($form->items['warehouse_phone_number']['fields'] as $key=>$fieldname)
			{
				$value = dbquote($form->items['warehouse_phone_number']['values'][$key]);
				$warehouse_address_fields[] = $fieldname . " = " . $value;

			}
		}


        $value = trim($form->value("warehouse_address_mobile_phone")) == "" ? "null" : dbquote($form->value("warehouse_address_mobile_phone"));
        $warehouse_address_fields[] = "order_address_mobile_phone = " . $value;


		if(array_key_exists('warehouse_mobile_phone_number', $form->items))
		{
			foreach($form->items['warehouse_mobile_phone_number']['fields'] as $key=>$fieldname)
			{
				$value = dbquote($form->items['warehouse_mobile_phone_number']['values'][$key]);
				$warehouse_address_fields[] = $fieldname . " = " . $value;

			}
		}

        $value = trim($form->value("warehouse_address_email")) == "" ? "null" : dbquote($form->value("warehouse_address_email"));
        $warehouse_address_fields[] = "order_address_email = " . $value;

        $value = $form->value("order_item_supplier");
        $warehouse_address_fields[] = "order_address_parent = " . $value;

        $value = trim($form->value("warehouse_address_contact")) == "" ? "null" : dbquote($form->value("warehouse_address_contact"));
        $warehouse_address_fields[] = "order_address_contact = " . $value;

        $value = "current_timestamp";
        $warehouse_address_fields[] = "date_modified = " . $value;

        if (isset($_SESSION["user_login"]))
        {
            $value = dbquote($_SESSION["user_login"]);
            $warehouse_address_fields[] = "user_modified = " . $value;
        }
		
		if($update_all_warehouse_addresses == 1) // update all warehouse addresses
		{
	        $sql = "select order_address_id " . 
	               "from order_addresses " .
	               "left join order_items on order_item_id = order_address_order_item " .
	               " where order_address_type = 4 ".
	               "    and order_address_order = " . $form->value("order_item_order") .
	               "    and order_item_supplier_address = " . $form->value("order_item_supplier");
	        
	        $res = mysql_query($sql) or dberror($sql);
		    while ($row = mysql_fetch_assoc($res)) 
		    {
		        $sql_u = "update order_addresses set " . join(", ", $warehouse_address_fields) . 
		               " where order_address_id = " . $row["order_address_id"];
		    	
		    	mysql_query($sql_u) or dberror($sql_u);
		    }
		}
		else
		{
			$sql = "update order_addresses set " . join(", ", $warehouse_address_fields) . 
	               " where order_address_type = 4 ".
	               "    and order_address_order = " . $form->value("order_item_order").
	               "    and order_address_order_item = ". $form->value("order_item_id");
	        
	        mysql_query($sql) or dberror($sql);
		}
        
    }
    else // insert new
    {
        // insert warehouse addresses into table order_addresses for all items
        // not having a warehose address and beloging to the same order and the
        // same supplier

        if($update_all_warehouse_addresses == 1) // update all warehouse addresses
		{
	        $sql_order_items = "select order_item_id, order_item_order ".
	                           "from order_items ".
	                           "where order_item_order = " . $form->value("order_item_order") .
	                           "    and order_item_supplier_address =" . $form->value("order_item_supplier") .
	                           "    and order_item_type <= ". ITEM_TYPE_SPECIAL;
		}
		else
		{
			$sql_order_items = "select order_item_id, order_item_order ".
	                           "from order_items ".
	                           "where order_item_order = " . $form->value("order_item_order") .
	                           "    and order_item_supplier_address =" . $form->value("order_item_supplier") .
	                           "    and order_item_id = ". $form->value("order_item_id") .
							   "    and order_item_type <= ". ITEM_TYPE_SPECIAL;
		}


        $res = mysql_query($sql_order_items) or dberror($sql_order_items);
        while ($row = mysql_fetch_assoc($res)) 
        {
            // check if record is already there
            $sql = "select order_address_id ".
                   "from order_addresses ".
                   "where order_address_order = " . $row["order_item_order"] .
                   "    and order_address_order_item = " . $row["order_item_id"].
                   "    and order_address_type = 4";

            $res1 = mysql_query($sql) or dberror($sql);
            if ($row1 = mysql_fetch_assoc($res1)) 
            {
                //nothing
            }
            else
            {
                // insert new record
        
                $warehouse_address_fields = array();
                $warehouse_address_values = array();

        
                $warehouse_address_fields[] = "order_address_order";
                $warehouse_address_values[] = $row["order_item_order"];

                $warehouse_address_fields[] = "order_address_order_item";
                $warehouse_address_values[] = $row["order_item_id"];

                $warehouse_address_fields[] = "order_address_type";
                $warehouse_address_values[] = 4;

                $warehouse_address_fields[] = "order_address_company";
                $warehouse_address_values[] = trim($form->value("warehouse_address_company")) == "" ? "null" : dbquote($form->value("warehouse_address_company"));

                $warehouse_address_fields[] = "order_address_company2";
                $warehouse_address_values[] = trim($form->value("warehouse_address_company2")) == "" ? "null" : dbquote($form->value("warehouse_address_company2"));

                $warehouse_address_fields[] = "order_address_address";
                $warehouse_address_values[] = trim($form->value("warehouse_address_address")) == "" ? "null" : dbquote($form->value("warehouse_address_address"));

				if(array_key_exists('warehouse_street', $form->items))
				{
					foreach($form->items['warehouse_street']['fields'] as $key=>$fieldname)
					{
						$value = dbquote($form->items['warehouse_street']['values'][$key]);
						$warehouse_address_fields[] = $fieldname;
						$warehouse_address_values[] = $value;
					}
				}

                $warehouse_address_fields[] = "order_address_address2";
                $warehouse_address_values[] = trim($form->value("warehouse_address_address2")) == "" ? "null" : dbquote($form->value("warehouse_address_address2"));

                $warehouse_address_fields[] = "order_address_zip";
                $warehouse_address_values[] = trim($form->value("warehouse_address_zip")) == "" ? "null" : dbquote($form->value("warehouse_address_zip"));

                if($new_place_id > 0)
				{
					$value = $new_place_id;
				}
				else
				{
					$value = $form->value("warehouse_address_place_id");
				}
				
				$warehouse_address_fields[] = "order_address_place_id";
                $warehouse_address_values[] = dbquote($value);

				$warehouse_address_fields[] = "order_address_place";
                $warehouse_address_values[] = trim($form->value("warehouse_address_place")) == "" ? "null" : dbquote($form->value("warehouse_address_place"));

                $warehouse_address_fields[] = "order_address_country";
                $warehouse_address_values[] = trim($form->value("warehouse_address_country")) == "" ? "null" : dbquote($form->value("warehouse_address_country"));

                $warehouse_address_fields[] = "order_address_phone";
                $warehouse_address_values[] = trim($form->value("warehouse_address_phone")) == "" ? "null" : dbquote($form->value("warehouse_address_phone"));

				if(array_key_exists('warehouse_phone_number', $form->items))
				{
					foreach($form->items['warehouse_phone_number']['fields'] as $key=>$fieldname)
					{
						$value = dbquote($form->items['warehouse_phone_number']['values'][$key]);
						$warehouse_address_fields[] = $fieldname;
						$warehouse_address_values[] = $value;
					}
				}

                $warehouse_address_fields[] = "order_address_mobile_phone";
                $warehouse_address_values[] = trim($form->value("warehouse_address_mobile_phone")) == "" ? "null" : dbquote($form->value("warehouse_address_mobile_phone"));

				if(array_key_exists('warehouse_mobile_phone_number', $form->items))
				{
					foreach($form->items['warehouse_mobile_phone_number']['fields'] as $key=>$fieldname)
					{
						$value = dbquote($form->items['warehouse_mobile_phone_number']['values'][$key]);
						$warehouse_address_fields[] = $fieldname;
						$warehouse_address_values[] = $value;
					}
				}

                $warehouse_address_fields[] = "order_address_email";
                $warehouse_address_values[] = trim($form->value("warehouse_address_email")) == "" ? "null" : dbquote($form->value("warehouse_address_email"));

                $warehouse_address_fields[] = "order_address_parent";
                $warehouse_address_values[] = dbquote($form->value("order_item_supplier"));

                $warehouse_address_fields[] = "order_address_contact";
                $warehouse_address_values[] = trim($form->value("warehouse_address_contact")) == "" ? "null" : dbquote($form->value("warehouse_address_contact"));

                $warehouse_address_fields[] = "date_created";
                $warehouse_address_values[] = "current_timestamp";

                $warehouse_address_fields[] = "date_modified";
                $warehouse_address_values[] = "current_timestamp";

                if (isset($_SESSION["user_login"]))
                {
                    $warehouse_address_fields[] = "user_created";
                    $warehouse_address_values[] = dbquote($_SESSION["user_login"]);

                    $warehouse_address_fields[] = "user_modified";
                    $warehouse_address_values[] = dbquote($_SESSION["user_login"]);
                }

                $sql = "insert into order_addresses (" . join(", ", $warehouse_address_fields) . ") values (" . join(", ", $warehouse_address_values) . ")";
                mysql_query($sql) or dberror($sql);
            }
        }
    }



	//update all slave items depending on the item, mark as delivered
	if ($form->value("expected_ready_for_pick_up") 
		and ($form->value("expected_ready_for_pick_up_old_value") == "" 
		or $form->value("expected_ready_for_pick_up_old_value") == "0000-00-00"))
    {

		
		if($form->value("change_all_dates_" . $supplier_address["id"]) == 1)
	    {
			$sql = "select item_slave_item " . 
				   "from order_items "  .
				   "left join items on item_id = order_item_item " .
				   " where order_item_order = " . $form->value("order_item_order") . 
				   " and order_item_supplier_address = " . $form->value("order_item_supplier") .
				   " and item_slave_item > 0";
		}
		else
		{
			$sql = "select item_slave_item " . 
				   "from order_items "  .
				   "left join items on item_id = order_item_item " .
				   " where order_item_order = " . $form->value("order_item_order") . 
				   " and order_item_id = " . $form->value("order_item_id") . 
				   " and item_slave_item > 0";
		}
		
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res)) 
		{
			$sql_u = "update order_items set " . 
					 "order_item_ready_for_pickup = " . dbquote(to_system_date($form->value("expected_ready_for_pick_up"))) . ", " . 
					 "order_item_ready_for_pickup_changes = 1,  " . 
					 "order_item_pickup = " . dbquote(to_system_date($form->value("expected_ready_for_pick_up"))) . ", " .
					 "order_item_pickup_changes = 1, " . 
					 "order_item_expected_arrival = " . dbquote(to_system_date($form->value("expected_ready_for_pick_up"))) . ", " .
					 "order_item_expected_arrival_changes = 1, " .
					 "order_item_arrival = " . dbquote(to_system_date($form->value("expected_ready_for_pick_up"))) . ", " .
					 "order_item_arrival_changes = 1 " .
					 "where order_item_item = " . $row["item_slave_item"] . 
				     " and order_item_supplier_address = " . $form->value("order_item_supplier") . 
				     " and order_item_order = " . $form->value("order_item_order");

			
			
			mysql_query($sql_u) or dberror($sql_u);

		    
			$sql_i = "select order_item_id " . 
				     "from order_items "  .
				     "where order_item_item = " . $row["item_slave_item"] . 
				     " and order_item_supplier_address = " . $form->value("order_item_supplier") . 
				     " and order_item_order = " . $form->value("order_item_order");

			
			$res_i = mysql_query($sql_i) or dberror($sql_i);
			while ($row_i = mysql_fetch_assoc($res_i)) 
			{
				insert_date_item($row_i["order_item_id"], $form->value("expected_ready_for_pick_up"), 'EXRP');
				insert_date_item($row_i["order_item_id"], $form->value("expected_ready_for_pick_up"), 'PICK');
				insert_date_item($row_i["order_item_id"], $form->value("expected_ready_for_pick_up"), 'EXAR');
				insert_date_item($row_i["order_item_id"], $form->value("expected_ready_for_pick_up"), 'ACAR');
			}
		}


	}


	//update all slave items depending on the item, mark as delivered
	if ($form->value("order_item_pickup") 
		and ($form->value("order_item_pickup_old_value") == "" 
		or $form->value("order_item_pickup_old_value") == "0000-00-00"))
    {

		
		if($form->value("change_all_dates_" . $supplier_address["id"]) == 1)
	    {
			$sql = "select item_slave_item " . 
				   "from order_items "  .
				   "left join items on item_id = order_item_item " .
				   " where order_item_order = " . $form->value("order_item_order") . 
				   " and order_item_supplier_address = " . $form->value("order_item_supplier") .
				   " and item_slave_item > 0";
		}
		else
		{
			$sql = "select item_slave_item " . 
				   "from order_items "  .
				   "left join items on item_id = order_item_item " .
				   " where order_item_order = " . $form->value("order_item_order") . 
				   " and order_item_id = " . $form->value("order_item_id") . 
				   " and item_slave_item > 0";
		}
		
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res)) 
		{
			$sql_u = "update order_items set " . 
					 "order_item_ready_for_pickup = " . dbquote(to_system_date($form->value("expected_ready_for_pick_up"))) . ", " . 
					 "order_item_ready_for_pickup_changes = 1,  " . 
					 "order_item_pickup = " . dbquote(to_system_date($form->value("expected_ready_for_pick_up"))) . ", " .
					 "order_item_pickup_changes = 1, " . 
					 "order_item_expected_arrival = " . dbquote(to_system_date($form->value("expected_ready_for_pick_up"))) . ", " .
					 "order_item_expected_arrival_changes = 1, " .
					 "order_item_arrival = " . dbquote(to_system_date($form->value("expected_ready_for_pick_up"))) . ", " .
					 "order_item_arrival_changes = 1 " .
					 "where order_item_item = " . $row["item_slave_item"] . 
				     " and order_item_supplier_address = " . $form->value("order_item_supplier") . 
				     " and order_item_order = " . $form->value("order_item_order");

			
			
			mysql_query($sql_u) or dberror($sql_u);

		    
			$sql_i = "select order_item_id " . 
				     "from order_items "  .
				     "where order_item_item = " . $row["item_slave_item"] . 
				     " and order_item_supplier_address = " . $form->value("order_item_supplier") . 
				     " and order_item_order = " . $form->value("order_item_order");

			
			$res_i = mysql_query($sql_i) or dberror($sql_i);
			while ($row_i = mysql_fetch_assoc($res_i)) 
			{
				insert_date_item($row_i["order_item_id"], $form->value("expected_ready_for_pick_up"), 'EXRP');
				insert_date_item($row_i["order_item_id"], $form->value("expected_ready_for_pick_up"), 'PICK');
				insert_date_item($row_i["order_item_id"], $form->value("expected_ready_for_pick_up"), 'EXAR');
				insert_date_item($row_i["order_item_id"], $form->value("expected_ready_for_pick_up"), 'ACAR');
			}
		}


	}

}

/***********************************************************************
   insert dates from traffic data into dates
************************************************************************/
function  update_traffic_data_item($form, $type, $supplier_addresses)
{

    $update_alldates = 0;
    $suppliers_ids = array();
    foreach($supplier_addresses as $key=>$supplier_address)
    {
    	if($form->value("change_all_dates_" . $supplier_address["id"]) == 1)
    	{
    		$suppliers_ids[] = $supplier_address["id"];
    		$update_alldates = 1;
    	}
        
    }
    
    $order_item_fields = array();

    $value = dbquote($form->value("order_item_shipment_code"));
    $order_item_fields[] = "order_item_shipment_code = " . $value;
	
	
	if(array_key_exists("order_item_preferred_arrival_date", $form->items))
	{
		$value = dbquote(from_system_date($form->value("order_item_preferred_arrival_date")));
		$order_item_fields[] = "order_item_preferred_arrival_date = " . $value;
	}
    
    $value = "current_timestamp";
    $order_item_fields[] = "date_modified = " . $value;
    
    if (isset($_SESSION["user_login"]))
    {
        $value = dbquote($_SESSION["user_login"]);
        $order_item_fields[] = "user_modified = " . $value;
    }

    $sql = "update order_items set " . join(", ", $order_item_fields) . " where order_item_id = " . $form->value("order_item_id");
    mysql_query($sql) or dberror($sql);


    // update shipment code for all empty items belonging to the same forwarder and p.o. number
    foreach($suppliers_ids as $key=>$supplier_id)
	{
		
		if(array_key_exists("order_item_preferred_arrival_date", $form->items))
		{
			$sql = "update order_items ".
				   "set order_item_preferred_arrival_date = " .  dbquote(from_system_date($form->value("order_item_preferred_arrival_date"))) . ", ".
				   " order_item_shipment_code=" . dbquote($form->value("order_item_shipment_code")) . " ".
				   "where order_item_order=" . $form->value("order_item_order").
				   "    and order_item_forwarder_address=" . $form->value("order_item_forwarder").
				   "    and order_item_po_number='" . $form->value("order_item_po_number"). "' " .
				   //"    and (order_item_shipment_code is Null or order_item_shipment_code = '') " . 
				   "    and order_item_supplier_address = " . $supplier_id;
		}
		else
		{
			$sql = "update order_items ".
				   "set order_item_shipment_code=" . dbquote($form->value("order_item_shipment_code")) . " ".
				   "where order_item_order=" . $form->value("order_item_order").
				   "    and order_item_forwarder_address=" . $form->value("order_item_forwarder").
				   "    and order_item_po_number='" . $form->value("order_item_po_number"). "' " .
				   //"    and (order_item_shipment_code is Null or order_item_shipment_code = '') " . 
				   "    and order_item_supplier_address = " . $supplier_id;
		}
		mysql_query($sql) or dberror($sql);
	}

    // pick up: insert new entry if new or date has changed
    /*
	if ($form->value("order_item_pickup") != $form->value("pick_up_old_value"))
    {
        if ($form->value("order_item_pickup"))
        {
            if($update_alldates == 0)
	        {
            	insert_date_item($form->value("order_item_id"), $form->value("order_item_pickup"), 'PICK');
	        }
            // insert the same date for all order item records belonging to the
	        // same p.o. number and the same forwarder
            foreach($suppliers_ids as $key=>$supplier_id)
	        {
	        	append_date_items($form->value("order_item_order"),
	                              $form->value("order_item_po_number"),
	                              $form->value("order_item_forwarder"), 
	                              $form->value("order_item_pickup"),
	                              'PICK',
	                              'FRWD',
	                              $update_alldates, 
	                              $supplier_id);
	        }
        }
		elseif(has_access("can_delete_project_traffic_dates") or has_access("can_delete_order_traffic_dates"))
		{
			insert_date_item($form->value("order_item_id"), $form->value("order_item_pickup"), 'PICK');
		}
    }
	*/

    // expected arrival: insert new entry if new or date has changed
    if ($form->value("expected_arrival") != $form->value("expected_arrival_old_value"))
    {
        if ($form->value("expected_arrival"))
        {
            if($update_alldates == 0)
	        {
            	insert_date_item($form->value("order_item_id"), $form->value("expected_arrival"), 'EXAR');
	        }
            
            // insert the same date for all order item records belonging to the
	        // same p.o. number and the same forwarder
	        foreach($suppliers_ids as $key=>$supplier_id)
	        {
	        	append_date_items($form->value("order_item_order"),
	                              $form->value("order_item_po_number"),
	                              $form->value("order_item_forwarder"), 
	                              $form->value("expected_arrival"),
	                              'EXAR',
	                              'FRWD',
	                              $update_alldates, 
	                              $supplier_id);
	        }
        }
		elseif(has_access("can_delete_project_traffic_dates") or has_access("can_delete_order_traffic_dates"))
		{
			insert_date_item($form->value("order_item_id"), $form->value("expected_arrival"), 'EXAR');
		}
    }

    // actual arrival: insert new entry if new or date has changed
    if ($form->value("arrival") != $form->value("arrival_old_value"))
    {
        if ($form->value("arrival"))
        {
            if($update_alldates == 0)
	        {
	        	insert_date_item($form->value("order_item_id"), $form->value("arrival"), 'ACAR');
	        }
            
            // insert the same date for all order item records belonging to the
	        // same p.o. number and the same forwarder
            foreach($suppliers_ids as $key=>$supplier_id)
	        {
	        	append_date_items($form->value("order_item_order"),
	                              $form->value("order_item_po_number"),
	                              $form->value("order_item_forwarder"), 
	                              $form->value("arrival"),
	                              'ACAR',
	                              'FRWD',
	                              $update_alldates, 
	                              $supplier_id);
	        }
        }
		elseif(has_access("can_delete_project_traffic_dates") or has_access("can_delete_order_traffic_dates"))
		{
			insert_date_item($form->value("order_item_id"), $form->value("arrival"), 'ACAR');
		}
    }
}


/***********************************************************************
   insert a new record into dates on the level of an order item
************************************************************************/
function insert_date_item($order_item_id, $date, $type)
{
    //get date_type_id
    $sql = "select date_type_id ".
           "from date_types ".
           "where date_type_code =" . dbquote($type);

    $res = mysql_query($sql) or dberror($sql);
    
    if ($row = mysql_fetch_assoc($res))
    {
        $date_fields = array();
        $date_values = array();

        $date_fields[] = "date_order_item";
        $date_values[] = $order_item_id;

        $date_fields[] = "date_type";
        $date_values[] = $row["date_type_id"];

        $date_fields[] = "date_date";
        if ($date == "current_timestamp")
        {
            $date_values[] = dbquote(from_system_date($date), true);
        }
        else
        {
            $date_values[] = dbquote(from_system_date($date), true);
        }

        $date_fields[] = "date_created";
        $date_values[] = "current_timestamp";

        $date_fields[] = "date_modified";
        $date_values[] = "current_timestamp";
                
        if (isset($_SESSION["user_login"]))
        {
            $date_fields[] = "user_created";
            $date_values[] = dbquote($_SESSION["user_login"]);

            $date_fields[] = "user_modified";
            $date_values[] = dbquote($_SESSION["user_login"]);
        }
    
        $sql = "insert into dates (" . join(", ", $date_fields) . ") values (" . join(", ", $date_values) . ")";
        mysql_query($sql) or dberror($sql);
   
    }
    update_order_item_dates($order_item_id, $date, $type);
}



/***********************************************************************
   append traffic data to dates

   append records for all other empty entries belonging
   to the same forwarder and the same P.O. Number
************************************************************************/
function  append_date_items($order, $order_po_number, $address, $date, $type, $address_type, $update_mode, $supplier_address)
{
    
    $date_type = $type;

    //get date_type_id
    $sql = "select date_type_id ".
           "from date_types ".
           "where date_type_code =" . dbquote($type);

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        if ($address_type == 'FRWD')
        {
            $sql = "select order_item_id ".
                   "from order_items ".
                   "where order_item_type <= " . ITEM_TYPE_SPECIAL .
                   "   and order_item_order = " . $order .
                   "   and order_item_po_number = " . dbquote($order_po_number) .
                   "   and order_item_forwarder_address = " . $address;
                   
                   
           $sql = "select order_item_id ".
                   "from order_items ".
                   "where order_item_type <= " . ITEM_TYPE_SPECIAL .
                   "   and order_item_order = " . $order .
                   "   and order_item_po_number <>'' " .
                   "   and order_item_supplier_address = " . $supplier_address . 
                   "   and order_item_forwarder_address = " . $address;
        }
        else
        {   
            $sql = "select order_item_id, order_item_supplier_address ".
                   "from order_items ".
                   "where order_item_type <= " . ITEM_TYPE_SPECIAL .
                   "   and order_item_order = " . $order .
                   "   and order_item_supplier_address = " . $address .
                   "   and order_item_po_number <>''";
        }


        $res1 = mysql_query($sql) or dberror($sql);
        while ($row1 = mysql_fetch_assoc($res1))
        {
            // check if a date record is present
            $sql = "select date_id " .
                   "from dates " .
                   "where date_order_item = " . $row1["order_item_id"] . 
                   " and date_type = " . $row["date_type_id"];

            $res2 = mysql_query($sql) or dberror($sql);
                
            if ($row2 = mysql_fetch_assoc($res2) && $update_mode == 0) // update dates
            {
				// check if ready fro pick up date is entered
                if ($address_type == 'FRWD')
                {

                    $sql_dates = "select date_id ".
                                 "from dates ".
                                 "left join date_types on date_type = date_type_id ".
                                 "where date_order_item = ". $row1["order_item_id"] .
                                 "    and date_type_code = 'EXRP'";

                    $res3= mysql_query($sql_dates) or dberror($sql_dates);
  
                    
                    if ($row3 = mysql_fetch_assoc($res3))
                    {
                        insert_date_item($row1["order_item_id"], $date, $date_type);
                    }
                }
                else
                {
                	insert_date_item($row1["order_item_id"], $date, $date_type);
                }
            }
            else // insert dates
            {
                // check if ready fro pick up date is entered
                if ($address_type == 'FRWD')
                {
                    $ready4pickup = 0;

                    $sql_dates = "select date_id ".
                                 "from dates ".
                                 "left join date_types on date_type = date_type_id ".
                                 "where date_order_item = ". $row1["order_item_id"] .
                                 "    and date_type_code = 'EXRP'";

                    $res3= mysql_query($sql_dates) or dberror($sql_dates);
  
                    if ($row3 = mysql_fetch_assoc($res3))
                    {
                        $ready4pickup = 1;
                    }

                    if ($ready4pickup == 1)
                    {
                        insert_date_item($row1["order_item_id"], $date, $date_type);
                    }                    
                }
                else
                {
                    insert_date_item($row1["order_item_id"], $date, $date_type);
                }
            }
        }

    }
}


/***********************************************************************
   update dates in order_items
************************************************************************/
function  update_order_item_dates($order_item_id, $date, $type)
{

    if ($type == 'EXRP')
    {
        
        $sql= "select order_item_ready_for_pickup, order_item_ready_for_pickup_changes ".
              "from order_items ".
              "where order_item_id = " . $order_item_id;
        
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $number_of_changes = $row["order_item_ready_for_pickup_changes"] + 1;
        }
        
        $sql = "Update order_items set order_item_ready_for_pickup = " . dbquote(from_system_date($date), true) . ", order_item_ready_for_pickup_changes = " .   $number_of_changes . " where order_item_id = " . $order_item_id;
        mysql_query($sql) or dberror($sql);
    }
    elseif ($type == 'PICK')
    {
        
        $sql= "select order_item_pickup, order_item_pickup_changes ".
              "from order_items ".
              "where order_item_id = " . $order_item_id;
        
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $number_of_changes = $row["order_item_pickup_changes"] + 1;
        }
        $sql = "Update order_items set order_item_pickup = " . dbquote(from_system_date($date), true) . ", order_item_pickup_changes = " .   $number_of_changes . " where order_item_id = " . $order_item_id;
        mysql_query($sql) or dberror($sql);
    }
    elseif ($type == 'EXAR')
    {
        $sql= "select order_item_expected_arrival, order_item_expected_arrival_changes ".
              "from order_items ".
              "where order_item_id = " . $order_item_id;
        
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $number_of_changes = $row["order_item_expected_arrival_changes"] + 1;
        }
        $sql = "Update order_items set order_item_expected_arrival = " . dbquote(from_system_date($date), true) . ", order_item_expected_arrival_changes = " .   $number_of_changes . " where order_item_id = " . $order_item_id;
        mysql_query($sql) or dberror($sql);
    }
    elseif ($type == 'ACAR')
    {
        
        $sql= "select order_item_arrival, order_item_arrival_changes ".
              "from order_items ".
              "where order_item_id = " . $order_item_id;
        
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $number_of_changes = $row["order_item_arrival_changes"] + 1;
        }
        $sql = "Update order_items set order_item_arrival = " . dbquote(from_system_date($date), true) . ", order_item_arrival_changes = " .   $number_of_changes . " where order_item_id = " . $order_item_id;
        mysql_query($sql) or dberror($sql);
    }
    elseif ($type == 'ORSU')
    {
        
        $sql= "select order_item_ordered, order_item_ordered_changes ".
              "from order_items ".
              "where order_item_id = " . $order_item_id;
        
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $number_of_changes = $row["order_item_ordered_changes"] + 1;
        }
        if($row["order_item_ordered"] == null or $row["order_item_ordered"] == "0000-00-00") 
        {
            $sql = "Update order_items set order_item_ordered = " . dbquote(from_system_date($date), true) . ", order_item_ordered_changes = " .   $number_of_changes . " where order_item_id = " . $order_item_id;
            mysql_query($sql) or dberror($sql);
        }
    }
}

/********************************************************************
    save comment
*********************************************************************/
function save_comment($form, $check_box_names)
{
    $comment_fields = array();
    $comment_values = array();

    $comment_fields[] = "comment_order";
    $comment_values[] = $form->value("order_id");

    $comment_fields[] = "comment_user";
    $comment_values[] = user_id();

    $comment_fields[] = "comment_category";
    $comment_values[] = $form->value("comment_category");

    $comment_fields[] = "comment_text";
    $comment_values[] = dbquote($form->value("comment_text"));

    $comment_fields[] = "date_created";
    $comment_values[] = "current_timestamp";

    $comment_fields[] = "date_modified";
    $comment_values[] = "current_timestamp";
                
    if (isset($_SESSION["user_login"]))
    {
        $comment_fields[] = "user_created";
        $comment_values[] = dbquote($_SESSION["user_login"]);

        $comment_fields[] = "user_modified";
        $comment_values[] = dbquote($_SESSION["user_login"]);
    }
    
    $sql = "insert into comments (" . join(", ", $comment_fields) . ") values (" . join(", ", $comment_values) . ")";
    mysql_query($sql) or dberror($sql);

    $comment_id = mysql_insert_id();


    // insert records into comment_addresses
	/*
    if (has_access("can_set_comment_accessibility_in_projects") 
		or has_access("can_set_comment_accessibility_in_orders"))
    {
	*/

        foreach ($check_box_names as $key=>$value)
        {
			if ($form->value($key))
            {
				$comment_address_fields = array();
                $comment_address_values = array();

                $comment_address_fields[] = "comment_address_comment";
                $comment_address_values[] = $comment_id;

                $comment_address_fields[] = "comment_address_address";
                $comment_address_values[] = $value;

                $comment_address_fields[] = "date_created";
                $comment_address_values[] = "current_timestamp";

                $comment_address_fields[] = "date_modified";
                $comment_address_values[] = "current_timestamp";
                
                if (isset($_SESSION["user_login"]))
                {
                    $comment_address_fields[] = "user_created";
                    $comment_address_values[] = dbquote($_SESSION["user_login"]);

                    $comment_address_fields[] = "user_modified";
                    $comment_address_values[] = dbquote($_SESSION["user_login"]);
                }
    
                $sql = "insert into comment_addresses (" . join(", ", $comment_address_fields) . ") values (" . join(",        ", $comment_address_values) . ")";
                mysql_query($sql) or dberror($sql);
            }
        }
    /*
	}
	*/
    return $comment_id;
}


/********************************************************************
    save attachment infos concerning visibility and roles
*********************************************************************/
function save_attachment_accessibility_info($form, $check_box_names)
{

    // get order_file_id from the inserted record
    $sql = "select order_file_id ".
           "from order_files ".
           "where order_file_order = " . $form->value("order_file_order") . " " .
           "order by date_created DESC ".
           "limit 1";

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $attachment_id = $row["order_file_id"];
    }

    // insert records into order_file_addresses
    //if (has_access("can_set_attachment_accessibility_in_orders") or has_access("can_set_attachment_accessibility_in_projects"))
    //{
    
        foreach ($check_box_names as $key=>$value)
        {
            if ($form->value($key))
            {
                $sql = "select user_address from users where user_id = " . $value;
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				$address_id = $row["user_address"];

				$attachment_address_fields = array();
                $attachment_address_values = array();

                $attachment_address_fields[] = "order_file_address_file";
                $attachment_address_values[] = $attachment_id;

                $attachment_address_fields[] = "order_file_address_address";
                $attachment_address_values[] = $address_id;

                $attachment_address_fields[] = "date_created";
                $attachment_address_values[] = "current_timestamp";

                $attachment_address_fields[] = "date_modified";
                $attachment_address_values[] = "current_timestamp";
                
                if (isset($_SESSION["user_login"]))
                {
                    $attachment_address_fields[] = "user_created";
                    $attachment_address_values[] = dbquote($_SESSION["user_login"]);

                    $attachment_address_fields[] = "user_modified";
                    $attachment_address_values[] = dbquote($_SESSION["user_login"]);
                }
    
                $sql = "insert into order_file_addresses (" . join(", ", $attachment_address_fields) . ") values (" . join(", ", $attachment_address_values) . ")";
                mysql_query($sql) or dberror($sql);

            }
        }

		if($form->value("supplier1")) {

				$address_id = $form->value("supplier1");

				$attachment_address_fields = array();
                $attachment_address_values = array();

                $attachment_address_fields[] = "order_file_address_file";
                $attachment_address_values[] = $attachment_id;

                $attachment_address_fields[] = "order_file_address_address";
                $attachment_address_values[] = $address_id;

                $attachment_address_fields[] = "date_created";
                $attachment_address_values[] = "current_timestamp";

                $attachment_address_fields[] = "date_modified";
                $attachment_address_values[] = "current_timestamp";
                
                if (isset($_SESSION["user_login"]))
                {
                    $attachment_address_fields[] = "user_created";
                    $attachment_address_values[] = dbquote($_SESSION["user_login"]);

                    $attachment_address_fields[] = "user_modified";
                    $attachment_address_values[] = dbquote($_SESSION["user_login"]);
                }
    
                $sql = "insert into order_file_addresses (" . join(", ", $attachment_address_fields) . ") values (" . join(", ", $attachment_address_values) . ")";
                mysql_query($sql) or dberror($sql);
		}

		if($form->value("supplier2")) {

				$address_id = $form->value("supplier2");

				$attachment_address_fields = array();
                $attachment_address_values = array();

                $attachment_address_fields[] = "order_file_address_file";
                $attachment_address_values[] = $attachment_id;

                $attachment_address_fields[] = "order_file_address_address";
                $attachment_address_values[] = $address_id;

                $attachment_address_fields[] = "date_created";
                $attachment_address_values[] = "current_timestamp";

                $attachment_address_fields[] = "date_modified";
                $attachment_address_values[] = "current_timestamp";
                
                if (isset($_SESSION["user_login"]))
                {
                    $attachment_address_fields[] = "user_created";
                    $attachment_address_values[] = dbquote($_SESSION["user_login"]);

                    $attachment_address_fields[] = "user_modified";
                    $attachment_address_values[] = dbquote($_SESSION["user_login"]);
                }
    
                $sql = "insert into order_file_addresses (" . join(", ", $attachment_address_fields) . ") values (" . join(", ", $attachment_address_values) . ")";
                mysql_query($sql) or dberror($sql);
		}
    //}
	
}


/********************************************************************
    update file accessibility
*********************************************************************/
function update_attachment_accessibility_info($id, $form, $check_box_names)
{
	$sql = "delete from order_file_addresses where order_file_address_file = " . $id;
    mysql_query($sql) or dberror($sql);

	foreach ($check_box_names as $key=>$value)
    {
		if ($form->value($key) == 1)
        {
			$sql = "select user_address from users where user_id = " . $value;
			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);
			$address_id = $row["user_address"];

			
			$attachment_address_fields = array();
            $attachment_address_values = array();

            $attachment_address_fields[] = "order_file_address_file";
            $attachment_address_values[] = $id;

            $attachment_address_fields[] = "order_file_address_address";
            $attachment_address_values[] = $address_id;

            $attachment_address_fields[] = "date_created";
            $attachment_address_values[] = "current_timestamp";

            $attachment_address_fields[] = "date_modified";
            $attachment_address_values[] = "current_timestamp";
                
            if (isset($_SESSION["user_login"]))
            {
                $attachment_address_fields[] = "user_created";
                $attachment_address_values[] = dbquote($_SESSION["user_login"]);

                $attachment_address_fields[] = "user_modified";
                $attachment_address_values[] = dbquote($_SESSION["user_login"]);
            }
    
            $sql = "insert into order_file_addresses (" . join(", ", $attachment_address_fields) . ") values (" . join(", ", $attachment_address_values) . ")";
            mysql_query($sql) or dberror($sql);

        }

    }

	if($form->value("supplier1")) {

				$address_id = $form->value("supplier1");

				$attachment_address_fields = array();
                $attachment_address_values = array();

                $attachment_address_fields[] = "order_file_address_file";
                $attachment_address_values[] = $id;

                $attachment_address_fields[] = "order_file_address_address";
                $attachment_address_values[] = $address_id;

                $attachment_address_fields[] = "date_created";
                $attachment_address_values[] = "current_timestamp";

                $attachment_address_fields[] = "date_modified";
                $attachment_address_values[] = "current_timestamp";
                
                if (isset($_SESSION["user_login"]))
                {
                    $attachment_address_fields[] = "user_created";
                    $attachment_address_values[] = dbquote($_SESSION["user_login"]);

                    $attachment_address_fields[] = "user_modified";
                    $attachment_address_values[] = dbquote($_SESSION["user_login"]);
                }
    
                $sql = "insert into order_file_addresses (" . join(", ", $attachment_address_fields) . ") values (" . join(", ", $attachment_address_values) . ")";
                mysql_query($sql) or dberror($sql);
		}

		if($form->value("supplier2")) {

				$address_id = $form->value("supplier2");

				$attachment_address_fields = array();
                $attachment_address_values = array();

                $attachment_address_fields[] = "order_file_address_file";
                $attachment_address_values[] = $id;

                $attachment_address_fields[] = "order_file_address_address";
                $attachment_address_values[] = $address_id;

                $attachment_address_fields[] = "date_created";
                $attachment_address_values[] = "current_timestamp";

                $attachment_address_fields[] = "date_modified";
                $attachment_address_values[] = "current_timestamp";
                
                if (isset($_SESSION["user_login"]))
                {
                    $attachment_address_fields[] = "user_created";
                    $attachment_address_values[] = dbquote($_SESSION["user_login"]);

                    $attachment_address_fields[] = "user_modified";
                    $attachment_address_values[] = dbquote($_SESSION["user_login"]);
                }
    
                $sql = "insert into order_file_addresses (" . join(", ", $attachment_address_fields) . ") values (" . join(", ", $attachment_address_values) . ")";
                mysql_query($sql) or dberror($sql);
		}
}


/********************************************************************
    update comment accessibility
*********************************************************************/
function update_comment_accessibility_info($id, $form, $check_box_names)
{
    $sql = "delete from comment_addresses where comment_address_comment = " . $id;
    mysql_query($sql) or dberror($sql);

    foreach ($check_box_names as $key=>$value)
    {	
        if ($form->value($key))
        {
            $comment_addressfields = array();
            $comment_addressvalues = array();

            $comment_addressfields[] = "comment_address_comment";
            $comment_addressvalues[] = $id;

            $comment_addressfields[] = "comment_address_address";
            $comment_addressvalues[] = $value;

            $comment_addressfields[] = "date_created";
            $comment_addressvalues[] = "current_timestamp";

            $comment_addressfields[] = "date_modified";
            $comment_addressvalues[] = "current_timestamp";
                
            if (isset($_SESSION["user_login"]))
            {
                $comment_addressfields[] = "user_created";
                $comment_addressvalues[] = dbquote($_SESSION["user_login"]);

                $comment_addressfields[] = "user_modified";
                $comment_addressvalues[] = dbquote($_SESSION["user_login"]);
            }
    
            $sql = "insert into comment_addresses (" . join(", ", $comment_addressfields) . ") values (" . join(", ", $comment_addressvalues) . ")";
                mysql_query($sql) or dberror($sql);

        }
    }
}

/********************************************************************
    delete attachment
*********************************************************************/
function delete_attachment($id)
{
    // delete access rights
    $sql = "delete from order_file_addresses where order_file_address_file = " . $id;
    mysql_query($sql) or dberror($sql);

    // delete file
    $sql = "select order_file_order, order_file_path from order_files where order_file_id = " . $id;

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        
		//check if there is a file with the same name
		//workaround for multiple submits (reload or save due to slow connections)
		
		$sql_f = "select count(order_file_id) as num_recs " . 
			     "from order_files " . 
			     "where order_file_order = " . dbquote($row["order_file_order"])  . 
			     " and order_file_path = "  . dbquote($row["order_file_path"]);

		$res_f = mysql_query($sql_f) or dberror($sql_f);
		$row_f = mysql_fetch_assoc($res_f);

		if($row_f["num_recs"] < 2 ) {
			$file = ".." . $row["order_file_path"];
			if (file_exists($file))
			{
				unlink($file);
			}
		}
    }

    // delete attachment data
    $sql = "delete from order_files where order_file_id = " . $id;
    mysql_query($sql) or dberror($sql);

	// delete order shipment documents
    $sql = "update order_shipping_documents set order_shipping_document_file_order_file_id = NULL where order_shipping_document_file_order_file_id = " . $id;
    mysql_query($sql) or dberror($sql);
}


/********************************************************************
    delete comment
*********************************************************************/
function delete_comment($id)
{
    // delete access rights
    $sql = "delete from comment_addresses where comment_address_comment = " . $id;
    mysql_query($sql) or dberror($sql);

    // delete comment data
    $sql = "delete from comments where comment_id = " . $id;
    mysql_query($sql) or dberror($sql);

}


/**************************************************************************************
    append record to the tabel actual_order_states
***************************************************************************************/
function append_order_state($order_id, $order_state_code, $type, $do_update, $keep_project_state = 0)
{
	// get order_state_id
    $sql = "select order_state_id, order_state_change_state, " .
           "order_state_order_state_type, " .
           "   order_state_delete_tasks " .
           "from order_states ".
           "left join order_state_groups on order_state_group = order_state_group_id ".
           "where order_state_code = " . dbquote($order_state_code) .
           "    and order_state_group_order_type = " .$type;

    $res = mysql_query($sql) or dberror($sql);
    
    if ($row = mysql_fetch_assoc($res))
    {
        $order_state_change_state = $row["order_state_change_state"];
        $order_state_delete_tasks = $row["order_state_delete_tasks"];
        $order_state_order_state_type = $row["order_state_order_state_type"];
        
        $order_state_fields = array();
        $order_state_values = array();
    
        // insert record into table orders

        $order_state_fields[] = "actual_order_state_order";
        $order_state_values[] = $order_id;

        $order_state_fields[] = "actual_order_state_state";
        $order_state_values[] = $row["order_state_id"];

        $order_state_fields[] = "actual_order_state_user";
        $order_state_values[] = user_id();

        $order_state_fields[] = "date_created";
        $order_state_values[] = "current_timestamp";

        $order_state_fields[] = "date_modified";
        $order_state_values[] = "current_timestamp";

        if (isset($_SESSION["user_login"]))
        {
            $order_state_fields[] = "user_created";
            $order_state_values[] = dbquote($_SESSION["user_login"]);

            $order_state_fields[] = "user_modified";
            $order_state_values[] = dbquote($_SESSION["user_login"]);
        }


        $sql = "insert into actual_order_states (" . join(", ", $order_state_fields) . ") values (" . join(", ", $order_state_values) . ")";
        mysql_query($sql) or dberror($sql);
    }

    if ($order_state_change_state == 1 and $do_update == 1)
    {
        $can_update = 1;
        
        // check if all partners have confirmed

        if($order_state_code == DELIVERY_CONFIRMED_FRW)
        {
            // delivery confirmation by forwarder
            $sql = "select count(order_item_id) as num_recs " .
                   "from order_items " .
                   "where order_item_order = " . $order_id .
                   "  and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                   "  and (order_item_arrival is null or order_item_arrival = '0000-00-00') " .
                   "  and (order_item_not_in_budget is null or order_item_not_in_budget = 0) " . 
				   " and order_item_quantity > 0";
        
        
            $res = mysql_query($sql) or dberror($sql);
    
            if ($row = mysql_fetch_assoc($res))
            {
                if($row["num_recs"] > 0)
                {
                    $can_update = 0;
                }
            }
        }
        elseif($order_state_code == REQUEST_FOR_DELIVERY_ACCEPTED)
        {
            // request for delivery accepted by forwarder
            $sql = "select count(order_item_id) as num_recs " .
                   "from order_items " .
                   "where order_item_order = " . $order_id .
                   "  and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                   "  and (order_item_expected_arrival is null or order_item_expected_arrival = '0000-00-00') " .
                   "  and (order_item_not_in_budget is null or order_item_not_in_budget = 0)" . 
				   " and order_item_quantity > 0";
        
        
            $res = mysql_query($sql) or dberror($sql);
    
            if ($row = mysql_fetch_assoc($res))
            {
                if($row["num_recs"] > 0)
                {
                    $can_update = 0;
                }
            }
        }
        elseif($order_state_code == CONFIRM_ORDER_BY_SUPPLIER)
        {
            // order confrimation by supplier
            $sql = "select count(order_item_id) as num_recs " .
                   "from order_items " .
                   "where order_item_order = " . $order_id .
                   "  and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                   "  and (order_item_ready_for_pickup is null or order_item_ready_for_pickup = '0000-00-00') " .
                   "  and (order_item_not_in_budget is null or order_item_not_in_budget = 0)" . 
				   " and order_item_quantity > 0";
        
        
            $res = mysql_query($sql) or dberror($sql);
    
            if ($row = mysql_fetch_assoc($res))
            {
                if($row["num_recs"] > 0)
                {
                    $can_update = 0;
                }
            }
        }
		elseif($order_state_code == PICK_UP_CONFIRMED)
        {
            
			// request for delivery accepted by forwarder
            $sql = "select count(order_item_id) as num_recs " .
                   "from order_items " .
                   "where order_item_order = " . $order_id .
                   "  and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                   "  and (order_item_pickup is null or order_item_pickup = '0000-00-00') " .
                   "  and (order_item_not_in_budget is null or order_item_not_in_budget = 0)" . 
				   " and order_item_quantity > 0";
        
        
            $res = mysql_query($sql) or dberror($sql);
    
            if ($row = mysql_fetch_assoc($res))
            {
                if($row["num_recs"] > 0)
                {
                    $can_update = 0;
                }
            }
        }

        // update table orders
        if($can_update == 1)
        {
			// never fall back if budget has been approved
            $sql = "select order_actual_order_state_code " .
                   "from orders where order_id = " . $order_id;

            $res = mysql_query($sql) or dberror($sql);
            $row = mysql_fetch_assoc($res);

            $actual_order_state_code = $row["order_actual_order_state_code"];
            
            if($actual_order_state_code < 620 and $keep_project_state == 0)
            {
				$sql = "update orders set order_actual_order_state_code = " . $order_state_code . " where order_id = " . $order_id;
                mysql_query($sql) or dberror($sql);
            }
            elseif($actual_order_state_code >= 620 and $keep_project_state == 0)
            {
				 $sql = "update orders set order_actual_order_state_code = " . $order_state_code . " where order_id = " . $order_id;
                 mysql_query($sql) or dberror($sql);
            }
        }

    }
}


/********************************************************************
    append records to table order_mails
*********************************************************************/
function append_mail($order_id, $recepient_id, $user_id, $text, $order_state_code, $type, $order_revision=0, $order_mail_is_cc = 0, $table = "", $table_key = 0)
{

    $text = str_replace("\n\n", "\n", $text);
	$text = str_replace("\n\n", "\n", $text);

	$sql = "select order_state_id " .
           "from order_states ".
           "left join order_state_groups on order_state_group = order_state_group_id ".
           "where order_state_code = " . dbquote($order_state_code) .
           "    and order_state_group_order_type = " .$type;

    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);

    $order_mail_fields = array();
    $order_mail_values = array();
    
    // insert record into table tasks

    $order_mail_fields[] = "order_mail_order";
    $order_mail_values[] = $order_id;

	if ($recepient_id != "")
    {
        $order_mail_fields[] = "order_mail_table";
        $order_mail_values[] = dbquote($table);
    }

	if ($recepient_id != "")
    {
        $order_mail_fields[] = "order_mail_table_key";
        $order_mail_values[] = dbquote($table_key);
    }

    if ($recepient_id != "")
    {
        $order_mail_fields[] = "order_mail_user";
        $order_mail_values[] = $recepient_id;
    }

    $order_mail_fields[] = "order_mail_from_user";
    $order_mail_values[] = $user_id;

    $order_mail_fields[] = "order_mail_text";
    $order_mail_values[] = dbquote($text);

    if ($order_state_code != "")
    {
        $order_mail_fields[] = "order_mail_order_state";
        $order_mail_values[] = $row["order_state_id"];
    }

	$order_mail_fields[] = "order_revisioned";
    $order_mail_values[] = $order_revision;

	$order_mail_fields[] = "order_mail_is_cc";
    $order_mail_values[] = $order_mail_is_cc;

	

    $order_mail_fields[] = "date_created";
    $order_mail_values[] = "current_timestamp";

    $order_mail_fields[] = "date_modified";
    $order_mail_values[] = "current_timestamp";

    if (isset($_SESSION["user_login"]))
    {
        $order_mail_fields[] = "user_created";
        $order_mail_values[] = dbquote($_SESSION["user_login"]);

        $order_mail_fields[] = "user_modified";
        $order_mail_values[] = dbquote($_SESSION["user_login"]);
    }

    $sql = "insert into order_mails (" . join(", ", $order_mail_fields) . ") values (" . join(", ", $order_mail_values) . ")";
    mysql_query($sql) or dberror($sql);

}

/********************************************************************
    append record to task list
*********************************************************************/
function append_task($order_id, $user_id, $text, $url, $due_date, $from_user, $order_state_code, $type, $keep_order_state = 0)
{

        $task_fields = array();
        $task_values = array();

        $order_state_id = null;
        $order_state_predecessor = "";
        $order_state_delete_tasks = 0;
        $order_state_manually_deleted = 0;

        // get order_state_id
        $sql = "select order_state_id, order_state_predecessor, ".
               "    order_state_delete_tasks, order_state_manually_deleted " .
               "from order_states ".
               "left join order_state_groups on order_state_group = order_state_group_id ".
               "where order_state_code = " . dbquote($order_state_code) .
               "    and order_state_group_order_type = " .$type;
        
        $res = mysql_query($sql) or dberror($sql);
    
        if ($row = mysql_fetch_assoc($res))
        {
            $order_state_id = $row["order_state_id"];
            $order_state_predecessor = $row["order_state_predecessor"];
            $order_state_delete_tasks = $row["order_state_delete_tasks"];
            $order_state_manually_deleted = $row["order_state_manually_deleted"];
        }

        // insert record into table tasks

        $task_fields[] = "task_order";
        $task_values[] = $order_id;

        $task_fields[] = "task_order_state";
        $task_values[] = $order_state_id;

        $task_fields[] = "task_user";
        $task_values[] = $user_id;

        $task_fields[] = "task_from_user";
        $task_values[] = $from_user;

        $task_fields[] = "task_text";
        $task_values[] = dbquote($text);

        $task_fields[] = "task_url";
        $task_values[] = dbquote($url);

        
		if($due_date)
		{
			$task_fields[] = "task_due_date";
			$task_values[] = dbquote($due_date);
		}

        $task_fields[] = "task_delete_task";
        $task_values[] = $order_state_delete_tasks;

        $task_fields[] = "task_manually_deleted";
        $task_values[] = $order_state_manually_deleted;

		$task_fields[] = "task_keep_order_state";
        $task_values[] = dbquote($keep_order_state);
		

        $task_fields[] = "date_created";
        $task_values[] = "current_timestamp";

        $task_fields[] = "date_modified";
        $task_values[] = "current_timestamp";

        if (isset($_SESSION["user_login"]))
        {
            $task_fields[] = "user_created";
            $task_values[] = dbquote($_SESSION["user_login"]);

            $task_fields[] = "user_modified";
            $task_values[] = dbquote($_SESSION["user_login"]);
        }

        $sql = "insert into tasks (" . join(", ", $task_fields) . ") values (" . join(", ", $task_values) . ")";
        mysql_query($sql) or dberror($sql);

}

/********************************************************************
    append record to task list set task_manually_deleted to 0
*********************************************************************/
function append_task2($order_id, $user_id, $text, $url, $due_date, $from_user, $order_state_code, $type)
{

        $task_fields = array();
        $task_values = array();

        $order_state_id = null;
        $order_state_predecessor = "";
        $order_state_delete_tasks = 0;
        $order_state_manually_deleted = 0;

        // get order_state_id
        $sql = "select order_state_id, order_state_predecessor, ".
               "    order_state_delete_tasks, order_state_manually_deleted " .
               "from order_states ".
               "left join order_state_groups on order_state_group = order_state_group_id ".
               "where order_state_code = " . dbquote($order_state_code) .
               "    and order_state_group_order_type = " .$type;
        
        $res = mysql_query($sql) or dberror($sql);
    
        if ($row = mysql_fetch_assoc($res))
        {
            $order_state_id = $row["order_state_id"];
            $order_state_predecessor = $row["order_state_predecessor"];
            $order_state_delete_tasks = $row["order_state_delete_tasks"];
            $order_state_manually_deleted = $row["order_state_manually_deleted"];
        }

        // insert record into table tasks

        $task_fields[] = "task_order";
        $task_values[] = $order_id;

        $task_fields[] = "task_order_state";
        $task_values[] = $order_state_id;

        $task_fields[] = "task_user";
        $task_values[] = $user_id;

        $task_fields[] = "task_from_user";
        $task_values[] = $from_user;

        $task_fields[] = "task_text";
        $task_values[] = dbquote($text);

        $task_fields[] = "task_url";
        $task_values[] = dbquote($url);

        $task_fields[] = "task_due_date";
        $task_values[] = dbquote($due_date);

        $task_fields[] = "task_delete_task";
        $task_values[] = $order_state_delete_tasks;

        $task_fields[] = "date_created";
        $task_values[] = "current_timestamp";

        $task_fields[] = "date_modified";
        $task_values[] = "current_timestamp";

        if (isset($_SESSION["user_login"]))
        {
            $task_fields[] = "user_created";
            $task_values[] = dbquote($_SESSION["user_login"]);

            $task_fields[] = "user_modified";
            $task_values[] = dbquote($_SESSION["user_login"]);
        }

        $sql = "insert into tasks (" . join(", ", $task_fields) . ") values (" . join(", ", $task_values) . ")";
        mysql_query($sql) or dberror($sql);

}


/********************************************************************
    delete all tasks of an order and a user
*********************************************************************/
function delete_user_tasks($order_id, $user_id, $text, $url, $due_date, $from_user, $order_state_code, $type)
{

        $order_state_id = null;
        $order_state_predecessor = "";
        // get order_state_id
        $sql = "select order_state_id, order_state_code, order_state_predecessor, ".
               "    order_state_delete_tasks, order_state_manually_deleted, order_state_delete_predecessor " .
               "from order_states ".
               "left join order_state_groups on order_state_group = order_state_group_id ".
               "where order_state_code = " . dbquote($order_state_code) .
               "    and order_state_group_order_type = " . $type;
     
        $res = mysql_query($sql) or dberror($sql);
    
        if ($row = mysql_fetch_assoc($res))
        {
            $order_state_id = $row["order_state_id"];
            $order_state_predecessor = $row["order_state_predecessor"];
            $order_state_delete_tasks = $row["order_state_delete_tasks"];
            $order_state_manually_deleted = $row["order_state_manually_deleted"];
			$order_state_delete_predecessor = $row["order_state_delete_predecessor"];
        }

        if ($order_state_delete_tasks == 1)
        {
			// deletes task when performed
            /*
			$sql = "delete from tasks ".
                   "where task_order = " . $order_id .
				   "   and (task_order_state = " . $row["order_state_id"] . " or task_order_state =  " . dbquote($order_state_predecessor) . ") " . 
                   "   and (task_manually_deleted <> 1 or task_manually_deleted is null)";

			mysql_query($sql) or dberror($sql);
			*/

			$additional_filter = "";
			if($order_state_id == 29)
			{
				$additional_filter = " or task_order_state = 30 ";
			}
			elseif($order_state_id == 103)
			{
				$additional_filter = " or task_order_state = 24 or task_order_state = 104 ";
			}
			elseif($order_state_id == 70)
			{
				$additional_filter = " or task_order_state = 38 ";
			}
			elseif($order_state_id == 71)
			{
				$additional_filter = " or task_order_state = 61 ";
			}
			elseif($order_state_id == 75)
			{
				$additional_filter = " or task_order_state = 76 ";
			}
			elseif($order_state_id == 22)
			{
				$additional_filter = " or task_order_state = 23 ";
			}
			elseif($order_state_id == 34)
			{
				$additional_filter = " or task_order_state = 33 ";
			}
			elseif($order_state_id == 52)
			{
				$additional_filter = " or task_order_state = 53 ";
			}
			elseif($order_state_id == 100)
			{
				$additional_filter = " or task_order_state = 101 ";
			}
			
			$sql = "delete from tasks ".
				   "where task_user = " . $from_user .
				   "   and task_order = " . $order_id .
				   "   and (task_order_state = " . $row["order_state_id"] . " or task_order_state =  " . dbquote($order_state_predecessor) . $additional_filter . ") " . 
				   "   and (task_manually_deleted <> 1 or task_manually_deleted is null)";
			mysql_query($sql) or dberror($sql);
			
        }

		if($order_state_delete_predecessor == 1)
		{
			//get preceeding order state in queue

			if($order_state_code >= '500' and $order_state_code < '800')
			{
				$sql = "select order_state_id, order_state_code, order_state_predecessor " .
					   " from order_states " . 
						"left join order_state_groups on order_state_group = order_state_group_id ".
					   " where order_state_code < " . dbquote($order_state_code) . 
					   "    and order_state_group_order_type = " . $type . 
					   " order by order_state_code DESC";


				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))

				{
					$sql = "delete from tasks ".
						   "where task_order_state = " . $row["order_state_predecessor"] .
						   " and task_user = " . dbquote($user_id) .
						   "   and task_order = " . $order_id;
					mysql_query($sql) or dberror($sql);
				}
			}
			else
			{
				$sql = "select order_state_id, order_state_code " .
					   " from order_states " . 
						"left join order_state_groups on order_state_group = order_state_group_id ".
					   " where order_state_code < " . dbquote($order_state_code) . 
					   "    and order_state_group_order_type = " . $type . 
					   " order by order_state_code DESC";


				$res = mysql_query($sql) or dberror($sql);
				while ($row = mysql_fetch_assoc($res))

				{
					$sql = "delete from tasks ".
						   "where task_order_state = " . $row["order_state_id"] .
						   "   and task_order = " . $order_id;
					mysql_query($sql) or dberror($sql);
				}
				
				/*
				$sql = "delete from tasks ".
					   "where date_created < " . dbquote(date("Y-m-d H:i:s")) .
					   "   and task_order = " . $order_id;

				mysql_query($sql) or dberror($sql);
				*/
			}
		}
}


/********************************************************************
    save task data
*********************************************************************/
function save_task_data($form)
{
   $task_fields = array();

   $value = dbquote(from_system_date($form->value("done_date")));
   $task_fields[] = "task_done_date = " . $value;

   $value = "current_timestamp";
   $task_fields[] = "date_modified = " . $value;

    if (isset($_SESSION["user_login"]))
    {
       $value = dbquote($_SESSION["user_login"]);
       $task_fields[] = "user_modified = " . $value;
    }

    $sql = "update tasks set " . join(", ",$task_fields) . " where task_id = " .  $form->value("tid");
    mysql_query($sql) or dberror($sql);
}


/********************************************************************
    update task done date
*********************************************************************/
function  update_task_done_date($id)
{
   $task_fields = array();

   $value = "current_timestamp";
   $task_fields[] = "task_done_date = " . $value;

   $value = "current_timestamp";
   $task_fields[] = "date_modified = " . $value;

    if (isset($_SESSION["user_login"]))
    {
       $value = dbquote($_SESSION["user_login"]);
       $task_fields[] = "user_modified = " . $value;
    }

    $sql = "update tasks set " . join(", ",$task_fields) . " where task_id = " .  $id;
    mysql_query($sql) or dberror($sql);
}


/********************************************************************
    delete task
*********************************************************************/
function  delete_task($id)
{

    $sql = "delete from tasks where task_id = " .  $id;
    mysql_query($sql) or dberror($sql);
}

/********************************************************************
    set archive date
*********************************************************************/
function  set_archive_date($order_id)
{
   $order_fields = array();

   $value = "current_timestamp";
   $order_fields[] = "order_archive_date = " . $value;

   $value = "current_timestamp";
   $order_fields[] = "date_modified = " . $value;

    if (isset($_SESSION["user_login"]))
    {
       $value = dbquote($_SESSION["user_login"]);
       $order_fields[] = "user_modified = " . $value;
    }

    $sql = "update orders set " . join(", ",$order_fields) . " where order_id = " .  $order_id;
    mysql_query($sql) or dberror($sql);

    // delete all pending tasks
    $sql = "delete from tasks where task_order = ". $order_id;
    mysql_query($sql) or dberror($sql);
}

/********************************************************************
    update visibility for supplöiers
    when project booklet is approved by client
*********************************************************************/
function set_visibility_for_suppliers($order_id = 0, $address_id = 0)
{
    $sql = "update orders set order_show_to_suppliers = 1 where order_id = " .  $order_id;
    mysql_query($sql) or dberror($sql);

    if ($address_id)
    {
        $sql = "update order_items ".
               "set order_item_show_to_suppliers = 1 ".
               "where order_item_order = " .  $order_id .
               "    and order_item_supplier_address = " . $address_id;

        mysql_query($sql) or dberror($sql);
    }
    else
    {
        $sql = "update order_items ".
               "set order_item_show_to_suppliers = 1 ".
               "where order_item_order = " .  $order_id;

        mysql_query($sql) or dberror($sql);
    }

}

/********************************************************************
    update visibility in delivery schedule
    when budget is approved by client
*********************************************************************/
function set_visibility_in_delivery_schedule($order_id)
{
    $sql = "update orders set order_show_in_delivery = 1 where order_id = " .  $order_id;
    mysql_query($sql) or dberror($sql);

}

/********************************************************************
    set order to cancelled
*********************************************************************/
function set_order_to_cancelled($order_id)
{
    $sql = "update orders set order_cancelled = 1 where order_id = " .  $order_id;
    mysql_query($sql) or dberror($sql);

	$sql = "update projects set project_state = 6 where project_order = " .  $order_id;
    mysql_query($sql) or dberror($sql);

}


/********************************************************************
    update budget state
*********************************************************************/
function update_budget_state($order_id, $state)
{
    $sql = "update orders set order_budget_is_locked = '" . $state . "' where order_id = " .  $order_id;
    mysql_query($sql) or dberror($sql);

	//update user_tracking
	$text = "User has changed budget state of order " . $order_id . " to " . $state;
	$sql = 'insert into user_tracking (user_tracking_user, user_tracking_track, date_created, user_created) VALUES (' .
		   user_id() . ', "' . $text . '", "' . date("Y-m-d H:i:s") . '", "' . user_login() . '")';

	 mysql_query($sql) or dberror($sql);


}


/********************************************************************
    freeze budget
*********************************************************************/
function freeze_budget_hq_supplied_items($order_id)
{
    
	//update order items with predefined cost_groups
	$sql = "select order_item_id, order_item_item, item_cost_group, item_costmonitoring_group " . 
		   "from order_items " . 
		   "left join items on item_id = order_item_item " .
		   "where order_item_order = " . $order_id . 
		   "  and order_item_cost_group = 0 and order_item_not_in_budget <> 1 " .
		   "  and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION . " or order_item_type >= " . ITEM_TYPE_LOCALCONSTRUCTIONCOST . ") ";


	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$sql_u = "update order_items Set order_item_cost_group =  " . dbquote($row["item_cost_group"]) . " " . 
				 " where (order_item_cost_group is NULL or order_item_cost_group = 0) " .
				 "  and order_item_id = " . $row["order_item_id"];

		mysql_query($sql_u) or dberror($sql_u);

		/* cost monitoring group was removed by Katrin Eckert on January 26th 2016
		$sql_u = "update order_items Set order_items_costmonitoring_group =  " . dbquote($row["item_costmonitoring_group"]) . " " . 
				 " where (order_items_costmonitoring_group is NULL or order_items_costmonitoring_group = 0) " .
				 "  and order_item_id = " . $row["order_item_id"];

		mysql_query($sql_u) or dberror($sql_u);
		*/
	}
	
	$budget = 0;
	$cost_groups = array();
	$cost_groups_lc = array();
	$cost_groups[2] = 0; // HQ Supplied items
	$cost_groups[6] = 0; // Freight Charges
	$cost_groups_lc[2] = 0; // HQ Supplied items in LOC
	$cost_groups_lc[6] = 0; // Freight Charges in LOC


    $sql =  "select order_item_id, order_item_text, order_item_quantity, order_item_cost_group, " .
            "order_item_system_price, order_item_client_price " .
            "from order_items " . 
            " where order_item_cost_group in (2) and order_item_not_in_budget <> 1 " .
            "  and order_item_order = " . $order_id;


    $res = mysql_query($sql) or dberror($sql);
    while($row = mysql_fetch_assoc($res))
    {
        if($row["order_item_quantity"] > 0)
        {
			$budget = $budget + $row["order_item_quantity"] * $row["order_item_system_price"];
			
			if(array_key_exists($row["order_item_cost_group"], $cost_groups))
			{
				$cost_groups[$row["order_item_cost_group"]] = $cost_groups[$row["order_item_cost_group"]]  + ($row["order_item_quantity"] * $row["order_item_system_price"]);
			}
			else
			{
				$cost_groups[$row["order_item_cost_group"]] = $row["order_item_quantity"] * $row["order_item_system_price"];
			}

			if(array_key_exists($row["order_item_cost_group"], $cost_groups_lc))
			{
				$cost_groups_lc[$row["order_item_cost_group"]] = $cost_groups_lc[$row["order_item_cost_group"]]  + ($row["order_item_quantity"] * $row["order_item_client_price"]);
			}
			else
			{
				$cost_groups_lc[$row["order_item_cost_group"]] = $row["order_item_quantity"] * $row["order_item_client_price"];
			}
        }
        else
        {
            $budget = $budget + $row["order_item_system_price"];

			if(array_key_exists($row["order_item_cost_group"], $cost_groups))
			{
				$cost_groups[$row["order_item_cost_group"]] = $cost_groups[$row["order_item_cost_group"]]  +  $row["order_item_system_price"];
			}
			else
			{
				$cost_groups[$row["order_item_cost_group"]] = $row["order_item_system_price"];
			}

			if(array_key_exists($row["order_item_cost_group"], $cost_groups_lc))
			{
				$cost_groups_lc[$row["order_item_cost_group"]] = $cost_groups_lc[$row["order_item_cost_group"]]  +  $row["order_item_client_price"];
			}
			else
			{
				$cost_groups_lc[$row["order_item_cost_group"]] = $row["order_item_client_price"];
			}
        }

        $quantity = $row["order_item_quantity"];
        $sprice = $row["order_item_system_price"];
        $cprice = $row["order_item_client_price"];

        if(!$quantity){$quantity = 0;}
        if(!$sprice){$sprice = 0;}
        if(!$cprice){$cprice = 0;}

        $sql_u = "Update order_items set " .
                 "order_item_in_freezed_hq_budget = 1, " .
                 "order_item_quantity_hq_freezed = " . $quantity  . ", " .
                 "order_item_system_price_hq_freezed = " . $sprice . ", " .
                 "order_item_client_price_hq_freezed = " . $cprice . ", " .
			     "order_item_in_freezed_budget = 1, " .
                 "order_item_quantity_freezed = " . $quantity  . ", " .
                 "order_item_system_price_freezed = " . $sprice . ", " .
                 "order_item_client_price_freezed = " . $cprice . " " .
                 "where order_item_id = " . $row["order_item_id"];
        
        $result = mysql_query($sql_u) or dberror($sql_u);
    }


    //check if the cost monitoring sheet exists already
    $sql = "select count(project_cost_id) as num_recs " .
           "from project_costs " .
           "where project_cost_order = "  . $order_id; 

    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);

    if($row["num_recs"] == 0)
    {
        
        $fields = array();
        $values = array();

        $fields[] = "project_cost_order";
        $values[] = $order_id;

        $fields[] = "date_created";
        $values[] = dbquote(date("Y-m-d"));

        $fields[] = "date_modified";
        $values[] = dbquote(date("Y-m-d"));

        $fields[] = "user_created";
        $values[] = dbquote(user_login());

        $fields[] = "user_modified";
        $values[] = dbquote(user_login());
        
        $sql = "insert into project_costs (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

        mysql_query($sql) or dberror($sql);
    }

	//update costgroups
	$sql = "update project_costs " .
           "set project_cost_hq_budget =  " . $budget . ", " .
		   "project_cost_hq_supplied = " . $cost_groups[2] . ", " .
		   "project_cost_hq_supplied_lc = " . $cost_groups_lc[2] . ", " .
		   "project_cost_freight_charges = " . $cost_groups[6] . ", " .
		   "project_cost_freight_charges_lc = " . $cost_groups_lc[6] . 
           " where project_cost_order = " . $order_id;


	mysql_query($sql) or dberror($sql);


    $sql = "update orders " .
           "set order_hq_budget_freezed_date =  " . dbquote(date("Y-m-d")) . 
           " where order_id = " . $order_id;

    mysql_query($sql) or dberror($sql);


	//update user_tracking
	$text = "User has freezed budget for HQ supplied items of order " . $order_id;
	$sql = 'insert into user_tracking (user_tracking_user, user_tracking_track, date_created, user_created) VALUES (' .
		   user_id() . ', "' . $text . '", "' . date("Y-m-d H:i:s") . '", "' . user_login() . '")';

	mysql_query($sql) or dberror($sql);

}


/********************************************************************
    freeze budget
*********************************************************************/
function freeze_budget($order_id)
{
    
	//update order items with predefined cost_groups
	$sql = "select order_item_id, order_item_item, item_cost_group, item_costmonitoring_group " . 
		   "from order_items " . 
		   "left join items on item_id = order_item_item " .
		   "where order_item_order = " . $order_id . 
		   "  and order_item_cost_group = 0 and order_item_not_in_budget <> 1 " .
		   "  and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION . " or order_item_type >= " . ITEM_TYPE_LOCALCONSTRUCTIONCOST . ") ";


	/*
	$sql = "select item_code, order_item_id, order_item_item, item_cost_group, item_costmonitoring_group " . 
		   "from order_items " . 
		   "left join items on item_id = order_item_item " .
		   "where order_item_order = " . $order_id . 
		   "  and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION . " or order_item_type >= " . ITEM_TYPE_LOCALCONSTRUCTIONCOST . ") ";

	*/

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$sql_u = "update order_items Set order_item_cost_group =  " . dbquote($row["item_cost_group"]) . " " . 
				 " where (order_item_cost_group is NULL or order_item_cost_group = 0) " .
				 "  and order_item_id = " . $row["order_item_id"];

		mysql_query($sql_u) or dberror($sql_u);

		$sql_u = "update order_items Set order_items_costmonitoring_group =  " . dbquote($row["item_costmonitoring_group"]) . " " . 
				 " where (order_items_costmonitoring_group is NULL or order_items_costmonitoring_group = 0) " .
				 "  and order_item_id = " . $row["order_item_id"];

		mysql_query($sql_u) or dberror($sql_u);
	}
	
	$budget = 0;
	$cost_groups = array();
	$cost_groups_lc = array();
	$cost_groups[2] = 0; // HQ Supplied items
	$cost_groups[6] = 0; // Freight Charges
	$cost_groups_lc[2] = 0; // HQ Supplied items in LOC
	$cost_groups_lc[6] = 0; // Freight Charges in LOC


    $sql =  "select order_item_id, order_item_text, order_item_quantity, order_item_cost_group, " .
            "order_item_system_price, order_item_client_price " .
            "from order_items " . 
            " where order_item_not_in_budget <> 1 " .
            "  and order_item_order = " . $order_id;

    $res = mysql_query($sql) or dberror($sql);
    while($row = mysql_fetch_assoc($res))
    {
        if($row["order_item_quantity"] > 0)
        {
			$budget = $budget + $row["order_item_quantity"] * $row["order_item_system_price"];
			
			if(array_key_exists($row["order_item_cost_group"], $cost_groups))
			{
				$cost_groups[$row["order_item_cost_group"]] = $cost_groups[$row["order_item_cost_group"]]  + ($row["order_item_quantity"] * $row["order_item_system_price"]);
			}
			else
			{
				$cost_groups[$row["order_item_cost_group"]] = $row["order_item_quantity"] * $row["order_item_system_price"];
			}

			if(array_key_exists($row["order_item_cost_group"], $cost_groups_lc))
			{
				$cost_groups_lc[$row["order_item_cost_group"]] = $cost_groups_lc[$row["order_item_cost_group"]]  + ($row["order_item_quantity"] * $row["order_item_client_price"]);
			}
			else
			{
				$cost_groups_lc[$row["order_item_cost_group"]] = $row["order_item_quantity"] * $row["order_item_client_price"];
			}
        }
        else
        {
            $budget = $budget + $row["order_item_system_price"];

			if(array_key_exists($row["order_item_cost_group"], $cost_groups))
			{
				$cost_groups[$row["order_item_cost_group"]] = $cost_groups[$row["order_item_cost_group"]]  +  $row["order_item_system_price"];
			}
			else
			{
				$cost_groups[$row["order_item_cost_group"]] = $row["order_item_system_price"];
			}

			if(array_key_exists($row["order_item_cost_group"], $cost_groups_lc))
			{
				$cost_groups_lc[$row["order_item_cost_group"]] = $cost_groups_lc[$row["order_item_cost_group"]]  +  $row["order_item_client_price"];
			}
			else
			{
				$cost_groups_lc[$row["order_item_cost_group"]] = $row["order_item_client_price"];
			}
        }

        $quantity = $row["order_item_quantity"];
        $sprice = $row["order_item_system_price"];
        $cprice = $row["order_item_client_price"];

        if(!$quantity){$quantity = 0;}
        if(!$sprice){$sprice = 0;}
        if(!$cprice){$cprice = 0;}

        $sql_u = "Update order_items set " .
                 "order_item_in_freezed_budget = 1, " .
                 "order_item_quantity_freezed = " . $quantity  . ", " .
                 "order_item_system_price_freezed = " . $sprice . ", " .
                 "order_item_client_price_freezed = " . $cprice . " " .
                 "where order_item_id = " . $row["order_item_id"];
        
        $result = mysql_query($sql_u) or dberror($sql_u);
    }


    //check if the cost monitoring sheet exists already
    $sql = "select count(project_cost_id) as num_recs " .
           "from project_costs " .
           "where project_cost_order = "  . $order_id; 

    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);

    if($row["num_recs"] == 0)
    {
        
        $fields = array();
        $values = array();

        $fields[] = "project_cost_order";
        $values[] = $order_id;

        $fields[] = "date_created";
        $values[] = dbquote(date("Y-m-d"));

        $fields[] = "date_modified";
        $values[] = dbquote(date("Y-m-d"));

        $fields[] = "user_created";
        $values[] = dbquote(user_login());

        $fields[] = "user_modified";
        $values[] = dbquote(user_login());
        
        $sql = "insert into project_costs (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

        mysql_query($sql) or dberror($sql);
    }

	//update costgroups
	$sql = "update project_costs " .
           "set project_cost_budget =  " . $budget . ", " .
		   "project_cost_hq_supplied = " . $cost_groups[2] . ", " .
		   "project_cost_hq_supplied_lc = " . $cost_groups_lc[2] . ", " .
		   "project_cost_freight_charges = " . $cost_groups[6] . ", " .
		   "project_cost_freight_charges_lc = " . $cost_groups_lc[6] . 
           " where project_cost_order = " . $order_id;


	mysql_query($sql) or dberror($sql);


    $sql = "update orders " .
           "set order_budget_freezed_date =  " . dbquote(date("Y-m-d")) . 
           " where order_id = " . $order_id;

    mysql_query($sql) or dberror($sql);



	//new version of costsheets
	$sql = "select project_id from projects " . 
		   " where project_order = " . dbquote($order_id);
	$res = mysql_query($sql) or dberror($sql);
    if($row = mysql_fetch_assoc($res))
    {
		$sql = "update costsheets set costsheet_budget_approved_amount = costsheet_budget_amount " . 
			   " where costsheet_is_in_budget = 1 and costsheet_version = 0
			and costsheet_project_id = " . $row["project_id"];

		 mysql_query($sql) or dberror($sql);
	}


	//update user_tracking
	$text = "User has freezed budget of order " . $order_id;
	$sql = 'insert into user_tracking (user_tracking_user, user_tracking_track, date_created, user_created) VALUES (' .
		   user_id() . ', "' . $text . '", "' . date("Y-m-d H:i:s") . '", "' . user_login() . '")';

	mysql_query($sql) or dberror($sql);


}


/********************************************************************
    update cost groups
*********************************************************************/
function project_sheet_update_cost_groups($order_id)
{
	//update order items with predefined cost_groups
	$sql = "select order_item_id, order_item_item, item_cost_group, item_costmonitoring_group " . 
		   "from order_items " . 
		   "left join items on item_id = order_item_item " .
		   "where order_item_order = " . $order_id . 
		   "  and order_item_not_in_budget <> 1 " .
		   "  and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION . " or order_item_type >= " . ITEM_TYPE_LOCALCONSTRUCTIONCOST . ") ";


	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$sql_u = "update order_items Set order_item_cost_group =  " . dbquote($row["item_cost_group"]) . " " . 
				 " where (order_item_cost_group is NULL or order_item_cost_group = 0) " .
				 "  and order_item_id = " . $row["order_item_id"];

		mysql_query($sql_u) or dberror($sql_u);

		/* cost monitoring group was removed by Katrin Eckert on January 26th 2016
		$sql_u = "update order_items Set order_items_costmonitoring_group =  " . dbquote($row["item_costmonitoring_group"]) . " " . 
				 " where (order_items_costmonitoring_group is NULL or order_items_costmonitoring_group = 0) " .
				 "  and order_item_id = " . $row["order_item_id"];

		mysql_query($sql_u) or dberror($sql_u);
		*/
	}
	
	$budget = 0;
	$cost_groups = array();
	$cost_groups_lc = array();
	$cost_groups[2] = 0; // HQ Supplied items
	$cost_groups[6] = 0; // Freight Charges
	$cost_groups_lc[2] = 0; // HQ Supplied items in LOC
	$cost_groups_lc[6] = 0; // Freight Charges in LOC


    $sql =  "select order_item_id, order_item_quantity, order_item_cost_group, " .
            "order_item_system_price, order_item_client_price " .
            "from order_items " . 
            " where order_item_not_in_budget <> 1 " .
            "  and order_item_order = " . $order_id;

    $res = mysql_query($sql) or dberror($sql);
    while($row = mysql_fetch_assoc($res))
    {
        if($row["order_item_quantity"] > 0)
        {
            $budget = $budget + $row["order_item_quantity"] * $row["order_item_system_price"];
			
			if(array_key_exists($row["order_item_cost_group"], $cost_groups))
			{
				$cost_groups[$row["order_item_cost_group"]] = $cost_groups[$row["order_item_cost_group"]]  + ($row["order_item_quantity"] * $row["order_item_system_price"]);
			}
			else
			{
				$cost_groups[$row["order_item_cost_group"]] = $row["order_item_quantity"] * $row["order_item_system_price"];
			}

			if(array_key_exists($row["order_item_cost_group"], $cost_groups_lc))
			{
				$cost_groups_lc[$row["order_item_cost_group"]] = $cost_groups_lc[$row["order_item_cost_group"]]  + ($row["order_item_quantity"] * $row["order_item_client_price"]);
			}
			else
			{
				$cost_groups_lc[$row["order_item_cost_group"]] = $row["order_item_quantity"] * $row["order_item_client_price"];
			}
        }
        else
        {
            $budget = $budget + $row["order_item_system_price"];

			if(array_key_exists($row["order_item_cost_group"], $cost_groups))
			{
				$cost_groups[$row["order_item_cost_group"]] = $cost_groups[$row["order_item_cost_group"]]  +  $row["order_item_system_price"];
			}
			else
			{
				$cost_groups[$row["order_item_cost_group"]] = $row["order_item_system_price"];
			}

			if(array_key_exists($row["order_item_cost_group"], $cost_groups_lc))
			{
				$cost_groups_lc[$row["order_item_cost_group"]] = $cost_groups_lc[$row["order_item_cost_group"]]  +  $row["order_item_client_price"];
			}
			else
			{
				$cost_groups_lc[$row["order_item_cost_group"]] = $row["order_item_client_price"];
			}
        }

    }


	//update costgroups
	$sql = "update project_costs " .
           "set project_cost_budget =  " . $budget . ", " .
		   "project_cost_hq_supplied = " . $cost_groups[2] . ", " .
		   "project_cost_hq_supplied_lc = " . $cost_groups_lc[2] . ", " .
		   "project_cost_freight_charges = " . $cost_groups[6] . ", " .
		   "project_cost_freight_charges_lc = " . $cost_groups_lc[6] . 
           " where project_cost_order = " . $order_id;


	mysql_query($sql) or dberror($sql);


    
	//??? unclear why we put these lines
	/*
	$sql = "update orders " .
           "set order_budget_freezed_date =  " . dbquote(date("Y-m-d")) . 
           " where order_id = " . $order_id;

    mysql_query($sql) or dberror($sql);
	*/

}

/********************************************************************
    delete design objective items of a project
*********************************************************************/
function delete_design_objective_items($project_id)
{
    $sql = "delete from project_items where project_item_project = " . $project_id;
    mysql_query($sql) or dberror($sql);

}

/********************************************************************
    update project type
*********************************************************************/
function project_update_project_product_line($project_id, $project_line)
{
    $sql = "update projects set project_product_line = " . $project_line . " where project_id = " .  $project_id;
    mysql_query($sql) or dberror($sql);

}

/********************************************************************
    delete one item of an order
*********************************************************************/
function order_delete_item($order_id = 0, $order_item_id = 0)
{
	
	$list_of_materials_was_changed = false;
	$is_hq_supplied_item = false;

	$sql = "select order_item_id, order_item_item, order_item_quantity, order_item_type, " . 
		   " order_item_text, order_item_cost_group " .
           "from order_items " .
           "where order_item_order = " . dbquote($order_id) . 
		   " and order_item_id = " . dbquote($order_item_id);
        
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        delete_record("order_items", $order_item_id); // cascaded deletion

		//add tracking information to table order_item_trackings
		if($row["order_item_quantity"] > 0)
		{
			$result = track_change_in_order_items('Delete item in list of materials', 'Quantity', $order_id, $row["order_item_id"], $row["order_item_item"], $row["order_item_type"], $row["order_item_text"], $row["order_item_quantity"], 0);
			$list_of_materials_was_changed = true;

			//check if item is a hq supplied item
			if($row["order_item_cost_group"] == 2 
			   or $row["order_item_cost_group"] == 6)
			{
				$is_hq_supplied_item = true;
			}
		}
    }

	//send mails on change of the list of materials
	if(param("pid") and $list_of_materials_was_changed == true)
	{
		$result = track_change_in_order_items_send_mail(param("pid"), $is_hq_supplied_item);
	}

}

/********************************************************************
    delete all items of an order
*********************************************************************/
function order_delete_all_items($order_id = 0, $cost_group_ids = 0)
{
    
	$list_of_materials_was_changed = false;
	$is_hq_supplied_item = false;
	
	$filter = " and order_item_cost_group in(" . $cost_group_ids .")";
		
	$sql = "select order_item_id, order_item_item, order_item_quantity, " . 
		   " order_item_type, order_item_text, order_item_cost_group " .
           "from order_items " .
           "where order_item_order = " . $order_id . " " . $filter;
        
    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $item_id = $row["order_item_id"];
        delete_record("order_items", $item_id); // cascaded deletion


		//add tracking information to table order_item_trackings
		if($row["order_item_quantity"] > 0)
		{
			$result = track_change_in_order_items('Delete item in list of materials', 'Quantity', param("oid"), $row["order_item_id"], $row["order_item_item"], $row["order_item_type"], $row["order_item_text"], $row["order_item_quantity"], 0);
			$list_of_materials_was_changed = true;

			//check if item is a hq supplied item
			if($row["order_item_cost_group"] == 2 
			   or $row["order_item_cost_group"] == 6)
			{
				$is_hq_supplied_item = true;
			}
		}
    }


	//send mails on change of the list of materials
	if(param("pid") and $list_of_materials_was_changed == true)
	{
		$result = track_change_in_order_items_send_mail(param("pid"), $is_hq_supplied_item);
	}
}

/********************************************************************
    unfreeze project budget of hq supplied items
*********************************************************************/
function unfreeze_project_hq_budget($order_id)
{

    $sql = "update orders set order_hq_budget_is_locked = 0, order_budget_is_locked = 0 where order_id = " .  $order_id;
    mysql_query($sql) or dberror($sql);

	//update user_tracking
	$text = "User has changed budget state of order " . $order_id . " to 0 by unfreezing the budget for hq supplied items";
	$sql = 'insert into user_tracking (user_tracking_user, user_tracking_track, date_created, user_created) VALUES (' .
		   user_id() . ', "' . $text . '", "' . date("Y-m-d H:i:s") . '", "' . user_login() . '")';

	 mysql_query($sql) or dberror($sql);

	
    $sql = "update order_items set " .
           "order_item_in_freezed_hq_budget = 0, " .
           "order_item_quantity_hq_freezed = '0000-00-00', " .
           "order_item_system_price_hq_freezed = NULL, " .
           "order_item_client_price_hq_freezed = NULL " .
           " where order_item_cost_group in (2) and order_item_order = " . $order_id;

	mysql_query($sql) or dberror($sql);


	
    $sql = "update orders " .
           "set order_hq_budget_freezed_date =  Null, order_hq_budget_unfreezed_date = '" . date('Y-m-d') . "', order_hq_budget_unfreezed_by = " . user_id() . 
           " where order_id = " . $order_id;

    mysql_query($sql) or dberror($sql);          

}

/********************************************************************
    unfreeze budget
*********************************************************************/
function unfreeze_project_budget($order_id)
{

    $sql = "update orders set order_budget_is_locked = 0 where order_id = " .  $order_id;
    mysql_query($sql) or dberror($sql);

	//update user_tracking
	$text = "User has changed budget state of order " . $order_id . " to 0 by unfreezing the budget";
	$sql = 'insert into user_tracking (user_tracking_user, user_tracking_track, date_created, user_created) VALUES (' .
		   user_id() . ', "' . $text . '", "' . date("Y-m-d H:i:s") . '", "' . user_login() . '")';

	 mysql_query($sql) or dberror($sql);


    $sql = "update order_items set " .
           "order_item_in_freezed_budget = 0, " .
           "order_item_quantity_freezed = '0000-00-00', " .
           "order_item_system_price_freezed = NULL, " .
           "order_item_client_price_freezed = NULL " .
           " where order_item_order = " . $order_id;

    mysql_query($sql) or dberror($sql);


    $sql = "update orders " .
           "set order_budget_freezed_date =  Null, order_budget_unfreezed_date = '" . date('Y-m-d') . "', order_budget_unfreezed_by = " . user_id() . 
           " where order_id = " . $order_id;

    mysql_query($sql) or dberror($sql);
	


	//new version of costsheets
	$sql = "select project_id from projects " . 
		   " where project_order = " . dbquote($order_id);
	$res = mysql_query($sql) or dberror($sql);
    if($row = mysql_fetch_assoc($res))
    {
		$sql = "update costsheets set costsheet_budget_approved_amount = 0 " . 
			   " where costsheet_is_in_budget = 1 and costsheet_version = 0
			and costsheet_project_id = " . $row["project_id"];

		 mysql_query($sql) or dberror($sql);
	}

}


/********************************************************************
    Send mail notifications for projects
*********************************************************************/
function project_send_mail_notifications($project_id)
{

	$recipients_already_served = array();
	$recepients = array();
	$sql = "select project_projectkind, project_number, project_product_line, project_postype, " .
		   "project_order, order_user, project_cost_type, order_client_address, " .
		   "order_shop_address_company, order_shop_address_place, order_shop_address_country, country_name, " . 
		   "user_firstname, user_name, user_email, user_id " . 
		   "from projects " . 
		   "left join orders on order_id = project_order " .
		   "left join project_costs on project_cost_order = project_order " . 
		   "left join countries on country_id = order_shop_address_country " .
		   "left join users on user_id = order_user " .
		   "where project_id = " . $project_id;

	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		
		//send mail to project leader and logistics coordinator
		
		$sql_a = "select project_number, project_postype, project_order, order_user, " .
				   "order_shop_address_company, order_shop_address_place, country_name, " .
				   "user_firstname, user_name, user_email, user_id, project_cost_type " . 
				   "from projects " . 
				   "left join orders on order_id = project_order " .
				   "left join project_costs on project_cost_order = project_order " . 
				   "left join countries on country_id = order_shop_address_country " .
				   "left join users on user_id = project_retail_coordinator " .
				   "where project_id = " . $project_id;

		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			$project_legal_type = $row_a["project_cost_type"];
			if($project_legal_type != 1)
			{
				$project_legal_type = 0;
			}
			$project_postype = $row_a["project_postype"];
			$order_id = $row_a["project_order"];
			$order_user = $row_a["order_user"];
			$recipient_user_ids = array();

			$subject = MAIL_SUBJECT_PREFIX . ": New project request - Project " . $row_a["project_number"] . ", " . $row_a["country_name"] . ", " .  $row_a["order_shop_address_company"];

			$sender_email = $row_a["user_email"];
			$sender_name =  $row_a["user_name"] . " " . $row_a["user_firstname"];

			$bodytext0 = "You were assigned as the project leader for this project " . "\n" . $row_a["project_number"] . ", " . $row_a["country_name"] . ", " .  $row_a["order_shop_address_company"];

			$mailaddress = 0;

			$mail = new Mail();
			$mail->set_subject($subject);
			$mail->set_sender($sender_email, $sender_name);

			$recipient_user_ids[$row_a["user_email"]] = $row_a["user_id"];
			foreach($recipient_user_ids as $key=>$user_id)
			{
				$mail->add_recipient($key);
				$mailaddress = 1;
			}

			$link ="project_view_client_data.php?pid=" . $project_id;
			$bodytext = $bodytext0 . "\nclick below to have direct access to the project:\n";
			$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
			$mail->add_text($bodytext);
			
			if($mailaddress == 1)
			{
				$result = $mail->send();
				
				foreach($recipient_user_ids as $key=>$user_id)
				{
					if($user_id > 0)
					{
						append_mail($order_id, $user_id,  user_id(), $bodytext, "100", 1,0);
					}
				}
			}
		}

		//logistics coordinator
		$sql_a = "select project_number, project_postype, project_order, " . 
			       "order_user, order_client_address, " .
				   "order_shop_address_company, order_shop_address_place, country_name, " .
				   "user_firstname, user_name, user_email, user_id, project_cost_type " . 
				   "from projects " . 
				   "left join orders on order_id = project_order " .
				   "left join project_costs on project_cost_order = project_order " . 
				   "left join countries on country_id = order_shop_address_country " .
				   "left join users on user_id = order_retail_operator " .
				   "where project_id = " . $project_id;

		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{
			$project_legal_type = $row_a["project_cost_type"];
			if($project_legal_type != 1)
			{
				$project_legal_type = 0;
			}
			$project_postype = $row_a["project_postype"];
			$order_id = $row_a["project_order"];
			$order_user = $row_a["order_user"];
			$recipient_user_ids = array();

			$subject = MAIL_SUBJECT_PREFIX . ": New project request - Project " . $row_a["project_number"] . ", " . $row_a["country_name"] . ", " .  $row_a["order_shop_address_company"];

			$sender_email = $row_a["user_email"];
			$sender_name =  $row_a["user_name"] . " " . $row_a["user_firstname"];

			$bodytext0 = "You were assigned as the logistics coordinator for this project " . "\n" . $row_a["project_number"] . ", " . $row_a["country_name"] . ", " .  $row_a["order_shop_address_company"];

			$mailaddress = 0;

			$mail = new Mail();
			$mail->set_subject($subject);
			$mail->set_sender($sender_email, $sender_name);

			$recipient_user_ids[$row_a["user_email"]] = $row_a["user_id"];
			foreach($recipient_user_ids as $key=>$user_id)
			{
				$mail->add_recipient($key);
				$mailaddress = 1;
			}

			$link ="project_view_client_data.php?pid=" . $project_id;
			$bodytext = $bodytext0 . "\nclick below to have direct access to the project:\n";
			$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
			$mail->add_text($bodytext);
			
			if($mailaddress == 1)
			{
				$result = $mail->send();
				
				foreach($recipient_user_ids as $key=>$user_id)
				{
					if($user_id > 0)
					{
						append_mail($order_id, $user_id,  user_id(), $bodytext, "100", 1,0);
					}
				}
			}
		}
		
		
		
		$recipients_already_served = array();
		if($row["project_projectkind"] == 5 
			or $row["project_projectkind"] == 4
			or $row["project_projectkind"] == 8) // lease renewal or take over or PopUp
		{
			$project_legal_type = $row["project_cost_type"];
			if($project_legal_type != 1)
			{
				$project_legal_type = 0;
			}
			$project_kind = $row["project_projectkind"];
			$pos_country = $row["order_shop_address_country"];
			$project_postype = $row["project_postype"];
			$project_product_line = $row["project_product_line"];
			$project_number = $row["project_number"];
			$client_address_id = $row["order_client_address"];
			
			$sender_email = $row["user_email"];
			$sender_name =  $row["user_name"] . " " . $row["user_firstname"];
			$order_id = $row["project_order"];
			$order_user = $row["order_user"];


			//users for regional responsabilities
			$regional_responsable_reciepients = get_regional_responsible($project_legal_type, $client_address_id);

			$recipient_user_ids = array();
			$cc_recipients = array();
			$mailaddress = 0;
			
			if($project_kind == 4) {
				$subject = MAIL_SUBJECT_PREFIX . ": Take Over - Project " . $project_number . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];

				$bodytext0 = "A new take over project has been added by " . $sender_name;
			}
			elseif($project_kind == 8) {
				$subject = MAIL_SUBJECT_PREFIX . ": PopUp - Project " . $project_number . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];

				$bodytext0 = "A new popup project has been added by " . $sender_name;
			}
			else
			{
				$subject = MAIL_SUBJECT_PREFIX . ": Lease Renewal - Project " . $project_number . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];

				$bodytext0 = "A new lease renewal project has been added by " . $sender_name;
			}

			//send mal to project type notification recipients
			$sql = 'select * from projectkinds ' . 
				   'where projectkind_id = ' . $project_kind . 
				   ' and (projectkind_email1 is not null or projectkind_email2 is not null)';
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$recipient1 = $row["projectkind_email1"];
				$recipient2 = $row["projectkind_email2"];
				$recipient3 = $row["projectkind_email3"];
				
				if($recipient1)
				{
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($recipient1);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[strtolower($recipient1)] = $row_u["user_id"];
						$tmp = get_cc_deputy_by_user_id($row_u["user_id"]);
						foreach($tmp as $key=>$cc_email)
						{
							$cc_recipients[strtolower($key)] = strtolower($cc_email);
						}
					}
					else
					{
						$recipient_user_ids[$recipient1] = 0;
					}
					$mailaddress = 1;
					
				}

				if($recipient2)
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($recipient2);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[strtolower($recipient2)] = $row_u["user_id"];
						$tmp = get_cc_deputy_by_user_id($row_u["user_id"]);
						foreach($tmp as $key=>$cc_email)
						{
							$cc_recipients[strtolower($key)] = strtolower($cc_email);
						}
					}
					else
					{
						$recipient_user_ids[$recipient2] = 0;
					}
					$mailaddress = 1;
					
				}

				if($recipient3)
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($recipient3);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[strtolower($recipient3)] = $row_u["user_id"];
						$tmp = get_cc_deputy_by_user_id($row_u["user_id"]);
						foreach($tmp as $key=>$cc_email)
						{
							$cc_recipients[strtolower($key)] = strtolower($cc_email);
						}
					}
					else
					{
						$recipient_user_ids[$recipient3] = 0;
					}
					$mailaddress = 1;
					
				}
			}


			
			// send email notification for new projects
			$sql = 'select * from projecttype_newproject_notifications ' . 
				   'where projecttype_newproject_notification_on_new_project = 1 ' . 
				   ' and projecttype_newproject_notification_country = ' . $pos_country . 
				   ' and projecttype_newproject_notification_postype = ' . $project_postype . 
				   ' and projecttype_newproject_notification_legal_type = ' . $project_legal_type;

			$res = mysql_query($sql) or dberror($sql);
			while ($row = mysql_fetch_assoc($res))
			{
				if($row["projecttype_newproject_notification_email"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_email"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[strtolower($row["projecttype_newproject_notification_email"])] = $row_u["user_id"];
						$tmp = get_cc_deputy_by_user_id($row_u["user_id"]);
						foreach($tmp as $key=>$cc_email)
						{
							$cc_recipients[strtolower($key)] = strtolower($cc_email);
						}
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_email"]] = 0;
					}
					$mailaddress = 1;
					
				}

				if($row["projecttype_newproject_notification_emailcc1"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc1"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[strtolower($row["projecttype_newproject_notification_emailcc1"])] = $row_u["user_id"];
						$tmp = get_cc_deputy_by_user_id($row_u["user_id"]);
						foreach($tmp as $key=>$cc_email)
						{
							$cc_recipients[strtolower($key)] = strtolower($cc_email);
						}
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc1"]] = 0;
					}
				}
				if($row["projecttype_newproject_notification_emailcc2"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc2"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[strtolower($row["projecttype_newproject_notification_emailcc2"])] = $row_u["user_id"];
						$tmp = get_cc_deputy_by_user_id($row_u["user_id"]);
						foreach($tmp as $key=>$cc_email)
						{
							$cc_recipients[strtolower($key)] = strtolower($cc_email);
						}
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc2"]] = 0;
					}
					
				}
				if($row["projecttype_newproject_notification_emailcc3"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc3"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[strtolower($row["projecttype_newproject_notification_emailcc3"])] = $row_u["user_id"];
						$tmp = get_cc_deputy_by_user_id($row_u["user_id"]);
						foreach($tmp as $key=>$cc_email)
						{
							$cc_recipients[strtolower($key)] = strtolower($cc_email);
						}
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc3"]] = 0;
					}
					
				}
				if($row["projecttype_newproject_notification_emailcc4"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc4"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[strtolower($row["projecttype_newproject_notification_emailcc4"])] = $row_u["user_id"];
						$tmp = get_cc_deputy_by_user_id($row_u["user_id"]);
						foreach($tmp as $key=>$cc_email)
						{
							$cc_recipients[strtolower($key)] = strtolower($cc_email);
						}
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc4"]] = 0;
					}
				
				}
				if($row["projecttype_newproject_notification_emailcc5"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc5"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[strtolower($row["projecttype_newproject_notification_emailcc5"])] = $row_u["user_id"];
						$tmp = get_cc_deputy_by_user_id($row_u["user_id"]);
						foreach($tmp as $key=>$cc_email)
						{
							$cc_recipients[strtolower($key)] = strtolower($cc_email);
						}
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc5"]] = 0;
					}
				
				}
				if($row["projecttype_newproject_notification_emailcc6"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc6"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[strtolower($row["projecttype_newproject_notification_emailcc6"])] = $row_u["user_id"];
						$tmp = get_cc_deputy_by_user_id($row_u["user_id"]);
						foreach($tmp as $key=>$cc_email)
						{
							$cc_recipients[strtolower($key)] = strtolower($cc_email);
						}
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc6"]] = 0;
					}
				
				}
				if($row["projecttype_newproject_notification_emailcc7"])
				{
					
					$sql = 'select user_id from users ' . 
						   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc7"]);

					$res_u = mysql_query($sql) or dberror($sql);
					if ($row_u = mysql_fetch_assoc($res_u))
					{
						$recipient_user_ids[strtolower($row["projecttype_newproject_notification_emailcc7"])] = $row_u["user_id"];
						$tmp = get_cc_deputy_by_user_id($row_u["user_id"]);
						foreach($tmp as $key=>$cc_email)
						{
							$cc_recipients[strtolower($key)] = strtolower($cc_email);
						}
					}
					else
					{
						$recipient_user_ids[$row["projecttype_newproject_notification_emailcc7"]] = 0;
					}
				}
			}


			
			$mail = new Mail();
			$mail->set_subject($subject);
			$mail->set_sender($sender_email, $sender_name);


			foreach($recipient_user_ids as $key=>$user_id)
			{
				$mail->add_recipient($key);
				$recipients_already_served[$key] = $user_id;
			}

			foreach($cc_recipients as $key=>$cc_email)
			{
				$mail->add_cc($cc_email);
				$recipients_already_served[$cc_email] = $cc_email;
			}


			if(count($regional_responsable_reciepients) > 0)
			{
				
				foreach($regional_responsable_reciepients as $user_id=>$email)
				{
					
					if(!array_key_exists($email, $recipients_already_served))
					{
						$recipient_user_ids[strtolower($email)] = $user_id;
						$mailaddress = 1;
					}
				}
			}

		
			$link ="project_view_client_data.php?pid=" . $project_id;
			$bodytext = $bodytext0 . "\nclick below to have direct access to the project:\n";
			$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
			$mail->add_text($bodytext);
			
			if($mailaddress == 1)
			{
				
				$result = $mail->send();
				
				foreach($recipient_user_ids as $key=>$user_id)
				{
					if($user_id > 0)
					{
						append_mail($order_id, $user_id,  user_id(), $bodytext, "100", 1,0);
					}
				}
			}
		}
		else
		{
			$recipient_user_ids = array();
			$cc_recipients = array();
			// send email notification to design supervisor
			$sql = "select project_number, project_postype, project_order, " . 
				   " order_user, order_client_address, " .
				   "order_shop_address_company, order_shop_address_place, country_name, " .
				   "user_firstname, user_name, user_email, user_id, project_cost_type " . 
				   "from projects " . 
				   "left join orders on order_id = project_order " .
				   "left join project_costs on project_cost_order = project_order " . 
				   "left join countries on country_id = order_shop_address_country " .
				   "left join users on user_id = order_user " .
				   "where project_id = " . $project_id;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$project_legal_type = $row["project_cost_type"];
				if($project_legal_type != 1)
				{
					$project_legal_type = 0;
				}
				$project_postype = $row["project_postype"];
				$order_id = $row["project_order"];
				$order_user = $row["order_user"];
				$client_address_id = $row["order_client_address"];

				$subject = MAIL_SUBJECT_PREFIX . ": New project request - Project " . $row["project_number"] . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];

				$sender_email = $row["user_email"];
				$sender_name =  $row["user_name"] . " " . $row["user_firstname"];

				$bodytext0 = "You were assigned as design supervisor by " . $sender_name;

				$mailaddress = 0;


				
				$sql = "select user_id, user_email from projects " . 
					   "left join users on user_id = project_design_supervisor " .
					   "where project_id = " . $project_id;

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					if($row["user_email"])
					{
						$mailaddress = 1;
						$recipient_user_ids[strtolower($row["user_email"])] = $row["user_id"];

						$tmp = get_cc_deputy_by_user_id($row["user_id"]);
						foreach($tmp as $key=>$cc_email)
						{
							$cc_recipients[strtolower($key)] = strtolower($cc_email);
						}


						$mail = new Mail();
						$mail->set_subject($subject);
						$mail->set_sender($sender_email, $sender_name);

						


						foreach($recipient_user_ids as $key=>$user_id)
						{
							$mail->add_recipient($key);
						}

						
						foreach($cc_recipients as $key=>$cc_email)
						{
							$mail->add_cc($cc_email);
						}

						
						$link ="project_view_client_data.php?pid=" . $project_id;
						$bodytext = $bodytext0 . "\nclick below to have direct access to the project:\n";
						$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
						$mail->add_text($bodytext);
						
						if($mailaddress == 1)
						{
							$result = $mail->send();
							
							foreach($recipient_user_ids as $key=>$user_id)
							{
								if($user_id > 0)
								{
									append_mail($order_id, $user_id,  user_id(), $bodytext, "100", 1,0);
								}
							}
						}
					}
				}
			}

			// send email notification for product lines and legal types
			$recipients_already_served = array();
			$recipient_user_ids = array();
			$cc_recipients = array();
			
			$sql = "select project_number, project_product_line, project_order, order_user, " .
				   "order_shop_address_company, order_shop_address_place, country_name, " .
				   "user_firstname, user_name, user_email, user_id, project_cost_type " . 
				   "from projects " . 
				   "left join orders on order_id = project_order " .
				   "left join countries on country_id = order_shop_address_country " .
				   "left join users on user_id = order_user " .
				   "left join project_costs on project_cost_order = project_order " . 
				   "where project_id = " . $project_id;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$project_legal_type = $row["project_cost_type"];
				if($project_legal_type != 1)
				{
					$project_legal_type = 0;
				}
				$product_line = $row["project_product_line"];
				$order_id = $row["project_order"];
				$order_user = $row["order_user"];
				

				//legal types
				$sql = "select * from project_costtypes " .
					   "left join users on user_email = project_costtype_email1 " . 
					   "where project_costtype_id = " . dbquote($project_legal_type);

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					
					if(!in_array($row["user_id"], $recipients_already_served) and $row["project_costtype_email1"])
					{
						$cc_recipients[strtolower($row["project_costtype_email1"])] = strtolower($row["project_costtype_email1"]);
						//$recipients_already_served[$row["project_costtype_email1"]] = $row["user_id"];

					}
					

				}
				$sql = "select * from project_costtypes " .
					   "left join users on user_email = project_costtype_email2 " . 
					   "where project_costtype_id = " . dbquote($project_legal_type);

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					if(!in_array($row["user_id"], $recipients_already_served) and $row["project_costtype_email2"])
					{
						$cc_recipients[strtolower($row["project_costtype_email2"])] = strtolower($row["project_costtype_email2"]);
						//$recipients_already_served[$row["project_costtype_email2"]] = $row["user_id"];
					}
				}
				$sql = "select * from project_costtypes " .
					   "left join users on user_email = project_costtype_email3 " . 
					   "where project_costtype_id = " . dbquote($project_legal_type);

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					if(!in_array($row["user_id"], $recipients_already_served) and $row["project_costtype_email3"])
					{
						$cc_recipients[strtolower($row["project_costtype_email3"])] = strtolower($row["project_costtype_email3"]);
						//$recipients_already_served[$row["project_costtype_email3"]] = $row["user_id"];
					}

				}
				$sql = "select * from project_costtypes " .
					   "left join users on user_email = project_costtype_email4 " . 
					   "where project_costtype_id = " . dbquote($project_legal_type);

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					if(!in_array($row["user_id"], $recipients_already_served) and $row["project_costtype_email4"])
					{
						$cc_recipients[strtolower($row["project_costtype_email4"])] = strtolower($row["project_costtype_email4"]);
						//$recipients_already_served[$row["project_costtype_email4"]] = $row["user_id"];
					}

				}

			}

			//send email notifications for possubclass recipients
			$sql = "select project_number, project_postype, project_pos_subclass, project_product_line, project_order, order_user, " .
				   "order_shop_address_company, order_shop_address_place, order_shop_address_country, country_name, " .
				   "user_firstname, user_name, user_email, user_id from projects " . 
				   "left join orders on order_id = project_order " .
				   "left join countries on country_id = order_shop_address_country " .
				   "left join users on user_id = order_user " .
				   "where project_id = " . $project_id;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$pos_country = $row["order_shop_address_country"];
				$project_postype = $row["project_postype"];
				$project_product_line = $row["project_product_line"];
				$order_id = $row["project_order"];
				$order_user = $row["order_user"];
				$project_pos_subclass = $row["project_pos_subclass"];
				
				$sql = 'select * from possubclasses ' . 
					   'where possubclass_id = ' . dbquote($project_pos_subclass);
				
				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					if($row["possubclass_email1"])
					{
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["possubclass_email1"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$cc_recipients[strtolower($row["possubclass_email1"])] = strtolower($row["possubclass_email1"]);
						}	
					}

					if($row["possubclass_email2"])
					{
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["possubclass_email2"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$cc_recipients[strtolower($row["possubclass_email2"])] = strtolower($row["possubclass_email2"]);
						}	
					}

					if($row["possubclass_email3"])
					{
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["possubclass_email3"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$cc_recipients[strtolower($row["possubclass_email3"])] = strtolower($row["possubclass_email3"]);
						}	
					}

					if($row["possubclass_email4"])
					{
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["possubclass_email4"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$cc_recipients[strtolower($row["possubclass_email4"])] = strtolower($row["possubclass_email4"]);
						}	
					}

					if($row["possubclass_email5"])
					{
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["possubclass_email5"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$cc_recipients[strtolower($row["possubclass_email5"])] = strtolower($row["possubclass_email5"]);
						}	
					}

					if($row["possubclass_email6"])
					{
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["possubclass_email6"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$cc_recipients[strtolower($row["possubclass_email6"])] = strtolower($row["possubclass_email6"]);
						}	
					}
				}
			}


			//send email notifications for area types recipients
			$sql = "select project_number, project_postype, project_product_line, project_order, project_projectkind, order_user, " .
				   "order_shop_address_company, order_shop_address_place, order_shop_address_country, country_name, " .
				   "user_firstname, user_name, user_email, user_id from projects " . 
				   "left join orders on order_id = project_order " .
				   "left join countries on country_id = order_shop_address_country " .
				   "left join users on user_id = order_user " .
				   "where project_id = " . $project_id;


			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$pos_country = $row["order_shop_address_country"];
				$project_postype = $row["project_postype"];
				$project_kind = $row["project_projectkind"];
				$project_product_line = $row["project_product_line"];
				$order_id = $row["project_order"];
				$order_user = $row["order_user"];

				//get areas
				$posareas = array();
				
				if($project_kind == 1 or $project_kind == 6) //new or relocation project
				{
					$sql_p = "select posarea_area from posorderspipeline " .
						   "left join posareaspipeline on posarea_posaddress = posorder_posaddress " . 
						   "where posorder_order = " . dbquote($order_id);
				}
				elseif($project_kind > 1) //renovation, takeover/renovation and takeover project
				{
					$sql_p = "select posarea_area from posorders " .
						   "left join posareas on posarea_posaddress = posorder_posaddress " . 
						   "where posorder_order = " . dbquote($order_id);
				}


				$res_p = mysql_query($sql_p) or dberror($sql_p);
				while ($row_p = mysql_fetch_assoc($res_p))
				{
					$posareas[$row_p["posarea_area"]] = $row_p["posarea_area"];
				}
				
				
				if(count($posareas) > 0)
				{
					
					if(count($posareas) > 0)
					{
						$filter = " IN (" . implode(',', $posareas ) . ')';
					}
					else
					{
						$filter = " = 0";
					}

					$sql = 'select * from posareatypes ' . 
						   'where posareatype_id ' . $filter;

					
					
					$res = mysql_query($sql) or dberror($sql);
					while ($row = mysql_fetch_assoc($res))
					{
						if($row["posareatype_email1"])
						{
							$sql = 'select user_id from users ' . 
								   'where user_email = ' . dbquote($row["posareatype_email1"]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$cc_recipients[strtolower($row["posareatype_email1"])] = strtolower($row["posareatype_email1"]);
							}	
						}

						if($row["posareatype_email2"])
						{
							$sql = 'select user_id from users ' . 
								   'where user_email = ' . dbquote($row["posareatype_email2"]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$cc_recipients[strtolower($row["posareatype_email2"])] = strtolower($row["posareatype_email2"]);
							}	
						}

						if($row["posareatype_email3"])
						{
							$sql = 'select user_id from users ' . 
								   'where user_email = ' . dbquote($row["posareatype_email3"]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$cc_recipients[strtolower($row["posareatype_email3"])] = strtolower($row["posareatype_email3"]);
							}	
						}

						if($row["posareatype_email4"])
						{
							$sql = 'select user_id from users ' . 
								   'where user_email = ' . dbquote($row["posareatype_email4"]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$cc_recipients[strtolower($row["posareatype_email4"])] = strtolower($row["posareatype_email4"]);
							}	
						}

						if($row["posareatype_email5"])
						{
							$sql = 'select user_id from users ' . 
								   'where user_email = ' . dbquote($row["posareatype_email5"]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$cc_recipients[strtolower($row["posareatype_email5"])] = strtolower($row["posareatype_email5"]);
							}	
						}

						if($row["posareatype_email6"])
						{
							$sql = 'select user_id from users ' . 
								   'where user_email = ' . dbquote($row["posareatype_email6"]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$cc_recipients[strtolower($row["posareatype_email6"])] = strtolower($row["posareatype_email6"]);
							}	
						}
					}
				}
			}


			// send email notification for new projects
			$sql = "select project_number, project_postype, project_product_line, project_order, order_user, order_client_address, " .
				   "order_shop_address_company, order_shop_address_place, order_shop_address_country, " . "country_name, project_cost_type, " .
				   "user_firstname, user_name, user_email, user_id from projects " . 
				   "left join orders on order_id = project_order " .
				   "left join project_costs on project_cost_order = project_order " . 
				   "left join countries on country_id = order_shop_address_country " .
				   "left join users on user_id = order_user " .
				   "where project_id = " . $project_id;


			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$project_legal_type = $row["project_cost_type"];
				if($project_legal_type != 1)
				{
					$project_legal_type = 0;
				}
				$pos_country = $row["order_shop_address_country"];
				$project_postype = $row["project_postype"];
				$project_product_line = $row["project_product_line"];
				$order_id = $row["project_order"];
				$order_user = $row["order_user"];

				$client_address_id  = $row["order_client_address"];


				//users for regional responsabilities
				$regional_responsable_reciepients = get_regional_responsible($project_legal_type, $client_address_id);


				$recipient_user_ids = array();

				
				$subject = MAIL_SUBJECT_PREFIX . ": New project request - Project " . $row["project_number"] . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];

				$sender_email = $row["user_email"];
				$sender_name =  $row["user_name"] . " " . $row["user_firstname"];

				$bodytext0 = "A new project request has been added by " . $sender_name;

				$mailaddress = 0;

				$mail = new Mail();
				$mail->set_subject($subject);
				$mail->set_sender($sender_email, $sender_name);

				$link ="project_view_client_data.php?pid=" . $project_id;
				$bodytext = $bodytext0 . "\nclick below to have direct access to the project:\n";
				$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
				$mail->add_text($bodytext);

				
				$sql = 'select * from projecttype_newproject_notifications ' . 
					   'where projecttype_newproject_notification_on_new_project = 1 ' . 
					   ' and projecttype_newproject_notification_country = ' . $pos_country . 
					   ' and projecttype_newproject_notification_postype = ' . $project_postype . 
					   ' and projecttype_newproject_notification_legal_type = ' . $project_legal_type;

				

				$res = mysql_query($sql) or dberror($sql);
				while ($row = mysql_fetch_assoc($res))
				{
					if($row["projecttype_newproject_notification_email"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["projecttype_newproject_notification_email"]);

						$res_u = mysql_query($sql) or dberror($sql);
						
						
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[strtolower($row["projecttype_newproject_notification_email"])] = $row_u["user_id"];
							$tmp = get_cc_deputy_by_user_id($row_u["user_id"]);
							foreach($tmp as $key=>$cc_email)
							{
								$cc_recipients[strtolower($key)] = strtolower($cc_email);
							}
						}
						else
						{
							$recipient_user_ids[strtolower($row["projecttype_newproject_notification_email"])] = 0;
						}
						$mailaddress = 1;
						
					}

					if($row["projecttype_newproject_notification_emailcc1"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc1"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[strtolower($row["projecttype_newproject_notification_emailcc1"])] = $row_u["user_id"];
							$tmp = get_cc_deputy_by_user_id($row_u["user_id"]);
							foreach($tmp as $key=>$cc_email)
							{
								$cc_recipients[strtolower($key)] = strtolower($cc_email);
							}
						}
						else
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc1"]] = 0;
						}
					}
					if($row["projecttype_newproject_notification_emailcc2"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc2"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[strtolower($row["projecttype_newproject_notification_emailcc2"])] = $row_u["user_id"];
							$tmp = get_cc_deputy_by_user_id($row_u["user_id"]);
							foreach($tmp as $key=>$cc_email)
							{
								$cc_recipients[strtolower($key)] = strtolower($cc_email);
							}
						}
						else
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc2"]] = 0;
						}
						
					}
					if($row["projecttype_newproject_notification_emailcc3"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc3"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[strtolower($row["projecttype_newproject_notification_emailcc3"])] = $row_u["user_id"];
							$tmp = get_cc_deputy_by_user_id($row_u["user_id"]);
							foreach($tmp as $key=>$cc_email)
							{
								$cc_recipients[strtolower($key)] = strtolower($cc_email);
							}
						}
						else
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc3"]] = 0;
						}
						
					}
					if($row["projecttype_newproject_notification_emailcc4"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc4"]);


						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[strtolower($row["projecttype_newproject_notification_emailcc4"])] = $row_u["user_id"];
							$tmp = get_cc_deputy_by_user_id($row_u["user_id"]);
							foreach($tmp as $key=>$cc_email)
							{
								$cc_recipients[strtolower($key)] = strtolower($cc_email);
							}
						}
						else
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc4"]] = 0;
						}
					
					}
					if($row["projecttype_newproject_notification_emailcc5"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc5"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[strtolower($row["projecttype_newproject_notification_emailcc5"])] = $row_u["user_id"];
							$tmp = get_cc_deputy_by_user_id($row_u["user_id"]);
							foreach($tmp as $key=>$cc_email)
							{
								$cc_recipients[strtolower($key)] = strtolower($cc_email);
							}
						}
						else
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc5"]] = 0;
						}
					
					}
					if($row["projecttype_newproject_notification_emailcc6"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc6"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[strtolower($row["projecttype_newproject_notification_emailcc6"])] = $row_u["user_id"];
							$tmp = get_cc_deputy_by_user_id($row_u["user_id"]);
							foreach($tmp as $key=>$cc_email)
							{
								$cc_recipients[strtolower($key)] = strtolower($cc_email);
							}
						}
						else
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc6"]] = 0;
						}
					
					}
					if($row["projecttype_newproject_notification_emailcc7"])
					{
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($row["projecttype_newproject_notification_emailcc7"]);

						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{
							$recipient_user_ids[strtolower($row["projecttype_newproject_notification_emailcc7"])] = $row_u["user_id"];
							$tmp = get_cc_deputy_by_user_id($row_u["user_id"]);
							foreach($tmp as $key=>$cc_email)
							{
								$cc_recipients[strtolower($key)] = strtolower($cc_email);
							}
						}
						else
						{
							$recipient_user_ids[$row["projecttype_newproject_notification_emailcc7"]] = 0;
						}
					}
				}


				if(count($regional_responsable_reciepients) > 0)
				{
					
					foreach($regional_responsable_reciepients as $user_id=>$email)
					{
						
						if(!array_key_exists($email, $recipients_already_served))
						{
							$recipient_user_ids[strtolower($email)] = $user_id;
							$mailaddress = 1;
						}
					}
				}

				foreach($recipient_user_ids as $key=>$user_id)
				{
					if(!in_array($user_id, $recipients_already_served) and $user_id > 0)
					{
						$mail->add_recipient($key);
						append_mail($order_id, $user_id,  user_id(), $bodytext, "100", 1,0);
						$recipients_already_served[$key] = $user_id;

					}
					
				}


				foreach($cc_recipients as $key=>$cc_email)
				{
					if(!in_array($cc_email, $recipients_already_served))
					{
						$mail->add_cc($cc_email);
						$recipients_already_served[$cc_email] = $cc_email;
						
						$sql = 'select user_id from users ' . 
							   'where user_email = ' . dbquote($cc_email);


						$res_u = mysql_query($sql) or dberror($sql);
						if ($row_u = mysql_fetch_assoc($res_u))
						{					
							append_mail($order_id, $row_u['user_id'],  user_id(), $bodytext, "100", 1,0, 1);
						}
					}
				}
				
				if($mailaddress == 1)
				{
					$result = $mail->send();
				}
			}
		}
	}

	//send mail notifications for CER/AF
	//check first if a mail has to be sent
	
	//replaced by step 140 in workflow 19.2.2018
	/*
	$cc_recipients = array();
	$sql = "select order_number, order_shop_address_place, country_name, order_shop_address_company, project_order, order_user, " .
		   "project_postype, project_projectkind, project_cost_type, project_projectkind, project_cost_type " . 
		   "from projects " .
		   "left join orders on order_id = project_order " .
		   "left join countries on country_id = order_shop_address_country " .
		   "left join project_costs on project_cost_order = order_id " .
		   "where project_id = " .  dbquote($project_id);

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		
		$recipient_user_ids = array();
		$order_id = $row["project_order"];
		$order_user = $row["order_user"];
		
		$project_kind = $row["project_projectkind"];
		$legal_type = $row["project_cost_type"];
		
		if($project_kind == 5) // lease renewal
		{
			$subject = "Request for LN submission - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_place"] . ", " . $row["order_shop_address_company"];
			

		}
		else
		{
			if($legal_type == 1) //Corporate
			{
				$subject = "Request for LN submission - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_place"] . ", " . $row["order_shop_address_company"];
			}
			else
			{
				$subject = "Request for AF submission - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_place"] . ", " . $row["order_shop_address_company"];
			}

		}
		
		
		$sql = "select posproject_type_needs_cer, posproject_type_needs_af, posproject_type_needs_inr03 " . 
			   "from posproject_types " . 
			   "where posproject_type_postype =  " . $row["project_postype"] .
			   " and posproject_type_projectcosttype =  " . $row["project_cost_type"] .
			   " and posproject_type_projectkind =  " . $row["project_projectkind"];

		
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			if($row["posproject_type_needs_cer"] == 1 
				or $row["posproject_type_needs_af"] == 1
				or $row["posproject_type_needs_inr03"] == 1
				)
			{
				
				if($row["posproject_type_needs_cer"] == 1)
				{
					if($project_kind == 5) // lease renewal
					{
						$sql = "select * from mail_alert_types " . 
						   "where mail_alert_type_id = 11";
						
					}
					else
					{
						$sql = "select * from mail_alert_types " . 
						       "where mail_alert_type_id = 3";

						
					}
					
					
				}
				else
				{
					$sql = "select * from mail_alert_types " . 
						   "where mail_alert_type_id = 7";
				}

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					$sender_email = $row["mail_alert_type_sender_email"];
					$sender_name = $row["mail_alert_type_sender_name"];
					$mail_text = $row["mail_alert_mail_text"];

					//get brand manager

					$sql = "select address_id " . 
						   "from projects " . 
						   "left join orders on order_id = project_order " . 
						   "left join addresses on address_id = order_client_address " . 
						   "where project_id = " . dbquote($project_id);
					
					$res = mysql_query($sql) or dberror($sql);
					if ($row = mysql_fetch_assoc($res))
					{
						$brand_manager_email = "";
						$brandmanager_name = "";
						$reciepient_emails = array();

						$address_id = $row["address_id"];
						
						$sql = "select * " .
							   "from user_roles " . 
							   "left join users on user_id = user_role_user " .
							   "left join addresses on address_id = user_address " . 
							   " where user_role_role = 15 " . 
							   " and address_type = 1 and address_id = " . dbquote($address_id) . 
							   " and user_active = 1";

						
						$res = mysql_query($sql) or dberror($sql);
						if ($row = mysql_fetch_assoc($res))
						{
							$brand_manager_email = $row["user_email"];
							$brandmanager_name  = $row["user_firstname"];
							$recipient_user_ids[$row["user_email"]] = $row["user_id"];

							$tmp = get_cc_deputy_by_user_id($row["user_id"]);
							foreach($tmp as $key=>$cc_email)
							{
								$cc_recipients[$key] = $cc_email;
							}
						}
						else
						{
							$sql_c = "select address_client_type, address_parent " . 
									 " from addresses " . 
									 " where address_id = " . dbquote($address_id);
							$res_c = mysql_query($sql_c) or dberror($sql_c);
							if($row_c = mysql_fetch_assoc($res_c))
							{
								if($row_c["address_client_type"] == 3) //Affiliate
								{
									$sql_c = "select user_id, user_firstname, user_name, user_email from users " .
											 "left join user_roles on user_role_user = user_id " . 
											 "where user_address = " . $row_c["address_parent"] . 
											 " and user_role_role = 15 and user_active = 1";

									$res_c = mysql_query($sql_c) or dberror($sql_c);

									if($row_c = mysql_fetch_assoc($res_c))
									{
										$brand_manager_email = $row_c["user_email"];
										$brandmanager_name  = $row_c["user_firstname"];
										$recipient_user_ids[$row_c["user_email"]] = $row_c["user_id"];

										$tmp = get_cc_deputy_by_user_id($row_c["user_id"]);
										foreach($tmp as $key=>$cc_email)
										{
											$cc_recipients[$key] = $cc_email;
										}
									}
								}
							}
						}


						//client
						$sql = "select * " .
							   "from users " . 
							   "where user_id = " . dbquote($order_user);

						$res = mysql_query($sql) or dberror($sql);
						if ($row = mysql_fetch_assoc($res))
						{
							
							if(!$brand_manager_email) {

								$brand_manager_email = $row["user_email"];
								$brandmanager_name  = $row["user_firstname"];
								$recipient_user_ids[$row["user_email"]] = $row["user_id"];

								$tmp = get_cc_deputy_by_user_id($row["user_id"]);
								foreach($tmp as $key=>$cc_email)
								{
									$cc_recipients[$key] = $cc_email;
								}
	
							}
							else {
								$reciepient_emails[$row["user_email"]] = $row["user_email"];
								$recipient_user_ids[$row["user_email"]] = $row["user_id"];
							}
						}
						
						//local store coordinator, retail manager
						$sql = "select * " .
							   "from user_roles " . 
							   "left join users on user_id = user_role_user " .
							   "left join addresses on address_id = user_address " . 
							   " where user_role_role = 16 " . 
							   " and address_type = 1 and address_id = " . dbquote($address_id) . 
							   " and user_active = 1";

						
						$res = mysql_query($sql) or dberror($sql);
						while ($row = mysql_fetch_assoc($res))
						{
							if($row["user_can_only_see_his_projects"] and $order_user != $row["user_id"])
							{
							}
							else
							{
								$reciepient_emails[$row["user_email"]] = $row["user_email"];
								$recipient_user_ids[$row["user_email"]] = $row["user_id"];

								$tmp = get_cc_deputy_by_user_id($row["user_id"]);
								foreach($tmp as $key=>$cc_email)
								{
									$cc_recipients[$key] = $cc_email;
								}
							}
						}

						$reciepient_emails[$sender_email] = $sender_email;

					}

					$bodytext0 = "Dear " . $brandmanager_name . "\n\n" . $mail_text;
					$link ="cer_application_general.php?pid=" . $project_id . "&id=" . $project_id;
					$bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
					$bodytext = $bodytext .  APPLICATION_URL . "/cer/" . $link . "\n\n";           
					
					$bodytext .= "Thank you very much.\nKind regards, " . $sender_name;

					
					$rctps = $brand_manager_email;

					$mail = new Mail();
					$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
					$mail->set_sender($sender_email, $sender_name);
					
					$mail->add_recipient($brand_manager_email);
					foreach($reciepient_emails as $key=>$email)
					{
						$mail->add_cc($email);
						$rctps = $rctps . $email . "\n";
					}

					foreach($cc_recipients as $key=>$cc_email)
					{
						$mail->add_cc($cc_email);
					}

					$mail->add_text($bodytext);

					$result = $mail->send();
					
					foreach($recipient_user_ids as $key=>$user_id)
					{
						if($user_id > 0)
						{
							append_mail($order_id, $user_id,  user_id(), $bodytext, "100", 1,0);
						}
					}

					if($result == 1)
					{
						$fields = array();
						$values = array();

						$fields[] = "cer_mail_project";
						$values[] = $project_id;

						$fields[] = "cer_mail_group";
						$values[] = dbquote($subject);

						$fields[] = "cer_mail_text";
						$values[] = dbquote($bodytext);

						$fields[] = "cer_mail_sender";
						$values[] = dbquote($sender_name);

						$fields[] = "cer_mail_sender_email";
						$values[] = dbquote($sender_email);

						$fields[] = "cer_mail_reciepient";
						$values[] = dbquote($rctps);

						$fields[] = "date_created";
						$values[] = "now()";

						$fields[] = "date_modified";
						$values[] = "now()";

						$fields[] = "user_created";
						$values[] = dbquote(user_login());

						$fields[] = "user_modified";
						$values[] = dbquote(user_login());

						$sql = "insert into cer_mails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
						
						mysql_query($sql) or dberror($sql);
					}
						   
				}
			}
		}
	}
	*/



	//send confirmation mail to sender
	$link = APPLICATION_URL . "/user/project_task_center.php?pid=" . $project_id;
	$actionmail = new ActionMail(155);
	$actionmail->setParam('projectID', $project_id);
	$actionmail->setParam('link', $link);
	$actionmail->send();

	return true;
}


/********************************************************************
    appends an item to the list of materials on performing an action
*********************************************************************/
function project_send_mail_notification_for_popups($project_id = 0)
{
	//send mail notifications for CER/AF
	//check first if a mail has to be sent
	
	$cc_recipients = array();
	$sql = "select order_number, order_shop_address_place, country_name, order_shop_address_company, project_order, order_user, " .
		   "project_postype, project_projectkind, project_cost_type, project_projectkind, project_cost_type " . 
		   "from projects " .
		   "left join orders on order_id = project_order " .
		   "left join countries on country_id = order_shop_address_country " .
		   "left join project_costs on project_cost_order = order_id " .
		   "where project_id = " .  dbquote($project_id);

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		
		$recipient_user_ids = array();
		$order_id = $row["project_order"];
		$order_user = $row["order_user"];
		
		$project_kind = $row["project_projectkind"];
		$legal_type = $row["project_cost_type"];
		
		if($legal_type == 1) //Corporate
		{
			$subject = "CER submission - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_place"] . ", " . $row["order_shop_address_company"];

			$sql = "select * from mail_alert_types " . 
						       "where mail_alert_type_id = 36";

			$subject = "Request for " . $subject;
		}
		else
		{
			$subject = "AF submission - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_place"] . ", " . $row["order_shop_address_company"];

			$sql = "select * from mail_alert_types " . 
						   "where mail_alert_type_id = 37";

			$subject = "Application request for " . $subject;
		}

		
		
		

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$sender_email = $row["mail_alert_type_sender_email"];
			$sender_name = $row["mail_alert_type_sender_name"];
			$mail_text = $row["mail_alert_mail_text"];

			//get brand manager

			$sql = "select address_id " . 
				   "from projects " . 
				   "left join orders on order_id = project_order " . 
				   "left join addresses on address_id = order_client_address " . 
				   "where project_id = " . dbquote($project_id);
			
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$brand_manager_email = "";
				$brandmanager_name = "";
				$reciepient_emails = array();

				$address_id = $row["address_id"];
				
				$sql = "select * " .
					   "from user_roles " . 
					   "left join users on user_id = user_role_user " .
					   "left join addresses on address_id = user_address " . 
					   " where user_role_role = 15 " . 
					   " and address_type = 1 and address_id = " . dbquote($address_id) . 
					   " and user_active = 1";

				
				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					$brand_manager_email = $row["user_email"];
					$brandmanager_name  = $row["user_firstname"];
					$recipient_user_ids[$row["user_email"]] = $row["user_id"];

					$tmp = get_cc_deputy_by_user_id($row["user_id"]);
					foreach($tmp as $key=>$cc_email)
					{
						$cc_recipients[$key] = $cc_email;
					}
				}
				else
				{
					$sql_c = "select address_client_type, address_parent " . 
							 " from addresses " . 
							 " where address_id = " . dbquote($address_id);
					$res_c = mysql_query($sql_c) or dberror($sql_c);
					if($row_c = mysql_fetch_assoc($res_c))
					{
						if($row_c["address_client_type"] == 3) //Affiliate
						{
							$sql_c = "select user_id, user_firstname, user_name, user_email from users " .
									 "left join user_roles on user_role_user = user_id " . 
									 "where user_address = " . $row_c["address_parent"] . 
									 " and user_role_role = 15 and user_active = 1";

							$res_c = mysql_query($sql_c) or dberror($sql_c);

							if($row_c = mysql_fetch_assoc($res_c))
							{
								$brand_manager_email = $row_c["user_email"];
								$brandmanager_name  = $row_c["user_firstname"];
								$recipient_user_ids[$row_c["user_email"]] = $row_c["user_id"];

								$tmp = get_cc_deputy_by_user_id($row_c["user_id"]);
								foreach($tmp as $key=>$cc_email)
								{
									$cc_recipients[$key] = $cc_email;
								}
							}
						}
					}
				}


				//client
				$sql = "select * " .
					   "from users " . 
					   "where user_id = " . dbquote($order_user);

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					$reciepient_emails[$row["user_email"]] = $row["user_email"];
					$recipient_user_ids[$row["user_email"]] = $row["user_id"];
				}
				
				//local store coordinator, retail manager
				$sql = "select * " .
					   "from user_roles " . 
					   "left join users on user_id = user_role_user " .
					   "left join addresses on address_id = user_address " . 
					   " where user_role_role = 16 " . 
					   " and address_type = 1 and address_id = " . dbquote($address_id) . 
					   " and user_active = 1";

				
				$res = mysql_query($sql) or dberror($sql);
				while ($row = mysql_fetch_assoc($res))
				{
					if($row["user_can_only_see_his_projects"] and $order_user != $row["user_id"])
					{
					}
					else
					{
						$reciepient_emails[$row["user_email"]] = $row["user_email"];
						$recipient_user_ids[$row["user_email"]] = $row["user_id"];

						$tmp = get_cc_deputy_by_user_id($row["user_id"]);
						foreach($tmp as $key=>$cc_email)
						{
							$cc_recipients[$key] = $cc_email;
						}
					}
				}

				$reciepient_emails[$sender_email] = $sender_email;

			}

			$bodytext0 = "Dear " . $brandmanager_name . "\n\n" . $mail_text;
			$link ="cer_project.php?pid=" . $project_id . "&id=" . $project_id;
			$bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
			$bodytext = $bodytext .  APPLICATION_URL . "/cer/" . $link . "\n\n";           
			
			$bodytext .= "Thank you very much.\nKind regards, " . $sender_name;

			
			$rctps = $brand_manager_email;

			$mail = new Mail();
			$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
			$mail->set_sender($sender_email, $sender_name);
			
			$mail->add_recipient($brand_manager_email);
			foreach($reciepient_emails as $key=>$email)
			{
				$mail->add_cc($email);
				$rctps = $rctps . $email . "\n";
			}

			foreach($cc_recipients as $key=>$cc_email)
			{
				$mail->add_cc($cc_email);
			}

			$mail->add_text($bodytext);

			$result = $mail->send();
			
			foreach($recipient_user_ids as $key=>$user_id)
			{
				if($user_id > 0)
				{
					append_mail($order_id, $user_id,  user_id(), $bodytext, "100", 1,0);
				}
			}
			$result = 1;

			if($result == 1)
			{
				$fields = array();
				$values = array();

				$fields[] = "cer_mail_project";
				$values[] = $project_id;

				$fields[] = "cer_mail_group";
				$values[] = dbquote($subject);

				$fields[] = "cer_mail_text";
				$values[] = dbquote($bodytext);

				$fields[] = "cer_mail_sender";
				$values[] = dbquote($sender_name);

				$fields[] = "cer_mail_sender_email";
				$values[] = dbquote($sender_email);

				$fields[] = "cer_mail_reciepient";
				$values[] = dbquote($rctps);

				$fields[] = "date_created";
				$values[] = "now()";

				$fields[] = "date_modified";
				$values[] = "now()";

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$fields[] = "user_modified";
				$values[] = dbquote(user_login());

				$sql = "insert into cer_mails (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

				mysql_query($sql) or dberror($sql);
			}
				   
		}
	}


	return true;
}



/********************************************************************
    appends an item to the list of materials on performing an action
*********************************************************************/
function append_auto_item($order_id, $taskwork_id)
{
	
	//get taskwork item
	$sql = "select taskwork_item from taskworks where taskwork_id = " . $taskwork_id;
	$res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		if($row["taskwork_item"] > 0)
		{
			$item_id = $row["taskwork_item"];

			//get item_type
			$sql = "select item_type from items where item_id = " . $item_id;
			$res1 = mysql_query($sql) or dberror($sql);

			if ($row = mysql_fetch_assoc($res1))
			{
				$type = $row["item_type"];
			}
		
			$order_item_fields = array();
			$order_item_values = array();

			$order_item_fields[] = "order_item_order";
			$order_item_values[] = $order_id;

			$order_item_fields[] = "order_item_item";
			$order_item_values[] = $item_id;

			$order_item_fields[] = "order_item_type";
			$order_item_values[] = $type;

			$order_item_fields[] = "order_item_quantity";
			$order_item_values[] = 1;

			// get supplier's address id (first record in case of several)
			$sql = "select address_id, address_company, supplier_item_price, supplier_item_currency ".
				   "from items ".
				   "left join suppliers on item_id = supplier_item ".
				   "left join addresses on supplier_address = address_id ".
				   "where item_id = " . $item_id;
		
			$res1 = mysql_query($sql) or dberror($sql);

			if ($row1 = mysql_fetch_assoc($res1))
			{
				if ($row1["address_id"])
				{
					
					if ($row1["supplier_item_currency"])
					{
						$currency = get_currency($row1["supplier_item_currency"]);
					}
					else
					{
						$currency = get_address_currency($row1["address_id"]);
					}
					
					$order_item_fields[] = "order_item_supplier_address";
					$order_item_values[] = $row1["address_id"];

					// get suppliers's currency information
					$supplier_currency = get_address_currency($row1["address_id"]);

					$order_item_fields[] = "order_item_supplier_currency";
					$order_item_values[] = $currency["id"];

					$order_item_fields[] = "order_item_supplier_exchange_rate";
					$order_item_values[] = $currency["exchange_rate"];

					$order_item_fields[] = "order_item_supplier_price";
					$order_item_values[] = $row1["supplier_item_price"];
				}
			}

			// get orders's currency
			$currency = get_order_currency($order_id);

			// get item data

			$sql = "select * ".
				   "from items ".
				   "where item_id = " . $item_id;

			$res1 = mysql_query($sql) or dberror($sql);

			if ($row = mysql_fetch_assoc($res1))
			{
				$order_item_fields[] = "order_item_category";
				$order_item_values[] = $row["item_category"];

				$order_item_fields[] = "order_item_text";
				$order_item_values[] = trim($row["item_name"]) == "" ? "null" : dbquote($row["item_name"]);

				$order_item_fields[] = "order_item_system_price";
				$order_item_values[] = trim($row["item_price"]) == "" ? "null" : dbquote($row["item_price"]);

				$client_price = $row["item_price"] / $currency["exchange_rate"] * $currency["factor"];
				$order_item_fields[] = "order_item_client_price";
				$order_item_values[] = trim($client_price) == "" ? "null" : dbquote($client_price);

				$order_item_fields[] = "order_item_cost_unit_number";
				$order_item_values[] = trim($row["category_cost_unit_number"]) == "" ? "null" : dbquote($row["category_cost_unit_number"]);

				/* cost monitoring group was removed by Katrin Eckert on January 26th 2016
				$order_item_fields[] = "order_items_costmonitoring_group";
				$order_item_values[] = dbquote($row["item_costmonitoring_group"]);
				*/

			}

			$order_item_fields[] = "order_item_no_offer_required";
			$order_item_values[] = 1;


			$order_item_fields[] = "order_item_cost_group";
			$order_item_values[] = dbquote($row["item_cost_group"]);


			$order_item_fields[] = "order_item_ordered";
			$order_item_values[] = "current_timestamp";


			$order_item_fields[] = "date_created";
			$order_item_values[] = "current_timestamp";

			$order_item_fields[] = "date_modified";
			$order_item_values[] = "current_timestamp";
			
			if (isset($_SESSION["user_login"]))
			{
				$order_item_fields[] = "user_created";
				$order_item_values[] = dbquote($_SESSION["user_login"]);

				$order_item_fields[] = "user_modified";
				$order_item_values[] = dbquote($_SESSION["user_login"]);
			}
			$sql = "insert into order_items (" . join(", ", $order_item_fields) . ") values (" . join(", ", $order_item_values) . ")";
			mysql_query($sql) or dberror($sql);
		}
	}
	

	return true;
}





/********************************************************************
    get investments from the list of materials
*********************************************************************/
function update_cer_investments_from_the_list_of_materials($project_id, $order_id, $country_id, $update_mode, $version = 0)
{
	$amounts = array();
	$comments = array();

	//get the local currency of shop
	$currency = get_cer_currency($project_id);

	$sql = "select * from cer_investments " .
		   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
		   "where cer_investment_project = " . $project_id . 
		   " and cer_investment_cer_version = " . $version;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		
		if($row["posinvestment_type_cms_sumcostgroups"])
		{
			$group_total = 0;
			$cost_groups = explode(",", $row["posinvestment_type_cms_sumcostgroups"]);
			foreach($cost_groups as $key=>$cost_group)
			{
				$sql_i = "select sum(order_item_system_price*order_item_quantity) as total_in_system_currency " . 
						 "from order_items ".
						 "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
						 "   and order_item_order = " . $order_id . 
						 "   and order_item_cost_group =  " . dbquote($cost_group);
				
				$res_i = mysql_query($sql_i) or dberror($sql_i);
				if ($row_i = mysql_fetch_assoc($res_i))
				{
					
					if($currency["exchange_rate"] > 0)
					{
						$group_total = $group_total + ($row_i["total_in_system_currency"] / $currency["exchange_rate"])*$currency["factor"];
					}
				}

				if($update_mode == 1)
				{
					$amounts[$row["cer_investment_id"]] = $group_total;
					$comments[$row["cer_investment_id"]] = "inserted from the list of materials";
				}
				else
				{
					if($row["cer_investment_amount_cer_loc"] == 0)
					{
						$amounts[$row["cer_investment_id"]] = $group_total;
						$comments[$row["cer_investment_id"]] = "inserted from the list of materials";
					}
				}
			}
		}
	}
	
	foreach($amounts as $id=>$value)
	{
		//if($value)
		//{
			$fields = array();
		
			$value = dbquote($value);
			$fields[] = "cer_investment_amount_cer_loc = " . $value;

			$value = dbquote($comments[$id]);
			$fields[] = "cer_investment_comment = " . $value;

			$value1 = "current_timestamp";
			$fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_investments set " . join(", ", $fields) . 
				   " where cer_investment_id = " . $id . 
				   " and cer_investment_cer_version = " . $version;

			mysql_query($sql) or dberror($sql);

			//echo $sql;
		//}

	}

	/*
	//update kl approved investments
	$sql = "update cer_investments set cer_investment_amount_cer_loc_approved = cer_investment_amount_cer_loc " . 
			"where cer_investment_amount_cer_loc_approved = 0 and cer_investment_type in(1, 3, 5, 7, 11) and cer_investment_project = " . $project_id and cer_investment_cer_version = 0 ";

	$result = mysql_query($sql) or dberror($sql);
	*/
}


/********************************************************************
    appends an item to the list of materials on performing an action
*********************************************************************/
function recalculate_cer_investments($project_id = 0, $legal_type = 0, $country_id = 0)
{

		
	//update CER currencies with actual exchange rates
	$currency_id = 0;
	$currency2_id = 0;

	$sql = "select cer_basicdata_currency, cer_basicdata_currency2, cer_basicdata_project " . 
		   "from cer_basicdata  " . 
		   "where cer_basicdata_project = " . $project_id . 
		   " and cer_basicdata_version = 0";

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$currency_id = $row["cer_basicdata_currency"];
		$currency2_id = $row["cer_basicdata_currency2"];

		$cer_project_id = $row["cer_basicdata_project"];
	}


	if($currency_id == 0 and $cer_project_id > 0)
	{
		$sql = "select country_currency " . 
			   "from projects " . 
			   "left join orders on order_id = project_order " . 
			   "left join addresses on address_id = order_client_address " . 
			   "left join countries on country_id = address_country " . 
			   "where project_id = " . $cer_project_id;
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$currency_id = $row["country_currency"];
		}

	}


	if($currency2_id == 0 and $cer_project_id > 0)
	{
		$sql = "select country_currency " . 
			   "from projects " . 
			   "left join orders on order_id = project_order " . 
			   "left join countries on country_id = order_shop_address_country " .
			   "where project_id = " . $cer_project_id;
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$currency2_id = $row["country_currency"];
		}

	}
	

	if($currency_id > 0) {
		$sql = "select * from currencies where currency_id = " . $currency_id;
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$sql = "update cer_basicdata set " .
				   "cer_basicdata_currency = " . $row["currency_id"] . ", " .
				   "cer_basicdata_exchangerate = " . $row["currency_exchange_rate"] . ", " .
				   "cer_basicdata_factor = " . $row["currency_factor"] . ", " .
				   "cer_basicdata_exr_date = " . dbquote(date("Y-m-d")) . 
				   " where cer_basicdata_project = " . $project_id  .
				   " and cer_basicdata_version = 0";

			$result = mysql_query($sql) or dberror($sql);

		}
	}


	if($currency2_id > 0) {
		$sql = "select * from currencies where currency_id = " . $currency2_id;
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$sql = "update cer_basicdata set " .
				   "cer_basicdata_currency2 = " . $row["currency_id"] . ", " .
				   "cer_basicdata_exchangerate2 = " . $row["currency_exchange_rate"] . ", " .
				   "cer_basicdata_factor2 = " . $row["currency_factor"] . 
				   " where cer_basicdata_project = " . $project_id . 
				   " and cer_basicdata_version = 0";

			$result = mysql_query($sql) or dberror($sql);
		}
	}


	$order_id = 0;

	$sql = "select project_order from projects " . 
		   "where project_id = " . $project_id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$order_id = $row["project_order"];
	}

			
	if($legal_type == 1)
	{
		$result = update_cer_investments_from_the_list_of_materials($project_id, $order_id, $country_id, 1);
	}
	else
	{
		//This dopy workaround is necessary since franchisees do not use the tool for entering local
		//construction works in the project
		$sql = "select cer_investment_amount_cer_loc " .
			   "from cer_investments " . 
			   "where cer_investment_type = 1 " . 
			   "and cer_investment_project = " . $project_id . 
			   " and cer_investment_cer_version = 0 ";

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$local_construction_works_for_franchisee_projects = $row["cer_investment_amount_cer_loc"];
		}
		$result = update_investments_from_the_list_of_materials($project_id, $order_id, $country_id, 1);
		
		$sql = "update cer_investments set cer_investment_amount_cer_loc = '" . $local_construction_works_for_franchisee_projects . "' " .
			   "where cer_investment_type = 1 " . 
			   "and cer_investment_project = " . $project_id . 
			   " and cer_investment_cer_version = 0 ";
		mysql_query($sql) or dberror($sql);

	}
}


/********************************************************************
    add record to order_item_tracking
*********************************************************************/
function track_change_in_order_items($action = '', $field = '', $order_id = 0, $order_item_id = 0, $item_id = 0, $item_type = 0, $item_text = '', $old_value = '', $new_value = '')
{
	if($order_id > 0)
	{
		//get actual order_state_id
		$order_state_id = 0;
		$order_type = 0;
		$project_id = 0;

		$sql = "select order_state_id, order_type, project_id from orders " .
			   " left join order_states on order_state_code = order_actual_order_state_code " . 
			   " left join order_state_groups on order_state_group_id = order_state_group " .
			   " left join projects on project_order = order_id " .
			   " where order_state_group_order_type = 1 " . 
			   " and order_state_code = order_actual_order_state_code " . 
			  " and order_id = " . dbquote($order_id);
		
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$order_state_id = $row["order_state_id"];
			$order_type = $row["order_type"];
			$project_id = $row["project_id"];
		}

		$order_item_tracking_fields = array();
		$order_item_tracking_values = array();

		$order_item_tracking_fields[] = "order_item_tracking_action";
		$order_item_tracking_values[] = dbquote($action);

		$order_item_tracking_fields[] = "order_item_tracking_field_name";
		$order_item_tracking_values[] = dbquote($field);

		$order_item_tracking_fields[] = "order_item_tracking_order_id";
		$order_item_tracking_values[] = $order_id;

		$order_item_tracking_fields[] = "order_item_tracking_order_item_id";
		$order_item_tracking_values[] = dbquote($order_item_id);

		$order_item_tracking_fields[] = "order_item_tracking_order_item_item_id";
		$order_item_tracking_values[] = dbquote($item_id);

		$order_item_tracking_fields[] = "order_item_tracking_item_type";
		$order_item_tracking_values[] = dbquote($item_type);

		$order_item_tracking_fields[] = "order_item_tracking_item_text";
		$order_item_tracking_values[] = dbquote($item_text);

		
		$order_item_tracking_fields[] = "order_item_tracking_old_value";
		$order_item_tracking_values[] = dbquote($old_value);
		
		$order_item_tracking_fields[] = "order_item_tracking_new_value";
		$order_item_tracking_values[] = dbquote($new_value);

		$order_item_tracking_fields[] = "order_item_tracking_order_state_id";
		$order_item_tracking_values[] = dbquote($order_state_id);

		$order_item_tracking_fields[] = "order_item_tracking_user_id";
		$order_item_tracking_values[] = dbquote(user_id());
		
		$order_item_tracking_fields[] = "user_created";
		$order_item_tracking_values[] = dbquote($_SESSION["user_login"]);

		$order_item_tracking_fields[] = "date_created";
		$order_item_tracking_values[] = "current_timestamp";


		$sql = "insert into order_item_trackings (" . join(", ", $order_item_tracking_fields) . ") values (" . join(", ", $order_item_tracking_values) . ")";
		mysql_query($sql) or dberror($sql);

	}
	return true;
}


/********************************************************************
    Send e-mail in case the list of materials was changed
*********************************************************************/
function track_change_in_order_items_send_mail($project_id, $is_hq_supplied_item = false)
{
	//check if mail has to be sent (order steps 620 or 840
	$legal_type = 0;
	$budget_hq_supplied_items_approved = false;
	$full_budgetapproved = false;
	$sql = "select project_cost_type, actual_order_state_state " . 
		   " from projects " . 
		   " left join project_costs on project_cost_order = project_order " .
		   " left join actual_order_states on actual_order_state_order = project_order " .
		   " where project_id = " . dbquote($project_id) . 
		   " and actual_order_state_state in (34, 87) ";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$legal_type = $row["project_cost_type"];

		if($row["actual_order_state_state"] == 34)
		{
			$budget_hq_supplied_items_approved = true;
		}
		elseif($row["actual_order_state_state"] == 87)
		{
			$full_budgetapproved = true;
		}
	}

	
	if($legal_type > 0 
		and ($budget_hq_supplied_items_approved == true or $full_budgetapproved == true))
	{

		$sql = "";
		if($legal_type > 1)
		{
			$sql = "select sum(order_item_quantity_freezed) as hq_total1, " . 
				   " sum(order_item_quantity_hq_freezed) as hq_total2 " .
				   " from projects " .
				   " left join orders on project_order = order_id " .
				   " left join order_items on order_item_order = order_id " .
				   " where project_id = " . dbquote($project_id) . 
				   " and order_item_cost_group in (2,6) ";
		}
		elseif($full_budgetapproved == true)
		{
			$sql = "select sum(order_item_quantity_freezed) as hq_total1, " . 
				   " sum(order_item_quantity_hq_freezed) as hq_total2 " .
				   " from projects " .
				   " left join orders on project_order = order_id " .
				   " left join order_items on order_item_order = order_id " .
				   " where project_id = " . dbquote($project_id);
		}
		elseif($budget_hq_supplied_items_approved == true 
			and $full_budgetapproved == false
			and $is_hq_supplied_item == true)
		{
			$sql = "select sum(order_item_quantity_freezed) as hq_total1, " . 
				   " sum(order_item_quantity_hq_freezed) as hq_total2 " .
				   " from projects " .
				   " left join orders on project_order = order_id " .
				   " left join order_items on order_item_order = order_id " .
				   " where project_id = " . dbquote($project_id) . 
				   " and order_item_cost_group in (2,6) ";
		}
		
		if($sql)
		{
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				
				if($project_id > 0 
					and ($row["hq_total1"] > 0 or $row["hq_total2"] > 0))
				{
					$project = get_project($project_id);
					$user = get_user(user_id());
					
					
					/*if($full_budgetapproved == true)
					{
						if($user["address"] == $project["order_client_address"])
						{
							$actionmail = new ActionMail(87);
						}
						else
						{
							$actionmail = new ActionMail(88);
						}
					}
					else
					{*/
						if($user["address"] == $project["order_client_address"])
						{
							$actionmail = new ActionMail(69);
						}
						else
						{
							$actionmail = new ActionMail(70);
						}
					//}
					$actionmail->setParam('id', $project_id);
					

					$actionmail->send();
				}
			}
		}
	}

	return false;
}

/********************************************************************
    update order with exchange rate information
*********************************************************************/
function update_order_with_exchange_rate_info($order_id = 0, $currency_id = 1, $exchange_rate = 1, $order_type = 0)
{

	$currency = get_currency($currency_id);
	$factor = $currency["factor"];
	
	if($order_type == 1)
	{
		//update offers for local works (only for projects)
		$sql = "select * from lwoffers " . 
			   "left join currencies on currency_id = lwoffer_currency " . 
			   "where lwoffer_order = " . dbquote($order_id) . 
			   " and  lwoffer_currency = " . dbquote($currency_id);
		
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$sql2 = "update lwoffers set " .
					"lwoffer_exchange_rate = " . $exchange_rate . ", " . 
					"lwoffer_factor = " . $factor . " " . 
					"where lwoffer_id = " . $row["lwoffer_id"];

			mysql_query($sql2) or dberror($sql2);
		}
		
		//update exchange rate info in the order
		$sql = "update orders set order_client_currency = " . dbquote($currency_id) . ", " . 
			   "                  order_client_exchange_rate = " . dbquote($exchange_rate) . 
			   " where order_id = " . dbquote($order_id);

		$result = mysql_query($sql) or dberror($sql);
		$result = recalculate_all_prices($order_id, $order_type);
		return true;
	}
	elseif($order_type == 2)
	{
		//update exchange rate info in the order
		$sql = "update orders set order_client_currency = " . dbquote($currency_id) . ", " . 
			   "                  order_client_exchange_rate = " . dbquote($exchange_rate) . 
			   " where order_id = " . dbquote($order_id);

		$result = mysql_query($sql) or dberror($sql);
		$result = recalculate_all_prices($order_id, $order_type);
		return true;
	}
	
	return false;

}


/********************************************************************
    update order with actual exchange rate
*********************************************************************/
function  update_exchange_rates($order_id, $order_type = 0)
{
    
	if($order_type == 1) // project
	{
		$sql = "select order_client_currency ".
			   "from orders ".
			   "where order_id = " . $order_id;

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$currency_id = $row["order_client_currency"];
		}

		$sql = "select currency_exchange_rate, currency_factor ".
			   "from currencies ".
			   "where currency_id = " . $currency_id ;

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$exchange_rate = $row["currency_exchange_rate"];
			$factor = $row["currency_factor"];
		}

		//update offers for local works (only for projects)
		$sql = "select * from lwoffers " . 
			   "left join currencies on currency_id = lwoffer_currency " . 
			   "where lwoffer_order = " . dbquote($order_id) . 
			   " and  lwoffer_currency = " . dbquote($currency_id);
		
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$sql2 = "update lwoffers set " .
					"lwoffer_exchange_rate = " . $exchange_rate . ", " . 
					"lwoffer_factor = " . $factor . " " . 
					"where lwoffer_id = " . $row["lwoffer_id"];

			mysql_query($sql2) or dberror($sql2);
		}


		//update order
		$sql = "update orders ".
			   "set order_client_exchange_rate = " . $exchange_rate .
			   " where order_id = " .  $order_id;

		mysql_query($sql) or dberror($sql);
		$result = recalculate_all_prices($order_id, $order_type);


		//update costsheeets
		$sql = "select costsheet_id, currency_exchange_rate 
		       from costsheets
			   left join projects on project_id = costsheet_project_id
			   left join currencies on currency_id = costsheet_currency_id
			   where costsheet_version = 0
			   and project_order = " . dbquote($order_id);

	    $res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$sql2 = 'update costsheets set costsheet_exchangerate = ' . dbquote($row['currency_exchange_rate']) .
				    ' where costsheet_id = ' . dbquote($row['costsheet_id']);
			
			mysql_query($sql2) or dberror($sql2);
		}

		//update bids
		$sql = "select costsheet_bid_id, currency_exchange_rate 
		       from costsheet_bids
			   left join projects on project_id = costsheet_bid_project_id
			   left join currencies on currency_id = costsheet_bid_currency
			   where project_order = " . dbquote($order_id);

	    $res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$sql2 = 'update costsheet_bids set costsheet_bid_exchange_rate = ' . dbquote($row['currency_exchange_rate']) .
				    ' where costsheet_bid_id = ' . dbquote($row['costsheet_bid_id']);
			
			mysql_query($sql2) or dberror($sql2);
		}
		
		return true;
	}
	elseif($order_type == 2)
	{
		$sql = "select order_client_currency ".
			   "from orders ".
			   "where order_id = " . $order_id;

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$currency_id = $row["order_client_currency"];
		}

		$sql = "select currency_exchange_rate, currency_factor ".
			   "from currencies ".
			   "where currency_id = " . $currency_id ;

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$exchange_rate = $row["currency_exchange_rate"];
			$factor = $row["currency_factor"];
		}

		
		//update order
		$sql = "update orders ".
			   "set order_client_exchange_rate = " . $exchange_rate .
			   " where order_id = " .  $order_id;

		mysql_query($sql) or dberror($sql);
		$result = recalculate_all_prices($order_id, $order_type);

		return true;
	}
	return false;
}


/********************************************************************
    update order items with exchange rates
*********************************************************************/
function recalculate_all_prices($order_id, $order_type = 0)
{
	$sql = "select order_client_currency, order_client_exchange_rate, currency_factor ".
		   "from orders ".
		   "left join currencies on currency_id = order_client_currency " . 
		   "where order_id = " . $order_id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{

		$currency_id = $row["order_client_currency"];
		$exchange_rate = $row["order_client_exchange_rate"];
		$factor = $row["currency_factor"];
	
		
		if($order_type == 1)
		{
			
			//update all furniture with the item price from catalogue
			$sql = "select order_item_id, item_price, supplier_item_price, currency_exchange_rate " .
				   "from order_items ".
				   "left join items on item_id = order_item_item " . 
				   "left join suppliers on supplier_item = order_item_item " .
				   "left join currencies on currency_id = order_item_supplier_currency " . 
				   "where order_item_price_entered_in_loc = 0 and item_type = 1 " . 
				   " and supplier_address = order_item_supplier_address ". 
				   " and order_item_order = " . $order_id;


			$res = mysql_query($sql) or dberror($sql);
			while ($row = mysql_fetch_assoc($res))
			{
				$sql_u = "update order_items set order_item_system_price = " . dbquote($row["item_price"]) . ", " . 
					     " order_item_supplier_price = " . dbquote($row["supplier_item_price"]) . ", " .
					     " order_item_supplier_exchange_rate = " . dbquote($row["currency_exchange_rate"]) .
						 " where order_item_id = " . dbquote($row["order_item_id"]);

				$result = mysql_query($sql_u) or dberror($sql_u);
			
			}

			
			//update order items entered in CHF
			$sql = "select order_item_id, order_item_system_price, order_item_real_system_price, order_item_supplier_currency, " . 
				   "order_item_system_price_hq_freezed, order_item_system_price_freezed, order_item_real_system_price " . 
				   "from order_items ".
				   "where order_item_price_entered_in_loc = 0 " . 
				   " and order_item_price_entered_in_loc = 0 " . 
				   " and order_item_order = " . $order_id;

			$res = mysql_query($sql) or dberror($sql);
			while ($row = mysql_fetch_assoc($res))
			{
				$supplier_exchange_rate = 0;
				$sql_s = "select currency_exchange_rate, currency_exchange_rate_furniture ".
						 "from currencies ".
						 "where currency_id = " . dbquote($row["order_item_supplier_currency"]);



				$res_s = mysql_query($sql_s) or dberror($sql_s);
				if ($row_s = mysql_fetch_assoc($res_s))
				{
					if($row_s["currency_exchange_rate_furniture"] > 0)
					{
						$supplier_exchange_rate = $row_s["currency_exchange_rate_furniture"];
					}
					else
					{
						$supplier_exchange_rate = $row_s["currency_exchange_rate"];
					}
				}
				$client_price = $row["order_item_system_price"] / $exchange_rate * $factor;
				$client_price_hq_freezed = $row["order_item_system_price_hq_freezed"] / $exchange_rate * $factor;
				$client_price_freezed = $row["order_item_system_price_freezed"] / $exchange_rate * $factor;
				$client_real_price = $row["order_item_real_system_price"] / $exchange_rate * $factor;

				if($supplier_exchange_rate > 0)
				{
					$sql_u = "update order_items set order_item_supplier_exchange_rate = " . dbquote($supplier_exchange_rate) . ", " .
							 " order_item_client_price = " . dbquote($client_price) . ", " .
							 "  order_item_client_price_hq_freezed = " . dbquote($client_price_hq_freezed) . ", " .
							 "   order_item_client_price_freezed = " . dbquote($client_price_freezed) . ", " . 
							 "   order_item_real_client_price = " . dbquote($client_real_price) . 
							 " where order_item_id = " . dbquote($row["order_item_id"]);
				}
				else
				{
					$sql_u = "update order_items set order_item_client_price = " . dbquote($client_price) . ", " .
							 "  order_item_client_price_hq_freezed = " . dbquote($client_price_hq_freezed) . ", " .
							 "   order_item_client_price_freezed = " . dbquote($client_price_freezed) . ", " . 
							 "   order_item_real_client_price = " . dbquote($client_real_price) . 
							 " where order_item_id = " . dbquote($row["order_item_id"]);
				}

				$result = mysql_query($sql_u) or dberror($sql_u);
			}


			//update order items being entered in local currency
			$sql = "select order_item_id, order_item_client_price, order_item_real_client_price, order_item_supplier_currency, " . 
				   "order_item_client_price_hq_freezed, order_item_client_price_freezed, order_item_real_client_price " . 
				   "from order_items ".
				   "where order_item_price_entered_in_loc = 1 " . 
				   " and order_item_order = " . $order_id;

			$res = mysql_query($sql) or dberror($sql);
			while ($row = mysql_fetch_assoc($res))
			{
				$supplier_exchange_rate = 0;
				$sql_s = "select currency_exchange_rate, currency_exchange_rate_furniture ".
						 "from currencies ".
						 "where currency_id = " . dbquote($row["order_item_supplier_currency"]);

				$res_s = mysql_query($sql_s) or dberror($sql_s);
				if ($row_s = mysql_fetch_assoc($res_s))
				{
					if($row_s["currency_exchange_rate_furniture"] > 0)
					{
						$supplier_exchange_rate = $row_s["currency_exchange_rate_furniture"];
					}
					else
					{
						$supplier_exchange_rate = $row_s["currency_exchange_rate"];
					}
				}
				
				$system_price = $row["order_item_client_price"] * $exchange_rate / $factor;
				$system_price_hq_freezed = $row["order_item_client_price_hq_freezed"] * $exchange_rate / $factor;
				$system_price_freezed = $row["order_item_client_price_freezed"] * $exchange_rate / $factor;
				$system_real_price = $row["order_item_real_client_price"] * $exchange_rate / $factor;
				
				if($supplier_exchange_rate > 0)
				{
					$sql_u = "update order_items set order_item_supplier_exchange_rate = " . dbquote($supplier_exchange_rate) . ", " .
							 " order_item_system_price = " . dbquote($system_price) . ", " .
							 "  order_item_system_price_hq_freezed = " . dbquote($system_price_hq_freezed) . ", " .
							 "   order_item_system_price_freezed = " . dbquote($system_price_freezed) . ", " . 
							 "   order_item_real_system_price = " . dbquote($system_real_price) . 
							 " where order_item_id = " . dbquote($row["order_item_id"]);
				}
				else
				{
					$sql_u = "update order_items set order_item_system_price = " . dbquote($system_price) . ", " .
							 "  order_item_system_price_hq_freezed = " . dbquote($system_price_hq_freezed) . ", " .
							 "   order_item_system_price_freezed = " . dbquote($system_price_freezed) . ", " . 
							 "   order_item_real_system_price = " . dbquote($system_real_price) . 
							 " where order_item_id = " . dbquote($row["order_item_id"]);
				}


				$result = mysql_query($sql_u) or dberror($sql_u);
			}

			//order items from catalogue orders
			$sql = "select order_items_in_project_id, order_items_in_project_real_price " . 
				   " from order_items_in_projects " .
				   " left join projects on project_order = order_items_in_project_project_order_id " .  
				   " where project_order = " . dbquote($order_id);


			$res = mysql_query($sql) or dberror($sql);
			while ($row = mysql_fetch_assoc($res))
			{
				$client_price = $row["order_items_in_project_real_price"] / $exchange_rate * $factor;
				$sql_u = "update order_items_in_projects " . 
					   " set order_items_in_project_real_client_price = " . dbquote($client_price ) . 
					   " where order_items_in_project_id = " . $row["order_items_in_project_id"];

				$result = mysql_query($sql_u) or dberror($sql_u);

			}

			return true;
		}
		elseif($order_type == 2)
		{
			//update all furniture with the item price from catalogue
			$sql = "select order_item_id, item_price, supplier_item_price, currency_exchange_rate " .
				   "from order_items ".
				   "left join items on item_id = order_item_item " . 
				   "left join suppliers on supplier_item = order_item_item " .
				   "left join currencies on currency_id = order_item_supplier_currency " . 
				   "where order_item_price_entered_in_loc = 0 and item_type = 1 " . 
				   " and supplier_address = order_item_supplier_address ". 
				   " and order_item_order = " . $order_id;


			$res = mysql_query($sql) or dberror($sql);
			while ($row = mysql_fetch_assoc($res))
			{
				$sql_u = "update order_items set order_item_system_price = " . dbquote($row["item_price"]) . ", " . 
					     " order_item_supplier_price = " . dbquote($row["supplier_item_price"]) . ", " .
					     " order_item_supplier_exchange_rate = " . dbquote($row["currency_exchange_rate"]) .
						 " where order_item_id = " . dbquote($row["order_item_id"]);

				$result = mysql_query($sql_u) or dberror($sql_u);
			
			}


			
			//update order items entered in CHF
			$sql = "select order_item_id, order_item_system_price, order_item_real_system_price, order_item_supplier_currency, " . 
				   "order_item_system_price_hq_freezed, order_item_system_price_freezed, order_item_real_system_price " . 
				   "from order_items ".
				   "where order_item_price_entered_in_loc = 0 " . 
				   " and order_item_order = " . $order_id;

			$res = mysql_query($sql) or dberror($sql);
			while ($row = mysql_fetch_assoc($res))
			{
				$supplier_exchange_rate = 0;
				$sql_s = "select currency_exchange_rate, currency_exchange_rate_furniture ".
						 "from currencies ".
						 "where currency_id = " . dbquote($row["order_item_supplier_currency"]);

				$res_s = mysql_query($sql_s) or dberror($sql_s);
				if ($row_s = mysql_fetch_assoc($res_s))
				{
					if($row_s["currency_exchange_rate_furniture"] > 0)
					{
						$supplier_exchange_rate = $row_s["currency_exchange_rate_furniture"];
					}
					else
					{
						$supplier_exchange_rate = $row_s["currency_exchange_rate"];
					}
				}
				
				$client_price = $row["order_item_system_price"] / $exchange_rate * $factor;


				if($supplier_exchange_rate > 0)
				{
					$sql_u = "update order_items set order_item_supplier_exchange_rate = " . dbquote($supplier_exchange_rate) . ", " .
							 " order_item_client_price = " . dbquote($client_price) . 
							 " where order_item_id = " . dbquote($row["order_item_id"]);
				}
				else
				{
					$sql_u = "update order_items set order_item_client_price = " . dbquote($client_price) . 
							 " where order_item_id = " . dbquote($row["order_item_id"]);
				}



				$result = mysql_query($sql_u) or dberror($sql_u);
			}

			//update order items being entered in local currency
			$sql = "select order_item_id, order_item_client_price, order_item_real_client_price, order_item_supplier_currency, " . 
				   "order_item_client_price_hq_freezed, order_item_client_price_freezed, order_item_real_client_price " . 
				   "from order_items ".
				   "where order_item_price_entered_in_loc = 1 " . 
				   " and order_item_order = " . $order_id;

			$res = mysql_query($sql) or dberror($sql);
			while ($row = mysql_fetch_assoc($res))
			{
				$supplier_exchange_rate = 0;
				$sql_s = "select currency_exchange_rate, currency_exchange_rate_furniture ".
						 "from currencies ".
						 "where currency_id = " . dbquote($row["order_item_supplier_currency"]);



				$res_s = mysql_query($sql_s) or dberror($sql_s);
				if ($row_s = mysql_fetch_assoc($res_s))
				{
					if($row_s["currency_exchange_rate_furniture"] > 0)
					{
						$supplier_exchange_rate = $row_s["currency_exchange_rate_furniture"];
					}
					else
					{
						$supplier_exchange_rate = $row_s["currency_exchange_rate"];
					}
				}
				
				$system_price = $row["order_item_client_price"] * $exchange_rate / $factor;
				
				if($supplier_exchange_rate > 0)
				{
					$sql_u = "update order_items set order_item_supplier_exchange_rate = " . dbquote($supplier_exchange_rate) . ", " .
							 " order_item_system_price = " . dbquote($system_price) . 
							 " where order_item_id = " . dbquote($row["order_item_id"]);
				}
				else
				{
					$sql_u = "update order_items set order_item_system_price = " . dbquote($system_price) . 
							 " where order_item_id = " . dbquote($row["order_item_id"]);
				}


				$result = mysql_query($sql_u) or dberror($sql_u);
			}
			
			return true;
		}
	}
	return false;
}

/********************************************************************
    save logistic state code for a project
*********************************************************************/
function save_logistic_state_code($order_id = 0, $order_actual_order_state_code = '')
{
	$code = '';

	
	//check if arrival of all items was confirmed
	$sql = "select actual_order_state_state " . 
		   " from actual_order_states " .
		   " left join users on user_id = actual_order_state_user " . 
		   " where actual_order_state_order = " . dbquote($order_id) .
		   " and actual_order_state_state in (41, 69) " . 
		   " order by actual_order_states.date_created DESC ";

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);

	if($row["actual_order_state_state"] == 41)
	{
		$result = update_logistic_status($order_id, "800");
		return '800';
	}
	elseif($row["actual_order_state_state"] == 69)
	{
		$result = update_logistic_status($order_id, "810");
		return '810';
	}



	//check if there is a logistic state available
	$sql = "select count(actual_order_state_id) as num_recs " . 
		   " from actual_order_states " . 
		   " left join order_states on order_state_id = actual_order_state_state " . 
		   " where actual_order_state_order = " . dbquote($order_id) .
		   " and (order_state_used_for_logistics = 1 or actual_order_state_state = 34)";

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	
	if($row["num_recs"] == 0)
	{
		$result = update_logistic_status($order_id, $code);
		return $code;
	}
	
	//get all involved suppliers
	$num_of_suppliers = 0;
	$sql = "select count(DISTINCT order_item_supplier_address) as num_suppliers  " . 
		   " from order_items " .
		   " where order_item_order = " . dbquote($order_id) .
		   " and order_item_quantity > 0 " .
		   " and order_item_supplier_address > 0 ";


	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$num_of_suppliers = $row["num_suppliers"];

	if($num_of_suppliers == 0)
	{
		$result = update_logistic_status($order_id, $code);
		return $code ;
	}



	//get all involved forwarders
	$num_of_forwarders = 0;
	$sql = "select count(DISTINCT order_item_forwarder_address) as num_forwarders  " . 
		   " from order_items " .
		   " where order_item_order = " . dbquote($order_id) .
		   " and order_item_quantity > 0 " .
		   " and order_item_forwarder_address > 0 ";


	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$num_of_forwarders = $row["num_forwarders"];





	//check if all forwarders have confirmed the arrival
	$sql = "select count(DISTINCT user_address) as num_forwarders " . 
		   " from actual_order_states " .
		   " left join users on user_id = actual_order_state_user " . 
		   " where actual_order_state_order = " . dbquote($order_id) .
		   " and actual_order_state_state = 40";

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);

	if($num_of_forwarders > 0 and $num_of_forwarders <= $row["num_forwarders"])
	{
		$result = update_logistic_status($order_id, "750");
		return '750';
	}


	//get number of items
	$sql = "select count(order_item_id) as num_recs, " .
		   " SUM(order_item_ordered <> '0000-00-00' and order_item_ordered is not null) AS num_ordered " . 
		   " from order_items " .
		   " where order_item_order = " . dbquote($order_id) .
		   " and order_item_supplier_address > 0 " . 
		   " and order_item_quantity > 0 " . 
		   " and order_item_not_in_budget <> 1 ";

	   
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$num_of_items = $row["num_recs"];
	$num_of_ordered_items = $row["num_ordered"];

	
	if($num_of_ordered_items == 0)
	{
		$result = update_logistic_status($order_id, "");
		return '';
	}
	elseif($num_of_ordered_items > 0 and $num_of_items > $num_of_ordered_items)
	{
		$result = update_logistic_status($order_id, "700");
		return '700';
	}

	//check if all suppliers have confirmed the order
	$sql = "select count(DISTINCT user_address) as num_suppliers " . 
		   " from actual_order_states " .
		   " left join users on user_id = actual_order_state_user " . 
		   " where actual_order_state_order = " . dbquote($order_id) .
		   " and actual_order_state_state = 37";

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);

	if($num_of_suppliers > 0 and $num_of_suppliers > $row["num_suppliers"])
	{
		$result = update_logistic_status($order_id, "700");
		return '700';
	}


	//get all forwarders having a request for delivery
	$sql = "select count(DISTINCT user_address) as num_forwarders " . 
		   " from order_mails " .
		   " left join users on user_id = order_mail_user " . 
		   " where order_mail_order = " . dbquote($order_id) . 
		   " and order_mail_is_cc <> 1 " . 
		   " and order_mail_order_state = 38";
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);

	if($num_of_forwarders > 0 and $num_of_forwarders > $row["num_forwarders"])
	{
		$result = update_logistic_status($order_id, "720");
		return '720';
	}


	//check if all forwarders have accepted request for delivery
	$sql = "select count(DISTINCT user_address) as num_forwarders " . 
		   " from actual_order_states " .
		   " left join users on user_id = actual_order_state_user " . 
		   " where actual_order_state_order = " . dbquote($order_id) .
		   " and actual_order_state_state = 39";

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);

	if($num_of_forwarders > 0 and $num_of_forwarders > $row["num_forwarders"])
	{
		$result = update_logistic_status($order_id, "730");
		return '730';
	}

	//check if all suppliers have confirmed pickup
	$sql = "select count(order_item_id) as num_recs, " .
		   " SUM(order_item_pickup <> '0000-00-00' and order_item_pickup is not null) AS num_picked_up " . 
		   " from order_items " .
		   " where order_item_order = " . dbquote($order_id) .
		   " and order_item_supplier_address > 0 " . 
		   " and order_item_quantity > 0 " . 
		   " and order_item_not_in_budget <> 1 ";

	   
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$num_of_items = $row["num_recs"];
	$num_of_picked_up_items = $row["num_picked_up"];

	
		
	if($num_of_picked_up_items >= 0 and $num_of_items > $num_of_picked_up_items)
	{
		$result = update_logistic_status($order_id, "740");
		return '740';
	}



	//check if all items have arrived
	if($num_of_forwarders > 0)
	{
		$sql = "select count(order_item_id) as num_recs, " .
			   " SUM(order_item_arrival <> '0000-00-00' and order_item_arrival is not null) AS num_arrived " . 
			   " from order_items " .
			   " where order_item_order = " . dbquote($order_id) .
			   " and order_item_supplier_address > 0 " . 
			   " and order_item_quantity > 0 " . 
			   " and order_item_not_in_budget <> 1 ";

		   
		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		$num_of_items = $row["num_recs"];
		$num_of_arrived_items = $row["num_arrived"];


		if($num_of_arrived_items >= 0 and $num_of_items > $num_of_arrived_items)
		{
			$result = update_logistic_status($order_id, "745");
			return '745';
		}
		else
		{
			//check if all forwarders have confirmed the arrival
			$sql = "select count(DISTINCT user_address) as num_forwarders " . 
				   " from actual_order_states " .
				   " left join users on user_id = actual_order_state_user " . 
				   " where actual_order_state_order = " . dbquote($order_id) .
				   " and actual_order_state_state = 40";

			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);

			if($num_of_forwarders > 0 and $num_of_forwarders > $row["num_forwarders"])
			{
				$result = update_logistic_status($order_id, "745");
				return '745';
			}
		}

	}

	return $code ;
}

/********************************************************************
    save development state for a project
*********************************************************************/
function save_project_development_status($order_id = 0, $order_state_code = '')
{
	
	$development_state = array("code"=>"", "name"=>"");

	$sql = "select order_state_code, concat(order_state_code, ' ', order_state_name) as ostate, " .
           " actual_order_states.date_created as submitted " . 
           " from actual_order_states " .
           " left join order_states on order_state_id = actual_order_state_state " .
           " where actual_order_state_order = " . dbquote($order_id) .
           " and order_state_used_for_logistics <> 1 " .
           " and order_state_code > '120' and order_state_code < '890' " .
           " order by actual_order_states.date_created DESC";

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $development_state = array("code"=>$row["order_state_code"], "name"=>$row["ostate"]);
    }
	else
	{
		$sql = "select order_state_code, concat(order_state_code, ' ', order_state_name) as ostate, " .
			   " actual_order_states.date_created as submitted " . 
			   " from actual_order_states " .
			   " left join order_states on order_state_id = actual_order_state_state " .
			   " where actual_order_state_order = " . dbquote($order_id) .
			   " and order_state_used_for_logistics <> 1 " .
			   " and order_state_code <= '120' " .
			   " order by order_state_code";

		$res = mysql_query($sql) or dberror($sql);

		while ($row = mysql_fetch_assoc($res))
		{
			$development_state = array("code"=>$row["order_state_code"], "name"=>$row["ostate"]);
		}
	
	}

	//check if budget for HQ supplied items was approved budget was not resubmitted.
	$approval_date = "0000-00-00";
	$submission_date = "0000-00-00";
	$development_state_submitted = array();
	$development_state_approved = array();

	$sql = "select order_state_code, concat(order_state_code, ' ', order_state_name) as ostate, " .
		   " actual_order_states.date_created as submitted " . 
		   " from actual_order_states " .
		   " left join order_states on order_state_id = actual_order_state_state " .
		   " where actual_order_state_order = " . dbquote($order_id) .
		   " and order_state_used_for_logistics <> 1 " .
		   " and actual_order_state_state in (34, 32) " . 
		   " order by actual_order_states.date_created ASC";

	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{
		if($row["order_state_code"] == '600')
		{
			$submission_date = $row["submitted"];
			$development_state_submitted = array("code"=>$row["order_state_code"], "name"=>$row["ostate"]);
		}
		elseif($row["order_state_code"] == '620')
		{
			$approval_date = $row["submitted"];
			$development_state_approved = array("code"=>$row["order_state_code"], "name"=>$row["ostate"]);
		}
	}

	if(count($development_state_approved) > 0)
	{
		if(count($development_state_submitted) > 0)
		{
			if($submission_date < $approval_date)
			{
				$development_state = $development_state_approved;
			}
			else
			{
				$development_state = $development_state_submitted;
			}
		}
		else
		{
			$development_state = $development_state_approved;
		}
	}



	//check if full budget  was approved full budget was not resubmitted.
	$approval_date = "0000-00-00";
	$submission_date = "0000-00-00";
	$development_state_submitted = array();
	$development_state_approved = array();

	$sql = "select order_state_code, concat(order_state_code, ' ', order_state_name) as ostate, " .
		   " actual_order_states.date_created as submitted " . 
		   " from actual_order_states " .
		   " left join order_states on order_state_id = actual_order_state_state " .
		   " where actual_order_state_order = " . dbquote($order_id) .
		   " and order_state_used_for_logistics <> 1 " .
		   " and actual_order_state_state in(85, 86, 87) " .
		   " order by actual_order_states.date_created ASC";

	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{
		if($row["order_state_code"] == '820')
		{
			$submission_date = $row["submitted"];
			$development_state_submitted = array("code"=>$row["order_state_code"], "name"=>$row["ostate"]);
		}
		elseif($row["order_state_code"] == '840')
		{
			$approval_date = $row["submitted"];
			$development_state_approved = array("code"=>$row["order_state_code"], "name"=>$row["ostate"]);
		}
		else
		{
			$development_state = array("code"=>$row["order_state_code"], "name"=>$row["ostate"]);
		}
	}

	if(count($development_state_approved) > 0)
	{
		if(count($development_state_submitted) > 0)
		{
			if($submission_date < $approval_date)
			{
				$development_state = $development_state_approved;
			}
			else
			{
				$development_state = $development_state_submitted;
			}
		}
		else
		{
			$development_state = $development_state_approved;
		}
	}
	elseif(count($development_state_submitted) > 0)
	{
		$development_state = $development_state_submitted;
	}


	$sql = "select order_state_code, concat(order_state_code, ' ', order_state_name) as ostate, " .
		   " actual_order_states.date_created as submitted " . 
		   " from actual_order_states " .
		   " left join order_states on order_state_id = actual_order_state_state " .
		   " where actual_order_state_order = " . dbquote($order_id) .
		   " and order_state_used_for_logistics <> 1 " .
		   " and actual_order_state_state > 87 " .
		   " and order_state_code < '890' " . 
		   " order by actual_order_states.date_created DESC";

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$development_state = array("code"=>$row["order_state_code"], "name"=>$row["ostate"]);
	}

	if($order_state_code)
	{
		$sql = "select concat(order_state_code, ' ', order_state_name) as ostate " .
			   " from order_states " .
			   " left join order_state_groups on order_state_group_id = order_state_group " .
			   " where order_state_group_order_type = 1 " . 
			   " and order_state_code = " . dbquote($order_state_code);

			$res = mysql_query($sql) or dberror($sql);

			if ($row = mysql_fetch_assoc($res))
			{
				$development_state = array("code"=>$order_state_code, "name"=>$row["ostate"]);
			}
	}


	//update development status
	$sql_u = "update orders set order_development_status = " . dbquote($development_state["code"]) . 
			 " where order_id = " . dbquote($order_id);

	$result = mysql_query($sql_u) or dberror($sql_u);
	
	return $development_state;
}


/********************************************************************
    save development state for a project
*********************************************************************/
function update_sales_area_from_approved_layout($project_id, $order_id = 0, $sqms = 0)
{
	if($sqms == 0)
	{
		return false;
	}


	//update project_costs
	$sql_u = "update project_costs set project_cost_sqms = " . dbquote($sqms) . 
		     " where project_cost_order = " . dbquote($order_id);

	$result = mysql_query($sql_u) or dberror($sql_u);

	//update posaddress
	$sql = "select posorder_posaddress, posorder_parent_table, project_actual_opening_date " . 
		   " from posorderspipeline " .
		   " left join projects on project_order = posorder_order " . 
		   " where posorder_order = " . dbquote($order_id);


	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		if($row["posorder_parent_table"] == 'posaddresses'
			and $row["project_actual_opening_date"] != NULL
			and $row["project_actual_opening_date"] != '0000-00-00')
		{
			$sql_u = "update posaddresses set posaddress_store_retailarea = " . dbquote($sqms) . 
				     " where posaddress_id = " . dbquote($row["posorder_posaddress"]);

			

			$result = mysql_query($sql_u) or dberror($sql_u);

		}
		elseif($row["posorder_parent_table"] == 'posaddressespipeline')
		{
			$sql_u = "update posaddressespipeline set posaddress_store_retailarea = " . dbquote($sqms) . 
				     " where posaddress_id = " . dbquote($row["posorder_posaddress"]);

			$result = mysql_query($sql_u) or dberror($sql_u);
		}
	}
	else
	{
		$sql = "select posorder_posaddress, project_actual_opening_date " . 
			   " from posorders " .
			   " left join projects on project_order = posorder_order " . 
			   " where posorder_order = " . dbquote($order_id);
		

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			if( $row["project_actual_opening_date"] != NULL
				and $row["project_actual_opening_date"] != '0000-00-00')
			{
				$sql_u = "update posaddresses set posaddress_store_retailarea = " . dbquote($sqms) . 
						 " where posaddress_id = " . dbquote($row["posorder_posaddress"]);

				$result = mysql_query($sql_u) or dberror($sql_u);

			}
		}
	}


	//update lan and af/cer
	
	$sql_u = "update ln_basicdata set ln_basicdata_version_sqms = " . dbquote($sqms) . 
			 " where ln_basicdata_version = 0 and ln_basicdata_project = " . dbquote($project_id);

	$result = mysql_query($sql_u) or dberror($sql_u);

	$sql_u = "update cer_basicdata set cer_basicdata_version_sqms = " . dbquote($sqms) . 
			 " where cer_basicdata_version = 0 and cer_basicdata_project = " . dbquote($project_id);

	$result = mysql_query($sql_u) or dberror($sql_u);

	return true;
}

?>