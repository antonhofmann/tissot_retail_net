<?php
/********************************************************************

    ajx_save_cost_sheet_position.php

    Save cosheet position

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2017-12-19
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2017-12-19
    Version:        1.0.0

    Copyright (c) 2017, Tissot SA, All Rights Reserved.

*********************************************************************/

require_once "../../include/frame.php";

if(!has_access("can_edit_project_costs_budget") ) {
	echo "error";
}
else {
	$action = '';
	if(array_key_exists('action', $_POST)) {
		$action = $_POST['action'];
	}
	
	
	if($action == 'save') {

		$return_values = array();
		$project_id = (int)$_POST['pid'];
		$id = $_POST['id'];
		$value = strip_tags($_POST['value']);

		$id = str_replace('__costsheets_', '', $id);
		$pos = strrpos ( $id , '_');

		$field = substr($id, 0, $pos);
		$key = (int)substr($id, $pos+1, strlen($id));

		$share = 0;

		$return_values['costsheet_id'] = $key;


		if($field == 'costsheet_currency_id') {
			$sql = "select currency_exchange_rate, currency_factor 
			        from currencies 
			        where currency_id = " . dbquote($value);
			$res = mysql_query($sql) or dberror($sql);

			if($row = mysql_fetch_assoc($res)) {

				$sql = "update costsheets
						 SET $field = " . dbquote($value) . ",
						 costsheet_exchangerate = " . $row['currency_exchange_rate'] . ",
						 date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", 
						 user_modified = " . dbquote(user_login()) .
					   " where costsheet_version = 0
					and costsheet_id = " . dbquote($key);

				$result = mysql_query($sql);
			}
		}
		else {
			$sql = "update costsheets
					 SET $field = " . dbquote($value) . ", 
					 date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", 
					 user_modified = " . dbquote(user_login()) .
				   " where costsheet_version = 0
				and costsheet_id = " . dbquote($key);

			$result = mysql_query($sql);
		}

		if(is_numeric($value)) {
			//calculate amount in CHF
			$return_values['amount_chf'] = '';
			$sql = "select costsheet_exchangerate, currency_factor, costsheet_budget_amount 
			        from costsheets
					left join currencies on currency_id = costsheet_currency_id 
			        where costsheet_version = 0
					and costsheet_id = " . dbquote($key);
			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res)) {

				$return_values['amount_chf'] = ($row["costsheet_exchangerate"] * $row["costsheet_budget_amount"])/$row["currency_factor"];
			}
			
			//get new group total;
			$sql = "select costsheet_pcost_group_id, costsheet_pcost_subgroup_id 
					from costsheets
					where costsheet_version = 0
			        and costsheet_id = " . dbquote($key);

			$result = mysql_query($sql);
			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res)) {
				$group = $row["costsheet_pcost_group_id"];
				$subgroup = $row["costsheet_pcost_subgroup_id"];
				$return_values['costsheet_pcost_group_id'] = $group;
				$return_values['gr_total'] = 0;
				$return_values['gr_total_chf'] = 0;
				$return_values['costsheet_pcost_subgroup_id'] = $subgroup;
				$return_values['sgr_total'] = 0;
				$return_values['sgr_total_chf'] = 0;
				$return_values['project_share_other'] = 0;
			
				$sql = "select costsheet_pcost_group_id, order_client_exchange_rate, currency_factor,
						(select sum(costsheet_budget_amount*costsheet_exchangerate/currency_factor) 
						   from costsheets 
							where costsheet_version = 0
							and costsheet_project_id = " . dbquote($project_id) .
							" and costsheet_pcost_group_id = " . dbquote($group) . ") as gr_total
					   from costsheets
					   left join projects on project_id = costsheet_project_id 
					   left join orders on order_id = project_order
					   left join currencies on currency_id = order_client_currency 
					   where costsheet_version = 0
			           and costsheet_project_id = " . dbquote($project_id) .
					   " and costsheet_pcost_group_id = " . dbquote($group) . 
					   " group by costsheet_pcost_group_id";

				$result = mysql_query($sql);
				$res = mysql_query($sql) or dberror($sql);
				if($row = mysql_fetch_assoc($res)) {
					$return_values['gr_total_chf'] = $row['gr_total'];
					$return_values['gr_total'] = $row['currency_factor']*$row['gr_total']/$row['order_client_exchange_rate'];
				}

				//get new sub group total;
				$sql = "select costsheet_pcost_subgroup_id, order_client_exchange_rate, currency_factor,
						(select sum(costsheet_budget_amount*costsheet_exchangerate/currency_factor) 
							from costsheets 
							where costsheet_version = 0 
							  and costsheet_project_id = " . dbquote($project_id) .
							" and costsheet_pcost_subgroup_id = " . dbquote($subgroup) . ") as sgr_total
					   from costsheets
					   left join projects on project_id = costsheet_project_id 
					   left join orders on order_id = project_order
					   left join currencies on currency_id = order_client_currency 
					   where costsheet_version = 0
			             and costsheet_project_id = " . dbquote($project_id) .
					   " and costsheet_pcost_subgroup_id = " . dbquote($subgroup) . 
					   " group by costsheet_pcost_subgroup_id";
	
				$result = mysql_query($sql);
				$res = mysql_query($sql) or dberror($sql);
				if($row = mysql_fetch_assoc($res)) {
					$return_values['sgr_total_chf'] = $row['sgr_total'];
					$return_values['sgr_total'] = $row['currency_factor']*$row['sgr_total']/$row['order_client_exchange_rate'];
				}
			}


			//update busines partner's shares
			$share = 0;
			$sql = "select costsheet_project_id, 
					sum(costsheet_budget_amount*costsheet_exchangerate/currency_factor) as budget_total,
					sum(costsheet_partner_contribution*costsheet_budget_amount*costsheet_exchangerate/currency_factor/100) as partner_total
					from costsheets
					left join projects on project_id = costsheet_project_id 
					   left join orders on order_id = project_order
					   left join currencies on currency_id = order_client_currency 
					where costsheet_version = 0
			and costsheet_project_id = " . dbquote($project_id);
			$res = mysql_query($sql);
			if($row = mysql_fetch_assoc($res)) {
				$budget_total = $row["budget_total"];
				$partner_total = $row["partner_total"];
				if($budget_total > 0) {
					$share = round(100*($partner_total/$budget_total), 2);
				}


				if($share > 0) {
					$sql = "update projects set " . 
						 "project_share_other = " . dbquote($share) . ", " .
						 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", " . 
						 "user_modified = " . dbquote(user_login()) . 
						 " where project_id = " . dbquote($project_id);

					$result = mysql_query($sql) or dberror($sql);

					$sql = "update cer_basicdata set " . 
						 "cer_basicdata_franchsiee_investment_share = " . dbquote($share) . ", " .
						 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", " . 
						 "user_modified = " . dbquote(user_login()) . 
						 " where cer_basicdata_version = 0 and cer_basicdata_project = " . dbquote($project_id);

					$result = mysql_query($sql) or dberror($sql);
				}
			}
			$return_values['project_share_other'] = $share;

			if(count($return_values) == 9) {
				echo json_encode($return_values);
			}
			else {
				echo 'error';
			}
		}
		else {
			echo 'error';
		}
	}
	elseif($action == 'new_line') {
		
		$data = $_POST['data'];
		$fields = explode('-', $data);

		if(count($fields) == 3)
		{
			$currency_id = '';
			$sql = "select order_client_currency, order_client_exchange_rate 
			        from projects
					left join orders on order_id = project_order
			        where project_id = " . dbquote($fields[0]);
			$res = mysql_query($sql) or dberror($sql);

			if($row = mysql_fetch_assoc($res)) {
				$currency_id = $row['order_client_currency'];
				$exchange_rate = $row['order_client_exchange_rate'];
				
			}
			
			if($currency_id > 0) {
				$sql = "INSERT into costsheets (costsheet_project_id, costsheet_version, costsheet_pcost_group_id, 
						costsheet_pcost_subgroup_id, costsheet_is_in_budget, costsheet_currency_id, costsheet_exchangerate, user_created, date_created)
						VALUES ("  . 
						dbquote($fields[0]) . ", 0, " .
						dbquote($fields[1]) . ", " . 
						dbquote($fields[2]) . ", 1, " .
						dbquote($currency_id) . ", " .
						dbquote($exchange_rate) . ", " .
						dbquote(user_login()) . ", " . 
						dbquote(date("Y-m-d H:i:s")) . ")";

				$result = mysql_query($sql);
				echo mysql_insert_id();
			}
			else {
				echo 'error';
			}
		}
		else {
			echo 'error';
		}
		
	}
	elseif($action == 'delete_line') {
		
		$project_id = (int)$_POST['pid'];
		$id = (int)$_POST['id'];


		$sql = "select costsheet_pcost_group_id, costsheet_pcost_subgroup_id 
				from costsheets
				where costsheet_version = 0
			and costsheet_id = " . dbquote($id);

		$result = mysql_query($sql);
		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res)) {

			$group = $row["costsheet_pcost_group_id"];
			$subgroup = $row["costsheet_pcost_subgroup_id"];
			$return_values['costsheet_pcost_group_id'] = $group;
			$return_values['gr_total'] = 0;
			$return_values['gr_total_chf'] = 0;
			$return_values['costsheet_pcost_subgroup_id'] = $subgroup;
			$return_values['sgr_total'] = 0;
			$return_values['sgr_total_chf'] = 0;
			$return_values['project_share_other'] = 0;

			$sql = "Delete from costsheets 
					where costsheet_version = 0
			and costsheet_id = " . dbquote($id);

			$result = mysql_query($sql);
		
			$sql = "select costsheet_pcost_group_id, order_client_exchange_rate, currency_factor,
					(select sum(costsheet_budget_amount*costsheet_exchangerate/currency_factor) 
					   from costsheets 
						where costsheet_version = 0
						and costsheet_project_id = " . dbquote($project_id) .
						" and costsheet_pcost_group_id = " . dbquote($group) . ") as gr_total
				   from costsheets
				   left join projects on project_id = costsheet_project_id 
				   left join orders on order_id = project_order
				   left join currencies on currency_id = order_client_currency 
				   where costsheet_version = 0
				   and costsheet_project_id = " . dbquote($project_id) .
				   " and costsheet_pcost_group_id = " . dbquote($group) . 
				   " group by costsheet_pcost_group_id";

			$result = mysql_query($sql);
			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res)) {
				$return_values['gr_total_chf'] = $row['gr_total'];
				$return_values['gr_total'] = $row['currency_factor']*$row['gr_total']/$row['order_client_exchange_rate'];
			}

			//get new sub group total;
			$sql = "select costsheet_pcost_subgroup_id, order_client_exchange_rate, currency_factor,
					(select sum(costsheet_budget_amount*costsheet_exchangerate/currency_factor) 
						from costsheets 
						where costsheet_version = 0 
						  and costsheet_project_id = " . dbquote($project_id) .
						" and costsheet_pcost_subgroup_id = " . dbquote($subgroup) . ") as sgr_total
				   from costsheets
				   left join projects on project_id = costsheet_project_id 
				   left join orders on order_id = project_order
				   left join currencies on currency_id = order_client_currency 
				   where costsheet_version = 0
					 and costsheet_project_id = " . dbquote($project_id) .
				   " and costsheet_pcost_subgroup_id = " . dbquote($subgroup) . 
				   " group by costsheet_pcost_subgroup_id";

			$result = mysql_query($sql);
			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res)) {
				$return_values['sgr_total_chf'] = $row['sgr_total'];
				$return_values['sgr_total'] = $row['currency_factor']*$row['sgr_total']/$row['order_client_exchange_rate'];
			}


			//update busines partner's shares
			$share = 0;
			$sql = "select costsheet_project_id, 
					sum(costsheet_budget_amount*costsheet_exchangerate/currency_factor) as budget_total,
					sum(costsheet_partner_contribution*costsheet_budget_amount*costsheet_exchangerate/currency_factor/100) as partner_total
					from costsheets
					left join projects on project_id = costsheet_project_id 
					   left join orders on order_id = project_order
					   left join currencies on currency_id = order_client_currency 
					where costsheet_version = 0
			and costsheet_project_id = " . dbquote($project_id);
			$res = mysql_query($sql);
			if($row = mysql_fetch_assoc($res)) {
				$budget_total = $row["budget_total"];
				$partner_total = $row["partner_total"];
				if($budget_total > 0) {
					$share = round(100*($partner_total/$budget_total), 2);
				}


				if($share > 0) {
					$sql = "update projects set " . 
						 "project_share_other = " . dbquote($share) . ", " .
						 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", " . 
						 "user_modified = " . dbquote(user_login()) . 
						 " where project_id = " . dbquote($project_id);

					$result = mysql_query($sql) or dberror($sql);

					$sql = "update cer_basicdata set " . 
						 "cer_basicdata_franchsiee_investment_share = " . dbquote($share) . ", " .
						 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", " . 
						 "user_modified = " . dbquote(user_login()) . 
						 " where cer_basicdata_version = 0 and cer_basicdata_project = " . dbquote($project_id);

					$result = mysql_query($sql) or dberror($sql);
				}
			}
			$return_values['project_share_other'] = $share;
			echo json_encode($return_values);
		}
		else {
			echo 'error';
		}
	}
	else {

		echo 'error';
	}

}

?>