<script language="Javascript">
	$(document).ready(function(){
		var exchangerates = new Array(); 
		<?php
		foreach($currency_list as $currency_id=>$currency_info)
		{
			echo 'exchangerates[' . $currency_id . '] = ' . $currency_info['exchange_rate'] . ';';
		}
		?>


		$("#order_client_currency" ).change(function() {
					
			var currency = $("#order_client_currency").val();

			$("#order_client_exchange_rate" ).val(exchangerates[currency]);					
		});
	
	});
</script>