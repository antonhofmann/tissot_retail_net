<?php
/********************************************************************

    ajx_cost_sheet_get_available_items.php

    Cost Sheet: get item selection

    Created by:     Anton Hofmann (anton.hofmann@codingroom.ch)
    Date created:   2017-12-28
    Modified by:    Anton Hofmann (anton.hofmann@codingroom.ch)
    Date modified:  2017-12-28
    Version:        1.0.0

    Copyright (c) 2017, Tissot, All Rights Reserved.

*********************************************************************/

require_once "../../include/frame.php";
require_once "get_functions.php";

if(!has_access("can_edit_project_costs_budget") ) {
	echo "error";
}
else {

	$project_id = (int)$_POST['pid'];
	$item_category_id = (int)$_POST['id'];

	if($project_id and $item_category_id) {

		
		//get all cost sub groups
		$cost_sub_groups = '<option Velue=""></option>';
		$sql = "select DISTINCT concat(costsheet_pcost_group_id, '-', costsheet_pcost_subgroup_id) as sgrid, 
		        concat(pcost_subgroup_code, ' ', pcost_subgroup_name) as sgrname
				from costsheets
				left join pcost_subgroups on pcost_subgroup_id = costsheet_pcost_subgroup_id
				where costsheet_version = 0
				and costsheet_project_id = " . dbquote($project_id) .
				" and costsheet_pcost_group_id in (2, 3)
				order by costsheet_pcost_group_id, pcost_subgroup_code";
		
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$cost_sub_groups .= '<option value="' . $row['sgrid'] .'">' . $row['sgrname'] .'</option>';
		}
		
		 



		
		$project = get_project($project_id);
		$client_address = get_address($project["order_client_address"]);
		$has_items = false;

		$html = '<table id="available_items">';

		$sql = "select DISTINCT item_id, concat(item_code, ': ', item_name) as iname
				from product_lines 
				INNER JOIN product_line_supplying_groups ON product_line_supplying_group_line_id = product_line_id
				INNER JOIN item_supplying_groups ON item_supplying_group_supplying_group_id = product_line_supplying_group_group_id
				INNER JOIN items ON item_id = item_supplying_group_item_id
				INNER JOIN supplying_groups on supplying_group_id = item_supplying_group_supplying_group_id 
				INNER JOIN productline_regions ON productline_region_productline = product_line_id
				INNER JOIN item_categories ON item_category_id = item_category
				LEFT JOIN item_subcategories on item_subcategory_id = item_subcategory
				INNER JOIN item_countries ON item_country_item_id = item_id 
				LEFT JOIN suppliers on supplier_item = item_id
				LEFT JOIN addresses on address_id = supplier_address 
				where item_category = " . dbquote($item_category_id) .
				" and product_line_id = " . dbquote($project["project_product_line"]) .
				" and product_line_budget = 1
				  and product_line_active = 1
				  and item_active = 1
				  and item_visible_in_projects = 1
				  and item_type in (1,2) 
				  and item_country_country_id = " . dbquote($project["order_shop_address_country"]) .
				" order by item_code, item_name";

			
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			$checkbox = '<input name="item_' . $row['item_id'] . '" id="item_' . $row['item_id'] . '" value="'. $row['item_id']. '"" type="checkbox">';

			$select = '<select id="cost_sub_group_"' . $row['item_id'] . ' name="cost_sub_group_' . $row['item_id']. '">';
			$select .= $cost_sub_groups;
			$select .= '</select>';
			
			$html .= '<tr><td class="list_text_odd">'. $checkbox . '</td><td class="list_text_odd">' . $row['iname'] . '&nbsp;&nbsp;</td><td class="list_text_odd">' . $select. '</td></tr>';

			$has_items = true;
		}


		if($has_items == true) {
			
			


			$button = "<input id=\"submit\" onmouseover=\"this.style.color='#FF0000';this.style.cursor='pointer'\" onmouseout=\"this.style.color='#006699'\" class=\"modalbuttonw\" value=\"Add Selected Items\" style=\"color: rgb(0, 102, 153); cursor: pointer;\" type=\"submit\">";

			$html .= '<tr><td colspan="3" class="list_text_odd">'. $button. '</td></tr>';
		}
		$html .= '</table>';

		echo $html;
	}
	else
	{
		echo 'error';
	}
}

?>