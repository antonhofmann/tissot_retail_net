<?php
/********************************************************************

    get_functions.php

    Various utility functions to get information from tables.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-07
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-19

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

/********************************************************************
    get category name
*********************************************************************/
function get_category_name($plid = 0, $cid = 0, $scid = 0)
{

    $category_name = "";

	$sql = "select product_line_name" . 
		   " from product_lines " .
		   " where product_line_id = " . dbquote($plid);


    $res = mysql_query($sql);
	if($row = mysql_fetch_assoc($res))
	{
	   $category_name = $row['product_line_name'];
    }


	$sql = "select item_category_name " . 
		   " from item_categories " .
		   " where item_category_id = " . dbquote($cid);


    $res = mysql_query($sql);
	if($row = mysql_fetch_assoc($res))
	{
	   $category_name .= " - " . $row['item_category_name'];
    }


	$sql = "select item_subcategory_name " . 
		   " from item_subcategories " .
		   " where item_subcategory_id = " . dbquote($scid);


    $res = mysql_query($sql);
	if($row = mysql_fetch_assoc($res))
	{
	   $category_name .= " - " . $row['item_subcategory_name'];
    }

	

    return $category_name;
}


/********************************************************************
    get currency informations assigned to an address
*********************************************************************/
function get_address_currency($address_id)
{
    $currency = array();

    $sql = "select address_currency from addresses where address_id = " . $address_id;
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $currency_id = $row["address_currency"];
    }

    if ($currency_id > 0)
    {
        $sql = "select * from currencies where currency_id = " . $currency_id;
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $currency["id"] = $currency_id;
            $currency["symbol"] = $row["currency_symbol"];
            $currency["exchange_rate"] = $row["currency_exchange_rate"];
			$currency["exchange_rate_furniture"] = $row["currency_exchange_rate_furniture"];
            $currency["factor"] = $row["currency_factor"];
        }
    }
    else
    {
        $currency["id"] = 0;
        $currency["symbol"] = "";
        $currency["exchange_rate"] = 0;
		$currency["exchange_rate_furniture"] = 0;
        $currency["factor"] = 0;
    }
    
    return $currency;
}


/********************************************************************
    get project standard roles
*********************************************************************/
function get_project_standard_roles($address_id, $pos_type, $legal_type)
{
    $standardroles = array();

	$standardroles['rtco'] = "";
	$standardroles['dsup'] = "";
	$standardroles['rtop'] = "";
	$standardroles['cms_approver'] = "";

    $sql = "select country_id from addresses " .
           "left join countries on country_id = address_country " .
           "where address_id = " . $address_id;

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $country = $row["country_id"];

		$sql = "select * from standardroles " . 
		       "where standardrole_country = " . $country . 
			   " and standardrole_postype = " . $pos_type . 
			   " and standardrole_legaltype = " . $legal_type;
		
		

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$standardroles['rtco'] = $row["standardrole_rtco_user"];
			$standardroles['dsup'] = $row["standardrole_dsup_user"];
			$standardroles['rtop'] = $row["standardrole_rtop_user"];
			$standardroles['cms_approver'] = $row["standardrole_cms_approver_user"];
		}
		else
		{
			$sql = "select * from standardroles " . 
				   "where standardrole_country = " . $country . 
				   " and standardrole_postype = " . $pos_type;

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$standardroles['rtco'] = $row["standardrole_rtco_user"];
				$standardroles['dsup'] = $row["standardrole_dsup_user"];
				$standardroles['rtop'] = $row["standardrole_rtop_user"];
				$standardroles['cms_approver'] = $row["standardrole_cms_approver_user"];
			}
		}
    }

    return $standardroles;
}



/********************************************************************
    get project standard roles
*********************************************************************/
function get_project_standard_roles_popup()
{
    $standardroles = array();

	$standardroles['rtco'] = "";
	$standardroles['dsup'] = "";
	$standardroles['rtop'] = "";
	$standardroles['cms_approver'] = "";

    $sql = "select projectkind_standard_project_manager,  projectkind_standard_logistics_coordinator " .
		   "from projectkinds " .
           "where projectkind_id = 8";

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$standardroles['rtco'] = $row["projectkind_standard_project_manager"];
		$standardroles['dsup'] = "";
		$standardroles['rtop'] = $row["projectkind_standard_logistics_coordinator"];
		$standardroles['cms_approver'] = "";
    }

    return $standardroles;
}


/********************************************************************
    get standard design contractor
*********************************************************************/
function get_standard_design_contractor($productline_id, $pos_type)
{
    $standard_design_contractor = "";

    $sql = "select standarddesigncontractor_dcon_user " . 
		   "from standarddesigncontractor " .
           "where standarddesigncontractor_productline = " . $productline_id . 
		   " and standarddesigncontractor_postype = " . $pos_type;

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $standard_design_contractor = $row["standarddesigncontractor_dcon_user"];
    }
    return $standard_design_contractor;
}


/********************************************************************
    get currency informations assigned to an order
*********************************************************************/
function get_order_currency($order_id = 0)
{
    $currency = array();

    $sql = "select * from orders where order_id = " . dbquote($order_id);
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $currency_id = $row["order_client_currency"];
        $currency["exchange_rate"] = $row["order_client_exchange_rate"];
    }


	if ($currency_id > 0)
    {
        $sql = "select * from currencies where currency_id = " . $currency_id;
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $currency["id"] = $currency_id;
            $currency["symbol"] = $row["currency_symbol"];
            //$currency["exchange_rate"] = $row["currency_exchange_rate"];
            $currency["factor"] = $row["currency_factor"];
        }
    }
    else
    {
        $currency["id"] = 0;
        $currency["symbol"] = "";
        $currency["exchange_rate"] = 0;
        $currency["factor"] = 0;
    }
    
    return $currency;
}


/********************************************************************
    get currency symbol
*********************************************************************/
function get_currency_symbol($currency_id)
{
    if ($currency_id)
    {        $sql = "select * from currencies where currency_id = " . $currency_id;
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $currency_symbol = $row["currency_symbol"];
        }
    }
    else
    {
        $currency_symbol = "n/a";
    }
    return $currency_symbol;
}


/********************************************************************
    get item currency symbol from supplier table
*********************************************************************/
function get_item_currency_symbol($supplier_id, $item_id)
{
    $currency_symbol = "";

    $sql = "select currency_symbol " .
           "from suppliers " .
           "left join currencies on supplier_item_currency = currency_id ".
           "where supplier_address = " . $supplier_id .
           "   and supplier_item = " . $item_id;
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $currency_symbol = $row["currency_symbol"];
    }
    
    
    return $currency_symbol;


}

/********************************************************************
    get system currency informations
*********************************************************************/
function get_system_currency_fields()
{
    $system_currency = array();

    $sql = "select * from currencies where currency_system=1";
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $system_currency["id"] = $row["currency_id"];
        $system_currency["symbol"] = $row["currency_symbol"];
        $system_currency["exchange_rate"] = $row["currency_exchange_rate"];
        $system_currency["factor"] = $row["currency_factor"];
    }
    
    return $system_currency;
}



/********************************************************************
    get currency informations
*********************************************************************/
function get_currency($currency_id)
{
    $currency = array();

    $sql = "select * from currencies where currency_id = " . $currency_id;
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $currency["id"] = $currency_id;
        $currency["symbol"] = $row["currency_symbol"];
        $currency["exchange_rate"] = $row["currency_exchange_rate"];
		$currency["exchange_rate_furniture"] = $row["currency_exchange_rate_furniture"];
        $currency["factor"] = $row["currency_factor"];
    }
    else
    {
        $currency["id"] = 0;
        $currency["symbol"] = "";
        $currency["exchange_rate"] = 0;
		$currency["exchange_rate_furniture"] = 0;
        $currency["factor"] = 0;
    }
    
    return $currency;
}


/********************************************************************
    get user's currency
*********************************************************************/

function get_user_currency ($user_id)
{
    $currency = array();
    
    $sql_currency = "select currency_symbol " .
                    "from currencies " .
                    "left join users on users.user_id = " . user_id() . " " .
                    "left join addresses on addresses.address_id = users.user_address " .
                    "where currencies.currency_id = addresses.address_currency ";
    
    $res = mysql_query($sql_currency) or dberror($sql_currency);
    if ($res)
    {
     $currency = mysql_fetch_assoc($res);
    }
    return $currency;
}


/********************************************************************
    get currency informations
*********************************************************************/
function get_bid_currency($bid_id = 0)
{
    $currency = array();

    $sql = "select * from costsheet_bids
		    left join currencies on currency_id = costsheet_bid_currency 
			where costsheet_bid_id = " . $bid_id;
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $currency["id"] = $row["currency_id"];
        $currency["symbol"] = $row["currency_symbol"];
        $currency["exchange_rate"] = $row["currency_exchange_rate"];
		$currency["exchange_rate_furniture"] = $row["currency_exchange_rate_furniture"];
        $currency["factor"] = $row["currency_factor"];
    }
    else
    {
        $currency["id"] = 0;
        $currency["symbol"] = "";
        $currency["exchange_rate"] = 0;
		$currency["exchange_rate_furniture"] = 0;
        $currency["factor"] = 0;
    }
    
    return $currency;
}

/********************************************************************
    get the field values of an address
*********************************************************************/
function get_address($id)
{
    $address = array();

    if ($id == '')
    {
            $address["id"] = "";
			$address["shortcut"] = "";
            $address["company"] = "";
            $address["company2"] = "";
            $address["address"] = "";
			$address["street"] = "";
			$address["streetnumber"] = "";
            $address["address2"] = "";
            $address["zip"] = "";
            $address["place"] = "";
			$address["place_id"] = "";
			$address["place_province"] = "";
            $address["country"] = "";
            $address["country_name"] = "";
            $address["currency"] = "";
            $address["phone"] = "";
			$address["phone_country"] = "";
			$address["phone_area"] = "";
			$address["phone_number"] = "";
            $address["mobile_phone"] = "";
			$address["mobile_phone_country"] = "";
			$address["mobile_phone_area"] = "";
			$address["mobile_phone_number"] = "";
            $address["email"] = "";
            $address["contact"] = "";
            $address["client_type"] = "";
			$address["contact_name"] = "";
			$address["website"] = "";
			$address["country_region"] = "";
			$address["invoice_recipient"] = "";
			$address["province_name"] = "";
			$address["client_type"] = "";
			$address["parent"] = "";
			$address["production_type"] = "";
    }

    else
    {
        $sql = "select * from addresses left join places on place_id = address_place_id where address_id = " . $id;
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $address["id"] = $row["address_id"];
			$address["shortcut"] = $row["address_shortcut"];
            $address["company"] = $row["address_company"];
            $address["company2"] = $row["address_company2"];
            $address["address"] = $row["address_address"];
			$address["street"] = $row["address_street"];
			$address["streetnumber"] = $row["address_streetnumber"];

            $address["address2"] = $row["address_address2"];
            $address["zip"] = $row["address_zip"];
            $address["place"] = $row["place_name"];
			$address["place_name"] = $row["place_name"];
			$address["place_id"] = $row["address_place_id"];
			$address["place_province"] = $row["place_province"];
            $address["country"] = $row["address_country"];
            $address["country_name"] = "";
            $address["currency"] = $row["address_currency"];
            $address["phone"] = $row["address_phone"];
			$address["phone_country"] = $row["address_phone_country"];
			$address["phone_area"] = $row["address_phone_area"];
			$address["phone_number"] = $row["address_phone_number"];
            $address["mobile_phone"] = $row["address_mobile_phone"];
			$address["mobile_phone_country"] = $row["address_mobile_phone_country"];
			$address["mobile_phone_area"] = $row["address_mobile_phone_area"];
			$address["mobile_phone_number"] = $row["address_mobile_phone_number"];
            $address["email"] = $row["address_email"];
            $address["contact"] = $row["address_contact"];
            $address["client_type"] = $row["address_client_type"];
			$address["contact_name"] = $row["address_contact_name"];
			$address["website"] = $row["address_website"];
			$address["invoice_recipient"] = $row["address_invoice_recipient"];
			$address["client_type"] = $row["address_client_type"];
			$address["parent"] = $row["address_parent"];
			$address["production_type"] = $row["address_standard_workflow"];

            $sql = "select country_id, country_name, country_region ".
                   "from countries ".
                   "where country_id = " . dbquote($address["country"]);



            $res = mysql_query($sql);
            if ($res)
            {
				$row = mysql_fetch_assoc($res);
                $address["country_name"] = $row['country_name'];
				$address["country_region"] = $row['country_region'];
            }


			$sql = "select province_id, province_canton ".
                   "from provinces ".
                   "where province_id = " . dbquote($address["place_province"]);



            $res = mysql_query($sql);
            if ($res)
            {
				$row = mysql_fetch_assoc($res);
                $address["province_name"] = $row['province_canton'];
            }

        }
    }


    return $address;
}

/********************************************************************
    get the region code of an address
*********************************************************************/
function get_address_region($address_id)
{
    $region = 0;

    $sql = "select country_region ".
           "from addresses " .
           "left join countries on address_country = country_id ".
           "where address_id = " . $address_id;

     $res = mysql_query($sql) or dberror($sql);
     if ($row = mysql_fetch_assoc($res))
     {
        $region = $row["country_region"];
     }
     return $region;
}


/********************************************************************
    get the region code of a user's address
*********************************************************************/
function get_user_region($user_id)
{
    $region = 0;

    $sql = "select country_region ".
           "from users " .
           "left join addresses on user_address = address_id ".
           "left join countries on address_country = country_id ".
           "where user_id = " . $user_id;

	 $res = mysql_query($sql) or dberror($sql);
     if ($row = mysql_fetch_assoc($res))
     {
        $region = $row["country_region"];
     }
     return $region;
}

/********************************************************************
    get the field values of a user
*********************************************************************/
function get_user($id)
{
    $user = array();

    if ($id == '' or $id == 0)
    {
		$user["firstname"] = "";
        $user["name"] = "";
        $user["contact"] = "";
        $user["phone"] = "";
		$user["phone_country"] = "";
		$user["phone_area"] = "";
		$user["phone_number"] = "";
        $user["mobile_phone"] = "";
		$user["mobile_phone_country"] = "";
		$user["mobile_phone_area"] = "";
		$user["mobile_phone_number"] = "";
        $user["email"] = "";
        $user["cc"] = "";
        $user["deputy"] = "";
        $user["address"] = "";
		$user["country"] = "";
		$user["has_access_to_travelling_projects"] = false;
		$user["user_can_only_see_projects_of_his_address"] = 0;
		$user["user_can_only_see_orders_of_his_address"] = 0;
		$user["user_project_requester"] = 0;
		
    }
    else
    {
        $sql = "select * from users left join addresses on address_id = user_address where user_id = " . $id;
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $user["firstname"] = $row["user_firstname"];
            $user["name"] = $row["user_name"];
            $user["contact"] = $row["user_name"] . " " . $row["user_firstname"];
            $user["phone"] = $row["user_phone"];
			$user["phone_country"] = $row["user_phone_country"];
			$user["phone_area"] = $row["user_phone_area"];
			$user["phone_number"] = $row["user_phone_number"];

            $user["mobile_phone"] = $row["user_mobile_phone"];
			$user["mobile_phone_country"] = $row["user_mobile_phone_country"];
			$user["mobile_phone_area"] = $row["user_mobile_phone_area"];
			$user["mobile_phone_number"] = $row["user_mobile_phone_number"];

            $user["email"] = $row["user_email"];
            $user["cc"] = $row["user_email_cc"];
            $user["deputy"] = $row["user_email_deputy"];
            $user["address"] = $row["user_address"];
			$user["country"] = $row["address_country"];

			$user["user_can_only_see_projects_of_his_address"] = $row["user_can_only_see_projects_of_his_address"];
			$user["user_can_only_see_orders_of_his_address"] = $row["user_can_only_see_orders_of_his_address"];

			$user["user_project_requester"] = $row["user_project_requester"];


			$sql = "select * from posareatypes " . 
				   "where posareatype_id IN (4, 5) and " . 
				   "posareatype_email1 = " . dbquote($user["email"]) . 
				   " or posareatype_email2 = " . dbquote($user["email"]) .
				   " or posareatype_email3 = " . dbquote($user["email"]) .
				   " or posareatype_email4 = " . dbquote($user["email"]) .
				   " or posareatype_email5 = " . dbquote($user["email"]) .
				   " or posareatype_email6 = " . dbquote($user["email"]);
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$user["has_access_to_travelling_projects"] = true;
			}
			else
			{
				$user["has_access_to_travelling_projects"] = false;
			}

			
			if($user["cc"])
			{
				$sql = "select user_id from users left join addresses on address_id = user_address where user_email = " . dbquote($user["cc"]);
				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					$user["cc_id"] = $row["user_id"];
				}
			}

			if($user["deputy"])
			{
				$sql = "select user_id from users left join addresses on address_id = user_address where user_email = " . dbquote($user["deputy"]);
				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					$user["deputy_id"] = $row["user_id"];
				}
			}
        }
    }
    return $user;
}


/*************************************************************************
   get all the roles of a user
**************************************************************************/
function get_user_roles($user_id)
{
    $user_roles = array();
    
    $sql = "select user_role_role ".
           "from user_roles ".
           "left join roles on user_role_role = role_id ".
           "where user_role_user = " . $user_id;

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $user_roles[] = $row["user_role_role"];
    }

    return $user_roles;
}


/*************************************************************************
   get project_state_restrictions
**************************************************************************/
function get_project_state_restrictions($user_id)
{
    $project_state_restrictions = array();
	$project_state_restrictions['from_state'] = "";
	$project_state_restrictions['to_state'] = "";

    
    $sql = "select role_order_state_visible_from, role_order_state_visible_to ".
           "from user_roles ".
           "left join roles on user_role_role = role_id ".
           "where user_role_user = " . $user_id;

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        if($row["role_order_state_visible_from"] and $row["role_order_state_visible_from"] > $project_state_restrictions['from_state'])
		{
			$project_state_restrictions['from_state'] = $row["role_order_state_visible_from"];
		}

		if($row["role_order_state_visible_to"] and ($project_state_restrictions['to_state'] == '' or $row["role_order_state_visible_to"] < $project_state_restrictions['to_state']))
		{
			$project_state_restrictions['to_state'] = $row["role_order_state_visible_to"];
		}
    }

    return $project_state_restrictions;
}

/********************************************************************
    get all franchisee addresses used by the client up to now
*********************************************************************/
function get_franchisee_addresses($id)
{
    $franchisee_addresses=array();

    $sql = "select order_id, ". 
           "concat(order_franchisee_address_company, ', ', order_franchisee_address_address, ', ', order_franchisee_address_contact) as full_name ".
           "from orders ".
           "where order_franchisee_address_company is not NULL and order_client_address = ". $id . " ".
           "order by order_franchisee_address_company ";


	$res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        $franchisee_addresses[$row["order_id"]]=$row["full_name"];
    }
    
    $franchisee_addresses=array_unique($franchisee_addresses);

    return $franchisee_addresses;
}

/********************************************************************
    get all billing addresses used by the client up to now
*********************************************************************/
function get_billing_addresses($id)
{
    $billing_addresses=array();

    $sql = "select order_id, ". 
           "concat(order_billing_address_company, ', ', order_billing_address_address, ', ', order_billing_address_contact) as full_name ".
           "from orders ".
           "where order_client_address = ". $id . " and order_billing_address_active = 1 ".
           "order by order_billing_address_company ";

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        $billing_addresses[$row["order_id"]]=$row["full_name"];
    }
    
    $billing_addresses=array_unique($billing_addresses);

    return $billing_addresses;
}


/********************************************************************
    get all delivery addresses used by the client up to now
*********************************************************************/
function get_delivery_addresses($id)
{
    
	//set all old delivery addresses to inactive
	$date_limit = (date("Y") -1) . "-". date("m") . "-" . date("d");
	
	$sql = "update order_addresses set order_address_inactive = 1 " . 
		   "where order_address_type=2 and order_address_inactive <> 1 " . 
		   "and order_address_parent = ". $id . " ". 
		   "and date_created < " . dbquote($date_limit);

	$result = mysql_query($sql) or dberror($sql);

	$delivery_addresses=array();

    $sql = "select order_address_id, if(order_address_contact <>'',concat(order_address_place, ': ', order_address_company, ', ', order_address_address, ', ', order_address_contact),concat(order_address_place, ': ', order_address_company, ', ', order_address_address)) as address ".
           "from order_addresses ".
           "where order_address_type=2 and order_address_inactive <> 1 " . 
		   "and order_address_parent = ". $id . " ".
		   " and order_address_place is not null " . 
		   " and order_address_place <> '' " .
           "order by order_address_place, order_address_company ";

	$res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        $delivery_addresses[$row["order_address_id"]]=$row["address"];
    }
    
    $delivery_addresses=array_unique($delivery_addresses);

    return $delivery_addresses;
}


/********************************************************************
    get all warehouse addresses used by the supplier up to now
*********************************************************************/
function get_warehouse_addresses($supplier_id)
{
    $warehouse_addresses=array();
    if ($supplier_id)
    {
        $sql = "select order_address_id, ".
               "concat(order_address_company, ', ', order_address_contact) as full_name ".
               "from order_addresses ".
               "where order_address_type=4 and order_address_inactive <> 1 " . 
			   "and order_address_parent = ". $supplier_id . " ".
               "order by order_address_company ";

        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {
            $warehouse_addresses[$row["order_address_id"]]=$row["full_name"];
        }
    
        $warehouse_addresses=array_unique($warehouse_addresses);
    }
    return $warehouse_addresses;
}

/********************************************************************
    get project and order data 
*********************************************************************/
function get_project($id = 0)
{
    $project = array();

    $sql = "select *, projects.date_created as date_created, " .
		   "IF(TIMESTAMPDIFF(MONTH,project_fagrstart,project_fagrend) >0, TIMESTAMPDIFF(MONTH,project_fagrstart,project_fagrend), 'n.a.') as dmonths " . 
           "from projects ".
           "left join product_lines on project_product_line = product_line_id ".
		   "left join productline_subclasses on productline_subclass_id = project_product_line_subclass ".
           "left join postypes on postype_id = project_postype ".
           "left join orders on project_order = order_id ".
           "left join transportation_types as transportation_types1 on order_preferred_transportation_arranged = transportation_types1. transportation_type_id ".
		   "left join transportation_types as transportation_types2 on order_preferred_transportation_mode = transportation_types2. transportation_type_id ".
		   "left join project_costs on project_cost_order = order_id " .
		   "left join project_costtypes on project_costtype_id = project_cost_type " .
		   "left join project_states on project_state_id = project_state ".
		   "left join projectkinds on projectkind_id = project_projectkind " .
		   "left join possubclasses on possubclass_id = project_pos_subclass " .
		   "left join agreement_types on agreement_type_id = project_fagagreement_type " .
		   "left join project_type_subclasses on project_type_subclasses.project_type_subclass_id = projects.project_type_subclass_id " . 
		   "left join users on user_id = order_user " .
           "where project_id  = " . dbquote($id);

    $res = mysql_query($sql) or dberror($sql);



    if ($row = mysql_fetch_assoc($res))
    {
		$project = $row;
		$project["submitted_by"] = $row["user_name"] . " " . $row["user_firstname"];
    }


	$project["project_manager"] = "";
	$sql = "select concat(user_name, ' ', user_firstname) as username from users where user_id =" . dbquote($project["project_retail_coordinator"]);
	$res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project["project_manager"] = $row["username"];
    }
	
	$project["operator"] = "";
	$project["operator2"] = "";
	$sql = "select user_firstname, user_name, concat(user_name, ' ', user_firstname) as username from users where user_id =" . dbquote($project["order_retail_operator"]);
	$res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project["operator"] = $row["username"];
		$project["operator2"] = substr($row["user_firstname"], 0, 1) . "." . $row["user_name"];
    }

	

	
	$project["order_franchisee_address_country_name"] = "";
	$sql = "select country_name ".
           "from countries ".
           "where country_id  = " . dbquote($project["order_franchisee_address_country"]);

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project["order_franchisee_address_country_name"] = $row["country_name"];
    }

    
	$project["order_shop_address_country_name"] = "";

	$sql = "select country_name ".
           "from countries ".
           "where country_id  = " . dbquote($project["order_shop_address_country"]);

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project["order_shop_address_country_name"] = $row["country_name"];
    }
	else
	{
		$project["order_shop_address_country"] = "0";
	}

    $sql = "select country_name ".
           "from countries ".
           "where country_id  = " . dbquote($project["order_billing_address_country"]);


	$res = mysql_query($sql) or dberror($sql);

	$project["order_billing_address_country_name"] = "";
    if ($row = mysql_fetch_assoc($res))
    {
        $project["order_billing_address_country_name"] = $row["country_name"];
    }
	
	//get pos address from POS Index
	$sql = "select posorder_posaddress from posorders where posorder_order = " . dbquote($project["order_id"]);
	$res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project["posaddress_id"] = $row["posorder_posaddress"];
		$project["pipeline"] = 0;
    }
	else
	{
		$sql = "select posorder_posaddress from posorderspipeline where posorder_order = " . dbquote($project["order_id"]);
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$project["posaddress_id"] = $row["posorder_posaddress"];
			$project["pipeline"] = 1;
		}
		else
		{
			$project["posaddress_id"] = "";
			$project["pipeline"] = 0;
		}
	}


	//postypes CER/AF
	$project["needs_cer"] = 0;
	$project["needs_af"] = 0;
	$project["needs_inr01"] = 0;

	$sql = "select posproject_type_needs_cer, posproject_type_needs_af, 
			posproject_type_needs_inr01_only, posproject_type_needs_inr03 " . 
		   "from posproject_types " . 
	       "where posproject_type_postype = " . dbquote($project["project_postype"]) .  
		   " and posproject_type_projectcosttype = " . dbquote($project["project_cost_type"]) . 
		   " and posproject_type_projectkind = " . dbquote($project["project_projectkind"]);

	$res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $project["needs_cer"] = $row["posproject_type_needs_cer"];
		$project["needs_af"] = $row["posproject_type_needs_af"];
		$project["needs_inr01"] = $row["posproject_type_needs_inr01_only"];
		$project["needs_inr03"] = $row["posproject_type_needs_inr03"];
    }
    return $project;
}


/********************************************************************
    get order data 
*********************************************************************/
function get_order($id)
{
    $order = array();

    $sql = "select * ".
           "from orders ".
           "left join transportation_types as transportation_types1 on order_preferred_transportation_arranged = transportation_types1. transportation_type_id ".
		   "left join transportation_types as transportation_types2 on order_preferred_transportation_mode = transportation_types2. transportation_type_id ".
		   "left join addresses on address_id =order_client_address " .
		   "left join users on user_id = order_user " . 
           "where order_id  = " . $id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $order = $row;

		$order["submitted_by"] = $row["user_name"] . " " . $row["user_firstname"];
    }

	$order["order_shop_address_country_name"] = "";
    $sql = "select country_name ".
           "from countries ".
           "where country_id  = " . dbquote($order["order_shop_address_country"]);

	$res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $order["order_shop_address_country_name"] = $row["country_name"];
    }

	if($order["order_shop_address_country_name"] == "")
	{
		$sql = "select country_name ".
			   "from countries ".
			   "where country_id  = " . dbquote($order["address_country"]);

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$order["order_shop_address_country_name"] = $row["country_name"];
		}
	}

    
	$order["order_billing_address_country_name"] = "";
	$sql = "select country_name ".
           "from countries ".
           "where country_id  = " . dbquote($order["order_billing_address_country"]);

    $res = mysql_query($sql) or dberror($sql);

    
	if ($row = mysql_fetch_assoc($res))
    {
        $order["order_billing_address_country_name"] = $row["country_name"];
    }

	$sql = "select posorder_posaddress from posorders where posorder_order = " . dbquote($id);
	$res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $order["posaddress_id"] = $row["posorder_posaddress"];
    }
	else
	{
		$order["posaddress_id"] = "";
	}
	
    return $order;
}


/********************************************************************
    get order item data
*********************************************************************/
function get_order_item($id)
{
    $order_item = array();


    $sql = "select *, ".
           "    if(order_item_item <>'', item_code, item_type_name) as item_shortcut, ".
           "    if(order_item_item <>'', 0, 1) as item_price_changable ".
           "from order_items ".
           "left join items on order_item_item = item_id ".
           "left join item_types on order_item_type = item_type_id ".
           "where order_item_id = " . $id; 

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $order_item["item"] = $row["order_item_item"];
        $order_item["code"] = $row["item_shortcut"];
		$order_item["category"] = $row["order_item_category"];
        $order_item["price_changable"] = $row["item_price_changable"];
        $order_item["text"] = $row["order_item_text"];
        $order_item["type"] = $row["order_item_type"];
        $order_item["quantity"] = $row["order_item_quantity"];
        $order_item["po_number"] = $row["order_item_po_number"];
        $order_item["client_price"] = $row["order_item_client_price"];
        $order_item["supplier"] = $row["order_item_supplier_address"];
        $order_item["supplier_price"] = $row["order_item_supplier_price"];
        $order_item["supplier_currency"] = $row["order_item_supplier_currency"];
        $order_item["supplier_exchange_rate"] = $row["order_item_supplier_exchange_rate"];
        $order_item["forwarder"] = $row["order_item_forwarder_address"];
        $order_item["system_price"] = $row["order_item_system_price"];
		$order_item["system_price_freezed"] = $row["order_item_system_price_freezed"];
        $order_item["transportation"] = $row["order_item_transportation"];
        $order_item["no_offer_required"] = $row["order_item_no_offer_required"];
		$order_item["only_quantity_proposal"] = $row["order_item_only_quantity_proposal"];
        $order_item["offer_number"] = $row["order_item_offer_number"];
        $order_item["production_time"] = $row["order_item_production_time"];
        $order_item["supplier_item_code"] = $row["order_item_supplier_item_code"];
        $order_item["shipment_code"] = $row["order_item_shipment_code"];
		$order_item["order_item_ordered"] = $row["order_item_ordered"];
		$order_item["order_revisions"] = $row["order_item_order_revisions"];
		$order_item["preferred_arrival_date"] = $row["order_item_preferred_arrival_date"];
		
		$order_item["ready_for_pickup"] = $row["order_item_ready_for_pickup"];

		$order_item["order_item_supplier_address"] = $row["order_item_supplier_address"];
		$order_item["order_item_forwarder_address"] = $row["order_item_forwarder_address"];


		$order_item["order_item_unit_id"] = $row["order_item_unit_id"];
		$order_item["order_item_packaging_type_id"] = $row["order_item_packaging_type_id"];

		$order_item["order_item_stackable"] = $row["order_item_stackable"];

		$order_item["order_item_localcostgroupcode"] = $row["order_item_localcostgroupcode"];


		$order_item["order_item_in_freezed_budget"] = $row["order_item_in_freezed_budget"];
		$order_item["order_item_quantity_freezed"] = $row["order_item_quantity_freezed"];
		$order_item["order_item_system_price_freezed"] = $row["order_item_system_price_freezed"];

		

		// get currency information
        $order_item["supplier_system_price"] = Null;
        $order_item["supplier_system_price"] = NULL;
        
        if($order_item["supplier_currency"])
        {
            $currency = get_currency($order_item["supplier_currency"]);
            $order_item["supplier_system_price"] = number_format($order_item["supplier_price"] * $order_item["supplier_exchange_rate"] / $currency["factor"],2, ".", "'");

        }
        else
        {
            if($order_item["supplier"])
            {
				$currency = get_address_currency($order_item["supplier"]);
                $order_item["supplier_system_price"] = number_format($order_item["supplier_price"] * $order_item["supplier_exchange_rate"] / $currency["factor"],2, ".", "'");

            }
        }

    }

    return $order_item;
}

/********************************************************************
    get project design objective items ids data 
*********************************************************************/
function get_project_design_objective_item_ids($id)
{
    $project_design_objective_item_ids = array();
    
    $sql = "select project_item_item, design_objective_group_id, design_objective_group_name ".
           "from project_items ".
           "left join design_objective_items on project_item_item = design_objective_item_id ".
           "left join design_objective_groups on design_objective_item_group = design_objective_group_id ".
           "where project_item_project  = " . $id ." ".
           "order by design_objective_group_priority, design_objective_item_priority";

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
         $project_design_objective_item_ids[$row["project_item_item"]] = $row["design_objective_group_name"];
    }

    return  $project_design_objective_item_ids;
}

/********************************************************************
    get the address of an order specified by an address_type
*********************************************************************/
function get_order_address($address_type, $order_id)
{

	$order_address = array();

	$order_address["id"] = "";
	$order_address["company"] = "";
	$order_address["company2"] = "";
	$order_address["address"] = "";
	$order_address["street"] = "";
	$order_address["streetnumber"] = "";
	$order_address["address2"] = "";
	$order_address["zip"] = "";
	$order_address["place"] = "";
	$order_address["place_id"] = "";
	$order_address["country"] = "";
	$order_address["country_name"] = "";
	$order_address["phone"] = "";
	$order_address["phone_country"] = "";
	$order_address["phone_area"] = "";
	$order_address["phone_number"] = "";

	$order_address["mobile_phone"] = "";
	$order_address["mobile_phone_country"] = "";
	$order_address["mobile_phone_area"] = "";
	$order_address["mobile_phone_number"] = "";

	$order_address["email"] = "";
	$order_address["contact"] = "";
    $order_address["country_name"] = "";

	
	
	$sql = "select * ".
           "from order_addresses ".
           "where order_address_order  = " . $order_id . " and order_address_type = " . $address_type;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $order_address["id"] = $row["order_address_id"];
        $order_address["company"] = $row["order_address_company"];
        $order_address["company2"] = $row["order_address_company2"];
        $order_address["address"] = $row["order_address_address"];
		$order_address["street"] = $row["order_address_street"];
		$order_address["streetnumber"] = $row["order_address_street_number"];
        
		$order_address["address2"] = $row["order_address_address2"];
        $order_address["zip"] = $row["order_address_zip"];
        $order_address["place"] = $row["order_address_place"];
		$order_address["place_id"] = $row["order_address_place_id"];
        $order_address["country"] = $row["order_address_country"];
        $order_address["country_name"] = "";
        $order_address["phone"] = $row["order_address_phone"];
		$order_address["phone_country"] = $row["order_address_phone_country"];
		$order_address["phone_area"] = $row["order_address_phone_area"];
		$order_address["phone_number"] = $row["order_address_phone_number"];

        $order_address["mobile_phone"] = $row["order_address_mobile_phone"];
		$order_address["mobile_phone_country"] = $row["order_address_mobile_phone_country"];
		$order_address["mobile_phone_area"] = $row["order_address_mobile_phone_area"];
		$order_address["mobile_phone_number"] = $row["order_address_mobile_phone_number"];

        $order_address["email"] = $row["order_address_email"];
        $order_address["contact"] = $row["order_address_contact"];


        $sql = "select country_id, country_name ".
               "from countries ".
               "where country_id = " . dbquote($order_address["country"]);

        $res = mysql_query($sql);
        if ($res)
        {
           $row = mysql_fetch_assoc($res);
           $order_address["country_name"] = $row['country_name'];
        }
    }  

    return $order_address;
}


/********************************************************************
    get the address of an order_item from table order_addresses
*********************************************************************/
function get_order_item_address($address_type, $order_id, $order_item_id)
{
    $order_address = array();

    $sql = "select * ".
           "from order_addresses ".
		   "left join places on place_id = order_address_place_id " . 
           "where order_address_order  = " . $order_id . 
           "    and order_address_order_item = " . $order_item_id .
           "    and order_address_type = " . $address_type;

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        $order_address["id"] = $row["order_address_id"];
        $order_address["company"] = $row["order_address_company"];
        $order_address["company2"] = $row["order_address_company2"];
        $order_address["address"] = $row["order_address_address"];
		$order_address["street"] = $row["order_address_street"];
		$order_address["streetnumber"] = $row["order_address_street_number"];
        $order_address["address2"] = $row["order_address_address2"];
        $order_address["zip"] = $row["order_address_zip"];
        $order_address["place"] = $row["place_name"];
		$order_address["place_id"] = $row["order_address_place_id"];
		$order_address["province_id"] = $row["place_province"];
        $order_address["country"] = $row["order_address_country"];
        $order_address["phone"] = $row["order_address_phone"];
		$order_address["phone_country"] = $row["order_address_phone_country"];
		$order_address["phone_area"] = $row["order_address_phone_area"];
		$order_address["phone_number"] = $row["order_address_phone_number"];

        $order_address["mobile_phone"] = $row["order_address_mobile_phone"];

		$order_address["mobile_phone_country"] = $row["order_address_mobile_phone_country"];
		$order_address["mobile_phone_area"] = $row["order_address_mobile_phone_area"];
		$order_address["mobile_phone_number"] = $row["order_address_mobile_phone_number"];


        $order_address["email"] = $row["order_address_email"];
        $order_address["contact"] = $row["order_address_contact"];
    }   

    return $order_address;
}



/********************************************************************
    get the supplier addresses of the order
*********************************************************************/
function get_order_item_supplier_addresses($order_id)
{
    $supplier_addresses = array();

    $sql = "select DISTINCT address_id, address_shortcut, address_company, address_place ".
           "from order_items " . 
		   "left join addresses on address_id = order_item_supplier_address ".
           "where order_item_supplier_address > 0 and order_item_order  = " . $order_id;

	$res = mysql_query($sql) or dberror($sql);

    while($row = mysql_fetch_assoc($res))
    {
        $address = array();
        $address["id"] = $row["address_id"];
        $address["shortcut"] = $row["address_shortcut"];
        $address["company"] = $row["address_company"];
        $address["place"] = $row["address_place"];

		$supplier_addresses[] = $address;
    }   

    return $supplier_addresses;
}


/********************************************************************
    get the forwarder addresses of the order
*********************************************************************/
function get_order_item_supplier_addresses_of_this_forwarder($order_id, $forwarder_address_id)
{
    $supplier_addresses = array();

    $sql = "select DISTINCT address_id, address_shortcut, address_company, address_place ".
           "from order_items " . 
		   "left join addresses on address_id = order_item_supplier_address ".
           "where order_item_supplier_address > 0 " .
           " and order_item_forwarder_address  = " . $forwarder_address_id . 
           " and order_item_order  = " . $order_id;

	$res = mysql_query($sql) or dberror($sql);

    while($row = mysql_fetch_assoc($res))
    {
        $address = array();
        $address["id"] = $row["address_id"];
        $address["shortcut"] = $row["address_shortcut"];
        $address["company"] = $row["address_company"];
        $address["place"] = $row["address_place"];

		$supplier_addresses[] = $address;
    }   

    return $supplier_addresses;
}

/*************************************************************************
    get the grand totals of a list of order_items of a certain item type
**************************************************************************/
function get_order_item_type_total($order_id, $item_type, $item_cost_group = 0)
{
    $totals = array();
    $totals["in_system_currency"] = 0;
    $totals["in_order_currency"] = 0;

	if($item_type == ITEM_TYPE_STANDARD)
	{
		$filter = " and (order_item_type=" . ITEM_TYPE_STANDARD . " or order_item_type = " . ITEM_TYPE_SERVICES . ")";
	}
	else
	{
		$filter = " and order_item_type=" . $item_type;
	}

	if($item_cost_group > 0)
	{
		$filter .= " and order_item_cost_group = " . dbquote($item_cost_group);
	}


	 $sql = "select order_item_order, order_item_type, order_item_not_in_budget, ".
           "    order_item_system_price,order_item_quantity, ".
           "    order_item_client_price ".
           "from order_items ".
           "where order_item_order=" . $order_id . 
           "   and (order_item_not_in_budget = 0 or order_item_not_in_budget is null)  ".
           $filter;


	$res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        if($row["order_item_quantity"] > 0 or $row["order_item_type"] == 1 or $row["order_item_type"] == 2)
		{
			$totals["in_system_currency"] = $totals["in_system_currency"] + ($row["order_item_quantity"]*$row["order_item_system_price"]);
			$totals["in_order_currency"] = $totals["in_order_currency"] + ($row["order_item_quantity"]*$row["order_item_client_price"]);
		}
		else
		{
			$totals["in_system_currency"] = $totals["in_system_currency"] + $row["order_item_system_price"];
			$totals["in_order_currency"] = $totals["in_order_currency"] + $row["order_item_client_price"];
		}
    }


    return $totals;
}


/****************************************************************************************
    get the grand totals of a list of order_items of a certain item type budget freezed
*****************************************************************************************/
function get_order_item_type_total_freezed($order_id, $item_type)
{
    
	$totals = array();
    $totals["in_system_currency"] = 0;
    $totals["in_order_currency"] = 0;

	if($item_type == ITEM_TYPE_STANDARD)
	{
		$filter = " (order_item_type=" . ITEM_TYPE_STANDARD . " or order_item_type = " . ITEM_TYPE_SERVICES . ")";
	}
	else
	{
		$filter = " order_item_type=" . $item_type;
	}


	 $sql = "select order_item_order, order_item_type, order_item_not_in_budget, ".
           "    order_item_system_price_freezed,order_item_quantity_freezed, ".
           "    order_item_client_price_freezed ".
           "from order_items ".
           "where order_item_order=" . $order_id . 
           "   and (order_item_not_in_budget = 0 or order_item_not_in_budget is null)  ".
           "having " . $filter;



	$res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        if($row["order_item_quantity_freezed"] > 0 or $row["order_item_type"] == 1 or $row["order_item_type"] == 2)
		{
			$totals["in_system_currency"] = $totals["in_system_currency"] + ($row["order_item_quantity_freezed"]*$row["order_item_system_price_freezed"]);
			$totals["in_order_currency"] = $totals["in_order_currency"] + ($row["order_item_quantity_freezed"]*$row["order_item_client_price_freezed"]);
		}
		else
		{
			$totals["in_system_currency"] = $totals["in_system_currency"] + $row["order_item_system_price_freezed"];
			$totals["in_order_currency"] = $totals["in_order_currency"] + $row["order_item_client_price_freezed"];
		}
    }
    return $totals;
	
}



/*************************************************************************
   build group totals for catalog items
**************************************************************************/
function get_group_total_of_standard_items($order_id, $field)
{
    $group_totals = array();
    
    $sql = "select item_category_name, ".
           "sum(order_item_quantity * " . $field . ") AS group_total ".
           "from order_items LEFT JOIN item_categories ON order_item_category = item_category_id ".
           "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) " . 
		   "and order_item_order=" . $order_id . 
		   " and (order_item_type = " . ITEM_TYPE_STANDARD . " or order_item_type = " . ITEM_TYPE_SERVICES . ") ".
           "group by item_category_name " . 
		   "order by item_category_name";

	$res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $group_totals[$row["item_category_name"]] = $row["group_total"];

    }
    return $group_totals;
}


/*************************************************************************
   build group totals for catalog items, budget freezed
**************************************************************************/
function get_group_total_of_standard_items_freezed($order_id, $field)
{
    $group_totals = array();
    
    $sql = "select item_category_name, ".
           "sum(order_item_quantity_freezed * " . $field . ") AS group_total ".
           "from order_items LEFT JOIN item_categories ON order_item_category = item_category_id ".
           "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) and order_item_order=" . $order_id . 
		   " AND (order_item_type= " . ITEM_TYPE_STANDARD . " or order_item_type= " . ITEM_TYPE_SERVICES . ") " .
           "group by item_category_id ";

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $group_totals[$row["item_category_name"]] = $row["group_total"];
    }
    return $group_totals;
}


/*************************************************************************
   build supplier's group totals
**************************************************************************/
function get_group_total_of_suppliers($order_id, $field, $currency_id = 0)
{
    $group_totals = array();
    $sql = "select address_company, currency_symbol, ".
           "sum(order_item_quantity * " . $field . ") AS group_total ".
           "from order_items ".
           "left join addresses on order_item_supplier_address = address_id ".
           "left join currencies on currency_id = order_item_supplier_currency ".
           "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
           "      and order_item_order = " . $order_id . " ".
           "group by address_company, order_item_supplier_currency ".
           "order by address_company";

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $key = $row["address_company"] . ', ' . $row["currency_symbol"];
        $group_totals["currency"][$key] = $row["currency_symbol"];
        $group_totals["total"][$key] = number_format($row["group_total"], 2, ".", "'");
    }
    return $group_totals;
}



/*************************************************************************
   build line numbers for budget (position numbers)
**************************************************************************/
function get_budet_line_numbers($order_id)
{
    $line_numbers = array();

    // build budget position numbers for standard items
    $sql_1 = "select order_item_id, order_item_text, order_item_quantity, ".
             "    order_item_po_number, item_id, order_item_system_price, ".
             "    (order_item_quantity * order_item_system_price) as total_price, ".
             "    order_item_client_price, ".
             "    item_code, ".
             "    address_shortcut, item_type_id, item_type_name, ".
             "    item_type_priority, order_item_type ".
             "from order_items ".
             "left join items on order_item_item = item_id ".
             "left join item_categories on order_item_category = item_category_id ".
             "left join addresses on order_item_supplier_address = address_id ".
             "left join item_types on order_item_type = item_type_id ";
    $sql_2 = "where (order_item_type = " . ITEM_TYPE_STANDARD . " or order_item_type = " . ITEM_TYPE_SERVICES . ") ";
    $sql_3 = "and order_item_order = " . $order_id . " ".
             "and (order_item_not_in_budget = 0 or order_item_not_in_budget is null)  " .
             "order by item_category_name, item_code";

    $sql_order_items = $sql_1 . $sql_2 . $sql_3;

    $line_numbers_tmp = array();
    $i = 1;

    $res = mysql_query($sql_order_items) or dberror($sql_order_items);
    while ($row = mysql_fetch_assoc($res))
    {
        if ($i < 10)
        {
            $p = "00" . $i;
        }
        else if ($i < 100)
        {
            $p = "0" . $i;
        }
        else
        {
            $p = $i;
        }
        $line_numbers_tmp[$row["order_item_id"]] = $p;
        $i++;
    }

    $line_numbers[ITEM_TYPE_STANDARD] = $line_numbers_tmp;

    // build budget position numbers for special items
    $sql_2 = "where order_item_type = " . ITEM_TYPE_SPECIAL . " ";
    $sql_3 = "and order_item_order = " . $order_id . " ".
             "and (order_item_not_in_budget = 0 or order_item_not_in_budget is null)  " .
             "order by item_code";


    $sql_order_items = $sql_1 . $sql_2 . $sql_3;

    $line_numbers_tmp = array();

    $res = mysql_query($sql_order_items) or dberror($sql_order_items);
    while ($row = mysql_fetch_assoc($res))
    {
        if ($i < 10)
        {
            $p = "00" . $i;
        }
        else if ($i < 100)
        {
            $p = "0" . $i;
        }
        else
        {
            $p = $i;
        }
        $line_numbers_tmp[$row["order_item_id"]] = $p;
        $i++;
    }

    $line_numbers[ITEM_TYPE_SPECIAL] = $line_numbers_tmp;

    // build budget position numbers for local construction cost  positions
    $sql_2 = "where order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST . " ";
    $sql_3 = "and order_item_order = " . $order_id;
 
    $sql_order_items = $sql_1 . $sql_2 . $sql_3;

    $line_numbers_tmp = array();

    $res = mysql_query($sql_order_items) or dberror($sql_order_items);
    while ($row = mysql_fetch_assoc($res))
    {
        if ($i < 10)
        {
            $p = "00" . $i;
        }
        else if ($i < 100)
        {
            $p = "0" . $i;
        }
        else
        {
            $p = $i;
        }
        $line_numbers_tmp[$row["order_item_id"]] = $p;
        $i++;
    }

    
    $line_numbers[ITEM_TYPE_LOCALCONSTRUCTIONCOST] = $line_numbers_tmp;

    // build budget position numbers for exclusions
    $sql_2 = "where order_item_type = " . ITEM_TYPE_COST_ESTIMATION . " ";
    $sql_3 = "and order_item_order = " . $order_id;

    $sql_order_items = $sql_1 . $sql_2 . $sql_3;

    $line_numbers_tmp = array();

    $res = mysql_query($sql_order_items) or dberror($sql_order_items);
    while ($row = mysql_fetch_assoc($res))
    {
        if ($i < 10)
        {
            $p = "00" . $i;
        }
        else if ($i < 100)
        {
            $p = "0" . $i;
        }
        else
        {
            $p = $i;
        }
        $line_numbers_tmp[$row["order_item_id"]] = $p;
        $i++;
    }

    $line_numbers[ITEM_TYPE_COST_ESTIMATION] = $line_numbers_tmp;

    // build budget position numbers for exclusions
    $sql_2 = "where order_item_type = " . ITEM_TYPE_EXCLUSION . " ";
    $sql_3 = "and order_item_order = " . $order_id;

    $sql_order_items = $sql_1 . $sql_2 . $sql_3;

    $line_numbers_tmp = array();

    $res = mysql_query($sql_order_items) or dberror($sql_order_items);
    while ($row = mysql_fetch_assoc($res))
    {
        if ($i < 10)
        {
            $p = "00" . $i;
        }
        else if ($i < 100)
        {
            $p = "0" . $i;
        }
        else
        {
            $p = $i;
        }
        $line_numbers_tmp[$row["order_item_id"]] = $p;
        $i++;
    }

    $line_numbers[ITEM_TYPE_EXCLUSION] = $line_numbers_tmp;

    // build budget position numbers for notifications
    $sql_2 = "where order_item_type = " . ITEM_TYPE_NOTIFICATION . " ";
    $sql_3 = "and order_item_order = " . $order_id;

    $sql_order_items = $sql_1 . $sql_2 . $sql_3;

    $line_numbers_tmp = array();

    $res = mysql_query($sql_order_items) or dberror($sql_order_items);
    while ($row = mysql_fetch_assoc($res))
    {
        if ($i < 10)
        {
            $p = "00" . $i;
        }
        else if ($i < 100)
        {
            $p = "0" . $i;
        }
        else
        {
            $p = $i;
        }
        $line_numbers_tmp[$row["order_item_id"]] = $p;
        $i++;
    }

    $line_numbers[ITEM_TYPE_NOTIFICATION] = $line_numbers_tmp;

    return $line_numbers;
}


/*************************************************************************
   build array with dates from table dates
**************************************************************************/
function get_order_dates_from_dates($sql_order_items)
{

    $dates = array();
	$dates_pref = array();
    $dates_expr = array();
    $dates_pick = array();
    $dates_exar = array();
    $dates_acar = array();
    $dates_ordr = array();
	$dates_delivery_request  = array();


    $res = mysql_query($sql_order_items) or dberror($sql_order_items);
    while ($row = mysql_fetch_assoc($res))
    {
        
		//prefered arrival date
		$dates_pref[$row["order_item_id"]] =  to_system_date($row["order_item_preferred_arrival_date"]);
		
		// expected ready fro pickup date
        $number_tag = "";
        $number_of_records = 0;
        $dates_expr[$row["order_item_id"]] =  "";

        $sql = "select count(date_id) as number_of_records ".
               "from dates ".
               "left join date_types on date_type = date_type_id ".
               "where date_order_item = " . $row["order_item_id"] .
               "    and date_type_code = 'EXRP' ";

        $res1 = mysql_query($sql) or dberror($sql);

        $limit = "";        
        if ($row1 = mysql_fetch_assoc($res1))
        {
            $number_of_records = $row1["number_of_records"];
        }


        if ($number_of_records > 0)
        {
            $number_tag = "[" . $number_of_records . "]";
        }

        $limit = "";
        if ($number_of_records > 1)
        {
            $start = $number_of_records -1;
            $limit = "limit " . $start .", 1";
        }
        
        $sql = "select date_date ".
               "from dates ".
               "left join date_types on date_type = date_type_id ".
               "where date_order_item = " . $row["order_item_id"] .
               "    and date_type_code = 'EXRP' " .
               "    order by dates.date_modified ".
               $limit;


        $res1 = mysql_query($sql) or dberror($sql);
        if ($row1 = mysql_fetch_assoc($res1))
        {
            $dates_expr[$row["order_item_id"]] = $number_tag . to_system_date($row1["date_date"]);
        }
        else
        {
            $dates_expr[$row["order_item_id"]] = "n/a";
        }


        // pick up date
        $number_tag = "";
        $number_of_records = 0;
        $dates_pick[$row["order_item_id"]] =  "";

        $sql = "select count(date_id) as number_of_records ".
               "from dates ".
               "left join date_types on date_type = date_type_id ".
               "where date_order_item = " . $row["order_item_id"] .
               "    and date_type_code = 'PICK' ";

        $res1 = mysql_query($sql) or dberror($sql);

        if ($row1 = mysql_fetch_assoc($res1))
        {
            $number_of_records = $row1["number_of_records"];
        }

        if ($number_of_records > 0)
        {
            $number_tag = "[" . $number_of_records . "]";
        }

        
        $limit = "";
        if ($number_of_records > 1)
        {
            $start = $number_of_records -1;
            $limit = "limit " . $start .", 1";
        }

        $sql = "select date_date ".
               "from dates ".
               "left join date_types on date_type = date_type_id ".
               "where date_order_item = " . $row["order_item_id"] .
               "    and date_type_code = 'PICK' ".
               "    order by dates.date_modified ".
               $limit;
        
        $res1 = mysql_query($sql) or dberror($sql);
        if ($row1 = mysql_fetch_assoc($res1))
        {
            $dates_pick[$row["order_item_id"]] =  $number_tag . to_system_date($row1["date_date"]);
        }
        else
        {
            $dates_pick[$row["order_item_id"]] = "n/a";
        }

        // expected arrival date
        $number_tag = "";
        $number_of_records = 0;
        $dates_exar[$row["order_item_id"]] =  "";

        $sql = "select count(date_id) as number_of_records ".
               "from dates ".
               "left join date_types on date_type = date_type_id ".
               "where date_order_item = " . $row["order_item_id"] .
               "    and date_type_code = 'EXAR' ";

        $res1 = mysql_query($sql) or dberror($sql);

        if ($row1 = mysql_fetch_assoc($res1))
        {
            $number_of_records = $row1["number_of_records"];
        }

        if ($number_of_records > 0)
        {
            $number_tag = "[" . $number_of_records . "]";
        }

        
        $limit = "";
        if ($number_of_records > 1)
        {
            $start = $number_of_records -1;
            $limit = "limit " . $start .", 1";
        }

        $sql = "select date_date ".
               "from dates ".
               "left join date_types on date_type = date_type_id ".
               "where date_order_item = " . $row["order_item_id"] .
               "    and date_type_code = 'EXAR' ".
               "    order by dates.date_modified ".
               $limit;
        
        $res1 = mysql_query($sql) or dberror($sql);
        if ($row1 = mysql_fetch_assoc($res1))
        {
            $dates_exar[$row["order_item_id"]] =  $number_tag . to_system_date($row1["date_date"]);
        }
        else
        {
            $dates_exar[$row["order_item_id"]] = "n/a";
        }


        // actual arrival date
        $number_tag = "";
        $number_of_records = 0;
        $dates_acar[$row["order_item_id"]] =  "";

        $sql = "select count(date_id) as number_of_records ".
               "from dates ".
               "left join date_types on date_type = date_type_id ".
               "where date_order_item = " . $row["order_item_id"] .
               "    and date_type_code = 'ACAR' ";

        $res1 = mysql_query($sql) or dberror($sql);

        if ($row1 = mysql_fetch_assoc($res1))
        {
            $number_of_records = $row1["number_of_records"];
        }

        if ($number_of_records > 0)
        {
            $number_tag = "[" . $number_of_records . "]";
        }
        
        $limit = "";
        if ($number_of_records > 1)
        {
            $start = $number_of_records -1;
            $limit = "limit " . $start .", 1";
        }

        $sql = "select date_date ".
               "from dates ".
               "left join date_types on date_type = date_type_id ".
               "where date_order_item = " . $row["order_item_id"] .
               "    and date_type_code = 'ACAR' ".
               "    order by dates.date_modified ".
               $limit;

        $res1 = mysql_query($sql) or dberror($sql);
        if ($row1 = mysql_fetch_assoc($res1))
        {
            $dates_acar[$row["order_item_id"]] =  $number_tag . to_system_date($row1["date_date"]);
        }
        else
        {
            $dates_acar[$row["order_item_id"]] = "n/a";
        }

        // order date (order to supplier)
        $number_tag = "";
        $number_of_records = 0;
        $dates_ordr[$row["order_item_id"]] =  "";

        $sql = "select count(date_id) as number_of_records ".
               "from dates ".
               "left join date_types on date_type = date_type_id ".
               "where date_order_item = " . $row["order_item_id"] .
               "    and date_type_code = 'ORSU' ";

        $res1 = mysql_query($sql) or dberror($sql);

        if ($row1 = mysql_fetch_assoc($res1))
        {
            $number_of_records = $row1["number_of_records"];
        }

        if ($number_of_records > 0)
        {
            $number_tag = "[" . $number_of_records . "]";
        }
        
        $limit = "";
        if ($number_of_records > 1)
        {
            $start = $number_of_records -1;
            $limit = "limit " . $start .", 1";
        }

        $sql = "select date_date ".
               "from dates ".
               "left join date_types on date_type = date_type_id ".
               "where date_order_item = " . $row["order_item_id"] .
               "    and date_type_code = 'ORSU' ".
               "    order by dates.date_modified ".
               $limit;

        $res1 = mysql_query($sql) or dberror($sql);
        if ($row1 = mysql_fetch_assoc($res1))
        {
            $dates_ordr[$row["order_item_id"]] =  $number_tag . to_system_date($row1["date_date"]);
        }
        else
        {
            $dates_ordr[$row["order_item_id"]] = "n/a";
        }


		//order date (delivery request to forwarder)
        $number_tag = "";
        $number_of_records = 0;
        $dates_delivery_request[$row["order_item_id"]] =  "";

        $sql = "select count(date_id) as number_of_records ".
               "from dates ".
               "left join date_types on date_type = date_type_id ".
               "where date_order_item = " . $row["order_item_id"] .
               "    and date_type_code = 'ORFW' ";

        $res1 = mysql_query($sql) or dberror($sql);

        if ($row1 = mysql_fetch_assoc($res1))
        {
            $number_of_records = $row1["number_of_records"];
        }

        if ($number_of_records > 0)
        {
            $number_tag = "[" . $number_of_records . "]";
        }
        
        $limit = "";
        if ($number_of_records > 1)
        {
            $start = $number_of_records -1;
            $limit = "limit " . $start .", 1";
        }

        $sql = "select date_date ".
               "from dates ".
               "left join date_types on date_type = date_type_id ".
               "where date_order_item = " . $row["order_item_id"] .
               "    and date_type_code = 'ORFW' ".
               "    order by dates.date_modified ".
               $limit;

        $res1 = mysql_query($sql) or dberror($sql);
        if ($row1 = mysql_fetch_assoc($res1))
        {
            $dates_delivery_request[$row["order_item_id"]] =  $number_tag . to_system_date($row1["date_date"]);
        }
        else
        {
            $dates_delivery_request[$row["order_item_id"]] = "n/a";
        }

    }
	$dates["PREF"] = $dates_pref;
    $dates["EXRP"] = $dates_expr;
    $dates["PICK"] = $dates_pick;
    $dates["EXAR"] = $dates_exar;
    $dates["ACAR"] = $dates_acar;
    $dates["ORDR"] = $dates_ordr;
	$dates["ORFW"] =$dates_delivery_request;

    return $dates;

}

/*************************************************************************
   get the last date entry of a series of a specific date type from dates
**************************************************************************/
function get_last_order_item_date($order_item_id, $type)
{
    $date_entry = array();    

    $number_of_records = 0;

    $sql = "select count(date_id) as number_of_records ".
           "from dates ".
           "left join date_types on date_type = date_type_id ".
           "where date_order_item = " . $order_item_id .
           "    and date_type_code = '" . $type ."' ";

    $res1 = mysql_query($sql) or dberror($sql);

    if ($row1 = mysql_fetch_assoc($res1))
    {
        $number_of_records = $row1["number_of_records"];
    }

    if ($number_of_records > 0)
    {
        $date_entry["changes"] = $number_of_records - 1;
    }
    else
    {
        $date_entry["changes"] = "0";
    }
        
    $limit = "";
    if ($number_of_records > 1)
    {
        $start = $number_of_records -1;
        $limit = "limit " . $start .", 1";
    }
    
    $sql_order_item_dates = "select date_date, date_type_name ".
                            "from dates ".
                            "left join date_types on date_type = date_type_id ".
                            "where date_order_item=" .$order_item_id . 
                            "    and date_type_code= '". $type . "' ".
                            "    order by dates.date_modified ".
                            $limit;


    $res = mysql_query($sql_order_item_dates) or dberror($sql_order_item_dates);
    if ($row = mysql_fetch_assoc($res))
    {
        $date_entry["last_date"] = to_system_date($row["date_date"]);
    }
    else
    {
        $date_entry["last_date"] = "";
    }

    return $date_entry;

}

/*************************************************************************
   get item stock data
**************************************************************************/
function get_item_stock_data($item_id)
{
 
    require_once "include/order_state_constants.php";

    $item_stock_data = array();

    $sql = "select item_code, item_name, supplier_address, address_company, ".
           "    store_global_order, ".
           "    store_last_global_order, store_last_global_order_date, ".
           "    store_last_global_order_confirmation_date, ".
           "    store_inventory, store_physical_stock, ".
           "    store_stock_control_starting_date, store_consumed_upto_021101, " .
           "    store_minimum_order_quantity, ".
           "    store_reproduction_time_in_weeks, store_consumption_coverage ".
           "from suppliers ".
           "left join addresses on supplier_address = address_id ".
           "left join items on supplier_item = item_id ".
           "left join stores on (item_id = store_item and supplier_address = store_address) ".
           "where item_id = ". $item_id;

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $item_stock_data["code"] = $row["item_code"];
        $item_stock_data["name"] = $row["item_name"];
        $item_stock_data["supplier_id"] = $row["supplier_address"];
        $item_stock_data["supplier_name"] = $row["address_company"];
        $item_stock_data["global_order"] = $row["store_global_order"];
        $item_stock_data["last_global_order"] = $row["store_last_global_order"];
        $item_stock_data["last_global_order_date"] = to_system_date($row["store_last_global_order_date"]);
        $item_stock_data["last_global_order_confirmation_date"] = to_system_date($row["store_last_global_order_confirmation_date"]);
        $item_stock_data["inventory"] = $row["store_inventory"];
        $item_stock_data["physical_stock"] = $row["store_physical_stock"];

        $item_stock_data["stock_control_starting_date"] =  $row["store_stock_control_starting_date"];
        $item_stock_data["store_consumed_upto_021101"] = $row["store_consumed_upto_021101"];
        $item_stock_data["minimum_order_quantity"] = $row["store_minimum_order_quantity"];
        $item_stock_data["reproduction_time_in_weeks"] = $row["store_reproduction_time_in_weeks"];
        $item_stock_data["consumption_coverage"] = $row["store_consumption_coverage"];

        
        // calculate quantities having a pick up date
        $sum_of_quantity_having_a_pick_up_date = 0;
        $sql = "Select sum(order_item_quantity) as tmp_sum ".
               "from order_items ".
               "left join orders on order_item_order = order_id ".
               "where order_item_item= " . $item_id .
               "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
               "    and order_item_pickup <> '0000-00-00' ".
               "    and order_item_pickup is not null ".
               "    and order_cancelled is null";
    
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $sum_of_quantity_having_a_pick_up_date = $row["tmp_sum"];

        }

        
        $item_stock_data["remaining_global_order"] = $item_stock_data["global_order"] - $sum_of_quantity_having_a_pick_up_date;
        $item_stock_data["remaining_inventory"] = $item_stock_data["inventory"]  - $sum_of_quantity_having_a_pick_up_date;
    
        // calculate quantities having an expected ready for pick up date
        $sum_of_quantity_having_a_ready_for_pick_up_date = 0;
        $sql = "Select sum(order_item_quantity) as tmp_sum ".
               "from order_items ".
               "left join orders on order_item_order = order_id ".
               "where order_item_item= " . $item_id .
               "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
               "    and order_item_ready_for_pickup <> '0000-00-00' ".
               "    and order_item_ready_for_pickup is not null ".
               "    and order_cancelled is null";

        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $sum_of_quantity_having_a_ready_for_pick_up_date = $row["tmp_sum"];

        }

        // calculate quantities having actual arrival date
        $sum_of_quantity_having_an_actual_arrival_date = 0;
        $sql = "Select sum(order_item_quantity) as tmp_sum ".
               "from order_items ".
               "left join orders on order_item_order = order_id ".
               "where order_item_item= " . $item_id .
               "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
               "    and order_item_arrival <> '0000-00-00' ".
               "    and order_item_arrival is not null ".
               "    and order_cancelled is null";
    
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $sum_of_quantity_having_an_actual_arrival_date = $row["tmp_sum"];

        }
    
    
        $item_stock_data["external_stock"] = $sum_of_quantity_having_a_ready_for_pick_up_date - $sum_of_quantity_having_an_actual_arrival_date;



        // calculate quantities having an order confirmation date (for orders)
        $sum_of_quantity_having_an_order_confirmation_date = 0;

        $sql = "Select sum(order_item_quantity) as tmp_sum ".
               "from order_items ".
               "left join orders on order_item_order = order_id ".
               "where order_item_item= " . $item_id .
               "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
               "    and order_cancelled is null " .
               "    and order_actual_order_state_code >= " . ORDER_CONFIRMED .
               "    and order_type = 2";

        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $sum_of_quantity_having_an_order_confirmation_date = $row["tmp_sum"];

        }

        // calculate quantities having an approved budget (for projects)
        $sum_of_quantity_having_an_approved_budget = 0;
        
        $sql = "Select sum(order_item_quantity) as tmp_sum ".
               "from order_items ".
               "left join orders on order_item_order = order_id ".
               "where order_item_item= " . $item_id .
               "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
               "    and order_cancelled is null " .
               "    and order_actual_order_state_code >= " . BUDGET_APPROVED .
               "    and order_type = 1";

        $res = mysql_query($sql) or dberror($sql);
        while ($row = mysql_fetch_assoc($res))
        {
            $sum_of_quantity_having_an_approved_budget = $row["tmp_sum"];
        }
        
        $item_stock_data["bookings"] = $sum_of_quantity_having_an_approved_budget + $sum_of_quantity_having_an_order_confirmation_date - $sum_of_quantity_having_a_ready_for_pick_up_date;

        $item_stock_data["available"] = $item_stock_data["remaining_inventory"] - $item_stock_data["bookings"];


        // calculate Average Consumption per Week
        if ($item_stock_data["stock_control_starting_date"])
        {
            $date=$item_stock_data["stock_control_starting_date"];
            $y = substr($date, 0, 4);
            $m = substr($date, 5, 2);
            $d = substr($date, 8, 2);
            $now = mktime(); 
            $then = mktime(0, 0, 0, $m, $d, $y);
            $num_of_seconds = ($now - $then);
            $num_of_days = $num_of_seconds / 86400; 
            $num_of_weeks = $num_of_days / 7;
        
          
            $item_stock_data["weeks_since_stock_control_started"] = number_format($num_of_weeks, 2, ".", "'");

            // calcualte delivered as from stock_control_starting_date
            $sum_of_quantity_delivered = 0;
            $sql = "Select sum(order_item_quantity) as tmp_sum ".
               "from order_items ".
               "left join orders on order_item_order = order_id ".
               "where order_item_item= " . $item_id .
               "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
               "    and order_item_arrival >= " . dbquote($item_stock_data["stock_control_starting_date"]).
               "    and order_cancelled is null";
            
            $res = mysql_query($sql) or dberror($sql);
            if ($row = mysql_fetch_assoc($res))
            {
                $sum_of_quantity_delivered = $row["tmp_sum"];

            }

            $item_stock_data["delivered_since_stock_control_started"] = $sum_of_quantity_delivered + $item_stock_data["store_consumed_upto_021101"];
            
            if ($num_of_weeks > 0)
            {
                $item_stock_data["average_per_week"] = number_format(($item_stock_data["store_consumed_upto_021101"] + $sum_of_quantity_delivered) / $num_of_weeks , 2, ".", "'");

                if ($item_stock_data["average_per_week"] > 0)
                {
                    $item_stock_data["medium_range"] = number_format($item_stock_data["available"] /  $item_stock_data["average_per_week"],2, ".", "'");
                }
                else
                {
                    $item_stock_data["medium_range"] = "n/a";  
                }
            }
            else
            {
                $item_stock_data["average_per_week"] = "n/a";
                $item_stock_data["weeks_since_stock_control_started"] = "n/a";
                $item_stock_data["delivered_since_stock_control_started"] = "n/a";
                $item_stock_data["medium_range"] = "n/a";
            }
        
        }
        else
        {
            $item_stock_data["average_per_week"] = "n/a";
            $item_stock_data["weeks_since_stock_control_started"] = "n/a";
            $item_stock_data["delivered_since_stock_control_started"] = "n/a";
            $item_stock_data["medium_range"] = "n/a";
        }
        $item_stock_data["stock_control_starting_date"] = to_system_date($item_stock_data["stock_control_starting_date"]);
    }

    return $item_stock_data;

}


/*************************************************************************
   get warehouse address from table order_addresses
**************************************************************************/
function get_order_warehouses($sql)
{
    $warehouses = array();

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
        $sql = "select order_address_company ".
               "from order_addresses ".
               "where order_address_order = " .  $row["order_item_order"] .
               "    and order_address_order_item = " . $row["order_item_id"] .
               "    and order_address_type = 4";

        $res1 = mysql_query($sql) or dberror($sql);
        if ($row1 = mysql_fetch_assoc($res1))
        {
            $warehouses[$row["order_item_id"]] = $row1["order_address_company"];
        }
        else
        {
            $warehouses[$row["order_item_id"]] = "";
        }
    }
    return $warehouses;
}


/*************************************************************************
   get addresses from table order_addresses belonging to all order_items
**************************************************************************/
function get_order_adresses($order_id, $type)
{
    $addresses = array();

    $sql = "select * from order_addresses ".
		   " left join places on place_id = order_address_place_id " .
		   " left join orders on order_id = order_address_order " . 
           "where order_address_order = " . $order_id .
           "   and (order_address_order_item <> 0 or order_address_order_item is not null) ".
           "   and order_address_type = " . $type;

    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {

            $province_name = "";
			$posinfo = "";
			if($type == 2) //delivery address
			{
				$sql1 = "select province_canton ".
					   "from places " .
					   "left join provinces on province_id = place_province ".
					   "where place_id = " . dbquote($row['order_address_place_id']);

				$res1 = mysql_query($sql1);
				if ($res1)
				{
					$row1 = mysql_fetch_assoc($res1);
					$province_name = $row1['province_canton'];
				}


				//check if delivery address is POS address
				if($row["order_type"] == 1 
					and $row["order_shop_address_company"] ==  $row["order_address_company"])
				{
					
					//get pos type
					$sql_p = "select postype_name " . 
						     " from projects " .
						     " left join postypes on postype_id = project_postype " . 
						     " where project_order = " . dbquote($row["order_address_order"]) . 
						     " and postype_id in (1,2,3)";
					
					$res_p = mysql_query($sql_p);
					if ($row_p = mysql_fetch_assoc($res_p))
					{
						$posinfo = " / Tissot " . $row_p["postype_name"];
					}
				}
				elseif($row["order_type"] == 2)
				{

					$sql_p = "select postype_name " . 
						     " from posorders " . 
						     " left join posaddresses on posaddress_id = posorder_posaddress " . 
						     " left join postypes on postype_id = posaddress_store_postype " . 
						     " where posorder_order = " . dbquote($row["order_address_order"]) . 
						     " and postype_id in (1,2,3)";
					$res_p = mysql_query($sql_p);
					if ($row_p = mysql_fetch_assoc($res_p))
					{
						$posinfo = " / Tissot " . $row_p["postype_name"];
					}

				}

			}

			$sql1 = "select country_id, country_name ".
                   "from countries ".
                   "where country_id = " . dbquote($row['order_address_country']);

            $res1 = mysql_query($sql1);
            if ($res1)
            {
                $row1 = mysql_fetch_assoc($res1);
                $country_name = $row1['country_name'];
            }

			if($province_name)
			{
				$country_name = $province_name . ', ' . $country_name;
			}
            
            $tmp = $row["order_address_company"] . $posinfo . "\n";
			
			if($row["order_address_company2"])
			{
				$tmp .= $row["order_address_company2"] . "\n";
			}
            $tmp .= $row["order_address_address"] . "\n";
            if($row["order_address_address2"])
			{
				 $tmp .= $row["order_address_address2"] . "\n";
			}
			$tmp .= $row["order_address_zip"] . " " . $row["place_name"]  . ", ";
            $tmp .= $country_name  . "\n";
            $tmp .= 'Contact: '. $row["order_address_contact"] . "\n";
			if($row["order_address_email"])
			{
				$tmp .=	$row["order_address_email"] . "\n";
			}
            $tmp .= 'Phone: ' . $row["order_address_phone"] . "\n";
			if($row["order_address_mobile_phone"])
			{
				$tmp .= 'Mobile Phone: ' . $row["order_address_mobile_phone"] . "\n";
			}
			


            $addresses[$row["order_address_order_item"]] = $tmp;
    }
    return $addresses;
}


/***************************************************************************
   get the last date entry of a series of order states for a specific order
****************************************************************************/
function get_order_states($order_id, $type)
{
    
    require_once "include/order_state_constants.php";

    $order_states = array();
    $order_states_dates = array();
    $order_states_done_dates = array();
    $order_states_users = array();
    $order_states_users_done = array();
    $order_states_performers = array();
    $order_states_recepients = array();


    // --------------------------------------
    // assign process steps to the different roles
    
    // client
    $steps_client = array();
    $steps_client[] = "100";
    if ($type == 1) // project
    {
        $steps_client[] = "340";
        $steps_client[] = "350";
    }
    $steps_client[] = "610";
    $steps_client[] = "620";
    $steps_client[] = "800";
    $steps_client[] = "810";

 
    if ($type == 1) // project
    {
        // retail coordinator
        $steps_rtco = array();
        $steps_rtco[] = "110";
        $steps_rtco[] = "120";
        $steps_rtco[] = "130";
        $steps_rtco[] = "140";
        $steps_rtco[] = "230";
        $steps_rtco[] = "280";
        $steps_rtco[] = "290";
        $steps_rtco[] = "300";
        $steps_rtco[] = "330";

		$steps_rtco[] = "400";
		$steps_rtco[] = "412";
		$steps_rtco[] = "413";
		$steps_rtco[] = "414";
		$steps_rtco[] = "415";

        $steps_rtco[] = "420";
        $steps_rtco[] = "425";
        $steps_rtco[] = "426";
        $steps_rtco[] = "510";
        $steps_rtco[] = "550";
        $steps_rtco[] = "560";
        $steps_rtco[] = "600";

        // design contractor
        $steps_design_contractor = array();
        $steps_design_contractor[] = "240";
        $steps_design_contractor[] = "250";
        $steps_design_contractor[] = "260";
        $steps_design_contractor[] = "270";
        
		$steps_design_contractor[] = "405";
		$steps_design_contractor[] = "410";
		$steps_design_contractor[] = "411";
		
		$steps_design_contractor[] = "421";
        $steps_design_contractor[] = "422";
        $steps_design_contractor[] = "423";
        $steps_design_contractor[] = "424";

        // design supervisor
        $steps_design_supervisor = array();
        $steps_design_supervisor[] = "310";
        $steps_design_supervisor[] = "320";
    }

    // retail operator
    $steps_rto = array();
    if ($type == 2) // order
    {
        $steps_rto[] = "110";
        $steps_rto[] = "200";
        $steps_rto[] = "210";
        $steps_rto[] = "510";
        $steps_rto[] = "550";
        $steps_rto[] = "560";
        $steps_rto[] = "600";
    }
    $steps_rto[] = "700";
    $steps_rto[] = "730";
    $steps_rto[] = "890";
    $steps_rto[] = "900";


    // suppliers to offer
    $steps_supp_offer = array();
    $steps_supp_offer[] = "520";
    $steps_supp_offer[] = "530";
    $steps_supp_offer[] = "540";

    // suppliers delivering
    $steps_supp_order = array();
    $steps_supp_order[] = "710";
    $steps_supp_order[] = "720";

    // forwarders        
    $steps_frwd = array();
    $steps_frwd[] = "740";
    $steps_frwd[] = "750";

    // --------------------------------------
    // get all relevant order- / project data 
    
    // $client
    $client = "";
    $sql = "select address_shortcut ".
           "from orders ".
           "left join addresses on address_id = order_client_address ".
           "where address_id is not null ".
           "   and order_id = " . $order_id;

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $client = $row["address_shortcut"];
    }

    if ($type == 1) 
    {
        // retail coordinator
        $retail_coordinator = "";
        $sql = "select address_shortcut ".
               "from projects ".
               "left join users on user_id = project_retail_coordinator ".
               "left join addresses on address_id = user_address ".
               "where address_id is not null ".
               "   and project_order = " . $order_id;
    
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $retail_coordinator = $row["address_shortcut"];
        }
    

        // design contractor
        $design_contractor = "";
        $sql = "select address_shortcut ".
               "from projects ".
               "left join users on user_id = project_design_contractor ".
               "left join addresses on address_id = user_address ".
               "where address_id is not null ".
               "   and project_order = " . $order_id;
    
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $design_contractor = $row["address_shortcut"];
        }


        // design supervisor
        $design_supervisor = "";
        $sql = "select address_shortcut ".
               "from projects ".
               "left join users on user_id = project_design_supervisor ".
               "left join addresses on address_id = user_address ".
               "where address_id is not null ".
               "   and project_order = " . $order_id;
    
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            $design_supervisor = $row["address_shortcut"];
        }
    }


    // retail operator
    $retail_operator = "";
    $sql = "select address_shortcut ".
           "from orders ".
           "left join users on user_id = order_retail_operator ".
           "left join addresses on address_id = user_address ".
           "where address_id is not null ".
           "   and order_id = " . $order_id;
   
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $retail_operator = $row["address_shortcut"];
    }


    // supplieres to make an offer
    $suppliers_offering = "";
    $suppliers_offering_ids = array();

    $sql = "select distinct address_id, address_shortcut ".
           "from order_items ".
           "left join addresses on address_id = order_item_supplier_address ".
           "where order_item_order = " . $order_id .
           "   and (order_item_not_in_budget is null or order_item_not_in_budget = 0) ".
           "   and (order_item_no_offer_required is null or order_item_no_offer_required = 0) " .
           "   and address_id is not null ".
           "order by address_shortcut";

     $res = mysql_query($sql) or dberror($sql);
     while ($row = mysql_fetch_assoc($res))
     {
          $suppliers_offering = $suppliers_offering . $row["address_shortcut"] . "\n";
          $suppliers_offering_ids[$row["address_id"]] = $row["address_id"];
     }


    // supplieres to make an order to
    $suppliers = "";
    $suppliers_ids = array();

    $sql = "select distinct address_id, address_shortcut ".
           "from order_items ".
           "left join addresses on address_id = order_item_supplier_address ".
           "where order_item_order = " . $order_id .
           "   and (order_item_not_in_budget is null or order_item_not_in_budget = 0) ".
           "   and address_id is not null ".
           "order by address_shortcut";

     $res = mysql_query($sql) or dberror($sql);
     while ($row = mysql_fetch_assoc($res))
     {
          $suppliers = $suppliers . $row["address_shortcut"] . "\n";
          $suppliers_ids[$row["address_id"]] = $row["address_id"];
     }

    // forwarders
    $forwarders = "";
    $forwarders_ids = array();

    $sql = "select distinct address_id, address_shortcut ".
           "from order_items ".
           "left join addresses on address_id = order_item_forwarder_address ".
           "where order_item_order = " . $order_id .
           "   and (order_item_not_in_budget is null or order_item_not_in_budget = 0) ".
           "   and address_id is not null ".
           "order by address_shortcut";

     $res = mysql_query($sql) or dberror($sql);
     while ($row = mysql_fetch_assoc($res))
     {
          $forwarders = $forwarders . $row["address_shortcut"] . "\n";
          $forwarders_ids[$row["address_id"]] = $row["address_id"];
     }

    
    // --------------------------------------
    // assign recipients
    
    $recipients = array();
    $recipients["100"] = "Retail Net";

    if($type ==1)
    {
        $recipients["110"] = $retail_coordinator;
        $recipients["120"] = $retail_coordinator;
    }
    else
    {
        $recipients["110"] = $retail_operator;
    }
 
    $recipients["200"] = $client;
    $recipients["210"] = $client;

    if($type ==1)
    {
        $recipients["220"] = $design_contractor;    
        $recipients["230"] = $retail_coordinator;
        $recipients["240"] = $retail_coordinator;
        $recipients["250"] = $retail_coordinator;
        $recipients["260"] = $retail_coordinator;
        $recipients["270"] = $design_contractor;
        $recipients["280"] = $design_contractor;
        $recipients["300"] = $design_supervisor;    
        $recipients["310"] = $retail_coordinator;
        $recipients["320"] = $retail_coordinator;
        $recipients["330"] = $client;
        $recipients["340"] = $retail_coordinator;
        $recipients["350"] = $retail_coordinator;
        
		$recipients["400"] = $design_contractor;
		$recipients["405"] = $retail_coordinator;
		$recipients["410"] = $retail_coordinator;
		$recipients["411"] = $retail_coordinator;
		$recipients["412"] = $design_contractor;
		$recipients["413"] = $design_contractor;
		$recipients["414"] = $design_contractor;
		
		$recipients["415"] = $client;
		
		
		$recipients["416"] = $retail_coordinator;
		$recipients["417"] = $retail_coordinator;
		
		$recipients["420"] = $design_contractor;
        $recipients["421"] = $retail_coordinator;    
        $recipients["422"] = $retail_coordinator;
        $recipients["423"] = $retail_coordinator;
        $recipients["424"] = $retail_coordinator;
        $recipients["425"] = $design_contractor;
        $recipients["426"] = $design_contractor;
        $recipients["510"] = $suppliers_offering;
        $recipients["520"] = $retail_coordinator;    
        $recipients["530"] = $retail_coordinator;
        $recipients["540"] = $retail_coordinator;
        $recipients["550"] = $suppliers_offering;
        $recipients["560"] = $suppliers_offering;
        $recipients["600"] = $client;
        $recipients["610"] = $retail_coordinator;
        $recipients["620"] = $retail_coordinator;    
    }

    if ($type == 2)
    {
        $recipients["510"] = $suppliers_offering;
        $recipients["520"] = $retail_operator;    
        $recipients["530"] = $retail_operator;
        $recipients["540"] = $retail_operator;
        $recipients["550"] = $suppliers_offering;
        $recipients["560"] = $suppliers_offering;
        $recipients["600"] = $client;
        $recipients["610"] = $retail_operator;
        $recipients["620"] = $retail_operator;
    }

    $recipients["700"] = $suppliers;
    $recipients["710"] = $retail_operator;
    $recipients["720"] = $retail_operator;
    $recipients["730"] = $forwarders;
    $recipients["740"] = $retail_operator;
    $recipients["750"] = $retail_operator;
    $recipients["800"] = $retail_operator;    
    $recipients["810"] = $retail_operator;
    $recipients["890"] = "Retail Net";
    $recipients["900"] = "Retail Net";    
    $recipients["910"] = "";


    // build step relations 1:n
    $steps_one_to_n = array();
    $steps_one_to_n[] = "510";
    $steps_one_to_n[] = "550";
    $steps_one_to_n[] = "560";
    $steps_one_to_n[] = "700";
    $steps_one_to_n[] = "730";

    // build step relations n:1
    $steps_n_to_one = array();
    $steps_n_to_one[] = "520";
    $steps_n_to_one[] = "530";
    $steps_n_to_one[] = "540";
    $steps_n_to_one[] = "700";
    $steps_n_to_one[] = "710";
    $steps_n_to_one[] = "720";
    $steps_n_to_one[] = "740";
    $steps_n_to_one[] = "750";




    // --------------------------------------
    // build history data 

    $where_clause_addon = "";
    if ($type == 1)
    {
        if (!has_access("can_view_order_step_user_names_in_projects"))
        {
            $where_clause_addon = "    and actual_order_state_user = " . user_id();
        }
    }
    else
    {
        if (!has_access("can_view_order_step_user_names_in_orders"))
        {
            $where_clause_addon = "    and actual_order_state_user = " . user_id();
        }
    }

    
    $sql = "select  order_state_id, order_state_code ".
           "from order_states ".
           "left join order_state_groups on order_state_group = order_state_group_id ".
           "where order_state_group_order_type = " . $type . " ".
           "order by order_state_code";
    

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
       $number_of_records = 0;

       $sql = "select count(actual_order_state_id) as number_of_records ".
              "from actual_order_states ".
              "where actual_order_state_order = " . $order_id .
              "    and actual_order_state_state = ". $row["order_state_id"] .
              $where_clause_addon;

        $res1 = mysql_query($sql) or dberror($sql);

        if ($row1 = mysql_fetch_assoc($res1))
        {
            $number_of_records = $row1["number_of_records"];
        }

        if ($number_of_records > 0)
        {
            $entries = "[" . $number_of_records . "]";
        }
        else
        {
            $entries = "";
        }

        $limit = "";
        if ($number_of_records > 1)
        {
            $start = $number_of_records -1;
            $limit = "limit " . $start .", 1";
        }

        $sql = "select actual_order_states.date_created as state_date, ".
               "    concat(user_name, ' ', left(user_firstname,1), '.') as user_fullname ".
               "from actual_order_states ".
               "left join users on actual_order_state_user = user_id ".
               "where actual_order_state_order = " . $order_id . 
               "    and actual_order_state_state = ". $row["order_state_id"] . 
               $where_clause_addon . " " . $limit;

        $res2 = mysql_query($sql) or dberror($sql);
        if ($row2 = mysql_fetch_assoc($res2))
        {
            $order_states_dates[$row["order_state_code"]] = $entries . to_system_date($row2["state_date"]);
            $order_states_users[$row["order_state_code"]] = $row2["user_fullname"];
        }


        // get roles to perform an action
        if(in_array($row["order_state_code"], $steps_client))
        {
            $order_states_performers[$row["order_state_code"]] = $client;
        }
        else if(in_array($row["order_state_code"], $steps_rto))
        {
            $order_states_performers[$row["order_state_code"]] = $retail_operator;
        }
        else if(in_array($row["order_state_code"], $steps_supp_offer))
        {
            $order_states_performers[$row["order_state_code"]] = $suppliers_offering;
        }
        else if(in_array($row["order_state_code"], $steps_supp_order))
        {
           $order_states_performers[$row["order_state_code"]] = $suppliers;
        }
        else if(in_array($row["order_state_code"], $steps_frwd))
        {
            $order_states_performers[$row["order_state_code"]] = $forwarders;
        }

        if ($type == 1) // project
        {
            if(in_array($row["order_state_code"], $steps_rtco))
            {
                $order_states_performers[$row["order_state_code"]] = $retail_coordinator;
            }
            else if(in_array($row["order_state_code"], $steps_design_contractor))
            {
                $order_states_performers[$row["order_state_code"]] = $design_contractor;
            }
            else if(in_array($row["order_state_code"], $steps_design_supervisor))
            {
                $order_states_performers[$row["order_state_code"]] = $design_supervisor;
            }
        }

        $order_states_recepients[$row["order_state_code"]] = $recipients[$row["order_state_code"]];



        //check if a user of the performing address has made an email to the recepient address
        // 1:n Relations are: Tissot to suppliers, Tissot to forwarders
        // n:1 Relations are: suppliers to Tissot, forwarders to Tissot

        if(in_array($row["order_state_code"], $steps_one_to_n))
        {
                // process 1:n relations
                if($row["order_state_code"] == REQUEST_FOR_OFFER_SUBMITTED or $row["order_state_code"] == OFFER_REJECTED or $row["order_state_code"] == OFFER_ACCEPTED)
                {
                    $address_ids = array_values(array_unique($suppliers_offering_ids));  
                }
                elseif($row["order_state_code"] == ORDER_TO_SUPPLIER_SUBMITTED)
                {
                    $address_ids = array_values(array_unique($suppliers_ids));  
                }
                elseif($row["order_state_code"] == REQUEST_FOR_DELIVERY_SUBMITTED)
                {
                    $address_ids = array_values(array_unique($forwarders_ids));  
                }

                $tmp1 = "";
                $tmp2 = "";

                foreach($address_ids as $key => $value)
                {
                    //echo $row["order_state_code"] . " " . $value . "<br>";
                    // check if reciepient already has got a mail
                    $sql = "select order_mail_id, order_mails.date_created, ".
                           "    concat(users_2.user_name, ' ', left(users_2.user_firstname,1), '.') as from_user_fullname ".
                           "from order_mails ".
                           "left join users as users_1 on users_1.user_id = order_mail_user ".
                           "left join users as users_2 on users_2.user_id = order_mail_from_user ".
                           "where order_mail_order = ". $order_id .
                           "   and order_mail_order_state = " . $row["order_state_id"] .
                           "   and users_1.user_address = " . $value . " " .
                           "order by order_mails.date_created desc";

                    $res4 = mysql_query($sql) or dberror($sql);

                    if ($row4 = mysql_fetch_assoc($res4))
                    {
                        $tmp1 = $tmp1 . $row4["from_user_fullname"] . "\n";
                        $tmp2 = $tmp2 . to_system_date($row4["date_created"]) . "\n";
                    }
                    else
                    {
                        $tmp1 = $tmp1 .  " \n";
                        $tmp2 = $tmp2 .  " \n";
                    }
                }
                $order_states_users_done[$row["order_state_code"]] = $tmp1;
                $order_states_done_dates[$row["order_state_code"]] = $tmp2;
        }
        elseif(in_array($row["order_state_code"], $steps_n_to_one))
        {
                // process n:1 relations
                if($row["order_state_code"] == REQUEST_FOR_OFFER_REJECTED or $row["order_state_code"] == REQUEST_FOR_OFFER_ACCEPTED or $row["order_state_code"] == OFFER_SUBMITTED)
                {
                    $address_ids = array_values(array_unique($suppliers_offering_ids));
                }
                elseif($row["order_state_code"] == REJECT_ORDER_BY_SUPPLIER or $row["order_state_code"] == CONFIRM_ORDER_BY_SUPPLIER)
                {
                    $address_ids = array_values(array_unique($suppliers_ids));  
                }
                elseif($row["order_state_code"] == REQUEST_FOR_DELIVERY_ACCEPTED or $row["order_state_code"] == DELIVERY_CONFIRMED_FRW)
                {
                    $address_ids = array_values(array_unique($forwarders_ids));  
                }
                

                $tmp1 = "";
                $tmp2 = "";

                foreach($address_ids as $key => $value)
                {
                    // check if sender has already has  a mail
                    $sql = "select order_mail_id, order_mails.date_created, ".
                           "    concat(users_2.user_name, ' ', left(users_2.user_firstname,1), '.') as from_user_fullname ".
                           "from order_mails ".
                           "left join users as users_1 on users_1.user_id = order_mail_user ".
                           "left join users as users_2 on users_2.user_id = order_mail_from_user ".
                           "where order_mail_order = ". $order_id .
                           "   and order_mail_order_state = " . $row["order_state_id"] .
                           "   and users_2.user_address = " . $value . " " .
                           "order by order_mails.date_created desc";

                    $res4 = mysql_query($sql) or dberror($sql);

                    if ($row4 = mysql_fetch_assoc($res4))
                    {
                        $tmp1 = $tmp1 . $row4["from_user_fullname"] . "\n";
                        $tmp2 = $tmp2 . to_system_date($row4["date_created"]) . "\n";
                    }
                    else
                    {
                        $tmp1 = $tmp1 .  " \n";
                        $tmp2 = $tmp2 .  " \n";
                    }
                }
                $order_states_users_done[$row["order_state_code"]] = $tmp1;
                $order_states_done_dates[$row["order_state_code"]] = $tmp2;
        }
        else
        {
            // process 1:1 relations
            $sql = "select actual_order_states.date_created, ".
                   "   concat(user_name, ' ', left(user_firstname,1), '.') as from_user_fullname ".
                   "from actual_order_states ".
                   "left join users on user_id = actual_order_state_user ".
                   "left join addresses on address_id = user_address ".
                   "where actual_order_state_order = ". $order_id .
                   "   and actual_order_state_state = " . $row["order_state_id"] . " " .
                   "order by actual_order_states.date_created desc;";

             $res4 = mysql_query($sql) or dberror($sql);

             if ($row4 = mysql_fetch_assoc($res4))
             {
                $order_states_users_done[$row["order_state_code"]] = $row4["from_user_fullname"];
                $order_states_done_dates[$row["order_state_code"]] = to_system_date($row4["date_created"]);
             }
        }
    }

    $order_states["dates"] = $order_states_dates;
    $order_states["done_dates"] = $order_states_done_dates;
    $order_states["users"] = $order_states_users;
    $order_states["users_done"] = $order_states_users_done;
    $order_states["performers"] = $order_states_performers;
    $order_states["recepients"] = $order_states_recepients;
    return $order_states;

}

/*************************************************************************
   get actual order state of a specific order
**************************************************************************/
function get_actual_order_state($order_id)
{
    $actual_order_state = array();
    $images = array();

    //if (has_access("can_view_order_step_user_names_in_orders") or has_access("can_view_order_step_user_names_in_projects"))
    //{
        $sql = "select order_state_id, order_state_code, order_state_name ".
               "from actual_order_states ".
               "left join order_states on actual_order_state_state = order_state_id ".
               "where actual_order_state_order = " . $order_id . " " .
               "order by actual_order_states.date_modified desc";

        $res = mysql_query($sql) or dberror($sql);

        if ($row = mysql_fetch_assoc($res))
        {
            $actual_order_state["code"] = $row["order_state_code"];
            $actual_order_state["name"] = $row["order_state_name"];
            $images[$row["order_state_code"]] = "/pictures/actualorderstate.gif";
            $actual_order_state["images"] = $images;
        }
        else
        {
            $actual_order_state["code"] = "";
            $actual_order_state["name"] = "";
            $actual_order_state["images"] = "/pictures/actualorderstate_empty.gif";
        }
    //}
    //else
    //{
    //   $sql = "select order_state_id, order_state_code, order_state_name ".
    //           "from actual_order_states ".
    //           "left join order_states on actual_order_state_state = order_state_id ".
    //           "where actual_order_state_order = " . $order_id . " " .
    //           "order by actual_order_states.date_modified desc";

   //     $res = mysql_query($sql) or dberror($sql);

   //     if ($row = mysql_fetch_assoc($res))
   //     {
   //         $actual_order_state["code"] = $row["order_state_code"];
   //         $actual_order_state["name"] = $row["order_state_name"];
   //     }
   //     else
   //     {
   //         $actual_order_state["code"] = "";
   //         $actual_order_state["name"] = "";
   //     }
        
   //     $sql = "select order_state_id, order_state_code, order_state_name ".
   //            "from actual_order_states ".
   //            "left join order_states on actual_order_state_state = order_state_id ".
   //            "where actual_order_state_order = " . $order_id . 
   //            "    and actual_order_state_user = " . user_id() . " " .
   //            "order by actual_order_states.date_modified desc";

   //     $res = mysql_query($sql) or dberror($sql);

   //     if ($row = mysql_fetch_assoc($res))
   //     {
   //         $images[$row["order_state_code"]] = "/pictures/actualorderstate.gif";
   //         $actual_order_state["images"] = $images;
   //     }
   //     else
   //     {
   //         $images[$actual_order_state["code"]] = "/pictures/actualorderstate_empty.gif";
   //         $actual_order_state["images"] =  $images;
   //     }
   // }
    return $actual_order_state;

}

/*************************************************************************
   get project state name
**************************************************************************/
function get_project_state_name($id)
{
    $name = "";

    $sql = "select project_state_text ".
           "from project_states ".
           "where project_state_id = " . $id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $name = $row["project_state_text"];
    }

    return $name;
}

/*************************************************************************
   get actual order state name
**************************************************************************/
function get_actual_order_state_name($code, $type)
{
    $name = "";

    $sql = "select order_state_name ".
           "from order_states ".
           "left join order_state_groups on order_state_group_id = order_state_group ".
           "where order_state_group_order_type = " . $type .
           "   and order_state_code = '" . $code . "'";

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $name = $row["order_state_name"];
    }

    return $name;
}

/*************************************************************************
   check a specific order state (if present in actual_order_states)
**************************************************************************/
function is_present_order_state($order_id, $step)
{
    
    $sql = "select order_state_code, order_state_name ".
           "from actual_order_states ".
           "left join order_states on actual_order_state_state = order_state_id ".
           "where actual_order_state_order = " . $order_id . 
           "    and order_state_code = " . dbquote($step);

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        return true;
    }
    else
    {
        return false;
    }
}



/*************************************************************************
   check a specific order state (if present in actual_order_states)
**************************************************************************/
function get_last_order_state_performed($order_id)
{
    
    $sql = "select actual_order_state_state ".
           "from actual_order_states ".
           "where actual_order_state_order = " . $order_id .
           " order by actual_order_state_id DESC";

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        return $row["actual_order_state_state"];
    }
    else
    {
        return 0;
    }
}


/*************************************************************************
   get code of last order_state performed
**************************************************************************/
function get_code_of_last_order_state_performed($order_id)
{
    
    $sql = "select order_state_code ".
           "from actual_order_states ".
           "left join order_states on order_state_id = actual_order_state_state " . 
           "where actual_order_state_order = " . $order_id .
           " order by actual_order_state_id DESC";

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        return $row["order_state_code"];
    }
    else
    {
        return 0;
    }
}



/*************************************************************************
   get code of last order_state performed by a user
**************************************************************************/
function get_code_of_last_order_state_performed_by_user($order_id, $user_id)
{
    
    $sql = "select order_state_code ".
           "from actual_order_states ".
           "left join order_states on order_state_id = actual_order_state_state " . 
           "where actual_order_state_order = " . $order_id .
           "   and actual_order_state_user = " . $user_id .
           " order by actual_order_state_id DESC";

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        return $row["order_state_code"];
    }
    else
    {
        return 0;
    }
}

/**************************************************************************
   check if a specific predecessor action has been performed (order states)
***************************************************************************/
function predecessor_is_performed($order_id, $step, $type, $no_planning)
{
    if ($no_planning == 1)
    {
        $predecessor_field = "order_state_predecessor_no_planning";
    }
    else
    {
        $predecessor_field = "order_state_predecessor";
    }

        $sql = "select order_state_code, order_state_predecessor, order_state_predecessor_no_planning ".
               "from order_state_groups ".
               "left join order_states on order_state_group_id = order_state_group ".
               "where order_state_group_order_type = " . $type .
               "    and order_state_code = " . dbquote($step);
		
        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res))
        {
            if($row[$predecessor_field] == 0 or $row[$predecessor_field] == null)
            {
                return true;
            }
            else
            {
                $sql = "select actual_order_state_id ".
                       "from actual_order_states ".
                       "left join order_states on actual_order_state_state = order_state_id ".
                       "where actual_order_state_order = " . $order_id . " " .
                       "    and order_state_id = " . $row[$predecessor_field];

			
				$res = mysql_query($sql) or dberror($sql);

                if ($row = mysql_fetch_assoc($res))
                {
                    return true;
                }
                else
                {   
                    return false;
                }
            }
        }
        else
        {
            return false;
        }
}


/*************************************************************************
   get date and user of hq budget approval
**************************************************************************/
function get_hq_budget_approval_data($order_id)
{
    
    $sql = "select concat(user_name, ' ', user_firstname) as username, " . 
		   "actual_order_states.date_created as created ".
           "from actual_order_states ".
           "left join order_states on order_state_id = actual_order_state_state " .
		   " left join users on user_id = actual_order_state_user " . 
           "where actual_order_state_order = " . $order_id .
           "   and order_state_code = '620' " . 
           " order by actual_order_state_id DESC";

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        return $row;
    }
    else
    {
        return array();;
    }
}

/*************************************************************************
   get date and user of budget approval
**************************************************************************/
function get_budget_approval_data($order_id)
{
    
    $sql = "select concat(user_name, ' ', user_firstname) as username, " . 
		   "actual_order_states.date_created as created ".
           "from actual_order_states ".
           "left join order_states on order_state_id = actual_order_state_state " .
		   " left join users on user_id = actual_order_state_user " . 
           "where actual_order_state_order = " . $order_id .
           "   and order_state_code = '840' " . 
           " order by actual_order_state_id DESC";

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        return $row;
    }
    else
    {
        return array();;
    }
}

/**************************************************************************
   check if a specific predecessor action has been performed (order states)
***************************************************************************/
function get_order_state_predecessor($step, $type, $no_planning)
{
    if ($no_planning == 1)
    {
        $predecessor_field = "order_state_predecessor_no_planning";
    }
    else
    {
        $predecessor_field = "order_state_predecessor";
    }

    $sql = "select order_state_code, order_state_predecessor, order_state_predecessor_no_planning ".
           "from order_state_groups ".
           "left join order_states on order_state_group_id = order_state_group ".
           "where order_state_group_order_type = " . $type .
           "    and order_state_code = " . dbquote($step);

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        return $row[$predecessor_field];
    }
}

/*************************************************************************
   get the parameters for the action selected in project_flow_control.php
**************************************************************************/
function get_action_parameter($code, $type)
{
    $action_parameters = array();

    $sql = "select  order_state_id, order_state_code, ".
           "    order_state_name, order_state_action_name, order_state_mail_template_id, order_state_used_for_logistics, order_state_regional_responsible_as_cc, ".
           "    order_state_append_task, order_state_send_email, order_state_change_state, ".
		   "    order_state_file_category_id, order_state_file_category_id_to_select_from, " . 
           "    notification_recipient_name, user_firstname, user_name, user_email " .
           "from order_states ".
           "left join order_state_groups on order_state_group = order_state_group_id ".
           "left join notification_recipients on order_state_notification_recipient = notification_recipient_id ".
		   "left join users on user_id = order_state_email_copy_to " . 
           "where order_state_group_order_type = " . $type . " ".
           "    and order_state_code = " . $code;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $action_parameters["name"] = $row["order_state_name"];
        $action_parameters["action_name"] = $row["order_state_action_name"];
        $action_parameters["recipient"] = $row["notification_recipient_name"];
        $action_parameters["append_task"] = $row["order_state_append_task"];
        $action_parameters["send_email"] = $row["order_state_send_email"];
        $action_parameters["change_state"] = $row["order_state_change_state"];
		$action_parameters["order_state_email_copy_to"] = $row["user_name"] . " " . $row["user_firstname"];
		$action_parameters["order_state_email_copy_to_email"] = $row["user_email"];
		$action_parameters["order_state_file_category_id"] = $row["order_state_file_category_id"];
		$action_parameters["order_state_file_category_id_to_select_from"] = $row["order_state_file_category_id_to_select_from"];
		$action_parameters["mail_template"] = $row["order_state_mail_template_id"];
        $action_parameters["used_for_logistics"] = $row["order_state_used_for_logistics"];
		$action_parameters["order_state_regional_responsible_as_cc"] = $row["order_state_regional_responsible_as_cc"];

    }

    return $action_parameters;

}


/*************************************************************************
   get task data
**************************************************************************/
function get_task_data($task_id)
{
    $task_data = array();

    $sql =  "select * ".
            "from tasks ".
            "left join orders on order_id = task_order ".
            "left join projects on project_order = task_order ".
            "left join users on task_from_user = user_id ".
            "where task_id=" . $task_id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {

        $task_data["user"] = $row["task_user"];
        $task_data["text"] = $row["task_text"];
        $task_data["url"] = $row["task_url"];
        $task_data["due_date"] = to_system_date($row["task_due_date"]);
        $task_data["done_date"] = to_system_date($row["task_done_date"]);
        
        $task_data["assigned_by"] = $row["user_name"] . " " . $row["user_firstname"];

        $task_data["number"] = $row["order_number"];
        if ($row["project_id"])
        {
            $task_data["type_name"] = "Project";
        }
        else
        {
            $task_data["type_name"] = "Order";
        }
    
    }

    return $task_data;
}


/*************************************************************************
   get message data
**************************************************************************/
function get_message($id)
{
    $message_data = array();

    $sql =  "select * ".
            "from messages ".
            "where message_id=" . $id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $message_data["title"] = $row["message_title"];
        $message_data["text"] = $row["message_text"];

		if($row["message_file1"])
		{
			$sql_l = "select * from links " . 
				     " where link_id = " . $row["message_file1"];

			$res_l = mysql_query($sql_l) or dberror($sql_l);

			if ($row_l = mysql_fetch_assoc($res_l))
			{
				
				$link = '<a href="' . $row_l["link_path"] . '" target="_blank">Download ' . $row_l["link_title"] . '</a>';
				
				$message_data["text"] .= "<br />" . $link;
			}

		}

		if($row["message_file2"])
		{
			$sql_l = "select * from links " . 
				     " where link_id = " . $row["message_file2"];

			$res_l = mysql_query($sql_l) or dberror($sql_l);

			if ($row_l = mysql_fetch_assoc($res_l))
			{
				
				$link = '<a href="' . $row_l["link_path"] . '" target="_blank">Download ' . $row_l["link_title"] . '</a>';
				
				$message_data["text"] .= "<br />" . $link;
			}

		}

		if($row["message_file3"])
		{
			$sql_l = "select * from links " . 
				     " where link_id = " . $row["message_file3"];

			$res_l = mysql_query($sql_l) or dberror($sql_l);

			if ($row_l = mysql_fetch_assoc($res_l))
			{
				
				$link = '<a href="' . $row_l["link_path"] . '" target="_blank">Download ' . $row_l["link_title"] . '</a>';
				
				$message_data["text"] .= "<br />" . $link;
			}

		}

		if($row["message_file4"])
		{
			$sql_l = "select * from links " . 
				     " where link_id = " . $row["message_file4"];

			$res_l = mysql_query($sql_l) or dberror($sql_l);

			if ($row_l = mysql_fetch_assoc($res_l))
			{
				
				$link = '<a href="' . $row_l["link_path"] . '" target="_blank">Download ' . $row_l["link_title"] . '</a>';
				
				$message_data["text"] .= "<br />" . $link;
			}

		}
    }

	$message_data = str_replace("\r\n", "<br />", $message_data);
	$message_data = str_replace("\r", "<br />", $message_data);
	$message_data = str_replace("\n", "<br />", $message_data);


	

    return $message_data;
}


/*************************************************************************
   get all the companies, involved in an order for attachments
**************************************************************************/
function get_involved_companies($order_id, $id)
{
    $users_already_there = array();
    $companies = array();

    // get the retail coordinator
    $sql = "select distinct address_id, address_company, user_id, user_name, user_firstname ".
           "from orders ".
           "left join projects on project_order = order_id ".
           "left join users on user_id = project_retail_coordinator ".
           "left join addresses on address_id = user_address ".
           "where order_id = ". $order_id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        		
		if ($row["address_id"])
        {
            $company = array();
            $company["id"] = $row["address_id"];
            $company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["user_id"];
			$company["role"] = "Project Leader";
			$company["send_default_mail"] = 1;
            
            $sql1 = "select order_file_address_id from order_file_addresses ". 
                    "where order_file_address_file = " . $id .
                    "    and order_file_address_address = " .dbquote($row["address_id"]);

            $res1 = mysql_query($sql1) or dberror($sql1);

            if ($row1 = mysql_fetch_assoc($res1))
            {
                $company["access"] = 1;            
            }
            else
            {
                $company["access"] = 0;
            }

            if(!in_array($company["user"], $users_already_there))
			{
				$companies[$company["user"]] = $company;
			}
			$users_already_there[] = $company["user"];
        }
    }


    // get the retail operator
    $sql = "select distinct address_id, address_company, user_id, user_name, user_firstname ".
           "from orders ".
           "left join projects on project_order = order_id ".
           "left join users on user_id = order_retail_operator ".
           "left join addresses on address_id = user_address ".
           "where order_id = ". $order_id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        if ($row["address_id"])
        {
            $company = array();
            $company["id"] = $row["address_id"];
            $company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["user_id"];
			$company["role"] = "Logistics Coordinator";
			$company["send_default_mail"] = 1;

            $sql1 = "select order_file_address_id from order_file_addresses ". 
                    "where order_file_address_file = " . $id .
                    "    and order_file_address_address = " .dbquote($row["address_id"]);

            $res1 = mysql_query($sql1) or dberror($sql1);

            if ($row1 = mysql_fetch_assoc($res1))
            {
                $company["access"] = 1;            
            }
            else
            {
                $company["access"] = 0;
            }

            if(!in_array($company["user"], $users_already_there))
			{
				$companies[$company["user"]] = $company;
			}
			$users_already_there[] = $company["user"];
        }
    }
    
    
	// get the client
	$sql = "select distinct order_type, address_id, address_company, order_user, user_name, user_firstname ".
		   "from orders ".
		   "left join users on user_id = order_user ".
		   "left join addresses on address_id = order_client_address ".
		   "where order_id = ". $order_id;

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$company = array();
		$company["id"] = $row["address_id"];
		$company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
		$company["user"] = $row["order_user"];
		$company["send_default_mail"] = 0;

		if($row["order_type"] == 1)
		{
			$company["role"] = "Project Owner";
		}
		else
		{
			$company["role"] = "Client";
		}

			$sql1 = "select order_file_address_id from order_file_addresses ". 
					"where order_file_address_file = " . $id .
					"    and order_file_address_address = " .dbquote($row["address_id"]);

			
			$res1 = mysql_query($sql1) or dberror($sql1);

			if ($row1 = mysql_fetch_assoc($res1))
			{
				$company["access"] = 1;            
			}
			else
			{
				$company["access"] = 0;
			}

			if(!in_array($company["user"], $users_already_there))
			{
				$companies[$company["user"]] = $company;
			}
			$users_already_there[] = $company["user"];
	}


	// get the local project Leader
	$sql = "select distinct address_id, address_company, project_local_retail_coordinator, user_name, user_firstname ".
		   "from projects ".
		   "left join users on user_id = project_local_retail_coordinator ".
		   "left join addresses on address_id = user_address ".
		   "where project_order = ". $order_id;

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		if($row["address_id"] > 0)
		{
			$company = array();
			$company["id"] = $row["address_id"];
			$company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["project_local_retail_coordinator"];
			$company["role"] = "Local Project Leader";
			$company["send_default_mail"] = 0;

				$sql1 = "select order_file_address_id from order_file_addresses ". 
						"where order_file_address_file = " . $id .
						"    and order_file_address_address = " .dbquote($row["address_id"]);

				$res1 = mysql_query($sql1) or dberror($sql1);

				if ($row1 = mysql_fetch_assoc($res1))
				{
					$company["access"] = 1;            
				}
				else
				{
					$company["access"] = 0;
				}

			if(!in_array($company["user"], $users_already_there))
			{
				$companies[$company["user"]] = $company;
			}
			$users_already_there[] = $company["user"];
		}
	}


	// get the design contractor
	$sql = "select distinct address_id, address_company, user_id, user_name, user_firstname ".
		   "from orders ".
		   "left join projects on project_order = order_id ".
		   "left join users on user_id = project_design_contractor ".
		   "left join addresses on address_id = user_address ".
		   "where order_id = ". $order_id;

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		if ($row["address_id"])
		{
			$company = array();
			$company["id"] = $row["address_id"];
			$company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["user_id"];
			$company["role"] = "Design Contractor";
			$company["send_default_mail"] = 0;
			
			$sql1 = "select order_file_address_id from order_file_addresses ". 
					"where order_file_address_file = " . $id .
					"    and order_file_address_address = " .dbquote($row["address_id"]);

			$res1 = mysql_query($sql1) or dberror($sql1);

			if ($row1 = mysql_fetch_assoc($res1))
			{
				$company["access"] = 1;            
			}
			else
			{
				$company["access"] = 0;
			}

			if(!in_array($company["user"], $users_already_there))
			{
				$companies[$company["user"]] = $company;
			}
			$users_already_there[] = $company["user"];
		}
	}
   
	
	// get the design supervisor
	$sql = "select distinct address_id, address_company, user_id, user_name, user_firstname ".
		   "from orders ".
		   "left join projects on project_order = order_id ".
		   "left join users on user_id = project_design_supervisor ".
		   "left join addresses on address_id = user_address ".
		   "where order_id = ". $order_id;

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		if ($row["address_id"])
		{
			$company = array();
			$company["id"] = $row["address_id"];
			$company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["user_id"];
			$company["role"] = "Design Supervisor";
			$company["send_default_mail"] = 1;
			
			$sql1 = "select order_file_address_id from order_file_addresses ". 
					"where order_file_address_file = " . $id .
					"    and order_file_address_address = " .dbquote($row["address_id"]);

			$res1 = mysql_query($sql1) or dberror($sql1);

			if ($row1 = mysql_fetch_assoc($res1))
			{
				$company["access"] = 1;            
			}
			else
			{
				$company["access"] = 0;
			}

			if(!in_array($company["user"], $users_already_there))
			{
				$companies[$company["user"]] = $company;
			}
			$users_already_there[] = $company["user"];
		}
	}
	
	

	// get the suppliers
	$sql = "select distinct order_item_supplier_address, address_company,user_id, user_name, user_firstname ".
		   "from orders ".
		   "left join order_items on order_item_order = order_id ".
		   "left join addresses on address_id = order_item_supplier_address " .
		   "left join users on user_address = order_item_supplier_address ".
		   "where address_id != 2390 and user_active = 1 and order_id = ". $order_id;

	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{
		if ($row["order_item_supplier_address"])
		{
			$company = array();
			$company["id"] = $row["order_item_supplier_address"];
			$company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["user_id"];
			$company["role"] = "Supplier";
			$company["send_default_mail"] = 0;

			$sql1 = "select order_file_address_id from order_file_addresses ". 
					"where order_file_address_file = " . $id .
					"    and order_file_address_address = " . $row["order_item_supplier_address"];

			$res1 = mysql_query($sql1) or dberror($sql1);

			if ($row1 = mysql_fetch_assoc($res1))
			{
				$company["access"] = 1;            
			}
			else
			{
				$company["access"] = 0;
			}

			if(!in_array($company["user"], $users_already_there))
			{
				$companies[$company["user"]] = $company;
			}
			$users_already_there[] = $company["user"];
		}
	}

	// get the forwarders
	$sql = "select distinct order_item_forwarder_address, address_company,user_id, user_name, user_firstname ".
		   "from orders ".
		   "left join order_items on order_item_order = order_id ".
		   "left join addresses on address_id = order_item_forwarder_address ".
		   "left join users on user_address = order_item_forwarder_address ".
		   "where address_id != 2390 and user_active = 1 and order_id = ". $order_id;

	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{
		if ($row["order_item_forwarder_address"])
		{
			$company = array();
			$company["id"] = $row["order_item_forwarder_address"];
			$company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["user_id"];
			$company["role"] = "Forwarder";
			$company["send_default_mail"] = 0;

			$sql1 = "select order_file_address_id from order_file_addresses  ". 
					"where order_file_address_file = " . $id .
					"    and order_file_address_address = " . $row["order_item_forwarder_address"];

			$res1 = mysql_query($sql1) or dberror($sql1);

			if ($row1 = mysql_fetch_assoc($res1))
			{
				$company["access"] = 1;            
			}
			else
			{
				$company["access"] = 0;
			}

			if(!in_array($company["user"], $users_already_there))
			{
				$companies[$company["user"]] = $company;
			}
			$users_already_there[] = $company["user"];
		}
	}

	
	// get speical users as additional reciepeints
	$users = array();
	$sql = "select order_type, address_country, order_client_address ".
		   "from orders ".
		   "left join addresses on address_id = order_client_address " . 
		   "where order_id = ". $order_id;

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		
		
		//get users
		if($row["order_type"] == 1) // project
		{
			$sql = "select user_id, user_substitute " . 
				   "from users " . 
				   "left join addresses on address_id = user_address " .
				   "where address_id = " . $row["order_client_address"] .
				   " and user_active = 1 and user_project_reciepient = 1";
		}
		else
		{
			$sql = "select user_id, user_substitute " . 
				   "from users " . 
				   "left join addresses on address_id = user_address " .
				   "where address_id = " . $row["order_client_address"] .
				   " and user_active = 1 and user_order_reciepient = 1";
		}
				
		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			$users[]  = $row["user_id"];
		}
		
		if(count($users) > 0)
		{
			$user_filter = implode(',', $users);
			$sql = "select distinct address_id, address_company, user_id, user_name, user_firstname ".
				   "from addresses ".
				   "left join users on user_address = address_id ".
				   "where user_id IN (" . $user_filter . ")";


			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{
				$company = array();
				$company["id"] = $row["address_id"];
				$company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
				$company["user"] = $row["user_id"];
				$company["role"] = "Additional Recipient";
				$company["send_default_mail"] = 0;


				$sql1 = "select order_file_address_id from order_file_addresses ". 
						"where order_file_address_file = " . $id .
						"    and order_file_address_address = " .dbquote($row["address_id"]);

				$res1 = mysql_query($sql1) or dberror($sql1);

				if ($row1 = mysql_fetch_assoc($res1))
				{
					$company["access"] = 1;            
				}
				else
				{
					$company["access"] = 0;
				}

				 if(!in_array($company["user"], $users_already_there))
				 {
					$companies[$company["user"]] = $company;
				 }
				 $users_already_there[] = $company["user"];
			}


		}

	}


	// get regional reponsibles in the context of projects
	$users = array();
	$sql = "select order_client_address, project_cost_type ".
		   "from orders ".
		   " left join project_costs on project_cost_order = order_id " . 
		   "where order_type = 1 and order_id = ". $order_id;

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		if($row["project_cost_type"] == 1) {
			$sql = "select user_id, user_substitute " . 
				   "from user_company_responsibles " . 
				   " left join users on user_id = user_company_responsible_user_id " . 
				   " left join addresses on address_id = user_address " .
				   " where user_company_responsible_retail = 1  " . 
				   " and user_company_responsible_address_id = " . $row["order_client_address"] .
				   " and user_active = 1";
		}
		else {
			$sql = "select user_id, user_substitute " . 
				   "from user_company_responsibles " . 
				   " left join users on user_id = user_company_responsible_user_id " . 
				   " left join addresses on address_id = user_address " .
				   " where user_company_responsible_wholsale = 1  " . 
				   " and user_company_responsible_address_id = " . $row["order_client_address"] .
				   " and user_active = 1";
		}
						
		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			$users[]  = $row["user_id"];
		}
		
		if(count($users) > 0)
		{
			$user_filter = implode(',', $users);
			$sql = "select distinct address_id, address_company, user_id, user_name, user_firstname ".
				   "from addresses ".
				   "left join users on user_address = address_id ".
				   "where user_id IN (" . $user_filter . ")";


			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{
				$company = array();
				$company["id"] = $row["address_id"];
				$company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
				$company["user"] = $row["user_id"];
				$company["role"] = "Additional Recipient";
				$company["send_default_mail"] = 0;


				$sql1 = "select order_file_address_id from order_file_addresses ". 
						"where order_file_address_file = " . $id .
						"    and order_file_address_address = " .dbquote($row["address_id"]);

				$res1 = mysql_query($sql1) or dberror($sql1);

				if ($row1 = mysql_fetch_assoc($res1))
				{
					$company["access"] = 1;            
				}
				else
				{
					$company["access"] = 0;
				}

				 if(!in_array($company["user"], $users_already_there))
				 {
					$companies[$company["user"]] = $company;
				 }
				 $users_already_there[] = $company["user"];
			}


		}

	}
	
    return $companies;
}



/*************************************************************************
   get all the companies, involved in an order for comments
**************************************************************************/
function get_involved_companies_2($order_id, $id)
{
    
    $companies = array();
	$users_already_there = array();

    // get the retail coordinator
    $sql = "select distinct address_id, address_company, user_id, user_name, user_firstname ".
           "from orders ".
           "left join projects on project_order = order_id ".
           "left join users on user_id = project_retail_coordinator ".
           "left join addresses on address_id = user_address ".
           "where order_id = ". $order_id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        if ($row["address_id"])
        {
            $company = array();
            $company["id"] = $row["address_id"];
            $company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["user_id"];
			$company["role"] = "Project Leader";
            
            $sql1 = "select comment_address_id from comment_addresses ". 
                    "where comment_address_comment = " . $id .
                    "    and comment_address_address = " . $row["address_id"];

            $res1 = mysql_query($sql1) or dberror($sql1);

            if ($row1 = mysql_fetch_assoc($res1))
            {
                $company["access"] = 1;            
            }
            else
            {
                $company["access"] = 0;
            }

            if(!in_array($company["user"], $users_already_there))
			{
				$companies[$company["user"]] = $company;
			}
			$users_already_there[] = $company["user"];
        }
    }


    // get the retail operator
    $sql = "select distinct address_id, address_company, user_id, user_name, user_firstname ".
           "from orders ".
           "left join projects on project_order = order_id ".
            "left join users on user_id = order_retail_operator ".
           "left join addresses on address_id = user_address ".
           "where order_id = ". $order_id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        if ($row["address_id"])
        {
            $company = array();
            $company["id"] = $row["address_id"];
            $company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["user_id"];
			$company["role"] = "Logistics Coordinator";

            $sql1 = "select comment_address_id from comment_addresses ". 
                    "where comment_address_comment = " . $id .
                    "    and comment_address_address = " . $row["address_id"];
            
            $res1 = mysql_query($sql1) or dberror($sql1);

            if ($row1 = mysql_fetch_assoc($res1))
            {
                $company["access"] = 1;            
            }
            else
            {
                $company["access"] = 0;
            }

            if(!in_array($company["user"], $users_already_there))
			{
				$companies[$company["user"]] = $company;
			}
			$users_already_there[] = $company["user"];
        }
    }
    
    // get the client
    $sql = "select distinct order_type, address_id, address_company, order_user, user_name, user_firstname ".
           "from orders ".
		    "left join users on user_id = order_user ".
           "left join addresses on address_id = order_client_address ".
           "where order_id = ". $order_id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $company = array();
        $company["id"] = $row["address_id"];
        $company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
		$company["user"] = $row["order_user"];
		
		if($row["order_type"] == 1)
		{
			$company["role"] = "Project Owner";
		}
		else
		{
			$company["role"] = "Client";
		}

            $sql1 = "select comment_address_id from comment_addresses ". 
                    "where comment_address_comment = " . $id .
                    "    and comment_address_address = " . $row["address_id"];

            $res1 = mysql_query($sql1) or dberror($sql1);

            if ($row1 = mysql_fetch_assoc($res1))
            {
                $company["access"] = 1;            
            }
            else
            {
                $company["access"] = 0;
            }

			if(!in_array($company["user"], $users_already_there))
			{
				$companies[$company["user"]] = $company;
			}
			$users_already_there[] = $company["user"];
    }


    // get the design contractor
    $sql = "select distinct address_id, address_company, user_id, user_name, user_firstname ".
           "from orders ".
           "left join projects on project_order = order_id ".
           "left join users on user_id = project_design_contractor ".
           "left join addresses on address_id = user_address ".
           "where order_id = ". $order_id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        if ($row["address_id"])
        {
            $company = array();
            $company["id"] = $row["address_id"];
            $company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["user_id"];
			$company["role"] = "Design Contractor";
            
            $sql1 = "select comment_address_id from comment_addresses ". 
                    "where comment_address_comment = " . $id .
                    "    and comment_address_address = " . $row["address_id"];

            $res1 = mysql_query($sql1) or dberror($sql1);

            if ($row1 = mysql_fetch_assoc($res1))
            {
                $company["access"] = 1;            
            }
            else
            {
                $company["access"] = 0;
            }

            if(!in_array($company["user"], $users_already_there))
			{
				$companies[$company["user"]] = $company;
			}
			$users_already_there[] = $company["user"];
        }
    }

    
    
    
    // get the design supervisor
    $sql = "select distinct address_id, address_company, user_id, user_name, user_firstname ".
           "from orders ".
           "left join projects on project_order = order_id ".
           "left join users on user_id = project_design_supervisor ".
           "left join addresses on address_id = user_address ".
           "where order_id = ". $order_id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        if ($row["address_id"])
        {
            $company = array();
            $company["id"] = $row["address_id"];
            $company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["user_id"];
			$company["role"] = "Design Supervisor";
            
            $sql1 = "select comment_address_id from comment_addresses ". 
                    "where comment_address_comment = " . $id .
                    "    and comment_address_address = " . $row["address_id"];

            $res1 = mysql_query($sql1) or dberror($sql1);

            if ($row1 = mysql_fetch_assoc($res1))
            {
                $company["access"] = 1;            
            }
            else
            {
                $company["access"] = 0;
            }

			if(!in_array($company["user"], $users_already_there))
			{
				$companies[$company["user"]] = $company;
			}
			$users_already_there[] = $company["user"];
        }
    }
    
    

    // get the suppliers
    $sql = "select distinct order_item_supplier_address, address_company, user_id, user_name, user_firstname ".
           "from orders ".
           "left join order_items on order_item_order = order_id ".
           "left join addresses on address_id = order_item_supplier_address ".
           "left join users on user_address = order_item_supplier_address ".
           "where address_id != 2390 and user_active = 1 and order_id = ". $order_id;

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        if ($row["order_item_supplier_address"])
        {
            $company = array();
            $company["id"] = $row["order_item_supplier_address"];
            $company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["user_id"];
			$company["role"] = "Supplier";

            $sql1 = "select comment_address_id from comment_addresses ". 
                    "where comment_address_comment = " . $id .
                    "    and comment_address_address = " . $row["order_item_supplier_address"];

            $res1 = mysql_query($sql1) or dberror($sql1);

            if ($row1 = mysql_fetch_assoc($res1))
            {
                $company["access"] = 1;            
            }
            else
            {
                $company["access"] = 0;
            }

            if(!in_array($company["user"], $users_already_there))
			{
				$companies[$company["user"]] = $company;
			}
			$users_already_there[] = $company["user"];
        }
    }

    // get the forwarders
    $sql = "select distinct order_item_forwarder_address, address_company, user_id, user_name, user_firstname ".
           "from orders ".
           "left join order_items on order_item_order = order_id ".
           "left join addresses on address_id = order_item_forwarder_address ".
           "left join users on user_address = order_item_forwarder_address ".
           "where address_id != 2390 and user_active = 1 and order_id = ". $order_id;

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        if ($row["order_item_forwarder_address"])
        {
            $company = array();
            $company["id"] = $row["order_item_forwarder_address"];
            $company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
			$company["user"] = $row["user_id"];
			$company["role"] = "Forwarder";

            $sql1 = "select comment_address_id from comment_addresses ". 
                    "where comment_address_comment = " . $id .
                    "    and comment_address_address = " . $row["order_item_forwarder_address"];

            $res1 = mysql_query($sql1) or dberror($sql1);

            if ($row1 = mysql_fetch_assoc($res1))
            {
                $company["access"] = 1;            
            }
            else
            {
                $company["access"] = 0;
            }

            if(!in_array($company["user"], $users_already_there))
			{
				$companies[$company["user"]] = $company;
			}
			$users_already_there[] = $company["user"];
        }
    }


	// get speical users as additional reciepeints
	$users = array();
	$sql = "select order_type, address_country, order_client_address ".
		   "from orders ".
		   "left join addresses on address_id = order_client_address " . 
		   "where order_id = ". $order_id;

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		
		
		//get users
		if($row["order_type"] == 1) // project
		{
			$sql = "select user_id, user_substitute " . 
				   "from users " . 
				   "left join addresses on address_id = user_address " .
				   "where address_country = " . $row["address_country"] . 
				   " and user_active = 1 and user_project_reciepient = 1";
		}
		else
		{
			$sql = "select user_id, user_substitute " . 
				   "from users " . 
				   "left join addresses on address_id = user_address " .
				   "where address_country = " . $row["address_country"] . 
				   " and user_active = 1 and user_order_reciepient = 1";
		}
				
		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			$users[]  = $row["user_id"];
		}
		
		if(count($users) > 0)
		{

			$user_filter = implode(',', $users);
			$sql = "select distinct address_id, address_company, user_id, user_name, user_firstname ".
				   "from addresses ".
				   "left join users on user_address = address_id ".
				   "where user_id IN (" . $user_filter . ")";

			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{
				$company = array();
				$company["id"] = $row["address_id"];
				$company["name"] = $row["user_name"] . ' ' .  $row["user_firstname"] . ' (' . $row["address_company"] . ')';
				$company["user"] = $row["user_id"];
				$company["role"] = "Additional Recipient";
				$company["send_default_mail"] = 0;


				$sql1 = "select comment_address_id from comment_addresses ". 
						"where comment_address_comment = " . $id .
						"    and comment_address_address = " . dbquote($row["address_id"]);

						$res1 = mysql_query($sql1) or dberror($sql1);

						if ($row1 = mysql_fetch_assoc($res1))
						{
							$company["access"] = 1;            
						}
						else
						{
							$company["access"] = 0;
						}

				if(!in_array($company["user"], $users_already_there))
				{
					$companies[$company["user"]] = $company;
				}
				$users_already_there[] = $company["user"];
			}


		}

	}


    return $companies;
}



/*************************************************************************
   get order_mail_recipients
**************************************************************************/
function get_order_mail_recipients($order_id, $table, $table_key)
{
	$recipients = array();

	$sql = "select distinct address_id, address_company, user_id, " . 
		   "concat(user_name, ' ', user_firstname) as username, order_mail_is_cc ".
           "from order_mails ".
           "left join users on user_id = order_mail_user ".
           "left join addresses on address_id = user_address ".
           "where order_mail_order = ". $order_id . 
		   " and order_mail_table = " . dbquote($table) . 
		   " and order_mail_table_key = " . dbquote($table_key) . 
		   " order by order_mail_is_cc, username";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$tmp = array();
		$tmp["address_id"] = $row["address_id"];
		if($row["order_mail_is_cc"] == 1)
		{
			$recipients[$row["user_id"]] = "cc: " . $row["username"] . " (" . $row["address_company"] . ")";
		}
		else
		{
			$recipients[$row["user_id"]] = $row["username"] . " (" . $row["address_company"] . ")";
		}
	}


	return $recipients;
}

/*************************************************************************
   set a picture for every order the user has to do something
**************************************************************************/
function set_to_do_pictures($sql_images, $type)
{
    $images = array();

    $res = mysql_query($sql_images) or dberror($sql_images);

    while ($row = mysql_fetch_assoc($res))
    {

        $sql = "select task_id " .
               "from tasks " .
               "where task_order = " . $row["order_id"] .
               "   and task_user = " . user_id() .
               "   and task_done_date is null ";

        $res1 = mysql_query($sql) or dberror($sql);

        if ($row1 = mysql_fetch_assoc($res1))
        {            
            if ($type == 1)
            {
                $images[$row["project_id"]] = "/pictures/todo.gif";
            }
            else
            {
                $images[$row["order_id"]] = "/pictures/todo.gif";
            }
        }
        else
        {

            if ($type == 1 and has_access("can_edit_retail_data"))
            {
                if (!$row["concat(user_name,' ',user_firstname)"])
                {
                    $images[$row["project_id"]] = "/pictures/bullet_ball_glass_red.gif";
                }
            }
            else if ($type == 2 and has_access("can_edit_retail_data"))
            {
                if (!$row["concat(user_name,' ',user_firstname)"])
                {
                    $images[$row["order_id"]] = "/pictures/bullet_ball_glass_red.gif";
                }
            }

        }
    }

    return $images;

}


/*************************************************************************
   get file owener
**************************************************************************/
function get_file_owner($id)
{
    $sql = "select order_file_owner from order_files where order_file_id = " . $id;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {            
        $owner = $row["order_file_owner"];
    }
    else
    {
        $owener = 0;
    }
    return $owner;
}


/*************************************************************************
   check if all supplier's offers have a price entered
**************************************************************************/
function check_if_all_items_have_prices($oid, $supplier)
{
    
    $num_recs = 0;

    // check if all special items have a price entered
    $sql = "select count(order_item_id) as num_recs " .
           "from order_items " .
           "where order_item_type = " . ITEM_TYPE_SPECIAL . 
           "    and order_item_item is null " .
           "    and order_item_order = " . $oid .
           "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) " . 
           "    and (order_item_no_offer_required = 0 or order_item_no_offer_required is null) " .
           "    and (order_item_supplier_price = 0 or order_item_supplier_price is null) " .
		   "    and order_item_only_quantity_proposal = 0 " . 
           "    and order_item_supplier_address = " . $supplier;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {            
        $num_recs = $row["num_recs"];
    }

    if ($num_recs > 0) 
    {
        return 0;
    }
    else
    {
        return 1;
    }

}


/*************************************************************************
   check if all supplier's offes have been accepted
**************************************************************************/
function check_if_all_items_are_offered($oid)
{
    
    $num_recs = 0;

    // check if all special items have been offered by suppliers
    
	
	//alte version vo 5.6.2015
	/*
	$sql = "select count(order_item_id) as num_recs " .
           "from order_items " .
           "where order_item_type = " . ITEM_TYPE_SPECIAL . 
           "    and order_item_item is null " .
           "    and order_item_order = " . $oid .
           "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) " . 
           "    and (order_item_no_offer_required = 0 or order_item_no_offer_required is null) " .
		   "    and order_item_only_quantity_proposal = 0 " .
           "    and order_item_client_price = 0 ";


    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {            
        $num_recs = $row["num_recs"];
    }

    if ($num_recs > 0) 
    {
        return 0;
    }


	$offers_accepted = 1;

    $sql = "select distinct order_item_supplier_address ".
           "from order_items ".
           "where (order_item_type = " . ITEM_TYPE_STANDARD . 
		   "    or order_item_type = " . ITEM_TYPE_SPECIAL . ") " .  
           "    and order_item_order = " . $oid .
           "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
		   "    and order_item_only_quantity_proposal = 0 " .
           "    and (order_item_no_offer_required = 0 or order_item_no_offer_required is null) ";


	$res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {  
        $sql = "select count(order_mail_id) as num_recs " .
               "from order_mails ".
               "left join order_states on order_state_id = order_mail_order_state ".
               "left join users on order_mail_user = user_id ".
               "left join addresses on user_address = address_id ".
               "where order_mail_order = " . $oid .
               "    and order_state_code = " . OFFER_ACCEPTED .
               "    and user_address = " . $row["order_item_supplier_address"];

        $res1 = mysql_query($sql) or dberror($sql);


        if ($row1 = mysql_fetch_assoc($res1))
        {
            if($row1["num_recs"] == 0)
            {
                $offers_accepted = 0;
            }
        }
    }

    if ($offers_accepted == 0)
    {
        return 0;
    }
    else
    {
		return 1;
    }
	*/


	//neue version 5.6.2015
	// block budget submission if we have special items in the budget having no selling price
	$sql = "select count(order_item_id) as num_recs " .
           "from order_items " .
           "where order_item_type = " . ITEM_TYPE_SPECIAL . 
           "    and order_item_item is null " .
           "    and order_item_order = " . $oid .
           "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) " .
		   "    and order_item_client_price = 0";


    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {            
        $num_recs = $row["num_recs"];
    }

    if ($num_recs > 0) 
    {
        return 0;
    }
	

	//check if offers were approved
	$offers_accepted = 1;

    $sql = "select distinct order_item_supplier_address ".
           "from order_items ".
           "where (order_item_type = " . ITEM_TYPE_STANDARD . 
		   "    or order_item_type = " . ITEM_TYPE_SPECIAL . ") " .  
           "    and order_item_order = " . $oid .
           "    and (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
		   "    and order_item_only_quantity_proposal = 0 " .
           "    and (order_item_no_offer_required = 0 or order_item_no_offer_required is null) ";


	$res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {  
        $sql = "select count(order_mail_id) as num_recs " .
               "from order_mails ".
               "left join order_states on order_state_id = order_mail_order_state ".
               "left join users on order_mail_user = user_id ".
               "left join addresses on user_address = address_id ".
               "where order_mail_order = " . $oid .
               "    and order_state_code = " . OFFER_ACCEPTED .
               "    and user_address = " . $row["order_item_supplier_address"];

        $res1 = mysql_query($sql) or dberror($sql);


        if ($row1 = mysql_fetch_assoc($res1))
        {
            if($row1["num_recs"] == 0)
            {
                $offers_accepted = 0;
            }
        }
    }

    if ($offers_accepted == 0)
    {
        return 0;
    }
    else
    {
		return 1;
    }

}



/*************************************************************************
   check if all items do have a ready for pick up date
**************************************************************************/
function check_if_all_items_hav_pick_up_date($oid, $address_id)
{
    
    $sql = "select count(order_item_id) as num_recs " .
                   "from order_items " .
                   "where order_item_order = " . $oid .
                   "  and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                   "  and (order_item_ready_for_pickup is null or order_item_ready_for_pickup = '0000-00-00') " .
                   "  and (order_item_not_in_budget is null or order_item_not_in_budget = 0) " .
		           "  and order_item_quantity > 0 " .
                   "  and order_item_supplier_address = " . $address_id;
        
    $res = mysql_query($sql) or dberror($sql);
    
    if ($row = mysql_fetch_assoc($res))
    {
        if($row["num_recs"] > 0)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
}


/*************************************************************************
   check if all items do have an arrival date
**************************************************************************/
function check_if_all_items_hav_arrival_date($oid, $address_id)
{
    
    if($address_id)
    {
        $sql = "select count(order_item_id) as num_recs " .
                       "from order_items " .
                       "where order_item_order = " . $oid .
                       "  and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                       "  and (order_item_arrival is null or order_item_arrival = '0000-00-00') " .
                       "  and (order_item_not_in_budget is null or order_item_not_in_budget = 0) " .
			           "  and order_item_quantity > 0 " . 
                       "  and order_item_forwarder_address = " . $address_id;
    }
    else
    {
        $sql = "select count(order_item_id) as num_recs " .
                       "from order_items " .
                       "where order_item_order = " . $oid .
                       "  and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                       "  and (order_item_arrival is null or order_item_arrival = '0000-00-00') " .
			           "  and order_item_quantity > 0 " . 
                       "  and (order_item_not_in_budget is null or order_item_not_in_budget = 0) ";
    }

    $res = mysql_query($sql) or dberror($sql);
    
    if ($row = mysql_fetch_assoc($res))
    {
        if($row["num_recs"] > 0)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }

}


/*************************************************************************
   check if all items do have an expected arrival date
**************************************************************************/
function check_if_all_items_hav_expected_arrival_date($oid, $address_id)
{
    
    
    //old version before 23.10.05
    //all items have to have a ready for pick up date
    /*
    $sql = "select count(order_item_id) as num_recs " .
                   "from order_items " .
                   "where order_item_order = " . $oid .
                   "  and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                   "  and (order_item_expected_arrival is null or order_item_expected_arrival = '0000-00-00') " .
                   "  and (order_item_not_in_budget is null or order_item_not_in_budget = 0) " .
                   "  and order_item_forwarder_address = " . $address_id;
        
    $res = mysql_query($sql) or dberror($sql);
    
    if ($row = mysql_fetch_assoc($res))
    {
        if($row["num_recs"] > 0)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
    */


    //new version after 22.10.05
    //if there is at least one item haveing a ready for pick up date it can be delivered
    $sql = "select count(order_item_id) as num_recs " .
                   "from order_items " .
                   "where order_item_order = " . $oid .
                   "  and order_item_type <= " . ITEM_TYPE_SPECIAL . 
                   "  and (order_item_expected_arrival is not null and order_item_expected_arrival <> '0000-00-00') " .
                   "  and (order_item_not_in_budget is null or order_item_not_in_budget = 0) " .
                   "  and order_item_forwarder_address = " . $address_id;
        
    $res = mysql_query($sql) or dberror($sql);
    
    if ($row = mysql_fetch_assoc($res))
    {
        if($row["num_recs"] > 0)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
}

/*************************************************************************
   check if all items do have an order date
**************************************************************************/
function check_if_all_items_hav_order_date($oid, $address_id)
{
    
    if($address_id)
    {
        $sql = "select count(order_item_id) as num_recs " .
                       "from order_items " .
                       "where order_item_order = " . $oid .
                       "  and order_item_type <= " . ITEM_TYPE_SPECIAL .
			           "  and order_item_quantity > 0 " .
                       "  and (order_item_ordered is null or order_item_ordered = '0000-00-00') " .
                       "  and (order_item_not_in_budget is null or order_item_not_in_budget = 0) " .
                       "  and order_item_forwarder_address = " . $address_id;
    }
    else
    {
        $sql = "select count(order_item_id) as num_recs " .
                       "from order_items " .
                       "where order_item_order = " . $oid .
                       "  and order_item_type <= " . ITEM_TYPE_SPECIAL .
			           "  and order_item_quantity > 0 " .
                       "  and (order_item_ordered is null or order_item_ordered = '0000-00-00') " .
                       "  and (order_item_not_in_budget is null or order_item_not_in_budget = 0) ";
    }

    $res = mysql_query($sql) or dberror($sql);
    
    if ($row = mysql_fetch_assoc($res))
    {
        if($row["num_recs"] > 0)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
}


/*************************************************************************
   check if there was made a request for delivery for all items
**************************************************************************/
function check_requast_for_deleivery_for_all_items($oid, $order_state_code)
{
    
    // count forwarders in order
    $num_forwarders = 0;

    $sql = "select distinct order_item_forwarder_address as forwarder " .
                       "from order_items " .
                       "where order_item_order = " . $oid .
                       "  and order_item_type <= " . ITEM_TYPE_SPECIAL .
		               "  and order_item_quantity > 0 " .
                       "  and (order_item_not_in_budget is null or order_item_not_in_budget = 0) ";


    $res = mysql_query($sql) or dberror($sql);
    
    while ($row = mysql_fetch_assoc($res))
    {
        $num_forwarders++;
    }


    // count distinct mails made
    $num_mails = 0;
    $sql = "select distinct order_mail_order, order_state_code, order_mail_user ".
           "from order_mails ".
           "left join order_states on order_state_id = order_mail_order_state " .
           "where order_mail_order = " . $oid . " and order_state_code = " . $order_state_code;

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        $num_mails++;
    }


    if ($num_mails >= $num_forwarders)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

/*************************************************************************
   get the role of a user in the context of an order or project
   contains the orders the user has access to because of his role
**************************************************************************/
function get_user_specific_order_list($user_id, $type, $user_roles = array(), $travelling_projects = false)
{
    
    $order_ids = array();
    $condition = "";
    $user_address = 0;
	$project_access_filter = "";
	$can_only_see_projects_of_his_address = false;
	$can_only_see_orders_of_his_address = false;

    // get user's address
    $sql = "select user_address, user_can_only_see_his_projects, user_can_only_see_his_orders, " .    
		   "user_can_only_see_orders_of_his_address, user_can_only_see_projects_of_his_address " . 
		   "from users ".
           "where user_id = " . user_id();

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {            
        $user_address = $row["user_address"];

		$project_access_filter = "";
		if($type == 1 and $row["user_can_only_see_his_projects"] == 1)
		{
			$project_access_filter = " and order_user = " . user_id() . " ";
		}
		elseif($type == 2 and $row["user_can_only_see_his_orders"] == 1)
		{
			$project_access_filter = " and order_user = " . user_id() . " ";
		}

		if($row["user_can_only_see_projects_of_his_address"] == 1)
		{
			$can_only_see_projects_of_his_address = true;
		}

		if($row["user_can_only_see_orders_of_his_address"] == 1)
		{
			$can_only_see_orders_of_his_address = true;
		}
    }


    //get users country
    $user_country = 0;
    $sql = "select address_country from addresses " .
           "where address_id = " . $user_address;

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $user_country = $row["address_country"];
    }


    // check client
    if($can_only_see_projects_of_his_address == true)
	{
		$sql = "select order_id " .
			   "from orders ".
			   "where (order_archive_date is null or order_archive_date = '0000-00-00') and order_client_address = " . $user_address;
	}
	elseif($can_only_see_orders_of_his_address == true)
	{
		$sql = "select order_id " .
			   "from orders ".
			   "where (order_archive_date is null or order_archive_date = '0000-00-00') and order_client_address = " . $user_address;
	}
	elseif(has_access("has_access_to_all_projects_of_his_company"))
	{
		$sql = "select order_id " .
			   "from orders ".
			   "where (order_archive_date is null or order_archive_date = '0000-00-00') and order_client_address = " . $user_address;
	}
	else
	{
		$sql = "select order_id " .
			   "from orders ".
			   "where (order_archive_date is null or order_archive_date = '0000-00-00') and order_user = " . $user_id . 
			   "   and order_client_address = " . $user_address;
	}


	if($project_access_filter)
    {
		$sql .= " " . $project_access_filter;
	}


	
	$res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {            
        $order_ids[] = $row["order_id"];
    }


	

	// retail operator
    $sql = "select order_id " .
           "from orders ".
           "where (order_archive_date is null or order_archive_date = '0000-00-00') and order_retail_operator = " . $user_id;

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {            
        $order_ids[] = $row["order_id"];
    }


    // local agent
    if ($can_only_see_projects_of_his_address == false and has_access("has_access_to_all_projects_of_his_country") AND $type == 1)
    {

		$country_filter = "";
		$tmp = array();
        $sql = "select * from country_access " .
               "where country_access_user = " . user_id();

        $res = mysql_query($sql) or dberror($sql);

		while ($row = mysql_fetch_assoc($res))
        {            
            $tmp[] = $row["country_access_country"];
        }

        if(count($tmp) > 0) {
			
			$country_filter = " and address_country IN (" . implode(",", $tmp) . ") ";

		}
        
		if($project_access_filter and $country_filter)
		{
			$filter = "((order_archive_date is null or order_archive_date = '0000-00-00') " . $project_access_filter . ") ";
			$filter .= " or ((order_archive_date is null or order_archive_date = '0000-00-00') " . $country_filter. ")";
		}
		elseif($project_access_filter)
		{
			$filter = "(order_archive_date is null or order_archive_date = '0000-00-00') " . $project_access_filter;
		}
		elseif($country_filter)
		{
			$filter = "(order_archive_date is null or order_archive_date = '0000-00-00') " . $country_filter;
		}
		else
		{
			$filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and address_country  = " . $user_country;
		}

		
        
        $sql = "select project_order " .
               "from projects ".
               "left join orders on order_id = project_order " .
               "left join addresses on address_id = order_client_address " .
               "where " . $filter;
        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {            
            $order_ids[] = $row["project_order"];
        }

    }
	

    if ($can_only_see_orders_of_his_address == false and has_access("has_access_to_all_orders_of_his_country") AND $type == 2)
    {
		$country_filter = "";
		$tmp = array();
        $sql = "select * from country_access " .
               "where country_access_user = " . user_id();


        $res = mysql_query($sql) or dberror($sql);

		while ($row = mysql_fetch_assoc($res))
        {            
            $tmp[] = $row["country_access_country"];
        }

        if(count($tmp) > 0) {
			$country_filter = " and address_country IN (" . implode(",", $tmp) . ", " . $user_country . ") ";
		}

        
		if($project_access_filter and $country_filter)
		{
			$filter = "((order_archive_date is null or order_archive_date = '0000-00-00') " . $project_access_filter . ") ";
			$filter .= " or ((order_archive_date is null or order_archive_date = '0000-00-00') " . $country_filter. ")";
		}
		elseif($project_access_filter)
		{
			$filter = "(order_archive_date is null or order_archive_date = '0000-00-00') " . $project_access_filter;
		}
		elseif($country_filter)
		{
			$filter = "(order_archive_date is null or order_archive_date = '0000-00-00') " . $country_filter;
		}
		else
		{
			$filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and address_country  = " . $user_country;
		}

		       
        
        $sql = "select order_id " .
               "from orders ".
               "left join addresses on address_id = order_client_address " .
               "where " . $filter;


        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {            
            $order_ids[] = $row["order_id"];
        }
    }

    //orders
    if($type == 2)
    {
        // check supplier or forwarder
        if (has_access("can_view_empty_orders"))
        {
            $sql = "select distinct order_item_order " .
                   "from order_items ".
				   "left join orders on order_id = order_item_order " . 
                   "where (order_archive_date is null or order_archive_date = '0000-00-00') and order_item_supplier_address = " .$user_address . 
                   "   or order_item_forwarder_address = " . $user_address;
        }
        else
        {
            $sql = "select distinct order_item_order " .
                   "from order_items ".
				   "left join orders on order_id = order_item_order " .
                   "where (order_archive_date is null or order_archive_date = '0000-00-00') and (order_item_supplier_address = " .$user_address . 
                   "   or order_item_forwarder_address = " . $user_address . ")" .
                   "   and (order_item_not_in_budget = 0 ".
                   "   or order_item_not_in_budget is null)";
        }


        $res = mysql_query($sql) or dberror($sql);
    
        while ($row = mysql_fetch_assoc($res))
        {            
            $order_ids[] = $row["order_item_order"];
        }
		

    }

    //projects
    if($type == 1)
    {
        // retail coordinator, design contractor, design supervisor
        if(count($user_roles) == 1 and in_array(7, $user_roles)) // design contractor
		{
			$sql = "select project_order " .
				   "from projects ".
				   "left join orders on order_id = project_order " .
				   "where (order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code >= 220 " . 
				   "   and (project_retail_coordinator = " . $user_id . 
				   "   or project_design_contractor = " . $user_id .
				   "   or project_design_supervisor = " . $user_id . 
				   ")";
		}
		else
		{

			$sql = "select project_order " .
				   "from projects ".
				   "left join orders on order_id = project_order " .
				   "where (order_archive_date is null or order_archive_date = '0000-00-00') and project_retail_coordinator = " . $user_id . 
				   "   or project_design_contractor = " . $user_id .
				   "   or project_design_supervisor = " . $user_id;
		}

        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {            
            $order_ids[] = $row["project_order"];
        }

       
        // check supplier or forwarder
        if (has_access("can_view_empty_orders") and has_access("can_view_order_before_booklet_approval"))
        {
            $sql = "select distinct order_item_order " .
                   "from order_items ".
				   "left join orders on order_id = order_item_order " .
                   "where (order_archive_date is null or order_archive_date = '0000-00-00') and order_item_supplier_address = " .$user_address . 
                   "   or order_item_forwarder_address = " . $user_address;
        }
        elseif (has_access("can_view_empty_orders") and !has_access("can_view_order_before_booklet_approval"))
        {
            $sql = "select distinct order_item_order " .
                   "from order_items ".
				   "left join orders on order_id = order_item_order " .
                   "where (order_archive_date is null or order_archive_date = '0000-00-00') and (order_item_supplier_address = " .$user_address .
                   "   or order_item_forwarder_address = " . $user_address . ")".
                   "   and order_item_show_to_suppliers = 1";
        }
        elseif (!has_access("can_view_empty_orders") and has_access("can_view_order_before_booklet_approval"))
        {
            $sql = "select distinct order_item_order " .
                   "from order_items ".
				   "left join orders on order_id = order_item_order " .
                   "where (order_archive_date is null or order_archive_date = '0000-00-00') and (order_item_supplier_address = " .$user_address . 
                   "   or order_item_forwarder_address = " . $user_address . ")" .
                   "   and (order_item_not_in_budget = 0 ".
                   "   or order_item_not_in_budget is null)";
        }
        elseif (!has_access("can_view_empty_orders") and !has_access("can_view_order_before_booklet_approval"))
        {
			$sql = "select distinct order_item_order " .
                   "from order_items " .
				   "left join orders on order_id = order_item_order " .
                   "where (order_archive_date is null or order_archive_date = '0000-00-00') " . 
				   " and (order_item_supplier_address = " .$user_address . 
                   "   or order_item_forwarder_address = " . $user_address . ")" .
                   "   and (order_item_not_in_budget = 0 ".
                   "   or order_item_not_in_budget is null)" .
                   "   and order_item_show_to_suppliers = 1";
        }

        $res = mysql_query($sql) or dberror($sql);
    
        while ($row = mysql_fetch_assoc($res))
        {            
            $order_ids[] = $row["order_item_order"];
        }
    }


	if(in_array(5, $user_roles)) // supplier
	{
		//check if supplier has a request for offer without having items in the list of materials
		$sql = "select task_order from tasks " . 
			   " where task_user = " . dbquote($user_id) . 
			   " and task_order_state in (26, 27, 28) ";
		
		$sql = "select task_order from tasks " . 
			   " where task_user = " . dbquote($user_id);

		$res = mysql_query($sql) or dberror($sql);
    
        while ($row = mysql_fetch_assoc($res))
        {            
            $order_ids[] = $row["task_order"];
        }

	}


	//get projects from attachments only for suppliers
	if(in_array(5, $user_roles) or in_array(29, $user_roles)) {
		$sql_order_addresses = 'select DISTINCT order_file_order from order_files ' . 
						   'left join order_file_addresses on order_file_address_file = order_file_id ' .
						   'where order_file_address_address = ' . $user_address;


		$res = mysql_query($sql_order_addresses) or dberror($sql_order_addresses);
		while ($row = mysql_fetch_assoc($res))
		{
			 $order_ids[] = $row["order_file_order"];
		}
	}

	
    
	/*
	$condition = implode(" or order_id = ", $order_ids);
    
    if($condition != "")
    {
        $condition = "order_id = " . $condition;
    }
	*/


	//user has access to all travelling projects
	if($travelling_projects == true)
	{
		$sql = "select distinct posorder_order " .
			   "from posorderspipeline ".
			   "left join orders on order_id = posorder_order " .
			   "left join posareaspipeline on posarea_posaddress = posorder_posaddress " .
			   "where (order_archive_date is null or order_archive_date = '0000-00-00') " . 
			   " and (posarea_area IN (4,5) or posorder_subclass IN(17)) and posorder_order > 0 and posorder_type = 1";


		$res = mysql_query($sql) or dberror($sql);
    
        while ($row = mysql_fetch_assoc($res))
        {            
            $order_ids[] = $row["posorder_order"];
        }


		$sql = "select distinct posorder_order " .
			   "from posorders ".
			   "left join orders on order_id = posorder_order " .
			   "left join posareas on posarea_posaddress = posorder_posaddress " .
			   "where (order_archive_date is null or order_archive_date = '0000-00-00') " . 
			   " and (posarea_area IN (4,5) or posorder_subclass IN(17)) and posorder_order > 0 and posorder_type = 1";


		$res = mysql_query($sql) or dberror($sql);
    
        while ($row = mysql_fetch_assoc($res))
        {            
            $order_ids[] = $row["posorder_order"];
        }

	}


	//access to project of granted users
	if($type == 1)
	{
	
		$granted_users = array();
		$sql = "select project_access_granted_user, concat(user_name, ' ', user_firstname) as username " . 
			   "from project_access " . 
			   "left join users on user_id = project_access_granted_user " . 
			   "where project_access_owner_user = " . $user_id . 
			   " order by username";
		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res)) 
		{
			$granted_users[] = $row["project_access_granted_user"];
		}

		if(count($granted_users) > 0)
		{
			$tmp = implode(',', $granted_users);
			
			$filter = "(order_archive_date is null or order_archive_date = '0000-00-00') ";
			$filter = $filter . " and order_user in(" . $tmp . ")";
			
			$sql = "select order_id " .
				   "from orders ".
				   "where " . $filter;

			$res = mysql_query($sql) or dberror($sql);
    
			while ($row = mysql_fetch_assoc($res))
			{            
				$order_ids[] = $row["order_id"];
			}
			
		}
	
	}


	if(count($order_ids) > 0)
	{
		$tmp = implode(",", $order_ids);
		$condition = " order_id IN (" .  $tmp . ") ";
	
	
	}

    return $condition;

}



/*************************************************************************
   set a picture for every new comment (introduced 2006-05-18
**************************************************************************/
function set_new_comment_pictures($sql, $oid)
{
    $images = array();

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        $users = explode(",", $row["comment_visited"]);
        
        if($row["date_created"] > '2006-05-18' and !in_array(user_id(), $users))
        {
            $images[$row["comment_id"]] = "/pictures/bullet_ball_glass_red.gif";

            $visited = user_id() . "," . implode(",", $users);
            
            $sql = "Update comments SET " .
                   "comment_visited = " . dbquote($visited) .
                   " where comment_id = " . $row["comment_id"];

            $result = mysql_query($sql) or dberror($sql);
        }
    }

    return $images;

}


/*************************************************************************
   set a picture for every new attachment (introduced 2006-05-18
**************************************************************************/
function set_new_attachment_pictures($sql, $oid)
{
    $images = array();

    $res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        $users = explode(",", $row["order_file_visited"]);
        
        if($row["date_created"] > '2006-05-18' and !in_array(user_id(), $users))
        {
            $images[$row["order_file_id"]] = "/pictures/bullet_ball_glass_red.gif";
        
            $visited = user_id() . "," . implode(",", $users);
            
            $sql = "Update order_files SET " .
                   "order_file_visited = " . dbquote($visited) .
                   " where order_file_id = " . $row["order_file_id"];

            $result = mysql_query($sql) or dberror($sql);
        }
    }

    return $images;

}


/********************************************************************
    get cer currency
*********************************************************************/
function get_cer_currency($pid)
{
    $currency = array();
    
	$sql = "select * from cer_basicdata " . 
		   "left join currencies on currency_id = cer_basicdata_currency " . 
		   "where cer_basicdata_project = " . $pid;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$currency["id"] = $row["cer_basicdata_currency"];
		$currency["symbol"] = $row["currency_symbol"];
		$currency["exchange_rate"] = $row["cer_basicdata_exchangerate"];
		$currency["factor"] = $row["cer_basicdata_factor"];
	}

    else
    {
        $currency["id"] = 0;
        $currency["symbol"] = "";
        $currency["exchange_rate"] = 0;
        $currency["factor"] = 0;
    }
    
    return $currency;
}


/********************************************************************
    get intangibles
*********************************************************************/
function get_pos_intangibles($project_id, $investment_type)
{
	$posintangibles = array();
	
	$sql = "select * from cer_investments " .
		   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
		   "where cer_investment_cer_version = 0 and cer_investment_project = " . $project_id .
		   " and cer_investment_type = $investment_type ";

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$posintangibles = $row;
	}
	return $posintangibles;
}


/********************************************************************
    check if cer_summary exists
*********************************************************************/
function build_missing_cer_records($pid)
{
	$sql = "select count(cer_summary_id) as num_recs " . 
       "from cer_summary " . 
	   "where cer_summary_project = " . $pid;

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	if ($row["num_recs"] == 0)
	{
		//create cer_revenue records
	
		$fields = array();
		$values = array();

		$fields[] = "cer_summary_project";
		$values[] = param("pid");

	
		$fields[] = "date_created";
		$values[] = "now()";

		$fields[] = "date_modified";
		$values[] = "now()";

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());

		$sql = "insert into cer_summary (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
		
		mysql_query($sql) or dberror($sql);

	}

	// cer basic data
	$currency_id = 0;
	$exchange_rate = 0;
	$factor = 0;
	$country_id = 0;
	$order_state = 0;

	$sql = "select currency_id, currency_exchange_rate, currency_factor, country_id, " .
		   "order_actual_order_state_code " . 
		   "from projects " . 
		   "left join orders on order_id = project_order " . 
		   "left join addresses on address_id = order_client_address " .
		   "left join countries on country_id = address_country " . 
		   "left join currencies on currency_id = country_currency " . 
		   "where project_id = " . $pid;

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$currency_id = $row["currency_id"];
		$exchange_rate = $row["currency_exchange_rate"];
		$factor = $row["currency_factor"];
		$country_id = $row["country_id"];
		$order_state = $row["order_actual_order_state_code"];
	}


	//get currency data
	$currency_id2 = 0;
	$exchange_rate2 = 0;
	$factor2 = 0;

	$sql = "select currency_id, currency_exchange_rate, currency_factor " . 
		   "from projects " . 
		   "left join orders on order_id = project_order " . 
		   "left join countries on country_id = order_shop_address_country " . 
		   "left join currencies on currency_id = country_currency " . 
		   "where project_id = " . $pid;

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$currency_id2 = $row["currency_id"];
		$exchange_rate2 = $row["currency_exchange_rate"];
		$factor2 = $row["currency_factor"];
	}

	
	//update cer_basic_data
	$sql = "select count(cer_basicdata_id) as num_recs " . 
			"from cer_basicdata " . 
			"where cer_basicdata_project = " . $pid;

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	if ($row["num_recs"] == 0)
	{
		
		$fields = array();
		$values = array();

		$fields[] = "cer_basicdata_project";
		$values[] = param("pid");

		$fields[] = "cer_basicdata_currency";
		$values[] = dbquote($currency_id);

		$fields[] = "cer_basicdata_factor";
		$values[] = dbquote($factor);


		$fields[] = "cer_basicdata_currency2";
		$values[] = dbquote($currency_id2);

		$fields[] = "cer_basicdata_factor2";
		$values[] = dbquote($factor2);


		$fields[] = "date_created";
		$values[] = "now()";

		$fields[] = "date_modified";
		$values[] = "now()";

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());

		$sql = "insert into cer_basicdata (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
		
		mysql_query($sql) or dberror($sql);

	}


	$sql = "select * from posinvestment_types where posinvestment_type_active = 1";
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$sql_i = "select cer_investment_id from cer_investments " .
				 "where cer_investment_project = " .  $pid . 
				 " and cer_investment_type = " . $row["posinvestment_type_id"];

		$res_i = mysql_query($sql_i) or dberror($sql_i);
		if (!$row_i = mysql_fetch_assoc($res_i))
		{
			$sql = "insert into cer_investments (" .
				   "cer_investment_project, " .
				   "cer_investment_type, " .
				   "user_created, date_created) values (".
				   param("pid") . ", " .
				   $row["posinvestment_type_id"] . ", " . 
				   dbquote(user_login()) . ", " . 
				   "current_timestamp)";

			$result = mysql_query($sql) or dberror($sql);
		}
	}
	
	return true;

}


/********************************************************************
    get investments from the list of materials
*********************************************************************/
function update_investments_from_the_list_of_materials($project_id, $order_id, $update_mode)
{
	$amounts = array();
	$comments = array();

	//get the local currency of shop
	$currency = get_cer_currency($project_id);

	$sql = "select * from cer_investments " .
		   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
		   "where cer_investment_project = " . $project_id;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		if($row["posinvestment_type_cms_sumcostgroups"])
		{
			$group_total = 0;
			$cost_groups = explode(",", $row["posinvestment_type_cms_sumcostgroups"]);

			foreach($cost_groups as $key=>$cost_group)
			{
				$sql_i = "select sum(order_item_system_price*order_item_quantity) as total_in_system_currency " . 
						 "from order_items ".
						 "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
						 "   and order_item_order = " . $order_id . 
						 "   and order_item_cost_group =  " . dbquote($cost_group);

				$res_i = mysql_query($sql_i) or dberror($sql_i);
				if ($row_i = mysql_fetch_assoc($res_i))
				{
					if($currency["exchange_rate"] > 0)
					{
						$group_total = $group_total + ($row_i["total_in_system_currency"] / $currency["exchange_rate"])*$currency["factor"];
					}
				}

				if($update_mode == 1)
				{
					$amounts[$row["cer_investment_id"]] = $group_total;
					$comments[$row["cer_investment_id"]] = "inserted from the list of materials";
				}
				else
				{
					if($row["cer_investment_amount_cer_loc"] == 0)
					{
						$amounts[$row["cer_investment_id"]] = $group_total;
						$comments[$row["cer_investment_id"]] = "inserted from the list of materials";
					}
				}
			}
		}
	}
	
	foreach($amounts as $id=>$value)
	{
		if($value)
		{
			$fields = array();
		
			$value = dbquote($value);
			$fields[] = "cer_investment_amount_cer_loc = " . $value;

			$value = dbquote($comments[$id]);
			$fields[] = "cer_investment_comment = " . $value;

			$value1 = "current_timestamp";
			$project_fields[] = "date_modified = " . $value1;

			if (isset($_SESSION["user_login"]))
			{
				$value1 = $_SESSION["user_login"];
				$project_fields[] = "user_modified = " . dbquote($value1);
			}
	   
			$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $id;

			mysql_query($sql) or dberror($sql);
		}

	}

	/*
	//update kl approved investments
	$sql = "update cer_investments set cer_investment_amount_cer_loc_approved = cer_investment_amount_cer_loc " . 
			"where cer_investment_amount_cer_loc_approved = 0 and cer_investment_type in(1, 3, 5, 7, 11) and cer_investment_project = " . $project_id;

	$result = mysql_query($sql) or dberror($sql);
	*/

}


/********************************************************************
    get prroved investments from the list of materials
*********************************************************************/
function update_approved_investments_from_the_list_of_materials($project_id, $order_id, $update_mode)
{
	$amounts = array();
	$comments = array();

	//get the local currency of shop
	$currency = get_cer_currency($project_id);

	$sql = "select * from cer_investments " .
		   "left join posinvestment_types on posinvestment_type_id = cer_investment_type " . 
		   "where cer_investment_project = " . $project_id;

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		
		if($row["posinvestment_type_cms_sumcostgroups"])
		{
			$group_total = 0;
			$cost_groups = explode(",", $row["posinvestment_type_cms_sumcostgroups"]);

			foreach($cost_groups as $key=>$cost_group)
			{
				$sql_i = "select sum(order_item_system_price_freezed*order_item_quantity_freezed) as total_in_system_currency " . 
						 "from order_items ".
						 "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
						 "   and order_item_order = " . $order_id . 
						 "   and order_item_cost_group =  " . dbquote($cost_group);

				$res_i = mysql_query($sql_i) or dberror($sql_i);
				if ($row_i = mysql_fetch_assoc($res_i))
				{
					if($currency["exchange_rate"] > 0)
					{
						$group_total = $group_total + ($row_i["total_in_system_currency"] / $currency["exchange_rate"])*$currency["factor"];
					}
				}

				if($update_mode == 1)
				{
					$amounts[$row["cer_investment_id"]] = $group_total;
					$comments[$row["cer_investment_id"]] = "inserted from the list of materials";
				}
				else
				{
					if($row["cer_investment_amount_cer_loc"] == 0)
					{
						$amounts[$row["cer_investment_id"]] = $group_total;
						$comments[$row["cer_investment_id"]] = "inserted from the list of materials";
					}
				}
			}
		}
	}
	
	foreach($amounts as $id=>$value)
	{
		$fields = array();
    
		$value = dbquote($value);
		$fields[] = "cer_investment_amount_cer_loc = " . $value;

		$value = dbquote($comments[$id]);
		$fields[] = "cer_investment_comment = " . $value;

		$value1 = "current_timestamp";
		$project_fields[] = "date_modified = " . $value1;

		if (isset($_SESSION["user_login"]))
		{
			$value1 = $_SESSION["user_login"];
			$project_fields[] = "user_modified = " . dbquote($value1);
		}
   
		$sql = "update cer_investments set " . join(", ", $fields) . " where cer_investment_id = " . $id;
		mysql_query($sql) or dberror($sql);

	}


	//update kl approved investments
	/*
	$sql = "update cer_investments set cer_investment_amount_cer_loc_approved = cer_investment_amount_cer_loc " . 
			"where cer_investment_amount_cer_loc_approved = 0 and cer_investment_type in(1, 3, 5, 7, 11) and cer_investment_project = " . $project_id;

	$result = mysql_query($sql) or dberror($sql);
    */
}


/********************************************************************
    get_province_name
*********************************************************************/
function get_province_name($place_id)
{
	$province_name = "";

	$sql = "select province_canton " . 
	       "from places " .
		   "left join provinces on province_id = place_province " . 
		   "where place_id = " . dbquote($place_id);

	$res = mysql_query($sql) or dberror($sql);
	
	if($row = mysql_fetch_assoc($res))
	{
		$province_name = $row["province_canton"];
	}

	return $province_name;
}


/********************************************************************
    get pos data
*********************************************************************/
function get_pos_data($order_id)
{
	$posdata = array();
	$project_is_in_pipeline = 0;

	//check if project is in pipeline
	$sql = "select count(posorder_id) as num_recs " . 
		   "from posorderspipeline " . 
	       "where posorder_order = " . dbquote($order_id);

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	if ($row["num_recs"] > 0)
	{
		$project_is_in_pipeline = 1;
	}

	if($project_is_in_pipeline == 1)
	{
		
		$parent_table = "";
		$sql = "select * from posorderspipeline " . 
			   "where posorder_order = " . dbquote($order_id);


		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$parent_table = $row["posorder_parent_table"];
		}
		
		$sql = "select * from posorderspipeline " . 
			   "left join " . $parent_table . " on posaddress_id = posorder_posaddress " . 
			   "where posorder_order = " . dbquote($order_id);

		
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			
			/*
			if(!$row["posaddress_id"])
			{
				$sql = "select * from posorderspipeline " . 
					   "left join posaddresses on posaddress_id = posorder_posaddress " . 
					   "where posorder_order = " . $order_id;
				$res = mysql_query($sql) or dberror($sql);

				if ($row = mysql_fetch_assoc($res))
				{
					$posdata = $row;
					$posdata["table"] = "posaddresses";
					$posdata["table_leases"] = "posleasepipeline";
				}
			}
			else
			{
				$posdata = $row;
				$posdata["table"] = "posaddressespipeline";
				$posdata["table_leases"] = "posleasepipeline";
			}
			*/

			$posdata = $row;
			$posdata["table"] = $parent_table;
			$posdata["table_leases"] = "posleasepipeline";
			
		}
		
		if(count($posdata) > 0)
		{

			$areas = "";
			$sql = "select * from posareaspipeline " .
				   "left join posareatypes on posareatype_id = posarea_area " . 
				   "where posarea_posaddress = " . dbquote($posdata["posaddress_id"]);
			
			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{
				$areas .= $row["posareatype_name"] . ", ";
			}
			
			$areas = substr($areas, 0, strlen($areas)-2);
			$posdata["posareas"] = $areas;
		}
	}
	else
	{
		$sql = "select * from posorders " . 
			   "left join posaddresses on posaddress_id = posorder_posaddress " . 
			   "where posorder_order = " . dbquote($order_id);
		
		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$posdata = $row;
			$posdata["table"] = "posaddresses";
			$posdata["table_leases"] = "posleases";
		}

		if(count($posdata) > 0)
		{
			$areas = "";
			$sql = "select * from posareas " .
				   "left join posareatypes on posareatype_id = posarea_area " . 
				   "where posarea_posaddress = " . dbquote($posdata["posaddress_id"]);
			
			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{
				$areas .= $row["posareatype_name"] . ", ";
			}

			$areas = substr($areas, 0, strlen($areas)-2);

			$posdata["posareas"] = $areas;
		}

	}
	
	if(count($posdata) > 0)
	{
		$sql = "select country_id, country_name ".
			   "from countries ".
			   "where country_id = " . dbquote($posdata["posaddress_country"]);

		$res = mysql_query($sql);
		if ($res)
		{
			$row = mysql_fetch_assoc($res);
			$posdata["country_name"] = $row['country_name'];
		}


		$sql = "select place_province ".
			   "from places ".
			   "where place_id = " . dbquote($posdata["posaddress_place_id"]);

		$res = mysql_query($sql);
		if ($res)
		{
			$row = mysql_fetch_assoc($res);
			$posdata["place_province"] = $row['place_province'];
		}
	}

	return $posdata;
}

/********************************************************************
    get pos lease data
*********************************************************************/
function get_pos_leasedata($posaddress_id, $project_order = 0 )
{
	$posleasedata = array();
	$project_is_in_pipeline = 0;

	//check if project is in pipeline
	$sql = "select count(posorder_id) as num_recs " . 
		   "from posorderspipeline " . 
	       "where posorder_order = " . dbquote($project_order);

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	if ($row["num_recs"] > 0)
	{
		$project_is_in_pipeline = 1;
	}

	if($project_is_in_pipeline == 1)
	{
		$sql = "select * from posleasespipeline " . 
			   " left join poslease_types on poslease_type_id = poslease_lease_type " .
			   " where poslease_posaddress = " . $posaddress_id .
			   " and poslease_order = " . $project_order .
			   " order by poslease_startdate DESC ";

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$posleasedata = $row;
			$posleasedata["table"] = "posleasespipeline";
		}
	}
	else
	{
		$sql = "select * from posleases " .
			   "left join poslease_types on poslease_type_id = poslease_lease_type " .
			   "where poslease_posaddress = " . $posaddress_id .
			   " and poslease_order = " . $project_order .
			   " order by poslease_startdate DESC ";

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$posleasedata = $row;
			$posleasedata["table"] = "posleases";
		}
	
	}
	
	return $posleasedata;

}

/********************************************************************
    get restrictions for file categoreis
*********************************************************************/
function get_file_category_restirctions($user_roles)
{
	$restrictions = array();

	$user_has_restrictions = true;

	foreach($user_roles as $key=>$user_role)
	{
		$sql = "select count(role_file_category_file_category_id) as num_recs ". 
			   "from role_file_categories " . 
			   " where role_file_category_role_id = " . $user_role;
		
		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		
		if($row["num_recs"] == 0)
		{
			$user_has_restrictions = false;
		}
		
	}

	if($user_has_restrictions == true and count($user_roles) > 0)
	{
		$sql = "select role_file_category_file_category_id ". 
			   "from role_file_categories " . 
			   " where role_file_category_role_id IN (" . implode(',', $user_roles) . ") ";
		
		$res = mysql_query($sql) or dberror($sql);

		while ($row = mysql_fetch_assoc($res))
		{
			$restrictions[] = $row["role_file_category_file_category_id"];
		}
				   
	}


	return $restrictions;
}


/********************************************************************
    get notification recipients on cancellation of a project
*********************************************************************/
function get_recipients_on_cancellation($project_id)
{

	$creps = array();

	$user_ids = array();
	$user_roles = array();
	$roles = array();

	$order_id = 0;
    $product_line = 0;
	$postype = 0;
	

	//get_role_names
	$sql = "select role_id, role_name from roles where role_application = 'retailnet'";
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$roles[$row["role_id"]] = $row["role_name"];
	}



	//get all project partners
	$sql = "select order_id, project_retail_coordinator, project_hq_project_manager, project_local_retail_coordinator, " . 
		   "project_design_contractor, project_design_supervisor, order_retail_operator, order_user, " . 
		   "project_product_line, project_postype " .
		   "from projects " . 
		   "left join orders on order_id = project_order " . 
		   "where project_id = " . dbquote($project_id);


	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$order_id = $row["order_id"];
		$product_line = $row["project_product_line"];
		$postype = $row["project_postype"];



		if($row["project_retail_coordinator"])
		{
			$user_ids[$row["project_retail_coordinator"]] = $row["project_retail_coordinator"];
			$user_roles[$row["project_retail_coordinator"]] = 3;
		}
		
		/*
		if($row["project_hq_project_manager"])
		{
			$user_ids[$row["project_hq_project_manager"]] = $row["project_hq_project_manager"];
			$user_roles[$row["project_hq_project_manager"]] = 19;
		}
		*/

		if($row["project_local_retail_coordinator"])
		{
			$user_ids[$row["project_local_retail_coordinator"]] = $row["project_local_retail_coordinator"];
			$user_roles[$row["project_local_retail_coordinator"]] = 10;
		}

		if($row["project_design_contractor"])
		{
			$user_ids[$row["project_design_contractor"]] = $row["project_design_contractor"];
			$user_roles[$row["project_design_contractor"]] = 7;
		}

		if($row["project_design_supervisor"])
		{
			$user_ids[$row["project_design_supervisor"]] = $row["project_design_supervisor"];
			$user_roles[$row["project_design_supervisor"]] = 8;
		}

		/*
		if($row["order_retail_operator"])
		{
			$user_ids[$row["order_retail_operator"]] = $row["order_retail_operator"];
			$user_roles[$row["order_retail_operator"]] = 2;
		}
		*/

		if($row["order_user"])
		{
			$user_ids[$row["order_user"]] = $row["order_user"];
			$user_roles[$row["order_user"]] = 4;
		}

		
		//suppliers
		$sql = "select distinct user_id " . 
			   "from order_items " . 
			   "left join users on user_address = order_item_supplier_address " . 
			   " where order_item_supplier_address > 0 and user_active = 1 and order_item_order = " . $order_id;
		
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			if($row["user_id"])
			{
				$user_ids[$row["user_id"]] = $row["user_id"];
				$user_roles[$row["user_id"]] = 5;
			}
		}


		//forwarders
		$sql = "select distinct user_id " . 
			   "from order_items " . 
			   "left join users on user_address = order_item_forwarder_address " . 
			   " where order_item_supplier_address > 0 and user_active = 1 and order_item_order = " . $order_id;
		
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
			if($row["user_id"])
			{
				$user_ids[$row["user_id"]] = $row["user_id"];
				$user_roles[$row["user_id"]] = 6;
			}
		}
	}


	//select standard recipients
	if($product_line > 0) 
	{
		$sql = "select postype_notification_email2 from postype_notifications " .
			   "where postype_notification_postype = " . $postype . 
			   "  and postype_notification_prodcut_line = " . $product_line;
	}
	else
	{
		$sql = "select DISTINCT postype_notification_email2 from postype_notifications " .
			   "where postype_notification_postype = " . $postype;
	}

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		if($row["postype_notification_email2"])
		{
			
			$sql_u = "select user_id from users " .
					 "where user_email = '" . $row["postype_notification_email2"] .  "'";
			$res_u = mysql_query($sql_u) or dberror($sql_u);
			if($row_u = mysql_fetch_assoc($res_u))
			{
				$user_ids[$row_u["user_id"]] = $row_u["user_id"];
				$user_roles[$row_u["user_id"]] = 0;
			}
		}
	}



	$sql = "select user_id, user_name, user_firstname, address_company " . 
		   "from users " . 
		   "left join addresses on address_id = user_address " . 
		   "where user_id IN (" . implode(',', $user_ids) . ")"  . 
		   " order by address_company, user_name";

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$full_name = $row["address_company"] . ", " . $row["user_name"] . " " . $row["user_firstname"];
		
		if($user_roles[$row["user_id"]] == 0)
		{
			$creps[$row["user_id"]] = array('user_id'=>$row["user_id"], 'full_name'=>$full_name, 'role_name'=>'Standard Recipient');
		}
		else
		{
			$creps[$row["user_id"]] = array('user_id'=>$row["user_id"], 'full_name'=>$full_name, 'role_name'=>$roles[$user_roles[$row["user_id"]]]);
		}
	}



	return $creps;

}


/********************************************************************
    get POS Info of relocated POS
*********************************************************************/
function get_relocated_pos_info($pos_id)
{
	$pod_data = array();

	$sql = "select posaddress_id, posaddress_name, posaddress_zip, place_name, country_name,
	        posaddress_store_postype, posaddress_store_furniture, posaddress_store_furniture_subclass,
			posaddress_store_subclass, posaddress_franchisee_id " .
		   "from posaddresses " . 
		   "left join places on place_id = posaddress_place_id " .
		   "left join countries on country_id = posaddress_country " .
		   "where posaddress_id = " . $pos_id;

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$pos_data = $row;
	}

	return $pos_data;
}


/********************************************************************
    get POS Info of relocated POS
*********************************************************************/
function get_renovated_pos_info($order_id)
{
	$pos_id = 0;

	$sql = "select posaddress_id " .
		   "from posorders " .
		   "left join posaddresses on posaddress_id = posorder_posaddress " . 
		   "where posorder_order = " . dbquote($order_id);

	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$pos_id = $row["posaddress_id"];
	}

	return $pos_id;
}


/********************************************************************
    get delivery addresses for all order_items
*********************************************************************/
function get_all_order_item_delivery_addresses($order_id)
{
	$delivery_addresses = array();

    $sql = "select * " .
		   "from order_items " .
           "left join order_addresses on order_address_order_item = order_item_id " .
		   "left join places on place_id = order_address_place_id " . 
           "where order_address_order  = " . $order_id . 
           "    and order_address_type = 2";

	$res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
        $order_address = array();
		$delivery_address = array();

		$order_address["order_item_id"] = $row["order_item_id"];
		$order_address["order_address_id"] = $row["order_address_id"];
        $delivery_address["company"] = $row["order_address_company"];
        $delivery_address["company2"] = $row["order_address_company2"];
        $delivery_address["address"] = $row["order_address_address"];
        $delivery_address["address2"] = $row["order_address_address2"];
        $delivery_address["zip"] = $row["order_address_zip"];
        $delivery_address["place"] = $row["order_address_place"];
		$delivery_address["place_id"] = $row["order_address_place_id"];
		$delivery_address["province_id"] = $row["place_province"];
        $delivery_address["country"] = $row["order_address_country"];
        $delivery_address["phone"] = $row["order_address_phone"];
        $delivery_address["mobile_phone"] = $row["order_address_mobile_phone"];
        $delivery_address["email"] = $row["order_address_email"];
        $delivery_address["contact"] = $row["order_address_contact"];

		$order_address["delivery_address"] = $delivery_address;

		$delivery_addresses[$row["order_item_id"]] = $order_address;
    }   

    return $delivery_addresses;
}


/********************************************************************
    getproject milestone
*********************************************************************/
function get_project_milestone($project_id, $milestone_id)
{
    $milestone = array();
    
	$sql = "select * from project_milestones " .
		   " left join users on user_id = project_milestone_date_enterd_by_user ".
		   "where project_milestone_project = " . $project_id .
		   " and project_milestone_milestone = " . $milestone_id;

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$milestone = $row;
	}
    
    return $milestone;
}



/********************************************************************
    get supplier's currency
*********************************************************************/
function get_suplier_currencies($supplier_id = 0)
{
    $currencies = array();
    
	$sql = "select address_currency, address_currency2 " . 
		   "from addresses " . 
		   "where address_id = " . dbquote($supplier_id);

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$currencies[] = $row["address_currency"];
		if( $row["address_currency2"] > 0)
		{
			$currencies[] = $row["address_currency2"];
		}
	}
    
    return $currencies;
}


/********************************************************************
    check if delivery address is entered for the project's items
*********************************************************************/
function check_if_delivery_address_is_entered($order_id = 0)
{
	//check if all items have a preferred arrival date

	$sql = "select count(order_item_id) as num_recs " . 
		   " from order_items " .
		   " where order_item_order = " . $order_id .
		   " and order_item_supplier_address > 0 " . 
		   " and (order_item_preferred_arrival_date is NULL or order_item_preferred_arrival_date = '0000-00-00') ";
	
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	if($row["num_recs"] > 0)
	{
		return false;
	}
	
	$sql = "select order_address_company, order_address_place_id " . 
		   " from order_addresses " . 
		   " where order_address_order = " . $order_id .
		   " and (order_address_order_item = 0 or order_address_order_item is null) " . 
		   " and order_address_type = 2";

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		if($row["order_address_place_id"] > 0)
		{
			return true;
		}
	}
	return false;
}


/********************************************************************
    create CER Version
*********************************************************************/
function create_cer_version($project_id, $context = "", $project_pipeline = 0, $ln_basicdata_id = 0)
{

	$grosssqms = 0;
	$totalsqms = 0;
	$retailsqms = 0;
	$backofficesqms = 0;
	

	//get surfaces
	$sql_c = "select project_cost_grosssqms, project_cost_totalsqms, project_cost_sqms, project_cost_backofficesqms " . 
		   "from projects " .
		   " left join project_costs on project_cost_order = project_order " . 
		   " where project_id = " . dbquote($project_id);
	
	$res_c = mysql_query($sql_c) or dberror($sql_c);
	if ($row_c = mysql_fetch_assoc($res_c))
	{
		//$grosssqms = $row_c["project_cost_grosssqms"];
		$grosssqms = $row_c["project_cost_totalsqms"];
		$totalsqms = $row_c["project_cost_totalsqms"];
		$retailsqms = $row_c["project_cost_sqms"];
		$backofficesqms = $row_c["project_cost_backofficesqms"];
	}
	
	
	
	//version 0 is always the actual version in use
	//versions higher than 0 are archived versions from earlier time
	
	$new_cer_version = 0;

	$sql = "select cer_basicdata_id, cer_basicdata_version " . 
		   "from cer_basicdata " . 
		   "where cer_basicdata_project = " . dbquote($project_id) . 
		   "order by cer_basicdata_version DESC";
	
	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$new_version = (int)$row["cer_basicdata_version"] + 1;
		$new_cer_version = $new_version;
		
		
		//create archive version of CER havin version 0

		
		//basic data
		$sql = "select * from cer_basicdata " . 
			   "where cer_basicdata_version = 0 ". 
			   " and cer_basicdata_project = " . dbquote($project_id);

		
		$res = mysql_query($sql) or dberror($sql);	   
		$row = mysql_fetch_assoc($res);


		$fields = 'cer_basicdata_version,';
		$values = $new_version . ',';

		$fields .= 'cer_basicdata_version_context,';
		$values .= dbquote($context) . ',';

		$fields .= 'cer_basicdata_version_user_id,';
		$values .= user_id() . ',';

		foreach($row as $field_name=>$value)
		{
			if($field_name != 'cer_basicdata_id' 
				and $field_name != 'cer_basicdata_version'
			    and $field_name != 'cer_basicdata_version_context'
				and $field_name != 'cer_basicdata_version_user_id'
				and $field_name != 'cer_basicdata_version_grosssqms'
				and $field_name != 'cer_basicdata_version_totalsqms'
				and $field_name != 'cer_basicdata_version_sqms'
				and $field_name != 'cer_basicdata_version_backofficesqms'
				and $field_name != 'user_created'
				and $field_name != 'date_created'
				and $field_name != 'user_modified'
				and $field_name != 'date_modified'
			) {
				$fields .= $field_name . ',';
				$values .= dbquote($value) . ',';
			}
			
		}


		$fields .= 'cer_basicdata_version_grosssqms,';
		$values .= $grosssqms . ',';

		$fields .= 'cer_basicdata_version_totalsqms,';
		$values .= $totalsqms . ',';

		$fields .= 'cer_basicdata_version_sqms,';
		$values .= $retailsqms . ',';

		$fields .= 'cer_basicdata_version_backofficesqms,';
		$values .= $backofficesqms . ',';

		$fields .= 'user_created,';
		$values .= dbquote(user_login()) . ',';

		$fields .= 'date_created,';
		$values .= dbquote(date("Y-m-d H:i:s")) . ',';

		$fields = substr($fields, 0, strlen($fields)-1);
		$values = substr($values, 0, strlen($values)-1);

		$sql_i = 'insert into cer_basicdata (' . $fields . ') VALUES (' . $values . ')';
		$result = mysql_query($sql_i) or dberror($sql_i);

		$new_cer_id = mysql_insert_id();



		//cer summary
		$sql = "select * from cer_summary " . 
			   "where cer_summary_cer_version = 0 ". 
			   " and cer_summary_project = " . dbquote($project_id);

		
		$res = mysql_query($sql) or dberror($sql);	   
		$row = mysql_fetch_assoc($res);


		$fields = 'cer_summary_cer_version,';
		$values = $new_version . ',';


		foreach($row as $field_name=>$value)
		{
			if($field_name != 'cer_summary_id' 
				and $field_name != 'cer_summary_cer_version'
				and $field_name != 'user_created'
				and $field_name != 'date_created'
				and $field_name != 'user_modified'
				and $field_name != 'date_modified'
			) {
				$fields .= $field_name . ',';
				$values .= dbquote($value) . ',';
			}
			
		}

		$fields .= 'user_created,';
		$values .= dbquote(user_login()) . ',';

		$fields .= 'date_created,';
		$values .= dbquote(date("Y-m-d H:i:s")) . ',';

		$fields = substr($fields, 0, strlen($fields)-1);
		$values = substr($values, 0, strlen($values)-1);

		$sql_i = 'insert into cer_summary (' . $fields . ') VALUES (' . $values . ')';
		$result = mysql_query($sql_i) or dberror($sql_i);


		//update cer data with ln_data in case the context is ln
		if($context == "ln")
		{
			$ln_basicdata = get_ln_basicdata($project_id);
			
			$fields = array();

			$value = dbquote($ln_basicdata["ln_basicdata_currency"]);
			$fields[] = "cer_basicdata_currency = " . $value;

			$value = dbquote($ln_basicdata["ln_basicdata_exchangerate"]);
			$fields[] = "cer_basicdata_exchangerate = " . $value;

			$value = dbquote($ln_basicdata["ln_basicdata_factor"]);
			$fields[] = "cer_basicdata_factor = " . $value;

			
			$sql = "update cer_basicdata set " . join(", ", $fields) . " where cer_basicdata_id = " .$new_cer_id;
			mysql_query($sql) or dberror($sql);
		}


		//expenses
		$sql = 'Select * from cer_expenses ' . 
			   'where cer_expense_project =' . dbquote($project_id) . 
			   ' and cer_expense_cer_version = 0';


		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = 'cer_expense_cer_version,';
			$values = $new_version . ',';
			foreach($row as $field_name=>$value)
			{
				if($field_name != 'cer_expense_id' 
					and $field_name != 'cer_expense_cer_version'
				    and $field_name != 'user_created'
					and $field_name != 'date_created'
					and $field_name != 'user_modified'
					and $field_name != 'date_modified') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields .= 'user_created,';
			$values .= dbquote(user_login()) . ',';

			$fields .= 'date_created,';
			$values .= dbquote(date("Y-m-d H:i:s")) . ',';

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_expenses (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//new additional rental costs
		$sql = 'Select * from cer_additional_rental_costs ' . 
			   'where cer_additional_rental_cost_project =' . dbquote($project_id) . 
			   ' and cer_additional_rental_cost_cer_version = 0';


		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = 'cer_additional_rental_cost_cer_version,';
			$values = $new_version . ',';
			foreach($row as $field_name=>$value)
			{
				if($field_name != 'cer_additional_rental_cost_id' 
					and $field_name != 'cer_additional_rental_cost_cer_version'
				    and $field_name != 'user_created'
					and $field_name != 'date_created'
					and $field_name != 'user_modified'
					and $field_name != 'date_modified') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields .= 'user_created,';
			$values .= dbquote(user_login()) . ',';

			$fields .= 'date_created,';
			$values .= dbquote(date("Y-m-d H:i:s")) . ',';

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_additional_rental_costs (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		$sql = 'Select * from cer_additional_rental_cost_amounts ' . 
			   'where cer_additional_rental_cost_amount_project =' . dbquote($project_id) . 
			   ' and cer_additional_rental_cost_amount_version = 0';


		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = 'cer_additional_rental_cost_amount_version,';
			$values = $new_version . ',';
			foreach($row as $field_name=>$value)
			{
				if($field_name != 'cer_additional_rental_cost_amount_id' 
					and $field_name != 'cer_additional_rental_cost_amount_version'
				    and $field_name != 'user_created'
					and $field_name != 'date_created'
					and $field_name != 'user_modified'
					and $field_name != 'date_modified') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields .= 'user_created,';
			$values .= dbquote(user_login()) . ',';

			$fields .= 'date_created,';
			$values .= dbquote(date("Y-m-d H:i:s")) . ',';

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_additional_rental_cost_amounts (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}

		//end new additional rental costs

		//turnover based rentals
		$sql = 'Select * from cer_rent_percent_from_sales ' . 
			   'where cer_rent_percent_from_sale_project =' . dbquote($project_id) . 
			   ' and cer_rent_percent_from_sale_cer_version = 0';
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = 'cer_rent_percent_from_sale_cer_version,';
			$values = $new_version . ',';
			foreach($row as $field_name=>$value)
			{
				if($field_name != 'cer_rent_percent_from_sale_id' 
					and $field_name != 'cer_rent_percent_from_sale_cer_version'
				    and $field_name != 'user_created'
					and $field_name != 'date_created'
					and $field_name != 'user_modified'
					and $field_name != 'date_modified') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields .= 'user_created,';
			$values .= dbquote(user_login()) . ',';

			$fields .= 'date_created,';
			$values .= dbquote(date("Y-m-d H:i:s")) . ',';
			
			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_rent_percent_from_sales (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}



		//fixed rents
		$sql = 'Select * from cer_fixed_rents ' . 
			   'where cer_fixed_rent_project_id =' . dbquote($project_id) . 
			   ' and cer_fixed_rent_cer_version = 0';
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = 'cer_fixed_rent_cer_version,';
			$values = $new_version . ',';
			foreach($row as $field_name=>$value)
			{
				if($field_name != 'cer_fixed_rent_id' 
					and $field_name != 'cer_fixed_rent_cer_version'
				    and $field_name != 'user_created'
					and $field_name != 'date_created'
					and $field_name != 'user_modified'
					and $field_name != 'date_modified') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields .= 'user_created,';
			$values .= dbquote(user_login()) . ',';

			$fields .= 'date_created,';
			$values .= dbquote(date("Y-m-d H:i:s")) . ',';
			
			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_fixed_rents (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//investments
		$sql = 'Select * from cer_investments ' . 
			   'where cer_investment_project =' . dbquote($project_id) . 
			   ' and cer_investment_cer_version = 0';
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = 'cer_investment_cer_version,';
			$values = $new_version . ',';
			foreach($row as $field_name=>$value)
			{
				if($field_name != 'cer_investment_id' 
					and $field_name != 'cer_investment_cer_version'
				    and $field_name != 'user_created'
					and $field_name != 'date_created'
					and $field_name != 'user_modified'
					and $field_name != 'date_modified') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields .= 'user_created,';
			$values .= dbquote(user_login()) . ',';

			$fields .= 'date_created,';
			$values .= dbquote(date("Y-m-d H:i:s")) . ',';
			
			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_investments (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//payments
		$sql = 'Select * from cer_paymentterms ' . 
			   'where cer_paymentterm_project =' . dbquote($project_id) . 
			   ' and cer_paymentterm_cer_version = 0';
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = 'cer_paymentterm_cer_version,';
			$values = $new_version . ',';
			foreach($row as $field_name=>$value)
			{
				if($field_name != 'cer_paymentterm_id' 
					and $field_name != 'cer_paymentterm_cer_version'
				    and $field_name != 'user_created'
					and $field_name != 'date_created'
					and $field_name != 'user_modified'
					and $field_name != 'date_modified') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields .= 'user_created,';
			$values .= dbquote(user_login()) . ',';

			$fields .= 'date_created,';
			$values .= dbquote(date("Y-m-d H:i:s")) . ',';
			
			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_paymentterms (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//revenues
		$sql = 'Select * from cer_revenues ' . 
			   'where cer_revenue_project =' . dbquote($project_id) . 
			   ' and cer_revenue_cer_version = 0';
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = 'cer_revenue_cer_version,';
			$values = $new_version . ',';
			foreach($row as $field_name=>$value)
			{
				if($field_name != 'cer_revenue_id' 
					and $field_name != 'cer_revenue_cer_version'
				    and $field_name != 'user_created'
					and $field_name != 'date_created'
					and $field_name != 'user_modified'
					and $field_name != 'date_modified') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			
			$fields .= 'user_created,';
			$values .= dbquote(user_login()) . ',';

			$fields .= 'date_created,';
			$values .= dbquote(date("Y-m-d H:i:s")) . ',';
			
			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_revenues (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//salaries
		$sql = 'Select * from cer_salaries ' . 
			   'where cer_salary_project =' . dbquote($project_id) . 
			   ' and cer_salary_cer_version = 0';
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = 'cer_salary_cer_version,';
			$values = $new_version . ',';
			foreach($row as $field_name=>$value)
			{
				if($field_name != 'cer_salary_id' 
					and $field_name != 'cer_salary_cer_version'
				    and $field_name != 'user_created'
					and $field_name != 'date_created'
					and $field_name != 'user_modified'
					and $field_name != 'date_modified') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields .= 'user_created,';
			$values .= dbquote(user_login()) . ',';

			$fields .= 'date_created,';
			$values .= dbquote(date("Y-m-d H:i:s")) . ',';
			
			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_salaries (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}


		//stocks
		$sql = 'Select * from cer_stocks ' . 
			   'where cer_stock_project =' . dbquote($project_id) . 
			   ' and cer_stock_cer_version = 0';
		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = 'cer_stock_cer_version,';
			$values = $new_version . ',';
			foreach($row as $field_name=>$value)
			{
				if($field_name != 'cer_stock_id' 
					and $field_name != 'cer_stock_cer_version'
				    and $field_name != 'user_created'
					and $field_name != 'date_created'
					and $field_name != 'user_modified'
					and $field_name != 'date_modified') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields .= 'user_created,';
			$values .= dbquote(user_login()) . ',';

			$fields .= 'date_created,';
			$values .= dbquote(date("Y-m-d H:i:s")) . ',';
			
			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_stocks (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}

		//sellouts
		$sql = 'Select * from cer_sellouts ' . 
			   'where cer_sellout_project_id =' . dbquote($project_id) . 
			   ' and cer_sellout_version = 0';


		$res = mysql_query($sql) or dberror($sql);
		while ($row = mysql_fetch_assoc($res))
		{
		
			$fields = 'cer_sellout_version,';
			$values = $new_version . ',';
			foreach($row as $field_name=>$value)
			{
				if($field_name != 'cer_sellout_id' 
					and $field_name != 'cer_sellout_version'
				    and $field_name != 'user_created'
					and $field_name != 'date_created'
					and $field_name != 'user_modified'
					and $field_name != 'date_modified') {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
			}

			$fields .= 'user_created,';
			$values .= dbquote(user_login()) . ',';

			$fields .= 'date_created,';
			$values .= dbquote(date("Y-m-d H:i:s")) . ',';

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into cer_sellouts (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);
		
		}
	}

	//create LN-version
	$new_ln_version = 0;
	if($context == 'ln' and $new_cer_version > 0)
	{
		$sql = "select ln_basicdata_version " . 
			   "from ln_basicdata " . 
			   "where ln_basicdata_project = " . dbquote($project_id) . 
			   "order by ln_basicdata_version DESC";
		
		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$new_version = (int)$row["ln_basicdata_version"] + 1;
			$new_ln_version = $new_version;
			
			//create archive version of LN having version 0

			//basic data
			$sql = "select * from ln_basicdata " . 
				   "where ln_basicdata_version = 0 ". 
				   " and ln_basicdata_project = " . dbquote($project_id);

			
			$res = mysql_query($sql) or dberror($sql);	   
			$row = mysql_fetch_assoc($res);


			$fields = 'ln_basicdata_version,';
			$values = $new_version . ',';

			
			$fields .= 'ln_basicdata_version_user_id,';
			$values .= user_id() . ',';


			$fields .= 'ln_basicdata_version_cer_version,';
			$values .= $new_cer_version . ',';

			foreach($row as $field_name=>$value)
			{
				if($field_name != 'ln_basicdata_id' 
					and $field_name != 'ln_basicdata_version'
					and $field_name != 'ln_basicdata_version_user_id'
					and $field_name != 'ln_basicdata_version_cer_version'
					and $field_name != 'user_created'
					and $field_name != 'date_created'
					and $field_name != 'user_modified'
					and $field_name != 'date_modified'
					and $field_name != 'ln_basicdata_version_grosssqms'
					and $field_name != 'ln_basicdata_version_totalsqms'
					and $field_name != 'ln_basicdata_version_sqms'
					and $field_name != 'ln_basicdata_version_backofficesqms'
				) {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
				
			}


			$fields .= 'ln_basicdata_version_grosssqms,';
			$values .= $grosssqms . ',';

			$fields .= 'ln_basicdata_version_totalsqms,';
			$values .= $totalsqms . ',';

			$fields .= 'ln_basicdata_version_sqms,';
			$values .= $retailsqms . ',';

			$fields .= 'ln_basicdata_version_backofficesqms,';
			$values .= $backofficesqms . ',';

			$fields .= 'user_created,';
			$values .= dbquote(user_login()) . ',';

			$fields .= 'date_created,';
			$values .= dbquote(date("Y-m-d H:i:s")) . ',';

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into ln_basicdata (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);

			$new_ln_id = mysql_insert_id();
		}



		//LN POS Lease Version
		$poslease = get_ln_lease_data($project_id, $ln_basicdata_id, $project_pipeline, 0);

		$fields = 'poslease_ln_basicdata_id,';
		$values = $new_ln_id . ',';


		foreach($poslease as $field_name=>$value)
		{
			if($field_name != 'poslease_id'
				and $field_name != 'poslease_posaddress'
				and $field_name != 'poslease_order'
				and $field_name != 'poslease_breakpoint_percent'
				and $field_name != 'poslease_breakpoint_amount'
				and $field_name != 'poslease_breakpoint_percent2'
				and $field_name != 'poslease_breakpoint_amount2'
				and $field_name != 'poslease_breakpoint_percent3'
				and $field_name != 'poslease_breakpoint_amount3'
				and $field_name != 'user_created'
				and $field_name != 'date_created'
				and $field_name != 'user_modified'
				and $field_name != 'date_modified'
				and $field_name != 'poslease_type_id'
				and $field_name != 'poslease_type_name'
			) {
				$fields .= $field_name . ',';
				$values .= dbquote($value) . ',';
			}
			
		}

		$fields .= 'user_created,';
		$values .= dbquote(user_login()) . ',';

		$fields .= 'date_created,';
		$values .= dbquote(date("Y-m-d H:i:s")) . ',';

		$fields = substr($fields, 0, strlen($fields)-1);
		$values = substr($values, 0, strlen($values)-1);

		$sql_i = 'insert into ln_version_leases (' . $fields . ') VALUES (' . $values . ')';
		$result = mysql_query($sql_i) or dberror($sql_i);
	}


	//LN Basic DATA IN03
	$sql = "select cer_basicdata_id " . 
		   "from cer_basicdata " . 
		   "where cer_basicdata_version = 0 and cer_basicdata_project = " . dbquote($project_id) . 
		   "order by cer_basicdata_version DESC";
	
	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$old_cer_id = $row["cer_basicdata_id"];

		if($new_ln_version > $new_cer_version)
		{
			$new_version = $new_ln_version;
		}
		else
		{
			$new_version = $new_cer_version;
		}
	
		//basic data
		$sql = "select * from ln_basicdata_inr03 " . 
			   "where ln_basicdata_lnr03_cer_version = 0 " . 
			   " and ln_basicdata_lnr03_cerbasicdata_id = " . $old_cer_id;

		//echo $sql . "<br />";
		$res = mysql_query($sql) or dberror($sql);	   
		while ($row = mysql_fetch_assoc($res))
		{
			$fields = 'ln_basicdata_lnr03_cerbasicdata_id,';
			$values = $new_cer_id . ',';

			$fields .= 'ln_basicdata_lnr03_cer_version,';
			$values .= $new_version . ',';

			foreach($row as $field_name=>$value)
			{
				if($field_name != 'ln_basicdata_lnr03_id' 
					and $field_name != 'ln_basicdata_lnr03_cerbasicdata_id'
					and $field_name != 'ln_basicdata_lnr03_cer_version'
					and $field_name != 'user_created'
					and $field_name != 'date_created'
					and $field_name != 'user_modified'
					and $field_name != 'date_modified'
				) {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
				
			}

			$fields .= 'user_created,';
			$values .= dbquote(user_login()) . ',';

			$fields .= 'date_created,';
			$values .= dbquote(date("Y-m-d H:i:s")) . ',';

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into ln_basicdata_inr03 (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);

			//echo $sql_i . "<br />";
		}



		//brand data data
		$sql = "select * from ln_basicdata_lnr03_brands " . 
			   "where ln_basicdata_lnr03_brand_cer_version = 0 " . 
			   " and ln_basicdata_lnr03_brand_cerbasicdata_id = " . $old_cer_id;

		//echo $sql . "<br />";
		$res = mysql_query($sql) or dberror($sql);	   
		while ($row = mysql_fetch_assoc($res))
		{
			$fields = 'ln_basicdata_lnr03_brand_cerbasicdata_id,';
			$values = $new_cer_id . ',';

			$fields .= 'ln_basicdata_lnr03_brand_cer_version,';
			$values .= $new_version . ',';

			foreach($row as $field_name=>$value)
			{
				if($field_name != 'ln_basicdata_lnr03_brand_id' 
					and $field_name != 'ln_basicdata_lnr03_brand_cerbasicdata_id'
					and $field_name != 'ln_basicdata_lnr03_brand_cer_version'
					and $field_name != 'user_created'
					and $field_name != 'date_created'
					and $field_name != 'user_modified'
					and $field_name != 'date_modified'
				) {
					$fields .= $field_name . ',';
					$values .= dbquote($value) . ',';
				}
				
			}

			$fields .= 'user_created,';
			$values .= dbquote(user_login()) . ',';

			$fields .= 'date_created,';
			$values .= dbquote(date("Y-m-d H:i:s")) . ',';

			$fields = substr($fields, 0, strlen($fields)-1);
			$values = substr($values, 0, strlen($values)-1);

			$sql_i = 'insert into ln_basicdata_lnr03_brands (' . $fields . ') VALUES (' . $values . ')';
			$result = mysql_query($sql_i) or dberror($sql_i);

			//echo $sql_i . "<br />";
		}
	}


	return $new_cer_version;

}


/********************************************************************
    get ln_basicdata
*********************************************************************/
function get_ln_basicdata($project_id, $version = 0)
{

	$basic_data = array();

    $sql = "select *,ln_basicdata.date_created as versiondate  from ln_basicdata " . 
	       "left join projects on project_id = ln_basicdata_project " . 
		   "where ln_basicdata_project = " . dbquote($project_id) . 
		   " and ln_basicdata_version = " . $version;
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$basic_data = $row;
    }
	else
	{
		
		//get currency data
		$currency_id = 0;
		$exchange_rate = 0;
		$factor = 0;

		$sql = "select currency_id, currency_exchange_rate, currency_factor, country_id, " .
			   "order_actual_order_state_code " . 
			   "from projects " . 
			   "left join orders on order_id = project_order " . 
			   "left join addresses on address_id = order_client_address " .
			   "left join countries on country_id = address_country " . 
			   "left join currencies on currency_id = country_currency " . 
			   "where project_id = " . $project_id;

		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$currency_id = $row["currency_id"];
			$exchange_rate = $row["currency_exchange_rate"];
			$factor = $row["currency_factor"];
			$country_id = $row["country_id"];
			$order_state = $row["order_actual_order_state_code"];
		}


		//get currency data
		$currency_id = 0;
		$exchange_rate = 0;
		$factor = 0;

		$sql = "select currency_id, currency_exchange_rate, currency_factor " . 
			   "from projects " . 
			   "left join orders on order_id = project_order " . 
			   "left join countries on country_id = order_shop_address_country " . 
			   "left join currencies on currency_id = country_currency " . 
			   "where project_id = " . $project_id;

		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$currency_id = $row["currency_id"];
			$exchange_rate = $row["currency_exchange_rate"];
			$factor = $row["currency_factor"];
		}



		$fields = array();
		$values = array();

		$fields[] = "ln_basicdata_project";
		$values[] = dbquote($project_id);

		$fields[] = "ln_basicdata_currency";
		$values[] = dbquote($currency_id);

		$fields[] = "ln_basicdata_exchangerate";
		$values[] = dbquote($exchange_rate);

		$fields[] = "ln_basicdata_exr_date";
		$values[] = dbquote(date("Y-m-d"));

		$fields[] = "ln_basicdata_factor";
		$values[] = dbquote($factor);

		$fields[] = "date_created";
		$values[] = "now()";

		$fields[] = "date_modified";
		$values[] = "now()";

		$fields[] = "user_created";
		$values[] = dbquote(user_login());

		$fields[] = "user_modified";
		$values[] = dbquote(user_login());

		$sql = "insert into ln_basicdata (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
		
		mysql_query($sql) or dberror($sql);

		 $sql = "select * from ln_basicdata " . 
			   "left join projects on project_id = ln_basicdata_project " . 
			   "where ln_basicdata_project = " . dbquote($project_id) . 
			   " and ln_basicdata_version = " . $version;
		$res = mysql_query($sql) or dberror($sql);

		$basic_data =mysql_fetch_assoc($res);
	
	}

	return $basic_data;

}


/********************************************************************
    Get LN lease data
*********************************************************************/
function get_ln_lease_data($project_id, $ln_basicdata_id, $project_pipeline, $version = 0)
{
	$poslease = array();
	if($version == 0)
	{
	
		if($project_pipeline == 0)
		{
			$sql_i = "select * from posleases " . 
					 "where poslease_order = " . $project_id . 
					 " order by poslease_id DESC";
			
		}
		elseif($project_pipeline == 1)
		{
			$sql_i = "select * from posleasespipeline " . 
					 "where poslease_order = " . $project_id . 
					 " order by poslease_id DESC";
		}
		$res_i = mysql_query($sql_i) or dberror($sql_i);
		if ($row_i = mysql_fetch_assoc($res_i))
		{
			$poslease = $row_i;
		}
	}
	else
	{
		$sql_i = "select * from ln_version_leases " . 
					 "where poslease_ln_basicdata_id = " . $ln_basicdata_id . 
					 " order by poslease_id DESC";

		$res_i = mysql_query($sql_i) or dberror($sql_i);
		if ($row_i = mysql_fetch_assoc($res_i))
		{
			$poslease = $row_i;
		}
	}

	return $poslease;

}


/********************************************************************
    get ln_basicdata
*********************************************************************/
function get_exchange_rate_info($project_id = 0)
{
	$exchange_rate_info = array();
	$sql = "select currency_id, currency_symbol, currency_exchange_rate, currency_factor, " . 
		   "cer_basicdata_exchangerate, cer_basicdata_cer_locked " .
		   " from cer_basicdata " . 
		   "left join currencies on currency_id = cer_basicdata_currency " . 
		   " where cer_basicdata_project = " . dbquote($project_id) . 
	       " and cer_basicdata_version = 0 ";

	$res = mysql_query($sql) or dberror($sql);

	if($row = mysql_fetch_assoc($res))
	{
		if($row["currency_id"] > 1)
		{
			$exchange_rate_info["symbol"] = $row["currency_symbol"];
			$exchange_rate_info["factor"] = $row["currency_factor"];
			
			if($row["cer_basicdata_cer_locked"] == 1)
			{
				$exchange_rate_info["exchange_rate"] = $row["cer_basicdata_exchangerate"];
			}
			else
			{
				$exchange_rate_info["exchange_rate"] = $row["currency_exchange_rate"];
			}
			return $exchange_rate_info;
		}
	}
	
	return $exchange_rate_info;
}


/********************************************************************
    get ln_basicdata
*********************************************************************/
function must_attach_file_to_mail($file_category = 0)
{
	$sql = "select order_file_category_attach_in_mails " . 
		   " from order_file_categories " . 
		   " where order_file_category_id = " . $file_category;

	$res = mysql_query($sql) or dberror($sql);

	if($row = mysql_fetch_assoc($res))
	{
		return $row["order_file_category_attach_in_mails"];
	}
	return 0;

}


/********************************************************************
   check if latest task has to keep the order's state
*********************************************************************/
function must_keep_order_state($order_id = 0)
{
	$sql = "select task_keep_order_state, task_order_state from tasks " . 
		   " where task_order = " . dbquote($order_id) .
		   " order by task_id DESC"; 
	
	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		return $row["task_order_state"];
	}
	
	return 0;
}

/********************************************************************
    get logistic state
*********************************************************************/
function get_logistic_state($order_id = 0, $user_id = 0, $order_actual_order_state_code = '')
{
	
	$logistic_state = array();

	$logistic_state["info"] = "";
	$logistic_state["data"] = "";
	$logistic_state["code"] = "";

	$update_logistic_state = true;

	//check if there is a logistic state available
	$sql = "select count(actual_order_state_id) as num_recs " . 
		   " from actual_order_states " . 
		   " left join order_states on order_state_id = actual_order_state_state " . 
		   " where actual_order_state_order = " . dbquote($order_id) .
		   " and (order_state_used_for_logistics = 1 or actual_order_state_state = 34)";

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	
	if($row["num_recs"] == 0)
	{
		return $logistic_state ;
	}


	//check if arrival of all items was confirmed
	$sql = "select actual_order_state_state " . 
		   " from actual_order_states " .
		   " left join users on user_id = actual_order_state_user " . 
		   " where actual_order_state_order = " . dbquote($order_id) .
		   " and actual_order_state_state in (41, 69) " . 
		   " order by actual_order_states.date_created DESC ";

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);

	if($row["actual_order_state_state"] == 41)
	{
		$logistic_state["info"] = "800 Arrival of all items is confirmed";
		$result = update_logistic_status($order_id, '800');
		return $logistic_state;
	}
	elseif($row["actual_order_state_state"] == 69)
	{
		
		$logistic_state["info"] = "810 Complaint about delivery";
		$result = update_logistic_status($order_id, '810');
		return $logistic_state;
	}


	//get all involved suppliers
	$suppliers = array();
	$sql = "select  DISTINCT order_item_supplier_address, address_company " . 
		   " from order_items " .
		   " left join addresses on address_id = order_item_supplier_address " .
		   " where order_item_order = " . dbquote($order_id) .
		   " and order_item_quantity > 0 " .
		   " and order_item_supplier_address > 0 ";


	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$suppliers[$row["order_item_supplier_address"]] = $row["address_company"];
	}

	if(count($suppliers) == 0)
	{
		return "";
	}
	
	//get all involved forwarders
	$forwarders = array();
	$sql = "select DISTINCT order_item_forwarder_address, address_company " . 
		   " from order_items " .
		   " left join addresses on address_id = order_item_forwarder_address " .
		   " where order_item_order = " . dbquote($order_id) .
		   " and order_item_quantity > 0 " .
		   " and order_item_forwarder_address > 0 ";


	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$forwarders[$row["order_item_forwarder_address"]] = $row["address_company"];
	}



	//check if all items were orderd
	$sql = "select count(order_item_id) as num_ordered " . 
		   " from order_items " .
		   " where order_item_order = " . dbquote($order_id) .
		   " and order_item_supplier_address > 0 " . 
		   " and order_item_quantity > 0 " .
		   " and order_item_ordered is not null " . 
		   " and order_item_ordered <> '0000-00-00' " . 
		   " and order_item_not_in_budget <> 1 ";

	   
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$num_of_ordered_items = $row["num_ordered"];



	$all_suppliers_confirmed = true;
	$sql = "select  DISTINCT order_item_supplier_address, address_company " . 
		   " from order_items " .
		   " left join addresses on address_id = order_item_supplier_address " . 
		   " where order_item_order = " . dbquote($order_id) .
		   " and order_item_supplier_address > 0 " . 
		   " and order_item_quantity > 0 " .
		   " and (order_item_ordered is null or order_item_ordered = '0000-00-00') " . 
		   " and order_item_not_in_budget <> 1 ";


	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		if($all_suppliers_confirmed == true)
		{
			$logistic_state["data"] = "<strong>Missing orders for</strong><br />";
		}
		$logistic_state["data"] = $logistic_state["data"] . "&bull;&nbsp;" . $row["address_company"] . "<br />";
		$all_suppliers_confirmed = false;
	}
	
	if($all_suppliers_confirmed == false)
	{
		if($num_of_ordered_items == 0)
		{
			$logistic_state["info"] = " No items were ordered";
			$update_logistic_state = false;
		}
		else
		{
			$logistic_state["info"] = "700 Not all items were ordered";
			$logistic_state["code"] = "700";
			$update_logistic_state = false;
		}
	}
	elseif($num_of_ordered_items == 0)
	{
		$logistic_state["info"] = " No items were ordered";
		$update_logistic_state = false;
	}
	else
	{
		$logistic_state["info"] = "700 All items are ordered";
		$logistic_state["code"] = "700";
	}


	//check if all suppliers have confirmed the order
	if($update_logistic_state == true)
	{
		$order_state_suppliers = array();
		$sql = "select DISTINCT user_address " . 
			   " from actual_order_states " .
			   " left join users on user_id = actual_order_state_user " . 
			   " where actual_order_state_order = " . dbquote($order_id) .
			   " and actual_order_state_state = 37";

		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			$order_state_suppliers[$row["user_address"]] = $row["user_address"];
		}

		
		$all_suppliers_confirmed = true;
		foreach($suppliers as $supplier_id=>$company)
		{
			if(!in_array($supplier_id, $order_state_suppliers))
			{
				if($all_suppliers_confirmed == true)
				{
					$logistic_state["data"] = $logistic_state["data"] . "<strong>Order was not confirmed by</strong><br />";
				}
				$logistic_state["data"] = $logistic_state["data"] . "&bull;&nbsp;" . $company . "<br />";
				$all_suppliers_confirmed = false;
			}
		}


		if($all_suppliers_confirmed == false)
		{
			if($update_logistic_state == true)
			{
				$logistic_state["info"] = "700 Not all suppliers have confirmed the order";
				$update_logistic_state = false;
			}
		}
		elseif($all_suppliers_confirmed == true and $update_logistic_state == true)
		{
			//check if all items have a pickup date
			$sql = "select DISTINCT order_item_supplier_address, address_company " . 
				   " from order_items " .
				   " left join addresses on address_id = order_item_supplier_address " .
				   " where order_item_order = " . dbquote($order_id) .
				   " and order_item_supplier_address > 0 " . 
				   " and order_item_quantity > 0 " .
				   " and (order_item_ready_for_pickup is null or order_item_ready_for_pickup = '0000-00-00') " . 
				   " and order_item_not_in_budget <> 1 ";
				   
			$res = mysql_query($sql) or dberror($sql);
			while($row = mysql_fetch_assoc($res))
			{
			
				if($all_suppliers_confirmed == true)
				{
					$logistic_state["data"] = $logistic_state["data"] . "<strong>Order was not confirmed by</strong><br />";
				}
				$logistic_state["data"] = $logistic_state["data"] . "&bull;&nbsp;" . $row["address_company"] . "<br />";
				$all_suppliers_confirmed = false;
				$update_logistic_state = false;
			}

			if($update_logistic_state == true and $all_suppliers_confirmed == true)
			{
				$logistic_state["info"] = "720 All suppliers have confirmed the order";
			}
			elseif($update_logistic_state == true)
			{
				$logistic_state["info"] = "700 Not all suppliers have confirmed the order";
				$update_logistic_state = false;
			}
		}
	}

	

	if($update_logistic_state == true)
	{
		//check if all forwarders got the request for delivery step 730
		$order_state_forwarders = array();
		$sql = "select DISTINCT user_address " . 
			   " from order_mails " .
			   " left join users on user_id = order_mail_user " . 
			   " where order_mail_order = " . dbquote($order_id) . 
			   " and order_mail_is_cc <> 1 " . 
			   " and order_mail_order_state = 38";

		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			$order_state_forwarders[$row["user_address"]] = $row["user_address"];
		}

		
		$all_forwarders_confirmed = true;
		foreach($forwarders as $forwarder_id=>$company)
		{
			if(!in_array($forwarder_id, $order_state_forwarders))
			{
				if($all_forwarders_confirmed == true)
				{
					$logistic_state["data"] = $logistic_state["data"] . "<strong>Request for delivery was not sent to</strong><br />";
				}
				$logistic_state["data"] = $logistic_state["data"] . "&bull;&nbsp;" . $company . "<br />";
				$all_forwarders_confirmed = false;
			}
		}

		if(count($forwarders) == 0)
		{
			$logistic_state["data"] = $logistic_state["data"] . "<strong>Request for delivery was not sent</strong><br />";
			$all_forwarders_confirmed = false;
		}

		if($update_logistic_state == true and $all_forwarders_confirmed == false)
		{
			$logistic_state["info"] = "720 Not all forwarders have a request for delivery";
			$update_logistic_state = false;
		}
		elseif($update_logistic_state == true)
		{
			$logistic_state["info"] = "730 All forwarders have a request for delivery";
		}
	}

	
	
	//check if all forwarders have accepted the request for delivery step 740
	$order_state_forwarders = array();
	$sql = "select DISTINCT user_address " . 
		   " from actual_order_states " .
		   " left join users on user_id = actual_order_state_user " . 
		   " where actual_order_state_order = " . dbquote($order_id) .
		   " and actual_order_state_state = 39";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$order_state_forwarders[$row["user_address"]] = $row["user_address"];
	}

	
	$all_forwarders_confirmed = true;
	foreach($forwarders as $forwarder_id=>$company)
	{
		if(!in_array($forwarder_id, $order_state_forwarders))
		{
			if($all_forwarders_confirmed == true)
			{
				$logistic_state["data"] = $logistic_state["data"] . "<strong>Request for delivery was not confirmed by</strong><br />";
			}
			$logistic_state["data"] = $logistic_state["data"] . "&bull;&nbsp;" . $company . "<br />";
			$all_forwarders_confirmed = false;
		}
	}


	if($update_logistic_state == true and $all_forwarders_confirmed == false)
	{
		$logistic_state["info"] = "730 Not all forwarders have accepted the request for delivery";
		$update_logistic_state = false;
	}
	elseif($update_logistic_state == true)
	{
		$logistic_state["info"] = "740 All forwarders have accepted the request for delivery";
	}

	
	
	
	//check if all suppliers have confirmed the pick up step 745
	if($update_logistic_state == true and $num_of_ordered_items > 0)
	{
		$order_state_suppliers = array();
		$sql = "select DISTINCT user_address " . 
			   " from actual_order_states " .
			   " left join users on user_id = actual_order_state_user " . 
			   " where actual_order_state_order = " . dbquote($order_id) .
			   " and actual_order_state_state = 70";

		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			$order_state_suppliers[$row["user_address"]] = $row["user_address"];
		}

		
		$all_suppliers_confirmed = true;
		foreach($suppliers as $supplier_id=>$company)
		{
			if(!in_array($supplier_id, $order_state_suppliers))
			{
				if($all_suppliers_confirmed == true)
				{
					$logistic_state["data"] = $logistic_state["data"] . "<strong>Pick up of goods was not confirmed by</strong><br />";
				}
				$logistic_state["data"] = $logistic_state["data"] . "&bull;&nbsp;" . $company . "<br />";
				$all_suppliers_confirmed = false;
			}
		}

		if($all_suppliers_confirmed == false)
		{
			if($update_logistic_state == true)
			{
				$logistic_state["info"] = "740 Not all items were picked up";
				$update_logistic_state = false;
			}
		}
		elseif($all_suppliers_confirmed == true)
		{
			//check if all items have a pickup date
			$sql = "select DISTINCT order_item_supplier_address, address_company " . 
				   " from order_items " .
				   " left join addresses on address_id = order_item_supplier_address " .
				   " where order_item_order = " . dbquote($order_id) .
				   " and order_item_supplier_address > 0 " .
				   " and order_item_quantity > 0 " .
				   " and (order_item_pickup is null or order_item_pickup = '0000-00-00') " . 
				   " and order_item_not_in_budget <> 1 ";
				   
			$res = mysql_query($sql) or dberror($sql);
			while($row = mysql_fetch_assoc($res))
			{
				if($all_suppliers_confirmed == true)
				{
					$logistic_state["data"] = $logistic_state["data"] . "<strong>Pick up of goods was not confirmed by</strong><br />";
				}
				$logistic_state["data"] = $logistic_state["data"] . "&bull;&nbsp;" . $row["address_company"] . "<br />";
				$all_suppliers_confirmed = false;
			}

			if($all_suppliers_confirmed == true)
			{
				$logistic_state["info"] = "745 All suppliers have confirmed the pick up of goods";
			}
			
		}
	}

	
	//check if all forwarders have confirmed the arrival (750)
	
	if($num_of_ordered_items > 0)
	{
		//check if all forwarders have confirmed the arrival
		$sql = "select count(DISTINCT user_address) as num_forwarders " . 
			   " from actual_order_states " .
			   " left join users on user_id = actual_order_state_user " . 
			   " where actual_order_state_order = " . dbquote($order_id) .
			   " and actual_order_state_state = 40";

		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);

		if(count($forwarders) > 0 and count($forwarders) > $row["num_forwarders"])
		{
		
			$order_state_forwarders = array();
			$sql = "select DISTINCT user_address " . 
				   " from actual_order_states " .
				   " left join users on user_id = actual_order_state_user " . 
				   " where actual_order_state_order = " . dbquote($order_id) .
				   " and actual_order_state_state = 40";

			$res = mysql_query($sql) or dberror($sql);
			while($row = mysql_fetch_assoc($res))
			{
				$order_state_forwarders[$row["user_address"]] = $row["user_address"];
			}

			
			$all_forwarders_confirmed = true;
			foreach($forwarders as $forwarder_id=>$company)
			{
				if(!in_array($forwarder_id, $order_state_forwarders))
				{
					if($all_forwarders_confirmed == true)
					{
						$logistic_state["data"] = $logistic_state["data"] . "<strong>Arrival of goods was not confirmed by</strong><br />";
					}
					$logistic_state["data"] = $logistic_state["data"] . "&bull;&nbsp;" . $company . "<br />";
					$all_forwarders_confirmed = false;
				}
			}
		}

		if($update_logistic_state == true and $all_forwarders_confirmed == true)
		{
			$logistic_state["info"] = "750 Delivery of all items is confirmed";
		}
		elseif($update_logistic_state == true)
		{
			$logistic_state["info"] = "745 Not all forwarders confirmed delivery of goods";
			$update_logistic_state = false;
		}
		
		//check if all items have arrived
		if($update_logistic_state == true)
		{
			$all_forwarders_confirmed = true;
			$sql = "select  DISTINCT order_item_forwarder_address, address_company " . 
				   " from order_items " .
				   " left join addresses on address_id = order_item_forwarder_address " .
				   " where order_item_order = " . dbquote($order_id) .
				   " and order_item_supplier_address > 0 " .
				   " and order_item_quantity > 0 " .
				   " and (order_item_arrival is null or order_item_arrival = '0000-00-00') " . 
				   " and order_item_not_in_budget <> 1 ";
				   
			$res = mysql_query($sql) or dberror($sql);
			while($row = mysql_fetch_assoc($res))
			{
				if($all_forwarders_confirmed == true)
				{
					$logistic_state["data"] = $logistic_state["data"] . "<strong>Arrival of goods was not confirmed by</strong><br />";
				}
				$logistic_state["data"] = $logistic_state["data"] . "&bull;&nbsp;" . $row["address_company"] . "<br />";
				$all_forwarders_confirmed = false;
			}


			if($all_forwarders_confirmed == false)
			{
				$logistic_state["info"] = "745 Not all items have an arrival date";
				$update_logistic_state = false;
			}
		}


		//chek if arrival of all items was confirmed
		$sql = "select actual_order_state_state " . 
			   " from actual_order_states " .
			   " left join users on user_id = actual_order_state_user " . 
			   " where actual_order_state_order = " . dbquote($order_id) .
			   " and actual_order_state_state in (41, 69) " . 
			   " order by actual_order_states.date_created DESC ";

		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);

		if($row["actual_order_state_state"] == 41)
		{
			$logistic_state["info"] = "800 Arrival of all items is confirmed";
		}
		elseif($row["actual_order_state_state"] == 69)
		{
			$logistic_state["info"] = "810 Complaint about delivery";
		}
		elseif($update_logistic_state == true)
		{
			$logistic_state["info"] = "750 Delivery of all items is confirmed";
		}
	}
	if(substr($logistic_state["info"], 0, 3) == ' No')
	{
		$result = update_logistic_status($order_id, "");
	}
	else
	{
		$result = update_logistic_status($order_id, substr($logistic_state["info"], 0, 3));
	}
	return $logistic_state;

}



/********************************************************************
    get offer state
*********************************************************************/
function get_offer_state($order_id = 0, $user_id = 0, $order_actual_order_state_code = '')
{
	$offer_state = array();

	$offer_state["info"] = "";
	$offer_state["data"] = "";
	$offer_state["code"] = "";


	
	//get all suppliers having a request for offer
	$suppliers = array();
	$sql = "select DISTINCT user_address, address_company " . 
		   " from order_mails " .
		   " left join users on user_id = order_mail_user " .
		   " left join addresses on address_id = user_address " . 
		   " where order_mail_order = " . dbquote($order_id) .
		   " and order_mail_is_cc <> 1 " . 
		   " and order_mail_order_state = 26";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$suppliers[$row["user_address"]] = $row["address_company"];
	}

	$suppliers_having_an_accepted_offer = array();
	$sql = "select DISTINCT user_address, address_company " . 
		   " from order_mails " .
		   " left join users on user_id = order_mail_user " .
		   " left join addresses on address_id = user_address " . 
		   " where order_mail_order = " . dbquote($order_id)  .
		   " and order_mail_is_cc <> 1 " . 
		   " and order_mail_order_state = 31";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$suppliers_having_an_accepted_offer[$row["user_address"]] = $row["user_address"];
	}

	//check if all offers were accepted
	$all_offers_accepted = true;
	foreach($suppliers as $supplier_id=>$company)
	{
		if(!in_array($supplier_id, $suppliers_having_an_accepted_offer))
		{
			if($all_offers_accepted == true)
			{
				$offer_state["info"] = "540 All offers were submitted";
				$offer_state["data"] = $offer_state["data"] . "<strong>Offer was not approved for</strong><br />";
			}
			$all_offers_accepted = false;

			$offer_state["data"] = $offer_state["data"] . "&bull;&nbsp;" . $company . "<br />";
			
		}
	}

	if($all_offers_accepted == false)
	{
		//check if offers were submitted by all suppliers
		if(count($suppliers) > 0 and count($suppliers) > count($suppliers_having_an_accepted_offer))
		{
			$suppliers_having_submitted_an_offer = array();

			$sql = "select DISTINCT user_address, address_company " . 
				   " from order_mails " .
				   " left join users on user_id = order_mail_from_user " .
				   " left join addresses on address_id = user_address " . 
				   " where order_mail_order = " . dbquote($order_id)  .
		           " and order_mail_is_cc <> 1 " . 
				   " and order_mail_order_state = 29";

			$res = mysql_query($sql) or dberror($sql);
			while($row = mysql_fetch_assoc($res))
			{
				$suppliers_having_submitted_an_offer[$row["user_address"]] = $row["user_address"];
			}

			//check if all offers were submitted
			$all_offers_submitted = true;
			foreach($suppliers as $supplier_id=>$company)
			{
				if(!in_array($supplier_id, $suppliers_having_submitted_an_offer))
				{
					if($all_offers_submitted == true)
					{
						$offer_state["info"] = "530 All requests for offer were accepted";
						$offer_state["data"] = $offer_state["data"] . "<strong>Offer was not submitted by</strong><br />";
					}
					$all_offers_submitted = false;
					$offer_state["data"] = $offer_state["data"] . "&bull;&nbsp;" . $company . "<br />";
				}
			}

			if(count($suppliers) > 0 and count($suppliers) == count($suppliers_having_submitted_an_offer))
			{
				$offer_state["data"] = $offer_state["data"] . "<strong>Offer was submitted by all suppliers</strong><br />";
			}
		}


		//check if offers were accepted by all suppliers
		if(count($suppliers) > 0 and count($suppliers) > count($suppliers_having_an_accepted_offer))
		{
			$suppliers_having_accepted_request_for_offer = array();
			$sql = "select DISTINCT user_address, address_company " . 
				   " from actual_order_states " .
				   " left join users on user_id = actual_order_state_user " .
				   " left join addresses on address_id = user_address " . 
				   " where actual_order_state_order = " . dbquote($order_id) .
				   " and actual_order_state_state = 28";

			$res = mysql_query($sql) or dberror($sql);
			while($row = mysql_fetch_assoc($res))
			{
				$suppliers_having_accepted_request_for_offer[$row["user_address"]] = $row["user_address"];
			}

			//check if all offers were accepted
			$all_requests_for_offer_accepted = true;
			foreach($suppliers as $supplier_id=>$company)
			{
				if(!in_array($supplier_id, $suppliers_having_accepted_request_for_offer))
				{
					if($all_requests_for_offer_accepted == true)
					{
						$offer_state["info"] = "510 Not all requests for offer were accepted";
						$offer_state["data"] = $offer_state["data"] . "<strong>Request for Offer was not accepted by</strong><br />";
					}
					$all_requests_for_offer_accepted = false;
					$offer_state["data"] = $offer_state["data"] . "&bull;&nbsp;" . $company . "<br />";
				}
			}

			if(count($suppliers) > 0 and count($suppliers) == count($suppliers_having_accepted_request_for_offer))
			{
				$offer_state["data"] = $offer_state["data"] . "<strong>Request for Offer was accepted by all suppliers</strong><br />";
			}
		}
	}

	return $offer_state;
}

/********************************************************************
    get logistics state for forwarder or supplier
*********************************************************************/
function get_external_losgistics_state($order_id = 0, $address_id = 0)
{
	$logistics_state = array();

	//get all users
	$users = array();
	$sql = "select user_id from users where user_address = " . dbquote($address_id);
	$res = mysql_query($sql) or dberror($sql);

    while ($row = mysql_fetch_assoc($res))
    {
		$users[] = $row["user_id"];
	}


	//check if all suppliers have confirmed pickup
	$sql = "select count(order_item_id) as num_recs, " .
		   " SUM(order_item_pickup <> '0000-00-00' and order_item_pickup is not null) AS num_picked_up " . 
		   " from order_items " .
		   " where order_item_order = " . dbquote($order_id) .
		   " and order_item_supplier_address = " . dbquote($address_id) .  
		   " and order_item_quantity > 0 " . 
		   " and order_item_not_in_budget <> 1 ";

	   
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	$num_of_items = $row["num_recs"];
	$num_of_picked_up_items = $row["num_picked_up"];

	if(count($users) > 0)
	{
		$user_filter = " and (order_mail_user in (" . implode(',', $users)  . ") or order_mail_from_user in (" . implode(',', $users)  . ")) ";

		$sql = "select order_state_code, concat(order_state_code, ' ', order_state_name) as ostate " . 
			   " from order_mails " .
			   " left join order_states on order_state_id = order_mail_order_state " .
			   " where order_mail_order = " . dbquote($order_id) .
			   " and ((order_mail_order_state > 0 and order_mail_order_state < 41) or order_mail_order_state = 70) "  .
			   " and order_mail_is_cc <> 1 " . 
			   $user_filter .
			   " order by order_state_code DESC";


		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$logistics_state = array("code"=>$row["order_state_code"], "name"=>$row["ostate"]);

			if($row["order_state_code"] == '745') 
			{
				$sql_a = "select DISTINCT task_id " . 
					   " from tasks " .
					   " left join users on user_id = task_user " .
					   " left join addresses on address_id = user_address " . 
					   " where task_order = " . dbquote($order_id) .
					   " and user_address = " . dbquote($address_id) .
					   " and task_order_state = 38";
				
				$res_a = mysql_query($sql_a) or dberror($sql_a);
				if($row_a = mysql_fetch_assoc($res_a))
				{
					$logistics_state = array("code"=>'720', "name"=>"720 Order confirmed");
				}
			}
		}
	}
	
    
    return $logistics_state;
}


/********************************************************************
    update logistic state for a project
*********************************************************************/
function update_logistic_status($order_id = 0, $status = 0)
{
	$sql_u = "update orders set order_logistic_status = " . dbquote($status) . 
				 " where order_id = " . dbquote($order_id);

	$result = mysql_query($sql_u) or dberror($sql_u);

	return true;
}


/********************************************************************
    get development state for a project
*********************************************************************/
function get_project_development_status($order_id = 0)
{

	$development_state = array("code"=>"", "name"=>"");

	$sql = "select order_development_status, concat(order_state_code, ' ', order_state_name) as ostate " .
           " from orders " .
           " left join order_states on order_state_code = order_development_status " .
		   " left join order_state_groups on order_state_group_id = order_state_group " .
		   " where order_state_group_order_type = 1 " . 
           " and order_id = " . dbquote($order_id);

    $res = mysql_query($sql) or dberror($sql);

    if ($row = mysql_fetch_assoc($res))
    {
        $development_state = array("code"=>$row["order_development_status"], "name"=>$row["ostate"]);
    }

	return $development_state;
}


/********************************************************************
    checks if a catalogue order is a replacment order for a project
*********************************************************************/
function get_related_replacement_project($order_id = 0)
{
	$sql = "select project_id, project_number, order_archive_date ". 
		   " from order_item_replacements " . 
		   " left join projects on project_order = order_item_replacement_order_id " .
		   " left join orders on order_id = project_order " . 
		   " where order_item_replacement_catalog_order_id = " . dbquote($order_id);

	
	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$tmp = array();
		$tmp["project_id"] = $row["project_id"];
		$tmp["project_number"] = $row["project_number"];
		$tmp["order_archive_date"] = $row["order_archive_date"];
		
		return $tmp;
	}

	return "";
}


/********************************************************************
   get item's replacment information from project
*********************************************************************/
function get_items_replacement_information($order_item_id = 0)
{
	$sql = "select order_item_replacement_reason_text, order_item_replacement_remarks, order_item_warranty_type_text, ".
		   " furniturepayer.replacements_payer_name_name as furniture_payed_by, " .
		   " freightpayer.replacements_payer_name_name as freight_payed_by, " . 
		   " order_item_text, order_item_quantity " .
		   " from order_item_replacements " . 
		   " left join replacements_payer_names as furniturepayer on .furniturepayer.replacements_payer_name_id = order_item_replacement_item_to_be_payed_by " .
		   " left join replacements_payer_names as freightpayer on freightpayer.replacements_payer_name_id = order_item_replacement_ferigeht_to_be_payed_by " . 
		   " left join order_items on order_item_id = order_item_replacement_order_item_id " . 
		   " left join order_item_warranty_types on order_item_warranty_type_id = order_item_replacement_warranty_type_id " . 
		   " left join order_item_replacement_reasons on order_item_replacement_reasons.order_item_replacement_reason_id = order_item_replacements.order_item_replacement_reason_id " . 
		   " where order_item_replacement_catalog_order_order_item_id = " . dbquote($order_item_id);


	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		return $row;
	}

	return "";
}

/********************************************************************
   get item's replacment information from project
*********************************************************************/
function get_concerned_suppliers($order_id = 0, $project_is_urgent = 0)
{

	//update all new items in the list of materials for suppliers with visible items

	if($project_is_urgent == 1)
	{
		$sql = "select DISTINCT order_item_supplier_address " . 
			   " from order_items " . 
			   " where order_item_supplier_address > 0 " . 
			   " and order_item_show_to_suppliers <> 1 " . 
			   " and order_item_order = " . dbquote($order_id);
		
		$res = mysql_query($sql) or dberror($sql);

		while ($row = mysql_fetch_assoc($res))
		{
			$sql = "update order_items " . 
				   " set order_item_show_to_suppliers = 1 " . 
				   " where order_item_supplier_address = " . dbquote($row["order_item_supplier_address"]);

			$result = mysql_query($sql) or dberror($sql);
		}


	}
	
	$suppliers = array();
	
	$sql = "select DISTINCT order_item_supplier_address, address_company " . 
		   " from order_items " . 
		   " left join addresses on address_id = order_item_supplier_address ". 
		   " where order_item_supplier_address > 0 " . 
		   " and order_item_order = " . dbquote($order_id) . 
		   " order by address_company";

	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{
		$suppliers[$row["order_item_supplier_address"]] = $row["address_company"];
	}

	return $suppliers;
}



/********************************************************************
   getregional responsible
*********************************************************************/
function get_regional_responsible($legal_type = 0, $client_address_id = 0)
{
	$regional_responsables = array();

	if($legal_type == 1
		or $legal_type == 3
		or $legal_type == 4
		or $legal_type == 5)
	{
		$sql = "select user_company_responsible_user_id, user_email " . 
			   "from user_company_responsibles " . 
			   "left join users on user_id = user_company_responsible_user_id " . 
			   "where user_company_responsible_address_id = " . $client_address_id . 
			   " and user_company_responsible_retail = 1";
	}
	else // wholesale
	{
		$sql = "select user_company_responsible_user_id, user_email " . 
			   "from user_company_responsibles " . 
			   "left join users on user_id = user_company_responsible_user_id " . 
			   "where user_company_responsible_address_id = " . $client_address_id . 
			   " and user_company_responsible_wholsale = 1";
	}

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$regional_responsables[$row["user_company_responsible_user_id"]]  = $row["user_email"];
	}

	return $regional_responsables;
}


/********************************************************************
   getregional responsible
*********************************************************************/
function get_regional_responsibles_full_data($legal_type = 0, $client_address_id = 0)
{
	$regional_responsables = array();

	if($legal_type == 1
		or $legal_type == 3
		or $legal_type == 4
		or $legal_type == 5)
	{
		$sql = "select user_id, user_company_responsible_user_id, user_email, " .
			   "concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_fullname, " . 
			   " user_company_responsible_address_id as address_id, " .
			   " (select role_name from user_roles
left join roles on role_id = user_role_role
where user_role_role in (33, 56) and user_role_user = user_company_responsible_user_id limit 0,1) as role_name " . 
			   "from user_company_responsibles " . 
			   "left join users on user_id = user_company_responsible_user_id " . 
			   " left join addresses on address_id = user_address ". 
			   "where user_active = 1 and user_company_responsible_address_id = " . $client_address_id . 
			   " and user_company_responsible_retail = 1";
	}
	else // wholesale
	{
		$sql = "select user_id, user_company_responsible_user_id, user_email, " .
			   "concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_fullname, " . 
			   " user_company_responsible_address_id as address_id, " .
			" (select role_name from user_roles
left join roles on role_id = user_role_role
where user_role_role in (33, 56) and user_role_user = user_company_responsible_user_id limit 0,1) as role_name " . 
			   "from user_company_responsibles " . 
			   "left join users on user_id = user_company_responsible_user_id " . 
			   " left join addresses on address_id = user_address ".
			   "where user_active = 1 and user_company_responsible_address_id = " . $client_address_id . 
			   " and user_company_responsible_wholsale = 1";
	}

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$regional_responsables[$row["user_id"]]  = $row;
	}

	return $regional_responsables;
}



/********************************************************************
   getregional responsible
*********************************************************************/
function get_regional_sales_managers_hq($legal_type = 0, $client_address_id = 0)
{
	$regional_responsables = array();

	if($legal_type == 1
		or $legal_type == 3
		or $legal_type == 4
		or $legal_type == 5)
	{
		$sql = "select user_id, user_company_responsible_user_id, user_email, " .
			   "concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_fullname, " . 
			   " user_company_responsible_address_id as address_id, " .
			   " (select role_name from user_roles
left join roles on role_id = user_role_role
where user_role_role in (33) and user_role_user = user_company_responsible_user_id limit 0,1) as role_name " . 
			   "from user_company_responsibles " . 
			   "left join users on user_id = user_company_responsible_user_id " . 
			   " left join addresses on address_id = user_address ". 
			   "where user_active = 1 and user_company_responsible_address_id = " . $client_address_id . 
			   " and user_company_responsible_retail = 1";
	}
	else // wholesale
	{
		$sql = "select user_id, user_company_responsible_user_id, user_email, " .
			   "concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_fullname, " . 
			   " user_company_responsible_address_id as address_id, " .
			" (select role_name from user_roles
left join roles on role_id = user_role_role
where user_role_role in (33) and user_role_user = user_company_responsible_user_id limit 0,1) as role_name " . 
			   "from user_company_responsibles " . 
			   "left join users on user_id = user_company_responsible_user_id " . 
			   " left join addresses on address_id = user_address ".
			   "where user_active = 1 and user_company_responsible_address_id = " . $client_address_id . 
			   " and user_company_responsible_wholsale = 1";
	}

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$regional_responsables[$row["user_id"]]  = $row;
	}

	return $regional_responsables;
}


/********************************************************************
   getregional responsible
*********************************************************************/
function get_hq_management()
{
	$hq_managers = array();

	$sql = "select user_id, user_email, " .
			   "concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_fullname, " . 
			   " user_address as address_id, role_name " .
			   "from users " . 
			   " left join addresses on address_id = user_address ".
	           " left join user_roles on user_role_user = user_id " . 
		       " left join roles on role_id = user_role_role " .
			   "where user_active = 1 and user_role_role in (87)";
	

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$hq_managers[$row["user_id"]]  = $row;
	}

	return $hq_managers;
}

?>