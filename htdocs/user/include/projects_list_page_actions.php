<?php


if (has_access("can_create_new_projects") and $user_data["user_project_requester"] != 1)
{
    $page->register_action('new', 'Project Request', "project_new_01.php");

}

if (has_access("has_access_to_projects_dashboard"))
{
	//$page->register_action('dashboard', "Dashboard Logistics", "", "", "dashboard");
}

if(has_access("has_access_to_all_projects") or has_access("can_complete_cms") or has_access("can_approve_cms"))
{
	$page->register_action('home', 'Projects with completed CMS', "projects_cms_completed.php");
}



if (has_access("can_view_only_his_project_forecasts")
   or has_access("can_view_project_forecasts")
   or has_access("can_edit_only_his_project_forecasts")
   or has_access("can_edit_project_forecasts"))
{
    $page->register_action('forecasts', 'Project Forecast ', "project_forecasts.php");
}



//check if there are changes in project numbers
if(isset($list_filter))
{
	$sql_y = "select DISTINCT YEAR(oldproject_numbers.date_created) as year " . 
		   "from oldproject_numbers "  .
		   "left join projects on project_id = oldproject_number_project_id " . 
		   "left join orders on project_order = order_id ".
		   "inner join project_costs on project_cost_order = project_order " .
		   "left join project_costtypes on project_costtype_id = project_cost_type " .
		   "left join product_lines on project_product_line = product_line_id ".
		   "left join postypes on postype_id = project_postype ".
		   "left join project_states on project_state_id = project_state ".
		   "left join countries on order_shop_address_country = countries.country_id ".
		   "left join users as rtcs on project_retail_coordinator = rtcs.user_id " .
		   "left join projectkinds on projectkind_id = project_projectkind ".
		   "left join users as rtos on order_retail_operator = rtos.user_id ";

		   " where " . $list_filter .
		   " order by year DESC";

	$res = mysql_query($sql_y) or dberror($sql_y);
	if ($row = mysql_fetch_assoc($res))
	{
		$page->register_action('changedprojectnumbers', 'Info about Changed Project Numbers', "changed_projectnumbers.php");
	}
}

if(has_access("has_access_to_all_projects"))
{
	$page->register_action('_project_numbers', 'Latest Project Numbers', "latest_project_numbers.php");
}

if(has_access("has_access_to_all_projects"))
{
	$page->register_action('projects_data_integrity', 'Data Integrity', "projects_data_integrity.php");
}

$page->register_action('home', 'Home', "welcome.php");