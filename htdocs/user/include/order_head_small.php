<?php
/********************************************************************

    order_head_small.php

    Show order information (Page heading only)

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-10-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-07-17
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.
*********************************************************************/

//get data to see it order is a replacement
$related_replacement_project = get_related_replacement_project($order["order_id"]);

//update project with actual exchange rates as long as budget is not approved
//check if there are items having freezed amounts
if($related_replacement_project == "" and function_exists ( "update_exchange_rates" ))
{
	if($order["order_actual_order_state_code"] < '700')
	{
		update_exchange_rates($order["order_id"], 2);
	}
}



$order_state_name = get_actual_order_state_name($order["order_actual_order_state_code"], 2);

$form->add_label("order_number", "Order Number", 0, $order["order_number"]);


if(has_access("can_view_material_replacements") 
	or has_access("can_edit_material_replacements"))
{
	if(is_array($related_replacement_project) 
		and ($related_replacement_project["order_archive_date"] == null or $related_replacement_project["order_archive_date"] == '0000-00-00'))
	{
		$link = '<a href=/user/project_edit_material_replacements.php?pid=' . $related_replacement_project["project_id"] . ' target="_blank">Replacement Order for Project ' . $related_replacement_project["project_number"] . '</a>';
		$form->add_label("replacement_project", "Related Project", RENDER_HTML, $link);
	}
	elseif(is_array($related_replacement_project))
	{
		$link = '<a href=/archive/project_edit_material_replacements.php?pid=' . $related_replacement_project["project_id"] . ' target="_blank">Replacement Order for Project ' . $related_replacement_project["project_number"] . '</a>';
		$form->add_label("replacement_project", "Related Project", RENDER_HTML, $link);
	}
}
else
{
	if(is_array($related_replacement_project) 
		and ($related_replacement_project["order_archive_date"] == null or $related_replacement_project["order_archive_date"] == '0000-00-00'))
	{
		$link = '<a href=/user/project_task_center.php?pid=' . $related_replacement_project["project_id"] . ' target="_blank">Replacement Order for Project ' . $related_replacement_project["project_number"] . '</a>';
		$form->add_label("replacement_project", "Related Project", RENDER_HTML, $link);
	}
	elseif(is_array($related_replacement_project))
	{
		$link = '<a href=/archive/project_task_center.php?pid=' . $related_replacement_project["project_id"] . ' target="_blank">Replacement Order for Project ' . $related_replacement_project["project_number"] . '</a>';
		$form->add_label("replacement_project", "Related Project", RENDER_HTML, $link);
	}
}

$form->add_label("submitted_by", "Order Date", 0, to_system_date($order["order_date"]) . " by " . $order["submitted_by"]);

$form->add_label("status", "Status", 0, $order["order_actual_order_state_code"] . " " . $order_state_name);

$line = "concat(user_name, ' ', user_firstname)";

if ($order["order_retail_operator"])
{
    $form->add_lookup("retail_operator_name", "Logistics Coordinator", "users", $line, 0, $order["order_retail_operator"]);
}
else
{
    $form->add_label("retail_operator_name", "Logistics Coordinator");
}


$client = $client_address["company"] . ", " .
          $client_address["zip"] . " " . $client_address["place"] . ", " .
          $client_address["country_name"];


$form->add_label("client_address", "Client", 0, $client);


$client_user = get_user($order["order_user"]);
$form->add_label("client_user", "Owner", 0, $client_user["name"] . " " . $client_user["firstname"]);


if($order["order_shop_address_company"])
{
	$shop = $order["order_shop_address_company"] . ", " .
			$order["order_shop_address_zip"] . " " .
			$order["order_shop_address_place"] . ", " .
			$order["order_shop_address_country_name"];
			
	$form->add_label("shop_address", "POS Location Address", 0, $shop);
}
elseif(substr($order["order_number"], 0, 1) == 'R') // replacement order
{
	$sql_r = "select country_name, posaddress_name, place_name, posaddress_zip " . 
			   "from order_item_replacements " .
			   " left join orders on order_id = order_item_replacement_order_id " .
			   "left join posorders on posorder_order = order_id " . 
			   "left join posaddresses on posaddress_id = posorder_posaddress " . 
			   "left join places on place_id = posaddress_place_id " . 
   			   "left join countries on posaddress_country = countries.country_id ".
			   " where order_item_replacement_catalog_order_id = " . dbquote($order["order_id"]);

	$res_r = mysql_query($sql_r) or dberror($sql_r);

	if ($row_r = mysql_fetch_assoc($res_r))
	{
		$shop = $row_r["posaddress_name"] . ", " .
				$row_r["posaddress_zip"] . " " .
				$row_r["place_name"] . ", " .
				$row_r["country_name"];
				
		$form->add_label("shop_address", "POS Location Address", 0, $shop);
	}
}

?>