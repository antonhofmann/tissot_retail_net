<?php
/********************************************************************

    ajx_save_cms_position.php

    Save cosheet position

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2017-12-19
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2017-12-19
    Version:        1.0.0

    Copyright (c) 2017, Tissot SA, All Rights Reserved.

*********************************************************************/

require_once "../../include/frame.php";

if(!has_access("can_edit_project_cost_real_costs") ) {
	echo "error";
}
else {
	$action = '';
	if(array_key_exists('action', $_POST)) {
		$action = $_POST['action'];
	}
	
	
	if($action == 'save') {

		$return_values = array();
		$project_id = (int)$_POST['pid'];
		$id = $_POST['id'];
		$value = strip_tags($_POST['value']);

		$id = str_replace('__costsheets_', '', $id);
		$pos = strrpos ( $id , '_');

		$field = substr($id, 0, $pos);
		$key = (int)substr($id, $pos+1, strlen($id));

		$share = 0;

		$return_values['costsheet_id'] = $key;
	
		
		if($field == 'costsheet_currency_id') {
			$sql = "select currency_exchange_rate, currency_factor 
			        from currencies 
			        where currency_id = " . dbquote($value);
			$res = mysql_query($sql) or dberror($sql);

			if($row = mysql_fetch_assoc($res)) {

				$sql = "update costsheets
						 SET $field = " . dbquote($value) . ",
						 costsheet_exchangerate = " . $row['currency_exchange_rate'] . ",
						 date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", 
						 user_modified = " . dbquote(user_login()) .
					   " where costsheet_version = 0
					and costsheet_id = " . dbquote($key);

				$result = mysql_query($sql);
			}
		}
		else {
			$sql = "update costsheets
					 SET $field = " . dbquote($value) . ", 
					 date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", 
					 user_modified = " . dbquote(user_login()) .
				   " where costsheet_version = 0
				and costsheet_id = " . dbquote($key);

			$result = mysql_query($sql);
		}


		if(is_numeric($value)) {
			//calculate amount in CHF
			$return_values['amount_chf'] = '';
			$sql = "select costsheet_exchangerate, currency_factor, costsheet_real_amount 
			        from costsheets
					left join currencies on currency_id = costsheet_currency_id 
			        where costsheet_version = 0
					and costsheet_id = " . dbquote($key);

			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res)) {

				$return_values['amount_chf'] = ($row["costsheet_exchangerate"] * $row["costsheet_real_amount"])/$row["currency_factor"];
			}
		}
		if(count($return_values) == 2) {
			echo json_encode($return_values);
		}
		else {
			echo 'error';
		}

	}
	elseif($action == 'new_line') {
		
		$data = $_POST['data'];
		$fields = explode('-', $data);

		if(count($fields) == 3)
		{

			$currency_id = '';
			$sql = "select order_client_currency, order_client_exchange_rate 
			        from projects
					left join orders on order_id = project_order
			        where project_id = " . dbquote($fields[0]);
			$res = mysql_query($sql) or dberror($sql);

			if($row = mysql_fetch_assoc($res)) {
				$currency_id = $row['order_client_currency'];
				$exchange_rate = $row['order_client_exchange_rate'];
				
			}
			
			if($currency_id > 0) {
				$sql = "INSERT into costsheets (costsheet_project_id, costsheet_version, costsheet_pcost_group_id, 
						costsheet_pcost_subgroup_id, costsheet_is_in_budget, costsheet_currency_id, costsheet_exchangerate, user_created, date_created)
						VALUES ("  . 
						dbquote($fields[0]) . ", 0, " .
						dbquote($fields[1]) . ", " . 
						dbquote($fields[2]) . ", 1, " .
						dbquote($currency_id) . ", " .
						dbquote($exchange_rate) . ", " .
						dbquote(user_login()) . ", " . 
						dbquote(date("Y-m-d H:i:s")) . ")";

				$result = mysql_query($sql);
				echo mysql_insert_id();
			}
			else {
				echo 'error';
			}
		}
		else {
			echo 'error';
		}
		
	}
	elseif($action == 'delete_line') {
		
		$project_id = (int)$_POST['pid'];
		$id = (int)$_POST['id'];
		$sql = "Delete from costsheets 
				where costsheet_version = 0
			and costsheet_id = " . dbquote($id);

		$result = mysql_query($sql);


		echo 'success';
	}
	else {

		echo 'error';
	}

}

?>