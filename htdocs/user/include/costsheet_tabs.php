<?php

if (has_access("can_view_budget_in_projects")
	or has_access("can_view_project_costs"))
{
	$page->add_tab("oveview", "Overview", "project_costs_overview.php?pid=" . param("pid"), $target = "_self", $flags = 0);
}

if (has_access("can_view_budget_in_projects")
	or has_access("can_view_project_costs")
	or has_access('can_edit_project_costs_budget'))
{
	$page->add_tab("budget", "Budget", "project_costs_budget.php?pid=" . param("pid"), $target = "_self", $flags = 0);
}

if (has_access("can_view_budget_in_projects")
	or has_access("can_view_project_costs")
	or has_access('can_edit_project_cost_real_costs'))
{
	$page->add_tab("real", "Real Costs", "project_costs_real_costs.php?pid=" . param("pid"), $target = "_self", $flags = 0);
}

if (has_access("can_view_budget_in_projects")
	or has_access("can_view_project_costs")
	or has_access("can_edit_project_costs_bids"))
{
	$page->add_tab("bids_cw", "Bids", "project_costs_bids.php?pid=" . param("pid"), $target = "_self", $flags = 0);
}
$page->add_tab("attachments", "Attachments", "project_costs_attachments.php?pid=" . param("pid"), $target = "_self", $flags = 0);

$page->tabs();

?>