<?php
/********************************************************************

    ajx_save_bid_position.php

    Save bid position

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2017-12-19
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2017-12-19
    Version:        1.0.0

    Copyright (c) 2017, Tissot SA, All Rights Reserved.

*********************************************************************/

require_once "../../include/frame.php";

if(!has_access("can_edit_project_costs_bids") ) {
	echo "error";
}
else {
	$action = '';
	if(array_key_exists('action', $_POST)) {
		$action = $_POST['action'];
	}
	

	if($action == 'save') {

		$project_id = (int)$_POST['pid'];
		$bid_id = (int)$_POST['bid'];
		$id = $_POST['id'];
		$value = strip_tags($_POST['value']);

		$id = str_replace('__costsheet_bid_positions_', '', $id);
		$pos = strrpos ( $id , '_');

		$field = substr($id, 0, $pos);
		$key = (int)substr($id, $pos+1, strlen($id));

	
		$sql = "update costsheet_bid_positions
				 SET $field = " . dbquote($value) . ", 
				 date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", 
				 user_modified = " . dbquote(user_login()) .
			   " where costsheet_bid_position_id = " . dbquote($key);

		$result = mysql_query($sql);


		//update budget
		
		$sql = "select * from costsheet_bid_positions " .
			   "left join costsheet_bids on costsheet_bid_id = costsheet_bid_position_costsheet_bid_id " .
			   "where costsheet_bid_position_is_in_budget = 1 " . 
			   " and costsheet_bid_position_id = " . dbquote($key);

		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
		
			$sql_c = "select costsheet_id " . 
					 "from costsheets " . 
					 " where costsheet_version = 0
			and costsheet_id = " . dbquote($row["costsheet_bid_position_costsheet_id"]);

			$res_c = mysql_query($sql_c) or dberror($sql_c);
			if($row_c = mysql_fetch_assoc($res_c)) // cost sheet positions exists
			{
				$fields = array();

				$fields[] = "costsheet_code = " . dbquote($row["costsheet_bid_position_code"]);
				$fields[] = "costsheet_text = " . dbquote($row["costsheet_bid_position_text"]);
				$fields[] = "costsheet_company = " . dbquote($row["costsheet_bid_company"]);

				$fields[] = "costsheet_currency_id = " . dbquote($row["costsheet_bid_currency"]);
				$fields[] = "costsheet_exchangerate = " . dbquote($row["costsheet_bid_exchange_rate"]);


				$fields[] = "costsheet_budget_amount = " . dbquote($row["costsheet_bid_position_amount"]);
				$fields[] = "date_modified = " . dbquote(date("Y-m-d H:i:s"));
				$fields[] = "user_modified = " . dbquote(user_login());

				$sql = "update costsheets set " . join(", ", $fields) . " where costsheet_version = 0
			and costsheet_id = " . $row_c["costsheet_id"];
				mysql_query($sql) or dberror($sql);
			}
			else
			{
				$fields = array();
				$values = array();

				$fields[] = "costsheet_project_id";
				$values[] = dbquote($row["costsheet_bid_position_project_id"]);

				$fields[] = "costsheet_version";
				$values[] = 0;

				$fields[] = "costsheet_pcost_group_id";
				$values[] = dbquote($row["costsheet_bid_position_pcost_group_id"]);

				$fields[] = "costsheet_pcost_subgroup_id";
				$values[] = dbquote($row["costsheet_bid_position_pcost_subgroup_id"]);

				$fields[] = "costsheet_code";
				$values[] = dbquote($row["costsheet_bid_position_code"]);

				$fields[] = "costsheet_text";
				$values[] = dbquote($row["costsheet_bid_position_text"]);

				$fields[] = "costsheet_currency_id";
				$values[] = dbquote($row["costsheet_bid_currency"]);

				$fields[] = "costsheet_exchangerate";
				$values[] = dbquote($row["costsheet_bid_exchange_rate"]);

				$fields[] = "costsheet_budget_amount";
				$values[] = dbquote($row["costsheet_bid_position_amount"]);

				$fields[] = "costsheet_company";
				$values[] = dbquote($row["costsheet_bid_company"]);

				$fields[] = "costsheet_is_in_budget";
				$values[] = 1;

				$fields[] = "date_created";
				$values[] = dbquote(date("Y-m-d H:i:s"));

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$sql = "insert into costsheets (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

				$result = mysql_query($sql) or dberror($sql);
				$new_id = mysql_insert_id();

				//connect new bid position with budget record (update)
				$sql_u = "update costsheet_bid_positions " . 
						 "set costsheet_bid_position_costsheet_id = " . dbquote($new_id) .
						 " where  costsheet_bid_position_id = " . $row["costsheet_bid_position_id"];

				$result = mysql_query($sql_u) or dberror($sql_u);

						
			}
		}

		$return_values = array();
		//get new group total;
		$sql = "select costsheet_bid_position_pcost_group_id, costsheet_bid_position_pcost_subgroup_id 
				from costsheet_bid_positions
				where costsheet_bid_position_id = " . dbquote($key);


		$result = mysql_query($sql);
		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res)) {
			$group = $row["costsheet_bid_position_pcost_group_id"];
			$subgroup = $row["costsheet_bid_position_pcost_subgroup_id"];
			$return_values['costsheet_bid_position_pcost_group_id'] = $group;
			$return_values['gr_total'] = 0;
			$return_values['costsheet_bid_position_pcost_subgroup_id'] = $subgroup;
			$return_values['sgr_total'] = 0;
		
			$sql = "select costsheet_bid_position_pcost_group_id,
					(select sum(costsheet_bid_position_amount) from costsheet_bid_positions as gr where gr.costsheet_bid_position_pcost_group_id = costsheet_bid_positions.costsheet_bid_position_pcost_group_id) as gr_total
				   from costsheet_bid_positions
				   where costsheet_bid_position_project_id = " . dbquote($project_id) .
					" and costsheet_bid_position_costsheet_bid_id = " . dbquote($bid_id) .
				   " and costsheet_bid_position_pcost_group_id = " . dbquote($group) . 
				" group by costsheet_bid_position_pcost_group_id";

			

			$result = mysql_query($sql);
			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res)) {
				$return_values['gr_total'] = $row['gr_total'];
			}

			//get new sub group total;
			$sql = "select costsheet_bid_position_pcost_subgroup_id,
					(select sum(costsheet_bid_position_amount) from costsheet_bid_positions as sgr where sgr.costsheet_bid_position_pcost_subgroup_id = costsheet_bid_positions.costsheet_bid_position_pcost_subgroup_id) as sgr_total
				   from costsheet_bid_positions
				   where costsheet_bid_position_project_id = " . dbquote($project_id) .
				   " and costsheet_bid_position_costsheet_bid_id = " . dbquote($bid_id) .
				   " and costsheet_bid_position_pcost_subgroup_id = " . dbquote($subgroup) .
				   " group by costsheet_bid_position_pcost_subgroup_id";
			$result = mysql_query($sql);
			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res)) {
				$return_values['sgr_total'] = $row['sgr_total'];
			}


			if(count($return_values) == 4) {
			echo json_encode($return_values);
			}
			else {
				echo 'error';
			}
		}
		else {
			echo 'error';
		}
		
	}
	elseif($action == 'new_line') {
		
		$data = $_POST['data'];
		$fields = explode('-', $data);

		if(count($fields) == 4)
		{

			$sql = "INSERT into costsheet_bid_positions (
					costsheet_bid_position_project_id,
					costsheet_bid_position_costsheet_bid_id,
					costsheet_bid_position_pcost_group_id, 
					costsheet_bid_position_pcost_subgroup_id, 
					user_created, date_created)
					VALUES ("  . 
					dbquote($fields[0]) . ", " .
					dbquote($fields[1]) . ", " . 
					dbquote($fields[2]) . ", " .
					dbquote($fields[3]) . ", " . 
					dbquote(user_login()) . ", " . 
					dbquote(date("Y-m-d H:i:s")) . ")";

			$result = mysql_query($sql);
			echo mysql_insert_id();
		}
		else {
			echo 'error';
		}
		
	}
	elseif($action == 'delete_line') {
		
		$project_id = (int)$_POST['pid'];
		$bid_id = (int)$_POST['bid'];
		$id = (int)$_POST['id'];


		$sql = "select costsheet_bid_position_pcost_group_id, costsheet_bid_position_pcost_subgroup_id 
				from costsheet_bid_positions
				where costsheet_bid_position_id = " . dbquote($id);

		$result = mysql_query($sql);
		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res)) {
			
			$group = $row["costsheet_bid_position_pcost_group_id"];
			$subgroup = $row["costsheet_bid_position_pcost_subgroup_id"];

			$return_values = array();
			$return_values['costsheet_bid_position_pcost_group_id'] = $group;
			$return_values['gr_total'] = 0;
			$return_values['costsheet_bid_position_pcost_subgroup_id'] = $subgroup;
			$return_values['sgr_total'] = 0;


			$sql = "Delete from costsheet_bid_positions 
					where costsheet_bid_position_id = " . dbquote($id);

			$result = mysql_query($sql);


			// new grou�p totals
			$sql = "select costsheet_bid_position_pcost_group_id,
					(select sum(costsheet_bid_position_amount) from costsheet_bid_positions as gr where gr.costsheet_bid_position_pcost_group_id = costsheet_bid_positions.costsheet_bid_position_pcost_group_id) as gr_total
				   from costsheet_bid_positions
				   where costsheet_bid_position_project_id = " . dbquote($project_id) .
					" and costsheet_bid_position_costsheet_bid_id = " . dbquote($bid_id) .
				   " and costsheet_bid_position_pcost_group_id = " . dbquote($group) . 
				" group by costsheet_bid_position_pcost_group_id";

			

			$result = mysql_query($sql);
			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res)) {
				$return_values['gr_total'] = $row['gr_total'];
			}

			//get new sub group total;
			$sql = "select costsheet_bid_position_pcost_subgroup_id,
					(select sum(costsheet_bid_position_amount) from costsheet_bid_positions as sgr where sgr.costsheet_bid_position_pcost_subgroup_id = costsheet_bid_positions.costsheet_bid_position_pcost_subgroup_id) as sgr_total
				   from costsheet_bid_positions
				   where costsheet_bid_position_project_id = " . dbquote($project_id) .
				   " and costsheet_bid_position_costsheet_bid_id = " . dbquote($bid_id) .
				   " and costsheet_bid_position_pcost_subgroup_id = " . dbquote($subgroup) .
				   " group by costsheet_bid_position_pcost_subgroup_id";
			$result = mysql_query($sql);
			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res)) {
				$return_values['sgr_total'] = $row['sgr_total'];
			}
					
			echo json_encode($return_values);
		}
		else {
			echo 'error';
		}
	}
	elseif($action == 'update_budget') {
		
		$project_id = (int)$_POST['pid'];
		$bid_id = (int)$_POST['bid'];
		$id = $_POST['id'];
		$value = strip_tags($_POST['value']);


		$id = str_replace('__costsheet_bid_positions_', '', $id);
		$pos = strrpos ( $id , '_');

		$field = substr($id, 0, $pos);
		$key = (int)substr($id, $pos+1, strlen($id));

		$sql = "update costsheet_bid_positions
				 SET $field = " . dbquote($value) . ", 
				 date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", 
				 user_modified = " . dbquote(user_login()) .
			   " where costsheet_bid_position_id = " . dbquote($key);


		$result = mysql_query($sql);

		if($value == 0) {
			
			$sql = "select costsheet_bid_position_costsheet_id from costsheet_bid_positions " .
				   "where costsheet_bid_position_id = " . dbquote($key);

			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$sql = "delete from costsheets where costsheet_version = 0
					and costsheet_id = " . dbquote($row["costsheet_bid_position_costsheet_id"]);

				mysql_query($sql) or dberror($sql);
			}
	
		}
		else {
		
			$sql = "select * from costsheet_bid_positions " .
				   "left join costsheet_bids on costsheet_bid_id = costsheet_bid_position_costsheet_bid_id " . 
				   "where costsheet_bid_position_is_in_budget = 1 " . 
				   " and costsheet_bid_position_costsheet_bid_id = " . dbquote($bid_id);

			$res = mysql_query($sql) or dberror($sql);
			while($row = mysql_fetch_assoc($res))
			{
			
				$sql_c = "select costsheet_id " . 
						 "from costsheets " . 
						 " where costsheet_version = 0
				and costsheet_id = " . dbquote($row["costsheet_bid_position_costsheet_id"]);

				$res_c = mysql_query($sql_c) or dberror($sql_c);
				if($row_c = mysql_fetch_assoc($res_c)) // cost sheet positions exists
				{
					$fields = array();

					$fields[] = "costsheet_code = " . dbquote($row["costsheet_bid_position_code"]);
					$fields[] = "costsheet_text = " . dbquote($row["costsheet_bid_position_text"]);
					$fields[] = "costsheet_company = " . dbquote($row["costsheet_bid_company"]);

					$fields[] = "costsheet_currency_id = " . dbquote($row["costsheet_bid_currency"]);
					$fields[] = "costsheet_exchangerate = " . dbquote($row["costsheet_bid_exchange_rate"]);


					$fields[] = "costsheet_budget_amount = " . dbquote($row["costsheet_bid_position_amount"]);
					$fields[] = "date_modified = " . dbquote(date("Y-m-d H:i:s"));
					$fields[] = "user_modified = " . dbquote(user_login());

					$sql = "update costsheets set " . join(", ", $fields) . " where costsheet_version = 0
				and costsheet_id = " . $row_c["costsheet_id"];
					mysql_query($sql) or dberror($sql);

					echo $sql;

				}
				else
				{
					$fields = array();
					$values = array();

					$fields[] = "costsheet_project_id";
					$values[] = dbquote($row["costsheet_bid_position_project_id"]);

					$fields[] = "costsheet_version";
					$values[] = 0;

					$fields[] = "costsheet_pcost_group_id";
					$values[] = dbquote($row["costsheet_bid_position_pcost_group_id"]);

					$fields[] = "costsheet_pcost_subgroup_id";
					$values[] = dbquote($row["costsheet_bid_position_pcost_subgroup_id"]);

					$fields[] = "costsheet_code";
					$values[] = dbquote($row["costsheet_bid_position_code"]);

					$fields[] = "costsheet_text";
					$values[] = dbquote($row["costsheet_bid_position_text"]);

					$fields[] = "costsheet_currency_id";
					$values[] = dbquote($row["costsheet_bid_currency"]);

					$fields[] = "costsheet_exchangerate";
					$values[] = dbquote($row["costsheet_bid_exchange_rate"]);

					$fields[] = "costsheet_budget_amount";
					$values[] = dbquote($row["costsheet_bid_position_amount"]);

					$fields[] = "costsheet_company";
					$values[] = dbquote($row["costsheet_bid_company"]);

					$fields[] = "costsheet_is_in_budget";
					$values[] = 1;

					$fields[] = "date_created";
					$values[] = dbquote(date("Y-m-d H:i:s"));

					$fields[] = "user_created";
					$values[] = dbquote(user_login());

					$sql = "insert into costsheets (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

					$result = mysql_query($sql) or dberror($sql);
					$new_id = mysql_insert_id();

					//connect new bid position with budget record (update)
					$sql_u = "update costsheet_bid_positions " . 
							 "set costsheet_bid_position_costsheet_id = " . dbquote($new_id) .
							 " where  costsheet_bid_position_id = " . $row["costsheet_bid_position_id"];

					$result = mysql_query($sql_u) or dberror($sql_u);
				}
			}
		}
		echo 'success';

	}
	else {

		echo 'error';
	}

}

?>