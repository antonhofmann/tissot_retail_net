<?php

session_name("retailnet");
session_start();

	
require $_SERVER["DOCUMENT_ROOT"] . "/include/frame.php";


$model = new Model(Connector::DB_CORE);

$query = "select standard_shipping_document_id, standard_shipping_document_name " . 
	     " from orders " .
	     " left join address_shipping_documents on address_shipping_document_address_id = order_client_address " .
	     " left join standard_shipping_documents on standard_shipping_document_id = address_shipping_document_document_id " . 
	     " where order_id = ? " . 
	     " order by standard_shipping_document_name";



$sth = $model->db->prepare($query);
$sth->execute(array(
	$_REQUEST["fields"]["order_id"]
));
$documents = $sth->fetchAll();

?>


<!doctype html>
<!--[if IE 7]>    <html class="ie ie7" lang="en"> <![endif]-->
<!--[if gt IE 7]>    <html class="ie" lang="en"> <![endif]-->
<!--[if !IE]><!--><html lang="en"> <!--<![endif]--> 
<head>
	<meta http-equiv='cache-control' content='no-cache'>
	<meta http-equiv='expires' content='0'>
	<meta http-equiv='pragma' content='no-cache'>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	

	<script src="/public/scripts/jquery/1.7.2.js" type="text/javascript"></script>
	<script src="/public/scripts/jquery/migrate.min.js" type="text/javascript"></script>
	<script src="/public/scripts/jquery/jquery.ui.1.10.4.min.js"></script>
	<link type="text/css" href="/public/scripts/jquery/jquery.ui.1.10.4.min.css" rel="stylesheet" />
	
	<link rel="stylesheet" type="text/css" href="/public/scripts/fancybox/fancybox.css?v=2.1.5" />
	<script type="text/javascript" src="/public/scripts/fancybox/fancybox.js?v=2.1.5"></script>

	<link href="/public/scripts/adomodal/adomodal.css" rel="stylesheet" type="text/css" />
	<script src="/public/scripts/adomodal/adomodal.js" type="text/javascript"></script> 
	<script type="text/javascript" src="/public/scripts/jquery.actual.min.js"></script>
	<link rel="stylesheet" href="/public/css/spinners.css">
	
	<script type="text/javascript" src="/public/scripts/jgrowl/jgrowl.js"></script>
	<link rel="stylesheet" type="text/css" href="/public/scripts/jgrowl/jgrowl.css" />
	
	<script type="text/javascript" src="/public/scripts/message.js"></script>
	<script type="text/javascript" src="/public/scripts/retailnet.js"></script>

	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/modal.css" />
	<link rel="stylesheet" href="/public/scripts/bootstrap/css/bootstrap.compiled.css">
	<link rel="stylesheet" href="/public/css/font-awesome/css/font-awesome.min.css">
	
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/modal.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/reset.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/layout.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/table.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/form.css" />
	<link rel="stylesheet" type="text/css" href="/public/css/gui.css" />
	<link rel="stylesheet" type="text/css" href="/public/themes/default/css/gui.css" />
	
	<link rel="stylesheet" href="/public/scripts/fileuploader/css/style.css">
	<link rel='stylesheet' href='/public/scripts/fileuploader/css/blueimp-gallery.min.css'>
	<link rel="stylesheet" href="/public/scripts/fileuploader/css/jquery.fileupload.css">
	<link rel="stylesheet" href="/public/scripts/fileuploader/css/jquery.fileupload-ui.css">

	<style type="text/css">
	
		body,
		body.adomat {
			background: #eeeeee !important;
			min-width: 400px !important;
		}
		
		.modalbox-container {
			display: block !important;
			height: 535px;
		}
		
		
	</style>
	
</head>
<body class="adomat">
<div class="adomat modalbox-container">
	<div id="fileUploader" 
		class="adomat modalbox bootstrap fileuploader modal-750"
		data-url="/applications/helpers/file.uploader.php"
		data-upload-template-id="retailnet-template-upload"
		data-download-template-id="retailnet-template-download"
		data-auto-upload="true"
		data-sequential-uploads="true">
		<form method="POST" enctype="multipart/form-data" action="<?php echo $_REQUEST['action'] ?>" >
			<?php 
				if ($_REQUEST['fields']) {
					foreach ($_REQUEST['fields'] as $k => $v) {
						echo "<input type=hidden name='$k' class='$k' value='$v' >";
					}
				}
			?>	
			<div class="modalbox-header" >
				<div class='title'><?php echo $_REQUEST['title'] ?></div>
				<div class='subtitle'><?php echo $_REQUEST['subtitle'] ?></div>
				<div class='modal-infotex'><?php echo $_REQUEST['infotext'] ?></div>
			</div>

			<div class="modalbox-content-container" >
				<div class="modalbox-content" >
					<!-- The table listing the files available for upload/download -->
					<table role="presentation" class="table table-striped">
						<tbody class="files"></tbody>
					</table>
				</div>
			</div>

			
			<div class="modalbox-footer" >
				<div class="modalbox-actions fileupload-buttonbar" >
					<span class="btn btn-default btn-sm modal-close pull-left">
						<span>Close</span>
					</span>
					<span class="btn btn-sm btn-success fileinput-button">
						<i class="fa fa-plus"></i>
						<span>Add Other Files</span>
						<input type="file" name="files[]" multiple>
					</span>
					
					<button type="button" class="btn btn-sm btn-primary uploader">
						<i class="fa fa-arrow-circle-up"></i>
						<span>Save Uploaded Files</span>
					</button>	
				</div>
			</div>
	    </form>
	</div>
</div>
<!-- The blueimp Gallery widget -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>
<!-- The template to display files available for upload -->
<script id="retailnet-template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td width="80px" align="center" >
            <span class="preview"></span>
        </td>
		<td>
			<span class="input-group-addon">
					<select name="doc[{%=file.id%}]" data-value="{%=file.doc_id%}" class="required">
						<option value="">Select</option>
						<?php
						foreach($documents as $key=>$document)
						{
							echo '<option value="' . $document["standard_shipping_document_id"]. '">' . $document["standard_shipping_document_name"]. '</option>';
						}
						?>
						
					</select>
				</span>
			<input type="title[]" class="form-control input-sm required" value="{%=file.title%}" placeholder="File Title">
			<span class="help-block">
				{% if (!i && !o.options.autoUpload) { %}
                	<button class="btn btn-xs btn-primary start chankuploader" disabled>
                    	<i class="glyphicon glyphicon-upload"></i>
                    	<span>Upload</span>
                	</button>
            	{% } %}
				{% if (!i) { %}
                	<button class="btn btn-xs btn-warning cancel">
                    	<i class="fa fa-minus-circle"></i>
                    	<span>Cancel</span>
                	</button>&nbsp;
            	{% } %}
				<span class="name">{%=file.name%}</span>&nbsp;
				<span class="size">Processing...</span>
			</span>
			<strong class="error text-danger"></strong>
			<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
		</td>
    </tr>
{% } %}
	</script>
<!-- The template to display files available for download -->
<script id="retailnet-template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td width="80px" align=center >
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.title%}" download="{%=file.name%}" {%=file.gallery ? 'data-gallery':''%} ><img src="{%=file.thumbnailUrl%}" style="max-width:80px"></a>
                {% } %}
            </span>
        </td>
		<td>
			<div class="input-group input-group-sm">
				<span class="input-group-addon">
					<select name="doc[{%=file.id%}]" data-value="{%=file.sdoc_id%}" class="required">
						<option value="">Select</option>
						<?php
						foreach($documents as $key=>$document)
						{
							echo '<option value="' . $document["standard_shipping_document_id"]. '">' . $document["standard_shipping_document_name"]. '</option>';
						}
						?>
					</select>
				</span>
				<input type="text" name="title[{%=file.id%}]" class="form-control input-sm required file-title" value="{%=file.title%}" placeholder="File Title">
			</div>
			<input type="hidden" name="file[{%=file.id%}]" class="form-control input-sm required" value="{%=file.url%}">
			<span class="help-block">
				<span class="filename name">
					{% if (file.url) { %}
            			<a class="btn btn-xs btn-info" href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.gallery?'data-gallery':''%}>
							<i class="fa fa-file"></i>
                    		<span>{%=file.gallery ? 'Show File' : 'Download File' %}</span>
						</a>&nbsp;
            		{% } %}
					{% if (file.deleteUrl) { %}
                		<button class="btn btn-xs btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                	    	<i class="fa fa-times-circle"></i>
                 	   		<span>Delete</span>
                		</button>
          	  		{% } else { %}
                		<button class="btn btn-xs btn-warning cancel">
                    		<i class="fa fa-minus-circle"></i>
                    		<span>Cancel</span>
                		</button>
						{%=file.name%}
            		{% } %}
					&nbsp;<span class="size">{%=o.formatFileSize(file.size)%}</span>
				</span>
			</span>
			{% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
		</td>
    </tr>
{% } %}
</script>
<script src='/public/scripts/fileuploader/js/jquery.ui.widget.js'></script>
<script src="/public/scripts/fileuploader/js/tmpl.min.js"></script>
<script src="/public/scripts/fileuploader/js/load-image.min.js"></script>
<script src="/public/scripts/fileuploader/js/canvastoblob.min.js"></script>
<script src='/public/scripts/fileuploader/js/jquery.blueimp-gallery.min.js'></script>
<script src="/public/scripts/fileuploader/js/jquery.iframe-transport.js"></script>
<script src="/public/scripts/fileuploader/js/jquery.fileupload.js"></script>
<script src="/public/scripts/fileuploader/js/jquery.fileupload-process.js"></script>
<script src="/public/scripts/fileuploader/js/jquery.fileupload-image.js"></script>
<script src="/public/scripts/fileuploader/js/jquery.fileupload-audio.js"></script>
<script src="/public/scripts/fileuploader/js/jquery.fileupload-video.js"></script>
<script src="/public/scripts/fileuploader/js/jquery.fileupload-validate.js"></script>
<script src="/public/scripts/fileuploader/js/jquery.fileupload-ui.js"></script>
<!--[if (gte IE 8)&(lt IE 10)]><script src="/public/scripts/fileuploader/js/cors/jquery.xdr-transport.js"></script><![endif]-->
<script src="/public/js/file.uploader.js"></script>
</body>
</html>