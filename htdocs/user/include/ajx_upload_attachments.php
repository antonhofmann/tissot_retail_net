<?php 
	
/********************************************************************

ajx_upload_attachments.php

Upload Design Briefing documents

Created by:     Anton Hofmann (anton.hofmann@codingroom.ch)
Date created:   2017-12-20
Modified by:    Anton Hofmann (anton.hofmann@codingroom.ch)
Date modified:  2017-12-201
Version:        1.0.0

Copyright (c) 2017, Tissot, All Rights Reserved.
*********************************************************************/
	
session_name("retailnet");
session_start();


require "../../include/frame.php";
require "get_functions.php";

switch ($_REQUEST['section']) {
	
	// save files
	case 'save':
		if( (has_access("can_add_attachments_in_projects") 
			 or has_access("can_edit_attachment_data_in_projects"))
		    and $_REQUEST["id"] 
		    and $_REQUEST["file"]) {
			
			$model = new Model(Connector::DB_CORE);
			
			$project = get_project($_REQUEST["id"]);
			$order_id = $project["project_order"];
			$orderNumber = $project["order_number"];

			$category = $_REQUEST["category"];
	
			foreach($_REQUEST["file"] as $key => $file) {
				
				// insert new file
				if (strpos($file, '/tmp/') !== false) {
					
					$dir = "/files/orders/$orderNumber";
					$filename = basename($file);
					$extension = File::extension($filename);
					
					// move temporary file to target destination
					$copy = Upload::moveTemporaryFile($file, $dir);
					
					if ($copy) { 
						
						$type = $model->query("
							SELECT file_type_id 
							FROM file_types 
							WHERE file_type_extension LIKE '%$extension%'
						")->fetch();
						
						// save file in db
						$sth = $model->db->prepare("
							INSERT INTO order_files (
								order_file_order,
								order_file_title,
								order_file_path,
								order_file_type,
								order_file_category,
								order_file_owner,
								date_created,
								user_created
							) VALUES (
								:order, :title, :file, :type, :category, :owner, CURRENT_TIMESTAMP, :user
							)
						");


						$action = $sth->execute(array(
							'order' => $order_id,
							'title' => $_REQUEST['title'][$key],
							'file' => "$dir/$filename",
							'type' => $type['file_type_id'],
							'category' => $category,
							'owner' => User::instance()->id,
							'user' => User::instance()->login
						));

						// save file access in db
						$sth = $model->db->prepare("
							INSERT INTO order_file_addresses (
								order_file_address_file,
								order_file_address_address,
								date_created,
								user_created
							) VALUES (
								:file_id, :address, CURRENT_TIMESTAMP, :user
							)
						");

						$action = $sth->execute(array(
							'file_id' => $model->db->lastInsertId(),
							'address' => User::instance()->address,
							'user' => User::instance()->login
						));
					}
				} 
				// update file data
				elseif (isset($_REQUEST['title'][$key])) {
					
					// order file exist
					$result = $model->query("
						SELECT order_file_id 
						FROM order_files 
						WHERE order_file_id = $key
					")->fetch();
					
					// update file title
					if ($result['order_file_id']) { 
						
						$sth = $model->db->prepare("
							UPDATE order_files SET
								order_file_title = :title
							WHERE order_file_id = :id
						");
						
						$action = $sth->execute(array(
							'title' => $_REQUEST['title'][$key],
							'id' => $key
						));
					}
				}
			}
			
			$response = array(
				'response' => $action,
				'reload' => true
			);
		}
		
	break;
	
	
	// delete file
	case 'delete':
		
		if( has_access("can_delete_attachment_in_projects")
		    and $_REQUEST["id"] ) {
			$id = $_REQUEST['id'];
			$model = new Model(Connector::DB_CORE);
				
			$file = $model->query("
				SELECT *
				FROM order_files
				WHERE order_file_id = $id
			")->fetch();
				
			if (file_exists($_SERVER['DOCUMENT_ROOT'].$file['order_file_path'])) {
		
				$filename = basename($file['order_file_path']);
				$dirpath = file::dirpath($file['order_file_path']);
		
				file::remove($file['order_file_path']);
				file::remove("$dirpath/thumbnail/$filename");
		
				$delete = $model->db->exec("
					DELETE FROM order_file_addresses
					WHERE order_file_address_file = $id
				");
				
				$delete = $model->db->exec("
					DELETE FROM order_files
					WHERE order_file_id = $id
				");
		
				$response = array($filename => $delete);
			}
		}
	
	break;
	
	// load files
	default:

		if( has_access("can_view_attachments_in_projects")
		    and $_REQUEST["id"]
			and $_REQUEST["category"]) {

			$id = $_REQUEST['id'];
			$category = $_REQUEST['category'];
			
			$project = get_project($_REQUEST['id']);
			$order = $project["project_order"];
			
			$model = new Model(Connector::DB_CORE);
				
			if( has_access("has_access_to_all_attachments_in_projects")) {
				$result = $model->query("
					SELECT DISTINCT
						order_file_id AS id,
						order_file_title AS title,
						order_file_description AS description,
						order_file_path AS file
					FROM order_files
					LEFT JOIN order_file_categories ON order_file_category_id = order_file_category
					LEFT JOIN users ON user_id = order_file_owner
					LEFT JOIN file_types ON order_file_type = file_type_id
					WHERE order_file_category_id = $category AND order_file_order = $order AND order_file_id > 0
				")->fetchAll();
			}
			else {

				//get users access to files
				$file_ids = array();
				
				$result = $model->query("
					SELECT DISTINCT
					select order_file_address_file from order_file_addresses
					where order_file_address_address = " . User::instance()->address . "
				")->fetchAll();
				
				

				
				foreach($result as $key=>$row) {
					$file_ids[] = $row['order_file_address_file'];
				}

				$file_filter = "";
				if(count($file_ids) > 0) {
					$file_filter = ' or order_file_id IN (' . implode(',', $file_ids) . ')';
				}

				$result = $model->query("
					SELECT DISTINCT
						order_file_id AS id,
						order_file_title AS title,
						order_file_description AS description,
						order_file_path AS file
					FROM order_files
					LEFT JOIN order_file_categories ON order_file_category_id = order_file_category
					LEFT JOIN users ON user_id = order_file_owner
					LEFT JOIN file_types ON order_file_type = file_type_id
					WHERE order_file_category_id = $category AND order_file_order = $order AND order_file_id > 0
					and (order_file_owner = " . User::instance()->id . $file_filter . ")
				")->fetchAll();
			
			
			}
			
			$response = Upload::get($result);
		}
		
	break;
}

header('Content-Type: text/json');
echo json_encode($response);

?>	