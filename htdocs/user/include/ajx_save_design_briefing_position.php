<?php
/********************************************************************

    ajx_save_design_briefing_position.php

    Save design briefing position

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2017-12-19
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2017-12-19
    Version:        1.0.0

    Copyright (c) 2017, Tissot, All Rights Reserved.

*********************************************************************/

require_once "../../include/frame.php";

if(!has_access("can_edit_design_briefing") ) {
	echo "error";
}
else {
	
	$id = (int)$_POST['id'];
	$value = strip_tags($_POST['value']);
	$pid = (int)$_POST['pid'];
	$oid = (int)$_POST['oid'];


	$sql = "update project_design_brief_positions
			 SET project_design_brief_position_content = " . dbquote($value) . ", 
			 date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", 
			 user_modified = " . dbquote(user_login()) .
		   " where project_design_brief_position_id = " . dbquote($id);

	$resuslt = mysql_query($sql);


	//check if pos is in pipeline and pos has an actual opening date
	$table = '';
	$pos_address_id = '';
	$actual_opening_date = '';
	$sql = "select posorder_parent_table, posorder_posaddress, project_actual_opening_date
	        from posorderspipeline
			left join projects on project_order = posorder_order
			where posorder_order = " . dbquote($oid);
	$res = mysql_query($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$table = $row['posorder_parent_table'];
		$pos_address_id = $row['posorder_posaddress'];
		$actual_opening_date = $row['project_actual_opening_date'];
	}
	else {
		$sql = "select posorder_posaddress, project_actual_opening_date
				from posorders
				left join projects on project_order = posorder_order
				where posorder_order = " . dbquote($oid);
		$res = mysql_query($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$table = 'posaddresses';
			$pos_address_id = $row['posorder_posaddress'];
			$actual_opening_date = $row['project_actual_opening_date'];
		}
	}

	//update project data
	$sql = "select project_design_brief_position_position_id from 
	        project_design_brief_positions
			where project_design_brief_position_id = " . dbquote($id);
	
	$res = mysql_query($sql);
	if($row = mysql_fetch_assoc($res))
	{
		if($row['project_design_brief_position_position_id'] == 79) //construction starting date in projects
		{
			$sql = "update projects
					SET project_construction_startdate = " . dbquote(from_system_date($value)) .
				   " where project_id = " . dbquote($pid);

			$result = mysql_query($sql);

		}
		elseif($row['project_design_brief_position_position_id'] == 84
			or $row['project_design_brief_position_position_id'] == 130
			or $row['project_design_brief_position_position_id'] == 127) //total surface
		{
			$sql = "update project_costs
					SET project_cost_totalsqms = " . dbquote($value) .
				   " where project_cost_order = " . dbquote($oid);

			$result = mysql_query($sql);

			if($pos_address_id > 0
				and $table = 'posaddresses' 
				and $actual_opening_date != NULL 
				and $actual_opening_date <> '0000-00-00') {
			
				$sql = "update posaddresses
						SET posaddress_store_totalsurface = " . dbquote($value) .
						" where posaddress_id = " . dbquote($pos_address_id);

				$result = mysql_query($sql);
			}
			elseif($pos_address_id > 0
				and $table = 'posaddressespipeline') {
			
				$sql = "update posaddressespipeline
						SET posaddress_store_totalsurface = " . dbquote($value) .
						" where posaddress_id = " . dbquote($pos_address_id);

				$result = mysql_query($sql);
			}

	
		}
		elseif($row['project_design_brief_position_position_id'] == 74
			or $row['project_design_brief_position_position_id'] == 131
			or $row['project_design_brief_position_position_id'] == 128) //original sales surface
		{
			$sql = "update project_costs
					SET project_cost_original_sqms = " . dbquote($value) .
					", project_cost_sqms = " . dbquote($value) .
				   " where project_cost_order = " . dbquote($oid);

			$result = mysql_query($sql);

			$sql = "update project_costs
					SET project_cost_sqms = " . dbquote($value) .
				   " where project_cost_order = " . dbquote($oid);

			$result = mysql_query($sql);

			if($pos_address_id > 0
				and $table = 'posaddresses' 
				and $actual_opening_date != NULL 
				and $actual_opening_date <> '0000-00-00') {
			
				$sql = "update posaddresses
						SET posaddress_store_retailarea = " . dbquote($value) .
						" where posaddress_id = " . dbquote($pos_address_id);

				$result = mysql_query($sql);
			}
			elseif($pos_address_id > 0
				and $table = 'posaddressespipeline') {
			
				$sql = "update posaddressespipeline
						SET posaddress_store_retailarea = " . dbquote($value) .
						" where posaddress_id = " . dbquote($pos_address_id);

				$result = mysql_query($sql);
			}
		}
		elseif($row['project_design_brief_position_position_id'] == 85
			or $row['project_design_brief_position_position_id'] == 132
			or $row['project_design_brief_position_position_id'] == 129) //backoffice
		{
			$sql = "update project_costs
					SET project_cost_backofficesqms = " . dbquote($value) .
				   " where project_cost_order = " . dbquote($oid);

			$result = mysql_query($sql);

			if($pos_address_id > 0
				and $table = 'posaddresses' 
				and $actual_opening_date != NULL 
				and $actual_opening_date <> '0000-00-00') {
			
				$sql = "update posaddresses
						SET posaddress_store_backoffice = " . dbquote($value) .
						" where posaddress_id = " . dbquote($pos_address_id);

				$result = mysql_query($sql);
			}
			elseif($pos_address_id > 0
				and $table = 'posaddressespipeline') {
			
				$sql = "update posaddressespipeline
						SET posaddress_store_backoffice = " . dbquote($value) .
						" where posaddress_id = " . dbquote($pos_address_id);

				$result = mysql_query($sql);
			}
		}
	}
	

	echo 'success';

}

?>