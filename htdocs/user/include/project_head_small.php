<?php
/********************************************************************

    project_head_small.php

    Show order information (Page heading only)

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-10-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-05-17
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.


*********************************************************************/
//update project with actual exchange rates as long as budget is not approved
//check if there are items having freezed amounts
if(function_exists ( "update_exchange_rates" ))
{
	$sql_i = "select count(order_item_id) as num_recs " . 
			 " from order_items " . 
			 " where order_item_system_price_freezed > 0 " . 
			 " and order_item_order = " . $project["project_order"];

	$res = mysql_query($sql_i) or dberror($sql_i);
	$row = mysql_fetch_assoc($res);
	if($row["num_recs"] == 0)
	{
		update_exchange_rates($project["project_order"], 1);
	}
}

if(!isset($user_roles))
{
	$user_roles = get_user_roles(user_id());
}

//get order state
$order_state_name = get_actual_order_state_name($project["order_actual_order_state_code"], 1);
$development_state = get_project_development_status($project["project_order"]);
/*
if(has_access("can_see_logistic_status"))
{
	$logistic_state = get_logistic_state($project["project_order"], user_id(), $project["order_actual_order_state_code"]);
	$offer_state = get_offer_state($project["project_order"], user_id(), $project["order_actual_order_state_code"]);
}
*/

if(($project["project_projectkind"] == 1 
     and $project["project_actual_opening_date"] != NULL 
	 and $project["project_actual_opening_date"] != '0000-00-00') 
    or $project["project_projectkind"] == 2 
	or $project["project_projectkind"] == 3
	or $project["project_projectkind"] == 4
	or $project["project_projectkind"] == 5
	or $project["project_projectkind"] == 7
	or $project["project_projectkind"] == 8) // renovation or takeover/renovation or takeover, lease renewal, equipment
{
	$renovated_pos_id = get_renovated_pos_info($project["project_order"]);
	if ($renovated_pos_id > 0)
	{
		if (has_access("can_edit_pos_data") or has_access("can_view_pos_data"))
		{ 
			$shop_address = $project["order_shop_address_zip"] . " " .
							$project["order_shop_address_place"] . ", " .
							$project["order_shop_address_country_name"];
			
			$tmp = '<a href="../pos/posindex_pos.php?country=' . $project["order_shop_address_country"] . '&ltf=all&ostate=&province=&id=' . $renovated_pos_id . '" target="_blank">' . $project["order_shop_address_company"] . '</a>, ' . $shop_address;
		}
		else
		{ 
			$shop_address = $project["order_shop_address_zip"] . " " .
							$project["order_shop_address_place"] . ", " .
							$project["order_shop_address_country_name"];
			
			$tmp = $shop_address ;
		}
		
		$form->add_label("shop_address_label", "POS Location Address", RENDER_HTML, $tmp);
	}
	else
	{	
		$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];
		$form->add_label("shop_address_label", "POS Location Address", 0, $shop);
	}
}
elseif(($project["project_projectkind"] == 6 or $project["project_projectkind"] == 9)
	and $project["project_relocated_posaddress_id"] > 0) // relocation
{
	if ($project["project_actual_opening_date"] != NULL 
		and $project["project_actual_opening_date"] != '0000-00-00'
	    and (has_access("can_edit_pos_data") or has_access("can_view_pos_data")))
	{ 
		$pos_id = get_renovated_pos_info($project["project_order"]);

		$shop_address = $project["order_shop_address_zip"] . " " .
		$project["order_shop_address_place"] . ", " .
		$project["order_shop_address_country_name"];

		
		$shop = '<a href="../pos/posindex_pos.php?country=' . $project["order_shop_address_country"] . '&ltf=all&ostate=&province=&id=' . $pos_id . '" target="_blank">' . $project["order_shop_address_company"] . '</a>, ' . $shop_address;
	}
	else
	{
		$shop = $project["order_shop_address_company"] . ", " .
		$project["order_shop_address_zip"] . " " .
		$project["order_shop_address_place"] . ", " .
		$project["order_shop_address_country_name"];
	}
	
	
	$form->add_label("shop_address_label", "POS Location Address", RENDER_HTML, $shop);
	
	$relocated_pos = get_relocated_pos_info($project["project_relocated_posaddress_id"]);

	if (count($relocated_pos) > 0)
	{
		
		$shop1 = $relocated_pos["posaddress_name"];
		$shop2 = ", " .
				$relocated_pos["posaddress_zip"] . " " .
				$relocated_pos["place_name"] . ", " .
				$relocated_pos["country_name"];
	
		if (has_access("can_edit_pos_data") or has_access("can_view_pos_data"))
		{ 
			$tmp = '<a href="../pos/posindex_pos.php?country=' . $project["order_shop_address_country"] . '&ltf=all&ostate=&province=&id=' . $project["project_relocated_posaddress_id"] . '" target="_blank">' . $shop1 . '</a>' . $shop2;
			$form->add_label("relocated_pos_label", "Relocated POS", RENDER_HTML, $tmp );
		}
		else
		{
			$form->add_label("relocated_pos_label", "Relocated POS", 0, $shop );
		}
	}
}
else
{
	$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];
	$form->add_label("shop_address_label", "POS Location Address", 0, $shop);
}




$client = $client_address["company"] . ", " .
          $client_address["zip"] . " " . $client_address["place"] . ", " .
          $client_address["country_name"];




$form->add_label("client_address_label", "Client", 0, $client);

$tmp = get_address($project["order_franchisee_address_id"]);

$franchisee = $tmp["company"] . ", " .
        $tmp["zip"] . " " .
        $tmp["place"] . ", " .
        $tmp["country_name"];


if(has_access("can_edit_posindex") or has_access("can_view_posindex"))
{
	$link = '<a href="/pos/poscompany.php?country=' . $project["order_franchisee_address_country"] . '&address_filter=a&id=' . $project["order_franchisee_address_id"]. '" target="_blank">' . $franchisee . '</a>';
	
	$franchisee = $link;
}
elseif(has_access("can_view_his_posindex") or has_access("can_edit_his_posindex"))
{

	$user = get_user(user_id());

	$country_filter = "";
	$tmp = array();
	$sql_c = "select * from country_access " .
		   "where country_access_user = " . user_id();


	$res = mysql_query($sql_c) or dberror($sql_c);

	while ($row = mysql_fetch_assoc($res))
	{            
		$tmp[] = $row["country_access_country"];
	}
	$tmp[] = $user["country"];
	if(in_array($project["order_franchisee_address_country"], $tmp))
	{
		$link = '<a href="/pos/poscompany.php?country=' . $project["order_franchisee_address_country"] . '&address_filter=a&id=' . $project["order_franchisee_address_id"]. '" target="_blank">' . $franchisee . '</a>';
	
		$franchisee = $link;
	}
}

if($project["project_cost_type"] != 6 and $project["project_postype"] != 2)
{
	$form->add_label("franchisee_address_label", "Owner Company", RENDER_HTML, $franchisee);		
}
else
{
	$form->add_label("franchisee_address_label", "Owner Company", RENDER_HTML, $franchisee);		
}

$form->add_lookup("production_type", "Production Type", "production_types",
                  "production_type_name",HIDEEMPTY ,$project["project_production_type"]);

if($project["project_popup_name"])
{
	$form->add_label("type3_label", "Project Legal Type / Project Type", 0, $project["project_costtype_text"] . " / " . $project["projectkind_name"] . ", " . $project["project_popup_name"]);
}
else
{
	$form->add_label("type3_label", "Project Legal Type / Project Type", 0, $project["project_costtype_text"] . " / " . $project["projectkind_name"]);
}

$form->add_label("stype3_label", "Project Type Subclass", 0, $project["project_type_subclass_name"]);

if($project["possubclass_name"])
{
	$form->add_label("project_postype_label", "POS Type / Subclass", 0, $project["postype_name"] . " / " . $project["possubclass_name"]);
}
else
{
	$form->add_label("project_postype_name_label", "POS Type / Subclass", 0, $project["postype_name"]);
}

if($project["product_line_name"] and $project["productline_subclass_name"])
{
	$form->add_label("product_line_label", "Product Line / Subclass", 0, $project["product_line_name"] . " / " . $project["productline_subclass_name"]);
}
elseif($project["product_line_name"])
{
	$form->add_label("product_line_label", "Product Line / Subclass", 0, $project["product_line_name"]);
}
else
{
	$form->add_label("product_line_label", "Product Line / Subclass", 0, "");
}


$form->add_label("project_number_label", "Project Number  /Treatment State", RENDER_HTML, "<span class='text'><strong>" . $project["project_number"] . "</strong></span> / " . $project["project_state_text"]);



$form->add_label("status_label", "Project Status", 0, $project["order_actual_order_state_code"] . " " . $order_state_name);
/*
if($project["order_archive_date"] != NULL 
  and $project["order_archive_date"] != '0000-00-00')
{
	$form->add_label("status_label", "Project Status", 0, $project["order_actual_order_state_code"] . " " . $order_state_name);
}
elseif(count($user_roles) == 1 
	and (in_array(5, $user_roles) or in_array(6, $user_roles)))
{
	$udata = get_user(user_id());
	$tmp = get_external_losgistics_state($project["project_order"], $udata["address"]);
	if(count($tmp))
	{
		$form->add_label("status_label", "Logistic Status", 0, $tmp["name"]);
	}
	else
	{
		$form->add_label("status_label", "Project Status Development", 0, $development_state["name"]);
	}
}
else
{
	if(isset($offer_state) and $offer_state["data"])
	{
		$form->add_label("status_label", "Project Status Development", 0,  $development_state["name"], 1, "offer_info");
	}
	else
	{
		$form->add_label("status_label", "Project Status Development", 0,  $development_state["name"]);
	}
}
*/


if(has_access("can_see_logistic_status") and isset($logistic_state) and is_array($logistic_state) and $logistic_state["data"])
{
	$form->add_label("logistic_status_label", "Project Status Logistic", 0, $logistic_state["info"], 1, "logistic_info");
}
elseif(has_access("can_see_logistic_status") and isset($logistic_state) and is_array($logistic_state) and $logistic_state["info"])
{
	$form->add_label("logistic_status_label", "Project Status Logistic", 0, $logistic_state["info"]);
}



//$form->add_label("staff_label", "Project Leader / Logistics Coordinator", 0, $project["project_manager"] . " / " . $project["operator"]);

$form->add_label("staff_label", "Project Leader", 0, $project["project_manager"]);

$form->add_section(" ");


$form->add_label("submitted_by_label", "Project Starting Date", 0, to_system_date($project["order_date"])  . " owned by " . $project["submitted_by"]);


if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
{
	if($project["project_real_opening_date"] != NULL 
		and $project["project_real_opening_date"] != '0000-00-00'
	    and $project["project_actual_opening_date"] != NULL 
		and $project["project_actual_opening_date"] != '0000-00-00')
	{
		$form->add_label("real_opening_date_label", $project["projectkind_milestone_name_01"] . " / " . $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_real_opening_date"]) . " / " . to_system_date($project["project_actual_opening_date"]));
	}
	elseif($project["project_real_opening_date"] != NULL 
		and $project["project_real_opening_date"] != '0000-00-00')
	{
		$form->add_label("real_opening_date_label", $project["projectkind_milestone_name_01"] . " / " . $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_real_opening_date"]));
	}
	else
	{
		$form->add_label("real_opening_date_label", $project["projectkind_milestone_name_01"] . " / " . $project["projectkind_milestone_name_02"], 0, "");
	}
}
elseif($project["project_projectkind"] == 3
	or $project["project_projectkind"] == 9) //Take Over and renovation
{
	$form->add_label("planned_takeover_date_label", "Client's Preferred Takeover Date", 0, to_system_date($project["project_planned_takeover_date"]));
	
	if($project["project_real_opening_date"] != NULL 
		and $project["project_real_opening_date"] != '0000-00-00'
	    and $project["project_actual_opening_date"] != NULL 
		and $project["project_actual_opening_date"] != '0000-00-00')
	{
		$form->add_label("planned_opening_date_label", "Client's Preferred / Agreed / Actual Opening Date", 0, to_system_date($project["project_planned_opening_date"]) . " / " . to_system_date($project["project_real_opening_date"]) . " / " . to_system_date($project["project_actual_opening_date"]));
	}
	elseif($project["project_real_opening_date"] != NULL 
		and $project["project_real_opening_date"] != '0000-00-00')
	{
		$form->add_label("planned_opening_date_label", "Client's Preferred / Agreed / Actual Opening Date", 0, to_system_date($project["project_planned_opening_date"]) . " / " . to_system_date($project["project_real_opening_date"]));
	}
	else
	{
		$form->add_label("planned_opening_date_label", "Client's Preferred / Agreed / Actual Opening Date", 0, to_system_date($project["project_planned_opening_date"]));
	}
}
else
{
	if($project["project_real_opening_date"] != NULL 
		and $project["project_real_opening_date"] != '0000-00-00'
	    and $project["project_actual_opening_date"] != NULL 
		and $project["project_actual_opening_date"] != '0000-00-00')
	{
		$form->add_label("planned_opening_date_label", "Client's Preferred / Agreed / Actual Opening Date", 0, to_system_date($project["project_planned_opening_date"]) . " / " . to_system_date($project["project_real_opening_date"]) . " / " . to_system_date($project["project_actual_opening_date"]));

		//$date_string = "Client's Preferred Opening Date: " . to_system_date($project["project_planned_opening_date"]) . " / Agreed Opening Date: " . to_system_date($project["project_real_opening_date"]) . " / Actual Opening Date: " . to_system_date($project["project_actual_opening_date"]);
		//$form->add_label("planned_opening_date", "Dates", 0,  $date_string);
	}
	elseif($project["project_real_opening_date"] != NULL 
		and $project["project_real_opening_date"] != '0000-00-00')
	{
		$form->add_label("planned_opening_date_label", "Client's Preferred / Agreed / Actual Opening Date", 0, to_system_date($project["project_planned_opening_date"]) . " / " . to_system_date($project["project_real_opening_date"]));
	}
	else
	{
		$form->add_label("planned_opening_date_label", "Client's Preferred / Agreed / Actual Opening Date", 0, to_system_date($project["project_planned_opening_date"]));
	}
}

if($project["project_projectkind"] == 8) //PopUp Project
{
	$form->add_label("planned_closing_date_label", "Planned Closing Date / Closing Date", 0, to_system_date($project["project_planned_closing_date"]) . " / "  . to_system_date($project["project_popup_closingdate"]));

}
else
{
	$form->add_label("planned_closing_date_label", "Planned Closing Date / Closing Date", 0, to_system_date($project["project_planned_closing_date"]) . " / "  . to_system_date($project["project_shop_closingdate"]));
}

//$form->add_label("sqm_label", "Gross/Total/Sales Surface in sqms", 0,  $project["project_cost_grosssqms"] . "/" . $project["project_cost_totalsqms"] . "/" . $project["project_cost_sqms"]);
$form->add_label("sqm_label", "Total/Sales Surface in sqms", 0,  $project["project_cost_totalsqms"] . "/" . $project["project_cost_sqms"]);


?>