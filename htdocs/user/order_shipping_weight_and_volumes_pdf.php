<?php
/********************************************************************

    order_shipping_weight_and_volumes_pdf.php

    Print shipping details: volumes an weights

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2015-10-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2015-10-22
    Version:        1.0.0

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_delivery_schedule_in_orders");



/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());

// read project and order details
$order = get_order(param("oid"));

global $page_title;
$page_title = "Weight and Volumes for Order " . $order["order_number"];

$shop = $order["order_shop_address_company"] . ", " .
        $order["order_shop_address_zip"] . " " .
        $order["order_shop_address_place"] . ", " .
        $order["order_shop_address_country_name"];


$order_state_name = get_actual_order_state_name($order["order_actual_order_state_code"], 2);
$retail_operator = get_user($order["order_retail_operator"]);


$invoice_address = "";
$billing_address_province_name = "";
if($order["order_billing_address_place_id"])
{
	$billing_address_province_name = get_province_name($order["order_billing_address_place_id"]);
}


$invoice_address = $invoice_address . $order["order_billing_address_company"];
if ($order["order_billing_address_company2"])
{
    $invoice_address = $invoice_address . ", " . $order["order_billing_address_company2"];
}

$invoice_address = $invoice_address . ", " . $order["order_billing_address_address"];

if ($order["order_billing_address_address2"])
{
    $invoice_address = $invoice_address . ", " . $order["order_billing_address_address2"];
}

$invoice_address = $invoice_address . ", " . $order["order_billing_address_zip"] . " " . $order["order_billing_address_place"];

if($billing_address_province_name) {
	$invoice_address .= ", " . $billing_address_province_name;
}

$invoice_address = $invoice_address . ", " . $order["order_billing_address_country_name"];
$invoice_address = $invoice_address . ", Phone: " . $order["order_billing_address_phone"];

// get company's address
$client_address = get_address($order["order_client_address"]);
$client = $client_address["company"] . ", " .
          $client_address["zip"] . " " . $client_address["place"] . ", " .
          $client_address["country_name"];

$captions1 = array();
$captions1[] = "Order Number:";
$captions1[] = "Order Date:";
$captions1[] = "Status:";
$captions1[] = "Logistics Coordinator:";
$captions1[] = '';

$captions2 = array();
$captions2[] = "Preferred Transportation:";
//$captions2[] = "Packaging Retraction Desired:";
$captions2[] = "Pedestrian Area Approval Needed:";
$captions2[] = "Full Delivery Desired:";
$captions2[] = "Insurance:";

$captions3 = array();
$captions3[] = "Delivery Comments:";
$captions3[] = "Client:";
$captions3[] = "Shop:";
if(!in_array(5, $user_roles) and !in_array(29, $user_roles)) // no supplier, no warehouse
{
	$captions3[] = "Bill to:";
}

$data1 = array();
$data1[] = $order["order_number"];
$data1[] = to_system_date($order["order_date"]);
$data1[] = $order["order_actual_order_state_code"] . " " . $order_state_name;
$data1[] = $retail_operator["firstname"] . " " . $retail_operator["name"];
$data1[] = '';

$data2 = array();
$data2[] = $order["transportation_type_name"];

/*
$value="no";
if ($order["order_packaging_retraction"])
{
    $value="yes";
}
$data2[] = $value;
*/

$value="no";
if ($order["order_pedestrian_mall_approval"])
{
    $value="yes";
}
$data2[] = $value;
$value="no";
if ($order["order_full_delivery"])
{
    $value="yes";
}
$data2[] = $value;


$data3[] = $order["order_delivery_comments"];

if ($order["order_insurance"] == 1)
{
    $data2[] = "Insurance covered by Tissot/Forwarder";;
}
else
{
	$data2[] = "Insurance not covered by Tissot/Forwarder";
}


$data3[] = $client;
$data3[] = $shop;
if(!in_array(5, $user_roles) and !in_array(29, $user_roles)) // no supplier, no warehouse
{
	$data3[] = $invoice_address;
}


//delivery data
$captions41 = array();
$captions41[] = "Item Code";
$captions41[] = "Description";
$captions41[] = "Quantity";
$captions41[] = "Unit";
$captions41[] = "Gross Weight kg";
$captions41[] = "LxWxH cm";
$captions41[] = "CBM";
$captions41[] = "Packaging";


// prepare SQLs
$sql_order_items = "select order_item_id, order_item_text, addresses.address_company as supplier_company, ".
                   "    if(order_item_item <>'', item_code, item_type_name) as item_code, " .
                   "    concat(if(addresses.address_company is not null, addresses.address_company, '!! no supplier assigned !!'), ' / ', if(addresses1.address_company is not null, addresses1.address_company, '!! no forwarder assigned !!')) as group_head, " .
				   " if(order_item_stackable = 1, 'x', '') as stackable " .
                   "from order_items ".
                   "left join items on order_item_item = item_id ".
                   "left join item_types on order_item_type = item_type_id ".
                   "left join addresses on addresses.address_id = order_item_supplier_address " . 
				   "left join addresses as addresses1 on addresses1.address_id = order_item_forwarder_address ";


if (has_access("has_access_to_all_traffic_data_in_orders"))
{
    $list_filter = " (order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
		              "   and order_item_quantity > 0 " .
                      "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . $order["order_id"];

}
elseif(in_array(6, $user_roles)) // forwarder
{
    $list_filter = " (order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
		              "   and order_item_quantity > 0 " .
                      "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . $order["order_id"] .
                      "   and order_item_forwarder_address = " . $user_data["address"];
}
elseif(in_array(5, $user_roles)) // supplier
{
    $list_filter = " (order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
		              "   and order_item_quantity > 0 " .
                      "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . $order["order_id"] .
                      "   and order_item_supplier_address = " . $user_data["address"];
}
else
{
    $list_filter = " (order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
		              "   and order_item_quantity > 0 " .
                      "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . $order["order_id"] .
                      "   and order_item_forwarder_address = 0";
}

$sql = $sql_order_items . " where " . $list_filter . " order by group_head, order_item_type, item_code";


/********************************************************************
    prepare pdf
*********************************************************************/

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');


class MYPDF extends TCPDF
{
    //Page header
    function Header()
    {
        global $page_title;
		//Logo
        $this->Image('../pictures/brand_logo.jpg',10,8,33);
        //arialn bold 15
        $this->SetFont('arialn','B',12);
        //Move to the right
        $this->Cell(80);
        //Title
        $this->Cell(0,34,$page_title,0,0,'R');
        //Line break
        $this->Ln(20);

    }

    //Page footer
    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //arialn italic 8
        $this->SetFont('arialn','I',8);
        //Page number
        $this->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$this->PageNo().'/'.$this->getAliasNbPages(),0,0,'R');
    }
   
}

//Instanciation of inherited class
$pdf = new MYPDF("L", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 22, 12);

$pdf->Open();

$pdf->SetLineWidth(0.1);
$pdf->SetFillColor(220, 220, 220); 

$pdf->AddFont('arialn');
$pdf->AddFont('arialn', 'B');

$pdf->AddPage();


// output project headernformations
$pdf->SetFont('arialn','B',12);
$pdf->Cell(140,5,"Order Details",1);
$pdf->Cell(10,5," ",0);
$pdf->Cell(125,5,"Traffic Checklist",1);

$pdf->Ln();


$pdf->SetFont('arialn','',9);

foreach($captions2 as $key=>$value)
{
    if($key <=3)
    {
        $pdf->Cell(70,5,$captions1[$key],1);
        $pdf->Cell(70,5,$data1[$key],1);

        $pdf->Cell(10,5," ",0);
    }
    else
    {
        $pdf->Cell(150,5," ",0);
    }

    

    $pdf->Cell(60,5,$captions2[$key],1);
    $pdf->multiCell(65,5,$data2[$key],1);

}


$pdf->Ln();

foreach($captions3 as $key=>$value)
{
    $pdf->SetFont('arialn','B',9);
    $pdf->Cell(40,5,$captions3[$key],1);
    $pdf->SetFont('arialn','',9);
    $pdf->multiCell(235,5,$data3[$key],1);
}


// Item Data
$old_address_grouping = "";

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
    
	$sql_p = "select order_item_packaging_number, " . 
				" ROUND(order_item_quantity*order_item_packaging_number*order_item_packaging_weight_gross,2) as gross_weight, " .
				" concat(ROUND(order_item_packaging_length), 'x', ROUND(order_item_packaging_width), 'x', ROUND(order_item_packaging_height)) as lxwxh, " .
				" ROUND((order_item_quantity*order_item_packaging_number*(order_item_packaging_length*order_item_packaging_width*order_item_packaging_height/1000000)), 2) as cbm, " .
				" unit_name, packaging_type_name " .
				"from order_item_packaging ".
		        " left join order_items on order_item_id = order_item_packaging_order_item_id " .
				"left join units on unit_id = order_item_packaging_unit_id " . 
				"left join packaging_types on packaging_type_id = order_item_packaging_packaging_id " .
				"where order_item_packaging_order_item_id = " . $row["order_item_id"];

    $res_p = mysql_query($sql_p) or dberror($sql_p);
	$num_of_elements = mysql_num_rows($res_p);
	$needed_lines = 6*$num_of_elements;
    
    $y = $pdf->getY();
    if($y+$needed_lines >= 170)
    {
        $pdf->AddPage();
    }

    
    // titles

	if($old_address_grouping != ($row["group_head"] . $row["order_item_id"]))
	{
		$pdf->SetFont('arialn','B',9);
		$old_address_grouping = $row["group_head"] . $row["order_item_id"];
		
		$pdf->Ln();
				
		$pdf->Cell(275,5, $row["group_head"],1, 0, 'L', 1);
		$pdf->Ln();
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(30,5, $captions41[0],1, 0, 'L', 1);
		$pdf->Cell(146,5, $captions41[1],1, 0, 'L', 1);
		$pdf->Cell(11,5, $captions41[2],1, 0, 'R', 1);
		$pdf->Cell(15,5, $captions41[3],1, 0, 'L', 1);
		$pdf->Cell(21,5, $captions41[4],1, 0, 'R', 1);
		$pdf->Cell(16,5, $captions41[5],1, 0, 'R', 1);
		$pdf->Cell(12,5, $captions41[6],1, 0, 'R', 1);
		$pdf->Cell(24,5, $captions41[7],1, 0, 'L', 1);
		$pdf->Ln();
		$pdf->SetFont('arialn','',8);
		$total_item_weight = 0;
		$total_item_cbm = 0;
	}



    //data    
    $pdf->Cell(30,5, $row["item_code"], 1);
    
	if(strlen($row["order_item_text"]) > 110)
	{
		$pdf->Cell(146,5, substr($row["order_item_text"], 0, 110) . "...", 1);
	}
	else
	{
		$pdf->Cell(146,5, $row["order_item_text"], 1);
	}

	//get packagin data
	$first_row = true;
	$sql_p = "select order_item_packaging_number, " . 
				" ROUND(order_item_quantity*order_item_packaging_number*order_item_packaging_weight_gross,2) as gross_weight, " .
				" concat(ROUND(order_item_packaging_length), 'x', ROUND(order_item_packaging_width), 'x', ROUND(order_item_packaging_height)) as lxwxh, " .
				" ROUND((order_item_quantity*order_item_packaging_number*(order_item_packaging_length*order_item_packaging_width*order_item_packaging_height/1000000)), 2) as cbm, " .
				" unit_name, packaging_type_name " .
				"from order_item_packaging ".
		        " left join order_items on order_item_id = order_item_packaging_order_item_id " .
				"left join units on unit_id = order_item_packaging_unit_id " . 
				"left join packaging_types on packaging_type_id = order_item_packaging_packaging_id " .
				"where order_item_packaging_order_item_id = " . $row["order_item_id"];

    $res_p = mysql_query($sql_p) or dberror($sql_p);

	while ($row_p = mysql_fetch_assoc($res_p))
	{
		if($first_row == false)
		{
			$pdf->Cell(176,5, "", 0);
		}
		$pdf->Cell(11,5, $row_p["order_item_packaging_number"], 1, 0, 'R');
		$pdf->Cell(15,5, $row_p["unit_name"], 1);
		$pdf->Cell(21,5, $row_p["gross_weight"], 1, 0, 'R');
		$pdf->Cell(16,5, $row_p["lxwxh"], 1, 0, 'R');
		$pdf->Cell(12,5, $row_p["cbm"], 1, 0, 'R');
		$pdf->Cell(24,5, $row_p["packaging_type_name"], 1);
		$pdf->Ln();
		$first_row = false;

		$total_item_weight = $total_item_weight + $row_p["gross_weight"];
		$total_item_cbm = $total_item_cbm + $row_p["cbm"];
   
	}

	//Group totals
	$pdf->SetFont('arialn','B',9);
	$pdf->Cell(30,5, "", 0);
	$pdf->Cell(146,5, "", 0);
	$pdf->Cell(11,5, "", 0, 0, 'R');
	$pdf->Cell(15,5, "", 0);
	$pdf->Cell(21,5, $total_item_weight, 1, 0, 'R');
	$pdf->Cell(16,5, "", 0, 0, 'R');
	$pdf->Cell(12,5, $total_item_cbm, 1, 0, 'R');
	$pdf->Cell(24,5, "", 0);
	$pdf->Cell(16,5, "", 0);
	$pdf->Ln();
}




// write pdf

$file_name = "weight_and_volumes_order_" . $order["order_number"] . ".pdf";
$pdf->Output($file_name);


?>