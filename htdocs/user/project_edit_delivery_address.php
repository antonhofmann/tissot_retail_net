<?php
/********************************************************************

    project_edit_delivery_address.php

    Edit item's delivery address

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-11-19
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-02-16
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_edit_delivery_addresses_in_projects");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));

// read project and order details
$order = get_order($project["project_order"]);

// read order item details
$order_item = get_order_item(id());

// get company's address
$client_address = get_address($order["order_client_address"]);

// read information from order_addresses
$delivery_address = get_order_item_address(2, $project["project_order"], id());

// create array for the delivery address listbox
$delivery_addresses = get_delivery_addresses($order["order_client_address"]);

// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";



$delivery_address_province_name = "";
if(param("delivery_address_place_id"))
{
	$delivery_address_province_name = get_province_name(param("delivery_address_place_id"));
}
elseif($delivery_address["place_id"])
{
	$delivery_address_province_name = get_province_name($delivery_address["place_id"]);
}


if(param("delivery_address_country")) {
	$sql_delivery_places = "select place_id, place_name from places " . 
		          "where place_country = " .  param("delivery_address_country") . 
		          " order by place_name";
}
elseif($delivery_address['country']) {
	$sql_delivery_places = "select place_id, place_name from places " . 
		          "where place_country = " .  $delivery_address['country'] . 
		          " order by place_name";
}
else {
	$sql_delivery_places = "select place_id, place_name from places " . 
		          "where place_country =  " . dbquote($order["order_shop_address_country"])  .
		          " order by place_name";
}

$deleivery_places = array();
$deleivery_places[999999999] = "Other city not listed below";

$res = mysql_query($sql_delivery_places);

while ($row = mysql_fetch_assoc($res))
{
	$deleivery_places[$row["place_id"]] = $row["place_name"];
}


//get all delivery addresses for all items of the order
$old_delivery_addresses = get_all_order_item_delivery_addresses($project["project_order"]);

/********************************************************************
    build form
*********************************************************************/
$form = new Form("order_addresses", "order_address");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid",$project["project_order"]);
$form->add_hidden("order_item_order", $project["project_order"]);
$form->add_hidden("order_item_id", id());
$form->add_hidden("order_client_address", $order["order_client_address"]);

$form->add_hidden("order_item_supplier_address", $order_item["order_item_supplier_address"]);
$form->add_hidden("order_item_forwarder_address", $order_item["order_item_forwarder_address"]);


require_once "include/project_head_small.php";


$form->add_section("Item Details");
$form->add_label("order_item_code", "Item Code", 0, $order_item["code"]);
$form->add_label("order_item_text", "", 0, $order_item["text"]);
$form->add_label("order_item_quantity", "Quantity", 0, $order_item["quantity"]);


$tmp_text1="Please indicate the ship to information.\n";
$tmp_text2="You can either select an existing address or enter a new address.";

$form->add_section("Ship to");
if (count($delivery_addresses) > 0 )
{
    $form->add_comment($tmp_text1 . $tmp_text2);
    $form->add_list("delivery_address_id", "Ship to", $delivery_addresses, SUBMIT);
}
else
{
    $form->add_comment($tmp_text1);
}
if (count($delivery_address) > 0)
{
    $form->add_edit("delivery_address_company", "Company*", NOTNULL, $delivery_address["company"], TYPE_CHAR);
    $form->add_edit("delivery_address_company2", "", 0, $delivery_address["company2"], TYPE_CHAR);

	$form->add_hidden("delivery_address_address");
	$form->add_multi_edit("delivery_street", array("order_address_street", "order_address_street_number"), "Street/Street number", array(NOTNULL, ''), array($delivery_address["street"], $delivery_address["streetnumber"]), array('', ''), array(200, 6), array(), 0, '', '', array(40, 5));

    $form->add_edit("delivery_address_address2", "Additional Address Info", 0, $delivery_address["address2"], TYPE_CHAR);
    
	$form->add_list("delivery_address_country", "Country*", $sql_countries, NOTNULL | SUBMIT, $delivery_address["country"]);
	$form->add_edit("delivery_address_zip", "ZIP", 0, $delivery_address["zip"], TYPE_CHAR, 20);
    
	$form->add_list("delivery_address_place_id", "City*", $deleivery_places, NOTNULL | SUBMIT, $delivery_address['place_id']);
	
	
	if(param("delivery_address_place_id") and param("delivery_address_place_id") == "999999999")
	{
		$sql_provinces = "select province_id, province_canton " . 
			             "from provinces " . 
			             "where province_country = " . param("delivery_address_country") . 
			             " order by province_canton";

		$form->add_list("delivery_address_province", "Province*", $sql_provinces, NOTNULL |SUBMIT, "");
		$form->add_edit("delivery_address_place", "City*", NOTNULL, "", TYPE_CHAR);
	}
	else
	{
		$form->add_label("delivery_address_place", "City", 0, $delivery_address['place']);
		$form->add_label("delivery_address_province_name", "Province", 0, $delivery_address_province_name);
	}
    
	
   $form->add_hidden("delivery_address_phone", $delivery_address["phone"]);
	$form->add_multi_edit("delivery_phone_number", array("order_address_phone_country", "order_address_phone_area", "order_address_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array($delivery_address["phone_country"], $delivery_address["phone_area"], $delivery_address["phone_number"]), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));


    $form->add_hidden("delivery_address_mobile_phone", $delivery_address["mobile_phone"]);
	$form->add_multi_edit("delivery_mobile_phone_number", array("order_address_mobile_phone_country", "order_address_mobile_phone_area", "order_address_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER ), array($delivery_address["mobile_phone_country"], $delivery_address["mobile_phone_area"], $delivery_address["mobile_phone_number"]), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

    $form->add_edit("delivery_address_email", "Email", 0, $delivery_address["email"], TYPE_CHAR);
    $form->add_edit("delivery_address_contact", "Contact*", NOTNULL, $delivery_address["contact"], TYPE_CHAR);

	$form->add_section(" ");
	$form->add_checkbox("same_supplier", "all items of the same supplier", 0, 0, "Apply Change to");
	$form->add_checkbox("same_forwarder", "all items of the same forwarder", 0, 0);
}
else
{
    $form->add_edit("delivery_address_company", "Company*", NOTNULL, "", TYPE_CHAR);
    $form->add_edit("delivery_address_company2", "", 0, "", TYPE_CHAR);
    $form->add_hidden("delivery_address_address");
	$form->add_multi_edit("delivery_street", array("order_address_street", "order_address_street_number"), "Street/Street number", array(NOTNULL, ''), array('', ''), array('', ''), array(200, 6), array(), 0, '', '', array(40, 5));


    $form->add_edit("delivery_address_address2", "Additional Address Info", 0, "", TYPE_CHAR);

	 $form->add_list("delivery_address_country", "Country*", $sql_countries, NOTNULL, "");

    $form->add_edit("delivery_address_zip", "ZIP", 0, "", TYPE_CHAR, 20);
    
	$form->add_list("delivery_address_place_id", "City*", $deleivery_places, NOTNULL |SUBMIT, $delivery_address['place_id']);
	
	if(param("delivery_address_place_id") and param("delivery_address_place_id") == "999999999")
	{
		 $sql_provinces = "select province_id, province_canton " . 
			             "from provinces " . 
			             "where province_country = " . param("delivery_address_country") . 
			             " order by province_canton";
		$form->add_list("delivery_address_province", "Province*", $sql_provinces, NOTNULL |SUBMIT, "");
		 $form->add_edit("delivery_address_place", "City*", NOTNULL, "", TYPE_CHAR);
	}
	else
	{
		$form->add_label("delivery_address_place", "City", 0, $delivery_address['place']);
		$form->add_label("delivery_address_province_name", "Province");
	}

   
    $form->add_hidden("delivery_address_phone", '');
	$form->add_multi_edit("delivery_phone_number", array("order_address_phone_country", "order_address_phone_area", "order_address_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array('', '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));


    $form->add_hidden("delivery_address_mobile_phone", '');
	$form->add_multi_edit("delivery_mobile_phone_number", array("order_address_mobile_phone_country", "order_address_mobile_phone_area", "order_address_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER ), array('', '', ''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

    $form->add_edit("delivery_address_email", "Email", 0, "", TYPE_CHAR);
    $form->add_edit("delivery_address_contact", "Contact*", NOTNULL, "", TYPE_CHAR);
}





$form->add_button("save", "Save Data");
$form->add_button(FORM_BUTTON_BACK, "Back");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


if ($form->button("save"))
{
    
	$form->value("delivery_address_phone", $form->unify_multi_edit_field($form->items["delivery_phone_number"]));
	$form->value("delivery_address_mobile_phone", $form->unify_multi_edit_field($form->items["delivery_mobile_phone_number"]));
	
	$form->add_validation("{delivery_address_phone} != '' or {delivery_address_mobile_phone} != ''", "Please indcate either the phone number or the mobile phone number!");
	
	if ($form->validate())
    {
        
		if($form->value("delivery_address_place_id") == "999999999")
		{
			$sql_i = "insert into places " . 
				     "(place_country, place_province, place_name, date_created, user_created ) VALUES (" . 
				    $form->value("delivery_address_country") . ", " . 
					$form->value("delivery_address_province") . ", " .
					dbquote($form->value("delivery_address_place")) . ", " .
				    dbquote(date("Y-m-d H:i:s")) . ", " .
				    dbquote(user_login()) . ")";

			$res = mysql_query($sql_i) or dberror($sql_i);
			$new_place_id = mysql_insert_id();
			$form->value("delivery_address_place_id", $new_place_id);
		}

		
		$form->value("delivery_address_address", $form->unify_multi_edit_field($form->items["delivery_street"], get_country_street_number_rule($form->value("delivery_address_country"))));
		
		project_update_delivery_address($form);

        
		
		//check if there was a change in a delivery address and send mail alerts to
		//related supplier, forwarder and retail operator
		$forwarders = array();
		$suppliers = array();
		$actual_delivery_addresses = get_all_order_item_delivery_addresses($project["project_order"]);
		foreach($actual_delivery_addresses as $order_item_id=>$item_delivery_address)
		{

			$tmp_a = implode('',$item_delivery_address["delivery_address"]);
			if(array_key_exists($order_item_id, $old_delivery_addresses))
			{
				$tmp_o = implode('',$old_delivery_addresses[$order_item_id]["delivery_address"]);
				if($tmp_a != $tmp_o)
				{
					$sql = "select order_item_supplier_address, order_item_forwarder_address, " .
						   " order_item_text, item_code " . 
						   "from order_items " . 
						   " left join items on item_id = order_item_item " . 
						   " where order_item_id = " . $item_delivery_address["order_item_id"];

					$res = mysql_query($sql) or dberror($sql);

					if ($row = mysql_fetch_assoc($res))
					{
						$suplier_address_id = $row["order_item_supplier_address"];
						$forwarder_address_id = $row["order_item_forwarder_address"];
						
						if($row["item_code"])
						{
							$item_info = $row["item_code"] . " " . $row["order_item_text"];
						}
						else
						{
							$item_info = $row["order_item_text"];
						}

						$forwarders[$forwarder_address_id] = $forwarder_address_id;
						$suppliers[] = array('supplier_address_id'=>$suplier_address_id, 'forwarder_address_id'=>$forwarder_address_id, 'item_info'=>$item_info);
					}


					
				}

				
			}
		
		}

		foreach($forwarders as $fkey=>$forwarder_address_id)
		{
			

			$recipients = array();
			$cc_recipients = array();
			$cc_supplier_recipients = array();
			$item_info = "";

			//get forwarder email
			$sql = "select user_id, " .
				   "concat(user_firstname, ' ', user_name) as user_name, " . 
				   "user_email, user_email_cc, user_email_deputy " .
				   "from users " . 
				   " where user_address = " . dbquote($forwarder_address_id) . 
				   " and user_active = 1";

			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{
				$recipients[$row["user_id"]] = array("user_email"=>$row["user_email"], "user_name"=>$row["user_name"]);
				
				
				if($row["user_email_cc"])
				{
					$sql_c = "select user_id, " .
							   "concat(user_firstname, ' ', user_name) as user_name, " . 
							   "user_email " .
							   "from users " . 
							   " where user_email = " . dbquote($row["user_email_cc"]) . 
							   " and user_active = 1";
					$res_c = mysql_query($sql_c) or dberror($sql_c);
					if ($row_c = mysql_fetch_assoc($res_c))
					{
						$cc_recipients[$row_c["user_id"]] = array("user_email"=>$row_c["user_email"], "user_name"=>$row_c["user_name"]);
					}
				}

				
				if($row["user_email_deputy"])
				{
					$sql_c = "select user_id, " .
							   "concat(user_firstname, ' ', user_name) as user_name, " . 
							   "user_email " .
							   "from users " . 
							   " where user_email_deputy = " . dbquote($row["user_email_deputy"]) . 
							   " and user_active = 1";
					$res_c = mysql_query($sql_c) or dberror($sql_c);
					if ($row_c = mysql_fetch_assoc($res_c))
					{
						$cc_recipients[$row_c["user_id"]] = array("user_email"=>$row_c["user_email"], "user_name"=>$row_c["user_name"]);
					}
				}

			}

			
			foreach($suppliers as $skey=>$supplier)
			{
				
				if($supplier["forwarder_address_id"] == $forwarder_address_id)
				{
					
					$item_info .= $supplier["item_info"] . "\n";
					$sql = "select user_id, " .
						   "concat(user_firstname, ' ', user_name) as user_name, " . 
						   "user_email, user_email_cc, user_email_deputy " .
						   "from users " . 
						   " where user_address = " . dbquote($supplier["supplier_address_id"]) . 
						   " and user_active = 1";

					$res = mysql_query($sql) or dberror($sql);

					while ($row = mysql_fetch_assoc($res))
					{
						$cc_recipients[$row["user_id"]] = array("user_email"=>$row["user_email"], "user_name"=>$row["user_name"]);
						
						
						if($row["user_email_cc"])
						{
							$sql_c = "select user_id, " .
									   "concat(user_firstname, ' ', user_name) as user_name, " . 
									   "user_email " .
									   "from users " . 
									   " where user_email = " . dbquote($row["user_email_cc"]) . 
									   " and user_active = 1";
							$res_c = mysql_query($sql_c) or dberror($sql_c);
							if ($row_c = mysql_fetch_assoc($res_c))
							{
								$cc_recipients[$row_c["user_id"]] = array("user_email"=>$row_c["user_email"], "user_name"=>$row_c["user_name"]);
							}
						}

						if($row["user_email_deputy"])
						{
							$sql_c = "select user_id, " .
									   "concat(user_firstname, ' ', user_name) as user_name, " . 
									   "user_email " .
									   "from users " . 
									   " where user_email_deputy = " . dbquote($row["user_email_deputy"]) . 
									   " and user_active = 1";
							$res_c = mysql_query($sql_c) or dberror($sql_c);
							if ($row_c = mysql_fetch_assoc($res_c))
							{
								$cc_recipients[$row_c["user_id"]] = array("user_email"=>$row_c["user_email"], "user_name"=>$row_c["user_name"]);
							}
						}

					}
				}

				
			}


			//send mails
			if(count($recipients) > 0)
			{
				
				$sql_c = "select user_id, " .
						   "concat(user_firstname, ' ', user_name) as user_name, " . 
						   "user_email " .
						   "from users " . 
						   " where user_id = " . dbquote($project["order_retail_operator"]) . 
						   " and user_active = 1";
				
				$res_c = mysql_query($sql_c) or dberror($sql_c);
				if ($row_c = mysql_fetch_assoc($res_c))
				{
					$cc_recipients[$row_c["user_id"]] = array("user_email"=>$row_c["user_email"], "user_name"=>$row_c["user_name"]);
				}

				

				if($row["user_email_cc"])
				{
					$sql_c = "select user_id, " .
							   "concat(user_firstname, ' ', user_name) as user_name, " . 
							   "user_email " .
							   "from users " . 
							   " where user_email_deputy = " . dbquote($row["user_email_cc"]) . 
							   " and user_active = 1";
					$res_c = mysql_query($sql_c) or dberror($sql_c);
					if ($row_c = mysql_fetch_assoc($res_c))
					{
						$cc_recipients[$row_c["user_id"]] = array("user_email"=>$row_c["user_email"], "user_name"=>$row_c["user_name"]);
					}
				}
				

				//get mail params

				$sql = "select user_id, user_email, concat(user_name, ' ', user_firstname) as username ".
					   "from users ".
					   "where (user_id = '" . user_id() . "' " . 
					   "   and user_active = 1)";
				
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				
				$sender_id = $row["user_id"];
				$sender_email = $row["user_email"];
				$sender_name = $row["username"];



				$sql = "select * from mail_alert_types " .
					   "where mail_alert_type_id = 27";

				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				$text = $row["mail_alert_mail_text"];

				
				$tmp_project = $project["order_number"] . ", " . $project["order_shop_address_country_name"] . ", " . $project["order_shop_address_company"];
				$link = APPLICATION_URL . "/user/project_view_traffic_data_pdf.php?pid=" . $project["project_id"];
				$subject = "Changes in ship to information - Project " . $tmp_project;

				
				
				$bodytext = str_replace('{item_list}', $item_info, $text);
				$bodytext = str_replace('{link}', $link, $bodytext);
				$bodytext = str_replace('{sender_name}', $sender_name, $bodytext);


				$mail = new Mail();
				$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
				$mail->set_sender($sender_email, $sender_name);
				
				foreach($recipients as $user_id=>$recipient)
				{
					$mail->add_recipient($recipient["user_email"], $recipient["user_name"]);
				}

				foreach($cc_recipients as $user_id=>$cc_recipient)
				{
					$mail->add_cc($cc_recipient["user_email"], $cc_recipient["user_name"]);
				}

			
				

				$mail->add_text($bodytext);
				
				
				if($senmail_activated == true)
				{
					$result = $mail->send();
				}
				else
				{
					$result = 1;
				}

				foreach($recipients as $user_id=>$recipient)
				{
					append_mail($project["project_order"], $user_id, $sender_id, $bodytext, "910", 1);
				}

				foreach($cc_recipients as $user_id=>$recipient)
				{
					append_mail($project["project_order"], $user_id, $sender_id, $bodytext, "910", 1);
				}
			}
		}
		
		$link = "project_edit_delivery_addresses.php?pid=" . param("pid");
        redirect ($link);
    }
}
else if ($form->button("delivery_address_id"))
{
    // set new delivery address
    $form->value("delivery_address_company", "");
    $form->value("delivery_address_company2",  "");
    $form->value("delivery_address_address",  "");
	if(array_key_exists('delivery_street', $form->items))
	{
		$form->items['delivery_street']['values'][0] = "";
		$form->items['delivery_street']['values'][1] = "";
	}
    $form->value("delivery_address_address2",  "");
    $form->value("delivery_address_zip",  "");
    $form->value("delivery_address_place",  "");
	$form->value("delivery_address_place_id",  "");
    $form->value("delivery_address_country",  0);
	$form->value("delivery_address_province_name",  0);
    $form->value("delivery_address_phone",  "");
	if(array_key_exists('delivery_phone_number', $form->items))
	{
		$form->items['delivery_phone_number']['values'][0] = "";
		$form->items['delivery_phone_number']['values'][1] = "";
		$form->items['delivery_phone_number']['values'][2] = "";
	}
    $form->value("delivery_address_mobile_phone",  "");
	if(array_key_exists('delivery_mobile_phone_number', $form->items))
	{
		$form->items['delivery_mobile_phone_number']['values'][0] = "";
		$form->items['delivery_mobile_phone_number']['values'][1] = "";
		$form->items['delivery_mobile_phone_number']['values'][2] = "";
	}
    $form->value("delivery_address_email",  "");
    $form->value("delivery_address_contact",  "");

    if ($form->value("delivery_address_id"))
    {
        $sql = "select * from order_addresses where order_address_id = " . $form->value("delivery_address_id");
        $res = mysql_query($sql) or dberror($sql);

        if ($row = mysql_fetch_assoc($res))
        {
            $form->value("delivery_address_company", $row["order_address_company"]);
            $form->value("delivery_address_company2",  $row["order_address_company2"]);
            $form->value("delivery_address_address",  $row["order_address_address"]);
			if(array_key_exists('delivery_street', $form->items))
			{
				$form->items['delivery_street']['values'][0] = $row["order_address_street"];
				$form->items['delivery_street']['values'][1] = $row["order_address_street_number"];
			}
            $form->value("delivery_address_address2",  $row["order_address_address2"]);
            $form->value("delivery_address_zip",  $row["order_address_zip"]);
            $form->value("delivery_address_place",  $row["order_address_place"]);
			$form->value("delivery_address_place_id",  $row["order_address_place_id"]);
            $form->value("delivery_address_country",  $row["order_address_country"]);
            $form->value("delivery_address_phone",  $row["order_address_phone"]);
			if(array_key_exists('delivery_phone_number', $form->items))
			{
				$form->items['delivery_phone_number']['values'][0] = $row["order_address_phone_country"];
				$form->items['delivery_phone_number']['values'][1] = $row["order_address_phone_area"];
				$form->items['delivery_phone_number']['values'][2] = $row["order_address_phone_number"];
			}

            $form->value("delivery_address_mobile_phone",  $row["order_address_mobile_phone"]);
			if(array_key_exists('delivery_mobile_phone_number', $form->items))
			{
				$form->items['delivery_mobile_phone_number']['values'][0] = $row["order_address_mobile_phone_country"];
				$form->items['delivery_mobile_phone_number']['values'][1] = $row["order_address_mobile_phone_area"];
				$form->items['delivery_mobile_phone_number']['values'][2] = $row["order_address_mobile_phone_number"];
			}
            $form->value("delivery_address_email",  $row["order_address_email"]);
            $form->value("delivery_address_contact",  $row["order_address_contact"]);

			$sql = "select province_canton from places " .
			       "left join provinces on province_id = place_province " . 
				   "where place_id = " . dbquote($row["order_address_place_id"]);
			
			$res = mysql_query($sql) or dberror($sql);

			if ($row = mysql_fetch_assoc($res))
			{
				$form->value("delivery_address_province_name", $row["province_canton"]);
			}
        }
    }
}
elseif($form->button("delivery_address_place_id"))
{
	
	if($form->value("delivery_address_place_id") == "999999999")
	{
		$form->value("delivery_address_place", "");
		$form->value("delivery_address_zip", "");

	}
	else
	{
	
		$sql= "select place_name, province_canton ".
			  "from places " .
			  "left join provinces on province_id = place_province " .
			  "where place_id = " . dbquote($form->value("delivery_address_place_id"));

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$form->value('delivery_address_place', $row['place_name']);
			$form->value("delivery_address_province_name", $row["province_canton"]);
		}
	}
}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Edit Item: Ship To Information");
$form->render();
require_once "include/project_footer_logistic_state.php";
$page->footer();

?>