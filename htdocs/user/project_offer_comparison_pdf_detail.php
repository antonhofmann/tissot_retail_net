<?php
/********************************************************************

    project_offer_comparison_pdf_detail.php

    Print Offer details for Offer Comparisikon for Local Construction Work

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-09
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-09
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/


//print column headers
$pdf->SetFont('arialn','B',9);

$pdf->SetFillColor(220, 220, 220);
$pdf->Cell(80,5, "Cost Group", 1, 0, 'L', 1);

foreach($offers as $key=>$value)
{
    $pdf->Cell(49,5, substr($value, 0,30), 1, 0, 'L', 1);
}

$pdf->Ln();

$pdf->SetFont('arialn','',9);


$i = 0;
foreach($offergroups as $group=>$value)
{
    $pdf->Cell(80,5, $value, 1, 0, 'L', 0);
    
    foreach($offers as $key=>$value2)
    {
        $totals = array();
        
        if(isset($offertotals[$key]))
        {
            $totals = $offertotals[$key];
        
        
            if(array_key_exists($group, $totals))
			{
				$pdf->Cell(35,5, number_format($totals[$group],2, ".", "'"), 1, 0, 'R', 0);
				if($totals[$group] > 0)
				{
					$p = number_format(100*$totals[$group]/$grandtotals[$key],2, ".", "'") . "%";

					$pdf->Cell(14,5, $p, 1, 0, 'R', 0);
				}
				else
				{
					$pdf->Cell(14,5, "0.00%", 1, 0, 'R', 0);
				}
				}
			else
			{
				$pdf->Cell(35,5, number_format(0,2, ".", "'"), 1, 0, 'R', 0);
				$pdf->Cell(14,5, "0.00%", 1, 0, 'R', 0);
			}

            
        }
        else
        {
            $pdf->Cell(35,5, "0.00", 1, 0, 'R', 0);
            $pdf->Cell(14,5, "0.00%", 1, 0, 'R', 0);
        }
    
    }

    $pdf->Ln();
    $i++;

    
	/*
	if($i == count($offergroups) - 1)
    {
        
        //print grand total
        $pdf->Ln();
        $pdf->SetFont('arialn','B',9);

        $pdf->Cell(80,5, "Total", 1, 0, 'L', 0);

        
		foreach($offers as $key=>$value2)
        {
            
            $pdf->Cell(35,5, $currencies[$key] . " " . number_format($smalltotals[$key],2, ".", "'"), 1, 0, 'R', 0);
            
            if($smalltotals[$key] > 0)
            {
                $p = number_format(100*$smalltotals[$key]/$grandtotals[$key],2, ".", "'") . "%";

                $pdf->Cell(14,5, $p, 1, 0, 'R', 0);
            }
            else
            {
                $pdf->Cell(14,5, "0.00%", 1, 0, 'R', 0);
            }

        }
        $pdf->Ln();
    }
	*/
    
}

$pdf->Ln();
$pdf->SetFont('arialn','B',9);

$pdf->Cell(80,5, "Grand Total in Local Currency", 1, 0, 'L', 0);

foreach($offers as $key=>$value2)
{
    
    $pdf->Cell(35,5, $currencies[$key] . " " . number_format($grandtotals[$key],2, ".", "'"), 1, 0, 'R', 0);
    $pdf->Cell(14,5, "100.00%", 1, 0, 'R', 0);

}

$pdf->Ln();
$pdf->Cell(80,5, "Grand Total in CHF", 1, 0, 'L', 0);
foreach($offers as $key=>$value2)
{
    
    $pdf->Cell(35,5, "CHF " . number_format($grandtotals_chf[$key],2, ".", "'"), 1, 0, 'R', 0);
    $pdf->Cell(14,5, "100.00%", 1, 0, 'R', 0);

}

$pdf->Ln();
$pdf->Cell(80,5, "Comparison", 1, 0, 'L', 0);
foreach($offers as $key=>$value2)
{
    
    $pdf->Cell(35,5, $offer_shares[$key] . "%", 1, 0, 'R', 0);
    $pdf->Cell(14,5, "", 1, 0, 'R', 0);

}




$pdf->Ln();
$pdf->Ln();
$pdf->Cell(80,5, "Remarks", 0, 0, 'L', 0);
$pdf->SetFont('arialn','',8);

$x = 90;
$y = $pdf->GetY();
foreach($offers as $key=>$value2)
{
	$pdf->MultiCell(49,3.5, $remarks[$key], 'R', "L");
	$x = $x + 49;
	$pdf->SetY($y);
	$pdf->SetX($x);
}


?>