<?php
/********************************************************************

    delivery_redirect_page.php

    redirect page to order_edit_supplier_data_item or
                     order_edit_traffic_data_item or
                     project_edit_supplier_data_item or
                     project_edit_traffic_data_item
    depending on the parameters

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-29
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2002-09-29
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

$page_type = param("page_type");

$link="delivery.php";

$sql = "select order_id, project_id ".
       "from order_items ".
       "left join orders on order_id = order_item_order ".
       "left join projects on order_id = project_order ".
        "where order_item_id = " . id();

$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{

    
if ($page_type == "forwarder")
    {
        if ($row["project_id"] > 0)
        {
            $link = "project_edit_traffic_data_item.php?pid=" . $row["project_id"] . "&id=" .id();
        }
        else
        {
            $link = "order_edit_traffic_data_item.php?oid=" . $row["order_id"] . "&id=" .id();
        }
    }
else if ($page_type == "supplier")
{
    if ($row["project_id"] > 0)
    {
        $link = "project_edit_supplier_data_item.php?pid= " . $row["project_id"] . "&id=" .id();
    }
    else
    {
        $link = "order_edit_supplier_data_item.php?oid=" . $row["order_id"] . "&id=" .id();
    }
}
}


redirect ($link);
?>