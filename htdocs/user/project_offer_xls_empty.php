<?php
/********************************************************************

    project_offer_xls_empty.php

    Generate Excel-File: Offer form for Local Construction Work

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-09
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-09
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/offer_functions.php";
require_once "../include/xls/Writer.php";

check_access("can_edit_local_constrction_work");



/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());

// read project and order details
$project = get_project(param("pid"));
$offer = get_offer(param("lwoid"));


//read offer

// get orders's currency
$order_currency = get_order_currency($project["project_order"]);

$page_title = "Description for Contractors Local Work - Project " . $project["project_number"];

$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];


$order_state_name = get_actual_order_state_name($project["order_actual_order_state_code"], 1);
$retail_coordinator = get_user($project["project_retail_coordinator"]);
$retail_operator = get_user($project["order_retail_operator"]);


$invoice_address = "";
$billing_address_province_name = "";
if($project["order_billing_address_place_id"])
{
	$billing_address_province_name = get_province_name($project["order_billing_address_place_id"]);
}
$invoice_address = $invoice_address . $project["order_billing_address_company"];
if ($project["order_billing_address_company2"])
{
    $invoice_address = $invoice_address . ", " . $project["order_billing_address_company2"];
}

$invoice_address = $invoice_address . ", " . $project["order_billing_address_address"];

if ($project["order_billing_address_address2"])
{
    $invoice_address = $invoice_address . ", " . $project["order_billing_address_address2"];
}

$invoice_address = $invoice_address . ", " . $project["order_billing_address_zip"] . " " . $project["order_billing_address_place"];

if($billing_address_province_name) {
	$invoice_address .= ', ' . $billing_address_province_name;
}
$invoice_address = $invoice_address . ", " . $project["order_billing_address_country_name"];
$invoice_address = $invoice_address . ", Phone: " . $project["order_billing_address_phone"];

// get company's address
$client_address = get_address($project["order_client_address"]);
$client = $client_address["company"] . ", " .
          $client_address["zip"] . " " . $client_address["place"] . ", " .
          $client_address["country_name"];


// prepare output fields
$captions1 = array();
$captions1[] = "Project Number:";
$captions1[] = "Product Line / Type:";
$captions1[] = "Project Starting Date / Realisitc Opening Date:";
$captions1[] = "Project Leader / Logistics Coordinator:";

$captions2 = array();
$captions2[] = "Company:";
$captions2[] = "Address:";
$captions2[] = "Zip / City:";
$captions2[] = "Contact:";
$captions2[] = "Phone / Email:";


$captions3 = array();
$captions3[] = "Client:";
$captions3[] = "Shop:";
//$captions3[] = "Bill to:";


$captions4 = array();
$captions4[] = "The marked positions are to be calculated. However further positions should not be marked, the bidder is obligated to mark them or add text under Position 14.00.";
$captions4[] = "Before offer delivery, it would be of advantage to visit the site.";
$captions4[] = "All prices must be indicated in the local currency. Please, mention the calculated currency in this list.";
$captions4[] = "The companies should define the prices so that the prices keeps the validity during one year.";
$captions4[] = "Percentage for the VAT and RESERVE have to be defined corresponding to the local site situation.";


$captions5 = array();
$captions5[] = "Total";
$captions5[] = "VAT";
$captions5[] = "Grand Total";
$captions5[] = "Currency";

$data1 = array();
$data1[] = $project["project_number"];
$data1[] = $project["product_line_name"] . " / " . $project["postype_name"];
$data1[] = to_system_date($project["order_date"]) . " / " .  to_system_date($project["project_real_opening_date"]);
$data1[] = $retail_coordinator["firstname"] . " " . $retail_coordinator["name"] . " / " . $retail_operator["firstname"] . " " . $retail_operator["name"];

$data2 = array();
$data2[] = $offer["lwoffer_company"];
$data2[] = $offer["lwoffer_address"];
$data2[] = $offer["lwoffer_zip"] . " " . $offer["lwoffer_place"];
$data2[] = $offer["lwoffer_contact"];
$data2[] = $offer["lwoffer_phone"] . " " . $offer["lwoffer_email"];

$data3 = array();
$data3[] = $client;
$data3[] = $shop;
//$data3[] = $invoice_address;


$footer_text = "Contractors Local Work for Project " . $project["project_number"] . " / " . date("d.m.Y");


/********************************************************************
    prepare excel
*********************************************************************/


$filename = str_replace(" ", "", $offer["lwoffer_company"]) . "_" .$project["project_number"];
$xls =& new Spreadsheet_Excel_Writer(); 

$xls->send($filename); 
$xls->setVersion(8);
$sheet =& $xls->addWorksheet("Worksheet");
$sheet->setInputEncoding("UTF-8");

$sheet->setLandscape();
$sheet->setMarginLeft($margin = 0.5);
$sheet->setMarginRight($margin = 0.5);
$sheet->setMarginTop($margin = 0.5);
$sheet->setMarginBottom($margin = 1.0);
$sheet->hideGridlines();
$sheet->hideScreenGridlines();

$sheet->setFooter($footer_text,$margin = 0.50);


$sheet->setColumn(0, 0, 2);
$sheet->setColumn(1, 1, 7);
$sheet->setColumn(2, 2, 27);
$sheet->setColumn(3, 3, 53);
$sheet->setColumn(4, 4, 10);


//formats
$normal =& $xls->addFormat();
$normal->setSize(9);
$normal->setAlign('top');

$normal_border =& $xls->addFormat();
$normal_border->setSize(9);
$normal_border->setAlign('top');
$normal_border->setBorder(1);

$normal_wrap =& $xls->addFormat();
$normal_wrap->setSize(9);
$normal_wrap->setTextWrap();
$normal_wrap->setAlign('top');


$normal_wrap_border =& $xls->addFormat();
$normal_wrap_border->setSize(9);
$normal_wrap_border->setTextWrap();
$normal_wrap_border->setAlign('top');
$normal_wrap_border->setBorder(1);

$bold =& $xls->addFormat();
$bold->setBold();
$bold->setSize(10);
$bold->setAlign('top');

$bold_border =& $xls->addFormat();
$bold_border->setBold();
$bold_border->setSize(10);
$bold_border->setAlign('top');
$bold_border->setBorder(1);

$bold_center =& $xls->addFormat();
$bold_center->setBold();
$bold_center->setSize(9);
$bold_center->setAlign('top');
$bold_center->setAlign('center');
$bold_center->setPattern(2);
$bold_center->setColor("white");
$bold_center->setBgColor('red');
$bold_center->setBorder(1);


$bggrey =& $xls->addFormat();
$bggrey->setBold();
$bggrey->setColor("white");
$bggrey->setBgColor(8);
$bold_center->setBorder(1);


$sheet->insertBitmap(0, 6, '../pictures/brand_logo.bmp', $x = 0, $y = 0, $scale_x = 0.53, $scale_y = 0.53);




$sheet->write(0, 0, "Description for Contractors Local Work", $bold);



$row_index = 2;
$cell_index = 0;
$doublecell_height = 25.5;
$triplecell_height = 38.25;
$maxchars = 105;
$maxchars2 = 210;

$sheet->write($row_index, $cell_index, "Contractor", $bold);

$row_index++;

foreach($captions2 as $key=>$value)
{
    
    $sheet->write($row_index, $cell_index, $captions2[$key]);
    $sheet->write($row_index, $cell_index+3, $data2[$key], $normal);
    $row_index++;
}

$row_index++;

$sheet->write($row_index, $cell_index, "Project Details", $bold);
$row_index++;

foreach($captions1 as $key=>$value)
{
    $sheet->write($row_index, $cell_index, $captions1[$key]);
    $sheet->write($row_index, $cell_index+3, $data1[$key], $normal);
    $row_index++;
}


foreach($captions3 as $key=>$value)
{
    $sheet->write($row_index, $cell_index, $captions3[$key]);
    $sheet->write($row_index, $cell_index+3, $data3[$key], $normal);
    $row_index++;
}

$row_index++;

//offer Positions

$sql_offer_positions = "select lwofferposition_id, lwofferposition_lwoffergroup, lwofferposition_code, " .
                       "lwofferposition_text, lwofferposition_action, lwofferposition_unit, " . 
                       "lwofferposition_numofunits, lwofferposition_price, lwofferposition_hasoffer, " .
                       "concat(lwoffergroup_code,  ' ', lwoffergroup_text) as lwoffergroup " .
                       "from lwofferpositions " .       
                       "left join lwoffergroups on lwoffergroup_id = lwofferposition_lwoffergroup ". 
                       "where lwoffergroup_offer = " . param("lwoid")  . 
                       "  and lwoffergroup_group < 8 " . 
                       " order by lwofferposition_lwoffergroup, lwofferposition_code";





$res = mysql_query($sql_offer_positions) or dberror($sql_offer_positions);

$group = "";
$first_group = 0;

while ($row = mysql_fetch_assoc($res))
{
    
    if($group != $row["lwoffergroup"])
    {
        
        if($first_group == 1)
        {
            //empty lines
            $sheet->write($row_index, $cell_index, "", $normal_border);
            $sheet->write($row_index, $cell_index+1, "", $normal_border);
            $sheet->setMerge($row_index, $cell_index+2, $row_index, $cell_index+3);
            $sheet->write($row_index, $cell_index+2, "", $normal_wrap_border);
            $sheet->write($row_index, $cell_index+3, "", $normal_wrap_border);
            $sheet->write($row_index, $cell_index+4, "", $normal_border);
            $sheet->write($row_index, $cell_index+5, "", $normal_border);
            $sheet->write($row_index, $cell_index+6, "", $normal_border);
            $sheet->write($row_index, $cell_index+7, "", $normal_border);
            $sheet->write($row_index, $cell_index+8, "", $normal_border);
            $row_index++;

            $sheet->write($row_index, $cell_index, "", $normal_border);
            $sheet->write($row_index, $cell_index+1, "", $normal_border);
            $sheet->setMerge($row_index, $cell_index+2, $row_index, $cell_index+3);
            $sheet->write($row_index, $cell_index+2, "", $normal_wrap_border);
            $sheet->write($row_index, $cell_index+3, "", $normal_wrap_border);
            $sheet->write($row_index, $cell_index+4, "", $normal_border);
            $sheet->write($row_index, $cell_index+5, "", $normal_border);
            $sheet->write($row_index, $cell_index+6, "", $normal_border);
            $sheet->write($row_index, $cell_index+7, "", $normal_border);
            $sheet->write($row_index, $cell_index+8, "", $normal_border);
            $row_index++;
        }
        else
        {
            //empty lines
            $sheet->write($row_index, $cell_index, "", $normal);
            $sheet->write($row_index, $cell_index+1, "", $normal);
            $sheet->setMerge($row_index, $cell_index+2, $row_index, $cell_index+3);
            $sheet->write($row_index, $cell_index+2, "", $normal_wrap);
            $row_index++;

            $sheet->write($row_index, $cell_index, "", $normal);
            $sheet->write($row_index, $cell_index+1, "", $normal);
            $sheet->setMerge($row_index, $cell_index+2, $row_index, $cell_index+3);
            $sheet->write($row_index, $cell_index+2, "", $normal_wrap);
            $row_index++;
            $first_group = 1;
        }


        //header data

        $group = $row["lwoffergroup"];

        $sheet->setMerge($row_index, $cell_index, $row_index, $cell_index+3);
        $sheet->write($row_index, $cell_index, $row["lwoffergroup"], $bggrey);
        $sheet->write($row_index, $cell_index+4, "Action", $bggrey);
        $sheet->write($row_index, $cell_index+5, "Unit", $bggrey);
        $sheet->write($row_index, $cell_index+6, "Units", $bggrey);
        $sheet->write($row_index, $cell_index+7, "Price", $bggrey);
        $sheet->write($row_index, $cell_index+8, "Total", $bggrey);

        $row_index++;

        
    }


    
    if($row["lwofferposition_hasoffer"] == 1)
    {
        $sheet->write($row_index, $cell_index, "X", $bold_center);
    }
    else
    {
        $sheet->write($row_index, $cell_index, "", $normal_border);
    }

    $sheet->write($row_index, $cell_index+1, $row["lwofferposition_code"], $normal_border);
    $sheet->setMerge($row_index, $cell_index+2, $row_index, $cell_index+3);
    
    if(strlen($row["lwofferposition_text"]) > $maxchars2)
    {
        $sheet->setRow($row_index, $triplecell_height);
    }
    elseif(strlen($row["lwofferposition_text"]) > $maxchars)
    {
        $sheet->setRow($row_index, $doublecell_height);
    }
    $sheet->write($row_index, $cell_index+3, "", $normal_wrap_border); //empty cell
    $sheet->write($row_index, $cell_index+2, $row["lwofferposition_text"], $normal_wrap_border);
    $sheet->write($row_index, $cell_index+4, $row["lwofferposition_action"], $normal_border);
    $sheet->write($row_index, $cell_index+5, $row["lwofferposition_unit"], $normal_border);
    $sheet->write($row_index, $cell_index+6, $row["lwofferposition_numofunits"], $normal_border);
    $sheet->write($row_index, $cell_index+7, "", $normal_border);

    //$sheet->write($row_index, $cell_index+9, strlen($row["lwofferposition_text"]));
    
    $row_index2 = $row_index+1;
    $sheet->writeFormula($row_index, $cell_index+8, "=G" . $row_index2 ."*H" .$row_index2 ,$normal_border);
    

    $row_index++;

}



//empty lines
$sheet->write($row_index, $cell_index, "", $normal_border);
$sheet->write($row_index, $cell_index+1, "", $normal_border);
$sheet->setMerge($row_index, $cell_index+2, $row_index, $cell_index+3);
$sheet->write($row_index, $cell_index+2, "", $normal_wrap_border);
$sheet->write($row_index, $cell_index+3, "", $normal_wrap_border);
$sheet->write($row_index, $cell_index+4, "", $normal_border);
$sheet->write($row_index, $cell_index+5, "", $normal_border);
$sheet->write($row_index, $cell_index+6, "", $normal_border);
$sheet->write($row_index, $cell_index+7, "", $normal_border);
$sheet->write($row_index, $cell_index+8, "", $normal_border);
$row_index++;

$sheet->write($row_index, $cell_index, "", $normal_border);
$sheet->write($row_index, $cell_index+1, "", $normal_border);
$sheet->setMerge($row_index, $cell_index+2, $row_index, $cell_index+3);
$sheet->write($row_index, $cell_index+2, "", $normal_wrap_border);
$sheet->write($row_index, $cell_index+3, "", $normal_wrap_border);
$sheet->write($row_index, $cell_index+4, "", $normal_border);
$sheet->write($row_index, $cell_index+5, "", $normal_border);
$sheet->write($row_index, $cell_index+6, "", $normal_border);
$sheet->write($row_index, $cell_index+7, "", $normal_border);
$sheet->write($row_index, $cell_index+8, "", $normal_border);
$row_index++;

//print footer of form
$row_index++;
$row_index++;


$sheet->setMerge($row_index, $cell_index, $row_index, $cell_index+8);
$sheet->write($row_index, $cell_index, "", $bggrey);
$row_index++;


$sheet->write($row_index, $cell_index+1, "", $normal_wrap_border);
$sheet->write($row_index, $cell_index+2, "", $normal_wrap_border);
$sheet->write($row_index, $cell_index+3, "", $normal_wrap_border);
$sheet->write($row_index, $cell_index+4, "", $normal_wrap_border);
$sheet->setMerge($row_index, $cell_index, $row_index, $cell_index+4);
if(strlen($captions4[0]) > $maxchars)
{
    $sheet->setRow($row_index, $doublecell_height);
}
$sheet->write($row_index, $cell_index, $captions4[0], $normal_wrap_border);
//$sheet->write($row_index, $cell_index+9, strlen($captions4[0]));
$row_index++;


$sheet->write($row_index, $cell_index+1, "", $normal_wrap_border);
$sheet->write($row_index, $cell_index+2, "", $normal_wrap_border);
$sheet->write($row_index, $cell_index+3, "", $normal_wrap_border);
$sheet->write($row_index, $cell_index+4, "", $normal_wrap_border);
$sheet->setMerge($row_index, $cell_index, $row_index, $cell_index+4);
if(strlen($captions4[1]) > $maxchars)
{
    $sheet->setRow($row_index, $doublecell_height);
}
$sheet->write($row_index, $cell_index, $captions4[1], $normal_wrap_border);
//$sheet->write($row_index, $cell_index+9, strlen($captions4[1]));
$sheet->write($row_index, $cell_index+6, $captions5[3], $bold_border);
$sheet->write($row_index, $cell_index+7, "", $bold_border);
$sheet->write($row_index, $cell_index+8, $offer["currency_symbol"], $bold_border);
$row_index++;


$sheet->write($row_index, $cell_index+1, "", $normal_wrap_border);
$sheet->write($row_index, $cell_index+2, "", $normal_wrap_border);
$sheet->write($row_index, $cell_index+3, "", $normal_wrap_border);
$sheet->write($row_index, $cell_index+4, "", $normal_wrap_border);
$sheet->setMerge($row_index, $cell_index, $row_index, $cell_index+4);
if(strlen($captions4[2]) > $maxchars)
{
    $sheet->setRow($row_index, $doublecell_height);
}
$sheet->write($row_index, $cell_index, $captions4[2], $normal_wrap_border);
//$sheet->write($row_index, $cell_index+9, strlen($captions4[2]));
$sheet->write($row_index, $cell_index+6, $captions5[0], $bold_border);
$sheet->write($row_index, $cell_index+7, "", $bold_border);
$sheet->write($row_index, $cell_index+8, "", $bold_border);
$row_index++;


$sheet->write($row_index, $cell_index+1, "", $normal_wrap_border);
$sheet->write($row_index, $cell_index+2, "", $normal_wrap_border);
$sheet->write($row_index, $cell_index+3, "", $normal_wrap_border);
$sheet->write($row_index, $cell_index+4, "", $normal_wrap_border);
$sheet->setMerge($row_index, $cell_index, $row_index, $cell_index+4);
if(strlen($captions4[3]) > $maxchars)
{
    $sheet->setRow($row_index, $doublecell_height);
}
$sheet->write($row_index, $cell_index, $captions4[3], $normal_wrap_border);
//$sheet->write($row_index, $cell_index+9, strlen($captions4[3]));
$sheet->write($row_index, $cell_index+6, $captions5[1], $bold_border);
$sheet->write($row_index, $cell_index+7, "...%", $bold_border);
$sheet->write($row_index, $cell_index+8, "", $bold_border);
$row_index++;


$sheet->write($row_index, $cell_index+1, "", $normal_wrap_border);
$sheet->write($row_index, $cell_index+2, "", $normal_wrap_border);
$sheet->write($row_index, $cell_index+3, "", $normal_wrap_border);
$sheet->write($row_index, $cell_index+4, "", $normal_wrap_border);
$sheet->setMerge($row_index, $cell_index, $row_index, $cell_index+4);
//if(strlen($captions4[4]) > $maxchars)
//{
//    $sheet->setRow($row_index, $doublecell_height);
//}
//$sheet->write($row_index, $cell_index, $captions4[4], $normal_wrap_border);
$sheet->write($row_index, $cell_index, "", $normal_wrap_border);
//$sheet->write($row_index, $cell_index+9, strlen($captions4[4]));
$sheet->write($row_index, $cell_index+6, $captions5[2], $bold_border);
$sheet->write($row_index, $cell_index+7, "", $bold_border);
$sheet->write($row_index, $cell_index+8, "", $bold_border);
$row_index++;


$xls->close(); 
 
    
    

?>