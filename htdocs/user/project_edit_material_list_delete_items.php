<?php
/********************************************************************
    project_edit_material_list_delete_items.php

    Delete Items in the list of materials

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-30
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-30
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.


*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access();

$project = get_project(param("pid"));

$pid = param("pid");
$order = param("oid");
$cost_group = param("cg");

$item = param("item");


if($cost_group == '2,6,10')
{
    $text = "Are you sure to delete all fixturingfurniture items?";
}
elseif($cost_group == 2)
{
    $text = "Are you sure to delete all special items?";
}
elseif($cost_group == 3)
{
    $text = "Are you sure to delete all cost estimation positions?";
}
elseif($cost_group == 7)
{
    $text = "Are you sure to delete all local construction positions?";
}
elseif($item > 0)
{
    $text = "Are you sure to delete the item?";
}

$form = new Form();
$form->add_hidden("pid", $pid);
$form->add_hidden("oid", $order);
$form->add_hidden("cg", $cost_group);
$form->add_hidden("item", $item);


$form->add_comment($text);

if($cost_group > 0)
{
	$form->add_button("delete_items", "Yes");
}
elseif($item > 0)
{
	$form->add_button("delete_item", "Yes");
}
$form->add_button("abort", "No");


if ($form->button("delete_items"))
{
	order_delete_all_items(param("oid"), param("cg"));
    $link = "project_edit_material_list.php?pid=" . param("pid");
    redirect($link);
}
elseif ($form->button("delete_item"))
{
	order_delete_item(param("oid"), param("item"));
    $link = "project_edit_material_list.php?pid=" . param("pid");
    redirect($link);
}
else if ($form->button("abort"))
{
    $link = "project_edit_material_list.php?pid=" . param("pid");
    redirect($link);
}

$page = new Page();

require "include/project_page_actions.php";

$page->header();
$page->title("Delete Items");
$form->render();
$page->footer();
?>