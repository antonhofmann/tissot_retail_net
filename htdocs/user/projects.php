<?php
/********************************************************************

    projects.php

    Entry page for the projects section.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-08-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-06-20
    Version:        1.1.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
//require_once "include/save_functions.php";
require_once "include/order_state_constants.php";
require_once "../shared/access_filters.php";

check_access("can_view_projects");

register_param("showall");

if(param('search_term')
	or param('legaltype_filter')
	or param('postype_filter')
	or param('country_filter')
	or param('product_line_filter')
	or param('pm_filter')
	or param('lc_filter')
	or param('year_filter') 
	or param('dstate') 
	or param('lstate')) {
	param("showall", 1);
}



set_referer("projects_archive.php");


/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());
$project_state_restrictions = get_project_state_restrictions(user_id());

$show_cer_state = false;

// create sql
$sql = "select distinct project_id, project_order, project_number, project_projectkind, " .
       "project_actual_opening_date, product_line_name, postype_name, project_is_urgent, " . 
       "concat(order_shop_address_place,', ', order_shop_address_company), country_name, ".
       "    rtcs.user_name as project_retail_coordinator, rtos.user_name as order_retail_operator,".
       "    order_id, order_actual_order_state_code, order_development_status, order_logistic_status, " . 
	   "    project_cost_type, project_costtype_text,  project_postype, ".
	   " project_cost_cms_completed, project_cost_cms_approved, project_state_text, projectkind_code, " .
	   "if (
	   		project_state IN (2,4), 
	   		if(
	   			project_state = 2, 
	   			concat('<span class=\"error\">', project_state_text, '</span>'), 
	   			concat('<strong>', project_state_text, '</strong>')
	   		), 
			if (
				project_state = 5 OR project_popup_closingdate IS NOT NULL, 
				concat('<strong>closed</strong>'),
				project_real_opening_date
			)
		) as real_opening_date, " . 
	   " cer_basicdata_no_cer_submission_needed ".
       "from projects ".
       "left join orders on project_order = order_id ".
	   "inner join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
       "left join product_lines on project_product_line = product_line_id ".
       "left join postypes on postype_id = project_postype ".
	   "left join project_states on project_state_id = project_state ".
       "left join countries on order_shop_address_country = countries.country_id ".
       "left join users as rtcs on project_retail_coordinator = rtcs.user_id " .
	   "left join projectkinds on projectkind_id = project_projectkind ".
	   "left join users as rtos on order_retail_operator = rtos.user_id " . 
	   "left join cer_basicdata on cer_basicdata_project = project_id ";

$sql_count = "select count(project_id) as num_recs " . 
       "from projects ".
       "left join orders on project_order = order_id ".
	   "inner join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join project_states on project_state_id = project_state ".
       "left join product_lines on project_product_line = product_line_id ".
       "left join postypes on postype_id = project_postype ".
       "left join countries on order_shop_address_country = countries.country_id ".
       "left join users as rtcs on project_retail_coordinator = rtcs.user_id " . 
	   "left join users as rtos on order_retail_operator = rtos.user_id ";

// create list filter
$condition = "";
$list_filter = "";

if(in_array(1, $user_roles) 
	or in_array(2, $user_roles) 
	or in_array(3, $user_roles) 
	or in_array(8, $user_roles))
{

	$show_cer_state = true;
	if (has_access("has_access_to_all_projects") and has_access("can_view_all_entries_at_start_in_projects"))
    {
      //nop
	  
    }
    else if (has_access("has_access_to_all_projects") and param("showall"))
    {
		//nop
		
    }
    else if (has_access("has_access_to_all_projects") and !param("showall"))
    {
		$list_filter = "(project_retail_coordinator = " . user_id() .
                       " or project_design_supervisor = " . user_id() . 
                       " or project_design_contractor = " . user_id() . 
                       " or order_retail_operator = " . user_id() . 
                       " or order_retail_operator is null " . 
			           " or order_actual_order_state_code = '100')";
    }
    else
    {
		
		if (has_access("can_view_order_before_budget_approval_in_projects"))
		{
            $list_filter = "(project_retail_coordinator = " . user_id() .
               " or project_design_supervisor = " . user_id() . 
               " or project_design_contractor = " . user_id() . 
               " or order_retail_operator = " . user_id() . 
               " or order_retail_operator is null)";
        }
        else
        {
            $list_filter = "order_show_in_delivery = 1 ".
               " and (project_retail_coordinator = " . user_id() .
               " or project_design_supervisor = " . user_id() . 
               " or project_design_contractor = " . user_id() . 
               " or order_retail_operator = " . user_id() . 
               " or order_retail_operator is null)";
        }
    }

    if(in_array(7, $user_roles) and $list_filter) // design contractor
    {
        $list_filter.= " and project_design_contractor = "  . user_id();
    }
	elseif(in_array(7, $user_roles) and $list_filter) // design contractor
    {
		$list_filter.= " project_design_contractor = "  . user_id();
	}

}
else
{

	if(has_access("has_access_to_all_travalling_retail_data"))
	{
		$condition = get_user_specific_order_list(user_id(), 1, $user_roles, true);
	}
	else
	{
		$condition = get_user_specific_order_list(user_id(), 1, $user_roles, false);
	}

    if (has_access("has_access_to_all_projects") and has_access("can_view_all_entries_at_start_in_projects"))
    {
		$show_cer_state = true;
    }
    else if (has_access("has_access_to_all_projects") and param("showall"))
    {
		$show_cer_state = true;
    }
    else if (has_access("has_access_to_all_projects") and !param("showall"))
    {
		if ($condition == "")
        {
            //$list_filter = " order_retail_operator is null";
			$list_filter = " order_actual_order_state_code = '100'";
        }
        else
        {
            if($list_filter)
			{
				//$list_filter = " and (" . $condition . " or order_retail_operator is null)";
				$list_filter = " and (" . $condition . ")";
			}
			else
			{
				//$list_filter = " (" . $condition . " or order_retail_operator is null)";
				$list_filter = " (" . $condition . ")";
			}
        }
		$show_cer_state = true;
    }
    else
    {
		if ($condition == "")
        {
            // order_type = 0 is a work around to sho no orders
            $list_filter = "order_type = 0 ";
        }
        else
        {
           
			if (has_access("can_view_order_before_budget_approval_in_projects"))
            {   
				$list_filter = " (" . $condition . ")";
				
            }
			
        else
            {
               
				$list_filter = " order_show_in_delivery = 1 ".
                               "   and (" . $condition . ")";

            }
        }
    }
}

//get filter for country responsbales
if (!has_access("has_access_to_all_projects"))
{
	$company_access_filter = get_users_regional_access_to_projects(user_id());
	if($company_access_filter)
	{
		if($list_filter)
		{
			$list_filter = "(( " . $list_filter . " ) or ( " . $company_access_filter . ")) ";
		}
		else
		{
			$list_filter = "(" . $company_access_filter . ") ";
		}
	}
}


if($list_filter)
{
	$list_filter .= " and (order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '890'  ";
}
else
{
	$list_filter = " (order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '890'  ";
}

if(param('search_term') and in_array(2, $user_roles))
{
	//special rule for logistic coordinators
}
elseif(count($user_roles) >1 
	and in_array(7, $user_roles)) //design contractor
{
	//special rule for users being design contracors at the same time
}
else
{
	if($project_state_restrictions['from_state'])
	{
		$list_filter .= " and order_actual_order_state_code >= '" . $project_state_restrictions['from_state'] . "' ";
	}
	if($project_state_restrictions['to_state'])
	{
		$list_filter .= " and order_actual_order_state_code <= '" . $project_state_restrictions['to_state'] . "' ";
	}
}

//check if there is only one project

if(param('search_term')) {

	$search_term = trim(param('search_term'));
	$list_filter .= ' and (project_number like "%' . $search_term . '%" 
	                  or postype_name like "%' . $search_term . '%" 
					  or project_costtype_text like "%' . $search_term . '%"
					  or project_state_text like "%' . $search_term . '%"
					  or product_line_name like "%' . $search_term . '%"
					  or postype_name like "%' . $search_term . '%"
					  or order_actual_order_state_code like "%' . $search_term . '%"
					  or order_shop_address_place like "%' . $search_term . '%"
					  or order_shop_address_company like "%' . $search_term . '%"
					  or country_name like "%' . $search_term . '%")';

	$sql_count = $sql_count . ' where ' . $list_filter;
	$res = mysql_query($sql_count) or dberror($sql_count);

	if ($row = mysql_fetch_assoc($res))
	{
		if ($row['num_recs'] == 1)
		{
			$sql = $sql . ' where ' . $list_filter;
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$link = "project_task_center.php?pid=" . $row['project_id'];
				redirect($link);
			}
		}
	}
}


if(param('legaltype_filter')) {
	$list_filter .= " and project_cost_type  = " . param('legaltype_filter') . " ";
}

if(param('postype_filter')) {
	$list_filter .= " and project_postype  = " . param('postype_filter') . " ";
}

if(param('country_filter')) {
	$list_filter .= " and order_shop_address_country  = " . param('country_filter') . " ";
}

if(param('product_line_filter')) {
	$list_filter .= " and  project_product_line = " . param('product_line_filter') . " ";
}

if(param('pm_filter')) {
	$list_filter .= " and  project_retail_coordinator = " . param('pm_filter') . " ";
}

if(param('lc_filter')) {
	$list_filter .= " and  order_retail_operator = " . param('lc_filter') . " ";
}

if(param('year_filter')) {
	$list_filter .= " and  year(projects.date_created) = " . param('year_filter') . " ";
}


$list_filter.= " and (cer_basicdata_version = 0 or cer_basicdata_version is null or cer_basicdata_version = '')";


// create list filter DROP DOWNS

if (has_access("has_access_to_all_projects") or has_access("can_view_all_entries_at_start_in_projects"))
{
    $sql_product_lines = "select distinct product_line_id, product_line_name ".
                         "from projects ".
                         "left join product_lines on  project_product_line = product_line_id ".
                         "left join orders on project_order = order_id ".
                         "where (order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '890' and project_product_line > 0 ".
                         "order by product_line_name";
    
    $sql_pos_types = "select distinct postype_id, postype_name ".
					 "from projects ".
					 "left join postypes on  postype_id = project_postype ".
					 "left join orders on project_order = order_id ".
					 "where (order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '890'  ".
					 "order by postype_name";

	$sql_legal_types =   "select distinct project_costtype_id, project_costtype_text " . 
		                 "from projects " .
		                 "left join orders on project_order = order_id ".
		                 "left join project_costs on project_cost_order = project_order " .
		                 "left join project_costtypes on project_costtype_id = project_cost_type " . 
		                 "where (order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '890'  ".
                         "order by project_costtype_text";


	$sql_rtcs = "select DISTINCT user_id, concat(user_name) as user_name " . 
		        "from projects " .
		        "left join orders on project_order = order_id ".
		        "left join users on user_id = project_retail_coordinator " . 
		        "where user_name <> '' and (order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '890' " . 
		        "order by user_name ";


	$sql_rtos = "select DISTINCT user_id, concat(user_name) as user_name " . 
		        "from projects " .
		        "left join orders on project_order = order_id ".
		        "left join users on user_id = order_retail_operator " . 
		        "where user_name <> '' and (order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '890' " . 
		        "order by user_name ";


	$sql_countries = "select distinct country_id, country_name ".
                     "from projects ".
                     "left join orders on project_order = order_id ".
					 "left join countries on order_shop_address_country = country_id ".
                     "where (order_archive_date is null or order_archive_date = '0000-00-00') " . 
		             " and order_actual_order_state_code < '890'  ".
                     " order by country_name";

	$sql_years = "select distinct year(projects.date_created) as value, year(projects.date_created) as name ".
                         "from projects ".
                         "left join product_lines on  project_product_line = product_line_id ".
                         "left join orders on project_order = order_id ".
                         "where (order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '890' and project_product_line > 0 ".
                         "order by product_line_name";
	
}
else
{
    $sql_product_lines = "select distinct product_line_id, product_line_name ".
                         "from projects ".
                         "left join product_lines on  project_product_line = product_line_id ".
                         "left join orders on project_order = order_id ".
						 "left join order_items on order_item_order = order_id ".
                         "left join addresses on order_client_address = address_id ".
                         "where project_product_line > 0 ".
                         "    and (";
    
    $sql_pos_types = "select distinct postype_id, postype_name ".
                         "from projects ".
                         "left join postypes on  postype_id = project_postype ".
                         "left join orders on project_order = order_id ".
		                 "left join order_items on order_item_order = order_id ".
                         "left join addresses on order_client_address = address_id ".
                         "where (";

    
	$sql_legal_types =   "select distinct project_costtype_id, project_costtype_text " . 
		                 "from projects " . 
		                 "left join project_costs on project_cost_order = project_order " .
		                 "left join project_costtypes on project_costtype_id = project_cost_type " .
		                 "left join orders on project_order = order_id ".
                         "left join order_items on order_item_order = order_id ".
                         "left join addresses on order_client_address = address_id ".
		                 "where (";


	$sql_rtcs = "select DISTINCT user_id, concat(user_name) as user_name " . 
		        "from projects " .
		        "left join orders on project_order = order_id ".
		        "left join order_items on order_item_order = order_id ".
		        "left join users on user_id = project_retail_coordinator " . 
		        "where user_name <> '' and (";


	$sql_rtos = "select DISTINCT user_id, concat(user_name) as user_name " . 
		        "from projects " .
		        "left join orders on project_order = order_id ".
		        "left join order_items on order_item_order = order_id ".
		        "left join users on user_id = order_retail_operator " . 
		        "where user_name <> '' and (";
	
	
	$sql_countries = "select distinct country_id, country_name ".
                     "from projects ".
                     "left join orders on project_order = order_id ".
		             "left join project_costs on project_cost_order = order_id " . 
					 "left join order_items on order_item_order = order_id " . 
					 "left join countries on order_shop_address_country = country_id ";
                     

	$country_access_filter = get_users_regional_access_to_countries(1, user_id());
	if($country_access_filter)
	{
		$sql_countries .= "where (" . $country_access_filter . ") or ((";
	}
	else
	{
		$sql_countries .= "where ((";
	}


	$sql_years = "select distinct year(projects.date_created) as value, year(projects.date_created) as name ".
				 "from projects ".
				 "left join product_lines on  project_product_line = product_line_id ".
				 "left join orders on project_order = order_id ".
				 "left join order_items on order_item_order = order_id ".
				 "left join addresses on order_client_address = address_id ".
				 "where project_product_line > 0 and (";
	
	$filter_tmp = "order_item_supplier_address = " . $user_data["address"] . " " .
                  "or order_item_forwarder_address = " . $user_data["address"] . " " .
                  "or order_retail_operator = " . user_id() . " " .
                  "or project_retail_coordinator = " . user_id()  . " " .
                  "or project_design_contractor = " . user_id()  . " " .
                  "or project_design_supervisor = " . user_id()   . " " .
                  "or order_client_address = " . $user_data["address"];

	
	if($condition)
	{
		$filter_tmp = $condition;
	}
	

	

	$only_active_projects = " and (order_archive_date is null or order_archive_date = '0000-00-00') " . 
		             " and order_actual_order_state_code < '890' ";

	$sql_product_lines = $sql_product_lines . $filter_tmp . ") " . $only_active_projects . " order by product_line_name";

    $sql_pos_types = $sql_pos_types . $filter_tmp . ") " . $only_active_projects . "order by postype_name";

	$sql_legal_types = $sql_legal_types . $filter_tmp  . ") " . $only_active_projects . "order by project_costtype_text";

	$sql_rtcs = $sql_rtcs . $filter_tmp  . ") " . $only_active_projects . "order by user_name";
	$sql_rtos = $sql_rtos . $filter_tmp  . ") " . $only_active_projects . "order by user_name";

	$sql_countries = $sql_countries . $filter_tmp  . ")) " . $only_active_projects . "order by country_name";

	$sql_years = $sql_years . $filter_tmp   . ") " . $only_active_projects;

}


$legaltype_filter = array();
$postype_filter = array();
$country_filter = array();
$product_line_filter = array();
$pm_filter = array();
$lc_filter = array();
$year_filter = array();

$res = mysql_query($sql_legal_types);
while($row = mysql_fetch_assoc($res))
{
	$legaltype_filter[$row["project_costtype_id"]] = $row["project_costtype_text"];
}

$res = mysql_query($sql_pos_types);
while($row = mysql_fetch_assoc($res))
{
	$postype_filter[$row["postype_id"]] = $row["postype_name"];
}

$res = mysql_query($sql_countries);
while($row = mysql_fetch_assoc($res))
{
	$country_filter[$row["country_id"]] = $row["country_name"];
}

$res = mysql_query($sql_product_lines);
while($row = mysql_fetch_assoc($res))
{
	$product_line_filter[$row["product_line_id"]] = $row["product_line_name"];
}

$res = mysql_query($sql_rtcs);
while($row = mysql_fetch_assoc($res))
{
	$pm_filter[$row["user_id"]] = $row["user_name"];
}

$res = mysql_query($sql_rtos);
while($row = mysql_fetch_assoc($res))
{
	$lc_filter[$row["user_id"]] = $row["user_name"];
}

$res = mysql_query($sql_years);
while($row = mysql_fetch_assoc($res))
{
	$year_filter[$row["value"]] = $row["name"];
}
asort($year_filter);

// get 2do column (show if user has to do something)
//$sql_images = $sql . " where " . $list_filter;
//$images = set_to_do_pictures($sql_images, 1);


$images = array();
$cms_states = array();

$sql_images = "select project_id from tasks " .
	   "left join orders on order_id = task_order " .
	   "left join projects on project_order = order_id " .
	   "left join project_costs on project_cost_order = order_id " .
	   "left join project_costtypes on project_costtype_id = project_cost_type " .
	   "left join project_states on project_state_id = project_state ".
	   "left join product_lines on project_product_line = product_line_id ".
	   "left join postypes on postype_id = project_postype ".
	   "left join countries on order_shop_address_country = countries.country_id ".
	   "left join cer_basicdata on cer_basicdata_project = project_id " .
	   "where task_user = " . user_id() .
	   "   and task_done_date is null " .
	   "   and " . $list_filter;


$res = mysql_query($sql_images) or dberror($sql_images);

while ($row = mysql_fetch_assoc($res))
{
    $images[$row["project_id"]] = "/pictures/todo.gif";
}

if (has_access("can_edit_retail_data"))
{
    
	
	/*
	$sql_images = "select project_id from projects " .
				  "left join orders on order_id = project_order " .
				  "left join project_costs on project_cost_order = order_id " .
				   "left join project_costtypes on project_costtype_id = project_cost_type " .
				   "left join product_lines on project_product_line = product_line_id ".
				   "left join postypes on postype_id = project_postype ".
				   "left join countries on order_shop_address_country = countries.country_id ".
					"left join project_states on project_state_id = project_state ".
		          "left join cer_basicdata on cer_basicdata_project = project_id " .
				  "where (order_retail_operator is null " .
				  "or project_retail_coordinator is null " .
				  "or project_real_opening_date is null " . 
				  "or project_real_opening_date = '0000-00-00') " .
				  "and order_actual_order_state_code = '100' ".
				  "   and " . $list_filter;
	*/
	$sql_images = "select project_id from projects " .
				  "left join orders on order_id = project_order " .
				  "left join project_costs on project_cost_order = order_id " .
				   "left join project_costtypes on project_costtype_id = project_cost_type " .
				   "left join product_lines on project_product_line = product_line_id ".
				   "left join postypes on postype_id = project_postype ".
				   "left join countries on order_shop_address_country = countries.country_id ".
					"left join project_states on project_state_id = project_state ".
		          "left join cer_basicdata on cer_basicdata_project = project_id " .
				  "where (project_retail_coordinator is null " .
				  "or project_real_opening_date is null " . 
				  "or project_real_opening_date = '0000-00-00') " .
				  "and order_actual_order_state_code = '100' ".
				  "   and " . $list_filter;

    $res = mysql_query($sql_images) or dberror($sql_images);

    while ($row = mysql_fetch_assoc($res))
    {
        $images[$row["project_id"]] = "/pictures/bullet_ball_glass_red.gif";
    }
}


// check if all 1:n relations in the process are fully processed
$fully_processed = array();
$duty_free_projects = array();
$cer_approved = array();
$project_states = array();
$logistic_states = array();
$logistic_states_tmp = array();

$dstate_filter = array();

$dstate_filter["100-140"] = "100-140";
$dstate_filter["200-290"] = "200-290";
$dstate_filter["330-380"] = "330-380";
$dstate_filter["400-417"] = "400-417";
$dstate_filter["420-441"] = "420-441";
//$dstate_filter["510-560"] = "510-560";
$dstate_filter["600-670"] = "600-670";
//$dstate_filter["800-851"] = "800-851";


$lstate_filter = array();
$lstate_filter_sup_forw = array();
$project_states_projects = array();
$logistic_states_projects = array();

$excluded_projects = array();

$sql_tmp = $sql . " where " . $list_filter;
$res = mysql_query($sql_tmp);
while($row = mysql_fetch_assoc($res))
{
	$att = "";
	//$result = save_project_development_status($row["project_order"]);

	if($row["project_is_urgent"] == 1 and !in_array($row["project_id"], $images))
	{
		
		
		
		if($row["project_cost_type"] == 1
			and ($row["project_projectkind"] == 2
			or $row["project_projectkind"] == 3)
			and is_present_order_state($row["order_id"], '840') != 1) {
			$images[$row["project_id"]] = "/pictures/urgent.png";
		}
		elseif(is_present_order_state($row["order_id"], '840') != 1 
			and is_present_order_state($row["order_id"], '620') != 1) {
			$images[$row["project_id"]] = "/pictures/urgent.png";
		}
	}

	if(count($user_roles) == 1 
	and (in_array(5, $user_roles) or in_array(6, $user_roles)))
	{
		$tmp = get_external_losgistics_state($row["project_order"], $user_data["address"]);
		if(count($tmp) > 0)
		{
			$project_states[$row['project_id']] = $tmp["code"];

			$lstate_filter_sup_forw[$tmp["code"]] = $tmp["code"];

			if(param("lstate") and param("lstate")!= $tmp["code"]) {
				$excluded_projects[] = $row["project_id"];
			}
		}
		else
		{
			//$development_state = get_project_development_status($row["project_order"]);
			//$project_states[$row['project_id']] = $development_state["code"];
		}

	
	}
	else
	{

		
		//$development_state = $row["order_development_status"];
		$development_state = $row["order_actual_order_state_code"];
		if($development_state)
		{
			$dstate_filter[$development_state] = $development_state;

			if(!array_key_exists($development_state, $project_states_projects))
			{
				$project_states_projects[$development_state] = array();
			}
			array_push($project_states_projects[$development_state], $row["project_id"]);
		}			
		
		
		if(has_access("has_access_to_order_status_report_in_projects"))
		{
			$popup_link = "javascript:popup('project_status_report.php?pid=" .$row["project_id"] . "', 800, 640)";
			$project_states[$row['project_id']] = '<a href="' . $popup_link . '">' . $development_state . '</a>';
		}
		else
		{
			$project_states[$row['project_id']] = $development_state;
		}

		/*
		if(has_access("can_see_logistic_status"))
		{
			$logistic_state = $row["order_logistic_status"];
			$logistic_states[$row['project_id']] = $logistic_state;

			if($logistic_state)
			{
				$lstate_filter[$logistic_state] = $logistic_state;
				if(!array_key_exists($logistic_state, $logistic_states_projects))
				{
					$logistic_states_projects[$logistic_state] = array();
				}
				array_push($logistic_states_projects[$logistic_state], $row["project_id"]);
			}
		}
		*/
	}


	if($row['order_actual_order_state_code'] >= ORDER_TO_SUPPLIER_SUBMITTED)
	{
		//check if all items were ordered
		$ordered = check_if_all_items_hav_order_date($row['order_id'], "");
		if ($ordered == 0)
		{
			$fully_processed[$row['project_id']] = "/pictures/wf_warning.gif";
		}
	}
	elseif($row['order_actual_order_state_code'] >= REQUEST_FOR_DELIVERY_SUBMITTED)
	{
		//check if a request for delivery was made for all items
		$delivered = check_requast_for_deleivery_for_all_items($row['order_id'], REQUEST_FOR_DELIVERY_SUBMITTED);
		if ($delivered == 0)
		{
			$fully_processed[$row['project_id']] = "/pictures/wf_warning.gif";
		}
	}

	if($row['project_cost_cms_completed'] == 1)
	{
		$cms_states[$row["project_id"]] = "/pictures/ok.gif";
	}
	else
	{
		$cms_states[$row["project_id"]] = "/pictures/not_ok.gif";
	}
	
	
	//check if project is a duty free project
	$project_design_objective_item_ids = get_project_design_objective_item_ids($row["project_id"]);
	
	if(array_key_exists(161 ,$project_design_objective_item_ids))
	{
		$duty_free_projects[$row["project_id"]] = "/pictures/dutyfree.png";
	}

	//check if cer was approved (miletones id = 21 or 12)
	if($show_cer_state == true)
	{
		if(($row['project_cost_type'] == 6 or $row['project_cost_type'] == 2) and $row['project_postype'] == 2)
		{
			//do nothing
		}
		elseif($row['order_actual_order_state_code'] == '620')
		{
			
			/*old code before new project steps
			$mileston_date_set = false;
			$sql_m = "select project_milestone_date " . 
				     "from project_milestones " . 
				     "where project_milestone_project = " . $row["project_id"] . 
				     " and project_milestone_milestone in (12, 21, 30)";

			$res_m = mysql_query($sql_m) or dberror($sql_m);
			while ($row_m = mysql_fetch_assoc($res_m))
			{
				if($row_m["project_milestone_date"] != NULL and $row_m["project_milestone_date"] != '0000-00-00')
				{
					$mileston_date_set = true;
				}
			}

			if($row["cer_basicdata_no_cer_submission_needed"] == 1)
			{
				//echo $row['project_id'] . "<br />";
				$cer_approved[$row['project_id']] = "/pictures/money_ok.gif";
			}
			elseif($mileston_date_set == false and $row["cer_basicdata_no_cer_submission_needed"] != 1)
			{
				$cer_approved[$row['project_id']] = "/pictures/money_not_ok.gif";
			}
			else
			{
				$cer_approved[$row['project_id']] = "/pictures/money_ok.gif";
			}
			*/

			//always show cash ok if step is 620
			$cer_approved[$row['project_id']] = "/pictures/money_ok.gif";
		}
	}	
	

}

if(in_array(2, $user_roles))
{
	$list_filter .= " and ((project_cost_cms_completed = 0) or (project_cost_cms_completed = 1 and order_actual_order_state_code < 800))";
}

if(in_array(4, $user_roles) and $user_data["user_can_only_see_projects_of_his_address"] == 1)
{
	$list_filter.= " and order_client_address = " . dbquote($user_data["address"]);
}


$dstate_order = false;
if(param("dstate") and strlen(param("dstate")) > 3)
{
	$tmp = explode('-', param("dstate"));
	$tmp_projects = array();

	foreach($project_states_projects as $dstate=>$value)
	{
		if($dstate >= $tmp[0] and $dstate <= $tmp[1] and array_key_exists($dstate, $project_states_projects))
		{
			$tmp_projects = array_merge ($project_states_projects[$dstate], $tmp_projects);
		}
	}
	if(count($tmp_projects) > 0)
	{
		$list_filter.= " and project_id in (" . implode(',', $tmp_projects) . ") ";
		$dstate_order = true;
	}
}
elseif(param("dstate") and count($project_states_projects[param("dstate")]) > 0)
{
	$list_filter.= " and project_id in (" . implode(',', $project_states_projects[param("dstate")]) . ") ";
}

if(count($excluded_projects)) {
	$list_filter.= " and project_id not in  ( " . implode(',', $excluded_projects) . ") ";
}
elseif(param("lstate") and count($logistic_states_projects[param("lstate")]) > 0)
{
	$list_filter.= " and project_id in ( " . implode(',', $logistic_states_projects[param("lstate")]) . ") ";
}

asort($dstate_filter);
asort($lstate_filter);
asort($lstate_filter_sup_forw);

//echo $sql . " where " . $list_filter;

//this session value is needed for printing the user specific project's masterplan.
$_SESSION["project_lits_filter_settings"] = $list_filter;

/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);
$list->add_hidden("showall", param("showall"));

$list->set_entity("projects");

if($dstate_order)
{
	$list->set_order("order_actual_order_state_code");
}
else
{
	$list->set_order("left(projects.date_created, 10) desc");
}

$list->set_filter($list_filter);   


//filter section
if (has_access("has_access_to_all_projects") or has_access("can_view_all_entries_at_start_in_projects"))
{
	$list->add_listfilters("search_term", "Search Term", 'input', "", param("search_term"));
	$list->add_listfilters("country_filter", "Country", 'select', $country_filter, param("country_filter"));
	$list->add_listfilters("legaltype_filter", "Legal Type", 'select', $legaltype_filter, param("legaltype_filter"));
	$list->add_listfilters("postype_filter", "POS Type", 'select', $postype_filter, param("postype_filter"));
	//$list->add_listfilters("product_line_filter", "Furniture", 'select', $product_line_filter, param("product_line_filter"));
	$list->add_listfilters("pm_filter", "PL", 'select', $pm_filter, param("pm_filter"));
	//$list->add_listfilters("lc_filter", "LC", 'select', $lc_filter, param("lc_filter"));
	$list->add_listfilters("year_filter", "Year", 'select', $year_filter, param("year_filter"));

	$list->add_listfilters("dstate", "Status", 'select', $dstate_filter, param("dstate"));
	
	/*
	if(has_access("can_see_logistic_status"))
	{
		$list->add_listfilters("lstate", "Status L", 'select', $lstate_filter, param("lstate"));
	}
	*/

}
else
{

	$list->add_listfilters("search_term", "Search Term", 'input', "", param("search_term"));
	$list->add_listfilters("country_filter", "Country", 'select', $country_filter, param("country_filter"));
	$list->add_listfilters("legaltype_filter", "Legal Type", 'select', $legaltype_filter, param("legaltype_filter"));
	$list->add_listfilters("postype_filter", "POS Type", 'select', $postype_filter, param("postype_filter"));
	//$list->add_listfilters("product_line_filter", "Furniture", 'select', $product_line_filter, param("product_line_filter"));
	$list->add_listfilters("pm_filter", "PL", 'select', $pm_filter, param("pm_filter"));
	//$list->add_listfilters("lc_filter", "LC", 'select', $lc_filter, param("lc_filter"));
	$list->add_listfilters("year_filter", "Year", 'select', $year_filter, param("year_filter"));
	
	if(count($user_roles) == 1 
	and (in_array(5, $user_roles) or in_array(6, $user_roles)))
	{
		//$list->add_listfilters("lstate", "Status L", 'select', $lstate_filter_sup_forw, param("lstate"));

	}
	/*
	elseif(has_access("can_see_logistic_status"))
	{
		$list->add_listfilters("lstate", "Status L", 'select', $lstate_filter, param("lstate"));
	}
	*/
	

	
}


$list->add_image_column("todo", "Job", 0, $images);
if (preg_match("/" . APPLICATION_URLPART . "/", $_SERVER["HTTP_HOST"]))
{
	$link = APPLICATION_URL . "/user";
}
else
{
	$link = "/user";
}


$list->add_column("project_number", "Project No.", $link . "/project_task_center.php?pid={project_id}", "", "", COLUMN_NO_WRAP);
$list->add_column("project_costtype_text", "Legal Type", "", "", "", COLUMN_NO_WRAP);
$list->add_column("projectkind_code", "Kind", "", 0, "", COLUMN_NO_WRAP);
$list->add_column("product_line_name", "Product Line", "", "", "", COLUMN_NO_WRAP);
$list->add_column("postype_name", "POS Type", "", "", "", COLUMN_NO_WRAP);


if (has_access("has_access_to_all_projects"))
{
	$list->add_image_column("order_state", "", 0, $fully_processed);
}


if(count($user_roles) == 1 
and (in_array(5, $user_roles) or in_array(6, $user_roles)))
{
	$list->add_text_column("state", "Status", COLUMN_UNDERSTAND_HTML | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $project_states);
}
else
{
	
	//$list->add_text_column('state',"Status", COLUMN_UNDERSTAND_HTML | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $project_states);
	
	if(has_access("has_access_to_order_status_report_in_projects"))
	{
		$popup_link = "popup1:project_status_report.php?pid=";
		//$list->add_column("order_development_status", "Status", $popup_link . "{project_id}" , "", "", COLUMN_ALIGN_RIGHT |COLUMN_NO_WRAP);
		$list->add_column("order_actual_order_state_code", "Status", $popup_link . "{project_id}" , "", "", COLUMN_ALIGN_RIGHT |COLUMN_NO_WRAP);
	}
	else
	{
		//$list->add_column("order_development_status", "Status", "", "", "", COLUMN_ALIGN_RIGHT |COLUMN_NO_WRAP);
		$list->add_column("order_actual_order_state_code", "Status", "", "", "", COLUMN_ALIGN_RIGHT |COLUMN_NO_WRAP);
	}
}

/*
if(has_access("can_see_logistic_status"))
{
	//$list->add_text_column("state2", "Status L", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $logistic_states);
	$list->add_column("order_logistic_status", "Status L", "", "", "", COLUMN_ALIGN_RIGHT |COLUMN_NO_WRAP);
	//$list->add_text_column("state2", "Status L tmp", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $logistic_states_tmp);
}
*/

if($show_cer_state == true)
{
	$list->add_image_column("cer_approved", "", 0, $cer_approved);
}


$list->add_column("real_opening_date", "Agreed \nOpening Date", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK | COLUMN_UNDERSTAND_HTML);


$list->add_column("country_name", "Country", "", "", "");
$list->add_column("concat(order_shop_address_place,', ', order_shop_address_company)", "POS Address");

$list->add_column("project_retail_coordinator", "PL" , "", "", "");
//$list->add_column("order_retail_operator", "RTLC", "", "", "");

$list->add_image_column("dutyfree", "DF", 0, $duty_free_projects);



if (has_access("has_access_to_all_projects"))
{
	$list->add_image_column("cms", "CMS", 0, $cms_states);
}

if (has_access("has_access_to_all_projects") and has_access("can_view_all_entries_at_start_in_projects"))
{
}
else if (has_access("has_access_to_all_projects"))
{
    
	if(param('search_term') and param("showall")) {
		$list->add_button("show_all", "Show Complete List");
	}
	elseif (param("showall"))
    {
        $list->add_button("show_my_list", "Show My List");
    }
    else
    {
        $list->add_button("show_all", "Show Complete List");
    }
}

if (has_access("can_view_extended_view_in_projects"))
{
   //$list->add_button("extend", "Extended View");
}
if (has_access("has_access_to_archive"))
{
   $list->add_button("archive", "Access Archive");
}

if(has_access("has_access_to_all_projects"))
{
	if(param('search_term')
		or param('legaltype_filter')
		or param('postype_filter')
		or param('country_filter')
		or param('product_line_filter')
		or param('pm_filter')
		or param('lc_filter')
		or param('year_filter'))
	{		
		$link = "../mis/projects_query_31_xls.php?cfplist=1";
		$link = "javascript:popup('" . $link . "');";
		$list->add_button("print_master_plan", "Print Masterplan", $link);
	}
	
}
elseif(has_access("can_print_masterplan"))
{
	$link = "../mis/projects_query_31_xls.php?cfplist=1";
	$link = "javascript:popup('" . $link . "');";
	$list->add_button("print_master_plan", "Print Masterplan", $link);
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$list->process();


if ($list->button("new"))
{
    redirect("project_new_01.php");
}
if ($list->button("show_all"))
{
    $link = "projects.php?showall=1";
    redirect($link);
}
else if ($list->button("show_my_list"))
{
    $link = "projects.php";
    redirect($link);
}
else if ($list->button("extend"))
{
    if (param("showall"))
    {
        $link = "projects_extended_view.php?showall=1";
    }
    else
    {
        $link = "projects_extended_view.php";
    }
    redirect($link);
}
else if ($list->button("archive"))
{
    $link = "../archive/projects_archive.php";
    redirect($link);
}

$page = new Page("projects");

require_once("include/projects_list_page_actions.php");

$page->header();
$page->title("Projects");


$list->render();

?>

<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#dashboard').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
	  width: 840,
	  url: '/user/dashboard_projects.php'
    });
    return false;
  }); 
});
</script>

<?php

$page->footer();
?>