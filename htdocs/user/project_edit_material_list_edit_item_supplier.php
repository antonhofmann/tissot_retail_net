<?php
/********************************************************************

    project_edit_material_list_edit_item_supplier.php

    Edit item position in material list for suppliers' use only.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2003-01-03
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-11-13
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_edit_his_list_of_materials_in_projects");


register_param("pid");
register_param("id");
set_referer("project_edit_material_list_supplier.php");

/********************************************************************
    prepare all data needed
*********************************************************************/

// read project and order details
$project = get_project(param("pid"));
$order_state_code = $project["order_actual_order_state_code"];

// get company's address
$client_address = get_address($project["order_client_address"]);

// get order_item's information
$order_item = get_order_item(id());

// get supplier's currency
$supplier_currency_symbol = "";
if($order_item["item"])
{
    $supplier_currency_symbol = get_item_currency_symbol($order_item["supplier"], $order_item["item"]);
}
if (!$supplier_currency_symbol)
{
    $supplier_currency_symbol = get_currency_symbol($order_item["supplier_currency"]);
}



$sql_units = "select unit_id, unit_name " . 
             " from units " . 
			 " order by unit_name";

$sql_packaging_types = "select packaging_type_id, packaging_type_name " . 
             " from packaging_types " . 
			 " order by packaging_type_name";

/********************************************************************
    build form
*********************************************************************/
$form = new Form("order_items", "item from list of materials");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid",$project["project_order"]);
$form->add_hidden("order_item_item", $order_item["item"]);
$form->add_hidden("order_item_id", id());
$form->add_hidden("order_item_type", $order_item["type"]);
$form->add_hidden("order_item_supplier_address");
$form->add_hidden("order_item_currency", $order_item["supplier_currency"]);

require_once "include/project_head_small.php";

$form->add_section("Item Information");
if ($order_item["type"] == ITEM_TYPE_STANDARD)
{
    $form->add_label("order_item_code", "Code", 0, $order_item["code"]);
	$form->add_label("order_item_text", "Description*");
}

if($order_item["only_quantity_proposal"] == 1)
{
	$form->add_comment("This item only needs your quantity proposal.");
}

$form->add_edit("order_item_quantity", "Quantity*", NOTNULL);

$form->add_hidden("order_item_client_price");
$form->add_hidden("order_item_system_price");
$form->add_hidden("order_item_supplier_system_price");


//if ($order_item["type"] == ITEM_TYPE_SPECIAL and 
//   (($order_item["supplier_price"] == 0 or $order_state_code < 600) or $order_state_code == 550)
if ($order_item["type"] == ITEM_TYPE_SPECIAL and 
   (($order_item["supplier_price"] == 0 or $order_state_code < 600))
   )
{
	$form->add_multiline("order_item_text", "Description*", 4, NOTNULL);
    $form->add_edit("order_item_quantity", "Quantity*", NOTNULL);
    if($order_item["only_quantity_proposal"] == 1)
	{
		$form->add_hidden("order_item_supplier_price", 0);
	}
	else 
	{
		$form->add_edit("order_item_supplier_price", "Price in " . $supplier_currency_symbol, 0, "", TYPE_DECIMAL, 20,2);
	}
    
	
	
	$form->add_section("Volumes and Weight");
	$form->add_list("order_item_unit_id", "Unit",$sql_units, 0, $order_item["order_item_unit_id"]);
	$form->add_edit("order_item_width", "Width in cm", 0, "" , TYPE_DECIMAL, 10, 2);
	$form->add_edit("order_item_height", "Height in cm", 0, "" , TYPE_DECIMAL, 10, 2);
	$form->add_edit("order_item_length", "length in cm", 0, "" , TYPE_DECIMAL, 10, 2);
	$form->add_edit("order_item_gross_weight", "Gross Weight in kg", 0, "" , TYPE_DECIMAL, 10, 2);
	$form->add_list("order_item_packaging_type_id", "Packaging",$sql_packaging_types, 0, $order_item["order_item_packaging_type_id"]);
	$form->add_checkbox("order_item_stackable", "", $order_item["order_item_stackable"], 0, "Stackable");
	
	
	$form->add_section("Other Information");
	$form->add_edit("order_item_supplier_item_code", "Supplier's Item Code");
    $form->add_edit("order_item_offer_number", "Supplier's Offer Number");
    $form->add_edit("order_item_production_time", "Production Time");


	$form->add_button("save_data", "Save");
    $form->add_button(FORM_BUTTON_DELETE, "Delete");
}
elseif ($order_item["type"] == ITEM_TYPE_SPECIAL and $order_item["supplier_price"] > 0)
{
    $form->add_label("order_item_text", "Description");
    $form->add_label("order_item_quantity", "Quantity");
    $form->add_label("order_item_supplier_price", "Price in " . $supplier_currency_symbol);

    
	$form->add_hidden("order_item_unit_id");
	$form->add_hidden("order_item_width");
	$form->add_hidden("order_item_height");
	$form->add_hidden("order_item_length");
	$form->add_hidden("order_item_gross_weight");
	$form->add_hidden("order_item_packaging_type_id");
	$form->add_hidden("order_item_stackable");
	
	$form->add_label("order_item_supplier_item_code", "Supplier's Item Code");
    $form->add_label("order_item_offer_number", "Supplier's Offer Number");
    $form->add_label("order_item_production_time", "Production Time");

}
else
{
	$form->add_label("order_item_supplier_price", "Price in " . $supplier_currency_symbol);
}




$form->add_button(FORM_BUTTON_BACK, "Back");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("save_data"))
{
    if ($form->validate())
    {
         $link = "project_edit_material_list_supplier.php?pid=" . param("pid");
         project_edit_order_item_save_supplier_data($form);
         redirect($link);
    }
}


   
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("List of Materials: Edit Supplier's Item Position");
$form->render();

require_once "include/project_footer_logistic_state.php";
$page->footer();

?>