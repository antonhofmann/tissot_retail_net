<?php
/********************************************************************

    order_checkout.php

    Checkout for new catalog-order

    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2002-09-01
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-06-15
    Version:        1.0.8

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
define('BILLING_ADDRESS', 1);
define('DELIVERY_ADDRESS',2);
define('SHOP_ADDRESS',3);
define('ADDRESS_ADDRESS', 4);

require_once "../include/frame.php";
require_once "include/order_functions.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_view_orders");

//coices for delivery address
$user = get_user(user_id());

if(param("address_id") > 0)
{
	$address = get_address(param("address_id"));
}
else
{
	$address = get_address($user["address"]);
}



$choices = array();

if(param("posaddress_id") or param('target_order_id') > 0)
{
	$choices[1] = "POS location";
}

$choices[2] = $address["place"] . ": " . $address['company'] . ", " . $address['address'] . ", " . $address['contact_name'];



//get all delivery addresses ever used
$delivery_addresses_used = get_delivery_addresses($address['id']);

foreach($delivery_addresses_used as $key=>$value)
{
	if(!in_array($value, $choices))
	{
		$choices[$key] = $value;
	}
}

if(param('target_order_id') > 0)
{
	$sql_delivery_addresses = "select DISTINCT concat(order_address_company, ', ', order_address_address , ', ', order_address_zip, ' ', order_address_place) as company " .
	" from order_addresses " . 
	" where order_address_order = " . param('target_order_id') . 
	" and order_address_type = 2 " . 
	" order by company";


	$res_d = mysql_query($sql_delivery_addresses);
	while($row_d = mysql_fetch_assoc($res_d))
	{
		$choices[$row_d["company"]] = $row_d["company"];
	}
	
}

//asort($choices);

$choices[3] = "Other";

$page = new Page("catalog");
$page->register_action('basket', 'Shopping List');


// set default checkout-status


$checkout_status = param('status');


// form


$form = new Form("orders", "order");
$form->add_section("Client");


// helper-form: display buttons at bottom of page


$form3 = new Form();


// get user's basket


$res = mysql_query("select basket_id from baskets where basket_user=" . user_id());
if (mysql_num_rows($res) >= 1)
{
    $row = mysql_fetch_assoc($res);
    $basket = $row['basket_id'];
} else {
    mysql_query("insert into baskets (basket_user_id) values (" . user_id() . ")");
    $basket = mysql_insert_id();
}


$user_properties = get_user(user_id());


if(param("address_id") > 0)
{
	$address_id = param("address_id");
}
else
{
	$address_id = $user_properties['address'];
}

if ($checkout_status != "valid")
{
	get_checkout_form($form, $address_id, $choices, param("delivery_is_address"));


    $form->populate();
}
else
{
	list($list,$list2) = get_order_confirmation($form, $address_id, $basket);
}


if  ($form->button(LIST_BUTTON_BACK))
{
    dump_order_data();
}


$form->process();


// Checkout-Actions


if ($form->button("basket"))
{
    redirect("basket.php");
}


if ($form->button("cancel_order"))
{
    redirect("orders.php");
}
else if ($form->button("address_id"))
{
    $form->value("delivery_address_country", $address["country"]);

	
	$standard_delivery_address = array();
	$sql_delivery_addresses = "select * " . 
							  "from standard_delivery_addresses " . 
							  "where delivery_address_address_id = " . dbquote($address_id) . 
							  " order by delivery_address_company";

	$res = mysql_query($sql_delivery_addresses);

	if ($row = mysql_fetch_assoc($res))
	{
		$standard_delivery_address = $row;

		$sql = "select place_province from places where place_id = " . $standard_delivery_address["delivery_address_place_id"];
		$res = mysql_query($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$standard_delivery_address["delivery_address_province_id"] = $row["place_province"];

		}
	}

	if(count($standard_delivery_address) > 0) {
	   	   
	    $form->value("delivery_address_company", $standard_delivery_address["delivery_address_company"]);
		$form->value("delivery_address_company2", $standard_delivery_address["delivery_address_company2"]);
		$form->value("delivery_address_address", $standard_delivery_address["delivery_address_address"]);
		if(array_key_exists('delivery_to_street', $form->items))
		{
			$form->items['delivery_to_street']['values'][0] = $standard_delivery_address["delivery_address_street"];
			$form->items['delivery_to_street']['values'][1] = $standard_delivery_address["delivery_address_streetnumber"];
		}
		$form->value("delivery_address_address2", $standard_delivery_address["delivery_address_address2"]);
		$form->value("delivery_address_place", $standard_delivery_address["delivery_address_place"]);
		$form->value("delivery_address_place_id", $standard_delivery_address["delivery_address_place_id"]);
		$form->value("delivery_address_zip", $standard_delivery_address["delivery_address_zip"]);
		$form->value("delivery_address_province_id", $standard_delivery_address["delivery_address_province_id"]);
		$form->value("delivery_address_country", $standard_delivery_address["delivery_address_country"]);
		$form->value("delivery_address_phone", $standard_delivery_address["delivery_address_phone"]);
		if(array_key_exists('delivery_to_phone_number', $form->items))
		{
			$form->items['delivery_to_phone_number']['values'][0] = $standard_delivery_address["delivery_address_phone_country"];
			$form->items['delivery_to_phone_number']['values'][1] = $standard_delivery_address["delivery_address_phone_area"];
			$form->items['delivery_to_phone_number']['values'][2] = $standard_delivery_address["delivery_address_phone_number"];
		}

		$form->value("delivery_address_mobile_phone", $standard_delivery_address["delivery_address_mobile_phone"]);
		if(array_key_exists('delivery_to_mobile_phone_number', $form->items))
		{
			$form->items['delivery_to_mobile_phone_number']['values'][0] = $standard_delivery_address["delivery_address_mobile_phone_country"];
			$form->items['delivery_to_mobile_phone_number']['values'][1] = $standard_delivery_address["delivery_address_mobile_phone_area"];
			$form->items['delivery_to_mobile_phone_number']['values'][2] = $standard_delivery_address["delivery_address_mobile_phone_number"];
		}
		$form->value("delivery_address_email", $standard_delivery_address["delivery_address_email"]);
		$form->value("delivery_address_contact", $standard_delivery_address["delivery_address_contact"]);
	}
	else
	{
		$form->value("delivery_address_company", "");
		$form->value("delivery_address_company2", "");
		$form->value("delivery_address_address", "");
		if(array_key_exists('delivery_to_street', $form->items))
		{
			$form->items['delivery_to_street']['values'][0] = "";
			$form->items['delivery_to_street']['values'][1] = "";
		}
		$form->value("delivery_address_address2", "");
		$form->value("delivery_address_place", "");
		$form->value("delivery_address_place_id", 0);
		$form->value("delivery_address_zip", "");
		$form->value("delivery_address_province_id", 0);
		$form->value("delivery_address_country", "");
		$form->value("delivery_address_phone", "");
		if(array_key_exists('delivery_to_phone_number', $form->items))
		{
			$form->items['delivery_to_phone_number']['values'][0] = "";
			$form->items['delivery_to_phone_number']['values'][1] = "";
			$form->items['delivery_to_phone_number']['values'][2] = "";
		}

		$form->value("delivery_address_mobile_phone", "");
		if(array_key_exists('delivery_to_mobile_phone_number', $form->items))
		{
			$form->items['delivery_to_mobile_phone_number']['values'][0] = "";
			$form->items['delivery_to_mobile_phone_number']['values'][1] = "";
			$form->items['delivery_to_mobile_phone_number']['values'][2] = "";
		}
		$form->value("delivery_address_email", "");
		$form->value("delivery_address_contact", "");
	}
	
	populate_address(param("address_id"), "address");
}
else if ($form->button("delivery_address_place_id"))
{
    if ($form->value("delivery_address_place_id"))
    {
        $sql = "select place_name, place_province from places where place_id = " . dbquote($form->value("delivery_address_place_id"));
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			//$form->value("delivery_is_address", "");
			$form->value("delivery_address_place", $row["place_name"]);
			$form->value("delivery_address_zip", "");
			$form->value("delivery_address_address", "");
			$form->value("delivery_address_address2", "");
			$form->value("delivery_address_phone", "");
			$form->value("delivery_address_mobile_phone", "");
			$form->value("delivery_address_contact", "");
			$form->value("delivery_address_province_id", $row["place_province"]);
		}
		else
		{
			//$form->value("delivery_is_address", "");
			$form->value("delivery_address_place", "");
			$form->value("delivery_address_zip", "");
			$form->value("delivery_address_address", "");
			$form->value("delivery_address_address2", "");
			$form->value("delivery_address_phone", "");
			$form->value("delivery_address_mobile_phone", "");
			$form->value("delivery_address_contact", "");
			//$form->value("delivery_address_province_id", "");
		}
    }
}
else if ($form->button("delivery_address_country"))
{
	//$form->value("delivery_is_address", "");
	$form->value("delivery_address_place", "");
	$form->value("delivery_address_place_id", 0);
	$form->value("delivery_address_zip", "");
	$form->value("delivery_address_address", "");
	$form->value("delivery_address_address2", "");
	$form->value("delivery_address_phone", "");
	$form->value("delivery_address_mobile_phone", "");
	$form->value("delivery_address_contact", "");
}
else if ($form->button("delivery_address_province_id"))
{
	//$form->value("delivery_is_address", "");
	$form->value("delivery_address_place", "");
	$form->value("delivery_address_place_id", 0);
	$form->value("delivery_address_zip", "");
	$form->value("delivery_address_address", "");
	$form->value("delivery_address_address2", "");
	$form->value("delivery_address_phone", "");
	$form->value("delivery_address_mobile_phone", "");
	$form->value("delivery_address_contact", "");
}
else if ($form->button("posaddress_id"))
{

	if ($form->value("posaddress_id"))
    {
	   populate_address($form->value("posaddress_id"), "pos_address");

	   if ($form->value("delivery_is_address") == 1)
	   {
		   populate_deliveryaddress($form->value("delivery_is_address"), $address, $form->value("posaddress_id"));
	   }
    }
	elseif ($form->value("delivery_is_address") == 1)
	{
		populate_address($form->value("posaddress_id"), "pos_address");
		$form->value("delivery_address_company", "");
		$form->value("delivery_address_company2", "");
		$form->value("delivery_address_address", "");
		$form->value("delivery_address_address2", "");
		//$form->value("delivery_address_place", "");
		//$form->value("delivery_address_place_id", 0);
		$form->value("delivery_address_zip", "");
		//$form->value("delivery_address_province_id", 0);
		$form->value("delivery_address_country", "");
		$form->value("delivery_address_phone", "");
		$form->value("delivery_address_mobile_phone", "");
		$form->value("delivery_address_email", "");
		$form->value("delivery_address_contact", "");
	}
	elseif(!$form->value("posaddress_id"))
	{
		populate_address($form->value("posaddress_id"), "pos_address");
	}
}
else if ($form->button("delivery_is_address"))
{
	if($form->value("posaddress_id") > 0 and param("target_order_id") > 0)
	{
		$form->error("Please select EITHER a POS or a project but not both!");
	}
	elseif ($form->value("delivery_is_address"))
    {
       populate_deliveryaddress($form->value("delivery_is_address"), $address, $form->value("posaddress_id"), param("target_order_id"));
    }
	else
	{
		$form->value("delivery_address_company", "");
		$form->value("delivery_address_company2", "");
		$form->value("delivery_address_address", "");
		if(array_key_exists('delivery_to_street', $form->items))
		{
			$form->items['delivery_to_street']['values'][0] = "";
			$form->items['delivery_to_street']['values'][1] = "";
		}
		$form->value("delivery_address_address2", "");
		$form->value("delivery_address_place", "");
		$form->value("delivery_address_place_id", 0);
		$form->value("delivery_address_zip", "");
		$form->value("delivery_address_province_id", 0);
		$form->value("delivery_address_country", "");
		$form->value("delivery_address_phone", "");
		if(array_key_exists('delivery_to_phone_number', $form->items))
		{
			$form->items['delivery_to_phone_number']['values'][0] = "";
			$form->items['delivery_to_phone_number']['values'][1] = "";
			$form->items['delivery_to_phone_number']['values'][2] = "";
		}

		$form->value("delivery_address_mobile_phone", "");
		if(array_key_exists('delivery_to_mobile_phone_number', $form->items))
		{
			$form->items['delivery_to_mobile_phone_number']['values'][0] = "";
			$form->items['delivery_to_mobile_phone_number']['values'][1] = "";
			$form->items['delivery_to_mobile_phone_number']['values'][2] = "";
		}
		$form->value("delivery_address_email", "");
		$form->value("delivery_address_contact", "");
	}
}
else if ($form->button("order"))
{
    // store address-info 

    $form->value("billing_address_phone", $form->value("billing_address_phone_country") . " " .$form->value("billing_address_phone_area") . " " . $form->value("billing_address_phone_number"));
	
	if($form->value("billing_address_mobile_phone_number"))
	{
		$form->value("billing_address_mobile_phone", $form->value("billing_address_mobile_phone_country") . " " .$form->value("billing_address_mobile_phone_area") . " " . $form->value("billing_address_mobile_phone_number"));
	}
	else
	{
		$form->value("billing_address_mobile_phone", "");
	}

	
	$form->value("billing_address_address",$form->value("billing_address_street") . " " .$form->value("billing_address_streetnumber"));

	
	$form->value("shop_address_phone", $form->value("shop_address_phone_country") . " " .$form->value("shop_address_phone_area") . " " . $form->value("shop_address_phone_number"));
	
	
	if($form->value("shop_address_mobile_phone_number"))
	{
		$form->value("shop_address_mobile_phone", $form->value("shop_address_mobile_phone_country") . " " .$form->value("shop_address_mobile_phone_area") . " " . $form->value("shop_address_mobile_phone_number"));
	}
	else
	{
		$form->value("shop_address_mobile_phone", "");
	}
	

	
	$form->value("shop_address_address",$form->value("shop_address_street") . " " .$form->value("shop_address_streetnumber"));


		
	$form->value("delivery_address_phone", $form->unify_multi_edit_field($form->items["delivery_to_phone_number"]));
	$form->value("delivery_address_mobile_phone", $form->unify_multi_edit_field($form->items["delivery_to_mobile_phone_number"]));


	$form->add_validation("{delivery_address_phone} != '' or {delivery_address_mobile_phone} != ''", "Please indcate either the phone number or the mobile phone number!");
	
	$form->value("delivery_address_address", $form->unify_multi_edit_field($form->items["delivery_to_street"], get_country_street_number_rule($form->value("delivery_address_country"))));

	
	
	dump_order_data();

    $form->add_validation("from_system_date({preferred_delivery_date}) >  " . dbquote(date("Y-m-d")), "The preferred arrival date must be a future date!");

    $form->add_validation("{delivery_address_company} and " .
                              "{delivery_address_address} and ".
							  "{delivery_address_place_id} and " .
		                      "{delivery_address_province_id} and " .
                              "{delivery_address_place} and " .
                              "{delivery_address_country} and " .
                              "{delivery_address_contact}", "The ship to information is not complete.");



	$error = 0;
	if(strlen(preg_replace('/[^0-9a-z_ %\[\]\.\(\)%&-]/s', '', $form->value("delivery_address_address"))) < strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("delivery_address_address")))
		and strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("delivery_address_address"))) > 10)
	{
		$error = 1;
		$form->error("Company names and addresses must contain lower caracters. Do not use capital letters only.");
	}
	elseif(strlen(preg_replace('/[^0-9a-z_ %\[\]\.\(\)%&-]/s', '', $form->value("delivery_address_address2"))) < strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("delivery_address_address2")))
		and strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("delivery_address_address2"))) > 10)
	{
		$error = 1;
		$form->error("Company names and addresses must contain lower caracters. Do not use capital letters only.");
	}


    if ($error == 0 and $form->validate())
    {
        unset($form);
        $form = new Form("orders", "order");
        $checkout_status = "valid";


        list($list,$list2) = get_order_confirmation($form, $address_id, $basket);
        
        $form3->add_button("submit_order", "Submit Order");
        $form3->add_button("checkout_edit", "Back");
    }
}




$form3->process();


if ($form3->button("submit_order"))
{
	save_order($form, $senmail_activated);
    redirect("order_new_submitted.php");
}
else if ($form3->button("checkout_edit"))
{
    unset($form);
    $checkout_status = "edit";


    $form = new Form("orders", "order");
    get_checkout_form($form, $address_id, $choices);
    $form->populate();
}



$form->add_hidden("status", $checkout_status);


if ($checkout_status == "valid")
{
    // Cost overview


    $currency = get_user_currency(user_id());


    $form2 = new Form();
    $form2->add_section("Cost Overview");
    $form2->add_label("grand_total1", "Standard Items/Add-On's " . $currency["currency_symbol"], 0,
            number_format(get_basket_part_total($basket, false) + get_basket_part_total($basket, true),2, ".", "'"));
}


$page->register_action('home', 'Home', "welcome.php");


// render page


$page->header();
if ($checkout_status == "valid")
{
    $page->title('Order Preview');
    $form->render();
    $form2->render();
    
	
	if(has_access("can_edit_catalog") or has_access("can_enter_orders_for_other_companies"))
	{
		
		
		/*****************************************************************************/
		// Special Items
		$sql = "select basket_special_item_id, basket_special_item_description, " .
		   " basket_special_item_quantity, concat(basket_special_item__supplier_price, ' ', currency_symbol) as price, " .
		   " concat(basket_special_item_quantity*basket_special_item__supplier_price, ' ', currency_symbol) as total_price, " . 
		   " address_company " . 
		   " from basket_special_items " . 
		   " left join currencies on currency_id = basket_special_item_supplier_currency_id " . 
		   " left join addresses on address_id = basket_special_item_supplier";

		$list3 = new ListView($sql, LIST_HAS_FOOTER | LIST_HAS_HEADER);
		$list3->set_title("Special Items");
		
		$list3->set_filter("basket_special_item_basket=" . $basket);
		
		$list3->add_column("basket_special_item_description", "Item", "", LIST_FILTER_FREE);
		$list3->add_column("basket_special_item_quantity", "Quantity", "", LIST_FILTER_FREE);
		$list3->add_column("price", "Price", "", LIST_FILTER_FREE);
		$list3->add_column("total_price", "Sub Total", "", LIST_FILTER_FREE);
		$list3->add_column("address_company", "Supplier", "", LIST_FILTER_FREE);

		$list3->render();
		echo '<p>&nbsp;</p>';
	}
	
	
	
	
	$list->render();
    $list2->render();
    $form3->render();

	
}
else 
{
    $page->title('Checkout');
    $form->render();
}

?>

<div id="city" style="display:none;">
    Please indicate the new city's name in English!
</div>


<script languege="javascript">
	
	$(document).ready(function(){


		$('#preferred_transportation_arranged').change(function() {
			
			if($(this).val() == 7) {
				$('#insurance_info').html('Insurance is covered by Tissot.');
			}
			else {
				$('#insurance_info').html('Insurance has to be covered by the client.');
			}
			
		});

	});
</script>

<?php
$page->footer();




?>