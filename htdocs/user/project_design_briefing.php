<?php
/********************************************************************

    project_design_briefing.php

    Edit/View project's design briefing

    Created by:     Anton Hofmann (anton.hofmann@codingroom.ch)
    Date created:   2017-12-14
    Modified by:    Anton Hofmann (anton.hofmann@codingroom.ch)
    Date modified:  2017-12-14
    Version:        1.0.0

    Copyright (c) 2017, Tissot, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";
require_once "include/order_state_constants.php";


if(!has_access("can_view_design_briefing") and !has_access("can_edit_design_briefing") ) {
	redirect("noaccess.php");
}
if(!param("pid")) {
	redirect("noaccess.php");
}

register_param("pid");
set_referer("project_perform_action.php");

/********************************************************************
    prepare all data needed
*********************************************************************/

// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);


if($project["pipeline"] == 0)
{
	$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");

	$sql = "select posarea_area, posareatype_name " . 
		   "from posareas " . 
		   " left join posareatypes on posareatype_id = posarea_area " . 
		   "where posarea_posaddress = " . dbquote($pos_data["posaddress_id"]) . 
		   " order by posarea_area";
}
elseif($project["pipeline"] == 1)
{
	$pos_data = get_poslocation_from_pipeline($project["posaddress_id"], $project["order_id"]);

	$sql = "select posarea_area, posareatype_name  
		   from posareaspipeline
		   left join posareatypes on posareatype_id = posarea_area 
		   where posarea_posaddress = " . dbquote($pos_data["posaddress_id"]) . 
		   " order by posarea_area";
}

//get posareas
$posareas = '';
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$pos_posareas[]	= $row["posareatype_name"];
}
if(count($pos_posareas) > 0 ){
	$posareas = implode(', ', $pos_posareas);
}

//get floor
$posareas = $posareas . ", " . $project["project_floor"];


/********************************************************************
	build design briefing records if not available
*********************************************************************/
$design_brief_id = '';
$design_briefing_locked = false;
$submitted = '';
$rejected = '';
$resubmitted = '';

$date_submitted = '';
$date_rejected = '';
$date_resubmitted = '';


$sql = "select *
	   from project_design_briefs 
	   where project_design_brief_project_id = " . dbquote(param("pid"));

$res = mysql_query($sql);
if($row = mysql_fetch_assoc($res))
{
	$design_brief_id = $row["project_design_brief_id"];
	if($row["project_design_brief_submission_date"] != NULL 
		      and  substr($row["project_design_brief_submission_date"], 0, 4) != '0000'
		      and ($row["project_design_brief_rejection_date"] == NULL 
				or  substr($row["project_design_brief_rejection_date"], 0, 4) == '0000')) {
		//submitted but not rejected
		$design_briefing_locked = true;
	}

	if($row["project_design_brief_rejection_date"] != NULL 
		      and  substr($row["project_design_brief_rejection_date"], 0, 4) != '0000'
		      and $row["project_design_brief_resubmission_date"] >= $row["project_design_brief_rejection_date"]) {
		//rejected and later resubmitted
		$design_briefing_locked = true;
	}

	$tmp = get_user($row["project_design_brief_submitted_by"]);
	$submitted = to_system_date($row["project_design_brief_submission_date"]) . " " . $tmp['name'] . " ".  $tmp['firstname'];
	$tmp = get_user($row["project_design_brief_rejected_by"]);
	$rejected = to_system_date($row["project_design_brief_rejection_date"]) . " " . $tmp['name'] . " ".  $tmp['firstname'];
	$tmp = get_user($row["project_design_brief_resubmitted_by"]);
	$resubmitted = to_system_date($row["project_design_brief_resubmission_date"]) . " " . $tmp['name'] . " ".  $tmp['firstname'];


	$date_submitted = $row["project_design_brief_submission_date"];
	$date_rejected = $row["project_design_brief_rejection_date"];
	$date_resubmitted = $row["project_design_brief_resubmission_date"];
	
}



if($design_briefing_locked == false) {
	
	$design_brief_id = '';
	$sql = "select project_design_brief_id
	       from project_design_briefs 
		   where project_design_brief_project_id = " . dbquote(param("pid"));

	$res = mysql_query($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$design_brief_id = $row["project_design_brief_id"];
	}
	else {

		//insert record
		$sql_i = "INSERT INTO project_design_briefs (
					project_design_brief_project_id,user_created, date_created ) VALUES (" . 
					dbquote(param("pid")) . ", " .
					dbquote(user_login()) . ", " . 
					dbquote(date("Y-m-d h:i:s")) . ")";

		$result = mysql_query($sql_i);
		
		$design_brief_id = mysql_insert_id();
	}

	//positions
	
	$pos_type_filter = '';
	if($project['project_postype'] == 1) {
		$pos_type_filter = ' and design_brief_position_store = 1 ';
	}
	elseif($project['project_postype'] == 3) {
		$pos_type_filter = ' and design_brief_position_kiosk = 1 ';
	}
	else {
		$pos_type_filter = ' and design_brief_position_sis = 1 ';
	}

	$sql = "select *
		   from design_brief_positions 
		   left join design_brief_groups on design_brief_group_id = design_brief_position_group_id
		   where design_brief_group_active = 1 and design_brief_position_active = 1
		   $pos_type_filter
		   order by design_brief_group_sort, design_brief_position_sort";

	$res = mysql_query($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$sql_p = "select project_design_brief_position_id
				 from project_design_brief_positions
				 where project_design_brief_position_design_brief_id = " . dbquote($design_brief_id) .
				 " and project_design_brief_position_group_id = " . dbquote($row['design_brief_group_id']) .
				 " and project_design_brief_position_position_id= " . dbquote($row['design_brief_position_id']);


		$res_p = mysql_query($sql_p);
		if(!$row_p = mysql_fetch_assoc($res_p))
		{
			//insert record
			$sql_i = "INSERT INTO project_design_brief_positions (
						project_design_brief_position_design_brief_id, project_design_brief_position_group_id, 
						project_design_brief_position_position_id, project_design_brief_position_type, 
						project_design_brief_position_values, 
						user_created, date_created ) VALUES (" . 
						dbquote($design_brief_id) . ", " .
						dbquote($row['design_brief_group_id']) . ", " . 
						dbquote($row['design_brief_position_id']) . ", " . 
						dbquote($row['design_brief_position_type']) . ", " .
						dbquote($row['design_brief_position_values']) . ", " .
						dbquote(user_login()) . ", " . 
						dbquote(date("Y-m-d h:i:s")) . ")";

			$result = mysql_query($sql_i);

		}
	}
}


/********************************************************************
    prepare data
*********************************************************************/


//design briefing documents
$files = array();
$files2 = array();
$files3 = array();
$files4 = array();
$sql = "select distinct order_file_id, order_file_visited, 
		  order_file_title, order_file_description, 
		  order_file_path, file_type_name, 
		  order_files.date_created, 
		  order_file_category_name, order_file_category_priority, 
		  concat(user_name, ' ', user_firstname) as owner_fullname,
		  order_file_category
	  from order_files  
	  left join order_file_categories on order_file_category_id = order_file_category 
	  left join users on user_id = order_file_owner 
	  left join file_types on order_file_type = file_type_id 
	  where order_file_category in (6, 28, 29, 30) and order_file_order = " . dbquote($project["project_order"]);


$res = mysql_query($sql);
while($row = mysql_fetch_assoc($res))
{
	if($row['order_file_category'] == 6) {
		$files[] = array('title'=>$row["order_file_title"], 'path'=>$row['order_file_path'], 'date'=>$row['date_created']);
	}
	elseif($row['order_file_category'] == 28) {
		$files2[] = array('title'=>$row["order_file_title"], 'path'=>$row['order_file_path'], 'date'=>$row['date_created']);
	}
	elseif($row['order_file_category'] == 29) {
		$files3[] = array('title'=>$row["order_file_title"], 'path'=>$row['order_file_path'], 'date'=>$row['date_created']);
	}
	elseif($row['order_file_category'] == 30) {
		$files4[] = array('title'=>$row["order_file_title"], 'path'=>$row['order_file_path'], 'date'=>$row['date_created']);
	}
}

$order_number = $project['order_number'];

$filter_locked = '';
if($design_briefing_locked == true) {
	$filter_locked = "project_design_brief_position_content <> ''
	   and project_design_brief_position_content is not null
	   and ";
}
	
$sql = "select * from project_design_brief_positions
       left join design_brief_groups on design_brief_group_id = project_design_brief_position_group_id
	   left join design_brief_positions on design_brief_position_id = project_design_brief_position_position_id 
	   where " . $filter_locked . " project_design_brief_position_design_brief_id = " . dbquote($design_brief_id) .
	   " order by design_brief_group_sort, design_brief_position_sort";


/********************************************************************
    build form
*********************************************************************/
$form = new Form("project_design_briefs", "project design briefings");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid", $project["project_order"]);
$form->add_hidden("dbid", $design_brief_id);

include('include/project_head_small.php');

if($posareas) {
	$form->add_label("environment", "Environment",0, $posareas);
}

$form->add_section("Submission");
$form->add_label('submitted', 'Submitted', 0, $submitted);
$form->add_label('rejected', 'Rejected', 0, $rejected);
$form->add_label('resubmitted', 'Resubmitted', 0, $resubmitted);

$link = "project_design_briefing_pdf.php?pid=" . param("pid"); 
$link = "javascript:popup('". $link . "', 800, 600)";
$form->add_button("pdf1", "Print Design Briefing", $link);

$link = "project_design_briefing_pdf.php?pid=" . param("pid") . "&files=1"; 
$link = "javascript:popup('". $link . "', 800, 600)";
$form->add_button("pdf2", "Print Design Briefing with File Attachments<br /><br />", $link);

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();

/********************************************************************
    Populate forms and process button clicks
*********************************************************************/ 
$formnames = array();
$field_names = array();
$field_types = array();
$field_sets = array();
$sql = "select distinct project_design_brief_position_group_id, design_brief_group_name
	   from project_design_brief_positions
	   left join design_brief_groups on design_brief_group_id = project_design_brief_position_group_id
	   left join design_brief_positions on design_brief_position_id = project_design_brief_position_position_id 
	   where " . $filter_locked . " project_design_brief_position_design_brief_id = " . dbquote($design_brief_id) .
	   " order by design_brief_group_sort, design_brief_position_sort";

$group = '';
$res = mysql_query($sql);
while($row = mysql_fetch_assoc($res))
{
	$formname = "form" . $row["project_design_brief_position_group_id"];
	
	$group_ids[$row["project_design_brief_position_group_id"]] = $row["project_design_brief_position_group_id"];
	$group_titles[$row["project_design_brief_position_group_id"]] = $row["design_brief_group_name"];

	$formnames[$row["project_design_brief_position_group_id"]] = $formname;

	$$formname = new Form("project_design_briefs", "project design briefings");
	


	//compose form
	$sql2 = "select * from project_design_brief_positions
		   left join design_brief_groups on design_brief_group_id = project_design_brief_position_group_id
		   left join design_brief_positions on design_brief_position_id = project_design_brief_position_position_id 
		   where " . $filter_locked . " project_design_brief_position_design_brief_id = " . dbquote($design_brief_id) .
		   " and project_design_brief_position_group_id = " . dbquote($row["project_design_brief_position_group_id"]) .
		   " order by design_brief_position_sort";

	$res2 = mysql_query($sql2);
	while($row2 = mysql_fetch_assoc($res2))
	{
		if($row2["project_design_brief_position_type"] == 7) {
			$selection = explode("\r\n", $row2['project_design_brief_position_values']);
			foreach($selection as $key=>$value) {
				$field_names[] = "P" . $row2["project_design_brief_position_id"] . "-" . $key;
			}
		}
		else {
			$field_names[] = "P" . $row2["project_design_brief_position_id"];
		}
		$field_types[] = $row2["project_design_brief_position_type"];

		$not_null = 0;
		$asterix = '';
		if($row2['design_brief_position_mandatory'] == 1) {
			$not_null = NOTNULL;
			$asterix = '*';
		}

		$text2 = '';
		if($row2["design_brief_position_infotext"]) {
			$text2 = '<br /><span class="text">'. nl2br($row2["design_brief_position_infotext"]) . '</span>';
		}

		switch ($row2['project_design_brief_position_type']) {
			case 1:
				
				if(has_access("can_edit_design_briefing") and $design_briefing_locked == false) {
					$$formname->add_edit('P' . $row2["project_design_brief_position_id"], $row2["design_brief_position_text"] . $asterix . $text2, $not_null, $row2["project_design_brief_position_content"], TYPE_CHAR, 255, 0, 0, "", "", true);
				}
				else {
					$$formname->add_label('RO' . $row2["project_design_brief_position_id"], $row2["design_brief_position_text"] . $asterix . $text2, 0, $row2["project_design_brief_position_content"]);
				}
				break;
			case 2:
				if(has_access("can_edit_design_briefing") and $design_briefing_locked == false) {
					$$formname->add_edit('P' . $row2["project_design_brief_position_id"], $row2["design_brief_position_text"] . $asterix . $text2, $not_null, $row2["project_design_brief_position_content"], TYPE_INT, 10, 0, 0, "", "", true);
				}
				else {
					$$formname->add_label('RO' . $row2["project_design_brief_position_id"], $row2["design_brief_position_text"] . $asterix . $text2, 0, $row2["project_design_brief_position_content"]);
				}
				break;
			case 3:
				if(has_access("can_edit_design_briefing") and $design_briefing_locked == false) {
					$$formname->add_edit('P' . $row2["project_design_brief_position_id"], $row2["design_brief_position_text"] . $asterix . $text2, $not_null, $row2["project_design_brief_position_content"], TYPE_DECIMAL, 10, 0, 0, "", "", true);
				}
				else {
					$$formname->add_label('RO' . $row2["project_design_brief_position_id"], $row2["design_brief_position_text"] . $asterix . $text2, 0, $row2["project_design_brief_position_content"]);
				}
				break;
			case 4:
				if(has_access("can_edit_design_briefing") and $design_briefing_locked == false) {
					$$formname->add_edit('P' . $row2["project_design_brief_position_id"], $row2["design_brief_position_text"] . $asterix . $text2, $not_null, $row2["project_design_brief_position_content"], TYPE_DATE, 10, 0, 0, "", "", true);
				}
				else {
					$$formname->add_label('RO' . $row2["project_design_brief_position_id"], $row2["design_brief_position_text"] . $asterix . $text2, 0, $row2["project_design_brief_position_content"]);
				}
				break;
			case 5:
				if(has_access("can_edit_design_briefing") and $design_briefing_locked == false) {
					$$formname->add_multiline('P' . $row2["project_design_brief_position_id"], $row2["design_brief_position_text"] . $asterix . $text2, 8, $not_null, $row2["project_design_brief_position_content"], 0, "", 76, true);
				}
				else {
					$$formname->add_label('RO' . $row2["project_design_brief_position_id"], $row2["design_brief_position_text"] . $asterix . $text2, 0, $row2["project_design_brief_position_content"]);
				}
				break;
			case 6:
				$selection = explode("\r\n", $row2['project_design_brief_position_values']);

				if(has_access("can_edit_design_briefing") and $design_briefing_locked == false) {
					$$formname->add_list('P' . $row2["project_design_brief_position_id"], $row2["design_brief_position_text"] . $asterix . $text2, $selection, $not_null, $row2["project_design_brief_position_content"],0, "", true);
				}
				else {
					
					$selection = explode("\r\n", $row2['project_design_brief_position_values']);
					if(array_key_exists($row2["project_design_brief_position_content"], $selection)) {
						$$formname->add_label('RO' . $row2["project_design_brief_position_id"], $row2["design_brief_position_text"] . $asterix . $text2, 0, $selection[$row2["project_design_brief_position_content"]]);
					}
					else {
						$$formname->add_label('RO' . $row2["project_design_brief_position_id"], $row2["design_brief_position_text"] . $asterix . $text2, 0, "");
					}
				}
				break;
			case 7:
				$selection = explode("\r\n", $row2['project_design_brief_position_values']);
				
				$field_sets['P' . $row2["project_design_brief_position_id"]] = $selection;
				$content = json_decode($row2['project_design_brief_position_content'], true);
				$$formname->add_label('L' . $row2["project_design_brief_position_id"], $row2["design_brief_position_text"] . $asterix . $text2);
				foreach($selection as $key=>$value) {
					$selected = '';
					if($content[$key] == 1) {
						$selected = 'checked';
					}
					
					if(has_access("can_edit_design_briefing") and $design_briefing_locked == false) {
						$$formname->add_checkbox('P' . $row2["project_design_brief_position_id"] . '-' . $key, "", $selected, $not_null, $value, true);
					}
					else {
						$$formname->add_checkbox('RO' . $row2["project_design_brief_position_id"] . '-' . $key, "", $selected, $not_null, $value, true);
					}
				
				}
				break;
		}
		
	}


	//files for project information
	if ($row["project_design_brief_position_group_id"] == 13
		and has_access("can_add_attachments_in_projects"))
	{
		
		$$formname->add_comment('<br /><strong>PLEASE PROVIDE THE FOLLOWING DOCUMENTS TOGETHER WITH THIS COMPLETED BRIEFING</strong>');

		
		//section files 1
		$$formname->add_section('CAD DRAWING PACKAGE');
		$$formname->add_comment('DWG and PDF format, including layout, ceiling, elevation of each wall, shopfront elevation and shopfront section.');
		
		
		if(count($files) > 0) {
			$html_filelist = '<table>';
			foreach($files as $key=>$file) {
				$html_filelist .= '<tr>';
				$html_filelist .= '<td><a href="'.$file['path'].'"><img src="/pictures/document_view.gif" /></a></td>';
				$html_filelist .= '<td><a href="'.$file['path'].'">' .$file['title'] . '</a></td>';
				$html_filelist .= '<td>&nbsp;&nbsp;</td>';
				$html_filelist .= '<td>' . to_system_date($file['date']) . '</td>';
				$html_filelist .= '</tr>';
			}
			$html_filelist .= '</table>';
			$$formname->add_comment($html_filelist);
		
		}
		
		if(has_access("can_edit_design_briefing") and $design_briefing_locked == false) {
			$urlQuery = http_build_query(array(
				'action' => '/user/include/ajx_upload_attachments.php',
				'title' => 'Upload Files',
				'subtitle' => 'Design Briefing Documents: CAD DRAWING PACKAGE',
				'infotext' => 'Allowed file types are: pdf, jpg, jpeg, dwg',
				'fields' => array(
					'id' => param("pid"),
					'category' => 6,
					'extensions' => 'dwg,pdf,jpg,jpeg',
					'startload' => true
				)
			));
			
			$iframeAttributes = array(
				"data-fancybox-type" => "iframe",
				"data-fancybox-width" => "800",
				"data-fancybox-height" => "620",
				"href" => "/public/themes/default/multifile.uploader.php?$urlQuery"
			);
			$btnAttributes = '';
			foreach ($iframeAttributes as $k=>$v) {
				$btnAttributes .= "$k=\"$v\" ";
			}
			
			$$formname->add_comment('<a class="fancy-frame" ' . $btnAttributes . ' ><span class="upload_button">Upload CAD Drawings</span></a>');

		}

		//section files 2
		$$formname->add_section('PICTURES');
		$$formname->add_comment('Maximum amount of photographs of daytime and night-time situations including interior (all walls), shop front and environment around the store (left, right and front)');


		if(count($files2) > 0) {
			$html_filelist = '<table>';
			foreach($files2 as $key=>$file) {
				$html_filelist .= '<tr>';
				$html_filelist .= '<td><a href="'.$file['path'].'"><img src="/pictures/document_view.gif" /></a></td>';
				$html_filelist .= '<td><a href="'.$file['path'].'">' .$file['title'] . '</a></td>';
				$html_filelist .= '<td>&nbsp;&nbsp;</td>';
				$html_filelist .= '<td>' . to_system_date($file['date']) . '</td>';
				$html_filelist .= '</tr>';
			}
			$html_filelist .= '</table>';
			$$formname->add_comment($html_filelist);
		
		}


		if(has_access("can_edit_design_briefing") and $design_briefing_locked == false) {
			$urlQuery = http_build_query(array(
				'action' => '/user/include/ajx_upload_attachments.php',
				'title' => 'Upload Files',
				'subtitle' => 'Design Briefing Documents: ',
				'infotext' => 'Allowed file types are: pdf, jpg, jpeg, dwg',
				'fields' => array(
					'id' => param("pid"),
					'category' => 28,
					'extensions' => 'dwg,pdf,jpg,jpeg',
					'startload' => true
				)
			));
			
			$iframeAttributes = array(
				"data-fancybox-type" => "iframe",
				"data-fancybox-width" => "800",
				"data-fancybox-height" => "620",
				"href" => "/public/themes/default/multifile.uploader.php?$urlQuery"
			);
			$btnAttributes = '';
			foreach ($iframeAttributes as $k=>$v) {
				$btnAttributes .= "$k=\"$v\" ";
			}
			
			$$formname->add_comment('<a class="fancy-frame" ' . $btnAttributes . ' ><span class="upload_button">Upload Pictures</span></a>');

		}

		//section files 3
		$$formname->add_section('TRAFFIC FLOW');
		$$formname->add_comment('Maximum amount of photographs of daytime and night-time situations including interior (all walls), shop front and environment around the store (left, right and front)');


		if(count($files3) > 0) {
			$html_filelist = '<table>';
			foreach($files3 as $key=>$file) {
				$html_filelist .= '<tr>';
				$html_filelist .= '<td><a href="'.$file['path'].'"><img src="/pictures/document_view.gif" /></a></td>';
				$html_filelist .= '<td><a href="'.$file['path'].'">' .$file['title'] . '</a></td>';
				$html_filelist .= '<td>&nbsp;&nbsp;</td>';
				$html_filelist .= '<td>' . to_system_date($file['date']) . '</td>';
				$html_filelist .= '</tr>';
			}
			$html_filelist .= '</table>';
			$$formname->add_comment($html_filelist);
		
		}

		if(has_access("can_edit_design_briefing") and $design_briefing_locked == false) {
			$urlQuery = http_build_query(array(
				'action' => '/user/include/ajx_upload_attachments.php',
				'title' => 'Upload Files',
				'subtitle' => 'Design Briefing Documents: ',
				'infotext' => 'Allowed file types are: pdf, jpg, jpeg, dwg',
				'fields' => array(
					'id' => param("pid"),
					'category' => 29,
					'extensions' => 'dwg,pdf,jpg,jpeg',
					'startload' => true
				)
			));
			
			$iframeAttributes = array(
				"data-fancybox-type" => "iframe",
				"data-fancybox-width" => "800",
				"data-fancybox-height" => "620",
				"href" => "/public/themes/default/multifile.uploader.php?$urlQuery"
			);
			$btnAttributes = '';
			foreach ($iframeAttributes as $k=>$v) {
				$btnAttributes .= "$k=\"$v\" ";
			}
			
			$$formname->add_comment('<a class="fancy-frame" ' . $btnAttributes . ' ><span class="upload_button">Upload Traffic Pictures</span></a>');

		}

		//section files 4
		$$formname->add_section('TENANT FIT-OUT MANUAL OR CRITERIA PACKAGE');
		$$formname->add_comment('Requirements and guidelines provided by the mall, landlord or management.');


		if(count($files4) > 0) {
			$html_filelist = '<table>';
			foreach($files4 as $key=>$file) {
				$html_filelist .= '<tr>';
				$html_filelist .= '<td><a href="'.$file['path'].'"><img src="/pictures/document_view.gif" /></a></td>';
				$html_filelist .= '<td><a href="'.$file['path'].'">' .$file['title'] . '</a></td>';
				$html_filelist .= '<td>&nbsp;&nbsp;</td>';
				$html_filelist .= '<td>' . to_system_date($file['date']) . '</td>';
				$html_filelist .= '</tr>';
			}
			$html_filelist .= '</table>';
			$$formname->add_comment($html_filelist);
		
		}

		if(has_access("can_edit_design_briefing") and $design_briefing_locked == false) {
			$urlQuery = http_build_query(array(
				'action' => '/user/include/ajx_upload_attachments.php',
				'title' => 'Upload Files',
				'subtitle' => 'Design Briefing Documents: ',
				'infotext' => 'Allowed file types are: pdf, jpg, jpeg, dwg',
				'fields' => array(
					'id' => param("pid"),
					'category' => 30,
					'extensions' => 'dwg,pdf,jpg,jpeg',
					'startload' => true
				)
			));
			
			$iframeAttributes = array(
				"data-fancybox-type" => "iframe",
				"data-fancybox-width" => "800",
				"data-fancybox-height" => "620",
				"href" => "/public/themes/default/multifile.uploader.php?$urlQuery"
			);
			$btnAttributes = '';
			foreach ($iframeAttributes as $k=>$v) {
				$btnAttributes .= "$k=\"$v\" ";
			}
			
			$$formname->add_comment('<a class="fancy-frame" ' . $btnAttributes . ' ><span class="upload_button">Upload Fit-out Manuals/Criteria Packages</span></a>');

		}
	}
	else {
		if(has_access("can_edit_design_briefing") and $design_briefing_locked == false) {
			//$$formname->add_comment('<a class="dummysave" href="javascript:void(0);">Save</a><div class="dummysaved highlite_value_bold" style="display:none;">Your data was sucessfully saved!</div>');
		}
	}
}
if(has_access("can_edit_design_briefing") and $design_briefing_locked == false) {
	$$formname->add_button("save", "Save Data");

}


/*
$date_submitted = '';
$date_rejected = '';
$date_resubmitted = '';
*/

if($project['order_actual_order_state_code'] < ACCEPT_DESIGN_BRIEFING) {
	if((has_access("can_edit_catalog")
		or user_id() == $project['project_design_supervisor'])
		and $date_submitted != NULL 
		and $date_submitted != '0000-00-00'
		and ($date_rejected == NULL or $date_rejected == '0000-00-00')
		){
		$$formname->add_button("accept", "Accept Design Briefing");
		$$formname->add_button("reject", "Reject Design Briefing");
	}
	elseif((has_access("can_edit_catalog")
		or user_id() == $project['project_design_supervisor'])
		and $date_resubmitted != NULL 
		and $date_resubmitted != '0000-00-00'
		and ($date_rejected <= $date_resubmitted)
		){
		$$formname->add_button("accept", "Accept Design Briefing");
		$$formname->add_button("reject", "Reject Design Briefing");
	}
}

$form->populate();
$$formname->process();
if(has_access("can_edit_design_briefing") 
	and $design_briefing_locked == false
	and $$formname->button("save")) {

		$sql = "select count(project_design_brief_position_id) as num_recs  
				from project_design_brief_positions 
				left join project_design_briefs on project_design_brief_id = project_design_brief_position_design_brief_id
				left join design_brief_positions on design_brief_position_id = project_design_brief_position_position_id
				where project_design_brief_project_id = " . dbquote($project["project_id"]) . 
				" and design_brief_position_mandatory = 1
				  and (project_design_brief_position_content = '' or project_design_brief_position_content is null)";

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			if($row["num_recs"] > 0)
			{
				$form->error("Please fill in all mandatory fields of the design briefing!");
			}
			else {
				$form->message("Your data has been saved.");
			}
		}

}
elseif($$formname->button("accept")) {
	$link = "project_perform_action.php?id=" . ACCEPT_DESIGN_BRIEFING . "&pid=" . $project['project_id'];
	redirect($link);
}
elseif($$formname->button("reject")) {
	$link = "project_perform_action.php?id=". REJECT_DESIGN_BRIEFING . "&pid=" . $project['project_id'];
	redirect($link);
}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Design Briefing");

$form->render();




foreach($formnames as $key=>$formname) {

	$toggler = '<div class="toggler_pointer" id="f' . $key . '_on"><span class="fa fa-minus-square toggler"></span>' .$group_titles[$key] . '</div>';
	echo $toggler;

	$toggler = '<div class="toggler_pointer toggler_pointer_off" id="f' . $key . '_off"><span class="fa fa-plus-square toggler"></span>' .$group_titles[$key] . '</div>';
	echo $toggler;
	echo '<br />';
	
	$$formname->render($key);

}


require_once "include/project_footer_logistic_state.php";




?>
<script language="javascript">
jQuery(document).ready(function($) {

	
	var dateRegex = /^(?=\d)(?:(?:31(?!.(?:0?[2469]|11))|(?:30|29)(?!.0?2)|29(?=.0?2.(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00)))(?:\x20|$))|(?:2[0-8]|1\d|0?[1-9]))([-.\/])(?:1[012]|0?[1-9])\1(?:1[6-9]|[2-9]\d)?\d\d(?:(?=\x20\d)\x20|$))?(((0?[1-9]|1[012])(:[0-5]\d){0,2}(\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$/;


	<?php
	foreach($formnames as $key=>$formname)
	{
		if(array_key_exists("designbrief", $_SESSION) and array_key_exists($formname, $_SESSION["designbrief"]))
		{
			if($_SESSION["designbrief"][$formname] == 0)
			{
				?>
				$('#tid<?php echo $key;?>').css('display', 'none');
				$('#f<?php echo $key;?>_on').css('display', 'none');
				$('#f<?php echo $key;?>_off').css('display', 'block');
				<?php
			}
		}
	
	?>
		$('#f<?php echo $key;?>_on').click(function()  {
			$('#tid<?php echo $key;?>').css('display', 'none');
			$('#f<?php echo $key;?>_on').css('display', 'none');
			$('#f<?php echo $key;?>_off').css('display', 'block');

			$.ajax({
				type: "POST",
				data: "formname=<?php echo $formname;?>&visibility=0",
				url: "../shared/ajx_design_brief_formstaes.php",
				success: function(msg){
				}
			});
		});
		
		$('#f<?php echo $key;?>_off').click(function()  {
			$('#tid<?php echo $key;?>').css('display', 'block');
			$('#f<?php echo $key;?>_on').css('display', 'block');
			$('#f<?php echo $key;?>_off').css('display', 'none');

			$.ajax({
				type: "POST",
				data: "formname=<?php echo $formname;?>&visibility=1",
				url: "../shared/ajx_design_brief_formstaes.php",
				success: function(msg){
				}
			});
		});
	<?php
	}

	foreach($field_names as $key=>$field_name)
	{
		?>
		
		
		$('#<?php echo $field_name;?>').change(function()  {
			
			var error = 0;
			$(this).next("span").remove();
			$(this).removeClass( "error_field" );

			<?php
				if($field_types[$key] == 2 or $field_types[$key] == 3) {
			?>
					if($.isNumeric( $('#<?php echo $field_name;?>').val())) {
						
					}
					else if($('#<?php echo $field_name;?>').val() != '' ) {
						$(this).addClass( "error_field" );
						$(this).next("span").remove();
						$('#<?php echo $field_name;?>').after('<span class="highlite_value"><img class="img_helper"  border="none" src="/pictures/data_not_ok.png" />&nbsp;Only numbers allowed here</span>');
						error = 1;
						
					}
				
			<?php
				}
				elseif($field_types[$key] == 4) {
			?>
				if($('#<?php echo $field_name;?>').val() != '' ) {
					if(dateRegex.test( $('#<?php echo $field_name;?>').val()) == false) {
						$(this).addClass( "error_field" );
						$(this).next("span").remove();
						$('#<?php echo $field_name;?>').after('<span class="highlite_value"><img class="img_helper"  border="none" src="/pictures/data_not_ok.png" />&nbsp;Invalid date, use "dd.mm.yyyy".</span>');
						error = 1;
					}
				}

			<?php
				}
			?>
			
			if(error == 0) {
				
				$('#<?php echo $field_name;?>').after('<span><img class="img_helper"  border="none" src="/public/data/images/icon-checked.png" /></span>');

				$('#<?php echo $field_name;?>').next("span").fadeOut(3000, function(){ $(this).remove();});				
				
				if($("#<?php echo $field_name;?>").is(':checkbox')) {
					
					<?php
					if(count($field_sets) > 0) {
					?>
						var tmp = new Array();
						<?php
							$data_ids = explode('-', $field_name);

							foreach($field_sets[$data_ids[0]] as $key=>$value) {
								$field_id = $data_ids[0] . '-' . $key;
							?>
							if($("#<?php echo $field_id;?>").is(":checked") == true) {
								tmp[<?php echo $key;?>] = 1;
							}
							else {
								tmp[<?php echo $key;?>] = 0;
							}
							<?php
							}
							
						?>
						var params = "id=<?php echo substr($data_ids[0], 1, strlen($data_ids[0]));?>&value=" + JSON.stringify(tmp) + "&pid=<?php echo $project['project_id'];?>&oid=<?php echo $project['project_order'];?>";
					<?php
					}
					?>
				}
				else {
					var params = "id=<?php echo substr($field_name, 1, strlen($field_name));?>&value=" + $('#<?php echo $field_name;?>').val() + "&pid=<?php echo $project['project_id'];?>&oid=<?php echo $project['project_order'];?>";
				}

				$.ajax({
					type: "POST",
					data: params,
					url: "include/ajx_save_design_briefing_position.php",
					success: function(msg){
					}
				});
			}
			
		});
			
		<?php
	}
	?>
	
	$('.dummysave').click(function()  {
		$('.dummysave').hide();
		$('.dummysaved').fadeIn("slow");

		setTimeout(function (){

			$('.dummysaved').hide();
			$('.dummysave').fadeIn("slow");
			}, 3000); // How long do you want the delay to be (in milliseconds)? 
	});
});




</script>


	<?php
$page->footer();

?>