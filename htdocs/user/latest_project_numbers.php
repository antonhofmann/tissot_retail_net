<?php
/********************************************************************

    latest_project_numbers.php

    Lists used project numbers.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2013-07-22
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2013-07-22
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_access_to_all_projects");



//get all countries
$countries = array();
$sql_countries = "select distinct address_id, country_name, address_company " . 
				 " from orders " .
				 " left join addresses on address_id = order_client_address " . 
				 " left join countries on country_id = address_country " .
				 " where order_type = 1 and country_name is not null " . 
				 " order by country_name";

/*
$orders = array();
$sql = "select distinct country_id, project_postype, " .
	   " max(order_id) as latest_id " . 
	   " from orders " .
	   " left join projects on project_order = order_id " . 
	   " left join addresses on address_id = order_client_address " . 
	   " left join countries on country_id = address_country " .
	   " where order_type = 1 and country_name is not null  and project_postype in (1, 2, 3) " .
	   " and order_number not like '%T%' and order_number  not like '%L%' and order_number  not like '%P%' " . 
	   " group by country_id, project_postype";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$orders[$row["latest_id"]] = $row["latest_id"];
}


$sql = "select distinct country_id, project_postype, " .
	   " max(order_id) as latest_id " . 
	   " from orders " .
	   " left join projects on project_order = order_id " . 
	   " left join addresses on address_id = order_client_address " . 
	   " left join countries on country_id = address_country " .
	   " where order_type = 1 and country_name is not null  and project_postype in (1, 2, 3) " .
	   " and order_number like '%T%' " . 
	   " group by country_id, project_postype";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$orders[$row["latest_id"]] = $row["latest_id"];
}

$sql = "select distinct country_id, project_postype, " .
	   " max(order_id) as latest_id " . 
	   " from orders " .
	   " left join projects on project_order = order_id " . 
	   " left join addresses on address_id = order_client_address " . 
	   " left join countries on country_id = address_country " .
	   " where order_type = 1 and country_name is not null  and project_postype in (1, 2, 3) " .
	   " and order_number like '%L%' " . 
	   " group by country_id, project_postype";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$orders[$row["latest_id"]] = $row["latest_id"];
}


$sql = "select distinct country_id, project_postype, " .
	   " max(order_id) as latest_id " . 
	   " from orders " .
	   " left join projects on project_order = order_id " . 
	   " left join addresses on address_id = order_client_address " . 
	   " left join countries on country_id = address_country " .
	   " where order_type = 1 and country_name is not null  and project_postype in (1, 2, 3) " .
	   " and order_number like '%P%' " . 
	   " group by country_id, project_postype";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$orders[$row["latest_id"]] = $row["latest_id"];
}


$order_filter = $filter = " and order_id IN (" . implode(',', $orders). ")";
*/

//get store_projects
$stores = array();
$sql = "select distinct order_client_address, order_number, " .
       " REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (order_number,'29.','209.'),'28.','208.'),'27.','207.'),'26.','206.'),'25.','205.'),'24.','204.'),'23.','203.'),'22.','202.') AS order_number2 " . 
       " from orders " .
	   " left join projects on project_order = order_id " . 
       " where order_number not like '%T%' and order_number  not like '%L%' and order_number  not like '%P%' " . 
	   " and project_postype = 1 " . 
	   " order by order_number2";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$stores[$row["order_client_address"]] = $row["order_number"];
}


//get sis projects
$sis = array();
$sql = "select distinct order_client_address, order_number, " .
       " REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (order_number,'29.','209.'),'28.','208.'),'27.','207.'),'26.','206.'),'25.','205.'),'24.','204.'),'23.','203.'),'22.','202.') AS order_number2 " . 
       " from orders " .
	   " left join projects on project_order = order_id " . 
       " where order_number not like '%T%' and order_number  not like '%L%' and order_number  not like '%P%' " . 
	   " and project_postype = 2" . 
	   " order by order_number2";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$sis[$row["order_client_address"]] = $row["order_number"];
}


//get kiosk projects
$kiosk = array();
$sql = "select distinct order_client_address, order_number, " .
        " REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (order_number,'29.','209.'),'28.','208.'),'27.','207.'),'26.','206.'),'25.','205.'),'24.','204.'),'23.','203.'),'22.','202.') AS order_number2 " . 
       " from orders " .
	   " left join projects on project_order = order_id " . 
       " where order_number not like '%T%' and order_number  not like '%L%' and order_number  not like '%P%' " . 
	   " and project_postype = 3" . 
	   " order by order_number2";
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$kiosk[$row["order_client_address"]] = $row["order_number"];
}


//get takeover projects
$takeover = array();
$sql = "select distinct order_client_address, order_number, " .
        " REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (order_number,'29.','209.'),'28.','208.'),'27.','207.'),'26.','206.'),'25.','205.'),'24.','204.'),'23.','203.'),'22.','202.') AS order_number2 " . 
       " from orders " .
	   " left join projects on project_order = order_id " . 
       " where order_number like '%T%' " . 
	   " order by order_number2";
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$takeover[$row["order_client_address"]] = $row["order_number"];
}

//get lease renewal projects
$lease_renewals = array();
$sql = "select distinct order_client_address, order_number, " .
        " REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (order_number,'29.','209.'),'28.','208.'),'27.','207.'),'26.','206.'),'25.','205.'),'24.','204.'),'23.','203.'),'22.','202.') AS order_number2 " . 
       " from orders " .
	   " left join projects on project_order = order_id " . 
       " where order_number like '%L%' " . 
	   " order by order_number2";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$lease_renewals[$row["order_client_address"]] = $row["order_number"];
}


//get popups projects
$popups = array();
$sql = "select distinct order_client_address, order_number, " .
        " REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (REPLACE (order_number,'29.','209.'),'28.','208.'),'27.','207.'),'26.','206.'),'25.','205.'),'24.','204.'),'23.','203.'),'22.','202.') AS order_number2 " . 
       " from orders " .
	   " left join projects on project_order = order_id " . 
       " where order_number like '%P%' " . 
	   " order by order_number2";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$popups[$row["order_client_address"]] = $row["order_number"];
}

$list = new ListView($sql_countries);
$list->set_entity("projects");
$list->add_column("country_name", "Country");
$list->add_column("address_company", "Company");
$list->add_text_column("stores", "Stores", 0, $stores);
$list->add_text_column("sis", "SIS", 0, $sis);
$list->add_text_column("kiosk", "Kiosk", 0, $kiosk);
$list->add_text_column("takeover", "Take Over", 0, $takeover);
$list->add_text_column("leaserenewal", "Lease Renewal", 0, $lease_renewals);
$list->add_text_column("popups", "PopUps", 0, $popups);






$page = new Page("projects");

require_once("include/projects_list_page_actions.php");

$page->header();
$page->title("Latest Project Numbers");

$list->render();

$page->footer();

?>
