<?php
/********************************************************************

    project_perform_action.php

    Perform an action selected from the action list
    in project_task_center.php

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2002-10-03
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2015-08-16
    Version:        2.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "include/order_state_constants.php";
require_once "../shared/project_cost_functions.php";

check_access("can_use_taskcentre_in_projects");

/********************************************************************
    get project and client data
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

// get user data of order's user
$client_user = get_user($project["order_user"]);
$logged_user = get_user(user_id());

$user_roles = get_user_roles(user_id());


/********************************************************************
   build form: 
   - first part: project is cancelled or moved to the records
   - second part: all other steps
*********************************************************************/
if (id() == MOVED_TO_THE_RECORDS 
	or id() == ORDER_CANCELLED)
{
    $form = new Form("projects", "project");
    $form->add_hidden("pid", param("pid"));
    $form->add_hidden("order_id", $project["project_order"]);

    require_once "include/project_head_small.php";

    $error = 0;
    if (id() == MOVED_TO_THE_RECORDS)
    {
	        
        //check if cost monitoring sheet is approved
		if($project['project_projectkind'] != 4 
			and $project['project_projectkind'] != 5) // no take over, noe lease renewal
		{
			
			// ase: remove cms approval for system administrators
			if (!in_array(1, $user_roles)) 
			{
				$sql = "select project_cost_cms_approved, project_cost_cms_controlled " . 
					   "from project_costs " . 
					   "where project_cost_order = " . dbquote($project["project_order"]);

				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				
				
					
				if($project["project_cost_type"] == 1
					and $row["project_cost_cms_controlled"] == 0)
				{
					$error = 1;        
					$form->error("The project can not be put to the records since the cost monitoring sheet has not been approved! <br />Please submit the CMS for approval!");
				}
				elseif($project["project_cost_type"] != 1
					and $row["project_cost_cms_approved"] == 0)
				{
					$error = 1;        
					$form->error("The project can not be put to the records since the cost monitoring sheet has not been approved! Please submit the project for approval of the CMS to the Project Leader!");
				}
			}
			
		}
		
		//check if POS has a POS opening date
		if($error == 0 
			and ($project["project_actual_opening_date"] == NULL 
			     or $project["project_actual_opening_date"] == '0000-00-00')
		  )
		{
			$error = 1;        
			$form->error("The project can not be put to the records since the actual POS opening date is missing!");
		}
    }
    elseif (id() == ORDER_CANCELLED)
    {
        $creps = get_recipients_on_cancellation(param("pid"));
		$form->add_comment("Please confirm the cancellation of the project!<br />The following persons will be informed by an email notification.");

		foreach($creps as $key=>$user)
		{
			if($user["role_name"] == "Standard Recipient")
			{
				$form->add_checkbox("CR" . $user["user_id"], $user["role_name"], true, 0, $user["full_name"]);
			}
			else
			{
				$form->add_checkbox("CR" . $user["user_id"], $user["role_name"], true, 0, $user["full_name"]);
			}
		}
		
		$form->add_comment("Please indicate the reason!");
		$form->add_multiline("reason_for_cancelling", "Reason for cancelling*", 4, NOTNULL);
		
    }

    if($error == 0)
    {
       $form->add_comment("Please confirm that you want to put the project to the records!");
	   $form->add_button("confirm", "Put the Project to the Records");
    }

    $form->add_button("back", "Back");

    $form->populate();
    $form->process();

    if ($form->button("back"))
    {
        $link = "project_task_center.php?pid=" . param("pid") . "&id=" . param("pid");
        redirect($link);
    }
    if ($form->button("confirm") and $form->validate())
    {
        $uids = "";
		foreach($creps as $key=>$user)
		{
			if($user["role_name"] == "Standard Recipient")
			{
				$uids .= $user["user_id"] . "-";

			}
			elseif($form->value("CR" . $user["user_id"]) == 1)
			{
				$uids .= $user["user_id"] . "-";
			}
		}

	if(param('reason_for_cancelling'))
	{
		$comment = "manually cancelled" . "\n\r" . param('reason_for_cancelling');
		$_SESSION["reason_for_cancellation"] = $comment;
	}
	else
	{
		$comment = "manually cancelled";
	}
	
		//project tracking
		$field = "project_real_opening_date";
		$sql = "Insert into projecttracking (" . 
			   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
			   user_id() . ", " . 
			   $project["project_id"] . ", " . 
			   dbquote("order_actual_order_state_code") . ", " . 
			   dbquote($project["order_actual_order_state_code"]) . ", " . 
			   dbquote("900") . ", " . 
			   dbquote($comment) . ", " . 
			   dbquote(date("Y-m-d H:i:s")) . ")"; 
			   
		$result = mysql_query($sql) or dberror($sql);
		
		$link = "project_perform_action_confirm.php?pid=" . param("pid") . "&id=" . param("pid") . "&action=" . id() ."&uids=" . $uids;
        redirect($link);
    }

    /********************************************************************
        render page
    *********************************************************************/
    $page = new Page("projects");

    $page->header();
    
    if (id() == MOVED_TO_THE_RECORDS)
    {    
        $page->title("Move project to the records");
    }
    if (id() == ORDER_CANCELLED)
    {    
        $page->title("Cancel project");
    }

    $form->render();
	
	require_once "include/project_footer_logistic_state.php";
    $page->footer();
}
else
{
	/***************************************************************************
    get the message text from table strings
	****************************************************************************/
	
	//submission of LN/AF/INR03
	$string_name = "project_" .id();
	if(id() == PROJECT_REQUEST_ACCEPTED
		and $project['project_cost_type'] == 1) {
		$string_name = $string_name . "C";

	}
	elseif(id() == PROJECT_REQUEST_ACCEPTED
		and $project['project_cost_type'] != 1) {
		$string_name = $string_name . "O";

	}
	
	//layout locally provided
	$layout_is_provided_locally = false;
	if($project['project_production_type'] == 4) {
		$layout_is_provided_locally = true;
	}


	//local production
	$local_prodcution = false;
	if($project['project_production_type'] == 2
		or $project['project_production_type'] == 4) {
		$local_prodcution = true;
	}
	
	$message_text = get_string($string_name);
	$message_text = str_replace('{sender_firstname}', $logged_user["firstname"], $message_text);
	$message_text = str_replace('{sender_name}', $logged_user["name"], $message_text);

	
	/***************************************************************************
    check if delivery address is entered in case of step 620
	which is a necessary condition for budget approval of HQ supplied items
	****************************************************************************/
	/*
	if((param("id") == 620 or param("id") == 840) and !param("nop"))
	{
		$delivery_information_is_entered = check_if_delivery_address_is_entered($project["project_order"]);
		if($delivery_information_is_entered == false)
		{
			$link = "project_add_delivery_address.php?pid=" . param("pid") . "&id=" . param("id");
			redirect($link);
		}
	}
	*/


	/********************************************************************
		Budget approval is not possible in case the project is a
		corporate project and the ln is not approved
	*********************************************************************/
	$buget_approval_is_possible = true;
	if($project["project_cost_type"] == 1) // corporate
	{
		$cer_is_needed = false;
		$af_is_needed = false;
		//postypes CER check if CER is needed
		$sql = "select posproject_type_needs_cer, posproject_type_needs_af, 
				posproject_type_needs_inr03 from posproject_types " . 
			   "where posproject_type_postype = " . $project["project_postype"] .  
			   " and posproject_type_projectcosttype = " . $project["project_cost_type"] . 
			   " and posproject_type_projectkind = " . $project["project_projectkind"];

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			if($row["posproject_type_needs_cer"] == 1 or $row["posproject_type_needs_inr03"])
			{
				$cer_is_needed = true;
			}
		}

		//check if ln was approved in case CER is needed
		if($cer_is_needed == true)
		{
			//chek if ln submission is necessary
			$sql = "select ln_no_ln_submission_needed " . 
				   "from ln_basicdata " . 
				   " where ln_basicdata_version = 0 " . 
				   " and ln_basicdata_project = " . $project["project_id"];
			
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				if($row["ln_no_ln_submission_needed"] != 1)
				{
					$milestone = get_project_milestone($project["project_id"], 13);
			
					if(count($milestone) > 0 
						and ($milestone['project_milestone_date'] == NULL 
							 or $milestone['project_milestone_date'] == '0000-00-00')) // LN was not approved yet
					{
						$buget_approval_is_possible = false;
					}
				}
			
			}
		}
	}
	else
	{
		$cer_is_needed = false;
		$af_is_needed = false;
		//postypes CER check if CER is needed
		$sql = "select posproject_type_needs_cer, posproject_type_needs_af, 
				posproject_type_needs_inr03 from posproject_types " . 
			   "where posproject_type_postype = " . $project["project_postype"] .  
			   " and posproject_type_projectcosttype = " . $project["project_cost_type"] . 
			   " and posproject_type_projectkind = " . $project["project_projectkind"];

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			if($row["posproject_type_needs_af"] or $row["posproject_type_needs_inr03"])
			{
				$af_is_needed = true;
			}
		}
	}


	/********************************************************************
		Budget approval for hq supplied items is not posssible if the
		conditions are not given
		only if planning is required
	*********************************************************************/
	$budget_actions_hq_supplied_items_possible = true;

	if($project["project_no_planning"] != 1) {
		if($project["project_projectkind"] == 1
			or $project["project_projectkind"] == 2
			or $project["project_projectkind"] == 3
			or $project["project_projectkind"] == 6
			or $project["project_projectkind"] == 9
			or $project["project_projectkind"] == 7) {
		
			//SIS and Kiosk
			if($project["project_postype"] == 2
				or $project["project_postype"] == 3) {
				
				//check if step 417 was performed
				$sql = "select count(actual_order_state_id) as num_recs " . 
					 " from actual_order_states " . 
					 " where actual_order_state_order = " . dbquote($project["project_order"]) . 
					 " and actual_order_state_state = 81";
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				if ($row["num_recs"] == 0) {
					$budget_actions_hq_supplied_items_possible = false;
				}

			}
			elseif($project["project_postype"] == 1) {
			
				//check if step 441 was performed
				$sql = "select count(actual_order_state_id) as num_recs " . 
					 " from actual_order_states " . 
					 " where actual_order_state_order = " . dbquote($project["project_order"]) . 
					 " and actual_order_state_state = 84";
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				if ($row["num_recs"] == 0) {
					$budget_actions_hq_supplied_items_possible = false;
				}

			}
		
		}
	}

	/********************************************************************
	   check if a cer of af submission is needed for the project
	*********************************************************************/
	$af_cer_submission_is_needed = false;
	$sql = "select posproject_type_needs_cer, posproject_type_needs_af, posproject_type_needs_inr03 " . 
		   "from posproject_types " . 
		   "where posproject_type_postype =  " . $project["project_postype"] .
		   " and posproject_type_projectcosttype =  " . $project["project_cost_type"] .
		   " and posproject_type_projectkind =  " . $project["project_projectkind"];
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		if($row["posproject_type_needs_cer"] == 1 
			or $row["posproject_type_needs_af"] == 1
			or $row["posproject_type_needs_inr03"] == 1)
		{
			$af_cer_submission_is_needed = true;
		}
	}


	/********************************************************************
	   check if ticker in cer is not set
	*********************************************************************/
	if($af_cer_submission_is_needed == true)
	{
		$sql = "select cer_basicdata_no_cer_submission_needed " . 
			   " from cer_basicdata " . 
			   " where cer_basicdata_version = 0 " . 
			   " and cer_basicdata_project = " . dbquote($project["project_id"]);

		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			if($row["cer_basicdata_no_cer_submission_needed"] == 1)
			{
				$af_cer_submission_is_needed = false;
			}
		}

	}


	/********************************************************************
	   check if the POS has pictures uploaded
	*********************************************************************/
	$pos_pictures_uploaded = false;
	if(id() == CMS_COMPLETTION_DEVELOPMENT or id() == CMS_APPROVE_BY_CONTROLLER)
	{
		$sql = "select count(order_file_id) as num_recs from order_files " . 
			   " where order_file_category = 11 " . 
			   " and order_file_order = " . dbquote($project["project_order"]);

		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		if ($project['project_postype'] == 1 and $row["num_recs"] > 4)
		{
			$pos_pictures_uploaded = true;
		}
		elseif($project['project_postype'] == 2 and  $row["num_recs"] > 2)
		{
			$pos_pictures_uploaded = true;
		}
		elseif($row["num_recs"] > 0) {
			$pos_pictures_uploaded = true;
		}
	}


	/********************************************************************
	   check CMS completion state
	*********************************************************************/
	$cms_completed_by_lc = false;
	$cms_completed_by_client = false;
	$cms_completed_by_pl = false;

	$sql = "select project_cost_cms_completed, project_cost_cms_completed2, project_cost_cms_approved " .
		   " from project_costs " . 
		   " where project_cost_order = " . dbquote($project["project_order"]);

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		if($row["project_cost_cms_completed"] == 1)
		{
			$cms_completed_by_lc = true;
		}

		if($row["project_cost_cms_completed2"] == 1)
		{
			$cms_completed_by_client = true;
		}

		if($row["project_cost_cms_approved"] == 1)
		{
			$cms_completed_by_pl = true;
		}
	}


	/********************************************************************
	   check local costs are in the project
	*********************************************************************/
	$local_costs_are_in_the_project = false;
	$sql = "select sum(order_item_system_price) as lc " . 
		   " from order_items " . 
		   " where order_item_order = " . dbquote($project["project_order"]) .
		   " and order_item_cost_group in (7, 8, 9, 11)";


	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		if($row["lc"] > 0)
		{
			$local_costs_are_in_the_project = true;
		}
	}

	
	/********************************************************************
	   check if project state must be kept
	   415 done, but 417 not yet done
	   and offer process is started
	*********************************************************************/
	$_keep_project_state = false;
	if(id() >= '510' and id() <= '560') {
	
		if($project["order_development_status"] == '415') {
			$_keep_project_state = true;
		}
	}
	
	//no check for non corporate projects
	if($project["project_cost_type"] != 1)
	{
		//$local_costs_are_in_the_project = false;
	}


	//only for corporate projects without local costs
	if($project["project_cost_type"] == 1 and $local_costs_are_in_the_project == false)
	{
		//$cms_completed_by_client = true;
	}



	/********************************************************************
	   check if design briefing is in the project
	*********************************************************************/
	$design_briefing_is_missing = true;
	$sql = "select count(project_design_brief_id) as num_recs " . 
		   " from project_design_briefs " . 
		   " where project_design_brief_project_id = " . dbquote($project["project_id"]);

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		if($row["num_recs"] > 0)
		{
			$design_briefing_is_missing = false;
		}
	}

	$design_briefing_is_incomplete = false;
	$sql = "select count(project_design_brief_position_id) as num_recs  
		    from project_design_brief_positions 
		    left join project_design_briefs on project_design_brief_id = project_design_brief_position_design_brief_id
			left join design_brief_positions on design_brief_position_id = project_design_brief_position_position_id
		    where project_design_brief_project_id = " . dbquote($project["project_id"]) . 
			" and design_brief_position_mandatory = 1
			  and (project_design_brief_position_content = '' or project_design_brief_position_content is null)";

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		if($row["num_recs"] > 0)
		{
			$design_briefing_is_incomplete = true;
		}
	}



	/********************************************************************
	   get regional responsibles
	*********************************************************************/
	$regional_responsable_reciepients = get_regional_responsibles_full_data($project["project_cost_type"], $project["order_client_address"]);

	$regional_sales_managers_hq = get_regional_sales_managers_hq($project["project_cost_type"], $project["order_client_address"]);


	$managers_hq = get_hq_management();


	/********************************************************************
	   init variables
	*********************************************************************/
	$recipients_already_there = array();
	$write_budget_to_disk = true;
	$files_are_to_be_uploaded = false;
	$order_step_attachments = array();
	$order_step_attachment_file_paths = array();
	$attachments = array();
	$attachment_recipients = array();
	$delete_task_confirm_pickup = false;
	$project_can_be_terminated = false;

	$total_file_size = 0;
	$m_result = false;


	// get Action parameter
    $action_parameter = get_action_parameter(id(), 1);

    // people involved in the project step
    $recipients = array();
	
	// people to be put on cc
    $cc_recipients = array();

	// suppliers involved in the project step
	//neded only for step 730 to add task for step 745
    $involved_suppliers = array();

	//people from procurement
	$procurement = array();


	/********************************************************************
	   get notification recipients for attachments of the type layout
	*********************************************************************/
	$notification_recipients1 = array();
	$notification_recipients2 = array();
	$notification_recipients3 = array();
	$notification_recipients4 = array();
	$notification_recipients5 = array();
	$notification_recipients6 = array();

	$sql = "select * from postype_notifications " .
		   "where postype_notification_postype = " . dbquote($project["project_postype"]) . 
		   " and postype_notification_prodcut_line = " . dbquote($project["project_product_line"]);


	$res = mysql_query($sql) or dberror($sql);

	while ($row = mysql_fetch_assoc($res))
	{
		if($row["postype_notification_email5"])
		{
			$notification_recipients1[] = $row["postype_notification_email5"];
		}
		if($row["postype_notification_email6"])
		{
			$notification_recipients2[] = $row["postype_notification_email6"];
		}
		if($row["postype_notification_email7"])
		{
			$notification_recipients3[] = $row["postype_notification_email7"];
		}
		if($row["postype_notification_email8"])
		{
			$notification_recipients4[] = $row["postype_notification_email8"];
		}

		if($row["postype_notification_email9"])
		{
			$notification_recipients5[] = $row["postype_notification_email9"];
		}
		if($row["postype_notification_email10"])
		{
			$notification_recipients6[] = $row["postype_notification_email10"];
		}
	}


	
	/********************************************************************
        init error messages
    *********************************************************************/
    $error = array();
    $error[1] = "No mail was sent!<br />Please select a recipient at the bottom of the page.";
    $error[2] = "There are no recipients for this step! The cause might be one of the following:<br />- you have not entered the P.O. Numbers (edit list of materials)<br />- you have already ordered all items<br />- the supplier has no email address specified in the address data (contact administrator)";
    $error[3] = "There are no recipients for this step! The cause might be one of the following:<br />- you have not assigned forwarders (edit list of materials)<br />- you have made no order to a supplier (step 700)<br />- the supplier has not entered an expected ready for pick up date<br />- all items already have a pickup date<br />- the forwarder has no email address specified in the address data (contact administrator)<br />- there are items not having a ship to information";
    $error[4] = "There are no recipients for this step! The cause might be one of the following:<br />- you have not assigned a design contractor<br />- the design contractor has no email address specified in the address data (contact administrator)";
    $error[5] = "There are no recipients for this step! The cause might be one of the following:<br />- you have not assigned a design supervisor<br />- the design supervisor has no email address specified in the address data (contact administrator)";
    $error[6] = "There are no recipients for this step! The cause might be one of the following:<br />- there is no item requiering an offer<br />- you have no entries in the list of materials<br />- the suppliers have no email address specified in the address data (contact administrator)";
    $error[7] = "You can not confirm delivery because there are items in your project not having an arrival date.";
    $error[8] = "You can not submit a request for budget approval since not all items have been offered by the suppliers or not all offers made have been accepted! <br />Another cause might be that the owner of the project is a user not beeing active anymore and was not replaced by another person.";
    $error[9] = "You can not submit an offer without having indicated in the prices (edit offer data).";
    $error[10] = "You can not confirm an order without having indicated the ready for pick up data<br />or your might not be logged in as a valid supplier.";
    $error[11] = "You can not accept a request for delivery without having indicated <br />the expected date of arrival.";
	$error[12] = "Layout request can not be accepted. Please enter the realistic shop opening date first (edit pso data).";
	
	$error[13] = "Submission is not possible since LN has not been approved yet.";
	$error[14] = "Budget can not be accepted since LN has not been approved yet.";
	
	$error[15] = "You cannot confirm the pick up. The reason might be one of the following:<br/>- you already have confirmed the pick up of all items<br />- you have not entered a pick up date for one or more items.";
	$error[16] = "The file type of the file you try to upload has an extension not allowed to be uploaded!";

	$error[17] = "Confirmation is not possible since the CMS was not confirmed by the Logistic Coordinator (step 850) neither by the Client (Step 851)!";
	$error[18] = "Confirmation is not possible since the CMS was not confirmed by the Logistic Coordinator (Step 850)!";
	$error[19] = "Confirmation is not possible since the CMS was not confirmed by the Client (Step 851)!";
	$error[20] = "CMS Approval is not possible since the CMS was not confiremd by the Project Leader (Step 866)!";

	$error[21] = "Action is not possible since required planning documents were not approved (417 for SIS/Kiosk, 441 for Stores).";



	$error[22] = "Action is not possible since required design briefing is missing.";
	$error[23] = "Action is not possible since not all mandatory fields of the design briefing are filled.";

	$error[24] = "You must select at least one attachment form the section layouts and booklets.";

    
	/********************************************************************
        get all recipients involved in the specific step
    *********************************************************************/

    if ($action_parameter["recipient"] == 'Overall RN Executive')
    {
		$sql = "select user_id, address_id, ".
               "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy, address_type ".
               "from users ".
               "left join addresses on user_address = address_id ".
               "where user_email<>'' ".
               "    and user_active = 1 " .
               "    and user_id = " . dbquote($project["order_user"]);

		$res = mysql_query($sql) or dberror($sql);

        if ($row = mysql_fetch_assoc($res))
        {
            $user_data = array();
            $user_data["id"] = $row["user_id"];
            $user_data["email"] = $row["user_email"];
            $user_data["cc"] = $row["user_email_cc"];
            $user_data["deputy"] = $row["user_email_deputy"];
            $user_data["full_name"] = $row["user_full_name"];
            $user_data["address"] = $row["address_id"];
            $recipients[$row["user_id"]] = $user_data;
        }
    }
	elseif ($action_parameter["recipient"] == 'Local Project Leader')
    {
		$sql = "select user_id, address_id, ".
               "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy, address_type ".
               "from users ".
               "left join addresses on user_address = address_id ".
               "where user_email<>'' ".
               "    and user_active = 1 " .
               "    and user_id = " . dbquote($project["project_local_retail_coordinator"]);

		$res = mysql_query($sql) or dberror($sql);

        if ($row = mysql_fetch_assoc($res))
        {
            $user_data = array();
            $user_data["id"] = $row["user_id"];
            $user_data["email"] = $row["user_email"];
            $user_data["cc"] = $row["user_email_cc"];
            $user_data["deputy"] = $row["user_email_deputy"];
            $user_data["full_name"] = $row["user_full_name"];
            $user_data["address"] = $row["address_id"];
            $recipients[$row["user_id"]] = $user_data;
        }
    }
    else if ($action_parameter["recipient"] == 'Supplier')
    {
		$where_clause = "";
		if (id() == SUBMIT_REQUEST_FOR_OFFER 
				 or id() == OFFER_REJECTED 
				 or id() == OFFER_ACCEPTED)
		{
			$where_clause = " and (order_item_no_offer_required is null or order_item_no_offer_required = 0) ";
		}

		$sql =  "select distinct user_id, address_id, address_type, order_item_order_revisions, ".
				"    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
				"    user_email, user_email_cc, user_email_deputy ".
				"from order_items ".
				"left join addresses on order_item_supplier_address = address_id ".
				"left join users on user_address = address_id ".
				"where user_email<>'' " . $where_clause .
				"    and user_active = 1 " .
				"    and order_item_order = " . $project["project_order"] . " " .
				"    and (order_item_not_in_budget is null or order_item_not_in_budget =0)" .
				" order by address_company, user_name";

	
		$res = mysql_query($sql) or dberror($sql);
        while ($row = mysql_fetch_assoc($res))
        {
			if (id() == ORDER_TO_SUPPLIER_SUBMITTED)
            {
                // check if there are items in the order not having been ordered already
                $sql_ordered = "select order_item_id, order_item_order_revisions, " .
                               "   order_item_po_number, order_item_ordered ".
                               "from order_items ".
                               "where order_item_type <= " . ITEM_TYPE_SPECIAL .
                               "    and order_item_supplier_address = ". $row["address_id"] .
                               "    and (order_item_not_in_budget is null or order_item_not_in_budget =0)" .
                               "    and order_item_order = " . $project["project_order"] . 
					           "    and order_item_quantity > 0";

                $res1= mysql_query($sql_ordered) or dberror($sql_ordered);
				$revisions = 0;

                while ($row1 = mysql_fetch_assoc($res1))
                {
                    $already_ordered = 1;
                    $has_po_number = 0;
                    if ($row1["order_item_ordered"] == "0000-00-00"   
						or $row1["order_item_ordered"] == Null)
                    {
                        $already_ordered = 0;
                    }

					//check if order was cancelled by supplier and therefore allow resubmission of order
					$order_was_confirmed_by_supplier = 0;
					$sql_os = 'select user_address ' . 
					          'from actual_order_states ' .
						      'left join users on user_id = actual_order_state_user ' .
					          'where actual_order_state_order = ' . $project["project_order"] .
						      ' and actual_order_state_state = 37';

					$res_os= mysql_query($sql_os) or dberror($sql_os);
					if ($row_os = mysql_fetch_assoc($res_os))
					{
						if($row["address_id"] == $row_os['user_address'])
						{
							$order_was_confirmed_by_supplier = 1;
						}
					}

					if($order_was_confirmed_by_supplier == 0)
					{
						$sql_os = 'select user_address ' . 
								  'from actual_order_states ' .
								  'left join users on user_id = actual_order_state_user ' .
								  'where actual_order_state_order = ' . $project["project_order"] .
								  ' and actual_order_state_state = 36';

						$res_os= mysql_query($sql_os) or dberror($sql_os);
						if ($row_os = mysql_fetch_assoc($res_os))
						{
							if($row["address_id"] == $row_os['user_address'])
							{
								$already_ordered = 0;
							}
						}
					}
               
                    if ($row1["order_item_po_number"])
                    {
                        $has_po_number = 1;
                    }

					if($revisions == 0) // number of order revisions
					{
						$revisions = $row1["order_item_order_revisions"];
					}

                    if ($already_ordered == 0 and $has_po_number == 1)
                    {
						
						$user_data = array();
						$user_data["id"] = $row["user_id"];
						$user_data["email"] = $row["user_email"];
						$user_data["cc"] = $row["user_email_cc"];
						$user_data["deputy"] = $row["user_email_deputy"];
						$user_data["full_name"] = $row["user_full_name"];
						$user_data["address"] = $row["address_id"];
						$user_data["order_revisions"] = $revisions;
						$user_data["role"] = "Supplier";
						$user_data["address_type"] = $row["address_type"];
						$recipients[$row["user_id"]] = $user_data;
                    }
                }
            }
            else
            {
				$user_data = array();
				$user_data["id"] = $row["user_id"];
				$user_data["email"] = $row["user_email"];
				$user_data["cc"] = $row["user_email_cc"];
				$user_data["deputy"] = $row["user_email_deputy"];
				$user_data["full_name"] = $row["user_full_name"];
				$user_data["address"] = $row["address_id"];
				$user_data["role"] = "Supplier";
				$recipients[$row["user_id"]] = $user_data;

            }
        }

		
		//add suppliers being able to submit a quote without items in the list of materials
		//having  submitted an offer
		if (id() == OFFER_REJECTED 
				 or id() == OFFER_ACCEPTED)
		{
			//get all suppliers having submitted an offer
			$pending_suppliers = array();
			$sql_c = "select task_from_user " . 
					 " from tasks where task_order_state = 29 " . 
					 " and task_order = " . $project["project_order"];
			$res_c= mysql_query($sql_c) or dberror($sql_c);

			while ($row_c = mysql_fetch_assoc($res_c))
			{
				$pending_suppliers[$row_c["task_from_user"]] = $row_c["task_from_user"];
			}

			if(count($pending_suppliers) > 0)
			{
				
				$sql_c =  "select distinct user_id, address_id, address_type, ".
							"    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
							"    user_email, user_email_cc, user_email_deputy ".
							"from users ".
							"left join addresses on address_id = user_address ".
							"where user_email<>'' and user_id in (" . implode(', ', $pending_suppliers) . ") " .
							"    and user_active = 1 " .
							" order by address_company, user_name";

				

			
				$res_c = mysql_query($sql_c) or dberror($sql_c);
				while ($row_c = mysql_fetch_assoc($res_c))
				{
					$user_data = array();
					$user_data["id"] = $row_c["user_id"];
					$user_data["email"] = $row_c["user_email"];
					$user_data["cc"] = $row_c["user_email_cc"];
					$user_data["deputy"] = $row_c["user_email_deputy"];
					$user_data["full_name"] = $row_c["user_full_name"];
					$user_data["address"] = $row_c["address_id"];
					$user_data["role"] = "Supplier";
					$recipients[$row_c["user_id"]] = $user_data;
				}
			}
		}

		//add suppliers being able to submit a quote without items in the list of materials
		if (id() == SUBMIT_REQUEST_FOR_OFFER)
		{
			$sql_f = "select user_id, address_id, ".
				   "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
				   "    user_email, user_email_cc, user_email_deputy, address_type ".
				   "from users " . 
				   "left join addresses on user_address = address_id ".
				   "where address_can_be_requested_for_quote = 1 " .  
				   " and user_email<>'' ".
				   "    and user_active = 1 ";
			
			$res_f = mysql_query($sql_f) or dberror($sql_f);

			while ($row_f = mysql_fetch_assoc($res_f))
			{
				$user_data = array();
				$user_data["id"] = $row_f["user_id"];
				$user_data["email"] = $row_f["user_email"];
				$user_data["cc"] = $row_f["user_email_cc"];
				$user_data["deputy"] = $row_f["user_email_deputy"];
				$user_data["full_name"] = $row_f["user_full_name"];
				$user_data["address"] = $row_f["address_id"];
				$user_data["role"] = "Supplier";
				$recipients[$row_f["user_id"]] = $user_data;
			}


			//get people from procurement
			$sql = "select *, " . 
				   " concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name ". 
				   " from user_roles " . 
				   " left join users on user_id = user_role_user " .
				   " left join addresses on address_id = user_address ".
				   " where user_active = 1 " .
				   " and user_role_role = 46";
			
			
			$res = mysql_query($sql) or dberror($sql);
			while ($row = mysql_fetch_assoc($res))
			{
				$user_data = array();
				$user_data["id"] = $row["user_id"];
				$user_data["email"] = $row["user_email"];
				$user_data["cc"] = $row["user_email_cc"];
				$user_data["deputy"] = $row["user_email_deputy"];
				$user_data["full_name"] = $row["user_full_name"];
				$user_data["address"] = $row["address_id"];
				$user_data["role"] = "Procurement";
				$user_data["address_type"] = $row["address_type"];
				$procurement[$row["user_id"]] = $user_data;
			}
		}
    }
    else if (id() == REQUEST_FOR_DELIVERY_SUBMITTED 
		and $action_parameter["recipient"] == 'Forwarder')
    {
		$sql = "select distinct order_item_id, user_id, address_id, ".
               "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy, address_type, order_item_supplier_address ".
               "from order_items ".
               "left join addresses on order_item_forwarder_address = address_id ".
               "left join users on user_address = address_id ".
			   "left join user_roles on user_role_user = user_id " .
               "where user_email<>'' ".
               "    and user_active = 1 " .
               "    and order_item_order = " . $project["project_order"] . " " .
               "    and (order_item_not_in_budget is null or order_item_not_in_budget =0)" .
			   "    and (user_role_role in (6) or user_id = " . dbquote($project["order_user"]) . ")" . 
               " order by address_company, user_name";

	
		
		$res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {
			
			
			// check if there are items in the order not having been ordered already
			$sql_ordered = "select order_item_id, order_item_order, order_item_item, " .
						   "   order_item_ready_for_pickup, " .
						   "   order_item_pickup, order_item_expected_arrival " .
						   "from order_items ".
						   "where order_item_type <= " . ITEM_TYPE_SPECIAL .
						   "    and order_item_forwarder_address = ". $row["address_id"] .
						   "    and (order_item_not_in_budget is null or order_item_not_in_budget =0)" .
						   "    and order_item_order = " .$project["project_order"] . 
						   "    and order_item_quantity > 0";
			$res1= mysql_query($sql_ordered) or dberror($sql_ordered);
			while ($row1 = mysql_fetch_assoc($res1))
			{
				$ready_for_pick_up = 0;
				$pick_up_date_entered = 1;
				$expected_arrival_entered = 1;

				if ($row1["order_item_ready_for_pickup"])
				{
					$ready_for_pick_up = 1;
				}

				if ($row1["order_item_pickup"] == "0000-00-00"   or $row1["order_item_pickup"] == Null)
				{
					$pick_up_date_entered = 0;
				}

				if ($row1["order_item_expected_arrival"] == "0000-00-00"   or $row1["order_item_expected_arrival"] == Null)
				{
					$expected_arrival_entered = 0;
				}

				//check if there is a delivery address for the item
				$sql_d = "select count(order_address_id) as num_recs " . 
						 " from order_addresses " . 
						 " where order_address_order = " . dbquote($row1["order_item_order"]) .
						 " and order_address_order_item = " . dbquote($row1["order_item_id"]) . 
						 " and order_address_type = 2 " .
						 " and order_address_place_id > 0";

				$res_d= mysql_query($sql_d) or dberror($sql_d);
				$row_d = mysql_fetch_assoc($res_d);
				if($row_d["num_recs"] == 0)
				{
					$ready_for_pick_up = 0;
				}


				if ($ready_for_pick_up == 1 
					and $pick_up_date_entered == 0 
					and $expected_arrival_entered == 0)
				{
					
					$user_data = array();
					$user_data["id"] = $row["user_id"];
					$user_data["email"] = $row["user_email"];
					$user_data["cc"] = $row["user_email_cc"];
					$user_data["deputy"] = $row["user_email_deputy"];
					$user_data["full_name"] = $row["user_full_name"];
					$user_data["address"] = $row["address_id"];
					
					if($row["address_id"] == $project["order_client_address"])
					{
						$user_data["role"] = "Project Owner";
					}
					else
					{
						$user_data["role"] = "Forwarder";
					}
					
					$user_data["address_type"] = $row["address_type"];
					$recipients[$row["user_id"]] = $user_data;

					//get involved suppliers to prepare 745

					$sql_s = "select distinct user_id, address_id, ".
							   "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
							   "    user_email, user_email_cc, user_email_deputy, address_type ".
							   "from order_items ".
							   "left join addresses on order_item_supplier_address = address_id ".
							   "left join users on user_address = address_id ".
							   "where user_email<>'' ".
							   "    and user_active = 1 " .
							   "    and order_item_id = " . $row["order_item_id"] . 
							   "    and order_item_quantity > 0 " . 
							   "    and order_item_pickup_confirmed <> 1 " .
							   "    and order_item_forwarder_address = " . dbquote($row["address_id"]) . 
							   " order by address_company, user_name";

					$res_s = mysql_query($sql_s) or dberror($sql_s);

					if ($row_s = mysql_fetch_assoc($res_s))
					{
						
						//check if already has a task

						$sql_su = "select count(task_id) as num_recs " . 
							     " from tasks " .
							     " left join users on user_id = task_user " . 
							     " where task_order = " . dbquote($project["project_order"]) . 
							     " and task_order_state = 38 " .
							     " and user_address = " . dbquote($row_s["address_id"]);

					
						$res_su = mysql_query($sql_su) or dberror($sql_su);
						$row_su = mysql_fetch_assoc($res_su);

						if ($row_su["num_recs"] == 0)
						{
						
							$user_data = array();
							$user_data["id"] = $row_s["user_id"];
							$user_data["email"] = $row_s["user_email"];
							$user_data["cc"] = $row_s["user_email_cc"];
							$user_data["deputy"] = $row_s["user_email_deputy"];
							$user_data["full_name"] = $row_s["user_full_name"];
							$user_data["address"] = $row_s["address_id"];
							$user_data["role"] = "Supplier";
							$user_data["address_type"] = $row_s["address_type"];
							$involved_suppliers[$row["user_id"]][$user_data["id"]] = $user_data;
						}
					}
				}
			}
        }

		
    }
    elseif (id() == MINI_BOOKLET_APPROVAL_BY_MANAGEMENT)
	{
		
		foreach($managers_hq as $user_id=>$user) {
			$user_data = array();
			$user_data["id"] = $user_id;
			$user_data["email"] = $user["user_email"];
			$user_data["cc"] = "";
			$user_data["deputy"] = "";
			$user_data["full_name"] = $user["user_fullname"];
			$user_data["address"] = $user["address_id"];
			$user_data["role"] = "HQ Management";
			$recipients[$user_id] = $user_data;
		}
		
		foreach($regional_sales_managers_hq as $user_id=>$user) {
			$user_data = array();
			$user_data["id"] = $user_id;
			$user_data["email"] = $user["user_email"];
			$user_data["cc"] = "";
			$user_data["deputy"] = "";
			$user_data["full_name"] = $user["user_fullname"];
			$user_data["address"] = $user["address_id"];
			$user_data["role"] = "Regional Sales Manager HQ";
			$recipients[$user_id] = $user_data;
		}
	}
	elseif (id() == CMS_APPROVE_BY_CONTROLLER and $cms_completed_by_pl == false)
	{
		//no operation

	}
	else if ($action_parameter["recipient"] == 'Project Leader')
    {
		$sql = "select user_id, address_id, ".
              "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy ".
               "from users ".
               "left join addresses on user_address = address_id ".
               "where user_email<>'' ".
               "    and user_active = 1 " .
               "    and user_id = " . dbquote($project["project_retail_coordinator"]);

        $res = mysql_query($sql) or dberror($sql);

        if ($row = mysql_fetch_assoc($res))
        {
            $user_data = array();
            $user_data["id"] = $row["user_id"];
            $user_data["email"] = $row["user_email"];
            $user_data["cc"] = $row["user_email_cc"];
            $user_data["deputy"] = $row["user_email_deputy"];
            $user_data["full_name"] = $row["user_full_name"];
            $user_data["address"] = $row["address_id"];
            $recipients[$row["user_id"]] = $user_data;
        }

		if (id() == CMS_APPROVE_BY_CONTROLLER)
		{	
			$sql = "select user_id, address_id, ".
						   "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
						   "    user_email, user_email_cc, user_email_deputy ".
						   "from users ".
						   "left join addresses on user_address = address_id ".
						   "left join user_roles on user_role_user = user_id " . 
						   "where user_email<>'' ".
						   "    and user_active = 1 " .
						   "    and user_role_role = 45 " . 
						   " and address_id = " . dbquote($project["order_client_address"]);

					$res = mysql_query($sql) or dberror($sql);

					while ($row = mysql_fetch_assoc($res))
					{
						$user_data = array();
						$user_data["id"] = $row["user_id"];
						$user_data["email"] = $row["user_email"];
						$user_data["cc"] = $row["user_email_cc"];
						$user_data["deputy"] = $row["user_email_deputy"];
						$user_data["full_name"] = $row["user_full_name"];
						$user_data["address"] = $row["address_id"];
						$user_data["role"] = 'Local Financing';
						$recipients[$row["user_id"]] = $user_data;
					}
		}
    }
    else if ($action_parameter["recipient"] == 'Design Contractor')
    {
		
		
		/*
		if(id() == MINI_BOOKLET_APPROVED_BY_MANAGEMENT)
		{
			$sql = "select user_id, address_id, ".
				   "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
				   "    user_email, user_email_cc, user_email_deputy ".
				   "from users ".
				   "left join addresses on user_address = address_id ".
				   "where user_email<>'' ".
				   "    and user_active = 1 " .
				   "    and user_id in ( " . dbquote($project["project_design_contractor"]) . ", " . dbquote($project["project_retail_coordinator"]) . ")";
		}
		else
		{
			$sql = "select user_id, address_id, ".
				   "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
				   "    user_email, user_email_cc, user_email_deputy ".
				   "from users ".
				   "left join addresses on user_address = address_id ".
				   "where user_email<>'' ".
				   "    and user_active = 1 " .
				   "    and user_id in ( " . dbquote($project["project_design_contractor"]) . ")";
		}
		*/


		$sql = "select user_id, address_id, ".
				   "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
				   "    user_email, user_email_cc, user_email_deputy ".
				   "from users ".
				   "left join addresses on user_address = address_id ".
				   "where user_email<>'' ".
				   "    and user_active = 1 " .
				   "    and user_id in ( " . dbquote($project["project_design_contractor"]) . ")";

        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {
            $user_data = array();
            $user_data["id"] = $row["user_id"];
            $user_data["email"] = $row["user_email"];
            $user_data["cc"] = $row["user_email_cc"];
            $user_data["deputy"] = $row["user_email_deputy"];
            $user_data["full_name"] = $row["user_full_name"];
            $user_data["address"] = $row["address_id"];
            $recipients[$row["user_id"]] = $user_data;
        }
		
    }
    else if ($action_parameter["recipient"] == 'Design Supervisor')
    {
		$sql = "select user_id, address_id, ".
               "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy ".
               "from users ".
               "left join addresses on user_address = address_id ".
               "where user_email<>'' ".
               "    and user_active = 1 " .
               "    and user_id = " . dbquote($project["project_design_supervisor"]);

        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {
            $user_data = array();
            $user_data["id"] = $row["user_id"];
            $user_data["email"] = $row["user_email"];
            $user_data["cc"] = $row["user_email_cc"];
            $user_data["deputy"] = $row["user_email_deputy"];
            $user_data["full_name"] = $row["user_full_name"];
            $user_data["address"] = $row["address_id"];
            $recipients[$row["user_id"]] = $user_data;
        }
    }
    else if ($action_parameter["recipient"] == 'Logistics Coordinator')
    {
        $sql = "select user_id, address_id, ".
               "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
               "    user_email, user_email_cc, user_email_deputy ".
               "from users ".
               "left join addresses on user_address = address_id ".
               "where user_email<>'' ".
               "    and user_active = 1 " .
               "    and user_id = " . dbquote($project["order_retail_operator"]);

        $res = mysql_query($sql) or dberror($sql);

        while ($row = mysql_fetch_assoc($res))
        {
            if (id() == DELIVERY_CONFIRMED)
            {
                // check if there are items in the order not having an actual arrival date
                $sql_ordered = "select order_item_id, order_item_po_number ".
                               "from order_items ".
                               "where order_item_type <= " . ITEM_TYPE_SPECIAL .
                               "    and order_item_order = " . $project["project_order"] .
                               "    and (order_item_not_in_budget is null or order_item_not_in_budget =0) " . 
					           "    and order_item_quantity > 0";

                $res1= mysql_query($sql_ordered) or dberror($sql_ordered);

                $already_arrived = 1;
                while ($row1 = mysql_fetch_assoc($res1))
                {

                    $sql_dates = "select date_id ".
                                 "from dates ".
                                 "left join date_types on date_type = date_type_id ".
                                 "where date_order_item = ". $row1["order_item_id"] .
                                 "    and date_type_code = 'ACAR'";

                    $res2= mysql_query($sql_dates) or dberror($sql_dates);
  
                    if ($row2 = mysql_fetch_assoc($res2))
                    {
                      // nothing
                    }
                    else
                    {
                      $already_arrived = 0;
                    }
                }

                if ($already_arrived == 1)
                {
                    $user_data = array();
                    $user_data["id"] = $row["user_id"];
                    $user_data["email"] = $row["user_email"];
                    $user_data["cc"] = $row["user_email_cc"];
                    $user_data["deputy"] = $row["user_email_deputy"];
                    $user_data["full_name"] = $row["user_full_name"];
                    $user_data["address"] = $row["address_id"];
                    $recipients[$row["user_id"]] = $user_data;
                }

            }
			elseif (id() == PICK_UP_CONFIRMED)
            {
                //check if there is at least on item having a pickup date and is not confirmed yeat
				$sql_pickup = "select count(order_item_id) as num_recs " .
					          " from order_items " . 
					          " where order_item_type <= " . ITEM_TYPE_SPECIAL .
					          "    and order_item_pickup_confirmed = 0 " . 
                              "    and order_item_order = " . $project["project_order"] .
                              "    and order_item_pickup is not null and order_item_pickup <> '0000-00-00' " . 
					          "    and order_item_supplier_address = " . $logged_user["address"];

				$res1= mysql_query($sql_pickup) or dberror($sql_pickup);
				$row1 = mysql_fetch_assoc($res1);
				if($row1["num_recs"] > 0)
				{
					$user_data = array();
                    $user_data["id"] = $row["user_id"];
                    $user_data["email"] = $row["user_email"];
                    $user_data["cc"] = $row["user_email_cc"];
                    $user_data["deputy"] = $row["user_email_deputy"];
                    $user_data["full_name"] = $row["user_full_name"];
                    $user_data["address"] = $row["address_id"];
                    $recipients[$row["user_id"]] = $user_data;
                }

				//check if all items have a pick up date
				$sql_pickup = "select count(order_item_id) as num_recs " .
					          " from order_items " . 
					          " where order_item_type <= " . ITEM_TYPE_SPECIAL .
                              "    and order_item_order = " . $project["project_order"] .
                              "    and (order_item_pickup is null or order_item_pickup = '0000-00-00') " . 
					          "    and order_item_supplier_address = " . $logged_user["address"];

                $res1= mysql_query($sql_pickup) or dberror($sql_pickup);
                $row1 = mysql_fetch_assoc($res1);
                if($row1["num_recs"] == 0)
				{
					$delete_task_confirm_pickup = true;
				}
            }
            else
            {
                $user_data = array();
                $user_data["id"] = $row["user_id"];
                $user_data["email"] = $row["user_email"];
                $user_data["cc"] = $row["user_email_cc"];
                $user_data["deputy"] = $row["user_email_deputy"];
                $user_data["full_name"] = $row["user_full_name"];
                $user_data["address"] = $row["address_id"];
                $recipients[$row["user_id"]] = $user_data;
            }
        }
		
		//check if there is a task for the performing user that tells us to keep the order state
		$sql_t = "select task_keep_order_state " . 
			     "from tasks " . 
			     "where task_order = " . $project["project_order"] .
			     " and task_user = " . user_id() . 
			     " order by task_id";
		$res_t = mysql_query($sql_t) or dberror($sql_t);
		if ($row_t = mysql_fetch_assoc($res_t))
		{
			$_keep_project_state = $row_t["task_keep_order_state"];
		}
    }
	else if (id() == CMS_COMPLETTION_DEVELOPMENT)
	{
		/*
		if($project["project_cost_type"] == 1 and 
			$cms_completed_by_lc == true 
			and $cms_completed_by_client == true)
		{
		*/

		if($cms_completed_by_lc == true 
			and $cms_completed_by_client == true)
		{
			$sql = "select user_id, address_id, ".
				   "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
				   "    user_email, user_email_cc, user_email_deputy ".
				   "from users ".
				   "left join addresses on user_address = address_id ".
				   "left join user_roles on user_role_user = user_id " . 
				   "where user_email<>'' ".
				   "    and user_active = 1 " .
				   "    and user_role_role = 78 ";

			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{
				$user_data = array();
				$user_data["id"] = $row["user_id"];
				$user_data["email"] = $row["user_email"];
				$user_data["cc"] = $row["user_email_cc"];
				$user_data["deputy"] = $row["user_email_deputy"];
				$user_data["full_name"] = $row["user_full_name"];
				$user_data["address"] = $row["address_id"];
				$recipients[$row["user_id"]] = $user_data;
			}
		}
		else
		{
			$project_can_be_terminated = true;
		}
	}
	else // all addresses in the project
    {
		//ceck use roles
		$user_roles = get_user_roles(user_id());
		//var_dump($user_roles);
		$selectabel_email_recipients_are_restricted = false;
		if(in_array(1, $user_roles)
			or in_array(2, $user_roles)
			or in_array(3, $user_roles)
			or in_array(8, $user_roles)
			or in_array(13, $user_roles))
		{
			//no restircion for
			//System Administrator
			//Logistics Coordinator
			//Project Leader
			//Supervisor
			//Contract Supervisor
		}
		else
		{
			$selectabel_email_recipients_are_restricted = true; 
		}

		if($selectabel_email_recipients_are_restricted == true)
		{
			// only allow retail coordinator & retail operator as recipients
			$sql = "select user_id, address_id, ".
				   "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
				   "    user_email, user_email_cc, user_email_deputy ".
				   "from users ".
				   "left join addresses on user_address = address_id ".
				   "where address_id != 2390 and user_email<>'' ".
				   "    and user_active = 1 " .
				   "    and (user_id = " . dbquote($project["project_retail_coordinator"]) . 
				   "    or user_id = " . dbquote($project["order_retail_operator"]) . ")";

			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{
				if($row["user_id"] > 0) {
					$user_data = array();
					$user_data["id"] = $row["user_id"];
					$user_data["email"] = $row["user_email"];
					$user_data["cc"] = $row["user_email_cc"];
					$user_data["deputy"] = $row["user_email_deputy"];
					$user_data["full_name"] = $row["user_full_name"];
					$user_data["address"] = $row["address_id"];
					$recipients[$row["user_id"]] = $user_data;
				}
			}
		}
		else
		{
			// client, project leader, logistics coordinator, supervisor, design contractor
			$sql = "select user_id, address_id, ".
				   "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
				   "    user_email, user_email_cc, user_email_deputy ".
				   "from users ".
				   "left join addresses on user_address = address_id ".
				   "where user_email <>'' ".
				   "    and user_active = 1 " .
				   "    and (user_id = " . dbquote($project["order_user"]) .
				   "    or user_id = " . dbquote($project["project_retail_coordinator"]) . 
				   "    or user_id = " . dbquote($project["order_retail_operator"]) . 
				   "    or user_id = " . dbquote($project["project_design_supervisor"]) .
				   "    or user_id = " . dbquote($project["project_design_contractor"]) . ")";

			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{
				if($row["user_id"] > 0) {
					$user_data = array();
					$user_data["id"] = $row["user_id"];
					$user_data["email"] = $row["user_email"];
					$user_data["cc"] = $row["user_email_cc"];
					$user_data["deputy"] = $row["user_email_deputy"];
					$user_data["full_name"] = $row["user_full_name"];
					$user_data["address"] = $row["address_id"];
					$recipients[$row["user_id"]] = $user_data;
				}
			}

			
			// supplier
			$sql = "select distinct user_id, address_id, ".
				   "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
				   "    user_email, user_email_cc, user_email_deputy ".
				   "from order_items ".
				   "left join addresses on order_item_supplier_address = address_id ".
				   "left join users on user_address = address_id ".
				   "where address_id != 2390 and user_email<>'' ".
				   "    and user_active = 1 " .
				   "    and order_item_order = " . $project["project_order"] . " " .
				   "    and (order_item_not_in_budget is null or order_item_not_in_budget =0)" .
				   "    and order_item_quantity > 0 " . 
				   " order by address_company";

			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{
				$user_data = array();
				$user_data["id"] = $row["user_id"];
				$user_data["email"] = $row["user_email"];
				$user_data["cc"] = $row["user_email_cc"];
				$user_data["deputy"] = $row["user_email_deputy"];
				$user_data["full_name"] = $row["user_full_name"];
				$user_data["address"] = $row["address_id"];
				$user_data["role"] = "Supplier";
				$recipients[$row["user_id"]] = $user_data;
			}

			// forwarder
			$sql = "select distinct user_id, address_id, ".
				   "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
				   "    user_email, user_email_cc, user_email_deputy ".
				   "from order_items ".
				   "left join addresses on order_item_forwarder_address = address_id ".
				   "left join users on user_address = address_id ".
				   "where address_id != 2390 and user_email<>'' ".
				   "    and user_active = 1 " .
				   "    and order_item_order = " . $project["project_order"] . " " .
				   "    and (order_item_not_in_budget is null or order_item_not_in_budget =0) " .
				   "    and order_item_quantity > 0 " . 
				   " order by address_company";

			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{
				$user_data = array();
				$user_data["id"] = $row["user_id"];
				$user_data["email"] = $row["user_email"];
				$user_data["cc"] = $row["user_email_cc"];
				$user_data["deputy"] = $row["user_email_deputy"];
				$user_data["full_name"] = $row["user_full_name"];
				$user_data["address"] = $row["address_id"];
				$user_data["role"] = "Forwarder";
				$recipients[$row["user_id"]] = $user_data;
			}

			// special users
			$sql = "select distinct user_id, address_id, ".
				   "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
				   "    user_email, user_email_cc, user_email_deputy ".
				   "from orders ".
				   "left join addresses on order_client_address = address_id ".
				   "left join users on user_address = address_id ".
				   "where user_email<>'' ".
				   "    and user_active = 1 and user_project_reciepient = 1 " .
				   " and user_address = address_id " .
				   " and order_id = " . $project["project_order"] .
				   " order by address_company";

			$res = mysql_query($sql) or dberror($sql);

			while ($row = mysql_fetch_assoc($res))
			{
				$user_data = array();
				$user_data["id"] = $row["user_id"];
				$user_data["email"] = $row["user_email"];
				$user_data["cc"] = $row["user_email_cc"];
				$user_data["deputy"] = $row["user_email_deputy"];
				$user_data["full_name"] = $row["user_full_name"];
				$user_data["address"] = $row["address_id"];
				$user_data["role"] = "Additional Recipient";
				$recipients[$row["user_id"]] = $user_data;
			}
		}
        
    }


	/********************************************************************
        validation of different states depending on the step perfomed
		if conditions are not true then delete all recipients
    *********************************************************************/
    if (id() == OFFER_SUBMITTED)
    {
        //check if all budget positions have a valid price in case a sn offer is submitted
		$all_offered = check_if_all_items_have_prices($project["project_order"], $logged_user["address"]);
        if ($all_offered == 0)
        {
            $recipients = array();
        }
    }
    elseif (id() == REQUEST_FOR_BUDGET_APPROVAL_SUBMITTED
		        or id() == REQUEST_FOR_BUDGET_APPROVAL_SUBMITTED_HQ
				or id() == REQUEST_FOR_FULL_BUDGET_APPROVAL_SUBMITTED)
    {
		// check if all budget positions have been accepted and have a valid price
		
		$all_offered = check_if_all_items_are_offered($project["project_order"]);
        if ($all_offered == 0)
        {
			$recipients = array();
        }
		
    }
    elseif (id() == CONFIRM_ORDER_BY_SUPPLIER)
    {
        // check if all budget positions have have a pick up date
		$pickupdates_entered = check_if_all_items_hav_pick_up_date($project["project_order"], $logged_user["address"]);
        if ($pickupdates_entered == 0)
        {
            $recipients = array();
        }
    }
    elseif (id() == DELIVERY_CONFIRMED_FRW)
    {
        // check if all budget positions have have an arrival date
		$arrival_entered = check_if_all_items_hav_arrival_date($project["project_order"], $logged_user["address"]);
        if ($arrival_entered == 0)
        {
            $recipients = array();
        }
    }
    elseif (id() == REQUEST_FOR_DELIVERY_ACCEPTED)
    {
		// check if all positions have an expected date of arrival
		$expected_arrival_entered = check_if_all_items_hav_expected_arrival_date($project["project_order"], $logged_user["address"]);
        if ($expected_arrival_entered == 0)
        {
            $recipients = array();
        }
    }


    /********************************************************************
        get the text and subject for the email
    *********************************************************************/
    //subject
    $sql = "select order_shop_address_company, " .
           "order_shop_address_place, country_name " .
           "from projects " .
           "left join orders on order_id = project_order " .
           "left join countries on country_id = order_shop_address_country " .
           "where project_id = " . $project["project_id"];

    $res = mysql_query($sql) or dberror($sql);
    $row = mysql_fetch_assoc($res);
    $subject = $action_parameter["name"] . " (Step " . id() . ") - Project " . $project["order_number"] . ", " . $row["country_name"]  . ", " . $row["order_shop_address_company"];

	$subject_suppliers = "Project " . $project["order_number"] . ", " . $row["country_name"]  . ", " . $row["order_shop_address_company"];

	

	/********************************************************************
        get standard texts for works
    *********************************************************************/
	$show_standardtexts = 0;
	$sql_taskworks = "select count(taskwork_id) as num_recs " .
		             "from taskworks " . 
		             "left join order_states on order_state_id = taskwork_order_state " . 
		             "where taskwork_productline = " . $project["project_product_line"] . 
		             " and taskwork_postype = " . $project["project_postype"] . 
		             " and order_state_code = " . id();


	$res_t = mysql_query($sql_taskworks) or dberror($sql_taskworks);
    $row_t = mysql_fetch_assoc($res_t);
	
	if($row_t["num_recs"] > 0)
	{
		$show_standardtexts = 1;
	}


	$addresses = array();
	foreach($recipients as $key=>$recipient)
	{
		$addresses[] = $recipient["address"];
	}

	$filter_taskworks = "";
	$sql_taskworks = "select taskwork_id, taskwork_shortcut, taskwork_address " .
		             "from taskworks " . 
		             "left join order_states on order_state_id = taskwork_order_state " . 
		             "where taskwork_productline = " . $project["project_product_line"] . 
		             " and taskwork_postype = " . $project["project_postype"] . 
		             " and order_state_code = " . id();

	
	$res_t = mysql_query($sql_taskworks) or dberror($sql_taskworks);
    while($row_t = mysql_fetch_assoc($res_t))
	{
		if($row_t["taskwork_address"] > 0)
		{
			foreach($addresses as $key=>$address)
			{
				if($row_t["taskwork_address"] == $address)
				{
					$filter_taskworks = " and (taskwork_address = " . $recipient["address"] . " or  taskwork_address is Null) ";
					$sql_taskworks .= $filter_taskworks;
				}
			}
		}
	}

	if($filter_taskworks == "")
	{
		$filter_taskworks = " and taskwork_address is Null ";
		$sql_taskworks .= $filter_taskworks;
	}

	
	/********************************************************************
        build form
    *********************************************************************/
    $form = new Form("projects", "project");

	$form->add_hidden("pid", param("pid"));
    $form->add_hidden("order_id", $project["project_order"]);

    require_once "include/project_head_small.php";
	

	
	/*
	if(id() == MINI_BOOKLET_APPROVED_BY_MANAGEMENT)
	{
		$form->add_section("Sales Surface According to Approved Layout");
		$form->add_edit("store_retailarea", "Sales Surface in sqms according to layout*", NOTNULL, 0, TYPE_DECIMAL, 10,2);

		
		$design_objectives_listbox_names = array();
		$design_objectives_checklist_names = array();
		
		if($project["date_created"] < '2010-12-05')
		{
			// count design_objective_groups old version when design objectives were depending on the product line
			$sql_design_objective_group = "select design_objective_group_id, design_objective_group_name, " .
										  "    design_objective_group_multiple " .
										  "from design_objective_groups " .
										  "where design_objective_group_product_line=  " . $project["project_product_line"] . " " .
										  "order by design_objective_group_priority";

			$res = mysql_query($sql_design_objective_group) or dberror($sql_design_objective_group);
			$number_of_design_objective_groups = mysql_num_rows($res);
		}
		else
		{
			// count design_objective_groups new version, design objectives depending on the postype
			$sql_design_objective_group = "select design_objective_group_id, design_objective_group_name, " .
										  "    design_objective_group_multiple " .
										  "from design_objective_groups " .
										  "where design_objective_group_postype =  " . $project["project_postype"] . " " .
										  "order by design_objective_group_priority";

			$res = mysql_query($sql_design_objective_group) or dberror($sql_design_objective_group);
			$number_of_design_objective_groups = mysql_num_rows($res);
		}
		
		if ($number_of_design_objective_groups > 0)
		{
			$form->add_section("Design Objectives");
			$form->add_comment("Please indicate your basic needs and requirements to assist out design process. ".
			"\n A Project Leader will ensure that the design is consistent with the ".
			"Tissot Retail objectives."); 

			
			// read design_item_ids from project items
			$project_design_objective_item_ids = get_project_design_objective_item_ids(param("pid"));
			
			$i = 1;
			
			while ($row = mysql_fetch_assoc($res))
			{
				$sql_design_objective_item = "select design_objective_item_id, design_objective_item_name ".
											 "from design_objective_items ".
											 "where design_objective_item_group=" .    $row["design_objective_group_id"] . " ".
											 "order by design_objective_item_priority";

				if ($row["design_objective_group_multiple"] == 0) 
				{
					$value = "";
					foreach ($project_design_objective_item_ids as $key=>$element)
					{
					  if ($element == $row["design_objective_group_name"])
					  {
						  $value = $key;
					  }
					}

					$form->add_list("design_objective_items" . $i, $row["design_objective_group_name"] . "*", $sql_design_objective_item, NOTNULL, $value);
					$design_objectives_listbox_names[] = "design_objective_items" . $i;
				}
				else 
				{
					$values = array();
					foreach ($project_design_objective_item_ids as $key=>$element)
					{
						if ($element == $row["design_objective_group_name"])
						{
							$values[] = $key;
						}
						next($project_design_objective_item_ids);
					}
					$form->add_checklist("design_objective_items" . $i, $row["design_objective_group_name"] . "*", "project_items", $sql_design_objective_item, NOTNULL, $values);
					$design_objectives_checklist_names[] = "design_objective_items" . $i;
				}
				
				$i++;   
			}
		}
	}
	*/

	usort($recipients, function($a, $b) {
		
		if ($a['full_name'] == $b['full_name']) {
		  return 0;
		}
		return ($a['full_name'] < $b['full_name']) ? -1 : 1;
		

	});


    $form->add_section("Email and Task Entry");
    $form->add_label("action_name", "Action to be Performed", 0, $action_parameter["action_name"]);
    $form->add_label("sender_name", "Sender", 0, $logged_user["contact"]);
    $form->add_hidden("sender_email",  $logged_user["email"]);
    $form->add_label("dummy1", "", 0, "");

    if($project_can_be_terminated == false)
	{
		$form->add_edit("subject", "Subject*", NOTNULL, $subject, TYPE_CHAR);

		if($show_standardtexts == 1)
		{
			$form->add_list("standard_text", "Choose Standard Option", $sql_taskworks, SUBMIT);
			$form->add_multiline("body_text", "Mail Message*", 6, NOTNULL);

		}
		else
		{
			$form->add_multiline("body_text", "Mail Message*", 6, NOTNULL, $message_text);
		}
		$form->add_edit("due_date", "Preferred Due Date", 0, "", TYPE_DATE, 10);

	
		// create recipient section depending on the action selected
		$form->add_comment("Select the recipients of your message from the following list.");

		foreach ($recipients as $key=>$recipient)
		{
			//get user_role
			$user_role = "";
			if($recipient["id"] == $project["project_retail_coordinator"]) {$user_role = "Project Leader";}
			elseif($recipient["id"] == $project["project_local_retail_coordinator"]) {$user_role = "Local Project Leader";}
			elseif($recipient["id"] == $project["project_design_supervisor"]) {$user_role = "Design Supervisor";}
			elseif($recipient["id"] == $project["project_design_contractor"]) {$user_role = "Design Contractor";}
			elseif($recipient["id"] == $project["order_retail_operator"]) {$user_role = "Logistics Coordinator";}
			elseif($recipient["id"] == $project["order_user"]) {$user_role = "Project Owner";}
			elseif(array_key_exists('role', $user_data)){$user_role = $recipient["role"];}
			
			if(id() == SUBMIT_DESIGN_BRIEFING) {
				$user_role = "Design Supervisor";
			}
			
			$form->add_checkbox("R" . $recipient["id"], $recipient["full_name"], "", 0, $user_role);
		}

		//add people from procrement
		if (id() == SUBMIT_REQUEST_FOR_OFFER)
		{
			usort($procurement, function($a, $b) {
		
				if ($a['full_name'] == $b['full_name']) {
				  return 0;
				}
				return ($a['full_name'] < $b['full_name']) ? -1 : 1;
				

			});
			
			foreach ($procurement as $key=>$recipient)
			{
				$form->add_checkbox("P" . $recipient["id"], $recipient["full_name"], "", 0, $recipient["role"]);
			}
		}

		
		
		//add logged user to section "send a copy to"
		$show_copy_to = 1;
		foreach ($recipients as $key=>$recipient)
		{
			if($recipient["email"] == $logged_user["email"])
			{
				$show_copy_to = 0;
			}
		}
		if($show_copy_to == 1)
		{
			//ad logged user
			$copy_to_name = $logged_user["firstname"] . " " . $logged_user["name"];
			$form->add_comment("Send a Copy of the Email to the Following Recepient");
			$form->add_checkbox("copy_to", $copy_to_name);
		}

		//add order_state email copy to "send a copy to"
		$show_copy2_to = 1;
		if(isset($action_parameter["order_state_email_copy_to_email"]) 
				 and $action_parameter["order_state_email_copy_to_email"])
		{
			foreach ($recipients as $key=>$recipient)
			{
				if($recipient["email"] == $action_parameter["order_state_email_copy_to_email"])
				{
					$show_copy2_to = 0;
				}
			}
			
			if($show_copy2_to = 1 and $show_copy_to != 1)
			{
				$form->add_comment("Send a Copy of the Email to the Following Recepient");
			}
			if($show_copy2_to = 1)
			{
				$form->add_checkbox("order_state_email_copy_to", $action_parameter["order_state_email_copy_to"], 1, 0);
			}
			else
			{
				$form->add_hidden("order_state_email_copy_to",0);
			}
		}
		else
		{
			$form->add_hidden("order_state_email_copy_to",0);
		}
	}
	else
	{
		$form->add_hidden("subject", $subject);
		$form->add_hidden("body_text", $message_text);
		$form->add_hidden("due_date");
		$form->add_hidden("order_state_email_copy_to",0);

	
		
	}


	//send copy to regional responsibles
	//add regional responsible as additional recipients in case of files uploaded
	if($action_parameter["order_state_regional_responsible_as_cc"] == 1)
	{
		foreach($regional_responsable_reciepients as $user_id=>$user) {
			$user_data = array();
			$user_data["id"] = $user_id;
			$user_data["email"] = $email;
			$user_data["cc"] = "";
			$user_data["deputy"] = "";
			$user_data["full_name"] = $user["user_fullname"];
			$user_data["address"] = $user["address_id"];
			$user_data["role"] = $user["role_name"];
			$cc_recipients[$user_id] = $user_data;

			$form->add_checkbox("nr" . $user_id , $user["user_fullname"], 1, 0, $user["role_name"]);
		}
	}

	

	//send copy to logistics coordinator on budget approval
	/*
	if (id() == BUDGET_APPROVED
		or id() == BUDGET_APPROVED_HQ
		or id() == FULL_BUDGET_APPROVED
		or id() == LAYOUT_ACCEPTED)
    {
		$sql = "select user_id, address_id, ".
			   "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
			   "    user_email, user_email_cc, user_email_deputy ".
			   "from users ".
			   "left join addresses on user_address = address_id ".
			   "where address_id != 2390 and user_email<>'' ".
			   "    and user_active = 1 " .
			   "    and user_id = " . dbquote($project["order_retail_operator"]);

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$user_data = array();
			$user_data["id"] = $row["user_id"];
			$user_data["email"] = $row["user_email"];
			$user_data["cc"] = $row["user_email_cc"];
			$user_data["deputy"] = $row["user_email_deputy"];
			$user_data["full_name"] = $row["user_full_name"];
			$user_data["address"] = $row["address_id"];
			$cc_recipients[$row["user_id"]] = $user_data;
			
			$form->add_checkbox("copy_to_logistics_coordinator", $row["user_full_name"], 1, 0);
		}
	}
	*/

	//send copy to design supervisor
	/*
	if(id() == SUBMIT_MINI_BOOKLET_FOR_APPROVAL)
	{
		//ad design supervisor as CC
		$sql = "select user_id, address_id, ".
			   "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
			   "    user_email, user_email_cc, user_email_deputy ".
			   "from users ".
			   "left join addresses on user_address = address_id ".
			   "where address_id != 2390 and user_email<>'' ".
			   "    and user_active = 1 " .
			   "    and user_id = " . dbquote($project["project_design_supervisor"]);

		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$_tmp = false;
			foreach($recipients as $key=>$data)
			{
				if( $data['id'] == $row["user_id"]) {
					$_tmp = true;
				}
			}
			if($_tmp == false) {
				$user_data = array();
				$user_data["id"] = $row["user_id"];
				$user_data["email"] = $row["user_email"];
				$user_data["cc"] = $row["user_email_cc"];
				$user_data["deputy"] = $row["user_email_deputy"];
				$user_data["full_name"] = $row["user_full_name"];
				$user_data["address"] = $row["address_id"];
				$cc_recipients[$row["user_id"]] = $user_data;
				$form->add_checkbox("copy_to_design_supervisor", $row["user_full_name"], 1, 0);
			}
		}
	}
	*/

	//copies to notification recipients
	if (id() == LAYOUT_PREVIEW_ACCEPTED)
    {
		if(count($notification_recipients4) > 0)
		{
			foreach($notification_recipients4 as $key=>$email)
			{
				$sql = "select user_id, address_id, ".
					   "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
					   "    user_email, user_email_cc, user_email_deputy ".
					   "from users ".
					   "left join addresses on user_address = address_id ".
					   "where user_email <> '' ".
					   "    and user_active = 1 " .
					   "    and user_email = " . dbquote($email);

				$res = mysql_query($sql) or dberror($sql);

				if ($row = mysql_fetch_assoc($res))
				{
					if(!array_key_exists($row["user_id"], $cc_recipients))
					{
						$user_data = array();
						$user_data["id"] = $row["user_id"];
						$user_data["email"] = $row["user_email"];
						$user_data["cc"] = $row["user_email_cc"];
						$user_data["deputy"] = $row["user_email_deputy"];
						$user_data["full_name"] = $row["user_full_name"];
						$user_data["address"] = $row["address_id"];
						$cc_recipients[$row["user_id"]] = $user_data;
						$form->add_checkbox("nr" . $row["user_id"] , $row["user_full_name"], 1, 0);
					}
				}
			}
		}
	}
	elseif (id() == LAYOUT_ACCEPTED)
	{
		if(count($notification_recipients3) > 0)
		{
			foreach($notification_recipients3 as $key=>$email)
			{
				$sql = "select user_id, address_id, ".
					   "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
					   "    user_email, user_email_cc, user_email_deputy ".
					   "from users ".
					   "left join addresses on user_address = address_id ".
					   "where user_email <> '' ".
					   "    and user_active = 1 " .
					   "    and user_email = " . dbquote($email);

				$res = mysql_query($sql) or dberror($sql);

				if ($row = mysql_fetch_assoc($res))
				{
					if(!array_key_exists($row["user_id"], $cc_recipients))
					{
						$user_data = array();
						$user_data["id"] = $row["user_id"];
						$user_data["email"] = $row["user_email"];
						$user_data["cc"] = $row["user_email_cc"];
						$user_data["deputy"] = $row["user_email_deputy"];
						$user_data["full_name"] = $row["user_full_name"];
						$user_data["address"] = $row["address_id"];
						$cc_recipients[$row["user_id"]] = $user_data;
						$form->add_checkbox("nr" . $row["user_id"], $row["user_full_name"], 1, 0);
					}
				}
			}
		}

		
	}
	elseif (id() == REQUEST_FOR_BUDGET_APPROVAL_SUBMITTED
		    or id() == REQUEST_FOR_BUDGET_APPROVAL_SUBMITTED_HQ)
	{
		if(count($notification_recipients1) > 0)
		{
			foreach($notification_recipients1 as $key=>$email)
			{
				$sql = "select user_id, address_id, ".
					   "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
					   "    user_email, user_email_cc, user_email_deputy ".
					   "from users ".
					   "left join addresses on user_address = address_id ".
					   "where user_email <> '' ".
					   "    and user_active = 1 " .
					   "    and user_email = " . dbquote($email);

				$res = mysql_query($sql) or dberror($sql);

				if ($row = mysql_fetch_assoc($res))
				{
					if(!array_key_exists($row["user_id"], $cc_recipients))
					{
						$user_data = array();
						$user_data["id"] = $row["user_id"];
						$user_data["email"] = $row["user_email"];
						$user_data["cc"] = $row["user_email_cc"];
						$user_data["deputy"] = $row["user_email_deputy"];
						$user_data["full_name"] = $row["user_full_name"];
						$user_data["address"] = $row["address_id"];
						$cc_recipients[$row["user_id"]] = $user_data;
						$form->add_checkbox("nr" . $row["user_id"], $row["user_full_name"], 1, 0);
					}
				}
			}
		}
	}
	elseif (id() == REQUEST_FOR_FULL_BUDGET_APPROVAL_SUBMITTED)
	{
		if(count($notification_recipients5) > 0)
		{
			foreach($notification_recipients5 as $key=>$email)
			{
				$sql = "select user_id, address_id, ".
					   "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
					   "    user_email, user_email_cc, user_email_deputy ".
					   "from users ".
					   "left join addresses on user_address = address_id ".
					   "where user_email <> '' ".
					   "    and user_active = 1 " .
					   "    and user_email = " . dbquote($email);

				$res = mysql_query($sql) or dberror($sql);

				if ($row = mysql_fetch_assoc($res))
				{
					if(!array_key_exists($row["user_id"], $cc_recipients))
					{
						$user_data = array();
						$user_data["id"] = $row["user_id"];
						$user_data["email"] = $row["user_email"];
						$user_data["cc"] = $row["user_email_cc"];
						$user_data["deputy"] = $row["user_email_deputy"];
						$user_data["full_name"] = $row["user_full_name"];
						$user_data["address"] = $row["address_id"];
						$cc_recipients[$row["user_id"]] = $user_data;
						$form->add_checkbox("nr" . $row["user_id"], $row["user_full_name"], 5, 0);
					}
				}
			}
		}
	}
	elseif (id() == BUDGET_APPROVED
		or id() == BUDGET_APPROVED_HQ)
    {
		if(count($notification_recipients2) > 0)
		{
			foreach($notification_recipients2 as $key=>$email)
			{
				$sql = "select user_id, address_id, ".
					   "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
					   "    user_email, user_email_cc, user_email_deputy ".
					   "from users ".
					   "left join addresses on user_address = address_id ".
					   "where user_email <> '' ".
					   "    and user_active = 1 " .
					   "    and user_email = " . dbquote($email);

				$res = mysql_query($sql) or dberror($sql);

				if ($row = mysql_fetch_assoc($res))
				{
					if(!array_key_exists($row["user_id"], $cc_recipients))
					{
						$user_data = array();
						$user_data["id"] = $row["user_id"];
						$user_data["email"] = $row["user_email"];
						$user_data["cc"] = $row["user_email_cc"];
						$user_data["deputy"] = $row["user_email_deputy"];
						$user_data["full_name"] = $row["user_full_name"];
						$user_data["address"] = $row["address_id"];
						$cc_recipients[$row["user_id"]] = $user_data;
						$form->add_checkbox("nr" . $row["user_id"], $row["user_full_name"], 2, 0);
					}
				}
			}
		}

		//remove all empty budget positions

	}
	elseif (id() == FULL_BUDGET_APPROVED)
    {
		if(count($notification_recipients6) > 0)
		{
			foreach($notification_recipients6 as $key=>$email)
			{
				$sql = "select user_id, address_id, ".
					   "    concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name, ".
					   "    user_email, user_email_cc, user_email_deputy ".
					   "from users ".
					   "left join addresses on user_address = address_id ".
					   "where user_email <> '' ".
					   "    and user_active = 1 " .
					   "    and user_email = " . dbquote($email);

				$res = mysql_query($sql) or dberror($sql);

				if ($row = mysql_fetch_assoc($res))
				{
					if(!array_key_exists($row["user_id"], $cc_recipients))
					{
						$user_data = array();
						$user_data["id"] = $row["user_id"];
						$user_data["email"] = $row["user_email"];
						$user_data["cc"] = $row["user_email_cc"];
						$user_data["deputy"] = $row["user_email_deputy"];
						$user_data["full_name"] = $row["user_full_name"];
						$user_data["address"] = $row["address_id"];
						$cc_recipients[$row["user_id"]] = $user_data;
						$form->add_checkbox("nr" . $row["user_id"], $row["user_full_name"], 6, 0);
					}
				}
			}
		}
	}


	//add attachment section to the form
	if($project_can_be_terminated == true)
	{
		$form->add_hidden("keep_project_state", $_keep_project_state);
	}
	elseif (count($recipients)>0)
    {

		//make it possible to perform an action without changing the project state
		if($action_parameter["recipient"] == 'Supplier' 
			or $action_parameter["recipient"] == 'Forwarder')
		{
			$form->add_section("Delivery Request out of Sequence");
			$form->add_comment("Only use this option in case of a special request where the project's state should not be changed!");
			
			if($_keep_project_state == true) {
				$form->add_checkbox("keep_project_state", "keep project's state", 1, 0, "Special Delivery Request");
			}
			else {
				$form->add_checkbox("keep_project_state", "keep project's state", 0, 0, "Special Delivery Request");
			}
		}
		else
		{
			if($_keep_project_state == true) {
				$form->add_hidden("keep_project_state", 1);
			}
			else {
				$form->add_hidden("keep_project_state", $_keep_project_state);
			}
		}
		
		
		//check if order step allow to select from existing attachments
		if($action_parameter["order_state_file_category_id_to_select_from"] > 0)
		{
			$files_are_to_be_uploaded = false;
			foreach($recipients as $key=>$data)
			{
				$attachment_recipients[$key] = array("id"=>$data["id"], "address"=>$data["address"]);
			}

			if(count($attachment_recipients) > 0)
			{
				$form->add_section("Project Attachments");
				$form->add_comment("You can grant access to the selected recipients for one or more of the follwoing files.");

				$attach_file_to_mails = 0;
				$sql_attachment = "select distinct order_file_id, ".
								  "    order_file_title, order_file_description, ".
								  "    order_file_path, file_type_name, ".
								  "    order_files.date_created, ".
								  "    order_file_category_name, order_file_category_priority, ".
								  "    concat(user_name, ' ', user_firstname) as owner_fullname, ".
								  "    order_files.date_created as datecreated, order_file_category_attach_in_mails " . 
								  "from order_files  " .
								  "left join order_file_categories on order_file_category_id = order_file_category ".
								  "left join users on user_id = order_file_owner ".
								  "left join file_types on order_file_type = file_type_id " . 
								  " where order_file_order = " . $project["order_id"] .
								  " and order_file_category_id = " . $action_parameter["order_state_file_category_id_to_select_from"] .  
								  " order by order_file_category_priority, date_created DESC";


				$res = mysql_query($sql_attachment) or dberror($sql_attachment);
				while ($row = mysql_fetch_assoc($res)) {
					
					$link = '<a href="http://' . $_SERVER["HTTP_HOST"] . '/include/openfile.php?id=' . $row["order_file_id"] . '"  target = "_blank">' . $row["order_file_title"] . '</a> (' .  $row["file_type_name"] . ' - ' . $row["datecreated"] . ')' ;
					
					$form->add_checkbox("attachment_" . $row["order_file_id"], $link, 0, RENDER_HTML,  $row["order_file_category_name"]);

					$attachments[] = $row["order_file_id"];
					$attach_file_to_mails = $row["order_file_category_attach_in_mails"];
				}

				$form->add_hidden("order_file_category_attach_in_mails",$attach_file_to_mails);
			}
		}
		
		//check if order state has a file category to upload files
		$sql_os = "select order_state_file_category_id, order_file_category_attach_in_mails " . 
			      "from order_states " .
			      " left join order_state_groups on order_state_group_id = order_state_group " . 
				  " left join order_file_categories on order_file_category_id = order_state_file_category_id " .
				  " where order_state_group_order_type = 1 " . 
			      " and order_state_code = " . dbquote(id());
		$res_os = mysql_query($sql_os) or dberror($sql_os);
		if ($row_os = mysql_fetch_assoc($res_os)) {
			if($row_os["order_state_file_category_id"] > 0)
			{
				$files_are_to_be_uploaded = true;
				$order_number = $project["project_number"];
				
				$form->add_section("Attachments");
				$form->add_comment("Please add all files in conncetion with your task.");
				$form->add_hidden("order_file_category_attach_in_mails",$row_os["order_file_category_attach_in_mails"]);	
				$form->add_hidden("file_category", $row_os["order_state_file_category_id"]);
				//file 1
				$form->add_edit("attachment_title_1", "Title File 1", 0, "", TYPE_CHAR);
				
				
				/*
				if(id() == MINI_BOOKLET_APPROVED_BY_MANAGEMENT)
				{
					$form->add_checkbox("order_file_attach_to_cer_booklet1", "Attach this file to the AF/CER Booklet", "", 0, "AF/CER Booklte");
				}
				else
				{
					$form->add_hidden("order_file_attach_to_cer_booklet1", 0);
				}
				*/

				$form->add_hidden("order_file_attach_to_cer_booklet1", 0);
			

				$form->add_upload("order_file_path1", "File 1", "/files/orders/$order_number", 0);

				//files 2
				$form->add_edit("attachment_title_2", "Title File 2", 0, "", TYPE_CHAR);
				
				/*
				if(id() == MINI_BOOKLET_APPROVED_BY_MANAGEMENT)
				{
					$form->add_checkbox("order_file_attach_to_cer_booklet2", "Attach this file to the AF/CER Booklet", "", 0, "AF/CER Booklte");
				}
				else
				{
					$form->add_hidden("order_file_attach_to_cer_booklet2", 0);
				}
				*/
				$form->add_hidden("order_file_attach_to_cer_booklet2", 0);

				$form->add_upload("order_file_path2", "File 2", "/files/orders/$order_number", 0);

				//file 3
				$form->add_edit("attachment_title_3", "Title File 3", 0, "", TYPE_CHAR);
				/*
				if(id() == MINI_BOOKLET_APPROVED_BY_MANAGEMENT)
				{
					$form->add_checkbox("order_file_attach_to_cer_booklet3", "Attach this file to the AF/CER Booklet", "", 0, "AF/CER Booklte");
				}
				else
				{
					$form->add_hidden("order_file_attach_to_cer_booklet3", 0);
				}
				*/
				$form->add_hidden("order_file_attach_to_cer_booklet3", 0);

				$form->add_upload("order_file_path3", "File 3", "/files/orders/$order_number", 0);

				//file4
				$form->add_edit("attachment_title_4", "Title File 4", 0, "", TYPE_CHAR);
				/*
				if(id() == MINI_BOOKLET_APPROVED_BY_MANAGEMENT)
				{
					$form->add_checkbox("order_file_attach_to_cer_booklet4", "Attach this file to the AF/CER Booklet", "", 0, "AF/CER Booklte");
				}
				else
				{
					$form->add_hidden("order_file_attach_to_cer_booklet4", 0);
				}
				*/
				$form->add_hidden("order_file_attach_to_cer_booklet4", 0);

				$form->add_upload("order_file_path4", "File 4", "/files/orders/$order_number", 0);
			}
		}
    }

	if(id() == EMAIL_SENT 
		and isset($selectabel_email_recipients_are_restricted) 
		and $selectabel_email_recipients_are_restricted == false)
	{
		$form->add_section("CC Recipients");
		$form->add_modal_selector("ccmails", "Selected Recipients", 8);
	}
	else
	{
		$form->add_hidden("ccmails");
	}

    
	

	//add "send" button in case there are recipeints, otherwise show error message
    if($budget_actions_hq_supplied_items_possible == false 
			and (id() == REQUEST_FOR_BUDGET_APPROVAL_SUBMITTED
		    or id() == REQUEST_FOR_BUDGET_APPROVAL_SUBMITTED_HQ
			or id() == REQUEST_FOR_FULL_BUDGET_APPROVAL_SUBMITTED
			or id() == BUDGET_APPROVED
		    or id() == BUDGET_APPROVED_HQ
			or id() == FULL_BUDGET_APPROVED)
		)
	{
		$form->error($error[21]);
	}
	elseif($buget_approval_is_possible == false 
			and (id() == REQUEST_FOR_BUDGET_APPROVAL_SUBMITTED 
		    or id() == REQUEST_FOR_BUDGET_APPROVAL_SUBMITTED_HQ
			or id() == REQUEST_FOR_FULL_BUDGET_APPROVAL_SUBMITTED)
		  )
	{
		$form->error($error[13]);
	}
	elseif($buget_approval_is_possible == false 
		and (id() == BUDGET_APPROVED 
			or id() == BUDGET_APPROVED_HQ
		    or id() == FULL_BUDGET_APPROVED)
	)
	{
		$form->error($error[14]);
	}
	elseif (id() == ORDER_TO_SUPPLIER_SUBMITTED)
	{
		$form->error($error[2]);
	}
	else if (id() == REQUEST_FOR_DELIVERY_SUBMITTED)
	{
		$form->error($error[3]);
	}
	else if (id() == SUBMIT_BRIEFING_FOR_LAYOUT_PREVIEW
		and count($recipients) == 0)
	{
		$form->error($error[4]);
	}
	else if (id() == SUBMIT_REQUEST_FOR_DESIGN_APPROVAL)
	{
		$form->error($error[5]);
	}
	else if (id() == SUBMIT_REQUEST_FOR_OFFER)
	{
		$form->error($error[6]);
	}
	else if (id() == DELIVERY_CONFIRMED_FRW)
	{
		$form->error($error[7]);
	}
	else if (1==2 and (id() == REQUEST_FOR_BUDGET_APPROVAL_SUBMITTED 
		or id() == REQUEST_FOR_BUDGET_APPROVAL_SUBMITTED_HQ
		or id() == REQUEST_FOR_FULL_BUDGET_APPROVAL_SUBMITTED))
	{
		//not used here
		$form->error($error[8]);
	}
	else if (id() == OFFER_SUBMITTED)
	{
		$form->error($error[9]);
	}
	else if (id() == CONFIRM_ORDER_BY_SUPPLIER)
	{
		$form->error($error[10]);
	}
	else if (id() == REQUEST_FOR_DELIVERY_ACCEPTED)
	{
		$form->error($error[11]);
	}
	else if (id() == PICK_UP_CONFIRMED)
	{
		$form->error($error[15]);
	}
	elseif (id() == LAYOUT_REQUEST_ACCEPTED 
		and ($project["project_real_opening_date"] === Null 
			or $project["project_real_opening_date"] == '0000-00-00'))
	{
		$form->error($error[12]);
	}
	else if (id() == CMS_COMPLETTION_DEVELOPMENT)
	{
		if($cms_completed_by_lc == false and $cms_completed_by_client == false)
		{
			$form->error($error[17]);
		}
		elseif($cms_completed_by_lc == false)
		{
			$form->error($error[18]);
		}
		elseif($local_costs_are_in_the_project == true and $cms_completed_by_client == false)
		{
			$form->error($error[19]);
		}
		elseif($project_can_be_terminated == true)
		{
			$form->add_button("send", "Save");
		}
	}
	else if (id() == CMS_APPROVE_BY_CONTROLLER 
		and $cms_completed_by_pl == false)
	{
		$form->error($error[20]);
	}
	else if ((id() == SUBMIT_DESIGN_BRIEFING
		 or id() == REJECT_DESIGN_BRIEFING
		 or id() == ACCEPT_DESIGN_BRIEFING)
		and $design_briefing_is_missing == true)
	{
		$form->error($error[22]);
	}
	else if ((id() == SUBMIT_DESIGN_BRIEFING
		 or id() == REJECT_DESIGN_BRIEFING
		 or id() == ACCEPT_DESIGN_BRIEFING)
		and $design_briefing_is_incomplete == true)
	{
		$form->error($error[23]);
	}
	elseif (count($recipients)>0)
    {
		$form->add_button("send", "Send");
	}

	$form->add_button(FORM_BUTTON_BACK, "Back");


    /********************************************************************
        Populate form and process button clicks
    *********************************************************************/ 
    $form->populate();
    $form->process();


    if ($form->button("standard_text"))
    {
		if($form->value("standard_text"))
		{
			$sql = "select taskwork_text, taskwork_alert_text from taskworks where taskwork_id = " . $form->value("standard_text");
			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);

			$form->value("body_text", $row["taskwork_text"]);

			if($row["taskwork_alert_text"])
			{
				$form->error($row["taskwork_alert_text"]);
			}
		}
		else
		{
			$string_name = "project_" .id();
			$form->value("body_text", get_string($string_name));
		}
	}
	elseif ($form->button("send"))
    {
		$send_error = 0;
		
		//check if a recipient was selected
		$recipient_was_selected = 0;
		foreach ($recipients as $key=>$recipient)
		{
			if ($form->value("R" . $recipient["id"]) == 1)
			{
				$recipient_was_selected = 1;
			}
		}
		
		/*this validation is not used sinc July 2015 and might be added later again
		//validation for selected files from the attachment section
		$attachment_recipeint_selected = false;
		if($action_parameter["order_state_file_category_id_to_select_from"] > 0 
		   and count($attachment_recipients) > 0 and count($attachments) > 0)
		{
			   foreach($attachment_recipients as $key=>$attachment_recepient)
			   {
				  if(array_key_exists("R" . $key, $_POST) 
					  and $_POST["R" . $key] == 1)
				  {
					   $attachment_recipeint_selected = true;
					   $send_error = 1;
				  }
			   }

			   if($attachment_recipeint_selected == true)
			   {
				   foreach($attachments as $key=>$order_file_id)
				   {
					  if(array_key_exists("attachment_" . $order_file_id, $_POST) 
						  and $_POST["attachment_" . $order_file_id] == 1)
					  {
						$send_error = 0;
					  }
				   }
			   }
		}
		*/


		//check if attachments are selected
		$_attachments_selected = true;
		if($action_parameter["order_state_file_category_id_to_select_from"])
		{
			$_attachments_selected = false;
			foreach($attachments as $key=>$order_file_id)
			{
				if(array_key_exists("attachment_" . $order_file_id, $_POST) 
				  and $_POST["attachment_" . $order_file_id] == 1)
				{
					$_attachments_selected = true;
				}
			}
		}

		
		//validation of uploeded files
		if($files_are_to_be_uploaded == true)
		{
			if($form->value('order_file_path1') and !$form->value('attachment_title_1'))
			{
				$send_error = 2;
			}
			elseif(!$form->value('order_file_path1') and $form->value('attachment_title_1'))
			{
				$send_error = 3;
			}

			if($form->value('order_file_path2') and !$form->value('attachment_title_2'))
			{
				$send_error = 2;
			}
			elseif(!$form->value('order_file_path2') and $form->value('attachment_title_2'))
			{
				$send_error = 3;
			}

			if($form->value('order_file_path3') and !$form->value('attachment_title_3'))
			{
				$send_error = 2;
			}
			elseif(!$form->value('order_file_path3') and $form->value('attachment_title_3'))
			{
				$send_error = 3;
			}

			if($form->value('order_file_path4') and !$form->value('attachment_title_4'))
			{
				$send_error = 2;
			}
			elseif(!$form->value('order_file_path4') and $form->value('attachment_title_4'))
			{
				$send_error = 3;
			}

			if($form->value('order_file_path1'))
			{

				$path = $form->value("order_file_path1");
				$ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));

				$sql = "select file_type_id from file_types " . 
					   " where file_type_extension like '%" . $ext ."%'";

				
				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					//nop
				}
				else
				{
					$send_error = 5;
				}
			}
			
			if($form->value('order_file_path2'))
			{

				$path = $form->value("order_file_path2");
				$ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));

				$sql = "select file_type_id from file_types " . 
					   " where file_type_extension like '%" . $ext ."%'";

				
				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					//nop
				}
				else
				{
					$send_error = 5;
				}
			}

			if($form->value('order_file_path3'))
			{

				$path = $form->value("order_file_path3");
				$ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));

				$sql = "select file_type_id from file_types " . 
					   " where file_type_extension like '%" . $ext ."%'";

				
				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					//nop
				}
				else
				{
					$send_error = 5;
				}
			}

			if($form->value('order_file_path4'))
			{

				$path = $form->value("order_file_path4");
				$ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));

				$sql = "select file_type_id from file_types " . 
					   " where file_type_extension like '%" . $ext ."%'";

				
				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					//nop
				}
				else
				{
					$send_error = 5;
				}
			}

		}

		if($send_error == 1)
		{
			$form->error("You must select at least one attachment!");
		}
		elseif($send_error == 2)
		{
			$form->error("Please indicate a title for every file you like to upload!");
		}
		elseif($send_error == 3)
		{
			$form->error("Please upload a file for every attachment where you indicated a title.");
		}
		elseif($send_error == 5)
		{
			$form->error($error[16]);
		}
		elseif($recipient_was_selected == 0 and $project_can_be_terminated == false)
		{
			$form->error($error[1]);
		}
		/*
		elseif(id() == MINI_BOOKLET_APPROVED_BY_MANAGEMENT and !$form->value("store_retailarea"))
		{	
			$form->error("You must indicate the sales surface in sqms according to layout.");
		}
		*/
		elseif (id() == MINI_BOOKLET_APPROVAL_BY_MANAGEMENT
			and $_attachments_selected == false) {
			
			$form->error($error[24]);
		}
		elseif ($form->validate())
        {
			$additional_cc_recipients = array();
			
			// 01 prepare all cc recipients for the mail to be sent
			foreach ($recipients as $key=>$recipient)
            {
				if ($form->value("R" . $recipient["id"]) == 1)
                {
					
					$sql = "select user_id,user_email, " . 
						   " concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name  " . 
						   " from users " . 
						   " left join addresses on address_id = user_address " . 
						   " where user_active = 1 " . 
						   " and user_email = " . dbquote($recipient["cc"]);
					
					$res = mysql_query($sql) or dberror($sql);

					if ($row = mysql_fetch_assoc($res))
					{
						if(!array_key_exists($row["user_id"], $recipients))
						{
							$user_data = array();
							$user_data["id"] = $row["user_id"];
							$user_data["email"] = $row["user_email"];
							$user_data["full_name"] = $row["user_full_name"];
							$user_data["address"] = $row["address_id"];
							$cc_recipients[$row["user_id"]] = $user_data;
						}
					}
					else
					{
						$additional_cc_recipients[strtolower($recipient["cc"])] = strtolower($recipient["cc"]);
					}

					$sql = "select user_id,user_email, " . 
						   " concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name  " . 
						   " from users " . 
						   " left join addresses on address_id = user_address " .
						   " where user_active = 1 " . 
						   " and user_email = " . dbquote($recipient["deputy"]);
					
					$res = mysql_query($sql) or dberror($sql);

					if ($row = mysql_fetch_assoc($res))
					{
						if(!array_key_exists($row["user_id"], $recipients)
							and !array_key_exists($row["user_id"], $cc_recipients)
						  )
						{
							$user_data = array();
							$user_data["id"] = $row["user_id"];
							$user_data["email"] = $row["user_email"];
							$user_data["full_name"] = $row["user_full_name"];
							$user_data["address"] = $row["address_id"];
							$cc_recipients[$row["user_id"]] = $user_data;
						}
					}
					else
					{
						$additional_cc_recipients[strtolower($recipient["deputy"])] = strtolower($recipient["deputy"]);
					}
				}
			}
			
			//02 add cc recipients from copy to
			if($show_copy_to == 1)
			{
				if($form->value("copy_to"))
				{
					if(!array_key_exists(user_id(), $recipients))
					{
						$user_data = array();
						$user_data["id"] = user_id();
						$user_data["email"] = $logged_user["email"];
						$user_data["full_name"] = $logged_user["name"] . " " . $logged_user["firstname"];
						$user_data["address"] = $logged_user["address"];
						$cc_recipients[user_id()] = $user_data;
					}
				}
			}


			//03 add cc recipients from order step copy to
			if($form->value("order_state_email_copy_to"))
			{
				if($show_copy2_to == 1)
				{
					if($action_parameter["order_state_email_copy_to_email"])
					{
						$sql = "select user_id,user_email, " . 
							   " concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name  " . 
							   " from users " . 
							   " left join addresses on address_id = user_address " .
							   " where user_active = 1 " . 
							   " and user_email = " . dbquote($action_parameter["order_state_email_copy_to_email"]);
						
						$res = mysql_query($sql) or dberror($sql);

						if ($row = mysql_fetch_assoc($res))
						{
							if(!array_key_exists($row["user_id"], $recipients)
								and !array_key_exists($row["user_id"], $cc_recipients)
							  )
							{
								$user_data = array();
								$user_data["id"] = $row["user_id"];
								$user_data["email"] = $row["user_email"];
								$user_data["full_name"] = $row["user_full_name"];
								$user_data["address"] = $row["address_id"];
								$cc_recipients[$row["user_id"]] = $user_data;
							}
						}
					}
				}
			}

			//04 add all recipeints from the textarea CC Recipients
			$ccmails =  explode(' ', preg_replace("'\r?\n'"," ",$form->value('ccmails'))); 

			foreach($ccmails as $ccmail) {
				if(is_email_address($ccmail)) {
					
					$sql = "select user_id,user_email, " . 
						   " concat(user_name, ' ', user_firstname, ' (', address_company, ')') as user_full_name  " . 
						   " from users " . 
						   " left join addresses on address_id = user_address " .
						   " where user_active = 1 " . 
						   " and user_email = " . dbquote($ccmail);
					
					$res = mysql_query($sql) or dberror($sql);

					if ($row = mysql_fetch_assoc($res))
					{
						if(!array_key_exists($row["user_id"], $recipients)
							and !array_key_exists($row["user_id"], $cc_recipients)
						  )
						{
							$user_data = array();
							$user_data["id"] = $row["user_id"];
							$user_data["email"] = $row["user_email"];
							$user_data["full_name"] = $row["user_full_name"];
							$user_data["address"] = $row["address_id"];
							$cc_recipients[$row["user_id"]] = $user_data;
						}
					}
					else
					{
						$additional_cc_recipients[strtolower($ccmail)] = strtolower($ccmail);
					}
					
				}
			}
			
			
			//05 add uploaded attachments
			if($files_are_to_be_uploaded == true)
			{
				if($form->value('order_file_path1') and $form->value('attachment_title_1'))
				{
					$order_step_attachments['f1'] = APPLICATION_URL . $form->value('order_file_path1');
					$order_step_attachment_file_paths['f1'] = array("title"=>$form->value('attachment_title_1'), "path"=>$form->value('order_file_path1'));
				}
				if($form->value('order_file_path2') and $form->value('attachment_title_2'))
				{

					$order_step_attachments['f2'] = APPLICATION_URL . $form->value('order_file_path2');
					$order_step_attachment_file_paths['f2'] = array("title"=>$form->value('attachment_title_2'), "path"=>$form->value('order_file_path2'));
				}
				if($form->value('order_file_path3') and $form->value('attachment_title_3'))
				{
					$order_step_attachments['f3'] = APPLICATION_URL . $form->value('order_file_path3');
					$order_step_attachment_file_paths['f3'] = array("title"=>$form->value('attachment_title_3'), "path"=>$form->value('order_file_path3'));
				}
				if($form->value('order_file_path4') and $form->value('attachment_title_4'))
				{
					$order_step_attachments['f4'] = APPLICATION_URL . $form->value('order_file_path4');
					$order_step_attachment_file_paths['f4'] = array("title"=>$form->value('attachment_title_4'), "path"=>$form->value('order_file_path4'));
				}
			}

			//06 add selected attachments
			if($action_parameter["order_state_file_category_id_to_select_from"] > 0 
			   and count($attachment_recipients) > 0 and count($attachments) > 0)
			{
				foreach($attachments as $key=>$order_file_id)
				{
					if(array_key_exists("attachment_" . $order_file_id, $_POST) 
					  and $_POST["attachment_" . $order_file_id] == 1)
					{
						$sql = "select order_file_title, order_file_path ". 
							   " from order_files  " . 
							   " where order_file_id = " . $order_file_id;
						$res = mysql_query($sql) or dberror($sql);
						if ($row = mysql_fetch_assoc($res)) 
						{
							$order_step_attachments[$order_file_id] = APPLICATION_URL . $row["order_file_path"];
							$order_step_attachment_file_paths[$order_file_id] = array("title"=>$row["order_file_title"], "path"=>$row["order_file_path"]);
						}
					}
				}
			}


			
			//07 check if there are order_items being ordere twice or more
			$order_revisioned = 0;
			$revision_subject = "";
			$revision_text = "";
			if(id() == ORDER_TO_SUPPLIER_SUBMITTED)
			{
				foreach ($recipients as $key=>$recipient)
				{
					if ($form->value("R" . $recipient["id"]) == 1)
					{
						$num_recs = 0;
						$sql = "select count(order_item_id) as num_recs " .
							   "from order_items " .
							   " where order_item_order = " . $project["project_order"] . 
							   " and (order_item_ordered is null or order_item_ordered = '0000-00-00') " . 
							   " and (order_item_not_in_budget is null or order_item_not_in_budget =0) " . 
							   "    and order_item_quantity > 0 " . 
							   " and order_item_ordered_changes > 0 and order_item_supplier_address = " . $recipient["address"];
						$res = mysql_query($sql) or dberror($sql);
						if ($row = mysql_fetch_assoc($res))
						{
							$num_recs = $row["num_recs"];
						}
						if (array_key_exists("order_revisions", $recipient) 
							and $num_recs > 0 and $recipient["order_revisions"] > 0)
						{
							$revision_subject = " (Revisioned Order)";
							$revision_text = "\n\nPLEASE NOTE: This is a revisioned order!\n\n";
							$order_revisioned = 1;
						}
					}
				}
			}



			//get filesize of all attachments
			foreach($order_step_attachments as $key=>$file)
			{
				$file_name = str_replace("/files/orders/". $project["order_number"]. "/", "",$order_step_attachment_file_paths[$key]["path"]);
				
				$filepath = $_SERVER["DOCUMENT_ROOT"] . "/" . $order_step_attachment_file_paths[$key]["path"];

				$total_file_size = $total_file_size + filesize ( $filepath );

			}
			
			//08 prepare mail message
			$mail = new PHPMailer();
			$mail->Subject = MAIL_SUBJECT_PREFIX . ": " . $form->value("subject") . $revision_subject;
			$mail->SetFrom($form->value("sender_email"), $form->value("sender_name"));
			$mail->AddReplyTo($form->value("sender_email"), $form->value("sender_name"));

			$bodytext0 = str_replace("\r\n", "\n", $form->value("body_text"))  . $revision_text;
			
			if(id() == CMS_APPROVE_BY_CONTROLLER) {
				$link ="/user/project_costs_real_costs.php?pid=" . param("pid");
			}
			elseif(($cer_is_needed == true or $af_is_needed == true)
				and id() == PROJECT_REQUEST_ACCEPTED) {
				$link ="/cer/cer_project.php?pid=" . param("pid") . "&id=" . param("pid");
			}
			elseif(id() == FULL_BUDGET_APPROVED) {
				$link ="/user/project_costs_overview.php?pid=" . param("pid");
			}
			elseif(id() == ACCEPT_DESIGN_BRIEFING or id() == ACCEPT_DESIGN_BRIEFING) {
				$link ="/user/project_design_briefing.php?pid=" . param("pid");
			}


			
			else {
				$link ="/user/project_task_center.php?pid=" . param("pid");
			}
			
			$bodytext = $bodytext0 . "\n\nClick below to have direct access to the project:\n";
			$bodytext = $bodytext .  APPLICATION_URL .  $link . "\n\n";

			if(count($order_step_attachments) > 0)
			{
				
				if(id() != SUBMIT_LAYOUT_APPROVAL_TO_CLIENT) {
					$bodytext .= "Directly access the project's attachment section by clicking the following link:" . "\n";
					$bodytext .= APPLICATION_URL . "/user/project_view_attachments.php?pid=" . $project["project_id"] . "\n\n";
				
					
				}
				
				foreach($order_step_attachments as $key=>$file)
				{
					if(id() != SUBMIT_LAYOUT_APPROVAL_TO_CLIENT) {
						$bodytext .= "Or directly access the following files in the context of your task:" . "\n";
						$bodytext .= $file . "\n";
					}

					$file_name = str_replace("/files/orders/". $project["order_number"]. "/", "",$order_step_attachment_file_paths[$key]["path"]);
					
					$filepath = $_SERVER["DOCUMENT_ROOT"] . "/" . $order_step_attachment_file_paths[$key]["path"];
					
					if($total_file_size < 7340032
						and $form->value("order_file_category_attach_in_mails") == 1)
					{
						$mail->AddAttachment($filepath);
					}
					
				}
			}

			$mail->Body = $bodytext;


			//08 add recipients
			foreach ($recipients as $key=>$recipient)
			{
				if ($form->value("R" . $recipient["id"]) == 1)
                {
					$mail->AddAddress($recipient["email"]);
				}
			}
			
			//add cc recipients
			foreach ($cc_recipients as $key=>$recipient)
			{
				$mail->AddCC($recipient["email"]);
			}

			//09 add BCC for procurement
			if (id() == SUBMIT_REQUEST_FOR_OFFER)
			{
				foreach ($procurement as $key=>$recipient)
				{
					if ($form->value("P" . $recipient["id"]) == 1)
					{
						$mail->AddBCC($recipient["email"]);
					}
				}
			}

			//10 add additional recipients not being users of etail net
			foreach($additional_cc_recipients as $key=>$email)
			{
				$mail->AddCC($email);
			}
			
			//11 send mail
			if($senmail_activated == true)
			{
				$m_result = $mail->send();
			}

			
			//12 POST Mail Handler
			if($m_result or $senmail_activated == false)
			{
				//12.1 append mails
				foreach ($recipients as $key=>$recipient)
				{
					if ($form->value("R" . $recipient["id"]) == 1)
					{
						append_mail($project["project_order"], $recipient["id"], user_id(), $bodytext, id(), 1, $order_revisioned);
					}
				}

				foreach ($cc_recipients as $key=>$recipient)
				{
					append_mail($project["project_order"], $recipient["id"], user_id(), $bodytext, id(), 1, $order_revisioned, 1);
				}

				foreach ($procurement as $key=>$recipient)
				{
					append_mail($project["project_order"], $recipient["id"], user_id(), $bodytext, id(), 1, $order_revisioned, 1);
				}
			}


			//13 set visibility of projects and update dates
			if (id() == ORDER_TO_SUPPLIER_SUBMITTED)
			{
				foreach ($recipients as $key=>$recipient)
				{
					if ($form->value("R" . $recipient["id"]) == 1)
					{
						set_visibility_for_suppliers($project["project_order"], $recipient["address"]);             
						append_order_dates($project["project_order"], $recipient["address"]);
					}
				}
			}
			elseif (id() == REQUEST_FOR_DELIVERY_SUBMITTED)
			{
				foreach ($recipients as $key=>$recipient)
				{
					if ($form->value("R" . $recipient["id"]) == 1)
					{
						append_request_for_delivery_dates($project["project_order"], $recipient["address"]);
					}
				}
			}
			elseif (id() == SUBMIT_REQUEST_FOR_OFFER)
			{
				foreach ($recipients as $key=>$recipient)
				{
				   if ($form->value("R" . $recipient["id"]) == 1)
				   {
					   set_visibility_for_suppliers($project["project_order"], $recipient["address"]);
				   }
				}
			}
			
			//14 freeze budget, set visibility for suppliers and forwarders
			if(id() == BUDGET_APPROVED
				or id() == BUDGET_APPROVED_HQ)
			{
				/*
				set_visibility_for_suppliers($project["project_order"], "");
                set_visibility_in_delivery_schedule($project["project_order"]);

				if($project["project_cost_type"] == 1) // only freeze budget for hq supplied items
				{
					update_exchange_rates($project["project_order"], 1);
					freeze_budget_hq_supplied_items($project["project_order"]);
				}
				else //update cer investments only for non corporate projects
				{
					update_exchange_rates($project["project_order"], 1);
					freeze_budget($project["project_order"]);
					recalculate_cer_investments($project["project_id"], $project["project_cost_type"], $project["order_shop_address_country"]);
				}
				*/


				set_visibility_for_suppliers($project["project_order"], "");
                set_visibility_in_delivery_schedule($project["project_order"]);
				
				update_exchange_rates($project["project_order"], 1);
				freeze_budget($project["project_order"]);
				update_budget_state($project["project_order"], 1);

				//update cer investments only for corporate projects
				if($project["project_cost_type"] == 1)
				{
					$order_currency = get_order_currency($project["project_order"]);
					$budget = get_project_budget_totals(param("pid"), $order_currency);
					$result = update_investments_from_project_cost_sheet(param("pid"), $budget, 0);
				}
				
			}
			elseif(id() == FULL_BUDGET_APPROVED)
			{
				
				/*
				set_visibility_for_suppliers($project["project_order"], "");
                set_visibility_in_delivery_schedule($project["project_order"]);
				
				update_exchange_rates($project["project_order"], 1);
				freeze_budget($project["project_order"]);

				//update cer investments only for corporate projects
				if($project["project_cost_type"] == 1)
				{
					recalculate_cer_investments($project["project_id"], $project["project_cost_type"], $project["order_shop_address_country"]);
				}
				*/

				set_visibility_for_suppliers($project["project_order"], "");
                set_visibility_in_delivery_schedule($project["project_order"]);
				
				update_exchange_rates($project["project_order"], 1);
				freeze_budget($project["project_order"]);
				update_budget_state($project["project_order"], 1);


				$order_currency = get_order_currency($project["project_order"]);
				$budget = get_project_budget_totals(param("pid"), $order_currency);
				$result = update_investments_from_project_cost_sheet(param("pid"), $budget, 0);

				//update cer investments only for corporate projects
				/*
				if($project["project_cost_type"] == 1)
				{
					$order_currency = get_order_currency($project["project_order"]);
					$budget = get_project_budget_totals(param("pid"), $order_currency);
					$result = update_investments_from_project_cost_sheet(param("pid"), $budget, 0);
				}
				*/
				
			}


			//15 lock budget
			
			/*
			if($project["project_cost_type"] == 1) // corporate
			{
				if (id() == FULL_BUDGET_APPROVED 
					or id() == DELIVERY_CONFIRMED 
					or id() == MOVED_TO_THE_RECORDS 
					or id() == ORDER_CANCELLED)
				{
					update_budget_state($project["project_order"], 1);
				}
			}
			else
			{
				if (id() == FULL_BUDGET_APPROVED 
					or id() == BUDGET_APPROVED
					or id() == BUDGET_APPROVED_HQ
					or id() == DELIVERY_CONFIRMED 
					or id() == MOVED_TO_THE_RECORDS 
					or id() == ORDER_CANCELLED)
				{
					update_budget_state($project["project_order"], 1);
				}
			}
			*/

			if (id() == FULL_BUDGET_APPROVED 
				or id() == BUDGET_APPROVED
				or id() == BUDGET_APPROVED_HQ
				or id() == DELIVERY_CONFIRMED 
				or id() == MOVED_TO_THE_RECORDS 
				or id() == ORDER_CANCELLED)
			{
				update_budget_state($project["project_order"], 1);
			}


			//16 create attachment of aopproved budget
			if(id() == BUDGET_APPROVED 
				    or id() == BUDGET_APPROVED_HQ
					or id() == FULL_BUDGET_APPROVED)
			{
				
				if($write_budget_to_disk == true)
				{
					$budget_approval_of_hq_supplied_items_corporate_porjects = false;
					/*
					if($project["project_cost_type"] == 1 and id() == BUDGET_APPROVED) {
						$budget_approval_of_hq_supplied_items_corporate_projects = true;
					}
					*/

					//12.5.1 create file in CHF 
					$sc = 1;
					$file_name = $project["project_number"] ."_approved_budget_CHF_" . id() . "_" . date("YmdHis") . ".pdf";
					include("project_costs_budget_pdf.php");
					unset($pdf);


					if(id() == BUDGET_APPROVED
						or id() == BUDGET_APPROVED_HQ)
					{
						$title = "Approved Budget in CHF";
					}
					else
					{
						$title = "Approved Full Budget in CHF";
					}

					//12.5.2 add attachment into category Budget & Offers
					
					$sql_i = 'insert into order_files (' . 
							 'order_file_order, order_file_title, order_file_path, order_file_type, order_file_category, order_file_owner, user_created, date_created' .
							 ') VALUES (' . 
							 dbquote($project['project_order']) . ', ' .
							 dbquote($title) . ', ' .
							 dbquote('/files/orders/' . $project["project_number"] . '/' . $file_name) . ', ' .
							 dbquote(13) . ', ' .
							 dbquote(5) . ', ' .
							 dbquote(user_id()) . ', ' .
							 dbquote(user_login()) . ', ' .
							 dbquote(date("Y-m-d H:i:s")) . ')';
					$result = mysql_query($sql_i) or dberror($sql_i);

					$attachment_id = mysql_insert_id();

					//12.5.3 save accessibility info
					if (count($recipients) > 0)
					{
						//compose filter
						$tmp = array();
						foreach($recipients as $key=>$recipient)
						{
							$tmp[$recipient["id"]] = $recipient["id"];
						}

						
						if (count($tmp) > 0)
						{
							$recipient_filter = " where user_id in (" . implode(',', $tmp) . ")";
							
							$sql = "select user_address from users " . $recipient_filter;
							$res = mysql_query($sql) or dberror($sql);
							
							while($row = mysql_fetch_assoc($res))
							{
								$address_id = $row["user_address"];

								$attachment_address_fields = array();
								$attachment_address_values = array();

								$attachment_address_fields[] = "order_file_address_file";
								$attachment_address_values[] = $attachment_id;

								$attachment_address_fields[] = "order_file_address_address";
								$attachment_address_values[] = $address_id;

								$attachment_address_fields[] = "date_created";
								$attachment_address_values[] = "current_timestamp";

								$attachment_address_fields[] = "date_modified";
								$attachment_address_values[] = "current_timestamp";
								
								if (isset($_SESSION["user_login"]))
								{
									$attachment_address_fields[] = "user_created";
									$attachment_address_values[] = dbquote($_SESSION["user_login"]);

									$attachment_address_fields[] = "user_modified";
									$attachment_address_values[] = dbquote($_SESSION["user_login"]);
								}
					
								$sql = "insert into order_file_addresses (" . join(", ", $attachment_address_fields) . ") values (" . join(", ", $attachment_address_values) . ")";
								mysql_query($sql) or dberror($sql);
							}
						}
					}

					
					
					//12.5.4 create file in LOC 
					$sc = 0;
					$order_currency = get_order_currency($project["project_order"]);
					$show_budget_in_loc = true;
					$file_name = $project["project_number"] ."_approved_budget_" . $order_currency["symbol"] ."_" . id() . "_" . date("YmdHis") . ".pdf";

					include("project_costs_budget_pdf.php");
					$write_budget_to_disk == false;
					
					if(id() == BUDGET_APPROVED
						or id() == BUDGET_APPROVED_HQ)
					{
						$title = "Approved Budget in " . $order_currency["symbol"];
					}
					else
					{
						$title = "Approved Full Budget in " . $order_currency["symbol"];
					}
					

					//12.5.5 add attachment into category Budget & Offers
					
					$sql_i = 'insert into order_files (' . 
							 'order_file_order, order_file_title, order_file_path, order_file_type, order_file_category, order_file_owner, user_created, date_created' .
							 ') VALUES (' . 
							 dbquote($project['project_order']) . ', ' .
							 dbquote($title) . ', ' .
							 dbquote('/files/orders/' . $project["project_number"] . '/' . $file_name) . ', ' .
							 dbquote(13) . ', ' .
							 dbquote(5) . ', ' .
							 dbquote(user_id()) . ', ' .
							 dbquote(user_login()) . ', ' .
							 dbquote(date("Y-m-d H:i:s")) . ')';
					$result = mysql_query($sql_i) or dberror($sql_i);


					$attachment_id = mysql_insert_id();

					//12.5.6 save accessibility info
					if (count($recipients) > 0)
					{
						//compose filter
						$tmp = array();
						foreach($recipients as $key=>$recipient)
						{
							$tmp[$recipient["id"]] = $recipient["id"];
						}

						
						if (count($tmp) > 0)
						{
							$recipient_filter = " where user_id in (" . implode(',', $tmp) . ")";
							
							$sql = "select user_address from users " . $recipient_filter;
							$res = mysql_query($sql) or dberror($sql);
							
							while($row = mysql_fetch_assoc($res))
							{
								$address_id = $row["user_address"];

								$attachment_address_fields = array();
								$attachment_address_values = array();

								$attachment_address_fields[] = "order_file_address_file";
								$attachment_address_values[] = $attachment_id;

								$attachment_address_fields[] = "order_file_address_address";
								$attachment_address_values[] = $address_id;

								$attachment_address_fields[] = "date_created";
								$attachment_address_values[] = "current_timestamp";

								$attachment_address_fields[] = "date_modified";
								$attachment_address_values[] = "current_timestamp";
								
								if (isset($_SESSION["user_login"]))
								{
									$attachment_address_fields[] = "user_created";
									$attachment_address_values[] = dbquote($_SESSION["user_login"]);

									$attachment_address_fields[] = "user_modified";
									$attachment_address_values[] = dbquote($_SESSION["user_login"]);
								}
					
								$sql = "insert into order_file_addresses (" . join(", ", $attachment_address_fields) . ") values (" . join(", ", $attachment_address_values) . ")";
								mysql_query($sql) or dberror($sql);
							}
						}
					
					}
				}
			}

			
			//17. append tasks for users
			if ($form->value("due_date"))
			{
				$date = from_system_date($form->value("due_date"));
			}
			else
			{
				$date = date("Y-m-d");
			}
			$link ="project_task_center.php?pid=" . param("pid");
			
			foreach ($recipients as $key=>$recipient)
			{
				if ($form->value("R" . $recipient["id"]) == 1)
				{
					if (id() == PICK_UP_CONFIRMED)
					{
						$sql_u = "update order_items set order_item_pickup_confirmed = 1 " .
								  " where order_item_type <= " . ITEM_TYPE_SPECIAL .
								  "    and order_item_order = " . dbquote($project["project_order"]) .
								  "    and order_item_pickup is not null " . 
								  "    and order_item_pickup <> '0000-00-00' " . 
								  "    and order_item_supplier_address = " . dbquote($logged_user["address"]);

						$res = mysql_query($sql_u) or dberror($sql_u);

						if($delete_task_confirm_pickup == true)
						{
							delete_user_tasks($project["project_order"], $recipient["id"], $form->value("body_text"), $link, $date, user_id(), id(), 1);
						}
					}
					else
					{
						delete_user_tasks($project["project_order"], $recipient["id"], $form->value("body_text"), $link, $date, user_id(), id(), 1);
					}

					if ($action_parameter["append_task"] == 1)
					{
						$revision_text = "";
						if (id() == ORDER_TO_SUPPLIER_SUBMITTED and $recipient["order_revisions"] > 0)
						{
							$revision_text = " PLEASE NOTE: This is a revisioned order!";
						}

						if(array_key_exists("role", $recipient) 
								and $recipient["role"] != "Additional Recipient")
						{
							append_task($project["project_order"], $recipient["id"], $form->value("body_text") . $revision_text, $link, $date, user_id(), id(), 1, $form->value("keep_project_state"));

							//echo "1: " . $recipient["id"] . "<br />";
						}
						else
						{
							if(id() == REJECT_DESIGN_BRIEFING) {
								append_task($project["project_order"], $recipient["id"], $form->value("body_text") . $revision_text, $link, $date, user_id(), SUBMIT_DESIGN_BRIEFING, 1, $form->value("keep_project_state"));
							}
							elseif(id() == ACCEPT_DESIGN_BRIEFING) {
								//no task
							}
							else {
								append_task($project["project_order"], $recipient["id"], $form->value("body_text") . $revision_text, $link, $date, user_id(), id(), 1, $form->value("keep_project_state"));
							}
							//echo "2: " . $recipient["id"] . "<br />";

							
						}
					}
					elseif($layout_is_provided_locally == true
						and (id() == LAYOUT_REJECTED_BY_HQ
						or id() == LAYOUT_APPROVED_BY_HQ)) {
						//append_task($project["project_order"], $recipient["id"], $form->value("body_text") . '', $link, $date, user_id(), SUBMIT_LAYOUT_FOR_APPROVAL_TO_HQ, 1, $form->value("keep_project_state"));
					}
					elseif($layout_is_provided_locally == true
						and id() == BUDGET_REJECTED_BY_HQ) {
						append_task($project["project_order"], $recipient["id"], $form->value("body_text") . '', $link, $date, user_id(), SUBMIT_BUDGET_FOR_HQ_APPROVAL, 1, $form->value("keep_project_state"));
					}
				}
			}
			
			//18 save uploaded files and set accessibility
			if($files_are_to_be_uploaded == true)
			{
				if($form->value('order_file_path1') and $form->value('attachment_title_1'))
				{

					$file_type_is_valid = false;
					$path = $form->value("order_file_path1");
					$ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));

					$sql = "select file_type_id from file_types " . 
						   " where file_type_extension like '%" . $ext ."%'";

					
					$res = mysql_query($sql) or dberror($sql);
					if ($row = mysql_fetch_assoc($res))
					{
						$file_type = $row["file_type_id"];
						$file_type_is_valid = true;
					}


					if($file_type_is_valid == true)
					{
					
						$sql_i = 'insert into order_files (' . 
								 'order_file_order, order_file_title, order_file_path, order_file_type, order_file_category, order_file_owner, order_file_attach_to_cer_booklet, user_created, date_created' .
								 ') VALUES (' . 
								 dbquote($project['project_order']) . ', ' .
								 dbquote($form->value('attachment_title_1')) . ', ' .
								 dbquote($form->value('order_file_path1')) . ', ' .
								 dbquote($file_type) . ', ' .
								 dbquote($form->value('file_category')) . ', ' .
								 dbquote(user_id()) . ', ' .
								 dbquote($form->value('order_file_attach_to_cer_booklet1')) . ', ' .
								 dbquote(user_login()) . ', ' .
								 dbquote(date("Y-m-d H:i:s")) . ')';
						$result = mysql_query($sql_i) or dberror($sql_i);

						$attachment_id = mysql_insert_id();



						//save accessibility info
						if (count($recipients) > 0)
						{
							//compose filter
							$tmp = array();
							foreach($recipients as $key=>$recipient)
							{
								$tmp[$recipient["id"]] = $recipient["id"];
							}

							
							if (count($tmp) > 0)
							{
								$recipient_filter = " where user_id in (" . implode(',', $tmp) . ")";
								
								$sql = "select user_address from users " . $recipient_filter;
								$res = mysql_query($sql) or dberror($sql);
								
								while($row = mysql_fetch_assoc($res))
								{
									$address_id = $row["user_address"];

									$attachment_address_fields = array();
									$attachment_address_values = array();

									$attachment_address_fields[] = "order_file_address_file";
									$attachment_address_values[] = $attachment_id;

									$attachment_address_fields[] = "order_file_address_address";
									$attachment_address_values[] = $address_id;

									$attachment_address_fields[] = "date_created";
									$attachment_address_values[] = "current_timestamp";

									$attachment_address_fields[] = "date_modified";
									$attachment_address_values[] = "current_timestamp";
									
									if (isset($_SESSION["user_login"]))
									{
										$attachment_address_fields[] = "user_created";
										$attachment_address_values[] = dbquote($_SESSION["user_login"]);

										$attachment_address_fields[] = "user_modified";
										$attachment_address_values[] = dbquote($_SESSION["user_login"]);
									}
						
									$sql = "insert into order_file_addresses (" . join(", ", $attachment_address_fields) . ") values (" . join(", ", $attachment_address_values) . ")";
									mysql_query($sql) or dberror($sql);
								}
							}
						}
					}
				}


				if($form->value('order_file_path2') and $form->value('attachment_title_2'))
				{
					
					
					$file_type_is_valid = false;
					$path = $form->value("order_file_path2");
					$ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));

					$sql = "select file_type_id from file_types " . 
						   " where file_type_extension like '%" . $ext ."%'";

					
					$res = mysql_query($sql) or dberror($sql);
					if ($row = mysql_fetch_assoc($res))
					{
						$file_type = $row["file_type_id"];
						$file_type_is_valid = true;
					}

					
					
					if($file_type_is_valid == true)
					{
						$sql_i = 'insert into order_files (' . 
								 'order_file_order, order_file_title, order_file_path, order_file_type, order_file_category, order_file_owner, order_file_attach_to_cer_booklet, user_created, date_created' .
								 ') VALUES (' . 
								 dbquote($project['project_order']) . ', ' .
								 dbquote($form->value('attachment_title_2')) . ', ' .
								 dbquote($form->value('order_file_path2')) . ', ' .
								 dbquote($file_type) . ', ' .
								 dbquote($form->value('file_category')) . ', ' .
								 dbquote(user_id()) . ', ' .
								 dbquote($form->value('order_file_attach_to_cer_booklet2')) . ', ' .
								 dbquote(user_login()) . ', ' .
								 dbquote(date("Y-m-d H:i:s")) . ')';
						$result = mysql_query($sql_i) or dberror($sql_i);

						$attachment_id = mysql_insert_id();

						

						//save accessibility info
						if (count($recipients) > 0)
						{
							//compose filter
							$tmp = array();
							foreach($recipients as $key=>$recipient)
							{
								$tmp[$recipient["id"]] = $recipient["id"];
							}

							
							if (count($tmp) > 0)
							{
								$recipient_filter = " where user_id in (" . implode(',', $tmp) . ")";
								
								$sql = "select user_address from users " . $recipient_filter;
								$res = mysql_query($sql) or dberror($sql);
								
								while($row = mysql_fetch_assoc($res))
								{
									$address_id = $row["user_address"];

									$attachment_address_fields = array();
									$attachment_address_values = array();

									$attachment_address_fields[] = "order_file_address_file";
									$attachment_address_values[] = $attachment_id;

									$attachment_address_fields[] = "order_file_address_address";
									$attachment_address_values[] = $address_id;

									$attachment_address_fields[] = "date_created";
									$attachment_address_values[] = "current_timestamp";

									$attachment_address_fields[] = "date_modified";
									$attachment_address_values[] = "current_timestamp";
									
									if (isset($_SESSION["user_login"]))
									{
										$attachment_address_fields[] = "user_created";
										$attachment_address_values[] = dbquote($_SESSION["user_login"]);

										$attachment_address_fields[] = "user_modified";
										$attachment_address_values[] = dbquote($_SESSION["user_login"]);
									}
						
									$sql = "insert into order_file_addresses (" . join(", ", $attachment_address_fields) . ") values (" . join(", ", $attachment_address_values) . ")";
									mysql_query($sql) or dberror($sql);
								}
							}
						}
					}
				}

				if($form->value('order_file_path3') and $form->value('attachment_title_3'))
				{
					
					$file_type_is_valid = false;
					$path = $form->value("order_file_path3");
					$ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));

					$sql = "select file_type_id from file_types " . 
						   " where file_type_extension like '%" . $ext ."%'";

					
					$res = mysql_query($sql) or dberror($sql);
					if ($row = mysql_fetch_assoc($res))
					{
						$file_type = $row["file_type_id"];
						$file_type_is_valid = true;
					}

											
					if($file_type_is_valid == true)
					{
						$sql_i = 'insert into order_files (' . 
								 'order_file_order, order_file_title, order_file_path, order_file_type, order_file_category, order_file_owner, order_file_attach_to_cer_booklet, user_created, date_created' .
								 ') VALUES (' . 
								 dbquote($project['project_order']) . ', ' .
								 dbquote($form->value('attachment_title_3')) . ', ' .
								 dbquote($form->value('order_file_path3')) . ', ' .
								 dbquote($file_type) . ', ' .
								 dbquote($form->value('file_category')) . ', ' .
								 dbquote(user_id()) . ', ' .
								 dbquote($form->value('order_file_attach_to_cer_booklet3')) . ', ' .
								 dbquote(user_login()) . ', ' .
								 dbquote(date("Y-m-d H:i:s")) . ')';
						$result = mysql_query($sql_i) or dberror($sql_i);

						$attachment_id = mysql_insert_id();

						


						//save accessibility info
						if (count($recipients) > 0)
						{
							//compose filter
							$tmp = array();
							foreach($recipients as $key=>$recipient)
							{
								$tmp[$recipient["id"]] = $recipient["id"];
							}

							
							if (count($tmp) > 0)
							{
								$recipient_filter = " where user_id in (" . implode(',', $tmp) . ")";
								
								$sql = "select user_address from users " . $recipient_filter;
								$res = mysql_query($sql) or dberror($sql);
								
								while($row = mysql_fetch_assoc($res))
								{
									$address_id = $row["user_address"];

									$attachment_address_fields = array();
									$attachment_address_values = array();

									$attachment_address_fields[] = "order_file_address_file";
									$attachment_address_values[] = $attachment_id;

									$attachment_address_fields[] = "order_file_address_address";
									$attachment_address_values[] = $address_id;

									$attachment_address_fields[] = "date_created";
									$attachment_address_values[] = "current_timestamp";

									$attachment_address_fields[] = "date_modified";
									$attachment_address_values[] = "current_timestamp";
									
									if (isset($_SESSION["user_login"]))
									{
										$attachment_address_fields[] = "user_created";
										$attachment_address_values[] = dbquote($_SESSION["user_login"]);

										$attachment_address_fields[] = "user_modified";
										$attachment_address_values[] = dbquote($_SESSION["user_login"]);
									}
						
									$sql = "insert into order_file_addresses (" . join(", ", $attachment_address_fields) . ") values (" . join(", ", $attachment_address_values) . ")";
									mysql_query($sql) or dberror($sql);
								}
							}
						}
					}
				}

				if($form->value('order_file_path4') and $form->value('attachment_title_4'))
				{
					
					
					$file_type_is_valid = false;
					$path = $form->value("order_file_path4");
					$ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));

					$sql = "select file_type_id from file_types " . 
						   " where file_type_extension like '%" . $ext ."%'";

					
					$res = mysql_query($sql) or dberror($sql);
					if ($row = mysql_fetch_assoc($res))
					{
						$file_type = $row["file_type_id"];
						$file_type_is_valid = true;
					}

					if($file_type_is_valid == true)
					{
					
						$sql_i = 'insert into order_files (' . 
								 'order_file_order, order_file_title, order_file_path, order_file_type, order_file_category, order_file_owner, order_file_attach_to_cer_booklet, user_created, date_created' .
								 ') VALUES (' . 
								 dbquote($project['project_order']) . ', ' .
								 dbquote($form->value('attachment_title_4')) . ', ' .
								 dbquote($form->value('order_file_path4')) . ', ' .
								 dbquote($file_type) . ', ' .
								 dbquote($form->value('file_category')) . ', ' .
								 dbquote(user_id()) . ', ' .
								 dbquote($form->value('order_file_attach_to_cer_booklet4')) . ', ' .
								 dbquote(user_login()) . ', ' .
								 dbquote(date("Y-m-d H:i:s")) . ')';
						$result = mysql_query($sql_i) or dberror($sql_i);

						$attachment_id = mysql_insert_id();

						


						//save accessibility info
						if (count($recipients) > 0)
						{
							//compose filter
							$tmp = array();
							foreach($recipients as $key=>$recipient)
							{
								$tmp[$recipient["id"]] = $recipient["id"];
							}

							
							if (count($tmp) > 0)
							{
								$recipient_filter = " where user_id in (" . implode(',', $tmp) . ")";
								
								$sql = "select user_address from users " . $recipient_filter;
								$res = mysql_query($sql) or dberror($sql);
								
								while($row = mysql_fetch_assoc($res))
								{
									$address_id = $row["user_address"];

									$attachment_address_fields = array();
									$attachment_address_values = array();

									$attachment_address_fields[] = "order_file_address_file";
									$attachment_address_values[] = $attachment_id;

									$attachment_address_fields[] = "order_file_address_address";
									$attachment_address_values[] = $address_id;

									$attachment_address_fields[] = "date_created";
									$attachment_address_values[] = "current_timestamp";

									$attachment_address_fields[] = "date_modified";
									$attachment_address_values[] = "current_timestamp";
									
									if (isset($_SESSION["user_login"]))
									{
										$attachment_address_fields[] = "user_created";
										$attachment_address_values[] = dbquote($_SESSION["user_login"]);

										$attachment_address_fields[] = "user_modified";
										$attachment_address_values[] = dbquote($_SESSION["user_login"]);
									}
						
									$sql = "insert into order_file_addresses (" . join(", ", $attachment_address_fields) . ") values (" . join(", ", $attachment_address_values) . ")";
									mysql_query($sql) or dberror($sql);
								}
							}
						}
					}
				}
			}


			//19 set accessibility for selected files
			if($action_parameter["order_state_file_category_id_to_select_from"] > 0 
			   and count($attachment_recipients) > 0 and count($attachments) > 0)
			{
				foreach($attachment_recipients as $key=>$attachment_recepient)
				{
					if(array_key_exists("R" . $attachment_recepient['id'], $_POST) 
						and $_POST["R" . $attachment_recepient['id']] == 1)
					{
						foreach($attachments as $key=>$order_file_id)
						{
							if(array_key_exists("attachment_" . $order_file_id, $_POST) 
							  and $_POST["attachment_" . $order_file_id] == 1)
							{
								$sql = "select order_file_title, order_file_path ". 
									   " from order_files  " . 
									   " where order_file_id = " . $order_file_id;
								$res = mysql_query($sql) or dberror($sql);
								if ($row = mysql_fetch_assoc($res)) 
								{
									$order_step_attachments[$order_file_id] = APPLICATION_URL . $row["order_file_path"];
									$order_step_attachment_file_paths[$order_file_id] = array("title"=>$row["order_file_title"], "path"=>$row["order_file_path"]);
								}


								//check if accessibility already was set
								$sql = "select order_file_address_id ". 
									   " from order_file_addresses  " . 
									   " where order_file_address_file = " . $order_file_id . 
									   " and order_file_address_address = " . $attachment_recepient["address"];
								$res = mysql_query($sql) or dberror($sql);
								if ($row = mysql_fetch_assoc($res)) 
								{
								}
								else
								{
									$sql_i = "INSERT INTO order_file_addresses " . 
											 "(order_file_address_file, " . 
											 "order_file_address_address, " . 
											 "user_created, date_created) VALUES (" .
											 $order_file_id . ", " .
											 $attachment_recepient["address"] . ", " .
											 dbquote(user_login()) . ", " . 
											 dbquote(date("Y-m-d H:i:s")) .
											 ")";

									$result = mysql_query($sql_i) or dberror($sql_i);
								}
							}
						}
					}
				}
			}


			//20 send mail to the client to prepare next steps after HQ-budget approval
			//only for corporate projects
			// MAIL TEMPLATE 55
			if ((id() == BUDGET_APPROVED  or id() == BUDGET_APPROVED_HQ)
				and $af_cer_submission_is_needed == true
				and $project["project_cost_type"] == 1) //corporate projects
			{
				
				/*
				$actionmail = new ActionMail(55);
				$actionmail->setParam('id', param("pid"));
				$actionmail->setParam('step', id());

				$attachment_text = "";
				if(count($order_step_attachments) > 0)
				{
					$attachment_text .= "Directly access the project's attachment section by clicking the following link:" . "\n";
					$attachment_text .= APPLICATION_URL . "/user/project_view_attachments.php?pid=" . $project["project_id"] . "\n\n";

					
					$attachment_text .= "Or directly access the following files in the context of your task:" . "\n";
					foreach($order_step_attachments as $key=>$file)
					{
						$attachment_text .= $file . "\n";

						$file_name = str_replace("/files/orders/". $project["order_number"]. "/", "",$order_step_attachment_file_paths[$key]["path"]);
						
						$filepath = $_SERVER["DOCUMENT_ROOT"] . "/" . $order_step_attachment_file_paths[$key]["path"];
						
						
						if($total_file_size < 7340032 
							and $form->value("order_file_category_attach_in_mails") == 1)
						{
							$actionmail->addAttachment($filepath);
						}
						
					}

				}
				$actionmail->setParam('attachment_text', $attachment_text);

				

				if($senmail_activated == true)
				{
					$actionmail->send();
				}
				*/
			}
			
			//21 Send mail to client for corporate projects to tell him to prepare the CER
			//MAIL TEMPLATE 56
			if ((id() == BUDGET_APPROVED or id() == BUDGET_APPROVED_HQ)
				and $af_cer_submission_is_needed == true
				and $project["project_cost_type"] == 1) //corporate projects
			{
				/*
				$actionmail = new ActionMail(56);
				$actionmail->setParam('id', param("pid"));
				$actionmail->setParam('step', id());

				$attachment_text = "";
				if(count($order_step_attachments) > 0)
				{
					$attachment_text .= "Directly access the project's attachment section by clicking the following link:" . "\n";
					$attachment_text .= APPLICATION_URL . "/user/project_view_attachments.php?pid=" . $project["project_id"] . "\n\n";

					
					$attachment_text .= "Or directly access the following files in the context of your task:" . "\n";
					foreach($order_step_attachments as $key=>$file)
					{
						$attachment_text .= $file . "\n";

						$file_name = str_replace("/files/orders/". $project["order_number"]. "/", "",$order_step_attachment_file_paths[$key]["path"]);
						
						$filepath = $_SERVER["DOCUMENT_ROOT"] . "/" . $order_step_attachment_file_paths[$key]["path"];
						
						
						if($total_file_size < 7340032 
							and $form->value("order_file_category_attach_in_mails") == 1)
						{
							$actionmail->addAttachment($filepath);
						}
						
					}

				}
				$actionmail->setParam('attachment_text', $attachment_text);
				if(1==1 or $senmail_activated == true)
				{
					$actionmail->send();
				}

				$recipients = $actionmail->getRecipients();

				if ($actionmail->isSuccess() && $recipients) {
					
					foreach($recipients as $id=>$recipient)
					{
						append_task($project["project_order"], $project["order_user"], $recipient->getContent(), "", "", user_id(), FULL_BUDGET_APPROVED, 1, 1);
					}
				}
				*/
			}


			//22. Mini Booklet was accepted
			//send mail to project manager and assign a task for the project manager
			//MAIL TEMPLATE 57
			if (id() == MINI_BOOKLET_APPROVED)
			{
				
				
				$actionmail = new ActionMail(57);
				$actionmail->setParam('id', param("pid"));
				$actionmail->setParam('step', id());

				$attachment_text = "";
				if(count($order_step_attachments) > 0)
				{
					$attachment_text .= "Directly access the project's attachment section by clicking the following link:" . "\n";
					$attachment_text .= APPLICATION_URL . "/user/project_view_attachments.php?pid=" . $project["project_id"] . "\n\n";

					
					$attachment_text .= "Or directly access the following files in the context of your task:" . "\n";
					foreach($order_step_attachments as $key=>$file)
					{
						$attachment_text .= $file . "\n";

						$file_name = str_replace("/files/orders/". $project["order_number"]. "/", "",$order_step_attachment_file_paths[$key]["path"]);
						
						$filepath = $_SERVER["DOCUMENT_ROOT"] . "/" . $order_step_attachment_file_paths[$key]["path"];
						
						
						if($total_file_size < 7340032 
							and $form->value("order_file_category_attach_in_mails") == 1)
						{
							

							if($senmail_activated == true)
							{
								$actionmail->addAttachment($filepath);
							}
						}
						
					}

				}
				$actionmail->setParam('attachment_text', $attachment_text);
				$actionmail->send();
				$recipients = $actionmail->getRecipients();

				if ($actionmail->isSuccess() && $recipients) {
					
					foreach($recipients as $id=>$recipient)
					{
						$link ="project_task_center.php?pid=" . param("pid");
						append_task($project["project_order"],$project["project_retail_coordinator"], $recipient->getContent(), $link, "", user_id(), id(), 1, 0);
					}
				}

			}

			//23. Mini booklet was accepted
			//send mail  to design contracotr if the minibooklet was accepted
			// MAIL Template 58

			/*
			if (id() == MINI_BOOKLET_APPROVED_BY_MANAGEMENT)
			{
				$actionmail = new ActionMail(58);
				$actionmail->setParam('id', param("pid"));
				$actionmail->setParam('step', id());

				$attachment_text = "";
				if(count($order_step_attachments) > 0)
				{
					$attachment_text .= "Directly access the project's attachment section by clicking the following link:" . "\n";
					$attachment_text .= APPLICATION_URL . "/user/project_view_attachments.php?pid=" . $project["project_id"] . "\n\n";

					
					$attachment_text .= "Or directly access the following files in the context of your task:" . "\n";
					foreach($order_step_attachments as $key=>$file)
					{
						$attachment_text .= $file . "\n";

						$file_name = str_replace("/files/orders/". $project["order_number"]. "/", "",$order_step_attachment_file_paths[$key]["path"]);
						
						$filepath = $_SERVER["DOCUMENT_ROOT"] . $order_step_attachment_file_paths[$key]["path"];
						
						
						if($total_file_size < 7340032 
							and $form->value("order_file_category_attach_in_mails") == 1)
						{
							if($senmail_activated == true)
							{
								$actionmail->addAttachment($filepath);
							}
						}
						
					}

				}
				$actionmail->setParam('attachment_text', $attachment_text);
				$actionmail->send();
			}
			*/
			
			//24. send a mail to the client if he has approved the project booklet
			//to remind him to get offers for local construction work
			//only for corporate projects
			// MAIL TEMPLATE 59
			if ((id() == PROJECT_BOOKLET_ACCEPTED_BY_CLIENT 
				or id() == PROJECT_BOOKLET_APPROVED)
				and $project["project_cost_type"] == 1
				and $local_prodcution == true)
			{
				
				$actionmail = new ActionMail(59);
				$actionmail->setParam('id', param("pid"));
				$actionmail->setParam('step', id());

				$attachment_text = "";
				if(count($order_step_attachments) > 0)
				{
					$attachment_text .= "Directly access the project's attachment section by clicking the following link:" . "\n";
					$attachment_text .= APPLICATION_URL . "/user/project_view_attachments.php?pid=" . $project["project_id"] . "\n\n";

					
					$attachment_text .= "Or directly access the following files in the context of your task:" . "\n";
					foreach($order_step_attachments as $key=>$file)
					{
						$attachment_text .= $file . "\n";

						$file_name = str_replace("/files/orders/". $project["order_number"]. "/", "",$order_step_attachment_file_paths[$key]["path"]);
						
						$filepath = $_SERVER["DOCUMENT_ROOT"] . "/" . $order_step_attachment_file_paths[$key]["path"];
						
						
						if($total_file_size < 7340032 
							and $form->value("order_file_category_attach_in_mails") == 1)
						{
							if($senmail_activated == true)
							{
								$actionmail->addAttachment($filepath);
							}
						}
						
					}

				}
				$actionmail->setParam('attachment_text', $attachment_text);
				$actionmail->send();

				
				$recipients = $actionmail->getRecipients();

				if ($actionmail->isSuccess() && $recipients) {
					
					foreach($recipients as $id=>$recipient) {
						$link ="project_task_center.php?pid=" . param("pid");
						append_task2($project["project_order"], $project["order_user"], $recipient->getContent(), $link, $date, user_id(), '650', 1);
					}
				}
				
			}
			elseif ((id() == PROJECT_BOOKLET_ACCEPTED_BY_CLIENT 
				or id() == PROJECT_BOOKLET_APPROVED)
				and $project["project_cost_type"] == 1
				and $local_prodcution == true)
			{
				append_task2($project["project_order"], $project["order_user"], $recipient->getContent(), $link, $date, user_id(), '650', 1);
			}

			
			//25 send mail to client to confirm partial or full delivery of all goods
			// MAIL TEMPLATE 60, 92
			if (id() == DELIVERY_CONFIRMED_FRW)
			{
			
				$arrival_entered = check_if_all_items_hav_arrival_date($project["project_order"], "");
				if ($arrival_entered == 1) // full delivery
				{
					
					$actionmail = new ActionMail(60);
					$actionmail->setParam('id', param("pid"));
					$actionmail->setParam('step', id());

					$attachment_text = "";
					if(count($order_step_attachments) > 0)
					{
						$attachment_text .= "Directly access the project's attachment section by clicking the following link:" . "\n";
						$attachment_text .= APPLICATION_URL . "/user/project_view_attachments.php?pid=" . $project["project_id"] . "\n\n";

						
						$attachment_text .= "Or directly access the following files in the context of your task:" . "\n";
						foreach($order_step_attachments as $key=>$file)
						{
							$attachment_text .= $file . "\n";

							$file_name = str_replace("/files/orders/". $project["order_number"]. "/", "",$order_step_attachment_file_paths[$key]["path"]);
							
							$filepath = $_SERVER["DOCUMENT_ROOT"] . "/" . $order_step_attachment_file_paths[$key]["path"];
							
							
							if($total_file_size < 7340032 
							and $form->value("order_file_category_attach_in_mails") == 1)
							{
								if($senmail_activated == true)
								{
									$actionmail->addAttachment($filepath);
								}

							}
							
						}

					}
					$actionmail->setParam('attachment_text', $attachment_text);
					$actionmail->send();
					$recipients = $actionmail->getRecipients();

					if ($actionmail->isSuccess() && $recipients) {
						
						foreach($recipients as $id=>$recipient) {
							$link ="project_task_center.php?pid=" . param("pid");
							append_task2($project["project_order"], $project["order_user"], $recipient->getContent(), $link, $date, user_id(), id(), 1);
						}
					}
				}
				else //pratial delivery
				{
					//get all items of the forwarder confirming delivery
					$deliverd_items = array();
					$sql = "select order_item_id, order_item_text, order_item_quantity " . 
						   " from order_items " . 
						   " where order_item_order = " . dbquote($project["project_order"]) . 
						   " and order_item_forwarder_address = " . dbquote($logged_user["address"]) . 
						   " and order_item_arrival is not null " . 
						   " and order_item_arrival <> '0000-00-00' " . 
						   " and order_item_arrival_announced <> 1";

					$res = mysql_query($sql) or dberror($sql);
					while ($row = mysql_fetch_assoc($res))
					{
						$deliverd_items[$row["order_item_id"]] = $row["order_item_quantity"] . " * " . $row["order_item_text"];
					}

					if(count($deliverd_items) > 0)
					{
						$list_of_delivered_items = "";

						foreach($deliverd_items as $order_item_id=>$text)
						{
							$list_of_delivered_items .= $text . "\n";

							$sql_u = "update order_items set order_item_arrival_announced = 1 " . 
								     " where order_item_id = " . dbquote($order_item_id);

							$result = mysql_query($sql_u) or dberror($sql_u);
						}



						$actionmail = new ActionMail(92);
						$actionmail->setParam('id', param("pid"));
						$actionmail->setParam('step', id());

						$attachment_text = "";
						if(count($order_step_attachments) > 0)
						{
							$attachment_text .= "Directly access the project's attachment section by clicking the following link:" . "\n";
							$attachment_text .= APPLICATION_URL . "/user/project_view_attachments.php?pid=" . $project["project_id"] . "\n\n";

							
							$attachment_text .= "Or directly access the following files in the context of your task:" . "\n";
							foreach($order_step_attachments as $key=>$file)
							{
								$attachment_text .= $file . "\n";

								$file_name = str_replace("/files/orders/". $project["order_number"]. "/", "",$order_step_attachment_file_paths[$key]["path"]);
								
								$filepath = $_SERVER["DOCUMENT_ROOT"] . "/" . $order_step_attachment_file_paths[$key]["path"];
								
								
								if($total_file_size < 7340032 
								and $form->value("order_file_category_attach_in_mails") == 1)
								{
									if($senmail_activated == true)
									{
										$actionmail->addAttachment($filepath);
									}
								}
								
							}

						}
						$actionmail->setParam('attachment_text', $attachment_text);
						$actionmail->setParam('item_list', $list_of_delivered_items);
						$actionmail->send();
					}
				}
			}
			
			//26. append task depending on the project step

			if (id() == REQUEST_FOR_LAYOUT_PREVIEW_ACCEPTED)
			{
				//task is created by design contractor for himself
				$sql = "select string_text ".
						   "from strings ".
						   "where string_name = 'general_240'";

				$res = mysql_query($sql) or dberror($sql);

				if ($row = mysql_fetch_assoc($res))
				{
					$text = $row["string_text"];
					append_task($project["project_order"], user_id(), $text, $link, $date, user_id(), id(), 1, $form->value("keep_project_state"));
				}

				
			}
			elseif (id() == REQUEST_FOR_BOOKLET_ACCEPTED)
			{
				//task is created by design contractor for himself
				$sql = "select string_text ".
						   "from strings ".
						   "where string_name = 'general_422'";

				$res = mysql_query($sql) or dberror($sql);

				if ($row = mysql_fetch_assoc($res))
				{
					$text = $row["string_text"];
					append_task($project["project_order"], user_id(), $text, $link, $date, user_id(), '424', 1, $form->value("keep_project_state"));
				}
			}
			elseif (id() == REQUEST_FOR_MINI_BOOKLET_ACCEPTED)
			{
				//task is created by design contractor for himself
				$sql = "select string_text ".
						   "from strings ".
						   "where string_name = 'general_410'";

				$res = mysql_query($sql) or dberror($sql);

				if ($row = mysql_fetch_assoc($res))
				{
					$text = $row["string_text"];
					append_task($project["project_order"], user_id(), $text, $link, $date, user_id(), '411', 1, $form->value("keep_project_state"));
				}
			}
			elseif (id() == REQUEST_FOR_OFFER_ACCEPTED)
			{
				//task is created by supplier for himself
				$sql = "select string_text ".
						   "from strings ".
						   "where string_name = 'general_530'";

				$res = mysql_query($sql) or dberror($sql);

				if ($row = mysql_fetch_assoc($res))
				{
					$text = $row["string_text"];
					append_task($project["project_order"], user_id(), $text, $link, $date, user_id(), id(), 1, $form->value("keep_project_state"));
				}
			}
			elseif (id() == REQUEST_FOR_DELIVERY_ACCEPTED)
			{
				//task is created by forwarder for himself
				$sql = "select string_text ".
						   "from strings ".
						   "where string_name = 'general_740'";

				$res = mysql_query($sql) or dberror($sql);

				if ($row = mysql_fetch_assoc($res))
				{
					$text = $row["string_text"];
					append_task($project["project_order"], user_id(), "Please confirm delivery on arrival!", $link, $date, user_id(), id(), 1, $form->value("keep_project_state"));
				}
			}
			elseif (id() == REQUEST_FOR_DELIVERY_SUBMITTED)
			{
				set_visibility_in_delivery_schedule($project["project_order"]);
				
				//task is created by for all suppliers
				$sql = "select string_text ".
						   "from strings ".
						   "where string_name = 'general_730'";

				$res = mysql_query($sql) or dberror($sql);

				if ($row = mysql_fetch_assoc($res))
				{
					$text = $row["string_text"];

					foreach ($recipients as $key=>$recipient)
					{
						if ($form->value("R" . $recipient["id"]) == 1)
						{
							if(array_key_exists($recipient["id"], $involved_suppliers))
							{
								foreach ($involved_suppliers[$recipient["id"]] as $user_id=>$supplier_data)
								{
									
									$link ="project_task_center.php?pid=" . param("pid");
									append_task($project["project_order"],$user_id, $text, $link, "", user_id(), id(), 1, 0);

									//send mail to the supplier to perform step 745

									$mail = new PHPMailer();
									$mail->Subject = MAIL_SUBJECT_PREFIX . ": " . $form->value("subject") . $revision_subject;
									$mail->SetFrom($form->value("sender_email"), $form->value("sender_name"));
									$mail->AddReplyTo($form->value("sender_email"), $form->value("sender_name"));

									$bodytext0 = str_replace("\r\n", "\n", $text)  . $revision_text;
									$link ="project_task_center.php?pid=" . param("pid");
									$bodytext = $bodytext0 . "\n\nClick below to have direct access to the project:\n";
									$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";

									if(count($order_step_attachments) > 0)
									{
										$bodytext .= "Directly access the project's attachment section by clicking the following link:" . "\n";
										$bodytext .= APPLICATION_URL . "/user/project_view_attachments.php?pid=" . $project["project_id"] . "\n\n";
										
										$bodytext .= "Or directly access the following files in the context of your task:" . "\n";
										foreach($order_step_attachments as $key=>$file)
										{
											$bodytext .= $file . "\n";

											$file_name = str_replace("/files/orders/". $project["order_number"]. "/", "",$order_step_attachment_file_paths[$key]["path"]);
											
											$filepath = $_SERVER["DOCUMENT_ROOT"] . "/" . $order_step_attachment_file_paths[$key]["path"];
											
											if($total_file_size < 7340032 
												and $form->value("order_file_category_attach_in_mails") == 1)
											{
												$mail->AddAttachment($filepath);
											}
											
										}
									}

									$mail->Body = $bodytext;
									
									$mail->AddAddress($supplier_data["email"]);
									
									//add cc recipients
									if($supplier_data["cc"])
									{
										$mail->AddCC($supplier_data["cc"]);
									}
									if($supplier_data["deputy"])
									{
										$mail->AddCC($supplier_data["deputy"]);
									}

																			
									//send mail
									$m_result = false;
									if($senmail_activated == true)
									{
										$m_result = $mail->send();

										
									}

									if($m_result)
									{
										append_mail($project["project_order"], $supplier_data["id"], user_id(), $text, id(), 1, $order_revisioned, 0);
									}

								}
							}
						}
					}
				}

			}


			//27. delete alls tasks id delivery is confirmed
			if (id() == DELIVERY_CONFIRMED)
			{
				//unclear what it should by after introduction of step 840
				/*
				$sql = "delete from tasks ".
					   "where task_user = " . user_id() .
					   "   and task_order = " . $project["project_order"];

				mysql_query($sql) or dberror($sql);
				*/
			}




			//28. append auto item to the list of materials
            if (count($recipients) > 0 
				and $show_standardtexts == 1 
				and $form->value("standard_text") > 0)
            {
				$taskwork_id =  $form->value("standard_text");
				//append_auto_item($project["project_order"], $taskwork_id); //appneds the item to the list of materials			
			}


			//29. append record to table actual_order_states
			append_order_state($project["project_order"], id(), 1, 1, $form->value("keep_project_state"));

			
			//30. Update CMS Workflow States

			if (id() == CMS_COMPLETTION_LOGISTICS)
			{
				$approval_fields = "project_cost_cms_completed = 1, ";
				$approval_fields .= "project_cost_cms_completed_by = '" . user_id() . "', ";
				$approval_fields .= "project_cost_cms_completion_date = '" . date("Y.m.d") . "'";

				$sql = "update project_costs set " .
					   $approval_fields . 
					   " where project_cost_order = " . $project["project_order"];
				$result = mysql_query($sql) or dberror($sql);
			}
			elseif (id() == CMS_COMPLETTION_CLIENT)
			{
				$approval_fields = "project_cost_cms_completed2 = 1, ";
				$approval_fields .= "project_cost_cms_completed2_by = '" . user_id() . "', ";
				$approval_fields .= "project_cost_cms_completion2_date = '" . date("Y.m.d") . "'";

				$sql = "update project_costs set " .
					   $approval_fields . 
					   " where project_cost_order = " . $project["project_order"];
				$result = mysql_query($sql) or dberror($sql);



				//check if project can be put to the archive
				if($pos_pictures_uploaded == true 
					and $project["project_actual_opening_date"] != NULL 
					and $project["project_actual_opening_date"] != '0000-00-00'
					and ($project["order_archive_date"] == NULL 
					or $project["order_archive_date"] == '0000-00-00'))
				{
					if($project["order_actual_order_state_code"] < '890')
					{
						append_order_state($project["project_order"], '890', 1, 1);
						set_archive_date($project["project_order"]);

						$sql = "delete from tasks ".
							   "where task_user = " . dbquote(user_id()) .
							   "   and task_order = " . dbquote($project["project_order"]);

						mysql_query($sql) or dberror($sql);
					}
				}

				if($pos_pictures_uploaded == false) 
				{
					$actionmail = new ActionMail(65);
					$actionmail->setParam('id', param("pid"));
					$actionmail->setParam('step', id());
					

					if($senmail_activated == true)
					{
						$actionmail->send();
					}

					$recipients = $actionmail->getRecipients();

					if ($actionmail->isSuccess() && $recipients) {

						foreach($recipients as $id=>$recipient)
						{
							append_task($project["project_order"], $project["order_user"], $recipient->getContent(), "", "", user_id(), CMS_APPROVE_BY_CONTROLLER, 1, 1);
						}
					}
				}
			}
			elseif (id() == CMS_REJECT_LOGISTICS)
			{
				$approval_fields = "project_cost_cms_completed = 0, ";
				$approval_fields .= "project_cost_cms_completed_by = NULL, ";
				$approval_fields .= "project_cost_cms_completion_date = NULL";

				$sql = "update project_costs set " .
					   $approval_fields . 
					   " where project_cost_order = " . $project["project_order"];
				$result = mysql_query($sql) or dberror($sql);
				
			}
			elseif (id() == CMS_REJECT_CLIENT)
			{
				$approval_fields = "project_cost_cms_completed2 = 0, ";
				$approval_fields .= "project_cost_cms_completed2_by = NULL, ";
				$approval_fields .= "project_cost_cms_completion2_date = NULL";

				$sql = "update project_costs set " .
					   $approval_fields . 
					   " where project_cost_order = " . $project["project_order"];
				$result = mysql_query($sql) or dberror($sql);

				append_task($project["project_order"], $recipient["id"], $form->value("body_text") . $revision_text, $link, $date, user_id(), '851', 1, $form->value("keep_project_state"));


			}
			elseif (id() == CMS_COMPLETTION_DEVELOPMENT)
			{
				$approval_fields = "project_cost_cms_completed = 1, ";
				$approval_fields = "project_cost_cms_completed2 = 1, ";
				$approval_fields = "project_cost_cms_approved = 1, ";
				$approval_fields .= "project_cost_cms_approved_by = '" . user_id() . "', ";
				$approval_fields .= "project_cost_cms_approved_date = '" . date("Y.m.d") . "'";

				$sql = "update project_costs set " .
					   $approval_fields . 
					   " where project_cost_order = " . $project["project_order"];
				$result = mysql_query($sql) or dberror($sql);

				$sql = "delete from tasks ".
					   "where task_order_state in(88, 89, 90,91, 93) " . 
					   "   and task_order = " . dbquote($project["project_order"]);

				mysql_query($sql) or dberror($sql);


				if($project["project_cost_type"] > 1) // non corporate projects
				{
					//check if project can be put to the archive
					if($pos_pictures_uploaded == true 
						and $project["project_actual_opening_date"] != NULL 
						and $project["project_actual_opening_date"] != '0000-00-00'
						and ($project["order_archive_date"] == NULL 
						or $project["order_archive_date"] == '0000-00-00'))
					{
						if($project["order_actual_order_state_code"] < '890')
						{
							append_order_state($project["project_order"], '890', 1, 1);
							set_archive_date($project["project_order"]);

							$sql = "delete from tasks ".
								   "where task_user = " . dbquote(user_id()) .
								   "   and task_order = " . dbquote($project["project_order"]);

							mysql_query($sql) or dberror($sql);
						}
					}
					
					if($pos_pictures_uploaded == false) 
					{
						$actionmail = new ActionMail(65);
						$actionmail->setParam('id', param("pid"));
						$actionmail->setParam('step', id());
						

						if($senmail_activated == true)
						{
							$actionmail->send();
						}

						$recipients = $actionmail->getRecipients();

						if ($actionmail->isSuccess() && $recipients) {

							foreach($recipients as $id=>$recipient)
							{
								append_task($project["project_order"], $project["order_user"], $recipient->getContent(), "", "", user_id(), CMS_COMPLETTION_DEVELOPMENT, 1, 1);
							}
						}
					}
				}

			}
			elseif (id() == CMS_REJECT_BY_CONTROLLER)
			{
				$approval_fields = "project_cost_cms_approved = 0, ";
				$approval_fields .= "project_cost_cms_approved_by = NULL, ";
				$approval_fields .= "project_cost_cms_approved_date = NULL";

				$sql = "update project_costs set " .
					   $approval_fields . 
					   " where project_cost_order = " . $project["project_order"];
				$result = mysql_query($sql) or dberror($sql);
			}
			elseif (id() == CMS_APPROVE_BY_CONTROLLER)
			{
				$approval_fields = "project_cost_cms_completed = 1, ";
				$approval_fields = "project_cost_cms_completed2 = 1, ";
				$approval_fields = "project_cost_cms_approved = 1, ";

				$approval_fields = "project_cost_cms_controlled = 1, ";
				$approval_fields .= "project_cost_cms_controlled_by = '" . user_id() . "', ";
				$approval_fields .= "project_cost_cms_controlled_date = '" . date("Y.m.d") . "'";

				$sql = "update project_costs set " .
					   $approval_fields . 
					   " where project_cost_order = " . $project["project_order"];
				$result = mysql_query($sql) or dberror($sql);


				$sql = "delete from tasks ".
					   "where task_order_state in(88, 89, 90,91, 92, 93, 96) " . 
					   "   and task_order = " . dbquote($project["project_order"]);

				mysql_query($sql) or dberror($sql);


				//check if project can be put to the archive
				if($pos_pictures_uploaded == true 
					and $project["project_actual_opening_date"] != NULL 
					and $project["project_actual_opening_date"] != '0000-00-00'
					and ($project["order_archive_date"] == NULL 
					or $project["order_archive_date"] == '0000-00-00'))
				{
					if($project["order_actual_order_state_code"] < '890')
					{
						append_order_state($project["project_order"], '890', 1, 1);
						set_archive_date($project["project_order"]);

						$sql = "delete from tasks ".
							   "where task_user = " . dbquote(user_id()) .
							   "   and task_order = " . dbquote($project["project_order"]);

						mysql_query($sql) or dberror($sql);
					}
				}

				if($pos_pictures_uploaded == false) 
				{
					$actionmail = new ActionMail(65);
					$actionmail->setParam('id', param("pid"));
					$actionmail->setParam('step', id());
					

					if($senmail_activated == true)
					{
						$actionmail->send();
					}

					$recipients = $actionmail->getRecipients();

					if ($actionmail->isSuccess() && $recipients) {

						foreach($recipients as $id=>$recipient)
						{
							append_task($project["project_order"], $project["order_user"], $recipient->getContent(), "", "", user_id(), CMS_APPROVE_BY_CONTROLLER, 1, 1);
						}
					}
				}
			}
			
			//31. Update development and logistics state
			if($action_parameter["used_for_logistics"] != 1 
				and ($_keep_project_state != 1 and $form->value("keep_project_state") != 1)
				and id() < '890')
			{
				$result = save_project_development_status($project["project_order"], id());
			}
			elseif($action_parameter["used_for_logistics"] == 1 
				and ($_keep_project_state != 1 and $form->value("keep_project_state") != 1)
				)
			{
				$result = save_logistic_state_code($project["project_order"], id()  and id() < '890');
			}
			

			//32. Update sales area according to approved layout
			/*
			if(id() == MINI_BOOKLET_APPROVED_BY_MANAGEMENT)
			{
				$result = update_sales_area_from_approved_layout($project["project_id"], $project["project_order"], $form->value("store_retailarea"));
			}
			*/


			//33. Update design objectives
			/*
			if(id() == MINI_BOOKLET_APPROVED_BY_MANAGEMENT)
			{
				// delete all records form project_items belonign to the project

				$project_item_fields = array();
				$project_item_values = array();

				$sql = "delete from project_items where project_item_project = " . $form->value("pid");
				mysql_query($sql) or dberror($sql);

				// insert records into table project_items
				foreach ($design_objectives_listbox_names as $element)
				{
					$project_item_fields[0] = "project_item_project";
					$project_item_values[0] = $form->value("pid");

					$project_item_fields[1] = "project_item_item";
					$project_item_values[1] = $form->value($element);
					
					$project_item_fields[2] = "date_created";
					$project_item_values[2] = "current_timestamp";

					$project_item_fields[3] = "date_modified";
					$project_item_values[3] = "current_timestamp";
							
					if (isset($_SESSION["user_login"]))
					{
						$project_item_fields[4] = "user_created";
						$project_item_values[4] = dbquote($_SESSION["user_login"]);

						$project_item_fields[5] = "user_modified";
						$project_item_values[5] = dbquote($_SESSION["user_login"]);
					}
					$sql = "insert into project_items (" . join(", ", $project_item_fields) . ") values (" . join(", ", $project_item_values) . ")";
					mysql_query($sql) or dberror($sql);
				}
				
				$project_item_fields = array();
				$project_item_values = array();
				
				foreach ($design_objectives_checklist_names as $name)
				{
					$tmp = $form->value($name);
					foreach ($tmp as $value)
					{
					$project_item_fields[0] = "project_item_project";
					$project_item_values[0] =$form->value("pid");

					$project_item_fields[1] = "project_item_item";
					$project_item_values[1] = $value;
					
					$project_item_fields[2] = "date_created";
					$project_item_values[2] = "current_timestamp";

					$project_item_fields[3] = "date_modified";
					$project_item_values[3] = "current_timestamp";
							
					if (isset($_SESSION["user_login"]))
					{
						$project_item_fields[4] = "user_created";
						$project_item_values[4] = dbquote($_SESSION["user_login"]);

						$project_item_fields[5] = "user_modified";
						$project_item_values[5] = dbquote($_SESSION["user_login"]);
					}
					$sql = "insert into project_items (" . join(", ", $project_item_fields) . ") values (" . join(", ", $project_item_values) . ")";
					mysql_query($sql) or dberror($sql);
					}
				}
			}
			*/


			//34. Design Briefing Updates
			if (id() == SUBMIT_DESIGN_BRIEFING)
			{
				$submitted_by = 0;
				$sql = "select project_design_brief_submitted_by 
				        from project_design_briefs 
						where project_design_brief_project_id = " . $project["project_id"];
				
				$res = mysql_query($sql);
				if($row = mysql_fetch_assoc($res))
				{
					$submitted_by = $row['project_design_brief_submitted_by'];
				}

				if($submitted_by > 0) {
					$fields = "project_design_brief_resubmitted_by = " . user_id() . ", ";
					$fields .= "project_design_brief_resubmission_date = " . dbquote(date("Y-m-d H:i:s"));

				}
				else {
					$fields = "project_design_brief_submitted_by = " . user_id() . ", ";
					$fields .= "project_design_brief_submission_date = " . dbquote(date("Y-m-d H:i:s"));
				}
				
				
				$sql = "update project_design_briefs set " .
					   $fields . 
					   " where project_design_brief_project_id = " . $project["project_id"];
				$result = mysql_query($sql) or dberror($sql);
			}
			elseif (id() == SUBMIT_DESIGN_BRIEFING)
			{
				$fields = "project_design_brief_resubmitted_by = " . user_id() . ", ";
				$fields .= "project_design_brief_resubmission_date = " . dbquote(date("Y-m-d H:i:s"));

				$sql = "update project_design_briefs set " .
					   $fields . 
					   " where project_design_brief_project_id = " . $project["project_id"];
				$result = mysql_query($sql) or dberror($sql);
			}
			elseif (id() == REJECT_DESIGN_BRIEFING)
			{
				$fields = "project_design_brief_rejected_by = " . user_id() . ", ";
				$fields .= "project_design_brief_rejection_date = " . dbquote(date("Y-m-d H:i:s"));

				$sql = "update project_design_briefs set " .
					   $fields . 
					   " where project_design_brief_project_id = " . $project["project_id"];
				$result = mysql_query($sql) or dberror($sql);
			}

			

			//$params = "?pid=" . param("pid") . "&num_mails=" . $num_mails . "&num_tasks=" . $num_tasks;
            //$link = "project_send_message_confirm.php" . $params;
            $link = "project_task_center.php?pid=" . param("pid");

			redirect ($link);
		}
    }

      
    /********************************************************************
        render page
    *********************************************************************/
    $page = new Page("projects");

    require "include/project_page_actions.php";

    $page->header();
    $page->title($action_parameter["action_name"]);
    $form->render();

	?>

	<script type="text/javascript">
	  jQuery(document).ready(function($) {
	  $('#ccmails_selector').click(function(e) {
		e.preventDefault();
		$.nyroModalManual({
		  url: '/shared/select_mail_recipients.php'
		});
		return false;
	  });
	});
	</script>

	<?php
	require_once "include/project_footer_logistic_state.php";
    $page->footer();
}

?>