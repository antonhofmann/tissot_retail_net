<?php 
	
/********************************************************************

ajx_upload_shipping_documents.php

Upload shipping documents

Created by:     Anton Hofmann (aho@mediaparx.ch)
Date created:   2015-01-14
Modified by:    Anton Hofmann (aho@mediaparx.ch)
Date modified:  2015-01-14
Version:        1.0.0

Copyright (c) 2014, Swatch AG, All Rights Reserved.
*********************************************************************/
	
	session_name("retailnet");
	session_start();

	
	require "../include/frame.php";
	require "include/get_functions.php";


	switch ($_REQUEST['section']) {
		
		// save files
		case 'save':
			if( (has_access("has_access_to_shipping_details_in_projects") 
				or has_access("has_access_to_shipping_details_in_orders"))
				&& $_REQUEST["order_id"] 
				&& $_REQUEST["supplier_id"] 
				&& $_REQUEST["forwarder_id"] 
				&& $_REQUEST["file"]) {
				
				
				$model = new Model(Connector::DB_CORE);
				
				$order_id = $_REQUEST["order_id"];

				$orderNumber = $_REQUEST["order_number"];
				$category = $_REQUEST["category"];

				foreach($_REQUEST["file"] as $key => $file) {

					// insert new file
					if (strpos($file, '/tmp/') !== false) {
						
						$dir = "/files/orders/$orderNumber";
						$filename = basename($file);
						$extension = File::extension($filename);
						
						// move temporary file to target destination
						$copy = Upload::moveTemporaryFile($file, $dir);
						
						if ($copy) { 
							
							$type = $model->query("
								SELECT file_type_id 
								FROM file_types 
								WHERE file_type_extension LIKE '%$extension%'
							")->fetch();
							
							// save file in db
							$sth = $model->db->prepare("
								INSERT INTO order_files (
									order_file_order,
									order_file_title,
									order_file_path,
									order_file_type,
									order_file_category,
									order_file_owner,
									date_created,
									user_created
								) VALUES (
									:order, :title, :file, :type, :category, :owner, CURRENT_TIMESTAMP, :user
								)
							");
								
							
							$action = $sth->execute(array(
								'order' => $order_id,
								'title' => $_REQUEST['title'][$key],
								'file' => "$dir/$filename",
								'type' => $type['file_type_id'],
								'category' => $category,
								'owner' => User::instance()->id,
								'user' => User::instance()->login
							));

							$doc_id = $model->db->lastinsertId();

							
							
							//update order_shipping documents
							if($doc_id)
							{
								
								//check if doc has a file
								$doc = $model->query("
									SELECT * 
									FROM order_shipping_documents
									WHERE order_shipping_document_order_id = " . $order_id .
									" and order_shipping_document_supplier_id = " . $_REQUEST["supplier_id"] . 
									" and order_shipping_document_forwarder_id = " . $_REQUEST["forwarder_id"] .
									" and order_shipping_document_standard_document_id = " . $_REQUEST['doc'][$key]

								)->fetch();

								if($doc["order_shipping_document_file_order_file_id"] > 0)
								{
									

									$query = "INSERT INTO order_shipping_documents
											(order_shipping_document_order_id, order_shipping_document_supplier_id, order_shipping_document_forwarder_id, order_shipping_document_standard_document_id, order_shipping_document_name, order_shipping_document_file_order_file_id, user_created, date_created)
											VALUES (" .
											$doc['order_shipping_document_order_id'] . ", " . 
											$doc['order_shipping_document_supplier_id'] . ", " .
											$doc['order_shipping_document_forwarder_id'] . ", " .
											$_REQUEST['doc'][$key] . ", " .
											"'" . $doc['order_shipping_document_name'] . "', " .
											":doc_id, :user, CURRENT_TIMESTAMP)";
											

									$sth = $model->db->prepare($query);
									$action = $sth->execute(array(
										'doc_id' => $doc_id,
										'user' => User::instance()->login
									));
								}
								else
								{
									// update file title
									$sth = $model->db->prepare("
										UPDATE order_shipping_documents SET
											order_shipping_document_file_order_file_id = :doc_id
										WHERE order_shipping_document_order_id = :order_id
										AND order_shipping_document_supplier_id =  :supplier_id
										AND order_shipping_document_forwarder_id = :forwarder_id
										AND order_shipping_document_id = :id
									");
									
									
									$action = $sth->execute(array(
										'doc_id' => $doc_id,
										'order_id' => $_REQUEST['order_id'],
										'supplier_id' => $_REQUEST['supplier_id'],
										'forwarder_id' => $_REQUEST['forwarder_id'],
										'id' => $doc["order_shipping_document_id"]
									));

								}
							}
						}
					} 
					// update file data
					elseif (isset($_REQUEST['title'][$key])) {
						
						//var_dump($_REQUEST);cde();
						
						// order file exist
						$result = $model->query("
							SELECT order_file_id 
							FROM order_files 
							WHERE order_file_id = $key
						")->fetch();
						
						// update file title
						if ($result['order_file_id']) { 
							
							$sth = $model->db->prepare("
								UPDATE order_files SET
									order_file_title = :title
								WHERE order_file_id = :id
							");
							
							
							$action = $sth->execute(array(
								'title' => $_REQUEST['title'][$key],
								'id' => $key
							));
							
						}
					}
				}
				
				$response = array(
					'response' => $action,
					'reload' => true
				);
			}
			
		break;


		// delete file
		case 'delete':
			
			if ($_REQUEST['id']) {
					
				$id = $_REQUEST['id'];
				$model = new Model(Connector::DB_CORE);


				$sth = $model->db->prepare("
						UPDATE order_shipping_documents SET
							order_shipping_document_file_order_file_id = :file_id
						WHERE order_shipping_document_file_order_file_id = :id
					");
						
						
				$action = $sth->execute(array(
					'file_id' => NULL,
					'id' => $id
				));

									
				$file = $model->query("
					SELECT *
					FROM order_files
					WHERE order_file_id = $id
				")->fetch();
					
				if (file_exists($_SERVER['DOCUMENT_ROOT'].$file['order_file_path'])) {
			
					$filename = basename($file['order_file_path']);
					$dirpath = file::dirpath($file['order_file_path']);
			
					file::remove($file['order_file_path']);
					file::remove("$dirpath/thumbnail/$filename");
			
					
					$delete = $model->db->exec("
						DELETE FROM order_files
						WHERE order_file_id = $id
					");

					$response = array($filename => $delete);



					$delete = $model->db->exec("
						DELETE FROM order_file_addresses
						WHERE order_file_address_file = $id
					");

					$response = array($filename => $delete);
				}
			}
		
		break;
		
		
		// load files
		default:
			
			$order_id = $_REQUEST['order_id'];
			$supplier_id = $_REQUEST['supplier_id'];
			$forwarder_id = $_REQUEST['forwarder_id'];

			
			if ($order_id) {
				
				$query = "SELECT order_file_id AS id, order_file_title AS title, " . 
						 "order_file_description AS description, order_file_path AS file, " .
					     "order_shipping_document_id as doc_id, " .
					     "order_shipping_document_standard_document_id as sdoc_id " .
						 "FROM order_shipping_documents " . 
						 "LEFT JOIN order_files on order_file_id = order_shipping_document_file_order_file_id " . 
						 "WHERE order_shipping_document_file_order_file_id > 0 " . 
					     " AND order_shipping_document_order_id = " . $order_id .
						 " AND order_shipping_document_supplier_id = " . $supplier_id . 
						 " AND order_shipping_document_forwarder_id = " . $forwarder_id;

				
				//echo $query;				
				$model = new Model(Connector::DB_CORE);
					
				$result = $model->query($query)->fetchAll();
				
				$response = Upload::get($result);
			}
			
		break;
	}

	//var_dump($response);

	header('Content-Type: text/json');
	echo json_encode($response);
?>	