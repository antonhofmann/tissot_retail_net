<?php
/********************************************************************

    snagging_template.php

    Creation and mutation of snagging_list templates.

    Created by:     Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2003-05-22
    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date modified:  2003-05-29
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../admin/snagging_elements.php";

//check_access("can_edit_catalog");

$sql= "select snagging_list_id, snagging_list_name ".
      "from snagging_lists ";

$form = new Form("snagging_list_elements", "snagging");
$form->add_hidden("pid", param("id"));

$list = new SnaggingList(param("id"), param("template"));
$list->load_structure();
$list->edit();

$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button("save_elements", "Save");


$form->populate();
$form->process();

if ($form->button("save_elements"))
{
    echo "Save-function not yet implemented";

}

$page = new Page("projects");
require "include/project_page_actions.php";
$page->header();

$page->title("Edit Snagging List Content");
$form->render();
$page->footer();