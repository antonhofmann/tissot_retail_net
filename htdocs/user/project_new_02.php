<?php
/********************************************************************

    project_new_02.php

    Creation of a new project step 02

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-07-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-07-14
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_create_new_projects");

set_referer("project_new_03.php");

if(!isset($_SESSION["new_project_step_1"]))
{
	$link = "project_new_01.php";
	redirect($link);
}
if(!isset($_SESSION["new_project_step_2"]))
{
	$_SESSION["new_project_step_2"] = array();
}

if(count($_POST) == 0 and isset($_SESSION["new_project_step_2"]))
{
	$_SESSION["new_project_step_2"]["action"] = "";
	foreach($_SESSION["new_project_step_2"] as $key=>$value)
	{
		register_param($key);
		param($key, $value);
	}
}


/********************************************************************
    prepare all data needed
*********************************************************************/
// get user data
$user = get_user(user_id());

// get users' company address
$address = get_address($user["address"]);

// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";

$country_phone_prefix = '';
if(param("franchisee_address_country"))
{
	$country_phone_prefix = get_country_phone_prefix(param("franchisee_address_country")); 
}


$franchisee_address = array();

$franchisee_address["address_id"] = 0;
$franchisee_address["address_company"] = "";
$franchisee_address["address_company2"] = "";
$franchisee_address["address_address"] = "";
$franchisee_address["address_street"] = "";
$franchisee_address["address_streetnumber"] = "";
$franchisee_address["address_address2"] = "";
$franchisee_address["address_zip"] = "";
$franchisee_address["address_place"] = "";
$franchisee_address["address_place_id"] = 0;
$franchisee_address["place_province"] = 0;
$franchisee_address["address_country"] = 0;
$franchisee_address["address_phone"] = "";
$franchisee_address["address_phone_country"] = $country_phone_prefix;
$franchisee_address["address_phone_area"] = "";
$franchisee_address["address_phone_number"] = "";

$franchisee_address["address_mobile_phone"] = "";
$franchisee_address["address_mobile_phone_country"] = $country_phone_prefix;
$franchisee_address["address_mobile_phone_area"] = "";
$franchisee_address["address_mobile_phone_number"] = "";

$franchisee_address["address_email"] = "";
$franchisee_address["address_contact_name"] = "";
$franchisee_address["address_website"] = "";

if(param('franchisee_address_id') > 0 and param('franchisee_address_id') < 999999999)
{
	$f_address = get_address(param('franchisee_address_id'));

	$franchisee_address["address_id"] = $f_address["id"];
	$franchisee_address["address_company"] = $f_address["company"];
	$franchisee_address["address_company2"] = $f_address["company2"];
	$franchisee_address["address_address"] = $f_address["address"];
	$franchisee_address["address_street"] = $f_address["street"];
	$franchisee_address["address_streetnumber"] = $f_address["streetnumber"];
	$franchisee_address["address_address2"] = $f_address["address2"];
	$franchisee_address["address_zip"] = $f_address["zip"];
	$franchisee_address["address_place"] = $f_address["place"];
	$franchisee_address["address_place_id"] = $f_address["place_id"];
	$franchisee_address["place_province"] = $f_address["place_province"];
	$franchisee_address["address_country"] = $f_address["country"];
	$franchisee_address["address_phone"] = $f_address["phone"];
	$franchisee_address["address_phone_country"] = $f_address["phone_country"];
	$franchisee_address["address_phone_area"] = $f_address["phone_area"];
	$franchisee_address["address_phone_number"] = $f_address["phone_number"];
	$franchisee_address["address_mobile_phone"] = $f_address["mobile_phone"];
	$franchisee_address["address_mobile_phone_country"] = $f_address["mobile_phone_country"];
	$franchisee_address["address_mobile_phone_area"] = $f_address["mobile_phone_area"];
	$franchisee_address["address_mobile_phone_number"] = $f_address["mobile_phone_number"];
	$franchisee_address["address_email"] = $f_address["email"];
	$franchisee_address["address_contact_name"] = $f_address["contact_name"];
	$franchisee_address["address_website"] = $f_address["website"];
}
elseif($_SESSION["new_project_step_1"]["posaddress_id"] > 1)
{
	$sql = "select posaddress_franchisee_id ".
		   "from posaddresses ".
		   "where posaddress_id  = " . dbquote($_SESSION["new_project_step_1"]["posaddress_id"]);


	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);

	$f_address = get_address($row["posaddress_franchisee_id"]);

	$franchisee_address["address_id"] = $f_address["id"];
	$franchisee_address["address_company"] = $f_address["company"];
	$franchisee_address["address_company2"] = $f_address["company2"];
	$franchisee_address["address_address"] = $f_address["address"];
	$franchisee_address["address_street"] = $f_address["street"];
	$franchisee_address["address_streetnumber"] = $f_address["streetnumber"];
	$franchisee_address["address_address2"] = $f_address["address2"];
	$franchisee_address["address_zip"] = $f_address["zip"];
	$franchisee_address["address_place"] = $f_address["place"];
	$franchisee_address["address_place_id"] = $f_address["place_id"];
	$franchisee_address["place_province"] = $f_address["place_province"];
	$franchisee_address["address_country"] = $f_address["country"];
	$franchisee_address["address_phone"] = $f_address["phone"];
	$franchisee_address["address_phone_country"] = $f_address["phone_country"];
	$franchisee_address["address_phone_area"] = $f_address["phone_area"];
	$franchisee_address["address_phone_number"] = $f_address["phone_number"];
	$franchisee_address["address_mobile_phone"] = $f_address["mobile_phone"];
	$franchisee_address["address_mobile_phone_country"] = $f_address["mobile_phone_country"];
	$franchisee_address["address_mobile_phone_area"] = $f_address["mobile_phone_area"];
	$franchisee_address["address_mobile_phone_number"] = $f_address["mobile_phone_number"];
	$franchisee_address["address_email"] = $f_address["email"];
	$franchisee_address["address_contact_name"] = $f_address["contact_name"];
	$franchisee_address["address_website"] = $f_address["website"];
}
elseif(array_key_exists("project_relocated_posaddress_id", $_SESSION["new_project_step_1"]) 
    and $_SESSION["new_project_step_1"]["project_relocated_posaddress_id"] > 1)
{
	$sql = "select posaddress_franchisee_id ".
		   "from posaddresses ".
		   "where posaddress_id  = " . dbquote($_SESSION["new_project_step_1"]["project_relocated_posaddress_id"]);


	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);

	$f_address = get_address($row["posaddress_franchisee_id"]);

	$franchisee_address["address_id"] = $f_address["id"];
	$franchisee_address["address_company"] = $f_address["company"];
	$franchisee_address["address_company2"] = $f_address["company2"];
	$franchisee_address["address_address"] = $f_address["address"];
	$franchisee_address["address_street"] = $f_address["street"];
	$franchisee_address["address_streetnumber"] = $f_address["streetnumber"];
	$franchisee_address["address_address2"] = $f_address["address2"];
	$franchisee_address["address_zip"] = $f_address["zip"];
	$franchisee_address["address_place"] = $f_address["place"];
	$franchisee_address["address_place_id"] = $f_address["place_id"];
	$franchisee_address["place_province"] = $f_address["place_province"];
	$franchisee_address["address_country"] = $f_address["country"];
	$franchisee_address["address_phone"] = $f_address["phone"];
	$franchisee_address["address_phone_country"] = $f_address["phone_country"];
	$franchisee_address["address_phone_area"] = $f_address["phone_area"];
	$franchisee_address["address_phone_number"] = $f_address["phone_number"];
	$franchisee_address["address_mobile_phone"] = $f_address["mobile_phone"];
	$franchisee_address["address_mobile_phone_country"] = $f_address["mobile_phone_country"];
	$franchisee_address["address_mobile_phone_area"] = $f_address["mobile_phone_area"];
	$franchisee_address["address_mobile_phone_number"] = $f_address["mobile_phone_number"];
	$franchisee_address["address_email"] = $f_address["email"];
	$franchisee_address["address_contact_name"] = $f_address["contact_name"];
	$franchisee_address["address_website"] = $f_address["website"];
}


//get selection of franchisee addresses
$franchisee_addresses = array();
if($_SESSION["new_project_step_1"]["project_cost_type"] == 1) //corporate
{
	$sql = "select count(address_id) as num_recs from addresses " . 
		   "where address_type <> 7 ". 
		   " and address_canbefranchisee = 1 " . 
		   " and address_active = 1 " . 
		   " and address_client_type > 1 " . 
		   " and address_country = " . $user["country"] . 
		   " and address_parent = " . $user["address"];
		   " order by address_company";

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		if($row["num_recs"] > 1) // the client has more than 1 franchisee address in the context of corporate projects
		{
			$franchisee_addresses = "select address_id, concat(address_company, ', ', address_place) as company " . 
									   "from addresses " .
									   "where address_type <> 7 " .
									   " and address_canbefranchisee = 1 " .
									   " and address_active = 1 " . 
									   " and address_client_type > 1 " . 
									   " and address_country = " . $user["country"] . 
									   " and address_parent = " . $user["address"] .
									   " and address_id <> " . $user["address"] . 
									   " order by address_company";


			if($user["address"] == 13) // Tissot ag general
			{
				$franchisee_addresses = "select address_id, concat(address_company, ', ', address_place) as company " . 
										   "from addresses " .
										   "where address_type <> 7 " .
										   " and address_canbefranchisee = 1 " .
										   " and address_active = 1 " . 
										   " and address_client_type > 1 " . 
										   " and address_country = " . $user["country"] . 
										   " and address_parent = " . $user["address"] .
										   " order by address_company";
			
			}
		}
		else
		{
			$franchisee_address["address_id"] = $address["id"];
		}
	}
}
else // franchisee project
{
	if($address["client_type"] == 1) // Agent
	{
		
		if($_SESSION["new_project_step_1"]["project_kind"] == 7) //equipment of an independent retailer with furniture
		{
			$sql = "select address_id, concat(address_company, ', ', address_place) as company from addresses " . 
				   "where (address_is_independent_retailer = 1 and address_active = 1 and address_parent = " . $user["address"] . ") " . 
				   " or (address_type = 7 and address_active = 1 and address_is_independent_retailer) or (address_id = " . $franchisee_address["address_id"]  . ") " . 
				   " order by address_company";
		}
		else
		{
			$sql = "select address_id, concat(address_company, ', ', address_place) as company from addresses " . 
				   "where (address_canbefranchisee = 1 and address_active = 1 and address_parent = " . $user["address"] . ") " .
				   " or address_canbefranchisee_worldwide = 1 " .
				   " order by address_company";
		}
	}
	else
	{
		
		if($_SESSION["new_project_step_1"]["project_kind"] == 7) //equipment of an independent retailer with furniture
		{
			$sql = "select address_id, concat(address_company, ', ', address_place) as company from addresses " . 
				   "where (address_is_independent_retailer = 1 and address_active = 1 and address_parent = " . $user["address"] . ") " . 
				   " or (address_type = 7 and address_active = 1 and address_is_independent_retailer) or (address_id = " . $franchisee_address["address_id"]  . ") " . 
				   " order by address_company";
		}
		else
		{
			if($_SESSION["new_project_step_1"]["project_kind"] == 1
				and ($_SESSION["new_project_step_1"]["project_postype"] == 1
				or $_SESSION["new_project_step_1"]["project_postype"] == 3)) //new franchisee pos, store, kiosk
			{
				$sql = "select address_id, concat(address_company, ', ', address_place) as company, " .
					   "address_is_independent_retailer " . 
					   "from addresses " . 
					   "where ((address_id <> " . $user["address"] . " and address_canbefranchisee = 1 or address_is_independent_retailer = 1) " . 
					   "        and address_active = 1 and address_parent = " . $user["address"] . ") " . 
					   " or (address_id <> " . $user["address"] . " and address_canbefranchisee_worldwide = 1 and address_active = 1) " .
					   " order by address_company";
			}
			else
			{
				/*
				$sql = "select address_id, concat(address_company, ', ', address_place) as company from addresses " . 
					   "where (address_id <> " . $user["address"] . " and address_canbefranchisee = 1 and address_active = 1 and address_parent = " . $user["address"] . ") " . 
					   " or (address_id <> " . $user["address"] . " and address_canbefranchisee_worldwide = 1 and address_active = 1) " . 
					   " order by address_company";
				*/

			   $sql = "select address_id, concat(address_company, ', ', address_place) as company, " .
					   "address_is_independent_retailer " . 
					   "from addresses " . 
					   "where ((address_id <> " . $user["address"] . " and address_canbefranchisee = 1 or address_is_independent_retailer = 1) " . 
					   "        and address_active = 1 and address_parent = " . $user["address"] . ") " . 
					   " or (address_id <> " . $user["address"] . " and address_canbefranchisee_worldwide = 1 and address_active = 1) " .
					   " order by address_company";
			}
		}
	}

	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$franchisee_addresses[$row["address_id"]] = $row["company"];
	}
	
	
	if($_SESSION["new_project_step_1"]["project_kind"] != 2) // renovation
	{
		$franchisee_addresses[0] = "...";
		$franchisee_addresses[999999999] = "Create a new address";
	}


	if(param('franchisee_address_id'))
	{
		$sql = "select * from addresses " . 
			   "left join countries on country_id = address_country " .
			   "left join places on place_id = address_place_id " . 
			   "where address_id = " . dbquote(param("franchisee_address_id"));
		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$franchisee_address = $row;
		}
	}
}


$can_add_new_province = false;
if(param("franchisee_address_country"))
{
	$sql_c = "select country_provinces_complete " . 
		     "from countries " . 
		     "where country_id = " . param("franchisee_address_country");
	
	$res = mysql_query($sql_c) or dberror($sql_c);
	$row = mysql_fetch_assoc($res);

	if ($row["country_provinces_complete"] == 0)
	{
		$can_add_new_province = true;
	}
}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("", "project");


if($franchisee_address["address_id"] != $user['address'])
{
	$form->add_section(" ");

	
	if($_SESSION["new_project_step_1"]["project_kind"] != 2
			or $_SESSION["new_project_step_1"]["project_kind"] != 6 
			or $_SESSION["new_project_step_1"]["project_kind"] != 8 ) // renovation, relocation, popup
	{
		if($_SESSION["new_project_step_1"]["project_kind"] == 7 or $_SESSION["new_project_step_1"]["project_postype"] == 2) 
		{
			$form->add_section("Address of Owner Company");
		}
		else
		{
			$form->add_section("Franchisee Address");
		}
	}
	

	if(count($franchisee_addresses) > 0)
	{
		
		if($_SESSION["new_project_step_1"]["project_kind"] == 2
			or $_SESSION["new_project_step_1"]["project_kind"] == 6
			or $_SESSION["new_project_step_1"]["project_kind"] == 9
			or $_SESSION["new_project_step_1"]["project_kind"] == 8 ) // renovation, relocation, popup
		{
			$form->add_hidden('franchisee_address_id',  $franchisee_address["address_id"]);
		}
		elseif($_SESSION["new_project_step_1"]["project_kind"] == 7) //Equip Retailer
		{
			$form->add_comment("Please choose an existing company address from drop down list.");
			$form->add_list("franchisee_address_id", "Owner Company*", $franchisee_addresses, SUBMIT, $franchisee_address["address_id"]);
		}
		else
		{
			
			if($_SESSION["new_project_step_1"]["project_cost_type"] == 6)
			{
				$form->add_comment("Please choose an existing company address from drop down list.");
				$form->add_list("franchisee_address_id", "Owner Company*", $franchisee_addresses, SUBMIT, $franchisee_address["address_id"]);
			}
			else
			{
				$form->add_comment("Please choose an existing Franchisee address from drop down list.");
				$form->add_list("franchisee_address_id", "Franchisee*", $franchisee_addresses, SUBMIT, $franchisee_address["address_id"]);
			}
		}
	}
	else
	{
		$form->add_hidden('franchisee_address_id',  $franchisee_address["address_id"]);
	}

	if($franchisee_address["address_id"] > 0 or param('franchisee_address_id') > 0)
	{

		$form->add_comment("Please check the <strong>company's address</strong> details and correct if necessary.");
		$form->add_edit("franchisee_address_company", "Company*",NOTNULL , $franchisee_address["address_company"], TYPE_CHAR);
		$form->add_edit("franchisee_address_company2", "", 0, $franchisee_address["address_company2"], TYPE_CHAR);
		
		
		$form->add_hidden("franchisee_address_address", $franchisee_address["address_address"]);
		$form->add_multi_edit("franchisee_street", array("franchisee_address_street", "franchisee_address_streetnumber"), "Street/Street number", array(NOTNULL, ''), array($franchisee_address["address_street"], $franchisee_address["address_streetnumber"]), array('', ''), array(200, 5), array(), 0, '', '', array(), array('Street name', 'Number'));
		
		
		$form->add_edit("franchisee_address_address2", "Additional Address Info", 0, $franchisee_address["address_address2"], TYPE_CHAR);
		$form->add_list("franchisee_address_country", "Country*", $sql_countries, NOTNULL | SUBMIT, $franchisee_address["address_country"]);


		$provinces = array();
		if($can_add_new_province == true)
		{
			$provinces[999999999] = "Other province not listed below";
		}
		
		if(param("franchisee_address_id") == 999999999) {
			$sql_provinces = "select province_id, province_canton from provinces where province_country = " . dbquote(param("franchisee_address_country")) . " order by province_canton";
		}
		else
		{
			$sql_provinces = "select province_id, province_canton from provinces where province_country = " . dbquote($franchisee_address["address_country"]) . " order by province_canton";
		}

		$res = mysql_query($sql_provinces);
		while ($row = mysql_fetch_assoc($res))
		{
			$provinces[$row["province_id"]] = $row["province_canton"];
		}

		if(param("franchisee_address_province_id") == 999999999) {
			$form->add_hidden("franchisee_address_province_id", 999999999);
			$form->add_edit("franchisee_address_province", "Province*",NOTNULL , '', TYPE_CHAR, 50, 0, 1, "province");
		}
		else
		{
			$form->add_list("franchisee_address_province_id", "Province Selection*", $provinces, NOTNULL | SUBMIT, $franchisee_address["place_province"]);
		}



		$fplaces = array();
		$fplaces[999999999] = "Other city not listed below";
		
		
		if(param("franchisee_address_id") == 999999999 and param("franchisee_address_province_id") > 0) {
			$sql_places = "select place_id, place_name from places where place_province = " . dbquote(param("franchisee_address_province_id")) . " order by place_name";
		}
		elseif(param("franchisee_address_id") == 999999999) {
			$sql_places = "select place_id, place_name from places where place_country = " . dbquote(param("franchisee_address_country")) . " order by place_name";
		}
		else
		{
			$sql_places = "select place_id, place_name from places where place_country = " . $franchisee_address["address_country"] . " order by place_name";
		}

		
		$res = mysql_query($sql_places);

		while ($row = mysql_fetch_assoc($res))
		{
			$fplaces[$row["place_id"]] = $row["place_name"];
		}


		if(param("franchisee_address_place_id") == 999999999) {
			$form->add_hidden("franchisee_address_place_id", 999999999);
			$form->add_edit("franchisee_address_place", "City*",NOTNULL , '', TYPE_CHAR, 50, 0, 1, "city");
		}
		else
		{
			$form->add_list("franchisee_address_place_id", "City Selection*", $fplaces, NOTNULL | SUBMIT, $franchisee_address["address_place_id"]);
			$form->add_hidden("franchisee_address_place", $address["place"]);
		}



		$form->add_edit("franchisee_address_zip", "ZIP",0 , $franchisee_address["address_zip"], TYPE_CHAR, 20);


		$form->add_hidden("franchisee_address_phone", $franchisee_address["address_phone"]);
		$form->add_multi_edit("franchisee_phone_number", array("franchisee_phone_country", "franchisee_phone_area", "franchisee_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array($franchisee_address["address_phone_country"], $franchisee_address["address_phone_area"], $franchisee_address["address_phone_number"]), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20), array(), 0, '', '', array(), array('Country', 'Area', 'Number'));

		$form->add_hidden("franchisee_address_mobile_phone", $franchisee_address["address_mobile_phone"]);
		$form->add_multi_edit("franchisee_mobile_phone_number", array("franchisee_mobile_phone_country", "franchisee_mobile_phone_area", "franchisee_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array($franchisee_address["address_mobile_phone_country"], $franchisee_address["address_mobile_phone_area"], $franchisee_address["address_mobile_phone_number"]), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20), array(), 0, '', '', array(), array('Country', 'Area', 'Number'));



		$form->add_edit("franchisee_address_email", "Email",0 , $franchisee_address["address_email"], TYPE_CHAR,100);
		$form->add_edit("franchisee_address_contact_name", "Contact Name*",NOTNULL , $franchisee_address["address_contact_name"], TYPE_CHAR,100);
		$form->add_edit("franchisee_address_website", "Website",'' , $franchisee_address["address_website"]);
	}
	else
	{
		$form->add_hidden("franchisee_address_address", $address["address"]);
		$form->add_hidden("franchisee_address_address2", $address["address2"]);
	}
	
	
	$form->add_section(" ");
	$form->add_section(" ");
}

else
{
	//$form->add_edit("franchisee_address_id", "ZIP",0 , $address["id"]);
	$form->add_hidden("franchisee_address_id", $address["id"]);
	$form->add_hidden("franchisee_address_address", $address["address"]);
	$form->add_hidden("franchisee_address_address2", $address["address2"]);

	/*
	$form->add_hidden("franchisee_address_company", $address["company"]);
	$form->add_hidden("franchisee_address_company2", $address["company2"]);
	$form->add_hidden("franchisee_address_address", $address["address"]);
	$form->add_hidden("franchisee_address_address2", $address["address2"]);
	$form->add_hidden("franchisee_address_zip", $address["zip"]);
	$form->add_hidden("franchisee_address_place_id", $address["place_id"]);
	$form->add_hidden("franchisee_address_province_id");
	$form->add_hidden("franchisee_address_country", $address["country"]);
	$form->add_hidden("franchisee_address_phone", $address["phone"]);
	$form->add_hidden("franchisee_address_mobile_phone", $address["mobile_phone"]);
	$form->add_hidden("franchisee_address_email", $address["email"]);
	$form->add_hidden("franchisee_address_contact_name", $address["contact_name"]);
	$form->add_hidden("franchisee_address_website", $address["website"]);
    */

	if($_SESSION["new_project_step_1"]["project_kind"] == 3 
		or $_SESSION["new_project_step_1"]["project_kind"] == 4
		or $_SESSION["new_project_step_1"]["project_kind"] == 6
		or $_SESSION["new_project_step_1"]["project_kind"] == 9)
	{
		$form->add_section(" ");
		$form->add_section("Former Owner Company");

		$form->add_label("f_address_company", "Company",0 , $franchisee_address["address_company"]);
		$form->add_label("f_address_company2", "", 0, $franchisee_address["address_company2"]);
		$form->add_label("f_address_address", "Street",0 , $franchisee_address["address_address"]);
		$form->add_label("f_address_address2", "Additional Address Info", 0, $franchisee_address["address_address2"]);

		$form->add_label("f_address_zip", "ZIP",0 , $franchisee_address["address_zip"]);
		$form->add_lookup("f_address_place", "Place", "places", "place_name", 0, $franchisee_address["address_place_id"]);
		$form->add_lookup("f_address_country", "Country", "countries", "country_name", 0, $franchisee_address["address_country"]);
	}
}


//client address
$form->add_section("Your Address");
$form->add_hidden("client_address_id", $user["address"]);

if($_SESSION["new_project_step_1"]["project_cost_type"] == 1 and count($franchisee_addresses) == 0) //corporate
{
	$form->add_hidden("franchiseet_address_id", $user["address"]);
}

$form->add_comment("Please check <strong>your address</strong> details and report corrections to the retail team.");
$form->add_edit("client_address_company", "Company*",DISABLED , $address["company"], TYPE_CHAR);
$form->add_edit("client_address_company2", "", DISABLED, $address["company2"], TYPE_CHAR);
$form->add_edit("client_address_address", "Street*",DISABLED , $address["address"], TYPE_CHAR);
$form->add_edit("client_address_address2", "Additional Address Info", DISABLED, $address["address2"], TYPE_CHAR);

$form->add_hidden("client_address_country", $address["country"]);
//$form->add_list("client_address_country", "Country*", $sql_countries, DISABLED | SUBMIT, $address["country"]);
$form->add_edit("country_name", "Country", DISABLED, $address["country_name"], TYPE_CHAR);



$cplaces = array();

if(param('client_address_country')) {
	$sql_places = "select place_id, place_name from places where place_country = " . param('client_address_country') . " order by place_name";
}
else {
	$sql_places = "select place_id, place_name from places where place_country = " . $address["country"] . " order by place_name";
}
$res = mysql_query($sql_places);

while ($row = mysql_fetch_assoc($res))
{
	$cplaces[$row["place_id"]] = $row["place_name"];
}

$form->add_hidden("client_address_place_id", $address["place_id"]);
$form->add_hidden("client_address_province_id", $address["place_province"]);
//$form->add_list("client_address_place_id", "City Selection*", $cplaces, DISABLED, $address["place_id"]);
$form->add_edit("client_address_place", "", DISABLED, $address["place"], TYPE_CHAR);


$form->add_edit("client_address_zip", "ZIP",DISABLED , $address["zip"], TYPE_CHAR, 20);

$form->add_edit("client_address_phone", "Phone*",DISABLED , $address["phone"], TYPE_CHAR, 50, 0, 1, "phone1");
$form->add_edit("client_address_mobile_phone", "Mobile Phone",DISABLED , $address["mobile_phone"], TYPE_CHAR, 50, 0, 1, "mobile_phone1");
$form->add_edit("client_address_email", "Email",DISABLED , $address["email"], TYPE_CHAR,100);
$form->add_edit("client_address_contact_name", "Contact Name*",DISABLED , $address["contact_name"], TYPE_CHAR,100);


if($_SESSION["new_project_step_1"]["project_kind"] == 4) //Take Over
{
	$form->add_button("step4", "Proceed to next step");
}
else
{
	$form->add_button("step3", "Proceed to next step");
}

$form->add_button("back", "Back");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$form->populate();
$form->process();


//clean fields from apostrophes
foreach($form->items as $key=>$value)
{
	if(array_key_exists('value', $value)) {
		$form->value($key, str_replace('"', '', $form->value($key)));
	}
}


if ($form->button("back"))
{
	
	if(is_array($form->items['franchisee_phone_number']))
	{
		$form->value("franchisee_address_address", $form->unify_multi_edit_field($form->items["franchisee_street"], get_country_street_number_rule($form->value("franchisee_address_country"))));
		$form->value("franchisee_address_phone", $form->unify_multi_edit_field($form->items["franchisee_phone_number"]));
		$form->value("franchisee_address_mobile_phone", $form->unify_multi_edit_field($form->items["franchisee_mobile_phone_number"]));


		$_POST["franchisee_address_address"] = $form->unify_multi_edit_field($form->items["franchisee_street"], get_country_street_number_rule($form->value("franchisee_address_country")));
		$_POST["franchisee_street"] = trim($form->items['franchisee_street']['values'][0]);
		$_POST["franchisee_streetnumber"] = trim($form->items['franchisee_street']['values'][1]);
		
		$_POST["franchisee_address_phone"] = $form->unify_multi_edit_field($form->items["franchisee_phone_number"]);
		$_POST["franchisee_phone_country"] = trim($form->items['franchisee_phone_number']['values'][0]);
		$_POST["franchisee_phone_area"] = trim($form->items['franchisee_phone_number']['values'][1]);
		$_POST["franchisee_phone_number"] = trim($form->items['franchisee_phone_number']['values'][2]);
		
		$_POST["franchisee_address_mobile_phone"] = $form->unify_multi_edit_field($form->items["franchisee_mobile_phone_number"]);
		$_POST["franchisee_mobile_phone_country"] = trim($form->items['franchisee_mobile_phone_number']['values'][0]);
		$_POST["franchisee_mobile_phone_area"] = trim($form->items['franchisee_mobile_phone_number']['values'][1]);
		$_POST["franchisee_mobile_phone_number"] = trim($form->items['franchisee_mobile_phone_number']['values'][2]);
	}
	
	$_SESSION["new_project_step_2"] = $_POST;
	redirect("project_new_01.php");
}
elseif ($form->button("step3"))
{
	


	if(is_array($form->items['franchisee_phone_number']))
	{
		$form->value("franchisee_address_address", $form->unify_multi_edit_field($form->items["franchisee_street"], get_country_street_number_rule($form->value("franchisee_address_country"))));
		$form->value("franchisee_address_phone", $form->unify_multi_edit_field($form->items["franchisee_phone_number"]));
		$form->value("franchisee_address_mobile_phone", $form->unify_multi_edit_field($form->items["franchisee_mobile_phone_number"]));

		$form->add_validation("{franchisee_address_phone} != '' or {franchisee_address_mobile_phone} != ''", "Please indcate either the phone number or the mobile phone number!");


		$_POST["franchisee_address_address"] = $form->unify_multi_edit_field($form->items["franchisee_street"], get_country_street_number_rule($form->value("franchisee_address_country")));
		$_POST["franchisee_street"] = trim($form->items['franchisee_street']['values'][0]);
		$_POST["franchisee_streetnumber"] = trim($form->items['franchisee_street']['values'][1]);
		
		$_POST["franchisee_address_phone"] = $form->unify_multi_edit_field($form->items["franchisee_phone_number"]);
		$_POST["franchisee_phone_country"] = trim($form->items['franchisee_phone_number']['values'][0]);
		$_POST["franchisee_phone_area"] = trim($form->items['franchisee_phone_number']['values'][1]);
		$_POST["franchisee_phone_number"] = trim($form->items['franchisee_phone_number']['values'][2]);
		
		$_POST["franchisee_address_mobile_phone"] = $form->unify_multi_edit_field($form->items["franchisee_mobile_phone_number"]);
		$_POST["franchisee_mobile_phone_country"] = trim($form->items['franchisee_mobile_phone_number']['values'][0]);
		$_POST["franchisee_mobile_phone_area"] = trim($form->items['franchisee_mobile_phone_number']['values'][1]);
		$_POST["franchisee_mobile_phone_number"] = trim($form->items['franchisee_mobile_phone_number']['values'][2]);

		
	}


	$_SESSION["new_project_step_2"] = $_POST;

	$form->add_validation("{franchisee_address_id}", "The franchisee address must be indicated!");

	$error = 0;
	if(strlen(preg_replace('/[^0-9a-z_ %\[\]\.\(\)%&-]/s', '', $form->value("franchisee_address_address"))) < strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("franchisee_address_address")))
		and strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("franchisee_address_address"))) > 10)
	{
		$error = 1;
		$form->error("Company names and addresses must contain lower caracters. Do not use capital letters only.");
	}
	elseif(strlen(preg_replace('/[^0-9a-z_ %\[\]\.\(\)%&-]/s', '', $form->value("franchisee_address_address2"))) < strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("franchisee_address_address2")))
		and strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("franchisee_address_address2"))) > 10)
	{
		$error = 1;
		$form->error("Company names and addresses must contain lower caracters. Do not use capital letters only.");
	}

	if($error == 0 and $form->validate())
	{
		$link = "project_new_03.php";
		redirect($link);
	}
}
elseif ($form->button("step4"))
{
	
	if(is_array($form->items['franchisee_phone_number']))
	{
		$form->value("franchisee_address_address", $form->unify_multi_edit_field($form->items["franchisee_street"], get_country_street_number_rule($form->value("franchisee_address_country"))));
		$form->value("franchisee_address_phone", $form->unify_multi_edit_field($form->items["franchisee_phone_number"]));
		$form->value("franchisee_address_mobile_phone", $form->unify_multi_edit_field($form->items["franchisee_mobile_phone_number"]));

		$form->add_validation("{franchisee_address_phone} != '' or {franchisee_address_mobile_phone} != ''", "Please indcate either the phone number or the mobile phone number!");


		$_POST["franchisee_address_address"] = $form->unify_multi_edit_field($form->items["franchisee_street"], get_country_street_number_rule($form->value("franchisee_address_country")));
		$_POST["franchisee_street"] = trim($form->items['franchisee_street']['values'][0]);
		$_POST["franchisee_streetnumber"] = trim($form->items['franchisee_street']['values'][1]);
		
		$_POST["franchisee_address_phone"] = $form->unify_multi_edit_field($form->items["franchisee_phone_number"]);
		$_POST["franchisee_phone_country"] = trim($form->items['franchisee_phone_number']['values'][0]);
		$_POST["franchisee_phone_area"] = trim($form->items['franchisee_phone_number']['values'][1]);
		$_POST["franchisee_phone_number"] = trim($form->items['franchisee_phone_number']['values'][2]);
		
		$_POST["franchisee_address_mobile_phone"] = $form->unify_multi_edit_field($form->items["franchisee_mobile_phone_number"]);
		$_POST["franchisee_mobile_phone_country"] = trim($form->items['franchisee_mobile_phone_number']['values'][0]);
		$_POST["franchisee_mobile_phone_area"] = trim($form->items['franchisee_mobile_phone_number']['values'][1]);
		$_POST["franchisee_mobile_phone_number"] = trim($form->items['franchisee_mobile_phone_number']['values'][2]);
	}

	$_SESSION["new_project_step_2"] = $_POST;
	$form->add_validation("{franchisee_address_id}", "The franchisee address must be indicated!");

	if($form->validate())
	{
		$_SESSION["new_project_step_3"] = "";
		$link = "project_new_04.php";
		redirect($link);
	}	
}
elseif($form->button("client_address_country"))
{
	$form->value("client_address_place_id", 0);
	$form->value("client_address_province_id", 0);
}
elseif($form->button("client_address_place_id"))
{
	$form->value("client_address_place", $cplaces[$form->value("client_address_place_id")]);
}
elseif($form->button("franchisee_address_place_id"))
{
	if($form->value("franchisee_address_place_id") != 999999999) {
		$form->value("franchisee_address_place", $fplaces[$form->value("franchisee_address_place_id")]);
	}
	else
	{
		$form->value("franchisee_address_place", "");
	}
}
elseif($form->button("franchisee_address_id"))
{
	
	if($form->value("franchisee_address_id") > 0) {

		if(array_key_exists("franchisee_address_company", $form->items)) {
		
			$form->value("franchisee_address_company", "");
			$form->value("franchisee_address_company2",  "");
			$form->value("franchisee_address_address",  "");
			if(array_key_exists('franchisee_street', $form->items))
			{
				$form->items['franchisee_street']['values'][0] = "";
				$form->items['franchisee_street']['values'][1] = "";
			}
			$form->value("franchisee_address_address2",  "");
			$form->value("franchisee_address_zip",  "");
			$form->value("franchisee_address_place_id",  0);
			$form->value("franchisee_address_province_id",  0);
			$form->value("franchisee_address_country",  0);
			$form->value("franchisee_address_phone",  "");

			if(array_key_exists('franchisee_phone_number', $form->items))
			{
				$form->items['franchisee_phone_number']['values'][0] = "";
				$form->items['franchisee_phone_number']['values'][1] = "";
				$form->items['franchisee_phone_number']['values'][2] = "";
			}

			$form->value("franchisee_address_mobile_phone",  "");

			if(array_key_exists('franchisee_mobile_phone_number', $form->items))
			{
				$form->items['franchisee_mobile_phone_number']['values'][0] = "";
				$form->items['franchisee_mobile_phone_number']['values'][1] = "";
				$form->items['franchisee_mobile_phone_number']['values'][2] = "";
			}

			$form->value("franchisee_address_email",  "");
			$form->value("franchisee_address_contact_name",  "");
			$form->value("franchisee_address_website",  "");
			
			if($form->value("franchisee_address_id") < 999999999)
			{
				$sql = "select * from addresses " .
					   "left join countries on country_id = address_country " . 
					   "left join places on place_id = address_place_id " . 
					   "where address_id = " . dbquote($form->value("franchisee_address_id"));
				$res = mysql_query($sql) or dberror($sql);

				if ($row = mysql_fetch_assoc($res))
				{
					$form->value("franchisee_address_company", $row["address_company"]);
					$form->value("franchisee_address_company2",  $row["address_company2"]);
					$form->value("franchisee_address_address",  $row["address_address"]);

					if(array_key_exists('franchisee_street', $form->items))
					{
						$form->items['franchisee_street']['values'][0] = $row["address_street"];
						$form->items['franchisee_street']['values'][1] = $row["address_streetnumber"];
					}

					$form->value("franchisee_address_address2",  $row["address_address2"]);
					$form->value("franchisee_address_zip",  $row["address_zip"]);
					$form->value("franchisee_address_place_id",  $row["address_place_id"]);
					$form->value("franchisee_address_province_id",  $row["place_province"]);
					$form->value("franchisee_address_country",  $row["address_country"]);
					$form->value("franchisee_address_phone",  $row["address_phone"]);
					if(array_key_exists('franchisee_phone_number', $form->items))
					{
						$form->items['franchisee_phone_number']['values'][0] = $row["address_phone_country"];
						$form->items['franchisee_phone_number']['values'][1] = $row["address_phone_area"];
						$form->items['franchisee_phone_number']['values'][2] = $row["address_phone_number"];
					}
					$form->value("franchisee_address_mobile_phone",  $row["address_mobile_phone"]);

					if(array_key_exists('franchisee_mobile_phone_number', $form->items))
					{
						$form->items['franchisee_mobile_phone_number']['values'][0] = $row["address_mobile_phone_country"];
						$form->items['franchisee_mobile_phone_number']['values'][1] = $row["address_mobile_phone_area"];
						$form->items['franchisee_mobile_phone_number']['values'][2] = $row["address_mobile_phone_number"];
					}

					$form->value("franchisee_address_email",  $row["address_email"]);
					$form->value("franchisee_address_contact_name",  $row["address_contact_name"]);
					$form->value("franchisee_address_website",  $row["address_website"]);
				}
			}
		}
	}
}
   
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

$page->register_action('home', 'Home', "welcome.php");

$page->header();
$page->title("Project Request");


echo "<div>";
echo "<img class='stepcounter' src='/pictures/numbers/01_off.gif' alt=\"\" />";
echo "<span class='step_inactive'>Basic Project Data</span>";

if($_SESSION["new_project_step_1"]["project_kind"] == 4) // Take over
{
	echo "<img class='stepcounter' src='/pictures/numbers/02_on.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Addresses</span>";
	echo "<img class='stepcounter' src='/pictures/numbers/03_off.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Environment</span>";
}
elseif($_SESSION["new_project_step_1"]["project_kind"] == 5) // lease renewal
{
	echo "<img class='stepcounter' src='/pictures/numbers/02_on.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Environment</span>";
}
elseif($_SESSION["new_project_step_1"]["project_kind"] == 8) // popup
{
	echo "<img class='stepcounter' src='/pictures/numbers/02_on.gif' alt=\"\" />";
	echo "<span class='step_inactive'>PopUp Information</span>";
	echo "<img class='stepcounter' src='/pictures/numbers/03_off.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Comments</span>";
}
else
{
	echo "<img class='stepcounter' src='/pictures/numbers/02_on.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Addresses</span>";
	echo "<img class='stepcounter' src='/pictures/numbers/03_off.gif' alt=\"\" />";
	echo "<span class='step_inactive'>POS Information</span>";
	echo "<img class='stepcounter' src='/pictures/numbers/04_off.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Environment</span>";
	echo "<img class='stepcounter' src='/pictures/numbers/05_off.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Comments</span>";
}
echo "<br /><br /><br /></div>";


$form->render();

?>


<div id="phone" style="display:none;">
    Please indicate the phone number according to international conventions like:<br />
	<strong>+41 61 926 67 00</strong><br />use only the 'plus sign', 'blank' and 'numbers'.
</div> 

<div id="mobile_phone" style="display:none;">
    Please indicate the mobile_phone umber according to international conventions like:<br />
	<strong>+41 61 926 67 00</strong><br />use only the 'plus sign', 'blank' and 'numbers'.
</div> 

<div id="phone1" style="display:none;">
    Please indicate the phone number according to international conventions like:<br />
	<strong>+41 61 926 67 00</strong><br />use only the 'plus sign', 'blank' and 'numbers'.
</div> 

<div id="mobile_phone1" style="display:none;">
    Please indicate the mobile_phone umber according to international conventions like:<br />
	<strong>+41 61 926 67 00</strong><br />use only the 'plus sign', 'blank' and 'numbers'.
</div> 

<div id="city" style="display:none;">
    Please indicate the new city's name in English!
</div> 

<div id="province" style="display:none;">
    Please indicate the new province's name in English!
</div> 

<div id="city2" style="display:none;">
    Please indicate the new city's name in English!
</div> 

<div id="province2" style="display:none;">
    Please indicate the new province's name in English!
</div> 

<?php

$page->footer();


?>