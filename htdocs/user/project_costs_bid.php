<?php
/********************************************************************

    project_costs_costsheet_bids.php

    View or edit bids for a project

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-09
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2012, OMEGA SA, All Rights Reserved.

*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/project_cost_functions.php";

if (has_access("can_view_project_costs") 
   or has_access("can_view_budget_in_projects")
   or  has_access('can_edit_project_costs_bids')
   )
{ 
}
else {
	redirect("noaccess.php");
}


if(!param("pid"))
{
	$link = "welcome.php";
	redirect($link);
}



/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));
// get company's address
$client_address = get_address($project["order_client_address"]);
$order_currency = get_order_currency($project["project_order"]);

$currency_symbol = '';
if(id() > 0)
{
	$currency = get_bid_currency(id());
	$currency_symbol = $currency['symbol'];
}

$can_edit = false;
if($project["order_budget_is_locked"] == 0) {
	$can_edit = true;
}

if (!has_access('can_edit_project_costs_bids')
	or $project["project_state"] == 2)
{
	$can_edit = false;
}

/********************************************************************
    check if project cost positions are present and create if not
*********************************************************************/

$sql = "select count(costsheet_id) as num_recs from costsheets " . 
       " where costsheet_version = 0
			and costsheet_project_id = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

if($row["num_recs"] == 0) //make the user select a cost template
{
	$link = "project_costs_overview.php?pid=" . param("pid");
	redirect($link);
}


/********************************************************************
    check if bid positions are present 
*********************************************************************/
$has_positions = true;

$sql = "select count(costsheet_bid_position_id) as num_recs from costsheet_bid_positions " . 
       " where costsheet_bid_position_costsheet_bid_id = " . id();

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

if($row["num_recs"] == 0) 
{
	$has_positions = false;
}

/********************************************************************
	prepare data
*********************************************************************/ 
$sql_cost_groups = "select DISTINCT costsheet_pcost_group_id, " . 
		               "concat(pcost_group_code, ' ', pcost_group_name) as costgroup " .
		               "from costsheets " .
					   "left join pcost_groups on pcost_group_id = costsheet_pcost_group_id " .
					   "where costsheet_version = 0
						and costsheet_project_id = " . param("pid") . 
					   " order by costgroup";


$sql_bids = "select costsheet_bid_id, costsheet_bid_company " . 
            " from costsheet_bids " . 
			" where costsheet_bid_project_id = " . param("pid") . 
			" order by costsheet_bid_company";

$has_bids = false;

$res = mysql_query($sql_bids) or dberror($sql_bids);
if($row = mysql_fetch_assoc($res))
{
	$has_bids = true;
}


$sql_currencies = "select currency_id, currency_symbol
					from currencies
					order by currency_symbol";


/********************************************************************
	Create Form
*********************************************************************/ 

$form = new Form("costsheet_bids", "costsheet_bids");


$form->add_section("Project");
$form->add_hidden("pid", param('pid'));


require_once "include/project_head_small.php";

$form->add_section("General Information");
$form->add_hidden("costsheet_bid_project_id", param("pid"));
$form->add_list("costsheet_bid_currency", "Currency*", $sql_currencies, NOTNULL);
$form->add_edit("costsheet_bid_company", "Company*", NOTNULL);
$form->add_edit("costsheet_bid_date", "Date*", NOTNULL, "", TYPE_DATE);
$form->add_multiline("costsheet_bid_remark", "Remarks", 4);
$form->add_hidden('costsheet_bid_exchange_rate');
$form->add_hidden('costsheet_bid_factor');



if($has_positions == true)
{
	$form->add_hidden("new_sheet", 0);
}
elseif($can_edit == true)
{
	$form->add_hidden("new_sheet", 1);
	
	if($has_bids == true)
	{
		$form->add_section("Copy from existing Bid");
		$form->add_comment("Copy sheet structure from an existing bid.");
		$form->add_list("bid_id", "Existing Bid", $sql_bids, SUBMIT);
	}
	else
	{
		$form->add_hidden("bid_id", 0);
	}
	
	
	$form->add_section("Costgroups");
	$form->add_comment("Please select the cost groups to be contained in the bid form.");
	
	$check_box_names = array();
	$res = mysql_query($sql_cost_groups) or dberror($sql_cost_groups);
	while($row = mysql_fetch_assoc($res))
	{
		$form->add_checkbox("CG" . $row["costsheet_pcost_group_id"], "", 0, 0, $row["costgroup"]);
		$check_box_names["CG" . $row["costsheet_pcost_group_id"]] = $row["costsheet_pcost_group_id"];
	}
}




//if(has_access("can_edit_local_constrction_work") and $project["order_budget_is_locked"] == 0)
if($can_edit == true)
{
	
	if($project["order_budget_is_locked"] == 0)
	{
		if(id() > 0)
		{
			$form->add_button("delete", "Delete this Bid");
		}
		
		$form->add_button("save", "Save General Information");
		$link = "javascript:add_new_cost_position(" . param("pid") . ", ". param("id") . ")";
		$form->add_button("add_cost_positions", "Add Cost Positions", $link);
	}
}

$link = "javascript:popup('/user/project_costs_bid_pdf.php?pid=" . param("pid") . "&bid=" . id() . "', 800, 600);";
$form->add_button("print_bid", "Print Bid in " . $currency_symbol, $link);	

$form->add_button("back", "Back");

if($project["order_budget_is_locked"] == 1)
{
	$form->error("Budget is locked, no more changes can be made!");
}

$form->populate();
$form->process();


if($form->button("save") or $form->button("bid_id"))
{
	
	

	if($form->validate())
	{
		$form->save();

		$currency = get_bid_currency(id());
		$form->value('costsheet_bid_exchange_rate', $currency['exchange_rate']);
		$form->value('costsheet_bid_factor', $currency['factor']);
		$form->save();

		if($form->value("new_sheet") == 1)
		{
			$new_bid = mysql_insert_id();
			
			if($form->button("bid_id") or $form->value("bid_id") > 0 and $new_bid > 0)
			{
				$sql = "select * from costsheet_bid_positions " . 
					   "where costsheet_bid_position_costsheet_bid_id = " . $form->value("bid_id");

				$res = mysql_query($sql) or dberror($sql);
				while($row = mysql_fetch_assoc($res))
				{
					$fields = array();
					$values = array();

					$fields[] = "costsheet_bid_position_costsheet_bid_id";
					$values[] = $new_bid;

					$fields[] = "costsheet_bid_position_project_id";
					$values[] = dbquote(param("pid"));
					
					$fields[] = "costsheet_bid_position_costsheet_id";
					$values[] = dbquote($row["costsheet_bid_position_costsheet_id"]);

					$fields[] = "costsheet_bid_position_pcost_group_id";
					$values[] = dbquote($row["costsheet_bid_position_pcost_group_id"]);

					$fields[] = "costsheet_bid_position_pcost_subgroup_id";
					$values[] = dbquote($row["costsheet_bid_position_pcost_subgroup_id"]);

					$fields[] = "costsheet_bid_position_code";
					$values[] = dbquote($row["costsheet_bid_position_code"]);

					$fields[] = "costsheet_bid_position_text";
					$values[] = dbquote($row["costsheet_bid_position_text"]);

					$fields[] = "date_created";
					$values[] = "now()";

					$fields[] = "date_modified";
					$values[] = "now()";

					$fields[] = "user_created";
					$values[] = dbquote(user_login());

					$fields[] = "user_modified";
					$values[] = dbquote(user_login());

					$sql = "insert into costsheet_bid_positions (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
					
					mysql_query($sql) or dberror($sql);
				}
				$link = "project_costs_bid.php?pid=" . param("pid"). "&id=" . $new_bid;
				redirect($link);
			}
			else
			{
				$selected_costgroups = array();
				foreach ($check_box_names as $key=>$value)
				{
					if ($form->value($key))
					{
						$selected_costgroups[] = $value;
					}
				}
				if(count($selected_costgroups) > 0)
				{
					$result = create_bid_positions( id(), param("pid"), $selected_costgroups);
					$link = "project_costs_bid.php?id=". id() . "&pid=" . param("pid");
					redirect($link);

				}
				else
				{
					$form->error("Please select at least one cost group.");
				}
			}
		}
		else
		{
			//update budget with selected positions
			$form->message("Your data has been saved.");
		}
	}

}
elseif($form->button("back"))
{
	$link = "project_costs_bids.php?pid=" . param("pid");
	redirect($link);
}
elseif($form->button("delete"))
{
	
	//check if bid is used in budget
	$sql = "select count(costsheet_bid_position_id) as num_recs from costsheet_bid_positions " . 
		   " where costsheet_bid_position_is_in_budget = 1 and costsheet_bid_position_costsheet_bid_id = " . id();
	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);

	if($row["num_recs"] == 0) 
	{
		$sql = "delete from costsheet_bid_positions " . 
				   "where costsheet_bid_position_costsheet_bid_id = " . id();
			mysql_query($sql) or dberror($sql);

			$sql = "delete from costsheet_bids " . 
						   "where costsheet_bid_id = " . id();
					mysql_query($sql) or dberror($sql);

			$link = "project_costs_bids.php?pid=" . param("pid");
			redirect($link);
	}
	else
	{
		$form->error("The bid can not be deleted since it is used in the project's budget.");
	}
	
	
}





/********************************************************************
	Create list of cost positions
*********************************************************************/ 
$list_names = array();
$group_ids = array();
$group_titles = array();
$sub_group_ids = array();


//get all subgroups
$sql = "select costsheet_bid_position_id, costsheet_bid_position_pcost_group_id, 
       costsheet_bid_position_pcost_subgroup_id,  costsheet_bid_position_costsheet_id, 
	   costsheet_bid_position_code, costsheet_bid_position_text, costsheet_bid_position_comment, 
	   concat(pcost_subgroup_code, ' ', pcost_subgroup_name) as subgroup 
	   from costsheet_bid_positions 
	    left join pcost_subgroups on pcost_subgroup_id = costsheet_bid_position_pcost_subgroup_id  
        where costsheet_bid_position_project_id = " . dbquote(param("pid")) .
		" and costsheet_bid_position_costsheet_bid_id = " . dbquote(param("id"));

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$sub_group_ids[$row['subgroup']] = $row['costsheet_bid_position_pcost_subgroup_id'];
}

if($has_positions == true)
{	
	//get data 
	$bid_totals = get_project_bid_totals(id(), $order_currency);
	$code_data = array();
	$amount_data = array();
	$currency_data = array();
	$text_data = array();
	$comment_data = array();
	$in_budget = array();
	$delete_links = array();
	

	$sql = "select costsheet_bid_position_id, costsheet_bid_position_pcost_group_id, " . 
		   "costsheet_bid_position_code, costsheet_bid_position_text, " . 
		   "costsheet_bid_position_amount, costsheet_bid_position_comment, costsheet_bid_position_is_in_budget " . 
		   "from costsheet_bid_positions " .
		   "where costsheet_bid_position_costsheet_bid_id =" .id();

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$code_data[$row["costsheet_bid_position_id"]] = $row["costsheet_bid_position_code"];
		$text_data[$row["costsheet_bid_position_id"]] = $row["costsheet_bid_position_text"];
		$amount_data[$row["costsheet_bid_position_id"]] = $row["costsheet_bid_position_amount"];
		$currency_data[$row["costsheet_bid_position_id"]] = $currency_symbol;
		$comment_data[$row["costsheet_bid_position_id"]] = $row["costsheet_bid_position_comment"];
		$in_budget[$row["costsheet_bid_position_id"]] = $row["costsheet_bid_position_is_in_budget"];
		$checkbox_ids[$row["costsheet_bid_position_pcost_group_id"]][$row["costsheet_bid_position_id"]] = "__costsheet_bid_positions_costsheet_bid_position_is_in_budget_" . $row["costsheet_bid_position_id"];
		$delete_ids[$row["costsheet_bid_position_pcost_group_id"]][$row["costsheet_bid_position_id"]] = "__costsheet_bid_positions_select_to_delete_" . $row["costsheet_bid_position_id"];
		
		$list_has_positions[$row["costsheet_bid_position_pcost_group_id"]] = true;

		$delete_links[$row["costsheet_bid_position_id"]] = '<a class="delete_line" data="'.$row['costsheet_bid_position_id'].'" href="javascript:void(0);" title="Remove line"><img src="/pictures/closed.gif" /></i></a>';
	}

	


	//add all cost groups and cost sub groups
	$select_button_names = array();
	$group_ids = array();
	$group_titles = array();
	

	$sql = "select DISTINCT costsheet_bid_position_pcost_group_id, pcost_group_code, pcost_group_name " .
		   "from costsheet_bid_positions " .
		   "left join pcost_groups on pcost_group_id = costsheet_bid_position_pcost_group_id " . 
		   "where costsheet_bid_position_costsheet_bid_id = " . id() . 
		   " order by pcost_group_code";


	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{
		$listname = "list" . $row["pcost_group_code"];
		$select_buttonname = "select_all" . $row["pcost_group_code"];
		$list_names[] = $listname;
		$select_button_names[] = $select_buttonname;
		$group_ids[] = $row["costsheet_bid_position_pcost_group_id"];
		$group_titles[] = $row["pcost_group_name"];

		
		$sql = "select costsheet_bid_position_id, costsheet_bid_position_code, costsheet_bid_position_text, " . 
			   "costsheet_bid_position_amount, costsheet_bid_position_comment, costsheet_bid_position_is_in_budget, " . 
			   "concat(pcost_subgroup_code, ' ', pcost_subgroup_name) as subgroup " . 
			   "from costsheet_bid_positions " .
			   "left join pcost_groups on pcost_group_id = costsheet_bid_position_pcost_group_id " .
			   " left join pcost_subgroups on pcost_subgroup_id = costsheet_bid_position_pcost_subgroup_id"; 

		$list_filter = "costsheet_bid_position_costsheet_bid_id = " . id() . " and costsheet_bid_position_pcost_group_id = " . $row["costsheet_bid_position_pcost_group_id"];
		
		$toggler = '<div class="toggler_pointer" id="l' . $row["costsheet_bid_position_pcost_group_id"] . '_on"><span class="fa fa-minus-square toggler"></span>' .$row["pcost_group_name"] . '</div>';
		
		$$listname = new ListView($sql, LIST_HAS_HEADER | LIST_HAS_FOOTER);

		$$listname->set_entity("costsheet_bid_positions");
		$$listname->set_order("LENGTH(costsheet_bid_position_code), COALESCE(costsheet_bid_position_code,'Z')");
		$$listname->set_group("subgroup");
		$$listname->set_filter($list_filter);
		$$listname->set_title($toggler);

		
		

		
		if($can_edit == true) {
			
			$link = "javascript:select_all_cost_positions_for_budget(" . $row["costsheet_bid_position_pcost_group_id"]. ")";
			$c1 = '<a href"javascript:void(0);" onclick="' .$link . '"><span class="fa fa-check-square-o checker"></span></a><span id="selector' . $row["costsheet_bid_position_pcost_group_id"] . '">&nbsp;In Budget</span>';


			$$listname->add_checkbox_column("costsheet_bid_position_is_in_budget", $c1, COLUMN_UNDERSTAND_HTML, $in_budget);
			$$listname->add_edit_column("costsheet_bid_position_code", "Code", 4, 0, $code_data);
			$$listname->add_edit_column("costsheet_bid_position_text", "Text", 70, 0, $text_data, 'textarea', 3);
			$$listname->add_text_column("currency", "", 0, $currency_data);
			$$listname->add_edit_column("costsheet_bid_position_amount", "Costs", 12, 0, $amount_data);
			
			$$listname->add_edit_column("costsheet_bid_position_comment", "Comment", 30, 0, $comment_data, 'textarea', 3);
		}
		else {
			
			$$listname->add_checkbox_column("costsheet_bid_position_is_in_budget", "", COLUMN_UNDERSTAND_HTML, $in_budget);
			$$listname->add_text_column("costsheet_bid_position_code", "Code", 0, $code_data);
			$$listname->add_text_column("costsheet_bid_position_text", "Text", 0, $text_data);
			$$listname->add_text_column("currency", "", 0, $currency_data);
			$$listname->add_text_column("costsheet_bid_position_amount", "Costs", 0, $amount_data);
			$$listname->add_text_column("costsheet_bid_position_comment", "Comment", 0, $comment_data);
		}
		if($can_edit == true) {
			$$listname->add_text_column("delete_links", "", COLUMN_UNDERSTAND_HTML, $delete_links);
		}


		
		if(array_key_exists($row["costsheet_bid_position_pcost_group_id"], $list_has_positions))
		{
			foreach($bid_totals["subgroup_totals"] as $subgroup=>$bid_sub_group_total)
			{
				if($can_edit == true) {
					$tmp = param("pid") . '-' . param("id") . '-' . $row['costsheet_bid_position_pcost_group_id']. '-'. $sub_group_ids[$subgroup];
					$$listname->set_group_footer("delete_links", $subgroup , '<a class="add_new_line" data="'.$tmp.'" href="javascript:void(0);" title="Add new line">&nbsp;<i class="fa fa-plus" aria-hidden="true"></i></a>');

				}
				
				$$listname->set_group_footer("costsheet_bid_position_text", $subgroup , "Subgroup Total");
				$$listname->set_group_footer("costsheet_bid_position_amount",  $subgroup , number_format($bid_sub_group_total, 2));

				$$listname->set_group_footer("costsheet_bid_position_text", $subgroup , "Subgroup Total");
				$$listname->set_group_footer("costsheet_bid_position_amount",  $subgroup , number_format($bid_sub_group_total, 2, ".", "'"), 'sgt' . $sub_group_ids[$subgroup]);
			}
					
			$$listname->set_footer("costsheet_bid_position_code", "Total");
			$$listname->set_footer("costsheet_bid_position_amount", number_format($bid_totals["group_totals"][$row["costsheet_bid_position_pcost_group_id"]], 2, ".", "'"), 'gt' . $row["costsheet_bid_position_pcost_group_id"]);	
			
		}

		$$listname->populate();
		$$listname->process();
	}
	
}

$page = new Page("projects");


require "include/project_page_actions.php";

$page->header();
$page->title(id() ? "Project Costs - Edit Bid" : "Project Costs - Add Bid");

require_once("include/costsheet_tabs.php");
$form->render();


foreach($list_names as $key=>$listname)
{
	
	if(array_key_exists("costsheet", $_SESSION) and array_key_exists($listname, $_SESSION["costsheet"]))
	{
		if($_SESSION["costsheet"][$listname] == 0)
		{
			echo '<p>&nbsp;</p>';
			$toggler = '<div class="toggler_pointer" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
			echo $toggler;
			
			echo '<div style="display:none;" id="' . $listname . '">';
			$$listname->render();
			echo '</div>';
		}
		else
		{
			echo '<p>&nbsp;</p>';
			$toggler = '<div class="toggler_pointer toggler_pointer_off" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
			echo $toggler;
			
			echo '<div id="' . $listname . '">';
			$$listname->render();
			echo '</div>';
		}
	}
	else
	{
		echo '<p>&nbsp;</p>';
		$toggler = '<div class="toggler_pointer toggler_pointer_off" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
		echo $toggler;
		
		echo '<div id="' . $listname . '">';
		$$listname->render();
		echo '</div>';
	}
}


?>
<script language="javascript">
jQuery(document).ready(function($) {
	<?php
	foreach($list_names as $key=>$listname)
	{
	?>
		$('#l<?php echo $group_ids[$key];?>_on').click(function()  {
			$('#<?php echo $listname;?>').css('display', 'none');
			$('#l<?php echo $group_ids[$key];?>_off').css('display', 'block');

			$.ajax({
				type: "POST",
				data: "listname=<?php echo $listname;?>&visibility=0",
				url: "../shared/ajx_costsheet_liststaes.php",
				success: function(msg){
				}
			});
		});
		
		$('#l<?php echo $group_ids[$key];?>_off').click(function()  {
			$('#<?php echo $listname;?>').css('display', 'block');
			$('#l<?php echo $group_ids[$key];?>_off').css('display', 'none');

			$.ajax({
				type: "POST",
				data: "listname=<?php echo $listname;?>&visibility=1",
				url: "../shared/ajx_costsheet_liststaes.php",
				success: function(msg){
				}
			});
		});
	<?php
	}
	
	
	if($can_edit == true) {
	?>
		$(document).on("change","input",function(){
			
			var field = $(this);
			$(this).removeClass( "error_field" );
			var field_id = $(this).attr('id');
			var error = 0;

			if (field_id.indexOf("bid_position_code") >= 0
				|| field_id.indexOf("bid_position_text") >= 0
				|| field_id.indexOf("bid_position_amount") >= 0
				|| field_id.indexOf("bid_position_comment") >= 0) {

				if(field_id.indexOf("bid_position_amount") >= 0) {
					if($(this).val() == ''
						|| $.isNumeric( $(this).val())) {
							
					}
					else {
						$(this).addClass( "error_field" );
						error = 1;
					}
				}
				if(error == 0) {	
					
					$(this).css({ 'background': 'transparent url("/public/data/images/icon-checked.png") 3px 50% no-repeat', 'background-position':'center right'});

					$(this).fadeTo('slow', 0.3, function()
					{
						$(this).css('background-image', 'url()');
					}).fadeTo('slow', 1);

					var params = "pid=<?php echo param('pid');?>&bid=<?php echo param('id');?>&id=" + field_id + "&value=" + $(this).val() + '&action=save';
					$.ajax({
						type: "POST",
						data: params,
						url: "include/ajx_save_bid_position.php",
						success: function(msg){

							if(msg != 'error') {
								result = $.parseJSON(msg);
								var sgt = '#sgt' + result['costsheet_bid_position_pcost_subgroup_id'];
								var amount = result['sgr_total'];
								$(sgt).html($.number( amount, 2, ".", "'" ));

								var gt = '#gt' + result['costsheet_bid_position_pcost_group_id'];
								var amount = result['gr_total'];
								$(gt).html($.number( amount, 2, ".", "'" ));

							}
						}
					});

					
				}
			}
		});


		$( document ).on( "change", "input[type='checkbox']", function() {
		  var field_id = $(this).attr('id');

		  if (field_id.indexOf("bid_position_is_in_budget") >= 0) {
			  
			 
			  var value = 0;
			  if($('#' + field_id).is(":checked")) {
				  value = 1;
			  }


			  $("#" + $(this).attr('id')).after('<span><img class="img_helper"  border="none" src="/public/data/images/icon-checked.png" /></span>');

			  $(this).next("span").fadeOut(2000, function(){ $(this).remove();});	

			  var params = "pid=<?php echo param('pid');?>&bid=<?php echo param('id');?>&id=" + field_id + "&value=" + value + '&action=update_budget';
				
			  $.ajax({
					type: "POST",
					data: params,
					url: "include/ajx_save_bid_position.php",
					success: function(msg){
						if(msg != 'error') {

						}
					}
				});
		  }
		  
		});


		$('.add_new_line').click(function(event) {
			 
			var object = $(this);
			var data = $(this).attr('data');
			var params = "data=" + data + '&action=new_line';
			$.ajax({
				type: "POST",
				data: params,
				url: "include/ajx_save_bid_position.php",
				success: function(msg){
					
					if(msg != 'error') {
						new_tr='<tr valign="top"><td><input name="__costsheet_bid_positions_costsheet_bid_position_is_in_budget_' + msg + '" id="__costsheet_bid_positions_costsheet_bid_position_is_in_budget_' + msg + '" value="1" type="checkbox"> </td><td>&nbsp;</td><td><input name="__costsheet_bid_positions_costsheet_bid_position_code_' + msg + '" id="__costsheet_bid_positions_costsheet_bid_position_code_' + msg + '" value="" size="4" type="text"></td><td>&nbsp;</td><td><input name="__costsheet_bid_positions_costsheet_bid_position_text_' + msg + '" id="__costsheet_bid_positions_costsheet_bid_position_text_' + msg + '" value="" size="70" type="text"></td><td>&nbsp;</td><td><?php echo $currency_symbol;?></td><td>&nbsp;</td><td><input name="__costsheet_bid_positions_costsheet_bid_position_amount_' + msg + '" id="__costsheet_bid_positions_costsheet_bid_position_amount_' + msg + '" value="0.00" size="12" type="text"></td><td>&nbsp;</td><td><input name="__costsheet_bid_positions_costsheet_bid_position_comment_' + msg + '" id="__costsheet_bid_positions_costsheet_bid_position_comment_' + msg + '" value="" size="30" type="text"></td><td>&nbsp;</td><td><span id="__costsheet_bid_positions_delete_links_' + msg + '"><a class="delete_line" data="' + msg + '" href="javascript:void(0);" title="Remove line"><img src="/pictures/closed.gif"></a></span></td></tr>';

						object.closest('tr').before(new_tr);
						
					}
				}
			});
		});

		$(document).on('click', '.delete_line', function(){
			var object = $(this);
			var data = $(this).attr('data');
			var params = "pid=<?php echo param('pid');?>&bid=<?php echo param('id');?>&id=" + data + '&action=delete_line';

			$.ajax({
				type: "POST",
				data: params,
				url: "include/ajx_save_bid_position.php",
				success: function(msg){
					
					object.parent("span").parent("td").parent("tr").remove();
					if(msg != 'error') {
						result = $.parseJSON(msg);
						var sgt = '#sgt' + result['costsheet_bid_position_pcost_subgroup_id'];
						var amount = result['sgr_total'];
						$(sgt).html($.number( amount, 2, ".", "'" ));

						var gt = '#gt' + result['costsheet_bid_position_pcost_group_id'];
						var amount = result['gr_total'];
						$(gt).html($.number( amount, 2, ".", "'" ));
					}
				}
			});
		});
	<?php
	}
	?>
});


function add_new_cost_position(pid,bid)
{		
	url = '/user/project_costs_add_positions.php?pid=' + pid + '&mode=bid' + '&bid=' + bid;
	$.nyroModalManual({
	  url: url
	});

}

function select_all_cost_positions_for_budget(gid)
{
	var selector = "#selector" + gid;

	if($(selector).html() == ' Budget ')
	{
		<?php
			foreach($checkbox_ids as $key=>$ids)
			{
				?>
				if(<?php echo $key;?> == gid){
					<?php
						foreach($ids as $key2=>$id)
						{
							echo "$('#" . $id . "').attr('checked', false);";

							?>
							
							var params = "pid=<?php echo param('pid');?>&bid=<?php echo param('id');?>&id=<?php echo $id;?>&value=0&action=update_budget";
		
							  $.ajax({
									type: "POST",
									data: params,
									url: "include/ajx_save_bid_position.php",
									success: function(msg){
										if(msg != 'error') {

										}
									}
								});

							<?php
						}
					?>

				    
				}
				<?php
			}
		?>
		$(selector).html(' Budget');
	}
	else
	{
		<?php
			foreach($checkbox_ids as $key=>$ids)
			{
				?>
				if(<?php echo $key;?> == gid){
					<?php
						foreach($ids as $key2=>$id)
						{
							echo "$('#" . $id . "').attr('checked', true);";

							?>
							
							var params = "pid=<?php echo param('pid');?>&bid=<?php echo param('id');?>&id=<?php echo $id;?>&value=1&action=update_budget";
		
							  $.ajax({
									type: "POST",
									data: params,
									url: "include/ajx_save_bid_position.php",
									success: function(msg){
										if(msg != 'error') {

										}
									}
								});

							<?php
						}
					?>

				}
				<?php
			}
		?>
		$(selector).html(' Budget ');
	}
}

</script>

<?php

$page->footer();

?>