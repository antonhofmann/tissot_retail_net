<?php
/********************************************************************

    project_add_delivery_address.php

    Add delivery address on trying to approve budget (step 620)

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-04-24
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-04-24
    Version:        1.0.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";

check_access("can_create_new_projects");


/********************************************************************
    prepare all data needed
*********************************************************************/


$project = get_project(param("pid"));


if($project["pipeline"] == 0)
{
	$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");
}
elseif($project["pipeline"] == 1)
{
	$pos_data = get_poslocation_from_pipeline($project["posaddress_id"], $project["order_id"]);
}


$roles = get_user_roles(user_id());

// get the region of users' address
$user_region = get_user_region(user_id());
// get user data
$user = get_user(user_id());

// get users' company address
$client_address = get_address($project["order_client_address"]);



$franchisee_address = get_address($project["order_franchisee_address_id"]);


// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";


// create sql for the transportation type listbox
$sql_transportation_modes = "select transportation_type_id, transportation_type_name ".
                            "from transportation_types ".
                            "where transportation_type_visible = 1 " .
							" and transportation_type_code = 'mode' " . 
                            "order by transportation_type_name";


$sql_transportation_arranged = "select transportation_type_id, transportation_type_name ".
								"from transportation_types ".
								"where transportation_type_visible = 1 " .
								" and transportation_type_code = 'arranged' " . 
								"order by transportation_type_name";

$choices = array();

//standard delivery_addresses
$sql_delivery_addresses = "select * " . 
                          "from standard_delivery_addresses " . 
						  "where delivery_address_address_id = " . $project["order_client_address"] . 
						  " order by delivery_address_company";

$res = mysql_query($sql_delivery_addresses);

while ($row = mysql_fetch_assoc($res))
{
	$choices["s" . $row["delivery_address_id"]] = $row["delivery_address_company"] . ", " . $row["delivery_address_place"];
}

$choices[1] = "POS location";
$choices[2] = $client_address['company'];


if($project["project_cost_type"] == 2 or $project["project_cost_type"] == 5) // franchisee or cooperation Tissot 
{
	$choices[3] = "POS Owner Address";
}

$choices[4] = "Other";


//get invoice_addresses
$invoice_addresses = array();

$sql_inv = "select invoice_address_id, concat(invoice_address_company, ', ' ,place_name, ', ', country_name) as company " .
    "from invoice_addresses " .
	"left join places on place_id = invoice_address_place_id " . 
	"left join countries on country_id = invoice_address_country_id " . 
	"where invoice_address_active = 1 " . 
	" and invoice_address_address_id = " . $project["order_client_address"] . 
    " order by invoice_address_company";

$res = mysql_query($sql_inv) or dberror($sql_inv);
while ($row = mysql_fetch_assoc($res))
{
	$invoice_addresses[$row["invoice_address_id"]] = $row["company"];
}

$delivery_address = get_order_address(2, $project["project_order"]);


/********************************************************************
    build form
*********************************************************************/
$form = new Form("order_addresses", "project");

$form->add_hidden("pid", param('pid'));
$form->add_hidden("id", param('id'));
$form->add_hidden("order_id", $project["project_order"]);


require_once "include/project_head_small.php";

if(count($invoice_addresses) > 0)
{
	$invoice_addresses[0] = $_SESSION["new_project_step_2"]["client_address_company"];
	
	$form->add_section("Bill to");
	$form->add_comment("Please indicate the billing address in case suppliers do send invoices directly to your company.");
	$form->add_list("invoice_address_id", "Bill to", $invoice_addresses);
}
else
{
	$form->add_hidden("invoice_address_id", 0);
}


$form->add_section("Ship to");
$tmp_text1="Please indicate or enter the ship to information.";

$form->add_charlist("delivery_is_address", "Ship to address is identical to*", $choices, SUBMIT);
$form->add_hidden("delivery_address_id");

if(param("delivery_is_address") and param("delivery_is_address") == 4)
{
	$form->add_edit("delivery_address_company", "Company*", NOTNULL, "", TYPE_CHAR);
	$form->add_edit("delivery_address_company2", "", 0, "", TYPE_CHAR);
	
	$form->add_list("delivery_address_country", "Country*", $sql_countries, NOTNULL | SUBMIT);


	if(param("delivery_address_country")) {
		$sql_provinces = "select province_id, province_canton from provinces where province_country = " . param("delivery_address_country") . " order by province_canton";
	}
	else
	{
		$sql_provinces = "select province_id, province_canton from provinces where province_country = 0";
	}

	$form->add_list("delivery_address_province_id", "Province*", $sql_provinces, SUBMIT,
                isset($order_data['delivery_address_province_id']) ?
                $order_data['delivery_address_province_id'] :  "");

	
	if(param("delivery_address_province_id")) {
		$sql_places = "select place_id, place_name from places where place_province = " . param("delivery_address_province_id") . " order by place_name";
	}
	elseif(param("delivery_address_country")) {
		$sql_places = "select place_id, place_name from places where place_country = " . param("delivery_address_country") . " order by place_name";
	}
	else
	{
		$sql_places = "select place_id, place_name from places where place_country = 0";
	}
	
	
	$places = array();
	$places[999999999] = "Other city not listed below";
	$res = mysql_query($sql_places);
	while ($row = mysql_fetch_assoc($res))
	{
		$places[$row["place_id"]] = $row["place_name"];
	}

	$form->add_list("delivery_address_place_id", "City*", $places, SUBMIT,
                isset($order_data['delivery_address_place_id']) ?
                $order_data['delivery_address_place_id'] :  "");

	if(param("delivery_address_place_id") == 999999999)
	{
		$form->add_edit("delivery_address_place", "City*", NOTNULL, "", TYPE_CHAR, 20, 0, 1, "city");
	}
	else {
		$form->add_edit("delivery_address_place", "City", DISABLED);
	}

	$form->add_edit("delivery_address_zip", "ZIP", 0, "", TYPE_CHAR, 20);


	$form->add_edit("delivery_address_address", "Street*", NOTNULL, "", TYPE_CHAR);

	$form->add_hidden("delivery_address_address");
	$form->add_multi_edit("delivery_street", array("order_address_street", "order_address_street_number"), "Street/Street number", array(NOTNULL, ''), array('', ''), array('', ''), array(200, 6), array(), 0, '', '', array(40, 5));

	$form->add_edit("delivery_address_address2", "Additional Address Info", 0, "", TYPE_CHAR);
	

	$form->add_hidden("delivery_address_phone");
	$form->add_multi_edit("delivery_phone_number", array("order_address_phone_country", "order_address_phone_area", "order_address_phone_number"), "Phone: Country/Area/Number*", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array('','',''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

	$form->add_hidden("delivery_address_mobile_phone");
	$form->add_multi_edit("delivery_mobile_phone_number", array("order_address_mobile_phone_country", "order_address_mobile_phone_area", "order_address_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array('','',''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

	$form->add_edit("delivery_address_email", "Email",  0, "", TYPE_CHAR);
	$form->add_edit("delivery_address_contact", "Contact*",  NOTNULL, "", TYPE_CHAR);

}
elseif(substr(param("delivery_is_address"), 0,1) == 's') // standard delivery address
{
	$form->add_edit("delivery_address_company", "Company*", NOTNULL, "", TYPE_CHAR);
	$form->add_edit("delivery_address_company2", "", 0, "", TYPE_CHAR);

	
	$form->add_list("delivery_address_country", "Country*", $sql_countries, NOTNULL | SUBMIT);


	if(param("delivery_address_country")) {
		$sql_provinces = "select province_id, province_canton from provinces where province_country = " . param("delivery_address_country") . " order by province_canton";
	}
	else
	{
		$did = substr(param("delivery_is_address"), 1, strlen(param("delivery_is_address")));
		$sql_delivery_addresses = "select * " . 
								  "from standard_delivery_addresses " .
			                      "left join places on place_id = delivery_address_place_id " . 
			                      "left join provinces on province_id = place_province " . 
						          "where delivery_address_id = " . dbquote($did);

		$res = mysql_query($sql_delivery_addresses);

		if ($row = mysql_fetch_assoc($res))
		{
			$sql_provinces = "select province_id, province_canton from provinces where province_country = " . $row["delivery_address_country"];
		}
		else
		{
			$sql_provinces = "select province_id, province_canton from provinces where province_country = 0";
		}
	}

	$form->add_list("delivery_address_province_id", "Province*", $sql_provinces, SUBMIT,
                isset($order_data['delivery_address_province_id']) ?
                $order_data['delivery_address_province_id'] :  "");


	if(param("delivery_address_province_id")) {
		$sql_places = "select place_id, place_name from places where place_province = " . param("delivery_address_province_id") . " order by place_name";
	}
	elseif(param("delivery_address_country")) {
		$sql_places = "select place_id, place_name from places where place_country = " . param("delivery_address_country") . " order by place_name";
	}
	else
	{
		$did = substr(param("delivery_is_address"), 1, strlen(param("delivery_is_address")));
		$sql_delivery_addresses = "select * " . 
								  "from standard_delivery_addresses " .
			                      "left join places on place_id = delivery_address_place_id " . 
			                      "left join provinces on province_id = place_province " . 
						          "where delivery_address_id = " . dbquote($did);

		$res = mysql_query($sql_delivery_addresses);

		if ($row = mysql_fetch_assoc($res))
		{
			$sql_places = "select place_id, place_name from places where place_province = " . $row["place_province"] . " order by place_name";
		}
		else
		{
			$sql_places = "select place_id, place_name from places where place_country = 0";
		}
	}


	$places = array();
	$places[999999999] = "Other city not listed below";
	$res = mysql_query($sql_places);
	while ($row = mysql_fetch_assoc($res))
	{
		$places[$row["place_id"]] = $row["place_name"];
	}
	
	$form->add_list("delivery_address_place_id", "City*", $places, SUBMIT,
                isset($order_data['delivery_address_place_id']) ?
                $order_data['delivery_address_place_id'] :  "");

	if(param("delivery_address_place_id") == 999999999)
	{
		$form->add_edit("delivery_address_place", "City*", NOTNULL, "", TYPE_CHAR, 20, 0, 1, "city");
	}
	else {
		$form->add_edit("delivery_address_place", "City", DISABLED);
	}

	$form->add_hidden("delivery_address_address");
	$form->add_multi_edit("delivery_street", array("order_address_street", "order_address_street_number"), "Street/Street number", array(NOTNULL, ''), array('', ''), array('', ''), array(200, 6), array(), 0, '', '', array(40, 5));

	$form->add_edit("delivery_address_address2", "Additional Address Info", 0, "", TYPE_CHAR);
	$form->add_edit("delivery_address_zip", "ZIP", 0, "", TYPE_CHAR, 20);

	$form->add_hidden("delivery_address_phone");
	$form->add_multi_edit("delivery_phone_number", array("order_address_phone_country", "order_address_phone_area", "order_address_phone_number"), "Phone: Country/Area/Number*", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array('','',''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

	$form->add_hidden("delivery_address_mobile_phone");
	$form->add_multi_edit("delivery_mobile_phone_number", array("order_address_mobile_phone_country", "order_address_mobile_phone_area", "order_address_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array('','',''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));


	$form->add_edit("delivery_address_email", "Email",  0, "", TYPE_CHAR);
	$form->add_edit("delivery_address_contact", "Contact*",  NOTNULL, "", TYPE_CHAR);
}
elseif(param("delivery_is_address") > 0)
{
	if(param("delivery_is_address") == 1)
	{
		$form->add_label("delivery_address_company", "POS Name");
	}
	elseif(param("delivery_is_address") == 3)
	{
		$form->add_label("delivery_address_company", "POS Owner Company");
	}
	else
	{
		$form->add_label("delivery_address_company", "Company");
	}
	$form->add_label("delivery_address_company2", "");
	$form->add_label("delivery_address_address", "Street");
	$form->add_hidden("delivery_address_street");
	$form->add_hidden("delivery_address_streetnumber");

	$form->add_label("delivery_address_address2", "Additional Address Info");
	$form->add_label("delivery_address_zip", "ZIP");
	$form->add_label("delivery_address_place", "City");
	$form->add_hidden("delivery_address_place_id");
	$form->add_hidden("delivery_address_province_id");

	$form->add_hidden("delivery_address_country");
	
	if(param("delivery_is_address") == 1)
	{
		$country = $pos_data["posaddress_country"];
	}
	elseif(param("delivery_is_address") == 2)
	{
		$country = $client_address["country"];
	}
	elseif(param("delivery_is_address") == 3)
	{
		$country = $franchisee_address["country"];
	}
	$form->add_lookup("delivery_address_country_name", "Country",  "countries", "country_name", 0, $country);
	
	

	if(param("delivery_is_address") != 4)
	{
		$form->add_hidden("delivery_address_phone");
		$form->add_multi_edit("delivery_phone_number", array("order_address_phone_country", "order_address_phone_area", "order_address_phone_number"), "Phone: Country/Area/Number*", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array('','',''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

		$form->add_hidden("delivery_address_mobile_phone");
		$form->add_multi_edit("delivery_mobile_phone_number", array("order_address_mobile_phone_country", "order_address_mobile_phone_area", "order_address_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array('','',''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

		$form->add_edit("delivery_address_email", "Email",  0, "", TYPE_CHAR);
		$form->add_edit("delivery_address_contact", "Contact*",  NOTNULL, "", TYPE_CHAR);
	}
	else
	{
		$form->add_hidden("delivery_address_phone");
		$form->add_hidden("delivery_address_phone_counutry");
		$form->add_hidden("delivery_address_phone_area");
		$form->add_hidden("delivery_address_phone_number");

		$form->add_hidden("delivery_address_mobile_phone");
		$form->add_hidden("delivery_address_mobile_phone_counutry");
		$form->add_hidden("delivery_address_mobile_phone_area");
		$form->add_hidden("delivery_address_mobile_phone_number");

		$form->add_hidden("delivery_address_email", "Email");
		$form->add_hidden("delivery_address_contact");
	}
}
elseif(count($delivery_address) > 0 and $delivery_address["place_id"] > 0)
{
	$form->add_edit("delivery_address_company", "Company*", NOTNULL, $delivery_address["company"], TYPE_CHAR);
	$form->add_edit("delivery_address_company2", "", 0, $delivery_address["company2"], TYPE_CHAR);
	
	$form->add_list("delivery_address_country", "Country*", $sql_countries, NOTNULL | SUBMIT, isset($delivery_address["country"]) ?
                $delivery_address["country"] :  "");


	if(param("delivery_address_country")) {
		$sql_provinces = "select province_id, province_canton from provinces where province_country = " . param("delivery_address_country") . " order by province_canton";
	}
	else
	{
		$sql_provinces = "select province_id, province_canton from provinces where province_country = " . dbquote($delivery_address["country"]);
	}

	$form->add_list("delivery_address_province_id", "Province*", $sql_provinces, SUBMIT,
                isset($delivery_address["province_id"]) ?
                $delivery_address["province_id"] :  "");

	
	if(param("delivery_address_province_id")) {
		$sql_places = "select place_id, place_name from places where place_province = " . param("delivery_address_province_id") . " order by place_name";
	}
	elseif(param("delivery_address_country")) {
		$sql_places = "select place_id, place_name from places where place_country = " . param("delivery_address_country") . " order by place_name";
	}
	else
	{
		$sql_places = "select place_id, place_name from places where place_province = " . dbquote($delivery_address["province_id"]);
	}
	
	
	$places = array();
	$places[999999999] = "Other city not listed below";
	$res = mysql_query($sql_places);
	while ($row = mysql_fetch_assoc($res))
	{
		$places[$row["place_id"]] = $row["place_name"];
	}

	$form->add_list("delivery_address_place_id", "City*", $places, SUBMIT,
                isset($delivery_address["place_id"]) ?
                $delivery_address["place_id"] :  "");

	if(param("delivery_address_place_id") == 999999999)
	{
		$form->add_edit("delivery_address_place", "City*", NOTNULL, "", TYPE_CHAR, 20, 0, 1, "city");
	}
	else {
		$form->add_edit("delivery_address_place", "City", DISABLED);
	}

	$form->add_edit("delivery_address_zip", "ZIP", 0, $delivery_address["zip"], TYPE_CHAR, 20);

	$form->add_hidden("delivery_address_address", $delivery_address["address"]);
	$form->add_multi_edit("delivery_street", array("order_address_street", "order_address_street_number"), "Street/Street number", array(NOTNULL, ''), array('', ''), array('', ''), array(40, 6), array(), 0, '', '', array(40, 5));


	$form->add_edit("delivery_address_address2", "Additional Address Info", 0, $delivery_address["address2"], TYPE_CHAR);
	
	$form->add_hidden("delivery_address_phone", $delivery_address["phone"]);
	$form->add_multi_edit("delivery_phone_number", array("order_address_phone_country", "order_address_phone_area", "order_address_phone_number"), "Phone: Country/Area/Number*", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array($delivery_address["phone_country"], $delivery_address["phone_area"], $delivery_address["phone_number"]), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

	$form->add_hidden("delivery_address_mobile_phone",  $delivery_address["mobile_phone"]);
	$form->add_multi_edit("delivery_mobile_phone_number", array("order_address_mobile_phone_country", "order_address_mobile_phone_area", "order_address_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array($delivery_address["mobile_phone_country"], $delivery_address["mobile_phone_area"], $delivery_address["mobile_phone_number"]), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

	$form->add_edit("delivery_address_email", "Email",  0, $delivery_address["email"], TYPE_CHAR);
	$form->add_edit("delivery_address_contact", "Contact*",  NOTNULL, $delivery_address["contact"], TYPE_CHAR);

}
else
{

	$form->add_hidden("delivery_address_company", "");
	$form->add_hidden("delivery_address_company2", "");
	$form->add_hidden("delivery_address_address", "");
	$form->add_hidden("delivery_address_street", "");
	$form->add_hidden("delivery_address_streetnumber", "");
	$form->add_hidden("delivery_address_address2", "");
	$form->add_hidden("delivery_address_zip", "");
	$form->add_hidden("delivery_address_place", "");
	$form->add_hidden("delivery_address_place_id");
	$form->add_hidden("delivery_address_province_id");
	$form->add_hidden("delivery_address_country");
	$form->add_hidden("delivery_address_phone", "");
	$form->add_hidden("delivery_address_phone_country", "");
	$form->add_hidden("delivery_address_phone_area", "");
	$form->add_hidden("delivery_address_phone_number", "");
	$form->add_hidden("delivery_address_mobile_phone", "");
	$form->add_hidden("delivery_address_mobile_phone_country", "");
	$form->add_hidden("delivery_address_mobile_phone_area", "");
	$form->add_hidden("delivery_address_mobile_phone_number", "");
	$form->add_hidden("delivery_address_email", "");
	$form->add_hidden("delivery_address_contact");
}

$form->add_section("Preferred Arrival Dates of Goods for each Supplier");
$form->add_comment("Please enter the date in the form of dd.mm.yy");


//add preferred arrival dates for each supplier
$supplier_fields = array();
$sql_suppliers = "select distinct address_id, address_company, " . 
                 "item_category, item_category_name, order_item_preferred_arrival_date " . 
			     " from order_items " . 
				   " left join addresses on address_id = order_item_supplier_address " .
				   " left join items on item_id = order_item_item " . 
				   " left join item_categories on item_category_id = item_category " . 
				   " where order_item_order = " . $project["project_order"] .
				   " and address_id > 0 " .
				   " and (order_item_preferred_arrival_date is null or order_item_preferred_arrival_date = '0000-00-00') ".
				   " and order_item_not_in_budget <> 1 " .
				   " order by address_company";

$res = mysql_query($sql_suppliers) or dberror($sql_suppliers);
while ($row = mysql_fetch_assoc($res))
{
	if(!$row["item_category"])
	{
		$row["item_category"] = 0;
		$row["item_category_name"] = "Special Items";
	}

	$f = $row["address_id"] . '_' . $row["item_category"];
	
	$label = '<strong>' . $row["item_category_name"] . '</strong>  - ' . $row["address_company"];
	$form->add_edit("preferred_delivery_date_" . $row["address_id"] . '_' . $row["item_category"], $label, NOTNULL, to_system_date($row["order_item_preferred_arrival_date"]), TYPE_DATE, 10, "", 4, "preferred_arrival_date" . $f);
	
	$supplier_fields[$f] = "preferred_delivery_date_" . $row["address_id"] . '_' . $row["item_category"];
}

//$form->add_edit("preferred_delivery_date", "Preferred Arrival Date*", NOTNULL, to_system_date($project["order_preferred_delivery_date"]), TYPE_DATE, 20);
$form->add_section("Preferences and Traffic Checklist");

$form->add_list("preferred_transportation_arranged", "Transportation arranged by*", $sql_transportation_arranged, NOTNULL, $project["order_preferred_transportation_arranged"]);
$form->add_list("preferred_transportation_mode", "Transportation mode*", $sql_transportation_modes, NOTNULL, $project["order_preferred_transportation_mode"]);



//$form->add_radiolist( "packaging_retraction", "Packaging Retraction Desired", array(0 => "no", 1 => "yes"), 0, 0);

$form->add_comment("Please indicate if there is a special approval needed for delivery into ".
                   "a pedestrian area."); 
$form->add_radiolist( "pedestrian_mall_approval", "Pedestrian Area Approval Needed",
    array(0 => "no", 1 => "yes"), 0, $project["order_pedestrian_mall_approval"]);
$form->add_comment("Please indicate if partial delivery is possible or full delivery is required."); 
$form->add_radiolist( "full_delivery", "Full Delivery",
    array(0 => "no", 1 => "yes"), 0, $project["order_full_delivery"]);
$form->add_comment("Please indicate any other circumstances/restrictions concerning delivery and traffic."); 
$form->add_multiline("delivery_comments", "Delivery Comments", 4, 0, $project["order_delivery_comments"]);


$form->add_section("Insurance");

if($project["order_preferred_transportation_arranged"] == 7) {
	$insurance_info = '<span class="highlite_value_bold" id="insurance_info">Insurance is covered by Tissot.</span>';
}
else
{
	$insurance_info = '<span class="highlite_value_bold" id="insurance_info">Insurance has to be covered by the client.</span>';
}

$form->add_comment($insurance_info);

/*
	$form->add_radiolist("order_insurance", array(1=>"Insurance by Tissot/Forwarder", 0=>""), array(1=>"covered",0=>"not covered"), DISABLED,$project["order_insurance"]);
*/

$form->add_section("General Comments");
$form->add_multiline("comments", "Comments", 4, 0, $project["project_comments"]);

$form->add_section(" ");

if(count($delivery_address) > 0 and $delivery_address["place_id"] > 0)
{
	$form->error("Before accepting the budget you must verify the information about the delivery.");
}
else
{
	$form->error("Before accepting the budget you must provide all neccessary information about the delivery.");
}
$form->add_button("back", "Back");
$form->add_button("save", "Save Delivery Information");

if($user["address"] != $client_address["id"])
{
	$form->add_button("proceed", "Skip this Step");
}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$form->populate();
$form->process();


//clean fields from apostrophes
foreach($form->items as $key=>$value)
{
	if(array_key_exists('value', $value)) {
		$form->value($key, str_replace('"', '', $form->value($key)));
	}
}

if ($form->button("back"))
{
	$link = "project_task_center.php?pid=" . param("pid");
	redirect($link);
}
else if ($form->button("delivery_address_place_id"))
{
    $country_phone_prefix = get_country_phone_prefix(param("delivery_address_country"));
	
	if ($form->value("delivery_address_place_id"))
    {
       $sql = "select place_name, place_province from places where place_id = " . dbquote($form->value("delivery_address_place_id"));
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$form->value("delivery_address_place", $row["place_name"]);
			$form->value("delivery_address_province_id", $row["place_province"]);
			$form->value("delivery_address_zip", "");
			$form->value("delivery_address_address", "");
			if(array_key_exists('delivery_street', $form->items))
			{
				$form->items['delivery_street']['values'][0] = "";
				$form->items['delivery_street']['values'][1] = "";
			}
			elseif(array_key_exists('delivery_address_street', $form->items))
			{
				$form->value("delivery_address_street", "");
				$form->value("delivery_address_streetnumber", "");
			}

			$form->value("delivery_address_address2", "");
			$form->value("delivery_address_phone", "");
			if(array_key_exists('delivery_phone_number', $form->items))
			{
				$form->items['delivery_phone_number']['values'][0] = $country_phone_prefix;
				$form->items['delivery_phone_number']['values'][1] = "";
				$form->items['delivery_phone_number']['values'][2] = "";
			}
			elseif(array_key_exists('delivery_address_phone_country', $form->items))
			{
				$form->value("delivery_address_phone_country", "");
				$form->value("delivery_address_phone_area", "");
				$form->value("delivery_address_phone_number", "");
			}
			$form->value("delivery_address_mobile_phone", "");
			if(array_key_exists('delivery_mobile_phone_number', $form->items))
			{
				$form->items['delivery_mobile_phone_number']['values'][0] = $country_phone_prefix;
				$form->items['delivery_mobile_phone_number']['values'][1] = "";
				$form->items['delivery_mobile_phone_number']['values'][2] = "";
			}
			elseif(array_key_exists('delivery_address_mobile_phone_country', $form->items))
			{
				$form->value("delivery_address_mobile_phone_country", "");
				$form->value("delivery_address_mobile_phone_area", "");
				$form->value("delivery_address_mobile_phone_number", "");
			}
			

			$form->value("delivery_address_contact", "");
		}
    }
}
else if ($form->button("delivery_address_country"))
{
	$country_phone_prefix = get_country_phone_prefix(param("delivery_address_country"));
	
	$form->value("delivery_address_place", "");
	$form->value("delivery_address_place_id", 0);
	$form->value("delivery_address_province_id", 0);
	$form->value("delivery_address_zip", "");
	$form->value("delivery_address_address", "");
	if(array_key_exists('delivery_street', $form->items))
	{
		$form->items['delivery_street']['values'][0] = "";
		$form->items['delivery_street']['values'][1] = "";
	}
	elseif(array_key_exists('delivery_address_street', $form->items))
	{
		$form->value("delivery_address_street", "");
		$form->value("delivery_address_streetnumber", "");
	}
	$form->value("delivery_address_address2", "");
	$form->value("delivery_address_phone", "");
	if(array_key_exists('delivery_phone_number', $form->items))
	{
		$form->items['delivery_phone_number']['values'][0] = $country_phone_prefix;
		$form->items['delivery_phone_number']['values'][1] = "";
		$form->items['delivery_phone_number']['values'][2] = "";
	}
	elseif(array_key_exists('delivery_address_phone_country', $form->items))
	{
		$form->value("delivery_address_phone_country", "");
		$form->value("delivery_address_phone_area", "");
		$form->value("delivery_address_phone_number", "");
	}
	$form->value("delivery_address_mobile_phone", "");
	if(array_key_exists('delivery_mobile_phone_number', $form->items))
	{
		$form->items['delivery_mobile_phone_number']['values'][0] = $country_phone_prefix;
		$form->items['delivery_mobile_phone_number']['values'][1] = "";
		$form->items['delivery_mobile_phone_number']['values'][2] = "";
	}
	elseif(array_key_exists('delivery_address_mobile_phone_country', $form->items))
	{
		$form->value("delivery_address_mobile_phone_country", "");
		$form->value("delivery_address_mobile_phone_area", "");
		$form->value("delivery_address_mobile_phone_number", "");
	}
	$form->value("delivery_address_contact", "");
}
else if ($form->button("delivery_address_province_id"))
{
	
	$country_phone_prefix = get_country_phone_prefix(param("delivery_address_country"));

	$form->value("delivery_address_place", "");
	$form->value("delivery_address_place_id", 0);
	$form->value("delivery_address_province_id", param("delivery_address_province_id"));
	$form->value("delivery_address_zip", "");
	$form->value("delivery_address_address", "");
	if(array_key_exists('delivery_street', $form->items))
	{
		$form->items['delivery_street']['values'][0] = "";
		$form->items['delivery_street']['values'][1] = "";
	}
	elseif(array_key_exists('delivery_address_street', $form->items))
	{
		$form->value("delivery_address_street", "");
		$form->value("delivery_address_streetnumber", "");
	}
	$form->value("delivery_address_address2", "");
	$form->value("delivery_address_phone", "");
	if(array_key_exists('delivery_phone_number', $form->items))
	{
		$form->items['delivery_phone_number']['values'][0] = $country_phone_prefix;
		$form->items['delivery_phone_number']['values'][1] = "";
		$form->items['delivery_phone_number']['values'][2] = "";
	}
	elseif(array_key_exists('delivery_address_phone_country', $form->items))
	{
		$form->value("delivery_address_phone_country", "");
		$form->value("delivery_address_phone_area", "");
		$form->value("delivery_address_phone_number", "");
	}
	$form->value("delivery_address_mobile_phone", "");
	if(array_key_exists('delivery_mobile_phone_number', $form->items))
	{
		$form->items['delivery_mobile_phone_number']['values'][0] = $country_phone_prefix;
		$form->items['delivery_mobile_phone_number']['values'][1] = "";
		$form->items['delivery_mobile_phone_number']['values'][2] = "";
	}
	elseif(array_key_exists('delivery_address_mobile_phone_country', $form->items))
	{
		$form->value("delivery_address_mobile_phone_country", "");
		$form->value("delivery_address_mobile_phone_area", "");
		$form->value("delivery_address_mobile_phone_number", "");
	}
	$form->value("delivery_address_contact", "");
}
elseif ($form->button("delivery_is_address"))
{
	// set delivery address
    if($form->value("delivery_is_address") == 1) //POS Address
	{
		$form->value("delivery_address_id", 0);
		$form->value("delivery_address_company", $pos_data["posaddress_name"]);
		$form->value("delivery_address_company2",  $pos_data["posaddress_name2"]);
		$form->value("delivery_address_address",  $pos_data["posaddress_address"]);

		if(array_key_exists('delivery_street', $form->items))
		{
			$form->items['delivery_street']['values'][0] = $pos_data["posaddress_street"];
			$form->items['delivery_street']['values'][1] = $pos_data["posaddress_street_number"];
		}
		elseif(array_key_exists('delivery_address_street', $form->items))
		{
			$form->value("delivery_address_street", $pos_data["posaddress_street"]);
			$form->value("delivery_address_streetnumber", $pos_data["posaddress_street_number"]);
		}

		$form->value("delivery_address_address2",  $pos_data["posaddress_address2"]);
		$form->value("delivery_address_zip",  $pos_data["posaddress_zip"]);
		$form->value("delivery_address_place",  $pos_data["posaddress_place"]);
		$form->value("delivery_address_place_id",  $pos_data["posaddress_place_id"]);
		$form->value("delivery_address_province_id", $pos_data["province_id"]);
		$form->value("delivery_address_country",  $pos_data["posaddress_country"]);
		$form->value("delivery_address_phone",  $pos_data["posaddress_phone"]);
		if(array_key_exists('delivery_phone_number', $form->items))
		{
			$form->items['delivery_phone_number']['values'][0] = $pos_data["posaddress_phone_country"];
			$form->items['delivery_phone_number']['values'][1] = $pos_data["posaddress_phone_area"];
			$form->items['delivery_phone_number']['values'][2] = $pos_data["posaddress_phone_number"];
		}
		elseif(array_key_exists('delivery_address_phone_country', $form->items))
		{
			$form->value("delivery_address_phone_country", $pos_data["posaddress_phone_country"]);
			$form->value("delivery_address_phone_area", $pos_data["posaddress_phone_area"]);
			$form->value("delivery_address_phone_number", $pos_data["posaddress_phone_number"]);
		}
		$form->value("delivery_address_mobile_phone",  $pos_data["posaddress_mobile_phone"]);

		if(array_key_exists('delivery_mobile_phone_number', $form->items))
		{
			$form->items['delivery_mobile_phone_number']['values'][0] = $pos_data["posaddress_mobile_phone_country"];
			$form->items['delivery_mobile_phone_number']['values'][1] = $pos_data["posaddress_mobile_phone_area"];
			$form->items['delivery_mobile_phone_number']['values'][2] = $pos_data["posaddress_mobile_phone_number"];
		}
		elseif(array_key_exists('delivery_address_mobile_phone_country', $form->items))
		{
			$form->value("delivery_address_mobile_phone_country", $pos_data["posaddress_mobile_phone_country"]);
			$form->value("delivery_address_mobile_phone_area", $pos_data["posaddress_mobile_phone_area"]);
			$form->value("delivery_address_mobile_phone_number", $pos_data["posaddress_mobile_phone_number"]);
		}

		$form->value("delivery_address_email",  $pos_data["posaddress_email"]);
	}
	elseif($form->value("delivery_is_address") == 2) //client Address
	{
		$form->value("delivery_address_id", 0);
		$form->value("delivery_address_company", $client_address["company"]);
		$form->value("delivery_address_company2",  $client_address["company2"]);
		$form->value("delivery_address_address",  $client_address["address"]);

		if(array_key_exists('delivery_street', $form->items))
		{
			$form->items['delivery_street']['values'][0] = $client_address["street"];
			$form->items['delivery_street']['values'][1] = $client_address["streetnumber"];
		}
		elseif(array_key_exists('delivery_address_street', $form->items))
		{
			$form->value("delivery_address_street", $client_address["street"]);
			$form->value("delivery_address_streetnumber", $client_address["streetnumber"]);
		}

		$form->value("delivery_address_address2",  $client_address["address2"]);
		$form->value("delivery_address_zip",  $client_address["zip"]);
		$form->value("delivery_address_place",  $client_address["place"]);
		$form->value("delivery_address_place_id",  $client_address["place_id"]);
		$form->value("delivery_address_province_id", $client_address["place_province"]);
		$form->value("delivery_address_country",  $client_address["country"]);
		$form->value("delivery_address_phone",  $client_address["phone"]);
		if(array_key_exists('delivery_phone_number', $form->items))
		{
			$form->items['delivery_phone_number']['values'][0] = $client_address["phone_country"];
			$form->items['delivery_phone_number']['values'][1] = $client_address["phone_area"];
			$form->items['delivery_phone_number']['values'][2] = $client_address["phone_number"];
		}
		elseif(array_key_exists('delivery_address_phone_country', $form->items))
		{
			$form->value("delivery_address_phone_country", $client_address["phone_country"]);
			$form->value("delivery_address_phone_area", $client_address["phone_area"]);
			$form->value("delivery_address_phone_number", $client_address["phone_number"]);
		}
		$form->value("delivery_address_mobile_phone",  $client_address["mobile_phone"]);
		if(array_key_exists('delivery_mobile_phone_number', $form->items))
		{
			$form->items['delivery_mobile_phone_number']['values'][0] = $client_address["mobile_phone_country"];
			$form->items['delivery_mobile_phone_number']['values'][1] = $client_address["mobile_phone_area"];
			$form->items['delivery_mobile_phone_number']['values'][2] = $client_address["mobile_phone_number"];
		}
		elseif(array_key_exists('delivery_address_mobile_phone_country', $form->items))
		{
			$form->value("delivery_address_mobile_phone_country", $client_address["mobile_phone_country"]);
			$form->value("delivery_address_mobile_phone_area", $client_address["mobile_phone_area"]);
			$form->value("delivery_address_mobile_phone_number", $client_address["mobile_phone_number"]);
		}

		$form->value("delivery_address_email",  $client_address["email"]);
		$form->value("delivery_address_contact",  $client_address["contact_name"]);
	}
	elseif($form->value("delivery_is_address") == 3) // franchisee address
	{
		$form->value("delivery_address_id", 0);
		$form->value("delivery_address_company", $franchisee_address["company"]);
		$form->value("delivery_address_company2",  $franchisee_address["company2"]);
		$form->value("delivery_address_address",  $franchisee_address["address"]);
		if(array_key_exists('delivery_street', $form->items))
		{
			$form->items['delivery_street']['values'][0] = $franchisee_address["street"];
			$form->items['delivery_street']['values'][1] = $franchisee_address["streetnumber"];
		}
		elseif(array_key_exists('delivery_address_street', $form->items))
		{
			$form->value("delivery_address_street", $franchisee_address["street"]);
			$form->value("delivery_address_streetnumber", $franchisee_address["streetnumber"]);
		}
		$form->value("delivery_address_address2",  $franchisee_address["address2"]);
		$form->value("delivery_address_zip",  $franchisee_address["zip"]);
		$form->value("delivery_address_place",  $franchisee_address["place"]);
		$form->value("delivery_address_place_id",  $franchisee_address["place_id"]);
		$form->value("delivery_address_province_id", $franchisee_address["place_province"]);
		$form->value("delivery_address_country",  $franchisee_address["country"]);
		$form->value("delivery_address_phone",  $franchisee_address["phone"]);
		if(array_key_exists('delivery_phone_number', $form->items))
		{
			$form->items['delivery_phone_number']['values'][0] = $franchisee_address["phone_country"];
			$form->items['delivery_phone_number']['values'][1] = $franchisee_address["phone_area"];
			$form->items['delivery_phone_number']['values'][2] = $franchisee_address["phone_number"];
		}
		elseif(array_key_exists('delivery_address_phone_country', $form->items))
		{
			$form->value("delivery_address_phone_country", $franchisee_address["phone_country"]);
			$form->value("delivery_address_phone_area", $franchisee_address["phone_area"]);
			$form->value("delivery_address_phone_number", $franchisee_address["phone_number"]);
		}
		$form->value("delivery_address_mobile_phone",  $franchisee_address["mobile_phone"]);
		if(array_key_exists('delivery_mobile_phone_number', $form->items))
		{
			$form->items['delivery_mobile_phone_number']['values'][0] = $franchisee_address["mobile_phone_country"];
			$form->items['delivery_mobile_phone_number']['values'][1] = $franchisee_address["mobile_phone_area"];
			$form->items['delivery_mobile_phone_number']['values'][2] = $franchisee_address["mobile_phone_number"];
		}
		elseif(array_key_exists('delivery_address_mobile_phone_country', $form->items))
		{
			$form->value("delivery_address_mobile_phone_country", $franchisee_address["mobile_phone_country"]);
			$form->value("delivery_address_mobile_phone_area", $franchisee_address["mobile_phone_area"]);
			$form->value("delivery_address_mobile_phone_number", $franchisee_address["mobile_phone_number"]);
		}
		$form->value("delivery_address_email",  $franchisee_address["email"]);
		$form->value("delivery_address_contact",  $franchisee_address["contact_name"]);
	}
	elseif(substr($form->value("delivery_is_address"), 0, 1) == 's') // standard delivery_address
	{
		$did = substr($form->value("delivery_is_address"), 1, strlen($form->value("delivery_is_address")));
		$sql_delivery_addresses = "select * " . 
								  "from standard_delivery_addresses " .
			                      "left join places on place_id = delivery_address_place_id " . 
			                      "left join provinces on province_id = place_province " . 
						          "where delivery_address_id = " . dbquote($did);


		$res = mysql_query($sql_delivery_addresses);

		if ($row = mysql_fetch_assoc($res))
		{
			$form->value("delivery_address_id", $row["delivery_address_id"]);
			$form->value("delivery_address_company", $row["delivery_address_company"]);
			$form->value("delivery_address_company2",  $row["delivery_address_company2"]);
			$form->value("delivery_address_address",  $row["delivery_address_address"]);
			if(array_key_exists('delivery_street', $form->items))
			{
				$form->items['delivery_street']['values'][0] = $row["delivery_address_street"];
				$form->items['delivery_street']['values'][1] = $row["delivery_address_streetnumber"];
			}
			elseif(array_key_exists('delivery_address_street', $form->items))
			{
				$form->value("delivery_address_street", $row["delivery_address_street"]);
				$form->value("delivery_address_streetnumber", $row["delivery_address_streetnumber"]);
			}
			$form->value("delivery_address_address2",  $row["delivery_address_address2"]);
			$form->value("delivery_address_zip",  $row["delivery_address_zip"]);
			$form->value("delivery_address_province_id",  $row["province_id"]);
			$form->value("delivery_address_place",  $row["delivery_address_place"]);
			$form->value("delivery_address_place_id",  $row["delivery_address_place_id"]);
			$form->value("delivery_address_country",  $row["delivery_address_country"]);
			$form->value("delivery_address_phone",  $row["delivery_address_phone"]);
			if(array_key_exists('delivery_phone_number', $form->items))
			{
				$form->items['delivery_phone_number']['values'][0] = $row["delivery_address_phone_country"];
				$form->items['delivery_phone_number']['values'][1] = $row["delivery_address_phone_area"];
				$form->items['delivery_phone_number']['values'][2] = $row["delivery_address_phone_number"];
			}
			elseif(array_key_exists('delivery_address_phone_country', $form->items))
			{
				$form->value("delivery_address_phone_country", $row["delivery_address_phone_country"]);
				$form->value("delivery_address_phone_area", $row["delivery_address_phone_area"]);
				$form->value("delivery_address_phone_number", $row["delivery_address_phone_number"]);
			}
			$form->value("delivery_address_mobile_phone",  $row["delivery_address_mobile_phone"]);
			if(array_key_exists('delivery_mobile_phone_number', $form->items))
			{
				$form->items['delivery_mobile_phone_number']['values'][0] = $row["delivery_address_mobile_phone_country"];
				$form->items['delivery_mobile_phone_number']['values'][1] = $row["delivery_address_mobile_phone_area"];
				$form->items['delivery_mobile_phone_number']['values'][2] = $row["delivery_address_mobile_phone_number"];
			}
			elseif(array_key_exists('delivery_address_mobile_phone_country', $form->items))
			{
				$form->value("delivery_address_mobile_phone_country", $row["delivery_address_mobile_phone_country"]);
				$form->value("delivery_address_mobile_phone_area", $row["delivery_address_mobile_phone_area"]);
				$form->value("delivery_address_mobile_phone_number", $row["delivery_address_mobile_phone_number"]);
			}
			$form->value("delivery_address_email",  $row["delivery_address_email"]);
			$form->value("delivery_address_contact",  $row["delivery_address_contact"]);
		}
		
	}
	else
	{
		$form->value("delivery_address_id", 0);
		$form->value("delivery_address_company", "");
		$form->value("delivery_address_company2",  "");
		$form->value("delivery_address_address",  "");
		if(array_key_exists('delivery_street', $form->items))
		{
			$form->items['delivery_street']['values'][0] = "";
			$form->items['delivery_street']['values'][1] = "";
		}
		elseif(array_key_exists('delivery_address_street', $form->items))
		{
			$form->value("delivery_address_street", "");
			$form->value("delivery_address_streetnumber", "");
		}
		$form->value("delivery_address_address2",  "");
		$form->value("delivery_address_zip",  "");
		$form->value("delivery_address_place",  "");
		$form->value("delivery_address_province_id",  "");
		$form->value("delivery_address_place_id",  "");
		$form->value("delivery_address_country",  0);
		$form->value("delivery_address_phone",  "");
		if(array_key_exists('delivery_phone_number', $form->items))
		{
			$form->items['delivery_phone_number']['values'][0] = "";
			$form->items['delivery_phone_number']['values'][1] = "";
			$form->items['delivery_phone_number']['values'][2] = "";
		}
		elseif(array_key_exists('delivery_address_phone_country', $form->items))
		{
			$form->value("delivery_address_phone_country", "");
			$form->value("delivery_address_phone_area", "");
			$form->value("delivery_address_phone_number", "");
		}
		$form->value("delivery_address_mobile_phone",  "");
		if(array_key_exists('delivery_mobile_phone_number', $form->items))
		{
			$form->items['delivery_mobile_phone_number']['values'][0] = "";
			$form->items['delivery_mobile_phone_number']['values'][1] = "";
			$form->items['delivery_mobile_phone_number']['values'][2] = "";
		}
		elseif(array_key_exists('delivery_address_mobile_phone_country', $form->items))
		{
			$form->value("delivery_address_mobile_phone_country", "");
			$form->value("delivery_address_mobile_phone_area", "");
			$form->value("delivery_address_mobile_phone_number", "");
		}
		$form->value("delivery_address_email",  "");
		$form->value("delivery_address_contact",  "");
	}
}
elseif ($form->button("save"))
{
	
	//check if all suppliers have a preferred delivery date
	$error = 0;

	foreach($supplier_fields as $key=>$field_name)
	{
		if(!$form->value($field_name))
		{
			$error = 1;
		}
		elseif(from_system_date($form->value($field_name)) <= date("Y-m-d"))
		{
			$error = 2;
		}
	}


	
	if($form->value("delivery_address_place_id") < 999999999)
	{
		$form->add_validation("{delivery_address_place_id} > 0", "You must select a city.");
	}

	if($error == 1)
	{
		$form->error("Please indicate a correct preferred arrival date for each supplier.");
	}
	elseif($error == 2)
	{
		$form->error("Preferred arrival dates must be future dates.");
	}
	elseif($error == 0)
	{
		//$form->add_validation("from_system_date({preferred_delivery_date}) >  " . dbquote(date("Y-m-d")), "The preferred arrival date must be a future date!");
	
		if(count($delivery_address) > 0 and $delivery_address["place_id"] > 0)
		{
			//nop
		}
		else
		{
			$form->add_validation("{delivery_is_address}", "You must choose an option from the drop down list \"Ship to is identical to\".");
		}

		$form->add_validation("{delivery_address_province_id}", "You must indicate a province.");


		$error = 0;
		if(strlen(preg_replace('/[^0-9a-z_ %\[\]\.\(\)%&-]/s', '', $form->value("delivery_address_address"))) < strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("delivery_address_address")))
			and strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("delivery_address_address"))) > 10)
		{
			$error = 1;
			$form->error("Company names and addresses must contain lower caracters. Do not use capital letters only.");
		}
		elseif(strlen(preg_replace('/[^0-9a-z_ %\[\]\.\(\)%&-]/s', '', $form->value("delivery_address_address2"))) < strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("delivery_address_address2")))
			and strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("delivery_address_address2"))) > 10)
		{
			$error = 1;
			$form->error("Company names and addresses must contain lower caracters. Do not use capital letters only.");
		}



		if(array_key_exists('delivery_phone_number', $form->items))
		{
			$form->value("delivery_address_phone", $form->unify_multi_edit_field($form->items["delivery_phone_number"]));
			
		}

		if(array_key_exists('delivery_mobile_phone_number', $form->items))
		{
			$form->value("delivery_address_mobile_phone", $form->unify_multi_edit_field($form->items["delivery_mobile_phone_number"]));

			$form->add_validation("{delivery_address_phone} != '' or {delivery_address_mobile_phone} != ''", "Please indcate either the phone number or the mobile phone number!");
		}
		else
		{
			$form->value("delivery_address_mobile_phone", "");
		}

		if ($error == 0 and $form->validate())
		{
			
			if(array_key_exists('delivery_street', $form->items))
			{
				$form->value("delivery_address_address", $form->unify_multi_edit_field($form->items["delivery_street"], get_country_street_number_rule($form->value("delivery_address_country"))));
			}

			
			
			
			//update order_items with prefered delivery dates
			foreach($supplier_fields as $key=>$field_name)
			{
				$tmp = explode('_', $key);
				$supplier_id = $tmp[0];
				$item_category = $tmp[1];

				
				if($item_category == 0)
				{
					$sql = "select order_item_id " . 
						   " from order_items " . 
						   " left join items on item_id = order_item_item " . 
						   " left join item_categories on item_category_id = item_category " . 
						   " where order_item_order = " . $project["project_order"] .
						   " and order_item_supplier_address = " . $supplier_id . 
						   " and (item_category = 0 or item_category is null)";
				}
				else
				{
					$sql = "select order_item_id " . 
						   " from order_items " . 
						   " left join items on item_id = order_item_item " . 
						   " left join item_categories on item_category_id = item_category " . 
						   " where order_item_order = " . $project["project_order"] .
						   " and order_item_supplier_address = " . $supplier_id . 
						   " and item_category = " . $item_category;
				}
				
				$res = mysql_query($sql) or dberror($sql);
				while ($row = mysql_fetch_assoc($res))
				{
					$fields = array();

					$value = dbquote(trim(from_system_date($form->value($field_name))));
					$fields[] = "order_item_preferred_arrival_date = " . $value;

					$value = "current_timestamp";
					$fields[] = "date_modified = " . $value;

					$value = dbquote(user_login());
					$fields[] = "user_modified = " . $value;

					$sql_u = "update order_items set " . join(", ", $fields) . " where order_item_id = " . $row["order_item_id"];
					mysql_query($sql_u) or dberror($sql_u);
				}
			}
			
			
			//insert new record into places for new city in the Delivery Address

			$place_id = $form->value("delivery_address_place_id");
			$place_name = $form->value("delivery_address_place");
			if(array_key_exists("delivery_address_place_id", $form->items))
			{
				if($form->value("delivery_address_place_id") == "999999999")
				{
					$country_id = $form->value("delivery_address_country");
					$province_id = $form->value("delivery_address_province_id");
					$place_name = $form->value("delivery_address_place");

					$sql = "insert into places (place_country, place_province, place_name, date_created, user_created) Values (" . 
						   $country_id .  "," . dbquote($province_id) . "," . dbquote($place_name) . "," . dbquote(date('Y-m-d')) . "," . dbquote(user_login()) . ")";

					$result = mysql_query($sql) or dberror($sql);

					$place_id = mysql_insert_id();
				}
			}


			//update standard delivery address
			if(array_key_exists("delivery_address_id", $form->items)
				and $form->value("delivery_address_id") > 0)
			{
				$fields = array();
				$values = array();

				$value = dbquote($form->value("delivery_address_company"));
				$fields[] = "delivery_address_company = " . $value;

				$value = dbquote($form->value("delivery_address_company2"));
				$fields[] = "delivery_address_company2 = " . $value;

				$value = dbquote($form->value("delivery_address_address"));
				$fields[] = "delivery_address_address = " . $value;

				if(array_key_exists('delivery_street', $form->items))
				{
					$value = dbquote($form->items['delivery_street']['values'][0]);
					$fields[] = "delivery_address_street = " . $value;

					$value = dbquote($form->items['delivery_street']['values'][1]);
					$fields[] = "delivery_address_streetnumber = " . $value;
				}

				$value = dbquote($form->value("delivery_address_address2"));
				$fields[] = "delivery_address_address2 = " . $value;

				$value = dbquote($form->value("delivery_address_zip"));
				$fields[] = "delivery_address_zip = " . $value;

				$value = dbquote(trim($form->value("delivery_address_place")));
				$fields[] = "delivery_address_place = " . $value;

				$fields[] = "delivery_address_place_id = " . dbquote($place_id);

				$value = dbquote($form->value("delivery_address_country"));
				$fields[] = "delivery_address_country = " . $value;

				$value = dbquote($form->value("delivery_address_phone"));
				$fields[] = "delivery_address_phone = " . $value;

				if(array_key_exists('delivery_phone_number', $form->items))
				{
					foreach($form->items['delivery_phone_number']['fields'] as $key=>$fieldname)
					{
						$fieldname = str_replace('order', 'delivery', $fieldname);
						$value = dbquote($form->items['delivery_phone_number']['values'][$key]);
						$fields[] = $fieldname . " = " . $value;

					}
				}

				$value = dbquote($form->value("delivery_address_mobile_phone"));
				$fields[] = "delivery_address_mobile_phone = " . $value;

				if(array_key_exists('delivery_mobile_phone_number', $form->items))
				{
					foreach($form->items['delivery_mobile_phone_number']['fields'] as $key=>$fieldname)
					{
						$fieldname = str_replace('order', 'delivery', $fieldname);
						$value = dbquote($form->items['delivery_mobile_phone_number']['values'][$key]);
						$fields[] = $fieldname . " = " . $value;

					}
				}

				$value = dbquote($form->value("delivery_address_email"));
				$fields[] = "delivery_address_email = " . $value;

				$value = dbquote($form->value("delivery_address_contact"));
				$fields[] = "delivery_address_contact = " . $value;

				$value = dbquote(date("Y-m-d"));
				$fields[] = "date_modified = " . $value;

				$value = dbquote(user_login());
				$fields[] = "user_modified = " . $value;

				$sql = "update standard_delivery_addresses set " . join(", ", $fields) . " where delivery_address_id = " . $form->value("delivery_address_id");
				mysql_query($sql) or dberror($sql);

			}

			
			
			$sql = "delete from order_addresses " . 
				   " where order_address_order = " . dbquote($project["project_order"]) .
				   " and order_address_type = 2 " . 
				   " and order_address_order_item is null";
			$result = mysql_query($sql) or dberror($sql);
			

			$delivery_address_fields = array();
			$delivery_address_values = array();

			// insert delivery address into table order_addresses
			
			$delivery_address = array();
			
			$delivery_address_fields[] = "order_address_order";
			$delivery_address_values[] = $project["project_order"];
			$delivery_address["order_id"] = $project["project_order"];

			$delivery_address_fields[] = "order_address_type";
			$delivery_address_values[] = 2;
			$delivery_address["address_type"] = 2;

			$delivery_address_fields[] = "order_address_company";
			$delivery_address_values[] = trim($form->value("delivery_address_company")) == "" ? "null" : dbquote($form->value("delivery_address_company"));
			$delivery_address["address_company"] = $form->value("delivery_address_company");
			
			$delivery_address_fields[] = "order_address_company2";
			$delivery_address_values[] = trim($form->value("delivery_address_company2")) == "" ? "null" : dbquote($form->value("delivery_address_company2"));
			$delivery_address["address_company2"] = $form->value("delivery_address_company2");
			
			$delivery_address_fields[] = "order_address_address";
			$delivery_address_values[] = trim($form->value("delivery_address_address")) == "" ? "null" : dbquote($form->value("delivery_address_address"));
			$delivery_address["address_address"] =$form->value("delivery_address_address");
			

			if(array_key_exists('delivery_street', $form->items))
			{
				$delivery_address_fields[] = "order_address_street";
				$delivery_address_values[] = dbquote(trim($form->items['delivery_street']['values'][0]));
				$delivery_address["address_street"] = trim($form->items['delivery_street']['values'][0]);

				$delivery_address_fields[] = "order_address_street_number";
				$delivery_address_values[] = dbquote(trim($form->items['delivery_street']['values'][1]));
				$delivery_address["address_streetnumber"] = trim($form->items['delivery_street']['values'][1]);
			}
			elseif(array_key_exists('delivery_address_street', $form->items))
			{
				$delivery_address_fields[] = "order_address_street";
				$delivery_address_values[] = trim($form->value("delivery_address_street")) == "" ? "null" : dbquote($form->value("delivery_address_street"));
				$delivery_address["address_street"] =$form->value("delivery_address_street");

				$delivery_address_fields[] = "order_address_street_number";
				$delivery_address_values[] = trim($form->value("delivery_address_streetnumber")) == "" ? "null" : dbquote($form->value("delivery_address_streetnumber"));
				$delivery_address["address_streetnumber"] =$form->value("delivery_address_streetnumber");
			}
			

			$delivery_address_fields[] = "order_address_address2";
			$delivery_address_values[] = trim($form->value("delivery_address_address2")) == "" ? "null" : dbquote($form->value("delivery_address_address2"));
			$delivery_address["address_address2"] = $form->value("delivery_address_address2");
			
			$delivery_address_fields[] = "order_address_zip";
			$delivery_address_values[] = trim($form->value("delivery_address_zip")) == "" ? "null" : dbquote($form->value("delivery_address_zip"));
			$delivery_address["address_zip"] = $form->value("delivery_address_zip");


			$delivery_address_fields[] = "order_address_place_id";
			$delivery_address_values[] = dbquote($place_id);
			$delivery_address["address_place_id"] = $place_id;
			
		
			$delivery_address_fields[] = "order_address_place";
			$delivery_address_values[] = dbquote($place_name);
			$delivery_address["address_place"] = $place_name;
			
			$delivery_address_fields[] = "order_address_country";
			$delivery_address_values[] = trim($form->value("delivery_address_country")) == "" ? "null" : dbquote($form->value("delivery_address_country"));
			$delivery_address["address_country"] = $form->value("delivery_address_country");
			
			$delivery_address_fields[] = "order_address_phone";
			$delivery_address_values[] = trim($form->value("delivery_address_phone")) == "" ? "null" : dbquote($form->value("delivery_address_phone"));
			$delivery_address["address_phone"] = $form->value("delivery_address_phone");

			if(array_key_exists('delivery_phone_number', $form->items))
			{
				$delivery_address_fields[] = "order_address_phone_country";
				$delivery_address_values[] = dbquote(trim($form->items['delivery_phone_number']['values'][0]));
				$delivery_address["address_phone_country"] = trim($form->items['delivery_phone_number']['values'][0]);

				$delivery_address_fields[] = "order_address_phone_area";
				$delivery_address_values[] = dbquote(trim($form->items['delivery_phone_number']['values'][1]));
				$delivery_address["address_phone_area"] = trim($form->items['delivery_phone_number']['values'][1]);

				$delivery_address_fields[] = "order_address_phone_number";
				$delivery_address_values[] = dbquote(trim($form->items['delivery_phone_number']['values'][2]));
				$delivery_address["address_phone_number"] = trim($form->items['delivery_phone_number']['values'][2]);
			}
			elseif(array_key_exists('delivery_address_phone_country', $form->items))
			{
				$delivery_address_fields[] = "order_address_phone_country";
				$delivery_address_values[] = trim($form->value("delivery_address_phone_country")) == "" ? "null" : dbquote($form->value("delivery_address_phone_country"));
				$delivery_address["address_phone_country"] = $form->value("delivery_address_phone_country");

				$delivery_address_fields[] = "order_address_phone_area";
				$delivery_address_values[] = trim($form->value("delivery_address_phone_area")) == "" ? "null" : dbquote($form->value("delivery_address_phone_area"));
				$delivery_address["address_phone_area"] = $form->value("delivery_address_phone_area");

				$delivery_address_fields[] = "order_address_phone_number";
				$delivery_address_values[] = trim($form->value("delivery_address_phone_number")) == "" ? "null" : dbquote($form->value("delivery_address_phone_number"));
				$delivery_address["address_phone_number"] = $form->value("delivery_address_phone_number");
			}

			
			$delivery_address_fields[] = "order_address_mobile_phone";
			$delivery_address_values[] = trim($form->value("delivery_address_mobile_phone")) == "" ? "null" : dbquote($form->value("delivery_address_mobile_phone"));
			$delivery_address["address_mobile_phone"] = $form->value("delivery_address_mobile_phone");

			if(array_key_exists('delivery_mobile_phone_number', $form->items))
			{
				if($form->items['delivery_mobile_phone_number']['values'][2])
				{
					$delivery_address_fields[] = "order_address_mobile_phone_country";
					$delivery_address_values[] = dbquote(trim($form->items['delivery_mobile_phone_number']['values'][0]));
					$delivery_address["address_mobile_phone_country"] = trim($form->items['delivery_mobile_phone_number']['values'][0]);

					$delivery_address_fields[] = "order_address_mobile_phone_area";
					$delivery_address_values[] = dbquote(trim($form->items['delivery_mobile_phone_number']['values'][1]));
					$delivery_address["address_mobile_phone_area"] = trim($form->items['delivery_mobile_phone_number']['values'][1]);

					$delivery_address_fields[] = "order_address_mobile_phone_number";
					$delivery_address_values[] = dbquote(trim($form->items['delivery_mobile_phone_number']['values'][2]));
					$delivery_address["address_mobile_phone_number"] = trim($form->items['delivery_mobile_phone_number']['values'][2]);
				}
				else
				{
					$delivery_address["address_mobile_phone_country"] = "";
					$delivery_address["address_mobile_phone_area"] = "";
					$delivery_address["address_mobile_phone_number"] = "";
				}
			}
			elseif(array_key_exists('delivery_address_mobile_phone_country', $form->items))
			{
				$delivery_address_fields[] = "order_address_mobile_phone_country";
				$delivery_address_values[] = trim($form->value("delivery_address_mobile_phone_country")) == "" ? "null" : dbquote($form->value("delivery_address_mobile_phone_country"));
				$delivery_address["address_mobile_phone_country"] = $form->value("delivery_address_mobile_phone_country");

				$delivery_address_fields[] = "order_address_mobile_phone_area";
				$delivery_address_values[] = trim($form->value("delivery_address_mobile_phone_area")) == "" ? "null" : dbquote($form->value("delivery_address_mobile_phone_area"));
				$delivery_address["address_mobile_phone_area"] = $form->value("delivery_address_mobile_phone_area");

				$delivery_address_fields[] = "order_address_mobile_phone_number";
				$delivery_address_values[] = trim($form->value("delivery_address_mobile_phone_number")) == "" ? "null" : dbquote($form->value("delivery_address_mobile_phone_number"));
				$delivery_address["address_mobile_phone_number"] = $form->value("delivery_address_mobile_phone_number");
			}
			
			$delivery_address_fields[] = "order_address_email";
			$delivery_address_values[] = trim($form->value("delivery_address_email")) == "" ? "null" : dbquote($form->value("delivery_address_email"));
			$delivery_address["address_email"] = $form->value("delivery_address_email");
			
			$delivery_address_fields[] = "order_address_parent";
			$delivery_address_values[] = $user["address"];
			$delivery_address["address_parent"] = $user["address"];

			$delivery_address_fields[] = "order_address_contact";
			$delivery_address_values[] = trim($form->value("delivery_address_contact")) == "" ? "null" : dbquote($form->value("delivery_address_contact"));
			$delivery_address["address_contact"] = $form->value("delivery_address_contact");
			
			$delivery_address_fields[] = "date_created";
			$delivery_address_values[] = "current_timestamp";

			$delivery_address_fields[] = "date_modified";
			$delivery_address_values[] = "current_timestamp";

			if (isset($_SESSION["user_login"]))
			{
				$delivery_address_fields[] = "user_created";
				$delivery_address_values[] = dbquote($_SESSION["user_login"]);

				$delivery_address_fields[] = "user_modified";
				$delivery_address_values[] = dbquote($_SESSION["user_login"]);
			}


			
			
			$sql = "insert into order_addresses (" . join(", ", $delivery_address_fields) . ") values (" . join(", ", $delivery_address_values) . ")";
			mysql_query($sql) or dberror($sql);


			$sql = "delete from order_addresses " . 
				   " where order_address_order = " . dbquote($project["project_order"]) .
				   " and order_address_type = 2 " . 
				   " and order_address_order_item > 0";
				$result = mysql_query($sql) or dberror($sql);


			// insert delivery addresses into table order_addresses for all items
			// not having a delivery address and beloging to the same order
			$sql_order_items = "select order_item_id, order_item_order ".
							   "from order_items ".
							   "where order_item_order = " . $project["project_order"] .
							   "    and order_item_type <= ". ITEM_TYPE_SPECIAL;


			$res = mysql_query($sql_order_items) or dberror($sql_order_items);

			while ($row = mysql_fetch_assoc($res)) 
			{
				
				
				
				// insert new record
		
				$delivery_address_fields = array();
				$delivery_address_values = array();

				$delivery_address_fields[] = "order_address_order";
				$delivery_address_values[] = $row["order_item_order"];

				$delivery_address_fields[] = "order_address_order_item";
				$delivery_address_values[] = $row["order_item_id"];

				$delivery_address_fields[] = "order_address_type";
				$delivery_address_values[] = 2;

				$delivery_address_fields[] = "order_address_company";
				$delivery_address_values[] = dbquote($delivery_address["address_company"]);

				$delivery_address_fields[] = "order_address_company2";
				$delivery_address_values[] = dbquote($delivery_address["address_company2"]);

				$delivery_address_fields[] = "order_address_address";
				$delivery_address_values[] = dbquote($delivery_address["address_address"]);

				$delivery_address_fields[] = "order_address_street";
				$delivery_address_values[] = dbquote($delivery_address["address_street"]);

				$delivery_address_fields[] = "order_address_street_number";
				$delivery_address_values[] = dbquote($delivery_address["address_streetnumber"]);

				$delivery_address_fields[] = "order_address_address2";
				$delivery_address_values[] = dbquote($delivery_address["address_address2"]);

				$delivery_address_fields[] = "order_address_zip";
				$delivery_address_values[] = dbquote($delivery_address["address_zip"]);

				$delivery_address_fields[] = "order_address_place_id";
				$delivery_address_values[] = dbquote(trim($delivery_address["address_place_id"]));

				$delivery_address_fields[] = "order_address_place";
				$delivery_address_values[] = dbquote(trim($delivery_address["address_place"]));

				$delivery_address_fields[] = "order_address_country";
				$delivery_address_values[] = dbquote($delivery_address["address_country"]);

				$delivery_address_fields[] = "order_address_phone";
				$delivery_address_values[] = dbquote($delivery_address["address_phone"]);

				$delivery_address_fields[] = "order_address_phone_country";
				$delivery_address_values[] = dbquote($delivery_address["address_phone_country"]);

				$delivery_address_fields[] = "order_address_phone_area";
				$delivery_address_values[] = dbquote($delivery_address["address_phone_area"]);

				$delivery_address_fields[] = "order_address_phone_number";
				$delivery_address_values[] = dbquote($delivery_address["address_phone_number"]);

				$delivery_address_fields[] = "order_address_mobile_phone";
				$delivery_address_values[] = dbquote($delivery_address["address_mobile_phone"]);

				$delivery_address_fields[] = "order_address_mobile_phone_country";
				$delivery_address_values[] = dbquote($delivery_address["address_mobile_phone_country"]);

				$delivery_address_fields[] = "order_address_mobile_phone_area";
				$delivery_address_values[] = dbquote($delivery_address["address_mobile_phone_area"]);

				$delivery_address_fields[] = "order_address_mobile_phone_number";
				$delivery_address_values[] = dbquote($delivery_address["address_mobile_phone_number"]);

				$delivery_address_fields[] = "order_address_email";
				$delivery_address_values[] = dbquote($delivery_address["address_email"]);

				$delivery_address_fields[] = "order_address_parent";
				$delivery_address_values[] = dbquote($delivery_address["address_parent"]);;

				$delivery_address_fields[] = "order_address_contact";
				$delivery_address_values[] = dbquote($delivery_address["address_contact"]);

				$delivery_address_fields[] = "date_created";
				$delivery_address_values[] = "current_timestamp";

				$delivery_address_fields[] = "user_created";
				$delivery_address_values[] = dbquote($_SESSION["user_login"]);


				
				$sql = "insert into order_addresses (" . join(", ", $delivery_address_fields) . ") values (" . join(", ", $delivery_address_values) . ")";
				mysql_query($sql) or dberror($sql);
				
			}
			

			//update order
			$order_fields = array();

			//$value = dbquote(trim(from_system_date($form->value("preferred_delivery_date"))));
			//$order_fields[] = "order_preferred_delivery_date = " . $value;

			$value = dbquote(trim($form->value("preferred_transportation_arranged")));
			$order_fields[] = "order_preferred_transportation_arranged = " . $value;

			$value = dbquote(trim($form->value("preferred_transportation_mode")));
			$order_fields[] = "order_preferred_transportation_mode = " . $value;

			$value = dbquote(trim($form->value("pedestrian_mall_approval")));
			$order_fields[] = "order_pedestrian_mall_approval = " . $value;

			$value = dbquote(trim($form->value("full_delivery")));
			$order_fields[] = "order_full_delivery = " . $value;

			$value = dbquote(trim($form->value("delivery_comments")));
			$order_fields[] = "order_delivery_comments = " . $value;

			if($form->value('preferred_transportation_arranged') == 7) {
				$value = 1;
			}
			else {
				$value = 0;
			}
			
			$order_fields[] = "order_insurance = " . $value;

			$value = dbquote(trim($form->value("invoice_address_id")));
			$order_fields[] = "order_direct_invoice_address_id = " . $value;

			$value = "current_timestamp";
			$order_fields[] = "date_modified = " . $value;

			$value = dbquote(user_login());
			$order_fields[] = "user_modified = " . $value;

			$sql = "update orders set " . join(", ", $order_fields) . " where order_id = " . $project["project_order"];
			mysql_query($sql) or dberror($sql);



			//update project
			$project_fields = array();
			
			$value = dbquote(trim($form->value("comments")));
			$project_fields[] = "project_comments = " . $value;
			$sql = "update projects set " . join(", ", $project_fields) . " where project_id = " . $project["project_id"];
			mysql_query($sql) or dberror($sql);




			//send mail if one of the preferred delivery dates is beyond the agreed opening date
			$delivery_date_beyond_agreed_opening = false;
			foreach($supplier_fields as $key=>$field_name)
			{
				
				if($project['project_real_opening_date'] < from_system_date($form->value($field_name)))
				{
					$delivery_date_beyond_agreed_opening = true;
				}
			}


			if($delivery_date_beyond_agreed_opening == true)
			{
				$mail = new ActionMail('preferred.arrival.date.beyond.agreed.opening.date');
				$mail->setParam('projectID', param("pid"));
				$mail->setParam('link', URLBuilder::projectViewTrafficData(param("pid")));
				$mail->setParam('allowResend', true);
				if($senmail_activated == true)
				{
					$mail->send();
				}
			
			}
			
			$link = "project_perform_action.php?pid=" . param("pid") . "&nop=1&id=" . param("id");
			redirect($link);
		}
	}
}
elseif ($form->button("proceed"))
{
	$link = "project_perform_action.php?pid=" . param("pid") . "&nop=1&id=" . param("id");
	redirect($link);
}
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

$page->register_action('home', 'Home', "welcome.php");


require "include/project_page_actions.php";


$page->header();
$page->title("Ship to");



$form->render();


?>

<div id="city" style="display:none;">
    Please indicate the new city's name in English!
</div>


<?php
foreach($supplier_fields as $key=>$field_name)
{
?>
	<div id="preferred_arrival_date<?php echo $key;?>" style="display:none;">
		<span class="red">Your preferred arrival date is after the current</br>agreed opening date for this project.</br>Please contact your Project Leader</br>to help postponing the agreed opening date accordingly</span>
	</div>
<?php
}
?>

<script languege="javascript">
	
	
	function toMySQLDate(d) {
		
		var date = "";
		var date_parts = d.split(".");
		
		if(date_parts.length == 3)
		{
			if(date_parts[0] < 10){date_parts[0] = '0' + date_parts[0];}
			if(date_parts[1] < 10){date_parts[1] = '0' + date_parts[1];}
			if(date_parts[2] < 100){date_parts[2] = '20' + date_parts[2];}
			
			
			date = date_parts[2] + "-" + date_parts[1] + "-" + date_parts[0];
		}

		return date;
	}
	
	
	$(document).ready(function(){

		var agreed_opening_date = "<?php echo $project['project_real_opening_date'];?>";
		<?php
			foreach($supplier_fields as $key=>$field_name)
			{
			?>	
				$("#preferred_delivery_date_<?php echo $key?>" ).change(function() {
					if(agreed_opening_date < toMySQLDate($("#preferred_delivery_date_<?php echo $key?>" ).val()))
					{
						$('#h_preferred_arrival_date<?php echo $key?>').trigger('mouseover');						
					}
					else
					{
						$('#h_preferred_arrival_date<?php echo $key?>').trigger('mouseout');
					}
				});
			<?php
			}
		?>


		$('#preferred_transportation_arranged').change(function() {
			
			if($(this).val() == 7) {
				$('#insurance_info').html('Insurance is covered by Tissot.');
			}
			else {
				$('#insurance_info').html('Insurance has to be covered by the client.');
			}
			
		});

	});
</script>

<?php





require_once "include/project_footer_logistic_state.php";
$page->footer();


?>