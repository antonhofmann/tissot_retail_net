<?php
/********************************************************************

    project_edit_project_budget.php

    Edit Project's Budget Sheet

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-06-15
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

require_once "../shared/func_eventmails.php";

check_access("can_edit_budget_in_projects");

register_param("pid");
set_referer("project_edit_budget_position.php");
set_referer("project_add_budget_position.php");


/********************************************************************
    prepare all data needed
*********************************************************************/
$user_roles = get_user_roles(user_id());

// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

// get system currency
$system_currency = get_system_currency_fields();

// get orders's currency
$order_currency = get_order_currency($project["project_order"]);


$currency_list = array();
$sql_currency = "select DISTINCT currency_id, currency_symbol " . 
                " from currencies " .
				" left join countries on country_currency = currency_id " . 
				" where currency_id = 1 " . 
				" or currency_id = " . $project["order_client_currency"] . 
				" or country_id in (" . $client_address["country"] . ", " . $project["order_shop_address_country"] . ") " . 
				" order by currency_symbol";

$res = mysql_query($sql_currency) or dberror($sql_currency);
while ($row = mysql_fetch_assoc($res))
{
	$currency_list[$row["currency_id"]] = get_currency($row["currency_id"]);
}

// build line numbers for budget positions

//check if CER was approved
$change_of_budget_possible = true;
$milestone1 = get_project_milestone($project["project_id"], 12);
$milestone2 = get_project_milestone($project["project_id"], 21);
	
if(count($milestone1) > 0 and ($milestone1['project_milestone_date'] != NULL and $milestone1['project_milestone_date'] != '0000-00-00')) // LN was not approved yet
{
	$change_of_budget_possible = false;
}
if(count($milestone2) > 0 and ($milestone2['project_milestone_date'] != NULL and $milestone2['project_milestone_date'] != '0000-00-00')) // LN was not approved yet
{
	$change_of_budget_possible = false;
}

if($project["project_cost_type"] != 1)
{
	$change_of_budget_possible = true; // allow for all except corporate projects
}

// build group_totals of standard item groups


if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
{
    
    $sql_order_items = "select order_item_id, order_item_text, order_item_quantity, ".
                 "    order_item_po_number, item_id, order_item_system_price, ".
                 "    TRUNCATE(order_item_quantity * order_item_system_price, 2) as total_price, ".
                 "    order_item_client_price, ".
                 "    if(item_code <> '', item_code, item_type_name) as item_shortcut, ".
                 "    address_shortcut, item_type_id, ".
                 "    item_type_priority, order_item_type, ".
                 "    concat(project_cost_groupname_name, ' - ', item_category_name) as cat_name " .
                 "from order_items ".
                 "left join items on order_item_item = item_id ".
                 "left join item_categories on item_category_id = item_category " .
                 "left join addresses on order_item_supplier_address = address_id ".
                 "left join item_types on order_item_type = item_type_id " . 
		         "left join project_cost_groupnames on project_cost_groupname_id = order_item_cost_group "  . 
			     "left join project_cost_groupnames on project_cost_groupname_id = order_item_cost_group ";


    $list1_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) and order_item_order = " . $project["project_order"] .  " and (order_item_type = " . ITEM_TYPE_STANDARD . " or order_item_type = " . ITEM_TYPE_SERVICES . ")";

    $list2_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) and order_item_order = " . $project["project_order"] .  " and order_item_type = " . ITEM_TYPE_SPECIAL;

    $list3_filter = "order_item_order = " . $project["project_order"] .  " and order_item_type = " . ITEM_TYPE_COST_ESTIMATION;

    $list6_filter = "order_item_order = " . $project["project_order"] .  " and order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST;
    
    $group_totals = get_group_total_of_standard_items($project["project_order"], "order_item_system_price");
    $standard_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_STANDARD);
    $special_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_SPECIAL);
    $cost_estimation_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_COST_ESTIMATION);

    $local_construction_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_LOCALCONSTRUCTIONCOST);
}
else
{
    $sql_order_items = "select order_item_id, order_item_text, order_item_quantity_freezed, ".
                 "    order_item_po_number, item_id, order_item_system_price_freezed, ".
                 "    TRUNCATE(order_item_quantity_freezed * order_item_system_price_freezed, 2) as total_price, ".
                 "    order_item_client_price_freezed, ".
                 "    if(item_code <> '', item_code, item_type_name) as item_shortcut, ".
                 "    address_shortcut, item_type_id, ".
                 "    item_type_priority, order_item_type, " .
                 "    concat(project_cost_groupname_name, ' - ', item_category_name) as cat_name " .
                 "from order_items ".
                 "left join items on order_item_item = item_id ".
                 "left join item_categories on item_category_id = item_category " .
                 "left join addresses on order_item_supplier_address = address_id ".
                 "left join item_types on order_item_type = item_type_id " . 
			     "left join project_cost_groupnames on project_cost_groupname_id = order_item_cost_group ";


    $list1_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) and order_item_order = " . $project["project_order"] .  " and (order_item_type = " . ITEM_TYPE_STANDARD . " or order_item_type = " . ITEM_TYPE_SERVICES . ") and order_item_in_freezed_budget = 1";

    $list2_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) and order_item_order = " . $project["project_order"] .  " and order_item_type = " . ITEM_TYPE_SPECIAL . " and order_item_in_freezed_budget = 1";

    $list3_filter = "order_item_order = " . $project["project_order"] .  " and order_item_type = " . ITEM_TYPE_COST_ESTIMATION . " and order_item_in_freezed_budget = 1";

    $list6_filter = "order_item_order = " . $project["project_order"] .  " and order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST . " and order_item_in_freezed_budget = 1";
    
}



//calculate cost group totals
$grouptotals_investment = array();
$grouptotals_investment[2] = 0;
$grouptotals_investment[6] = 0;
$grouptotals_investment[7] = 0;
$grouptotals_investment[8] = 0;
$grouptotals_investment[9] = 0;
$grouptotals_investment[10] = 0;
$grouptotals_investment[11] = 0;

$grouptotals_hq_investment = array();
$grouptotals_hq_investment[2] = 0;
$grouptotals_hq_investment[6] = 0;

$show_budget_in_loc = false;

if($show_budget_in_loc == true)
{
	$sql = "select order_item_cost_group, order_item_type, ".
		   "order_item_client_price, order_item_quantity,  ".
		   "order_item_client_price_freezed, order_item_quantity_freezed, ".
		   "order_item_system_price_hq_freezed, order_item_client_price_hq_freezed, order_item_quantity_hq_freezed " .
		   "from order_items ".
		   "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
		   "   and order_item_order=" . $project["project_order"] . 
		   "   and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION .
		   "   or order_item_type = " . ITEM_TYPE_SERVICES .
		   "   or order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST .
		   " ) " .
		   " and order_item_cost_group > 0 " . 
		   " order by order_item_cost_group";
}
else
{
	$sql = "select order_item_cost_group, order_item_type, ".
		   "order_item_system_price, order_item_quantity,  ".
		   "order_item_system_price_freezed, order_item_quantity_freezed, ".
		   "order_item_system_price_hq_freezed, order_item_system_price_hq_freezed, order_item_quantity_hq_freezed " .
		   "from order_items ".
		   "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
		   "   and order_item_order=" . $project["project_order"] . 
		   "   and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION .
		   "   or order_item_type = " . ITEM_TYPE_SERVICES .
		   "   or order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST .
		   " ) " .
		   " and order_item_cost_group > 0 " . 
		   " order by order_item_cost_group";
}

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	
	if($show_budget_in_loc == true)
	{
		
		if($row["order_item_quantity"] > 0 or $row["order_item_type"] == 1  or $row["order_item_type"] == 2)
		{
			$grouptotals_hq_investment[$row["order_item_cost_group"]] = $grouptotals_hq_investment[$row["order_item_cost_group"]] + $row["order_item_client_price"] * $row["order_item_quantity"];

		}
		else
		{
			$grouptotals_hq_investment[$row["order_item_cost_group"]] = $grouptotals_hq_investment[$row["order_item_cost_group"]] + $row["order_item_client_price"];
		}
		

		
		if($row["order_item_quantity"] > 0 or $row["order_item_type"] == 1  or $row["order_item_type"] == 2)
		{
			$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_client_price"] * $row["order_item_quantity"];

		}
		else
		{
			$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_client_price"];
		}
	}
	else
	{
		if($row["order_item_quantity"] > 0 or $row["order_item_type"] == 1  or $row["order_item_type"] == 2)
		{
			$grouptotals_hq_investment[$row["order_item_cost_group"]] = $grouptotals_hq_investment[$row["order_item_cost_group"]] + $row["order_item_system_price"] * $row["order_item_quantity"];

		}
		else
		{
			$grouptotals_hq_investment[$row["order_item_cost_group"]] = $grouptotals_hq_investment[$row["order_item_cost_group"]] + $row["order_item_system_price"];
		}
		
		if($row["order_item_quantity"] > 0 or $row["order_item_type"] == 1  or $row["order_item_type"] == 2)
		{
			$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_system_price"] * $row["order_item_quantity"];

		}
		else
		{
			$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_system_price"];
		}
	}
}

//budget for hq supplierd items
$hq_budget_total = $grouptotals_hq_investment[2] + $grouptotals_hq_investment[6];

//budget
$project_cost_hq_fixturing = $grouptotals_hq_investment[6] + $grouptotals_hq_investment[2];
$project_cost_construction = $grouptotals_investment[7];
$project_cost_fixturing = $grouptotals_investment[10] + $grouptotals_investment[6] + $grouptotals_investment[2];
$project_cost_architectural = $grouptotals_investment[9];
$project_cost_equipment = $grouptotals_investment[11];
$project_cost_other = + $grouptotals_investment[8];
$budget_total = $project_cost_construction + $project_cost_fixturing + $project_cost_architectural + $project_cost_equipment + $project_cost_other;


//total cost
$standard_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_STANDARD);
$special_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_SPECIAL);
$special_item_total_hq = get_order_item_type_total($project["project_order"],  ITEM_TYPE_SPECIAL, 2);
$freight_charges = get_order_item_type_total($project["project_order"],  ITEM_TYPE_COST_ESTIMATION, 6);
$cost_estimation_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_COST_ESTIMATION);
$local_construction_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_LOCALCONSTRUCTIONCOST);

if($show_budget_in_loc == true)
{
	$total_cost = $standard_item_total["in_order_currency"] + $special_item_total["in_order_currency"] + $freight_charges["in_order_currency"]  + $cost_estimation_item_total["in_order_currency"] + $local_construction_item_total["in_order_currency"];

	$hq_total_cost = $standard_item_total["in_order_currency"] + $special_item_total_hq["in_order_currency"] + $freight_charges["in_order_currency"];
}
else
{
	$total_cost = $standard_item_total["in_system_currency"] + $special_item_total["in_system_currency"] + $freight_charges["in_system_currency"] + $cost_estimation_item_total["in_system_currency"] + $local_construction_item_total["in_system_currency"];

	$hq_total_cost = $standard_item_total["in_system_currency"] + $special_item_total_hq["in_system_currency"] + $freight_charges["in_system_currency"];
}


$hq_budget_approval_data = get_hq_budget_approval_data($project["project_order"]);
$budget_approval_data = get_budget_approval_data($project["project_order"]);


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");

require_once "include/project_head_small.php";



$form->add_section("Client Currency Information");

if(has_access("can_change_currency_data_in_projects"))
{
	$form->add_list("order_client_currency", "Currency", $sql_currency, NOTNULL, $project["order_client_currency"]);
	$form->add_edit("order_client_exchange_rate", "Exchange Rate", NOTNULL, $project["order_client_exchange_rate"] , TYPE_DECIMAL, 10, 6);
}
else
{
	$form->add_label("client_currency", "Currency", 0, $order_currency["symbol"]);
	$form->add_label("exchange_rate", "Exchange Rate", 0, $order_currency["exchange_rate"]);
}

$form->add_hidden("old_order_client_currency", $project["order_client_currency"]);
$form->add_hidden("old_order_client_exchange_rate", $project["order_client_exchange_rate"]);


if($project["order_budget_freezed_date"] and count($budget_approval_data) > 0)
{
	$form->add_section("Full Budget Approved by " . $budget_approval_data["username"] . " as per " . to_system_date($budget_approval_data["created"]));
}
elseif($project["order_hq_budget_freezed_date"] and count($hq_budget_approval_data) > 0)
{
	$form->add_section("Budget of HQ Supplied Items Approved by " . $hq_budget_approval_data["username"] . " as per " . to_system_date($hq_budget_approval_data["created"]));
}
else
{
	$form->add_section("Cost Overview as per " . date("d.m.Y"));

}

if($show_budget_in_loc == true)
{
	$form->add_label("l1_lc", "Local Construction in " . $order_currency["symbol"], 0, number_format($project_cost_construction,2, ".", "'"));
	$form->add_label("l2_lc", "Store fixturing / Furniture in " . $order_currency["symbol"], 0, number_format($project_cost_fixturing,2, ".", "'"));
	$form->add_label("l3_lc", "Architectural Services in " . $order_currency["symbol"], 0, number_format($project_cost_architectural,2, ".", "'"));
	$form->add_label("l4_lc", "Equipment in " . $order_currency["symbol"], 0, number_format($project_cost_equipment,2, ".", "'"));
	$form->add_label("l5_lc", "Other in " . $order_currency["symbol"], 0, number_format($project_cost_other,2, ".", "'"));

	if($project["order_hq_budget_freezed_date"] and count($hq_budget_approval_data) > 0)
	{
		
		
		$tmp1 = "Approved Budget of HQ Supplied Items in " . $order_currency["symbol"];
		$tmp2 = number_format($hq_budget_total,2, ".", "'") . " (" . to_system_date($project["order_hq_budget_freezed_date"]) . ")";
		$form->add_label("l6_lc", $tmp1, 0, $tmp2);
		
		
		if(round($hq_total_cost,2) != round($hq_budget_total,2))
		{
			$tmp = "<span class='red'>" . number_format($hq_total_cost,2, ".", "'") . "</span> as per " . date("d.m.Y H:i:s");
			
			$form->add_label("l7_lc", "Total cost of HQ supplied items from the list of materials in  " . $order_currency["symbol"], RENDER_HTML, $tmp);
		}
		else
		{
			$tmp = number_format($hq_total_cost,2, ".", "'") . " as per " . date("d.m.Y");
			$form->add_label("l8_lc", "Total cost of HQ supplied items from the list of materials in  " . $order_currency["symbol"], RENDER_HTML, $tmp);
		}
	}
	else
	{
		$form->add_label("l9_lc", "Budget of HQ Supplied Items in " . $order_currency["symbol"], 0, number_format($hq_budget_total,2, ".", "'"));

	}
	if($project["order_budget_freezed_date"] and count($budget_approval_data) > 0)
	{
		
		$tmp1 = "Approved Full Budget in " . $order_currency["symbol"];
		$tmp2 = number_format($budget_total,2, ".", "'") . " (" . to_system_date($project["order_budget_freezed_date"]) . ")";
		$form->add_label("l10_lc", $tmp1, 0, $tmp2);

		
		if(round($total_cost,2) != round($budget_total,2))
		{
			
			$tmp = "<span class='red'>" . number_format($total_cost,2, ".", "'") . "</span> as per " . date("d.m.Y H:i:s");
			
			$form->add_label("l11_lc", "Total Cost from the list of materials in  " . $order_currency["symbol"], RENDER_HTML, $tmp);
		}
		else
		{
			$tmp = number_format($total_cost,2, ".", "'") . " as per " . date("d.m.Y");
			$form->add_label("l12_lc", "Total Cost from the list of materials in  " . $order_currency["symbol"], RENDER_HTML, $tmp);
		}
	}
	else
	{
		 $form->add_label("l13_lc", "Full Budget in " . $order_currency["symbol"], 0, number_format($budget_total,2, ".", "'"));
	}

	if(count($hq_budget_approval_data) == 0 and count($budget_approval_data) == 0)
	{
		$form->add_label("l14_lc", "Full Budget Total in " . $order_currency["symbol"], 0, number_format($budget_total,2, ".", "'"));
	}
}
else
{
	$form->add_label("l1", "Local Construction in " . $system_currency["symbol"], 0, number_format($project_cost_construction,2, ".", "'"));
	$form->add_label("l2", "Store fixturing / Furniture in " . $system_currency["symbol"], 0, number_format($project_cost_fixturing,2, ".", "'"));
	$form->add_label("l3", "Architectural Services in " . $system_currency["symbol"], 0, number_format($project_cost_architectural,2, ".", "'"));
	$form->add_label("l4", "Equipment in " . $system_currency["symbol"], 0, number_format($project_cost_equipment,2, ".", "'"));
	$form->add_label("l5", "Other in " . $system_currency["symbol"], 0, number_format($project_cost_other,2, ".", "'"));

	if($project["order_hq_budget_freezed_date"] and count($hq_budget_approval_data) > 0)
	{
		
		
		$tmp1 = "Approved Budget of HQ Supplied Items in " . $system_currency["symbol"];
		$tmp2 = number_format($hq_budget_total,2, ".", "'") . " (" . to_system_date($project["order_hq_budget_freezed_date"]) . ")";
		$form->add_label("l6", $tmp1, 0, $tmp2);
		
		
		if(round($hq_total_cost,2) != round($hq_budget_total,2))
		{
			$tmp = "<span class='red'>" . number_format($hq_total_cost,2, ".", "'") . "</span> as per " . date("d.m.Y H:i:s");
			
			$form->add_label("l7", "Total cost of HQ supplied items from the list of materials in  " . $system_currency["symbol"], RENDER_HTML, $tmp);
		}
		else
		{
			$tmp = number_format($hq_total_cost,2, ".", "'") . " as per " . date("d.m.Y");
			$form->add_label("l8", "Total cost of HQ supplied items from the list of materials in  " . $system_currency["symbol"], RENDER_HTML, $tmp);
		}
	}
	else
	{
		$form->add_label("l9", "Budget of HQ Supplied Items in " . $system_currency["symbol"], 0, number_format($hq_budget_total,2, ".", "'"));

	}
	if($project["order_budget_freezed_date"] and count($budget_approval_data) > 0)
	{
		
		$tmp1 = "Approved Full Budget in " . $system_currency["symbol"];
		$tmp2 = number_format($budget_total,2, ".", "'") . " (" . to_system_date($project["order_budget_freezed_date"]) . ")";
		$form->add_label("l10", $tmp1, 0, $tmp2);

		
		if(round($total_cost,2) != round($budget_total,2))
		{
			
			$tmp = "<span class='red'>" . number_format($total_cost,2, ".", "'") . "</span> as per " . date("d.m.Y H:i:s");
			
			$form->add_label("l11", "Total Cost from the list of materials in  " . $system_currency["symbol"], RENDER_HTML, $tmp);
		}
		else
		{
			$tmp = number_format($total_cost,2, ".", "'") . " as per " . date("d.m.Y");
			$form->add_label("l12", "Total Cost from the list of materials in  " . $system_currency["symbol"], RENDER_HTML, $tmp);
		}
	}
	else
	{
		 $form->add_label("l13", "Full Budget in " . $system_currency["symbol"], 0, number_format($budget_total,2, ".", "'"));
	}

	if(count($hq_budget_approval_data) == 0 and count($budget_approval_data) == 0)
	{
		$form->add_label("l14", "Full Budget Total in " . $system_currency["symbol"], 0, number_format($budget_total,2, ".", "'"));
	}
}

$form->add_section("Budget State");
if($project["order_hq_budget_unfreezed_by"] > 0) {
	$form->add_label("hq_unfreezed_date", "Date last unfreeze budget of HQ supplied items", 0, to_system_date($project["order_hq_budget_unfreezed_date"]));
	$form->add_lookup("hq_unfreezed_by", "HQ supplied items unfreezed by", "users", "concat(user_name, ' ', user_firstname)", 0, $project["order_hq_budget_unfreezed_by"]);
}
else
{
	$form->add_label("hq_unfreezed_date", "Date last unfreeze budget of HQ supplied items", 0);
	$form->add_label("hq_unfreezed_by", "Unfreezed by", 0);
}

if($project["order_budget_unfreezed_by"] > 0) {
	$form->add_label("unfreezed_date", "Date last unfreeze of full budget", 0, to_system_date($project["order_budget_unfreezed_date"]));
	$form->add_lookup("unfreezed_by", "Unfreezed by", "users", "concat(user_name, ' ', user_firstname)", 0, $project["order_budget_unfreezed_by"]);
}
else
{
	$form->add_label("unfreezed_date", "Date last unfreeze of full budget", 0);
	$form->add_label("unfreezed_by", "Unfreezed by", 0);
}

$form->add_checkbox("order_budget_is_locked", "Budget is locked", $project["order_budget_is_locked"], 0);
$form->add_hidden("old_order_budget_is_locked", $project["order_budget_is_locked"]);


if(has_access("can_change_currency_data_in_projects"))
{
	$form->add_button("save_exr", "Save Exchang Rate");
}

if($change_of_budget_possible == true or has_access("can_edit_catalog"))
{
	$form->add_button("save", "Update Budget State");
	$form->add_button("recalculate", "Recalculate Budget");

	if(has_access("can_unfreeze_budget"))
	{
		$form->add_button("unfreeze_hq", "Unfreeze Budget of HQ supplied items");
		$form->add_button("unfreeze", "Unfreeze Full Budget");
		//$form->add_button("correct_prices", "Correct Prices");
		
	}
}
else
{
	if($project["project_cost_type"] == 1) // corporate
	{
		$form->error("CER is already approved. Please contact Retail Controlling in case of action needed.");
	}
	else
	{
		$form->error("AF is already approved. Please contact Retail Controlling in case of action needed.");
	}
}



if(has_access("can_freeze_budget"))
{
	$form->add_button("freeze_hq", "(Freeze Budget of HQ Supplied Items)");
	$form->add_button("freeze", "(Freeze Full Budget)");
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("freeze_hq"))
{
    if($project["project_cost_type"] != 1) // corporate
	{
		update_budget_state($project["project_order"], 1);
		freeze_budget($project["project_order"]);
	}
	else
	{
		freeze_budget_hq_supplied_items($project["project_order"]);
	}
    $link = "project_edit_project_budget.php?pid=" . param("pid");
    redirect($link);
}
elseif ($form->button("freeze"))
{
    update_budget_state($project["project_order"], 1);
	freeze_budget($project["project_order"]);
    $link = "project_edit_project_budget.php?pid=" . param("pid");
    redirect($link);
}
elseif ($form->button("save"))
{
    update_budget_state($project["project_order"], $form->value("order_budget_is_locked"));


	if($form->value("old_order_budget_is_locked") != $form->value("order_budget_is_locked"))
	{
		if($form->value("order_budget_is_locked") == 1)
		{
			$actionmail = new ActionMail(35);
		}
		else
		{
			$actionmail = new ActionMail(34);
		}
		$actionmail->setParam('id', param("pid"));
		
		if($senmail_activated == true)
		{
			$actionmail->send();
		}

		//echo "<pre>";
		//print_r($actionmail);
		//echo "</pre>";
		//die;
		
		$_RESPONSE = $actionmail->isSuccess();
		$recipient = $actionmail->getRecipient($project['project_retail_coordinator']);

		if ($_RESPONSE && $recipient->isSendMail()) {
			$bodytext = $recipient->getContent();
			append_mail($project["project_order"], $project['project_retail_coordinator'], user_id(), $bodytext, "910", 1);
			append_mail($project["project_order"], $project['order_retail_operator'], user_id(), $bodytext, "910", 1);
		}
	}




    $link = "project_edit_project_budget.php?pid=" . param("pid");
    redirect($link);
}
elseif($form->button("save_exr"))
{
	if($form->validate())
	{
		if(($form->value('old_order_client_currency') != $form->value('order_client_currency'))
			or ($form->value('old_order_client_exchange_rate') != $form->value('order_client_exchange_rate'))
			)
		{
			$old_currency = $currency_list[$form->value('old_order_client_currency')]["symbol"] . " " . $form->value('old_order_client_exchange_rate');

			$new_currency = $currency_list[$form->value('order_client_currency')]["symbol"] . " " . $form->value('order_client_exchange_rate');


			//project tracking
			$field = "currency";
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote("order_client_currency") . ", " . 
				   dbquote($old_currency ) . ", " . 
				   dbquote($new_currency) . ", " . 
				   dbquote('manually changed in edit budget') . ", " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
			$result = mysql_query($sql) or dberror($sql);
		}

		$result = update_order_with_exchange_rate_info(param("oid"), $form->value("order_client_currency"), $form->value("order_client_exchange_rate"), 1);
	}
}
elseif($form->button("recalculate"))
{
    update_exchange_rates($project["project_order"], 1);
    $link = "project_edit_project_budget.php?pid=" . param("pid");
    redirect($link);

}
elseif ($form->button("correct_prices"))
{
	$sql_u = "update order_items set " . 
		     "order_item_client_price = order_item_system_price, " . 
		     "order_item_client_price_freezed = order_item_system_price_freezed " . 
		     "where order_item_order = " . $project["project_order"];

	$res = mysql_query($sql_u);

	$sql_u = "update orders set " . 
		     "order_client_currency = 1, " . 
		     "order_client_exchange_rate = 1 " . 
		     "where order_item_order = " . $project["project_order"];

	$res = mysql_query($sql_u);


	$form->message("Prices were corrected.");
}
elseif ($form->button("unfreeze_hq"))
{
    unfreeze_project_hq_budget($project["project_order"]);
	unfreeze_project_budget($project["project_order"]);

	//send_project_event_mail(user_id(), 'unfreeze_project_budget', $project);

	//send mail to logistics coordinator and retail administrator
	$today = date("Y-m-d");

	//get sender
	$sql = "select user_id, user_email, user_name, user_firstname , " .
		   "concat(user_name, ' ', user_firstname) as sender_name " .
		   "from users where user_id = " . dbquote(user_id());


	$res = mysql_query($sql) or dberror($sql);
    if($row = mysql_fetch_assoc($res))
	{
		$sender_name = $row['sender_name'];
		$sender_firstname = $row['user_firstname'];
		$sender_lastname = $row['user_name'];
		$sender_email = $row['user_email'];
		$sender_id = user_id();

		$sql = " select * from mail_alert_types where mail_alert_type_id = 39";

		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$subject = "Budget for HQ supplied items was unfreezed";
			$text = $row["mail_alert_mail_text"];
			
			$cc_mail1 = $row["mail_alert_type_cc1"];
			$cc_mail2 = $row["mail_alert_type_cc2"];
			$cc_mail3 = $row["mail_alert_type_cc3"];
			$cc_mail4 = $row["mail_alert_type_cc4"];

			$subject = MAIL_SUBJECT_PREFIX . ": Project " . $project["order_number"] . ": " . $subject . " for " .  $project["order_shop_address_company"];


			$text = str_replace("\r\n", "\n", $text) . "\n\n";
			$link ="project_task_center.php?pid=" . $project["project_id"];
			$link = APPLICATION_URL ."/user/" . $link; 
			
			$text = str_replace("{link}", $link, $text);
			$text = str_replace("{sender_firstname}", $sender_firstname, $text);
			$text = str_replace("{sender_name}", $sender_lastname, $text);

			
			$mail = new PHPMailer();
			$mail->Subject = $subject;
			$mail->SetFrom($sender_email, $sender_name);
			$mail->AddReplyTo($sender_email, $sender_name);

			$rcpts = "";
			$recipient_ids = array();
			$cc_ids = array();
			
			$sql = "select user_id, user_email, user_firstname, " . 
				   "concat(user_name, ' ', user_firstname) as user_name " .
		           "from users where user_id = " . dbquote($project["order_retail_operator"]);

			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$mail->AddAddress($row["user_email"], $row["user_name"]);
				$rcpts .= $row["user_email"] . "\n";

				$text = str_replace("{recipient_fistname}",  $row["user_firstname"], $text);
				$recipient_ids[] = $row["user_id"];

				if($cc_mail1) {
					$mail->AddCC($cc_mail1);
					$rcpts .= $cc_mail1 . "\n";
				}
				if($cc_mail2) {
					$mail->AddCC($cc_mail2);
					$rcpts .= $cc_mail2 . "\n";
				}
				if($cc_mail3) {
					$mail->AddCC($cc_mail3);
					$rcpts .= $cc_mail3 . "\n";
				}
				if($cc_mail3) {
					$mail->AddCC($cc_mail3);
					$rcpts .= $cc_mail3 . "\n";
				}
			}


			$sql = "select user_id, user_email, user_firstname, " . 
				   "concat(user_name, ' ', user_firstname) as user_name " .
		           "from users where user_id = " . dbquote($project["project_retail_coordinator"]);

			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				
				$mail->AddCC($row["user_email"]);
				$rcpts .= $row["user_email"] . "\n";
				$cc_ids[] = $row["user_id"];
			}

			$mail->Body = $text;

			if($rcpts) {			
				if($senmail_activated == true)
				{
					if($senmail_activated == true)
					{
						$mail->send();
					}
				}
				
				foreach($recipient_ids as $key=>$recipient_id) {
					append_mail($project['project_order'], $recipient_id, $sender_id, $text, '910', 1,0);
				}

				foreach($cc_ids as $key=>$recipient_id) {
					append_mail($project['project_order'], $recipient_id, $sender_id, $text, '910', 1,0, 1);
				}
			}

		}	
	}

    $link = "project_edit_project_budget.php?pid=" . param("pid");
    redirect($link);
}
elseif ($form->button("unfreeze"))
{
    unfreeze_project_budget($project["project_order"]);
	//send_project_event_mail(user_id(), 'unfreeze_project_budget', $project);

	//send mail to logistics coordinator and retail administrator
	$today = date("Y-m-d");

	//get sender
	$sql = "select user_id, user_email, user_name, user_firstname , " .
		   "concat(user_name, ' ', user_firstname) as sender_name " .
		   "from users where user_id = " . dbquote(user_id());


	$res = mysql_query($sql) or dberror($sql);
    if($row = mysql_fetch_assoc($res))
	{
		$sender_name = $row['sender_name'];
		$sender_firstname = $row['user_firstname'];
		$sender_lastname = $row['user_name'];
		$sender_email = $row['user_email'];
		$sender_id = user_id();

		$sql = " select * from mail_alert_types where mail_alert_type_id = 39";

		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$subject = "Full Budget was unfreezed";
			$text = $row["mail_alert_mail_text"];
			
			$cc_mail1 = $row["mail_alert_type_cc1"];
			$cc_mail2 = $row["mail_alert_type_cc2"];
			$cc_mail3 = $row["mail_alert_type_cc3"];
			$cc_mail4 = $row["mail_alert_type_cc4"];

			$subject = MAIL_SUBJECT_PREFIX . ": Project " . $project["order_number"] . ": " . $subject . " for " .  $project["order_shop_address_company"];


			$text = str_replace("\r\n", "\n", $text) . "\n\n";
			$link ="project_task_center.php?pid=" . $project["project_id"];
			$link = APPLICATION_URL ."/user/" . $link; 
			
			$text = str_replace("{link}", $link, $text);
			$text = str_replace("{sender_firstname}", $sender_firstname, $text);
			$text = str_replace("{sender_name}", $sender_lastname, $text);

			
			$mail = new PHPMailer();
			$mail->Subject = $subject;
			$mail->SetFrom($sender_email, $sender_name);
			$mail->AddReplyTo($sender_email, $sender_name);

			$rcpts = "";
			$recipient_ids = array();
			$cc_ids = array();
			
			$sql = "select user_id, user_email, user_firstname, " . 
				   "concat(user_name, ' ', user_firstname) as user_name " .
		           "from users where user_id = " . dbquote($project["order_retail_operator"]);

			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$mail->AddAddress($row["user_email"], $row["user_name"]);
				$rcpts .= $row["user_email"] . "\n";

				$text = str_replace("{recipient_fistname}",  $row["user_firstname"], $text);
				$recipient_ids[] = $row["user_id"];

				if($cc_mail1) {
					$mail->AddCC($cc_mail1);
					$rcpts .= $cc_mail1 . "\n";
				}
				if($cc_mail2) {
					$mail->AddCC($cc_mail2);
					$rcpts .= $cc_mail2 . "\n";
				}
				if($cc_mail3) {
					$mail->AddCC($cc_mail3);
					$rcpts .= $cc_mail3 . "\n";
				}
				if($cc_mail3) {
					$mail->AddCC($cc_mail3);
					$rcpts .= $cc_mail3 . "\n";
				}
			}


			$sql = "select user_id, user_email, user_firstname, " . 
				   "concat(user_name, ' ', user_firstname) as user_name " .
		           "from users where user_id = " . dbquote($project["project_retail_coordinator"]);

			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				
				$mail->AddCC($row["user_email"]);
				$rcpts .= $row["user_email"] . "\n";
				$cc_ids[] = $row["user_id"];
			}

			$mail->Body = $text;

			if($rcpts) {			
				if($senmail_activated == true)
				{
					if($senmail_activated == true)
					{
						$mail->send();
					}
				}
				
				foreach($recipient_ids as $key=>$recipient_id) {
					append_mail($project['project_order'], $recipient_id, $sender_id, $text, '910', 1,0);
				}

				foreach($cc_ids as $key=>$recipient_id) {
					append_mail($project['project_order'], $recipient_id, $sender_id, $text, '910', 1,0, 1);
				}
			}

		}	
	}

    $link = "project_edit_project_budget.php?pid=" . param("pid");
    redirect($link);
}

/********************************************************************
    Create Exclusion Positions
*********************************************************************/ 
$list_exclusions = new ListView($sql_order_items, LIST_HAS_HEADER);

$list_exclusions->set_title("Exclusions");
$list_exclusions->set_entity("order_item");
$list_exclusions->set_filter("order_item_order = " . $project["project_order"] .  " and order_item_type = " . ITEM_TYPE_EXCLUSION);
$list_exclusions->set_order("item_shortcut");

$list_exclusions->add_hidden("pid", param("pid"));
$list_exclusions->add_hidden("oid",$project["project_order"]);

$link="project_edit_budget_position.php?pid=" . param("pid");

if ($project["order_budget_is_locked"] == 1)
{
    $list_exclusions->add_column("item_shortcut", "Item Code");   
}
else
{
    $list_exclusions->add_column("item_shortcut", "Item Code", $link);
}

$list_exclusions->add_column("order_item_text", "Exclusion");

if ($project["order_budget_is_locked"] == 1)
{
    $list_exclusions->add_button("nothing", "");
}
else
{
    $list_exclusions->add_button("add_s_exclusion", "Add Standard Exclusions");
    $list_exclusions->add_button("add_exclusion", "Add Individual Exclusion");
    $list_exclusions->add_button("delete_exclusions", "Delete All Exclusions");
}


//process exclusions
$list_exclusions->populate();
$list_exclusions->process();

if ($list_exclusions->button("add_s_exclusion"))
{
     add_standard_budget_positions(ITEM_TYPE_EXCLUSION, $project["project_order"], $project["project_cost_type"]);
     redirect ("project_edit_project_budget.php?pid=" . param("pid"));
}
else if ($list_exclusions->button("add_exclusion"))
{
    $link = "project_add_budget_position.php?pid=" . param("pid") . "&oid=" . $project["project_order"] . "&type=" . ITEM_TYPE_EXCLUSION; 
    redirect($link);
}
else if ($list_exclusions->button("delete_exclusions"))
{
     delete_all_budget_positions(ITEM_TYPE_EXCLUSION, $project["project_order"]);
     redirect ("project_edit_project_budget.php?pid=" . param("pid"));
}

/********************************************************************
    Create Notification Positions
*********************************************************************/ 
$list_notifications = new ListView($sql_order_items, LIST_HAS_HEADER);

$list_notifications->set_title("Notifications");
$list_notifications->set_entity("order_item");
$list_notifications->set_filter("order_item_order = " . $project["project_order"] .  " and order_item_type = " . ITEM_TYPE_NOTIFICATION);
$list_exclusions->set_order("item_shortcut");

$list_notifications->add_hidden("pid", param("pid"));
$list_notifications->add_hidden("oid",$project["project_order"]);

$link="project_edit_budget_position.php?pid=" . param("pid");

if ($project["order_budget_is_locked"] == 1)
{
    $list_notifications->add_column("item_shortcut", "Item Code");   
}
else
{
    $list_notifications->add_column("item_shortcut", "Item Code", $link);
}

$list_notifications->add_column("order_item_text", "Notification");

if ($project["order_budget_is_locked"] == 1)
{
    $list_notifications->add_button("nothing", "");
}
else
{
    $list_notifications->add_button("add_s_notification", "Add Standard Notifications");
    $list_notifications->add_button("add_notification", "Add Individual Notification");
    $list_notifications->add_button("delete_notifications", "Delet All Notifications");
}


//rpocess notifications
$list_notifications->populate();
$list_notifications->process();

if ($list_notifications->button("add_s_notification"))
{
     add_standard_budget_positions(ITEM_TYPE_NOTIFICATION, $project["project_order"], $project["project_cost_type"]);
     redirect ("project_edit_project_budget.php?pid=" . param("pid"));
}
elseif ($list_notifications->button("add_notification"))
{
    $link = "project_add_budget_position.php?pid=" . param("pid") . "&oid=" . $project["project_order"] . "&type=" . ITEM_TYPE_NOTIFICATION; 
    redirect($link);
}
elseif ($list_notifications->button("delete_notifications"))
{
     delete_all_budget_positions(ITEM_TYPE_NOTIFICATION, $project["project_order"]);
     redirect ("project_edit_project_budget.php?pid=" . param("pid"));
}


/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Edit Budget");

$form->render();

echo "<br /><br />";
$list_exclusions->render();
echo "<br /><br />";
$list_notifications->render();

require_once("include/order_currency_helper.php");

require_once "include/project_footer_logistic_state.php";
$page->footer();

?>