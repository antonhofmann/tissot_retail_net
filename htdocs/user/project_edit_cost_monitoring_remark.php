<?php
/********************************************************************

    project_edit_cost_monitoring_remark.php

    Enter remarks for the cost monitoring sheet.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2007-01-15
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2007-01-15
    Version:        1.0.0

    Copyright (c) 2007, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_edit_cost_monitoring_remarks");

register_param("pid");


// read project and order details
$project = get_project(param("pid"));


// get company's address
$client_address = get_address($project["order_client_address"]);

//get old remark
$old_remark = "";

if(id() > 0)
{
	$sql = "select project_cost_remark_remark " . 
		   " from project_cost_remarks " . 
		   " where project_cost_remark_id = " . id();

	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$old_remark = $row["project_cost_remark_remark"];
	}
}

/********************************************************************
    from data
*********************************************************************/
$form = new Form("project_cost_remarks", "project_cost_remark");

$form->add_section();

$form->add_hidden("id", param("id"));
$form->add_hidden("pid", param("pid"));
$form->add_hidden("project_cost_remark_order", $project["project_order"]);

require_once "include/project_head_small.php";

$form->add_multiline("project_cost_remark_remark", "Remark", 6, NOTNULL);


$form->add_button("save", "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");
$form->add_button("delete", "Delete", "", OPTIONAL);





$form->populate();
$form->process();


//add tracking information
if($form->button("save") and $form->validate())
{
	$form->save();
	if($form->value("project_cost_remark_remark") != $old_remark)
	{
		 $result = track_change_in_order_items('Change remark in CMS', 'General Remark', $project["project_order"], 0, 0,  0, "", $old_remark, $form->value("project_cost_remark_remark"));

	}
}
elseif($form->button("delete"))
{
	
	$result = track_change_in_order_items('Delete remark in CMS', 'General Remark', $project["project_order"], 0, 0,  0, "", $old_remark, "");

	$sql = "Delete from project_cost_remarks " . 
		   " where project_cost_remark_id = " . id();

	$result = mysql_query($sql) or dberror($sql);
	redirect("project_edit_cost_monitoring.php?pid=" .param("pid"));
}

$page = new Page("orders");

require "include/project_page_actions.php";


$page->header();
$page->title(id() ? "Edit Remark of Cost Monitoring Sheet" : "Add Remark to Cost Monitoring Sheet");
$form->render();

require_once "include/project_footer_logistic_state.php";
$page->footer();

?>