<?php
/********************************************************************

    project_tracking_pdf.php

    Print Hitrory of agreed opening dates

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2011-10-03
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2011-10-03
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";
check_access("can_view_client_data_in_projects");


/********************************************************************
    prepare all data needed
*********************************************************************/

$system_currency = get_system_currency_fields();

$project = get_project(param("pid"));

if($project["pipeline"] == 0)
{
	$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");
}
elseif($project["pipeline"] == 1)
{
	$pos_data = get_poslocation_from_pipeline($project["posaddress_id"], $project["order_id"]);
}


if (param("pid"))
{
	global $page_title;
	$page_title = "Project Tracking: " . $project["project_number"];

	
	//POS Basic Data
	$posname = $project["order_shop_address_company"];

	if($project["order_shop_address_company2"])
    {
		$posname .= ", " . $project["order_shop_address_company2"];
	}

	$posaddress = $project["order_shop_address_address"];
	
	if($project["order_shop_address_address2"])
    {
		$posaddress .= ", " . $project["order_shop_address_address2"];
	}
	if($project["order_shop_address_zip"])
    {
		$posaddress .= ", " . $project["order_shop_address_zip"] . " " . $project["order_shop_address_place"];
	}
	else
	{
		$posaddress .= ", " . $project["order_shop_address_place"];
	}
	if($project["order_shop_address_country_name"])
    {
		$posaddress .= ", " . $project["order_shop_address_country_name"];
	}
	

	$poslegaltype = $project["project_costtype_text"];
	$projectkind = $project["projectkind_name"];
	$posphone = $project["order_shop_address_phone"];
	$posmobile_phone = $project["order_shop_address_mobile_phone"];
	$posemail = $project["order_shop_address_email"];
	$poswebsite = "";
	$plannedopeningdate = to_system_date($project["project_planned_opening_date"]);
	$realisticopeningdate = to_system_date($project["project_real_opening_date"]);

	$posopeningdate = to_system_date($project["project_actual_opening_date"]);
	$posclosingdate = to_system_date($project["project_shop_closingdate"]);

	$projectbudget = number_format($project["project_approximate_budget"], 2, ".", "'");

	
	$postype = $project["postype_name"];
	$possubclass = $project["possubclass_name"];
	
	if($project["productline_subclass_name"])
	{
		$posfurniture = $project["product_line_name"] . " / " . $project["productline_subclass_name"];
	}
	else
	{
		$posfurniture = $project["product_line_name"];
	}


	



	$posareas = "";
	if(count($pos_data) > 0)
	{
		if($project["pipeline"] == 0)
		{
			$sql_i = "select * from posareas " . 
					 "left join posareatypes on posareatype_id = posarea_area " .
					 "where posarea_posaddress = " . $project["posaddress_id"];
			
		}
		elseif($project["pipeline"] == 1)
		{
			$sql_i = "select * from posareaspipeline " . 
					 "left join posareatypes on posareatype_id = posarea_area " .
					 "where posarea_posaddress = " . $project["posaddress_id"];
		}
		$res_i = mysql_query($sql_i) or dberror($sql_i);
		while ($row_i = mysql_fetch_assoc($res_i))
		{
			$posareas .= $row_i["posareatype_name"] . ", ";
		}
		$posareas = substr($posareas,0,strlen($posareas)-2);

		
		
		
	}
	


	//history of agreed opening dates

	$tracking_info = array();
	$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
		   "concat(user_name, ' ', user_firstname) as user_name " . 
		   "from projecttracking " . 
		   "left join users on user_id = projecttracking_user_id " . 
		   "where projecttracking_project_id = " . param("pid") . 
		   " and projecttracking_field = 'project_real_opening_date' " . 
		   " order by projecttracking_id";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$tracking_info[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
			"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
			"projecttracking_comment"=>$row["projecttracking_comment"],
			"projecttracking_time"=>$row["projecttracking_time"],
			"user_name"=>$row["user_name"]
			);
	}


	$tracking_info2 = array();
	$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
		   "concat(user_name, ' ', user_firstname) as user_name " . 
		   "from projecttracking " . 
		   "left join users on user_id = projecttracking_user_id " . 
		   "where projecttracking_project_id = " . param("pid") . 
		   " and projecttracking_field = 'project_state' " . 
		   " order by projecttracking_id";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$tracking_info2[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
			"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
			"projecttracking_comment"=>$row["projecttracking_comment"],
			"projecttracking_time"=>$row["projecttracking_time"],
			"user_name"=>$row["user_name"]
			);
	}


	$tracking_info3 = array();
	$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
		   "concat(user_name, ' ', user_firstname) as user_name " . 
		   "from projecttracking " . 
		   "left join users on user_id = projecttracking_user_id " . 
		   "where projecttracking_project_id = " . param("pid") . 
		   " and projecttracking_field = 'project_construction_startdate' " . 
		   " order by projecttracking_id";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$tracking_info3[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
			"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
			"projecttracking_comment"=>$row["projecttracking_comment"],
			"projecttracking_time"=>$row["projecttracking_time"],
			"user_name"=>$row["user_name"]
			);
	}


	$tracking_info4 = array();
	$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
		   "concat(user_name, ' ', user_firstname) as user_name " . 
		   "from projecttracking " . 
		   "left join users on user_id = projecttracking_user_id " . 
		   "where projecttracking_project_id = " . param("pid") . 
		   " and projecttracking_field = 'project_actual_opening_date' " . 
		   " order by projecttracking_id";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$tracking_info4[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
			"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
			"projecttracking_comment"=>$row["projecttracking_comment"],
			"projecttracking_time"=>$row["projecttracking_time"],
			"user_name"=>$row["user_name"]
			);
	}



	$tracking_info5 = array();
	$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
		   "concat(user_name, ' ', user_firstname) as user_name " . 
		   "from projecttracking " . 
		   "left join users on user_id = projecttracking_user_id " . 
		   "where projecttracking_project_id = " . param("pid") . 
		   " and projecttracking_field = 'project_number' " . 
		   " order by projecttracking_id";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$tracking_info5[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
			"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
			"projecttracking_comment"=>$row["projecttracking_comment"],
			"projecttracking_time"=>$row["projecttracking_time"],
			"user_name"=>$row["user_name"]
			);
	}


	$tracking_info6 = array();
	$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
		   "concat(user_name, ' ', user_firstname) as user_name, p1.project_costtype_text as ov, " .
		   "p2.project_costtype_text as nv " .
		   "from projecttracking " .
		   "left join project_costtypes as p1 on p1.project_costtype_id = projecttracking_oldvalue " .
		   "left join project_costtypes as p2 on p2.project_costtype_id = projecttracking_newvalue " . 
		   "left join users on user_id = projecttracking_user_id " . 
		   "where projecttracking_project_id = " . param("pid") . 
		   " and projecttracking_field = 'project_cost_type' " . 
		   " order by projecttracking_id";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$tracking_info6[] = array("projecttracking_oldvalue"=>$row["ov"],
			"projecttracking_newvalue"=>$row["nv"],
			"projecttracking_comment"=>$row["projecttracking_comment"],
			"projecttracking_time"=>$row["projecttracking_time"],
			"user_name"=>$row["user_name"]
			);
	}


	$tracking_info7 = array();
	$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
		   "concat(user_name, ' ', user_firstname) as user_name, p1.postype_name as ov, " .
		   "p2.postype_name as nv " .
		   "from projecttracking " .
		   "left join postypes as p1 on p1.postype_id = projecttracking_oldvalue " .
		   "left join postypes as p2 on p2.postype_id = projecttracking_newvalue " . 
		   "left join users on user_id = projecttracking_user_id " . 
		   "where projecttracking_project_id = " . param("pid") . 
		   " and projecttracking_field = 'project_postype' " . 
		   " order by projecttracking_id";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$tracking_info7[] = array("projecttracking_oldvalue"=>$row["ov"],
			"projecttracking_newvalue"=>$row["nv"],
			"projecttracking_comment"=>$row["projecttracking_comment"],
			"projecttracking_time"=>$row["projecttracking_time"],
			"user_name"=>$row["user_name"]
			);
	}


	$tracking_info8 = array();
	$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
		   "concat(user_name, ' ', user_firstname) as user_name, p1.projectkind_name as ov, " .
		   "p2.projectkind_name as nv " .
		   "from projecttracking " .
		   "left join projectkinds as p1 on p1.projectkind_id = projecttracking_oldvalue " .
		   "left join projectkinds as p2 on p2.projectkind_id = projecttracking_newvalue " . 
		   "left join users on user_id = projecttracking_user_id " . 
		   "where projecttracking_project_id = " . param("pid") . 
		   " and projecttracking_field = 'project_projectkind' " . 
		   " order by projecttracking_id";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$tracking_info8[] = array("projecttracking_oldvalue"=>$row["ov"],
			"projecttracking_newvalue"=>$row["nv"],
			"projecttracking_comment"=>$row["projecttracking_comment"],
			"projecttracking_time"=>$row["projecttracking_time"],
			"user_name"=>$row["user_name"]
			);
	}


	
	
	$tracking_info9 = array();
	$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
		   "concat(user_name, ' ', user_firstname) as user_name " .
		   "from projecttracking " .
		   "left join users on user_id = projecttracking_user_id " . 
		   "where projecttracking_project_id = " . param("pid") . 
		   " and projecttracking_field = 'order_actual_order_state_code' " . 
		   " order by projecttracking_id";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$tracking_info9[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
			"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
			"projecttracking_comment"=>$row["projecttracking_comment"],
			"projecttracking_time"=>$row["projecttracking_time"],
			"user_name"=>$row["user_name"]
			);
	}



	$tracking_info10 = array();
	$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
		   "concat(user_name, ' ', user_firstname) as user_name, p1.project_type_subclass_name as ov, " .
		   "p2.project_type_subclass_name as nv " .
		   "from projecttracking " .
		   "left join project_type_subclasses as p1 on p1.project_type_subclass_id = projecttracking_oldvalue " .
		   "left join project_type_subclasses as p2 on p2.project_type_subclass_id = projecttracking_newvalue " . 
		   "left join users on user_id = projecttracking_user_id " . 
		   "where projecttracking_project_id = " . param("pid") . 
		   " and projecttracking_field = 'project_type_subclass_id' " . 
		   " order by projecttracking_id";


	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$tracking_info10[] = array("projecttracking_oldvalue"=>$row["ov"],
			"projecttracking_newvalue"=>$row["nv"],
			"projecttracking_comment"=>$row["projecttracking_comment"],
			"projecttracking_time"=>$row["projecttracking_time"],
			"user_name"=>$row["user_name"]
			);
	}


	$tracking_info11 = array();
	$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
		   "concat(user_name, ' ', user_firstname) as user_name " .
		   "from projecttracking " .
		   "left join users on user_id = projecttracking_user_id " . 
		   "where projecttracking_project_id = " . param("pid") . 
		   " and projecttracking_field = 'project_cost_cms_completion_due_date' " . 
		   " order by projecttracking_id";


	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$tracking_info11[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
			"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
			"projecttracking_comment"=>$row["projecttracking_comment"],
			"projecttracking_time"=>$row["projecttracking_time"],
			"user_name"=>$row["user_name"]
			);
	}




	$tracking_info12 = array();
	$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
		   "concat(user_name, ' ', user_firstname) as user_name " .
		   "from projecttracking " .
		   "left join users on user_id = projecttracking_user_id " . 
		   "where projecttracking_project_id = " . param("pid") . 
		   " and projecttracking_field = 'order_client_currency' " . 
		   " order by projecttracking_id";


	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$tracking_info12[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
			"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
			"projecttracking_comment"=>$row["projecttracking_comment"],
			"projecttracking_time"=>$row["projecttracking_time"],
			"user_name"=>$row["user_name"]
			);
	}



	$tracking_info13 = array();
	$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
		   "concat(user_name, ' ', user_firstname) as user_name " .
		   "from projecttracking " .
		   "left join users on user_id = projecttracking_user_id " . 
		   "where projecttracking_project_id = " . param("pid") . 
		   " and projecttracking_field = 'retail_coordinator' " . 
		   " order by projecttracking_id";


	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$tracking_info13[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
			"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
			"projecttracking_comment"=>$row["projecttracking_comment"],
			"projecttracking_time"=>$row["projecttracking_time"],
			"user_name"=>$row["user_name"]
			);
	}

	$tracking_info14 = array();
	$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
		   "concat(user_name, ' ', user_firstname) as user_name " .
		   "from projecttracking " .
		   "left join users on user_id = projecttracking_user_id " . 
		   "where projecttracking_project_id = " . param("pid") . 
		   " and projecttracking_field = 'retail_operator' " . 
		   " order by projecttracking_id";


	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$tracking_info14[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
			"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
			"projecttracking_comment"=>$row["projecttracking_comment"],
			"projecttracking_time"=>$row["projecttracking_time"],
			"user_name"=>$row["user_name"]
			);
	}


	$tracking_info15 = array();
	$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
		   "concat(user_name, ' ', user_firstname) as user_name " .
		   "from projecttracking " .
		   "left join users on user_id = projecttracking_user_id " . 
		   "where projecttracking_project_id = " . param("pid") . 
		   " and projecttracking_field = 'order_user' " . 
		   " order by projecttracking_id";


	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$tracking_info15[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
			"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
			"projecttracking_comment"=>$row["projecttracking_comment"],
			"projecttracking_time"=>$row["projecttracking_time"],
			"user_name"=>$row["user_name"]
			);
	}




	/********************************************************************
		prepare pdf
	*********************************************************************/

	require_once('../include/tcpdf/config/lang/eng.php');
	require_once('../include/tcpdf/tcpdf.php');


	class MYPDF extends TCPDF
	{
		//Page header
		function Header()
		{
			global $page_title;
			//Logo
			$this->Image('../pictures/brand_logo.jpg',10,8,33);
			//arialn bold 15
			$this->SetFont('arialn','B',12);
			//Move to the right
			$this->Cell(80);
			//Title
			$this->Cell(0,33,$page_title,0,0,'R');
			//Line break
			$this->Ln(20);

		}

		//Page footer
		function Footer()
		{
			//Position at 1.5 cm from bottom
			$this->SetY(-15);
			//arialn italic 8
			$this->SetFont('arialn','I',8);
			//Page number
			$this->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$this->PageNo().'/'.$this->getAliasNbPages(),0,0,'R');
		}

	}

	//Instanciation of inherited class
	$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);
	$pdf->SetMargins(10, 23, 10);

	$pdf->Open();

	$pdf->SetFillColor(220, 220, 220); 
	$pdf->AddFont('arialn');
	$pdf->AddFont('arialn', 'B');

	$pdf->AddPage();
	$first_page_has_data = 0;


	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(190,7,$posname,1, 0, 'L', 1);
	$pdf->Ln();
	$pdf->SetFont('arialn','',10);
	$pdf->Cell(190,7,$posaddress,1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();

	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Phone",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$posphone,1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Mobile Phone",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$posmobile_phone,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Email",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$posemail,1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Website",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$poswebsite,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Environment",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(163, 5,$posareas,1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();

	
	if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
	{
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(68, 5,"",1, 0, 'L', 0);
	}
	else
	{
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"Planned Opening",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Cell(68, 5,$plannedopeningdate,1, 0, 'L', 0);
	}
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,$project["projectkind_milestone_shortname_01"],1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$realisticopeningdate,1, 0, 'L', 0);
	$pdf->Ln();

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,$project["projectkind_milestone_shortname_02"],1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$posopeningdate,1, 0, 'L', 0);
	
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Closing Date",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$posclosingdate,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Legal Type",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$poslegaltype,1, 0, 'L', 0);
	
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Project Type",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$projectkind,1, 0, 'L', 0);
	$pdf->Ln();
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"POS Type",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$postype,1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Furniture",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$posfurniture,1, 0, 'L', 0);
	$pdf->Ln();


	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"POS Type Subcl.",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$possubclass,1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Aprox. Budget",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(68, 5,$projectbudget,1, 0, 'L', 0);
	$pdf->Ln();

	
	if(count($tracking_info) > 0)
	{
		$first_page_has_data = 1;
		$pdf->Ln();
		$pdf->Ln();


		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"Agreed Opening Dates",1, 0, 'L', 0);

		$pdf->Ln();
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
		$pdf->Cell(27, 5,"Time",1, 0, 'L', 0);
		$pdf->Cell(15, 5,'Old Value',1, 0, 'L', 0);
		$pdf->Cell(15, 5,'New Value',1, 0, 'L', 0);
		$pdf->Cell(106, 5,'Comment',1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		
		
		
		foreach($tracking_info as $key=>$values)
		{
			
			$pdf->Cell(27, 5,$values['user_name'],1, 0, 'L', 0);
			$pdf->Cell(27, 5,$values['projecttracking_time'],1, 0, 'L', 0);
			$pdf->Cell(15, 5,$values['projecttracking_oldvalue'],1, 0, 'L', 0);
			$pdf->Cell(15, 5,$values['projecttracking_newvalue'],1, 0, 'L', 0);
			//$pdf->Cell(106, 5,$values['projecttracking_comment'],1, 0, 'L', 0);
			$pdf->MultiCell(106,5, $values['projecttracking_comment'], 1, "T");
			

		}
	}


	if(count($tracking_info4) > 0)
	{
		$first_page_has_data = 1;
		$pdf->Ln();
		$pdf->Ln();


		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"Actual Opening Dates",1, 0, 'L', 0);

		$pdf->Ln();
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
		$pdf->Cell(27, 5,"Time",1, 0, 'L', 0);
		$pdf->Cell(15, 5,'Old Value',1, 0, 'L', 0);
		$pdf->Cell(15, 5,'New Value',1, 0, 'L', 0);
		$pdf->Cell(106, 5,'Comment',1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();

		foreach($tracking_info4 as $key=>$values)
		{
			
			$pdf->Cell(27, 5,$values['user_name'],1, 0, 'L', 0);
			$pdf->Cell(27, 5,$values['projecttracking_time'],1, 0, 'L', 0);
			$pdf->Cell(15, 5,$values['projecttracking_oldvalue'],1, 0, 'L', 0);
			$pdf->Cell(15, 5,$values['projecttracking_newvalue'],1, 0, 'L', 0);
			//$pdf->Cell(106, 5,$values['projecttracking_comment'],1, 0, 'L', 0);
			$pdf->MultiCell(106,5, $values['projecttracking_comment'], 1, "T");
			

		}
	}


	if(count($tracking_info2) > 0)
	{
		$first_page_has_data = 1;
		$pdf->Ln();
		$pdf->Ln();

		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"Treatment State",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);

		$pdf->Ln();
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
		$pdf->Cell(27, 5,"Time",1, 0, 'L', 0);
		$pdf->Cell(20, 5,'Old Value',1, 0, 'L', 0);
		$pdf->Cell(20, 5,'New Value',1, 0, 'L', 0);
		$pdf->Cell(96, 5,'Comment',1, 0, 'L', 0);
		$pdf->Ln();
		
		
		
		
		foreach($tracking_info2 as $key=>$values)
		{
			
			$pdf->Cell(27, 5,$values['user_name'],1, 0, 'L', 0);
			$pdf->Cell(27, 5,$values['projecttracking_time'],1, 0, 'L', 0);
			$pdf->Cell(20, 5,$values['projecttracking_oldvalue'],1, 0, 'L', 0);
			$pdf->Cell(20, 5,$values['projecttracking_newvalue'],1, 0, 'L', 0);
			//$pdf->Cell(106, 5,$values['projecttracking_comment'],1, 0, 'L', 0);
			$pdf->MultiCell(96,5, $values['projecttracking_comment'], 1, "T");
			

		}
	}


	if(count($tracking_info3) > 0)
	{
		$first_page_has_data = 1;
		$pdf->Ln();
		$pdf->Ln();

		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"Construction Starting",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
		$pdf->Cell(27, 5,"Time",1, 0, 'L', 0);
		$pdf->Cell(20, 5,'Old Value',1, 0, 'L', 0);
		$pdf->Cell(20, 5,'New Value',1, 0, 'L', 0);
		$pdf->Cell(96, 5,'Comment',1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		
		
		
		foreach($tracking_info3 as $key=>$values)
		{
			
			$pdf->Cell(27, 5,$values['user_name'],1, 0, 'L', 0);
			$pdf->Cell(27, 5,$values['projecttracking_time'],1, 0, 'L', 0);
			$pdf->Cell(20, 5,$values['projecttracking_oldvalue'],1, 0, 'L', 0);
			$pdf->Cell(20, 5,$values['projecttracking_newvalue'],1, 0, 'L', 0);
			//$pdf->Cell(106, 5,$values['projecttracking_comment'],1, 0, 'L', 0);
			$pdf->MultiCell(96,5, $values['projecttracking_comment'], 1, "T");
			

		}
	}


	if(count($tracking_info5) > 0)
	{
		$first_page_has_data = 1;
		$pdf->Ln();
		$pdf->Ln();

		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"Project Number",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
		$pdf->Cell(27, 5,"Time",1, 0, 'L', 0);
		$pdf->Cell(20, 5,'Old Value',1, 0, 'L', 0);
		$pdf->Cell(20, 5,'New Value',1, 0, 'L', 0);
		$pdf->Cell(96, 5,'Comment',1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		
		
		
		foreach($tracking_info5 as $key=>$values)
		{
			
			$pdf->Cell(27, 5,$values['user_name'],1, 0, 'L', 0);
			$pdf->Cell(27, 5,$values['projecttracking_time'],1, 0, 'L', 0);
			$pdf->Cell(20, 5,$values['projecttracking_oldvalue'],1, 0, 'L', 0);
			$pdf->Cell(20, 5,$values['projecttracking_newvalue'],1, 0, 'L', 0);
			//$pdf->Cell(106, 5,$values['projecttracking_comment'],1, 0, 'L', 0);
			$pdf->MultiCell(96,5, $values['projecttracking_comment'], 1, "T");
			

		}
	}

	
	if(count($tracking_info6) > 0)
	{
		$first_page_has_data = 1;
		$pdf->Ln();
		$pdf->Ln();

		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"Legal Type",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
		$pdf->Cell(27, 5,"Time",1, 0, 'L', 0);
		$pdf->Cell(20, 5,'Old Value',1, 0, 'L', 0);
		$pdf->Cell(20, 5,'New Value',1, 0, 'L', 0);
		$pdf->Cell(96, 5,'Comment',1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		
		
		
		foreach($tracking_info6 as $key=>$values)
		{
			
			$pdf->Cell(27, 5,$values['user_name'],1, 0, 'L', 0);
			$pdf->Cell(27, 5,$values['projecttracking_time'],1, 0, 'L', 0);
			$pdf->Cell(20, 5,$values['projecttracking_oldvalue'],1, 0, 'L', 0);
			$pdf->Cell(20, 5,$values['projecttracking_newvalue'],1, 0, 'L', 0);
			//$pdf->Cell(106, 5,$values['projecttracking_comment'],1, 0, 'L', 0);
			$pdf->MultiCell(96,5, $values['projecttracking_comment'], 1, "T");
			

		}
	}


	if(count($tracking_info7) > 0)
	{
		$first_page_has_data = 1;
		$pdf->Ln();
		$pdf->Ln();

		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"POS Type",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
		$pdf->Cell(27, 5,"Time",1, 0, 'L', 0);
		$pdf->Cell(20, 5,'Old Value',1, 0, 'L', 0);
		$pdf->Cell(20, 5,'New Value',1, 0, 'L', 0);
		$pdf->Cell(96, 5,'Comment',1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		
		
		
		foreach($tracking_info7 as $key=>$values)
		{
			
			$pdf->Cell(27, 5,$values['user_name'],1, 0, 'L', 0);
			$pdf->Cell(27, 5,$values['projecttracking_time'],1, 0, 'L', 0);
			$pdf->Cell(20, 5,$values['projecttracking_oldvalue'],1, 0, 'L', 0);
			$pdf->Cell(20, 5,$values['projecttracking_newvalue'],1, 0, 'L', 0);
			//$pdf->Cell(106, 5,$values['projecttracking_comment'],1, 0, 'L', 0);
			$pdf->MultiCell(96,5, $values['projecttracking_comment'], 1, "T");
			

		}
	}


	if(count($tracking_info8) > 0)
	{
		$first_page_has_data = 1;
		$pdf->Ln();
		$pdf->Ln();

		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"Project Type",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
		$pdf->Cell(27, 5,"Time",1, 0, 'L', 0);
		$pdf->Cell(30, 5,'Old Value',1, 0, 'L', 0);
		$pdf->Cell(30, 5,'New Value',1, 0, 'L', 0);
		$pdf->Cell(76, 5,'Comment',1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		
		
		
		foreach($tracking_info8 as $key=>$values)
		{
			
			$pdf->Cell(27, 5,$values['user_name'],1, 0, 'L', 0);
			$pdf->Cell(27, 5,$values['projecttracking_time'],1, 0, 'L', 0);
			$pdf->Cell(30, 5,$values['projecttracking_oldvalue'],1, 0, 'L', 0);
			$pdf->Cell(30, 5,$values['projecttracking_newvalue'],1, 0, 'L', 0);
			//$pdf->Cell(106, 5,$values['projecttracking_comment'],1, 0, 'L', 0);
			$pdf->MultiCell(76,5, $values['projecttracking_comment'], 1, "T");
			

		}
	}



	if(count($tracking_info9) > 0)
	{
		$first_page_has_data = 1;
		$pdf->Ln();
		$pdf->Ln();

		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(50, 5,"Manually Set Order States",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
		$pdf->Cell(27, 5,"Time",1, 0, 'L', 0);
		$pdf->Cell(30, 5,'Old Value',1, 0, 'L', 0);
		$pdf->Cell(30, 5,'New Value',1, 0, 'L', 0);
		$pdf->Cell(76, 5,'Comment',1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		foreach($tracking_info9 as $key=>$values)
		{
			$pdf->Cell(27, 5,$values['user_name'],1, 0, 'L', 0);
			$pdf->Cell(27, 5,$values['projecttracking_time'],1, 0, 'L', 0);
			$pdf->Cell(30, 5,$values['projecttracking_oldvalue'],1, 0, 'L', 0);
			$pdf->Cell(30, 5,$values['projecttracking_newvalue'],1, 0, 'L', 0);
			$pdf->MultiCell(76,5, $values['projecttracking_comment'], 1, "T");

		}
	}


	if(count($tracking_info12) > 0)
	{
		$first_page_has_data = 1;
		$pdf->Ln();
		$pdf->Ln();

		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(50, 5,"Manually Set Currency/Exchange Rate",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
		$pdf->Cell(27, 5,"Time",1, 0, 'L', 0);
		$pdf->Cell(30, 5,'Old Value',1, 0, 'L', 0);
		$pdf->Cell(30, 5,'New Value',1, 0, 'L', 0);
		$pdf->Cell(76, 5,'Comment',1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		foreach($tracking_info12 as $key=>$values)
		{
			$pdf->Cell(27, 5,$values['user_name'],1, 0, 'L', 0);
			$pdf->Cell(27, 5,$values['projecttracking_time'],1, 0, 'L', 0);
			$pdf->Cell(30, 5,$values['projecttracking_oldvalue'],1, 0, 'L', 0);
			$pdf->Cell(30, 5,$values['projecttracking_newvalue'],1, 0, 'L', 0);
			$pdf->MultiCell(76,5, $values['projecttracking_comment'], 1, "T");

		}
	}



	if(count($tracking_info13) > 0)
	{
		$first_page_has_data = 1;
		$pdf->Ln();
		$pdf->Ln();

		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(50, 5,"Project Leader",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
		$pdf->Cell(27, 5,"Time",1, 0, 'L', 0);
		$pdf->Cell(30, 5,'Old Value',1, 0, 'L', 0);
		$pdf->Cell(30, 5,'New Value',1, 0, 'L', 0);
		$pdf->Cell(76, 5,'Comment',1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		foreach($tracking_info13 as $key=>$values)
		{
			$pdf->Cell(27, 5,$values['user_name'],1, 0, 'L', 0);
			$pdf->Cell(27, 5,$values['projecttracking_time'],1, 0, 'L', 0);
			$pdf->Cell(30, 5,$values['projecttracking_oldvalue'],1, 0, 'L', 0);
			$pdf->Cell(30, 5,$values['projecttracking_newvalue'],1, 0, 'L', 0);
			$pdf->MultiCell(76,5, $values['projecttracking_comment'], 1, "T");

		}
	}


	if(count($tracking_info14) > 0)
	{
		$first_page_has_data = 1;
		$pdf->Ln();
		$pdf->Ln();

		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(50, 5,"Logistics Coordinator",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
		$pdf->Cell(27, 5,"Time",1, 0, 'L', 0);
		$pdf->Cell(30, 5,'Old Value',1, 0, 'L', 0);
		$pdf->Cell(30, 5,'New Value',1, 0, 'L', 0);
		$pdf->Cell(76, 5,'Comment',1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		foreach($tracking_info14 as $key=>$values)
		{
			$pdf->Cell(27, 5,$values['user_name'],1, 0, 'L', 0);
			$pdf->Cell(27, 5,$values['projecttracking_time'],1, 0, 'L', 0);
			$pdf->Cell(30, 5,$values['projecttracking_oldvalue'],1, 0, 'L', 0);
			$pdf->Cell(30, 5,$values['projecttracking_newvalue'],1, 0, 'L', 0);
			$pdf->MultiCell(76,5, $values['projecttracking_comment'], 1, "T");

		}
	}


	if(count($tracking_info15) > 0)
	{
		$first_page_has_data = 1;
		$pdf->Ln();
		$pdf->Ln();

		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(50, 5,"Project Owner",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
		$pdf->Cell(27, 5,"Time",1, 0, 'L', 0);
		$pdf->Cell(30, 5,'Old Value',1, 0, 'L', 0);
		$pdf->Cell(30, 5,'New Value',1, 0, 'L', 0);
		$pdf->Cell(76, 5,'Comment',1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		foreach($tracking_info15 as $key=>$values)
		{
			$pdf->Cell(27, 5,$values['user_name'],1, 0, 'L', 0);
			$pdf->Cell(27, 5,$values['projecttracking_time'],1, 0, 'L', 0);
			$pdf->Cell(30, 5,$values['projecttracking_oldvalue'],1, 0, 'L', 0);
			$pdf->Cell(30, 5,$values['projecttracking_newvalue'],1, 0, 'L', 0);
			$pdf->MultiCell(76,5, $values['projecttracking_comment'], 1, "T");

		}
	}



	if(count($tracking_info10) > 0)
	{
		$first_page_has_data = 1;
		$pdf->Ln();
		$pdf->Ln();

		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(50, 5,"Manually Set Project Type Subclass",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
		$pdf->Cell(27, 5,"Time",1, 0, 'L', 0);
		$pdf->Cell(30, 5,'Old Value',1, 0, 'L', 0);
		$pdf->Cell(30, 5,'New Value',1, 0, 'L', 0);
		$pdf->Cell(76, 5,'Comment',1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		foreach($tracking_info10 as $key=>$values)
		{
			$pdf->Cell(27, 5,$values['user_name'],1, 0, 'L', 0);
			$pdf->Cell(27, 5,$values['projecttracking_time'],1, 0, 'L', 0);
			$pdf->Cell(30, 5,$values['projecttracking_oldvalue'],1, 0, 'L', 0);
			$pdf->Cell(30, 5,$values['projecttracking_newvalue'],1, 0, 'L', 0);
			$pdf->MultiCell(76,5, $values['projecttracking_comment'], 1, "T");

		}
	}


	if(count($tracking_info11) > 0)
	{
		$first_page_has_data = 1;
		$pdf->Ln();
		$pdf->Ln();

		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(50, 5,"Changes of CMS Due Date",1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
		$pdf->Cell(27, 5,"Time",1, 0, 'L', 0);
		$pdf->Cell(30, 5,'Old Value',1, 0, 'L', 0);
		$pdf->Cell(30, 5,'New Value',1, 0, 'L', 0);
		$pdf->Cell(76, 5,'Comment',1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);
		$pdf->Ln();
		
		foreach($tracking_info11 as $key=>$values)
		{
			$pdf->Cell(27, 5,$values['user_name'],1, 0, 'L', 0);
			$pdf->Cell(27, 5,$values['projecttracking_time'],1, 0, 'L', 0);
			$pdf->Cell(30, 5,$values['projecttracking_oldvalue'],1, 0, 'L', 0);
			$pdf->Cell(30, 5,$values['projecttracking_newvalue'],1, 0, 'L', 0);
			$pdf->MultiCell(76,5, $values['projecttracking_comment'], 1, "T");

		}
	}


	//get user_tracking

	

	$header_printed = false;

	$sql = "select *, concat(user_name, ' ', user_firstname) as user_name, " .
		   "user_tracking.date_created as projecttracking_time " . 
		   "from user_tracking ". 
		   "left join users on user_id = user_tracking_user " . 
		   "where user_tracking_track like '% " . $project["project_order"] . "%' ". 
	       "order by user_tracking.date_created ";

	$tmp = " of order ". $project["project_order"];
	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		
		
		if($header_printed == false)
		{
			$pdf->Ln();
			$pdf->Ln();

			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(54, 5,"Other Tracking Information",1, 0, 'L', 0);

			$pdf->Ln();
			
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
			$pdf->Cell(27, 5,"Time",1, 0, 'L', 0);
			$pdf->Cell(116, 5,'Tracking Info',1, 0, 'L', 0);
			$pdf->SetFont('arialn','',8);
			$pdf->Ln();
			$header_printed = true;
		}
		
		
		$pdf->Cell(27, 5,$row['user_name'],1, 0, 'L', 0);
		$pdf->Cell(27, 5,$row['projecttracking_time'],1, 0, 'L', 0);
		
		if(strpos($row['user_tracking_track'], 'deleted task for project state') > 0)
		{
			$tinfo = '';
			
			$tmp = $row['user_tracking_track'];
			$tmp = str_replace('User has deleted task for project state ', '', $tmp);
			$tmp = str_replace('of order ', '', $tmp);
			$tmp = str_replace('for user ', '', $tmp);

			$tmp = explode(' ', $tmp);

			$sql_t = "select order_state_code from order_states where order_state_id = " . $tmp[0];
			$res_t = mysql_query($sql_t);
			if($row_t = mysql_fetch_assoc($res_t))
			{
				$tinfo = $row['user_name'] ." has deleted task for step " . $row_t["order_state_code"] . " for ";
			}

			$sql_t = "select concat(user_name, ' ', user_firstname) as uname from users where user_id  = " . $tmp[2];
			$res_t = mysql_query($sql_t);
			if($row_t = mysql_fetch_assoc($res_t))
			{
				$tinfo .= $row_t["uname"];
			}

			$row['user_tracking_track'] = $tinfo;
		}
		
		
		$pdf->Cell(116, 5, str_replace($tmp, "", $row['user_tracking_track']),1, 0, 'L', 0);
		$pdf->Ln();
	}



	//print details of the list of materials
	$add_new_page = 1;
	$sql = "select order_item_trackings.date_created as projecttracking_time, " . 
		   "concat(user_name, ' ', user_firstname) as user_name, " . 
		   " order_state_code, order_item_tracking_action, order_item_tracking_field_name, order_item_tracking_item_text, " .
		   " order_item_tracking_old_value, order_item_tracking_new_value, " .
		   " item_code " . 
		   " from order_item_trackings " .
		   " left join users on user_id = order_item_tracking_user_id " . 
		   " left join order_states on order_state_id = order_item_tracking_order_state_id " . 
		   " left join items on item_id = order_item_tracking_order_item_item_id " . 
		   " where order_item_tracking_action like '%list of materials%' " . 
		   " and order_item_tracking_order_id = " . dbquote($project["project_order"]) . 
		   " order by order_item_trackings.date_created ASC";
	
	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		if($add_new_page == 1)
		{
			if($first_page_has_data == 1)
			{
				$pdf->AddPage();
			}
			
			
			$add_new_page = 0;

			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(54, 5,"Tracking List of Materials",1, 0, 'L', 0);

			$pdf->Ln();
			
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
			$pdf->Cell(27, 5,"Date",1, 0, 'L', 0);
			$pdf->Cell(10, 5,'Status',1, 0, 'L', 0);
			$pdf->Cell(126, 5,'Tracking Info',1, 0, 'L', 0);
			$pdf->SetFont('arialn','',8);
			$pdf->Ln();
		
		}

		$y1=$pdf->GetY();
		$x1=$pdf->GetX();
		$x2 = 74;

		$tracking_info = $row["order_item_tracking_action"] . ": " . $row["order_item_tracking_item_text"];
		$tracking_info .= "\n" . "Old " . $row["order_item_tracking_field_name"] . " = " .  $row["order_item_tracking_old_value"] . ", New " . $row["order_item_tracking_field_name"] . " = " . $row["order_item_tracking_new_value"];
		
		$pdf->SetX($x2);
		$pdf->MultiCell(126,0, $tracking_info, 1);

		$y2=$pdf->GetY();

		$pdf->SetXY($x1,$y1);
		$h = $y2 - $y1;
		$pdf->Cell(27, $h,$row["user_name"],1, 0, 'L', 0);
		$pdf->Cell(27, $h,$row["projecttracking_time"],1, 0, 'L', 0);
		$pdf->Cell(10, $h,$row["order_state_code"],1, 0, 'L', 0);


		$pdf->Ln();


		if($pdf->getY() > 265)
		{
			$pdf->AddPage();


			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(54, 5,"Tracking of the Cost Monitoring",1, 0, 'L', 0);

			$pdf->Ln();
			
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
			$pdf->Cell(27, 5,"Date",1, 0, 'L', 0);
			$pdf->Cell(10, 5,'Status',1, 0, 'L', 0);
			$pdf->Cell(126, 5,'Tracking Info',1, 0, 'L', 0);
			$pdf->SetFont('arialn','',8);
			$pdf->Ln();
		}

		


	}



	//print details for the cost monitoring
	$add_new_page = 1;
	$sql = "select order_item_trackings.date_created as projecttracking_time, " . 
		   "concat(user_name, ' ', user_firstname) as user_name, " . 
		   " order_state_code, order_item_tracking_action, order_item_tracking_field_name, order_item_tracking_item_text, " .
		   " order_item_tracking_old_value, order_item_tracking_new_value, " .
		   " item_code " . 
		   " from order_item_trackings " .
		   " left join users on user_id = order_item_tracking_user_id " . 
		   " left join order_states on order_state_id = order_item_tracking_order_state_id " . 
		   " left join items on item_id = order_item_tracking_order_item_item_id " . 
		   " where order_item_tracking_action like '%in CMS%' " . 
		   " and order_item_tracking_order_id = " . dbquote($project["project_order"]) . 
		   " order by order_item_trackings.date_created ASC";

	
	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		if($add_new_page == 1)
		{
			if($first_page_has_data == 1)
			{
				$pdf->AddPage();
			}
			
			
			$add_new_page = 0;

			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(54, 5,"Tracking of the Cost Monitoring",1, 0, 'L', 0);

			$pdf->Ln();
			
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
			$pdf->Cell(27, 5,"Date",1, 0, 'L', 0);
			$pdf->Cell(10, 5,'Status',1, 0, 'L', 0);
			$pdf->Cell(126, 5,'Tracking Info',1, 0, 'L', 0);
			$pdf->SetFont('arialn','',8);
			$pdf->Ln();
		
		}

		$y1=$pdf->GetY();
		$x1=$pdf->GetX();
		$x2 = 74;

		$tracking_info = $row["order_item_tracking_action"] . ": " . $row["order_item_tracking_item_text"];
		$tracking_info .= "\n" . "Old " . $row["order_item_tracking_field_name"] . " = " .  $row["order_item_tracking_old_value"] . ", New " . $row["order_item_tracking_field_name"] . " = " . $row["order_item_tracking_new_value"];
		
		$pdf->SetX($x2);
		$pdf->MultiCell(126,0, $tracking_info, 1);

		$y2=$pdf->GetY();

		$pdf->SetXY($x1,$y1);
		$h = $y2 - $y1;
		$pdf->Cell(27, $h,$row["user_name"],1, 0, 'L', 0);
		$pdf->Cell(27, $h,$row["projecttracking_time"],1, 0, 'L', 0);
		$pdf->Cell(10, $h,$row["order_state_code"],1, 0, 'L', 0);


		$pdf->Ln();

		if($pdf->getY() > 265)
		{
			$pdf->AddPage();


			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(54, 5,"Tracking of the Cost Monitoring",1, 0, 'L', 0);

			$pdf->Ln();
			
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
			$pdf->Cell(27, 5,"Date",1, 0, 'L', 0);
			$pdf->Cell(10, 5,'Status',1, 0, 'L', 0);
			$pdf->Cell(126, 5,'Tracking Info',1, 0, 'L', 0);
			$pdf->SetFont('arialn','',8);
			$pdf->Ln();
		}
	}



	//print details for catalogue orders

	$sql = "select order_item_trackings.date_created as projecttracking_time, " . 
		   "concat(user_name, ' ', user_firstname) as user_name, " . 
		   " order_state_code, order_item_tracking_action, order_item_tracking_field_name, order_item_tracking_item_text, " .
		   " order_item_tracking_old_value, order_item_tracking_new_value, " .
		   " item_code " . 
		   " from order_item_trackings " .
		   " left join users on user_id = order_item_tracking_user_id " . 
		   " left join order_states on order_state_id = order_item_tracking_order_state_id " . 
		   " left join items on item_id = order_item_tracking_order_item_item_id " . 
		   " where order_item_tracking_action like '%catalogue order%' " . 
		   " and order_item_tracking_order_id = " . dbquote($project["project_order"]) . 
		   " order by order_item_trackings.date_created ASC";

	
	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		if($add_new_page == 1)
		{
			if($first_page_has_data == 1)
			{
				$pdf->AddPage();
			}
			
			
			$add_new_page = 0;

			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(54, 5,"Tracking of assigned Catalogue Orders",1, 0, 'L', 0);

			$pdf->Ln();
			
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
			$pdf->Cell(27, 5,"Date",1, 0, 'L', 0);
			$pdf->Cell(10, 5,'Status',1, 0, 'L', 0);
			$pdf->Cell(126, 5,'Tracking Info',1, 0, 'L', 0);
			$pdf->SetFont('arialn','',8);
			$pdf->Ln();
		
		}

		$y1=$pdf->GetY();
		$x1=$pdf->GetX();
		$x2 = 74;

		$tracking_info = $row["order_item_tracking_action"] . ": " . $row["order_item_tracking_item_text"];
		$tracking_info .= "\n" . "Old " . $row["order_item_tracking_field_name"] . " = " .  $row["order_item_tracking_old_value"] . ", New " . $row["order_item_tracking_field_name"] . " = " . $row["order_item_tracking_new_value"];
		
		$pdf->SetX($x2);
		$pdf->MultiCell(126,0, $tracking_info, 1);

		$y2=$pdf->GetY();

		$pdf->SetXY($x1,$y1);
		$h = $y2 - $y1;
		$pdf->Cell(27, $h,$row["user_name"],1, 0, 'L', 0);
		$pdf->Cell(27, $h,$row["projecttracking_time"],1, 0, 'L', 0);
		$pdf->Cell(10, $h,$row["order_state_code"],1, 0, 'L', 0);


		$pdf->Ln();

		if($pdf->getY() > 265)
		{
			$pdf->AddPage();


			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(54, 5,"Tracking of assigned Catalogue Orders",1, 0, 'L', 0);

			$pdf->Ln();
			
			$pdf->SetFont('arialn','B',8);
			$pdf->Cell(27, 5,"User",1, 0, 'L', 0);
			$pdf->Cell(27, 5,"Date",1, 0, 'L', 0);
			$pdf->Cell(10, 5,'Status',1, 0, 'L', 0);
			$pdf->Cell(126, 5,'Tracking Info',1, 0, 'L', 0);
			$pdf->SetFont('arialn','',8);
			$pdf->Ln();
		}
	}

		

	// write pdf
	$pdf->Output("project_tracking_" . $project["project_number"]);

}

?>