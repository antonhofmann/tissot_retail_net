<?php
/********************************************************************

    project_offer_pdf.php

    Print Offer for Local Construction Work

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-09
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-09
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/offer_functions.php";

check_access("can_edit_local_constrction_work");



/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());

// read project and order details
$project = get_project(param("pid"));
$offer = get_offer(param("lwoid"));


//read offer

// get orders's currency
$order_currency = get_order_currency($project["project_order"]);

global $page_title;
$page_title = "Description for Contractors Local Work - Project " . $project["project_number"];

global $page_footer;
$page_footer = "Description for Contractors Local Work - Project " . $project["project_number"];

$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];


$order_state_name = get_actual_order_state_name($project["order_actual_order_state_code"], 1);
$retail_coordinator = get_user($project["project_retail_coordinator"]);
$retail_operator = get_user($project["order_retail_operator"]);


$invoice_address = "";
$billing_address_province_name = "";
if($project["order_billing_address_place_id"])
{
	$billing_address_province_name = get_province_name($project["order_billing_address_place_id"]);
}
$invoice_address = $invoice_address . $project["order_billing_address_company"];
if ($project["order_billing_address_company2"])
{
    $invoice_address = $invoice_address . ", " . $project["order_billing_address_company2"];
}

$invoice_address = $invoice_address . ", " . $project["order_billing_address_address"];

if ($project["order_billing_address_address2"])
{
    $invoice_address = $invoice_address . ", " . $project["order_billing_address_address2"];
}

$invoice_address = $invoice_address . ", " . $project["order_billing_address_zip"] . " " . $project["order_billing_address_place"];


if($billing_address_province_name) {
	$invoice_address .= ', ' . $billing_address_province_name;
}

$invoice_address = $invoice_address . ", " . $project["order_billing_address_country_name"];
$invoice_address = $invoice_address . ", Phone: " . $project["order_billing_address_phone"];

// get company's address
$client_address = get_address($project["order_client_address"]);
$client = $client_address["company"] . ", " .
          $client_address["zip"] . " " . $client_address["place"] . ", " .
          $client_address["country_name"];


// prepare output fields
$captions1 = array();
$captions1[] = "Project Number:";
$captions1[] = "Product Line / Type:";
$captions1[] = "Project Starting Date / Agreed Opening Date:";
$captions1[] = "Status:";
$captions1[] = "Project Leader / Logistics Coordinator:";

$captions2 = array();
$captions2[] = "Company:";
$captions2[] = "Address:";
$captions2[] = "Zip / City:";
$captions2[] = "Contact:";
$captions2[] = "Phone / Email:";


$captions3 = array();
$captions3[] = "Client:";
$captions3[] = "Shop:";
//$captions3[] = "Bill to:";

$data1 = array();
$data1[] = $project["project_number"];
$data1[] = $project["product_line_name"] . " / " . $project["postype_name"];
$data1[] = to_system_date($project["order_date"]) . " / " .  to_system_date($project["project_real_opening_date"]);
$data1[] = $project["order_actual_order_state_code"] . " " . $order_state_name;
$data1[] = $retail_coordinator["firstname"] . " " . $retail_coordinator["name"] . " / " . $retail_operator["firstname"] . " " . $retail_operator["name"];

$data2 = array();
$data2[] = $offer["lwoffer_company"];
$data2[] = $offer["lwoffer_address"];
$data2[] = $offer["lwoffer_zip"] . " " . $offer["lwoffer_place"];
$data2[] = $offer["lwoffer_contact"];
$data2[] = $offer["lwoffer_phone"] . " " . $offer["lwoffer_email"];

$data3 = array();
$data3[] = $client;
$data3[] = $shop;
//$data3[] = $invoice_address;


/********************************************************************
    prepare pdf
*********************************************************************/

require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');

class MYPDF extends TCPDF
{
    //Page header
    function Header()
    {
        global $page_title;
		//Logo
        $this->Image('../pictures/brand_logo.jpg',10,8,33);
        //arialn bold 15
        $this->SetFont('arialn','B',12);
        //Move to the right
        $this->Cell(80);
        //Title
        $this->Cell(0,34, $page_title, 0, 0, 'R');
        //Line break
        $this->Ln(20);

    }

    //Page footer
    function Footer()
    {
        global $page_footer;
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //arialn italic 8
        $this->SetFont('arialn','I',8);
        //Page number
        $this->Cell(0,10, $page_footer . " / " .to_system_date(date("d.m.y")) . ' / Page '.$this->PageNo().'/'.$this->getAliasNbPages(),0,0,'R');
    }
   
}

//Instanciation of inherited class
$pdf = new MYPDF("L", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 23, 12);

$pdf->Open();

$pdf->SetLineWidth(0.1);

$pdf->AddFont('arialn');
$pdf->AddFont('arialn', 'B');

$pdf->AddPage();


// output project header informations
$pdf->SetFont('arialn','B',12);
$pdf->Cell(140,6,"Project Details",1);
$pdf->Cell(5,5," ",0);
$pdf->Cell(130,6,"Contractor",1);
$pdf->Ln();


$pdf->SetFont('arialn','',9);

foreach($captions1 as $key=>$value)
{
    $pdf->SetFont('arialn','B',9);
    $pdf->Cell(70,5,$captions1[$key],1);
    $pdf->SetFont('arialn','',9);
    $pdf->Cell(70,5,$data1[$key],1);

    $pdf->Cell(5,5," ",0);

    if($key <=4)
    {
        $pdf->SetFont('arialn','B',9);
        $pdf->Cell(45,5,$captions2[$key],1);
        $pdf->SetFont('arialn','',9);
        $pdf->Cell(85,5,$data2[$key], 1, 0, 'L');
    }
    
    $pdf->Ln();
}

$pdf->SetFont('arialn','',9);

$pdf->Ln();
$pdf->Ln();

foreach($captions3 as $key=>$value)
{
    $pdf->SetFont('arialn','B',9);
    $pdf->Cell(40,6,$captions3[$key],1);
    $pdf->SetFont('arialn','',9);
    $pdf->MultiCell(235,5,$data3[$key],1);
}


$pdf->Ln();
$pdf->Ln();


// Offer Positions

$sql_offer_positions = "select lwofferposition_id, lwofferposition_lwoffergroup, lwofferposition_code, " .
                       "lwofferposition_text, lwofferposition_action, lwofferposition_unit, " . 
                       "lwofferposition_numofunits, lwofferposition_price, lwofferposition_hasoffer, " .
                       "concat(lwoffergroup_code,  ' ', lwoffergroup_text) as lwoffergroup " .
                       "from lwofferpositions " .       
                       "left join lwoffergroups on lwoffergroup_id = lwofferposition_lwoffergroup ". 
                       "where lwoffergroup_offer = " . param("lwoid")  . 
                       " order by lwofferposition_lwoffergroup, lwofferposition_code";


$pdf->SetFont('arialn','',9);


$res = mysql_query($sql_offer_positions) or dberror($sql_offer_positions);

$group = "";
$grand_total = 0;

while ($row = mysql_fetch_assoc($res))
{
    if($group != $row["lwoffergroup"])
    {
        
        $group = $row["lwoffergroup"];
        $pdf->SetFont('arialn','B',9);
        $pdf->SetFillColor(220, 220, 220); 
        $pdf->Cell(175,5, $row["lwoffergroup"], 1, 0, 'L', 1);
        $pdf->Cell(20,5, "Action", 1, 0, 'L', 1);
        $pdf->Cell(20,5, "Unit", 1, 0, 'L', 1);
        $pdf->Cell(20,5, "Units", 1, 0, 'L', 1);
        $pdf->Cell(20,5, "Price", 1, 0, 'L', 1);
        $pdf->Cell(20,5, "Total", 1, 0, 'L', 1);
        $pdf->Ln();
        $pdf->SetFont('arialn','',9);
    }

    

    
    if(strlen($row["lwofferposition_text"]) > 80)
    {
        if($row["lwofferposition_hasoffer"] == 1)
        {
            $pdf->SetFillColor(220, 220, 220); 
            $pdf->Cell(5,5, "X", 1, 0, 'L', 1);
        }
        else
        {
            $pdf->Cell(5,5, "", 1);
        }
        
        
        $pdf->Cell(20,5, $row["lwofferposition_code"], 1);
        $pdf->Cell(150,5, substr($row["lwofferposition_text"], 0, 80), 1);
        $pdf->Cell(20,5, "", 1);
        $pdf->Cell(20,5, "", 1);
        $pdf->Cell(20,5, "", 1);
        $pdf->Cell(20,5, "", 1);
        $pdf->Cell(20,5, "", 1);
        $pdf->Ln();
        
        if($row["lwofferposition_hasoffer"] == 1)
        {
            $pdf->SetFillColor(220, 220, 220); 
            $pdf->Cell(5,5, " ", 1, 0, 'L', 1);
        }
        else
        {
            $pdf->Cell(5,5, "", 1);
        }

        $pdf->Cell(20,5, "", 1);
        $pdf->Cell(150,5, substr($row["lwofferposition_text"], 79, 80), 1);
        $pdf->Cell(20,5, $row["lwofferposition_action"], 1);
        $pdf->Cell(20,5, $row["lwofferposition_unit"], 1);
        if($row["lwofferposition_numofunits"] >0)
        {
            $pdf->Cell(20,5, $row["lwofferposition_numofunits"], 1, 0, 'R');
        }
        else
        {
            $pdf->Cell(20,5, "", 1);
        }

        if($row["lwofferposition_price"] >0)
        {
            $pdf->Cell(20,5, $row["lwofferposition_price"], 1, 0, 'R');
        }
        else
        {
            $pdf->Cell(20,5, "", 1);
        }

        if($row["lwofferposition_price"] >0)
        {
            $pdf->Cell(20,5, number_format($row["lwofferposition_numofunits"]*$row["lwofferposition_price"], 2, ".", "'"), 1, 0, 'R');
            $grand_total = $grand_total + $row["lwofferposition_numofunits"]*$row["lwofferposition_price"];
        }
        else
        {
            $pdf->Cell(20,5, "", 1);
        }
        $pdf->Ln();
    }
    else
    {
        if($row["lwofferposition_hasoffer"] == 1)
        {
            $pdf->SetFillColor(220, 220, 220); 
            $pdf->Cell(5,5, "X", 1, 0, 'L', 1);
        }
        else
        {
            $pdf->Cell(5,5, "", 1);
        }

        $pdf->Cell(20,5, $row["lwofferposition_code"], 1);
        $pdf->Cell(150,5, $row["lwofferposition_text"], 1);
        $pdf->Cell(20,5, $row["lwofferposition_action"], 1);
        $pdf->Cell(20,5, $row["lwofferposition_unit"], 1);
        
        if($row["lwofferposition_numofunits"] >0)
        {
            $pdf->Cell(20,5, $row["lwofferposition_numofunits"], 1, 0, 'R');
        }
        else
        {
            $pdf->Cell(20,5, "", 1);
        }

        if($row["lwofferposition_price"] >0)
        {
            $pdf->Cell(20,5, $row["lwofferposition_price"], 1, 0, 'R');
        }
        else
        {
            $pdf->Cell(20,5, "", 1);
        }

        if($row["lwofferposition_price"] >0)
        {
            $pdf->Cell(20,5, number_format($row["lwofferposition_numofunits"]*$row["lwofferposition_price"], 2, ".", "'"), 1, 0, 'R');
            $grand_total = $grand_total + $row["lwofferposition_numofunits"]*$row["lwofferposition_price"];
        }
        else
        {
            $pdf->Cell(20,5, "", 1);
        }

        $pdf->Ln();
    }
    
    
}


if($grand_total > 0)
{
    $pdf->Ln();
    $pdf->SetFont('arialn','B',9);

    $pdf->Cell(235,5, "Grand Total", 1);
    $pdf->Cell(40,5, $offer["currency_symbol"] . " " . number_format($grand_total, 2, ".", "'"), 1, 0, 'R');
}


$pdf->Ln();
$pdf->Ln();





// write pdf
$pdf->Output();


?>