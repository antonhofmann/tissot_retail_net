<?php
/********************************************************************

    project_forecasts.php

    Lists planned projects (forecats) for editing

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2013-10-01
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2013-10-01
    Version:        1.0.0

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";


if (has_access("can_view_only_his_project_forecasts")
   or has_access("can_view_project_forecasts")
   or has_access("can_edit_only_his_project_forecasts")
   or has_access("can_edit_project_forecasts"))
{

}
elseif(!user_id())
{
	redirect("login.php");
}
else
{
	
	redirect("noaccess.php");
}

set_referer("project_forecast.php");


/********************************************************************
    prepare all data needed
*********************************************************************/
$user = get_user(user_id());
$user_address = get_address($user['address']);


if(param("country") > 0)
{
	$country = param("country");
}
else
{
	$country = $user_address["country"];
}


//create countries for filter drop down
$countries = array();

if (has_access("can_view_only_his_project_forecasts")
   or has_access("can_edit_only_his_project_forecasts"))
{
	//country access
	
	$sql_c = "select country_access_country, country_name from country_access " .
			 "left join countries on country_id = country_access_country ". 
			 "where country_access_user = " . user_id();
	$res_c = mysql_query($sql_c) or dberror($sql_c);
	while ($row = mysql_fetch_assoc($res_c))
	{            
		$countries[$row["country_access_country"]] = $row["country_name"];
	}
	$countries[$user_address["country"]] = $user_address["country_name"];
	
	
	//regional access
	/*
	$sql_c = "select country_id,country_name from user_company_responsibles " .
		     " left join addresses on address_id = user_company_responsible_address_id " . 
			 " left join countries on country_id = address_country ". 
			 "where user_company_responsible_user_id = " . user_id();
	$res_c = mysql_query($sql_c) or dberror($sql_c);
	while ($row = mysql_fetch_assoc($res_c))
	{            
		$countries[$row["country_id"]] = $row["country_name"];
	}
	$countries[$user_address["country"]] = $user_address["country_name"];
	*/
	
	
	
	
	asort($countries);
}
else
{
	$sql_c = "select DISTINCT country_id, country_name " . 
		     "from countries " .
			 "left join projectforecasts on country_id = projectforecast_country_id ". 
			 " order by country_name";

	$res_c = mysql_query($sql_c) or dberror($sql_c);
	while ($row = mysql_fetch_assoc($res_c))
	{            
		$countries[$row["country_id"]] = $row["country_name"];
	}
}

/*
$sql = "select projectforecast_id, projectforecast_year, projectforecast_quantity, " . 
       "address_company, concat('Q', projectforecast_planned_quartal) as planned_q, " .
       "country_name, project_costtype_text,  projectkind_name, postype_name, " .
	   "projectforecast_country_id, " .
	   " concat(projectforecast_year, ' - ', country_name, ' - ', project_costtype_text, ' - ', projectkind_name) as group_head " . 
       "from projectforecasts " . 
       " left join addresses on address_id = projectforecast_address_id " . 
	   " left join countries on country_id = projectforecast_country_id " .
	   " left join project_costtypes on project_costtype_id = projectforecast_project_costtype_id ". 
	   " left join projectkinds on projectkind_id = projectforecast_projectkind_id " . 
	   " left join postypes on postype_id = projectforecast_postype_id";

*/

$sql = "select projectforecast_id, projectforecast_year, projectforecast_quantity, " . 
       "address_company, concat('Q', projectforecast_planned_quartal) as planned_q, " .
       "country_name, postype_name, " .
	   "projectforecast_country_id, " .
	   " concat(projectforecast_year, ' - ', country_name, ' - ', postype_name) as group_head " . 
       "from projectforecasts " . 
       " left join addresses on address_id = projectforecast_address_id " . 
	   " left join countries on country_id = projectforecast_country_id " .
	   " left join postypes on postype_id = projectforecast_postype_id";

$list_filter = "projectforecast_address_id > 0 " . 
			   " and projectforecast_country_id = " . $country;

if(param("year"))
{
	if(param("year") and param("year") > 0)
	{
		$year = param("year");
		$list_filter .= " and projectforecast_year = " . $year;
	}
}
else
{
	$year = date("Y") + 1;
	$list_filter .= " and projectforecast_year = " . $year;
}



//creat records for a country an resztrict access
if (has_access("can_view_only_his_project_forecasts")
   or has_access("can_edit_only_his_project_forecasts"))
{
	$list_filter .= " and projectforecast_address_id = " . $user['address'];

	//add empty records in case there are no empty records
	$sql_r = "select count(projectforecast_id) as num_recs " . 
		     "from projectforecasts " . 
		     "where projectforecast_address_id = " . $user['address'] . 
		     " and projectforecast_country_id = " . $country . 
		     " and projectforecast_year = " . $year;

	//echo $sql_r . "<br />";

	$res = mysql_query($sql_r) or dberror($sql_r);
	$row = mysql_fetch_assoc($res);
	if($row["num_recs"] == 0)
	{
		
		/*
		
		//create all ecords for corporate
		$project_kinds = array(1, 2, 3, 6);
		
		if($user_address["client_type"] >1) // no agents
		{
		
			foreach($project_kinds as $project_kind) // Legal types
			{
				for($pos_type=1;$pos_type<4;$pos_type++) // POS types
				{
					for($quartal=1;$quartal<5;$quartal++) // quartals
					{
						$fields = array();

						$values = array();
						$fields[] = "projectforecast_address_id";
						$values[] = dbquote($user['address']);

						$fields[] = "projectforecast_year";
						$values[] = $year;

						$fields[] = "projectforecast_country_id";
						$values[] = $country;

						$fields[] = "projectforecast_project_costtype_id";
						$values[] = 1;

						$fields[] = "projectforecast_projectkind_id";
						$values[] = $project_kind;

						$fields[] = "projectforecast_postype_id";
						$values[] = $pos_type;

						$fields[] = "projectforecast_planned_quartal";
						$values[] = $quartal;

						$fields[] = "user_created";
						$values[] =  dbquote(user_login());

						$fields[] = "date_created";
						$values[] = dbquote(date("Y-m-d H:i:s"));
						
						$sql_i = "insert into projectforecasts (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

						mysql_query($sql_i) or dberror($sql_i);
					}
				}
			}
		}


		//create all ecords for franchisee
		$project_kinds = array(1, 2, 6);
		$pos_types = array(1, 3);
		
		foreach($project_kinds as $project_kind) // Legal types
		{
			foreach($pos_types as $pos_type) // POS types
			{
				for($quartal=1;$quartal<5;$quartal++) // quartals
				{
					$fields = array();

					$values = array();
					$fields[] = "projectforecast_address_id";
					$values[] = dbquote($user['address']);

					$fields[] = "projectforecast_year";
					$values[] = $year;

					$fields[] = "projectforecast_country_id";
					$values[] = $country;

					$fields[] = "projectforecast_project_costtype_id";
					$values[] = 2;

					$fields[] = "projectforecast_projectkind_id";
					$values[] = $project_kind;

					$fields[] = "projectforecast_postype_id";
					$values[] = $pos_type;

					$fields[] = "projectforecast_planned_quartal";
					$values[] = $quartal;

					$fields[] = "user_created";
					$values[] =  dbquote(user_login());

					$fields[] = "date_created";
					$values[] = dbquote(date("Y-m-d H:i:s"));
					
					$sql_i = "insert into projectforecasts (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

					mysql_query($sql_i) or dberror($sql_i);
				}
			}
		}


		//create all ecords for other
		$project_kinds = array(1, 2, 6, 7);
		$pos_types = array(2);
		
		foreach($project_kinds as $project_kind) // Legal types
		{
			foreach($pos_types as $pos_type) // POS types
			{
				for($quartal=1;$quartal<5;$quartal++) // quartals
				{
					$fields = array();

					$values = array();
					$fields[] = "projectforecast_address_id";
					$values[] = dbquote($user['address']);

					$fields[] = "projectforecast_year";
					$values[] = $year;

					$fields[] = "projectforecast_country_id";
					$values[] = $country;

					$fields[] = "projectforecast_project_costtype_id";
					$values[] = 6;

					$fields[] = "projectforecast_projectkind_id";
					$values[] = $project_kind;

					$fields[] = "projectforecast_postype_id";
					$values[] = $pos_type;

					$fields[] = "projectforecast_planned_quartal";
					$values[] = $quartal;

					$fields[] = "user_created";
					$values[] =  dbquote(user_login());

					$fields[] = "date_created";
					$values[] = dbquote(date("Y-m-d H:i:s"));
					
					$sql_i = "insert into projectforecasts (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

					mysql_query($sql_i) or dberror($sql_i);
				}
			}
		}
		*/

		//create all ecords for corporate
		$project_kinds = array(1, 2, 3, 6);
		
		
		for($pos_type=1;$pos_type<4;$pos_type++) // POS types
		{
			for($quartal=1;$quartal<5;$quartal++) // quartals
			{
				$fields = array();

				$values = array();
				$fields[] = "projectforecast_address_id";
				$values[] = dbquote($user['address']);

				$fields[] = "projectforecast_year";
				$values[] = $year;

				$fields[] = "projectforecast_country_id";
				$values[] = $country;

				$fields[] = "projectforecast_postype_id";
				$values[] = $pos_type;

				$fields[] = "projectforecast_planned_quartal";
				$values[] = $quartal;

				$fields[] = "user_created";
				$values[] =  dbquote(user_login());

				$fields[] = "date_created";
				$values[] = dbquote(date("Y-m-d H:i:s"));
				
				$sql_i = "insert into projectforecasts (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

				mysql_query($sql_i) or dberror($sql_i);
			}
		}
	}
}


//create years for filter drop down
$years = array();
$sql_l = "select DISTINCT projectforecast_year " . 
		"from projectforecasts " . 
		"where projectforecast_year > 0 and " . $list_filter;
$res = mysql_query($sql_l) or dberror($sql_l);
while($row = mysql_fetch_assoc($res))
{
	$years[$row['projectforecast_year']] = $row['projectforecast_year'];
}


$tmp = date("Y") + 1;

if(!in_array($tmp-1, $years))
{
	$years[$tmp-1] = $tmp-1;
}
if(!in_array($tmp, $years))
{
	$years[$tmp] = $tmp;
}
if(!in_array($tmp+1, $years))
{
	$years[$tmp+1] = $tmp+1;
}

if (is_array($years)) {
	ksort($years);
}



        

//get values of edit columns
$fquantities = array();
$fremarks = array();

$sql_l = "select projectforecast_id, projectforecast_year, projectforecast_country_id, " . 
		 "projectforecast_project_costtype_id, projectforecast_projectkind_id, projectforecast_postype_id, " . 
		 "projectforecast_planned_quartal, projectforecast_quantity, projectforecast_comment " . 
		 "from projectforecasts " . 
		 " where " . $list_filter;
$res = mysql_query($sql_l) or dberror($sql_l);
while($row = mysql_fetch_assoc($res))
{
	$fquantities[$row['projectforecast_id']] = $row['projectforecast_quantity'];
	$fremarks[$row['projectforecast_id']] = $row['projectforecast_comment'];
}

/********************************************************************
    create list for all entries
*********************************************************************/
$list = new ListView($sql);
$list->set_entity("projectforecasts");
$list->set_filter($list_filter);
//$list->set_order("projectforecast_year, projectforecast_country_id, projectforecast_project_costtype_id, projectforecast_projectkind_id, projectforecast_postype_id, projectforecast_planned_quartal");
$list->set_order("projectforecast_year, projectforecast_country_id, projectforecast_postype_id, projectforecast_planned_quartal");
$list->set_group("group_head");


$list->add_listfilters("country", "Country", 'select', $countries, $country);
$list->add_listfilters("year", "Year", 'select', $years, $year);


if (has_access("can_view_project_forecasts")
   or has_access("can_edit_project_forecasts"))
{
	$list->add_column("address_company", "Company", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK);
}


$list->add_column("projectforecast_year", "Year", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK);
$list->add_column("country_name", "Country", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK);
//$list->add_column("project_costtype_text", "Legal Type", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK);
//$list->add_column("projectkind_name", "Project Type", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK);
$list->add_column("postype_name", "POS Type", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK);
$list->add_column("planned_q", "Planned in", "", "", "", COLUMN_NO_WRAP | COLUMN_BREAK);

$list->add_edit_column("projectforecast_quantity", "Number of POS", 12, 0, $fquantities);
$list->add_edit_column("projectforecast_comment", "Remark", 40, 0, $fremarks);

if(has_access("can_edit_only_his_project_forecasts"))
{
	$list->add_button("save", "Save List");
}
elseif(has_access("can_edit_project_forecasts") )
{
	if(param("country"))
	{
		$list->add_button("save", "Save List");
	}
}

if(array_key_exists($country, $countries))
{
	$link = "javascript:popup('project_forecasts_xls.php?year=" . $year . "&country=" . $country . "',640,480);";
	$list->add_button("print", "Print List  for " . $countries[$country], $link);
}
if(count($countries) > 1)
{
	$link = "javascript:popup('project_forecasts_xls.php?year=" . $year . "',640,480);";
	$list->add_button("print_all", "Print List for all Countries", $link);
}

$list->populate();
$list->process();



$form = new Form("projectforecasts", "projectforecasts");

if(has_access("can_edit_only_his_project_forecasts"))
{
	$form->add_button("save", "Save List");
}
elseif(has_access("can_edit_project_forecasts") )
{
	if(param("country"))
	{
		$form->add_button("save", "Save List");
	}
}





$form->populate();
$form->process();

if($list->button("save") or $form->button("save"))
{

	foreach($list->values("projectforecast_quantity") as $key => $value)
	{
		$fields = array();

		$fields[] = "projectforecast_quantity = " . dbquote($value);

		$tmp = $list->values("projectforecast_comment");
		$value = dbquote($tmp[$key]);
		$fields[] = "projectforecast_comment = " . $value;

						
		$sql = "update projectforecasts set " . join(", ", $fields) . " where projectforecast_id = " . $key;
		mysql_query($sql) or dberror($sql);

	}
}



$page = new Page("projects");


if (has_access("can_create_new_projects"))
{
    $page->register_action('new', 'Project Request', "project_new_01.php");
}

if (has_access("can_view_only_his_project_forecasts")
   or has_access("can_view_project_forecasts")
   or has_access("can_edit_only_his_project_forecasts")
   or has_access("can_edit_project_forecasts"))
{
    $page->register_action('forecasts', 'Project Forecast ', "project_forecasts.php");
}

if (has_access("can_create_new_orders"))
{
   //$page->register_action('new', 'New Order', "catalog.php");
}
if (has_access("can_view_projects"))
{
   $page->register_action('projects', 'Projects', "projects.php");
}


$page->header();
$page->title("Project Forecast");
$list->render();

echo "<br />";
$form->render();

$page->footer();

?>
