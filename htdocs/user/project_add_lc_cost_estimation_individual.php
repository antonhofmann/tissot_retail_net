<?php
/********************************************************************
    project_add_lc_cost_estimation_individual.php

    Add individual local construction cost positions to an order

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-07
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-07
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("has_access_to_list_of_materials_in_projects");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

$exchange_rate_info = get_order_currency($project["project_order"]);

$currencies = array();
$currencies[1] = "CHF";
$currencies[2] = $exchange_rate_info["symbol"];

/********************************************************************
    build form
*********************************************************************/
$form = new Form("order_items", "order_item");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid",$project["project_order"]);
$form->add_hidden("order_item_id", id());
$form->add_hidden("order_item_order", $project["project_order"]);
$form->add_hidden("order_item_type", ITEM_TYPE_LOCALCONSTRUCTIONCOST);

require_once "include/project_head_small.php";

$form->add_section("Item Information");
$form->add_multiline("order_item_text", "Description*", 4, NOTNULL);

$form->add_edit("order_item_supplier_freetext", "Supplier Info");

if(array_key_exists("cg", $_GET) and $_GET["cg"] > 0 and $_GET["cg"] < 12)
{
	$sql = "select project_cost_groupname_id, project_cost_groupname_name " . 
		   "from project_cost_groupnames " . 
		   "where project_cost_groupname_id = " . $_GET["cg"] . 
		   " and project_cost_groupname_active = 1 order by project_cost_groupname_id";
	$form->add_list("order_item_cost_group", "Cost Group",$sql, NOTNULL, $_GET["cg"]);
}
else
{
	$sql = "select project_cost_groupname_id, project_cost_groupname_name " . 
		   "from project_cost_groupnames " . 
		   "where project_cost_groupname_active = 1 order by project_cost_groupname_id";
	$form->add_list("order_item_cost_group", "Cost Group",$sql, NOTNULL);
}

$form->add_list("currency", "Currency*",$currencies, NOTNULL);
$form->add_edit("price", "Price*", NOTNULL);

$form->add_hidden("order_item_system_price", 0);

$form->add_button("save_data", "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


if ($form->button("save_data"))
{
    if ($form->validate())
    {
        if($form->value("currency") == 1) // CHF
		{
			$chf_value = $form->value("price");
			$loc_value = $chf_value / $exchange_rate_info["exchange_rate"]*$exchange_rate_info["factor"];

			$form->value("order_item_system_price", $chf_value);
		}
		else // local currency
		{
			$loc_value = $form->value("price");
			$chf_value = $exchange_rate_info["exchange_rate"]*$loc_value/$exchange_rate_info["factor"];
			$form->value("order_item_system_price", $chf_value);
		
		}

		project_add_special_item_save($form);
        $link = "project_edit_material_list.php?pid=" . param("pid"); 
        redirect($link);
    }
}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();

if(array_key_exists("cg", $_GET) and $_GET["cg"] > 0 and $_GET["cg"] < 12)
{
	if($_GET["cg"] == 7)
	{
		$page->title("Add Local Construction");
	}
}
else
{
	$page->title("Add Individual Cost");
}
$form->render();

require_once "include/project_footer_logistic_state.php";
$page->footer();


?>