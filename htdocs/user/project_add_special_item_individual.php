<?php 
/******************************************************************** 
 
    project_add_special_item_individual.php 
 
    Add special item positions to an order 
 
    Created by:     Anton Hofmann (aho@mediaparx.ch) 
    Date created:   2002-09-13 
    Modified by:    Anton Hofmann (aho@mediaparx.ch) 
    Date modified:  2003-02-16 
    Version:        1.0.0 
 
    Copyright (c) 2002, Swatch AG, All Rights Reserved. 
 
*********************************************************************/ 
 
require_once "../include/frame.php"; 
require_once "include/get_functions.php"; 
require_once "include/save_functions.php"; 
 
check_access("has_access_to_list_of_materials_in_projects"); 
 
/******************************************************************** 
    prepare all data needed 
*********************************************************************/ 
 
// read project and order details 
$project = get_project(param("pid")); 
 
// get company's address 
$client_address = get_address($project["order_client_address"]); 
 
// create sql for the supplier listbox 
$sql_suppliers = "select address_id, address_company ". 
                 "from addresses ". 
                 "where address_type = 2 and address_active = 1 " . 
                 "order by address_company"; 

$supplier_currency_symbol = "";
if(param("order_item_supplier_address"))
{
	$supplier_currency = get_address_currency(param("order_item_supplier_address"));
	$supplier_currency_symbol = $supplier_currency["symbol"];
}


$sql_cg = "select project_cost_groupname_id, project_cost_groupname_name " . 
       "from project_cost_groupnames " . 
	   "where project_cost_groupname_active = 1 and project_cost_groupname_id in (2, 10) order by project_cost_groupname_id";

 
//$sql_cmg = "select costmonitoringgroup_id, costmonitoringgroup_text from costmonitoringgroups order by costmonitoringgroup_text";


$sql_forwarders = "select address_id, address_company ".
                  "from addresses ".
				  "where (address_type = 3 or address_id = " . $project["order_client_address"] . ") and address_active = 1 ".
                  "order by address_company";


// create sql for the transportation type listbox
$sql_transportation_types = "select transportation_type_id, transportation_type_name ".
                           "from transportation_types ".
                           "order by transportation_type_name";



//get supplier currencies
if(param("order_item_supplier_address") > 0)
{
	$supplier_currencies = get_suplier_currencies(param("order_item_supplier_address"));

	$sql_supplier_currencies = "select currency_id, currency_symbol " . 
							   " from currencies " . 
							   " where currency_id in ( " . implode(',', $supplier_currencies) . ") " . 
							   " order by currency_symbol";
}
else
{
	$sql_supplier_currencies = "select currency_id, currency_symbol " . 
							   " from currencies " . 
							   " where currency_id = 0";
}

if(id() == 0)
{
	$order_item["supplier_currency"] = "";
}

if(!isset($supplier_currency))
{
	$supplier_currency["exchange_rate"] = 0;
	$supplier_currency["exchange_rate_furniture"] = 0;
}


$sql_units = "select unit_id, unit_name " . 
             " from units " . 
			 " order by unit_name";

$sql_packaging_types = "select packaging_type_id, packaging_type_name " . 
             " from packaging_types " . 
			 " order by packaging_type_name";


$sql_item_categories = "select item_category_id, item_category_name	" . 
                       " from item_categories " . 
					   " where item_category_active = 1 " .
					   " order by item_category_name";

/******************************************************************** 
    build form 
*********************************************************************/ 
$form = new Form("order_items", "order_item"); 
 
$form->add_hidden("pid", param("pid")); 
$form->add_hidden("oid",$project["project_order"]); 
$form->add_hidden("order_item_id", id()); 
$form->add_hidden("order_item_order", $project["project_order"]); 
$form->add_hidden("order_item_type", ITEM_TYPE_SPECIAL); 


require_once "include/project_head_small.php"; 
 
$form->add_section("Item Information"); 
$form->add_list("order_item_supplier_address", "Supplier*", $sql_suppliers, SUBMIT | NOTNULL); 

$form->add_list("order_item_category", "Item Category*", $sql_item_categories, NOTNULL);

$form->add_multiline("order_item_text", "Description*", 4, NOTNULL); 


$form->add_section("Quantity and Price");
$form->add_edit("order_item_quantity", "Quantity*", NOTNULL);
$form->add_list("suppliers_currency", "Currency*",$sql_supplier_currencies, NOTNULL);


if($supplier_currency["exchange_rate_furniture"] > 0)
{
	$form->add_edit("order_item_supplier_exchange_rate", "Exchange Rate*", NOTNULL, $supplier_currency["exchange_rate_furniture"]);
}
else
{
	$form->add_edit("order_item_supplier_exchange_rate", "Exchange Rate*", NOTNULL, $supplier_currency["exchange_rate"]);
}
$form->add_edit("order_item_supplier_price", "Supplier's Price ");

$form->add_hidden("order_item_system_price", 0); 


$form->add_section("Supplying Options");
$form->add_checkbox("order_item_no_offer_required", "does not require a supplier's offer", "", 0, "Options");
$form->add_checkbox("order_item_only_quantity_proposal", "does only need a supplier's quantity proposal", "");


$form->add_section("Cost Monitoring");
$form->add_list("order_item_cost_group", "Cost Group*",$sql_cg, NOTNULL);
//$form->add_list("order_items_costmonitoring_group", "Type*",$sql_cmg, NOTNULL);

$form->add_section("Accounting Information");
$form->add_edit("order_item_cost_unit_number", "Cost Unit Number");
$form->add_edit("order_item_po_number", "P.O. Number");


$form->add_section("Volumes and Weight");
$form->add_list("order_item_unit_id", "Unit",$sql_units, 0);
$form->add_edit("order_item_width", "Width in cm", 0, "" , TYPE_DECIMAL, 10, 2);
$form->add_edit("order_item_height", "Height in cm", 0, "" , TYPE_DECIMAL, 10, 2);
$form->add_edit("order_item_length", "length in cm", 0, "" , TYPE_DECIMAL, 10, 2);
$form->add_edit("order_item_gross_weight", "Gross Weight in kg", 0, "" , TYPE_DECIMAL, 10, 2);
$form->add_list("order_item_packaging_type_id", "Packaging",$sql_packaging_types, 0);
$form->add_checkbox("order_item_stackable", "", "", 0, "Stackable");

$form->add_section("Traffic Information");
$form->add_list("order_item_transportation", "Transportation Type", $sql_transportation_types);
$form->add_list("order_item_forwarder_address", "Forwarder", $sql_forwarders);
$form->add_edit("order_item_staff_for_discharge", "Staff for Discharge", 0, "", TYPE_INT, 1);

$form->add_section("Budgeting Information");
$form->add_checkbox("order_item_not_in_budget", "does not appear in budget", "", "", "Project Budget");

$form->add_section("Project Sheet Budget Request");
$form->add_checkbox("order_item_exclude_from_ps", "local production", "", "", "Project Sheet");


$form->add_hidden("order_item_system_price", 0); 

 
 
$form->add_button("save_data", "Save"); 
 
$form->add_button(FORM_BUTTON_BACK, "Back"); 
 
 
/******************************************************************** 
    Populate form and process button clicks 
*********************************************************************/  
$form->populate(); 
$form->process(); 


if ($form->button("save_data")) 
{ 
    if ($form->validate()) 
    { 
		$item_id = project_add_special_item_save($form); 
		$link = "project_edit_material_list.php?pid=" . param("pid");
        redirect($link); 
    } 
}
elseif($form->button("order_item_supplier_address"))
{
	$form->value("suppliers_currency",$supplier_currency["id"]);
	if($supplier_currency["exchange_rate_furniture"] > 0)
	{
		$form->value("order_item_supplier_exchange_rate",$supplier_currency["exchange_rate_furniture"]);
	}
	else
	{
		$form->value("order_item_supplier_exchange_rate",$supplier_currency["exchange_rate"]);
	}
}
 
    
/******************************************************************** 
    render page 
*********************************************************************/ 
$page = new Page("projects"); 
 
require "include/project_page_actions.php"; 
 
$page->header(); 
$page->title("Add Individual Special Item"); 
$form->render(); 

require_once "include/project_footer_logistic_state.php";
$page->footer(); 
 
?>
