<?php
/********************************************************************

    project_design_briefing_pdf.php

    Print Design Briefing

    Created by:     Anton Hofmann (anton.hofmann@codingroom.ch)
    Date created:   2017-12-20
    Modified by:    Anton Hofmann (anton.hofmann@codingroom.ch)
    Date modified:  2017-12-20
    Version:        1.0.0

    Copyright (c) 2017, Tissot, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "../shared/func_posindex.php";
require_once "include/get_functions.php";
require_once "../shared/project_cost_functions.php";


if(!has_access("can_view_design_briefing") and !has_access("can_edit_design_briefing") ) {
	redirect("noaccess.php");
}

if(!array_key_exists('pid', $_GET)) {
	redirect("noaccess.php");
}

$project_id = $_GET['pid'];
$attach_files = false;
if(array_key_exists('files', $_GET) and $_GET['files'] == 1) {
	$attach_files = true;
}

/********************************************************************
    prepare all data needed
*********************************************************************/

$project = get_project($project_id);
$client_currency = get_client_currency($project['project_order']);

if($project["pipeline"] == 0)
{
	$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");

	$sql = "select posareatype_name " . 
		   "from posareas " .
		   "left join posareatypes on posareatype_id = posarea_area " .
		   "where posarea_posaddress = " . dbquote($pos_data["posaddress_id"]) . 
		   " order by posarea_area";
}
elseif($project["pipeline"] == 1)
{
	$pos_data = get_poslocation_from_pipeline($project["posaddress_id"], $project["order_id"]);

	$sql = "select posareatype_name  
		   from posareaspipeline
		   left join posareatypes on posareatype_id = posarea_area 
		   where posarea_posaddress = " . dbquote($pos_data["posaddress_id"]) . 
		   " order by posarea_area";
}

//get posareas
$posareas = '';
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$pos_posareas[]	= $row["posareatype_name"];
}
if(count($pos_posareas) > 0 ){
	$posareas = implode(', ', $pos_posareas);
}

//get floor
if($posareas) {
	$posareas = $posareas . ", " . $project["project_floor"];
}
else {
	$posareas = $project["project_floor"];
}

//POS Basic Data
$poslocation = $pos_data["country_name"];
$poslocation .= ", " . $pos_data["posaddress_name"];
$poslocation .= ", " . $pos_data["place_name"];

$poslegaltype = $project["project_costtype_text"];
	
if($project["project_popup_name"])
{
	$projectkind = $project["projectkind_name"] . ", " . $project["project_popup_name"];
}
else
{
	$projectkind = $project["projectkind_name"];
}

$projectkind_id = $project["project_projectkind"];


if(($project["project_projectkind"] == 6 or $project["project_projectkind"] == 9)
	and $project["project_relocated_posaddress_id"] > 0) // relocation
{
	$relocated_pos = get_relocated_pos_info($project["project_relocated_posaddress_id"]);
	$projectkind .= " of " . $relocated_pos["posaddress_name"];

}

//surfaces

$sql_m =  "select * from project_costs " .
        "where project_cost_order = " . dbquote($project["project_order"]);

$res_m = mysql_query($sql_m) or dberror($sql_m);
$row_m = mysql_fetch_assoc($res_m);

$posgrosssqm = $row_m["project_cost_grosssqms"] ? number_format($row_m["project_cost_grosssqms"], 2, ".", "'") : "";
$postotalsqm = $row_m["project_cost_totalsqms"] ? number_format($row_m["project_cost_totalsqms"], 2, ".", "'") : "";
$posretailsqm = $row_m["project_cost_sqms"] ? number_format($row_m["project_cost_sqms"], 2, ".", "'") : "";
$posofficesqm = $row_m["project_cost_backofficesqms"] ? number_format($row_m["project_cost_backofficesqms"], 2, ".", "'") : "";

/*
$posnumfloors = $row_m["project_cost_numfloors"] ? number_format($row_m["project_cost_numfloors"], 0, ".", "'") : "";
$posfloor1sqm = $row_m["project_cost_floorsurface1"] ? number_format($row_m["project_cost_floorsurface1"], 2, ".", "'") : "";
$posfloor2sqm = $row_m["project_cost_floorsurface2"] ? number_format($row_m["project_cost_floorsurface2"], 2, ".", "'") : "";
$posfloor3sqm = $row_m["project_cost_floorsurface3"] ? number_format($row_m["project_cost_floorsurface3"], 2, ".", "'") : "";
*/


$plannedopeningdate = to_system_date($project["project_planned_opening_date"]);
$realisticopeningdate = to_system_date($project["project_real_opening_date"]);


$postype = $project["postype_name"];
$possubclass = $project["possubclass_name"];

if($project["productline_subclass_name"])
{
	$posfurniture = $project["product_line_name"] . " / " . $project["productline_subclass_name"];
}
else
{
	$posfurniture = $project["product_line_name"];
}



$project_info = $poslegaltype . " / " . $projectkind . " / " . $postype;

if($possubclass) {
	$project_info .= " / " . $possubclass;
}

$project_info .= " / " . $posfurniture;


//lease starting date
$leasestartingdate = 'n.a.';
$posleases = get_pos_leasedata($pos_data["posaddress_id"], $project["project_order"]);
if(count($posleases) > 0) {
	$leasestartingdate = to_system_date($posleases['poslease_startdate']);
}

$constructionstartingdate = 'not yet available';
if($project['project_construction_startdate'] != NULL
	and substr($project['project_construction_startdate'], 0, 4) != '0000') {
		$constructionstartingdate = to_system_date($project['project_construction_startdate']);
}


//get design briefing data
$submission_date = 'not yet submitted';
$contact_name = '';
$contact_business_title = '';
$contact_email = '';
$contact_phone = '';
$project_owner_id = $project["order_user"];

$sql = "select *
	   from project_design_briefs 
	   where project_design_brief_project_id = " . dbquote($project_id);

$res = mysql_query($sql);
if($row = mysql_fetch_assoc($res))
{
	$submission_date = to_system_date($row['project_design_brief_submission_date']);
	
	if($row['project_design_brief_resubmitted_by'] > 0) {
		$project_owner_id = $row["project_design_brief_resubmitted_by"];
	}
	elseif($row['project_design_brief_submitted_by'] > 0) {
		$project_owner_id = $row["project_design_brief_submitted_by"];
	}
	
}

//get contact details
$sql = "select *
	   from users 
	   where user_id = " . dbquote($project_owner_id);
$res = mysql_query($sql);
if($row = mysql_fetch_assoc($res))
{
	$contact_name = $row['user_name'] . " " . $row['user_firstname'];
	$contact_business_title = $row['user_description'];
	$contact_email = $row['user_email'];
	if($row['user_mobile_phone'] and $row['user_phone']) {
		$contact_phone = $row['user_phone'] . "/" . $row['user_mobile_phone'];
	}
	elseif($row['user_phone']) {
		$contact_phone = $row['user_phone'];
	}
	elseif($row['user_mobile_phone']) {
		$contact_phone = $row['user_phone'];
	}
}

//get approval dates
$ln_approval = 'not yet approved';
$cer_approval = 'not yet approved';

$sql = "select project_milestone_milestone, project_milestone_date
        from project_milestones
		where project_milestone_milestone in (13, 12, 21)
		and project_milestone_project = " . dbquote($project_id);

$res = mysql_query($sql);
while($row = mysql_fetch_assoc($res))
{

	if($row['project_milestone_milestone'] == 13
		and $row['project_milestone_date'] != NULL
		and substr($row['project_milestone_date'], 0, 4) != '0000') {
			$ln_approval = to_system_date($row['project_milestone_date']);
	}
	elseif($row['project_milestone_milestone'] == 12
		and $row['project_milestone_date'] != NULL
		and substr($row['project_milestone_date'], 0, 4) != '0000') {
			$cer_approval = to_system_date($row['project_milestone_date']);
	}
	elseif($row['project_milestone_milestone'] == 21
		and $row['project_milestone_date'] != NULL
		and substr($row['project_milestone_date'], 0, 4) != '0000') {
			$cer_approval = to_system_date($row['project_milestone_date']);
	}
}


/********************************************************************
	get budget data from cost sheet
*********************************************************************/
$budget = get_project_budget_totals($project_id);


/********************************************************************
	prepare pdf
*********************************************************************/
global $page_title;
$page_title = "DESIGN BRIEFING: Project " . $project['project_number'];


require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');
require_once('../include/SetaPDF/Autoload.php');


class MYPDF extends TCPDF
{
	//Page header
	function Header()
	{
		global $page_title;
		//Logo
		$this->Image('../pictures/brand_logo.jpg',10,8,33);
		//arialn bold 15
		$this->SetFont('arialn','B',12);
		//Move to the right
		$this->Cell(80);
		//Title
		$this->Cell(0,33,$page_title,0,0,'R');
		//Line break
		$this->Ln(20);

	}

	//Page footer
	function Footer()
	{
		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		//arialn italic 8
		$this->SetFont('arialn','I',8);
		//Page number
		$this->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$this->PageNo().'/'.$this->getAliasNbPages(),0,0,'R');
	}

	function keepTogether($height) {
		$this->checkPageBreak($height);
	}

}

//Instanciation of inherited class
$pdf = new MYPDF("P", "mm", "A4", true, 'UTF-8', false);

$pdf->SetAutoPageBreak(true);

$pdf->SetMargins(10, 23, 10);

$pdf->Open();

$pdf->SetFillColor(244, 244, 244);
$pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(120, 120, 120)));

$pdf->AddFont('arialn');
$pdf->AddFont('arialn', 'B');

$pdf->AddPage();
$new_page = 0;



$pdf->SetFont('arialn','B',10);
$pdf->Cell(190,7,'CONTACT INFORMATION',1, 0, 'L', 1);
$pdf->Ln();

$pdf->SetFont('arialn','B',8);
$pdf->Cell(32, 5,"Contact Person's Name",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(63, 5,$contact_name,1, 0, 'L', 0);

$pdf->SetFont('arialn','B',8);
$pdf->Cell(32, 5,"Email",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(63, 5,$contact_email,1, 0, 'L', 0);
$pdf->Ln();

$pdf->SetFont('arialn','B',8);
$pdf->Cell(32, 5,"Business Title",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(63, 5,$contact_business_title,1, 0, 'L', 0);

$pdf->SetFont('arialn','B',8);
$pdf->Cell(32, 5,"Phone",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(63, 5,$contact_phone,1, 0, 'L', 0);
$pdf->Ln();

$pdf->SetFont('arialn','B',8);
$pdf->Cell(32, 5,"Client",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(63, 5,$pos_data['address_company'],1, 0, 'L', 0);

$pdf->SetFont('arialn','B',8);
$pdf->Cell(32, 5,"Submission Date",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(63, 5,$submission_date,1, 0, 'L', 0);
$pdf->Ln();
$pdf->Ln();



$pdf->SetFont('arialn','B',10);
$pdf->Cell(190,7,'POS Location',1, 0, 'L', 1);
$pdf->Ln();

$pdf->SetFont('arialn','B',8);
$pdf->Cell(32, 5,"POS Name",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(63, 5,$pos_data["posaddress_name"],1, 0, 'L', 0);

$pdf->SetFont('arialn','B',8);
$pdf->Cell(32, 5,"POS Name 2",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(63, 5,$pos_data["posaddress_name2"],1, 0, 'L', 0);
$pdf->Ln();

$pdf->SetFont('arialn','B',8);
$pdf->Cell(32, 5,"Street/Street number",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(158, 5,$pos_data["posaddress_address"],1, 0, 'L', 0);
$pdf->Ln();
if($pos_data["posaddress_address2"]) {
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(32, 5,"Additional Address Info",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(158, 5,$pos_data["posaddress_address2"],1, 0, 'L', 0);
	$pdf->Ln();
}

$pdf->SetFont('arialn','B',8);
$pdf->Cell(32, 5,"City",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(63, 5,trim($pos_data["posaddress_zip"] . " " . $pos_data["place_name"]),1, 0, 'L', 0);

$pdf->SetFont('arialn','B',8);
$pdf->Cell(32, 5,"Country",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(63, 5,$pos_data["country_name"],1, 0, 'L', 0);
$pdf->Ln();

$pdf->SetFont('arialn','B',8);
$pdf->Cell(32, 5,"Environment",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(158, 5,$posareas,1, 0, 'L', 0);
$pdf->Ln();
$pdf->Ln();


$pdf->SetFont('arialn','B',10);
$pdf->Cell(190,7,'PROJECT',1, 0, 'L', 1);
$pdf->Ln();


$pdf->SetFont('arialn','B',8);
$pdf->Cell(32, 5,"Project",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(158,5,$project["project_number"] . " / " . $poslocation,1, 0, 'L', 0);
$pdf->Ln();

$pdf->SetFont('arialn','B',8);
$pdf->Cell(32, 5,"Project Info",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(158, 5,$project_info,1, 0, 'L', 0);
$pdf->Ln();

$pdf->SetFont('arialn','B',8);
$pdf->Cell(32, 5,"LN Approval Date",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(63, 5,$ln_approval,1, 0, 'L', 0);

$pdf->SetFont('arialn','B',8);
$pdf->Cell(32, 5,"AF/CER Approval Date",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(63, 5,$cer_approval,1, 0, 'L', 0);
$pdf->Ln();

$pdf->SetFont('arialn','B',8);
$pdf->Cell(32, 5,"Agreed Opening Date",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(63, 5,$realisticopeningdate,1, 0, 'L', 0);

$pdf->SetFont('arialn','B',8);
$pdf->Cell(32, 5,"Lease Starting Date",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(63, 5,$leasestartingdate,1, 0, 'L', 0);
$pdf->Ln();

$pdf->SetFont('arialn','B',9);
$pdf->Cell(190, 5,"Surfaces in Square Meters",1, 0, 'L', 1);
$pdf->Ln();
$pdf->SetFont('arialn','B',8);
$pdf->Cell(32, 5,"Total",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(63, 5,$postotalsqm,1, 0, 'L', 0);

$pdf->SetFont('arialn','B',8);
$pdf->Cell(32, 5,"Retail",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(63, 5,$posretailsqm,1, 0, 'L', 0);
$pdf->Ln();

$pdf->SetFont('arialn','B',8);
$pdf->Cell(32, 5,"Office",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(63, 5,$posofficesqm,1, 0, 'L', 0);

$pdf->SetFont('arialn','B',8);
$pdf->Cell(32, 5,"1st Floor",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(63, 5,$posfloor1sqm,1, 0, 'L', 0);
$pdf->Ln();

$pdf->SetFont('arialn','B',8);
$pdf->Cell(32, 5,"2nd Floor",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(63, 5,$posfloor2sqm,1, 0, 'L', 0);

$pdf->SetFont('arialn','B',8);
$pdf->Cell(32, 5,"3rd Floor",1, 0, 'L', 0);
$pdf->SetFont('arialn','',8);
$pdf->Cell(63, 5,$posfloor3sqm,1, 0, 'L', 0);
$pdf->Ln();

//project costs

$amount = $client_currency['exchange_rate']*$budget["budget_total"]/$client_currency['factor'];

$pdf->SetFont('arialn','B',9);
$pdf->Cell(190, 5,"Project Budget in CHF: " . number_format($amount, 2, '.', "'"),1, 0, 'L', 1);
$pdf->Ln();

$counter = 1;
$sql = "select DISTINCT costsheet_pcost_group_id, left(pcost_group_name, 55) as pcost_group_name, 
	   concat(pcost_group_code, ' ', pcost_group_name) as costgroup 
	   from costsheets 
	   left join pcost_groups on pcost_group_id = costsheet_pcost_group_id 
	   where costsheet_version = 0
			and costsheet_project_id = " . dbquote($project_id);
		               
$res = mysql_query($sql);
while($row = mysql_fetch_assoc($res))
{
	$amount = $client_currency['exchange_rate']*$budget["group_totals"][$row['costsheet_pcost_group_id']]/$client_currency['factor'];

	$amount = number_format($amount, 2, '.', "'");

	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(72, 5,$row['pcost_group_name'],1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(23, 5,$amount,1, 0, 'L', 0);

	if ($counter & 1) {
	
	}
	else {
		$pdf->Ln();
	}
	$counter++;
}
$pdf->Ln();


//print design briefing for the project

//01 prepare layout
$columns = array();

$group = '';
$sql = "select * from project_design_briefs
	   left join project_design_brief_positions on project_design_brief_position_design_brief_id = project_design_brief_id
       left join design_brief_groups on design_brief_group_id = project_design_brief_position_group_id
	   left join design_brief_positions on design_brief_position_id = project_design_brief_position_position_id 
	   where project_design_brief_position_content <> ''
	   and  project_design_brief_position_content is not null
	   and project_design_brief_project_id = " . dbquote($project_id) .
	   " order by design_brief_group_sort, design_brief_position_sort";

$res = mysql_query($sql);
while($row = mysql_fetch_assoc($res))
{
	$columns[$row['project_design_brief_position_id']] = 1;
	if($row['project_design_brief_position_type'] != 5
		and strlen($row["design_brief_position_text"]) < 60) {
		$columns[$row['project_design_brief_position_id']] = 2;
	}
	
}


// 02 generate output

$pdf->SetFont('arialn','B',10);
$pdf->Cell(190,7,'DESIGN OBJECTIVES',1, 0, 'L', 1);

$group = '';
$sql = "select * from project_design_briefs
	   left join project_design_brief_positions on project_design_brief_position_design_brief_id = project_design_brief_id
       left join design_brief_groups on design_brief_group_id = project_design_brief_position_group_id
	   left join design_brief_positions on design_brief_position_id = project_design_brief_position_position_id 
	   where project_design_brief_position_content <> ''
	   and project_design_brief_position_content is not null
	   and project_design_brief_project_id = " . dbquote($project_id) .
	   " order by design_brief_group_sort, design_brief_position_sort";

$res = mysql_query($sql);
while($row = mysql_fetch_assoc($res))
{	   
	if($pdf->getY() > 260) {
		$pdf->AddPage();
	}
	if($row['project_design_brief_position_type'] == 5) {
		$number_of_lines = 1+(strlen($row["project_design_brief_position_content"]) / 150);
		$y = $pdf->getY();
		if(ceil(($y+$number_of_lines)) > 255)
		{
			$pdf->AddPage();
		}
	}
	
	if($group != $row["project_design_brief_position_group_id"]) {
		$group = $row["project_design_brief_position_group_id"];

		$pdf->Ln();
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(190,5,$row["design_brief_group_sort"] . " " . $row["design_brief_group_name"],1, 0, 'L', 1);
		$pdf->Ln();
	}

	if($columns[$row['project_design_brief_position_id']] == 2) {		
		$pdf->SetFont('arialn','B',8);
		$pdf->Cell(75, 4,sprintf('%02d',$row["design_brief_position_sort"]) . " " . $row["design_brief_position_text"],1, 0, 'L', 0);
		$pdf->SetFont('arialn','',8);

		

		switch ($row['project_design_brief_position_type']) {
			case 1:
				$pdf->Cell(115, 4,$row["project_design_brief_position_content"],1, 0, 'L', 0);
				break;
			case 2:
				$pdf->Cell(115, 4,$row["project_design_brief_position_content"],1, 0, 'L', 0);
				break;
			case 3:
				$pdf->Cell(115, 4,$row["project_design_brief_position_content"],1, 0, 'L', 0);
				break;
			case 4:
				$pdf->Cell(115, 4,from_system_date($row["project_design_brief_position_content"]),1, 0, 'L', 0);
				break;
			case 5:
				//$pdf->MultiCell(190, 0,  $row["project_design_brief_position_content"], 1, '', 0, 0, '', '', false, 0, false, false);
				$lineHeight = $pdf->getStringHeight(0, $row["project_design_brief_position_content"], false, true, '', 1);
				
				$pdf->keepTogether($lineHeight);

				$pdf->MultiCell(115, 0,  $row["project_design_brief_position_content"], 1, 'L', 0, 1);
				$pdf->Cell(190, 0,'',0, 0, 'L', 0);
				break;
			case 6:
				$selection = explode("\r\n", $row['project_design_brief_position_values']);
				
				if(array_key_exists($row["project_design_brief_position_content"], $selection)) {
					$pdf->Cell(115, 4,$selection[$row["project_design_brief_position_content"]],1, 0, 'L', 0);
				}
				else{
					$pdf->Cell(115, 4,"",1, 0, 'L', 0);
				}
				break;
			case 7:
				$selection = explode("\r\n", $row['project_design_brief_position_values']);
				$content = json_decode($row['project_design_brief_position_content'], true);

				$tmp = array();
				foreach($content as $key=>$value) {
					if($value == 1) {
						$tmp[] = $selection[$key];
					}
				}
				
				$pdf->Cell(115, 4,implode(', ', $tmp),1, 0, 'L', 0);
				break;
		}
	}
	else {
		$pdf->SetFont('arialn','B',8);
		$pdf->MultiCell(190,4, sprintf('%02d',$row["design_brief_position_sort"]) . " " . $row["design_brief_position_text"], 1);
		$pdf->SetFont('arialn','',8);

		switch ($row['project_design_brief_position_type']) {
			case 1:
				$pdf->Cell(190, 4,$row["project_design_brief_position_content"],1, 0, 'L', 0);
				break;
			case 2:
				$pdf->Cell(190, 4,$row["project_design_brief_position_content"],1, 0, 'L', 0);
				break;
			case 3:
				$pdf->Cell(190, 4,$row["project_design_brief_position_content"],1, 0, 'L', 0);
				break;
			case 4:
				$pdf->Cell(190, 4,from_system_date($row["project_design_brief_position_content"]),1, 0, 'L', 0);
				break;
			case 5:
				$pdf->MultiCell(190, 0,  $row["project_design_brief_position_content"] . $_y, 1, 'L', 0, 1);
				$pdf->Cell(190, 0,'',0, 0, 'L', 0);
				$pdf->SetY($pdf->GetY()-3.5);
				break;
			case 6:
				$pdf->Cell(190, 4,$row["project_design_brief_position_content"],1, 0, 'L', 0);
				break;
			case 7:
				$yes_no = array();
				$yes_no[1] = 'yes';
				$yes_no[2] = 'no';
				if(array_key_exists($row["project_design_brief_position_content"], $yes_no)) {
					$pdf->Cell(190, 4,$yes_no[$row["project_design_brief_position_content"]],1, 0, 'L', 0);
				}
				else {
					$pdf->Cell(190, 4,"",1, 0, 'L', 0);
				}
				break;
			case 8:
				$selection = explode("\r\n", $row['project_design_brief_position_values']);
				if(array_key_exists($row["project_design_brief_position_content"], $selection)) {
					$pdf->Cell(190, 4,$selection[$row["project_design_brief_position_content"]],1, 0, 'L', 0);
				}
				else{
					$pdf->Cell(190, 4,"",1, 0, 'L', 0);
				}
				break;
		}
	}
	
	$pdf->Ln();
}


if($attach_files == true) {
	$pdf->SetFont('arialn','B',8);

	//design briefing documents
	$files = array();
	$sql = "select distinct order_file_id, order_file_title, order_file_path
		  from order_files  
		  where order_file_category in (28, 29, 30) 
		  and order_file_order = " . dbquote($project["project_order"]) . 
		  " order by order_file_type";
	$res = mysql_query($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$source_file = ".." . $row["order_file_path"];

		if(file_exists($source_file))
		{
			//PDF
			if(substr($source_file, strlen($source_file)-3, 3) == "pdf" 
				or substr($source_file, strlen($source_file)-3, 3) == "PDF")
			{
				$pdfString = $pdf->output('', 'S');
				$tmp = SetaPDF_Core_Document::loadByString($pdfString);
				
				$merger = new SetaPDF_Merger();
				$merger->addDocument($tmp);

				$merger->addFile(array(
					'filename' => $source_file,
					'copyLayers' => true
				));


				$pdf = new TCPDF("P", "mm", "A4", true, 'UTF-8', false);
				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
				$pdf->SetMargins(10, 23, 10);
				$pdf->Open();

				$PDFmerger_was_used = true;


			}
			elseif(substr($source_file, strlen($source_file)-3, 3) == "jpg" 
				or substr($source_file, strlen($source_file)-3, 3) == "JPG"
				 or substr($pix2, strlen($pix2)-3, 3) == "jpeg"
				or substr($pix2, strlen($pix2)-3, 3) == "JPEG")
			{
				$pdf->AddPage("P");

				
				$image_info = getimagesize ($source_file);
				
				$pdf->Cell(190, 4,$row["order_file_title"],1, 0, 'L', 0);
				$pdf->Ln();
				
				if($image_info[0] >= $image_info[1]) {
					$pdf->Image($source_file,10,30, 190, 0, "", "", "", true, 300, "", false, false, 0, false, false,true);
				}
				else {
					$pdf->Image($source_file,10,30, 0, 250, "", "", "", true, 300, "", false, false, 0, false, false,true);
				}
			}
		}
		
	}
	
	if($PDFmerger_was_used == true) {
		
		/*
		$pdfString = $pdf->output('', 'S');
		$tmp = SetaPDF_Core_Document::loadByString($pdfString);
		$merger->addDocument($tmp);
		*/
		
		$merger->merge();
		$document = $merger->getDocument();

		$document->setWriter(new SetaPDF_Core_Writer_Http($file_name, true));
		$document->save()->finish();
	}


	

}

$file_name = "Project_design_briefing_" . $project["order_number"];
$pdf->Output($file_name);



?>