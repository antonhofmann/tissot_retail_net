<?php
/********************************************************************

    project_task_history.php

    Shows a history of steps of the actual project

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2005-11-26
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2005-11-26
    Version:        1.0.0

    Copyright (c) 2005, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("has_access_to_order_status_report_in_projects");

register_param("pid");


/********************************************************************
    prepare all data needed
*********************************************************************/
// read order details
$project = get_project(param("pid"));
$order_id = $project["project_order"];


// get company's address
$client_address = get_address($project["order_client_address"]);

/********************************************************************
    history data
*********************************************************************/

$sql_hi = "select DISTINCT order_state_code, order_state_action_name, " .
          "order_state_group_code, order_state_group_name, " .
          "concat(users.user_name, ' ', users.user_firstname) as fullname, " .
          "concat(tousers.user_name, ' ', tousers.user_firstname) as tofullname, " .
          "DATE_FORMAT(actual_order_states.date_created,'%d.%m.%y %H:%i') as performed " .
          "from order_state_groups " .
          "left join order_states on order_state_group = order_state_group_id " . 
		  "left join actual_order_states on actual_order_state_order = " . $order_id . " and actual_order_state_state = order_state_id " .
          "left join users on users.user_id = actual_order_state_user " .
          "left join order_mails on order_mail_order = " . $order_id . " " .
          "and order_mail_order_state = order_state_id " .
          "left join users as tousers on tousers.user_id = order_mail_user " .
          "left join notification_recipients on notification_recipient_id = order_state_notification_recipient ";

$filter_hi = "order_state_group_order_type = 1 " .
             "and users.user_name <> '' ";




$sql_hi = "SELECT order_mails.order_mail_id, 
			order_states.order_state_code,
			order_state_group_code,
			order_states.order_state_action_name,
			order_state_group_code, 
			order_state_group_name,
			concat(order_state_code, ' ', order_state_action_name) as group_head, 
			from_users.user_id as sender,
			to_users.user_id as recipient,
			DATE_FORMAT(
					order_mails.date_created,
					'%d.%m.%y %H:%i'
				) AS performed,
			order_mails.order_mail_text,
			order_mails.order_mail_order_state,
			order_mails.order_mail_is_cc
			from order_mails 
			INNER JOIN users AS to_users ON to_users.user_id = order_mail_user
			INNER JOIN users AS from_users ON from_users.user_id = order_mail_from_user
			INNER JOIN order_states ON order_mail_order_state = order_state_id 
			INNER JOIN order_state_groups ON  order_state_group_id = order_state_group";


$filter_hi = "order_mail_order = " . dbquote($order_id) .  
 			 "and order_mail_order_state > 0
			 and order_mail_is_cc = 0
			 and order_state_code < 900
			 and to_users.user_id <> from_users.user_id";


//get addresses
$senders = array();
$recipients = array();
$sql = $sql_hi . " where " . $filter_hi;
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$sql_a = "select concat(address_company, ': ', user_name, ' ', user_firstname) as company, " .
             " concat(user_name, ' ', user_firstname) as username " . 
		     " from users " . 
		     " left join addresses on address_id = user_address " . 
		     " where user_id = " . dbquote($row["sender"]);
	$res_a = mysql_query($sql_a) or dberror($sql_a);
	if ($row_a = mysql_fetch_assoc($res_a))
	{
		$senders[$row["order_mail_id"]] = $row_a["company"];
	}

	$sql_a = "select concat(address_company, ': ', user_name, ' ', user_firstname) as company, " .
		     " concat(user_name, ' ', user_firstname) as username " .
		     " from users " . 
		     " left join addresses on address_id = user_address " . 
		     " where user_id = " . dbquote($row["recipient"]);
	$res_a = mysql_query($sql_a) or dberror($sql_a);
	if ($row_a = mysql_fetch_assoc($res_a))
	{
		$recipients[$row["order_mail_id"]] = $row_a["company"];
	}

}



/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project_status_report", 640);
$form->add_hidden("pid", param("pid"));


require_once "include/project_head_small.php";


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Create History
*********************************************************************/ 
$history = new ListView($sql_hi, LIST_HAS_HEADER);

$history->set_title("History");
$history->set_entity("order_state");
$history->set_filter($filter_hi);

/*
$history->set_order("order_state_code, order_mails.date_created ASC");
$history->set_group("order_state_group_code", "order_state_group_name");

$history->add_column("order_state_code", "Step", "", "", "", COLUMN_NO_WRAP);
$history->add_column("order_state_action_name", "Action", "", "", "", COLUMN_NO_WRAP);
$history->add_column("performed", "Date", "", "", "", COLUMN_NO_WRAP);

$history->add_text_column("sender", "Performed by", 0 , $senders);
$history->add_text_column("recipient", "Recipient", 0, $recipients);
*/


$history->set_order("order_state_code, order_mails.date_created ASC");
$history->set_group("group_head");

//$history->add_column("order_state_code", "Step", "", "", "", COLUMN_NO_WRAP);
//$history->add_column("order_state_action_name", "Action", "", "", "", COLUMN_NO_WRAP);
$history->add_column("performed", "Date", "", "", "", COLUMN_NO_WRAP);

$history->add_text_column("sender", "Performed by", 0 , $senders);
$history->add_text_column("recipient", "Recipient", 0, $recipients);

$history->populate();
$history->process();

/********************************************************************
    render page
*********************************************************************/
$page = new Popup_Page("orders");

$page->header();
$page->title("Task History of Project - " . $project["order_number"]);

   
$form->render();

$history->render();

echo "<p>&nbsp;</p>";

require_once "include/project_footer_logistic_state.php";
$page->footer();

?>