<?php
/********************************************************************

    catalog_category.php

    Entry page for the catalog section group.

    Created by:     Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2002-08-20
    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date modified:  2003-04-29
    Version:        1.2.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_functions.php";

check_access("can_view_catalog");

$cid = param('cid');
$plid = param('plid');


$searchterm = '';
if(array_key_exists('searchterm', $_GET) and $_GET['searchterm'] != '') 
{
	$searchterm = $_GET['searchterm'];
}
else
{
	$searchterm = param('searchterm');
}


/********************************************************************
    prepare all data needed
*********************************************************************/
// get the region of users' address
$user_region = get_user_region(user_id());
$roles = get_user_roles(user_id());
$user = get_user(user_id());
$user_country = $user["country"];
$currency = get_address_currency($user["address"]);


// get category name
if($searchterm)
{
	$category_name = "Item List matching '" . $searchterm . "'";
}
else
{
	$category_name = get_category_name($plid, $cid, param("scid"));
}


$sql = "select DISTINCT item_id, item_category_id, item_category_name, item_code, item_name,item_price, ".
       " concat(address_company, ' - ', item_category_name) as group_head, item_visible, " .
	   "supplier_item_price, currency_symbol, supplier_address " . 
       " from product_lines  " .
	   "INNER JOIN product_line_supplying_groups ON product_line_supplying_group_line_id = product_line_id
	    INNER JOIN item_supplying_groups ON item_supplying_group_supplying_group_id = product_line_supplying_group_group_id
		INNER JOIN items ON item_id = item_supplying_group_item_id
		INNER JOIN supplying_groups on supplying_group_id = item_supplying_group_supplying_group_id 
		INNER JOIN productline_regions ON productline_region_productline = product_line_id
		INNER JOIN item_categories ON item_category_id = item_category
		LEFT JOIN item_subcategories on item_subcategory_id = item_subcategory
		INNER JOIN item_countries ON item_country_item_id = item_id 
		LEFT join item_pos_types on item_pos_type_item = item_id 
		LEFT JOIN suppliers on supplier_item = item_id
	    LEFT JOIN addresses on address_id = supplier_address  
		LEFT JOIN currencies on currency_id = supplier_item_currency";



if(has_access("can_edit_catalog") or has_access("can_enter_orders_for_other_companies"))
{
	
    if($searchterm)
	{
		$list_filter = "(REPLACE(LOWER(item_code), ' ', '') like '%" . strtolower($searchterm) . "%' OR REPLACE(LOWER(item_name), ' ', '') like '%" .  strtolower($searchterm) . "%' OR REPLACE(LOWER(item_category_name), ' ', '') like '%" .  strtolower($searchterm) . "%' OR REPLACE(LOWER(product_line_name), ' ', '') like '%" .  strtolower($searchterm) . "%' )";


		$list_filter .= " and (product_line_clients = 1 
						 or product_line_budget = 1)
						 and product_line_active = 1
						 and item_category_active = 1 
						 and item_active = 1
						  and item_type = 1";
	}
	else
	{
		$list_filter = " item_category = " . dbquote($cid) .
					   " and product_line_id = " . dbquote($plid) .
					   " and (product_line_clients = 1 
						 or product_line_budget = 1)
						 and product_line_active = 1
						 and item_category_active = 1 
						 and item_active = 1
						  and item_type = 1";
	}
}
else
{
	
	if($searchterm)
	{
		
		$list_filter = "(REPLACE(LOWER(item_code), ' ', '') like '%" . strtolower($searchterm) . "%' OR REPLACE(LOWER(item_name), ' ', '') like '%" .  strtolower($searchterm) . "%' OR REPLACE(LOWER(item_category_name), ' ', '') like '%" .  strtolower($searchterm) . "%' OR REPLACE(LOWER(product_line_name), ' ', '') like '%" .  strtolower($searchterm) . "%' )";

		$list_filter .= " and product_line_active = 1
						 and item_category_active = 1
						 and (item_visible = 1 or item_visible_in_projects) 
						 and item_active = 1
						 and item_type = 1 
						 and product_line_clients=1 
						 and item_country_country_id = " . dbquote($user_country);
	}
	else
	{
		$list_filter = " item_category = " . dbquote($cid) .
					    " and product_line_id = " . dbquote($plid) .
						" and product_line_active = 1
						 and item_category_active = 1
						 and (item_visible = 1 or item_visible_in_projects) 
						 and item_active = 1
						 and item_type = 1 
						 and product_line_clients=1 
						 and item_country_country_id = " . dbquote($user_country);
	}
}


if(param("scid")) {
	$list_filter .= ' and item_subcategory = ' . dbquote(param("scid"));
}
elseif(!$searchterm)
{
	$list_filter .= ' and (item_subcategory is null or item_subcategory = 0)';
}

if(param("sg")) {
	$list_filter .= ' and item_supplying_group_supplying_group_id = ' . dbquote(param("sg"));
}


$prices = array();
$prices_chf = array();
$shopping_list_links = array();
$tmp_sql = $sql . " where " . $list_filter;


$res = mysql_query($tmp_sql) or dberror($tmp_sql);
while ($row = mysql_fetch_assoc($res))
{
	//$prices[$row["item_id"]] = number_format(round((($row["supplier_item_price"] / $currency["exchange_rate"]) * $currency["factor"]),2), 2);

	$prices[$row["item_id"]] = number_format($row["supplier_item_price"], 2) . " " . $row["currency_symbol"];

	
	$supplier_currency = get_address_currency($row["supplier_address"]);
	$price_chf = $row["supplier_item_price"] * $supplier_currency["exchange_rate"] / $supplier_currency["factor"];
	$prices_client[$row["item_id"]] = number_format(round((($price_chf / $currency["exchange_rate"]) * $currency["factor"]),2), 2) . " " . $currency["symbol"];

	if(has_access("can_edit_catalog") or has_access("can_enter_orders_for_other_companies"))
	{
		$link = '<a title="Add item to shopping cart" href="order_category.php?action=basket_add&item_id=' . $row["item_id"]. '&plid=' . $plid. '&cid=' . $row["item_category_id"] .'"><i style="font-size:16px;color:#2fc100;" class="fa fa-shopping-cart" aria-hidden="true"></i></a>';
		$shopping_list_links[$row["item_id"]] = $link;
	}
	elseif($row["item_visible"] == 1)
	{
		$link = '<a title="Add item to shopping cart" href="order_category.php?action=basket_add&item_id=' . $row["item_id"]. '&plid=' . $plid. '&cid=' . $row["item_category_id"] .'"><i style="font-size:16px;color:#2fc100;" class="fa fa-shopping-cart" aria-hidden="true"></a>';
		$shopping_list_links[$row["item_id"]] = $link;
	}
	else
	{
		$shopping_list_links[$row["item_id"]] = " - ";
	}
}

/********************************************************************
	Create List
*********************************************************************/ 
$list = new ListView($sql);

$list->set_entity("items");
$list->set_filter($list_filter);
$list->set_order("item_code");

$list->set_title('<span style="font-weight:normal;color:#f4a11e;">Click the green shopping cart icon to add the item to your shopping list.</span>');

$list->add_hidden('cid' , $cid);
$list->add_hidden('plid' , $plid);
$list->add_hidden('searchterm' , $searchterm);




$list->add_column("item_code", "Code", "/applications/templates/item.info.modal.php?id={item_id}", LIST_FILTER_FREE, "", "", "", array('class'=>'item_info_box'));

if (has_access("can_create_new_orders"))
{
	$list->add_text_column("item_id", "", COLUMN_UNDERSTAND_HTML | COLUMN_ALIGN_CENTER, $shopping_list_links);
}

$list->add_column("item_name", "Item", "", LIST_FILTER_FREE);


$list->add_text_column("price", "Supplier's Price", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $prices);
$list->add_text_column("price_client", "Your Price", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $prices_client);


$list->add_button(LIST_BUTTON_BACK, "Back");




$entityValue = http_build_query(array(
	'frame' => true,
	'nofilters' => true,
	'entity' => 'catalog_category',
	'entity_values' => array(
		'searchterm' => $searchterm,
		'cid' => $cid,
		'plid' => $plid,
		'scid' => param("scid"),
		'sg' => param("sg")
	)
));

$list->add_button(LIST_BUTTON_LINK, "Print Product Info Sheets", array(
	'href' => '/applications/templates/item.booklet.php?'.$entityValue,
	'data-fancybox-type' => 'iframe'
));


$_print_link = 'print_price_list_xls.php' .
		 '?plid=' . param('plid') . '&cid=' . param('cid') . '&scid=0&searchterm=' . $searchterm;

$_print_link = "javascript:popup('" . $_print_link . "');";

$list->add_button("print_price_list", "Print Price List", $_print_link);

/********************************************************************
	Populate list and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();


$page = new Page("catalog");

$page->register_action('home', 'Home', "welcome.php");


if (has_access("can_create_new_orders"))
{
   if (!basket_is_empty())
    {
        $page->register_action('basket', 'Shopping List', "basket.php");
    }
}

if (has_access("can_create_new_orders"))
{
    //$page->register_action('new', 'New Order', "catalog.php");
    
}

$page->header();

if($searchterm) {
	$page->title('Search Results for "' . $searchterm . '"');
}
else {
	$page->title($category_name);
}
$list->render();

$page->footer();


?>