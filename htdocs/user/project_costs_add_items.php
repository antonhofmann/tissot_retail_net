<?php
/********************************************************************

    project_costs_add_items.php

    Add catalog items to costsheet

    Created by:     Anton Hofmann (anton.hofmann@codingroom.ch)
    Date created:   2017-12-28
    Modified by:    Anton Hofmann (anton.hofmann@codingroom.ch)
    Date modified:  2017-12-28
    Version:        1.0.0

    Copyright (c) 2017, Tissot, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../include/page_modal.php";
require_once "include/get_functions.php";


if (has_access('can_edit_project_costs_budget')
	or has_access('can_edit_project_cost_real_costs')
    or has_access('can_edit_project_costs_bids')
   )
{
}
else {
	redirect("noaccess.php");
}


if(!param("pid") and !param("mode"))
{
	$link = "noaccess.php";
	redirect($link);
}


/********************************************************************
    prepare data
*********************************************************************/

$project = get_project(param("pid"));
$client_address = get_address($project["order_client_address"]);

$order_currency = get_order_currency($project["project_order"]);


/********************************************************************
    save data
*********************************************************************/
if(param("save_form") and $_POST["mode"] == 'bid')
{


}
elseif(param("save_form"))
{
	$selected_items = array();
	foreach($_POST as $key=>$value) {
	
		
		if($key != 'item_category'
		   and strpos($key, 'item_') === 0) {
			
			$item_id = str_replace('item_', '', $key);
			$selected_items[$item_id] = $_POST['cost_sub_group_' . $item_id];
		}
	
	}

	foreach($selected_items as $item_id=>$data) {
		if($data) {
			$tmp = explode('-', $data);
			$group_id = $tmp[0];
			$subgroup_id = $tmp[1];

			$sql = "select item_code, item_name, concat(item_code, ': ', item_name) as iname,
					address_company, supplier_item_price, supplier_item_currency, currency_exchange_rate, currency_factor
					from items
			       LEFT JOIN suppliers on supplier_item = item_id
				   LEFT JOIN addresses on address_id = supplier_address 
				   LEFT JOIN currencies on currency_id = supplier_item_currency
				   where item_id = " . dbquote($item_id);


			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				 
				 if(!$row["supplier_item_price"]) {
					$row['supplier_item_currency'] = $order_currency['id'];
					$row["currency_exchange_rate"] = $order_currency['exchange_rate'];
				 }

				 $fields = array();
				 $values = array();

				 $fields[] = "costsheet_project_id";
				 $values[] = dbquote($_POST["pid"]);

				 $fields[] = "costsheet_version";
				 $values[] = 0;
				 
				 $fields[] = "costsheet_pcost_group_id";
				 $values[] = dbquote($group_id);

				 $fields[] = "costsheet_pcost_subgroup_id";
				 $values[] = dbquote($subgroup_id);

				 $fields[] = "costsheet_item_id";
				 $values[] = dbquote($item_id);

				 $fields[] = "costsheet_currency_id";
				 $values[] = dbquote($row['supplier_item_currency']);

				 $fields[] = "costsheet_exchangerate";
				 $values[] = dbquote($row["currency_exchange_rate"]);

				 $fields[] = "costsheet_code";
				 $values[] = dbquote($row['item_code']);

				 $fields[] = "costsheet_text";
				 $values[] = dbquote($row['item_name']);

				 $fields[] = "costsheet_budget_amount";
				 $values[] = dbquote($row["supplier_item_price"]);
				 

				 $fields[] = "costsheet_company";
				 $values[] = dbquote($row['address_company']);

				 if($_POST["mode"] == 'cms')
				 {
					$fields[] = "costsheet_is_in_cms";
					$values[] = 1;
				 }
				 else
				 {
					$fields[] = "costsheet_is_in_budget";
					$values[] = 1;
				 }
				 
				 $fields[] = "date_created";
				 $values[] = dbquote(date("yyyy-mm-dd H:i:s"));

				 $fields[] = "user_created";
				 $values[] = dbquote(user_login());

				 $sql = "insert into costsheets (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";

				 $result = mysql_query($sql) or dberror($sql);
			
			
			}
		
		}
	}
}


/********************************************************************
    prepare filter queries
*********************************************************************/
$sql_categories = "select DISTINCT item_category_id, item_category_name
        from product_lines 
	    INNER JOIN product_line_supplying_groups ON product_line_supplying_group_line_id = product_line_id
	    INNER JOIN item_supplying_groups ON item_supplying_group_supplying_group_id = product_line_supplying_group_group_id
		INNER JOIN items ON item_id = item_supplying_group_item_id
		INNER JOIN supplying_groups on supplying_group_id = item_supplying_group_supplying_group_id 
		INNER JOIN productline_regions ON productline_region_productline = product_line_id
		INNER JOIN item_categories ON item_category_id = item_category
		LEFT JOIN item_subcategories on item_subcategory_id = item_subcategory
		INNER JOIN item_countries ON item_country_item_id = item_id 
		LEFT join item_pos_types on item_pos_type_item = item_id 
		where product_line_id = " . dbquote($project["project_product_line"]) .
	    " and item_category_active = 1 
		 and item_active = 1
		 and item_visible_in_projects = 1
		 and item_type IN (1, 2) 
		and item_country_country_id = " . dbquote($project["order_shop_address_country"]) . 
		" order by item_category_name";



/********************************************************************
    render form
*********************************************************************/
$form = new Form("projects", "projects");
$form->add_hidden("save_form", "1");
$form->add_hidden("costsheet_project_id", param("pid"));
$form->add_hidden("bid", param("bid"));
$form->add_hidden("pid", param("pid"));
$form->add_hidden("mode", param("mode"));


$form->add_comment('Please select a category:<br /><br />');
$form->add_list("item_category", "Category*", $sql_categories, 0);

$form->add_comment('<div id="item_selection"></div>');


$page = new Page_Modal("projects");
$page->header();

$page->title("Add Items from Catalog");
$form->render();

if(param("save_form") and param("mode") == 'cms')
{
?>
	<script languege="javascript">
		var back_link = "project_costs_real_costs.php?pid=<?php echo param("pid")?>";
		$.nyroModalRemove();
	</script>
<?php
}
elseif(param("save_form") and param("mode") == 'bid')
{
?>
	<script languege="javascript">
		var back_link = "project_costs_bid.php?id=<?php echo param("bid");?>&pid=<?php echo param("pid")?>";
		$.nyroModalRemove();
	</script>
<?php
}
elseif(param("save_form"))
{
?>
	<script languege="javascript">
		var back_link = "project_costs_budget.php?pid=<?php echo param("pid")?>";
		$.nyroModalRemove();
	</script>
<?php
}
?>
<script language="javascript">

jQuery(document).ready(function($) {
	
	$(document).on("change","#item_category",function(){
	
		var params = "pid=<?php echo param('pid');?>&id=" + $(this).val() + '&action=save';
		$.ajax({
			type: "POST",
			data: params,
			url: "include/ajx_cost_sheet_get_available_items.php",
			success: function(msg){

				if(msg != 'error') {
					$('#item_selection').html(msg);
				}
			}
		});
	
	});

});

</script>

<?php

$page->footer();

?>