<?php
/********************************************************************

    project_send_message_confirm.php

    Confirmation for an Email beeing sent.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-10-01
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2002-10-01
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");
$form->add_hidden("pid", param("pid"));


$form->add_label("num_mails", "Number of Mails Sent", 0, param("num_mails"));
$form->add_label("num_tasks", "Number of Tasks Assigned", 0, param("num_tasks"));

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Confirmation of Transmission");

echo "<p>", "Your email has been submitted.", "</p>";
$form->render();

$page->footer();

?>