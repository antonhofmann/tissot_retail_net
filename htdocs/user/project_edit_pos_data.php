<?php
/********************************************************************

    project_edit_pos_data.php

    Edit POS Data

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-02-18
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-02-18
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "../shared/func_posindex.php";

if (has_access("can_edit_pos_data") or has_access("can_edit_pos_calendar_data"))
{
}
else
{
	check_access("can_edit_pos_data");
	//redirect("noaccess.php");
}


/********************************************************************
    prepare all data needed
*********************************************************************/
$user_roles = get_user_roles(user_id());

// read project and order details
$project = get_project(param("pid"));



// get company's address
$client_address = get_address($project["order_client_address"]);
$franchisee_address = get_address($project["order_franchisee_address_id"]);

//apply condition if client is owner company and client is an agent
$use_condition_for_agents = 0;
if($client_address["client_type"] == 1 
	and $project["order_client_address"] == $project["order_franchisee_address_id"])
{
	$use_condition_for_agents = 1;
}

$standard_dsitribution_channel = get_standard_distribution_channel($project["project_cost_type"], $project["project_postype"], $project["project_pos_subclass"], $use_condition_for_agents);

$tracking_info = array();
$sql = "select projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time, " . 
	   "concat(user_name, ' ', user_firstname) as user_name " . 
	   "from projecttracking " . 
       "left join users on user_id = projecttracking_user_id " . 
       "where projecttracking_project_id = " . param("pid") . 
	   " and projecttracking_field = 'project_real_opening_date' " . 
	   " order by projecttracking_time";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$tracking_info[] = array("projecttracking_oldvalue"=>$row["projecttracking_oldvalue"],
		"projecttracking_newvalue"=>$row["projecttracking_newvalue"],
		"projecttracking_comment"=>$row["projecttracking_comment"],
		"projecttracking_time"=>$row["projecttracking_time"],
		"user_name"=>$row["user_name"]
		);
}




if(count($franchisee_address) == 0)
{
	$franchisee_address["place_id"] = 0;
}

// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";

//get addresses from pos index
if(($project["project_projectkind"] == 1 
	or $project["project_projectkind"] == 6
	or $project["project_projectkind"] == 9) and $project["pipeline"] == 1) //POS is in pipeline and it is a new POS
{
	$sql_posaddresses = "select posaddress_id, concat(posaddress_place, ', ', posaddress_name) as posaddress " .
						"from posaddressespipeline " . 
						"where posaddress_country = " . $project["order_shop_address_country"] . 
						" order by posaddress_place, posaddress_name";
}
else
{
	$sql_posaddresses = "select posaddress_id, concat(posaddress_place, ', ', posaddress_name) as posaddress " .
						"from posaddresses " . 
						"where posaddress_country = " . $project["order_shop_address_country"] . 
						" order by posaddress_place, posaddress_name";
}


//get pos data
$table = "posaddresses";
$table2 = "posareas";

if($project["pipeline"] == 0)
{
	$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");
	$pos_order_sql = "select * from posorders where posorder_order = " . $project["order_id"];
}
elseif($project["pipeline"] == 1)
{
	$pos_data = get_poslocation_from_pipeline($project["posaddress_id"], $project["order_id"]);
	$table = "posaddressespipeline";
	$table2 = "posareaspipeline";

	$pos_order_sql = "select * from posorderspipeline where posorder_order = " . $project["order_id"];
}

//neighbourhood
$neighbourhoods = array();
$neighbourhoods_business_types = array();
$neighbourhoods_price_ranges = array();
$res = mysql_query($pos_order_sql) or dberror($pos_order_sql);
if ($row = mysql_fetch_assoc($res))
{
	$neighbourhoods["Shop on Left Side"] = $row["posorder_neighbour_left"];
	$neighbourhoods["Shop on Right Side"] = $row["posorder_neighbour_right"];
	$neighbourhoods["Shop Across Left Side"] = $row["posorder_neighbour_acrleft"];
	$neighbourhoods["Shop Across Right Side"] = $row["posorder_neighbour_acrright"];
	$neighbourhoods["Other Brands in Area"] = $row["posorder_neighbour_brands"];

	$neighbourhoods_business_types["Shop on Left Side"] = $row["posorder_neighbour_left_business_type"];
	$neighbourhoods_business_types["Shop on Right Side"] = $row["posorder_neighbour_right_business_type"];
	$neighbourhoods_business_types["Shop Across Left Side"] = $row["posorder_neighbour_acrleft_business_type"];
	$neighbourhoods_business_types["Shop Across Right Side"] = $row["posorder_neighbour_acrright_business_type"];

	$neighbourhoods_price_ranges["Shop on Left Side"] = $row["posorder_neighbour_left_price_range"];
	$neighbourhoods_price_ranges["Shop on Right Side"] = $row["posorder_neighbour_right_price_range"];
	$neighbourhoods_price_ranges["Shop Across Left Side"] = $row["posorder_neighbour_acrleft_price_range"];
	$neighbourhoods_price_ranges["Shop Across Right Side"] = $row["posorder_neighbour_acrright_price_range"];
}	

//posareas
$posareas = array();
$sql = "select posareatype_id, posareatype_name " . 
	   "from posareatypes " . 
	   " order by posareatype_name";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$posareas[$row["posareatype_id"]]	= $row["posareatype_name"];
}

$pos_posareas = array();

if(count($pos_data) > 0)
{
	$sql = "select posarea_area " . 
		   "from $table2 " . 
		   "where posarea_posaddress = " . dbquote($pos_data["posaddress_id"]) . 
		   " order by posarea_area";

	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$pos_posareas[$row["posarea_area"]]	= $row["posarea_area"];
	}
}

if($project["project_projectkind"] == 7) //Equip independent retailer with Tissot furniture
{
	$sql_franchisees = "select address_id, concat(address_company, ', ', address_place) as name " . 
					   "from addresses " . 
					   "where (address_active = 1 " .
					   "and (address_active = 1 and (address_is_independent_retailer = 1 or address_can_own_independent_retailers = 1)  " . 
					   "and (address_parent = " . $project["order_client_address"] .
					   " or address_id = " . $project["order_client_address"] . "))) " .  
		               " or address_id = " . $project["order_franchisee_address_id"] .
					   " order by address_company";
}
elseif($project["project_cost_type"] == 6) //Equip independent retailer with Tissot furniture
{
	$sql_franchisees = "select address_id, concat(address_company, ', ', address_place) as name " . 
					   "from addresses " . 
					   "where address_type = 7 and address_active = 1 " .
					   "and (address_canbefranchisee = 1  " . 
					   "and (address_parent = " . $project["order_client_address"] . 
					   " or address_id = " . $project["order_client_address"] . 
					   "  or address_canbefranchisee_worldwide = 1)) " . 
		                " or (address_active = 1 and (address_is_independent_retailer = 1 or address_can_own_independent_retailers = 1) and (address_id = " . $project["order_franchisee_address_id"] . " or address_id = " . $project["order_client_address"] . ")) " . 
					   " order by address_company";
	
}
else
{
	$sql_franchisees = "select address_id, concat(address_company, ', ', address_place) as name " . 
					   "from addresses " . 
					   "where address_active = 1 " .
					   "and (address_canbefranchisee = 1  " . 
					   "and (address_parent = " . $project["order_client_address"] . 
					   " or address_id = " . $project["order_client_address"] . 
					   "  or address_canbefranchisee_worldwide = 1)) " . 
		                " or (address_active = 1 and (address_is_independent_retailer = 1 or address_can_own_independent_retailers = 1) and (address_id = " . $project["order_franchisee_address_id"] . " or address_id = " . $project["order_client_address"] . ")) " . 
					   " order by address_company";

}


//get former franchisee address
$former_franchisee_address = array();
if($project["project_projectkind"] == 4 
	or $project["project_projectkind"] == 3 
	or $project["project_projectkind"] == 9)
{
	
	$sql = "select order_franchisee_address_id " . 
		   "from posorders " . 
		   "left join orders on order_id = posorder_order " .
		   "where posorder_type = 1 and posorder_project_kind in (1, 2, 9) " . 
		   " and posorder_posaddress = " . $project["posaddress_id"] . 
		   " and order_actual_order_state_code = '890' " . 
		   " order by posorder_id DESC";	

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$former_franchisee_address = get_address($row["order_franchisee_address_id"]);

	}
	elseif($project["project_projectkind"] == 9) {
		$relocated_pos = get_relocated_pos_info($project["project_relocated_posaddress_id"]);
		
		if(count($relocated_pos) > 0) {
			$former_franchisee_address = get_address($relocated_pos['posaddress_franchisee_id']);
		}
	}
}

$ratings = array();
$ratings[5] = "bad";
$ratings[4] = "poor";
$ratings[3] = "good";
$ratings[2] = "very good";
$ratings[1] = "excellent";

$units = array();
$units[1] = "Square Feet (sqf)";
$units[2] = "Square Meters (sqm)";


//get business types and price ranges

$businesstypes = array();
$sql = "select businesstype_id, businesstype_text " . 
	   "from businesstypes " . 
	   " order by businesstype_text";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$businesstypes[$row["businesstype_id"]]	= $row["businesstype_text"];
}
//$businesstypes["---"]	= "---------------------------------";
//$businesstypes[999999]	= "Other not listed above";

$priceranges = array();
$sql = "select pricerange_id, pricerange_text " . 
	   "from priceranges " . 
	   " order by pricerange_text";
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$priceranges[$row["pricerange_id"]]	= $row["pricerange_text"];
}


/********************************************************************
    Get Cost Monitoring Sheet Budget Data
*********************************************************************/ 
$sql =  "select * from project_costs " .
        "where project_cost_order = " . $project["project_order"];

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

$project_cost_sqms = $row["project_cost_sqms"];
$project_cost_original_sqms = $row["project_cost_original_sqms"];
$project_cost_grosssqms = $row["project_cost_grosssqms"];
$project_cost_totalsqms = $row["project_cost_totalsqms"];
$project_cost_bakoffocesqms = $row["project_cost_backofficesqms"];
$project_cost_numfloors = $row["project_cost_numfloors"];
$project_cost_floorsurface1 = $row["project_cost_floorsurface1"];
$project_cost_floorsurface2 = $row["project_cost_floorsurface2"];
$project_cost_floorsurface3 = $row["project_cost_floorsurface3"];

//get total surface from posorders in case it is zero
if(!$project_cost_grosssqms and array_key_exists("posaddress_store_grosssurface", $pos_data))
{
	$project_cost_grosssqms = $pos_data["posaddress_store_grosssurface"];
}
if(!$project_cost_totalsqms and array_key_exists("posaddress_store_totalsurface", $pos_data))
{
	$project_cost_totalsqms = $pos_data["posaddress_store_totalsurface"];
}
if(!$project_cost_bakoffocesqms and array_key_exists("posaddress_store_backoffice", $pos_data))
{
	$project_cost_bakoffocesqms = $pos_data["posaddress_store_backoffice"];
}
if(!$project_cost_numfloors and array_key_exists("posaddress_store_numfloors", $pos_data))
{
	$project_cost_numfloors = $pos_data["posaddress_store_numfloors"];
}
if(!$project_cost_floorsurface1 and array_key_exists("posaddress_store_floorsurface1", $pos_data))
{
	$project_cost_floorsurface1 = $pos_data["posaddress_store_floorsurface1"];
}
if(!$project_cost_floorsurface2 and array_key_exists("posaddress_store_floorsurface2", $pos_data))
{
	$project_cost_floorsurface2 = $pos_data["posaddress_store_floorsurface2"];
}
if(!$project_cost_floorsurface3 and array_key_exists("posaddress_store_floorsurface3", $pos_data))
{
	$project_cost_floorsurface3 = $pos_data["posaddress_store_floorsurface3"];
}


//sales
$sql_distribution_channels = "select mps_distchannel_id, concat(mps_distchannel_group_name, ' - ', mps_distchannel_name , ' - ', mps_distchannel_code) as channel " . 
"from mps_distchannels " . 
"left join mps_distchannel_groups on mps_distchannel_groups.mps_distchannel_group_id = mps_distchannels.mps_distchannel_group_id " . 
"where mps_distchannel_active = 1 or mps_distchannel_id = " . dbquote($project["project_distribution_channel"]) .
" order by mps_distchannel_group_name, mps_distchannel_name, mps_distchannel_code ";



/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid", $project["project_order"]);
$form->add_hidden("pipeline", $project["pipeline"]);
$form->add_hidden("posaddress_is_flagship", $project["project_is_flagship"]);


//Project Head
require_once "include/project_head_small.php";



if($project["project_cost_type"] == 1 
	and ($project["project_projectkind"] == 1
	    or $project["project_projectkind"] == 9
		or $project["project_projectkind"] == 6
		or $project["project_projectkind"] == 3 
		or $project["project_projectkind"] == 4)) // new corporate, relocation, take ober, takover renovation
{
	$form->add_edit("eprepnr", "Enterprise Reporting Number:", 0, $pos_data["posaddress_eprepnr"], TYPE_CHAR);
}
else
{
	//$form->add_label("lala", $pos_data["posaddress_eprepnr"]);
	$form->add_hidden("eprepnr", $pos_data["posaddress_eprepnr"]);
}



//End Project Head

$form->add_section("POS Location Address");


if(count($pos_data) > 0)
{
	$form->add_hidden("posaddress_id", $pos_data["posaddress_id"]);

	if($project["project_actual_opening_date"] == '0000-00-00' or !$project["project_actual_opening_date"]) 
	{
		if( $project["project_projectkind"] == 8) //popup
		{
			$form->add_edit("popup_name", "PopUp Description*", NOTNULL, $project["project_popup_name"], TYPE_CHAR, 0, 0, 2, "popup");
		}
		else
		{
			$form->add_hidden("popup_name");
		}
		$form->add_edit("shop_address_company", "Project Name*", NOTNULL, $project["order_shop_address_company"], TYPE_CHAR, 0, 0, 2, "shop_address_company");
		$form->add_edit("shop_address_company2", "", 0, $project["order_shop_address_company2"], TYPE_CHAR, 0, 0, 2, "shop_address_company2");
		
		$form->add_hidden("shop_address_address", $project["order_shop_address_address"]);
		$form->add_multi_edit("shop_street", array("order_shop_address_street", "order_shop_address_streetnumber"), "Street/Street number", array(NOTNULL, ''), array($project["order_shop_address_street"], $project["order_shop_address_streetnumber"]), array('', ''), array(200, 6), array('',''), 2, "shop_address_address", '', array(39, 6), array(), 0, '', '', array(40, 5));
		
		$form->add_edit("shop_address_address2", "Additional Address Info", 0, $project["order_shop_address_address2"], TYPE_CHAR, 0, 0, 2, "shop_address_address2");
		$form->add_edit("shop_address_zip", "ZIP", 0, $project["order_shop_address_zip"], TYPE_CHAR, 20);
		
		$form->add_list("posaddress_place_id", "City Selection*",
			"select place_id, place_name from places where place_country = " . dbquote($pos_data["posaddress_country"]) . " order by place_name", NOTNULL | SUBMIT, $pos_data["posaddress_place_id"]);

		$form->add_edit("shop_address_place", "City*", NOTNULL | DISABLED, $project["order_shop_address_place"], TYPE_CHAR, 20);

		$form->add_label("province", "Province", 0, $pos_data["province_canton"]);

		$form->add_list("shop_address_country", "Country*", $sql_countries, NOTNULL, $project["order_shop_address_country"]);
		
		$form->add_hidden("shop_address_phone", $project["order_shop_address_phone"]);
		$form->add_multi_edit("shop_phone_number", array("order_shop_address_phone_country", "order_shop_address_phone_area", "order_shop_address_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array($project["order_shop_address_phone_country"], $project["order_shop_address_phone_area"], $project["order_shop_address_phone_number"]), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

		$form->add_hidden("shop_address_mobile_phone", $project["order_shop_address_mobile_phone"]);
		$form->add_multi_edit("shop_mobile_phone_number", array("order_shop_address_mobile_phone_country", "order_shop_address_mobile_phone_area", "order_shop_address_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array($project["order_shop_address_mobile_phone_country"], $project["order_shop_address_mobile_phone_area"], $project["order_shop_address_mobile_phone_number"]), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

		$form->add_edit("shop_address_email", "Email", 0, $project["order_shop_address_email"], TYPE_CHAR, 50);
		$form->add_edit("sapnr", "SAP Number", 0, $pos_data["posaddress_sapnumber"], TYPE_CHAR);

		$form->add_section("Google Map Coordinates");
		$form->add_label("GM", "Google Map", RENDER_HTML);
		//$form->add_edit("posaddress_google_lat", "Latitude", NOTNULL | DISABLED, $pos_data["posaddress_google_lat"]);
		//$form->add_edit("posaddress_google_long", "Longitude", NOTNULL | DISABLED, $pos_data["posaddress_google_long"]);

		$form->add_edit("posaddress_google_lat", "Latitude", NOTNULL , $pos_data["posaddress_google_lat"]);
		$form->add_edit("posaddress_google_long", "Longitude", NOTNULL , $pos_data["posaddress_google_long"]);

		$form->add_hidden("posaddress_google_precision", 1);
		$form->add_label("gmcheck", "");

		if(($project["project_projectkind"] == 1 
			or $project["project_projectkind"] == 6
			or $project["project_projectkind"] == 9) 
			and $project["pipeline"] == 1) //POS is in pipeline and it is a new POS
		{
			$form->add_hidden("overwrite_posaddress_data", 1);
		}
		else
		{
			$form->add_hidden("overwrite_posaddress_data", 0);
		}
	}
	else
	{
		if( $project["project_projectkind"] == 8)
		{
			$form->add_label("popup_name", "PopUp Description", 0, $project["project_popup_name"]);
		}
		else
		{
			$form->add_hidden("popup_name");
		}
		
		$form->add_label("shop_address_company", "Project Name", 0, $project["order_shop_address_company"]);
		$form->add_label("shop_address_company2", "", 0, $project["order_shop_address_company2"]);
		$form->add_label("shop_address_address", "Street", 0, $project["order_shop_address_address"]);
		
		$form->add_label("shop_address_address2", "Additional Address Info", 0, $project["order_shop_address_address2"]);
		$form->add_label("shop_address_zip", "ZIP", 0, $project["order_shop_address_zip"]);
		
		$form->add_hidden("posaddress_place_id", $pos_data["posaddress_place_id"]);
		$form->add_label("shop_address_place", "City", 0 , $project["order_shop_address_place"]);
		$form->add_label("province", "Province", 0, $pos_data["province_canton"]);
		
		$form->add_list("shop_address_country", "Country*", $sql_countries, NOTNULL, $project["order_shop_address_country"]);
		$form->add_hidden("shop_address_country", $project["order_shop_address_country"]);

		$form->add_lookup("shop_address_country_name", "Country", "countries", "country_name", 0, $project["order_shop_address_country"]);

		$form->add_label("shop_address_phone", "Phone", 0, $project["order_shop_address_phone"]);

		$form->add_label("shop_address_mobile_phone", "Mobile Phone", 0, $project["order_shop_address_mobile_phone"]);
		$form->add_label("shop_address_email", "Email", 0, $project["order_shop_address_email"]);
		$form->add_label("sapnr", "SAP Number", 0, $pos_data["posaddress_sapnumber"]);

		$form->add_section("Google Map Coordinates");
		$form->add_label("GM", "Google Map", RENDER_HTML);
		//$form->add_edit("posaddress_google_lat", "Latitude", NOTNULL | DISABLED, $pos_data["posaddress_google_lat"]);
		//$form->add_edit("posaddress_google_long", "Longitude", NOTNULL | DISABLED, $pos_data["posaddress_google_long"]);

		$form->add_edit("posaddress_google_lat", "Latitude", NOTNULL, $pos_data["posaddress_google_lat"]);
		$form->add_edit("posaddress_google_long", "Longitude", NOTNULL, $pos_data["posaddress_google_long"]);


		$form->add_hidden("posaddress_google_precision", 1);
		$form->add_label("gmcheck", "");
		$form->add_hidden("overwrite_posaddress_data", 1);
	}

	$form->add_section("Surfaces");
	$form->add_list("unit_of_measurement", "Unit of measurement", $units, NOTNULL, 2);
	
	//$form->add_edit("shop_grosssqms", "Gross Surface in <span id='um00'>sqms</span>*", NOTNULL, $project_cost_grosssqms, TYPE_DECIMAL, 10,2, 3, "grosssqm");
	$form->add_hidden("shop_grosssqms");

	$form->add_edit("shop_totalsqms", "Total Surface in <span id='um01'>sqms</span>*", NOTNULL, $project_cost_totalsqms, TYPE_DECIMAL, 10,2, 3, "totalsqm");
	$form->add_label("requested_store_retailarea", "Sales Surface upon project entry in sqms*", 0, $project_cost_original_sqms);
	$form->add_edit("shop_sqms", "Sales Surface according to layout in <span id='um02'>sqms</span>*", NOTNULL, $project_cost_sqms, TYPE_DECIMAL, 10,2, 3, "retailsqm");
	$form->add_edit("shop_bakoffocesqms", "Other Surface in <span id='um03'>sqms</span>", 0, $project_cost_bakoffocesqms, TYPE_DECIMAL, 10,2, 3, "backofficesqm");
	
	/*
	$form->add_edit("shop_numfloors", "Number of Floors*", NOTNULL, $project_cost_numfloors, TYPE_DECIMAL, 10,2);
	$form->add_edit("shop_floorsurface1", "Floor 1: Surface in <span id='um04'>sqms</span>", 0, $project_cost_floorsurface1, TYPE_DECIMAL, 10,2);
	$form->add_edit("shop_floorsurface2", "Floor 2: Surface in <span id='um05'>sqms</span>", 0, $project_cost_floorsurface2, TYPE_DECIMAL, 10,2);
	$form->add_edit("shop_floorsurface3", "Floor 3: Surface in <span id='um06'>sqms</span>", 0, $project_cost_floorsurface3, TYPE_DECIMAL, 10,2);
	*/

	$form->add_hidden("shop_numfloors", $project_cost_numfloors);
	$form->add_hidden("shop_floorsurface1", $project_cost_floorsurface1);
	$form->add_hidden("shop_floorsurface2", $project_cost_floorsurface2);
	$form->add_hidden("shop_floorsurface3", $project_cost_floorsurface3);




	$form->add_section("Sales");
	if($project["project_distribution_channel"])
	{
		$form->add_list("posaddress_distribution_channel", "Distribution Channel", $sql_distribution_channels, 0, $project["project_distribution_channel"]);
	}
	else
	{
		$form->add_list("posaddress_distribution_channel", "Distribution Channel", $sql_distribution_channels, 0, $standard_dsitribution_channel);
	}


	$form->add_section("Environment");
	foreach($posareas as $key=>$name)
	{
		if(array_key_exists($key, $pos_posareas))
		{
			$form->add_checkbox("area". $key, $name, 1, 0, "");
		}
		else
		{
			$form->add_checkbox("area". $key, $name, "", 0, "");
		}

	}


	$form->add_comment("Please indicate on which floor the POS will be situated (Ground floor, Street Level, 1st Floor etc.).");

	$form->add_edit("project_floor", "Floor*", NOTNULL,$project["project_floor"], TYPE_CHAR, 30);


	$form->add_section("Area Perception");
	$form->add_comment("Please rate by clicking a star: 5 stars are the best rating.");
	if(count($pos_data) > 0 and array_key_exists($pos_data["posaddress_perc_class"], $ratings))
	{
		$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, $pos_data["posaddress_perc_class"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, $pos_data["posaddress_perc_tourist"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings,  $pos_data["posaddress_perc_transport"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings,  $pos_data["posaddress_perc_people"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings,  $pos_data["posaddress_perc_parking"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings,  $pos_data["posaddress_perc_visibility1"], NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from across the Street*", $ratings,  $pos_data["posaddress_perc_visibility2"], NOTNULL);
	}
	else
	{
		$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, 0, NOTNULL);
		$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from across the Street*", $ratings, 0, NOTNULL);
	}

	$form->add_section("Neighbourhood");
	$form->add_edit("posorder_neighbour_left", "Shop on Left Side", 0, $neighbourhoods["Shop on Left Side"]);

	$form->add_list("posorder_neighbour_left_business_type", "Business Type Shop on Left Side", $businesstypes, 0, $neighbourhoods_business_types["Shop on Left Side"]);
	if(param("posorder_neighbour_left_business_type") == 999999)
	{
		$form->add_edit("posorder_neighbour_left_business_type_new", "--- Business Type");
	}
	else
	{
		$form->add_hidden("posorder_neighbour_left_business_type_new");
	}
	
	$form->add_list("posorder_neighbour_left_price_range", "Price Range Shop on Left Side", $priceranges, 0, $neighbourhoods_price_ranges["Shop on Left Side"]);

	$form->add_edit("posorder_neighbour_right", "Shop on Right Side", 0, $neighbourhoods["Shop on Right Side"]);

	$form->add_list("posorder_neighbour_right_business_type", "Business Type Shop on Right Side", $businesstypes, 0, $neighbourhoods_business_types["Shop on Right Side"]);

	if(param("posorder_neighbour_right_business_type") == 999999)
	{
		$form->add_edit("posorder_neighbour_right_business_type_new", "--- Business Type");
	}
	else
	{
		$form->add_hidden("posorder_neighbour_right_business_type_new");
	}

	$form->add_list("posorder_neighbour_right_price_range", "Price Range Shop on Right Side", $priceranges, 0, $neighbourhoods_price_ranges["Shop on Right Side"]);

	$form->add_edit("posorder_neighbour_acrleft", "Shop Across Left Side", 0, $neighbourhoods["Shop Across Left Side"]);
	$form->add_list("posorder_neighbour_acrleft_business_type", "Business Type  Across Left Side", $businesstypes, 0, $neighbourhoods_business_types["Shop Across Left Side"]);

	if(param("posorder_neighbour_acrleft_business_type") == 999999)
	{
		$form->add_edit("posorder_neighbour_acrleft_business_type_new", "--- Business Type");
	}
	else
	{
		$form->add_hidden("posorder_neighbour_acrleft_business_type_new");
	}

	$form->add_list("posorder_neighbour_acrleft_price_range", "Price Range Shop  Across Left Side", $priceranges, 0, $neighbourhoods_price_ranges["Shop Across Left Side"]);

	$form->add_edit("posorder_neighbour_acrright", "Shop Across Right Side", 0, $neighbourhoods["Shop Across Right Side"]);

	$form->add_list("posorder_neighbour_acrright_business_type", "Business Type Shop Across Right Side", $businesstypes, 0, $neighbourhoods_business_types["Shop Across Right Side"]);

	if(param("posorder_neighbour_acrright_business_type") == 999999)
	{
		$form->add_edit("posorder_neighbour_acrright_business_type_new", "--- Business Type");
	}
	else
	{
		$form->add_hidden("posorder_neighbour_acrright_business_type_new");
	}

	$form->add_list("posorder_neighbour_acrright_price_range", "Price Range Shop Across Right Side", $priceranges, 0, $neighbourhoods_price_ranges["Shop Across Right Side"]);

	$form->add_multiline("posorder_neighbour_brands", "Other Brands in Area", 4, 0, $neighbourhoods["Other Brands in Area"]);
}
else
{
	$pos_data["posaddress_google_lat"] = 0;
	$pos_data["posaddress_google_long"] = 0;

	if (has_access("can_use_posindex"))
	{
		$form->add_comment("Assignment to POS Index.");
		$form->add_list("posaddress_id", "POS Index POS Name", $sql_posaddresses, SUBMIT, $project["posaddress_id"]);
	}
	else
	{
		$form->add_hidden("posaddress_id", $project["posaddress_id"]);
	}


	if( $project["project_projectkind"] == 8)
	{
		$form->add_edit("popup_name", "PopUp Description*", NOTNULL, $project["project_popup_name"], TYPE_CHAR);
	}
	else
	{
		$form->add_hidden("popup_name");
	}

	$form->add_edit("shop_address_company", "Project Name*", NOTNULL, $project["order_shop_address_company"], TYPE_CHAR);
	$form->add_edit("shop_address_company2", "", 0, $project["order_shop_address_company2"], TYPE_CHAR);
	$form->add_hidden("shop_address_address", $project["order_shop_address_address"]);
	
	
	$form->add_hidden("shop_address_address", $project["order_shop_address_address"]);
	$form->add_multi_edit("shop_street", array("order_shop_address_street", "order_shop_address_streetnumber"), "Street/Street number", array(NOTNULL, ''), array($project["order_shop_address_street"], $project["order_shop_address_streetnumber"]), array('', ''), array(200, 6), array('',''), 2, "shop_address_address", '', array(40, 5));

	$form->add_edit("shop_address_address2", "Additional Address Info", 0, $project["order_shop_address_address2"], TYPE_CHAR);
	$form->add_edit("shop_address_zip", "ZIP", 0, $project["order_shop_address_zip"], TYPE_CHAR, 20);
	
	$place_id = "";
	$province = "";
	$sql = "select place_id, province_canton " .
	       "from places " .
		   "left join provinces on province_id = place_province " . 
		   "where place_country = " . dbquote($project["order_shop_address_country"]) . 
		   " and place_name = " . dbquote($project["order_shop_address_place"]);
	
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$place_id = $row["place_id"];
		$province = $row["province_canton"];
	}
	
	$form->add_list("posaddress_place_id", "City Selection*",
		"select place_id, place_name from places where place_country = " . dbquote($project["order_shop_address_country"]) . " order by place_name", NOTNULL | SUBMIT, $place_id);

	$form->add_edit("shop_address_place", "City*", NOTNULL, $project["order_shop_address_place"], TYPE_CHAR, 20);

	$form->add_label("province", "Province", 0, $province);

	$form->add_list("shop_address_country", "Country*", $sql_countries, NOTNULL, $project["order_shop_address_country"]);
	$form->add_hidden("shop_address_phone", $project["order_shop_address_phone"]);
	$form->add_multi_edit("shop_phone_number", array("order_shop_address_phone_country", "order_shop_address_phone_area", "order_shop_address_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array($project["order_shop_address_phone_country"], $project["order_shop_address_phone_area"], $project["order_shop_address_phone_number"]), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

	$form->add_hidden("shop_address_mobile_phone", $project["order_shop_address_mobile_phone"]);
	$form->add_multi_edit("shop_mobile_phone_number", array("order_shop_address_mobile_phone_country", "order_shop_address_mobile_phone_area", "order_shop_address_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array($project["order_shop_address_mobile_phone_country"], $project["order_shop_address_mobile_phone_area"], $project["order_shop_address_mobile_phone_number"]), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

	$form->add_edit("shop_address_email", "Email", 0, $project["order_shop_address_email"], TYPE_CHAR, 50);
	$form->add_edit("sapnr", "SAP Number", 0, "", TYPE_CHAR);

	$form->add_section("Google Map Coordinates");
	$form->add_label("GM", "Google Map", RENDER_HTML);
	//$form->add_edit("posaddress_google_lat", "Latitude", NOTNULL | DISABLED, $pos_data["posaddress_google_lat"]);
	//$form->add_edit("posaddress_google_long", "Longitude", NOTNULL | DISABLED, $pos_data["posaddress_google_long"]);

	$form->add_edit("posaddress_google_lat", "Latitude", NOTNULL, $pos_data["posaddress_google_lat"]);
	$form->add_edit("posaddress_google_long", "Longitude", NOTNULL, $pos_data["posaddress_google_long"]);


	$form->add_hidden("posaddress_google_precision", 1);
	$form->add_label("gmcheck", "");
	$form->add_hidden("overwrite_posaddress_data", 1);

	$form->add_section("Surfaces");
	$form->add_list("unit_of_measurement", "Unit of measurement", $units, NOTNULL, 2);

	//$form->add_edit("shop_grosssqms", "Gross Surface in <span id='um00'>sqms</span>*", NOTNULL, $project_cost_grosssqms, TYPE_DECIMAL, 10,2, 3, "grosssqm");
	$form->add_hidden("shop_grosssqms");

	$form->add_edit("shop_totalsqms", "Total Surface in <span id='um01'>sqms</span>*", NOTNULL, $project_cost_totalsqms, TYPE_DECIMAL, 10,2, 3, "totalsqm");
	$form->add_label("requested_store_retailarea", "Sales Surface upon project entry in sqms*", 0, $project_cost_original_sqms);
	$form->add_edit("shop_sqms", "Sales Surface according to layout in <span id='um02'>sqms</span>*", NOTNULL, $project_cost_sqms, TYPE_DECIMAL, 10,2, 3, "retailsqm");
	$form->add_edit("shop_bakoffocesqms", "Other Surface in <span id='um03'>sqms</span>", 0, $project_cost_bakoffocesqms, TYPE_DECIMAL, 10,2, 3, "backofficesqm");
	
	/*
	$form->add_edit("shop_numfloors", "Number of Floors*", NOTNULL, $project_cost_numfloors, TYPE_DECIMAL, 10,2);
	$form->add_edit("shop_floorsurface1", "Floor 1: Surface in <span id='um04'>sqms</span>", 0, $project_cost_floorsurface1, TYPE_DECIMAL, 10,2);
	$form->add_edit("shop_floorsurface2", "Floor 2: Surface in <span id='um05'>sqms</span>", 0, $project_cost_floorsurface2, TYPE_DECIMAL, 10,2);
	$form->add_edit("shop_floorsurface3", "Floor 3: Surface in <span id='um06'>sqms</span>", 0, $project_cost_floorsurface3, TYPE_DECIMAL, 10,2);
    */


	$form->add_edit("shop_numfloors", $project_cost_numfloors);
	$form->add_edit("shop_floorsurface1", $project_cost_floorsurface1);
	$form->add_edit("shop_floorsurface2", $project_cost_floorsurface2);
	$form->add_edit("shop_floorsurface3", $project_cost_floorsurface3);


	$form->add_section("Sales");
	if($project["project_distribution_channel"])
	{
		$form->add_list("posaddress_distribution_channel", "Distribution Channel", $sql_distribution_channels, 0, $project["project_distribution_channel"]);
	}
	else
	{
		$form->add_list("posaddress_distribution_channel", "Distribution Channel", $sql_distribution_channels, 0, $standard_dsitribution_channel);
	}

	$form->add_section("Environment");
	foreach($posareas as $key=>$name)
	{
		if(array_key_exists($key, $pos_posareas))
		{
			$form->add_checkbox("area". $key, $name, 1, 0, "");
		}
		else
		{
			$form->add_checkbox("area". $key, $name, "", 0, "");
		}

	}

	$form->add_comment("Please indicate on which floor the POS will be situated (Ground floor, Street Level, 1st Floor etc.).");
	$form->add_edit("project_floor", "Floor*", NOTNULL,$project["project_floor"], TYPE_CHAR, 30);

	$form->add_section("Area Perception");
	$form->add_comment("Please rate by clicking a star: 5 stars are the best rating.");
	$form->add_rating_selector("posaddress_perc_class", "Class/Image Area*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_tourist", "Tourist/Historical Area*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_transport", "Public Transportation*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_people", "People Traffic Area*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_parking", "Parking Possibilities*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_visibility1", "Visibility from Pavement*", $ratings, 0, NOTNULL);
	$form->add_rating_selector("posaddress_perc_visibility2", "Visibility from across the Street*", $ratings, 0, NOTNULL);

	$form->add_section("Neighbourhood");
	$form->add_edit("posorder_neighbour_left", "Shop on Left Side", 0);
	$form->add_list("posorder_neighbour_left_business_type", "Business Type Shop on Left Side", $businesstypes, 0);
	if(param("posorder_neighbour_left_business_type") == 999999)
	{
		$form->add_edit("posorder_neighbour_left_business_type_new", "--- Business Type");
	}
	else
	{
		$form->add_hidden("posorder_neighbour_left_business_type_new");
	}
	
	$form->add_list("posorder_neighbour_left_price_range", "Price Range Shop on Left Side", $priceranges);


	$form->add_edit("posorder_neighbour_right", "Shop on Right Side", 0);
	$form->add_list("posorder_neighbour_right_business_type", "Business Type Shop on Right Side", $businesstypes, 0);

	if(param("posorder_neighbour_right_business_type") == 999999)
	{
		$form->add_edit("posorder_neighbour_right_business_type_new", "--- Business Type");
	}
	else
	{
		$form->add_hidden("posorder_neighbour_right_business_type_new");
	}

	$form->add_list("posorder_neighbour_right_price_range", "Price Range Shop on Right Side", $priceranges);



	$form->add_edit("posorder_neighbour_acrleft", "Shop Across Left Side", 0);

	$form->add_list("posorder_neighbour_acrleft_business_type", "Business Type Shop Across Left Side", $businesstypes, 0);

		if(param("posorder_neighbour_acrleft_business_type") == 999999)
		{
			$form->add_edit("posorder_neighbour_acrleft_business_type_new", "--- Business Type");
		}
		else
		{
			$form->add_hidden("posorder_neighbour_acrleft_business_type_new");
		}

		$form->add_list("posorder_neighbour_acrleft_price_range", "Price Range Shop Across Left Side", $priceranges);


	$form->add_edit("posorder_neighbour_acrright", "Shop Across Right Side", 0);

	$form->add_list("posorder_neighbour_acrright_business_type", "Business Type Shop Across Right Side", $businesstypes, 0);

	if(param("posorder_neighbour_acrright_business_type") == 999999)
	{
		$form->add_edit("posorder_neighbour_acrright_business_type_new", "--- Business Type");
	}
	else
	{
		$form->add_hidden("posorder_neighbour_acrright_business_type_new");
	}

	$form->add_list("posorder_neighbour_acrright_price_range", "Price Range Shop Across Right Side", $priceranges);

	$form->add_multiline("posorder_neighbour_brands", "Other Brands in Area", 4, 0);
}


if (has_access("can_edit_franchisee_data"))
{

	if($project["project_projectkind"] == 4 
		or $project["project_projectkind"] == 3
		or $project["project_projectkind"] == 9)
	{
		if(count($former_franchisee_address) > 0)
		{
			$form->add_section("Former POS Owner");

			$form->add_label("former_franchisee_address_company", "Company", 0, $former_franchisee_address["company"]);
			if($former_franchisee_address["company2"]) {
				$form->add_label("former_franchisee_address_company2", "", 0, $former_franchisee_address["company2"]);
			}
			$form->add_label("former_franchisee_address_address", "Street", 0, $former_franchisee_address["address"]);
			
			if($former_franchisee_address["address2"]) {
				$form->add_label("former_franchisee_address_address2","Additional Address Info", 0, $former_franchisee_address["address2"]);
			}
			$form->add_label("former_franchisee_address_zip", "ZIP", 0, $former_franchisee_address["zip"]);
			$form->add_label("former_franchisee_address_place", "Place", 0, $former_franchisee_address["place"]);
			$form->add_label("former_franchisee_address_country", "Country", 0, $former_franchisee_address["country_name"]);
		}
	}
	
	if($project["project_cost_type"] != 6 and $project["project_postype"] != 2)
	{
		$form->add_section("Address of Owner Company");
		$tmp_text1="Please indicate the address of the POS owner company.";
		$tmp_text2="\nYou can either select an existing address or enter a new address.";
		$form->add_comment($tmp_text1 . $tmp_text2);
		$form->add_list("order_franchisee_address_id", "Owner Company", $sql_franchisees, SUBMIT, $project["order_franchisee_address_id"]);
	}
	else
	{
		$form->add_section("Address of Owner Company");
		$tmp_text1="Please indicate the owner company address.";
		$tmp_text2="\nYou can either select an existing address or enter a new address.";
		$form->add_comment($tmp_text1 . $tmp_text2);
		$form->add_list("order_franchisee_address_id", "Owner Company", $sql_franchisees, SUBMIT, $project["order_franchisee_address_id"]);

	}
	
	if($project["project_cost_type"] == 1 
		and ($project["project_projectkind"] == 3 
			or  $project["project_projectkind"] == 4
		    or  $project["project_projectkind"] == 9))
	{
		$form->add_hidden("franchisee_address_parent", $project["order_client_address"]);
		$form->add_edit("franchisee_address_company", "Company*", 0, $franchisee_address["company"], TYPE_CHAR, 0, 0, 2, "franchisee_address_company");
		$form->add_edit("franchisee_address_company2", "", 0, $franchisee_address["company2"], TYPE_CHAR, 0, 0, 2, "franchisee_address_company2");
		
		
		$form->add_hidden("franchisee_address_address", $franchisee_address["address"]);
		$form->add_multi_edit("franchisee_street", array("order_franchisee_address_street", "order_franchisee_address_streetnumber"), "Street/Street number", array(NOTNULL, ''), array($franchisee_address["street"], $franchisee_address["streetnumber"]), array('', ''), array(200, 6), array('',''), 2, "franchisee_address_address", '', array(40, 5));

		
		$form->add_edit("franchisee_address_address2", "Additional Address Info", 0, $franchisee_address["address2"], TYPE_CHAR, 0, 0, 2, "franchisee_address_address2");

		$form->add_list("franchisee_address_country", "Country*", $sql_countries, SUBMIT, $franchisee_address["country"]);

		

		if(param("franchisee_address_country"))
		{
			$form->add_list("franchisee_address_place_id", "City Selection*",
				"select place_id, place_name from places where place_country = " . dbquote(param("franchisee_address_country")) . " order by place_name", NOTNULL | SUBMIT, $franchisee_address["place_id"]);
		}
		else
		{
			$form->add_list("franchisee_address_place_id", "City Selection*",
				"select place_id, place_name from places where ( place_country = " .  dbquote($pos_data["posaddress_country"]) . " or place_country = " . dbquote($franchisee_address["country"]) . ") order by place_name", NOTNULL | SUBMIT, $franchisee_address["place_id"]);
		}
		

		$form->add_edit("franchisee_address_place", "City*", DISABLED, $franchisee_address["place"], TYPE_CHAR, 20);

		$form->add_edit("franchisee_address_zip", "ZIP", 0, $franchisee_address["zip"], TYPE_CHAR, 20);
		
		
		
		$form->add_edit("franchisee_address_phone", "Phone", 0, $franchisee_address["phone"], TYPE_CHAR, 20);
		$form->add_edit("franchisee_address_mobile_phone", "Mobile Phone", 0, $franchisee_address["mobile_phone"], TYPE_CHAR, 20);


		$form->add_hidden("franchisee_address_phone", $franchisee_address["phone"]);
		$form->add_multi_edit("franchisee_phone_number", array("order_franchisee_address_phone_country", "order_franchisee_address_phone_area", "order_franchisee_address_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array($franchisee_address["phone_country"], $franchisee_address["phone_area"], $franchisee_address["phone_number"]), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));


		$form->add_hidden("franchisee_address_mobile_phone", $franchisee_address["mobile_phone"]);
		$form->add_multi_edit("franchisee_mobile_phone_number", array("order_franchisee_address_mobile_phone_country", "order_franchisee_address_mobile_phone_area", "order_franchisee_address_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array($franchisee_address["mobile_phone_country"], $franchisee_address["mobile_phone_area"], $franchisee_address["mobile_phone_number"]), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));


		$form->add_edit("franchisee_address_email", "Email", 0, $franchisee_address["email"], TYPE_CHAR);
		$form->add_edit("franchisee_address_contact", "Contact*", 0, $franchisee_address["contact_name"], TYPE_CHAR);
		$form->add_edit("franchisee_address_website", "Website", 0, $franchisee_address["website"], TYPE_CHAR);
	}
	else
	{
		
		$form->add_hidden("franchisee_address_parent", $project["order_client_address"]);
		$form->add_edit("franchisee_address_company", "Company*", 0, $franchisee_address["company"], TYPE_CHAR, 0, 0, 2, "franchisee_address_company");
		$form->add_edit("franchisee_address_company2", "", 0, $franchisee_address["company2"], TYPE_CHAR, 0, 0, 2, "franchisee_address_company2");
		
		
		$form->add_hidden("franchisee_address_address", $franchisee_address["address"]);
		$form->add_multi_edit("franchisee_street", array("order_franchisee_address_street", "order_franchisee_address_streetnumber"), "Street/Street number", array(NOTNULL, ''), array($franchisee_address["street"], $franchisee_address["streetnumber"]), array('', ''), array(200, 6), array('',''), 2, "franchisee_address_address", '', array(40, 5));

		$form->add_edit("franchisee_address_address2", "Additional Address Info", 0, $franchisee_address["address2"], TYPE_CHAR, 0, 0, 2, "franchisee_address_address2");

		$form->add_list("franchisee_address_country", "Country*", $sql_countries, SUBMIT, $franchisee_address["country"]);

		
		if(param("franchisee_address_country"))
		{
			$form->add_list("franchisee_address_place_id", "City Selection*",
				"select place_id, place_name from places where place_country = " . dbquote(param("franchisee_address_country")) . " order by place_name", NOTNULL | SUBMIT, $franchisee_address["place_id"]);
		}
		else
		{
			$form->add_list("franchisee_address_place_id", "City Selection*",
				"select place_id, place_name from places where ( place_country = " .  dbquote($pos_data["posaddress_country"]) . " or place_country = " . dbquote($franchisee_address["country"]) . ") order by place_name", NOTNULL | SUBMIT, $franchisee_address["place_id"]);

			
		}

		$form->add_edit("franchisee_address_place", "City*", DISABLED, $franchisee_address["place"], TYPE_CHAR, 20);

		$form->add_edit("franchisee_address_zip", "ZIP", 0, $franchisee_address["zip"], TYPE_CHAR, 20);
	
		
		$form->add_hidden("franchisee_address_phone", $franchisee_address["phone"]);
		$form->add_multi_edit("franchisee_phone_number", array("order_franchisee_address_phone_country", "order_franchisee_address_phone_area", "order_franchisee_address_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array($franchisee_address["phone_country"], $franchisee_address["phone_area"], $franchisee_address["phone_number"]), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));


		$form->add_hidden("franchisee_address_mobile_phone", $franchisee_address["mobile_phone"]);
		$form->add_multi_edit("franchisee_mobile_phone_number", array("order_franchisee_address_mobile_phone_country", "order_franchisee_address_mobile_phone_area", "order_franchisee_address_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE, PHONE_NUMBER), array($franchisee_address["mobile_phone_country"], $franchisee_address["mobile_phone_area"], $franchisee_address["mobile_phone_number"]), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20));

		$form->add_edit("franchisee_address_email", "Email", 0, $franchisee_address["email"], TYPE_CHAR);
		$form->add_edit("franchisee_address_contact", "Contact*", 0, $franchisee_address["contact_name"], TYPE_CHAR);
		$form->add_edit("franchisee_address_website", "Website", 0, $franchisee_address["website"], TYPE_CHAR);

		/*
		
		$form->add_hidden("franchisee_address_parent", $project["order_client_address"]);
		$form->add_edit("franchisee_address_company", "Company", 0, $project["order_franchisee_address_company"], TYPE_CHAR);
		$form->add_edit("franchisee_address_company2", "", 0, $project["order_franchisee_address_company2"], TYPE_CHAR);
		$form->add_edit("franchisee_address_address", "Street", 0, $project["order_franchisee_address_address"], TYPE_CHAR);
		$form->add_edit("franchisee_address_address2", "Additional Address Info", 0, $project["order_franchisee_address_address2"], TYPE_CHAR);
		
		$form->add_list("franchisee_address_country", "Country", $sql_countries, SUBMIT, $project["order_franchisee_address_country"]);

		if(param("franchisee_address_country"))
		{
			$form->add_list("franchisee_address_place_id", "City Selection*",
				"select place_id, place_name from places where place_country = " . dbquote(param("franchisee_address_country")) . " order by place_name", NOTNULL | SUBMIT, $franchisee_address["place_id"]);
		}
		elseif($project["order_franchisee_address_country"])
		{
			$form->add_list("franchisee_address_place_id", "City Selection*",
				"select place_id, place_name from places where place_country = " . dbquote($project["order_franchisee_address_country"]) . " order by place_name", NOTNULL | SUBMIT, $franchisee_address["place_id"]);
		}
		else
		{
			$form->add_list("franchisee_address_place_id", "City Selection*",
				"select place_id, place_name from places where place_country = " . dbquote($project["order_shop_address_country"]) . " order by place_name", NOTNULL | SUBMIT, $franchisee_address["place_id"]);
		}

		$form->add_edit("franchisee_address_place", "City", DISABLED, $project["order_franchisee_address_place"], TYPE_CHAR, 20);

		$form->add_edit("franchisee_address_zip", "ZIP", 0, $project["order_franchisee_address_zip"], TYPE_CHAR, 20);
		
		
		
		$form->add_edit("franchisee_address_phone", "Phone", 0, $project["order_franchisee_address_phone"], TYPE_CHAR, 20);
		$form->add_edit("franchisee_address_mobile_phone", "Mobile Phone", 0, $project["order_franchisee_address_mobile_phone"], TYPE_CHAR, 20);
		$form->add_edit("franchisee_address_email", "Email", 0, $project["order_franchisee_address_email"], TYPE_CHAR);
		$form->add_edit("franchisee_address_contact", "Contact", 0, $project["order_franchisee_address_contact"], TYPE_CHAR);
		$form->add_edit("franchisee_address_website", "Website", 0, $project["order_franchisee_address_website"], TYPE_CHAR);
		*/
	}
	
	$form->add_comment("Click the checkbox below in case you want to create a new POS owner address in 3rdparty adresses. <br />A new address will only be created if the drop down list 'Franchisee' is empty. <br />Only do this if you are sure that the franchisee is not contained in the list of existing franchisees.");
	$form->add_checkbox("create_new_franchisee_address", "Create New Address", "", 0, "POS Owner Address");
}
else
{	
	if($project["project_cost_type"] == 1 
		and ($project["project_projectkind"] == 3 
		or  $project["project_projectkind"] == 4
		or  $project["project_projectkind"] == 9))
	{
		$form->add_hidden("order_franchisee_address_id", $project["order_client_address"]);
		$form->add_hidden("franchisee_address_parent", $project["order_client_address"]);
		$form->add_hidden("franchisee_address_company", $franchisee_address["company"]);
		$form->add_hidden("franchisee_address_company2", $franchisee_address["company2"]);
		$form->add_hidden("franchisee_address_address", $franchisee_address["address"]);
		$form->add_hidden("franchisee_address_address2",$franchisee_address["address2"]);
		$form->add_hidden("franchisee_address_zip", $franchisee_address["zip"]);
		$form->add_hidden("franchisee_address_place", $franchisee_address["place"]);
		$form->add_hidden("franchisee_address_place_id", $franchisee_address["place_id"]);
		$form->add_hidden("franchisee_address_country", $franchisee_address["country"]);
		$form->add_hidden("franchisee_address_phone", $franchisee_address["phone"]);
		$form->add_hidden("franchisee_address_mobile_phone", $franchisee_address["mobile_phone"]);
		$form->add_hidden("franchisee_address_email", $franchisee_address["email"]);
		$form->add_hidden("franchisee_address_contact", $franchisee_address["contact_name"]);
		$form->add_hidden("franchisee_address_website", $franchisee_address["website"]);
	}
	else
	{
		/*
		$form->add_hidden("order_franchisee_address_id", $project["order_franchisee_address_id"]);
		$form->add_hidden("franchisee_address_parent", $project["order_client_address"]);
		$form->add_hidden("franchisee_address_company", $project["order_franchisee_address_company"]);
		$form->add_hidden("franchisee_address_company2", $project["order_franchisee_address_company2"]);
		$form->add_hidden("franchisee_address_address", $project["order_franchisee_address_address"]);
		$form->add_hidden("franchisee_address_address2",$project["order_franchisee_address_address2"]);
		$form->add_hidden("franchisee_address_zip", $project["order_franchisee_address_zip"]);
		$form->add_hidden("franchisee_address_place", $project["order_franchisee_address_place"]);
		$form->add_hidden("franchisee_address_place_id", $franchisee_address["place_id"]);
		$form->add_hidden("franchisee_address_country", $project["order_franchisee_address_country"]);
		$form->add_hidden("franchisee_address_phone", $project["order_franchisee_address_phone"]);
		$form->add_hidden("franchisee_address_mobile_phone", $project["order_franchisee_address_mobile_phone"]);
		$form->add_hidden("franchisee_address_email", $project["order_franchisee_address_email"]);
		$form->add_hidden("franchisee_address_contact", $project["order_franchisee_address_contact"]);
		$form->add_hidden("franchisee_address_website", $project["order_franchisee_address_website"]);
		*/

		$form->add_hidden("order_franchisee_address_id", $project["order_franchisee_address_id"]);
		$form->add_hidden("franchisee_address_parent", $project["order_client_address"]);
		$form->add_hidden("franchisee_address_company", $franchisee_address["company"]);
		$form->add_hidden("franchisee_address_company2", $franchisee_address["company2"]);
		$form->add_hidden("franchisee_address_address", $franchisee_address["address"]);
		$form->add_hidden("franchisee_address_address2",$franchisee_address["address2"]);
		$form->add_hidden("franchisee_address_zip", $franchisee_address["zip"]);
		$form->add_hidden("franchisee_address_place", $franchisee_address["place"]);
		$form->add_hidden("franchisee_address_place_id", $franchisee_address["place_id"]);
		$form->add_hidden("franchisee_address_country", $franchisee_address["country"]);
		$form->add_hidden("franchisee_address_phone", $franchisee_address["phone"]);
		$form->add_hidden("franchisee_address_mobile_phone", $franchisee_address["mobile_phone"]);
		$form->add_hidden("franchisee_address_email", $franchisee_address["email"]);
		$form->add_hidden("franchisee_address_contact", $franchisee_address["contact_name"]);
		$form->add_hidden("franchisee_address_website", $franchisee_address["website"]);
	}

	$form->add_hidden("create_new_franchisee_address", 0);
}


if($project["project_actual_opening_date"] == NULL or $project["project_actual_opening_date"] == '0000-00-00')
{
	if ($project['project_cost_type'] != 1
		and has_access("can_edit_franchisee_agreement_data"))
	{

		if(count($pos_data) > 0 and array_key_exists("posaddress_fagagreement_type", $pos_data) and  $pos_data["posaddress_fagagreement_type"] > 0 and $project["project_projectkind"] ==  2) // renovation projects
		{

			/*
			$type = $pos_data["posaddress_fagagreement_type"];
			$sent = $pos_data["posaddress_fagrsent"];
			$signed = $pos_data["posaddress_fagrsigned"];
			$start = $pos_data["posaddress_fagrstart"];
			$end = $pos_data["posaddress_fagrend"];
			$comment = $pos_data["posaddress_fag_comment"];
			*/

			
			$type = "";
			$sent = "";
			$signed = "";
			$start = "";
			$end = "";
			$comment = "";
		}
		else
		{
			$type = $project["project_fagagreement_type"];
			$sent = $project["project_fagrsent"];
			$signed = $project["project_fagrsigned"];
			$start = $project["project_fagrstart"];
			$end = $project["project_fagrend"];
			$comment = $project["project_fag_comment"];
		}
		
		$form->add_section("Agreement Data");
		$form->add_list("project_fagagreement_type", "Agreement Type*", "select agreement_type_id, agreement_type_name from agreement_types", 0, $type);
		$form->add_checkbox("project_fagrsent", "Sent", $sent, 0, "Agreement");
		$form->add_checkbox("project_fagrsigned", "Signed", $signed, 0, "Agreement");
		$form->add_label("dummy", "");
		$form->add_edit("project_fagrstart", "Agreement Starting Date", 0, to_system_date($start), TYPE_DATE);
		$form->add_edit("project_fagrend", "Agreement Ending Date", 0, to_system_date($end), TYPE_DATE);
		$form->add_multiline("project_fag_comment", "Comment", 4, 0, $comment);
	}
	else
	{	
		if(count($pos_data) > 0 and array_key_exists("posaddress_fagagreement_type", $pos_data) and  $pos_data["posaddress_fagagreement_type"] > 0)
		{
			$type = $pos_data["posaddress_fagagreement_type"];
			$sent = $pos_data["posaddress_fagrsent"];
			$signed = $pos_data["posaddress_fagrsigned"];
			$start = $pos_data["posaddress_fagrstart"];
			$end = $pos_data["posaddress_fagrend"];
			$comment = $pos_data["posaddress_fag_comment"];
		}
		else
		{
			$type = $project["project_fagagreement_type"];
			$sent = $project["project_fagrsent"];
			$signed = $project["project_fagrsigned"];
			$start = $project["project_fagrstart"];
			$end = $project["project_fagrend"];
			$comment = $project["project_fag_comment"];
		}
		
		$form->add_hidden("project_fagagreement_type", $type);
		$form->add_hidden("project_fagrsent", $sent);
		$form->add_hidden("project_fagrsigned", $signed);
		$form->add_hidden("project_fagrstart", to_system_date($start));
		$form->add_hidden("project_fagrend", to_system_date($end));
		$form->add_hidden("project_fag_comment",$comment);


	}
}
elseif (has_access("can_edit_franchisee_agreement_data"))
{
	if(count($pos_data) >0  and array_key_exists("posaddress_fagagreement_type", $pos_data) and  $pos_data["posaddress_fagagreement_type"] > 0)
	{
		$type = $pos_data["posaddress_fagagreement_type"];
		$sent = $pos_data["posaddress_fagrsent"];
		$signed = $pos_data["posaddress_fagrsigned"];
		$start = $pos_data["posaddress_fagrstart"];
		$end = $pos_data["posaddress_fagrend"];
		$comment = $pos_data["posaddress_fag_comment"];
	}
	else
	{
		$type = $project["project_fagagreement_type"];
		$sent = $project["project_fagrsent"];
		$signed = $project["project_fagrsigned"];
		$start = $project["project_fagrstart"];
		$end = $project["project_fagrend"];
		$comment = $project["project_fag_comment"];
	}


	if($project['project_cost_type'] != 1) {
		$form->add_section("Agreement");
		$form->add_list("project_fagagreement_type", "Agreement Type*", "select agreement_type_id, agreement_type_name from agreement_types", 0, $type);
		$form->add_checkbox("project_fagrsent", "Sent", $sent, 0, "Agreement");
		$form->add_checkbox("project_fagrsigned", "Signed", $signed, 0, "Agreement");
		$form->add_label("dummy", "");
		$form->add_edit("project_fagrstart", "Agreement Starting Date", 0, to_system_date($start), TYPE_DATE);
		$form->add_edit("project_fagrend", "Agreement Ending Date", 0, to_system_date($end), TYPE_DATE);
		$form->add_multiline("project_fag_comment", "Comment", 4, 0, $comment);
	}
	else {
	
		$form->add_hidden("project_fagagreement_type", $type);
		$form->add_hidden("project_fagrsent", $sent);
		$form->add_hidden("project_fagrsigned", $signed);
		$form->add_hidden("project_fagrstart", to_system_date($start));
		$form->add_hidden("project_fagrend", to_system_date($end));
		$form->add_hidden("project_fag_comment",$comment);
	}
}



$form->add_hidden("project_state", $project["project_state"]);


if(in_array(4, $user_roles) and has_access("can_edit_pos_calendar_data"))
{
	if(in_array(2, $user_roles) or in_array(3, $user_roles) or in_array(10, $user_roles) or in_array(80, $user_roles))
	{
		if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
		{
			$form->add_section("Project Dates");
		}
		else
		{
			$form->add_section("POS Opening Dates");
		}
		
		
		if($project["project_projectkind"] == 3
			or $project["project_projectkind"] == 9) //Take Over and renovation
		{
			$form->add_label("project_planned_takeover_date", "Client's Preferred Takover Date", 0, to_system_date($project["project_planned_takeover_date"]));

			$form->add_edit("project_actual_takeover_date", "Actual Takeover Date", 0, to_system_date($project["project_actual_takeover_date"]), TYPE_DATE, 20);
		}
		else
		{
			$form->add_hidden("project_actual_takeover_date");
		}

		$form->add_label("project_planned_opening_date", "Client's Preferred Opening Date", 0, to_system_date($project["project_planned_opening_date"]));

		

		

		if(count($tracking_info) > 0) {
			$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20, 0, 1, "changehistory");
			
		}
		else
		{
			$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20);
		}

		if($project["project_real_opening_date"] != NULL and $project["project_real_opening_date"] != '0000-00-00')
		{
			$form->add_edit("change_comment", "Reason for changing agreed opening date*", 0);
		}
		else
		{
			$form->add_hidden("change_comment");
		}
		

		$form->add_edit("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]), TYPE_DATE, 20);

		$form->add_section("POS Closing Dates");
		if($project["project_pos_subclass"] == 27 or $project["project_projectkind"] == 8) //temporary POS, PopUp
		{
			$form->add_edit("project_planned_closing_date", "Client's Planned Closing Date*", NOTNULL, to_system_Date($project["project_planned_closing_date"]), TYPE_DATE, 20);
		}
		else
		{
			$form->add_hidden("project_planned_closing_date");	
		}

		if($project["project_projectkind"] == 8) // popup
		{
			$form->add_hidden("shop_closing_date");
			$form->add_edit("popup_closing_date", "PopUp Closing Date", 0, to_system_date($project["project_popup_closingdate"]), TYPE_DATE, 20);
		}
		else
		{
			$form->add_hidden("popup_closing_date");
			$form->add_edit("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]), TYPE_DATE, 20);
		}
	}
	elseif($project["order_actual_order_state_code"] >= '800')
	{
		
		if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
		{
			$form->add_section("Project Dates");
		}
		else
		{
			$form->add_section("POS Opening Dates");

		}
		
		if($project["project_projectkind"] == 3
			or $project["project_projectkind"] == 9) //Take Over and renovation
		{
			$form->add_label("project_planned_takeover_date", "Client's Preferred Takover Date", 0, to_system_date($project["project_planned_takeover_date"]));

			$form->add_edit("project_actual_takeover_date", "Actual Takeover Date", 0, to_system_date($project["project_actual_takeover_date"]), TYPE_DATE, 20);
		}
		else
		{
			$form->add_hidden("project_actual_takeover_date");
		}

		$form->add_label("project_planned_opening_date", "Client's Preferred Opening Date", 0, to_system_date($project["project_planned_opening_date"]));

		

		

		if(count($tracking_info) > 0) {
			$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20, 0, 1, "changehistory");
			
		}
		else
		{
			$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20);
		}

		if($project["project_real_opening_date"] != NULL and $project["project_real_opening_date"] != '0000-00-00')
		{
			$form->add_edit("change_comment", "Reason for changing agreed opening date*", 0);
		}
		else
		{
			$form->add_hidden("change_comment");
		}

		$form->add_edit("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]), TYPE_DATE, 20);
		
		$form->add_section("POS Closing Dates");
		if($project["project_pos_subclass"] == 27 or $project["project_projectkind"] == 8) //temporary POS, PopUp
		{
			$form->add_edit("project_planned_closing_date", "Client's Planned Closing Date*", NOTNULL, to_system_Date($project["project_planned_closing_date"]), TYPE_DATE, 20);
		}
		else
		{
			$form->add_hidden("project_planned_closing_date");	
		}

		if($project["project_projectkind"] == 8) // popup
		{
			$form->add_hidden("shop_closing_date");
			$form->add_edit("popup_closing_date", "PopUp Closing Date", 0, to_system_date($project["project_popup_closingdate"]), TYPE_DATE, 20);
		}
		else
		{
			$form->add_hidden("popup_closing_date");
			$form->add_edit("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]), TYPE_DATE, 20);
		}
	}
	else
	{
		

		if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
		{
			$form->add_section("Project Dates");
		}
		else
		{
			$form->add_section("POS Opening Dates");

		}

		if($project["project_projectkind"] == 3
			or $project["project_projectkind"] == 9) //Take Over and renovation
		{
			$form->add_label("project_planned_takeover_date", "Client's Preferred Takover Date", 0, to_system_date($project["project_planned_takeover_date"]));

			$form->add_label("project_actual_takeover_date", "Actual Takeover Date", 0, to_system_date($project["project_actual_takeover_date"]));

		}
		else
		{
			$form->add_hidden("project_actual_takeover_date");
		}

		$form->add_label("project_planned_opening_date", "Client's Preferred Opening Date", 0, to_system_date($project["project_planned_opening_date"]));

		

		

		$form->add_label("shop_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]));
		$form->add_label("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]));

		$form->add_section("POS Closing Dates");
		if($project["project_pos_subclass"] == 27 or $project["project_projectkind"] == 8) //temporary POS, PopUp
		{
			$form->add_label("project_planned_closing_date", "Client's Planned Closing Date*", 0, to_system_Date($project["project_planned_closing_date"]));
		}
		else
		{
			$form->add_hidden("project_planned_closing_date");	
		}

		if($project["project_projectkind"] == 8) // popup
		{
			$form->add_hidden("shop_closing_date");
			$form->add_label("popup_closing_date", "PopUp Closing Date", 0, to_system_date($project["project_popup_closingdate"]));
		}
		else
		{
			$form->add_hidden("popup_closing_date");
			$form->add_label("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]));
		}
	}
}
elseif (has_access("can_edit_pos_calendar_data"))
{

	
	if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
	{
		$form->add_section("Project Dates");
	}
	else
	{
		$form->add_section("POS Opening Dates");
	}

	
	if($project["project_projectkind"] == 3
		or $project["project_projectkind"] == 9) //Take Over and renovation
	{
		$form->add_label("project_planned_takeover_date", "Client's Preferred Takover Date", 0, to_system_date($project["project_planned_takeover_date"]));

		$form->add_edit("project_actual_takeover_date", "Actual Takeover Date", 0, to_system_date($project["project_actual_takeover_date"]), TYPE_DATE, 20);
	}
	else
	{
		$form->add_hidden("project_actual_takeover_date");
	}

	$form->add_label("project_planned_opening_date", "Client's Preferred Opening Date", 0, to_system_date($project["project_planned_opening_date"]));

	

	

	if(count($tracking_info) > 0) {
	$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20, 0, 1, "changehistory");
		
	}
	else
	{
		$form->add_edit("shop_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]), TYPE_DATE, 20);
	}

	if($project["project_real_opening_date"] != NULL and $project["project_real_opening_date"] != '0000-00-00')
	{
		$form->add_edit("change_comment", "Reason for changing agreed opening date*", 0);
	}
	else
	{
		$form->add_hidden("change_comment");
	}

	$form->add_edit("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]), TYPE_DATE, 20);

	$form->add_section("POS Closing Dates");
	if($project["project_pos_subclass"] == 27 or $project["project_projectkind"] == 8) //temporary POS, PopUp
	{
		$form->add_edit("project_planned_closing_date", "Client's Planned Closing Date*", NOTNULL, to_system_Date($project["project_planned_closing_date"]), TYPE_DATE, 20);
	}
	else
	{
		$form->add_hidden("project_planned_closing_date");	
	}

	if($project["project_projectkind"] == 8) // popup
	{
		$form->add_hidden("shop_closing_date");
		$form->add_edit("popup_closing_date", "PopUp Closing Date", 0, to_system_date($project["project_popup_closingdate"]), TYPE_DATE, 20);
	}
	else
	{
		$form->add_hidden("popup_closing_date");
		$form->add_edit("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]), TYPE_DATE, 20);
	}
}
else
{

	
	if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
	{
		$form->add_section("Project Dates");
	}
	else
	{
		$form->add_section("POS Opening Dates");
	}
	
	if($project["project_projectkind"] == 3
		or $project["project_projectkind"] == 9) //Take Over and renovation
	{
		$form->add_label("project_planned_takeover_date", "Client's Preferred Takover Date", 0, to_system_date($project["project_planned_takeover_date"]));

		$form->add_label("project_actual_takeover_date", "Actual Takeover Date", 0, to_system_date($project["project_actual_takeover_date"]));
	}
	else
	{
		$form->add_hidden("project_actual_takeover_date");
	}

	$form->add_label("project_planned_opening_date", "Client's Preferred Opening Date", 0, to_system_date($project["project_planned_opening_date"]));

	

	

	$form->add_label("shop_real_opening_date", $project["projectkind_milestone_name_01"], 0, to_system_date($project["project_real_opening_date"]));
	$form->add_label("shop_actual_opening_date", $project["projectkind_milestone_name_02"], 0, to_system_date($project["project_actual_opening_date"]));
	
	if($project["project_pos_subclass"] == 27 or $project["project_projectkind"] == 8) //temporary POS, PopUp
	{
		$form->add_label("project_planned_closing_date", "Client's Planned Closing Date*", 0, to_system_Date($project["project_planned_closing_date"]));
	}
	else
	{
		$form->add_hidden("project_planned_closing_date");	
	}

	if($project["project_projectkind"] == 8) // popup
	{
		$form->add_hidden("shop_closing_date");
		$form->add_label("popup_closing_date", "PopUp Closing Date", 0, to_system_date($project["project_popup_closingdate"]));
	}
	else
	{
		$form->add_hidden("popup_closing_date");
		$form->add_label("shop_closing_date", "POS Closing Date", 0, to_system_date($project["project_shop_closingdate"]));
	}
	
}

$form->add_hidden("old_shop_actual_opening_date", to_system_date($project["project_actual_opening_date"]));
$form->add_hidden("old_shop_real_opening_date", to_system_date($project["project_real_opening_date"]));
$form->add_hidden("old_actual_takeover_date", to_system_date($project["project_actual_takeover_date"]));
$form->add_hidden("old_shop_closing_date", to_system_date($project["project_shop_closingdate"]));
$form->add_hidden("old_project_planned_closing_date", to_system_date($project["project_planned_closing_date"]));

$form->add_button("save", "Save Data");

/*
if ($form->value("popup_closing_date") && $project["project_state"] <> 5) { 
	mysql_query("UPDATE projects SET project_state = 5 WHERE project_id = ".param("pid") ); // ASE
}
*/

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("save"))
{	
    if($form->value("shop_actual_opening_date"))
	{
		if($project["project_projectkind"] != 5)
		{
			//$form->add_validation("from_system_date({shop_actual_opening_date}) <=  " . dbquote(date("Y-m-d")), "The actual POS Opening Date must not be a future date!");
			

			$form->add_validation("from_system_date({shop_actual_opening_date}) <=  " . dbquote(date("Y-m-d", strtotime("+3 days"))), "The actual POS opening date must be a date in the past or can only be at maximum three days in the future!");
		}


		if (has_access("can_edit_franchisee_data"))
		{
			$form->add_validation("{franchisee_address_company} != ''", "Please indicate the POS owner address (Company, Address, City, Country and Contact Name).");
			$form->add_validation("{franchisee_address_address} != ''", "Please indicate the POS owner address (Company, Address,City, Country and Contact Name).");
			$form->add_validation("{franchisee_address_place} != ''", "Please indicate the POS owner address (Company, Address, City, Country and Contact Name).");
			$form->add_validation("{franchisee_address_country} != ''", "Please indicate the POS owner address (Company, Address, City, Country and Contact Name).");
			$form->add_validation("{franchisee_address_contact} != ''", "Please indicate the POS owner address (Company, Address, City, Country and Contact Name).");
		}
	}

	if($form->value("shop_closing_date"))
	{ 
		$form->add_validation("from_system_date({shop_closing_date}) <=  " . dbquote(date("Y-m-d", strtotime("+2 days"))), "The POS closing date must be a date in the past or can only be at maximum two days in the future!");

	}

	if($form->value("popup_closing_date"))
	{  
		$form->add_validation("from_system_date({popup_closing_date}) <=  " . dbquote(date("Y-m-d", strtotime("+2 days"))), "The PopUp closing date must be a date in the past or can only be at maximum two days in the future!");

	}

	if($form->value("old_shop_real_opening_date") and $form->value("old_shop_real_opening_date") != $form->value("shop_real_opening_date"))
	{
		$form->add_validation("{change_comment} != ''", "Please indicate the reason for changing the agreed opening date!");
	}

	$form->add_validation("{shop_totalsqms} >= {shop_sqms}", "The total surface must not be less than the sales surface!");
	//$form->add_validation("{shop_grosssqms} >= {shop_totalsqms}", "The gross surface must be less than the total surface!");


	$error = 0;
	
	if($form->value("shop_closing_date"))
	{
		$closing_date = from_system_date($form->value("shop_closing_date"));
		$opening_date = from_system_date($form->value("shop_actual_opening_date"));
		if($closing_date < $opening_date)
		{
			$error = 3;
		}
	}
	

	if($form->value("project_planned_closing_date"))
	{
		$closing_date = from_system_date($form->value("project_planned_closing_date"));
		$opening_date = from_system_date($form->value("shop_actual_opening_date"));
		if($closing_date < $opening_date)
		{
			$error = 4;
		}
	}


	if($form->value("popup_closing_date"))
	{ 
		$closing_date = from_system_date($form->value("popup_closing_date"));
		$opening_date = from_system_date($form->value("shop_actual_opening_date"));
		
		if($closing_date < $opening_date)
		{ 
			$error = 3;
		}
	}

	if($project["project_projectkind"] !=5 
		and !$form->value("shop_actual_opening_date") 
		and $form->value("shop_real_opening_date")) {
		
		if(from_system_date($form->value("shop_real_opening_date")) <  date("Y-m-d")) {
			$error = 5;
		}
	}



	if(array_key_exists('shop_phone_number', $form->items))
	{
		$form->value("shop_address_phone", $form->unify_multi_edit_field($form->items["shop_phone_number"]));
	}

	if(array_key_exists('shop_mobile_phone_number', $form->items))
	{
		$form->value("shop_address_mobile_phone", $form->unify_multi_edit_field($form->items["shop_mobile_phone_number"]));
		
		
		if($project["project_projectkind"] != 1 
			and $project["project_projectkind"] != 6
			and $project["project_projectkind"] != 9)
		{
			//$form->add_validation("{shop_address_phone} != '' or {shop_address_mobile_phone} != ''", "POS address: Please indcate either the phone number or the mobile phone number!");
		}
	}

	if(array_key_exists('franchisee_phone_number', $form->items))
	{
		$form->value("franchisee_address_phone", $form->unify_multi_edit_field($form->items["franchisee_phone_number"]));
	}

	if(array_key_exists('franchisee_mobile_phone_number', $form->items))
	{
		$form->value("franchisee_address_mobile_phone", $form->unify_multi_edit_field($form->items["franchisee_mobile_phone_number"]));
		
		$form->add_validation("{franchisee_address_phone} != '' or {franchisee_address_mobile_phone} != ''", "Owner address: Please indcate either the phone number or the mobile phone number!");
	}



		
	if(!$form->value("create_new_franchisee_address") and $form->value("order_franchisee_address_id") == 0) {
	
		$form->error("Please select a POS owner address from the drop down or indicate that a new address schuld be created!");
	}
	elseif($error == 3)
	{
		$form->error("The POS closing date can not be a date in the past of the POS opening date!");
	}
	elseif($error == 4)
	{
		$form->error("The Planned POS closing date can not be a date in the past of the POS opening date!");
	}
	elseif($error == 5)
	{
		$form->error("The agreed opening date must be a future date.");
	}
	elseif ($form->validate())
    {
		
	   if($form->value("unit_of_measurement") == 1)
	   {
			$form->value("unit_of_measurement", 2);
			//$form->value("shop_grosssqms", feet_to_sqm($form->value("shop_grosssqms")));
			$form->value("shop_totalsqms", feet_to_sqm($form->value("shop_totalsqms")));
			$form->value("shop_sqms", feet_to_sqm($form->value("shop_sqms")));
			$form->value("shop_bakoffocesqms", feet_to_sqm($form->value("shop_bakoffocesqms")));
			$form->value("shop_floorsurface1", feet_to_sqm($form->value("shop_floorsurface1")));
			$form->value("shop_floorsurface2", feet_to_sqm($form->value("shop_floorsurface2")));
			$form->value("shop_floorsurface3", feet_to_sqm($form->value("shop_floorsurface3")));
	   }

	   if($form->value("shop_actual_opening_date"))
	   {
		   $form->value("overwrite_posaddress_data", 1);
	   }



       //update agreement data
	   if(has_access("can_edit_franchisee_agreement_data"))
	   {
		   if($project['project_projectkind'] == 1 
			   or $project['project_projectkind'] == 6
			   or $project['project_projectkind'] == 9
			   or $project['project_projectkind'] == 3)
		   {
			   if($form->value("old_shop_real_opening_date") != $form->value("shop_real_opening_date"))
			   {
					$realistic_opening_date = from_system_date($form->value("shop_real_opening_date"));

					//get the posaddress
					$sql = "select posaddress_id, posaddress_fagrstart " . 
						   "from posorderspipeline " . 
						   "left join posaddressespipeline on posaddress_id = posorder_posaddress " .
						   " where posorder_order = " . dbquote($project['project_order']);
					
					$res = mysql_query($sql) or dberror($sql);
					if ($row = mysql_fetch_assoc($res))
					{
						$pos_id = $row["posaddress_id"];

						
						if($realistic_opening_date)
						{
							$start = to_system_date($realistic_opening_date);
							$start = "01." . substr($start, 3, strlen($start)-1);


							$months = 13 - substr($realistic_opening_date, 5,2);
							$duration = "3 years and " . $months . " months";


							$end = to_system_date($realistic_opening_date);
							if(substr($end, 6, strlen($end)-1) < 10) {
								$end = "31.12.0" . (substr($end, 6, strlen($end)-1) + 3);
							}
							else
							{
								$end = "31.12." . (substr($end, 6, strlen($end)-1) + 3);
							}
						}
						else
						{
							$start = "";
							$end = "";

						}

						
						$form->value('project_fagrstart', $start);
						$form->value('project_fagrend', $end);

					}
			   }
		   }
		   else
		   {
			   $form->value('project_fagrstart', "");
			   $form->value('project_fagrend', "");
		   }
	   }

		
		if(array_key_exists('shop_street', $form->items))
		{
			$form->value("shop_address_address", $form->unify_multi_edit_field($form->items["shop_street"], get_country_street_number_rule($form->value("shop_address_country"))));
		}


			
		if(array_key_exists('franchisee_street', $form->items))
		{
			$form->value("franchisee_address_address", $form->unify_multi_edit_field($form->items["franchisee_street"], get_country_street_number_rule($form->value("franchisee_address_country"))));
		}
		
		
		
		$new_pos_id = project_update_pos_data($form, 0);
		mysql_select_db(RETAILNET_DB, $db);

		//update posareas
		$sql = "delete from $table2 where posarea_posaddress =" . dbquote($pos_data["posaddress_id"]);
		$result = $res = mysql_query($sql) or dberror($sql);
		foreach($posareas as $key=>$name)
		{
			if($form->value("area" . $key) == 1)
			{
				$fields = array();
				$values = array();

				$fields[] = "posarea_posaddress";
				$values[] = dbquote($pos_data["posaddress_id"]);

				$fields[] = "posarea_area";
				$values[] = dbquote($key);

				$fields[] = "date_created";
				$values[] = dbquote(date("Y-m-d"));

				$fields[] = "date_modified";
				$values[] = dbquote(date("Y-m-d"));

				$fields[] = "user_created";
				$values[] = dbquote(user_login());

				$fields[] = "user_modified";
				$values[] = dbquote(user_login());

				$sql = "insert into $table2 (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);
			}
		}


		

		if(!$form->value("shop_actual_opening_date"))
		{
			move_pos_data_to_pipeline($form);
		}

				
        $form->message("Your changes have been saved.");



		//create fake takeover project for teakover/renovation project
		if(!$form->value("old_actual_takeover_date") 
			and $form->value("project_actual_takeover_date")
			and $project["project_projectkind"] == 3)
		{

			//get latest project

			$sql =	"select * " .  
					"from posorders " .
					"left join orders on order_id = posorder_order " . 
					"where (order_actual_order_state_code <> '900' or order_actual_order_state_code is null) " . 
					" and posorder_type = 1 and posorder_posaddress = " . $pos_data["posaddress_id"] .
					" and (posorder_order is null or posorder_order <> " . $project["project_order"] . ") " . 
					" and posorder_opening_date is not null and posorder_opening_date <> '0000-00-00' " .  
					" order by posorder_year DESC, posorder_opening_date DESC";

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
		
				$fields = array();
				$values = array();


				$fields[] = "posorder_posaddress";
				$values[] = dbquote($pos_data["posaddress_id"]);

				$fields[] = "posorder_type";
				$values[] = 1;

				$fields[] = "posorder_ordernumber";
				$values[] = dbquote($project["project_number"]);

				$fields[] = "posorder_year";
				$values[] = dbquote(substr($form->value("project_actual_takeover_date"), 0, 4));

				$fields[] = "posorder_product_line";
				$values[] = dbquote($row["posorder_product_line"]);

				$fields[] = "posorder_product_line_subclass";
				$values[] = dbquote($row["posorder_product_line_subclass"]);

				$fields[] = "posorder_postype";
				$values[] = dbquote($row["posorder_postype"]);

				$fields[] = "posorder_subclass";
				$values[] = dbquote($row["posorder_subclass"]);

				$fields[] = "posorder_project_kind";
				$values[] = 4;

				$fields[] = "posorder_legal_type";
				$values[] = 1;

				$fields[] = "posorder_opening_date";
				$values[] = dbquote(from_system_date($form->value("project_actual_takeover_date")));

				$fields[] = "date_created";
				$values[] = dbquote(date("Y-m-d H:i:s"));

				$fields[] = "date_modified";
				$values[] = dbquote(date("Y-m-d H:i:s"));

				$fields[] = "user_created";
				$values[] = dbquote('system@retailnet');

				$fields[] = "user_modified";
				$values[] = dbquote(user_login());

				$sql = "insert into posorders (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);
			}
		}
		elseif(!$form->value("old_actual_takeover_date") 
			and $form->value("project_actual_takeover_date")
			and $project["project_projectkind"] == 9
			and count($relocated_pos) > 0)
		{
		
			$fields = array();
			$values = array();


			$fields[] = "posorder_posaddress";
			$values[] = dbquote($relocated_pos['posaddress_id']);

			$fields[] = "posorder_type";
			$values[] = 1;

			$fields[] = "posorder_ordernumber";
			$values[] = dbquote($project["project_number"]);

			$fields[] = "posorder_year";
			$values[] = dbquote(substr($form->value("project_actual_takeover_date"), 0, 4));

			$fields[] = "posorder_product_line";
			$values[] = dbquote($relocated_pos["posaddress_store_furniture"]);

			$fields[] = "posorder_product_line_subclass";
			$values[] = dbquote($relocated_pos["posaddress_store_furniture_subclass"]);
			
			$fields[] = "posorder_postype";
			$values[] = dbquote($relocated_pos['posaddress_store_postype']);

			$fields[] = "posorder_subclass";
			$values[] = dbquote($relocated_pos["posaddress_store_subclass"]);

			$fields[] = "posorder_project_kind";
			$values[] = 9;

			$fields[] = "posorder_legal_type";
			$values[] = 1;

			$fields[] = "posorder_opening_date";
			$values[] = dbquote(from_system_date($form->value("project_actual_takeover_date")));

			$fields[] = "date_created";
			$values[] = dbquote(date("Y-m-d H:i:s"));

			$fields[] = "date_modified";
			$values[] = dbquote(date("Y-m-d H:i:s"));

			$fields[] = "user_created";
			$values[] = dbquote('system@retailnet');

			$fields[] = "user_modified";
			$values[] = dbquote(user_login());

			$sql = "insert into posorders (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			mysql_query($sql) or dberror($sql);
		}



		$sql = "select project_number, project_postype, project_product_line, " .
			   "order_shop_address_company, order_shop_address_place, country_name " .
			   "from projects " . 
			   "left join orders on order_id = project_order " .
			   "left join countries on country_id = order_shop_address_country " .
			   "where project_id = " . param("pid");

		$res = mysql_query($sql) or dberror($sql);
		$row = mysql_fetch_assoc($res);
		$country_name = $row["country_name"];

		//send mail to recipient of mail_alert_type 1 if aggreed POS Opening Date was changed

		if(trim($form->value("old_shop_real_opening_date")) != trim($form->value("shop_real_opening_date")))
		{

			//project tracking
			$field = "project_real_opening_date";
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote($field) . ", " . 
				   dbquote(to_system_date($form->value("old_shop_real_opening_date"))) . ", " . 
				   dbquote(to_system_date($form->value("shop_real_opening_date"))) . ", " . 
				   dbquote($form->value("change_comment")) . ", " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
				   
			$result = mysql_query($sql) or dberror($sql);

			
			/*
			$sql = "select * from mail_alerts " .
				   "where (mail_alert_type = 1 or mail_alert_type = 2) " .
				   "and mail_alert_order = " . $project["project_order"];
			

			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
		    */
				$sql = "select * from mail_alert_types " . 
						   "where mail_alert_type_id = 1";

				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				
				$recipient = $row["mail_alert_type_sender_email"];

				$sql = "select user_id ".
					   "from users ".
					   "where user_email = '" . $recipient . "' ";
				
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				$reciepient_user_id = $row["user_id"];


				//project Leader
				$reciepient_user_id_rtc = $project["project_retail_coordinator"];
				$sql = "select user_email, concat(user_name, ' ', user_firstname) as username ".
					   "from users ".
					   "where user_id = '" . $reciepient_user_id_rtc . "' ";
				
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				$recipient_rtc = $row["user_email"];
				$recipient_rtc_name = $row["username"];
				


				//retail operator
				/*
				$reciepient_user_id_rto = $project["order_retail_operator"];
				$sql = "select user_email, concat(user_name, ' ', user_firstname) as username ".
					   "from users ".
					   "where user_id = '" . $reciepient_user_id_rto . "' ";
				
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				$recipient_rto = $row["user_email"];
				$recipient_rto_name = $row["username"];
				*/

				$reciepient_user_id_client = $project["order_user"];
				$sql = "select user_email, concat(user_name, ' ', user_firstname) as username ".
					   "from users ".
					   "where user_id = '" . $reciepient_user_id_client . "' ";
				
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				$recipient_client = $row["user_email"];
				$recipient_client_name = $row["username"];


				
				//travel retail projects
				//get areas

				$recipients_travel_emails = array();
				$posareas = array();
				
				if($project["project_projectkind"] == 1 
					or $project["project_projectkind"] == 6
					or $project["project_projectkind"] == 9) //new or relocation project
				{
					$sql_p = "select posarea_area from posorderspipeline " .
						   "left join posareaspipeline on posarea_posaddress = posorder_posaddress " . 
						   "where posorder_order = " . dbquote($project["project_order"]);
				}
				elseif($project["project_projectkind"] > 1) //renovation, takeover/renovation and takeover project
				{
					$sql_p = "select posarea_area from posorders " .
						   "left join posareas on posarea_posaddress = posorder_posaddress " . 
						   "where posorder_order = " . dbquote($project["project_order"]);
				}


				$res_p = mysql_query($sql_p) or dberror($sql_p);
				while ($row_p = mysql_fetch_assoc($res_p))
				{
					$posareas[$row_p["posarea_area"]] = $row_p["posarea_area"];
				}
				
				
				if(count($posareas) > 0)
				{
					if(count($posareas) > 0)
					{
						$filter = " IN (" . implode(',', $posareas ) . ')';
					}

					$sql_r = 'select * from posareatypes ' . 
						   'where posareatype_id ' . $filter;


					$res_r = mysql_query($sql_r) or dberror($sql_r);
					while ($row_r = mysql_fetch_assoc($res_r))
					{
						
						if($row_r["posareatype_email1"])
						{

							$sql = 'select user_id from users ' . 
									   'where user_active = 1 and user_email = ' . dbquote($row_r["posareatype_email1"]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$recipients_travel_emails[$row_u["user_id"]] = $row_r["posareatype_email1"];
							}
							
						}

						
						if($row_r["posareatype_email2"])
						{
							
							$sql = 'select user_id from users ' . 
									   'where user_active = 1 and user_email = ' . dbquote($row_r["posareatype_email2"]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$recipients_travel_emails[$row_u["user_id"]] = $row_r["posareatype_email2"];
							}
							
						}

						if($row_r["posareatype_email3"])
						{
							
							$sql = 'select user_id from users ' . 
									   'where user_active = 1 and user_email = ' . dbquote($row_r["posareatype_email3"]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$recipients_travel_emails[$row_u["user_id"]] = $row_r["posareatype_email3"];
							}
							
						}

						if($row_r["posareatype_email4"])
						{
							
							$sql = 'select user_id from users ' . 
									   'where user_active = 1 and user_email = ' . dbquote($row_r["posareatype_email4"]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$recipients_travel_emails[$row_u["user_id"]] = $row_r["posareatype_email4"];
							}
							
						}

						if($row_r["posareatype_email5"])
						{
							
							$sql = 'select user_id from users ' . 
									   'where user_active = 1 and user_email = ' . dbquote($row_r["posareatype_email5"]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$recipients_travel_emails[$row_u["user_id"]] = $row_r["posareatype_email5"];
							}
							
						}

						if($row_r["posareatype_email6"])
						{
							
							$sql = 'select user_id from users ' . 
									   'where user_active = 1 and user_email = ' . dbquote($row_r["posareatype_email6"]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$recipients_travel_emails[$row_u["user_id"]] = $row_r["posareatype_email6"];
							}
							
						}
						
					}
				}

				if($project["project_pos_subclass"] == 17) // duty free
				{
					$sql_r = 'select * from possubclasses ' . 
						   'where possubclass_id = 17 ';


					$res_r = mysql_query($sql_r) or dberror($sql_r);
					while ($row_r = mysql_fetch_assoc($res_r))
					{
						if($row_r["possubclass_email1"])
						{

							$sql = 'select user_id from users ' . 
									   'where user_active = 1 and user_email = ' . dbquote($row_r["possubclass_email1"]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$recipients_travel_emails[$row_u["user_id"]] = $row_r["possubclass_email1"];
							}
							
						}

						if($row_r["possubclass_email2"])
						{

							$sql = 'select user_id from users ' . 
									   'where user_active = 1 and user_email = ' . dbquote($row_r["possubclass_email2"]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$recipients_travel_emails[$row_u["user_id"]] = $row_r["possubclass_email2"];
							}
							
						}

						if($row_r["possubclass_email3"])
						{

							$sql = 'select user_id from users ' . 
									   'where user_active = 1 and user_email = ' . dbquote($row_r["possubclass_email3"]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$recipients_travel_emails[$row_u["user_id"]] = $row_r["possubclass_email3"];
							}
							
						}

						if($row_r["possubclass_email4"])
						{

							$sql = 'select user_id from users ' . 
									   'where user_active = 1 and user_email = ' . dbquote($row_r["possubclass_email4"]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$recipients_travel_emails[$row_u["user_id"]] = $row_r["possubclass_email4"];
							}
							
						}

						if($row_r["possubclass_email5"])
						{

							$sql = 'select user_id from users ' . 
									   'where user_active = 1 and user_email = ' . dbquote($row_r["possubclass_email5"]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$recipients_travel_emails[$row_u["user_id"]] = $row_r["possubclass_email5"];
							}
							
						}

						if($row_r["possubclass_email6"])
						{

							$sql = 'select user_id from users ' . 
									   'where user_active = 1 and user_email = ' . dbquote($row_r["possubclass_email6"]);

							$res_u = mysql_query($sql) or dberror($sql);
							if ($row_u = mysql_fetch_assoc($res_u))
							{
								$recipients_travel_emails[$row_u["user_id"]] = $row_r["possubclass_email6"];
							}
						}
					}
				}

				//users for regional responsabilities
				$regional_responsable_reciepients = get_regional_responsible($project["project_cost_type"], $project["order_client_address"]);

				$sql = "select user_id, user_email, concat(user_name, ' ', user_firstname) as username ".
					   "from users ".
					   "where (user_id = '" . user_id() . "' " . 
					   "   and user_active = 1)";
				
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				
				$sender_id = $row["user_id"];
				$sender_email = $row["user_email"];
				$sender_name = $row["username"];

				
				if($form->value("old_shop_real_opening_date"))
				{
					$fromto = "from " . $form->value("old_shop_real_opening_date") . " to " .  $form->value("shop_real_opening_date");
				}
				else
				{
					$fromto = " to " .  $form->value("shop_real_opening_date");
				}

				if($project["project_projectkind"] == 4) //Take Over 
				{
					$subject = "Project's planned takeover was changed - Project " . $project["order_number"] . ", " . $country_name . ", " . $project["order_shop_address_company"];
					$bodytext0 = $sender_name . " has changed the project's expected ending date " . $fromto;
				}
				elseif($project["project_projectkind"] == 5) //lease renewal
				{
					$subject = "Project's lease starting was changed - Project " . $project["order_number"] . ", " . $country_name . ", " . $project["order_shop_address_company"];
					$bodytext0 = $sender_name . " has changed the project's expected ending date " . $fromto;
				}
				else
				{
					$subject = "Agreed POS opening date was changed - Project " . $project["order_number"] . ", " . $country_name . ", " . $project["order_shop_address_company"];
					$bodytext0 = $sender_name . " has changed the agreed POS Opening Date of the project " . $fromto;
				}


				if($form->value("change_comment"))
				{
					$bodytext0 .= "\n" . "The reason is : " . $form->value("change_comment");
				}

			

				$mail = new Mail();
				$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
				$mail->set_sender($sender_email, $sender_name);
				$mail->add_recipient($recipient);

				if($recipient_rtc_name) 
				{
					$mail->add_cc($recipient_rtc, $recipient_rtc_name);
				}
				/*
				if($recipient_rto_name) 
				{
					$mail->add_cc($recipient_rto, $recipient_rto_name);
				}
				*/

				if($recipient_client_name) 
				{
					$mail->add_cc($recipient_client, $recipient_client_name);
				}

				
				foreach($recipients_travel_emails as $user_id=>$email)
				{
					$mail->add_cc($email);
				}

				foreach($regional_responsable_reciepients as $user_id=>$email)
				{
					$mail->add_cc($email);
				}

		
				$link ="project_edit_pos_data.php?pid=" . param("pid");
				$bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
				$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";
				         
				$mail->add_text($bodytext);
				
				if($senmail_activated == true)
				{
					$result = $mail->send();
				}
				else
				{
					$result = 1;
				}

				$sql = "delete from mail_alerts where mail_alert_type = 1 " .
					   "and mail_alert_order = " . $project["project_order"];
				mysql_query($sql) or dberror($sql);

				append_mail($project["project_order"], $reciepient_user_id, user_id(), $bodytext0, "910", 1);

				if($recipient_rtc_name) 
				{
					append_mail($project["project_order"], $reciepient_user_id_rtc, user_id(), $bodytext0, "910", 1, 0, 1);
				}

				/*
				if($recipient_rto_name) 
				{
					append_mail($project["project_order"], $reciepient_user_id_rto, user_id(), $bodytext0, "910", 1, 0, 1);
				}
				*/

				if($recipient_client_name) 
				{
					append_mail($project["project_order"], $reciepient_user_id_client, user_id(), $bodytext0, "910", 1, 0, 1);
				}

				foreach($recipients_travel_emails as $user_id=>$email)
				{
					append_mail($project["project_order"], $user_id, user_id(), $bodytext0, "910", 1, 0, 1);
				}

				
				foreach($regional_responsable_reciepients as $user_id=>$email)
				{
					append_mail($project["project_order"], $user_id, user_id(), $bodytext0, "910", 1, 0, 1);
				}

				
				$form->value("old_shop_real_opening_date", $form->value("shop_real_opening_date"));

	
			//}
		}
		
		
		//send mail to recipient of mail_alert_type 2 if actual POS Opening Date was changed
		if(trim($form->value("old_shop_actual_opening_date")) != trim($form->value("shop_actual_opening_date")))
		{
			
			//project tracking
			$field = "project_actual_opening_date";
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote($field) . ", " . 
				   dbquote(to_system_date($form->value("old_shop_actual_opening_date"))) . ", " . 
				   dbquote(to_system_date($form->value("shop_actual_opening_date"))) . ", " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
				   
			$result = mysql_query($sql) or dberror($sql);
			
			
			/*
			$sql = "select * from mail_alerts " .
				   "where (mail_alert_type = 1 or mail_alert_type = 2) " .
				   "and mail_alert_order = " . $project["project_order"];
			
			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
			*/
				/*
				if($row["mail_alert_type"] == 1)
				{
					$sql = "select * from mail_alert_types " .
						   "where mail_alert_type_id = 1";
				}
				else
				{
					$sql = "select * from mail_alert_types " .
						   "where mail_alert_type_id = 2";
				}
				*/


				$sql = "select * from mail_alert_types " .
						   "where mail_alert_type_id = 2";

				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				
				$recipient = $row["mail_alert_type_sender_email"];
				
				$sql = "select user_id ".
					   "from users ".
					   "where user_email = '" . $recipient . "' ";
		
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				$reciepient_user_id = $row["user_id"];


				$sql = "select user_id, user_email, concat(user_name, ' ', user_firstname) as username ".
					   "from users ".
					   "where (user_id = '" . user_id() . "' " . 
					   "   and user_active = 1)";
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				$sender_id = $row["user_id"];
				$sender_email = $row["user_email"];
				$sender_name = $row["username"];


				//mail to design supervisor in case the PM is not a HQ PM
				//check if PM is HQ PM
				$cc_recipient_user_id = "";
				$cc_recipient_email = "";
				$cc_recipient_name = "";

				$pm_recipient_user_id = "";
				$pm_recipient_email = "";
				$pm_recipient_name = "";


				$sql = "select user_address, user_id, user_email, concat(user_name, ' ', user_firstname) as username  " . 
					     " from users " . 
					     " where user_id = " . $project["project_retail_coordinator"];

				
				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);

				$pm_recipient_user_id = $row["user_id"];
				$pm_recipient_email = $row["user_email"];
				$pm_recipient_name =$row["username"];

				if($row["user_address"] != 13) //Swatch HQ
				{
					
					
					
					if($project["project_design_supervisor"] > 0)
					{
						$sql = "select user_id, user_email, concat(user_name, ' ', user_firstname) as username ".
							   "from users ".
							   "where (user_id = '" .  $project["project_design_supervisor"] . "' " . 
							   "   and user_active = 1)";
						$res = mysql_query($sql) or dberror($sql);
						$row = mysql_fetch_assoc($res);

						$cc_recipient_user_id = $row["user_id"];
						$cc_recipient_email = $row["user_email"];
						$cc_recipient_name =$row["username"];
					}
				}


				



				//users for regional responsabilities
				$regional_responsable_reciepients = array();
				
				//get users responsible for the country
				//retail 
				if($project["project_cost_type"] == 1
					or $project["project_cost_type"] == 3
					or $project["project_cost_type"] == 4
					or $project["project_cost_type"] == 5)
				{
					$sql = "select user_company_responsible_user_id, user_email " . 
						   "from user_company_responsibles " . 
						   "left join users on user_id = user_company_responsible_user_id " . 
						   "where user_company_responsible_address_id = " . $project["order_client_address"] . 
						   " and user_company_responsible_retail = 1";
				}
				else // wholesale
				{
					$sql = "select user_company_responsible_user_id, user_email " . 
						   "from user_company_responsibles " . 
						   "left join users on user_id = user_company_responsible_user_id " . 
						   "where user_company_responsible_address_id = " . $project["order_client_address"] . 
						   " and user_company_responsible_wholsale = 1";
				}

				$res = mysql_query($sql) or dberror($sql);
				while($row = mysql_fetch_assoc($res))
				{
					$regional_responsable_reciepients[$row["user_company_responsible_user_id"]]  = $row["user_email"];
				}
				



				if($form->value("old_shop_actual_opening_date"))
				{
					if($project["project_projectkind"] == 4) //Take Over 
					{
						$subject = "Project's take over was changed - Project " . $project["order_number"] . ", " . $country_name . ", " . $project["order_shop_address_company"];
						$bodytext0 = $sender_name . " has changed the project's actual ending date.";
					}
					elseif($project["project_projectkind"] == 5) //lease renewal
					{
						$subject = "Project's lease starting date was changed - Project " . $project["order_number"] . ", " . $country_name . ", " . $project["order_shop_address_company"];
						$bodytext0 = $sender_name . " has changed the project's actual ending date.";
					}
					else
					{
						
						$subject = "Actual POS opening date was changed - Project " . $project["order_number"] . ", " . $country_name . ", " . $project["order_shop_address_company"];
						$bodytext0 = $sender_name . " has changed the actual POS Opening Date of the project.";
					}
				}
				else
				{
					if($project["project_projectkind"] == 4) //Take Over 
					{
						$subject = "Project's takeover date was entered - Project " . $project["order_number"] . ", " . $country_name . ", " . $project["order_shop_address_company"];
						$bodytext0 = $sender_name . " has entered the project's actual ending date.";
					}
					elseif($project["project_projectkind"] == 5) //Take Over and lease renewal
					{
						$subject = "Project's lease starting date was entered - Project " . $project["order_number"] . ", " . $country_name . ", " . $project["order_shop_address_company"];
						$bodytext0 = $sender_name . " has entered the project's actual ending date.";
					}
					else
					{
						
						$subject = "Actual POS opening date was entered - Project " . $project["order_number"] . ", " . $country_name . ", " . $project["order_shop_address_company"];
						$bodytext0 = $sender_name . " has entered the actual POS Opening Date of the project.";
					}
				}

				$mail = new Mail();
				$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
				$mail->set_sender($sender_email, $sender_name);
				$mail->add_recipient($recipient);

				if($cc_recipient_email)
				{
					$mail->add_cc($cc_recipient_email, $cc_recipient_name);
				}

				if($pm_recipient_email)
				{
					$mail->add_cc($pm_recipient_email, $pm_recipient_name);
				}


				foreach($regional_responsable_reciepients as $user_id=>$email)
				{
					$mail->add_cc($email);
				}

				$link ="project_task_center.php?pid=" . param("pid");
				$bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
				$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";    

				$mail->add_text($bodytext);
				
				
				if($senmail_activated == true)
				{
					$result = $mail->send();
				}
				else
				{
					$result = 1;
				}

				$sql = "delete from mail_alerts where mail_alert_type IN (1, 13, 15, 2, 17, 18) " .
					   "and mail_alert_order = " . $project["project_order"];
				mysql_query($sql) or dberror($sql);

				append_mail($project["project_order"], $reciepient_user_id, user_id(), $bodytext0, "910", 1);
				if($cc_recipient_user_id > 0)
				{
					append_mail($project["project_order"], $cc_recipient_user_id, user_id(), $bodytext0, "910", 1, 0, 1);
				}

				if($pm_recipient_user_id > 0)
				{
					append_mail($project["project_order"], $pm_recipient_user_id, user_id(), $bodytext0, "910", 1, 0, 1);
				}


				foreach($regional_responsable_reciepients as $user_id=>$email)
				{
					append_mail($project["project_order"], $user_id, user_id(), $bodytext0, "910", 1, 0, 1);
				}

				$form->value("old_shop_actual_opening_date", $form->value("shop_actual_opening_date"));
				
			//}
		}


		


		//send mail in case pos opening was entered but distribution channel is missing
		if($form->value("old_shop_actual_opening_date") and !$project["project_distribution_channel"] and !$form->value("posaddress_distribution_channel"))
		{
			$sql = "select * from mail_alert_types " .
					   "where mail_alert_type_id = 22";

			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);
			
			$recipient = $row["mail_alert_type_sender_email"];

			$mail_alert_text  = $row["mail_alert_mail_text"];
			
			$sql = "select user_id ".
				   "from users ".
				   "where user_email = '" . $recipient . "' ";
			
			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);
			$reciepient_user_id = $row["user_id"];


			$sql = "select user_id, user_email, concat(user_name, ' ', user_firstname) as username ".
				   "from users ".
				   "where (user_id = '" . user_id() . "' " . 
				   "   and user_active = 1)";
			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);
			$sender_id = $row["user_id"];
			$sender_email = $row["user_email"];
			$sender_name = $row["username"];


			
			$subject = "Distribution channel is missing - Project " . $project["order_number"] . ", " . $country_name . ", " . $project["order_shop_address_company"];
			$bodytext0 = $mail_alert_text;


			$mail = new Mail();
			$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
			$mail->set_sender($sender_email, $sender_name);
			$mail->add_recipient($recipient);

			
			$link ="project_edit_pos_data.php?pid=" . param("pid");
			$bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
			$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
		

			$mail->add_text($bodytext);
			
			
			if($senmail_activated == true)
			{
				$result = $mail->send();
			}
			else
			{
				$result = 1;
			}

			append_mail($project["project_order"], $reciepient_user_id, user_id(), $bodytext, "910", 1);
			
		}



		//send reminder to enter closing date of a POS location being relocated
		if($form->value("shop_actual_opening_date")
			and $project["project_relocated_posaddress_id"] > 0)
		{
			//check if relocated pos has a closing date
			$sql = "select posaddress_store_closingdate " . 
				   " from posaddresses " . 
				   " where posaddress_id = " . $project["project_relocated_posaddress_id"];
			
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				if($row["posaddress_store_closingdate"] == NULL or $row["posaddress_store_closingdate"] == '0000-00-00')
				{
					
					//get latest project of relocated POS
					$sql =	"select project_id " .  
							"from posorders " .
							"left join orders on order_id = posorder_order " . 
						    "left join projects on project_order = order_id " . 
							"where (order_actual_order_state_code <> '900' or order_actual_order_state_code is null) " . 
							" and posorder_type = 1 and posorder_posaddress = " . $project["project_relocated_posaddress_id"] .
							" and posorder_opening_date is not null and posorder_opening_date <> '0000-00-00' " .  
							" order by posorder_year DESC, posorder_opening_date DESC";

					$res = mysql_query($sql) or dberror($sql);
					if ($row = mysql_fetch_assoc($res))
					{
						
						if($row["project_id"] > 0)
						{
							$relocated_project = get_project($row["project_id"]);
						
							//get mail params
							$sql = "select * from mail_alert_types " .
								   "where mail_alert_type_id = 26";

							$res = mysql_query($sql) or dberror($sql);
							$row = mysql_fetch_assoc($res);


							$sender_email = $row["mail_alert_type_sender_email"];
							$sender_name = $row["mail_alert_type_sender_name"];
							
							$text = $row["mail_alert_mail_text"];
							$cc1 = $row["mail_alert_type_cc1"];
							$cc2 = $row["mail_alert_type_cc2"];
							$cc3 = $row["mail_alert_type_cc3"];
							$cc4 = $row["mail_alert_type_cc4"];
							
							
							
							$cc_recipients_email = array();

							if($cc1)
							{
								$cc_recipients_email[$cc1] = $cc1;
							}
							if($cc2)
							{
								$cc_recipients_email[$cc1] = $cc2;
							}
							if($cc3)
							{
								$cc_recipients_email[$cc1] = $cc3;
							}
							if($cc4)
							{
								$cc_recipients_email[$cc1] = $cc4;
							}


							//get client
							$sql = "select user_id, user_email, user_email_cc, user_email_deputy, ".
								   "concat(user_firstname, ' ', user_name) as username " . 
								   "from users ".
								   "where (user_id = '" . $project["order_user"] . "' " . 
								   "   and user_active = 1)";
							$res = mysql_query($sql) or dberror($sql);
							$row = mysql_fetch_assoc($res);
							$recipient_id = $row["user_id"];
							$recipient_email = $row["user_email"];
							$recipient_name = $row["username"];

							if($row["user_email_cc"])
							{
								$cc_recipients_email[$row["user_email_cc"]] = $row["user_email_cc"];
							}
							
							if($row["user_email_deputy"])
							{
								$cc_recipients_email[$row["user_email_deputy"]] = $row["user_email_deputy"];
							}

							//get Project Leader
							$sql = "select user_id, user_email, user_email_cc, user_email_deputy, ".
								   "concat(user_firstname, ' ', user_name) as username " . 
								   "from users ".
								   "where (user_id = '" . $project["project_retail_coordinator"] . "' " . 
								   "   and user_active = 1)";
							$res = mysql_query($sql) or dberror($sql);
							$row = mysql_fetch_assoc($res);
							
							if($row["user_email"])
							{
								$cc_recipients_email[$row["user_email"]] = $row["user_email"];
							}

							if($row["user_email_cc"])
							{
								$cc_recipients_email[$row["user_email_cc"]] = $row["user_email_cc"];
							}
							
							if($row["user_email_deputy"])
							{
								$cc_recipients_email[$row["user_email_deputy"]] = $row["user_email_deputy"];
							}
							
							
							$tmp_project = $project["order_number"] . ", " . $project["order_shop_address_country_name"] . ", " . $project["order_shop_address_company"];

							$tmp_relocated_project = $relocated_project["order_number"] . ", " . $relocated_project["order_shop_address_country_name"] . ", " . $relocated_project["order_shop_address_company"];

							
							
							$link = APPLICATION_URL . "/mps/pos/address/" . $project["project_relocated_posaddress_id"];
							
							
							$subject = "Closing Date must be entered - Project " . $tmp_relocated_project;
							
							$bodytext = str_replace('{recipient_name}', $recipient_name, $text);
							$bodytext = str_replace('{pos_location}', $tmp_project, $bodytext);
							$bodytext = str_replace('{relocated_pos}', $tmp_relocated_project, $bodytext);
							$bodytext = str_replace('{link}', $link, $bodytext);
							$bodytext = str_replace('{sender_name}', $sender_name, $bodytext);
							

							$mail = new Mail();
							$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
							$mail->set_sender($sender_email, $sender_name);
							$mail->add_recipient($recipient_email, $recipient_name);

							foreach($cc_recipients_email as $email)
							{
								$mail->add_cc($email);
							}
							

							$mail->add_text($bodytext);
							
							
							if($senmail_activated == true)
							{
								$result = $mail->send();
							}
							else
							{
								$result = 1;
							}

							
							$sender_id = 0;
							$sql = "select user_id from users " . 
								   "where user_email = ". dbquote($sender_email);
							$res = mysql_query($sql) or dberror($sql);
							if($row = mysql_fetch_assoc($res))
							{
								append_mail($relocated_project["project_order"], $recipient_id, $row["user_id"], $bodytext, "910", 1);
								$sender_id = $row["user_id"];
							}
							
							foreach($cc_recipients_email as $email)
							{
								$sql = "select user_id from users " . 
										"where user_email = ". dbquote($email);
								$res = mysql_query($sql) or dberror($sql);
								if($row = mysql_fetch_assoc($res))
								{
									append_mail($relocated_project["project_order"], $row["user_id"], $sender_id, $bodytext, "910", 1);
								}
							}
							
						}
					}
				}
			}
		}

		//put project to archive in case of opening date and approval
		if($project["project_projectkind"] == 4 or $project["project_projectkind"] == 5) //Take Over and lease renewal
		{
			if($form->value("shop_actual_opening_date"))
			{
				append_order_state($project["project_order"], '890', 1, 1);
				set_archive_date($project["project_order"]);

				// send email notification for project types
				$sql = "select project_number, project_postype, project_product_line,  " .
					   "order_shop_address_company, order_shop_address_place, country_name, " .
					   "user_firstname, user_name, user_email from projects " . 
					   "left join orders on order_id = project_order " .
					   "left join countries on country_id = order_shop_address_country " .
					   "left join users on user_id =  " . user_id() .
					   "   where project_id = " . $project["project_id"];

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{        
					
					// send mail to cost manager
					$project_postype = $row["project_postype"];
					$project_product_line = $row["project_product_line"];
					
					$subject = MAIL_SUBJECT_PREFIX . ": Project was put to the records - Project " . $row["project_number"] . ", " . $row["country_name"] . ", " . $row["order_shop_address_company"];

					$sender_email = $row["user_email"];
					$sender_name =  $row["user_name"] . " " . $row["user_firstname"];

					$bodytext0 = "The project was put to the records by by " . $sender_name;

					$mailaddress = 0;

					$mail = new Mail();
					$mail->set_subject($subject);
					$mail->set_sender($sender_email, $sender_name);

					$sql = "select * from postype_notifications " .
						   "where postype_notification_postype = " . $project_postype . 
						   " and postype_notification_prodcut_line = " . $project_product_line;

					$res = mysql_query($sql) or dberror($sql);
					while ($row = mysql_fetch_assoc($res))
					{
						if($row["postype_notification_email3"])
						{
							$sql_u = "select user_id from users " .
									 "where user_email = '" . $row["postype_notification_email3"] .  "'";
							
							$res_u = mysql_query($sql_u) or dberror($sql_u);
							
							if($row_u = mysql_fetch_assoc($res_u))
							{
								$recepient_id = $row_u["user_id"];
							}
							else
							{
								$recepient_id = '0';
							}

							$mail->add_recipient($row["postype_notification_email3"]);
							$mailaddress = 1;
						}
					}
					
					$link ="project_view_client_data.php?pid=" . $project["project_id"];
					$bodytext = $bodytext0 . "\nclick below to have direct access to the project:\n";
					$bodytext = $bodytext .  APPLICATION_URL . "/archive/" . $link . "\n\n";           
					$mail->add_text($bodytext);
					
					if($mailaddress == 1)
					{
						if($senmail_activated == true)
						{
							$result = $mail->send();
						}
						else
						{
							$result = 1;
						}
						append_mail($project["project_order"], $recepient_id, user_id(), $bodytext, MOVED_TO_THE_RECORDS, 1);
						
					}
				}
			}
		}
		else
		{
			/* not used anymore since the introduction os separate steps for CMS approvals in 2015
			if($form->value("shop_actual_opening_date") and $project["project_cost_cms_approved"] == 1)
			{
				append_order_state($project["project_order"], '890', 1, 1);
				set_archive_date($project["project_order"]);

				// send email notification for project types
				$sql = "select project_number, project_postype, project_product_line,  " .
					   "order_shop_address_company, order_shop_address_place, country_name, " .
					   "user_firstname, user_name, user_email from projects " . 
					   "left join orders on order_id = project_order " .
					   "left join countries on country_id = order_shop_address_country " .
					   "left join users on user_id =  " . user_id() .
					   "   where project_id = " . $project["project_id"];

				$res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{        
					
					// send mail to cost manager
					$project_postype = $row["project_postype"];
					$project_product_line = $row["project_product_line"];
					
					$subject = MAIL_SUBJECT_PREFIX . ": Project was put to the records - Project " . $row["project_number"] . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];

					$sender_email = $row["user_email"];
					$sender_name =  $row["user_name"] . " " . $row["user_firstname"];

					$bodytext0 = "The project was put to the records by by " . $sender_name;

					$mailaddress = 0;

					$mail = new Mail();
					$mail->set_subject($subject);
					$mail->set_sender($sender_email, $sender_name);

					$sql = "select * from postype_notifications " .
						   "where postype_notification_postype = " . $project_postype . 
						   " and postype_notification_prodcut_line = " . $project_product_line;

					$res = mysql_query($sql) or dberror($sql);
					while ($row = mysql_fetch_assoc($res))
					{
						if($row["postype_notification_email3"])
						{
							$sql_u = "select user_id from users " .
									 "where user_email = '" . $row["postype_notification_email3"] .  "'";
							
							$res_u = mysql_query($sql_u) or dberror($sql_u);
							
							if($row_u = mysql_fetch_assoc($res_u))
							{
								$recepient_id = $row_u["user_id"];
							}
							else
							{
								$recepient_id = '0';
							}

							$mail->add_recipient($row["postype_notification_email3"]);
							$mailaddress = 1;
						}
					}
					
					$link ="project_view_client_data.php?pid=" . $project["project_id"];
					$bodytext = $bodytext0 . "\nclick below to have direct access to the project:\n";
					$bodytext = $bodytext .  APPLICATION_URL . "/archive/" . $link . "\n\n";           
					$mail->add_text($bodytext);
					
					if($mailaddress == 1)
					{
						if($senmail_activated == true)
						{
							$result = $mail->send();
						}
						else
						{
							$result = 1;
						}
						append_mail($project["project_order"], $recepient_id, user_id(), $bodytext, MOVED_TO_THE_RECORDS, 1);
						
					}
				}
			}
			*/

			//put project to archive (used instead
			if($form->value("shop_actual_opening_date"))
			{
				$pos_pictures_uploaded = false;
				$sql = "select count(order_file_id) as num_recs from order_files " . 
					   " where order_file_category = 11 " . 
					   " and order_file_order = " . dbquote($project["project_order"]);


				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				if ($row["num_recs"] > 0)
				{
					$pos_pictures_uploaded = true;
				}

				
				
				if($project["project_cost_type"] == 1) //corporate
				{
					if($pos_pictures_uploaded == true 
						and $project["project_cost_cms_controlled"] == 1
						and $form->value("shop_actual_opening_date") != ''
						and ($project["order_archive_date"] == NULL 
						or $project["order_archive_date"] == '0000-00-00'))
					{
						if($project["order_actual_order_state_code"] < '890')
						{
							append_order_state($project["project_order"], '890', 1, 1);
							set_archive_date($project["project_order"]);
						}
					}
				}
				else
				{
					if($pos_pictures_uploaded == true 
						and $project["project_cost_cms_approved"] == 1
						and $form->value("shop_actual_opening_date") != ''
						and ($project["order_archive_date"] == NULL 
						or $project["order_archive_date"] == '0000-00-00'))
					{
						if($project["order_actual_order_state_code"] < '890')
						{
							append_order_state($project["project_order"], '890', 1, 1);
							set_archive_date($project["project_order"]);
						}
					}
				}
			}
		}

		
		//send mail to enter SAP Number in case the opening date was entered
		if(param("shop_actual_opening_date") and param("old_shop_actual_opening_date") == '')
		{
			if($new_pos_id > 0 and ($pos_data["posaddress_sapnumber"] == NULL or $pos_data["posaddress_sapnumber"] == ''))
			{
				$pos_location = $pos_data["country_name"] . ", " . $pos_data["posaddress_name"] . ", " . $pos_data["place_name"];

				$sql = "select * from mail_alert_types " .
					   "where mail_alert_type_id = 33";

				$res = mysql_query($sql) or dberror($sql);
				$row = mysql_fetch_assoc($res);
				
				$sender_mail = $row["mail_alert_type_sender_email"];
				$sender_name = $row["mail_alert_type_sender_name"];
				$cc1 = $row["mail_alert_type_cc1"];
				$cc2 = $row["mail_alert_type_cc2"];
				$cc3 = $row["mail_alert_type_cc3"];
				$cc4 = $row["mail_alert_type_cc4"];

				$mail_alert_text  = $row["mail_alert_mail_text"];
				$recipient_id = 0;
				
				$sql = "select user_id, user_email, user_firstname, " . 
					   "concat(user_name, ' ', user_firstname) as username, ".
					   "user_email_cc, user_email_deputy " . 
					   "from users ".
					   "where (user_id = '" . $project["order_user"] . "' " . 
					   "   and user_active = 1)";
				$res = mysql_query($sql) or dberror($sql);
				
				
				if($row = mysql_fetch_assoc($res))
				{
					$recipient_id = $row["user_id"];
					$recipient_email = $row["user_email"];
					$recipient_name = $row["username"];
					$recipeint_firstname = $row["user_firstname"];
				}
				else // get brand manager
				{
					//get brand manager
					$sql_c = "select user_id, user_firstname, user_name, user_email, " . 
						     "concat(user_name, ' ', user_firstname) as username ".
						     "from users " .
							 "left join user_roles on user_role_user = user_id " . 
							 "where user_address = " . $project["order_client_address"] . 
							 " and user_role_role = 15 and user_active = 1";

					$res_c = mysql_query($sql_c) or dberror($sql_c);

					if($row_c = mysql_fetch_assoc($res_c))
					{
						$recipient_id = $row_c["user_id"];
						$recipient_email = $row_c["user_email"];
						$recipient_name = $row_c["username"];
						$recipeint_firstname = $row_c["user_firstname"];
					
					}
					else // get brand manager in case of an affiliate
					{
						$sql_c = "select address_client_type, address_parent " . 
								 " from addresses " . 
								 " where address_id = " . $row["posaddress_client_id"];
						$res_c = mysql_query($sql_c) or dberror($sql_c);
						if($row_c = mysql_fetch_assoc($res_c))
						{
							if($row_c["address_client_type"] == 3) //Affiliate
							{
								$sql_c = "select user_id, user_firstname, user_name, user_email, " . 
										 "concat(user_name, ' ', user_firstname) as username ".
										 "from users " .
										 "left join user_roles on user_role_user = user_id " . 
										 "where user_address = " . $row_c["address_parent"] . 
										 " and user_role_role = 15 and user_active = 1";

								$res_c = mysql_query($sql_c) or dberror($sql_c);

								if($row_c = mysql_fetch_assoc($res_c))
								{
									$recipient_id = $row_c["user_id"];
									$recipient_email = $row_c["user_email"];
									$recipient_name = $row_c["username"];
									$recipeint_firstname = $row_c["user_firstname"];
								}
							}
						}
					}
				}

				
				if($recipient_id > 0)
				{
					$subject = "SAP Customer Code is missing - Project " . $project["order_number"] . ", " . $country_name . ", " . $project["order_shop_address_company"];


					$mail = new Mail();
					$mail->set_subject(MAIL_SUBJECT_PREFIX . ": " . $subject);
					$mail->set_sender($sender_mail, $sender_name);
					$mail->add_recipient($recipient_email);

					if($row["user_email_cc"])
					{
						$mail->add_cc($row["user_email_cc"]);
					}

					if($row["user_email_deputy"])
					{
						$mail->add_cc($row["user_email_deputy"]);
					}

					if($cc1)
					{
						$mail->add_cc($cc1);
					}

					if($cc2)
					{
						$mail->add_cc($cc2);
					}

					if($cc3)
					{
						$mail->add_cc($cc3);
					}

					if($cc4)
					{
						$mail->add_cc($cc4);
					}


					

					
					$link = APPLICATION_URL ."/mps/pos/address/" . $new_pos_id ;
					
					$mail_alert_text = str_replace("{link}", $link, $mail_alert_text);
					$mail_alert_text = str_replace("{name}", $recipeint_firstname, $mail_alert_text);
					$mail_alert_text = str_replace("{pos_location}", $pos_location, $mail_alert_text);
					
					 
					$mail->add_text($mail_alert_text);
					
					
					if($senmail_activated == true)
					{
						$result = $mail->send();
					}
					else
					{
						$result = 1;
					}

					append_mail($project["project_order"], $recipient_id, user_id(), $mail_alert_text, "910", 1);
				}
			}
		}



		//send mail alert for new corporate POS openings
		if(param("shop_actual_opening_date") 
			and param("old_shop_actual_opening_date") == ''
		    and $project["project_cost_type"] == 1
			and ($project["project_projectkind"] == 1
				or $project["project_projectkind"] == 3
				or $project["project_projectkind"] == 4
				or $project["project_projectkind"] == 9))
		{
				$dchannel = 'not available';
				$sql = "select concat(mps_distchannel_name, ' - ', mps_distchannel_code) as dchannel 
				       from mps_distchannels 
					   where mps_distchannel_id = " . dbquote($form->value('posaddress_distribution_channel'));

				 $res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					$dchannel = $row["dchannel"];
				}
				
				if($project["project_projectkind"] == 1) {
					$Mail = new ActionMail('info.new.openings.corporate.pos');
				}
				else {
					$Mail = new ActionMail('info.takeover.corporate.pos');
				}

				
				if($project["project_projectkind"] == 1) {
					$data = array(
						'country' => $pos_data['country_name'],
						'pos_name' => $pos_data['posaddress_name'],
						'opening_date' => param("shop_actual_opening_date"),
						'eprnr' => $form->value('eprepnr') ? $form->value('eprepnr') : "not available",
						'dchannel' => $dchannel,
						'sap_nr' => $pos_data['posaddress_sapnumber'] ? $pos_data['posaddress_sapnumber'] : 'not available',
						'sap_shipto_nr' => $pos_data['posaddress_sap_shipto_number'] ? $pos_data['posaddress_sap_shipto_number'] : 'not available'
					);
				}
				else {
					$data = array(
						'country' => $pos_data['country_name'],
						'pos_name' => $pos_data['posaddress_name'],
						'opening_date' => to_system_date($pos_data['posaddress_store_openingdate']),
					    'takeover_date' => param("shop_actual_opening_date"),
						'eprnr' => $form->value('eprepnr') ? $form->value('eprepnr') : "not available",
						'dchannel' => $dchannel,
						'sap_nr' => $pos_data['posaddress_sapnumber'] ? $pos_data['posaddress_sapnumber'] : 'not available',
						'sap_shipto_nr' => $pos_data['posaddress_sap_shipto_number'] ? $pos_data['posaddress_sap_shipto_number'] : 'not available'
					);
				}

				$Mail->setDataloader($data);
				
				if($senmail_activated == true)
				{
					$Mail->send(true);

					$recipients = $Mail->getRecipients();

					foreach($recipients as $user_id=>$recipient_data)
					{
						append_mail($project["project_order"], $user_id, $Mail->getSender()->id, $recipient_data->getBody(true), "", 1);
					}
				}
		}


		//send mail alerts for closing of a Corporate POS
		
		if(param("shop_closing_date") 
			and param("old_shop_closing_date") == ''
		    and $project["project_cost_type"] == 1)
		{
				$dchannel = 'not available';
				$sql = "select concat(mps_distchannel_name, ' - ', mps_distchannel_code) as dchannel 
				       from mps_distchannels 
					   where mps_distchannel_id = " . dbquote($form->value('posaddress_distribution_channel'));

				 $res = mysql_query($sql) or dberror($sql);
				if ($row = mysql_fetch_assoc($res))
				{
					$dchannel = $row["dchannel"];
				}
				
				$Mail = new ActionMail('info.new.closings.corporate.pos');

				$data = array(
					'country' => $pos_data['country_name'],
					'pos_name' => $pos_data['posaddress_name'],
					'closing_date' => param("shop_closing_date"),
					'eprnr' => $form->value('eprepnr') ? $form->value('eprepnr') : "not available",
					'dchannel' => $dchannel,
					'sap_nr' => $pos_data['posaddress_sapnumber'] ? $pos_data['posaddress_sapnumber'] : 'not available',
					'sap_shipto_nr' => $pos_data['posaddress_sap_shipto_number'] ? $pos_data['posaddress_sap_shipto_number'] : 'not available'
				);

				$Mail->setDataloader($data);
				
				if($senmail_activated == true)
				{
					$Mail->send(true);

					$recipients = $Mail->getRecipients();
				

					foreach($recipients as $user_id=>$recipient_data)
					{
						append_mail($project["project_order"], $user_id, $Mail->getSender()->id, $recipient_data->getBody(true), "", 1);
					}
				}
		}


		set_owner_company_state($form->value("posaddress_id"), $form->value("shop_closing_date"));


		//update postracking
		if($project["pipeline"] == 0)
		{
			if($form->value("shop_closing_date") != $form->value("old_shop_closing_date"))
			{
			
				//project tracking
				$field = "posaddress_store_closingdate";
				$sql = "Insert into postracking (" . 
					   "postracking_user_id, postracking_pos_id, postracking_field, postracking_oldvalue, postracking_newvalue, postracking_comment, postracking_time) VALUES (" . 
					   user_id() . ", " . 
					   dbquote($pos_data["posaddress_id"]) . ", " . 
					   dbquote($field) . ", " . 
					   dbquote(from_system_date($form->value('old_shop_closing_date'))) . ", " . 
					   dbquote(from_system_date($form->value('shop_closing_date'))) . ", " . 
					   dbquote('changed by user') . ", " . 
					   dbquote(date("Y-m-d:H:i:s")) . ")"; 
					   
				$result = mysql_query($sql) or dberror($sql);

				//send mail alerts
				$mail_template_id = 19;
				if($form->value("old_shop_closing_date") == NULL 
					or $form->value("old_shop_closing_date") == '0000-00-00'
					or $form->value("old_shop_closing_date") == '')
				{
					$mail_template_id = 21;
				}

				$actionMail = new ActionMail($mail_template_id);
				
				$actionMail->setParam('id', $pos_data["posaddress_id"]);
				$actionMail->setParam('section', 'posaddress_store_planned_closingdate');
				
				$model = new Model(Connector::DB_CORE);
				$dataloader = $model->query("
					SELECT *
					FROM posaddresses 
					LEFT JOIN countries ON country_id = posaddress_country
					LEFT JOIN posowner_types ON posowner_type_id = posaddress_ownertype
					LEFT JOIN postypes ON postype_id = posaddress_store_postype
					WHERE posaddress_id = " . $pos_data["posaddress_id"] . "
				")->fetch();
				
				// link
				$protocol = Settings::init()->http_protocol.'://';
				$server = $_SERVER['SERVER_NAME'];
				$dataloader['link'] = $protocol.$server."/pos/posindex_pos.php?id=" . $pos_data["posaddress_id"];
				$dataloader['closing_date'] = $form->value("shop_closing_date");
				

				$actionMail->setDataloader($dataloader);
				
				// send mails
				

				if($senmail_activated == true)
				{
					$actionMail->send();
				}
				
			
			}
			

			

			if($form->value("project_planned_closing_date") != $form->value("old_project_planned_closing_date"))
			{
			
				//project tracking
				$field = "posaddress_store_planned_closingdate";
				$sql = "Insert into postracking (" . 
					   "postracking_user_id, postracking_pos_id, postracking_field, postracking_oldvalue, postracking_newvalue, postracking_comment, postracking_time) VALUES (" . 
					   user_id() . ", " . 
					   dbquote($pos_data["posaddress_id"]) . ", " . 
					   dbquote($field) . ", " . 
					   dbquote(from_system_date($form->value('old_project_planned_closing_date'))) . ", " . 
					   dbquote(from_system_date($form->value('project_planned_closing_date'))) . ", " . 
					   dbquote('changed by user') . ", " . 
					   dbquote(date("Y-m-d:H:i:s")) . ")"; 
					   
				$result = mysql_query($sql) or dberror($sql);

				//send mail alerts
				if($project["project_projectkind"] == 8) // popup
				{
					$mail_template_id = 36;
				}
				else
				{
					$mail_template_id = 20;
				}
				if($form->value("old_project_planned_closing_date") == NULL 
					or $form->value("old_project_planned_closing_date") == '0000-00-00'
					or $form->value("old_project_planned_closing_date") == '')
				{
					if($project["project_projectkind"] == 8) // popup
					{
						$mail_template_id = 37;
					}
					else
					{
						$mail_template_id = 22;
					}
				}
				
				$actionMail = new ActionMail($mail_template_id);
				
				$actionMail->setParam('id', $pos_data["posaddress_id"]);
				$actionMail->setParam('section', 'posaddress_store_planned_closingdate');

				
				$model = new Model(Connector::DB_CORE);
				$dataloader = $model->query("
					SELECT *
					FROM posaddresses 
					LEFT JOIN countries ON country_id = posaddress_country
					LEFT JOIN posowner_types ON posowner_type_id = posaddress_ownertype
					LEFT JOIN postypes ON postype_id = posaddress_store_postype
					WHERE posaddress_id = " . $pos_data["posaddress_id"] . "
				")->fetch();
				
				// link
				$protocol = Settings::init()->http_protocol.'://';
				$server = $_SERVER['SERVER_NAME'];
				$dataloader['link'] = $protocol.$server."/pos/posindex_pos.php?id=" . $pos_data["posaddress_id"];
				$dataloader['planned_closing_date'] = $form->value("project_planned_closing_date");

				$actionMail->setDataloader($dataloader);

				if($senmail_activated == true)
				{
					$actionMail->send();
				}
				
			}
		}


		//send mail for missing POS opening images
		if(param("shop_actual_opening_date") 
			and param("old_shop_actual_opening_date") == ''
			and $project["project_projectkind"] != 5)
		{
			$pos_pictures_uploaded = false;

			$sql = "select count(order_file_id) as num_recs from order_files " . 
				   " where order_file_category = 11 " . 
				   " and order_file_order = " . dbquote($project["project_order"]);

			$res = mysql_query($sql) or dberror($sql);
			$row = mysql_fetch_assoc($res);
			if ($project['project_postype'] == 1 and $row["num_recs"] > 4)
			{
				$pos_pictures_uploaded = true;
			}
			elseif($project['project_postype'] == 2 and  $row["num_recs"] > 2)
			{
				$pos_pictures_uploaded = true;
			}
			elseif($row["num_recs"] > 0) {
				$pos_pictures_uploaded = true;
			}

			$link = APPLICATION_URL . "/user/http://tissot/user/project_view_attachments.php?pid=" . $project['project_id'];
			
			$actionmail = new ActionMail(158);
			$actionmail->setParam('projectID', param("pid"));
			$actionmail->setParam('link', $link);

			if(1==1 or $senmail_activated == true)
			{
				$actionmail->send();
			}

			$recipients = $actionmail->getRecipients();

			if ($recipients) {

				foreach($recipients as $id=>$recipient)
				{
					append_task($project["project_order"], $project["order_user"], $recipient->getContent(), "", "", user_id(), '910', 1, 1);
				}
			}
		}

		$link = "project_edit_pos_data.php?pid=" . param("pid"); 
        redirect($link);

    }
}
elseif ($form->button("order_franchisee_address_id"))
{
    
	// set new franchisee address
	$form->value("franchisee_address_company", "");
	$form->value("franchisee_address_company2",  "");
	$form->value("franchisee_address_address",  "");
	if(array_key_exists('franchisee_street', $form->items))
	{
		$form->items['franchisee_street']['values'][0] = "";
		$form->items['franchisee_street']['values'][1] = "";
	}
	$form->value("franchisee_address_address2",  "");
	$form->value("franchisee_address_zip",  "");
	$form->value("franchisee_address_place",  "");
	$form->value("franchisee_address_place_id", 0);
	$form->value("franchisee_address_country",  0);
	$form->value("franchisee_address_phone",  "");
	if(array_key_exists('franchisee_phone_number', $form->items))
	{
		$form->items['franchisee_phone_number']['values'][0] = "";
		$form->items['franchisee_phone_number']['values'][1] = "";
		$form->items['franchisee_phone_number']['values'][2] = "";
	}
	$form->value("franchisee_address_mobile_phone",  "");
	if(array_key_exists('franchisee_mobile_phone_number', $form->items))
	{
		$form->items['franchisee_mobile_phone_number']['values'][0] = "";
		$form->items['franchisee_mobile_phone_number']['values'][1] = "";
		$form->items['franchisee_mobile_phone_number']['values'][2] = "";
	}
	$form->value("franchisee_address_email",  "");
	$form->value("franchisee_address_contact",  "");
	$form->value("franchisee_address_website",  "");

	if ($form->value("order_franchisee_address_id"))
	{
		$sql = "select * from addresses " .
			   "left join places on place_id = address_place_id " . 
		       "where address_id = " . $form->value("order_franchisee_address_id");
		$res = mysql_query($sql) or dberror($sql);

		if ($row = mysql_fetch_assoc($res))
		{
			$form->value("franchisee_address_company", $row["address_company"]);
			$form->value("franchisee_address_company2",  $row["address_company2"]);
			$form->value("franchisee_address_address",  $row["address_address"]);
			if(array_key_exists('franchisee_street', $form->items))
			{
				$form->items['franchisee_street']['values'][0] = $row["address_street"];
				$form->items['franchisee_street']['values'][1] = $row["address_streetnumber"];
			}
			$form->value("franchisee_address_address2",  $row["address_address2"]);
			$form->value("franchisee_address_zip",  $row["address_zip"]);
			$form->value("franchisee_address_place",  $row["place_name"]);
			$form->value("franchisee_address_place_id", $row["address_place_id"]);
			$form->value("franchisee_address_country",  $row["address_country"]);
			$form->value("franchisee_address_phone",  $row["address_phone"]);
			if(array_key_exists('franchisee_phone_number', $form->items))
			{
				$form->items['franchisee_phone_number']['values'][0] = $row["address_phone_country"];
				$form->items['franchisee_phone_number']['values'][1] = $row["address_phone_area"];
				$form->items['franchisee_phone_number']['values'][2] = $row["address_phone_number"];
			}
			$form->value("franchisee_address_mobile_phone",  $row["address_mobile_phone"]);
			if(array_key_exists('franchisee_mobile_phone_number', $form->items))
			{
				$form->items['franchisee_mobile_phone_number']['values'][0] = $row["address_mobile_phone_country"];
				$form->items['franchisee_mobile_phone_number']['values'][1] = $row["address_mobile_phone_area"];
				$form->items['franchisee_mobile_phone_number']['values'][2] = $row["address_mobile_phone_number"];
			}
			$form->value("franchisee_address_email",  $row["address_email"]);
			$form->value("franchisee_address_contact",  $row["address_contact_name"]);
			$form->value("franchisee_address_website",  $row["address_website"]);
		}
	}
}
elseif($form->button("posaddress_place_id"))
{
	$sql = "select place_name, province_canton " .
		   "from places " . 
		   "left join provinces on province_id = place_province " . 
		   "where place_id = " . dbquote($form->value("posaddress_place_id"));
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$form->value("shop_address_place", $row["place_name"]);
		$form->value("province", $row["province_canton"]);
	}
}
elseif($form->button("franchisee_address_place_id"))
{
	$sql = "select place_name " .
		   "from places " . 
		   "where place_id = " . dbquote($form->value("franchisee_address_place_id"));
	$res = mysql_query($sql) or dberror($sql);
	if ($row = mysql_fetch_assoc($res))
	{
		$form->value("franchisee_address_place",  $row["place_name"]);
	}
}
elseif($form->button("franchisee_address_country"))
{
	$form->value("franchisee_address_place",  "");
	$form->value("franchisee_address_place_id", 0);
}


//gogle map link
if($form->value("shop_address_country") and $form->value("shop_address_place") and $form->value("shop_address_address"))
{
	if($form->value("posaddress_id") > 0)
	{
		$googlemaplink = "Click <a href='javascript:popup(\"/pos/project_new_map.php?id=" . $form->value("posaddress_id") . "&table=" . $table . "&c=" . $form->value("shop_address_country") . "&p=" . str_replace("'", " ", $form->value("shop_address_place")) . "&a=" . str_replace("'", " ", $form->value("shop_address_address")) . "\", 700,650)'>here</a> to verify the geografical position of the POS.";
	}
	else
	{
		$googlemaplink = "Click <a href='javascript:popup(\"/pos/project_new_map.php?table=" . $table . "&c=" . $form->value("shop_address_country") . "&p=" . str_replace("'", " ", $form->value("shop_address_place")) . "&a=" . str_replace("'", " ", $form->value("shop_address_address")) . "\", 700,650)'>here</a> to choose the geografical position of the POS.";
	}
}
elseif($form->value("shop_address_country") and $form->value("shop_address_place"))
{
	if($form->value("posaddress_id") > 0)
	{
		$googlemaplink = "Click <a href='javascript:popup(\"/pos/project_new_map.php?id=" . $form->value("posaddress_id") . "&table=" . $table . "&c=" . $form->value("shop_address_country") . "&p=" . str_replace("'", " ", $form->value("shop_address_place")) . "&a=\", 700,650)'>here</a> to verify the geografical position of the POS.";
	}
	else
	{
		$googlemaplink = "Click <a href='javascript:popup(\"/pos/project_new_map.php?table=" . $table . "&c=" . $form->value("shop_address_country") . "&p=" . str_replace("'", " ", $form->value("shop_address_place")) . "&a=\", 700,650)'>here</a> to choose the geografical position of the POS.";
	}
}
elseif($form->value("shop_address_country"))
{
	if($form->value("posaddress_id") > 0)
	{
		$googlemaplink = "Click <a href='javascript:popup(\"/pos/project_new_map.php?id=" . $form->value("posaddress_id") . "&table=" . $table . "&c=" . $form->value("shop_address_country") . "&p=&a=\", 700,650)'>here</a> to verify the geografical position of the POS.";
	}
	else
	{
		$googlemaplink = "Click <a href='javascript:popup(\"/pos/project_new_map.php?table=" . $table . "&c=" . $form->value("shop_address_country") . "&p=&a=\", 700,650)'>here</a> to choose the geografical position of the POS.";
	}
}
elseif(!isset($googlemaplink))
{
	$googlemaplink = "";
}


$form->value("GM", $googlemaplink);

    
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Edit POS-Data");
$form->render();




?>
<!--
<div id="grosssqm" style="display:none;">
    Please indicate the Gross Surface in units of measurement <strong>rented by Tissot</strong><br />incl. corridors and other public areas, back office, toilets etc.
</div>
-->
<div id="totalsqm" style="display:none;">
    Please indicate the total surface in units of measurement <strong>occupied by Tissot</strong><br />incl. back office, toilets etc.
</div> 
<div id="retailsqm" style="display:none;">
    Please indicate the surface in units of measurement <strong>occupied by Tissot</strong><br />used for sales purposes (without back office, toiletsetc.).
</div> 
<div id="backofficesqm" style="display:none;">
    Please indicate the total other surface in units of measurement <strong>occupied by Tissot</strong><br />like back office, toilets, stock areas, etc.
</div> 

<div id="popup" style="display:none;">
    Please add a short description of the PopUplike e.g. <br />the name of the campaign or any other reason
</div> 



<script language="javascript">
	$("#h_popup").click(function() {
	   $('#popup_name').val($('#popup_name').val().toLowerCase());
	   var txt = $('#popup_name').val();

	   $('#popup_name').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	$("#h_shop_address_company").click(function() {
	   $('#shop_address_company').val($('#shop_address_company').val().toLowerCase());
	   var txt = $('#shop_address_company').val();

	   $('#shop_address_company').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	$("#h_shop_address_company2").click(function() {
	   $('#shop_address_company2').val($('#shop_address_company2').val().toLowerCase());
	   var txt = $('#shop_address_company2').val();

	   $('#shop_address_company2').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	$("#h_shop_address_address").click(function() {
	    $('#order_shop_address_street').val($('#order_shop_address_street').val().toLowerCase());
	    var txt = $('#order_shop_address_street').val();

	    $('#order_shop_address_street').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));

	});


	$("#h_shop_address_address2").click(function() {
	   $('#shop_address_address2').val($('#shop_address_address2').val().toLowerCase());
	   var txt = $('#shop_address_address2').val();

	   $('#shop_address_address2').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});



	$("#h_franchisee_address_company").click(function() {
	   $('#franchisee_address_company').val($('#franchisee_address_company').val().toLowerCase());
	   var txt = $('#franchisee_address_company').val();

	   $('#franchisee_address_company').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	$("#h_franchisee_address_company2").click(function() {
	   $('#franchisee_address_company2').val($('#franchisee_address_company2').val().toLowerCase());
	   var txt = $('#franchisee_address_company2').val();

	   $('#franchisee_address_company2').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	
	$("#h_franchisee_address_address2").click(function() {
	   $('#franchisee_address_address2').val($('#franchisee_address_address2').val().toLowerCase());
	   var txt = $('#franchisee_address_address2').val();

	   $('#franchisee_address_address2').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	
	$("#h_franchisee_address_address").click(function() {
	   $('#order_franchisee_address_street').val($('#order_franchisee_address_street').val().toLowerCase());
	   var txt = $('#order_franchisee_address_street').val();

	   $('#order_franchisee_address_street').val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
	});

	

	
</script>

<div id="changehistory" style="display:none;">
    <strong>Changes of the agreed opening date</strong>
	<table class="table_tracking">
	<tr>
	<td class="label">User</td>
	<td class="label">Time</td>
	<td class="label">Old Value</td>
	<td class="label">New Value</td>
	<td class="label">Comment</td>
	</tr>

	<?php
		foreach($tracking_info as $key=>$values)
		{
			echo '<tr class="tr_tracking"><td class="td_tracking_nobr">' . $values['user_name'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_time'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_oldvalue'] . '</td>';
			echo '<td class="td_tracking_nobr">' . $values['projecttracking_newvalue'] . '</td>';
			echo '<td class="td_tracking">' . $values['projecttracking_comment'] . '</td></tr>';
		}
	?>
	
	</table>
</div> 

<?php
echo "<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>";

?>
<script language="Javascript">

	function round_decimal(num,decimals){
		return Math.round(num*Math.pow(10,decimals))/Math.pow(10,decimals);
	}


	
	$(document).ready(function(){
		  $("#unit_of_measurement" ).change(function() {
				
				if($("#unit_of_measurement" ).val() == 1)
				{
					//$("#um00").html("<strong> sqf</strong>");
					$("#um01").html("<strong> sqf</strong>");
					$("#um02").html("<strong> sqf</strong>");
					$("#um03").html("<strong> sqf</strong>");
					$("#um04").html("<strong> sqf</strong>");
					$("#um05").html("<strong> sqf</strong>");
					$("#um06").html("<strong> sqf</strong>");
					
					//$("#shop_grosssqms").val(round_decimal(10.7639104*$("#shop_grosssqms").val(), 2));
					$("#shop_totalsqms").val(round_decimal(10.7639104*$("#shop_totalsqms").val(), 2));
					$("#shop_sqms").val(round_decimal(10.7639104*$("#shop_sqms").val(), 2));
					$("#shop_bakoffocesqms").val(round_decimal(10.7639104*$("#shop_bakoffocesqms").val(), 2));

					$("#shop_floorsurface1").val(round_decimal(10.7639104*$("#shop_floorsurface1").val(), 2));
					$("#shop_floorsurface2").val(round_decimal(10.7639104*$("#shop_floorsurface2").val(), 2));
					$("#shop_floorsurface3").val(round_decimal(10.7639104*$("#shop_floorsurface3").val(), 2));
				}
				else
			    {
					//$("#um00").html("<strong> sqms</strong>");
					$("#um01").html("<strong> sqms</strong>");
					$("#um02").html("<strong> sqms</strong>");
					$("#um03").html("<strong> sqms</strong>");
					$("#um04").html("<strong> sqms</strong>");
					$("#um05").html("<strong> sqms</strong>");
					$("#um06").html("<strong> sqms</strong>");

					
					//$("#shop_grosssqms").val(round_decimal(0.09290304*$("#shop_grosssqms").val(), 2));
					$("#shop_totalsqms").val(round_decimal(0.09290304*$("#shop_totalsqms").val(), 2));
					$("#shop_sqms").val(round_decimal(0.09290304*$("#shop_sqms").val(), 2));
					$("#shop_bakoffocesqms").val(round_decimal(0.09290304*$("#shop_bakoffocesqms").val(), 2));

					$("#shop_floorsurface1").val(round_decimal(0.09290304*$("#shop_floorsurface1").val(), 2));
					$("#shop_floorsurface2").val(round_decimal(0.09290304*$("#shop_floorsurface2").val(), 2));
					$("#shop_floorsurface3").val(round_decimal(0.09290304*$("#shop_floorsurface3").val(), 2));

				}
		  });


	});

</script>


<?php

require_once "include/project_footer_logistic_state.php";
$page->footer();



?>