<?php
/********************************************************************

    project_costs_budget.php

    View or edit the costs of a project

    Created by:     Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date created:   2014-01-09
    Modified by:    Anton Hofmann (anton.hofmann@mediaparx.ch)
    Date modified:  2014-01-09
    Version:        1.0.0

    Copyright (c) 2014, OMEGA SA, All Rights Reserved.

*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/project_cost_functions.php";
require_once "include/save_functions.php";
require_once "../shared/func_eventmails.php";

if (has_access("can_view_project_costs") 
   or has_access("can_view_budget_in_projects")
   or has_access('can_edit_project_costs_budget')
   )
{  
}
else {
	redirect("noaccess.php");
}

if(!param("pid"))
{
	$link = "welcome.php";
	redirect($link);
}

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));
// get company's address
$client_address = get_address($project["order_client_address"]);

$currency_symbol = get_currency_symbol($project["order_client_currency"]);
$order_currency = get_order_currency($project["project_order"]);

$can_edit = false;
if(has_access('can_edit_project_costs_budget') 
	and $project["order_budget_is_locked"] == 0) {
	$can_edit = true;
}

if($project["project_state"] == 2) {
		$can_edit = false;
}

/********************************************************************
    check if project cost positions are present and create if not
*********************************************************************/

$sql = "select count(costsheet_id) as num_recs from costsheets " . 
       " where costsheet_version = 0
			and costsheet_project_id = " . param("pid");
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

if($row["num_recs"] == 0) //make the user select a cost template
{
	$link = "project_costs_overview.php?pid=" . param("pid");
	redirect($link);
}

$currencies = array();
$dropdown_currencies_html = '<option value="0">';
$sql_currencies = "select currency_id, currency_symbol
					from currencies
					order by currency_symbol";
$res = mysql_query($sql_currencies) or dberror($sql_currencies);
while($row = mysql_fetch_assoc($res)) {
	$currencies[$row['currency_id']] = $row['currency_symbol'];

	if($project["order_client_currency"] == $row['currency_id']) {
		$dropdown_currencies_html .= '<option value="'. $row['currency_id'] .'" selected="selected">'. $row['currency_symbol'] .'</option>';
	}
	else {
		$dropdown_currencies_html .= '<option value="'. $row['currency_id'] .'">'. $row['currency_symbol'] .'</option>';
	}
}

/********************************************************************
	get budget data from cost sheet
*********************************************************************/
$budget = get_project_budget_totals(param("pid"), $order_currency);

$code_data = array();
$budget_data = array();
$currency_data = array();
$currency_data_chf = array();
$text_data = array();
$comment_data = array();
$amounts_chf = array();
$delete_ids = array();
$delete_links = array();
$list_has_positions = array();
$costsheet_partner_contribution = array();
$costsheet_currencies = array();



$sql = "select costsheet_id, costsheet_pcost_group_id, costsheet_currency_id, currency_symbol, " . 
       " costsheet_exchangerate, currency_factor, costsheet_code, costsheet_text, " . 
       "costsheet_budget_amount, costsheet_partner_contribution, costsheet_comment, costsheet_company " . 
	   "from costsheets " .
	   " left join currencies on currency_id = costsheet_currency_id " . 
	   "where costsheet_version = 0
			and costsheet_project_id = " . param("pid");


$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	$code_data[$row["costsheet_id"]] = $row["costsheet_code"];
	$budget_data[$row["costsheet_id"]] = $row["costsheet_budget_amount"];
	$currency_data[$row["costsheet_id"]] = $row["currency_symbol"];
	$text_data[$row["costsheet_id"]] = $row["costsheet_text"];
	$comment_data[$row["costsheet_id"]] = $row["costsheet_comment"];
	$company_data[$row["costsheet_id"]] = $row["costsheet_company"];
	$delete_ids[$row["costsheet_pcost_group_id"]][$row["costsheet_id"]] = "__costsheets_select_to_delete_" . $row["costsheet_id"];

	$list_has_positions[$row["costsheet_pcost_group_id"]] = true;

	$costsheet_partner_contribution[$row["costsheet_id"]] = $row["costsheet_partner_contribution"];

	$delete_links[$row["costsheet_id"]] = '<a class="delete_line" data="'.$row['costsheet_id'].'" href="javascript:void(0);" title="Remove line"><img src="/pictures/closed.gif" /></i></a>';

	$costsheet_currencies[$row["costsheet_id"]] = $row["costsheet_currency_id"];

	$amounts_chf[$row["costsheet_id"]] = round(($row["costsheet_exchangerate"] * $row["costsheet_budget_amount"])/$row["currency_factor"], 2);

	$amounts_chf[$row["costsheet_id"]] = number_format($amounts_chf[$row["costsheet_id"]], 2, '.', "'");


	

}


/********************************************************************
	Create Form
*********************************************************************/ 

$form = new Form("projects", "projects");


$form->add_section("Project");
$form->add_hidden("pid", param('pid'));
$form->add_hidden("order_id", $project["project_order"]);


require_once "include/project_head_small.php";


$form->add_section("Currency and Exchange Rate");

if($can_edit == true){
	$form->add_list("order_client_currency", "Currency",
	"select currency_id, concat(currency_symbol, ': ', currency_name) as currency from currencies order by currency_symbol", SUBMIT | NOTNULL, $project["order_client_currency"]);
	$form->add_edit("order_client_exchange_rate", "Exchange Rate", NOTNULL, $project["order_client_exchange_rate"], TYPE_DECIMAL, 10, 6);


	$form->add_section("Business Partner Contribution");
	$form->add_comment("Fill in only if the business partner's contribution is a global contribution over all cost positions. <br />Otherwise add the contribution in percent for each single cost position below.");
	$form->add_edit("project_share_other", "Business Partner Contribution in Percent", 0, $project["project_share_other"], TYPE_DECIMAL, 6,2);
}
else {
			
	$form->add_lookup("order_client_currency", "Currency", "currencies", "concat(currency_symbol, ': ', currency_name)", 0, $project["order_client_currency"]);

	$form->add_label("order_client_exchange_rate", "Exchange Rate", 0, $project["order_client_exchange_rate"]);

	$form->add_section("Business Partner Contribution");
	$form->add_label("project_share_other", "Business Partner Contribution in Percent", 0, $project["project_share_other"] . "%");
}


if(has_access("can_unfreeze_budget"))
{
	$form->add_section("Budget State");
	$form->add_checkbox("order_budget_is_locked", "Budget is locked", $project["order_budget_is_locked"], 0, "State");
	
	
	if($project["order_budget_unfreezed_by"] > 0) {
		$form->add_label("unfreezed_date", "Date last unfreeze", 0, to_system_date($project["order_budget_unfreezed_date"]));
		$form->add_lookup("unfreezed_by", "Unfreezed by", "users", "concat(user_name, ' ', user_firstname)", 0, $project["order_budget_unfreezed_by"]);
	}
	else
	{
		$form->add_label("unfreezed_date", "Date last unfreeze", 0);
		$form->add_label("unfreezed_by", "Unfreezed by", 0);
	}

	
}
else
{
	$form->add_hidden("order_budget_is_locked");
}

if($can_edit == true) {
	
	$form->add_button("save", "Save");
	if(has_access("can_unfreeze_budget") and $can_edit == true)
	{
		$form->add_button("freeze", "Freeze Budget");
		$form->add_button("unfreeze", "Unfreeze Budget");
	}


	
	$link = "javascript:add_items_from_catalog(" . param("pid") . ")";
	$form->add_button("add_items", "Add Items from Catalog", $link);
	$link = "javascript:add_new_cost_position(" . param("pid") . ")";
	$form->add_button("add_cost_positions", "Add Missing Cost Groups", $link);
}
else {
	if(has_access("can_unfreeze_budget") 
		and $project["order_budget_is_locked"] == 1)
	{
		$form->add_button("unlock", "Unlock Budget");
	}
}

$link = "javascript:popup('/user/project_costs_budget_pdf.php?pid=" . param("pid") . "', 800, 600);";
$form->add_button("print", "Print Budget in " . $currency_symbol, $link);

$link = "javascript:popup('/user/project_costs_budget_pdf.php?pid=" . param("pid") . "&sc=1', 800, 600);";
$form->add_button("print2", "Print Budget in CHF", $link);

if($project["order_budget_is_locked"] == 1)
{
	$form->error("Budget is locked, no more changes can be made!");
}

$form->populate();
$form->process();


if($form->button("order_client_currency"))
{
	$sql = "select currency_exchange_rate, currency_factor " . 
		   "from currencies " . 
		   "where currency_id = " . $form->value("order_client_currency");

	$res = mysql_query($sql) or dberror($sql);
	if($row = mysql_fetch_assoc($res))
	{
		$form->value("order_client_exchange_rate", $row["currency_exchange_rate"]);

		//update exchange rate in project data
		$sql = "update orders set " . 
				 "order_client_currency = " . dbquote($form->value("order_client_currency")) . ", " . 
				 "order_client_exchange_rate = " . dbquote($form->value("order_client_exchange_rate")) . ", " .
				 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", " . 
				 "user_modified = " . dbquote(user_login()) . 
				 " where order_id = " . dbquote($project["project_order"]);


		$result = mysql_query($sql) or dberror($sql);


		$sql = "update costsheets set " . 
				 "costsheet_exchangerate = " . dbquote($form->value("order_client_exchange_rate")) . ", " .
				 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", " . 
				 "user_modified = " . dbquote(user_login()) . 
				 " where costsheet_version = 0 
					and costsheet_project_id = " . dbquote($project["project_order"]) . 
				 "	and costsheet_currency_id = " . dbquote($form->value("order_client_currency"));


		$result = mysql_query($sql) or dberror($sql);


		$sql = "update costsheets set " . 
			     "costsheet_currency_id = " . dbquote($form->value("order_client_currency")) . ", " .
				 "costsheet_exchangerate = " . dbquote($form->value("order_client_exchange_rate")) . ", " .
				 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", " . 
				 "user_modified = " . dbquote(user_login()) . 
				 " where costsheet_version = 0 
					and costsheet_project_id = " . dbquote($project["project_order"]) . 
				 "	and costsheet_budget_amount = 0";


		$result = mysql_query($sql) or dberror($sql);


		$sql = "select distinct costsheet_id, currency_exchange_rate
				from costsheets
				left join currencies on currency_id = costsheet_currency_id
				 where costsheet_version = 0 
					and costsheet_project_id = " . dbquote($project["project_order"]) . 
				 "	and costsheet_currency_id <> " . dbquote($form->value("order_client_currency"));


		$res = mysql_query($sql) or dberror($sql);
		while($row = mysql_fetch_assoc($res))
		{
			$sql_u = "update costsheets set " . 
					 "costsheet_exchangerate = " . dbquote($row["currency_exchange_rate"]) . ", " .
					 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", " . 
					 "user_modified = " . dbquote(user_login()) . 
					 " where costsheet_id = " . dbquote($row['costsheet_id']);
			$result = mysql_query($sql_u) or dberror($sql_u);
		}
	}

	$link = "project_costs_budget.php?pid=" . $project['project_id'];
	redirect($link);

}
elseif($form->button("save"))
{
	//update exchange rate in project data
	$sql = "update orders set " . 
			 "order_client_currency = " . dbquote($form->value("order_client_currency")) . ", " . 
			 "order_client_exchange_rate = " . dbquote($form->value("order_client_exchange_rate")) . ", " .
			 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", " . 
			 "user_modified = " . dbquote(user_login()) . 
			 " where order_id = " . dbquote($project["project_order"]);


	$result = mysql_query($sql) or dberror($sql);


	$sql = "update costsheets set " . 
			 "costsheet_exchangerate = " . dbquote($form->value("order_client_exchange_rate")) . ", " .
			 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", " . 
			 "user_modified = " . dbquote(user_login()) . 
			 " where costsheet_version = 0 
				and costsheet_project_id = " . dbquote($project["project_order"]) . 
			 "	and costsheet_currency_id = " . dbquote($form->value("order_client_currency"));


	$result = mysql_query($sql) or dberror($sql);


	$sql = "select distinct costsheet_id, currency_exchange_rate
	        from costsheets
			left join currencies on currency_id = costsheet_currency_id
			 where costsheet_version = 0 
				and costsheet_project_id = " . dbquote($project["project_order"]) . 
			 "	and costsheet_currency_id <> " . dbquote($form->value("order_client_currency"));


	$res = mysql_query($sql) or dberror($sql);
	while($row = mysql_fetch_assoc($res))
	{
		$sql_u = "update costsheets set " . 
				 "costsheet_exchangerate = " . dbquote($row["currency_exchange_rate"]) . ", " .
				 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", " . 
				 "user_modified = " . dbquote(user_login()) . 
				 " where costsheet_id = " . dbquote($row['costsheet_id']);
		$result = mysql_query($sql_u) or dberror($sql_u);
	}
	
	
	//update business_partner_shares
	$sql = "select sum(costsheet_budget_amount) as budget_total
			from costsheets
			where costsheet_version = 0
			and costsheet_project_id = " . dbquote(param("pid"));

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	
	$budget_total = $row["budget_total"];

	$sql = "select sum(costsheet_partner_contribution*costsheet_budget_amount/100) as partner_total
			from costsheets
			where costsheet_version = 0
			and costsheet_partner_contribution > 0 
			   and costsheet_project_id = " . dbquote(param("pid"));

	$res = mysql_query($sql) or dberror($sql);
	$row = mysql_fetch_assoc($res);
	
	$partner_total = $row["partner_total"];
	
	$share = 0;
	if($budget_total > 0) {
		$share = round(100*($partner_total/$budget_total), 2);
	}


	if($share > 0) {
		$sql = "update projects set " . 
			 "project_share_other = " . dbquote($share) . ", " .
			 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", " . 
			 "user_modified = " . dbquote(user_login()) . 
			 " where project_id = " . dbquote(param("pid"));

		$result = mysql_query($sql) or dberror($sql);

		$sql = "update cer_basicdata set " . 
			 "cer_basicdata_franchsiee_investment_share = " . dbquote($share) . ", " .
			 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", " . 
			 "user_modified = " . dbquote(user_login()) . 
			 " where cer_basicdata_version = 0 and cer_basicdata_project = " . dbquote(param("pid"));

		$result = mysql_query($sql) or dberror($sql);
	}
	else {
		$sql = "update projects set " . 
			 "project_share_other = " . dbquote($form->value("project_share_other")) . ", " .
			 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", " . 
			 "user_modified = " . dbquote(user_login()) . 
			 " where project_id = " . dbquote(param("pid"));

		$result = mysql_query($sql) or dberror($sql);

		$sql = "update cer_basicdata set " . 
			 "cer_basicdata_franchsiee_investment_share = " . dbquote($form->value("project_share_other")) . ", " .
			 "date_modified = " . dbquote(date("Y-m-d H:i:s")) . ", " . 
			 "user_modified = " . dbquote(user_login()) . 
			 " where cer_basicdata_version = 0 and cer_basicdata_project = " . dbquote(param("pid"));

		$result = mysql_query($sql) or dberror($sql);
	}

	if($form->value("order_budget_is_locked")!= $project["order_budget_is_locked"])
	{
		update_budget_state($project["project_order"], $form->value("order_budget_is_locked"));
	}

	$link = "project_costs_budget.php?pid=" . param("pid");
	redirect($link);

	
}
elseif ($form->button("unfreeze"))
{
    unfreeze_project_budget($project["project_order"]);
	send_project_event_mail(user_id(), 'unfreeze_project_budget', $project);

    $link = "project_costs_budget.php?pid=" . param("pid");
	redirect($link);
}
elseif ($form->button("freze"))
{
    freeze_budget($project["project_order"]);

    $link = "project_costs_budget.php?pid=" . param("pid");
	redirect($link);
}
elseif ($form->button("unlock"))
{
	update_budget_state($project["project_order"], 0);
	$link = "project_costs_budget.php?pid=" . param("pid");
	redirect($link);
}


/********************************************************************
	Compose Cost Sheet
*********************************************************************/ 

//add all cost groups and cost sub groups

$list_names = array();
$group_ids = array();
$group_titles = array();
$sub_group_ids = array();

//get all subgroups
$sql = "select costsheet_id, costsheet_pcost_group_id, costsheet_pcost_subgroup_id,  
	   costsheet_code, costsheet_text, costsheet_comment, 
	   concat(pcost_subgroup_code, ' ', pcost_subgroup_name) as subgroup 
	   from costsheets 
	    left join pcost_subgroups on pcost_subgroup_id = costsheet_pcost_subgroup_id  
        where costsheet_version = 0
			and costsheet_project_id = " . dbquote(param("pid")) . 
		" and costsheet_is_in_budget = 1";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$sub_group_ids[$row['subgroup']] = $row['costsheet_pcost_subgroup_id'];
}


$sql2 = "select DISTINCT costsheet_pcost_group_id, pcost_group_code, pcost_group_name " .
	   "from costsheets " .
	   "left join pcost_groups on pcost_group_id = costsheet_pcost_group_id " .
	   " where costsheet_version = 0
			and costsheet_project_id = " . dbquote(param("pid")) . " and costsheet_is_in_budget = 1" .
	   " order by pcost_group_code";

$res = mysql_query($sql2) or dberror($sql2);
while ($row = mysql_fetch_assoc($res))
{	
	
	$listname = "list" . $row["pcost_group_code"];
	$list_names[] = $listname;
	$group_ids[] = $row["costsheet_pcost_group_id"];
	$group_titles[] = $row["pcost_group_name"];
	

	$toggler = '<div class="toggler_pointer" id="l' . $row["costsheet_pcost_group_id"] . '_on"><span class="fa fa-minus-square toggler"></span>' .$row["pcost_group_name"] . '</div>';

	$sql = "select costsheet_id, costsheet_pcost_group_id, costsheet_pcost_subgroup_id, " . 
		   "costsheet_code, costsheet_text, costsheet_comment, " .
		   "concat(pcost_subgroup_code, ' ', pcost_subgroup_name) as subgroup  " .
		   "from costsheets " .
		   " left join pcost_subgroups on pcost_subgroup_id = costsheet_pcost_subgroup_id"; 

	$list_filter = "costsheet_version = 0
			and costsheet_project_id = " . param("pid") . " and costsheet_pcost_group_id = " . $row["costsheet_pcost_group_id"] . " and costsheet_is_in_budget = 1";
	
	
	
	//compose list
	$$listname = new ListView($sql, LIST_HAS_HEADER | LIST_HAS_FOOTER);

	$$listname->set_entity("costsheets");
	$$listname->set_order("LENGTH(costsheet_code), COALESCE(costsheet_code,'Z')");
	$$listname->set_group("subgroup");
	$$listname->set_filter($list_filter);
	$$listname->set_title($toggler);

	if($can_edit == true) {
		$$listname->add_edit_column("costsheet_code", "Code", 4, 0, $code_data);
		$$listname->add_edit_column("costsheet_text", "Text", 60, 0, $text_data, 'textarea', 3);
		$$listname->add_edit_column("costsheet_company", "Supplier", 18, 0, $company_data, 'inputfield');
		$$listname->add_edit_column("costsheet_budget_amount", "Budget", 12, 0, $budget_data);
		$$listname->add_list_column("costsheet_currency_id", "Currency", $sql_currencies, NOTNULL, $costsheet_currencies);
		$$listname->add_text_column("chf", "CHF", 0, $amounts_chf);
		$$listname->add_edit_column("costsheet_partner_contribution", "% paid by<br />Business Partner", 6, COLUMN_UNDERSTAND_HTML, $costsheet_partner_contribution, 'textarea', 3);	
		$$listname->add_edit_column("costsheet_comment", "Comment", 30, 0, $comment_data, 'textarea', 3);

		if($can_edit == true) {
			$$listname->add_text_column("delete_links", "", COLUMN_UNDERSTAND_HTML, $delete_links);
		}
	}
	else {
		$$listname->add_text_column("costsheet_code", "Code", 0, $code_data);
		$$listname->add_text_column("costsheet_text", "Text", 0, $text_data);
		$$listname->add_text_column("costsheet_company", "Supplier", 0, $company_data);
		$$listname->add_text_column("costsheet_budget_amount", "Budget", 0, $budget_data);
		$$listname->add_text_column("currency", "", 0, $currency_data);
		$$listname->add_text_column("chf", "CHF", 0, $amounts_chf);
		$$listname->add_text_column("costsheet_partner_contribution", "% paid by<br />Business Partner", COLUMN_UNDERSTAND_HTML, $costsheet_partner_contribution);	
		$$listname->add_text_column("costsheet_comment", "Comment", 0, $comment_data);
	}
	
	if(array_key_exists($row["costsheet_pcost_group_id"], $list_has_positions))
	{
		foreach($budget["subgroup_totals"] as $subgroup=>$budget_sub_group_total)
		{
			if($can_edit == true) {
				$tmp = param("pid") . '-' . $row['costsheet_pcost_group_id']. '-'. $sub_group_ids[$subgroup];
				$$listname->set_group_footer("delete_links", $subgroup , '<a class="add_new_line" data="'.$tmp.'" href="javascript:void(0);" title="Add new line">&nbsp;<i class="fa fa-plus" aria-hidden="true"></i></a>');

			}
		
			$$listname->set_group_footer("costsheet_text", $subgroup , "Subgroup Total " . $currency_symbol . "/CHF");
			$$listname->set_group_footer("costsheet_budget_amount",  $subgroup , number_format($budget_sub_group_total, 2, ".", "'"), 'sgt' . $sub_group_ids[$subgroup]);
		}

		foreach($budget["subgroup_totals_chf"] as $subgroup=>$budget_sub_group_total)
		{
		
			$$listname->set_group_footer("chf",  $subgroup , number_format($budget_sub_group_total, 2, ".", "'"), 'sgt_chf' . $sub_group_ids[$subgroup]);
		}

		$$listname->set_footer("costsheet_code", "Total " . $currency_symbol . "/CHF");
		$$listname->set_footer("costsheet_budget_amount", number_format($budget["group_totals"][$row["costsheet_pcost_group_id"]], 2, ".", "'"), 'gt' . $row["costsheet_pcost_group_id"]);

		$$listname->set_footer("chf", number_format($budget["group_totals_chf"][$row["costsheet_pcost_group_id"]], 2, ".", "'"), 'gt_chf' . $row["costsheet_pcost_group_id"]);
	}

	$$listname->populate();
	$$listname->process();
}


$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Project Costs - Budget");

require_once("include/costsheet_tabs.php");
$form->render();



foreach($list_names as $key=>$listname)
{
		
	if(array_key_exists("costsheet", $_SESSION) and array_key_exists($listname, $_SESSION["costsheet"]))
	{
		
		if($_SESSION["costsheet"][$listname] == 0)
		{
			echo '<p>&nbsp;</p>';
			$toggler = '<div class="toggler_pointer" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
			echo $toggler;
			
			echo '<div style="display:none;" id="' . $listname . '">';
			$$listname->render();
			echo '</div>';
		}
		else
		{
			echo '<p>&nbsp;</p>';
			$toggler = '<div class="toggler_pointer toggler_pointer_off" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
			echo $toggler;
			
			echo '<div id="' . $listname . '">';
			$$listname->render();
			echo '</div>';
		}
	}
	else
	{
		echo '<p>&nbsp;</p>';
		$toggler = '<div class="toggler_pointer toggler_pointer_off" id="l' . $group_ids[$key] . '_off"><span class="fa fa-plus-square toggler"></span>' . $group_titles[$key] . '</div>';
		echo $toggler;
		
		echo '<div id="' . $listname . '">';
		$$listname->render();
		echo '</div>';
	}
}


?>
<script language="javascript">

jQuery(document).ready(function($) {
	
	<?php
	foreach($list_names as $key=>$listname)
	{
	?>
		$('#l<?php echo $group_ids[$key];?>_on').click(function()  {
			$('#<?php echo $listname;?>').css('display', 'none');
			$('#l<?php echo $group_ids[$key];?>_off').css('display', 'block');

			$.ajax({
				type: "POST",
				data: "listname=<?php echo $listname;?>&visibility=0",
				url: "../shared/ajx_costsheet_liststaes.php",
				success: function(msg){
				}
			});
		});
		
		$('#l<?php echo $group_ids[$key];?>_off').click(function()  {
			$('#<?php echo $listname;?>').css('display', 'block');
			$('#l<?php echo $group_ids[$key];?>_off').css('display', 'none');

			$.ajax({
				type: "POST",
				data: "listname=<?php echo $listname;?>&visibility=1",
				url: "../shared/ajx_costsheet_liststaes.php",
				success: function(msg){
				}
			});
		});
	<?php
	}
	
	if($can_edit == true) {
	?>

	
		$(document).on("change","select",function(){
		    //alert( this.value );
			if(this.value == 0) {
				$(this).addClass( "error_field" );
			}
			else {

				field_id = $(this).attr('id');

				var params = "pid=<?php echo param('pid');?>&id=" + field_id + "&value=" + this.value + '&action=save';
					$.ajax({
						type: "POST",
						data: params,
						url: "include/ajx_save_cost_sheet_position.php",
						success: function(msg){

							if(msg != 'error') {
								
								result = $.parseJSON(msg);
								var chf_field_id = '#__costsheets_chf_' + result['costsheet_id'];
								$(chf_field_id).html($.number( result['amount_chf'], 2, ".", "'" ));

								var sgt = '#sgt' + result['costsheet_pcost_subgroup_id'];
								var amount = result['sgr_total'];
								$(sgt).html($.number( amount, 2, ".", "'" ));

								var gt = '#gt' + result['costsheet_pcost_group_id'];
								amount = result['gr_total'];
								$(gt).html($.number( amount, 2, ".", "'" ));

								var sgt_chf = '#sgt_chf' + result['costsheet_pcost_subgroup_id'];
								amount = result['sgr_total_chf'];
								$(sgt_chf).html($.number( amount, 2, ".", "'" ));

								var gt_chf = '#gt_chf' + result['costsheet_pcost_group_id'];
								amount = result['gr_total_chf'];
								$(gt_chf).html($.number( amount, 2, ".", "'" ));

								if(result['project_share_other'] > 0) {
									$('#project_share_other').val(result['project_share_other']);
								}
							}
						}
					});
			}
			
		});

		$(document).on("change","input",function(){
			
			var field = $(this);
			$(this).removeClass( "error_field" );
			var field_id = $(this).attr('id');
			var error = 0;

			if (field_id.indexOf("costsheet_code") >= 0
				|| field_id.indexOf("costsheet_text") >= 0
				|| field_id.indexOf("costsheet_company") >= 0
				|| field_id.indexOf("costsheet_budget_amount") >= 0
				|| field_id.indexOf("costsheet_partner_contribution") >= 0
				|| field_id.indexOf("costsheet_comment") >= 0) {

				if(field_id.indexOf("costsheet_budget_amount") >= 0) {
					if($(this).val() == ''
						|| $.isNumeric( $(this).val())) {
							
					}
					else {
						$(this).addClass( "error_field" );
						error = 1;
					}
				}
				if(field_id.indexOf("costsheet_partner_contribution") >= 0) {
					if($(this).val() == ''
						|| $.isNumeric( $(this).val())) {
							
					}
					else {
						$(this).addClass( "error_field" );
						error = 1;
					}
				}
				if(error == 0) {	
					
					$(this).css({ 'background': 'transparent url("/public/data/images/icon-checked.png") 3px 50% no-repeat', 'background-position':'center right'});

					$(this).fadeTo('slow', 0.3, function()
					{
						$(this).css('background-image', 'url()');
					}).fadeTo('slow', 1);

					var params = "pid=<?php echo param('pid');?>&id=" + field_id + "&value=" + $(this).val() + '&action=save';
					$.ajax({
						type: "POST",
						data: params,
						url: "include/ajx_save_cost_sheet_position.php",
						success: function(msg){

							if(msg != 'error') {
								result = $.parseJSON(msg);
								var sgt = '#sgt' + result['costsheet_pcost_subgroup_id'];
								var amount = result['sgr_total'];
								$(sgt).html($.number( amount, 2, ".", "'" ));

								var gt = '#gt' + result['costsheet_pcost_group_id'];
								amount = result['gr_total'];
								$(gt).html($.number( amount, 2, ".", "'" ));

								var sgt_chf = '#sgt_chf' + result['costsheet_pcost_subgroup_id'];
								 amount = result['sgr_total_chf'];
								$(sgt_chf).html($.number( amount, 2, ".", "'" ));

								var gt_chf = '#gt_chf' + result['costsheet_pcost_group_id'];
								amount = result['gr_total_chf'];
								$(gt_chf).html($.number( amount, 2, ".", "'" ));

								if(result['project_share_other'] > 0) {
									$('#project_share_other').val(result['project_share_other']);
								}

								var chf_field_id = '#__costsheets_chf_' + result['costsheet_id'];
								$(chf_field_id).html($.number( result['amount_chf'], 2, ".", "'" ));
							}
						}
					});

					
				}
			}
		});
		$('.add_new_line').click(function(event) {
		 
			var object = $(this);
			var data = $(this).attr('data');
			var params = "data=" + data + '&action=new_line';
			$.ajax({
				type: "POST",
				data: params,
				url: "include/ajx_save_cost_sheet_position.php",
				success: function(msg){
					
					if(msg != 'error') {
						
						new_tr='<tr><td ><input name="__costsheets_costsheet_code_' + msg + '" id="__costsheets_costsheet_code_' + msg + '" value="" size="4" type="text"></td><td>&nbsp;</td><td ><input name="__costsheets_costsheet_text_' + msg + '" id="__costsheets_costsheet_text_' + msg + '" value="" size="60" type="text"></td><td>&nbsp;</td><td ><input name="__costsheets_costsheet_company_' + msg + '" id="__costsheets_costsheet_company_' + msg + '" value="" size="18" type="text"></td><td>&nbsp;</td><td><input name="__costsheets_costsheet_budget_amount_' + msg + '" id="__costsheets_costsheet_budget_amount_' + msg + '" value="" size="12" type="text"></td><td>&nbsp;</td><td align="right"><select name="__costsheets_costsheet_currency_id_' + msg + '" id="__costsheets_costsheet_currency_id_' + msg + '"><?php echo $dropdown_currencies_html;?></select></td><td>&nbsp;</td><td><span id="__costsheets_chf_' + msg + '"></span></td><td>&nbsp;</td><td ><input name="__costsheets_costsheet_partner_contribution_' + msg + '" id="__costsheets_costsheet_partner_contribution_' + msg + '" value="" size="6" type="text"></td><td>&nbsp;</td><td ><input name="__costsheets_costsheet_comment_' + msg + '" id="__costsheets_costsheet_comment_' + msg + '" value="" size="30" type="text"></td><td>&nbsp;</td><td><span id="__costsheet_bid_positions_delete_links_' + msg + '"><a class="delete_line" data="' + msg + '" href="javascript:void(0);" title="Remove line"><img src="/pictures/closed.gif"></a></span></td></tr>';

						object.closest('tr').before(new_tr);
						
					}
				}
			});
		});

		$(document).on('click', '.delete_line', function(){
			
			var object = $(this);
			var data = $(this).attr('data');
			var params = "pid=<?php echo param('pid');?>&id=" + data + '&action=delete_line';
			$.ajax({
				type: "POST",
				data: params,
				url: "include/ajx_save_cost_sheet_position.php",
				success: function(msg){
					
					object.parent("span").parent("td").parent("tr").remove();
					if(msg != 'error') {
						result = $.parseJSON(msg);
						var sgt = '#sgt' + result['costsheet_pcost_subgroup_id'];
						var amount = result['sgr_total'];
						$(sgt).html($.number( amount, 2, ".", "'" ));

						var gt = '#gt' + result['costsheet_pcost_group_id'];
						amount = result['gr_total'];
						$(gt).html($.number( amount, 2, ".", "'" ));

						var sgt_chf = '#sgt_chf' + result['costsheet_pcost_subgroup_id'];
						amount = result['sgr_total_chf'];
						$(sgt_chf).html($.number( amount, 2, ".", "'" ));

						var gt_chf = '#gt_chf' + result['costsheet_pcost_group_id'];
						amount = result['gr_total_chf'];
						$(gt_chf).html($.number( amount, 2, ".", "'" ));

						if(result['project_share_other'] > 0) {
							$('#project_share_other').val(result['project_share_other']);
						}
					}
					
				}
			});
		});
	<?php
	}
	?>

});

function add_new_cost_position(pid)
{		
	url = '/user/project_costs_add_positions.php?pid=' + pid + '&mode=budget';;
	$.nyroModalManual({
	  url: url
	});

}


function add_items_from_catalog(pid)
{		
	url = '/user/project_costs_add_items.php?pid=' + pid + '&mode=budget';;
	$.nyroModalManual({
	  url: url
	});

}


</script>

<?php

$page->footer();

?>