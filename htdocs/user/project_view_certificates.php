<?php
/********************************************************************

    project_view_certificates.php

    View Certificates for Items

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-05-01
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-05-01
    Version:        1.1.0

    Copyright (c) 2014, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

if (!has_access("can_view_certificates_in_projects") and !has_access("can_view_his_certificates_in_projects"))
{
	redirect("noaccess.php");
}


/********************************************************************
    prepare all data needed
*********************************************************************/
$user = get_user(user_id());
$user_roles = get_user_roles(user_id());


// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);


// create sql for order items
$sql_order_items = "select certificate_id, order_item_id, order_item_text, order_item_quantity_freezed, ".
                   "    if(item_code <> '', item_code, item_type_name) as item_shortcut, ".
				   " order_items.date_created as oi_created, order_item_ordered, " . 
				   "    address_company, certificate_name " . 
                   "from order_items ".
                   "INNER JOIN items ON order_item_item = item_id ".
					"INNER JOIN item_certificates ON item_certificate_item_id = item_id ".
					"INNER JOIN certificates ON item_certificate_certificate_id = certificate_id ".
					"INNER JOIN item_types ON order_item_type = item_type_id ".
					"INNER JOIN addresses ON address_id = order_item_supplier_address";

$list1_filter = "order_item_order = " . $project["project_order"] .  
				" and order_item_type < 3 " . 
				" and certificate_address_id = order_item_supplier_address ";

    

if(array_key_exists(5, $user_roles) and count($user_roles) == 1) // supplier
{
	$list1_filter .= " and certificate_address_id = " . dbquote($user["address"]); 
}
elseif(array_key_exists(6, $user_roles) and count($user_roles) == 1) // forwarder
{
	$list1_filter .= " and certificate_address_id = " . dbquote($user["address"]); 
}




$expiry_dates = array();
$links = array();
$versions = array();

$sql = $sql_order_items . " where " . $list1_filter;
//echo $sql;
$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	
	if($project["order_archive_date"] != '0000-00-00' and $project["order_archive_date"] != NULL)
	{
		$date_filter = $row["order_item_ordered"];
	}
	else
	{
		$date_filter = date("Y-m-d");
	}
	
	
	$sql_c = "select certificate_file_version, " . 
				   " DATE_FORMAT(certificate_file_expiry_date, '%d.%m.%Y') as certificate_file_expiry_date, " . 
		           " certificate_file_expiry_date as expiry_date, certificate_file_file " . 
                   " from certificate_files " . 
		           " where certificate_file_certificate_id = " . $row["certificate_id"];

		           " order by certificate_file_id DESC";
	$res_c = mysql_query($sql_c) or dberror($sql_c);
	if ($row_c = mysql_fetch_assoc($res_c))
	{
		$versions[$row["certificate_id"]] = $row_c["certificate_file_version"];
		
		if($row_c["expiry_date"] < $row["order_item_ordered"])
		{
			$expiry_dates[$row["certificate_id"]] = '<span class="red">' . $row_c["certificate_file_expiry_date"] . '</span>';
		}
		else
		{
			$expiry_dates[$row["certificate_id"]] = $row_c["certificate_file_expiry_date"];
		}

		$links[$row["certificate_id"]] = '<a href="' . $row_c["certificate_file_file"] . '" target="_blank">' . $row["certificate_name"] . "</a>";
	}
	else
	{
		$links[$row["certificate_id"]] = '<span class="error">' . $row["certificate_name"] . '</span>';
	}
}


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");

require_once "include/project_head_small.php";


$link = "{certificate_file_file}";


/********************************************************************
    Create Standard Item List
*********************************************************************/ 
$list1 = new ListView($sql_order_items);

$list1->set_title("Certificates");
$list1->set_entity("certificates");
$list1->set_filter($list1_filter);
$list1->set_order("item_code");


$list1->add_hidden("pid", param("pid"));

$list1->add_column("item_shortcut", "Item Code");
$list1->add_column("oi_created", "Date");

$list1->add_column("order_item_text", "Item Name");
$list1->add_text_column("certificate_name", "Certificate", COLUMN_UNDERSTAND_HTML, $links);
$list1->add_text_column("certificate_file_version", "Version", 0, $versions);
$list1->add_text_column("certificate_file_expiry_date", "Expiry", COLUMN_UNDERSTAND_HTML, $expiry_dates);
$list1->add_column("address_company", "Supplier");



/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list1->populate();


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Project Certificates");

$form->render();


$list1->render();

require_once "include/project_footer_logistic_state.php";
$page->footer();

?>