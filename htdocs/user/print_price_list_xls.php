<?php
/********************************************************************

    print_price_list_xls.php

    Export XLS from catalog

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2016-12-22
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2016-12-22
    Version:        1.0.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_view_catalog");

require_once "include/get_functions.php";
require_once "../libraries/phpexcel/Classes/PHPExcel.php";


/********************************************************************
    prepare all data needed
*********************************************************************/
// get the region of users' address
$user_region = get_user_region(user_id());
$roles = get_user_roles(user_id());
$user = get_user(user_id());
$user_country = $user["country"];
$currency = get_address_currency($user["address"]);


$cid = param('cid');
$plid = param('plid');


$searchterm = '';
if(array_key_exists('searchterm', $_GET) and $_GET['searchterm'] != '') 
{
	$searchterm = $_GET['searchterm'];
}
else
{
	$searchterm = param('searchterm');
}


// get category name
if($searchterm)
{
	$category_name = "Matching searchterm '" . $searchterm . "'";
}
elseif($cid)
{
	$category_name = get_category_name($plid, $cid, param("scid"));
}
else
{
	$category_name = get_category_name($plid, 0, 0);
}

$sql = "select DISTINCT item_id, item_category_id, item_category_name, item_code, item_name,item_price, ".
       " concat(address_company, ' - ', item_category_name) as group_head, item_visible, " .
	   "supplier_item_price, currency_symbol, supplier_address " . 
       " from product_lines  " .
	   "INNER JOIN product_line_supplying_groups ON product_line_supplying_group_line_id = product_line_id
	    INNER JOIN item_supplying_groups ON item_supplying_group_supplying_group_id = product_line_supplying_group_group_id
		INNER JOIN items ON item_id = item_supplying_group_item_id
		INNER JOIN supplying_groups on supplying_group_id = item_supplying_group_supplying_group_id 
		INNER JOIN productline_regions ON productline_region_productline = product_line_id
		INNER JOIN item_categories ON item_category_id = item_category
		LEFT JOIN item_subcategories on item_subcategory_id = item_subcategory
		INNER JOIN item_countries ON item_country_item_id = item_id 
		LEFT join item_pos_types on item_pos_type_item = item_id 
		LEFT JOIN suppliers on supplier_item = item_id
	    LEFT JOIN addresses on address_id = supplier_address  
		LEFT JOIN currencies on currency_id = supplier_item_currency";



if(has_access("can_edit_catalog") 
  or has_access("can_enter_orders_for_other_companies"))
{
	
    if($searchterm)
	{
		$list_filter = "(REPLACE(LOWER(item_code), ' ', '') like '%" . strtolower($searchterm) . "%' OR REPLACE(LOWER(item_name), ' ', '') like '%" .  strtolower($searchterm) . "%' OR REPLACE(LOWER(item_category_name), ' ', '') like '%" .  strtolower($searchterm) . "%' OR REPLACE(LOWER(product_line_name), ' ', '') like '%" .  strtolower($searchterm) . "%' )";


		$list_filter .= " and (product_line_clients = 1 
						 or product_line_budget = 1)
						 and product_line_active = 1
						 and item_category_active = 1 
						 and item_active = 1
						  and item_type = 1";
	}
	elseif($cid)
	{
		$list_filter = " item_category = " . dbquote($cid) .
					   " and product_line_id = " . dbquote($plid) .
					   " and (product_line_clients = 1 
						 or product_line_budget = 1)
						 and product_line_active = 1
						 and item_category_active = 1 
						 and item_active = 1
						  and item_type = 1";
	}
	else
	{
		$list_filter = " product_line_id = " . dbquote($plid) .
					   " and (product_line_clients = 1 
						 or product_line_budget = 1)
						 and product_line_active = 1
						 and item_category_active = 1 
						 and item_active = 1
						  and item_type = 1";
	}
}
else
{
	
	if($searchterm)
	{
		
		$list_filter = "(REPLACE(LOWER(item_code), ' ', '') like '%" . strtolower($searchterm) . "%' OR REPLACE(LOWER(item_name), ' ', '') like '%" .  strtolower($searchterm) . "%' OR REPLACE(LOWER(item_category_name), ' ', '') like '%" .  strtolower($searchterm) . "%' OR REPLACE(LOWER(product_line_name), ' ', '') like '%" .  strtolower($searchterm) . "%' )";

		$list_filter .= " and product_line_active = 1
						 and item_category_active = 1
						 and (item_visible = 1 or item_visible_in_projects) 
						 and item_active = 1
						 and item_type = 1 
						 and product_line_clients=1 
						 and item_country_country_id = " . dbquote($user_country);
	}
	elseif($cid)
	{
		$list_filter = " item_category = " . dbquote($cid) .
					    " and product_line_id = " . dbquote($plid) .
						" and product_line_active = 1
						 and item_category_active = 1
						 and (item_visible = 1 or item_visible_in_projects) 
						 and item_active = 1
						 and item_type = 1 
						 and product_line_clients=1 
						 and item_country_country_id = " . dbquote($user_country);
	}
	else
	{
		$list_filter = " product_line_id = " . dbquote($plid) .
						" and product_line_active = 1
						 and item_category_active = 1
						 and (item_visible = 1 or item_visible_in_projects) 
						 and item_active = 1
						 and item_type = 1 
						 and product_line_clients=1 
						 and item_country_country_id = " . dbquote($user_country);
	}
}


if(param("scid")) {
	$list_filter .= ' and item_subcategory = ' . dbquote(param("scid"));
}
elseif(!$searchterm)
{
	//$list_filter .= ' and (item_subcategory is null or item_subcategory = 0)';
}

if(param("sg")) {
	$list_filter .= ' and item_supplying_group_supplying_group_id = ' . dbquote(param("sg"));
}



/********************************************************************
    XLS Params
*********************************************************************/
if($cid) {
	$captions['A'] = "Item";
	$captions['B'] = "Name";
	$captions['C'] = "Supplier's price";
	$captions['D'] = "Your price";

	$colwidth = array();
	$colwidth['A'] = "20";
	$colwidth['B'] = "65";
	$colwidth['C'] = "14";
	$colwidth['D'] = "14";
}
else
{
	$captions['A'] = "Item category";
	$captions['B'] = "Item";
	$captions['C'] = "Name";
	$captions['D'] = "Supplier's price";
	$captions['E'] = "Your price";

	$colwidth = array();
	$colwidth['A'] = "20";
	$colwidth['B'] = "20";
	$colwidth['C'] = "65";
	$colwidth['D'] = "14";
	$colwidth['E'] = "14";
}

//output formats
$default_border = array(
    'style' => PHPExcel_Style_Border::BORDER_THIN,
    'color' => array('rgb'=>'1006A3')
);

$style_normal_border = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '1006A3'),
		),
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
	'font' => array(
	    'name' => 'Arial',
        'bold' => false,
		'size' => 9
    )
);


$style_normal_border_right = array(
	'borders' => array(
		'outline' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('rgb' => '1006A3'),
		),
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	),
	'font' => array(
	    'name' => 'Arial',
        'bold' => false,
		'size' => 9
    )
);


$style_title = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
    'font' => array(
	    'name' => 'Arial',
        'bold' => true,
		'size' => 16
    )
);


$style_title_right = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
    'font' => array(
	    'name' => 'Arial',
        'bold' => true,
		'size' => 16
    )
);

$style_header = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'font' => array(
	    'name' => 'Arial',
        'bold' => true,
    )
);

$style_header_right = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	),
    'font' => array(
	    'name' => 'Arial',
        'bold' => true,
    )
);

$style_bold = array(
	'font' => array(
	    'name' => 'Arial',
        'bold' => true
    )
);


$style_red = array(
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
	'font' => array(
	    'name' => 'Arial',
        'bold' => true,
		'size' => 8,
		'color' => array('rgb' => 'FF0000'),
    )
);



/********************************************************************
    Start output
*********************************************************************/
$objPHPExcel = new PHPExcel();

$logo = new PHPExcel_Worksheet_Drawing();
$logo->setName('Logo');
$logo->setDescription('Logo');
$logo->setPath('../pictures/brand_logo.jpg');
$logo->setHeight(36);
$logo->setWidth(113);


$sheet = $objPHPExcel->getActiveSheet();
$objPHPExcel->getActiveSheet()->setTitle('Price list');

$logo->setWorksheet($objPHPExcel->getActiveSheet());


//default styles
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(10); 


// HEADRES ROW 1
$sheet->setCellValue('B1', 'Price List ' . $category_name . ' (' . date("d.m.Y H:i:s") . ')');
$sheet->getStyle('B1')->applyFromArray( $style_title );
$sheet->getRowDimension('1')->setRowHeight(36);


$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
//$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
//$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(1);

$row_index = 2;

$sheet->setCellValue("A". $row_index, 'All amounts for HQ supplied items converted to your currency are for reference only as the exchange rate may change.');
$sheet->getStyle("A" . $row_index)->applyFromArray( $style_red );


$row_index = 4;

// HEADRES ROWS (Captions)

$sheet->setCellValue("A". $row_index, $captions['A']);
$sheet->setCellValue("B". $row_index, $captions['B']);
$sheet->setCellValue("C". $row_index, $captions['C']);
$sheet->setCellValue("D". $row_index, $captions['D']);

if(!$cid) {
	$sheet->setCellValue("E". $row_index, $captions['E']);
}

if($cid) {
	$sheet->getStyle("A" . $row_index)->applyFromArray( $style_header );
	$sheet->getStyle("B" . $row_index)->applyFromArray( $style_header );
	$sheet->getStyle("C" . $row_index)->applyFromArray( $style_header_right );
	$sheet->getStyle("D" . $row_index)->applyFromArray( $style_header_right );
}
else {
	$sheet->getStyle("A" . $row_index)->applyFromArray( $style_header );
	$sheet->getStyle("B" . $row_index)->applyFromArray( $style_header );
	$sheet->getStyle("C" . $row_index)->applyFromArray( $style_header );
	$sheet->getStyle("D" . $row_index)->applyFromArray( $style_header_right );
	$sheet->getStyle("E" . $row_index)->applyFromArray( $style_header_right );
}

//Format column heights and witdhs
foreach($colwidth as $col=>$width) {
	$sheet->getColumnDimension($col)->setWidth($width);
}

$row_index++;

//output items

$prices = array();
$prices_chf = array();
$tmp_sql = $sql . " where " . $list_filter . " order by item_category_name, item_code, item_name";


$res = mysql_query($tmp_sql) or dberror($tmp_sql);
while ($row = mysql_fetch_assoc($res))
{
	
	$prices[$row["item_id"]] = number_format($row["supplier_item_price"], 2) . " " . $row["currency_symbol"];

	
	$supplier_currency = get_address_currency($row["supplier_address"]);
	$price_chf = $row["supplier_item_price"] * $supplier_currency["exchange_rate"] / $supplier_currency["factor"];
	$prices_client[$row["item_id"]] = number_format(round((($price_chf / $currency["exchange_rate"]) * $currency["factor"]),2), 2) . " " . $currency["symbol"];


	if($cid) {
		$sheet->setCellValue("A". $row_index, $row["item_code"]);
		$sheet->setCellValue("B". $row_index, $row["item_name"]);
		$sheet->setCellValue("C". $row_index, $prices[$row["item_id"]]);
		$sheet->setCellValue("D". $row_index, $prices_client[$row["item_id"]]);
		
		
		$sheet->getStyle("A" . $row_index)->applyFromArray( $style_normal_border );
		$sheet->getStyle("B" . $row_index)->applyFromArray( $style_normal_border );
		$sheet->getStyle("C" . $row_index)->applyFromArray( $style_normal_border_right );
		$sheet->getStyle("D" . $row_index)->applyFromArray( $style_normal_border_right );
	}
	else
	{
		$sheet->setCellValue("A". $row_index, $row["item_category_name"]);
		$sheet->setCellValue("B". $row_index, $row["item_code"]);
		$sheet->setCellValue("C". $row_index, $row["item_name"]);
		$sheet->setCellValue("D". $row_index, $prices[$row["item_id"]]);
		$sheet->setCellValue("E". $row_index, $prices_client[$row["item_id"]]);
		
		$sheet->getStyle("A" . $row_index)->applyFromArray( $style_normal_border );
		$sheet->getStyle("B" . $row_index)->applyFromArray( $style_normal_border );
		$sheet->getStyle("C" . $row_index)->applyFromArray( $style_normal_border );
		$sheet->getStyle("D" . $row_index)->applyFromArray( $style_normal_border_right );
		$sheet->getStyle("E" . $row_index)->applyFromArray( $style_normal_border_right );
	}

	$row_index++;
	
}



/********************************************************************
    Start output
*********************************************************************/
$filename = 'item_price_list_' . $category_name . '_'. date('Ymd H:i:s') . '.xls';
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
header('Pragma: ');

//send data
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');


?>