<?php
/********************************************************************

    projects_cms_completed_xls.php

    Print list of completed CMS

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2016-02-01
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2016-02-01
    Version:        1.0.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("can_view_projects");
require_once "../include/phpexcel/PHPExcel.php";

require_once "include/get_functions.php";
require_once "include/order_state_constants.php";


/********************************************************************
    prepare all data needed
*********************************************************************/

$sql = "select distinct project_id, project_order, project_number, project_projectkind, project_cost_type, " .
       "project_actual_opening_date, product_line_name, postype_name, " . 
       "concat(order_shop_address_place,', ', order_shop_address_company) as posaddress, country_name, ".
       "    rtcs.user_name as project_retail_coordinator, rtos.user_name as order_retail_operator,".
       "    order_id, order_actual_order_state_code, project_costtype_text,  ".
	   " project_cost_cms_completion_date, project_cost_cms_completion2_date, project_cost_cms_approved_date, project_cost_cms_controlled_date, project_state_text, " .
	   "if(project_state IN (2,4), " . 
	   "if(project_state = 2, project_state_text, project_state_text), DATE_FORMAT(project_real_opening_date,'%d.%m.%Y')) as real_opening_date ".
       "from projects ".
       "left join orders on project_order = order_id ".
	   "inner join project_costs on project_cost_order = project_order " .
       "left join project_costtypes on project_costtype_id = project_cost_type " .
       "left join product_lines on project_product_line = product_line_id ".
       "left join postypes on postype_id = project_postype ".
	   "left join project_states on project_state_id = project_state ".
       "left join countries on order_shop_address_country = countries.country_id ".
       "left join users as rtcs on project_retail_coordinator = rtcs.user_id " . 
	   "left join users as rtos on order_retail_operator = rtos.user_id ";



if (has_access("has_access_to_all_projects"))
{
	$list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '890' ";
}
else
{

	$condition = get_user_specific_order_list(user_id(), 1, $user_roles);

	if ($condition == "")
	{
		//$list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '890'  " .
		//			   "   and order_retail_operator is null";

		$list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '890'  ";


	}
	else
	{
		//$list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '890'  " .
		//			   "   and (" . $condition . " or order_retail_operator is null)";

		$list_filter = "(order_archive_date is null or order_archive_date = '0000-00-00') and order_actual_order_state_code < '890'  " .
					   "   and (" . $condition . ")";
	}
}



$images = array();
$sql_images = "select project_id from projects " .
			  "left join orders on order_id = project_order " .
			  "where (order_retail_operator is null " .
			  "or project_retail_coordinator is null " .
			  "or project_real_opening_date is null " . 
			  "or project_real_opening_date = '0000-00-00') " .
			  "and order_actual_order_state_code = '100' ".
			  "   and " . $list_filter;


$res = mysql_query($sql_images) or dberror($sql_images);

while ($row = mysql_fetch_assoc($res))
{
	$images[$row["project_id"]] = "/pictures/bullet_ball_glass_red.gif";
}



// check if all 1:n relations in the process are fully processed
$cms_completed = array();
$cms_completed2 = array();
$cms_approved = array();
$cms_controlled = array();
$pos_pictures_uploaded = array();
$missing_pos_pictures = array();
if (has_access("has_access_to_all_projects"))
{
    $sql_tmp = $sql . " where " . $list_filter;
    $res = mysql_query($sql_tmp);

    while($row = mysql_fetch_assoc($res))
    {
		if($row['project_cost_cms_completion_date'] != NULL 
			and $row['project_cost_cms_completion_date'] != '0000-00-00')
		{
			$cms_completed[$row["project_id"]] = mysql_date_to_xls_date($row["project_cost_cms_completion_date"]);
		}
		else
		{
			$cms_completed[$row["project_id"]] = mysql_date_to_xls_date("");
		}

		if($row['project_cost_cms_completion2_date'] != NULL 
			and $row['project_cost_cms_completion2_date'] != '0000-00-00')
		{
			$cms_completed2[$row["project_id"]] =  mysql_date_to_xls_date($row["project_cost_cms_completion2_date"]);
		}
		else
		{
			$cms_completed2[$row["project_id"]] =  mysql_date_to_xls_date("");
		}


		if($row['project_cost_cms_approved_date'] != NULL 
			and $row['project_cost_cms_approved_date'] != '0000-00-00')
		{
			$cms_approved[$row["project_id"]] =  mysql_date_to_xls_date($row["project_cost_cms_approved_date"]);
		}
		else
		{
			$cms_approved[$row["project_id"]] =  mysql_date_to_xls_date("");
		}


		if($row['project_cost_cms_controlled_date'] != NULL 
			and $row['project_cost_cms_controlled_date'] != '0000-00-00')
		{
			$cms_controlled[$row["project_id"]] =  mysql_date_to_xls_date($row["project_cost_cms_controlled_date"]);
		}
		else
		{
			$cms_controlled[$row["project_id"]] =  mysql_date_to_xls_date("");
		}
	
		
		$sql_p = "select date_created from order_files " . 
				 " where order_file_category = 11 " . 
			     " and order_file_order = " . dbquote($row["project_order"]) .
			     " order by date_created DESC ";

		$res_p = mysql_query($sql_p) or dberror($sql_p);
		$row_p = mysql_fetch_assoc($res_p);
		if ($row_p["date_created"])
		{
			$pos_pictures_uploaded[$row["project_id"]] = substr($row_p["date_created"], 0, 10);
		}
		else
		{
			$pos_pictures_uploaded[$row["project_id"]] = "";
			$missing_pos_pictures[] =  $row["project_id"];
		}
    }
}


//$list_filter .= " and project_cost_cms_completed = 1 and order_actual_order_state_code >= 800 ";
$list_filter .= " and project_cost_cms_completed = 1";

if(param("statusfilter") == 2 and count($missing_pos_pictures) > 0)
{
	$list_filter .= " and project_id in (" . implode(', ', $missing_pos_pictures). ") ";
}
elseif(param("statusfilter") == 3)
{
	$list_filter .= " and project_cost_cms_approved <> 1";
}
elseif(param("statusfilter") == 4)
{
	$list_filter .= " and project_cost_cms_controlled <> 1 and project_cost_type = 1";
}
elseif(param("statusfilter") == 5)
{
	$list_filter .= " and (project_actual_opening_date is null or project_actual_opening_date = '0000-00-00' )";
}


if(param('search_term')) {

	$search_term = trim(param('search_term'));
	$list_filter .= ' and (project_number like "%' . $search_term . '%" 
	                  or postype_name like "%' . $search_term . '%" 
					  or project_costtype_text like "%' . $search_term . '%"
					  or project_state_text like "%' . $search_term . '%"
					  or product_line_name like "%' . $search_term . '%"
					  or order_actual_order_state_code like "%' . $search_term . '%"
					  or order_shop_address_place like "%' . $search_term . '%"
					  or order_shop_address_company like "%' . $search_term . '%"
					  or country_name like "%' . $search_term . '%")';
}



$sql .= " where " . $list_filter;

/********************************************************************
    XLS Params
*********************************************************************/
$captions['A'] = "Country";
$captions['B'] = "POS Address";
$captions['C'] = "Project Number";
$captions['D'] = "Legal Type";
$captions['E'] = "POS Type";
$captions['F'] = "Status";
$captions['G'] = "Project Leader";
//$captions['H'] = "Logistic Coordinator";
//$captions['I'] = "Agreed Opening Date";
//$captions['J'] = "Actual Opening Date";
//$captions['K'] = "POS Images Uploaded";
//$captions['L'] = "CMS Logistics";
//$captions['M'] = "CMS Client";
//$captions['N'] = "CMS Development";
//$captions['O'] = "CMS Controller";
$captions['H'] = "Agreed Opening Date";
$captions['I'] = "Actual Opening Date";
$captions['J'] = "POS Images Uploaded";
$captions['K'] = "CMS Client";


$colwidth = array();
$colwidth['A'] = "15";
$colwidth['B'] = "15";
$colwidth['C'] = "15";
$colwidth['D'] = "15";
$colwidth['E'] = "15";
$colwidth['F'] = "15";
$colwidth['G'] = "15";
$colwidth['H'] = "15";
$colwidth['I'] = "15";
$colwidth['J'] = "15";
$colwidth['K'] = "15";
//$colwidth['L'] = "15";
//$colwidth['M'] = "15";
//$colwidth['N'] = "15";
//$colwidth['O'] = "15";


//output formats
$default_border = array(
    'style' => PHPExcel_Style_Border::BORDER_THIN,
    'color' => array('rgb'=>'1006A3')
);

$style_normal = array(
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
);


$style_normal_right = array(
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	),
);


$style_title = array(
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
	'alignment' => array(
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
	),
    'font' => array(
        'bold' => true,
		'size' => 16
    )
);

$style_header = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
	),
    'font' => array(
        'bold' => true,
    )
);

$style_header_right = array(
    'borders' => array(
        'bottom' => $default_border,
        'left' => $default_border,
        'top' => $default_border,
        'right' => $default_border,
    ),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
	),
    'font' => array(
        'bold' => true,
    )
);

$style_bold = array(
	'font' => array(
        'bold' => true
    )
);



/********************************************************************
    Start output
*********************************************************************/
$objPHPExcel = new PHPExcel();

$logo = new PHPExcel_Worksheet_Drawing();
$sheet = $objPHPExcel->getActiveSheet();
$objPHPExcel->getActiveSheet()->setTitle('Project with completed CMS');



//default styles
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(10); 


// HEADRES ROW 1
$sheet->setCellValue('A1', 'Project with completed CMS (' . date("d.m.Y H:i:s") . ')');
$sheet->getStyle('A1')->applyFromArray( $style_title );
$sheet->getRowDimension('1')->setRowHeight(36);


$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(1);



$cell_index = 0;
$row_index = 3;

foreach($captions as $col=>$caption)
{
	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $caption);
	if($colwidth[$col] < strlen($caption)+ 3)
	{
		$colwidth[$col] = strlen($caption) + 3;
	}
	$cell_index++;
	$sheet->getStyle($col . '3')->applyFromArray( $style_header );
}

$cell_index = 0;
$row_index = 4;


$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["country_name"]);
	if($colwidth['A'] < strlen($row["country_name"])){$colwidth['A'] = strlen($row["country_name"]);}
	$cell_index++;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["posaddress"]);
	if($colwidth['B'] < strlen($row["posaddress"])){$colwidth['B'] = strlen($row["posaddress"]);}
	$cell_index++;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["project_number"]);
	if($colwidth['C'] < strlen($row["project_number"])){$colwidth['C'] = strlen($row["project_number"]);}
	$cell_index++;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["project_costtype_text"]);
	if($colwidth['D'] < strlen($row["project_costtype_text"])){$colwidth['D'] = strlen($row["project_costtype_text"]);}
	$cell_index++;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["postype_name"]);
	if($colwidth['E'] < strlen($row["postype_name"])){$colwidth['E'] = strlen($row["postype_name"]);}
	$cell_index++;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["order_actual_order_state_code"]);
	if($colwidth['H'] < strlen($row["order_actual_order_state_code"])){$colwidth['H'] = strlen($row["order_actual_order_state_code"]);}

	$sheet->getStyle('H' . $row_index)->applyFromArray( $style_normal );

	$cell_index++;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["project_retail_coordinator"]);
	if($colwidth['I'] < strlen($row["project_retail_coordinator"])){$colwidth['I'] = strlen($row["project_retail_coordinator"]);}
	$cell_index++;

	/*
	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["order_retail_operator"]);
	if($colwidth['J'] < strlen($row["order_retail_operator"])){$colwidth['J'] = strlen($row["order_retail_operator"]);}
	$cell_index++;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["real_opening_date"]);
	if($colwidth['K'] < strlen($row["real_opening_date"])){$colwidth['K'] = strlen($row["real_opening_date"]);}

	$sheet->getStyle('K' . $row_index)->applyFromArray( $style_normal);
	$sheet->getStyleByColumnAndRow($cell_index,$row_index)->getNumberFormat()->setFormatCode(       PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);


	$cell_index++;
	

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, mysql_date_to_xls_date($row["project_actual_opening_date"]));
	if($colwidth['L'] < strlen($row["project_actual_opening_date"])){$colwidth['L'] = strlen($row["project_actual_opening_date"]);}

	$sheet->getStyle('L' . $row_index)->applyFromArray( $style_normal);
	$sheet->getStyleByColumnAndRow($cell_index,$row_index)->getNumberFormat()->setFormatCode(       PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);

	$cell_index++;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index,  mysql_date_to_xls_date($pos_pictures_uploaded[$row["project_id"]]));
	if($colwidth['M'] < strlen($pos_pictures_uploaded[$row["project_id"]])){$colwidth['M'] = strlen($pos_pictures_uploaded[$row["project_id"]]);}

	$sheet->getStyle('M' . $row_index)->applyFromArray( $style_normal);
	$sheet->getStyleByColumnAndRow($cell_index,$row_index)->getNumberFormat()->setFormatCode(       PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);

	$cell_index++;


	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $cms_completed[$row["project_id"]]);
	if($colwidth['N'] < strlen($cms_completed[$row["project_id"]])){$colwidth['N'] = strlen($cms_completed[$row["project_id"]]);}

	$sheet->getStyle('N' . $row_index)->applyFromArray( $style_normal);
	$sheet->getStyleByColumnAndRow($cell_index,$row_index)->getNumberFormat()->setFormatCode(       PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);

	$cell_index++;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $cms_completed2[$row["project_id"]]);
	if($colwidth['O'] < strlen($cms_completed2[$row["project_id"]])){$colwidth['O'] = strlen($cms_completed2[$row["project_id"]]);}

	$sheet->getStyle('O' . $row_index)->applyFromArray( $style_normal);
	$sheet->getStyleByColumnAndRow($cell_index,$row_index)->getNumberFormat()->setFormatCode(       PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);

	$cell_index++;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $cms_approved[$row["project_id"]]);
	if($colwidth['P'] < strlen($cms_approved[$row["project_id"]])){$colwidth['P'] = strlen($cms_approved[$row["project_id"]]);}

	$sheet->getStyle('P' . $row_index)->applyFromArray( $style_normal);
	$sheet->getStyleByColumnAndRow($cell_index,$row_index)->getNumberFormat()->setFormatCode(       PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);

	$cell_index++;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $cms_controlled[$row["project_id"]]);
	if($colwidth['Q'] < strlen($cms_controlled[$row["project_id"]])){$colwidth['Q'] = strlen($cms_controlled[$row["project_id"]]);}

	$sheet->getStyle('Q' . $row_index)->applyFromArray( $style_normal);
	$sheet->getStyleByColumnAndRow($cell_index,$row_index)->getNumberFormat()->setFormatCode(       PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);
	*/


	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $row["real_opening_date"]);
	if($colwidth['J'] < strlen($row["real_opening_date"])){$colwidth['J'] = strlen($row["real_opening_date"]);}

	$sheet->getStyle('J' . $row_index)->applyFromArray( $style_normal);
	$sheet->getStyleByColumnAndRow($cell_index,$row_index)->getNumberFormat()->setFormatCode(       PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);


	$cell_index++;
	

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, mysql_date_to_xls_date($row["project_actual_opening_date"]));
	if($colwidth['K'] < strlen($row["project_actual_opening_date"])){$colwidth['K'] = strlen($row["project_actual_opening_date"]);}

	$sheet->getStyle('K' . $row_index)->applyFromArray( $style_normal);
	$sheet->getStyleByColumnAndRow($cell_index,$row_index)->getNumberFormat()->setFormatCode(       PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);

	$cell_index++;

	$sheet->setCellValueByColumnAndRow($cell_index, $row_index,  mysql_date_to_xls_date($pos_pictures_uploaded[$row["project_id"]]));
	if($colwidth['L'] < strlen($pos_pictures_uploaded[$row["project_id"]])){$colwidth['L'] = strlen($pos_pictures_uploaded[$row["project_id"]]);}

	$sheet->getStyle('L' . $row_index)->applyFromArray( $style_normal);
	$sheet->getStyleByColumnAndRow($cell_index,$row_index)->getNumberFormat()->setFormatCode(       PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);

	$cell_index++;


	$sheet->setCellValueByColumnAndRow($cell_index, $row_index, $cms_completed2[$row["project_id"]]);
	if($colwidth['M'] < strlen($cms_completed2[$row["project_id"]])){$colwidth['M'] = strlen($cms_completed2[$row["project_id"]]);}

	$sheet->getStyle('M' . $row_index)->applyFromArray( $style_normal);
	$sheet->getStyleByColumnAndRow($cell_index,$row_index)->getNumberFormat()->setFormatCode(       PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14);


	//$sheet->getStyle($col . '3')->applyFromArray( $style_header );


	$cell_index = 0;
	$row_index++;
}


//Format column heights and witdhs
foreach($colwidth as $col=>$width) {
	$sheet->getColumnDimension($col)->setWidth($width);
}


/********************************************************************
    Start output
*********************************************************************/
$filename = 'projects_with_completed_cms_'. date('Ymd H:i:s') . '.xls';
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
header('Pragma: ');

//send data
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');


?>