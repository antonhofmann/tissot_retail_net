<?php
/********************************************************************

    project_sheet_2016_pdf.php

    Creates a PDF project sheet for projects from 2016-08-08 on.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2016-08-08
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2016-08-08
    Version:        1.1.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.

*********************************************************************/
require "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";



function limit($str, $length)
{
    global $pdf;

    $length -= 2;

    while (true)
    {
        if ($pdf->GetStringWidth($str) < $length)
        {
            return $str;
        }
        else
        {
            $str = substr($str, 0, strlen($str) - 1);
        }
    }
}





//update necessary data in database
$project = get_project(param("pid"));
$client_address = get_address($project["order_client_address"]);


//Pos Name
$country_name = "";

$sql = "select country_name from countries where country_id = " . $project["order_shop_address_country"];
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$country_name = $row["country_name"];
}

$pos_name = $country_name . ", " . $project["order_shop_address_place"] . ", " . $project["order_shop_address_company"];

//business unit
$business_unit = "";
$sql = "select postype_business_unit from postypes where postype_id = " . $project["project_postype"];
$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$business_unit = $row["postype_business_unit"];
}


$retail_coordinator = get_user($project["order_retail_operator"]);
$project_leader = substr($retail_coordinator["firstname"], 0, 1) . ". " . $retail_coordinator["name"];

$project_cost_center = "10220 / Retail"; //default

$account_number_name = "E444 / 786000 Retail Furniture"; //default




// supplier with the highest amount of order items invoiced directly to Tissot
$supplier_with_the_highest_amount = 0;
$highest_amount = 0;

$number_of_suppliers = '';

$sql_suppliers = "select DISTINCT address_id, address_company, " . 
                 " sum(order_item_supplier_exchange_rate*order_item_quantity*order_item_supplier_price/currency_factor) as amount " . 
				 "from order_items " . 
				 "left join addresses on order_item_supplier_address = address_id ".
				 " left join currencies on currency_id = order_item_supplier_currency " . 
				 " where (order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
                      " and order_item_quantity > 0 " . 
					  "   and (order_item_type <= " . ITEM_TYPE_SPECIAL .
		              "   or order_item_type = " . ITEM_TYPE_SERVICES . ") " .
                      "   and order_item_order = " . $project["project_order"] . 
				 " group by address_id, address_company";

$res = mysql_query($sql_suppliers) or dberror($sql_suppliers);

while ($row = mysql_fetch_assoc($res))
{

	$sql = "select supplier_client_invoice_id ". 
		   "from supplier_client_invoices " . 
		   "where supplier_client_invoice_supplier = " . dbquote($row["address_id"]) .
		   " and supplier_client_invoice_client = " . dbquote($client_address["id"])  . 
		   " and supplier_client_invoice_startdate <= " . dbquote(date("Y-m-d")) . 
			   " and supplier_client_invoice_enddate >= " . dbquote(date("Y-m-d"));

	$res_s = mysql_query($sql) or dberror($sql);
	if ($row_s = mysql_fetch_assoc($res_s))
	{
		
	}
	else
	{
		if($row["amount"] > $highest_amount)
		{
			$supplier_with_the_highest_amount = $row["address_id"];
			$highest_amount = $row["amount"];
		}
	}

	if($number_of_suppliers == ''){
		$number_of_suppliers = 1;
	}
	else {
		$number_of_suppliers++;
	}

}


//check if standard financial justifications are present
$sql = "select count(project_financial_justification_id) as num_recs ". 
       "from project_financial_justifications " .
       "where project_financial_justification_project = " . id();

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);
if ($row["num_recs"] == 0)
{
	$rtc = get_user($project["project_retail_coordinator"]);
	$rto = get_user($project["order_retail_operator"]);
	
	$address = get_address($project["order_client_address"]);
	$cli = $address["company"] . ", " . $address["place"]  . ", " . $address["country_name"] ;
	
	//create standard financial justificateions
	$fj[1] = $project["product_line_name"] . " " . 
		     $project["postype_name"] . " in " . 
		     $country_name . " in " . 
		     $project["order_shop_address_place"] . ", " .
		     $project["order_shop_address_company"] . ", " .
			 $project["order_shop_address_address"];
	

	//$fj[2] = "Project Coordinator: " . $rtc["firstname"] . " " . $rtc["name"];
	//$fj[3] = "Project Operator: " . $rto["firstname"] . " " . $rto["name"];
	$fj[4] = "Project will be reinvoced 100% to: " . $cli;

	foreach($fj as $key=>$value)
	{
		$sql_i = "insert into project_financial_justifications (".
			     "project_financial_justification_project, " . 
				 "project_financial_justification_description, " .
				 "project_financial_justification_priority, " .
				 "user_created, " .
				 "date_created) values (" .
			     id() . ", " . 
			     dbquote($value) . ", " . 
			     $key . ", " . 
			     dbquote(user_login()) . ", " .
			     "now())";
				 $result = mysql_query($sql_i) or dberror($sql_i);
	}

}

//do the data update
$result = project_sheet_update_cost_groups($project["project_order"]);

//update project record
$fields = array();

$value = "Project Sheet " . date("Y");
$fields[] = "project_sheet_name = " . dbquote($value);

$value = $pos_name;
$fields[] = "project_name = " . dbquote($value);

$value = $business_unit;
$fields[] = "project_business_unit = " . dbquote($value);


$value = $project_cost_center;
$fields[] = "project_cost_center = " . dbquote($value);

$value = $account_number_name;
$fields[] = "project_account_number = " . dbquote($value);

$value = date("Y-m-d");
$fields[] = "project_opening_date = " . dbquote($value);

$value = 0;
$fields[] = "project_share_company = " . dbquote($value);

$value = 100;
$fields[] = "project_share_other = " . dbquote($value);

$value = 1;
$fields[] = "project_is_on_project_sheet_list = " . $value;


$value = "current_timestamp";
$fields[] = "date_modified = " . $value;

$value = dbquote($_SESSION["user_login"]);
$fields[] = "user_modified = " . $value;


$sql = "update projects set " . join(", ", $fields) . " where project_id = " . dbquote(param("pid"));

if($projcet["project_is_on_project_sheet_list"] != 1) //only update if not updated yet
{
	mysql_query($sql) or dberror($sql);



	if($supplier_with_the_highest_amount > 0)
	{
		$sql = "update orders ".
			   "set order_supplier_address = " . $supplier_with_the_highest_amount . " ".
			   "where order_id = ". $project["project_order"];

		$res = mysql_query($sql) or dberror($sql);
	}

}

//begin PDF output

$SUPPRESS_HEADERS = true;
define ('LINE_BREAK', false); // define, if lines of financial justification are wrapped 



require_once('../include/tcpdf/config/lang/eng.php');
require_once('../include/tcpdf/tcpdf.php');


// Check access and id parameter

if (!isset($id))
{
    error("Must be called width id of an existing order record");
}



//get directly invoiced suppliers and amounts
$project = get_project(param("pid"));
$project_manager = get_user($project["project_retail_coordinator"]);
$logistics_coordinator = get_user($project["order_retail_operator"]);

$client_address = get_address($project["order_client_address"]);

$directly_invoiced_amount = 0;
$directly_invoiced_amount_loc = 0;

$suppliers = array();
$swatch = array();
$excluded_suppliers = array();

/*
$sql_suppliers = "select DISTINCT order_item_supplier_address, address_id, address_company, address_shortcut, " .
                 " sum(order_item_quantity_freezed*order_item_system_price_freezed) as amount,  " .
				 " sum(order_item_quantity_freezed*order_item_client_price_freezed) as amount_loc " . 
				 "from order_items " . 
				 "left join addresses on order_item_supplier_address = address_id ".
				 " where (order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
                      "   and (order_item_type <= " . ITEM_TYPE_SPECIAL .
		              "   or order_item_type = " . ITEM_TYPE_SERVICES . ") " .
                      "   and order_item_order = " . $project["project_order"]  . 
				 " group by order_item_supplier_address, address_id, address_company";


*/
$sql_suppliers = "select DISTINCT order_item_supplier_address, address_id, address_company, address_shortcut, " .
                 " sum(order_item_quantity*order_item_system_price) as amount,  " .
				 " sum(order_item_quantity*order_item_client_price) as amount_loc " . 
				 "from order_items " . 
				 "left join addresses on order_item_supplier_address = address_id ".
				 " where (order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
                      "   and (order_item_type <= " . ITEM_TYPE_SPECIAL .
		              "   or order_item_type = " . ITEM_TYPE_SERVICES . ") " .
					  "   and order_item_quantity > 0 " . 
                      "   and order_item_order = " . $project["project_order"]  . 
				 " group by order_item_supplier_address, address_id, address_company";

$res = mysql_query($sql_suppliers) or dberror($sql_suppliers);

while ($row = mysql_fetch_assoc($res))
{
	
	$sql = "select supplier_client_invoice_id ". 
		   "from supplier_client_invoices " . 
		   "where supplier_client_invoice_supplier = " . dbquote($row["order_item_supplier_address"]) .
		   " and supplier_client_invoice_client = " . $client_address["id"]  . 
		   " and supplier_client_invoice_startdate <= " . dbquote(date("Y-m-d")) . 
			   " and supplier_client_invoice_enddate >= " . dbquote(date("Y-m-d"));

	$res_s = mysql_query($sql) or dberror($sql);
	if ($row_s = mysql_fetch_assoc($res_s))
	{
		$suppliers[$row["address_id"]] = $row["address_shortcut"]; 
		$directly_invoiced_amount = $directly_invoiced_amount + $row["amount"];
		$directly_invoiced_amount_loc = $directly_invoiced_amount + $row["amount_loc"];
	}
	else
	{
		$excluded_suppliers[] = $row["address_id"];
		$swatch[$row["address_id"]] = $row["address_shortcut"]; 
	}
}


//get budget data
//cost, budget situation from the list of materials
$line = 1;
$group_totals = array();
$list_total = 0;
$list_total_loc = 0;
$group_totals["HQ Supplied Items"] = 0;
$group_totals["HQ Freight Items"] = 0;
$hq_freight_charges = array();
$hq_freight_charges_loc = array();

$sql_c = "select order_item_cost_group, sum(order_item_quantity*order_item_system_price) as total, " .
         "sum(order_item_quantity*order_item_client_price) as total_loc " .
         "from order_items " . 
	     "where order_item_order = " .  $id .
		 " group by order_item_cost_group";

$res_c = mysql_query($sql_c) or dberror($sql_c);
while($row_c = mysql_fetch_assoc($res_c))
{
	if($row_c["order_item_cost_group"] == 2) // HQ Supplied
	{
		$group_totals["HQ Supplied Items"] = $group_totals["HQ Supplied Items"] + $row_c["total"];
		$list_total = $list_total + $row_c["total"];
		$list_total_loc = $list_total_loc - $row_c["total_loc"];
		
	}
	elseif($row_c["order_item_cost_group"] == 6) // Freight Charges
	{
		$group_totals["HQ Freight Items"] = $group_totals["HQ Freight Items"] + $row_c["total"];
		$list_total = $list_total + $row_c["total"];
		$list_total_loc = $list_total_loc - $row_c["total_loc"];

	}
}

//get list of freight charges
$sql_c = "select order_item_text, order_item_quantity*order_item_system_price as total, " .
         "order_item_quantity*order_item_client_price as total_loc " .
         "from order_items " . 
	     "where order_item_cost_group = 6 and order_item_order = " .  $id;

$res_c = mysql_query($sql_c) or dberror($sql_c);
while($row_c = mysql_fetch_assoc($res_c))
{
		$hq_freight_charges[$row_c["order_item_text"]] = $row_c["total"];
		$hq_freight_charges_loc[$row_c["order_item_text"]] = $row_c["total_loc"];
}


$local_production = 0;

$sql_c = "select order_item_cost_group, sum(order_item_quantity*order_item_system_price) as total, " .
         "sum(order_item_quantity*order_item_client_price) as total_loc " .
         "from order_items " . 
	     "where order_item_exclude_from_ps = 1 " . 
	     " and order_item_order = " .  $id .
		 " group by order_item_cost_group";


$res_c = mysql_query($sql_c) or dberror($sql_c);
while($row_c = mysql_fetch_assoc($res_c))
{
	$local_production = $row_c["total"];
	$local_production_loc = $row_c["total_loc"];

	if($row_c["order_item_cost_group"] == 2) // HQ Supplied
	{
		$group_totals["HQ Supplied Items"] = $group_totals["HQ Supplied Items"] - $local_production;
		$list_total = $list_total - $local_production;
		$list_total_loc = $list_total_loc - $local_production_loc;
	}
	elseif($row_c["order_item_cost_group"] == 6) // Freight Charges
	{
		$group_totals["Freight Charges"] = $group_totals["Freight Charges"] - $local_production;
		$list_total = $list_total - $local_production;
		$list_total_loc = $list_total_loc - $local_production_loc;
	}
	
}
if($directly_invoiced_amount > 0)
{
	$planned = ceil(($list_total - $directly_invoiced_amount)/1000) * 1000;
	$planned_loc = ceil(($list_total_loc - $directly_invoiced_amount_loc)/1000) * 1000;


}
else
{
	$planned = ceil($list_total/1000) * 1000;
	$planned_loc = ceil($list_total_loc/1000) * 1000;
}



//approval names
if($planned > 0)
{
	$sql_u = "update projects set project_planned_amount_current_year = " . $planned . 
			 "   where project_id = " . $project["project_id"];
	$result = mysql_query($sql_u) or dberror($sql_u);
}


$approval_names = array();
for($i = 1; $i <= 6; $i++)
{
	$approval_names["project_approval_person" . $i] = "";
}

$sql = "select budget_approval_user " .
	   "from budget_approvals " .
	   "where budget_approval_budget_low < ". intval($planned) . " " .
	   "order by budget_approval_priority asc";

$res = mysql_query($sql);
$approval_line = 1;
while($row = mysql_fetch_assoc($res))
{
	if (!in_array($row['budget_approval_user'], $approval_names))
	{
		while ($approval_line <= 6)
		{
			if (empty($approval_names["project_approval_person" . $approval_line]))
			{
				$approval_names["project_approval_person" . $approval_line] = $row['budget_approval_user'];
				
				break;
			}
			else
			{
				$approval_line++;
			}
		}
	}
}



$rto_set = false;
foreach($approval_names as $key=>$value)
{
	if($rto_set == false and !$value)
	{
		$rto_set = true;
		$approval_names[$key] = $project["operator2"];
	}
}

//update project record
$fields = array();

$value = $approval_names["project_approval_person1"];
$fields[] = "project_approval_person1 = " . dbquote($value);

$value = $approval_names["project_approval_person2"];
$fields[] = "project_approval_person2 = " . dbquote($value);

$value = $approval_names["project_approval_person3"];
$fields[] = "project_approval_person3 = " . dbquote($value);

$value = $approval_names["project_approval_person4"];
$fields[] = "project_approval_person4 = " . dbquote($value);

$value = $approval_names["project_approval_person5"];
$fields[] = "project_approval_person5 = " . dbquote($value);

$value = $approval_names["project_approval_person6"];
$fields[] = "project_approval_person6 = " . dbquote($value);

$value = $project_leader;
$fields[] = "project_approval_person9 = " . dbquote($project_leader);


$sql = "update projects set " . join(", ", $fields) . " where project_id = " . dbquote(param("pid"));
if($projcet["project_is_on_project_sheet_list"] != 1) //only update if not updated yet
{
	mysql_query($sql) or dberror($sql);
}


// Select project data

$sql = "select project_number, project_cost_center, project_account_number, ".       
       "    project_id, project_closing_date, project_ps_product_line, " .
       "    project_ps_spent_for, " .
       "    project_opening_date, " .
       "    project_closing_date_string, " .
       "    project_budget_total, project_budget_committed, project_budget_spent, " .
       "    project_budget_after_project, project_name, product_line_name, " .
       "    project_planned_amount_current_year, " .
       "    project_planned_amount_next_year, " .
       "    currency_symbol, currency_exchange_rate, currency_factor, " .
       "    project_share_company, project_share_other, " .
       "    project_approval_person1, project_approval_person2, ".
       "    project_approval_person3, project_approval_person4, ".
       "    project_approval_person5, project_approval_person6, " .
       "    project_approval_person7, project_approval_person8, " .
       "    project_approval_person9, business_unit_name, " .
       "    unix_timestamp(project_approval_date1) as project_approval_date1, " .
       "    unix_timestamp(project_approval_date2) as project_approval_date2, " .
       "    unix_timestamp(project_approval_date3) as project_approval_date3, " .
       "    unix_timestamp(project_approval_date4) as project_approval_date4, " .
       "    unix_timestamp(project_approval_date5) as project_approval_date5, " .
       "    unix_timestamp(project_approval_date6) as project_approval_date6, " .
       "    unix_timestamp(project_approval_date7) as project_approval_date7, " .
       "    unix_timestamp(project_approval_date8) as project_approval_date8, " .
       "    unix_timestamp(project_approval_date9) as project_approval_date9, " .
       "    project_approval_description, project_sheet_name, " .
       "    suppliers.address_company, suppliers.address_company2, " .
       "    suppliers.address_address, suppliers.address_address2, " .
       "    suppliers.address_zip, suppliers.address_place, " .
       "    country_name, user_firstname, user_name, user_phone, user_email, " .
	   "    project_costtype_text, projectkind_name, postype_name " .
       "from orders " .
	   "    left join projects on order_id = project_order " .
	   "    left join projectkinds on projectkind_id = project_projectkind " . 
       "    left join product_lines on project_product_line = product_line_id " .
       "    left join addresses on order_client_address = addresses.address_id  " .
       "    left join currencies on addresses.address_currency = currency_id " .
       "    left join addresses as suppliers on order_supplier_address = suppliers.address_id " .
       "    left join countries on suppliers.address_country = country_id " .
       "    left join users on suppliers.address_contact = user_id " .
       "    left join business_units on business_unit_id = project_business_unit " .
	   "    left join project_costs on project_cost_order = order_id " . 
	   "    left join project_costtypes on project_costtype_id = project_cost_type " .
	   "    left join postypes on postype_id = project_postype " . 
       "where order_id = $id";

$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);





define("COLUMN1_XPOS", 15);
define("COLUMN1_DATA_OFFSET", 25);
define("COLUMN1_DATA_WIDTH", 58);
define("COLUMN1_DATA2_OFFSET", 37);
define("COLUMN1_DATA2_WIDTH", 28);

define("COLUMN2_XPOS", 112);
define("COLUMN2_DATA_OFFSET", 25);
define("COLUMN2_DATA_WIDTH", 58);
define("COLUMN2_DATA2_WIDTH", 50);

define("TITLE_YPOS", 15);
define("GROUP1_YPOS", 30);
define("GROUP2_YPOS", 65);
define("GROUP3_YPOS", 105);
define("JUSTIFICATION_YPOS", 185);
define("JUSTIFICATION_COST_XPOS", 176);
define("SUPPLIER_YPOS", 248);


// Create and setup PDF document
$pdf = new TCPDF("P", "mm", "A4", true, 'UTF-8', false);
$pdf->SetMargins(10, 23, 13);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$pdf->Open();
$pdf->SetTitle($row['project_sheet_name']);
$pdf->SetAuthor("Tissot Retailnet");
$pdf->SetDisplayMode(150);

$pdf->AddFont('arialn');
$pdf->AddFont('arialn', 'B');

$pdf->AddPage();
$pdf->SetLeftMargin(0);


$pdf->Image('../pictures/brand_logo.jpg',16,8,33);

$pdf->SetFont('arialn','B',12);
$pdf->SetY(10);
$pdf->Cell(0,12, "Project Sheet ", 0, 0, 'R');


// Title
/*
$pdf->SetFont("arialn", "B", 18);
$pdf->SetXY(COLUMN1_XPOS, TITLE_YPOS);
$pdf->Cell(0, 0, strtoupper($row['project_sheet_name']));
*/

/////////// COLUMN 1

// Project number
$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS, GROUP1_YPOS);
$pdf->Cell(0, 6, "1. Project Number");

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS+COLUMN1_DATA_OFFSET, GROUP1_YPOS);
$pdf->Cell(COLUMN1_DATA_WIDTH, 6, $row["project_number"], 1, 0, "L");



// Project name
$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS, GROUP1_YPOS + 10);
$pdf->Cell(0, 6, "2. Project Name");

$pdf->SetFont("arialn", "", 7);
$text = substr($row["project_name"], 0, COLUMN1_DATA_WIDTH-1);

$pdf->SetXY(COLUMN1_XPOS+COLUMN1_DATA_OFFSET, GROUP1_YPOS + 10);
$pdf->Cell(COLUMN1_DATA_WIDTH, 6, $text, 1, 0, "L");


// Budget
$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS, GROUP1_YPOS + 20);
$pdf->Cell(0, 6, "3. Budget in CHF");

$pdf->SetFont("arialn", "", 7);
$number = number_format($planned, 2, ".", "'");

$pdf->SetXY(COLUMN1_XPOS+COLUMN1_DATA_OFFSET, GROUP1_YPOS + 20);
$pdf->Cell(COLUMN1_DATA_WIDTH, 6, $number, 1, 0, "L");


// Shares
$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS, GROUP1_YPOS + 30);
$pdf->Cell(0, 6, "4. Share HQ / Markets");

$pdf->SetFont("arialn", "", 7);
$number = number_format($planned, 2, ".", "'");

$pdf->SetXY(COLUMN1_XPOS+COLUMN1_DATA_OFFSET, GROUP1_YPOS + 30);
$pdf->Cell(COLUMN1_DATA_WIDTH/2-2, 6, number_format($row["project_share_company"], 0, ".", "'") . "%", 1, 0, "L");
$pdf->SetXY(COLUMN1_XPOS+COLUMN1_DATA_OFFSET + COLUMN1_DATA_WIDTH/2+2, GROUP1_YPOS + 30);
$pdf->Cell(COLUMN1_DATA_WIDTH/2-2, 6, number_format($row["project_share_other"], 0, ".", "'") . "%", 1, 0, "L");


$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS, GROUP1_YPOS + 40);
$pdf->Cell(0, 6, "7. Additional");

$pdf->SetFont("arialn", "", 7);
$number = number_format($planned, 2, ".", "'");

$pdf->SetXY(COLUMN1_XPOS+COLUMN1_DATA_OFFSET, GROUP1_YPOS + 40);
$pdf->Cell(COLUMN1_DATA_WIDTH/2-2, 6, "", 1, 0, "L");



/////////// COLUMN 2

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS, GROUP1_YPOS);
$pdf->Cell(0, 6, "Cost Center");

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP1_YPOS);
$pdf->Cell(COLUMN2_DATA_WIDTH, 6, $row["project_cost_center"], 1, 0, "L");


$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS, GROUP1_YPOS+10);
$pdf->Cell(0, 6, "5. Account Number");

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP1_YPOS+10);
$pdf->Cell(COLUMN2_DATA_WIDTH, 6, $row["project_account_number"], 1, 0, "L");


$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS, GROUP1_YPOS+20);
$pdf->Cell(0, 6, "6. BU Number");

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP1_YPOS+20);
$pdf->Cell(COLUMN2_DATA_WIDTH, 6, $row["business_unit_name"], 1, 0, "L");

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS, GROUP1_YPOS+30);
$pdf->Cell(0, 6, "Opening Date");

$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP1_YPOS+30);
$pdf->Cell(COLUMN2_DATA_WIDTH, 6, to_system_date($row["project_opening_date"]), 1, 0, "L");


///internal retail info


$pdf->SetFont("arialn", "", 7);
$pdf->SetXY(COLUMN2_XPOS, GROUP1_YPOS+38);
$pdf->Cell(0, 3, "Original to:");
$pdf->SetFont("arialn", "", 7);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP1_YPOS+38);
$pdf->Cell(COLUMN2_DATA_WIDTH, 3, "Finance Department", 0, 0, "L");


$pdf->SetFont("arialn", "", 7);
$pdf->SetXY(COLUMN2_XPOS, GROUP1_YPOS+41);
$pdf->Cell(0, 3, "CC to:");
$pdf->SetFont("arialn", "", 7);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP1_YPOS+41);
$pdf->Cell(COLUMN2_DATA_WIDTH, 3, "Logistic Coordinator, Retail Administrator", 0, 0, "L");


$pdf->SetFont("arialn", "", 7);
$pdf->SetXY(COLUMN2_XPOS, GROUP1_YPOS+48);
$pdf->Cell(0, 3, "Date / Signature:");
$pdf->SetFont("arialn", "", 7);
$pdf->SetXY(COLUMN2_XPOS + COLUMN2_DATA_OFFSET, GROUP1_YPOS+48);
$pdf->Cell(COLUMN2_DATA_WIDTH, 3, "...................... / .........................................................................", 0, 0, "L");




//project description rectangle
$yoffset = 52;
$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0));
$pdf->RoundedRect(COLUMN1_XPOS+2, GROUP1_YPOS+$yoffset+3, 177, 80, 2.50, '1111', 'DF', $style, array(255, 255, 255));

$pdf->SetFont("arialn", "B", 9);
$pdf->SetXY(COLUMN1_XPOS+4, GROUP1_YPOS+$yoffset);
$pdf->Cell(29, 6, "8. Project Description", 0, 0, "L", '#FFFFFF');

//-> project details
// Project legal type and project type
$yoffset = GROUP1_YPOS +$yoffset + 6;
$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS+4, $yoffset);
$pdf->Cell(0, 3.5, "Legal Type - Project Type - POS Type:");

$pdf->SetFont("arialn", "", 7);
$pdf->SetXY(COLUMN1_XPOS + 45, $yoffset);
$pdf->Cell(COLUMN1_DATA2_WIDTH, 3.5, $row["project_costtype_text"]. " - ". $row["projectkind_name"] . " - ". $row["postype_name"], 0, 0, "L");


//financial justifications
$yoffset = $yoffset + 6;

$sql = "select project_financial_justification_description, " .
       "    project_financial_justification_cost " .
       "from project_financial_justifications " .
       "where project_financial_justification_project = " . $row['project_id'] . " " .
       "order by project_financial_justification_priority asc";

$res_2 = mysql_query($sql);
$number_of_description_rows = (mysql_num_rows($res_2) > 10) ? mysql_num_rows($res_2) : 10 ;
$longer = false;


while ($row_2 = mysql_fetch_assoc($res_2))
{
    if (LINE_BREAK)
    {
		$text = $row_2['project_financial_justification_description'];
        while ($text)
        {
            if ($longer)
            {
                $yoffset +=3.5;
                $number_of_description_rows++;
            }
            $longer = true;
            $text_out = substr($text,0,60);
            $text = substr($text,60);

            $pdf->SetXY(COLUMN1_XPOS+4, $yoffset);
            $pdf->Cell(0, 6, 
                   $text_out, 0, "", "L");
        }
    }
    else
    {
        $text = limit($row_2['project_financial_justification_description'], (JUSTIFICATION_COST_XPOS - COLUMN1_XPOS));
        $pdf->SetXY(COLUMN1_XPOS+4, $yoffset);
        $pdf->Cell(0, 6, $text, 0, "", "L");
    }
    
    
    if (!LINE_BREAK)
    {
        $yoffset +=3.5;
    }
    
}

$pdf->SetXY(COLUMN1_XPOS+4, $yoffset);
$pdf->Cell(0, 6, "Project Leader: " . $project_manager["name"] . " " .  $project_manager["firstname"] , 0, "", "L");
$yoffset +=3.5;


$pdf->SetXY(COLUMN1_XPOS+4,$yoffset);
$pdf->Cell(0, 6, "Logistics Coordinator: " . $logistics_coordinator["name"] . " " .  $logistics_coordinator["firstname"] , 0, "", "L");





//other project details
$yoffset = $yoffset + 8;
$pdf->SetFont("arialn", "", 8);
$pdf->SetXY(COLUMN1_XPOS+4, $yoffset);
//$pdf->Cell(80,3, $row['business_unit_name']);
$pdf->Cell(80,3, "REINV");

$pdf->SetFont("arialn", "", 7);
$justification_cost_sum = 0;


//asort($suppliers);
//$directly_invoiced_amount
$yoffset = $yoffset + 6;
foreach($group_totals as $group_name=>$amount)
{
	if($group_name == "HQ Supplied Items" and $directly_invoiced_amount > 0)
	{
		$pdf->SetXY(COLUMN1_XPOS+4, $yoffset);
		$pdf->Cell(25, 3, $group_name, 0, "", "L");
		$pdf->Cell(30, 3, number_format($amount, 2, ".", "'"), 0, "", "R");
		$yoffset +=3.5;
		
		asort($suppliers);
		asort($swatch);
		$group_name = "./. direct invoicing:";
		$pdf->SetXY(COLUMN1_XPOS+4, $yoffset);
		$pdf->Cell(25, 3,$group_name , 0, "", "L");
		$pdf->Cell(30, 3, number_format($directly_invoiced_amount, 2, ".", "'"), 0, "", "R");
		$pdf->Cell(30, 3, " by " .  implode(', ', $suppliers), 0, "", "L");
		$yoffset +=3.5;

		$pdf->SetXY(COLUMN1_XPOS+4, $yoffset);
		$pdf->SetFont("arialn", "B", 7);
		$pdf->Cell(25, 3, "Total 1 (Tissot Invoice)", 0, "", "L");
		
		$pdf->Cell(30, 3, number_format($amount - $directly_invoiced_amount, 2, ".", "'"), 0, "", "R");
		$pdf->SetFont("arialn", "", 7);
		
		$yoffset +=3.5;

		$list_total = $list_total - $directly_invoiced_amount;
		$pdf->SetFont("arialn", "", 7);
	}
	elseif($group_name == "HQ Freight Items")
	{
		foreach($hq_freight_charges as $name=>$amount2)
		{
			$pdf->SetXY(COLUMN1_XPOS+4, $yoffset);
			$pdf->Cell(25, 3, substr($name, 0,32), 0, "", "L");
			$pdf->Cell(30, 3, number_format($amount2, 2, ".", "'"), 0, "", "R");
			$yoffset +=3.5;
		}

		$pdf->SetFont("arialn", "B", 7);
		$pdf->SetXY(COLUMN1_XPOS+4, $yoffset);
		$pdf->Cell(25, 3, "Total 2 (HQ Freight Items)", 0, "", "L");
		$pdf->Cell(30, 3, number_format($amount, 2, ".", "'"), 0, "", "R");
		$yoffset +=3.5;
		
	}
	else
	{
		$pdf->SetXY(COLUMN1_XPOS+4, $yoffset);
		$pdf->Cell(25, 3, $group_name, 0, "", "L");
		$pdf->Cell(30, 3, number_format($amount, 2, ".", "'"), 0, "", "R");
		$yoffset +=3.5;
	}

}


$pdf->SetFont("arialn", "B", 7);
$pdf->SetXY(COLUMN1_XPOS+4, $yoffset);
if($directly_invoiced_amount > 0)
{
	$pdf->Cell(25, 3, "Total 1+2", 0, "", "L");

	$list_total = round(($list_total+500)/1000, 0) * 1000;
	$pdf->Cell(30, 3, number_format($list_total, 2, ".", "'"), 0, "", "R");
}
else
{
	$pdf->Cell(25, 3, "Total 1+2", 0, "", "L");
	$pdf->Cell(30, 3, number_format($list_total, 2, ".", "'"), 0, "", "R");
}




//details supplier rectangle
$yoffset = 140;
$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0));
$pdf->RoundedRect(COLUMN1_XPOS+2, GROUP1_YPOS+$yoffset+2, 177, 25, 2.50, '1111', 'DF', $style, array(255, 255, 255));

$pdf->SetFont("arialn", "B", 9);
$pdf->SetXY(COLUMN1_XPOS+4, GROUP1_YPOS+$yoffset-1);
$pdf->Cell(24.5, 6, "9. Details Supplier", 0, 0, "L", '#FFFFFF');


$yoffset = GROUP1_YPOS+$yoffset+6;
$pdf->SetXY(COLUMN1_XPOS+4, $yoffset);
$pdf->SetFont("arialn", "", 7);
$pdf->Cell(35, 3, "Has the price been negotiated down?   No", 0, "", "L");
$yoffset +=3.5;
$pdf->SetXY(COLUMN1_XPOS+4, $yoffset);
$pdf->Cell(35, 3, "How many suppliers are involved?   " . $number_of_suppliers, 0, "", "L");
$yoffset +=3.5;
$pdf->SetXY(COLUMN1_XPOS+4, $yoffset);

$i = 1;
foreach($swatch as $address_id=>$supplier_name)
{
	if($i<4) {
		$pdf->Cell(35, 3, "Name Supplier " . $i . ":   " . $supplier_name, 0, "", "L");
		$yoffset +=3.5;
		$pdf->SetXY(COLUMN1_XPOS+4, $yoffset);
	}
	$i++;
}


/*
for ($i = 1; $i <= 6; $i++)
{
    $text = limit($row["project_approval_person" . $i], $width1);
	$text = $row["project_approval_person" . $i];
    if ($text)
    {

*/

//details approval
$yoffset = 173;
$style = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0));
$pdf->RoundedRect(COLUMN1_XPOS+2, GROUP1_YPOS+$yoffset+2, 177, 62, 2.50, '1111', 'DF', $style, array(255, 255, 255));

$pdf->SetFont("arialn", "B", 9);
$pdf->SetXY(COLUMN1_XPOS+4, GROUP1_YPOS+$yoffset-1);
$pdf->Cell(23.5, 6, "10. Approval Part", 0, 0, "L", '#FFFFFF');

//approval names
$pdf->SetFont("arialn", "B", 7);
$yoffset = GROUP1_YPOS+$yoffset+6;
$pdf->SetXY(COLUMN1_XPOS+6, $yoffset);
$pdf->Cell(50, 12, "", 1, 0, "L");

$pdf->SetXY(COLUMN1_XPOS+6+58, $yoffset);
$pdf->Cell(50, 12, "", 1, 0, "L");

$pdf->SetXY(COLUMN1_XPOS+6+116, $yoffset);
$pdf->Cell(50, 12, "", 1, 0, "L");


$persons = array();

for ($i = 1; $i <= 6; $i++)
{
    if($row["project_approval_person" . $i])
	{
		$persons[$i] = $row["project_approval_person" . $i];
	}
	else
	{
		$persons[$i] = '';
	}

}

$yoffset = $yoffset+12;
$pdf->SetXY(COLUMN1_XPOS+6, $yoffset);
$pdf->Cell(50, 6, $persons[1], 1, 0, "L");

$pdf->SetXY(COLUMN1_XPOS+6+58, $yoffset);
$pdf->Cell(50, 6, $persons[2], 1, 0, "L");

$pdf->SetXY(COLUMN1_XPOS+6+116, $yoffset);
$pdf->Cell(50, 6, $persons[3], 1, 0, "L");

$pdf->SetFont("arialn", "", 7);
$yoffset = $yoffset+6;
$pdf->SetXY(COLUMN1_XPOS+6, $yoffset);
$pdf->Cell(50, 6, "Date:", 1, 0, "L");

$pdf->SetXY(COLUMN1_XPOS+6+58, $yoffset);
$pdf->Cell(50, 6, "Date:", 1, 0, "L");

$pdf->SetXY(COLUMN1_XPOS+6+116, $yoffset);
$pdf->Cell(50, 6, "Date:", 1, 0, "L");

$pdf->SetFont("arialn", "B", 7);
$yoffset = $yoffset+12;
$pdf->SetXY(COLUMN1_XPOS+6, $yoffset);
$pdf->Cell(50, 12, "", 1, 0, "L");

$pdf->SetXY(COLUMN1_XPOS+6+58, $yoffset);
$pdf->Cell(50, 12, "", 1, 0, "L");

$pdf->SetXY(COLUMN1_XPOS+6+116, $yoffset);
$pdf->Cell(50, 12, "", 1, 0, "L");

$yoffset = $yoffset+12;
$pdf->SetXY(COLUMN1_XPOS+6, $yoffset);
$pdf->Cell(50, 6, $persons[4], 1, 0, "L");

$pdf->SetXY(COLUMN1_XPOS+6+58, $yoffset);
$pdf->Cell(50, 6, $persons[5], 1, 0, "L");

$pdf->SetXY(COLUMN1_XPOS+6+116, $yoffset);
$pdf->Cell(50, 6,$persons[6], 1, 0, "L");

$pdf->SetFont("arialn", "", 7);
$yoffset = $yoffset+6;
$pdf->SetXY(COLUMN1_XPOS+6, $yoffset);
$pdf->Cell(50, 6, "Date:", 1, 0, "L");

$pdf->SetXY(COLUMN1_XPOS+6+58, $yoffset);
$pdf->Cell(50, 6, "Date:", 1, 0, "L");

$pdf->SetXY(COLUMN1_XPOS+6+116, $yoffset);
$pdf->Cell(50, 6, "Date:", 1, 0, "L");


$pdf->Output("project_sheet_" . $row["project_number"]);

?>