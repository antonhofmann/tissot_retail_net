<?php
/********************************************************************

    project_offer_position

    Edit Local Constrction Cost Position

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-06
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-06
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_edit_local_constrction_work");


// read project and order details
$project = get_project(param("pid"));

//get offer currency
$sql = "select lwoffer_currency from lwoffers where lwoffer_id = " . param("lwoid");
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

$offer_currency = get_currency_symbol($row["lwoffer_currency"]);


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("lwofferpositions", "lwofferposition");

$form->add_section();
$form->add_hidden("pid", param("pid"));
$form->add_hidden("lwoid", param("lwoid"));

$form->add_list("lwofferposition_lwoffergroup", "Cost Group",
    "select lwoffergroup_id, concat(lwoffergroup_code, ' ' , lwoffergroup_text) " .
    "from lwoffergroups " .
    "where lwoffergroup_offer = " . param("lwoid") . 
    " order by lwoffergroup_group, lwoffergroup_code");

$form->add_edit("lwofferposition_code", "Code", NOTNULL);
$form->add_edit("lwofferposition_text", "Description", NOTNULL);
$form->add_edit("lwofferposition_action", "Action");
$form->add_edit("lwofferposition_unit", "Unit");

$form->add_edit("lwofferposition_numofunits", "Number of Units", 0, "", TYPE_DECIMAL, 12,2);
$form->add_edit("lwofferposition_price", "Price in " . $offer_currency, 0, "", TYPE_DECIMAL, 12,2);

$form->add_edit("lwofferposition_numofunits_billed", "Number of Billed Units", 0, "", TYPE_DECIMAL, 12,2);
$form->add_edit("lwofferposition_price_billed", "Billed Price in " . $offer_currency, 0, "", TYPE_DECIMAL, 12,2);

$form->add_checkbox("lwofferposition_hasoffer", "Has to be offered");

$form->add_checkbox("lwofferposition_inbudget", "Is in Budget");

$form->add_button(FORM_BUTTON_SAVE, "Save");

if(id())
{
    $form->add_button("delete", "Delete");
}

$form->add_button("back", "Back");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$form->populate();
$form->process();


if ($form->button("back"))
{
    $link = "project_offer.php?id=" . param("lwoid") . "&pid=" . param("pid");
    redirect ($link);
}
elseif ($form->button("delete"))
{
    $sql = "delete from lwofferpositions where lwofferposition_id = " . id();
    $res = mysql_query($sql) or dberror($sql);
    
    $link = "project_offer.php?id=" . param("lwoid") . "&pid=" . param("pid");
    redirect ($link);
}


/********************************************************************
    Populate list and process button clicks
*********************************************************************/

$page = new Page("projects");
require "include/project_page_actions.php";
$page->header();
$page->title("Edit Local Construction Group");
$form->render();
$page->footer();

?>