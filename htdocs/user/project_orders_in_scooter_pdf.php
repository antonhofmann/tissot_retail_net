<?php
/********************************************************************

    project_orders_in_scooter_pdf.php

    print projects's ordered values from scooter

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2018-03-21
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2018-03-21
    Version:        1.0.0

    Copyright (c) 2018, Tissot, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "../shared/func_posindex.php";
check_access("can_view_ordered_values_in_projects");


/********************************************************************
    prepare all data needed
*********************************************************************/

// get user_data
$user_data = get_user(user_id());


$system_currency = get_system_currency_fields();

$project = get_project(param("pid"));

if($project["pipeline"] == 0)
{
	$pos_data = get_poslocation($project["posaddress_id"], "posaddresses");
}
elseif($project["pipeline"] == 1)
{
	$pos_data = get_poslocation_from_pipeline($project["posaddress_id"], $project["order_id"]);
}


if (param("pid"))
{
		
	global $page_title;
	$page_title = "Orders to be placed in Scooter for Project: " . $project["project_number"];

	
	//POS Basic Data
	$posname = $project["order_shop_address_company"];

	if($project["order_shop_address_company2"])
    {
		$posname .= ", " . $project["order_shop_address_company2"];
	}

	$posaddress = $project["order_shop_address_address"];
	
	if($project["order_shop_address_address2"])
    {
		$posaddress .= ", " . $project["order_shop_address_address2"];
	}
	if($project["order_shop_address_zip"])
    {
		$posaddress .= ", " . $project["order_shop_address_zip"] . " " . $project["order_shop_address_place"];
	}
	else
	{
		$posaddress .= ", " . $project["order_shop_address_place"];
	}
	if($project["order_shop_address_country_name"])
    {
		$posaddress .= ", " . $project["order_shop_address_country_name"];
	}
	
	//Client Data
	$posclient = "";
	$sql_i = "select * from addresses " . 
		     "left join countries on country_id = address_country " .
	         "where address_id = " . $project["order_client_address"];

	$res_i = mysql_query($sql_i) or dberror($sql_i);
	if ($row_i = mysql_fetch_assoc($res_i))
	{
		$posclient = $row_i["address_company"];
		if($row_i["address_address"])
		{
			$posclient .= ", " . $row_i["address_address"];
		}
		if($row_i["address_zip"])
		{
			$posclient .= ", " . $row_i["address_zip"] . " " . $row_i["address_place"];
		}
		else
		{
			$posclient .= ", " . $row_i["address_place"];
		}
		if($row_i["country_name"])
		{
			$posclient .= ", " . $row_i["country_name"];
		}
	}



	// read project and order details
	$project = get_project(param("pid"));
	$order_currency2 = get_order_currency($project["project_order"]);

	// get company's address
	$client_address = get_address($project["order_client_address"]);

	$sql_order_items = "select item_code, costsheet_text, costsheet_budget_amount, 
					currency_symbol, address_company, costsheet_orderdate, costsheet_reinvoice_info
                    from costsheets
                    left join currencies on currency_id = costsheet_currency_id
					left join items on costsheet_item_id = item_id 
					left join suppliers on supplier_item = item_id
					left join addresses on address_id = supplier_address"; 

$list_filter = "costsheet_version = 0 
				and costsheet_item_id > 0 
				and item_stock_property_of_swatch = 1
				and costsheet_project_id = " . $project["project_id"];

	

	/********************************************************************
		prepare pdf
	*********************************************************************/

	require_once('../include/tcpdf/config/lang/eng.php');
	require_once('../include/tcpdf/tcpdf.php');


	class MYPDF extends TCPDF
	{
		//Page header
		function Header()
		{
			global $page_title;
			//Logo
			$this->Image('../pictures/brand_logo.jpg',10,8,33);
			//arialn bold 15
			$this->SetFont('arialn','B',12);
			//Move to the right
			$this->Cell(80);
			//Title
			$this->Cell(0,33,$page_title,0,0,'R');
			//Line break
			$this->Ln(20);

		}

		//Page footer
		function Footer()
		{
			//Position at 1.5 cm from bottom
			$this->SetY(-15);
			//arialn italic 8
			$this->SetFont('arialn','I',8);
			//Page number
			$this->Cell(0,10, to_system_date(date("d.m.y")) . ' / Page '.$this->PageNo().'/'.$this->getAliasNbPages(),0,0,'R');
		}

	}

	//Instanciation of inherited class
	$pdf = new MYPDF("L", "mm", "A4", true, 'UTF-8', false);
	$pdf->SetMargins(10, 23, 10);

	$pdf->Open();

	$pdf->SetFillColor(220, 220, 220); 
	$pdf->AddFont('arialn');
	$pdf->AddFont('arialn', 'B');

	$pdf->AddPage();
	$new_page = 0;

	$pdf->SetFont('arialn','B',10);
	$pdf->Cell(277,7,$posname,1, 0, 'L', 1);
	$pdf->Ln();
	$pdf->SetFont('arialn','',10);
	$pdf->Cell(277,7,$posaddress,1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();
		
	
	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(27, 5,"Client",1, 0, 'L', 0);
	$pdf->SetFont('arialn','',8);
	$pdf->Cell(250, 5,$posclient,1, 0, 'L', 0);
	$pdf->Ln();
	$pdf->Ln();


	$pdf->SetFont('arialn','B',8);
	$pdf->Cell(40, 5,"Supplier",1, 0, 'L', 0);
	$pdf->Cell(20, 5,"Item Code",1, 0, 'L', 0);
	$pdf->Cell(100, 5,"Item Name",1, 0, 'L', 0);
	$pdf->Cell(25, 5,"Amount",1, 0, 'R', 0);
	$pdf->Cell(12, 5,"Ordered",1, 0, 'R', 0);
	$pdf->Cell(80, 5,"Reinvoice Info",1, 0, 'L', 0);
	
	$pdf->SetFont('arialn','',8);

	$sql = $sql_order_items . " where " . $list_filter . " order by address_company, item_code";
	$res = mysql_query($sql) or dberror($sql);
	while ($row = mysql_fetch_assoc($res))
	{

		$pdf->Ln();
		
		$pdf->Cell(40, 5,$row["address_company"],1, 0, 'L', 0);
		$pdf->Cell(20, 5,$row["item_code"],1, 0, 'L', 0);
		$pdf->Cell(100, 5,$row["costsheet_text"],1, 0, 'L', 0);
		$pdf->Cell(25, 5,$row["costsheet_budget_amount"] . " " . $row["currency_symbol"],1, 0, 'R', 0);
		$pdf->Cell(12, 5,to_system_date($row["costsheet_orderdate"]),1, 0, 'R', 0);
		$pdf->Cell(80, 5,$row["costsheet_reinvoice_info"],1, 0, 'L', 0);
	}
	// write pdf
	$file_name = "orders_in_scooter_" . $project["project_number"] . ".pdf";
	$pdf->Output($file_name);

}

?>