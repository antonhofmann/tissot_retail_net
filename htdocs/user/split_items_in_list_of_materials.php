<?php
/********************************************************************

    split_items_in_list_of_materials.php

    Enter Filter Criteria for the Query

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2011-07-30
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2011-07-30
    Version:        1.0.0

    Copyright (c) 2011, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "../include/page_modal.php";

check_access("has_access_to_list_of_materials_in_projects");




/********************************************************************
    prepare all data needed
*********************************************************************/

$sql = "select order_item_id, order_item_text, order_item_quantity, addresses.address_shortcut as supplier_company ".
	   "from order_items ".
	   "left join items on order_item_item = item_id ".
	   "left join addresses on order_item_supplier_address = addresses.address_id ".
	   "where order_item_id = " . dbquote(param("order_item_id"));


$res = mysql_query($sql) or dberror($sql);
if ($row = mysql_fetch_assoc($res))
{
	$order_item = $row;
}



/********************************************************************
    do the splitting
*********************************************************************/
if(param("save_form") == 1) {
	
	$splitted_quantities = array();

	for ($i = 1; $i <= $order_item["order_item_quantity"]; $i++) {
		if($_POST["q". $i] > 0) {
			$splitted_quantities[] = $_POST["q". $i];
		}
	}

	if(count($splitted_quantities) > 0) {

		$order_item_info = array();
		$order_item_delivery_info = array();
		$original_order_item_id = 0;
		//get order_item info
		$sql = "select * from order_items where order_item_id = " . dbquote(param("order_item_id"));
		$res = mysql_query($sql) or dberror($sql);
		if ($order_item_info = mysql_fetch_assoc($res))
		{
			$original_order_item_id = $order_item_info["order_item_id"];
			unset($order_item_info["order_item_id"]);
			unset($order_item_info["user_modified"]);
			unset($order_item_info["date_modified"]);
			//get delivery info
			$sql = "select * from order_addresses where order_address_order_item = " . dbquote(param("order_item_id"));
			$res = mysql_query($sql) or dberror($sql);
			if ($order_item_delivery_info = mysql_fetch_assoc($res))
			{

				unset($order_item_delivery_info["order_address_id"]);
				unset($order_item_delivery_info["user_modified"]);
				unset($order_item_delivery_info["date_modified"]);
			}
		}


		if(count($order_item_info) > 0) {
			foreach($splitted_quantities as $key=>$quantity)
			{
				$order_item_info["order_item_quantity"] = $quantity;
				$order_item_info["user_created"] = user_login();
				$order_item_info["date_created"] =date("Y-m-d H:i:s");

				$fields = array();
				$values = array();

				foreach($order_item_info as $field=>$value) {
					$fields[] = $field;
					$values[] = dbquote($value);
				}
				$sql = "insert into order_items (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);

				$new_id = mysql_insert_id();
				$order_item_delivery_info['order_address_order_item'] = $new_id;


				$fields = array();
				$values = array();

				foreach($order_item_delivery_info as $field=>$value) {
					$fields[] = $field;
					$values[] = dbquote($value);
				}
				$sql = "insert into order_addresses (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
				mysql_query($sql) or dberror($sql);


			}

			$sql = "delete from order_items where order_item_id = " . dbquote($original_order_item_id);
			mysql_query($sql) or dberror($sql);

			$sql = "delete from order_addresses where order_address_order_item = " . dbquote($original_order_item_id);
			mysql_query($sql) or dberror($sql);
		}
	}

	
}

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");
$form->add_hidden("pid", param("pid"));
$form->add_hidden("order_item_id", param("order_item_id"));
$form->add_hidden("save_form", 1);

$form->add_section("Item Information");
$form->add_label("item_text", "Item", 0, $order_item["order_item_text"]);
$form->add_label("item_quantity", "Original Quantity", 0, $order_item["order_item_quantity"]);
$form->add_label("supplier", "Original Supplier", 0, $order_item["supplier_company"]);

$form->add_section("Splitting Information");
for ($i = 1; $i <= $order_item["order_item_quantity"]; $i++) {
    $form->add_edit("q" . $i, "Quantity", 0, "" , TYPE_DECIMAL, 10, 2);
}

$form->add_input_submit("submit", "Save", 0);


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
    Process buttons
*********************************************************************/ 

/********************************************************************
     Render Page
 *********************************************************************/ 
$page = new Page_Modal("cer_benchmarks");

$page->header();
$page->title("Split Quantities");

$form->render();

if(param("save_form") != '')
{
?>

<script languege="javascript">
	var back_link = "/user/project_edit_material_list.php?pid=<?php echo param("pid");?>"; 
	$.nyroModalRemove();

</script>

<?php
}

?>

</script>


<?php
$page->footer();
