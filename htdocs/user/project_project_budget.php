<?php
/********************************************************************

    project_project_budget.php

    Viwe AND Edit Project's Budget Sheet

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2015-08-20
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2015-08-20
    Version:        1.0.1

    Copyright (c) 2012, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

require_once "../shared/func_eventmails.php";

check_access("can_view_budget_in_projects");

register_param("pid");
set_referer("project_edit_budget_position.php");
set_referer("project_add_budget_position.php");

/********************************************************************
    prepare all data needed
*********************************************************************/


$user_roles = get_user_roles(user_id());


// read project and order details
$project = get_project(param("pid"));

//$project["order_budget_freezed_date"] = "0000-00-00";
// get company's address
$client_address = get_address($project["order_client_address"]);


// get system currency
$system_currency = get_system_currency_fields();

// get orders's currency
$order_currency = get_order_currency($project["project_order"]);


$currency_list = array();
$sql_currency = "select DISTINCT currency_id, currency_symbol " . 
                " from currencies " .
				" left join countries on country_currency = currency_id " . 
				" where currency_id = 1 " . 
				" or currency_id = " . $project["order_client_currency"] . 
				" or country_id in (" . $client_address["country"] . ", " . $project["order_shop_address_country"] . ") " . 
				" order by currency_symbol";

$res = mysql_query($sql_currency) or dberror($sql_currency);
while ($row = mysql_fetch_assoc($res))
{
	$currency_list[$row["currency_id"]] = get_currency($row["currency_id"]);
}

// build line numbers for budget positions

//check if CER was approved
$change_of_budget_possible = true;
$milestone1 = get_project_milestone($project["project_id"], 12);
$milestone2 = get_project_milestone($project["project_id"], 21);
	
if(count($milestone1) > 0 and ($milestone1['project_milestone_date'] != NULL and $milestone1['project_milestone_date'] != '0000-00-00')) // AF/CER was approved
{
	$change_of_budget_possible = false;
}
if(count($milestone2) > 0 and ($milestone2['project_milestone_date'] != NULL and $milestone2['project_milestone_date'] != '0000-00-00')) //  AF/CER was approvedt
{
	$change_of_budget_possible = false;
}

if($project["project_cost_type"] != 1)
{
	$change_of_budget_possible = true; // allow for all except corporate projects
}

// build group_totals of standard item groups

// create sql for order items
if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
{
	$sql_order_items = "select order_item_id, order_item_text, order_item_quantity, ".
				   "    order_item_po_number, item_id, " . 
	               "    order_item_system_price, order_item_client_price,".
				   "    TRUNCATE(order_item_quantity * order_item_system_price, 2) as total_price, ".
				   "    TRUNCATE(order_item_quantity * order_item_client_price, 2) as total_price_loc, ".
				   "    if(item_code <> '', item_code, item_type_name) as item_shortcut, ".
				   "    address_shortcut, item_type_id, " . 
				   "    if(address_company <> '', address_shortcut, order_item_supplier_freetext) as supplier_info, ".
				   "    item_type_priority, order_item_type, ".
				   
		           "    concat(project_cost_groupname_name, ' - ', item_category_name) as cat_name " .
				   "from order_items ".
				   "left join items on order_item_item = item_id ".
				   "left join item_categories on item_category_id = item_category " .
				   "left join addresses on order_item_supplier_address = address_id ".
				   "left join item_types on order_item_type = item_type_id " .
		           "left join project_cost_groupnames on project_cost_groupname_id = order_item_cost_group ";

	 $list1_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) " . 
		            " and order_item_order = " . $project["project_order"] .  
		            " and item_cost_group = 2 and order_item_type = 1";


	$list1a_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) " . 
		            " and order_item_order = " . $project["project_order"] .  
		            " and order_item_cost_group = 2 and order_item_type = 2";

	$list1b_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) " . 
		            " and order_item_order = " . $project["project_order"] .  
		            " and order_item_cost_group = 6";

	
	$list1c_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) " . 
		            " and order_item_order = " . $project["project_order"] .  
		            " and (order_item_cost_group = 10 or (order_item_type = 3 and order_item_cost_group = 2))";


	
    
    $list2_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) and order_item_order = " . $project["project_order"] .  " and order_item_cost_group = 9";

    $list3_filter = "order_item_order = " . $project["project_order"] .  " and order_item_cost_group = 11";

	$list7_filter = "order_item_order = " . $project["project_order"] .  " and order_item_cost_group = 8";

    $list6_filter = "order_item_order = " . $project["project_order"] .  " and order_item_cost_group = 7";


}
else
{

	$sql_order_items = "select order_item_id, order_item_text, order_item_quantity_freezed, ".
				   "    order_item_po_number, item_id, " . 
		           "    order_item_system_price_freezed, order_item_client_price_freezed, ".
				   "    TRUNCATE(order_item_quantity_freezed * order_item_client_price_freezed, 2) as total_price_loc, ".
		            "    TRUNCATE(order_item_quantity_freezed * order_item_system_price_freezed, 2) as total_price, ".
				   "    order_item_client_price_freezed,  ". 
				   "    if(address_company <> '', address_shortcut, order_item_supplier_freetext) as supplier_info, ".
				   "    if(item_code <> '', item_code, item_type_name) as item_shortcut, ".
				   "    address_shortcut, item_type_id, ".
				   "    item_type_priority, order_item_type, ".
				   "    concat(project_cost_groupname_name, ' - ', item_category_name) as cat_name " .
				   "from order_items ".
				   "left join items on order_item_item = item_id ".
				   "left join item_categories on item_category_id = item_category " .
				   "left join addresses on order_item_supplier_address = address_id ".
				   "left join item_types on order_item_type = item_type_id " .
		           "left join project_cost_groupnames on project_cost_groupname_id = order_item_cost_group ";
	    
    $list1_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) " .
		            " and order_item_order = " . $project["project_order"] .  
		            " and item_cost_group = 2 and order_item_type = 1 " . 
		            " and order_item_in_freezed_budget = 1";


	$list1a_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) " .
		            " and order_item_order = " . $project["project_order"] .  
		            " and order_item_cost_group = 2 and order_item_type = 2 " . 
		            " and order_item_in_freezed_budget = 1";

	$list1b_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) " .
		            " and order_item_order = " . $project["project_order"] .  
		            " and order_item_cost_group = 6 " . 
		            " and order_item_in_freezed_budget = 1";


	$list1c_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) " .
		            " and order_item_order = " . $project["project_order"] .  
		            " and (order_item_cost_group = 10 or (order_item_type = 3 and order_item_cost_group = 2))";

    $list2_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) and order_item_order = " . $project["project_order"] .  " and order_item_cost_group = 9 and order_item_in_freezed_budget = 1";

    $list3_filter = "order_item_order = " . $project["project_order"] .  " and order_item_cost_group = 11 and order_item_in_freezed_budget = 1";

	$list7_filter = "order_item_order = " . $project["project_order"] .  " and order_item_cost_group = 8 and order_item_in_freezed_budget = 1";

    $list6_filter = "order_item_order = " . $project["project_order"] .  " and order_item_cost_group = 7 and order_item_in_freezed_budget = 1";

}

//calculate budget group totals
$grouptotals_investment = array();
$grouptotals_investment[2] = 0;
$grouptotals_investment[6] = 0;
$grouptotals_investment[7] = 0;
$grouptotals_investment[8] = 0;
$grouptotals_investment[9] = 0;
$grouptotals_investment[10] = 0;
$grouptotals_investment[11] = 0;

$grouptotals_hq_investment = array();
$grouptotals_hq_investment[2] = 0;
$grouptotals_hq_investment[6] = 0;


$grouptotals_investment_loc = array();
$grouptotals_investment_loc[2] = 0;
$grouptotals_investment_loc[6] = 0;
$grouptotals_investment_loc[7] = 0;
$grouptotals_investment_loc[8] = 0;
$grouptotals_investment_loc[9] = 0;
$grouptotals_investment_loc[10] = 0;
$grouptotals_investment_loc[11] = 0;

$grouptotals_hq_investment_loc = array();
$grouptotals_hq_investment_loc[2] = 0;
$grouptotals_hq_investment_loc[6] = 0;


//list total for hq supplied items
$total_hq_supplied_standard_items = 0;
$total_hq_supplied_special_items = 0;
$total_hq_supplied_other = 0;

$total_hq_supplied_standard_items_loc = 0;
$total_hq_supplied_special_items_loc = 0;
$total_hq_supplied_other_loc = 0;


$sql = "select order_item_cost_group, order_item_type, ".
	   "order_item_quantity, order_item_quantity_freezed, order_item_quantity_hq_freezed, " . 
	   "order_item_system_price, order_item_system_price_freezed, order_item_system_price_hq_freezed, " .
	   "order_item_client_price, order_item_client_price_freezed, order_item_client_price_hq_freezed " .
	   "from order_items ".
	   "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
	   "   and order_item_order=" . $project["project_order"] . 
	   "   and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION .
	   "   or order_item_type = " . ITEM_TYPE_SERVICES .
	   "   or order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST .
	   " ) " .
	   " and order_item_cost_group > 0 " . 
	   " order by order_item_cost_group";


$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{

	if(($project["order_budget_freezed_date"] != NULL AND $project["order_budget_freezed_date"] != "0000-00-00")
		and ($row["order_item_cost_group"] == 2 or $row["order_item_cost_group"] == 6 or $row["order_item_cost_group"] == 10)) // full budget was approved

	{
		if($row["order_item_quantity_freezed"] > 0 or $row["order_item_type"] == 1 or $row["order_item_type"] == 2)
		{
			$grouptotals_hq_investment[$row["order_item_cost_group"]] = $grouptotals_hq_investment[$row["order_item_cost_group"]] + $row["order_item_system_price_freezed"] * $row["order_item_quantity_freezed"];

			$grouptotals_hq_investment_loc[$row["order_item_cost_group"]] = $grouptotals_hq_investment_loc[$row["order_item_cost_group"]] + $row["order_item_client_price_freezed"] * $row["order_item_quantity_freezed"];

			if($row["order_item_type"] == 1)
			{
				$total_hq_supplied_standard_items = $total_hq_supplied_standard_items + ($row["order_item_system_price_freezed"] * $row["order_item_quantity_freezed"]);

				$total_hq_supplied_standard_items_loc = $total_hq_supplied_standard_items_loc + ($row["order_item_client_price_freezed"] * $row["order_item_quantity_freezed"]);
			}
			elseif($row["order_item_type"] == 2 and $row["order_item_cost_group"] != 10)
			{
				$total_hq_supplied_special_items = $total_hq_supplied_special_items + ($row["order_item_system_price_freezed"] * $row["order_item_quantity_freezed"]);

				$total_hq_supplied_special_items_loc = $total_hq_supplied_special_items_loc + ($row["order_item_client_price_freezed"] * $row["order_item_quantity_freezed"]);
			}
			elseif($row["order_item_cost_group"] == 10 or ($row["order_item_type"] == 3 and $row["order_item_cost_group"] == 2))
			{
				$total_hq_supplied_other = $total_hq_supplied_other + ($row["order_item_system_price_freezed"] * $row["order_item_quantity_freezed"]);

				$total_hq_supplied_other_loc = $total_hq_supplied_other_loc + ($row["order_item_client_price_freezed"] * $row["order_item_quantity_freezed"]);
			}
		}
		else
		{
			$grouptotals_hq_investment[$row["order_item_cost_group"]] = $grouptotals_hq_investment[$row["order_item_cost_group"]] + $row["order_item_system_price_freezed"];

			$grouptotals_hq_investment_loc[$row["order_item_cost_group"]] = $grouptotals_hq_investment_loc[$row["order_item_cost_group"]] + $row["order_item_client_price_freezed"];


			if($row["order_item_type"] == 1)
			{
				/*
				$total_hq_supplied_standard_items = $total_hq_supplied_standard_items + $row["order_item_system_price_freezed"];

				$total_hq_supplied_standard_items_loc = $total_hq_supplied_standard_items_loc + $row["order_item_client_price_freezed"];
				*/

				$total_hq_supplied_standard_items = $total_hq_supplied_standard_items + ($row["order_item_system_price_freezed"] * $row["order_item_quantity_freezed"]);

				$total_hq_supplied_standard_items_loc = $total_hq_supplied_standard_items_loc + ($row["order_item_client_price_freezed"] * $row["order_item_quantity_freezed"]);
			}
			elseif($row["order_item_type"] == 2 and $row["order_item_cost_group"] != 10)
			{
				/*
				$total_hq_supplied_special_items = $total_hq_supplied_special_items + $row["order_item_system_price_freezed"];

				$total_hq_supplied_special_items_loc = $total_hq_supplied_special_items_loc + $row["order_item_client_price_freezed"];
				*/

				$total_hq_supplied_special_items = $total_hq_supplied_special_items + ($row["order_item_system_price_freezed"] * $row["order_item_quantity_freezed"]);

				$total_hq_supplied_special_items_loc = $total_hq_supplied_special_items_loc + ($row["order_item_client_price_freezed"] * $row["order_item_quantity_freezed"]);
			}
			elseif($row["order_item_cost_group"] == 10 or ($row["order_item_type"] == 3 and $row["order_item_cost_group"] == 2))
			{
				/*
				$total_hq_supplied_other = $total_hq_supplied_other + $row["order_item_system_price_freezed"];

				$total_hq_supplied_other_loc = $total_hq_supplied_other_loc + $row["order_item_client_price_freezed"];
				*/

				$total_hq_supplied_other = $total_hq_supplied_other + ($row["order_item_system_price_freezed"] * $row["order_item_quantity_freezed"]);

				$total_hq_supplied_other_loc = $total_hq_supplied_other_loc + ($row["order_item_client_price_freezed"] * $row["order_item_quantity_freezed"]);


			}

			
		}
	}
	elseif(($project["order_hq_budget_freezed_date"] == NULL OR $project["order_hq_budget_freezed_date"] == "0000-00-00")
		and ($row["order_item_cost_group"] == 2 or $row["order_item_cost_group"] == 6 or $row["order_item_cost_group"] == 10))
	{
		if($row["order_item_quantity"] > 0 or $row["order_item_type"] == 1  or $row["order_item_type"] == 2)
		{
			$grouptotals_hq_investment[$row["order_item_cost_group"]] = $grouptotals_hq_investment[$row["order_item_cost_group"]] + $row["order_item_system_price"] * $row["order_item_quantity"];

			$grouptotals_hq_investment_loc[$row["order_item_cost_group"]] = $grouptotals_hq_investment_loc[$row["order_item_cost_group"]] + $row["order_item_client_price"] * $row["order_item_quantity"];

			if($row["order_item_type"] == 1)
			{
				$total_hq_supplied_standard_items = $total_hq_supplied_standard_items + ($row["order_item_system_price"] * $row["order_item_quantity"]);

				$total_hq_supplied_standard_items_loc = $total_hq_supplied_standard_items_loc + ($row["order_item_client_price"] * $row["order_item_quantity"]);
			}
			elseif($row["order_item_type"] == 2 and $row["order_item_cost_group"] != 10)
			{
				/*
				$total_hq_supplied_special_items = $total_hq_supplied_special_items + ($row["order_item_system_price"] * $row["order_item_quantity"]);

				$total_hq_supplied_special_items_loc = $total_hq_supplied_special_items_loc + ($row["order_item_client_price"] * $row["order_item_quantity"]);
				*/

				$total_hq_supplied_special_items = $total_hq_supplied_special_items + ($row["order_item_system_price"] * $row["order_item_quantity"]);

				$total_hq_supplied_special_items_loc = $total_hq_supplied_special_items_loc + ($row["order_item_client_price"] * $row["order_item_quantity"]);
			}
			elseif($row["order_item_cost_group"] == 10 or ($row["order_item_type"] == 3 and $row["order_item_cost_group"] == 2))
			{
				$total_hq_supplied_other = $total_hq_supplied_other + ($row["order_item_system_price"] * $row["order_item_quantity"]);

				$total_hq_supplied_other_loc = $total_hq_supplied_other_loc + ($row["order_item_client_price"] * $row["order_item_quantity"]);
			}
		}
		else
		{
			$grouptotals_hq_investment[$row["order_item_cost_group"]] = $grouptotals_hq_investment[$row["order_item_cost_group"]] + $row["order_item_system_price"];

			$grouptotals_hq_investment_loc[$row["order_item_cost_group"]] = $grouptotals_hq_investment_loc[$row["order_item_cost_group"]] + $row["order_item_client_price"];

			if($row["order_item_type"] == 1)
			{
				/*
				$total_hq_supplied_standard_items = $total_hq_supplied_standard_items + $row["order_item_system_price"];

				$total_hq_supplied_standard_items_loc = $total_hq_supplied_standard_items_loc + $row["order_item_client_price"];
				*/

				$total_hq_supplied_standard_items = $total_hq_supplied_standard_items + ($row["order_item_system_price"] * $row["order_item_quantity"]);

				$total_hq_supplied_standard_items_loc = $total_hq_supplied_standard_items_loc + ($row["order_item_client_price"] * $row["order_item_quantity"]);
			}
			elseif($row["order_item_type"] == 2 and $row["order_item_cost_group"] != 10)
			{
				/*
				$total_hq_supplied_special_items = $total_hq_supplied_special_items + $row["order_item_system_price"];

				$total_hq_supplied_special_items_loc = $total_hq_supplied_special_items_loc + $row["order_item_client_price"];
				*/

				$total_hq_supplied_special_items = $total_hq_supplied_special_items + ($row["order_item_system_price"] * $row["order_item_quantity"]);

				$total_hq_supplied_special_items_loc = $total_hq_supplied_special_items_loc + ($row["order_item_client_price"] * $row["order_item_quantity"]);
			}
			elseif($row["order_item_cost_group"] == 10 or ($row["order_item_type"] == 3 and $row["order_item_cost_group"] == 2))
			{
				/*
				$total_hq_supplied_other = $total_hq_supplied_other + $row["order_item_system_price"];

				$total_hq_supplied_other_loc = $total_hq_supplied_other_loc + $row["order_item_client_price"];
				*/

				$total_hq_supplied_other = $total_hq_supplied_other + ($row["order_item_system_price"] * $row["order_item_quantity"]);

				$total_hq_supplied_other_loc = $total_hq_supplied_other_loc + ($row["order_item_client_price"] * $row["order_item_quantity"]);
			}
		}
	}
	elseif($row["order_item_cost_group"] == 2 
		or $row["order_item_cost_group"] == 6 
		or $row["order_item_cost_group"] == 10)
	{
		
		if($row["order_item_quantity_hq_freezed"] > 0 and($row["order_item_type"] == 1 or $row["order_item_type"] == 2))
		{
			$grouptotals_hq_investment[$row["order_item_cost_group"]] = $grouptotals_hq_investment[$row["order_item_cost_group"]] + $row["order_item_system_price_hq_freezed"] * $row["order_item_quantity_hq_freezed"];

			$grouptotals_hq_investment_loc[$row["order_item_cost_group"]] = $grouptotals_hq_investment_loc[$row["order_item_cost_group"]] + $row["order_item_client_price_hq_freezed"] * $row["order_item_quantity_hq_freezed"];

	
			if($row["order_item_type"] == 1)
			{
				/*
				$total_hq_supplied_standard_items = $total_hq_supplied_standard_items + $row["order_item_system_price_freezed"];

				$total_hq_supplied_standard_items_loc = $total_hq_supplied_standard_items_loc + $row["order_item_client_price_freezed"];
				*/

				$total_hq_supplied_standard_items = $total_hq_supplied_standard_items + ($row["order_item_system_price_freezed"] * $row["order_item_quantity_freezed"]);

				$total_hq_supplied_standard_items_loc = $total_hq_supplied_standard_items_loc + ($row["order_item_client_price_freezed"] * $row["order_item_quantity_freezed"]);
			}
			elseif($row["order_item_type"] == 2 and $row["order_item_cost_group"] != 10)
			{
				/*
				$total_hq_supplied_special_items = $total_hq_supplied_special_items + $row["order_item_system_price_freezed"];

				$total_hq_supplied_special_items_loc = $total_hq_supplied_special_items_loc + $row["order_item_client_price_freezed"];
				*/

				$total_hq_supplied_special_items = $total_hq_supplied_special_items + ($row["order_item_system_price_freezed"] * $row["order_item_quantity_freezed"]);

				$total_hq_supplied_special_items_loc = $total_hq_supplied_special_items_loc + ($row["order_item_client_price_freezed"] * $row["order_item_quantity_freezed"]);
			}
			elseif($row["order_item_cost_group"] == 10 or ($row["order_item_type"] == 3 and $row["order_item_cost_group"] == 2))
			{
				/*
				$total_hq_supplied_other = $total_hq_supplied_other + $row["order_item_system_price_freezed"];

				$total_hq_supplied_other_loc = $total_hq_supplied_other_loc + $row["order_item_client_price_freezed"];
				*/

				$total_hq_supplied_other = $total_hq_supplied_other + ($row["order_item_system_price_freezed"] * $row["order_item_quantity_freezed"]);

				$total_hq_supplied_other_loc = $total_hq_supplied_other_loc + ($row["order_item_client_price_freezed"] * $row["order_item_quantity_freezed"]);
			}

			
		}
		else
		{
			$grouptotals_hq_investment[$row["order_item_cost_group"]] = $grouptotals_hq_investment[$row["order_item_cost_group"]] + $row["order_item_system_price"];

			$grouptotals_hq_investment_loc[$row["order_item_cost_group"]] = $grouptotals_hq_investment_loc[$row["order_item_cost_group"]] + $row["order_item_client_price"];

			if($row["order_item_type"] == 1)
			{
				/*
				$total_hq_supplied_standard_items = $total_hq_supplied_standard_items + $row["order_item_system_price"];

				$total_hq_supplied_standard_items_loc = $total_hq_supplied_standard_items_loc + $row["order_item_client_price"];
				*/

				$total_hq_supplied_standard_items = $total_hq_supplied_standard_items + ($row["order_item_system_price"] * $row["order_item_quantity"]);

				$total_hq_supplied_standard_items_loc = $total_hq_supplied_standard_items_loc + ($row["order_item_client_price"] * $row["order_item_quantity"]);
			}
			elseif($row["order_item_type"] == 2 and $row["order_item_cost_group"] != 10)
			{
				/*
				$total_hq_supplied_special_items = $total_hq_supplied_special_items + $row["order_item_system_price"];

				$total_hq_supplied_special_items_loc = $total_hq_supplied_special_items_loc + $row["order_item_client_price"];
				*/

				$total_hq_supplied_special_items = $total_hq_supplied_special_items + ($row["order_item_system_price"] * $row["order_item_quantity"]);

				$total_hq_supplied_special_items_loc = $total_hq_supplied_special_items_loc + ($row["order_item_client_price"] * $row["order_item_quantity"]);
			}
			elseif($row["order_item_cost_group"] == 10 or ($row["order_item_type"] == 3 and $row["order_item_cost_group"] == 2))
			{
				/*
				$total_hq_supplied_other = $total_hq_supplied_other + $row["order_item_system_price"];

				$total_hq_supplied_other_loc = $total_hq_supplied_other_loc + $row["order_item_client_price"];
				*/

				$total_hq_supplied_other = $total_hq_supplied_other + ($row["order_item_system_price"] * $row["order_item_quantity"]);

				$total_hq_supplied_other_loc = $total_hq_supplied_other_loc + ($row["order_item_client_price"] * $row["order_item_quantity"]);
			}
		}
	}

	if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
	{
		if($row["order_item_quantity"] > 0 or $row["order_item_type"] == 1  or $row["order_item_type"] == 2)
		{
			$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_system_price"] * $row["order_item_quantity"];

			$grouptotals_investment_loc[$row["order_item_cost_group"]] = $grouptotals_investment_loc[$row["order_item_cost_group"]] + $row["order_item_client_price"] * $row["order_item_quantity"];

		}
		else
		{
			$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_system_price"];

			$grouptotals_investment_loc[$row["order_item_cost_group"]] = $grouptotals_investment_loc[$row["order_item_cost_group"]] + $row["order_item_client_price"];
		}
	}
	else
	{
		if($row["order_item_quantity_freezed"] > 0 or $row["order_item_type"] == 1 or $row["order_item_type"] == 2)
		{
			$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_system_price_freezed"] * $row["order_item_quantity_freezed"];

			$grouptotals_investment_loc[$row["order_item_cost_group"]] = $grouptotals_investment_loc[$row["order_item_cost_group"]] + $row["order_item_client_price_freezed"] * $row["order_item_quantity_freezed"];
		}
		else
		{
			$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_system_price_freezed"];

			$grouptotals_investment_loc[$row["order_item_cost_group"]] = $grouptotals_investment_loc[$row["order_item_cost_group"]] + $row["order_item_client_price_freezed"];
		}
	}

}


//budget for hq supplierd items
$hq_budget_total = $grouptotals_hq_investment[2] + $grouptotals_hq_investment[6];
$hq_budget_total_loc = $grouptotals_hq_investment_loc[2] + $grouptotals_hq_investment_loc[6];

//budget

$project_cost_construction = $grouptotals_investment[7];

$project_cost_fixturing = $grouptotals_investment[10] + $grouptotals_investment[6] + $grouptotals_investment[2];
$project_cost_hq_fixturing = $grouptotals_hq_investment[2];
$project_cost_fixturing_other = $grouptotals_investment[10];
$project_cost_freight_charges = $grouptotals_hq_investment[6];

$project_cost_architectural = $grouptotals_investment[9];
$project_cost_equipment = $grouptotals_investment[11];
$project_cost_other = + $grouptotals_investment[8];
$budget_total = $project_cost_construction + $project_cost_fixturing + $project_cost_architectural + $project_cost_equipment + $project_cost_other;

$project_cost_construction_loc = $grouptotals_investment_loc[7];

$project_cost_fixturing_loc = $grouptotals_investment_loc[10] + $grouptotals_investment_loc[6] + $grouptotals_investment_loc[2];
$project_cost_hq_fixturing_loc = $grouptotals_hq_investment_loc[2];
$project_cost_fixturing_other_loc = $grouptotals_investment_loc[10];
$project_cost_freight_charges_loc = $grouptotals_hq_investment_loc[6];

$project_cost_architectural_loc = $grouptotals_investment_loc[9];
$project_cost_equipment_loc = $grouptotals_investment_loc[11];
$project_cost_other_loc = + $grouptotals_investment_loc[8];
$budget_total_loc = $project_cost_construction_loc + $project_cost_fixturing_loc + $project_cost_architectural_loc + $project_cost_equipment_loc + $project_cost_other_loc;


//total cost
$standard_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_STANDARD);
$special_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_SPECIAL);
$special_item_total_hq = get_order_item_type_total($project["project_order"],  ITEM_TYPE_SPECIAL, 2);
$freight_charges = get_order_item_type_total($project["project_order"],  ITEM_TYPE_COST_ESTIMATION, 6);
$cost_estimation_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_COST_ESTIMATION);
$local_construction_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_LOCALCONSTRUCTIONCOST);


$total_cost = $standard_item_total["in_system_currency"] + $special_item_total["in_system_currency"] + $cost_estimation_item_total["in_system_currency"] + $local_construction_item_total["in_system_currency"];

//$hq_total_cost = $standard_item_total["in_system_currency"] + $special_item_total_hq["in_system_currency"] + $freight_charges["in_system_currency"];

$hq_total_cost = $standard_item_total["in_system_currency"] + $special_item_total_hq["in_system_currency"];


$total_cost_loc = $standard_item_total["in_order_currency"] + $special_item_total["in_order_currency"] + $cost_estimation_item_total["in_order_currency"] + $local_construction_item_total["in_order_currency"];

//$hq_total_cost_loc = $standard_item_total["in_order_currency"] + $special_item_total_hq["in_order_currency"] + $freight_charges["in_order_currency"];
$hq_total_cost_loc = $standard_item_total["in_order_currency"] + $special_item_total_hq["in_order_currency"];


$hq_budget_approval_data = get_hq_budget_approval_data($project["project_order"]);
$budget_approval_data = get_budget_approval_data($project["project_order"]);


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");

require_once "include/project_head_small.php";

$form->add_section("Client Currency Information");

if(has_access("can_change_currency_data_in_projects"))
{
	$form->add_list("order_client_currency", "Currency", $sql_currency, NOTNULL, $project["order_client_currency"]);
	$form->add_edit("order_client_exchange_rate", "Exchange Rate", NOTNULL, $project["order_client_exchange_rate"] , TYPE_DECIMAL, 10, 6);
}
else
{
	$form->add_label("client_currency", "Currency", 0, $order_currency["symbol"]);
	$form->add_label("exchange_rate", "Exchange Rate", 0, $order_currency["exchange_rate"]);
}

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$form->add_comment('<span class="error">All amounts for HQ supplied items converted from CHF into LOC are for reference only as the exchange rate may change.</span>');
}

$form->add_hidden("old_order_client_currency", $project["order_client_currency"]);
$form->add_hidden("old_order_client_exchange_rate", $project["order_client_exchange_rate"]);


if($project["order_budget_freezed_date"] and count($budget_approval_data) > 0)
{
	$form->add_section("Full Budget Approved by " . $budget_approval_data["username"] . " as per " . to_system_date($budget_approval_data["created"]));
}
elseif($project["order_hq_budget_freezed_date"] and count($hq_budget_approval_data) > 0)
{
	$form->add_section("Budget of HQ Supplied Items Approved by " . $hq_budget_approval_data["username"] . " as per " . to_system_date($hq_budget_approval_data["created"]));
}
else
{
	$form->add_section("Cost Overview as per " . date("d.m.Y"));

}

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$form->add_label("l1", "Local Construction in " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($project_cost_construction,2, ".", "'") . " / " . number_format($project_cost_construction_loc,2, ".", "'"));
	
	$form->add_label("l2", "Store fixturing / Furniture in " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($project_cost_fixturing,2, ".", "'") . " / " . number_format($project_cost_fixturing_loc,2, ".", "'"));
	
	if($project["order_hq_budget_freezed_date"] or $project["order_budget_freezed_date"])
	{
		$tmp1 = "- of which HQ Supplied Items in " . $system_currency["symbol"] . " / " . $order_currency["symbol"];
		if($project["order_budget_freezed_date"])
		{
			$tmp3 = to_system_date($project["order_budget_freezed_date"]);
		}
		else
		{
			$tmp3 = to_system_date($project["order_hq_budget_freezed_date"]);
		}

		$tmp2 = number_format($project_cost_hq_fixturing,2, ".", "'") . " / " . number_format($project_cost_hq_fixturing_loc,2, ".", "'") . " as per " . $tmp3;
		$form->add_label("l32", $tmp1, 0, $tmp2);
		
	
		$tmp1 = "- of which Freight Charges in " . $system_currency["symbol"] . " / " . $order_currency["symbol"];
		$tmp2 = number_format($project_cost_freight_charges,2, ".", "'") . " / " . number_format($project_cost_freight_charges_loc,2, ".", "'");
		$form->add_label("l33", $tmp1, 0, $tmp2);

		if(round($hq_total_cost,2) != round($project_cost_hq_fixturing,2))
		{
			$tmp = "<span class='red'>" . number_format($hq_total_cost,2, ".", "'") . " / " . number_format($hq_total_cost_loc,2, ".", "'") . "</span> as per " . date("d.m.Y H:i:s");
			
			$form->add_label("l4", " - Total cost of HQ supplied items from the list of materials in  " . $system_currency["symbol"] . " / " . $order_currency["symbol"], RENDER_HTML, $tmp);
		}
		
		/*
		else
		{
			$tmp = number_format($hq_total_cost,2, ".", "'") . " / " . number_format($hq_total_cost_loc,2, ".", "'") . " as per " . date("d.m.Y");
			$form->add_label("l5", " - Total cost of HQ supplied items from the list of materials in  " . $system_currency["symbol"] . " / " . $order_currency["symbol"], RENDER_HTML, $tmp);
		}
		*/

		$tmp1 = "- of which of which Fixturing: Other (local furniture, import taxes etc.) in " . $system_currency["symbol"] . " / " . $order_currency["symbol"];
		$tmp2 = number_format($project_cost_fixturing_other,2, ".", "'") . " / " . number_format($project_cost_fixturing_other_loc,2, ".", "'");
		$form->add_label("l31", $tmp1, 0, $tmp2);

	}
	else
	{
		
		$tmp1 = "- of which HQ Supplied Items in " . $system_currency["symbol"] . " / " . $order_currency["symbol"];
		$tmp2 = number_format($project_cost_hq_fixturing,2, ".", "'") . " / " . number_format($project_cost_hq_fixturing_loc,2, ".", "'");
		$form->add_label("l32", $tmp1, 0, $tmp2);
		
		$tmp1 = "- of which Freight Charges in " . $system_currency["symbol"] . " / " . $order_currency["symbol"];
		$tmp2 = number_format($project_cost_freight_charges,2, ".", "'") . " / " . number_format($project_cost_freight_charges_loc,2, ".", "'");
		$form->add_label("l33", $tmp1, 0, $tmp2);

		$tmp1 = "- of which of which Fixturing: Other (local furniture, import taxes etc.) in " . $system_currency["symbol"] . " / " . $order_currency["symbol"];
		$tmp2 = number_format($project_cost_fixturing_other,2, ".", "'") . " / " . number_format($project_cost_fixturing_other_loc,2, ".", "'");
		$form->add_label("l31", $tmp1, 0, $tmp2);


	}
	
	$form->add_label("l7", "Architectural Services in " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($project_cost_architectural,2, ".", "'") . " / " . number_format($project_cost_architectural_loc,2, ".", "'"));
	$form->add_label("l8", "Equipment in " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($project_cost_equipment,2, ".", "'") . " / " . number_format($project_cost_equipment_loc,2, ".", "'"));
	$form->add_label("l9", "Other in " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($project_cost_other,2, ".", "'") . " / " . number_format($project_cost_other_loc,2, ".", "'"));

	
	if($project["order_budget_freezed_date"] and count($budget_approval_data) > 0)
	{
		$tmp1 = "Approved Full Budget in " . $system_currency["symbol"] . " / " . $order_currency["symbol"];
		$tmp2 = number_format($budget_total,2, ".", "'") . " / " . number_format($budget_total_loc,2, ".", "'") . " as per " . to_system_date($project["order_budget_freezed_date"]);
		$form->add_label("l10", $tmp1, 0, $tmp2);

		
		if(round($total_cost,2) != round($budget_total,2))
		{
			
			$tmp = "<span class='red'>" . number_format($total_cost,2, ".", "'") . " / " . number_format($total_cost_loc,2, ".", "'") . "</span> as per " . date("d.m.Y H:i:s");
			
			$form->add_label("l11", "Total Cost from the list of materials in  " . $system_currency["symbol"] . " / " . $order_currency["symbol"], RENDER_HTML, $tmp);
		}
		else
		{
			$tmp = number_format($total_cost,2, ".", "'") . " / " . number_format($total_cost_loc,2, ".", "'") . " as per " . date("d.m.Y");
			$form->add_label("l12", "Total Cost from the list of materials in  " . $system_currency["symbol"] . " / " . $order_currency["symbol"], RENDER_HTML, $tmp);
		}
	}
	else
	{
		 $form->add_label("l13", "Full Budget in " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($budget_total,2, ".", "'") . " / " . number_format($budget_total_loc,2, ".", "'"));
	}

	/*
	if(count($hq_budget_approval_data) == 0 and count($budget_approval_data) == 0)
	{
		$form->add_label("l14", "Full Budget Total in " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($budget_total,2, ".", "'") . " / " . number_format($budget_total_loc,2, ".", "'"));
	}
	*/
}
else
{
	$form->add_label("l1", "Local Construction in " . $system_currency["symbol"], 0, number_format($project_cost_construction,2, ".", "'"));
	
	$form->add_label("l2", "Store fixturing / Furniture in " . $system_currency["symbol"], 0, number_format($project_cost_fixturing,2, ".", "'"));
	
	
	if($project["order_hq_budget_freezed_date"] or $project["order_budget_freezed_date"])
	{
		$tmp1 = "- of which HQ Supplied Items in " . $system_currency["symbol"] . " / " . $order_currency["symbol"];
		if($project["order_budget_freezed_date"])
		{
			$tmp3 = to_system_date($project["order_budget_freezed_date"]);
		}
		else
		{
			$tmp3 = to_system_date($project["order_hq_budget_freezed_date"]);
		}

		$tmp2 = number_format($project_cost_hq_fixturing,2, ".", "'") . " / " . number_format($project_cost_hq_fixturing_loc,2, ".", "'") . " as per " . $tmp3;
		$form->add_label("l32", $tmp1, 0, $tmp2);
	
		
		$tmp1 = "- of which Freight Charges in " . $system_currency["symbol"];
		$tmp2 = number_format($project_cost_freight_charges,2, ".", "'");
		$form->add_label("l33", $tmp1, 0, $tmp2);


		if(round($hq_total_cost,2) != round($project_cost_hq_fixturing,2))
		{
			$tmp = "<span class='red'>" . number_format($hq_total_cost,2, ".", "'") . "</span> as per " . date("d.m.Y H:i:s");
			
			$form->add_label("l4", " - Total cost of HQ supplied items from the list of materials in  " . $system_currency["symbol"], RENDER_HTML, $tmp);
		}
		/*
		else
		{
			$tmp = number_format($hq_total_cost,2, ".", "'") . " as per " . date("d.m.Y");
			$form->add_label("l5", " - Total cost of HQ supplied items from the list of materials in  " . $system_currency["symbol"], RENDER_HTML, $tmp);
		}
		*/

		$tmp1 = "- of which of which Fixturing: Other (local furniture, import taxes etc.) in " . $system_currency["symbol"];
		$tmp2 = number_format($project_cost_fixturing_other,2, ".", "'");
		$form->add_label("l31", $tmp1, 0, $tmp2);
	}
	else
	{
		
		

		$tmp1 = "- of which HQ Supplied Items in " . $system_currency["symbol"];
		$tmp2 = number_format($project_cost_hq_fixturing,2, ".", "'");
		$form->add_label("l32", $tmp1, 0, $tmp2);
		
		$tmp1 = "- of which Freight Charges in " . $system_currency["symbol"];
		$tmp2 = number_format($project_cost_freight_charges,2, ".", "'");
		$form->add_label("l33", $tmp1, 0, $tmp2);

		$tmp1 = "- of which of which Fixturing: Other (local furniture, import taxes etc.) in " . $system_currency["symbol"];
		$tmp2 = number_format($project_cost_fixturing_other,2, ".", "'");
		$form->add_label("l31", $tmp1, 0, $tmp2);


	}
	
	$form->add_label("l7", "Architectural Services in " . $system_currency["symbol"], 0, number_format($project_cost_architectural,2, ".", "'"));
	$form->add_label("l8", "Equipment in " . $system_currency["symbol"], 0, number_format($project_cost_equipment,2, ".", "'"));
	$form->add_label("l9", "Other in " . $system_currency["symbol"], 0, number_format($project_cost_other,2, ".", "'"));

	
	if($project["order_budget_freezed_date"] and count($budget_approval_data) > 0)
	{
		$tmp1 = "Approved Full Budget in " . $system_currency["symbol"];
		$tmp2 = number_format($budget_total,2, ".", "'") . " as per " . to_system_date($project["order_budget_freezed_date"]);
		$form->add_label("l10", $tmp1, 0, $tmp2);

		
		if(round($total_cost,2) != round($budget_total,2))
		{
			
			$tmp = "<span class='red'>" . number_format($total_cost,2, ".", "'") . "</span> as per " . date("d.m.Y H:i:s");
			
			$form->add_label("l11", "Total Cost from the list of materials in  " . $system_currency["symbol"], RENDER_HTML, $tmp);
		}
		else
		{
			$tmp = number_format($total_cost,2, ".", "'") . " as per " . date("d.m.Y");
			$form->add_label("l12", "Total Cost from the list of materials in  " . $system_currency["symbol"], RENDER_HTML, $tmp);
		}
	}
	else
	{
		 $form->add_label("l13", "Full Budget in " . $system_currency["symbol"], 0, number_format($budget_total,2, ".", "'"));
	}

	/*
	if(count($hq_budget_approval_data) == 0 and count($budget_approval_data) == 0)
	{
		$form->add_label("l14", "Full Budget Total in " . $system_currency["symbol"], 0, number_format($budget_total,2, ".", "'"));
	}
	*/
}

if(has_access("can_edit_budget_in_projects"))
{
	$form->add_section("Budget State");
	if($project["order_hq_budget_unfreezed_by"] > 0) {
		$form->add_label("hq_unfreezed_date", "Date last unfreeze budget of HQ supplied items", 0, to_system_date($project["order_hq_budget_unfreezed_date"]));
		$form->add_lookup("hq_unfreezed_by", "HQ supplied items unfreezed by", "users", "concat(user_name, ' ', user_firstname)", 0, $project["order_hq_budget_unfreezed_by"]);
	}
	else
	{
		$form->add_label("hq_unfreezed_date", "Date last unfreeze budget of HQ supplied items", 0);
		$form->add_label("hq_unfreezed_by", "Unfreezed by", 0);
	}

	if($project["order_budget_unfreezed_by"] > 0) {
		$form->add_label("unfreezed_date", "Date last unfreeze of full budget", 0, to_system_date($project["order_budget_unfreezed_date"]));
		$form->add_lookup("unfreezed_by", "Unfreezed by", "users", "concat(user_name, ' ', user_firstname)", 0, $project["order_budget_unfreezed_by"]);
	}
	else
	{
		$form->add_label("unfreezed_date", "Date last unfreeze of full budget", 0);
		$form->add_label("unfreezed_by", "Unfreezed by", 0);
	}

	
	if($change_of_budget_possible == true and has_access("can_freeze_budget"))
	{
		$form->add_checkbox("order_budget_is_locked", "Budget is locked", $project["order_budget_is_locked"], 0, "Budget State");
	}
	else
	{
		$form->add_checkbox("order_budget_is_locked", "Budget is locked", $project["order_budget_is_locked"], DISABLED, "Budget State");
	}
	$form->add_hidden("old_order_budget_is_locked", $project["order_budget_is_locked"]);
}


if(has_access("can_change_currency_data_in_projects"))
{
	$form->add_button("save_exr", "Save Exchang Rate");
}

if($change_of_budget_possible == true and has_access("can_edit_catalog"))
{
	$form->add_button("save", "Update Budget State");
	$form->add_button("recalculate", "Recalculate Budget");

	if(has_access("can_unfreeze_budget"))
	{
		$form->add_button("unfreeze_hq", "Unfreeze Budget of HQ supplied items");
		$form->add_button("unfreeze", "Unfreeze Full Budget");
		//$form->add_button("correct_prices", "Correct Prices");
		
	}
}

if($change_of_budget_possible == true and has_access("can_freeze_budget"))
{
	$form->add_button("freeze_hq", "(Freeze Budget of HQ Supplied Items)");
	$form->add_button("freeze", "(Freeze Full Budget)");
}

if($change_of_budget_possible == false)
{
	if($project["project_cost_type"] == 1) // corporate
	{
		$form->error("CER is already approved. Please contact Retail Controlling in case of action needed.");
	}
	else
	{
		$form->error("AF is already approved. Please contact Retail Controlling in case of action needed.");
	}
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("freeze_hq"))
{
    if($project["project_cost_type"] != 1) // corporate
	{
		update_budget_state($project["project_order"], 1);
		freeze_budget($project["project_order"]);
	}
	else
	{
		freeze_budget_hq_supplied_items($project["project_order"]);
	}
    $link = "project_project_budget.php?pid=" . param("pid");
    redirect($link);
}
elseif ($form->button("freeze"))
{
    update_budget_state($project["project_order"], 1);
	freeze_budget($project["project_order"]);
    $link = "project_project_budget.php?pid=" . param("pid");
    redirect($link);
}
elseif ($form->button("save"))
{
    update_budget_state($project["project_order"], $form->value("order_budget_is_locked"));


	if($form->value("old_order_budget_is_locked") != $form->value("order_budget_is_locked"))
	{
		if($form->value("order_budget_is_locked") == 1)
		{
			$actionmail = new ActionMail(35);
		}
		else
		{
			$actionmail = new ActionMail(34);
		}
		$actionmail->setParam('id', param("pid"));
		

		if($senmail_activated == true)
		{
			$actionmail->send();
		}

		$recipient = $actionmail->getRecipient($project['project_retail_coordinator']);

		if ($actionmail->isSuccess() && $recipient->isSendMail()) {
			$bodytext = $recipient->getContent();
			append_mail($project["project_order"], $project['project_retail_coordinator'], user_id(), $bodytext, "910", 1);
			append_mail($project["project_order"], $project['order_retail_operator'], user_id(), $bodytext, "910", 1);
		}

	}




    $link = "project_project_budget.php?pid=" . param("pid");
    redirect($link);
}
elseif($form->button("save_exr"))
{
	if($form->validate())
	{
		if(($form->value('old_order_client_currency') != $form->value('order_client_currency'))
			or ($form->value('old_order_client_exchange_rate') != $form->value('order_client_exchange_rate'))
			)
		{
			$old_currency = $currency_list[$form->value('old_order_client_currency')]["symbol"] . " " . $form->value('old_order_client_exchange_rate');

			$new_currency = $currency_list[$form->value('order_client_currency')]["symbol"] . " " . $form->value('order_client_exchange_rate');


			//project tracking
			$field = "currency";
			$sql = "Insert into projecttracking (" . 
				   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
				   user_id() . ", " . 
				   $project["project_id"] . ", " . 
				   dbquote("order_client_currency") . ", " . 
				   dbquote($old_currency ) . ", " . 
				   dbquote($new_currency) . ", " . 
				   dbquote('manually changed in edit budget') . ", " . 
				   dbquote(date("Y-m-d:H:i:s")) . ")"; 
			$result = mysql_query($sql) or dberror($sql);
		}

		$result = update_order_with_exchange_rate_info(param("oid"), $form->value("order_client_currency"), $form->value("order_client_exchange_rate"), 1);

		 $link = "project_project_budget.php?pid=" . param("pid");
		 redirect($link);
	}
}
elseif($form->button("recalculate"))
{
    update_exchange_rates($project["project_order"], 1);
    $link = "project_project_budget.php?pid=" . param("pid");
    redirect($link);

}
elseif ($form->button("unfreeze_hq"))
{
    unfreeze_project_hq_budget($project["project_order"]);
	unfreeze_project_budget($project["project_order"]);

	//send_project_event_mail(user_id(), 'unfreeze_project_budget', $project);

	//send mail to logistics coordinator and retail administrator
	$today = date("Y-m-d");

	//get sender
	$sql = "select user_id, user_email, user_name, user_firstname , " .
		   "concat(user_name, ' ', user_firstname) as sender_name " .
		   "from users where user_id = " . dbquote(user_id());


	$res = mysql_query($sql) or dberror($sql);
    if($row = mysql_fetch_assoc($res))
	{
		$sender_name = $row['sender_name'];
		$sender_firstname = $row['user_firstname'];
		$sender_lastname = $row['user_name'];
		$sender_email = $row['user_email'];
		$sender_id = user_id();

		$sql = " select * from mail_alert_types where mail_alert_type_id = 39";

		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$subject = "Budget for HQ supplied items was unfreezed";
			$text = $row["mail_alert_mail_text"];
			
			$cc_mail1 = $row["mail_alert_type_cc1"];
			$cc_mail2 = $row["mail_alert_type_cc2"];
			$cc_mail3 = $row["mail_alert_type_cc3"];
			$cc_mail4 = $row["mail_alert_type_cc4"];

			$subject = MAIL_SUBJECT_PREFIX . ": Project " . $project["order_number"] . ": " . $subject . " for " .  $project["order_shop_address_company"];


			$text = str_replace("\r\n", "\n", $text) . "\n\n";
			$link ="project_task_center.php?pid=" . $project["project_id"];
			$link = APPLICATION_URL ."/user/" . $link; 
			
			$text = str_replace("{link}", $link, $text);
			$text = str_replace("{sender_firstname}", $sender_firstname, $text);
			$text = str_replace("{sender_name}", $sender_lastname, $text);

			
			$mail = new PHPMailer();
			$mail->Subject = $subject;
			$mail->SetFrom($sender_email, $sender_name);
			$mail->AddReplyTo($sender_email, $sender_name);

			$rcpts = "";
			$recipient_ids = array();
			$cc_ids = array();
			
			$sql = "select user_id, user_email, user_firstname, " . 
				   "concat(user_name, ' ', user_firstname) as user_name " .
		           "from users where user_id = " . dbquote($project["order_retail_operator"]);

			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$mail->AddAddress($row["user_email"], $row["user_name"]);
				$rcpts .= $row["user_email"] . "\n";

				$text = str_replace("{recipient_fistname}",  $row["user_firstname"], $text);
				$recipient_ids[] = $row["user_id"];

				if($cc_mail1) {
					$mail->AddCC($cc_mail1);
					$rcpts .= $cc_mail1 . "\n";
				}
				if($cc_mail2) {
					$mail->AddCC($cc_mail2);
					$rcpts .= $cc_mail2 . "\n";
				}
				if($cc_mail3) {
					$mail->AddCC($cc_mail3);
					$rcpts .= $cc_mail3 . "\n";
				}
				if($cc_mail3) {
					$mail->AddCC($cc_mail3);
					$rcpts .= $cc_mail3 . "\n";
				}
			}


			$sql = "select user_id, user_email, user_firstname, " . 
				   "concat(user_name, ' ', user_firstname) as user_name " .
		           "from users where user_id = " . dbquote($project["project_retail_coordinator"]);

			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				
				$mail->AddCC($row["user_email"]);
				$rcpts .= $row["user_email"] . "\n";
				$cc_ids[] = $row["user_id"];
			}

			$mail->Body = $text;

			if($rcpts) {			
				if($senmail_activated == true)
				{
					$mail->send();
				}
				
				foreach($recipient_ids as $key=>$recipient_id) {
					append_mail($project['project_order'], $recipient_id, $sender_id, $text, '910', 1,0);
				}

				foreach($cc_ids as $key=>$recipient_id) {
					append_mail($project['project_order'], $recipient_id, $sender_id, $text, '910', 1,0, 1);
				}
			}

		}	
	}

    $link = "project_project_budget.php?pid=" . param("pid");
    redirect($link);
}
elseif ($form->button("unfreeze"))
{
    unfreeze_project_budget($project["project_order"]);
	//send_project_event_mail(user_id(), 'unfreeze_project_budget', $project);

	//send mail to logistics coordinator and retail administrator
	$today = date("Y-m-d");

	//get sender
	$sql = "select user_id, user_email, user_name, user_firstname , " .
		   "concat(user_name, ' ', user_firstname) as sender_name " .
		   "from users where user_id = " . dbquote(user_id());


	$res = mysql_query($sql) or dberror($sql);
    if($row = mysql_fetch_assoc($res))
	{
		$sender_name = $row['sender_name'];
		$sender_firstname = $row['user_firstname'];
		$sender_lastname = $row['user_name'];
		$sender_email = $row['user_email'];
		$sender_id = user_id();

		$sql = " select * from mail_alert_types where mail_alert_type_id = 39";

		$res = mysql_query($sql) or dberror($sql);
		if($row = mysql_fetch_assoc($res))
		{
			$subject = "Full Budget was unfreezed";
			$text = $row["mail_alert_mail_text"];
			
			$cc_mail1 = $row["mail_alert_type_cc1"];
			$cc_mail2 = $row["mail_alert_type_cc2"];
			$cc_mail3 = $row["mail_alert_type_cc3"];
			$cc_mail4 = $row["mail_alert_type_cc4"];

			$subject = MAIL_SUBJECT_PREFIX . ": Project " . $project["order_number"] . ": " . $subject . " for " .  $project["order_shop_address_company"];


			$text = str_replace("\r\n", "\n", $text) . "\n\n";
			$link ="project_task_center.php?pid=" . $project["project_id"];
			$link = APPLICATION_URL ."/user/" . $link; 
			
			$text = str_replace("{link}", $link, $text);
			$text = str_replace("{sender_firstname}", $sender_firstname, $text);
			$text = str_replace("{sender_name}", $sender_lastname, $text);

			
			$mail = new PHPMailer();
			$mail->Subject = $subject;
			$mail->SetFrom($sender_email, $sender_name);
			$mail->AddReplyTo($sender_email, $sender_name);

			$rcpts = "";
			$recipient_ids = array();
			$cc_ids = array();
			
			$sql = "select user_id, user_email, user_firstname, " . 
				   "concat(user_name, ' ', user_firstname) as user_name " .
		           "from users where user_id = " . dbquote($project["order_retail_operator"]);

			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				$mail->AddAddress($row["user_email"], $row["user_name"]);
				$rcpts .= $row["user_email"] . "\n";

				$text = str_replace("{recipient_fistname}",  $row["user_firstname"], $text);
				$recipient_ids[] = $row["user_id"];

				if($cc_mail1) {
					$mail->AddCC($cc_mail1);
					$rcpts .= $cc_mail1 . "\n";
				}
				if($cc_mail2) {
					$mail->AddCC($cc_mail2);
					$rcpts .= $cc_mail2 . "\n";
				}
				if($cc_mail3) {
					$mail->AddCC($cc_mail3);
					$rcpts .= $cc_mail3 . "\n";
				}
				if($cc_mail3) {
					$mail->AddCC($cc_mail3);
					$rcpts .= $cc_mail3 . "\n";
				}
			}


			$sql = "select user_id, user_email, user_firstname, " . 
				   "concat(user_name, ' ', user_firstname) as user_name " .
		           "from users where user_id = " . dbquote($project["project_retail_coordinator"]);

			$res = mysql_query($sql) or dberror($sql);
			if($row = mysql_fetch_assoc($res))
			{
				
				$mail->AddCC($row["user_email"]);
				$rcpts .= $row["user_email"] . "\n";
				$cc_ids[] = $row["user_id"];
			}

			$mail->Body = $text;

			if($rcpts) {			
				if($senmail_activated == true)
				{
					$mail->send();
				}
				
				foreach($recipient_ids as $key=>$recipient_id) {
					append_mail($project['project_order'], $recipient_id, $sender_id, $text, '910', 1,0);
				}

				foreach($cc_ids as $key=>$recipient_id) {
					append_mail($project['project_order'], $recipient_id, $sender_id, $text, '910', 1,0, 1);
				}
			}

		}	
	}

    $link = "project_project_budget.php?pid=" . param("pid");
    redirect($link);
}

/********************************************************************
    Create Locla COnstruction Cost Positions
*********************************************************************/ 
$list6 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list6->set_title("Local Construction");
$list6->set_entity("order_item");
$list6->set_filter($list6_filter);
$list6->set_order("item_code, order_item_id");

$list6->add_hidden("pid", param("pid"));
$list6->add_hidden("oid",$project["project_order"]);

$list6->add_column("item_shortcut", "Code");
$list6->add_column("order_item_text", "Text");

if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
{
    
	$list6->add_column("order_item_system_price", "Costs " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT);
	
	
	$list6->set_footer("item_shortcut",  "Total");
	$list6->set_footer("order_item_system_price", number_format($project_cost_construction,2, ".", "'") . "\n" . $system_currency["symbol"]);
	
	if($system_currency["symbol"] != $order_currency["symbol"])
	{
		$list6->add_column("order_item_client_price", "Costs " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
		$list6->set_footer("order_item_client_price", number_format($project_cost_construction_loc,2, ".", "'") . "\n" . $order_currency["symbol"] );
	}
	
}
else
{
    $list6->add_column("order_item_system_price_freezed", "Estimated Cost " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT);
	

	$list6->set_footer("item_shortcut",  "Total");
	$list6->set_footer("order_item_system_price_freezed", number_format($project_cost_construction,2, ".", "'") . "\n" . $system_currency["symbol"]);
	
	if($system_currency["symbol"] != $order_currency["symbol"])
	{
		$list6->add_column("order_item_client_price_freezed", "Estimated Cost " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
		$list6->set_footer("order_item_client_price_freezed", number_format($project_cost_construction_loc,2, ".", "'") . "\n" . $order_currency["symbol"]);
	}
}
$list6->add_column("supplier_info", "Supplier");



/********************************************************************
    Create Standard Item List
*********************************************************************/ 
$list1 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list1->set_title("Store fixturing / Furniture - Standard Items");
$list1->set_entity("order_item");
$list1->set_filter($list1_filter);
$list1->set_order("item_code");
$list1->set_group("cat_name");

$list1->add_hidden("pid", param("pid"));
$list1->add_hidden("oid",$project["project_order"]);

$list1->add_column("item_shortcut", "Item Code");
$list1->add_column("order_item_text", "Name");

if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
{
    $list1->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
	$list1->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	
	$list1->set_footer("item_shortcut", "Total");
	$list1->add_column("total_price", "Total " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list1->set_footer("total_price", number_format($total_hq_supplied_standard_items,2, ".", "'") . "\n" . $system_currency["symbol"]);

	if($system_currency["symbol"] != $order_currency["symbol"])
	{
		$list1->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
		$list1->add_column("total_price_loc", "Total " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
		$list1->set_footer("total_price_loc", number_format($total_hq_supplied_standard_items_loc,2, ".", "'")  . "\n" . $order_currency["symbol"] );
	}
}
else
{
    $list1->add_column("order_item_quantity_freezed", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
	$list1->add_column("order_item_system_price_freezed", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

	$list1->set_footer("item_shortcut", "Total");
	$list1->add_column("total_price", "Total " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list1->set_footer("total_price",  number_format($total_hq_supplied_standard_items,2, ".", "'") . "\n" . $system_currency["symbol"]);

	if($system_currency["symbol"] != $order_currency["symbol"])
	{
		$list1->add_column("order_item_client_price_freezed", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
		$list1->add_column("total_price_loc", "Total " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
		$list1->set_footer("total_price_loc", number_format($total_hq_supplied_standard_items_loc,2, ".", "'") . "\n" . $order_currency["symbol"] );
	}
}

$list1->add_column("supplier_info", "Supplier");

$list1->add_column("supplier_info", "Supplier");




/********************************************************************
    Create Special Items
*********************************************************************/ 
$list1a = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list1a->set_title("Store fixturing / Furniture - Special Items");
$list1a->set_entity("order_item");
$list1a->set_filter($list1a_filter);
$list1a->set_order("item_code");
$list1a->set_group("cat_name");

$list1a->add_hidden("pid", param("pid"));
$list1a->add_hidden("oid",$project["project_order"]);

$list1a->add_column("item_shortcut", "Item Code");
$list1a->add_column("order_item_text", "Name");

if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
{
    $list1a->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
	$list1a->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	
	$list1a->set_footer("item_shortcut", "Total");
	$list1a->add_column("total_price", "Total " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list1a->set_footer("total_price", number_format($total_hq_supplied_special_items,2, ".", "'") . "\n" . $system_currency["symbol"]);

	

	if($system_currency["symbol"] != $order_currency["symbol"])
	{
		$list1a->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
		$list1a->add_column("total_price_loc", "Total " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
		$list1a->set_footer("total_price_loc", number_format($total_hq_supplied_special_items_loc,2, ".", "'")  . "\n" . $order_currency["symbol"] );
	}
}
else
{
    $list1a->add_column("order_item_quantity_freezed", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
	$list1a->add_column("order_item_system_price_freezed", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

	$list1a->set_footer("item_shortcut", "Total");
	$list1a->add_column("total_price", "Total " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list1a->set_footer("total_price",  number_format($total_hq_supplied_special_items,2, ".", "'") . "\n" . $system_currency["symbol"]);

	if($system_currency["symbol"] != $order_currency["symbol"])
	{
		$list1a->add_column("order_item_client_price_freezed", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
		$list1a->add_column("total_price_loc", "Total " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
		$list1a->set_footer("total_price_loc", number_format($total_hq_supplied_special_items_loc,2, ".", "'") . "\n" . $order_currency["symbol"] );
	}
}

$list1a->add_column("supplier_info", "Supplier");

/********************************************************************
    Create list for freight charges
*********************************************************************/ 
$list1b = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list1b->set_title("Store fixturing / Furniture - Freight Charges");
$list1b->set_entity("order_item");
$list1b->set_filter($list1b_filter);
$list1b->set_order("item_code");
$list1b->set_group("cat_name");

$list1b->add_hidden("pid", param("pid"));
$list1b->add_hidden("oid",$project["project_order"]);

$list1b->add_column("item_shortcut", "Item Code");
$list1b->add_column("order_item_text", "Name");

if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
{
    //$list1b->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
	$list1b->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	
	$list1b->set_footer("item_shortcut", "Total");
	$list1b->add_column("total_price", "Total " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list1b->set_footer("total_price", number_format($project_cost_freight_charges,2, ".", "'") . "\n" . $system_currency["symbol"]);

	

	if($system_currency["symbol"] != $order_currency["symbol"])
	{
		$list1b->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
		$list1b->add_column("total_price_loc", "Total " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
		$list1b->set_footer("total_price_loc", number_format($project_cost_freight_charges_loc,2, ".", "'")  . "\n" . $order_currency["symbol"] );
	}
}
else
{
    //$list1b->add_column("order_item_quantity_freezed", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
	$list1b->add_column("order_item_system_price_freezed", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

	$list1b->set_footer("item_shortcut", "Total");
	$list1b->add_column("total_price", "Total " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list1b->set_footer("total_price",  number_format($project_cost_freight_charges,2, ".", "'") . "\n" . $system_currency["symbol"]);

	if($system_currency["symbol"] != $order_currency["symbol"])
	{
		$list1b->add_column("order_item_client_price_freezed", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
		$list1b->add_column("total_price_loc", "Total " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
		$list1b->set_footer("total_price_loc", number_format($project_cost_freight_charges_loc,2, ".", "'") . "\n" . $order_currency["symbol"] );
	}
}

$list1b->add_column("supplier_info", "Supplier");


/********************************************************************
    Create list for other cost hq supplied
*********************************************************************/ 
$list1c = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list1c->set_title("Store fixturing / Furniture - Other");
$list1c->set_entity("order_item");
$list1c->set_filter($list1c_filter);
$list1c->set_order("item_code");
$list1c->set_group("cat_name");

$list1c->add_hidden("pid", param("pid"));
$list1c->add_hidden("oid",$project["project_order"]);

$list1c->add_column("item_shortcut", "Item Code");
$list1c->add_column("order_item_text", "Name");

if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
{
    //$list1c->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
	$list1c->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	
	$list1c->set_footer("item_shortcut", "Total");
	$list1c->add_column("total_price", "Total " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list1c->set_footer("total_price", number_format($total_hq_supplied_other,2, ".", "'") . "\n" . $system_currency["symbol"]);


	if($system_currency["symbol"] != $order_currency["symbol"])
	{
		$list1c->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
		$list1c->add_column("total_price_loc", "Total " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
		$list1c->set_footer("total_price_loc", number_format($total_hq_supplied_other_loc,2, ".", "'")  . "\n" . $order_currency["symbol"] );
	}
}
else
{
    //$list1c->add_column("order_item_quantity_freezed", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
	$list1c->add_column("order_item_system_price_freezed", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

	$list1c->set_footer("item_shortcut", "Total");
	$list1c->add_column("total_price", "Total " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list1c->set_footer("total_price",  number_format($total_hq_supplied_other,2, ".", "'") . "\n" . $system_currency["symbol"]);

	if($system_currency["symbol"] != $order_currency["symbol"])
	{
		$list1c->add_column("order_item_client_price_freezed", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
		$list1c->add_column("total_price_loc", "Total " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
		$list1c->set_footer("total_price_loc", number_format($total_hq_supplied_other_loc,2, ".", "'") . "\n" . $order_currency["symbol"] );
	}
}

$list1c->add_column("supplier_info", "Supplier");


/********************************************************************
    Create Architectural Cost
*********************************************************************/ 
$list2 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list2->set_title("Architectural Services");
$list2->set_entity("order_item");
$list2->set_filter($list2_filter);
//$list2->set_order("item_code_text");

$list2->add_hidden("pid", param("pid"));
$list2->add_hidden("oid",$project["project_order"]);

$list2->add_column("item_shortcut", "Item Code");
$list2->add_column("order_item_text", "Name");

if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
{
	$list2->add_column("total_price", "Costs " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list2->set_footer("item_shortcut", "Total");
	$list2->set_footer("total_price", number_format($project_cost_architectural,2, ".", "'") . "\n" . $system_currency["symbol"] );

	if($system_currency["symbol"] != $order_currency["symbol"])
	{
		$list2->add_column("total_price_loc", "Costs " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
		$list2->set_footer("total_price_loc", number_format($project_cost_architectural_loc,2, ".", "'") . "\n" . $order_currency["symbol"]);
	}
}
else
{
	$list2->add_column("total_price", "Costs " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list2->set_footer("item_shortcut", "Total");
	$list2->set_footer("total_price", number_format($project_cost_architectural,2, ".", "'")  . "\n" . $system_currency["symbol"] );

	if($system_currency["symbol"] != $order_currency["symbol"])
	{
		$list2->add_column("total_price_loc", "Costs " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
		$list2->set_footer("total_price_loc", number_format($project_cost_architectural_loc,2, ".", "'") . "\n" . $order_currency["symbol"]);
	}
}


$list2->add_column("supplier_info", "Supplier");




/********************************************************************
    Create Equipment
*********************************************************************/ 
$list3 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list3->set_title("Equipment");
$list3->set_entity("order_item");
$list3->set_filter($list3_filter);

$list3->add_hidden("pid", param("pid"));
$list3->add_hidden("oid",$project["project_order"]);

$list3->add_column("item_shortcut", "Code");
$list3->add_column("order_item_text", "Text");

if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
{
	$list3->add_column("total_price", "Costs " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list3->set_footer("item_shortcut", "Total");
	$list3->set_footer("total_price", number_format($project_cost_equipment,2, ".", "'")  . "\n" . $system_currency["symbol"]);

	if($system_currency["symbol"] != $order_currency["symbol"])
	{
		$list3->add_column("total_price_loc", "Costs " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
		$list3->set_footer("total_price_loc", number_format($project_cost_equipment_loc,2, ".", "'") . "\n" . $order_currency["symbol"]);
	}
}
else
{
	$list3->add_column("total_price", "Costs " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list3->set_footer("item_shortcut", "Total");
	$list3->set_footer("total_price", number_format($project_cost_equipment,2, ".", "'") . "\n" . $system_currency["symbol"]);

	if($system_currency["symbol"] != $order_currency["symbol"])
	{
		$list3->add_column("total_price_loc", "Costs " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
		$list3->set_footer("total_price_loc", number_format($project_cost_equipment_loc,2, ".", "'")  . "\n" . $order_currency["symbol"]);
	}
}
$list3->add_column("supplier_info", "Supplier");


/********************************************************************
    Other Cost
*********************************************************************/ 
$list7 = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list7->set_title("Other");
$list7->set_entity("order_item");
$list7->set_filter($list7_filter);

$list7->add_hidden("pid", param("pid"));
$list7->add_hidden("oid",$project["project_order"]);

$list7->add_column("item_shortcut", "Code");
$list7->add_column("order_item_text", "Text");

if($project["order_budget_freezed_date"] == NULL OR $project["order_budget_freezed_date"] == "0000-00-00")
{
	$list7->add_column("total_price", "Costs " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list7->set_footer("item_shortcut", "Total");
	$list7->set_footer("total_price", number_format($project_cost_other,2, ".", "'") . "\n" . $system_currency["symbol"] );

	if($system_currency["symbol"] != $order_currency["symbol"])
	{
		$list7->add_column("total_price_loc", "Costs " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
		$list7->set_footer("total_price_loc", number_format($project_cost_other_loc,2, ".", "'")  . "\n" . $order_currency["symbol"]);
	}
}
else
{
	$list7->add_column("total_price", "Costs " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list7->set_footer("item_shortcut", "Total");
	$list7->set_footer("total_price", number_format($project_cost_other,2, ".", "'")  . "\n" . $system_currency["symbol"]);

	if($system_currency["symbol"] != $order_currency["symbol"])
	{
		$list7->add_column("total_price_loc", "Costs " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
		$list7->set_footer("total_price_loc", number_format($project_cost_other_loc,2, ".", "'")  . "\n" . $order_currency["symbol"]);
	}
}

$list7->add_column("supplier_info", "Supplier");


/********************************************************************
    Create Exclusion Positions
*********************************************************************/ 
$list_exclusions = new ListView($sql_order_items, LIST_HAS_HEADER);

$list_exclusions->set_title("Exclusions");
$list_exclusions->set_entity("order_item");
$list_exclusions->set_filter("order_item_order = " . $project["project_order"] .  " and order_item_type = " . ITEM_TYPE_EXCLUSION);
$list_exclusions->set_order("item_shortcut");

$list_exclusions->add_hidden("pid", param("pid"));
$list_exclusions->add_hidden("oid",$project["project_order"]);

$link="project_edit_budget_position.php?pid=" . param("pid");


if ($project["order_budget_is_locked"] == 1 or !has_access("can_edit_budget_in_projects"))
{
    $list_exclusions->add_column("item_shortcut", "Item Code");   
}
else
{
    $list_exclusions->add_column("item_shortcut", "Item Code", $link);
}

$list_exclusions->add_column("order_item_text", "Exclusion");


if ($project["order_budget_is_locked"] == 1 or !has_access("can_edit_budget_in_projects"))
{
	$list_exclusions->add_button("nothing", "");
}
else
{
	$list_exclusions->add_button("add_s_exclusion", "Add Standard Exclusions");
	$list_exclusions->add_button("add_exclusion", "Add Individual Exclusion");
	$list_exclusions->add_button("delete_exclusions", "Delete All Exclusions");
}


//process exclusions
$list_exclusions->populate();
$list_exclusions->process();

if ($list_exclusions->button("add_s_exclusion"))
{
     add_standard_budget_positions(ITEM_TYPE_EXCLUSION, $project["project_order"], $project["project_cost_type"]);
     redirect ("project_project_budget.php?pid=" . param("pid"));
}
else if ($list_exclusions->button("add_exclusion"))
{
    $link = "project_add_budget_position.php?pid=" . param("pid") . "&oid=" . $project["project_order"] . "&type=" . ITEM_TYPE_EXCLUSION; 
    redirect($link);
}
else if ($list_exclusions->button("delete_exclusions"))
{
     delete_all_budget_positions(ITEM_TYPE_EXCLUSION, $project["project_order"]);
     redirect ("project_project_budget.php?pid=" . param("pid"));
}

/********************************************************************
    Create Notification Positions
*********************************************************************/ 
$list_notifications = new ListView($sql_order_items, LIST_HAS_HEADER);

$list_notifications->set_title("Notifications");
$list_notifications->set_entity("order_item");
$list_notifications->set_filter("order_item_order = " . $project["project_order"] .  " and order_item_type = " . ITEM_TYPE_NOTIFICATION);
$list_exclusions->set_order("item_shortcut");

$list_notifications->add_hidden("pid", param("pid"));
$list_notifications->add_hidden("oid",$project["project_order"]);

$link="project_edit_budget_position.php?pid=" . param("pid");

if ($project["order_budget_is_locked"] == 1 or !has_access("can_edit_budget_in_projects"))
{
    $list_notifications->add_column("item_shortcut", "Item Code");   
}
else
{
    $list_notifications->add_column("item_shortcut", "Item Code", $link);
}

$list_notifications->add_column("order_item_text", "Notification");

if ($project["order_budget_is_locked"] == 1 or !has_access("can_edit_budget_in_projects"))
{
    $list_notifications->add_button("nothing", "");
}
else
{
    $list_notifications->add_button("add_s_notification", "Add Standard Notifications");
    $list_notifications->add_button("add_notification", "Add Individual Notification");
    $list_notifications->add_button("delete_notifications", "Delet All Notifications");
}


//rpocess notifications
$list_notifications->populate();
$list_notifications->process();

if ($list_notifications->button("add_s_notification"))
{
     add_standard_budget_positions(ITEM_TYPE_NOTIFICATION, $project["project_order"], $project["project_cost_type"]);
     redirect ("project_project_budget.php?pid=" . param("pid"));
}
elseif ($list_notifications->button("add_notification"))
{
    $link = "project_add_budget_position.php?pid=" . param("pid") . "&oid=" . $project["project_order"] . "&type=" . ITEM_TYPE_NOTIFICATION; 

    redirect($link);
}
elseif ($list_notifications->button("delete_notifications"))
{
     delete_all_budget_positions(ITEM_TYPE_NOTIFICATION, $project["project_order"]);
     redirect ("project_project_budget.php?pid=" . param("pid"));
}

/********************************************************************
    popluate lists
*********************************************************************/
$list6->populate();
//$list6->process();

$list1->populate();
//$list1->process();

$list1a->populate();

$list1b->populate();

$list1c->populate();

$list2->populate();
//$list2->process();

$list3->populate();
//$list3->process();


$list_exclusions->populate();
$list_exclusions->process();

$list_notifications->populate();
$list_notifications->process();

/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
if($project["order_budget_freezed_date"] and count($budget_approval_data) > 0)
{
	$page->title("Approved Project Budget");
}
elseif($project["order_hq_budget_freezed_date"] and count($hq_budget_approval_data) > 0)
{
	$page->title("Approved Project Budget");
}
else
{
	$page->title("Project Budget");
}

$form->render();
echo "<br />";
$list6->render();
echo "<br>";

echo "<br>";
$list1->render();
echo "<br>";


echo "<br>";
$list1a->render();
echo "<br>";


echo "<br>";
$list1b->render();
echo "<br>";

echo "<br>";
$list1c->render();
echo "<br>";

echo "<br>";
$list2->render();
echo "<br>";

echo "<br>";
$list3->render();
echo "<br>";

$list7->render();
echo "<br>";



$list_exclusions->render();
echo "<br /><br />";
$list_notifications->render();

require_once("include/order_currency_helper.php");

require_once "include/project_footer_logistic_state.php";

$page->footer();

?>