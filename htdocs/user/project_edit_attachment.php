<?php
/********************************************************************


    project_edit_attachment.php


    Add comments to a project


    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-10-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-06-18
    Version:        1.0.1


    Copyright (c) 2002, Swatch AG, All Rights Reserved.


*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_edit_attachment_data_in_projects");

/********************************************************************
    prepare all data needed
*********************************************************************/
$user_roles = get_user_roles(user_id());
$user = get_user(user_id());

// read project and order details
$project = get_project(param("pid"));


// get company's address
$client_address = get_address($project["order_client_address"]);


// buld sql for attachment categories


if($project["project_projectkind"] == 8)
{
	$sql_attachment_categories = "select order_file_category_id, concat(order_file_category_priority, ' ', order_file_category_name) as group_name ".
                             "from order_file_categories ".
                             "where order_file_category_type = 2 ".
                             "order by order_file_category_priority";
}
else
{
	$sql_attachment_categories = "select order_file_category_id, concat(order_file_category_priority, ' ', order_file_category_name) as group_name ".
                             "from order_file_categories ".
                             "where order_file_category_id <> 18 and order_file_category_type = 2 ".
                             "order by order_file_category_priority";
}




$supplier1 = 0;
$supplier2 = 0;
$supplier3 = 0;
$supplier4 = 0;
$supplier5 = 0;
$supplier6 = 0;


$sql_suppliers = 'select DISTINCT address_id, concat(address_company, " (" , user_name, user_firstname , ")") as supplier_name ' . 
                 'from addresses ' .
				 'left join users on user_address = address_id '.
				 ' where address_type IN (2,8) and address_active= 1 and user_active = 1 ' .
				 ' order by supplier_name';


// get addresses involved in the project
$companies = get_involved_companies($project["project_order"], id());


// determine users that have already got an email announcing the new attachment
$old_recipients = array();
foreach($companies as $key=>$value)
{
    if($value["access"] == 1)
    {
        $old_recipients[] = $value["user"];
    }
}


$sql_order_addresses = 'select DISTINCT order_file_address_address from order_files ' . 
                       'left join order_file_addresses on order_file_address_file = order_file_id ' .
					   'where order_file_address_file = ' . id() . ' and order_file_order = ' . $project['project_order'];

$res = mysql_query($sql_order_addresses) or dberror($sql_order_addresses);
while ($row = mysql_fetch_assoc($res))
{
	$set_supplier = true;
	foreach($companies as $key=>$company) {
	
		if($company["id"] == $row["order_file_address_address"]) {
			$set_supplier = false;
		}
	}
	
	if($set_supplier == true and $supplier1 == 0) {
		
		$supplier1 = $row["order_file_address_address"];
	}
	elseif($set_supplier == true and $supplier2 == 0) {
		$supplier2 = $row["order_file_address_address"];
	}
	elseif($set_supplier == true and $supplier3 == 0) {
		$supplier3 = $row["order_file_address_address"];
	}
	elseif($set_supplier == true and $supplier4 == 0) {
		$supplier4 = $row["order_file_address_address"];
	}
	elseif($set_supplier == true and $supplier5 == 0) {
		$supplier5 = $row["order_file_address_address"];
	}
	elseif($set_supplier == true and $supplier6 == 0) {
		$supplier6 = $row["order_file_address_address"];
	}
}


//get all involved suppliers
$involved_suppliers = array();

$sql =  "select DISTINCT order_item_supplier_address,  address_company " . 
		"from order_items ".
		"left join addresses on address_id = order_item_supplier_address " . 
		" where (order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
				  "   and order_item_quantity > 0 " .
				  "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
				  "   and order_item_order = " . $project["project_order"] . 
				  "   and order_item_supplier_address > 0 " . 
		"order by address_company";

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
	$involved_suppliers[$row["order_item_supplier_address"]] = $row["address_company"];
}

if(count($involved_suppliers) > 0)
{
	$involved_suppliers['nosupplier'] = "No specific supplier";
}

//check if user is one of the suppliers
$user_is_a_supplier = false;
if(array_key_exists($user["address"], $involved_suppliers))
{
	$user_is_a_supplier = true;
}


//compose invisible div for shipping documents selector
$former_attachment = array();

$sql = "select * " . 
	   "from order_files ". 
	   " where order_file_id = " . dbquote(id());

$res = mysql_query($sql) or dberror($sql);

if ($row = mysql_fetch_assoc($res))
{
	$former_attachment = $row;
}

$sql_shipping_documents = "select standard_shipping_document_id, standard_shipping_document_name " . 
                          "from address_shipping_documents " . 
						  " left join standard_shipping_documents  on standard_shipping_document_id = address_shipping_document_document_id " . 
						  " where address_shipping_document_address_id = " . dbquote($project["order_client_address"]) . 
						  " order by standard_shipping_document_name";

$shipping_document_categories = array();
$res = mysql_query($sql_shipping_documents) or dberror($sql_shipping_documents);
while ($row = mysql_fetch_assoc($res))
{
	$shipping_document_categories[$row["standard_shipping_document_id"]] = $row["standard_shipping_document_name"];
}

if(count($shipping_document_categories) > 0)
{
	$shipping_document_categories['other'] = "Other";
}

//only allow indicate shipping docs if user is supplier, logistics coordinator or project manager
if($user_is_a_supplier == false
   and !in_array(1, $user_roles)
   and !in_array(2, $user_roles)
   and !in_array(3, $user_roles))
{
	$shipping_document_categories = array();
}



if((count($former_attachment) > 0 and $former_attachment['order_file_category'] == 12)
	or param("order_file_category") == 12)
{
	$shipping_document_category_selector = '<div id="shipping_document_category_selector" style="display:block;">';
}
else
{
	$shipping_document_category_selector = '<div id="shipping_document_category_selector" style="display:none;">';
}




if((count($former_attachment) > 0 and $former_attachment['order_file_category'] == 12)
	or param("order_file_category") == 12)
{
	$shipping_document_category_selector = '<div id="shipping_document_category_selector" style="display:block;">';
}
else
{
	$shipping_document_category_selector = '<div id="shipping_document_category_selector" style="display:none;">';
}


$shipping_document_category_selector .= '<br />Please select the type of the shipping document:<br />';
foreach($shipping_document_categories as $id=>$name)
{
	$checked = "";
	if((count($former_attachment) > 0 and $former_attachment['order_file_standard_shipping_document_id'] == $id) or param("order_file_standard_shipping_document_id") == $id)
	{
		$checked = "checked";
	}
	elseif((count($former_attachment) > 0 and $former_attachment['order_file_standard_shipping_document_id'] == 0) or param("order_file_standard_shipping_document_id") == 'other')
	{
		$checked = "checked";
	}
	$shipping_document_category_selector .= "<input type=\"radio\" name=\"standard_shipping_document_id\" " . $checked . " value=\"" . $id . "\">" . $name . '<br />';
}


if($user_is_a_supplier == false)
{
	$shipping_document_category_selector .= '<br />Please tell us to which supplier this shipping document belongs:<br />';
	foreach($involved_suppliers as $id=>$name)
	{
		$checked = "";
		if((count($former_attachment) > 0 and $former_attachment['order_file_supplier_id'] == $id) or param("order_file_supplier_id") == $id)
		{
			$checked = "checked";
		}
		elseif((count($former_attachment) > 0 and $former_attachment['order_file_supplier_id'] == 0) or param("order_file_supplier_id") == 'nosupplier')
		{
			$checked = "checked";
		}
		$shipping_document_category_selector .= "<input type=\"radio\" name=\"standard_shipping_supplier_id\" " . $checked . " value=\"" . $id . "\">" . $name . '<br />';
	}
}

$shipping_document_category_selector .= '<br /></div>';


//get attachment data
$order_file_standard_shipping_document_id = 0;
$file_category = 0;
$sql = "select order_file_standard_shipping_document_id, order_file_category " . 
		   "from order_files ". 
		   " where order_file_id = " . dbquote(id());

$res = mysql_query($sql) or dberror($sql);

if ($row = mysql_fetch_assoc($res))
{
	$shipping_document_category_id = $row["order_file_standard_shipping_document_id"];
	$file_category = $row["order_file_category"];
}


// checkbox names
$check_box_names = array();


// get file owner
$owner = get_file_owner(id());


/********************************************************************
    Create Form
*********************************************************************/ 


$form = new Form("order_files", "file");


$form->add_hidden("pid", param("pid"));
$form->add_hidden("order_file_order", $project["project_order"]);
$form->add_hidden("order_file_owner", user_id());


require_once "include/project_head_small.php";


$form->add_section("Attachment");
$form->add_list("order_file_category", "Category*", $sql_attachment_categories, NOTNULL);

if(count($shipping_document_categories) > 0)
{
	$form->add_label("shipping_document_category", "", RENDER_HTML, $shipping_document_category_selector);
}

$form->add_hidden("order_file_standard_shipping_document_id",0);
$form->add_hidden("order_file_supplier_id",0);


$form->add_edit("order_file_title", "Title*", NOTNULL, "", TYPE_CHAR);
$form->add_multiline("order_file_description", "Description*", 4);


if (has_access("can_set_attachment_accessibility_in_projects"))
{
    $form->add_section("Accessibility");
    $form->add_comment("Please indicate who is allowed to view this attachment.");


    $num_checkboxes=1;
    foreach ($companies as $key=>$value_array)
    {
        if(!array_key_exists('role', $value_array)) {
			$value_array["role"] = "";
		}
		$form->add_checkbox("A" . $num_checkboxes, $value_array["name"], $value_array["access"], 0, $value_array["role"]);
        $check_box_names["A" . $num_checkboxes] = $value_array["user"];
        $num_checkboxes++;
    }

	$form->add_comment("Please indicate accessibility for potential suppliers.");
	$form->add_list("supplier1", "Supplier 1", $sql_suppliers, 0, $supplier1);
	$form->add_list("supplier2", "Supplier 2", $sql_suppliers, 0, $supplier2);
	$form->add_list("supplier3", "Supplier 3", $sql_suppliers, 0, $supplier3);
	$form->add_list("supplier4", "Supplier 4", $sql_suppliers, 0, $supplier4);
	$form->add_list("supplier5", "Supplier 5", $sql_suppliers, 0, $supplier5);
	$form->add_list("supplier6", "Supplier 6", $sql_suppliers, 0, $supplier6);

}

$order_number = $project["order_number"];


$form->add_section("File");
$form->add_hidden("order_file_type");
$form->add_upload("order_file_path", "File", "/files/orders/$order_number", NOTNULL);

//$form->add_section("Layout Options");
//s$form->add_checkbox("order_file_is_final_layout", "Layout signed by Tissot HQ management", "", 0, "Layout");
$form->add_hidden("order_file_is_final_layout", 0);


if(in_array(5, $user_roles) or in_array(6, $user_roles) or in_array(7, $user_roles))
{
	$form->add_hidden("order_file_attach_to_cer_booklet", 0);
}
else
{
	$form->add_section("CER/AF Booklet Options");
	$form->add_checkbox("order_file_attach_to_cer_booklet", "Attach this file to the AF/CER booklet", "", 0, "AF/CER Booklet");
}



$form->add_section("CC Recipients");
$form->add_modal_selector("ccmails", "Selected Recipients", 8);

/*
if(has_access("can_edit_catalog")) {
	$form->add_section("");
	$form->add_checkbox("donotsendmail", "Do NOT send mail upon changement", 0, 0, "Send Mail Options");
	$form->add_checkbox("sendonlytocc", "... but send notification to CC Recipients", 0, 0, "");
	$form->add_checkbox("sendonlytosupp", "... but send notification to indicated suppliers", 0, 0, "");
	$form->add_checkbox("sendonlytopm", "... but send notification to the project leader", 0, 0, "");
}
else
{
	$form->add_hidden("donotsendmail", 0);
	$form->add_hidden("sendonlytocc", 0);
	$form->add_hidden("sendonlytosupp", 0);
	$form->add_hidden("sendonlytopm", 0);
}
*/
$form->add_section("");
$form->add_checkbox("donotsendmail", "Do NOT send mail upon changement", 0, 0, "Send Mail Options");
$form->add_checkbox("sendonlytocc", "... but send notification to CC Recipients", 0, 0, "");
$form->add_checkbox("sendonlytosupp", "... but send notification to indicated suppliers", 0, 0, "");
$form->add_checkbox("sendonlytopm", "... but send notification to the project leader", 0, 0, "");


$form->add_button("save", "Save");


if(has_access("can_delete_attachment_in_projects")  or $owner == user_id())
{
    $form->add_button("delete", "Delete");
}

$form->add_button(FORM_BUTTON_BACK, "Back");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


if ($form->button("save"))
{
	if($form->value("order_file_category") != 8) //layout
	{
		$form->value("order_file_is_final_layout", 0);
	}

	if(count($shipping_document_categories) > 0
		and $form->value("order_file_category") == 12) // shipping document
	{
		if($form->value("order_file_standard_shipping_document_id") == 0
		   or $form->value("order_file_supplier_id") == 0)
		{
			//no validation
		}
		else
		{
			$form->add_validation("{order_file_standard_shipping_document_id} > 0", "You must indicate the type of the shipping document.");
			$form->add_validation("{order_file_supplier_id} > 0", "A shipping document must always be related to a supplier.");
		}
	}

	//get file_type
	$file_type_is_valid = false;
	$path = $form->value("order_file_path");
	$ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));

	$sql = "select file_type_id from file_types " . 
		   " where file_type_extension like '%" . $ext ."%'";
	
    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
		$form->value("order_file_type", $row["file_type_id"]);
		$file_type_is_valid = true;
	}


	if($file_type_is_valid == false)
	{
		$form->error("The file type of the file you try to upload has an extension not allowed to be uploaded!");
	}
	elseif($form->validate())
	{

		$form->save();
		
		// check if a recipient was selected
		if (has_access("can_set_attachment_accessibility_in_projects"))
		{
			$no_recipient = 1;
		}
		else
		{
			$no_recipient = 0;
		}


		foreach ($form->items as $item)
		{
			if ($item["type"] == "checkbox" and $item["value"] == "1")
			{
				$no_recipient = 0;
			}
		}

		$invalid_file_extension = false;
		
		if($form->value("order_file_category") == 11)
		{
			$ext = pathinfo($form->value("order_file_path"), PATHINFO_EXTENSION);
			
			if($ext != 'jpg' and $ext != 'JPG' and $ext != 'jpeg' and $ext != 'JPEG')
			{
				$invalid_file_extension = true;
			}
		}

		
		if($form->value("sendonlytocc") == 1
			or $form->value("sendonlytosupp") == 1
			or $form->value("sendonlytopm") == 1
			) //send attachment infor only to cc mails, suppliers or project Leader
		{
			update_attachment_accessibility_info(id(), $form,  $check_box_names);

			// send email notifocation to the retail staff
			$order_id = $project["project_order"];


			$sql = "select order_id, order_number, " .
				   "order_shop_address_company, order_shop_address_place, " .
				   "project_retail_coordinator, country_name, ".
				   "users.user_email as recepient1, users.user_address as address_id1, " .
				   "users2.user_email as recepient2, users2.user_address as address_id2, " .
				   "users1.user_email as sender, concat(users1.user_name, ' ', users1.user_firstname) as user_fullname ".
				   "from orders ".
				   "left join projects on project_order = " . $order_id . " " .
				   "left join countries on country_id = order_shop_address_country " .
				   "left join users on order_retail_operator = users.user_id ".
				   "left join users as users1 on " . user_id() . "= users1.user_id ".
				   "left join users as users2 on project_retail_coordinator = users2.user_id ".
				   "where order_id = " . $order_id;


			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res) and $row["recepient2"])
			{
				$project_manager_id = $row["project_retail_coordinator"];
				
				$subject = MAIL_SUBJECT_PREFIX . ": Attachment was updated - Project " . $row["order_number"] . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];


				$sender_email = $row["sender"];
				$sender_name =  $row["user_fullname"];


				$mail = new PHPMailer();
				$mail->Subject = $subject;

				$mail->SetFrom($sender_email, $sender_name);
				$mail->AddReplyTo($sender_email, $sender_name);

				
				$bodytext0 = "An attachment was updated by " . $sender_name . ":";
				$bodytext1 = "An attachment was updated by " . $sender_name . " for:";


				$recipeint_added = 0;
				$reciepients = array();
				
				//get all ccmails
				
				if($form->value("sendonlytocc") == 1)
				{
					$ccmails =  explode(' ', preg_replace("'\r?\n'"," ",$form->value('ccmails'))); 
					foreach($ccmails as $ccmail) {
						if(is_email_address($ccmail)) {
							
							$reciepients[strtolower($ccmail)] = strtolower($ccmail);
							$recipeint_added = 1;
						}
					
					}
				}


				if($form->value("sendonlytosupp") == 1)
				{
					if($form->value("supplier1") > 0) {
						$sql = "select user_email, user_email_cc, user_email_deputy ".
							   "from users ".
							   "where (user_address = " . $form->value("supplier1") . 
							   "   and user_active = 1)";
						
						$res1 = mysql_query($sql) or dberror($sql);
						while ($row1 = mysql_fetch_assoc($res1))
						{
							$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
							$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
							$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
						}
					}

					if($form->value("supplier2") > 0) {
						$sql = "select user_email, user_email_cc, user_email_deputy ".
							   "from users ".
							   "where (user_address = " . $form->value("supplier2") . 
							   "   and user_active = 1)";
						
						$res1 = mysql_query($sql) or dberror($sql);
						while ($row1 = mysql_fetch_assoc($res1))
						{
							$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
							$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
							$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
						}
					}

					if($form->value("supplier3") > 0) {
						$sql = "select user_email, user_email_cc, user_email_deputy ".
							   "from users ".
							   "where (user_address = " . $form->value("supplier3") . 
							   "   and user_active = 1)";
						
						$res1 = mysql_query($sql) or dberror($sql);
						while ($row1 = mysql_fetch_assoc($res1))
						{
							$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
							$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
							$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
						}
					}

					if($form->value("supplier4") > 0) {
						$sql = "select user_email, user_email_cc, user_email_deputy ".
							   "from users ".
							   "where (user_address = " . $form->value("supplier4") . 
							   "   and user_active = 1)";
						
						$res1 = mysql_query($sql) or dberror($sql);
						while ($row1 = mysql_fetch_assoc($res1))
						{
							$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
							$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
							$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
						}
					}

					if($form->value("supplier5") > 0) {
						$sql = "select user_email, user_email_cc, user_email_deputy ".
							   "from users ".
							   "where (user_address = " . $form->value("supplier5") . 
							   "   and user_active = 1)";
						
						$res1 = mysql_query($sql) or dberror($sql);
						while ($row1 = mysql_fetch_assoc($res1))
						{
							$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
							$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
							$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
						}
					}

					if($form->value("supplier6") > 0) {
						$sql = "select user_email, user_email_cc, user_email_deputy ".
							   "from users ".
							   "where (user_address = " . $form->value("supplier6") . 
							   "   and user_active = 1)";
						
						$res1 = mysql_query($sql) or dberror($sql);
						while ($row1 = mysql_fetch_assoc($res1))
						{
							$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
							$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
							$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
						}
					}
				}

				if($form->value("sendonlytopm") == 1 and $project_manager_id > 0)
				{
					$sql = "select user_email, user_email_cc, user_email_deputy ".
						   "from users ".
						   "where (user_id = " . $project_manager_id . 
						   "   and user_active = 1)";
								
					$res1 = mysql_query($sql) or dberror($sql);
					if ($row1 = mysql_fetch_assoc($res1))
					{
						$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
						$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
						$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
						$recipeint_added = 1;
					}
				}

				if($recipeint_added == 1)
				{
					foreach($reciepients as $key=>$email)
					{
						$bodytext1 = $bodytext1 . "\n" . $email;
						$mail->AddAddress($email);
					}
				}
				

				$bodytext0.= "\n\n<--\n";
				$bodytext0.= $form->value("order_file_title") . "\n";
				
				
				if($form->value("order_file_description"))
				{
					$bodytext0.= $form->value("order_file_description") . "\n";
				}
				/*
				$bodytext0.= "download the file here:\n" . APPLICATION_URL . $form->value("order_file_path");
				$bodytext0.= "\n-->";
				*/

				if($form->value("order_file_path"))
				{
					$filepath = $_SERVER["DOCUMENT_ROOT"] . $form->value("order_file_path");
					
					//7MB plus 30% overhead base64-ecoded = 9.1 MB, 10MB Restriction from Mail server
					if(filesize ( $filepath ) < 7340032 
						and must_attach_file_to_mail($form->value("order_file_category")) == 1)
					{
						$mail->AddAttachment($filepath);
					}
				}

				$link ="project_view_attachment.php?pid=" . param("pid") . "&id=" .  id();
				$bodytext = $bodytext0 . "\n\nclick below to have direct access to the project:\n";
				$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
				$mail->Body = $bodytext;
				
				if($recipeint_added == 1)
				{
					if($senmail_activated == true)
					{
						$mail->send();
					}


					$bodytext1.= "\n\n<--\n";
					$bodytext1.= $form->value("order_file_title") . "\n";
					$bodytext1.= "-->";

					$bodytext = $bodytext1 . "\n\nclick below to have direct access to the project:\n";
					$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";   

					append_mail($project["project_order"], "" , user_id(), $bodytext, "", 1);
				}

				
			}

			$link = "project_view_attachments.php?pid=" . param("pid");
			redirect($link);
		}

		if ($form->validate() and $no_recipient == 0 and $invalid_file_extension == false)
		{
			update_attachment_accessibility_info(id(), $form,  $check_box_names);

			// send email notifocation to the retail staff
			$order_id = $project["project_order"];


			$sql = "select order_id, order_number, " .
				   "order_shop_address_company, order_shop_address_place, " .
				   "project_retail_coordinator, country_name, ".
				   "users.user_email as recepient1, users.user_address as address_id1, " .
				   "users2.user_email as recepient2, users2.user_address as address_id2, " .
				   "users1.user_email as sender, concat(users1.user_name, ' ', users1.user_firstname) as user_fullname ".
				   "from orders ".
				   "left join projects on project_order = " . $order_id . " " .
				   "left join countries on country_id = order_shop_address_country " .
				   "left join users on order_retail_operator = users.user_id ".
				   "left join users as users1 on " . user_id() . "= users1.user_id ".
				   "left join users as users2 on project_retail_coordinator = users2.user_id ".
				   "where order_id = " . $order_id;

			$res = mysql_query($sql) or dberror($sql);
			if ($form->value("donotsendmail") == 0 and $row = mysql_fetch_assoc($res) and $row["recepient2"])
			{
				$subject = MAIL_SUBJECT_PREFIX . ": New attachment was added - Project " . $row["order_number"] . ", " . $row["country_name"]  . ", " . $row["order_shop_address_company"];


				$sender_email = $row["sender"];
				$sender_name =  $row["user_fullname"];


				$mail = new PHPMailer();
				$mail->Subject = $subject;

				$mail->SetFrom($sender_email, $sender_name);
				$mail->AddReplyTo($sender_email, $sender_name);


				$bodytext0 = "An attachment has been updated by " . $sender_name . ":";
				$bodytext1 = "An attachment has been updated by " . $sender_name . " for:";


				$recipeint_added = 0;
				$reciepients = array();
				$reciepient_ids = array();
				$reciepients_cc = array();
				$reciepients_dp = array();

				// remove setting of defaul recipients
				/*
				if($row["recepient1"])
				{
					$reciepients[strtolower($row["recepient1"])] = strtolower($row["recepient1"]);
				
				}
				if($row["recepient2"])
				{
					$reciepients[strtolower($row["recepient2"])] = strtolower($row["recepient2"]);
				}
				*/

				foreach ($check_box_names as $key=>$value)
				{
					if ($form->value($key) and !in_array($value, $old_recipients))
					{
						foreach($companies as $key1=>$company)
						{
							
							if($value == $company["user"])
							{
								$sql = "select user_email, user_email_cc, user_email_deputy ".
									   "from users ".
									   "where (user_id = " . $company["user"] . 
									   "   and user_active = 1)";
								
								$res1 = mysql_query($sql) or dberror($sql);
								if ($row1 = mysql_fetch_assoc($res1))
								{
									$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
									$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
									$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
									$recipeint_added = 1;

									$reciepient_ids[$company["user"]] = $company["user"];

								}
							}
						}
					}
				}
				


				//get all ccmails
				$ccmails =  explode(' ', preg_replace("'\r?\n'"," ",$form->value('ccmails'))); 
				foreach($ccmails as $ccmail) {
					if(is_email_address($ccmail)) {
						
						$reciepients_cc[strtolower($ccmail)] = strtolower($ccmail);
					}
				
				}


				//get potential suppliers
				if($form->value("supplier1") > 0) {
					$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
						   "from users ".
						   "where (user_address = " . $form->value("supplier1") . 
						   "   and user_active = 1)";
					
					$res1 = mysql_query($sql) or dberror($sql);
					while ($row1 = mysql_fetch_assoc($res1))
					{
						$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
						$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
						$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);
						$reciepient_ids[$row1["user_id"]] = $row1["user_id"];
					}
				}

				if($form->value("supplier2") > 0) {
					$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
						   "from users ".
						   "where (user_address = " . $form->value("supplier2") . 
						   "   and user_active = 1)";
					
					$res1 = mysql_query($sql) or dberror($sql);
					while ($row1 = mysql_fetch_assoc($res1))
					{
						$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
						$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
						$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);

						$reciepient_ids[$row1["user_id"]] = $row1["user_id"];
					}
				}

				if($form->value("supplier3") > 0) {
					$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
						   "from users ".
						   "where (user_address = " . $form->value("supplier3") . 
						   "   and user_active = 1)";
					
					$res1 = mysql_query($sql) or dberror($sql);
					while ($row1 = mysql_fetch_assoc($res1))
					{
						$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
						$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
						$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);

						$reciepient_ids[$row1["user_id"]] = $row1["user_id"];
					}
				}

				if($form->value("supplier4") > 0) {
					$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
						   "from users ".
						   "where (user_address = " . $form->value("supplier4") . 
						   "   and user_active = 1)";
					
					$res1 = mysql_query($sql) or dberror($sql);
					while ($row1 = mysql_fetch_assoc($res1))
					{
						$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
						$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
						$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);

						$reciepient_ids[$row1["user_id"]] = $row1["user_id"];
					}
				}

				if($form->value("supplier5") > 0) {
					$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
						   "from users ".
						   "where (user_address = " . $form->value("supplier5") . 
						   "   and user_active = 1)";
					
					$res1 = mysql_query($sql) or dberror($sql);
					while ($row1 = mysql_fetch_assoc($res1))
					{
						$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
						$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
						$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);

						$reciepient_ids[$row1["user_id"]] = $row1["user_id"];
					}
				}

				if($form->value("supplier6") > 0) {
					$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
						   "from users ".
						   "where (user_address = " . $form->value("supplier6") . 
						   "   and user_active = 1)";
					
					$res1 = mysql_query($sql) or dberror($sql);
					while ($row1 = mysql_fetch_assoc($res1))
					{
						$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
						$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
						$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);

						$reciepient_ids[$row1["user_id"]] = $row1["user_id"];
					}
				}

				
				if($recipeint_added == 1)
				{
					foreach($reciepients as $key=>$email)
					{
						$bodytext1 = $bodytext1 . "\n" . $email;
						$mail->AddAddress($email);
					}


					foreach($reciepients_cc as $key=>$email)
					{
						$bodytext1 = $bodytext1 . "\n" . $email;
						$mail->AddCC($email);
					}

					foreach($reciepients_dp as $key=>$email)
					{
						$bodytext1 = $bodytext1 . "\n" . $email;
						$mail->AddCC($email);
					}
				}
				else
				{
					foreach($reciepients_cc as $key=>$email)
					{
						$bodytext1 = $bodytext1 . "\n" . $email;
						$mail->AddAddress($email);
						$recipeint_added = 1;
					}
				}

				$bodytext0.= "\n\n<--\n";
				$bodytext0.= $form->value("order_file_title") . "\n";
				
				
				if($form->value("order_file_description"))
				{
					$bodytext0.= $form->value("order_file_description") . "\n";
				}
				
				$link ="project_view_attachment.php?pid=" . param("pid") . "&id=" .  id();
				$bodytext = $bodytext0 . "\n\nclick below to have direct access to the project:\n";
				$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";   
					   
				$mail->Body = $bodytext;


				if($form->value("order_file_path"))
				{
					$filepath = $_SERVER["DOCUMENT_ROOT"] . $form->value("order_file_path");
					//7MB plus 30% overhead base64-ecoded = 9.1 MB, 10MB Restriction from Mail server
					if(filesize ( $filepath ) < 7340032 
						and must_attach_file_to_mail($form->value("order_file_category")) == 1)
					{
						$mail->AddAttachment($filepath);
					}
				}


				if($senmail_activated == true)
				{
					$mail->send();
				}


				$bodytext1.= "\n\n<--\n";
				$bodytext1.= $form->value("order_file_title") . "\n";
				$bodytext1.= "-->";

				$bodytext = $bodytext1 . "\n\nclick below to have direct access to the project:\n";
				$bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";   

				if($recipeint_added == 1)
				{
					
					foreach($reciepient_ids as $key=>$user_id)
					{
						append_mail($project["project_order"], $user_id , user_id(), $bodytext, "", 1, 0, 0, 'order_files', id());
					}
										
								
					foreach($reciepients_cc as $key=>$email)
					{
						$sql = "select user_id from users " . 
							   " where LOWER(user_email) = " . dbquote($email);
						
						$res = mysql_query($sql) or dberror($sql);
						if ($row = mysql_fetch_assoc($res))
						{
							append_mail($project["project_order"], $row["user_id"] , user_id(), $bodytext, "", 1, 0, 1, 'order_files', id());
						}
					}

					foreach($reciepients_dp as $key=>$email)
					{
						$sql = "select user_id from users " . 
							   " where LOWER(user_email) = " . dbquote($email);
						
						$res = mysql_query($sql) or dberror($sql);
						if ($row = mysql_fetch_assoc($res))
						{
							append_mail($project["project_order"], $row["user_id"] , user_id(), $bodytext, "", 1, 0, 1, 'order_files', id());
						}
					}
				}
			}
			$link = "project_view_attachments.php?pid=" . param("pid");
			redirect($link);
		}
		else
		{
			if($invalid_file_extension == true)
			{
				$form->error("Please upload only files of the type 'jpg'.");
			}
			else
			{
				$form->error("Please select a least one person to have access to the attachment.");
			}
		}
	}
}
elseif ($form->button("delete"))
{
    delete_attachment(id());
    $link = "project_view_attachments.php?pid=" . param("pid");
    redirect($link);
}




$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Edit Attachment Data");
$form->render();

?>


<script language="Javascript">

	$(document).ready(function(){

		$("#order_file_category").change(function(){
		
			if( $('#shipping_document_category_selector').is(':hidden') ) 
			{
				if($("#order_file_category").val() == 12)
				{
					$("#shipping_document_category_selector").show();
				}
			}
			else
			{
				$("#shipping_document_category_selector").hide();
				$('input[name="standard_shipping_document_id"]').attr('checked', false);
				$("#order_file_standard_shipping_document_id").val(0);

				$('input[name="standard_shipping_supplier_id"]').attr('checked', false);
				$("#order_file_supplier_id").val(0);
			}
		});

		$("input[name='standard_shipping_document_id']").change(function(){
			
			var selectedVal = 0;
			var selected = $("input[type='radio'][name='standard_shipping_document_id']:checked");
			if (selected.length > 0) {
				selectedVal = selected.val();
			}
			
			$("#order_file_standard_shipping_document_id").val(selectedVal);
		});

		
		<?php
		if($user_is_a_supplier == false)
		{
		?>
			$("input[name='standard_shipping_supplier_id']").change(function(){
				
				var selectedVal = 0;
				var selected = $("input[type='radio'][name='standard_shipping_supplier_id']:checked");
				if (selected.length > 0) {
					selectedVal = selected.val();
				}
				$("#order_file_supplier_id").val(selectedVal);
			});
		<?php
		}
		else
		{
		?>
			$("#order_file_supplier_id").val(<?php echo $user["address"];?>);
		<?php
		}
		?>

	});

</script>


<script type="text/javascript">
  jQuery(document).ready(function($) {
  $('#ccmails_selector').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/shared/select_mail_recipients.php'
    });
    return false;
  });
});
</script>

<?php

require_once "include/project_footer_logistic_state.php";
$page->footer();


?>