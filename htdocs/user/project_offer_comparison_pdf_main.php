<?php
/********************************************************************

    project_offer_comparison_pdf_main.php

    Print empty Offer form for Local Construction Work

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-09
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-09
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/

/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());

// read project and order details
$project = get_project(param("pid"));


// get orders's currency
$order_currency = get_order_currency($project["project_order"]);

$page_title = "Contractor Cost Comparison for Local Works - Project " . $project["project_number"] . " / " . date("d.m.Y");

global $page_footer;
$page_footer = "Contractor Cost Comparison for Local Works - Project " . $project["project_number"];

$shop = $project["order_shop_address_company"] . ", " .
        $project["order_shop_address_zip"] . " " .
        $project["order_shop_address_place"] . ", " .
        $project["order_shop_address_country_name"];


$order_state_name = get_actual_order_state_name($project["order_actual_order_state_code"], 1);
$retail_coordinator = get_user($project["project_retail_coordinator"]);
$retail_operator = get_user($project["order_retail_operator"]);


$invoice_address = "";

$billing_address_province_name = "";
if($project["order_billing_address_place_id"])
{
	$billing_address_province_name = get_province_name($project["order_billing_address_place_id"]);
}

$invoice_address = $invoice_address . $project["order_billing_address_company"];
if ($project["order_billing_address_company2"])
{
    $invoice_address = $invoice_address . ", " . $project["order_billing_address_company2"];
}

$invoice_address = $invoice_address . ", " . $project["order_billing_address_address"];

if ($project["order_billing_address_address2"])
{
    $invoice_address = $invoice_address . ", " . $project["order_billing_address_address2"];
}

$invoice_address = $invoice_address . ", " . $project["order_billing_address_zip"] . " " . $project["order_billing_address_place"];

if($billing_address_province_name) {
	$invoice_address .= ', ' . $billing_address_province_name;
}
$invoice_address = $invoice_address . ", " . $project["order_billing_address_country_name"];
$invoice_address = $invoice_address . ", Phone: " . $project["order_billing_address_phone"];

// get company's address
$client_address = get_address($project["order_client_address"]);
$client = $client_address["company"] . ", " .
          $client_address["zip"] . " " . $client_address["place"] . ", " .
          $client_address["country_name"];


// prepare output fields
$captions1 = array();
$captions1[] = "Project Number:";
$captions1[] = "Product Line / Type:";
$captions1[] = "Project Starting / Agreed Opening Date:";
$captions1[] = "Status:";
$captions1[] = "Project Leader / Logistics Coordinator:";
$captions1[] = "";
$captions1[] = "";

$data1 = array();
$data1[] = $project["project_number"];
$data1[] = $project["product_line_name"] . " / " . $project["postype_name"];
$data1[] = to_system_date($project["order_date"]) . " / " .  to_system_date($project["project_real_opening_date"]);
$data1[] = $project["order_actual_order_state_code"] . " " . $order_state_name;
$data1[] = $retail_coordinator["firstname"] . " " . $retail_coordinator["name"] . " / " . $retail_operator["firstname"] . " " . $retail_operator["name"];
$data1[] = "";
$data1[] = "";

$captions3 = array();
$data3 = array();

$captions3[] = "Client:";
$captions3[] = "Shop:";
//$captions3[] = "Bill to:";

$data3[] = $client;
$data3[] = $shop;
//$data3[] = $invoice_address;


//build group totals for each offer
$offers = array();
$offertotals = array();
$smalltotals = array();
$grandtotals = array();
$grandtotals_chf = array();
$currencies = array();
$remarks = array();
$sql = "select * from lwoffers " . 
       "where lwoffer_order = " . $project["project_order"] .
       " order by lwoffer_id";

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
    $offers[$row["lwoffer_id"]] = $row["lwoffer_company"];
    $grandtotals[$row["lwoffer_id"]] = 0;
	$grandtotals_chf[$row["lwoffer_id"]] = 0;
    $smalltotals[$row["lwoffer_id"]] = 0;
	$remarks[$row["lwoffer_id"]] = $row["lwoffer_remark"];

}

$offergroups = array();
//build group_Totals fo each offer
foreach($offers as $key=>$value)
{
    
	// Changes 2008
	// reduction to groups, no more editing of single offer positions
	// leave the position editing in case it will be used/activated at a leter time

	//Offer Positions
	//build totals
	/*
	$sql = "select lwoffer_id, lwofferposition_lwoffergroup, lwoffergroup_code, lwoffergroup_group, " .
           "concat(lwoffergroup_code,  ' ', lwoffergroup_text) as lwoffergroup, " . 
           "sum(lwofferposition_numofunits*lwofferposition_price) as total, currency_symbol " .
           "from lwoffers " .
           "left join lwoffergroups on lwoffergroup_offer = lwoffer_id " . 
           "left join lwofferpositions on lwofferposition_lwoffergroup = lwoffergroup_id " .
           "left join currencies on lwoffer_currency = currency_id " . 
           "where lwoffer_id = " . $key .
           " GROUP BY lwoffergroup_group, lwoffergroup_code, " . 
           " concat(lwoffergroup_code,  ' ', lwoffergroup_text), lwofferposition_lwoffergroup, currency_symbol " . 
           "order by lwoffergroup_group";


	*/

	//Offer Groups
	//build totals
	$sql = "select lwoffer_id, lwoffergroup_group, lwoffergroup_code, lwoffergroup_group, " .
           "concat(lwoffergroup_code,  ' ', lwoffergroup_text) as lwoffergroup, lwoffergroup_discount, " . 
           "sum(lwoffergroup_price) as total, currency_symbol, lwoffer_exchange_rate, lwoffer_factor, lwoffergroup_hasoffer " . 
           "from lwoffers " .
           "left join lwoffergroups on lwoffergroup_offer = lwoffer_id " . 
           "left join currencies on lwoffer_currency = currency_id " .
           "where lwoffer_id = " . $key .
           " GROUP BY lwoffergroup_group, lwoffergroup_code, " . 
           " concat(lwoffergroup_code,  ' ', lwoffergroup_text), currency_symbol, lwoffergroup_hasoffer " . 
           "order by lwoffergroup_code, lwoffergroup_group";

	$totals = array();
    $res = mysql_query($sql) or dberror($sql);
    while ($row = mysql_fetch_assoc($res))
    {
		$dicount_rate = $row["lwoffergroup_discount"]/100;
		
		/*
		if($row["lwoffergroup_hasoffer"] == 1 and $row["total"])
        {
            $totals[$row["lwoffergroup_code"]] = $row["total"];
        }
        else
        {
            $totals[$row["lwoffergroup_code"]] = "";
        }
		*/
		$totals[$row["lwoffergroup_code"]] = $row["total"] - $row["total"]*$dicount_rate;
		
        /*
		if($row["lwoffergroup_hasoffer"] == 1 and $row["lwoffergroup_group"] < 8)
        {
            $smalltotals[$key] = $smalltotals[$key] + $row["total"];
        }

        if($row["lwoffergroup_hasoffer"] == 1)
        {
			$grandtotals[$key] = $grandtotals[$key] + $row["total"];
		}
		*/
		
		if($row["lwoffergroup_code"] != "13.00")
        {
			$smalltotals[$key] = $smalltotals[$key] + $row["total"] - $row["total"]*$dicount_rate;
        }
		else
		{
			//echo $row["lwoffergroup_code"];
			//abc();
		}

		$grandtotals[$key] = $grandtotals[$key] + ($row["total"]  - $row["total"]*$dicount_rate);

		$factor = $row["lwoffer_factor"];
		if($factor  == 0){ 
			$factor  = 1;
		}
		$grandtotals_chf[$key] = $grandtotals_chf[$key] + ($row["lwoffer_exchange_rate"] * ($row["total"]  - $row["total"]*$dicount_rate))/$factor ;


        $currencies[$key] = $row["currency_symbol"];
        $offergroups[$row["lwoffergroup_code"]] = $row["lwoffergroup"];

    }

    $offertotals[$key] = $totals;
}



//sort offers by totals
$tmp_offers = array();
$tmp_offertotals = array();
$tmp_grandtotals_chf = array();
$tmp_smalltotals = array();
$tmp_remarks = array();
$tmp_currencies = array();



asort($grandtotals);
foreach($grandtotals as $key=>$values)
{
	$tmp_offers[$key] = $offers[$key];
	$tmp_offertotals[$key] =  $offertotals[$key];
	$tmp_grandtotals_chf[$key] =  $grandtotals_chf[$key];
	$tmp_smalltotals[$key] = $smalltotals[$key];
	$tmp_remarks[$key] = $remarks[$key];
	$tmp_offertotals[$key] = $offertotals[$key];
	$tmp_currencies[$key] = $currencies[$key];
}

$offers = $tmp_offers;
$offertotals =  $tmp_offertotals;
$grandtotals_chf =  $tmp_grandtotals_chf;
$smalltotals = $tmp_smalltotals;
$remarks = $tmp_remarks;
$offertotals = $tmp_offertotals;
$currencies = $tmp_currencies;

//calculate shares
$offer_shares = array();
$first_offer = true;
$first_amount = 0;
foreach($grandtotals as $key=>$amount)
{
	if($first_offer == true)
	{
		
		$offer_shares[$key] = '100';
		$first_offer = false;
		$first_amount = $amount;
	}
	else
	{
		if($first_amount > 0)
		{
			$offer_shares[$key] = round(100*$amount/$first_amount, 0);
		}
		else
		{
			$offer_shares[$key] = "";
		}
	}
}

/********************************************************************
    prepare pdf
*********************************************************************/


$pdf->SetLineWidth(0.1);


//split pages
$pages = array();
$p_offers = array();
$p_offertotals = array();
$p_smalltotals = array();
$p_grandtotals = array();
$p_grandtotals_chf = array();
$p_currencies = array();
$p_remarks = array();

$i=1;
$page = 1;
foreach($offers as $key=>$value)
{
	if($i < 5 and $page == 1)
	{
		$p_offers[$key] = $offers[$key];
		$p_offertotals[$key] = $offertotals[$key];
		$p_smalltotals[$key] = $smalltotals[$key];
		$p_grandtotals[$key] = $grandtotals[$key];
		$p_grandtotals_chf[$key] = $grandtotals_chf[$key];
		$p_currencies[$key] = $currencies[$key];
		$p_remarks[$key] = $remarks[$key];
		$i++;
	}
	elseif($i < 4 and $page > 1)
	{
		$p_offers[$key] = $offers[$key];
		$p_offertotals[$key] = $offertotals[$key];
		$p_smalltotals[$key] = $smalltotals[$key];
		$p_grandtotals[$key] = $grandtotals[$key];
		$p_grandtotals_chf[$key] = $grandtotals_chf[$key];
		$p_currencies[$key] = $currencies[$key];
		$p_remarks[$key] = $remarks[$key];
		$i++;
	}
	else
	{
		$i = 1;
		$pages[$page]["offers"] = $p_offers;
		$pages[$page]["offertotals"] = $p_offertotals;
		$pages[$page]["smalltotals"] = $p_smalltotals;
		$pages[$page]["grandtotals"] = $p_grandtotals;
		$pages[$page]["grandtotals_chf"] = $p_grandtotals_chf;
		$pages[$page]["currencies"] = $p_currencies;
		$pages[$page]["remarks"] = $p_remarks;


		$p_offers = array();
		$p_offertotals = array();
		$p_smalltotals = array();
		$p_grandtotals = array();
		$p_grandtotals_chf = array();
		$p_currencies = array();
		$p_remarks = array();
		$page++;

		$p_offers[$key] = $offers[$key];
		$p_offertotals[$key] = $offertotals[$key];
		$p_smalltotals[$key] = $smalltotals[$key];
		$p_grandtotals[$key] = $grandtotals[$key];
		$p_grandtotals_chf[$key] = $grandtotals_chf[$key];
		$p_currencies[$key] = $currencies[$key];
		$p_remarks[$key] = $remarks[$key];
	
	}
}

$pages[$page]["offers"] = $p_offers;
$pages[$page]["offertotals"] = $p_offertotals;
$pages[$page]["smalltotals"] = $p_smalltotals;
$pages[$page]["grandtotals"] = $p_grandtotals;
$pages[$page]["grandtotals_chf"] = $p_grandtotals_chf;
$pages[$page]["currencies"] = $p_currencies;
$pages[$page]["remarks"] = $p_remarks;

foreach($pages as $page=>$content)
{
	$offers = $pages[$page]["offers"];
	$offertotals = $pages[$page]["offertotals"];
	$smalltotals = $pages[$page]["smalltotals"];
	$grandtotals = $pages[$page]["grandtotals"];
	$grandtotals_chf = $pages[$page]["grandtotals_chf"];
	$currencies = $pages[$page]["currencies"];
	$remarks = $pages[$page]["remarks"];

	include("project_offer_comparison_pdf_page.php");
	include("project_offer_comparison_pdf_detail.php");

}




?>