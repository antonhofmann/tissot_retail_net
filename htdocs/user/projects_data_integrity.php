<?php
/********************************************************************

    projects_data_integrity.php

    Lists different problems in projects.

    Created by:     Anton hofmann (aho@mediaparx.ch)
    Date created:   2013-09-29
    Modified by:    Anton hofmann (aho@mediaparx.ch)
    Date modified:  2013-09-29
    Version:        1.0.0

    Copyright (c) 2013, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

check_access("has_access_to_all_projects");


$sql = "select project_id, country_name, project_number, " . 
       "concat(user_name, ' ', user_firstname) as username " . 
       "from projects " . 
       "left join orders on order_id = project_order " .
	   "left join addresses on address_id = order_client_address " . 
	   "left join countries on country_id = address_country " . 
       "left join users on user_id = project_local_retail_coordinator ";
       

$list_filter = "user_active = 0 and order_actual_order_state_code < '890'";


$list = new ListView($sql);
$list->set_entity("projects");
$list->set_filter($list_filter);
$list->set_order("country_name, project_number");

$list->add_column("country_name", "Country");
$list->add_column("project_number", "Project", "/user/project_edit_retail_data.php?pid={project_id}");
$list->add_column("username", "Local Project Leader");







$page = new Page("projects");

require_once("include/projects_list_page_actions.php");

$page->header();
$page->title("Projects with Invalid Local Project Leader");

$list->render();

$page->footer();

?>
