<?php
/********************************************************************
    project_perform_action_confirmation.php

    confirm upon cancellation

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-06-18
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-06-18
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/order_state_constants.php";
require_once "include/save_functions.php";

check_access("can_use_taskcentre_in_projects");

/********************************************************************
    prepare data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

// append record to table actual_order_states
append_order_state($project["project_order"], param("action"), 1, 1);
set_archive_date($project["project_order"]);

if (param("action") == ORDER_CANCELLED)
{
	

	set_order_to_cancelled($project["project_order"]);

    
    // send email notification for project types
    $sql = "select project_number, project_postype, project_product_line, " .
           "order_shop_address_company, order_shop_address_place, country_name, " .
           "user_firstname, user_name, user_email " . 
		   "from projects " . 
           "left join orders on order_id = project_order " .
           "left join countries on country_id = order_shop_address_country " .
           "left join users on user_id =  " . user_id() .
           "   where project_id = " . $project["project_id"];

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {
        $recipeints = array();
		$project_postype = $row["project_postype"];
		$project_product_line = $row["project_product_line"];
        
        $subject = MAIL_SUBJECT_PREFIX . ": Project was cancelled - Project " . $row["project_number"] . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];

        $sender_email = $row["user_email"];
        $sender_name =  $row["user_name"] . " " . $row["user_firstname"];

		$sender_name2 =  $row["user_firstname"] . " " . $row["user_name"];

        $bodytext0 = "Dear all " . "\r\n";
		$bodytext0 .= "The project " .$row["project_number"] . " was cancelled.";

		if(array_key_exists("reason_for_cancellation", $_SESSION))
		{
			$bodytext0 .= "\r\n" . "The reason is:" . str_replace('manually cancelled', '', $_SESSION["reason_for_cancellation"]);
			$bodytext0 .= "\r\n";
		}

        $mailaddress = 0;

        $mail = new Mail();
        $mail->set_subject($subject);
        $mail->set_sender($sender_email, $sender_name);

        
		$uids = explode('-',$_GET['uids']);

		foreach($uids as $key=>$user_id)
		{
			$sql = "select user_id, user_name, user_firstname, user_email " . 
				   "from users " . 
				   "where user_id = " . dbquote($user_id);

			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$mail->add_recipient($row["user_email"], $row["user_firstname"] . " " . $row["user_name"]);
				$recipeints[] = $user_id;
				$mailaddress = 1;
			}


		}


				
		/*
		if($project["project_product_line"] > 0) 
		{
			$sql = "select postype_notification_email2 from postype_notifications " .
				   "where postype_notification_postype = " . $project_postype . 
				   "  and postype_notification_prodcut_line = " . $project_product_line;
		}
		else
		{
			$sql = "select DISTINCT postype_notification_email2 from postype_notifications " .
				   "where postype_notification_postype = " . $project_postype;
		}

		$res = mysql_query($sql) or dberror($sql);
        while ($row = mysql_fetch_assoc($res))
        {
            if($row["postype_notification_email2"])
            {
                
                $sql_u = "select user_id from users " .
                         "where user_email = '" . $row["postype_notification_email2"] .  "'";
                $res_u = mysql_query($sql_u) or dberror($sql_u);
                if($row_u = mysql_fetch_assoc($res_u))
                {
                    $recepient_id = $row_u["user_id"];
                }
                else
                {
                    $recepient_id = '0';
                }

                $mail->add_recipient($row["postype_notification_email2"]);
				$recipeints[] = $row_u["user_id"];

                $mailaddress = 1;
            }
        }
		*/

       
        $link ="project_view_client_data.php?pid=" . $project["project_id"];
        $bodytext = $bodytext0 . "\nClick below to have direct access to the project:\n";
        $bodytext = $bodytext .  APPLICATION_URL . "/archive/" . $link . "\r\n";
		
		$bodytext .= "Kind regards \r\n" . $sender_name2;
        $mail->add_text($bodytext);
        
        if($mailaddress == 1)
        {
            
			if($senmail_activated == true)
			{
				$mail->send();
			}
            
			foreach($recipeints as $key=>$value)
			{
				append_mail($project["project_order"], $value, user_id(), $bodytext, ORDER_CANCELLED, 1);
			}
        }
    }
}
elseif (param("action") == MOVED_TO_THE_RECORDS)
{
	
	//project tracking
	$field = "project_real_opening_date";
	$sql = "Insert into projecttracking (" . 
		   "projecttracking_user_id, projecttracking_project_id, projecttracking_field, projecttracking_oldvalue, projecttracking_newvalue, projecttracking_comment, projecttracking_time) VALUES (" . 
		   user_id() . ", " . 
		   $project["project_id"] . ", " . 
		   dbquote("order_actual_order_state_code") . ", " . 
		   dbquote($project["order_actual_order_state_code"]) . ", " . 
		   dbquote("890") . ", " . 
		   dbquote("manually archived") . ", " . 
		   dbquote(date("Y-m-d:H:i:s")) . ")"; 
		   
	$result = mysql_query($sql) or dberror($sql);
	
	
	
	// send email notification for project types
    $sql = "select project_number, project_postype, project_product_line, " .
           "order_shop_address_company, order_shop_address_place, country_name, " .
           "user_firstname, user_name, user_email from projects " . 
           "left join orders on order_id = project_order " .
           "left join countries on country_id = order_shop_address_country " .
           "left join users on user_id =  " . user_id() .
           "   where project_id = " . $project["project_id"];

    $res = mysql_query($sql) or dberror($sql);
    if ($row = mysql_fetch_assoc($res))
    {        
        $recipeints = array();
        // send mail to cost manager
        $project_postype = $row["project_postype"];
		$project_product_line = $row["project_product_line"];
        
        $subject = MAIL_SUBJECT_PREFIX . ": Project was put to the records - Project " . $row["project_number"] . ", " . $row["country_name"] . ", " .  $row["order_shop_address_company"];

        $sender_email = $row["user_email"];
        $sender_name =  $row["user_name"] . " " . $row["user_firstname"];

        $bodytext0 = "The project was put to the records by by " . $sender_name;

        $mailaddress = 0;

        $mail = new Mail();
        $mail->set_subject($subject);
        $mail->set_sender($sender_email, $sender_name);

        $sql = "select * from postype_notifications " .
               "where postype_notification_postype = " . dbquote($project_postype) . 
			   "  and postype_notification_prodcut_line = " .  dbquote($project_product_line);

        $res = mysql_query($sql) or dberror($sql);
        while ($row = mysql_fetch_assoc($res))
        {
            if($row["postype_notification_email3"])
            {
                $sql_u = "select user_id from users " .
                         "where user_email = '" . $row["postype_notification_email3"] .  "'";
                
                $res_u = mysql_query($sql_u) or dberror($sql_u);
                
                if($row_u = mysql_fetch_assoc($res_u))
                {
                    $recepient_id = $row_u["user_id"];
                }
                else
                {
                    $recepient_id = '0';
                }

                $mail->add_recipient($row["postype_notification_email3"]);
				$recipeints[] = $row_u["user_id"];
                $mailaddress = 1;
            }
        }
        
        $link ="project_view_client_data.php?pid=" . $project["project_id"];
        $bodytext = $bodytext0 . "\nclick below to have direct access to the project:\n";
        $bodytext = $bodytext .  APPLICATION_URL . "/archive/" . $link . "\r\n";           
        $mail->add_text($bodytext);
        
        if($mailaddress == 1)
        {
            if($senmail_activated == true)
			{
				$mail->send();
			}
            foreach($recipeints as $key=>$value)
			{
				append_mail($project["project_order"], $recepient_id, user_id(), $bodytext, MOVED_TO_THE_RECORDS, 1);
			}
            
        }
    }
}


/********************************************************************
    build form
*********************************************************************/

$form = new Form("projects", "project");
$form->add_hidden("pid", param("pid"));
$form->add_hidden("order_id", $project["project_order"]);

require_once "include/project_head_small.php";

/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

$page->header();

if (param("action") == ORDER_CANCELLED)
{
    $page->title("The following project was cancelled:");
}
else if (param("action") == MOVED_TO_THE_RECORDS)
{
    $page->title("The following project was moved to the records:");
}

$form->render();

require_once "include/project_footer_logistic_state.php";
$page->footer();

?>