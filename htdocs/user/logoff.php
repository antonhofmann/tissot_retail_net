<?php
/********************************************************************

    logoff.php

    Entry page for the logoff section group.

    Created by:     Claudio Felber (claudio.felber@perron2.ch)
    Date created:   2002-07-31
    Modified by:    Claudio Felber (claudio.felber@perron2.ch)
    Date modified:  2002-08-19
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";

unset($_SESSION["user_id"]);
unset($_SESSION["user_login"]);
unset($_SESSION["user_permissions"]);
unset($_SESSION["new_project_step_0"]);
unset($_SESSION["new_project_step_1"]);
unset($_SESSION["new_project_step_2"]);
unset($_SESSION["new_project_step_3"]);
unset($_SESSION["new_project_step_4"]);


$page = new Page("logoff");
$page->header();
echo "<p>", "You have been logged off.", "</p>";
echo "<p>", "<a href=\"login.php\">Login again</a>", "</p>";
$page->footer();

?>