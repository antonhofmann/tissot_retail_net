<?php
/********************************************************************

    stock_global_orders_edit.php

    View and edit global order data

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2004-01-23
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-01-23
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("has_access_to_all_stock_data");

set_referer("stock_global_orders_edit_order.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
$sql_global_orders = "select store_global_order_id,  store_id, store_item, ".
                     "   store_global_order_quantity,  store_global_order_date, ".
                     "   store_global_order_comment, ".
                     "   item_code, address_shortcut ".
                     "from store_global_orders " .
                     "left join stores on store_id = store_global_order_store " .
                     "left join items on item_id = store_item " .
                     "left join addresses on address_id = store_address ";


/********************************************************************
    Create List
*********************************************************************/ 

$list = new ListView($sql_global_orders);


$list->set_entity("store_global_orders");
$list->set_filter("store_global_order_store = " . id());
$list->set_order("item_code, address_shortcut, store_global_order_date DESC");

$list->add_column("item_code", "Code", "stock_global_orders_edit_order.php?sid={store_id}&item_id={store_item}");
$list->add_column("address_shortcut", "Supplier");
$list->add_column("store_global_order_date", "Date");
$list->add_column("store_global_order_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
$list->add_column("store_global_order_comment", "Comment");

$list->add_button(LIST_BUTTON_BACK, "Back");

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->process();



$page = new Page("stock");

$page->register_action('home', 'Home', "welcome.php");
$page->register_action('stock', 'Stock Control Data', "stock.php");

$page->header();
$page->title("View Global Orders");

$list->render();
$page->footer();

?>