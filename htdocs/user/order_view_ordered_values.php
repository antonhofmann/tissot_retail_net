<?php
/********************************************************************

    order_view_ordered_values.php

    View projects's itemlist in supplier's prices and currency

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2004-06-27
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2004-06-27
    Version:        1.0.0

    Copyright (c) 2004, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("can_view_ordered_values_in_orders");

register_param("oid");
set_referer("order_edit_supplier_data_item.php");
set_referer("order_new.php");

/********************************************************************
    prepare all data needed
*********************************************************************/

// get user_data
$user_data = get_user(user_id());

$user_roles = get_user_roles(user_id());


$user_us_supplier = false;
if(in_array(5, $user_roles))
{
	$user_us_supplier = true;
}

// read order details
$order = get_order(param("oid"));


$system_currency = get_system_currency_fields();
$order_currency = get_order_currency($order["order_id"]);


// get company's address
$client_address = get_address($order["order_client_address"]);

// create sql for order items
$sql_order_items = "select order_item_id, order_item_text, order_item_quantity, ".
                   "    order_item_supplier_price, order_item_po_number, " .
                   "    concat(RIGHT(order_item_ordered, 2), '.', MONTH(order_item_ordered), '.',  YEAR(order_item_ordered)) as order_date, ".
                   "    if(order_item_item <>'', item_code, item_type_name) as item_code, " .
                   "    TRUNCATE(order_item_supplier_price * order_item_quantity, 2) as total_price, ".
                   "    addresses.address_company as supplier_company, ".
                   "    currency_symbol, ".
                   "    address_company,  " .
				   "    order_item_supplier_currency, order_item_supplier_exchange_rate, " .
				   "    item_stock_property_of_swatch, unit_name ". 
                   "from order_items ".
                   "left join items on order_item_item = item_id " .
                   "left join item_types on order_item_type = item_type_id ".
                   "left join addresses on order_item_supplier_address = addresses.address_id ".
                   "left join currencies on order_item_supplier_currency = currency_id " .
				   "left join units on unit_id = item_unit ";



if (has_access("has_access_to_all_supplier_data_in_orders"))
{
    $list_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) and ".
                      "order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . param('oid');
}
else
{
    $list_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null) and ".          
                      "order_item_type <= " . ITEM_TYPE_SPECIAL .
                      "   and order_item_order = " . param('oid') .
                      "   and order_item_supplier_address = " . $user_data["address"] .
		              "   and order_item_ordered is not null " . 
		              "   and order_item_ordered <> '0000-00-00' ";
}


//get invoice addresses
$invoice_adresses = array();
$suppliers = array();

$sql_suppliers = "select DISTINCT order_item_supplier_address, address_id, address_company " . 
				 "from order_items " . 
				 "left join addresses on order_item_supplier_address = address_id ".
				 " where " . $list_filter;

$res = mysql_query($sql_suppliers) or dberror($sql_suppliers);


while ($row = mysql_fetch_assoc($res))
{
	$suppliers[$row["address_id"]] = $row["address_company"]; 
	$sql = "select supplier_client_invoice_id ". 
		   "from supplier_client_invoices " . 
		   "where supplier_client_invoice_supplier = " . dbquote($row["order_item_supplier_address"]) .
		   " and supplier_client_invoice_client = " . dbquote($client_address["id"])  . 
		   " and supplier_client_invoice_startdate <= " . dbquote(date("Y-m-d")) . 
			   " and supplier_client_invoice_enddate >= " . dbquote(date("Y-m-d"));
	
	$res_s = mysql_query($sql) or dberror($sql);
	if ($row_s = mysql_fetch_assoc($res_s))
	{
		//get invoice_address
		$sql_i = "select invoice_address_company, invoice_address_company2, order_direct_invoice_address_id, " . 
			     "invoice_address_address, invoice_address_address2, invoice_address_zip, " . 
			     "place_name, country_name, invoice_address_phone, invoice_address_mobile_phone, " .
			     "invoice_address_email, invoice_address_contact_name " . 
			     "from orders " .
			     "inner join invoice_addresses on invoice_address_id = order_direct_invoice_address_id " . 
			     "inner join countries on country_id = invoice_address_country_id ". 
			     "inner join places on place_id = invoice_address_place_id " . 
			     "where order_id = " . param("oid");
		
		
		$res_i = mysql_query($sql_i) or dberror($sql_i);
		if ($row_i = mysql_fetch_assoc($res_i))
		{
			$iaddress = $row_i["invoice_address_company"];

			if($row_i["invoice_address_company2"])
			{
				$iaddress .= ", " . $row_i["invoice_address_company2"];
			}

			if($row_i["invoice_address_address"])
			{
				$iaddress .= ", " . $row_i["invoice_address_address"];
			}

			if($row_i["invoice_address_address2"])
			{
				$iaddress .= ", " . $row_i["invoice_address_address2"];
			}

			$iaddress .= ", " . $row_i["invoice_address_zip"] . " " . $row_i["place_name"];
			$iaddress .= ", " . $row_i["country_name"];
			
			
			$invoice_adresses [$row["address_id"]] = $iaddress;
		
		}
		else
		{
			$iaddress = $client_address["company"];

			if($client_address["company2"])
			{
				$iaddress .= ", " . $client_address["company2"];
			}

			if($client_address["address"])
			{
				$iaddress .= ", " . $client_address["address"];
			}

			if($client_address["address2"])
			{
				$iaddress .= ", " . $client_address["address2"];
			}

			$iaddress .= ", " . $client_address["zip"] . " " . $client_address["place"];
			$iaddress .= ", " . $client_address["country_name"];

			$invoice_adresses [$row["address_id"]] = $iaddress;
		}
	}
	else
	{
		//get invoice_address
		$sql_i = "select address_company, address_company2,  " . 
			     "address_address, address_address2, address_zip, " . 
			     "place_name, country_name, address_phone, address_mobile_phone, " .
			     "address_email, address_contact_name " . 
			     "from addresses " .
			     "inner join countries on country_id = address_country ". 
			     "inner join places on place_id = address_place_id " . 
			     "where address_is_standard_invoice_address = 1";
		
		
		$res_i = mysql_query($sql_i) or dberror($sql_i);
		if ($row_i = mysql_fetch_assoc($res_i))
		{
			$iaddress = str_replace(" - General", "", $row_i["address_company"]);

			if($row_i["address_company2"])
			{
				$iaddress .= ", " . $row_i["address_company2"];
			}

			if($row_i["address_address"])
			{
				$iaddress .= ", " . $row_i["address_address"];
			}

			if($row_i["address_address2"])
			{
				$iaddress .= ", " . $row_i["address_address2"];
			}

			$iaddress .= ", " . $row_i["address_zip"] . " " . $row_i["place_name"];
			$iaddress .= ", " . $row_i["country_name"];
			
			$invoice_adresses [$row["address_id"]] = $iaddress;
		
		}
	}
}

asort($suppliers);
asort($invoice_adresses);


$property = array();
$replacement_information = array();
$res = mysql_query($sql_order_items . " where " . $list_filter) or dberror($sql_order_items . " where " . $list_filter);


while ($row = mysql_fetch_assoc($res))
{
    if($row["item_stock_property_of_swatch"] == 1)
	{
		$property[$row["order_item_id"]] = "/pictures/stockproperty.gif";
	}

	$tmp = get_items_replacement_information($row["order_item_id"]);
	if(is_array($tmp))
	{
		$replacement_information[$row["order_item_id"]] = $tmp;
	}
}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("orders", "order");

$form->add_hidden("oid", param("oid"));

require_once "include/order_head_small.php";


if($user_us_supplier == false and $order_currency["symbol"] != $system_currency["symbol"])
{
	$form->add_comment('<span class="error">All amounts for HQ supplied items converted from CHF into LOC are for reference only as the exchange rate may change.</span>');
}

$form->add_section("Suppliers send Invoices to the following Address");
foreach($suppliers as $id=>$companyname)
{
	$form->add_label("S" . $id, $companyname, RENDER_HTML, $invoice_adresses[$id]);
}



if(count($replacement_information) > 0)
{
	$form->add_section("Replacement Information");
	
	foreach($replacement_information as $key=>$tmp)
	{
		$form->add_comment("<strong>" . $tmp["order_item_text"] . "</strong>");
		$form->add_label("original_quantity", "Original Quantity", 0, $tmp["order_item_quantity"]);
		$form->add_label("reason", "Replacement Reason", 0, $tmp["order_item_replacement_reason_text"]);
		$form->add_label("warranty", "Warranty", 0, $tmp["order_item_warranty_type_text"]);
		$form->add_label("furniture_payed_by", "Furniture paid by", 0, $tmp["furniture_payed_by"]);
		$form->add_label("freight_payed_by", "Freight paid by", 0, $tmp["freight_payed_by"]);
		$form->add_label("remarks", "Remark", 0, $tmp["order_item_replacement_remarks"]);
	}
}
	
$link = "order_view_ordered_values_pdf.php?oid=" . param("oid");
$link = "javascript:popup('". $link . "', 800, 600)";
$form->add_button("print1", "Print PDF in Supplier's Currency", $link);


if($user_us_supplier == false)
{
	$link = "order_view_ordered_values_pdf.php?oid=" . param("oid") . "&chf=1";
	$link = "javascript:popup('". $link . "', 800, 600)";
	$form->add_button("print_chf", "Print PDF in CHF", $link);

	if($order_currency["symbol"] != $system_currency["symbol"])
	{
		$link = "order_view_ordered_values_pdf.php?oid=" . param("oid") . "&loc=1";
		$link = "javascript:popup('". $link . "', 800, 600)";
		$form->add_button("print2", "Print PDF in " . $order_currency["symbol"], $link);
	}
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

/********************************************************************
   Caclulate prices in different currencies
*********************************************************************/
$prices = array();
$total_prices = array();
$group_totals["currency"] = array();
$group_totals["total"] = array();

$group_totals_chf["currency"] = array();
$group_totals_chf["total"] = array();

$group_totals_loc["currency"] = array();
$group_totals_loc["total"] = array();


$sql = $sql_order_items . " where " . $list_filter;

$res = mysql_query($sql) or dberror($sql);
while ($row = mysql_fetch_assoc($res))
{
	
	
	$supplier_currency = get_currency($row["order_item_supplier_currency"]);
	$key = $row["address_company"];
	$group_totals["currency"][$key] = $supplier_currency ["symbol"];

	$prices[$row["order_item_id"]] = $row["order_item_supplier_price"];
	$total_prices[$row["order_item_id"]] = round($row["order_item_quantity"] * $row["order_item_supplier_price"], 2);

	// build group_totals of suppliers
	if(array_key_exists($key, $group_totals["total"]))
	{
		$group_totals["total"][$key] = $group_totals["total"][$key] + $row["order_item_quantity"] * $row["order_item_supplier_price"];
	}
	else
	{
		$group_totals["total"][$key] = $row["order_item_quantity"] * $row["order_item_supplier_price"];
	}

	
	$group_totals_chf["currency"][$key] = 'CHF';
	
	$price_in_chf = $row["order_item_supplier_exchange_rate"] * $row["order_item_supplier_price"] / $supplier_currency["factor"];
	
	$prices_chf[$row["order_item_id"]] = round($price_in_chf,2);
	$total_prices_chf[$row["order_item_id"]] = round($row["order_item_quantity"] * $price_in_chf, 2);

	// build group_totals of suppliers
	if(array_key_exists($key, $group_totals_chf["total"]))
	{
		$group_totals_chf["total"][$key] = $group_totals_chf["total"][$key] + $row["order_item_quantity"] * $price_in_chf;
	}
	else
	{
		$group_totals_chf["total"][$key] = $row["order_item_quantity"] * $price_in_chf;
	}


	
	$group_totals_loc["currency"][$key] = $order_currency["symbol"];

	$price_in_chf = $row["order_item_supplier_exchange_rate"] * $row["order_item_supplier_price"] / $supplier_currency["factor"];

	$price_in_loc = $price_in_chf / $order_currency["exchange_rate"] * $order_currency["factor"];
	
	$prices_loc[$row["order_item_id"]] = round($price_in_loc, 2);
	$total_prices_loc[$row["order_item_id"]] = round($row["order_item_quantity"] * $price_in_loc, 2);

	// build group_totals of suppliers
	if(array_key_exists($key, $group_totals_loc["total"]))
	{
		$group_totals_loc["total"][$key] = $group_totals_loc["total"][$key] + $row["order_item_quantity"] * $price_in_loc;
	}
	else
	{
		$group_totals_loc["total"][$key] = $row["order_item_quantity"] * $price_in_loc;
	}
}


/********************************************************************
    Create Item List
*********************************************************************/ 
$list = new ListView($sql_order_items, LIST_HAS_SEPARATOR | LIST_HAS_HEADER);

$list->set_title("Items Ordered");
$list->set_entity("order_item");
$list->set_group("address_company");
$list->set_filter($list_filter);


$list->set_order("order_item_type, item_code");

$list->add_column("item_code", "Item Code", "", "", "", COLUMN_BREAK);
$list->add_image_column("property", "", 0, $property);
$list->add_column("order_item_text", "Name", "", "", "", COLUMN_BREAK);
$list->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_BREAK);
$list->add_column("order_date", "Ordered", "", "", "", COLUMN_BREAK);
$list->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_BREAK);
$list->add_column("unit_name", "Unit", "", "", "", COLUMN_NO_WRAP);

$list->add_text_column("price", "Price Supplier", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $prices);
$list->add_text_column("total_price", "Total Supplier", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $total_prices);


if($user_us_supplier == false)
{
	$list->add_text_column("price_chf", "Price CHF", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $prices_chf);
	$list->add_text_column("total_price_chf", "Total CHF", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $total_prices_chf);

	if($order_currency["symbol"] != 'CHF')
	{
		$list->add_text_column("price_loc", "Price " . $order_currency["symbol"], COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $prices_loc);
		$list->add_text_column("total_price_loc", "Total " . $order_currency["symbol"], COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $total_prices_loc);
	}
}


// set group totals
foreach ($group_totals["total"] as $key=>$value)
{
    $value = number_format($value,2, ".", "'");
	$list->set_group_footer("total_price", $key , $value . "\n". $group_totals["currency"][$key]);
}

if($user_us_supplier == false)
{
	foreach ($group_totals_chf["total"] as $key=>$value)
	{
		$value = number_format($value,2, ".", "'");
		$list->set_group_footer("total_price_chf", $key , $value . "\n" . "CHF");
	}

	if($order_currency["symbol"] != 'CHF')
	{
		foreach ($group_totals_loc["total"] as $key=>$value)
		{
			$value = number_format($value,2, ".", "'");
			$list->set_group_footer("total_price_loc", $key , $value . "\n" . $group_totals_loc["currency"][$key]);
		}
	}
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("View Ordered Values in Supplier's Prices");
$form->render();
$list->render();

$page->footer();

?>