<?php
/********************************************************************

    project_new_01.php

    Creation of a new project step 01.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-07-14
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-07-14
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_create_new_projects");

set_referer("project_new_02.php");



function add_new_province_and_place() {
	//insert new record into provinces for new city in the POS Location Address
	$province_id = "";
	$place_id = "";
	if(isset($_SESSION["new_project_step_1"]["select_shop_address_province"]))
	{
		if($_SESSION["new_project_step_1"]["select_shop_address_province"] == "999999999")
		{
			$fields = array();
			$values = array();

			$fields[] = "province_country";
			$values[] = dbquote($_SESSION["new_project_step_1"]["shop_address_country"]);

			$fields[] = "province_canton";
			$values[] = dbquote($_SESSION["new_project_step_1"]["shop_address_province"]);

			$fields[] = "date_created";
			$values[] = "current_timestamp";

			$fields[] = "user_created";
			$values[] = dbquote($_SESSION["user_login"]);

			$sql = "insert into provinces (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			mysql_query($sql) or dberror($sql);

			$province_id =  mysql_insert_id();
			$_SESSION["new_project_step_1"]["select_shop_address_province"] = $province_id;
		}
	}

	//insert new record into places or update place
	if(isset($_SESSION["new_project_step_1"]["select_shop_address_place"]))
	{
		if($_SESSION["new_project_step_1"]["select_shop_address_place"] == "999999999")
		{

			$fields = array();
			$values = array();

			$fields[] = "place_country";
			$values[] = dbquote($_SESSION["new_project_step_1"]["shop_address_country"]);

			if($province_id)
			{
				$fields[] = "place_province";
				$values[] = dbquote($province_id);
			}
			else
			{
				$fields[] = "place_province";
				$values[] = dbquote($_SESSION["new_project_step_1"]["select_shop_address_province"]);
			}
			

			$fields[] = "place_name";
			$values[] = dbquote($_SESSION["new_project_step_1"]["shop_address_place"]);

			$fields[] = "date_created";
			$values[] = "current_timestamp";

			$fields[] = "user_created";
			$values[] = dbquote($_SESSION["user_login"]);

			$sql = "insert into places (" . join(", ", $fields) . ") values (" . join(", ", $values) . ")";
			mysql_query($sql) or dberror($sql);


			$place_id =  mysql_insert_id();
			$_SESSION["new_project_step_1"]["select_shop_address_place"] = $place_id;
			
		}
		else // update places with a new or another province
		{
			$fields = array();
			$values = array();

			$value = dbquote($_SESSION["new_project_step_1"]["select_shop_address_province"]);
			$fields[] = "place_province = " . $value;
			
			$value = dbquote(date("Y-m-d"));
			$fields[] = "date_modified = " . $value;

			$value = dbquote(user_login());
			$fields[] = "user_modified = " . $value;

			$sql = "update places set " . join(", ", $fields) . " where place_id = " . $_SESSION["new_project_step_1"]["select_shop_address_place"];
			mysql_query($sql) or dberror($sql);
		}
	}
	else // update places with a new or another province
	{
		if(isset($_SESSION["new_project_step_1"]["select_shop_address_place"]))
		{
			$place_id = "";
			$sql = "select place_id from places where place_id = " . dbquote($_SESSION["new_project_step_1"]["select_shop_address_place"]);
			$res = mysql_query($sql) or dberror($sql);
			if ($row = mysql_fetch_assoc($res))
			{
				$place_id = $row["place_id"];
			}

			$fields = array();
			$values = array();
			$value = dbquote($_SESSION["new_project_step_1"]["select_shop_address_province"]);
			$fields[] = "place_province = " . $value;
			
			$value = dbquote(date("Y-m-d"));
			$fields[] = "date_modified = " . $value;

			$value = dbquote(user_login());
			$fields[] = "user_modified = " . $value;

			$sql = "update places set " . join(", ", $fields) . " where place_id = " . dbquote($place_id);
			mysql_query($sql) or dberror($sql);
		}
	}
}


if(count($_POST) == 0 and isset($_SESSION["new_project_step_1"]))
{
	$_SESSION["new_project_step_1"]["action"] = "";
	foreach($_SESSION["new_project_step_1"] as $key=>$value)
	{
		register_param($key);
		param($key, $value);
	}
}

/********************************************************************
    prepare all data needed
*********************************************************************/
// get the region of users' address
$user_region = get_user_region(user_id());

// get user data
$user = get_user(user_id());

// get users' company address
$address = get_address($user["address"]);


if($user["user_project_requester"] == 1) {
	redirect("noaccess.php");
}

if(!param("shop_address_country"))
{
	register_param("shop_address_country");
	param("shop_address_country", $address["country"]);

	$param_country_is_submitted = false;

	$country_phone_prefix = get_country_phone_prefix($address["country"]); 
}
else
{
	$param_country_is_submitted = true;
	$country_phone_prefix = get_country_phone_prefix(param("shop_address_country")); 
}

// create sql for the country listbox
$sql_countries = "select country_id, country_name ".
                 "from countries ".
                 "order by country_name";




// create sql for the production_type listbox
$sql_production_types = "select production_type_id, production_type_name ".
						 "from production_types ".
						 " where production_type_id <> 6 " . 
						 "order by production_type_name";


//project legal types
$p_cost_types = array();

if($address["client_type"] == 1) // client is agent, only franchisee as option
{
    $sql = "select * from project_costtypes where project_costtype_id IN (2,6)";
}
else
{
    $sql = "select * from project_costtypes where project_costtype_id  IN (1, 2, 6)";
}
$res = mysql_query($sql);
while ($row = mysql_fetch_assoc($res))
{
    $p_cost_types[$row["project_costtype_id"]] = $row["project_costtype_text"];
}


if(param("project_cost_type"))
{
	
	if(param("project_cost_type") == 1)
	{
		$sql_pos_types = "select postype_id, postype_name ".
						 "from postypes ".
						 "where postype_id IN (1, 2, 3) " . 
						 " order by postype_name ";
	}
	elseif(param("project_cost_type") == 2)
	{
		$filter = "where postype_id IN (1, 2, 3) ";
		if(param("project_kind") == 7)
		{
			$filter = "where postype_id IN (2) ";
		}
		
		$sql_pos_types = "select postype_id, postype_name ".
						 "from postypes " .
						 $filter .
						 " order by postype_name ";
	}
	elseif(param("project_cost_type") == 6)
	{
		$sql_pos_types = "select postype_id, postype_name ".
						 "from postypes ".
						 "where postype_id IN (2) " . 
						 " order by postype_name ";
	}
	
}
else
{
	$sql_pos_types = "select postype_id, postype_name ".
					 "from postypes ".
					 "where postype_id = 0 " . 
					 " order by postype_name ";
}


if(param("project_postype"))
{
	$sql_subclasses = "select possubclass_id, possubclass_name " .
		              "from postype_subclasses " .
		              "left join possubclasses on possubclass_id = postype_subclass_subclass " .
		              "left join product_line_pos_types on  product_line_pos_type_id = postype_subclass_pl_pos_type " . 
		              "where  product_line_pos_type_pos_type = " . param("project_postype") .
		              " and product_line_pos_type_product_line is null " .
	                  " order by possubclass_name";
}
else
{
	$sql_subclasses = "select possubclass_id, possubclass_name " .
		              "from postype_subclasses " .
		              "left join possubclasses on possubclass_id = postype_subclass_subclass " .
		              "left join product_line_pos_types on  product_line_pos_type_id = postype_subclass_pl_pos_type " . 
		              "where  product_line_pos_type_pos_type = 0 " .
		              " and product_line_pos_type_product_line =  0" .
	                  " order by possubclass_name";
}


if(param("project_cost_type"))
{
	if(param("project_cost_type") == 1) // Corporate, joint venture or cooperation 3rd party
	{
		$sql_project_kinds = "select projectkind_id, projectkind_name " . 
			                 "from projectkinds " . 
			                 "where projectkind_id IN (1, 2,3, 4, 5, 6, 8, 9) 
		                      order by projectkind_name";
	}
	elseif(param("project_cost_type") == 2) // Franchisee
	{
		$sql_project_kinds = "select projectkind_id, projectkind_name " . 
			                 "from projectkinds " . 
			                 "where projectkind_id in (1, 2, 6, 7, 8)
		                      order by projectkind_name";
	}
	else
	{
		$sql_project_kinds = "select projectkind_id, projectkind_name " . 
			                 "from projectkinds " . 
			                 "where projectkind_id in (1, 2, 6, 7, 8)
		                      order by projectkind_name";
	}
}
else
{
	$sql_project_kinds = "select projectkind_id, projectkind_name " . 
			                 "from projectkinds " . 
			                 "where projectkind_id = ''";
}



if(param("project_kind") == 1) // new project
{
	$sql_posaddresses = "select posaddress_id, concat(country_name, ' ', posaddress_place,  ', ', posaddress_name) as pos " .
						"from posaddresses " .
						"left join countries on country_id = posaddress_country " . 
						" where (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00')   " . 
						" and posaddress_client_id = " .  $user["address"] . 
						" order by country_name, posaddress_place";
}
elseif(param("project_kind") == 2) // renovation 
{
	$sql_posaddresses = "select posaddress_id, concat(country_name, ' ', posaddress_place,  ', ', posaddress_name) as pos " .
						"from posaddresses " .
						"left join countries on country_id = posaddress_country " . 
						" where (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00') " . 
						" and posaddress_ownertype = " . param("project_cost_type") . " and posaddress_client_id = " .  $user["address"] .
		                " and posaddress_store_postype = " . dbquote(param('project_postype')) . 
						" order by country_name, posaddress_place";
}
elseif(param("project_kind") == 3 ) // takover/renovation 
{
	
	if($address["client_type"] == 3 and $address["parent"] > 0) // affiliate
	{
		$sql_posaddresses = "select posaddress_id, concat(country_name, ' ', posaddress_place,  ', ', posaddress_name) as pos " .
							"from posaddresses " .
							"left join countries on country_id = posaddress_country " . 
							" where (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00')   " . 
							" and posaddress_ownertype in (2, 6) and (posaddress_client_id = " .  $user["address"] . 
			                " or posaddress_client_id = " . $address["parent"] . ") " . 
							" and posaddress_store_postype = " . dbquote(param('project_postype')) . 
							" order by country_name, posaddress_place";
	}
	else
	{
		$sql_posaddresses = "select posaddress_id, concat(country_name, ' ', posaddress_place,  ', ', posaddress_name) as pos " .
							"from posaddresses " .
							"left join countries on country_id = posaddress_country " . 
							" where (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00')   " . 
							" and posaddress_ownertype in (2, 6) and posaddress_client_id = " .  $user["address"] . 
							" and posaddress_store_postype = " . dbquote(param('project_postype')) . 
							" order by country_name, posaddress_place";
	}
}
elseif(param("project_kind") == 4) // take over
{
		
	if($address["client_type"] == 3 and $address["parent"] > 0) // affiliate
	{
		$sql_posaddresses = "select posaddress_id, concat(country_name, ' ', posaddress_place,  ', ', posaddress_name) as pos " .
							"from posaddresses " .
							"left join countries on country_id = posaddress_country " . 
							" where (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00')   " . 
							" and posaddress_ownertype in (2,6) and (posaddress_client_id = " .  $user["address"] . 
			                " or posaddress_client_id = " . $address["parent"] . ") " .
							" order by country_name, posaddress_place";
	}
	else
	{
		
		$sql_posaddresses = "select posaddress_id, concat(country_name, ' ', posaddress_place,  ', ', posaddress_name) as pos " .
							"from posaddresses " .
							"left join countries on country_id = posaddress_country " . 
							" where (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00')   " . 
							" and posaddress_ownertype in (2,6) and posaddress_client_id = " .  $user["address"] . 
							" order by country_name, posaddress_place";
	}

}
elseif(param("project_kind") == 5) // lease renewal
{
	$sql_posaddresses = "select posaddress_id, concat(country_name, ' ', posaddress_place,  ', ', posaddress_name) as pos " .
						"from posaddresses " .
						"left join countries on country_id = posaddress_country " . 
						" where (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00')   " . 
						" and posaddress_ownertype = 1 and posaddress_client_id = " .  $user["address"] . 
						" order by country_name, posaddress_place";
}
elseif(param("project_kind") == 6) //relocation
{
	$sql_posaddresses = "select posaddress_id, concat(country_name, ' ', posaddress_place,  ', ', posaddress_name) as pos " .
						"from posaddresses " .
						"left join countries on country_id = posaddress_country " . 
						" where (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00')   " . 
						" and posaddress_client_id = " .  $user["address"] . 
		                " and posaddress_store_postype IN (1, 2, 3) " . 
		                " and posaddress_ownertype = " . param("project_cost_type") . 
						" order by country_name, posaddress_place";
}
elseif(param("project_kind") == 7) //equipment of an independent retailer with Tissot furniture
{
	$sql_posaddresses = "select posaddress_id, concat(country_name, ' ', posaddress_place,  ', ', posaddress_name) as pos " .
						"from posaddresses " .
						"left join countries on country_id = posaddress_country " . 
						" where (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00')   " . 
						" and posaddress_client_id = " .  $user["address"] . 
		                " and posaddress_store_postype IN (4) " . 
		                " and posaddress_ownertype = 6 " . 
						" order by country_name, posaddress_place";
}
elseif(param("project_kind") == 8) // PopUp
{
	$sql_posaddresses = "select posaddress_id, concat(country_name, ' ', posaddress_place,  ', ', posaddress_name) as pos " .
							"from posaddresses " .
							"left join countries on country_id = posaddress_country " . 
							" where (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00')   " . 
							" and posaddress_ownertype in (2,6) and (posaddress_client_id = " .  $user["address"] . 
			                " or posaddress_client_id = " . $address["parent"] . ") " .
							" order by country_name, posaddress_place";

}
elseif(param("project_kind") == 9) //takeover relocation
{
	$sql_posaddresses = "select posaddress_id, 
	                     concat(country_name, ' ', posaddress_place,  ', ', posaddress_name) as pos " .
						"from posaddresses " .
						"left join countries on country_id = posaddress_country " . 
						" where (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00')   " . 
						" and posaddress_client_id = " .  $user["address"] . 
		                " and posaddress_ownertype in (2,6) " . 
						" order by country_name, posaddress_place";
}


$poslocation = array();

$poslocation_product_line_name = '';
$poslocation_postype_name = '';

if(param("posaddress_id"))
{
	
	$sql = "select * ".
			   "from posaddresses ".
			   "left join countries on country_id = posaddress_country " .
			   "left join addresses on address_id = posaddress_client_id " .
		       "left join product_lines on product_line_id = posaddress_store_furniture " . 
		       "left join postypes on postype_id = posaddress_store_postype " . 
			   "where posaddress_id  = " . dbquote(param("posaddress_id"));


	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$poslocation = $row;

		$poslocation_product_line_name = $row['product_line_name'];
		$poslocation_postype_name = $row['postype_name'];

	}
}

/* not used anymore
if(isset($_SESSION["new_project_step_0"]["cid"]))
{
	$shop_configurator_product_line = 0;
    $shop_configurator_product_name = "";
	$shop_configurator_pos_type = 3;
    $shop_configurator_pos_type_name = "Kiosk";
	//get product line
	$sql = "select category_product_line, product_line_name " . 
		   "from categories " . 
		   "left join product_lines on product_line_id = category_product_line " . 
		   "where category_id = " . $_SESSION["new_project_step_0"]["cid"];
	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		$shop_configurator_product_line = $row["category_product_line"];
		$shop_configurator_product_name = $row["product_line_name"];
	}
}
*/

$can_add_new_province = false;
if(param("shop_address_country"))
{
	$sql_c = "select country_provinces_complete " . 
		     "from countries " . 
		     "where country_id = " . param("shop_address_country");
	
	$res = mysql_query($sql_c) or dberror($sql_c);
	$row = mysql_fetch_assoc($res);

	if ($row["country_provinces_complete"] == 0)
	{
		$can_add_new_province = true;
	}
}

/********************************************************************
    build form
*********************************************************************/
$form = new Form("", "project");

/*
if($address["client_type"] == 1) // client is agent, only franchisee as option
{
	 $form->add_hidden("project_cost_type", 2);
	 $form->add_label("pct", "Project Legal Type", 0, "POS Owner Company");
}
else
{
	$form->add_list("project_cost_type", "Project Legal Type*", $p_cost_types, NOTNULL  | SUBMIT, 0, 1, "ltype");
}
*/

$form->add_list("project_cost_type", "Project Legal Type*", $p_cost_types, NOTNULL  | SUBMIT, 0, 1, "ltype");

$form->add_list("project_kind", "Project Type*", $sql_project_kinds, NOTNULL  | SUBMIT, 0, 1, "pkind");
$form->add_hidden("old_project_kind");

if(isset($_SESSION["new_project_step_0"]["cid"]))
{
	$form->add_hidden("product_line", $shop_configurator_product_line);
	$form->add_hidden("project_postype", $shop_configurator_pos_type);
	$form->add_hidden("project_pos_subclass");
	$form->add_label("product_line_name", "Product Line", 0, $shop_configurator_product_name);
	$form->add_label("pos_type_name","POS Type", 0, $shop_configurator_pos_type_name);
}
else
{
	if(param("project_kind") == 2 or param("project_kind") == 3) // renovation or tekaover/renovation
	{
		$form->add_hidden("product_line", 0);
		//$form->add_list("product_line", "New Product Line*", $sql_product_lines, NOTNULL  | SUBMIT);
		$form->add_list("project_postype", "POS Type*", $sql_pos_types, NOTNULL  | SUBMIT, 0, 1, "ptype");
		$form->add_list("project_pos_subclass", "New POS Type Subclass", $sql_subclasses, 0, 0, 1, "sclass");
	}
	elseif(param("project_kind") == 4) // take over
	{
		if(array_key_exists('posaddress_store_furniture', $poslocation)) {
			$form->add_hidden("product_line", $poslocation['posaddress_store_furniture']);
			$form->add_hidden("project_postype", $poslocation['posaddress_store_postype']);
			$form->add_hidden("project_pos_subclass", $poslocation['posaddress_store_subclass']);	
		}
		else
		{
			$form->add_hidden("product_line");
			$form->add_hidden("project_postype");
			$form->add_hidden("project_pos_subclass");	
		}

		$form->add_label("pln", "Product Line", 0, $poslocation_product_line_name);
		$form->add_label("ptn","POS Type", 0, $poslocation_postype_name);
	}
	elseif(param("project_kind") == 5) // lease renewal
	{
		
		if(array_key_exists('posaddress_store_furniture', $poslocation)) {
			$form->add_hidden("product_line", $poslocation['posaddress_store_furniture']);
			$form->add_hidden("product_line_subclass", $poslocation['posaddress_store_furniture_subclass']);
			$form->add_hidden("project_postype", $poslocation['posaddress_store_postype']);
			$form->add_hidden("project_pos_subclass", $poslocation['posaddress_store_subclass']);	
		}
		else
		{
			$form->add_hidden("product_line");
			$form->add_hidden("project_postype");
			$form->add_hidden("project_pos_subclass");	
		}

		$form->add_label("pln", "Product Line", 0, $poslocation_product_line_name);
		$form->add_label("ptn","POS Type", 0, $poslocation_postype_name);

	}
	elseif(param("project_kind") == 8) // PopUp
	{
		if(array_key_exists('posaddress_store_furniture', $poslocation)) {
			
			$form->add_hidden("product_line", $poslocation['posaddress_store_furniture']);
			$form->add_hidden("project_postype", $poslocation['posaddress_store_postype']);
			$form->add_hidden("project_pos_subclass", $poslocation['posaddress_store_subclass']);
			$form->add_label("pln", "Product Line", 0, $poslocation_product_line_name);
			$form->add_label("ptn","POS Type", 0, $poslocation_postype_name);
		}
		else
		{
			$form->add_hidden("product_line");
			$form->add_hidden("project_postype");
			$form->add_hidden("project_pos_subclass");	
		}

		
	}
	else
	{
		$form->add_hidden("product_line", 0);
		//$form->add_list("product_line", "Product Line*", $sql_product_lines, NOTNULL  | SUBMIT);
		$form->add_list("project_postype", "POS Type*", $sql_pos_types, NOTNULL  | SUBMIT, 0, 1, "ptype");
		$form->add_list("project_pos_subclass", "POS Type Subclass", $sql_subclasses, 0, 0, 1, "sclass");
	}
}

if(param("project_kind"))
{

	if(param("project_kind") == 6
		or param("project_kind") == 9) //relocation and takover/relocation
	{
		$form->add_section(" ");
		$form->add_section("POS Location");
		$form->add_section(" ");
		$form->add_section("Relocation Info");
		$form->add_comment("Please indicate the POS to be relocated.");
		$form->error("In case the POS you want to relocate does not exist in our records <br />(POS address selection below) you have to choose 'New POS' <br />as project type instead of 'Relocation'.");
		$form->add_list("project_relocated_posaddress_id", "POS being relocated*", $sql_posaddresses, SUBMIT | NOTNULL);
	}
	else
	{
		$form->add_hidden("project_relocated_posaddress_id", 0);
	}

	

	if(param("project_kind") == 2 
		and param("project_cost_type") == 1) // Renovation of a corporate store
	{
		$form->add_section(" ");
		$form->add_section("POS Location");
		$form->add_list("posaddress_id", "POS Address*", $sql_posaddresses, NOTNULL | SUBMIT);
	}
	elseif((param("project_kind") == 3 or param("project_kind") == 4) 
		and param("project_cost_type") == 1) // Take Over/Renovation or Takeover
	{
		$form->add_section(" ");
		$form->add_section("POS Location");

		$form->error("In case the POS you want to take over does not exist in our records <br />(POS address selection below) you have to choose 'New POS' <br />as project type instead of 'Take over'.");
		$form->add_list("posaddress_id", "POS Address*", $sql_posaddresses, NOTNULL | SUBMIT);
		if(param("project_kind") == 4)
		{
			$form->add_edit("project_real_opening_date", "Planned Take Over Date*", NOTNULL, "", TYPE_DATE, 20);
		}
	}
	elseif(param("project_kind") == 5 and param("project_cost_type") == 1) //Lease Renewal
	{
		$form->add_section(" ");
		$form->add_section("POS Location");
		$form->error("In case the POS for which you want a lease renewal does not exist in our records <br />(POS address selection below) you have to choose 'New POS' <br />as project type instead of 'Lease Renewal'.");
		$form->add_list("posaddress_id", "POS Address*", $sql_posaddresses, NOTNULL | SUBMIT);
		
		if(param("project_kind") == 5)
		{
			$form->add_edit("project_real_opening_date", "Planned Lease Starting Date*", NOTNULL, "", TYPE_DATE, 20);
		}
	}
	elseif(param("project_kind") == 2 
		and (param("project_cost_type") == 2)
		) // Renovation of a franchisee store
	{
		$form->add_section(" ");
		$form->add_section("POS Location");
		$form->add_comment("Please indicate the POS to be renovated.");
		$form->error("In case the POS you want to renovate does not exist in our records <br />(POS address selection below) you have to choose 'New POS' <br />as project type instead of 'Renovation'.");
		$form->add_list("posaddress_id", "POS Address*", $sql_posaddresses, NOTNULL | SUBMIT);
	}
	elseif(param("project_kind") == 7) //equipment of an independent retailer with Tissot furniture
	{
		$form->add_section(" ");
		$form->add_section("POS Location");
		$form->add_section(" ");
		$form->add_section("Equipment Info");
		$form->add_comment("Please indicate the independent retailer to be equipped with Tissot furniture.");
		$form->error("In case the retailer you want to equip does not exist in our records <br />(POS address selection below) you have to choose 'New POS' <br />as project type instead of 'Equipment'.");
		$form->add_list("posaddress_id", "POS Address*", $sql_posaddresses, NOTNULL | SUBMIT);
	}
	elseif(param("project_kind") == 8) //PopUp
	{
		$form->add_section(" ");
		$form->add_section("POS Location");
		$form->add_comment("Please indicate the POS Locations for this PopUp project.");
		$form->add_list("posaddress_id", "POS Address*", $sql_posaddresses, NOTNULL | SUBMIT);
	}
	else
	{
		$form->add_hidden("posaddress_id");
	}

}
else
{
	$form->add_hidden("posaddress_id");
}

if(param("project_kind") == 5
	or param("project_kind") == 4) //relocation
{
	$form->add_hidden("project_production_type", 6);
}
else {
	$form->add_section("Production Type");
	$form->add_list("project_production_type", "Production Type*", $sql_production_types, NOTNULL);
}

if(param("project_kind") == 1 
	or param("project_kind") == 6
	or param("project_kind") == 9 
	or (param("project_kind") == 7 
		and param("posaddress_id") > 0) 
	or (param("posaddress_id") > 0 
		and param("old_project_kind") == param("project_kind"))) // new pos, relocation 
{
	if(param("posaddress_id"))
	{
		$form->add_comment("Please check the correctness of the POS address. <br /><strong><span style=\"color: #FF0000;\">IMPORTANT:</span></strong> Please read the helping tips for Project Names,  <br />Phone and Mobile Phone Numbers.");
	}
	else
	{
		$form->add_comment("Please indicate the POS address. <br/><strong><span style=\"color: #FF0000;\">IMPORTANT:</span></strong> Please read the helping tips for Project Names, <br />Phone and Mobile Phone Numbers.");
	}

	if(param("project_kind") == 8) //PopUp
	{
		$form->add_label("shop_address_company", "POS Name");
	}
	else
	{
		$form->add_edit("shop_address_company", "Project Name*", NOTNULL, "", TYPE_CHAR, 0, 0, 1, "poslocation");
	}
	$form->add_edit("shop_address_company2", "", 0, "", TYPE_CHAR);


	$form->add_hidden("shop_address_address");
	$form->add_multi_edit("shop_street", array("shop_address_street", "shop_address_streetnumber"), "Street/Street number", array(NOTNULL, ''), array('',''), array('', ''), array(200, 5), array(), 0, '', '', array(), array('Street name', 'Number'));




	$form->add_edit("shop_address_address2", "Additional Address Info", 0, "", TYPE_CHAR);
	
	
	
	if(param("project_kind") == 1 
		or param("project_kind") == 6
		or param("project_kind") == 9)
	{
		$form->add_list("shop_address_country", "Country*", $sql_countries, NOTNULL  | SUBMIT);
	}

	
	if(param("project_kind") == 1 
		or param("project_kind") == 6
		or param("project_kind") == 9)
	{
		if(param("shop_address_country") > 0)
		{	
			
			$places = array();
			$places[999999999] = "Other city not listed below";
			
			$sql_places = "select place_id, place_name from places where place_country = " . param("shop_address_country") . " order by place_name";
			$res = mysql_query($sql_places);

			while ($row = mysql_fetch_assoc($res))
			{
				$places[$row["place_id"]] = $row["place_name"];
			}
			
			
			$form->add_comment("Please select an existing city!");
			$form->add_list("select_shop_address_place", "City Selection*", $places, NOTNULL | SUBMIT);
		}

		if(param("select_shop_address_place") == 999999999)
		{
			$form->add_edit("shop_address_place", "City*", NOTNULL, "", TYPE_CHAR, 20, 0, 1, "city");
		}
		else
		{
			
			$form->add_edit("shop_address_place", "City*", DISABLED | NOTNULL, "", TYPE_CHAR, 20);
		}
		
	}
	else
	{
		$form->add_hidden("shop_address_place", "City");
		$form->add_hidden("shop_address_place_id");
		$form->add_hidden("shop_address_country", "Country");

		$form->add_label("label_shop_address_place", "City");
		$form->add_label("label_shop_address_country", "Country");
	}

	$form->add_edit("shop_address_zip", "ZIP", 0, "", TYPE_CHAR, 20);

    
	$provinces = array();
	if($can_add_new_province == true)
	{
		$provinces[999999999] = "Other province not listed below";
	}
	
	if(param("project_kind") == 1 
		or param("project_kind") == 6
		or param("project_kind") == 9) //new project, relocation project
	{
		$sql_provinces = "select province_id, province_canton from provinces where province_country = " . dbquote(param("shop_address_country")) . " order by province_canton";
	}
	else
	{
		$sql_provinces = "select province_id, province_canton from provinces where province_country = " . dbquote($poslocation["posaddress_country"]) . " order by province_canton";
	}
	$res = mysql_query($sql_provinces);
	while ($row = mysql_fetch_assoc($res))
	{
		$provinces[$row["province_id"]] = $row["province_canton"];
	}
	
	
	if(param("project_kind") == 8) //PopUp
	{
		$form->add_hidden("select_shop_address_province");
		$form->add_hidden("shop_address_province");
	}
	else
	{
		$form->add_comment("Please select an existing province!");
		$form->add_list("select_shop_address_province", "Province Selection*", $provinces, NOTNULL | SUBMIT);

		if(param("select_shop_address_province") == 999999999)
		{
			$form->add_edit("shop_address_province", "Province*", NOTNULL, "", TYPE_CHAR, 50, 0, 1, "province");
		}
		else
		{
			
			$form->add_edit("shop_address_province", "Province*", DISABLED | NOTNULL, "", TYPE_CHAR, 50);
		}
	}

	$form->add_hidden("shop_address_phone");
	$form->add_hidden("shop_address_mobile_phone");
	
	if(param("project_kind") != 1111 and param("project_kind") != 6666) {
		$form->add_multi_edit("shop_phone_number", array("shop_phone_country", "shop_phone_area", "shop_phone_number"), "Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array('','',''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20), array(), 0, '', '', array(), array('Country', 'Area', 'Number'));

		
		
	
		$form->add_multi_edit("shop_mobile_phone_number", array("shop_mobile_phone_country", "shop_mobile_phone_area", "shop_mobile_phone_number"), "Mobile Phone: Country/Area/Number", array(PHONE_COUNTRY_CODE, PHONE_AREA_CODE , PHONE_NUMBER), array('','',''), array(TYPE_CHAR, TYPE_INT, TYPE_INT), array(6, 6, 20), array(), 0, '', '', array(), array('Country', 'Area', 'Number'));

		
	

		$form->add_edit("shop_address_email", "Email", 0, "", TYPE_CHAR, 100);
	}
	else
	{
		$form->add_hidden("shop_address_email");
	}
	//$form->add_edit("shop_address_contact_name", "Contact", 0, "", TYPE_CHAR, 100);
	
	
	
	if(param("shop_address_country") or param("posaddress_id") > 0)
	{
		$form->add_section(" ");
		$form->add_section("Google Map Coordinates");
		$form->add_comment("Please verify that the map information is correct!");
		$form->add_label("GM", "Google Map", RENDER_HTML);

		
		if ($address["country"] == 19 
			or $address["country"] == 41 
			or $address["country"] == 153)
		{
			$form->add_edit("posaddress_google_lat", "Latitude");
			$form->add_edit("posaddress_google_long", "Longitude");
		}
		else
		{
			$form->add_edit("posaddress_google_lat", "Latitude*", NOTNULL);
			$form->add_edit("posaddress_google_long", "Longitude*", NOTNULL);
		}

		$form->add_hidden("posaddress_google_precision", 1);
		$form->add_label("gmcheck", "");
	}
	else
	{
		$form->add_hidden("GM", "Google Map", RENDER_HTML);
		
		if ($address["country"] == 19 
			or $address["country"] == 41 
			or $address["country"] == 153)
		{
			$form->add_hidden("posaddress_google_lat", "");
			$form->add_hidden("posaddress_google_long", "");
		}
		else
		{
			$form->add_hidden("posaddress_google_lat", "", NOTNULL);
			$form->add_hidden("posaddress_google_long", "", NOTNULL);
		}
		$form->add_hidden("posaddress_google_precision", 1);
		$form->add_hidden("gmcheck");
	}
}
else
{
	$form->add_hidden("shop_address_company");
	$form->add_hidden("shop_address_company2");
	$form->add_hidden("shop_address_address");
	$form->add_hidden("shop_address_address2");
	$form->add_hidden("shop_address_zip");
	$form->add_hidden("shop_address_place");
	$form->add_hidden("shop_address_place_id");
	$form->add_hidden("shop_address_country");
	$form->add_hidden("shop_address_phone");
	$form->add_hidden("shop_address_mobile_phone");
	$form->add_hidden("shop_address_email");
	//$form->add_edit("shop_address_contact_name");
	$form->add_hidden("posaddress_google_lat");
	$form->add_hidden("posaddress_google_long");
	$form->add_hidden("posaddress_google_precision");
	$form->add_hidden("GM");
}


$form->add_section(" ");


if(param("project_kind") == 5) //Lease Negotiaion
{
	$form->add_button("step4", "Proceed to next step");
}
elseif(param("project_kind") == 8) //Pop Up
{
	$form->add_button("step3", "Proceed to next step");
}
else {
	$form->add_button("step2", "Proceed to next step");
}

if(isset($_SESSION["new_project_step_0"]["cid"]))
{
	$form->add_button("backto_pos_config", "Back to POS Configuration");
}

$form->add_button("reset", "Clear all Data (reset form)");



if ($address["country"] != 19 
    and $address["country"] != 41 
	and $address["country"] != 153)
{

	//Google Map is blocked in China, Irak and Jordan
	$form->add_validation("{gmcheck}", "You have not checked the correctness of the map information!");
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$form->populate();
$form->process();

//clean fields from apostrophes
foreach($form->items as $key=>$value)
{
	if(array_key_exists('value', $value)) {
		$form->value($key, str_replace('"', '', $form->value($key)));
	}
}


if ($form->button("backto_pos_config"))
{
	redirect("project_new_00.php?id=" . $_SESSION["new_project_step_0"]["cid"]);
}
elseif($form->button("reset"))
{
	$_SESSION["new_project_step_1"] = array();
	$_SESSION["new_project_step_2"] = array();
	$_SESSION["new_project_step_3"] = array();
	$_SESSION["new_project_step_4"] = array();

	$link = "project_new_01.php";
	redirect($link);
}
elseif ($form->button("step2"))
{
	
	if ($address["country"] != 19 
		and $address["country"] != 41 
		and $address["country"] != 153)
	{
		$form->add_validation("{posaddress_google_lat}", "Google Map Coordinates must be indicated!");
		$form->add_validation("{posaddress_google_long}", "Google Map Coordinates must be indicated!");
	}

	$error = 0;
	

	$form->value("shop_address_address", $form->unify_multi_edit_field($form->items["shop_street"], get_country_street_number_rule($form->value("shop_address_country"))));

	$_POST["shop_address_address"] = $form->unify_multi_edit_field($form->items["shop_street"], get_country_street_number_rule($form->value("shop_address_country")));
	$_POST["shop_street"] = trim($form->items['shop_street']['values'][0]);
	$_POST["shop_streetnumber"] = trim($form->items['shop_street']['values'][1]);

	
	if(param("project_kind") != 1111 and param("project_kind") != 6666) {
		$form->value("shop_address_phone", $form->unify_multi_edit_field($form->items["shop_phone_number"]));
		$form->value("shop_address_mobile_phone", $form->unify_multi_edit_field($form->items["shop_mobile_phone_number"]));

		if(param("project_kind") != 1 and param("project_kind") != 6)
		{
			//$form->add_validation("{shop_address_phone} != '' or {shop_address_mobile_phone} != ''", "Please indcate either the phone number or the mobile phone number!");
		}
	}

	if(param("project_kind") != 1111 and param("project_kind") != 6666) {
		$_POST["shop_address_phone"] = $form->unify_multi_edit_field($form->items["shop_phone_number"]);
		$_POST["shop_phone_country"] = trim($form->items['shop_phone_number']['values'][0]);
		$_POST["shop_phone_area"] = trim($form->items['shop_phone_number']['values'][1]);
		$_POST["shop_phone_number"] = trim($form->items['shop_phone_number']['values'][2]);
		
		$_POST["shop_address_mobile_phone"] = $form->unify_multi_edit_field($form->items["shop_mobile_phone_number"]);
		$_POST["shop_mobile_phone_country"] = trim($form->items['shop_mobile_phone_number']['values'][0]);
		$_POST["shop_mobile_phone_area"] = trim($form->items['shop_mobile_phone_number']['values'][1]);
		$_POST["shop_mobile_phone_number"] = trim($form->items['shop_mobile_phone_number']['values'][2]);
	}
	else {
		$_POST["shop_address_phone"] = "";
		$_POST["shop_phone_country"] = "";
		$_POST["shop_phone_area"] = "";
		$_POST["shop_phone_number"] = "";
		
		$_POST["shop_address_mobile_phone"] = "";
		$_POST["shop_mobile_phone_country"] = "";
		$_POST["shop_mobile_phone_area"] = "";
		$_POST["shop_mobile_phone_number"] = "";
	}
	
	if(strlen(preg_replace('/[^0-9a-z_ %\[\]\.\(\)%&-]/s', '', $form->value("shop_address_address"))) < strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("shop_address_address")))
		and strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("shop_address_address"))) > 10)
	{
		$error = 1;
		$form->error("Project names and addresses must contain lower caracters. Do not use capital letters only.");
	}
	elseif(strlen(preg_replace('/[^0-9a-z_ %\[\]\.\(\)%&-]/s', '', $form->value("shop_address_address2"))) < strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("shop_address_address2")))
		and strlen(preg_replace('/[^A-Z%\[\]\.\(\)%&-]/s', '',  $form->value("shop_address_address2"))) > 10)
	{
		$error = 1;
		$form->error("Project names and addresses must contain lower caracters. Do not use capital letters only.");
	}

	
	if($error == 0 and $form->validate())
	{
		if(array_key_exists("new_place_inserted", $_SESSION["new_project_step_1"]))
		{
			$_SESSION["new_project_step_1"] = $_POST;
			$_SESSION["new_project_step_1"]["new_place_inserted"] = 1;
		}
		else
		{
			$_SESSION["new_project_step_1"] = $_POST;
		}

		if(!array_key_exists("new_place_inserted", $_SESSION["new_project_step_1"]))
		{
			$_SESSION["new_project_step_1"]["new_place_inserted"] = 1;
			add_new_province_and_place();
		}
		
		$link = "project_new_02.php";
		redirect($link);
	}
}
elseif ($form->button("step4"))
{
	
	
	
	$form->add_validation("{posaddress_google_lat}", "Google Map Coordinates must be indicated!");
	$form->add_validation("{posaddress_google_long}", "Google Map Coordinates must be indicated!");
	
	if($form->validate())
	{
		$form->value("shop_address_address", $form->unify_multi_edit_field($form->items["shop_street"], get_country_street_number_rule($form->value("shop_address_country"))));
		
		if(param("project_kind") != 1111 and param("project_kind") != 6666) {
			$form->value("shop_address_phone", $form->unify_multi_edit_field($form->items["shop_phone_number"]));
			$form->value("shop_address_mobile_phone", $form->unify_multi_edit_field($form->items["shop_mobile_phone_number"]));

			if(param("project_kind") != 1 and param("project_kind") != 6)
			{
				//$form->add_validation("{shop_address_phone} != '' or {shop_address_mobile_phone} != ''", "Please indcate either the phone number or the mobile phone number!");
			}
		}


		$_POST["shop_address_address"] = $form->unify_multi_edit_field($form->items["shop_street"], get_country_street_number_rule($form->value("shop_address_country")));
		$_POST["shop_street"] = trim($form->items['shop_street']['values'][0]);
		$_POST["shop_streetnumber"] = trim($form->items['shop_street']['values'][1]);
		
		
		if(param("project_kind") != 1111 and param("project_kind") != 6666) {
			$_POST["shop_address_phone"] = $form->unify_multi_edit_field($form->items["shop_phone_number"]);
			$_POST["shop_phone_country"] = trim($form->items['shop_phone_number']['values'][0]);
			$_POST["shop_phone_area"] = trim($form->items['shop_phone_number']['values'][1]);
			$_POST["shop_phone_number"] = trim($form->items['shop_phone_number']['values'][2]);
			
			$_POST["shop_address_mobile_phone"] = $form->unify_multi_edit_field($form->items["shop_mobile_phone_number"]);
			$_POST["shop_mobile_phone_country"] = trim($form->items['shop_mobile_phone_number']['values'][0]);
			$_POST["shop_mobile_phone_area"] = trim($form->items['shop_mobile_phone_number']['values'][1]);
			$_POST["shop_mobile_phone_number"] = trim($form->items['shop_mobile_phone_number']['values'][2]);
		}
		else {
			$_POST["shop_address_phone"] = "";
			$_POST["shop_phone_country"] = "";
			$_POST["shop_phone_area"] = "";
			$_POST["shop_phone_number"] = "";
			
			$_POST["shop_address_mobile_phone"] = "";
			$_POST["shop_mobile_phone_country"] = "";
			$_POST["shop_mobile_phone_area"] = "";
			$_POST["shop_mobile_phone_number"] = "";
		}

		
		if(array_key_exists("new_place_inserted", $_SESSION["new_project_step_1"]))
		{
			$_SESSION["new_project_step_1"] = $_POST;
			$_SESSION["new_project_step_1"]["new_place_inserted"] = 1;
		}
		else
		{
			$_SESSION["new_project_step_1"] = $_POST;
		}

		if(!array_key_exists("new_place_inserted", $_SESSION["new_project_step_1"]))
		{
			$_SESSION["new_project_step_1"]["new_place_inserted"] = 1;
			add_new_province_and_place();
		}
		
		$link = "project_new_04.php";
		redirect($link);
	}
}
elseif ($form->button("step3"))
{
	$form->add_validation("{posaddress_google_lat}", "Google Map Coordinates must be indicated!");
	$form->add_validation("{posaddress_google_long}", "Google Map Coordinates must be indicated!");
	
	if($form->validate())
	{
		$form->value("shop_address_address", $form->unify_multi_edit_field($form->items["shop_street"], get_country_street_number_rule($form->value("shop_address_country"))));
		
		if(param("project_kind") != 1111 and param("project_kind") != 6666) {
			$form->value("shop_address_phone", $form->unify_multi_edit_field($form->items["shop_phone_number"]));
			$form->value("shop_address_mobile_phone", $form->unify_multi_edit_field($form->items["shop_mobile_phone_number"]));
			
			if(param("project_kind") != 1 and param("project_kind") != 6)
			{
				//$form->add_validation("{shop_address_phone} != '' or {shop_address_mobile_phone} != ''", "Please indcate either the phone number or the mobile phone number!");
			}
		}

		$_POST["shop_address_address"] = $form->unify_multi_edit_field($form->items["shop_street"], get_country_street_number_rule($form->value("shop_address_country")));
		$_POST["shop_street"] = trim($form->items['shop_street']['values'][0]);
		$_POST["shop_streetnumber"] = trim($form->items['shop_street']['values'][1]);
		
		if(param("project_kind") != 1111 and param("project_kind") != 6666) {
			$_POST["shop_address_phone"] = $form->unify_multi_edit_field($form->items["shop_phone_number"]);
			$_POST["shop_phone_country"] = trim($form->items['shop_phone_number']['values'][0]);
			$_POST["shop_phone_area"] = trim($form->items['shop_phone_number']['values'][1]);
			$_POST["shop_phone_number"] = trim($form->items['shop_phone_number']['values'][2]);
			
			$_POST["shop_address_mobile_phone"] = $form->unify_multi_edit_field($form->items["shop_mobile_phone_number"]);
			$_POST["shop_mobile_phone_country"] = trim($form->items['shop_mobile_phone_number']['values'][0]);
			$_POST["shop_mobile_phone_area"] = trim($form->items['shop_mobile_phone_number']['values'][1]);
			$_POST["shop_mobile_phone_number"] = trim($form->items['shop_mobile_phone_number']['values'][2]);
		}
		else {
			$_POST["shop_address_phone"] = "";
			$_POST["shop_phone_country"] = "";
			$_POST["shop_phone_area"] = "";
			$_POST["shop_phone_number"] = "";
			
			$_POST["shop_address_mobile_phone"] = "";
			$_POST["shop_mobile_phone_country"] = "";
			$_POST["shop_mobile_phone_area"] = "";
			$_POST["shop_mobile_phone_number"] = "";
		}

		
		if(array_key_exists("new_place_inserted", $_SESSION["new_project_step_1"]))
		{
			$_SESSION["new_project_step_1"] = $_POST;
			$_SESSION["new_project_step_1"]["new_place_inserted"] = 1;
		}
		else
		{
			$_SESSION["new_project_step_1"] = $_POST;
		}

		if(!array_key_exists("new_place_inserted", $_SESSION["new_project_step_1"]))
		{
			$_SESSION["new_project_step_1"]["new_place_inserted"] = 1;
			add_new_province_and_place();
		}
		
		if(count($poslocation) > 0 and array_key_exists("posaddress_store_postype", $poslocation))
		{
			$_SESSION["new_project_step_1"]["project_postype"] = $poslocation["posaddress_store_postype"];
			$link = "project_new_03.php";
			redirect($link);
		}
	}
}
elseif($form->button("project_kind"))
{
	$form->value("posaddress_id", 0);
	$form->value("shop_address_company", "");
	$form->value("shop_address_company2", "");
	$form->value("shop_address_address", "");
	if(array_key_exists('shop_street', $form->items))
	{
		$form->items['shop_street']['values'][0] = "";
		$form->items['shop_street']['values'][1] = "";
	}
	$form->value("shop_address_address2", "");
	$form->value("shop_address_zip", "");
	$form->value("shop_address_place", "");
	//$form->value("shop_address_place_id", "");
	
	
	
	if(param("project_kind") == 1 or param("project_kind") == 6) // new POS, relocation
	{
		$form->value("shop_address_country", $address["country"]);
	}
	else {
		$form->value("shop_address_country", 0);
	}

	
	
	if(isset($form->items["shop_address_province"]))
	{
		$form->value("shop_address_province", "");
		$form->value("select_shop_address_province", "");
	}
	
	if(isset($form->items["label_shop_address_place"]))
	{
		$form->value("label_shop_address_place", "");
		$form->value("label_shop_address_country", "");
	}
	$form->value("shop_address_phone", "");
	if(array_key_exists('shop_phone_number', $form->items))
	{
		$form->items['shop_phone_number']['values'][0] = "";
		$form->items['shop_phone_number']['values'][1] = "";
		$form->items['shop_phone_number']['values'][2] = "";
	}
	$form->value("shop_address_mobile_phone", "");
	if(array_key_exists('shop_mobile_phone_number', $form->items))
	{
		$form->items['shop_mobile_phone_number']['values'][0] = "";
		$form->items['shop_mobile_phone_number']['values'][1] = "";
		$form->items['shop_mobile_phone_number']['values'][2] = "";
	}
	$form->value("shop_address_email", "");
	//$form->value("shop_address_contact_name", "");
	$form->value("posaddress_google_lat", "");
	$form->value("posaddress_google_long", "");
	$form->value("posaddress_google_precision", 0);
	$form->value("GM", "");
	$form->value("old_project_kind", $form->value("project_kind"));

	if(isset($form->items["pln"]))
	{
		$form->value("pln", "");
		$form->value("ptn", "");
	}
	
}
elseif($form->button("project_cost_type"))
{
	$_SESSION["new_project_step_1"] = array();
	$link = 'project_new_01.php?project_cost_type=' . $form->value("project_cost_type");
	redirect($link);
}
elseif($form->button("posaddress_id"))
{
	if(array_key_exists('posaddress_name', $poslocation))
	{
		$form->value("shop_address_company", $poslocation["posaddress_name"]);
		$form->value("shop_address_company2", "", 0, "");
		$form->value("shop_address_address", $poslocation["posaddress_address"]);

		if(array_key_exists('shop_street', $form->items))
		{
			$form->items['shop_street']['values'][0] = $poslocation["posaddress_street"];
			$form->items['shop_street']['values'][1] = $poslocation["posaddress_street_number"];
		}

		$form->value("shop_address_address2", $poslocation["posaddress_address2"]);
		$form->value("shop_address_zip", $poslocation["posaddress_zip"]);
		$form->value("shop_address_place", $poslocation["posaddress_place"]);
		$form->value("shop_address_place_id", $poslocation["posaddress_place_id"]);
		$form->value("shop_address_country", $poslocation["posaddress_country"]);

		$form->value("label_shop_address_place", $poslocation["posaddress_place"]);
		$form->value("label_shop_address_country", $poslocation["country_name"]);

		$form->value("shop_address_phone", $poslocation["posaddress_phone"]);
		if(array_key_exists('shop_phone_number', $form->items))
		{
			$form->items['shop_phone_number']['values'][0] = $poslocation["posaddress_phone_country"];
			$form->items['shop_phone_number']['values'][1] = $poslocation["posaddress_phone_area"];
			$form->items['shop_phone_number']['values'][2] = $poslocation["posaddress_phone_number"];
		}
		$form->value("shop_address_mobile_phone", $poslocation["posaddress_mobile_phone"]);
		if(array_key_exists('shop_mobile_phone_number', $form->items))
		{
			$form->items['shop_mobile_phone_number']['values'][0] = $poslocation["posaddress_mobile_phone_country"];
			$form->items['shop_mobile_phone_number']['values'][1] = $poslocation["posaddress_mobile_phone_area"];
			$form->items['shop_mobile_phone_number']['values'][2] = $poslocation["posaddress_mobile_phone_number"];
		}
		$form->value("shop_address_email", $poslocation["posaddress_email"]);
		//$form->value("shop_address_contact_name", $poslocation["posaddress_contact_name"]);
		$form->value("posaddress_google_lat",  $poslocation["posaddress_google_lat"]);
		$form->value("posaddress_google_long",  $poslocation["posaddress_google_long"]);
		$form->value("posaddress_google_precision",  $poslocation["posaddress_google_precision"]);

		if($poslocation["posaddress_country"] and $poslocation["posaddress_place"] and $poslocation["posaddress_address"])
		{
			$googlemaplink = "Click <a href='javascript:popup(\"/pos/project_new_map.php?id=" . $form->value("posaddress_id") . "&c=" . $poslocation["posaddress_country"] . "&p=" . $poslocation["posaddress_place"] . "&a=" . str_replace("'", "\'",$poslocation["posaddress_street"] . " " . $poslocation["posaddress_street_number"]) . "\", 700,650)'>here</a> to verify the geografical position of the POS.";
		}
		elseif($poslocation["posaddress_country"] and $poslocation["posaddress_place"])
		{
			$googlemaplink = "Click <a href='javascript:popup(\"/pos/project_new_map.php?id=" . $form->value("posaddress_id") . "&c=" . $poslocation["posaddress_country"] . "&p=" . $poslocation["posaddress_place"] . "&a=\", 700,650)'>here</a> to verify the geografical position of the POS.";
		}
		elseif($poslocation["posaddress_country"])
		{
			$googlemaplink = "Click <a href='javascript:popup(\"/pos/project_new_map.php?id=" . $form->value("posaddress_id") . "&c=" . $poslocation["posaddress_country"] . "&p=&a=\", 700,650)'>here</a> to verify the geografical position of the POS.";
		}

		$sql = "select place_province, province_canton " .
			   "from places " .
			   "left join provinces on province_id = place_province " . 
			   "where place_id = " . dbquote($poslocation["posaddress_place_id"]);
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$form->value("select_shop_address_province",  $row["place_province"]);
			$form->value("shop_address_province", $row["province_canton"]);
		}

		if($form->value("project_kind") == 4 or $form->value("project_kind") == 5) // take over, lease negotioation
		{
			$form->value("product_line", $poslocation["posaddress_store_furniture"]);
			$form->value("project_postype", $poslocation["posaddress_store_postype"]);
			$form->value("project_pos_subclass", $poslocation["posaddress_store_subclass"]);

			$form->value("pln", $poslocation_product_line_name);
			$form->value("ptn", $poslocation_postype_name);

		}
	}

	
}
elseif($form->button("project_relocated_posaddress_id"))
{
	$sql = "select * ".
			   "from posaddresses ".
			   "left join countries on country_id = posaddress_country " .
			   "left join addresses on address_id = posaddress_client_id " .
		       "left join product_lines on product_line_id = posaddress_store_furniture " . 
		       "left join postypes on postype_id = posaddress_store_postype " . 
			   "where posaddress_id  = " . dbquote(param("project_relocated_posaddress_id"));


	$res = mysql_query($sql) or dberror($sql);

	if ($row = mysql_fetch_assoc($res))
	{
		
		$form->value("shop_address_place", $row["posaddress_place"]);
		$form->value("select_shop_address_place", $row["posaddress_place_id"]);
		$form->value("shop_address_country", $row["posaddress_country"]);
		$form->value("shop_address_email", $row["posaddress_email"]);
		$form->value("posaddress_google_lat",  $row["posaddress_google_lat"]);
		$form->value("posaddress_google_long",  $row["posaddress_google_long"]);
		$form->value("posaddress_google_precision",  $row["posaddress_google_precision"]);
	}
	
}
elseif($form->button("shop_address_country"))
{
	$form->value("shop_address_place",  "");
	$form->value("shop_address_zip",  "");
	$form->value("shop_address_province", "");
	$form->value("select_shop_address_province", "");
}
elseif($form->button("select_shop_address_place"))
{
	if($form->value("select_shop_address_place") == 999999999 or $form->value("select_shop_address_place") == 0)
	{
		$form->value("shop_address_place",  "");
		$form->value("shop_address_zip",  "");
		$form->value("shop_address_province", "");
		$form->value("select_shop_address_province", "");
	}
	else
	{
		$sql = "select place_name from places where place_id = " . dbquote($form->value("select_shop_address_place"));
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$form->value("shop_address_place",  $row["place_name"]);
			$form->value("shop_address_zip",  "");
		}
		$sql = "select place_province, province_canton " .
			   "from places " .
			   "left join provinces on province_id = place_province " . 
			   "where place_id = " . dbquote(param("select_shop_address_place"));
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$form->value("select_shop_address_province",  $row["place_province"]);
			$form->value("shop_address_province", $row["province_canton"]);
		}
	}
}
elseif($form->button("select_shop_address_province"))
{
	if($form->value("select_shop_address_province") == 999999999 or $form->value("select_shop_address_province") == 0)
	{
		$form->value("shop_address_province", "");
	}
	else
	{
		$sql = "select province_canton " .
			   "from provinces " .
			   "where province_id = " . dbquote(param("select_shop_address_province"));
		$res = mysql_query($sql) or dberror($sql);
		if ($row = mysql_fetch_assoc($res))
		{
			$form->value("shop_address_province", $row["province_canton"]);
		}
	}
}
if($form->button("project_kind") and isset($_SESSION["new_project_step_1"]["project_kind"]) and $_SESSION["new_project_step_1"]["project_kind"] > 0)
{
	unset($_SESSION["new_project_step_2"]);
	unset($_SESSION["new_project_step_3"]);
	unset($_SESSION["new_project_step_4"]);
	unset($_SESSION["new_project_step_5"]);
}



if(array_key_exists('shop_street', $form->items))
{
	$tmp = $form->items['shop_street']['values'][0] . " ". $form->items['shop_street']['values'][1];
	$form->value("shop_address_address", $tmp);
}

if($form->value("shop_address_country") and $form->value("shop_address_place") and $form->value("shop_address_address"))
{
	
	if($form->value("posaddress_id") > 0)
	{
		$googlemaplink = "Click <a href=\"javascript:popup('/pos/project_new_map.php?id=" . $form->value("posaddress_id") . "&c=" . $form->value("shop_address_country") . "&p=" . str_replace("'", "\'", $form->value("shop_address_place")) . "&a=" . str_replace("'", "\'", $form->value("shop_address_address")) . "', 700,650);\">here</a> to verify the geografical position of the POS.";
	}
	else
	{
		$googlemaplink = "Click <a href=\"javascript:popup('/pos/project_new_map.php?c=" . $form->value("shop_address_country") . "&p=" . str_replace("'", "\'", $form->value("shop_address_place")) . "&a=" . str_replace("'", "\'",$form->value("shop_address_address")) . "', 700,650);\">here</a> to choose the geografical position of the POS.";
	}
}
elseif($form->value("shop_address_country") and $form->value("shop_address_place"))
{
	if($form->value("posaddress_id") > 0)
	{
		$googlemaplink = "Click <a href=\"javascript:popup('/pos/project_new_map.php?id=" . $form->value("posaddress_id") . "&c=" . $form->value("shop_address_country") . "&p=" . str_replace("'", "\'", $form->value("shop_address_place")) . "&a=', 700,650);\">here</a> to verify the geografical position of the POS.";
	}
	else
	{
		$googlemaplink = "Click <a href=\"javascript:popup('/pos/project_new_map.php?c=" . $form->value("shop_address_country") . "&p=" . str_replace("'", "\'", $form->value("shop_address_place")) . "&a=', 700,650);\">here</a> to choose the geografical position of the POS.";
	}
}
elseif($form->value("shop_address_country") and $param_country_is_submitted == true)
{
	if($form->value("posaddress_id") > 0)
	{
		$googlemaplink = "Click <a href=\"javascript:popup('/pos/project_new_map.php?id=" . $form->value("posaddress_id") . "&c=" . $form->value("shop_address_country") . "&p=&a=', 700,650);\">here</a> to verify the geografical position of the POS.";
	}
	elseif(param("project_kind") == 2 and !param("posaddress_id"))
	{
		$googlemaplink = "";
	}
	else
	{
		$googlemaplink = "Click <a href=\"javascript:popup('/pos/project_new_map.php?c=" . $form->value("shop_address_country") . "&p=&a=', 700,650);\">here</a> to choose the geografical position of the POS.";
	}


}
elseif(!isset($googlemaplink))
{
	$googlemaplink = "";
}

$form->value("GM", $googlemaplink);
   
/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

$page->register_action('home', 'Home', "welcome.php");

$page->header();
$page->title("Project Request");

echo "<div>";
echo "<img class='stepcounter' src='/pictures/numbers/01_on.gif' alt=\"\" />";
echo "<span class='step_inactive'>Basic Project Data</span>";

if(param("project_kind") == 4) // Take over
{
	echo "<img class='stepcounter' src='/pictures/numbers/02_off.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Addresses</span>";
	echo "<img class='stepcounter' src='/pictures/numbers/03_off.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Environment</span>";
}
elseif(param("project_kind") == 5) // lease renewal
{
	echo "<img class='stepcounter' src='/pictures/numbers/02_off.gif' alt=\"\" />";
		echo "<span class='step_inactive'>Environment</span>";
}
elseif(param("project_kind") == 8) // popup
{
	echo "<img class='stepcounter' src='/pictures/numbers/02_off.gif' alt=\"\" />";
	echo "<span class='step_inactive'>PopUp Information</span>";
	echo "<img class='stepcounter' src='/pictures/numbers/03_off.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Comments</span>";
}
else
{
	echo "<img class='stepcounter' src='/pictures/numbers/02_off.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Addresses</span>";
	echo "<img class='stepcounter' src='/pictures/numbers/03_off.gif' alt=\"\" />";
	echo "<span class='step_inactive'>POS Information</span>";
	echo "<img class='stepcounter' src='/pictures/numbers/04_off.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Environment</span>";
	echo "<img class='stepcounter' src='/pictures/numbers/05_off.gif' alt=\"\" />";
	echo "<span class='step_inactive'>Comments</span>";
}
echo "<br /><br /><br /></div>";



$form->render();

?>

<div id="ltype" style="display:none;">
    <ul class="tiplist">
	<li><strong>Corporate</strong><br />POS is fully owned and operated by SG country</li>
	<li><strong>Franchisee</strong><br />POS is fully owned and operated by a third party</li>
	<li><strong>Other</strong><br />For non corporate SIS, operated by third party</li>
	</ul>
</div> 

<div id="pkind" style="display:none;">
    <ul class="tiplist">
	<li><strong>New POS</strong><br />In case you are building a new POS</li>
	<li><strong>Renovation/Rebuilding</strong><br />In case you are renovating an existing POS</li>
	<?php
	if(param("project_cost_type") == 1 or param("project_cost_type") == 3 or param("project_cost_type") == 4) // Corporate Store, joint venture or cooperation 3rd party
	{
	?>
		<li><strong>Take over/Renovation</strong><br />In case you are taking over and renovating an existing POS</li>
		<li><strong>Take over</strong><br />In case you are taking over an existing POS without renovating it</li>
		<li><strong>Lease Renewal</strong><br />In case you only are going to renew the lease contract</li>
		<li><strong>PopUp</strong><br />In case you are doing a PopUp in an existing POS</li>
	<?php
	}
	?>
	</ul>
</div>


<div id="ptype" style="display:none;">
    <ul class="tiplist">
	<li><strong>Store</strong><br />Closed area with own entrance</li>
	<li><strong>SIS</strong><br />Shop in Shop: open space in Department store</li>
	<li><strong>Kiosk</strong><br />Freestanding, closed space in Airport, Train station, Shopping Mall</li>
	</ul>
</div> 

<div id="sclass" style="display:none;">
    Choose only if applicable, otherwise leave empty
</div> 


<div id="poslocation" style="display:none;">
    The POS location will be published on tissot.ch. Therefore it is important to follow a naming convention. The reason is that customers can easily find the POS Location. So we need the name of the shopping mall or the street's name or the area's name or the airpots'name AND the city. Please enter the projects's name as follows:
	<ul class="tiplist">
	<li><strong>Shopping Mall Example</strong>:<br />Shopping Mall Carabou, Madrid</li>
	<li><strong>Airport Example</strong><br />Int. Airport, Pantown</li>
	<li><strong>Area Example</strong><br />Westside, Pantown</li>
	<li><strong>Street Example</strong><br />Nassaustreet, Pantown</li>
	</ul>
</div> 

<div id="phone" style="display:none;">
    Please indicate the phone number according to international conventions like:<br />
	<strong>+41 61 926 67 00</strong><br />use only the 'plus sign', 'blank' and 'numbers'.
</div> 

<div id="mobile_phone" style="display:none;">
    Please indicate the mobile_phone umber according to international conventions like:<br />
	<strong>+41 61 926 67 00</strong><br />use only the 'plus sign', 'blank' and 'numbers'.
</div> 

<div id="city" style="display:none;">
    Please indicate the new city's name in English!
</div> 

<div id="province" style="display:none;">
    Please indicate the new province's name in English!
</div> 

<?php

$page->footer();


?>