<?php
/********************************************************************

    order_new.php

    Category selection when creating new orders.

    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date created:   2002-07-31
    Modified by:    Anton.Hofmann (aho@mediaparx.ch)
    Date modified:  2003-07-14
    Version:        1.0.2

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/order_functions.php";
require_once "include/get_functions.php";

check_access("can_create_new_orders");

set_referer("orders.php");
set_referer("basket.php");
set_referer("order_category.php");
set_referer("order_new_special_item.php");

$page = new Page("orders");
if (!basket_is_empty())
{
    $page->register_action('basket', 'Shopping List');
}

/********************************************************************
    prepare all data needed
*********************************************************************/
// get the region of users' address
$user = get_user(user_id());
$user_country = $user["country"];
$user_region = get_user_region(user_id());

$sql = "select DISTINCT concat(supplying_group_id, product_line_id, item_category_id, item_subcategory) as pcid, " .
       " concat(product_line_name, ' - ', supplying_group_name) as pl_sg, " . 
       " supplying_group_id, supplying_group_name, product_line_id, item_category_id, item_subcategory_id, " .
       " product_line_name, item_category_name, item_subcategory_name, item_subcategory " . 
       " from product_lines " .
	   "INNER JOIN product_line_supplying_groups ON product_line_supplying_group_line_id = product_line_id
	    INNER JOIN item_supplying_groups ON item_supplying_group_supplying_group_id = product_line_supplying_group_group_id
		INNER JOIN items ON item_id = item_supplying_group_item_id
		INNER JOIN supplying_groups on supplying_group_id = item_supplying_group_supplying_group_id 
		INNER JOIN productline_regions ON productline_region_productline = product_line_id
		INNER JOIN item_categories ON item_category_id = item_category
		LEFT JOIN item_subcategories on item_subcategory_id = item_subcategory
		INNER JOIN item_countries ON item_country_item_id = item_id";



if(has_access("can_edit_catalog") or has_access("can_enter_orders_for_other_companies"))
{
   $list_filter = "(product_line_visible = 1 
					 or product_line_budget = 1)
					 and product_line_active = 1
					 and item_category_active = 1 
					 and item_active = 1
					 and item_type = 1";
}
else
{
    $list_filter = "product_line_visible = 1 
					 and product_line_active = 1
					 and item_category_active = 1
					 and item_visible = 1 
					 and item_active = 1
					 and item_type = 1 
					 and item_country_country_id = " . dbquote($user_country) . 
					 " and productline_region_region = " . $user_region;
}



$links = array();
$sql_tmp = $sql . " where " . $list_filter;
$res = mysql_query($sql_tmp) or dberror($sql_tmp);
while ($row = mysql_fetch_assoc($res))
{
	
	if($row["item_subcategory"] > 0)
	{
		$links[$row["pcid"]] = '<a href="order_category.php?plid=' . $row["product_line_id"] . '&cid=' . $row["item_category_id"] . '&scid=' . $row["item_subcategory"] . '&sg=' . $row["supplying_group_id"] . '">' . $row["item_category_name"] . ' - '. $row["item_subcategory_name"] . '</a>';
	}
	else
	{
		$links[$row["pcid"]] = '<a href="order_category.php?plid=' . $row["product_line_id"] . '&cid=' . $row["item_category_id"] . '&scid=0' . '&sg=' . $row["supplying_group_id"] . '">' . $row["item_category_name"] . '</a>';
	}

}


/********************************************************************
    Search Form
*********************************************************************/ 
$form = new Form("orders", "order");
$form->add_comment("Enter a search term or choose from the list of item categories below.<br /><br />");
$form->add_edit("searchterm", "Search Term");

$form->add_button("search", "Search Catalog");
$form->add_button(LIST_BUTTON_BACK, "Back");
$form->add_button("cancel", "Cancel Ordering");

if(has_access("can_edit_catalog") or has_access("can_order_special_items"))
{
	$form->add_button("special_item", "Order A Special Item");
}

/********************************************************************
    Create List of product categories
*********************************************************************/ 
$list1 = new ListView($sql, LIST_HAS_HEADER);

$list1->set_entity("items");
$list1->set_order("product_line_name, supplying_group_name, item_category_name");
$list1->set_group("pl_sg");
$list1->set_title("Item Categories");
$list1->set_filter($list_filter);

$list1->add_text_column("category", "Item Category", COLUMN_UNDERSTAND_HTML, $links);


/********************************************************************
    Populate page
*********************************************************************/ 
$form->populate();
$form->process();


$list1->populate();
$list1->process();


if ($form->button("basket"))
{
    redirect("basket.php");
}
elseif ($form->button("cancel"))
{
    redirect("orders.php");
}
elseif ($form->button("search"))
{
	if(!$form->value("searchterm"))
	{
		$form->error("The search term must not be empty.");
	}
	else
	{
		$link = "order_new_select.php?searchterm=" . str_replace(' ', '', $form->value("searchterm"));
		redirect($link);
	}
}
elseif($form->button("special_item"))
{
	redirect("order_new_special_item.php");
}

$page->register_action('home', 'Home', "welcome.php");

$page->header();
$page->title('New Catalog Order');
$form->render();

echo '<br /><br />';
$list1->render();


?>

<script type="text/javascript">
	$(document).keypress(function(e) {
		if(e.which == 13) {
			
			if($("#searchterm").val())
			{
				button("search");
			}
		}
	});
</script>

<?php



$page->footer();

?>