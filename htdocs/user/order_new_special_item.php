<?php
/********************************************************************

    order_new_special_item.php

    Order a special item

    Modified by:    Anton.Hofmann (aho@mediaparx.ch)
    Date created:   2016-03-02
    Modified by:    Anton.Hofmann (aho@mediaparx.ch)
    Date modified:  2016-03-02
    Version:        1.0.0

    Copyright (c) 2016, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/order_functions.php";
require_once "include/get_functions.php";


if(!has_access("can_edit_catalog") and !has_access("can_enter_orders_for_other_companies"))
{
	redirect("noaccess.php");
}

$page = new Page("catalog");
if (!basket_is_empty())
{
    $page->register_action('basket', 'Shopping List');
}

/********************************************************************
    prepare all data needed
*********************************************************************/

$supplier_currency_symbol = "";
if(param("basket_special_item_supplier"))
{
	$supplier_currency = get_address_currency(param("basket_special_item_supplier"));
	$supplier_currency_symbol = $supplier_currency["symbol"];
}


// create sql for the supplier listbox
$sql_suppliers = "select address_id, address_company ".
                 "from addresses ".
                 "where address_type = 2 and address_active = 1 ".
                 "order by address_company";


if(param("basket_special_item_supplier") > 0)
{
	$supplier_currencies = get_suplier_currencies(param("basket_special_item_supplier"));


	$sql_supplier_currencies = "select currency_id, currency_symbol " . 
							   " from currencies " . 
							   " where currency_id in ( " . implode(',', $supplier_currencies) . ") " . 
							   " order by currency_symbol";
}
else
{
	$sql_supplier_currencies = "select currency_id, currency_symbol " . 
							   " from currencies " . 
							   " where currency_id = 0";
}


/********************************************************************
    Search Form
*********************************************************************/ 
$form = new Form("basket_special_items", "basket_special_items");


$form->add_section("Item Information");

$form->add_hidden("basket_special_item_basket", get_users_basket());
$form->add_list("basket_special_item_supplier", "Supplier*", $sql_suppliers, SUBMIT | NOTNULL);
$form->add_multiline("basket_special_item_description", "Description*", 4, NOTNULL);


$form->add_section("Quantity and Price");
$form->add_edit("basket_special_item_quantity", "Quantity*", NOTNULL);
$form->add_list("basket_special_item_supplier_currency_id", "Currency*",$sql_supplier_currencies, NOTNULL);

if($supplier_currency["exchange_rate_furniture"] > 0)
{
	$form->add_edit("basket_special_item_supplier_exchange_rate", "Exchange Rate*", NOTNULL, $supplier_currency["exchange_rate_furniture"]);
}
else
{
	$form->add_edit("basket_special_item_supplier_exchange_rate", "Exchange Rate*", NOTNULL, $supplier_currency["exchange_rate"]);
}

$form->add_edit("basket_special_item__supplier_price", "Supplier's Price ");



$form->add_section("Supplying Options");
$form->add_checkbox("basket_special_item_no_offer_required", "does not require a supplier's offer", "", 0, "Options");
$form->add_checkbox("basket_special_item_only_quantity_proposal", "does only need a supplier's quantity proposal", "");




$form->add_button("cancel", "Cancel Ordering");
$form->add_button("back", "Back");
$form->add_button("add_to_shopping_list", "Add Item to Shopping List");


/********************************************************************
    Populate page
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button("back"))
{
    redirect("catalog.php");
}
elseif ($form->button("cancel"))
{
    redirect("orders.php");
}
elseif($form->button("basket_special_item_supplier"))
{
	$form->value("basket_special_item_supplier_currency_id",$supplier_currency["id"]);
	
	if($supplier_currency["exchange_rate_furniture"] > 0)
	{
		$form->value("basket_special_item_supplier_exchange_rate",$supplier_currency["exchange_rate_furniture"]);
	}
	else
	{
		$form->value("basket_special_item_supplier_exchange_rate",$supplier_currency["exchange_rate"]);
	}
}
elseif ($form->button("add_to_shopping_list"))
{
	$fields[] = "basket_special_item_basket";
	$values[] = dbquote($form->value("basket_special_item_basket"));
	$fields[] = "basket_special_item_supplier";
	$values[] = dbquote($form->value("basket_special_item_supplier"));
	$fields[] = "basket_special_item_description";
	$values[] = dbquote($form->value("basket_special_item_description"));
	$fields[] = "basket_special_item_quantity";
	$values[] = dbquote($form->value("basket_special_item_quantity"));

	$fields[] = "basket_special_item_supplier_currency_id";
	$values[] = dbquote($form->value("basket_special_item_supplier_currency_id"));

	$fields[] = "basket_special_item_supplier_exchange_rate";
	$values[] = dbquote($form->value("basket_special_item_supplier_exchange_rate"));

	$fields[] = "basket_special_item__supplier_price";
	$values[] = dbquote($form->value("basket_special_item__supplier_price"));

	$fields[] = "basket_special_item_no_offer_required";
	$values[] = dbquote($form->value("basket_special_item_no_offer_required"));

	$fields[] = "basket_special_item_only_quantity_proposal";
	$values[] = dbquote($form->value("basket_special_item_only_quantity_proposal"));


	$fields[] = "user_created";
	$values[] = dbquote(user_login());
	$fields[] = "date_created";
	$values[] = dbquote(date("Y-m-d H:i:s"));


	$sql = "insert into basket_special_items (" . join(',', $fields) .") " .
		   "values (" . join(',', $values) . ")";
	mysql_query($sql) or dberror($sql);

	redirect("basket.php");

}

$page->register_action('home', 'Home', "welcome.php");

$page->header();
$page->title('New Catalog Order - Order A Special Item');
$form->render();




$page->footer();

?>