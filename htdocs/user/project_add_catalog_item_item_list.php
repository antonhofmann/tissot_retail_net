<?php
/********************************************************************
    project_add_catalog_item_item_list.php

    Add items to the list of materials from category item list.

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-11
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-02-16
    Version:        1.0.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.
*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("has_access_to_list_of_materials_in_projects");

register_param("pid");
register_param("oid");
register_param("cid");

if(!array_key_exists("cid", $_GET))
{
	redirect("/page_not_found.php");
}
$tmp = explode ('-', $_GET["cid"]); 
$item_category_id = $_GET["cid"];


/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));

param("oid", $project["order_id"]);

$order = get_order(param("oid"));

// get company's address
$client_address = get_address($order["order_client_address"]);

// get category name
if($searchterm)
{
	$category_name = "Item List matching '" . $searchterm . "'";
}
else
{
	$category_name = get_category_name($plid, $cid, param("scid"));
}

$sql = "select DISTINCT item_id, item_category_id, item_category_name, item_code, item_name,item_price, ".
       " concat(address_company, ' - ', item_category_name) as group_head " . 
       " from product_lines " .
	   "INNER JOIN product_line_supplying_groups ON product_line_supplying_group_line_id = product_line_id
	    INNER JOIN item_supplying_groups ON item_supplying_group_supplying_group_id = product_line_supplying_group_group_id
		INNER JOIN items ON item_id = item_supplying_group_item_id
		INNER JOIN supplying_groups on supplying_group_id = item_supplying_group_supplying_group_id 
		INNER JOIN productline_regions ON productline_region_productline = product_line_id
		INNER JOIN item_categories ON item_category_id = item_category
		LEFT JOIN item_subcategories on item_subcategory_id = item_subcategory
		INNER JOIN item_countries ON item_country_item_id = item_id 
		LEFT join item_pos_types on item_pos_type_item = item_id 
		LEFT JOIN suppliers on supplier_item = item_id
	    LEFT JOIN addresses on address_id = supplier_address ";
	


if(param("type") and param("type") == 2)
{
	$list_filter = " item_category = " . dbquote($item_category_id) .
				   " and product_line_id = " . dbquote($project["project_product_line"]) .
				   " and product_line_budget = 1
					 and product_line_active = 1
					 and item_category_active = 1 
					 and item_active = 1
					 and item_visible_in_projects = 1
					 and item_type = 2 
					 and item_country_country_id = " . dbquote($project["order_shop_address_country"]);
}
else
{
	$list_filter = " item_category = " . dbquote($item_category_id) .
				   " and product_line_id = " . dbquote($project["project_product_line"]) .
				   " and product_line_budget = 1 
					 and product_line_active = 1
					 and item_category_active = 1 
					 and item_active = 1
					 and item_visible_in_projects = 1
					 and item_type = 1 
					 and item_country_country_id = " . dbquote($project["order_shop_address_country"]) . 
					 "    and ((item_type = " . ITEM_TYPE_SERVICES . 
					 " and item_pos_type_pos_type = " . $project["project_postype"] . 
					 " and item_pos_type_product_line = " . $project["project_product_line"] . ") " . 
					 " or (item_type <> " . ITEM_TYPE_SERVICES . " and item_pos_type_pos_type is null))";
}

if(param("scid")) {
	$list_filter .= ' and item_subcategory = ' . dbquote(param("scid"));
}
else
{
	$list_filter .= ' and (item_subcategory is null or item_subcategory = 0)';
}

/*
if(param("sg")) {
	$list_filter .= ' and item_supplying_group_supplying_group_id = ' . dbquote(param("sg"));
}
*/



// read all items belonging to the category and beeing conteined in the order
$values = array();

$tmp_sql = $sql . " where " . $list_filter;
$res = mysql_query($tmp_sql) or dberror($tmp_sql);
while ($row = mysql_fetch_assoc($res))
{
    // Check if item is already in table order_items
    $sql_order_item = "select order_item_item, order_item_quantity ".
                      "from order_items ".
                      "where order_item_order = " . param('oid') .
                      "    and order_item_item = " . $row["item_id"];


    $res1 = mysql_query($sql_order_item) or dberror($sql_order_item);
    if ($row1 = mysql_fetch_assoc($res1))
    {
        $values[$row["item_id"]] = $row1["order_item_quantity"];
    }
    else
    {
        $values[$row["item_id"]] = "";
    }
}


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");

$form->add_hidden("cid", param("cid"));

require_once "include/project_head_small.php";

/********************************************************************
    Create List
*********************************************************************/ 
$list = new ListView($sql);

$list->set_entity("itmes");
$list->set_filter($list_filter);
$list->set_order("item_category_name, item_code");
$list->set_group("group_head");

$list->add_hidden("pid", param("pid"));
$list->add_hidden("oid", $project["order_id"]);
$list->add_hidden("cid", id());
$list->add_hidden("pline", $project["project_product_line"]);

$list->add_column("item_code", "Item Code", "/applications/templates/item.info.modal.php?id={item_id}", "", "", "", "", array('class'=>'item_info_box'));

$list->add_edit_column("item_entry_field", "Quantity", "4", 0, $values);
$list->add_column("item_name", "Item Name");


$list->add_button("add_items", "Add Items");
$list->add_button(LIST_BUTTON_BACK, "Back");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();

if ($list->button("add_items"))
{
	if(param("type") and param("type") == 2)
	{
		project_save_order_items($list, ITEM_TYPE_SPECIAL, 0);
	}
	else
	{
		project_save_order_items($list, ITEM_TYPE_STANDARD);
	}
    //$link = "project_edit_material_list.php?pid=" . param("pid"); 

	$link = "project_add_catalog_item_categories.php?pid=" . param("pid") . "&oid=" . param("oid");
    redirect($link);
}

/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Add Catalog Items: " . $category_name);
$form->render();

echo "<p>", "Please indicate the quantities to add to the list.", "</p>";

$list->render();

require_once "include/project_footer_logistic_state.php";
$page->footer();

?>