<?php
/********************************************************************

    project_costs_attachments.php

    List of attachments of a project cost sheet

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2014-02-17
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2014-02-17
    Version:        1.0.2

    Copyright (c) 2014, OMEGA SA, All Rights Reserved.


*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_view_attachments_in_projects");


register_param("pid");
set_referer("project_add_attachment.php");
set_referer("project_edit_attachment.php");



/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details

$project = get_project(param("pid"));


// get company's address
$client_address = get_address($project["order_client_address"]);


// get user data
$user_data = get_user(user_id());


// build sql for attachment entries
              


// build filter for the list of attachments
$list1_filter = "order_file_order = " . $project["project_order"] . " and order_file_category in(5) ";
$list2_filter = "order_file_order = " . $project["project_order"] . " and order_file_category in(12) ";

$file_category_filter = "";

$user_roles = get_user_roles(user_id());
$categroy_restrictions = get_file_category_restirctions($user_roles);

if(count($categroy_restrictions) > 0)
{
	$file_category_filter = " and order_file_category IN (" . implode(",", $categroy_restrictions). ") ";
}

$list1_filter .=  $file_category_filter;
$list2_filter .=  $file_category_filter;

if (has_access("has_access_to_all_attachments_in_projects"))
{
	$sql_attachment = "select distinct order_file_id, order_file_visited, ".
                  "    order_file_title, order_file_description, ".
                  "    order_file_path, file_type_name, ".
                  "    order_files.date_created, ".
                  "    order_file_category_name, order_file_category_priority, ".
                  "    concat(user_name, ' ', user_firstname) as owner_fullname ".
                  "from order_files  " .
                  "left join order_file_categories on order_file_category_id = order_file_category ".
                  "left join users on user_id = order_file_owner ".
                  "left join file_types on order_file_type = file_type_id ";
}
else
{
	$file_ids = array();
	$sql_oder_addresses = 'select order_file_address_file from order_file_addresses ' .
		                  'where order_file_address_address = ' . $user_data["address"];

	$res = mysql_query($sql_oder_addresses) or dberror($sql_oder_addresses);
    while ($row = mysql_fetch_assoc($res)) {
		$file_ids[] = $row['order_file_address_file'];
	}

	$file_filter = "";
	if(count($file_ids) > 0) {
		$file_filter = ' or order_file_id IN (' . implode(',', $file_ids) . ')';
	}
	
	
	$sql_attachment = "select distinct order_file_id, order_file_visited, ".
                  "    order_file_title, order_file_description, ".
                  "    order_file_path, file_type_name, ".
                  "    order_files.date_created, ".
                  "    order_file_category_name, order_file_category_priority, ".
                  "    concat(user_name, ' ', user_firstname) as owner_fullname ".
                  "from order_files " . 
                  "left join order_file_categories on order_file_category_id = order_file_category ".
                  "left join users on user_id = order_file_owner ".
                  "left join file_types on order_file_type = file_type_id ";
	
	$list1_filter = $list1_filter . " and (order_file_owner = " . user_id() . $file_filter . ")";
	$list2_filter = $list2_filter . " and (order_file_owner = " . user_id() . $file_filter . ")";


}




// get new comment info pix
$sql_pix = $sql_attachment . " where " . $list1_filter;
$images = set_new_attachment_pictures($sql_pix, $project["project_order"]);

$sql_pix = $sql_attachment . " where " . $list2_filter;
$images2 = set_new_attachment_pictures($sql_pix, $project["project_order"]);



/********************************************************************
    Create Upload Links
*********************************************************************/ 

$urlQuery = http_build_query(array(
	'action' => '/user/include/ajx_upload_attachments.php',
	'title' => 'Upload Files',
	'subtitle' => 'Offers: ',
	'infotext' => 'Allowed file types are: pdf, xls, xlsx',
	'fields' => array(
		'id' => param("pid"),
		'category' => 5,
		'extensions' => 'pdf,xls,xlsx',
		'startload' => true
	)
));

$iframeAttributes = array(
	"data-fancybox-type" => "iframe",
	"data-fancybox-width" => "800",
	"data-fancybox-height" => "620",
	"href" => "/public/themes/default/multifile.uploader.php?$urlQuery"
);

$btnAttributes = '';
foreach ($iframeAttributes as $k=>$v) {
	$btnAttributes .= "$k=\"$v\" ";
}

$upload_link_1 = '<a class="fancy-frame" ' . $btnAttributes . ' >Upload Offers</a>';



$urlQuery = http_build_query(array(
	'action' => '/user/include/ajx_upload_attachments.php',
	'title' => 'Upload Files',
	'subtitle' => 'Invoices: ',
	'infotext' => 'Allowed file types are: pdf, xls, xlsx',
	'fields' => array(
		'id' => param("pid"),
		'category' => 12,
		'extensions' => 'pdf,xls,xlsx',
		'startload' => true
	)
));

$iframeAttributes = array(
	"data-fancybox-type" => "iframe",
	"data-fancybox-width" => "800",
	"data-fancybox-height" => "620",
	"href" => "/public/themes/default/multifile.uploader.php?$urlQuery"
);

$btnAttributes = '';
foreach ($iframeAttributes as $k=>$v) {
	$btnAttributes .= "$k=\"$v\" ";
}

$upload_link_2 = '<a class="fancy-frame" ' . $btnAttributes . ' >Upload Invoices</a>';

/********************************************************************
    Create Form
*********************************************************************/ 


$form = new Form("order_files", "order_file");


$form->add_section("Project");
$form->add_hidden("pid", param("pid"));


require_once "include/project_head_small.php";


if(has_access("can_add_attachments_in_projects")) {
	$form->add_comment($upload_link_1 . " | " . $upload_link_2);
}



$list1 = new ListView($sql_attachment, LIST_HAS_HEADER);
$list1->set_title("<br />Offers<br /><br />");

$list1->set_entity("order_files");
$list1->set_filter($list1_filter);
$list1->set_order("order_files.date_created DESC");


$link = "";
if(has_access("can_edit_attachment_data_in_projects")) {
	$link = "project_edit_attachment.php?pid=" . param("pid");
}



if (has_access("can_edit_attachment_data_in_projects"))
{
    $list1->add_column("date_created", "Date/Time", $link, "", "", COLUMN_NO_WRAP);
}
else
{
    $list1->add_column("date_created", "Date/Time", "", "", "", COLUMN_NO_WRAP);
}

if(count($images)> 0)
{
    $list1->add_image_column("order_file_id", "New", 0, $images);
}

$list1->add_column("owner_fullname", "Made by", "", "", "", COLUMN_NO_WRAP);
$link = "http://" . $_SERVER["HTTP_HOST"] . "/include/openfile.php?id={order_file_id}";
$list1->add_column("order_file_title", "Title", $link, "", "", COLUMN_NO_WRAP);
$list1->add_column("file_type_name", "Type", "", "", "", COLUMN_NO_WRAP);
$list1->add_column("order_file_description", "Description");


/********************************************************************
    Create List of invoices
*********************************************************************/ 
$list2 = new ListView($sql_attachment, LIST_HAS_HEADER);
$list2->set_title("<br />Invoices<br /><br />");

$list2->set_entity("order_files");
$list2->set_filter($list2_filter);
$list2->set_order("order_files.date_created DESC");

$link = "";
if(has_access("can_edit_attachment_data_in_projects")) {
	$link = "project_edit_attachment.php?pid=" . param("pid");
}

if (has_access("can_edit_attachment_data_in_projects"))
{
    $list2->add_column("date_created", "Date/Time", $link, "", "", COLUMN_NO_WRAP);
}
else
{
    $list2->add_column("date_created", "Date/Time", "", "", "", COLUMN_NO_WRAP);
}

if(count($images)> 0)
{
    $list2->add_image_column("order_file_id", "New", 0, $images);
}

$list2->add_column("owner_fullname", "Made by", "", "", "", COLUMN_NO_WRAP);
$link = "http://" . $_SERVER["HTTP_HOST"] . "/include/openfile.php?id={order_file_id}";
$list2->add_column("order_file_title", "Title", $link, "", "", COLUMN_NO_WRAP);
$list2->add_column("file_type_name", "Type", "", "", "", COLUMN_NO_WRAP);
$list2->add_column("order_file_description", "Description");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

$list1->populate();
$list1->process();

$list2->populate();
$list2->process();


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$page = new Page("projects");


require "include/project_page_actions.php";


$page->header();
$page->title("Project Costs - Attachments");

require_once("include/costsheet_tabs.php");
$form->render();

$list1->render();
$list2->render();

$page->footer();


?>