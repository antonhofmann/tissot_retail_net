<?php
/********************************************************************


    order_edit_comment.php


    Add comments to a order


    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-02-28
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-06-18
    Version:        1.0.0


    Copyright (c) 2003, Swatch AG, All Rights Reserved.


*********************************************************************/


require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "include/order_functions.php";


check_access("can_edit_comment_data_in_orders");


/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$order = get_order(param("oid"));


// get company's address
$client_address = get_address($order["order_client_address"]);


// bulid sql for comment categoreis
$sql_comment_categories = "select comment_category_id, comment_category_name ".
                          "from comment_categories ".
                          "where comment_category_order_type = 1 ".
                          "order by comment_category_priority ";




// get addresses involved in the project
$companies = get_involved_companies_2(param('oid'), id());




// determine users that have already got an email announcing the new attachment
$old_recipients = array();
foreach($companies as $key=>$value)
{
    if($value["access"] == 1)
    {
        $old_recipients[] = $value["id"];
    }
}


// checkbox names
$check_box_names = array();


/********************************************************************
    Create Form
*********************************************************************/ 


$form = new Form("comments", "comment");


$form->add_section("Order");
$form->add_hidden("oid", param('oid'));
$form->add_hidden("order_id", param('oid'));


require_once "include/order_head_small.php";


$form->add_section("Comment");
$form->add_list("comment_category", "Category*", $sql_comment_categories, NOTNULL);
$form->add_multiline("comment_text", "Comment", 4, NOTNULL);


if (has_access("can_set_comment_accessibility_in_orders"))
{
    $form->add_section("Accessibility");
    $form->add_comment("Please indicate who is allowed to read this comment.");


    $num_checkboxes = 1;
    foreach ($companies as $key=>$value_array)
    {
        if(!array_key_exists('role', $value_array)) {
			$value_array["role"] = "";
		}
		$form->add_checkbox("A" . $num_checkboxes, $value_array["name"], $value_array["access"], 0, $value_array["role"]);
        $check_box_names["A" . $num_checkboxes] = $value_array["id"];
        
		$selected_companies["A" . $num_checkboxes] = $value_array;

        $num_checkboxes++;
    }
}




$form->add_button(FORM_BUTTON_SAVE, "Save");


if(has_access("can_delete_comment_in_orders"))
{
    $form->add_button("delete", "Delete");
}


$form->add_button(FORM_BUTTON_BACK, "Back");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


if ($form->button(FORM_BUTTON_SAVE))
{
    // check if a recipient was selected
    if (has_access("can_set_comment_accessibility_in_orders"))
    {
        $no_recipient = 1;
    }
    else
    {
        $no_recipient = 0;
    }


    foreach ($form->items as $item)
    {
        if ($item["type"] == "checkbox" and $item["value"] == "1")
        {
            $no_recipient = 0;
        }
    }


    if ($form->validate() and $no_recipient == 0)
    {
        update_comment_accessibility_info(id(), $form,  $check_box_names);


        // send email notifocation to the retail staff
        $order_id = $form->value("order_id");


        $sql = "select order_id, order_number,  ".
               "users.user_email as recepient, users.user_address as address_id, " .
               "users1.user_email as sender, concat(users1.user_name, ' ', users1.user_firstname) as user_fullname ".
               "from orders ".
               "left join users on order_retail_operator = users.user_id ".
               "left join users as users1 on " . user_id() . "= users1.user_id ".
               "where order_id = " . $order_id;


        $res = mysql_query($sql) or dberror($sql);
        if ($row = mysql_fetch_assoc($res) and $row["recepient"])
        {
            $subject = MAIL_SUBJECT_PREFIX . ": New comment was added - Order " . $row["order_number"]. " - " . $client_address["country_name"];
            $sender_email = $row["sender"];
            $sender_name =  $row["user_fullname"];


            $mail = new Mail();
            $mail->set_subject($subject);
            $mail->set_sender($sender_email, $sender_name);


            $bodytext0 = "A comment has been updated by " . $sender_name . ":";
			$bodytext1 = "A comment has been updated by " . $sender_name . " for:";


            $recipient_added = 0;
			$reciepients = array();
			$reciepient_ids = array();
			$reciepients_cc = array();
			$reciepients_dp = array();

            foreach ($check_box_names as $key=>$value)
            {
                
				if ($form->value($key) and !in_array($value, $old_recipients))
                {

					if ($form->value($key) == 1)
					{
						$sql = "select user_id, user_email, user_email_cc, user_email_deputy ".
							   "from users ".
							   "where (user_id = " . $selected_companies[$key]["user"] . 
							   "   and user_active = 1)";

					
						$res1 = mysql_query($sql) or dberror($sql);
						if ($row1 = mysql_fetch_assoc($res1))
						{
							$reciepients[strtolower($row1["user_email"])] = strtolower($row1["user_email"]);
							$reciepients_cc[strtolower($row1["user_email_cc"])] = strtolower($row1["user_email_cc"]);
							$reciepients_dp[strtolower($row1["user_email_deputy"])] = strtolower($row1["user_email_deputy"]);

							$recipient_added = 1;

							$reciepient_ids[$row1["user_id"]] = $row1["user_id"];
						}
							
					}
                }
            }

			
			if($recipient_added == 1)
            {
				foreach($reciepients as $key=>$email)
				{
					$bodytext1 = $bodytext1 . "\n" . $email;
					$mail->add_recipient($email);
				}


				foreach($reciepients_cc as $key=>$email)
				{
					if(!in_array($email, $reciepients))
					{
						$bodytext1 = $bodytext1 . "\n" . $email;
						$mail->add_cc($reciepients_cc[$email]);
					}
				}

				foreach($reciepients_dp as $key=>$email)
				{
					if(!in_array($email, $reciepients))
					{
						$bodytext1 = $bodytext1 . "\n" . $email;
						$mail->add_cc($reciepients_dp[$email]);
					}
				}
			}
			else
			{
				foreach($reciepients_cc as $key=>$email)
				{
					$bodytext1 = $bodytext1 . "\n" . $email;
					$mail->add_recipient($reciepients_cc[$email]);
					$recipient_added = 1;
				}
			}


            
			$bodytext0.= "\n\n<--\n";
            $bodytext0.= $form->value("comment_text") . "\n";
            $bodytext0.= "-->";

            $link ="order_view_comments.php?oid=" . $order["order_id"];
            $bodytext = $bodytext0 . "\n\nclick below to have direct access to the order:\n";
            $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           
            $mail->add_text($bodytext);
            if($senmail_activated == true)
			{
				$mail->send();
			}

			
			$bodytext1.= "\n\n<--\n";
            $bodytext1.= $form->value("comment_text") . "\n";
            $bodytext1.= "-->";

			$link ="order_view_comments.php?oid=" . $order["order_id"];
            $bodytext = $bodytext1 . "\n\nclick below to have direct access to the order:\n";
            $bodytext = $bodytext .  APPLICATION_URL . "/user/" . $link . "\n\n";           

            if($recipient_added == 1)
            {
                
				foreach($reciepient_ids as $key=>$user_id)
				{
					append_mail($order["order_id"], $user_id , user_id(), $bodytext, "", 2, 0, 0, 'comments', id());
				}
				

				foreach($reciepients_cc as $key=>$email)
				{
					$sql = "select user_id from users " . 
						   " where LOWER(user_email) = " . dbquote($email);
					
					$res = mysql_query($sql) or dberror($sql);
					if ($row = mysql_fetch_assoc($res))
					{
						append_mail($order["order_id"], $row["user_id"] , user_id(), $bodytext, "", 2, 0, 1, 'comments', id());
					}
				}

				foreach($reciepients_dp as $key=>$email)
				{
					$sql = "select user_id from users " . 
						   " where LOWER(user_email) = " . dbquote($email);
					
					$res = mysql_query($sql) or dberror($sql);
					if ($row = mysql_fetch_assoc($res))
					{
						append_mail($order["order_id"], $row["user_id"] , user_id(), $bodytext, "", 2, 0, 1, 'comments', id());
					}
				}
            }
        }


        $link = "order_view_comments.php?oid=" . param("oid");
        redirect($link);
    }
    else
    {
        $form->error("Please select a least one person to have access to the comment.");
    }
}
elseif ($form->button("delete"))
{
    delete_comment(id());
    $link = "order_view_comments.php?oid=" . param("oid");
    redirect($link);
}


$page = new Page("orders");


require "include/order_page_actions.php";


$page->header();
$page->title("Edit Comment");
$form->render();
$page->footer();


?>