<?php
/********************************************************************

    project_offers.php

    List of offers for a projects

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2006-11-09
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2006-11-09
    Version:        1.0.0

    Copyright (c) 2006, Swatch AG, All Rights Reserved.

*********************************************************************/
require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_edit_local_constrction_work");

register_param("pid");
set_referer("project_offer.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);
$currency = get_address_currency($project["order_client_address"]);


// get user data
$user_data = get_user(user_id());


// build sql for offer entries
$sql_offers = "select lwoffer_id, lwoffer_company, " .
              "lwoffer_zip, lwoffer_place, lwoffer_contact, " . 
              "lwoffer_email, lwoffer_accepted, " .
              "DATE_FORMAT(lwoffer_date, '%d.%c.%y') as odate, currency_symbol " .
              "from lwoffers " . 
              "left join currencies on currency_id = lwoffer_currency";
              
// build filter for the list of offers
$list1_filter = "lwoffer_order = " . $project["project_order"];




// Changes 2008
// reduction to groups, no more editing of single offer positions
// leave the position editing in case it will be used/activated at a leter time

//Offer Positions
//build totals
/*
$offers = array();
$budgets = array();
$realcosts = array();

$sql = $sql_offers . " where " . $list1_filter;
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{

    $sql_a = "select lwoffer_id, " . 
           "sum(lwofferposition_numofunits*lwofferposition_price) as total " .
           "from lwoffers " .
           "left join lwoffergroups on lwoffergroup_offer = lwoffer_id " . 
           "left join lwofferpositions on lwofferposition_lwoffergroup = lwoffergroup_id " . 
           "where lwoffer_id = " . $row["lwoffer_id"] .
           " GROUP BY lwoffer_id ";

	$offered = 0;
    $res_a = mysql_query($sql_a) or dberror($sql_a);
    if ($row_a = mysql_fetch_assoc($res_a))
    {
		$offers[$row["lwoffer_id"]] = number_format($row_a["total"],2, ".", "'");

    }

    $sql_a = "select lwoffer_id, " . 
           "sum(lwofferposition_numofunits*lwofferposition_price) as total " .
           "from lwoffers " .
           "left join lwoffergroups on lwoffergroup_offer = lwoffer_id " . 
           "left join lwofferpositions on lwofferposition_lwoffergroup = lwoffergroup_id " . 
           "where lwofferposition_inbudget = 1 and lwoffer_id = " . $row["lwoffer_id"] .
           " GROUP BY lwoffer_id ";

    $budget = 0;
    $res_a = mysql_query($sql_a) or dberror($sql_a);
    if ($row_a = mysql_fetch_assoc($res_a))
    {
        $budgets[$row["lwoffer_id"]] = number_format($row_a["total"],2, ".", "'");

    }

    $sql_a = "select lwoffer_id, " . 
           "sum(lwofferposition_numofunits*lwofferposition_price) as total " .
           "from lwoffers " .
           "left join lwoffergroups on lwoffergroup_offer = lwoffer_id " . 
           "left join lwofferpositions on lwofferposition_lwoffergroup = lwoffergroup_id " . 
           "where lwofferposition_price_billed <> 0 " . 
           "  and lwofferposition_price_billed is not NULL " . 
           "  and lwoffer_id = " . $row["lwoffer_id"] . 
           " GROUP BY lwoffer_id ";

    $real = 0;
    $res_a = mysql_query($sql_a) or dberror($sql_a);
    if ($row_a = mysql_fetch_assoc($res_a))
    {
        $realcosts[$row["lwoffer_id"]] = number_format($row_a["total"],2, ".", "'");

    }
}

//set icons for picture indicating column
$images = array();

$sql = $sql_offers . " where " . $list1_filter;

$res = mysql_query($sql) or dberror($sql);

while ($row = mysql_fetch_assoc($res))
{
    if($row["lwoffer_accepted"] == 1)
    {
        $images[$row["lwoffer_id"]] = "/pictures/accepted.gif";
    }
}
*/

//Offer Groups
//build totals
$offers = array();
$budgets = array();
$realcosts = array();
$images = array();

$sql = $sql_offers . " where " . $list1_filter;
$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{

    $sql_a = "select lwoffer_id, currency_symbol, lwoffer_exchange_rate, lwoffer_factor, " . 
           "sum(lwoffergroup_price) as total " .
           "from lwoffers " .
           "left join lwoffergroups on lwoffergroup_offer = lwoffer_id " .
		   "left join currencies on currency_id = lwoffer_currency " . 
           "where lwoffer_id = " . $row["lwoffer_id"] .
           " GROUP BY lwoffer_id, currency_symbol, currency_exchange_rate, currency_factor ";

	$offered = 0;
    $res_a = mysql_query($sql_a) or dberror($sql_a);
    if ($row_a = mysql_fetch_assoc($res_a))
    {
		$offered = number_format($row_a["total"],2, ".", "'");
		$offer_currency_exchangerate = $row_a["lwoffer_exchange_rate"];
		$offer_currency_factor = $row_a["lwoffer_factor"];
		$offers[$row["lwoffer_id"]] = number_format($row_a["total"],2, ".", "'");
    }

    $sql_a = "select lwoffer_id, " . 
           "sum(lwoffergroup_price) as total " .
           "from lwoffers " .
           "left join lwoffergroups on lwoffergroup_offer = lwoffer_id " . 
           "where lwoffergroup_inbudget = 1 and lwoffer_id = " . $row["lwoffer_id"] .
           " GROUP BY lwoffer_id ";

    $budget = 0;
    $res_a = mysql_query($sql_a) or dberror($sql_a);
    if ($row_a = mysql_fetch_assoc($res_a))
    {
        $budget = $row_a["total"]*$offer_currency_exchangerate/$offer_currency_factor;
		$budgets[$row["lwoffer_id"]] = number_format($budget,2, ".", "'");
		$images[$row["lwoffer_id"]] = "/pictures/accepted.gif";

    }

    $sql_a = "select lwoffer_id, " . 
           "sum(lwoffergroup_price_billed) as total " .
           "from lwoffers " .
           "left join lwoffergroups on lwoffergroup_offer = lwoffer_id " . 
           "where lwoffergroup_price_billed <> 0 " . 
           "  and lwoffergroup_price_billed is not NULL " . 
           "  and lwoffer_id = " . $row["lwoffer_id"] . 
           " GROUP BY lwoffer_id ";

    $real = 0;
    $res_a = mysql_query($sql_a) or dberror($sql_a);
    if ($row_a = mysql_fetch_assoc($res_a))
    {
        $real = $row_a["total"]*$offer_currency_exchangerate/$offer_currency_factor;
		$realcosts[$row["lwoffer_id"]] = number_format($real,2, ".", "'");

    }
}


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("lwoffers", "lwoffer");

$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid", $project["project_order"]);

require_once "include/project_head_small.php";


$link = "project_offer_comparison_pdf.php?pid=" . param("pid") . "&lwoid=" . id(); 
$link = "javascript:popup('". $link . "', 800, 600)";
$form->add_button("pdf", "Compare Offers - Print PDF", $link);


/********************************************************************
    Create List
*********************************************************************/ 


$list1 = new ListView($sql_offers);


$list1->set_entity("lwoffers");
$list1->set_filter($list1_filter);
$list1->set_order("odate DESC");

$link = "project_offer.php?pid=" . param("pid");

$list1->add_column("odate", "Date", $link);
//$list1->add_image_column("state", "", 0, $images);
$list1->add_column("lwoffer_company", "Company");
//$list1->add_column("lwoffer_zip", "Zip");
//$list1->add_column("lwoffer_place", "City");
//$list1->add_column("lwoffer_contact", "Contact");
//$list1->add_column("lwoffer_email", "Email");

$list1->add_column("currency_symbol", "Currency");
$list1->add_text_column("offered", "Offer", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $offers);
//$list1->add_text_column("budget", $currency["symbol"] . " in Budget", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $budgets);
//$list1->add_text_column("real", $currency["symbol"] . " Real Cost", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $realcosts);

$list1->add_text_column("budget", "CHF in Budget", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $budgets);
$list1->add_text_column("real", "CHF Real Cost", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP, $realcosts);


if ($project["order_budget_is_locked"] == 1)
{
}
else
{
	$list1->add_button("add_offer", "Add Offer");
}


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list1->populate();
$list1->process();


if ($list1->button("add_offer"))
{
    redirect ($link);
}

$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Offers for Local Construction Works");
$form->render();
echo "<br>";
$list1->render();
require_once "include/project_footer_logistic_state.php";
$page->footer();


?>