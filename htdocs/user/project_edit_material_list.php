<?php
/********************************************************************

    project_edit_material_list.php

    Edit List of Materials

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-09
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2003-11-13
    Version:        1.2.0

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";

check_access("has_access_to_list_of_materials_in_projects");

register_param("pid");
set_referer("project_add_catalog_item_categories.php");
set_referer("project_edit_material_list_edit_item.php");
set_referer("project_add_catalog_item_special_item_list.php");
set_referer("project_add_special_item_individual.php");
set_referer("project_add_catalog_item_cost_estimation_list.php");
set_referer("project_add_cost_estimation_individual.php");
set_referer("project_add_lc_cost_estimation_individual.php");

/********************************************************************
    prepare all data needed
*********************************************************************/
$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());

// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

// get orders's currency
$order_currency = get_order_currency($project["project_order"]);

//get involved suppliers
$concerned_suppliers = get_concerned_suppliers($project["project_order"], $project["project_is_urgent"]);



// get System currency
$system_currency = get_system_currency_fields();
$exchange_rate_info = get_order_currency($project["project_order"]);


$sql_order_items = "select order_item_id, order_item_text, order_item_quantity, order_item_order_revisions, ".
				   "    order_item_po_number, order_item_system_price, order_item_client_price, " . 
				   "    order_item_system_price_freezed,  order_item_discount, order_item_localcostgroupcode, " . 
				   "    item_id, order_item_supplier_address, ".
				   "    order_item_forwarder_address, " . 
				   "    if(order_item_not_in_budget=1 , 'no', 'yes') as order_item_in_budget, ".
				   "    TRUNCATE(order_item_quantity * order_item_system_price, 2) as total_price, ".
				   "    TRUNCATE(order_item_quantity * order_item_client_price, 2) as total_price_loc, ".
				   "    if(item_code <>'', item_code, item_type_name) as item_shortcut, ".
				   "    order_item_supplier_freetext, order_item_exclude_from_ps, ".
				   "    addresses.address_shortcut as supplier_company, ".
				   "    addresses_1.address_shortcut as forwarder_company, ".
				   "    item_type_id, item_type_name, ".
				   "    item_type_priority, " . 
				   "    concat(project_cost_groupname_name, ' - ', item_category_name) as cat_name, " .
				   "    order_item_type,  ".
				   "    item_stock_property_of_swatch, unit_name, order_item_client_price_freezed, " . 
				   " order_item_supplier_exchange_rate, project_cost_groupname_name " .
				   "from order_items ".
				   "left join items on order_item_item = item_id ".
				   "left join item_categories on item_category_id = item_category " .
				   "left join addresses on order_item_supplier_address = addresses.address_id ".
				   "left join addresses as addresses_1 ".
				   "     on order_item_forwarder_address = addresses_1.address_id ".
				   "left join item_types on order_item_type = item_type_id " .
				   "left join project_cost_groupnames on project_cost_groupname_id = order_item_cost_group " .
				   "left join units on unit_id = order_item_unit_id";


$where_clause = " where order_item_cost_group IN (10,6, 2) " . 
                "    and order_item_order = " . $project["project_order"];


$sql_suppliers = "select DISTINCT address_id, address_shortcut " . 
                 "from order_items " . 
				 "left join suppliers on supplier_item = order_item_item " . 
				 "left join addresses on address_id = supplier_address " . 
				 "where address_shortcut <> '' " . 
				 " and order_item_order = " . $project["project_order"] . 
				 " order by address_shortcut";


$sql_forwarders = "select DISTINCT address_id, address_shortcut " . 
				  "from addresses " . 
				  "where address_shortcut <> '' and address_type IN (3,9) and address_active = 1 " . 
				  " order by address_shortcut";

// create sql for the furniture height listbox
$sql_ceiling_heights = "select ceiling_height_id, ceiling_height_height ".
                "from ceiling_heights";




//get all list information
$item_suppliers = array();
$item_forwarders = array();

$quantities = array();
$revisions = array();
$exclude_from_ps = array();
$property = array();
$images = array();



$freezed_prices_exist = false;

$res = mysql_query($sql_order_items . $where_clause) or dberror($sql_order_items . $where_clause);

$num_of_standard_items = 0;
$num_of_special_items = 0;
$splitting_links = array();
while ($row = mysql_fetch_assoc($res))
{
    
	$quantities[$row["order_item_id"]] = $row["order_item_quantity"];
	$revisions[$row["order_item_id"]] = $row["order_item_order_revisions"];

    if($row["order_item_type"] == ITEM_TYPE_STANDARD or $row["order_item_type"] == ITEM_TYPE_SERVICES)
    {
        $num_of_standard_items++;
    }
    elseif($row["order_item_type"] == ITEM_TYPE_SPECIAL)
    {
        $num_of_special_items++;
    }

	$item_suppliers[$row["order_item_id"]] = $row["order_item_supplier_address"];
	$item_forwarders[$row["order_item_id"]] = $row["order_item_forwarder_address"];

	$exclude_from_ps[$row["order_item_id"]] = $row["order_item_exclude_from_ps"];

	if($row["item_stock_property_of_swatch"] == 1)
	{
		$property[$row["order_item_id"]] = "/pictures/stockproperty.gif";
	}

	//set icons for picture indicating column
	

	$link = "<a class=\"item-files-modal\" data-fancybox-type=\"iframe\" href=\"/applications/templates/item.info.modal.php?id=" . $row["item_id"] . "\"><img border=\"0\" src=\"/pictures/document_view.gif\" /></a>";
		
	$images[$row["order_item_id"]] = $link;
	

	if($row["order_item_system_price_freezed"] > 0)
	{
		$freezed_prices_exist = true;
	}

	if($row["order_item_quantity"] > 1 
		and $row["order_item_type"] == ITEM_TYPE_SPECIAL
		and $row["item_id"] > 0
		and ($row["order_item_ordered"] == NULL or $row["order_item_ordered"] == '0000-00-00')) { 
		$splitting_links[$row["order_item_id"]] = '<a href="#" id="item' . $row["order_item_id"] . '">split</a>';
	}
}

//get discount rates
$sql_lc = "select order_item_id, order_item_discount, order_item_localcostgroupcode, lwoffer_id, " .
          " order_item_system_price, order_item_client_price, order_item_supplier_freetext " .
          " from order_items " .
		  " left join lwoffers on lwoffer_order = order_item_order " . 
		  " where order_item_cost_group IN (7) " . 
          "    and order_item_order = " . $project["project_order"];
$res = mysql_query($sql_lc) or dberror($sql_lc);
$discount_rates = array();
$localcostgroupcodes = array();
$system_prices = array();
$client_prices = array();
$suppliers = array();

while ($row = mysql_fetch_assoc($res))
{
	$discount_rates[$row["order_item_id"]] = $row["order_item_discount"];
	$localcostgroupcodes[$row["order_item_id"]] = array('lwoffer_id'=>$row["lwoffer_id"], 'localcostgroupcode'=>$row["order_item_localcostgroupcode"]);

	$system_prices[$row["order_item_id"]] = $row["order_item_system_price"];
	$client_prices[$row["order_item_id"]] = $row["order_item_client_price"];

	$suppliers[$row["order_item_id"]] = $row["order_item_supplier_freetext"];

}


//calculate cost group totals
$grouptotals_investment = array();
$grouptotals_investment[2] = 0;
$grouptotals_investment[6] = 0;
$grouptotals_investment[7] = 0;
$grouptotals_investment[8] = 0;
$grouptotals_investment[9] = 0;
$grouptotals_investment[10] = 0;
$grouptotals_investment[11] = 0;

$grouptotals_hq_investment = array();
$grouptotals_hq_investment[2] = 0;
$grouptotals_hq_investment[6] = 0;


$grouptotals_investment_loc = array();
$grouptotals_investment_loc[2] = 0;
$grouptotals_investment_loc[6] = 0;
$grouptotals_investment_loc[7] = 0;
$grouptotals_investment_loc[8] = 0;
$grouptotals_investment_loc[9] = 0;
$grouptotals_investment_loc[10] = 0;
$grouptotals_investment_loc[11] = 0;

$grouptotals_hq_investment_loc = array();
$grouptotals_hq_investment_loc[2] = 0;
$grouptotals_hq_investment_loc[6] = 0;


$sql = "select order_item_cost_group, order_item_type, ".
	   "order_item_system_price, order_item_client_price, order_item_quantity,  ".
	   "order_item_system_price_freezed, order_item_client_price_freezed, order_item_quantity_freezed, ".
	   "order_item_system_price_hq_freezed, order_item_client_price_hq_freezed, order_item_quantity_hq_freezed " .
	   "from order_items ".
	   "where (order_item_not_in_budget = 0 or order_item_not_in_budget is null) ".
	   "   and order_item_order=" . $project["project_order"] . 
	   "   and (order_item_type <= " . ITEM_TYPE_COST_ESTIMATION .
	   "   or order_item_type = " . ITEM_TYPE_SERVICES .
	   "   or order_item_type = " . ITEM_TYPE_LOCALCONSTRUCTIONCOST .
	   " ) " .
	   " and order_item_cost_group > 0 " . 
	   " order by order_item_cost_group";

$res = mysql_query($sql) or dberror($sql);
while($row = mysql_fetch_assoc($res))
{
	

	if(!array_key_exists($row["order_item_cost_group"], $grouptotals_hq_investment))
	{
		$grouptotals_hq_investment[$row["order_item_cost_group"]] = 0;
		$grouptotals_hq_investment_loc[$row["order_item_cost_group"]] = 0;
	}
	if($row["order_item_quantity"] > 0 or $row["order_item_type"] == 1  or $row["order_item_type"] == 2)
	{
		$grouptotals_hq_investment[$row["order_item_cost_group"]] = $grouptotals_hq_investment[$row["order_item_cost_group"]] + $row["order_item_system_price"] * $row["order_item_quantity"];

		$grouptotals_hq_investment_loc[$row["order_item_cost_group"]] = $grouptotals_hq_investment_loc[$row["order_item_cost_group"]] + $row["order_item_client_price"] * $row["order_item_quantity"];

	}
	else
	{
		$grouptotals_hq_investment[$row["order_item_cost_group"]] = $grouptotals_hq_investment[$row["order_item_cost_group"]] + $row["order_item_system_price"];
		$grouptotals_hq_investment_loc[$row["order_item_cost_group"]] = $grouptotals_hq_investment_loc[$row["order_item_cost_group"]] + $row["order_item_client_price"];
	}
	

	
	if($row["order_item_quantity"] > 0 or $row["order_item_type"] == 1  or $row["order_item_type"] == 2)
	{
		$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_system_price"] * $row["order_item_quantity"];

		$grouptotals_investment_loc[$row["order_item_cost_group"]] = $grouptotals_investment_loc[$row["order_item_cost_group"]] + $row["order_item_client_price"] * $row["order_item_quantity"];

	}
	else
	{
		$grouptotals_investment[$row["order_item_cost_group"]] = $grouptotals_investment[$row["order_item_cost_group"]] + $row["order_item_system_price"];

		$grouptotals_investment_loc[$row["order_item_cost_group"]] = $grouptotals_investment_loc[$row["order_item_cost_group"]] + $row["order_item_client_price"];
	}
}


//budget

$project_cost_construction = $grouptotals_investment[7];

$project_cost_fixturing = $grouptotals_investment[10] + $grouptotals_investment[6] + $grouptotals_investment[2];
$project_cost_fixturing_other = $grouptotals_investment[10];
$project_cost_hq_fixturing = $grouptotals_hq_investment[2];
$project_cost_freight_charges = $grouptotals_investment[6];

$project_cost_architectural = $grouptotals_investment[9];
$project_cost_equipment = $grouptotals_investment[11];
$project_cost_other = + $grouptotals_investment[8];
$budget_total = $project_cost_construction + $project_cost_fixturing + $project_cost_architectural + $project_cost_equipment + $project_cost_other;



$project_cost_construction_loc = $grouptotals_investment_loc[7];

$project_cost_fixturing_loc = $grouptotals_investment_loc[10] + $grouptotals_investment_loc[6] + $grouptotals_investment_loc[2];
$project_cost_fixturing_other_loc = $grouptotals_investment_loc[10];
$project_cost_hq_fixturing_loc = $grouptotals_hq_investment_loc[2];
$project_cost_freight_charges_loc = $grouptotals_investment_loc[6];


$project_cost_architectural_loc = $grouptotals_investment_loc[9];
$project_cost_equipment_loc = $grouptotals_investment_loc[11];
$project_cost_other_loc = + $grouptotals_investment_loc[8];
$budget_total_loc = $project_cost_construction_loc + $project_cost_fixturing_loc + $project_cost_architectural_loc + $project_cost_equipment_loc + $project_cost_other_loc;


//total cost
$standard_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_STANDARD);
$special_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_SPECIAL);
$special_item_total_hq = get_order_item_type_total($project["project_order"],  ITEM_TYPE_SPECIAL, 2);
$freight_charges = get_order_item_type_total($project["project_order"],  ITEM_TYPE_COST_ESTIMATION, 6);
$cost_estimation_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_COST_ESTIMATION);
$cost_estimation_item_total_furniture = get_order_item_type_total($project["project_order"],  ITEM_TYPE_COST_ESTIMATION, 2);
$local_construction_item_total = get_order_item_type_total($project["project_order"],  ITEM_TYPE_LOCALCONSTRUCTIONCOST);

$total_cost = $standard_item_total["in_system_currency"] + $special_item_total["in_system_currency"] + $cost_estimation_item_total["in_system_currency"] + $local_construction_item_total["in_system_currency"];

$hq_total_cost = $standard_item_total["in_system_currency"] + $special_item_total_hq["in_system_currency"] + $freight_charges["in_system_currency"];

$total_cost_loc = $standard_item_total["in_order_currency"] + $special_item_total["in_order_currency"] + $cost_estimation_item_total["in_order_currency"] + $local_construction_item_total["in_order_currency"];

$hq_total_cost_loc = $standard_item_total["in_order_currency"] + $special_item_total_hq["in_order_currency"] + $freight_charges["in_order_currency"];


$hq_budget_approval_data = get_hq_budget_approval_data($project["project_order"]);
$budget_approval_data = get_budget_approval_data($project["project_order"]);



//cget the number standard special items are available
$sql_i = "select count(item_id) as num_recs from items left join suppliers on supplier_item = item_id where item_active and item_type = 2";
$res = mysql_query($sql_i) or dberror($sql_i);
$row = mysql_fetch_assoc($res);
$number_of_standard_special_items = $row["num_recs"];

/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("projects", "project");


$form->add_hidden("pid", param("pid"));
$form->add_hidden("oid",$project["project_order"]);


require_once "include/project_head_small.php";



$form->add_section("Client Currency Information");
$form->add_label("client_currency", "Currency", 0, $order_currency["symbol"]);
$form->add_label("exchange_rate", "Exchange Rate", 0, $order_currency["exchange_rate"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$form->add_comment('<span class="error">All amounts for HQ supplied items converted from CHF into LOC are for reference only as the exchange rate may change.</span>');
}

$form->add_section("Cost Overview as per " . date("d.m.Y"));

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$form->add_label("l1_lc", "Local Construction in " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($project_cost_construction,2, ".", "'") . " / " . number_format($project_cost_construction_loc,2, ".", "'"));
	
	$form->add_label("l2_lc", "Store fixturing / Furniture in " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($project_cost_fixturing,2, ".", "'"). " / " . number_format($project_cost_fixturing_loc,2, ".", "'"));
	
	
	$form->add_label("l9_ow2", "- of which HQ Supplied Items in " .  $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($project_cost_hq_fixturing,2, ".", "'"). " / " . number_format($project_cost_hq_fixturing_loc,2, ".", "'"));

	$form->add_label("l9_ow3", "- of which Freight Charges in " .  $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($project_cost_freight_charges,2, ".", "'"). " / " . number_format($project_cost_freight_charges_loc,2, ".", "'"));
	
	$form->add_label("l9_ow1", "- of which Fixturing: Other (local furniture, import taxes etc.) in " .  $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($project_cost_fixturing_other,2, ".", "'"). " / " . number_format($project_cost_fixturing_other_loc,2, ".", "'"));

	$form->add_label("l3_lc", "Architectural Services in " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($project_cost_architectural,2, ".", "'"). " / " . number_format($project_cost_architectural_loc,2, ".", "'"));
	$form->add_label("l4_lc", "Equipment in " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($project_cost_equipment,2, ".", "'"). " / " . number_format($project_cost_equipment_loc,2, ".", "'"));
	$form->add_label("l5_lc", "Other in " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($project_cost_other,2, ".", "'"). " / " . number_format($project_cost_other_loc,2, ".", "'"));

	
	$form->add_label("l13_lc", "Total Costs in " . $system_currency["symbol"] . " / " . $order_currency["symbol"], 0, number_format($budget_total,2, ".", "'"). " / " . number_format($budget_total_loc,2, ".", "'"));
}
else
{
	$form->add_label("l1", "Local Construction in " . $system_currency["symbol"], 0, number_format($project_cost_construction,2, ".", "'"));
	
	$form->add_label("l2", "Store fixturing / Furniture in " . $system_currency["symbol"], 0, number_format($project_cost_fixturing,2, ".", "'"));

		
	$form->add_label("l9_ow2", "- of which HQ Supplied Items in " .  $system_currency["symbol"], 0, number_format($project_cost_hq_fixturing,2, ".", "'"));
	
	$form->add_label("l9_ow3", "- of which Freight Charges in " .  $system_currency["symbol"], 0, number_format($project_cost_freight_charges,2, ".", "'"));

	$form->add_label("l9_ow1", "- of which Fixturing: Other (local furniture, import taxes etc.) in " .  $system_currency["symbol"], 0, number_format($project_cost_fixturing_other,2, ".", "'"));
	
	$form->add_label("l3", "Architectural Services in " . $system_currency["symbol"], 0, number_format($project_cost_architectural,2, ".", "'"));
	$form->add_label("l4", "Equipment in " . $system_currency["symbol"], 0, number_format($project_cost_equipment,2, ".", "'"));
	$form->add_label("l5", "Other in " . $system_currency["symbol"], 0, number_format($project_cost_other,2, ".", "'"));

	
	$form->add_label("l13", "Total Costs in " . $system_currency["symbol"], 0, number_format($budget_total,2, ".", "'"));
}

if ($project["order_budget_is_locked"] == 1)
{
    $form->error = "Budget is locked, quantities and prices can not be changed anymore.";
}


if(count($concerned_suppliers) > 0)
{
	$form->add_section("Options for Urgent Projects");
	$form->add_comment("Please select which suppliers already should have have access to the project.");

	foreach($concerned_suppliers as $address_id=>$company)
	{
		//check if urgent mail was sent
		//get recipient
		$sent = false;
		$number_of_suppliers = 0;
		$sql_a = "select user_id from users " .
			     "where user_address = " . dbquote($address_id) . 
			     " and user_active = 1 ";
		$res_a = mysql_query($sql_a) or dberror($sql_a);
		if ($row_a = mysql_fetch_assoc($res_a))
		{

			$sql_a = "select count(mail_tracking_id) as num_recs " . 
				     " from mail_trackings " . 
				     " where mail_tracking_mail_template_id = 108 " . 
				     " and mail_tracking_recipient_user_id = " . $row_a["user_id"] . 
				     " and mail_tracking_subject like '%" . $project["order_number"] . "%'";


			$res_a = mysql_query($sql_a) or dberror($sql_a);
			$row_a = mysql_fetch_assoc($res_a);
			if ($row_a["num_recs"] > 0)
			{
				$sent = true;
			}
			else {
				$number_of_suppliers++;
			}
		}
		
		if($sent == false) {
			$form->add_checkbox("s_" . $address_id, $company, "", 0, "Project should be visible to");
		}
		else
		{
			$form->add_checkbox("t_" . $address_id, $company, 1, DISABLED, "Project should be visible to");
		}
	}

	if($number_of_suppliers > 0) {
		$form->add_button("save", "Save Options for Urgent Projects and Send Mail to Suppliers");
	}
}





/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();


if($form->button("save"))
{
	$project_is_urgent = false;

	
	if(count($concerned_suppliers) > 0)
	{
		
		foreach($concerned_suppliers as $address_id=>$company)
		{
			if(array_key_exists("s_" . $address_id, $form->items) 
				and $form->value("s_" . $address_id) == 1)
			{
				$result = set_visibility_for_suppliers($project["project_order"], $address_id);	
				$project_is_urgent = true;


				//get recipient
				$sql = "select user_id from users " . 
					   "where user_address = " . dbquote($address_id) . 
					   " and user_active = 1";
				$res = mysql_query($sql) or dberror($sql);

				if ($row = mysql_fetch_assoc($res))
				{
					$link = APPLICATION_URL . "/user/project_edit_material_list_supplier.php?pid=" . $project["project_id"];
					//send mail to the supplier
					$actionmail = new ActionMail(108);
					$actionmail->setParam('projectID', $project["project_id"]);
					$actionmail->setParam('recipient', $row["user_id"]);
					$actionmail->setParam('link', $link);
					
					if($senmail_activated == true)
					{
						$actionmail->send();
					}
				}
			}
		}
	}

	if($project_is_urgent == true)
	{
		$sql = "update projects " .
			   "set project_is_urgent = 1 " .
			   " where project_id = " . dbquote(param("pid"));

		mysql_query($sql) or dberror($sql);
	}

	$link = "project_edit_material_list.php?pid=" . param("pid");
	redirect($link);
}


/********************************************************************
    Create List for Local Construction Cost Positions
*********************************************************************/ 
$list_co = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list_co->set_entity("order_item");
$list_co->set_filter("order_item_cost_group in(7) and order_item_order = " . $project["project_order"]);
$list_co->set_order("item_code, order_item_id");


$link="project_edit_material_list_edit_item.php?pid=" . param("pid");
$list_co->add_column("item_shortcut", "Item Code", $link, "", "", COLUMN_NO_WRAP);

$list_co->add_column("order_item_text", "Name");

$list_co->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT);
$list_co->set_footer("item_shortcut",  "Total");
$list_co->set_footer("order_item_system_price", number_format($project_cost_construction,2, ".", "'") . "\n" . $system_currency["symbol"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list_co->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_co->set_footer("order_item_client_price", number_format($project_cost_construction_loc,2, ".", "'") . "\n" . $order_currency["symbol"]);
}

$list_co->add_column("order_item_supplier_freetext", "Supplier Info");


if ($project["order_budget_is_locked"] == 1)
{
    $list_co->add_text_column("discount", "Discount in %", 0, $discount_rates);
}
else
{
    
	$list_co->add_edit_column("discount", "Discount in %", 10, 0, $discount_rates);

	//$list_co->add_button("add_lccost_1", "Add Local Construction from Catalog");
	if($freezed_prices_exist == false)
	{
		$list_co->add_button("delete_construction_cost", "Delete All Construction Cost");
	}

	$list_co->add_button("add_construction_cost", "Add Local Construction Position");
	//$list_co->add_button("add_standard_cost_estimation_co", "Add Standard Cost Estimation");

	$list_co->add_button("save_dicount", "Save Dicount Values");
}



//process list_co
$list_co->populate();
$list_co->process();

/*
if ($list_co->button("add_lccost_1"))
{
    $link = "project_add_catalog_item_lc_cost_estimation_list.php?pid=" . param("pid") . "&oid=" . param("oid");
    redirect($link);
}
*/
if ($list_co->button("add_construction_cost"))
{
    $link = "project_add_lc_cost_estimation_individual.php?pid=" . param("pid") . "&oid=" . $project["project_order"] . "&cg=7"; 
    redirect($link);
}
elseif ($list_co->button("add_standard_cost_estimation_co"))
{
	$link = "project_add_catalog_item_cost_estimation_list.php?pid=" . param("pid") . "&oid=" . param("oid") . "&cg=7"; 
    redirect($link);
}
elseif ($list_co->button("delete_construction_cost"))
{
	$link = "project_edit_material_list_delete_items.php?pid=" . param("pid") . "&oid=" . param("oid") . "&cg=7";
    redirect($link);
    //order_delete_all_items($project["project_order"], 6);
}
elseif ($list_co->button("save_dicount"))
{
	$new_discount_rates = array();
	foreach($list_co->columns as $key=>$column)
	{
		if($column["name"] == "discount")
		{
			foreach($column["values"] as $id=>$value)
			{
				$new_discount_rates[$id] = $value;

			}
		}
	}


	foreach($new_discount_rates as $order_item_id=>$value)
	{
		
		//$system_prices = array();
		//$client_prices = array();


		if($new_discount_rates[$order_item_id] != $discount_rates[$order_item_id])
		{
			$system_price = $system_prices[$order_item_id]/(1-$discount_rates[$order_item_id]/100);
			$client_price = $client_prices[$order_item_id]/(1-$discount_rates[$order_item_id]/100);

			$system_price = round($system_price - $system_price*$new_discount_rates[$order_item_id]/100);
			$client_price = round($client_price - $client_price*$new_discount_rates[$order_item_id]/100);
			
			
			$fields = array();
			$fields[] = "order_item_discount = " . dbquote($value);
			$fields[] = "order_item_system_price = " . dbquote($system_price);
			$fields[] = "order_item_client_price = " . dbquote($client_price);

			$sql = "update order_items set " . join(", ", $fields) . " where order_item_id = " . $order_item_id;
			mysql_query($sql) or dberror($sql);


			$lc = $localcostgroupcodes[$order_item_id];
			if($lc['localcostgroupcode']) {
			
				$fields = array();
				$fields[] = "lwoffergroup_discount = " . dbquote($value);

				$sql = "update lwoffergroups set " . join(", ", $fields) . " where lwoffergroup_offer = " . $lc['lwoffer_id'] . " and lwoffergroup_code = " . dbquote($lc['localcostgroupcode']);
				mysql_query($sql) or dberror($sql);
			
			}
		}
	}
}


/********************************************************************
    Create List for standard items
*********************************************************************/ 
$list_sf = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list_sf->set_entity("order_item");
$list_sf->set_filter("order_item_cost_group in(2) and order_item_type = 1 and order_item_order = " . $project["project_order"]);
$list_sf->set_order("supplier_company, item_code");
$list_sf->set_group("cat_name");

$link="project_edit_material_list_edit_item.php?pid=" . param("pid");
$list_sf->add_column("item_shortcut", "Item Code", $link, "", "", COLUMN_NO_WRAP);
$list_sf->add_text_column("pix", "Pix", COLUMN_UNDERSTAND_HTML, $images);

$list_sf->add_column("order_item_in_budget", "in Budget", "", "", "", COLUMN_NO_WRAP | COLUMN_ALIGN_CENTER);
$list_sf->add_image_column("property", "", 0, $property);
$list_sf->add_column("order_item_text", "Name");

$list_sf->add_checkbox_column("order_item_exclude_from_ps", "local", 0, $exclude_from_ps);

if ($project["order_budget_is_locked"] == 1)
{
    $list_sf->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
}
else
{
    $list_sf->add_edit_column("item_entry_field", "Quantity", "4", 0, $quantities);
}

$list_sf->add_column("unit_name", "Unit", "", "", "", COLUMN_NO_WRAP);

$list_sf->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT);
$list_sf->add_column("total_price", "Total " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list_sf->set_footer("item_shortcut", "Total");
$list_sf->set_footer("total_price", number_format($standard_item_total["in_system_currency"],2, ".", "'") . "\n" . $system_currency["symbol"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list_sf->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_sf->add_column("total_price_loc", "Total " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list_sf->set_footer("total_price_loc", number_format($standard_item_total["in_order_currency"],2, ".", "'") . "\n" . $order_currency["symbol"]);
}

$list_sf->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);


//$list_sf->add_list_column("item_suppliers", "Supplier", $sql_suppliers, NOTNULL, $item_suppliers);
//$list_sf->add_list_column("item_forwarderss", "Forwarder", $sql_forwarders, NOTNULL, $item_forwarders);
$list_sf->add_column("supplier_company", "Supplier");
$list_sf->add_column("forwarder_company", "Forwarder");


if ($project["order_budget_is_locked"] == 1)
{
    //$list_sf->add_button("nothing", "");
}
else
{
    if($freezed_prices_exist == false)
	{
		$list_sf->add_button("delete_items_sf", "Delete All HQ Supplied Items");
	}

    $list_sf->add_button("add_standad_items", "Add Standard Items");

	if($num_of_standard_items > 0)
    {
        $list_sf->add_button("save_list_sf", "Save List");
    }
    
	
}

$entityValue = http_build_query(array(
	'frame' => true,
	'nofilters' => true,
	'entity' => 'project_material_list',
	'entity_values' => array(
		'pid' => param("pid")
	)
));

$list_sf->add_button(LIST_BUTTON_LINK, "Print Product Info Sheets", array(
	'href' => '/applications/templates/item.booklet.php?'.$entityValue,
	'data-fancybox-type' => 'iframe'
));


//process list_sf
$list_sf->populate();
$list_sf->process();

if ($list_sf->button("add_standad_items"))
{
    $link = "project_add_catalog_item_categories.php?pid=" . param("pid") . "&oid=" . $project["project_order"]; 
    redirect($link);
}
elseif ($list_sf->button("save_list_sf"))
{
	
	// check if quntity has changed and set order_date to NULL
	foreach ($list_sf->values("item_entry_field") as $key=>$value)
    {
		if($project["order_actual_order_state_code"] > '620' and $quantities[$key] > 0 and $quantities[$key] != $value)
		 {
			 $revision = $revisions[$key] + 1;
			 $sql = "update order_items set " .
				   "order_item_ordered = NULL, " .
				   "order_item_order_revisions = " . $revision .
				   " where order_item_id = " . $key;

			$res = mysql_query($sql) or dberror($sql);
			
		 }
	}

	foreach($exclude_from_ps as $key=>$value)
	{
		if(isset($_REQUEST["__order_item_order_item_exclude_from_ps_" . $key]))
		{
			$sql = "update order_items set " .
				   "order_item_exclude_from_ps = 1 " .
				   " where order_item_id = " . $key;
		}
		else
		{
			$sql = "update order_items set " .
				   "order_item_exclude_from_ps = 0 " .
				   " where order_item_id = " . $key;
		}
		$res = mysql_query($sql) or dberror($sql);

	}

	

	project_update_order_items($list_sf);

	$link = "project_edit_material_list.php?pid=" . param("pid");
	redirect($link);
}
elseif ($list_sf->button("delete_items_sf"))
{
    //order_delete_all_items($project["project_order"], 1);
    $link = "project_edit_material_list_delete_items.php?pid=" . param("pid") . "&oid=" . param("oid") . "&cg=2,6,10";
    redirect($link);
}




/********************************************************************
    Create List for special items
*********************************************************************/ 
$list_si = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list_si->set_entity("order_item");
$list_si->set_filter("order_item_cost_group in(0, 2) and order_item_type = 2 and order_item_order = " . $project["project_order"]);
$list_si->set_order("supplier_company, item_code");
$list_si->set_group("cat_name");

$link="project_edit_material_list_edit_item.php?pid=" . param("pid");
$list_si->add_column("item_shortcut", "Item Code", $link, "", "", COLUMN_NO_WRAP);
//$list_si->add_text_column("pix", "Pix", COLUMN_UNDERSTAND_HTML, $images);

$list_si->add_column("order_item_in_budget", "in Budget", "", "", "", COLUMN_NO_WRAP | COLUMN_ALIGN_CENTER);
//$list_si->add_image_column("property", "", 0, $property);
$list_si->add_column("order_item_text", "Name");

$list_si->add_checkbox_column("order_item_exclude_from_ps", "local", 0, $exclude_from_ps);
$list_si->add_text_column("split", "", COLUMN_UNDERSTAND_HTML | COLUMN_NO_WRAP, $splitting_links);


if ($project["order_budget_is_locked"] == 1)
{
    $list_si->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
}
else
{
    $list_si->add_edit_column("item_entry_field", "Quantity", "4", 0, $quantities);
}

$list_si->add_column("unit_name", "Unit", "", "", "", COLUMN_NO_WRAP);

$list_si->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT);
$list_si->add_column("total_price", "Total " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list_si->set_footer("item_shortcut", "Total");
$list_si->set_footer("total_price", number_format($special_item_total["in_system_currency"],2, ".", "'") . "\n" . $system_currency["symbol"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list_si->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_si->add_column("total_price_loc", "Total " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list_si->set_footer("total_price_loc", number_format($special_item_total["in_order_currency"],2, ".", "'") . "\n" . $order_currency["symbol"]);
}

$list_si->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);


//$list_si->add_list_column("item_suppliers", "Supplier", $sql_suppliers, NOTNULL, $item_suppliers);
//$list_si->add_list_column("item_forwarderss", "Forwarder", $sql_forwarders, NOTNULL, $item_forwarders);
$list_si->add_column("supplier_company", "Supplier");
$list_si->add_column("forwarder_company", "Forwarder");



if ($project["order_budget_is_locked"] == 1)
{
    $list_si->add_button("nothing", "");
}
else
{
	$list_si->add_button("add_special_item", "Add Special Item");
	
	if($number_of_standard_special_items > 0)
	{
		$list_si->add_button("add_standard_special_items", "Add Standard Special Items");
	}
	if($num_of_special_items > 0)
    {
        $list_si->add_button("save_list_si", "Save List");
    }
}


//process list_si
$list_si->populate();
$list_si->process();

if ($list_si->button("add_special_item"))
{
    $link = "project_add_special_item_individual.php?pid=" . param("pid") . "&oid=" . $project["project_order"]; 
    redirect($link);
}
elseif($list_si->button("add_standard_special_items"))
{
	$link = "project_add_catalog_item_categories.php?pid=" . param("pid") . "&oid=" . $project["project_order"] ."&type=2"; 
	redirect($link);
}
elseif ($list_sf->button("save_list_si"))
{
	
	// check if quntity has changed and set order_date to NULL
	foreach ($list_sf->values("item_entry_field") as $key=>$value)
    {
		if($project["order_actual_order_state_code"] > '620' and $quantities[$key] > 0 and $quantities[$key] != $value)
		 {
			 $revision = $revisions[$key] + 1;
			 $sql = "update order_items set " .
				   "order_item_ordered = NULL, " .
				   "order_item_order_revisions = " . $revision .
				   " where order_item_id = " . $key;

			$res = mysql_query($sql) or dberror($sql);
			
		 }
	}

	foreach($exclude_from_ps as $key=>$value)
	{
		if(isset($_REQUEST["__order_item_order_item_exclude_from_ps_" . $key]))
		{
			$sql = "update order_items set " .
				   "order_item_exclude_from_ps = 1 " .
				   " where order_item_id = " . $key;
		}
		else
		{
			$sql = "update order_items set " .
				   "order_item_exclude_from_ps = 0 " .
				   " where order_item_id = " . $key;
		}
		$res = mysql_query($sql) or dberror($sql);

	}

	

	project_update_order_items($list_sf);

	$link = "project_edit_material_list.php?pid=" . param("pid");
	redirect($link);
}



/********************************************************************
    Create List for freight charges
*********************************************************************/ 
$list_fc = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list_fc->set_entity("order_item");
$list_fc->set_filter("order_item_cost_group in(6) and order_item_order = " . $project["project_order"]);
$list_fc->set_order("supplier_company, item_code");
$list_fc->set_group("cat_name");

$link="project_edit_material_list_edit_item.php?pid=" . param("pid");
$list_fc->add_column("item_shortcut", "Item Code", $link, "", "", COLUMN_NO_WRAP);
//$list_fc->add_text_column("pix", "Pix", COLUMN_UNDERSTAND_HTML, $images);

//$list_fc->add_column("order_item_in_budget", "in Budget", "", "", "", COLUMN_NO_WRAP | COLUMN_ALIGN_CENTER);
//$list_fc->add_image_column("property", "", 0, $property);
$list_fc->add_column("order_item_text", "Name");

//$list_fc->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
//$list_fc->add_column("unit_name", "Unit", "", "", "", COLUMN_NO_WRAP);

$list_fc->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT);
$list_fc->add_column("total_price", "Total " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list_fc->set_footer("item_shortcut", "Total");
$list_fc->set_footer("total_price", number_format($project_cost_freight_charges,2, ".", "'") . "\n" . $system_currency["symbol"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list_fc->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_fc->add_column("total_price_loc", "Total " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list_fc->set_footer("total_price_loc", number_format($project_cost_freight_charges_loc,2, ".", "'") . "\n" . $order_currency["symbol"]);
}

$list_fc->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);


$list_fc->add_column("order_item_supplier_freetext", "Supplier Info");



if ($project["order_budget_is_locked"] == 1)
{
    $list_fc->add_button("nothing", "");
}
else
{
	$list_fc->add_button("add_standard_cost_estimation_fc", "Add Standard Freight Charges");
	$list_fc->add_button("add_freight_charges", "Add Individual Freight Charges");
}


//process list_sf
$list_fc->populate();
$list_fc->process();

if ($list_fc->button("add_standard_cost_estimation_fc"))
{
	$link = "project_add_catalog_item_cost_estimation_list.php?pid=" . param("pid") . "&oid=" . param("oid") . "&cg=6"; 
    redirect($link);
}
elseif ($list_fc->button("add_freight_charges"))
{
	$link = "project_add_cost_estimation_individual.php?pid=" . param("pid") . "&oid=" . $project["project_order"] . "&cg=6"; 
    redirect($link);
}

/********************************************************************
    Create List for other cost
*********************************************************************/ 
$list_of = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list_of->set_entity("order_item");
$list_of->set_filter("(order_item_cost_group in(10) or (order_item_type = 3 and order_item_cost_group in(2))) and order_item_order = " . $project["project_order"]);
$list_of->set_order("supplier_company, item_code");
$list_of->set_group("cat_name");

$link="project_edit_material_list_edit_item.php?pid=" . param("pid");
$list_of->add_column("item_shortcut", "Item Code", $link, "", "", COLUMN_NO_WRAP);
//$list_of->add_text_column("pix", "Pix", COLUMN_UNDERSTAND_HTML, $images);

//$list_of->add_column("order_item_in_budget", "in Budget", "", "", "", COLUMN_NO_WRAP | COLUMN_ALIGN_CENTER);
//$list_of->add_image_column("property", "", 0, $property);
$list_of->add_column("order_item_text", "Name");

//$list_of->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT);
//$list_of->add_column("unit_name", "Unit", "", "", "", COLUMN_NO_WRAP);

$list_of->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT);
$list_of->add_column("total_price", "Total " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list_of->set_footer("item_shortcut", "Total");
$list_of->set_footer("total_price", number_format($project_cost_fixturing_other + $cost_estimation_item_total_furniture["in_system_currency"],2, ".", "'") . "\n" . $system_currency["symbol"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list_of->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_of->add_column("total_price_loc", "Total " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
	$list_of->set_footer("total_price_loc", number_format($project_cost_fixturing_other_loc+ $cost_estimation_item_total_furniture["in_order_currency"],2, ".", "'") . "\n" . $order_currency["symbol"]);
}

$list_of->add_column("order_item_po_number", "P.O. Number", "", "", "", COLUMN_NO_WRAP);


$list_of->add_column("order_item_supplier_freetext", "Supplier Info");


if ($project["order_budget_is_locked"] == 1)
{
    $list_of->add_button("nothing", "");
}
else
{
    
	//$list_of->add_button("add_standard_cost_estimation_of", "Add Standard Cost Esitmation");
	$list_of->add_button("add_individual_cost_estimation_of", "Add Cost Estimation");
}


//process list_of
$list_of->populate();
$list_of->process();

if ($list_of->button("add_standard_cost_estimation_of"))
{
	$link = "project_add_catalog_item_cost_estimation_list.php?pid=" . param("pid") . "&oid=" . param("oid") . "&cg=8"; 
    redirect($link);
}
elseif ($list_of->button("add_individual_cost_estimation_of"))
{
	$link = "project_add_cost_estimation_individual.php?pid=" . param("pid") . "&oid=" . $project["project_order"] . "&cg=10"; 
    redirect($link);
}


/********************************************************************
    Create List for Architectural Cost
*********************************************************************/ 
$list_ar = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list_ar->set_entity("order_item");
$list_ar->set_filter("order_item_cost_group in(9) and order_item_order = " . $project["project_order"]);
$list_ar->set_order("item_code, order_item_id");

$link="project_edit_material_list_edit_item.php?pid=" . param("pid");
$list_ar->add_column("item_shortcut", "Item Code", $link, "", "", COLUMN_NO_WRAP);

$list_ar->add_column("order_item_text", "Name");

$list_ar->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT);
$list_ar->set_footer("item_shortcut", "Total");
$list_ar->set_footer("order_item_system_price", number_format($project_cost_architectural,2, ".", "'") . "\n" . $system_currency["symbol"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list_ar->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_ar->set_footer("order_item_client_price",  number_format($project_cost_architectural_loc,2, ".", "'") . "\n" . $order_currency["symbol"]);
}
$list_ar->add_column("order_item_supplier_freetext", "Supplier Info");




if ($project["order_budget_is_locked"] == 1)
{
    $list_ar->add_button("nothing", "");
}
else
{
	if($freezed_prices_exist == false)
	{
		$list_ar->add_button("delete_architectural_cost", "Delete All Architectural Services");
	}

	$list_ar->add_button("add_architectural_cost", "Add Architectural Services");
	//$list_ar->add_button("add_standard_cost_estimation_ar", "Add Standard Cost Estimation");
}



//process list_ar
$list_ar->populate();
$list_ar->process();

if ($list_ar->button("add_architectural_cost"))
{
    $link = "project_add_cost_estimation_individual.php?pid=" . param("pid") . "&oid=" . $project["project_order"] . "&cg=9"; 
    redirect($link);
}
elseif ($list_ar->button("add_standard_cost_estimation_ar"))
{
	$link = "project_add_catalog_item_cost_estimation_list.php?pid=" . param("pid") . "&oid=" . param("oid") . "&cg=9"; 
    redirect($link);
}
elseif ($list_ar->button("delete_architectural_cost"))
{
	$link = "project_edit_material_list_delete_items.php?pid=" . param("pid") . "&oid=" . param("oid") . "&cg=9";
    redirect($link);
    //order_delete_all_items($project["project_order"], 6);
}




/********************************************************************
    Create List for Equipment
*********************************************************************/ 
$list_eq = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list_eq->set_entity("order_item");
$list_eq->set_filter("order_item_cost_group in(11) and order_item_order = " . $project["project_order"]);
$list_eq->set_order("item_code, order_item_id");

$link="project_edit_material_list_edit_item.php?pid=" . param("pid");

$list_eq->add_column("item_shortcut", "Item Code", $link, "", "", COLUMN_NO_WRAP);

$list_eq->add_column("order_item_text", "Name");
$list_eq->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT);
$list_eq->set_footer("item_shortcut", "Total");
$list_eq->set_footer("order_item_system_price", number_format($project_cost_equipment,2, ".", "'") . "\n" . $system_currency["symbol"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list_eq->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_eq->set_footer("order_item_client_price", number_format($project_cost_equipment_loc,2, ".", "'") . "\n" . $order_currency["symbol"]);
}
$list_eq->add_column("order_item_supplier_freetext", "Supplier Info");



if ($project["order_budget_is_locked"] == 1)
{
    $list_eq->add_button("nothing", "");
}
else
{
	if($freezed_prices_exist == false)
	{
		$list_eq->add_button("delete_equipment_cost", "Delete All Equipment");
	}

	$list_eq->add_button("add_equipment_cost", "Add Equipment");
	//$list_eq->add_button("add_standard_cost_estimation_eq", "Add Standard Cost Estimation");
}



//process list_eq
$list_eq->populate();
$list_eq->process();

if ($list_eq->button("add_equipment_cost"))
{
    $link = "project_add_cost_estimation_individual.php?pid=" . param("pid") . "&oid=" . $project["project_order"] . "&cg=11"; 
    redirect($link);
}
elseif ($list_eq->button("add_standard_cost_estimation_eq"))
{
	$link = "project_add_catalog_item_cost_estimation_list.php?pid=" . param("pid") . "&oid=" . param("oid") . "&cg=11"; 
    redirect($link);
}
elseif ($list_eq->button("delete_equipment_cost"))
{
	$link = "project_edit_material_list_delete_items.php?pid=" . param("pid") . "&oid=" . param("oid") . "&cg=11";
    redirect($link);
    //order_delete_all_items($project["project_order"], 6);
}


/********************************************************************
    Create List for Equipment
*********************************************************************/ 
$list_ot = new ListView($sql_order_items, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list_ot->set_entity("order_item");
$list_ot->set_filter("order_item_cost_group in(8) and order_item_order = " . $project["project_order"]);
$list_ot->set_order("item_code, order_item_id");

$link="project_edit_material_list_edit_item.php?pid=" . param("pid");

$list_ot->add_column("item_shortcut", "Item Code", $link, "", "", COLUMN_NO_WRAP);
$list_ot->add_column("order_item_text", "Name");
$list_ot->add_column("order_item_system_price", "Price " . $system_currency["symbol"], "", "", "", COLUMN_BG_STRONG | COLUMN_ALIGN_RIGHT);
$list_ot->set_footer("item_shortcut", "Total");
$list_ot->set_footer("order_item_system_price", number_format($project_cost_other,2, ".", "'") . "\n" . $system_currency["symbol"]);

if($order_currency["symbol"] != $system_currency["symbol"])
{
	$list_ot->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT);
	$list_ot->set_footer("order_item_client_price", number_format($project_cost_other_loc,2, ".", "'") . "\n" . $order_currency["symbol"]);
}
$list_ot->add_column("order_item_supplier_freetext", "Supplier Info");


if ($project["order_budget_is_locked"] == 1)
{
    $list_ot->add_button("nothing", "");
}
else
{
	if($freezed_prices_exist == false)
	{
		$list_ot->add_button("delete_other_cost", "Delete All Other");
	}

	$list_ot->add_button("add_other_cost", "Add Other");
	//$list_ot->add_button("add_standard_cost_estimation_ot", "Add Standard Cost Estimation");
}



//process list_ot
$list_ot->populate();
$list_ot->process();

if ($list_ot->button("add_other_cost"))
{
    $link = "project_add_cost_estimation_individual.php?pid=" . param("pid") . "&oid=" . $project["project_order"] . "&cg=8"; 
    redirect($link);
}
elseif ($list_ot->button("add_standard_cost_estimation_ot"))
{
	$link = "project_add_catalog_item_cost_estimation_list.php?pid=" . param("pid") . "&oid=" . param("oid") . "&cg=8"; 
    redirect($link);
}
elseif ($list_ot->button("delete_other_cost"))
{
	$link = "project_edit_material_list_delete_items.php?pid=" . param("pid") . "&oid=" . param("oid") . "&cg=8";
    redirect($link);
    //order_delete_all_items($project["project_order"], 6);
}



/********************************************************************
    Create List for item in projects coming from catalogue orders
*********************************************************************/ 
$has_catalogue_orders = false;
$sql_oip = "select order_items_in_project_id, order_items_in_project_order_item_id, " .
		   "order_items_in_project_id, order_item_order, order_items_in_project_cms_remark, " . 
		   " IF(address_shortcut <> '', address_shortcut, order_item_supplier_freetext) as address_shortcut, " .
		   "order_item_text,  order_item_id, " .
		   "DATE_FORMAT(order_item_ordered, '%d.%m.%y') as date1, " .
			"DATE_FORMAT(order_item_pickup, '%d.%m.%y') as date2, " .
			"DATE_FORMAT(order_item_arrival, '%d.%m.%y') as date3, " .
			"order_item_quantity, order_item_system_price, order_item_client_price, " . 
		   " order_item_po_number, order_item_quantity, project_cost_groupname_name, order_items_in_project_real_price " . 
		   " from order_items_in_projects " .
		   " left join order_items on order_item_id = order_items_in_project_order_item_id " .
		   " left join addresses on order_item_supplier_address = address_id ".
		   " left join project_cost_groupnames on project_cost_groupname_id =  order_items_in_project_costgroup_id " .
		   " where order_items_in_project_project_order_id = " . $project["project_order"];


$replacementinfos = array();
$res = mysql_query($sql_oip) or dberror($sql_oip);
while($row = mysql_fetch_assoc($res))
{
	//check if item is a replacement
	$sql_r = "select count(order_item_replacement_id) as num_recs " . 
		     " from order_item_replacements " . 
		     " where order_item_replacement_catalog_order_order_item_id = " . dbquote($row["order_item_id"]);


	$res_r = mysql_query($sql_r) or dberror($sql_r);
	$row_r = mysql_fetch_assoc($res_r);
	if($row_r["num_recs"] > 0)
	{
		$replacementinfos[$row["order_items_in_project_id"]] = '<span class="error">Replacement</span>';
	}
}

$link = "/user/order_assign_items_to_a_project.php?oid={order_item_order}";

$list_oip = new ListView($sql_oip, LIST_HAS_HEADER | LIST_HAS_FOOTER);

$list_oip->set_title("Catalogue Orders");
$list_oip->set_entity("order_items_in_projects");

$list_oip->add_column("address_shortcut", "Supplier", "", "", "", COLUMN_NO_WRAP);
$list_oip->add_column("project_cost_groupname_name", "Cost Group", "", "", "", COLUMN_NO_WRAP);
$list_oip->add_text_column("replacement", "", COLUMN_NO_WRAP | COLUMN_UNDERSTAND_HTML, $replacementinfos);
$list_oip->add_column("order_item_text", "Description", $link, "", "");
$list_oip->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);

$list_oip->add_column("order_item_system_price", "Price CHF", "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);
$list_oip->add_column("order_item_client_price", "Price " . $order_currency["symbol"], "", "", "", COLUMN_ALIGN_RIGHT | COLUMN_NO_WRAP);


$list_oip->populate();
$list_oip->process();


/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("Edit List of Materials");
$form->render();
echo "<br />";

if(has_access("can_edit_list_of_materials_in_projects_lc"))
{
	echo "<h3>Local Construction</h3><hr />";
	$list_co->render();
	echo "<br />";
	echo "<br />";
}

if(has_access("can_edit_list_of_materials_in_projects_sf"))
{
	echo "<h3>Store fixturing / Furniture - Standard Items</h3><hr />";
	$list_sf->render();
	echo "<br />";
	echo "<br />";

	echo "<h3>Store fixturing / Furniture - Special Items</h3><hr />";
	$list_si->render();
	echo "<br />";
	echo "<br />";


	echo "<h3>Store fixturing / Furniture - Freight Charges</h3><hr />";
	$list_fc->render();
	echo "<br />";
	echo "<br />";
}

if(has_access("can_edit_list_of_materials_in_projects_sfo"))
{
	echo "<h3>Store fixturing / Furniture - Other</h3><hr />";
	$list_of->render();
	echo "<br />";
	echo "<br />";
}

if(has_access("can_edit_list_of_materials_in_projects_arch"))
{
	echo "<h3>Architectural Services</h3><hr />";
	$list_ar->render();
	echo "<br />";
	echo "<br />";
}

if(has_access("can_edit_list_of_materials_in_projects_eq"))
{
	echo "<h3>Equipment</h3><hr />";
	$list_eq->render();
	echo "<br />";
	echo "<br />";
}

if(has_access("can_edit_list_of_materials_in_projects_otc"))
{
	echo "<h3>Other</h3><hr />";
	$list_ot->render();
	echo "<br />";
}


echo "<hr />";
$list_oip->render();


require_once("include/order_local_construction_discount.php");


//require_once("include/order_currency_helper.php");

require_once "include/project_footer_logistic_state.php";

?>

<script type="text/javascript">
jQuery(document).ready(function($) {
  
  <?php
  
	foreach($splitting_links as $order_item_id=>$link) {
  ?>
  
  $('#item<?php echo $order_item_id;?>').click(function(e) {
    e.preventDefault();
    $.nyroModalManual({
      url: '/user/split_items_in_list_of_materials.php?pid=<?php echo param("pid");?>&order_item_id=<?php echo $order_item_id;?>'
    });
    return false;
  });

  <?php
	}
  ?>
  
});


</script>

<?php

$page->footer();

?>