<?php
/********************************************************************

    order_budget_position.php

    Add cost estimation positions to an order

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2002-09-15
    Modified by:    Oliver Hofer (oliver.hofer@pageagent.com)
    Date modified:  2003-02-16
    Version:        1.0.1

    Copyright (c) 2002, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";
require_once "include/save_functions.php";
require_once "include/order_functions.php";

check_access("can_edit_budget_in_orders");


/********************************************************************
    prepare all data needed
*********************************************************************/
// read project and order details
$order = get_order(param("oid"));

// get company's address
$client_address = get_address($order["order_client_address"]);

// get system currency
$system_currency = get_system_currency_fields();

// get item type name
if (param("type"))
{
    $type = param("type");
}
else
{
    $type = param("order_item_type");
}


$sql_item_types = "select item_type_name ".
                  "from item_types ".
                  "where item_type_id = " .  $type ;
               
$res = mysql_query($sql_item_types) or dberror($sql_item_types);
if ($row = mysql_fetch_assoc($res))
{
    $section_title = $row["item_type_name"];
}


/********************************************************************
    build form
*********************************************************************/
$form = new Form("order_items", "order item", 640);

$form->add_section("Order");
$form->add_hidden("oid",param('oid'));
$form->add_hidden("order_item_id", id());
$form->add_hidden("order_item_order",param('oid'));
$form->add_hidden("order_item_type",  $type);

require_once "include/order_head_small.php";

$form->add_section($section_title);
$form->add_multiline("order_item_text", "Description*", 4, NOTNULL);


/********************************************************************
    Add Validation Rules
*********************************************************************/ 

$form->add_button(FORM_BUTTON_SAVE, "Save");
$form->add_button(FORM_BUTTON_BACK, "Back");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();
$form->process();

if ($form->button(FORM_BUTTON_SAVE))
{
    if ($form->validate())
    {
        $link = "order_edit_order_budget.php?oid=" . param("oid");         
        redirect($link);
    }
}

   
/********************************************************************
    render page
*********************************************************************/
$page = new Page("orders");

require "include/order_page_actions.php";

$page->header();
$page->title("Project Budget: Add Position");
$form->render();
$page->footer();

?>