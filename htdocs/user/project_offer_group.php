<?php
/********************************************************************

    project_offer_group

    Edit Local Constrction Cost Group

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2008-03-23
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2008-03-23
    Version:        1.0.0

    Copyright (c) 2008, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("can_edit_local_constrction_work");


// read project and order details
$project = get_project(param("pid"));

//get offer currency
$sql = "select lwoffer_currency from lwoffers where lwoffer_id = " . param("lwoid");
$res = mysql_query($sql) or dberror($sql);
$row = mysql_fetch_assoc($res);

$offer_currency = get_currency_symbol($row["lwoffer_currency"]);


/********************************************************************
    Create Form
*********************************************************************/ 

$form = new Form("lwoffergroups", "lwoffergroups");

$form->add_section();
$form->add_hidden("pid", param("pid"));
$form->add_hidden("lwoid", param("lwoid"));
$form->add_hidden("lwoffergroup_offer", param("lwoid"));

$form->add_list("lwoffergroup_group", "Cost Group",
    "select lwgroup_id, concat(lwgroup_code, ' ' , lwgroup_text) " .
    "from lwgroups " .
    " order by lwgroup_group, lwgroup_code");

$form->add_edit("lwoffergroup_code", "Code", NOTNULL);
$form->add_edit("lwoffergroup_text", "Description", NOTNULL);
$form->add_edit("lwoffergroup_action", "Action by");
$form->add_edit("lwoffergroup_price", "Price in " . $offer_currency, 0, "", TYPE_DECIMAL, 12,2);

//$form->add_edit("lwoffergroup_price_billed", "Billed Price in " . $offer_currency, 0, "", TYPE_DECIMAL, 12,2);

//$form->add_checkbox("lwoffergroup_hasoffer", "Has to be offered");

$form->add_checkbox("lwoffergroup_inbudget", "Is in Budget");

$form->add_button(FORM_BUTTON_SAVE, "Save");

if(id())
{
    $form->add_button("delete", "Delete");
}

$form->add_button("back", "Back");


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 

$form->populate();
$form->process();


if ($form->button("back"))
{
    $link = "project_offer.php?id=" . param("lwoid") . "&pid=" . param("pid");
    redirect ($link);
}
elseif ($form->button("delete"))
{
    $sql = "delete from lwoffergroups where lwoffergroup_id = " . id();
    $res = mysql_query($sql) or dberror($sql);
    
    $link = "project_offer.php?id=" . param("lwoid") . "&pid=" . param("pid");
    redirect ($link);
}


/********************************************************************
    Populate list and process button clicks
*********************************************************************/

$page = new Page("projects");
require "include/project_page_actions.php";
$page->header();
$page->title("Edit Local Construction Group");
$form->render();
$page->footer();

?>