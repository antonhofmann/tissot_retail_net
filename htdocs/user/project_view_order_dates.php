<?php
/********************************************************************

    project_view_order_dates.php

    View all order dates

    Created by:     Anton Hofmann (aho@mediaparx.ch)
    Date created:   2015-03-31
    Modified by:    Anton Hofmann (aho@mediaparx.ch)
    Date modified:  2015-03-31
    Version:        1.0.1

    Copyright (c) 2015, Swatch AG, All Rights Reserved.

*********************************************************************/

require_once "../include/frame.php";
require_once "include/get_functions.php";

check_access("has_access_to_all_projects");

register_param("pid");


/********************************************************************
    prepare all data needed
*********************************************************************/
// get user_data
$user_data = get_user(user_id());
$user_roles = get_user_roles(user_id());

// read project and order details
$project = get_project(param("pid"));

// get company's address
$client_address = get_address($project["order_client_address"]);

// create sql for order items
$sql_order_items = "select order_item_id, order_item_quantity, " .
				   "    DATE_FORMAT(order_item_ordered,'%d.%m.%y') as order_item_ordered, ".
                   "    concat(if(order_item_item <>'', item_code, item_type_name), ' ', order_item_text) as item_info, " .
				   "    order_item_preferred_arrival_date, " . 
                   "    addresses.address_company as forwarder_company, ".
                   "    addresses_1.address_company as supplier_company ".
				   "from order_items ".
                   "left join items on order_item_item = item_id ".
                   "left join orders on order_item_order = order_id ".
                   "left join projects on order_id = project_order ".
                   "left join item_types on order_item_type = item_type_id ".
                   "left join addresses on order_item_forwarder_address = addresses.address_id ".
                   "left join addresses AS addresses_1 on order_item_supplier_address = addresses_1.address_id ";


$list_filter = "(order_item_not_in_budget = 0 or order_item_not_in_budget is null)".
				  "   and order_item_type <= " . ITEM_TYPE_SPECIAL .
				  "   and order_item_order = " . $project["project_order"] .
				  "   and order_item_forwarder_address > 0 ";



// get order_item_dates
 $sql_order_item_dates = $sql_order_items . " " .
                         "where " . $list_filter;


$dates = get_order_dates_from_dates($sql_order_item_dates);

$ORSU = array();
foreach($dates["ORDR"] as $key=>$value)
{
     $ORSU[$key] = $value;
}

$EXRP = array();
foreach($dates["EXRP"] as $key=>$value)
{
     $EXRP[$key] = $value;
}

$ORFW = array();
foreach($dates["ORFW"] as $key=>$value)
{
     $ORFW[$key] = $value;
}


/********************************************************************
    build form
*********************************************************************/
$form = new Form("projects", "project");

$form->add_hidden("pid", param("pid"));

require_once "include/project_head_small.php";


/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$form->populate();


/********************************************************************
    Create Item List
*********************************************************************/ 

$sql_order_items = str_replace("order_item_text", "concat('<div style=\"max-width:350px;display:block;\">', order_item_text, '</div>')", $sql_order_items);


$list = new ListView($sql_order_items, LIST_HAS_SEPARATOR | LIST_HAS_HEADER);

$list->set_title("Order Dates");
$list->set_entity("order_item");

if(isset($_GET["s"]))
{
	$list->set_group("forwarder_company");
	$list->set_order("forwarder_company");
}
else
{
	$list->set_group("supplier_company");
	$list->set_order("supplier_company");
}


$list->set_filter($list_filter);



$list->add_column("item_info", "Item", "", "", "", COLUMN_UNDERSTAND_HTML | COLUMN_BREAK);
$list->add_column("order_item_quantity", "Quantity", "", "", "", COLUMN_UNDERSTAND_HTML | COLUMN_BREAK);

$list->add_text_column("ORSU", "700", COLUMN_NO_WRAP | COLUMN_BREAK, $ORSU);
$list->add_text_column("ORSU", "Ready for Pickup", COLUMN_NO_WRAP | COLUMN_BREAK, $EXRP);
$list->add_text_column("ORFW", "730", COLUMN_NO_WRAP | COLUMN_BREAK, $ORFW);



if(isset($_GET["s"]))
{
	$list->add_button("gbys", "Group List by Supplier");
}
else
{
	$list->add_button("gbyf", "Group List by Forwarder");
}

/********************************************************************
    Populate form and process button clicks
*********************************************************************/ 
$list->populate();
$list->process();


if ($list->button("gbys"))
{
     $link = "project_view_order_dates.php?pid=" . param("pid"); 
     redirect($link);
}
elseif ($list->button("gbyf"))
{
     
	 $link = "project_view_order_dates.php?pid=" . param("pid") . "&s=1"; 
     redirect($link);
}


/********************************************************************
    render page
*********************************************************************/
$page = new Page("projects");

require "include/project_page_actions.php";

$page->header();
$page->title("View Order Dates");
$form->render();
$list->render();

require_once "include/project_footer_logistic_state.php";
$page->footer();

?>