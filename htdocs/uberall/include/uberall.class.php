<?php

/*XML POS locations for uberall*/
class uberall_xml
{ 
	private $_retailnet_db = array();
	private $_storelocator_db = array();

	private $_countries = array();
	private $_openinghours = array();
	private $_customerservices = array();

	private $_brand_descriptions = array();

	private $_string_translations = array();


	public function __construct($retailnet_db = array(), $storelocator_db = array()) {
      
		
		
		$this->_retailnet_db = $retailnet_db;
		$this->_storelocator_db = $storelocator_db;

		$this->_get_Brand_Descriptions();
		$this->_get_string_translations();

		
    }


	public function createXML($type = 'all')
	{
		
		$xml = '<?xml version="1.0" encoding="UTF-8"?>' . "\r\n";
		$xml .= '<poslocations>' . "\r\n";

		$old_country = '';


		$this->_countries = $this->_getCountries(36, $type);
		
		
		
		
		//temp for developments
		/*
		$countries = array();
		$countries[19]["country_iso3166"] = 'CN';
		$countries[19]["country_store_locator_id"] = 47;
		$this->_countries = $countries;
		*/


		
		


		$languages = array();
		
		foreach($this->_countries as $retailnet_country_id=>$country_data)
		{
			$iso3166 = $country_data["country_iso3166"];
			$store_locator_country_id = $country_data["country_store_locator_id"];
			
			

			$this->_getOpeningHours($retailnet_country_id);
			$this->_getCustomerServices($retailnet_country_id);
			$pos_locations = $this->_getPOSlocations($retailnet_country_id, 36, $type);

			$selected_pos_locations = array();
	
			
			$xml .= '<country country_iso3166="' . $iso3166. '">' . "\r\n";
			
			$xml .= '<language language_iso639_1="en">' . "\r\n";

			foreach($pos_locations as $key=>$data)
			{	
				$selected_pos_locations[$data["posaddress_id"]] = $retailnet_country_id;

				$closed = $this->_check_if_pos_is_open_right_now($data["posaddress_id"]);
				
				
				$xml .= "\t" . '<poslocation identifier="P' . $data["posaddress_id"]. '">' . "\r\n";

				$xml .= "\t\t" . '<name><![CDATA[' . $data['posaddress_name'] .']]></name>' . "\r\n";
				
				
				$xml .= "\t\t" . '<address><![CDATA[' . $data['posaddress_address'] .']]></address>' . "\r\n";

				/*
				if($data['posaddress_street']) {
					$xml .= "\t\t" . '<street><![CDATA[' . $data['posaddress_street'] .']]></street>' . "\r\n";
					$xml .= "\t\t" . '<streetNo><![CDATA[' . $data['posaddress_street_number'] .']]></streetNo>' . "\r\n";
				}
				else {
					
					$xml .= "\t\t" . '<street><![CDATA[]]></street>' . "\r\n";
					$xml .= "\t\t" . '<streetNo><![CDATA[]]></streetNo>' . "\r\n";
				}
				*/

				$xml .= "\t\t" . '<zip><![CDATA[' . $data['posaddress_zip'] .']]></zip>' . "\r\n";
				$xml .= "\t\t" . '<city><![CDATA[' . $data['place_name'] .']]></city>' . "\r\n";
				
				
				if(array_key_exists($brand, $this->_brand_descriptions)) {

					$xml .= "\t\t" . '<category><![CDATA[' . $this->_brand_descriptions[$brand]["brand_uberal_category_ids"] . ']]></category>' . "\r\n";
				}
				else {

					$xml .= "\t\t" . '<category><![CDATA[]]></category>' . "\r\n";
			
				}
				
				$xml .= "\t\t" . '<addressExtra><![CDATA[' . $data['posaddress_address2'] .']]></addressExtra>' . "\r\n";
				$xml .= "\t\t" . '<CIFNIF><![CDATA[]]></CIFNIF>' . "\r\n";
				$xml .= "\t\t" . '<SIREN><![CDATA[]]></SIREN>' . "\r\n";
				$xml .= "\t\t" . '<phone><![CDATA[' . $data['posaddress_phone'] .']]></phone>' . "\r\n";
				$xml .= "\t\t" . '<mobile_phone><![CDATA[' . $data['posaddress_mobile_phone'] .']]></mobile_phone>' . "\r\n";
				$xml .= "\t\t" . '<cellphone><![CDATA[]]></cellphone>' . "\r\n";
				$xml .= "\t\t" . '<website><![CDATA[' . $data['country_website_swatch'] .']]></website>' . "\r\n";
				
				if($data['posaddress_email_on_web'] == 1)
				{
					$xml .= "\t\t" . '<email><![CDATA[' . $data['posaddress_email'] .']]></email>' . "\r\n";
				}
				else
				{
					$xml .= "\t\t" . '<email><![CDATA[]]></email>' . "\r\n";
				}
				
				if(array_key_exists($brand, $this->_brand_descriptions)) {

					$xml .= "\t\t" . '<descriptionShort><![CDATA[' . $this->_brand_descriptions[$brand]["brand_short_description"] . ']]></descriptionShort>' . "\r\n";
					$xml .= "\t\t" . '<descriptionLong><![CDATA[' . $this->_brand_descriptions[$brand]["brand_long_desicrption"] . ']]></descriptionLong>' . "\r\n";
				}
				else {

					$xml .= "\t\t" . '<descriptionShort><![CDATA[]]></descriptionShort>' . "\r\n";
					$xml .= "\t\t" . '<descriptionLong><![CDATA[]]></descriptionLong>' . "\r\n";
				
				}
				
				
				
				if(array_key_exists($data["posaddress_id"], $this->_openinghours) 
					and array_key_exists('open', $this->_openinghours[$data["posaddress_id"]]))
				{
					$xml .= "\t\t" . '<openingHours><![CDATA[' . $this->_openinghours[$data["posaddress_id"]]['open']. ']]></openingHours>' . "\r\n";
				}
				else
				{
					$xml .= "\t\t" . '<openingHours><![CDATA[]]></openingHours>' . "\r\n";
				}

				if(array_key_exists($data["posaddress_id"], $this->_openinghours) 
					and array_key_exists('closed', $this->_openinghours[$data["posaddress_id"]]))
				{
					
					$cstring = '';
					if(array_key_exists('pos.closing.hours', $this->_string_translations))
					{
						$cstring = $this->_string_translations['pos.closing.hours'] . ": ";
					}
					
					$xml .= "\t\t" . '<openingHoursNotes><![CDATA[' . $cstring . $this->_openinghours[$data["posaddress_id"]]['closed']. ']]></openingHoursNotes>' . "\r\n";
				}
				else
				{
					$xml .= "\t\t" . '<openingHoursNotes><![CDATA[]]></openingHoursNotes>' . "\r\n";
				}

				if(array_key_exists($brand, $this->_brand_descriptions)) {
					$xml .= "\t\t" . '<keywords><![CDATA['. $this->_brand_descriptions[$brand]['brand_keywords'] . ']]></keywords>' . "\r\n";
				}
				else
				{
					$xml .= "\t\t" . '<keywords><![CDATA[]]></keywords>' . "\r\n";
				}

				if(array_key_exists($brand, $this->_brand_descriptions) and $this->_brand_descriptions[$brand]['brand_logo']) {
					$xml .= "\t\t" . '<logo><![CDATA[' . APPLICATION_URL . $this->_brand_descriptions[$brand]['brand_logo'] . ']]></logo>' . "\r\n";
				}
				else
				{
					$xml .= "\t\t" . '<logo><![CDATA[]]></logo>' . "\r\n";
				}


				$xml .= "\t\t" . '<photo><![CDATA[]]></photo>' . "\r\n";

				if(array_key_exists($brand, $this->_brand_descriptions) and $this->_brand_descriptions[$brand]['brand_logo']) {
					$xml .= "\t\t" . '<squaredLogo><![CDATA[' . APPLICATION_URL . $this->_brand_descriptions[$brand]['brand_logo'] . ']]></squaredLogo>' . "\r\n";
				}
				else
				{
					$xml .= "\t\t" . '<squaredLogo><![CDATA[]]></squaredLogo>' . "\r\n";
				}
				
				$xml .= "\t\t" . '<specialOfferTitle><![CDATA[]]></specialOfferTitle>' . "\r\n";
				$xml .= "\t\t" . '<specialOfferDescription><![CDATA[]]></specialOfferDescription>' . "\r\n";
				$xml .= "\t\t" . '<specialOfferURL><![CDATA[]]></specialOfferURL>' . "\r\n";
				$xml .= "\t\t" . '<specialOfferDateStart><![CDATA[]]></specialOfferDateStart>' . "\r\n";
				$xml .= "\t\t" . '<specialOfferDateEnd><![CDATA[]]></specialOfferDateEnd>' . "\r\n";

				if(array_key_exists($brand, $this->_brand_descriptions)) {
					$xml .= "\t\t" . '<videoURL><![CDATA[' . $this->_brand_descriptions[$brand]['brand_video_url'] . ']]></videoURL>' . "\r\n";
					$xml .= "\t\t" . '<videoDescription><![CDATA[' . $this->_brand_descriptions[$brand]['brand_video_description'] . ']]></videoDescription>' . "\r\n";
				}
				else {
					$xml .= "\t\t" . '<videoURL><![CDATA[]]></videoURL>' . "\r\n";
					$xml .= "\t\t" . '<videoDescription><![CDATA[]]></videoDescription>' . "\r\n";
				}


				$social_profiles = $this->_get_Social_Profiles(36, $brand, $retailnet_country_id, $data["posaddress_id"]);


				if(count($social_profiles) > 0) {
					$xml .= "\t\t" . '<socialProfiles>' . "\r\n";
					foreach($social_profiles as $socialmedianame=>$url) {
						$xml .= "\t\t\t" . '<'. $socialmedianame . '><![CDATA[' . $url . ']]></' . $socialmedianame . '>' . "\r\n";
					}
					$xml .= "\t\t" . '</socialProfiles>' . "\r\n";
				}
				else {
					$xml .= "\t\t" . '<socialProfiles>' . "\r\n";
					$xml .= "\t\t" . '</socialProfiles>' . "\r\n";
				}


				$xml .= "\t\t" . '<brands><![CDATA[Tissot]]></brands>' . "\r\n";
				
				
				$xml .= "\t\t" . '<paymentMethods></paymentMethods>' . "\r\n";
				$xml .= "\t\t" . '<languages></languages>' . "\r\n";
				
				
				if(array_key_exists($data["posaddress_id"], $this->_customerservices) ){
					$xml .= "\t\t" . '<services><![CDATA['.$this->_customerservices[$data["posaddress_id"]] .']]></services>' . "\r\n";
				}
				else {
					$xml .= "\t\t" . '<services><![CDATA[]]></services>' . "\r\n";
				}

				$xml .= "\t\t" . '<LegalType><![CDATA[' . $data["posowner_type_name"] . ']]></LegalType>' . "\r\n";
				$xml .= "\t\t" . '<POStype><![CDATA[' . $data["postype_name"] . ']]></POStype>' . "\r\n";
				$xml .= "\t\t" . '<GogleLongitute><![CDATA[' . $data["posaddress_google_long"] . ']]></GogleLongitute>' . "\r\n";
				$xml .= "\t\t" . '<GogleLatitude><![CDATA[' . $data["posaddress_google_lat"] . ']]></GogleLatitude>' . "\r\n";
				$xml .= "\t\t" . '<isopen><![CDATA[' . $closed . ']]></isopen>' . "\r\n";



				$xml .= "\t" . '</poslocation>' . "\r\n";
			}
			
			$xml .= '</language>' . "\r\n";

			
			//get translated POS Data
			$languages = $this->_getLanguages($store_locator_country_id);
			foreach($languages as $language_id=>$iso_code)
			{
				
				$this->_get_Brand_Descriptions($language_id);
				$this->_get_string_translations($language_id);

				if(!array_key_exists($brand, $this->_brand_descriptions)){
					$this->_get_Brand_Descriptions(36);
				}
				
				if(array_key_exists($brand, $this->_brand_descriptions)){
					$xml .= '<language language_iso639_1="'. $iso_code . '">' . "\r\n";

					$this->_getOpeningHours($store_locator_country_id, $language_id);
					$this->_getCustomerServices($store_locator_country_id, $language_id);
					$translated_pos_locations = $this->_getPOSlocations($store_locator_country_id, $language_id);
									
					
					foreach($translated_pos_locations as $key=>$data)
					{	
						
						if(array_key_exists($data["posaddress_id"], $selected_pos_locations)) {
														
							$closed = $this->_check_if_pos_is_open_right_now($data["posaddress_id"]);
							
							$xml .= "\t" . '<poslocation identifier="P' . $data["posaddress_id"]. '">' . "\r\n";

							$xml .= "\t\t" . '<name><![CDATA[' . $data['posaddress_name'] .']]></name>' . "\r\n";
							
							
							$xml .= "\t\t" . '<address><![CDATA[' . $data['posaddress_address'] .']]></address>' . "\r\n";
							
							
							$xml .= "\t\t" . '<zip><![CDATA[' . $data['posaddress_zip'] .']]></zip>' . "\r\n";
							$xml .= "\t\t" . '<city><![CDATA[' . $data['place_name'] .']]></city>' . "\r\n";
							if(array_key_exists($brand, $this->_brand_descriptions)) {
								$xml .= "\t\t" . '<category><![CDATA[' . $this->_brand_descriptions[$brand]["brand_uberal_category_ids"] . ']]></category>' . "\r\n";
							}
							else {

								$xml .= "\t\t" . '<category><![CDATA[]]></category>' . "\r\n";
						
							}

							$xml .= "\t\t" . '<addressExtra><![CDATA[' . $data['posaddress_address2'] .']]></addressExtra>' . "\r\n";
							$xml .= "\t\t" . '<CIFNIF><![CDATA[]]></CIFNIF>' . "\r\n";
							$xml .= "\t\t" . '<SIREN><![CDATA[]]></SIREN>' . "\r\n";
							$xml .= "\t\t" . '<phone><![CDATA[' . $data['posaddress_phone'] .']]></phone>' . "\r\n";
							$xml .= "\t\t" . '<mobile_phone><![CDATA[' . $data['posaddress_mobile_phone'] .']]></mobile_phone>' . "\r\n";
							$xml .= "\t\t" . '<cellphone><![CDATA[]]></cellphone>' . "\r\n";
							$xml .= "\t\t" . '<website><![CDATA[' . $data['country_website_swatch'] .']]></website>' . "\r\n";
							$xml .= "\t\t" . '<email><![CDATA[' . $data['posaddress_email'] .']]></email>' . "\r\n";
							

							if(array_key_exists($brand, $this->_brand_descriptions)) {
								$xml .= "\t\t" . '<descriptionShort><![CDATA[' . $this->_brand_descriptions[$brand]["brand_short_description"] . ']]></descriptionShort>' . "\r\n";
								$xml .= "\t\t" . '<descriptionLong><![CDATA[' . $this->_brand_descriptions[$brand]["brand_long_desicrption"] . ']]></descriptionLong>' . "\r\n";
							}
							else {

								$xml .= "\t\t" . '<descriptionShort><![CDATA[]]></descriptionShort>' . "\r\n";
								$xml .= "\t\t" . '<descriptionLong><![CDATA[]]></descriptionLong>' . "\r\n";
							
							}
							
							
							if(array_key_exists($data["posaddress_id"], $this->_openinghours) 
							and array_key_exists('open', $this->_openinghours[$data["posaddress_id"]]))
						{
							$xml .= "\t\t" . '<openingHours><![CDATA[' . $this->_openinghours[$data["posaddress_id"]]['open']. ']]></openingHours>' . "\r\n";
						}
						else
						{
							$xml .= "\t\t" . '<openingHours><![CDATA[]]></openingHours>' . "\r\n";
						}

						if(array_key_exists($data["posaddress_id"], $this->_openinghours) 
							and array_key_exists('closed', $this->_openinghours[$data["posaddress_id"]]))
						{
							
							$cstring = '';
							if(array_key_exists('pos.closing.hours', $this->_string_translations))
							{
								$cstring = $this->_string_translations['pos.closing.hours'] . ": ";
							}
							
							$xml .= "\t\t" . '<openingHoursNotes><![CDATA[' . $cstring . $this->_openinghours[$data["posaddress_id"]]['closed']. ']]></openingHoursNotes>' . "\r\n";
						}
						else
						{
							$xml .= "\t\t" . '<openingHoursNotes><![CDATA[]]></openingHoursNotes>' . "\r\n";
						}

						if(array_key_exists($brand, $this->_brand_descriptions)) {
							$xml .= "\t\t" . '<keywords><![CDATA['. $this->_brand_descriptions[$brand]['brand_keywords'] . ']]></keywords>' . "\r\n";
						}
						else
						{
							$xml .= "\t\t" . '<keywords><![CDATA[]]></keywords>' . "\r\n";
						}

						if(array_key_exists($brand, $this->_brand_descriptions) and $this->_brand_descriptions[$brand]['brand_logo']) {
							$xml .= "\t\t" . '<logo><![CDATA[' . APPLICATION_URL . $this->_brand_descriptions[$brand]['brand_logo'] . ']]></logo>' . "\r\n";
						}
						else
						{
							$xml .= "\t\t" . '<logo><![CDATA[]]></logo>' . "\r\n";
						}


						$xml .= "\t\t" . '<photo><![CDATA[]]></photo>' . "\r\n";

						if(array_key_exists($brand, $this->_brand_descriptions) and $this->_brand_descriptions[$brand]['brand_logo']) {
							$xml .= "\t\t" . '<squaredLogo><![CDATA[' . APPLICATION_URL . $this->_brand_descriptions[$brand]['brand_logo'] . ']]></squaredLogo>' . "\r\n";
						}
						else
						{
							$xml .= "\t\t" . '<squaredLogo><![CDATA[]]></squaredLogo>' . "\r\n";
						}
						
						$xml .= "\t\t" . '<specialOfferTitle><![CDATA[]]></specialOfferTitle>' . "\r\n";
						$xml .= "\t\t" . '<specialOfferDescription><![CDATA[]]></specialOfferDescription>' . "\r\n";
						$xml .= "\t\t" . '<specialOfferURL><![CDATA[]]></specialOfferURL>' . "\r\n";
						$xml .= "\t\t" . '<specialOfferDateStart><![CDATA[]]></specialOfferDateStart>' . "\r\n";
						$xml .= "\t\t" . '<specialOfferDateEnd><![CDATA[]]></specialOfferDateEnd>' . "\r\n";

						if(array_key_exists($brand, $this->_brand_descriptions)) {
							$xml .= "\t\t" . '<videoURL><![CDATA[' . $this->_brand_descriptions[$brand]['brand_video_url'] . ']]></videoURL>' . "\r\n";
							$xml .= "\t\t" . '<videoDescription><![CDATA[' . $this->_brand_descriptions[$brand]['brand_video_description'] . ']]></videoDescription>' . "\r\n";
						}
						else {
							$xml .= "\t\t" . '<videoURL><![CDATA[]]></videoURL>' . "\r\n";
							$xml .= "\t\t" . '<videoDescription><![CDATA[]]></videoDescription>' . "\r\n";
						}

						$social_profiles = $this->_get_Social_Profiles($language_id, $brand, $selected_pos_locations[$data["posaddress_id"]], $data["posaddress_id"]);

						if(count($social_profiles) == 0) {
							$social_profiles = $this->_get_Social_Profiles(36, $brand, $selected_pos_locations[$data["posaddress_id"]], $data["posaddress_id"]);
						}



						if(count($social_profiles) > 0) {
							$xml .= "\t\t" . '<socialProfiles>' . "\r\n";
							foreach($social_profiles as $socialmedianame=>$url) {
								$xml .= "\t\t\t" . '<'. $socialmedianame . '><![CDATA[' . $url . ']]></' . $socialmedianame . '>' . "\r\n";
							}
							$xml .= "\t\t" . '</socialProfiles>' . "\r\n";
						}
						else {
							$xml .= "\t\t" . '<socialProfiles>' . "\r\n";
							$xml .= "\t\t" . '</socialProfiles>' . "\r\n";
						}

						$xml .= "\t\t" . '<brands><![CDATA[Tissot]]></brands>' . "\r\n";
						
						
						$xml .= "\t\t" . '<paymentMethods></paymentMethods>' . "\r\n";
						$xml .= "\t\t" . '<languages></languages>' . "\r\n";
						
						
						if(array_key_exists($data["posaddress_id"], $this->_customerservices) ){
							$xml .= "\t\t" . '<services><![CDATA['.$this->_customerservices[$data["posaddress_id"]] .']]></services>' . "\r\n";
						}
						else {
							$xml .= "\t\t" . '<services><![CDATA[]]></services>' . "\r\n";
						}

						
						$xml .= "\t\t" . '<LegalType><![CDATA[' . $data["posowner_type_name"] . ']]></LegalType>' . "\r\n";
						
						if(array_key_exists('loc_postype_name', $data)) {
							$xml .= "\t\t" . '<POStype><![CDATA[' . $data["loc_postype_name"] . ']]></POStype>' . "\r\n";
						}
						else {
							$xml .= "\t\t" . '<POStype><![CDATA[' . $data["postype_name"] . ']]></POStype>' . "\r\n";
						}

						$xml .= "\t\t" . '<GogleLongitute><![CDATA[' . $data["posaddress_google_long"] . ']]></GogleLongitute>' . "\r\n";
						$xml .= "\t\t" . '<GogleLatitude><![CDATA[' . $data["posaddress_google_lat"] . ']]></GogleLatitude>' . "\r\n";
						$xml .= "\t\t" . '<isopen><![CDATA[' . $closed . ']]></isopen>' . "\r\n";

						$postypes[$data['loc_postype_postype_id']] = $data["loc_postype_name"];




						$xml .= "\t" . '</poslocation>' . "\r\n";
						}
					
					}

					$xml .= '</language>' . "\r\n";
				}
			}

			$xml .= '</country>' . "\r\n";
		}

		$xml .= '</poslocations>';
		
		return $xml;
	
	}

		
	private function _getCountries($language_id = 36, $type)
	{
		$countries = array();
		if($language_id > 0 and $language_id != 36) {
			
			$db = new PDO("mysql:host=" . $this->_storelocator_db['server']. ";dbname=" . $this->_storelocator_db['db'] . ";charset=utf8mb4", $this->_storelocator_db['user'], $this->_storelocator_db['password']);
			
			$query = "SELECT loc_posaddresses.loc_posaddress_posaddress_id as posaddress_id, 
				loc_posaddresses.loc_posaddress_name as posaddress_name,
				loc_places.loc_place_name as place_name,
				loc_posaddresses.loc_posaddress_name2 as posaddress_name2,
				loc_posaddresses.loc_posaddress_address as posaddress_address, 
				loc_posaddresses.loc_posaddress_address2 as posaddress_address2,
				loc_posaddresses.loc_posaddress_zip as posaddress_zip,
				loc_posaddresses.loc_posaddress_phone as posaddress_phone,
				loc_posaddresses.loc_posaddress_mobile_phone as posaddress_mobile_phone,
				posaddresses.posaddress_website,
				posaddresses.posaddress_email
				FROM
				loc_posaddresses
				INNER JOIN posaddresses ON loc_posaddresses.loc_posaddress_posaddress_id = posaddresses.posaddress_id
				INNER JOIN loc_places ON posaddresses.posaddress_place_id = loc_places.loc_place_place_id
				where loc_posaddresses.loc_posaddress_country_id = ?
				and loc_posaddresses.loc_posaddress_language_id = ?
				and loc_places.loc_place_country_id = ?
				and loc_places.loc_place_language_id = ?";

			$stmt = $db->prepare($query);
			$stmt->execute(array($country_id, $language_id, $country_id, $language_id));

		}
		else
		{
			$db = new PDO("mysql:host=" . $this->_retailnet_db['server']. ";dbname=" . $this->_retailnet_db['db'] . ";charset=utf8mb4", $this->_retailnet_db['user'], $this->_retailnet_db['password']);
			
			$query = "SELECT DISTINCT posaddress_country, country_iso3166,
					 country_store_locator_id 
					 FROM posaddresses 
					 LEFT JOIN countries on country_id = posaddress_country
					 WHERE  posaddress_store_openingdate is not null
					 and posaddress_store_openingdate <> '0000-00-00'
					 and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00')
					 and posaddress_export_to_web = 1
					 order by posaddress_country";

			$stmt = $db->prepare($query);
			$stmt->execute();
		}
		
				
		
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$db = null;

		foreach($rows as $key=>$row) {
		
			$countries[$row["posaddress_country"]] = $row;
		}



		return $countries;
	}

	private function _getLanguages($country_id = 0)
	{
		$db = new PDO("mysql:host=" . $this->_storelocator_db['server']. ";dbname=" . $this->_storelocator_db['db'] . ";charset=utf8mb4", $this->_storelocator_db['user'], $this->_storelocator_db['password']);
		
		
		$query = "SELECT DISTINCT loc_posaddress_language_id, iso 
		         FROM loc_posaddresses
		         Left join language on language_id = loc_posaddress_language_id 
				 WHERE loc_posaddress_language_id <> 36 and loc_posaddress_country_id = ?"; 
		$stmt = $db->prepare($query);
		$stmt->execute(array($country_id));
		

		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$languages = array();
		foreach($rows as $key=>$data)
		{
			$languages[$data['loc_posaddress_language_id']] = $data["iso"];
		}
		
		$db = null;

		return $languages;
	}


	
	private function _getOpeningHours($country_id = 0, $language_id = 0)
	{
				
		//get opening hours
		if($language_id > 0) {

			$db = new PDO("mysql:host=" . $this->_storelocator_db['server']. ";dbname=" . $this->_storelocator_db['db'] . ";charset=utf8mb4", $this->_storelocator_db['user'], $this->_storelocator_db['password']);

			$query = "SELECT
						loc_posaddress_posaddress_id as pos_id,
						loc_weekday_name as weekdayname ,
						ofrom.loc_openinghr_time_24 as ofrom24,
						ofrom.loc_openinghr_time_12 as ofrom12,
						oto.loc_openinghr_time_24 as oto24,
						oto.loc_openinghr_time_12 as oto12
						FROM
						loc_posaddresses
						INNER JOIN posopeninghrs ON loc_posaddresses.loc_posaddress_posaddress_id = posopeninghr_posaddress_id
						INNER JOIN loc_weekdays ON posopeninghrs.posopeninghr_weekday_id = loc_weekdays.loc_weekday_weekday_id
						INNER JOIN loc_openinghrs as ofrom ON posopeninghr_from_openinghr_id = ofrom.loc_openinghr_openinghr_id
						INNER JOIN loc_openinghrs as oto ON posopeninghr_to_openinghr_id = oto.loc_openinghr_openinghr_id
						where loc_posaddress_country_id = ?
						and loc_posaddress_language_id = ? 
						and loc_weekday_language_id = ? 
						and ofrom.loc_openinghr_language_id = ?
						and oto.loc_openinghr_language_id = ?
					"; 
			$stmt = $db->prepare($query);
			$stmt->execute(array($country_id, $language_id, $language_id, $language_id, $language_id));
		}
		else {
			
			$db = new PDO("mysql:host=" . $this->_retailnet_db['server']. ";dbname=" . $this->_retailnet_db['db'] . ";charset=utf8mb4", $this->_retailnet_db['user'], $this->_retailnet_db['password']);

			$query = "SELECT
					posopeninghrs.posopeninghr_posaddress_id as pos_id,
					weekdays.weekday_name as weekdayname,
					ofrom.openinghr_time_24 as ofrom24,
					ofrom.openinghr_time_12 as ofrom12,
					oto.openinghr_time_24 as oto24,
					oto.openinghr_time_12 as oto12
					FROM
					posopeninghrs
					INNER join posaddresses on posaddress_id = posopeninghrs.posopeninghr_posaddress_id 
					INNER JOIN weekdays ON posopeninghrs.posopeninghr_weekday_id = weekdays.weekday_id
					INNER JOIN openinghrs as ofrom ON posopeninghrs.posopeninghr_from_openinghr_id = ofrom.openinghr_id
					INNER JOIN openinghrs as oto ON posopeninghrs.posopeninghr_to_openinghr_id = oto.openinghr_id
					where posaddress_country = ? 
					and posaddress_store_openingdate is not null
					and posaddress_store_openingdate <> '0000-00-00'
					and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00')
					and posaddress_export_to_web = 1
					order by posopeninghr_posaddress_id, weekday_id, ofrom.openinghr_time_24
					"; 
			
			$stmt = $db->prepare($query);
			$stmt->execute(array($country_id));
		}

		
		

		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		

		$openighrs = array();

		$old_pos_id = '';
		$tmp = array();
		foreach($rows as $key=>$data)
		{
			if($old_pos_id != $data['pos_id'])
			{
				if(count($tmp) > 0) {
					$openighrs[$old_pos_id]['open'] = implode(';', $tmp);
				}

				$tmp = array();
				$old_pos_id = $data['pos_id'];
			}
			
			if($language_id > 0)
			{
				$tmp[] = $data['weekdayname'] . '=' . $data['ofrom24'] . "-" .  $data['oto24'];
			}
			else
			{
				$tmp[] = substr($data['weekdayname'], 0, 2) . '=' . $data['ofrom24'] . "-" .  $data['oto24'];
			}
		}

		//last POS
		if(count($tmp) > 0) {
			$openighrs[$old_pos_id]['open'] = implode(';', $tmp);
		}



		//get closing hours

		if($language_id > 0) {
			$query = "SELECT loc_posaddresses.loc_posaddress_posaddress_id as pos_id, 
			            loc_posclosinghrs.loc_posclosinghr_text as posclosinghr_text
						FROM
						loc_posaddresses
						INNER JOIN loc_posclosinghrs ON loc_posaddresses.loc_posaddress_posaddress_id = loc_posclosinghrs.loc_posclosinghr_posaddress_id
						where loc_posaddress_country_id = ?
						and loc_posaddress_language_id = ?
						and loc_posclosinghr_language_id = ?

					";
			$stmt = $db->prepare($query);
			$stmt->execute(array($country_id, $language_id,  $language_id));
		}
		else
		{
			$query = "SELECT posclosinghr_posaddress_id as pos_id, posclosinghr_text
					FROM
					posclosinghrs
					INNER join posaddresses on posaddress_id = posclosinghr_posaddress_id 
					where posaddress_country = ? 
					and posaddress_store_openingdate is not null
					and posaddress_store_openingdate <> '0000-00-00'
					and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00')
					and posaddress_export_to_web = 1
					order by posclosinghr_posaddress_id
					";
			$stmt = $db->prepare($query);
			$stmt->execute(array($country_id));
		}



		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$old_pos_id = '';
		$tmp = array();
		foreach($rows as $key=>$data)
		{
			if($old_pos_id != $data['pos_id'])
			{
				if(count($tmp) > 0) {
					$openighrs[$old_pos_id]['closed'] = implode(';', $tmp);
				}

				$tmp = array();
				$old_pos_id = $data['pos_id'];
			}
			
			$tmp[] = $data['posclosinghr_text'];
		}

		//last pos
		if(count($tmp) > 0) {
			
			if(array_key_exists('pos.closing.hours', $this->_string_translations)) {
				
				$openighrs[$old_pos_id]['closed'] = $this->_string_translations['pos.closing.hours'] . ": " . implode(';', $tmp);
			}
			else
			{
				$openighrs[$old_pos_id]['closed'] = implode(';', $tmp);
			}
			
			
		}

		
		$this->_openinghours = $openighrs;
		$db = null;
	}



	private function _getCustomerServices($country_id = 0, $language_id = 0)
	{
		
		
		
		//get customer services
		if($language_id > 0) {

			$db = new PDO("mysql:host=" . $this->_storelocator_db['server']. ";dbname=" . $this->_storelocator_db['db'] . ";charset=utf8mb4", $this->_storelocator_db['user'], $this->_storelocator_db['password']);

			$query = "SELECT loc_posaddresses.loc_posaddress_posaddress_id as pos_id,
						loc_customerservices.loc_customerservice_text as customerservice_text
						FROM
						loc_posaddresses
						INNER JOIN posaddress_customerservices ON loc_posaddresses.loc_posaddress_posaddress_id = posaddress_customerservices.posaddress_customerservice_posaddress_id
						INNER JOIN loc_customerservices ON posaddress_customerservices.posaddress_customerservice_customerservice_id = loc_customerservices.loc_customerservice_customerservice_id
						where loc_posaddresses.loc_posaddress_country_id = ?
						and loc_posaddresses.loc_posaddress_language_id = ?
						and loc_customerservices.loc_customerservice_language_id = ?
					";
			$stmt = $db->prepare($query);
			$stmt->execute(array($country_id, $language_id, $language_id));
		}
		else
		{
			$db = new PDO("mysql:host=" . $this->_retailnet_db['server']. ";dbname=" . $this->_retailnet_db['db'] . ";charset=utf8mb4", $this->_retailnet_db['user'], $this->_retailnet_db['password']);

			$query = "SELECT posaddress_customerservice_posaddress_id as pos_id, customerservice_text
						FROM
						posaddress_customerservices
						INNER join posaddresses on posaddress_id = posaddress_customerservice_posaddress_id 
						INNER JOIN customerservices ON posaddress_customerservices.posaddress_customerservice_customerservice_id = customerservices.customerservice_id
						where posaddress_country = ? 
						and posaddress_store_openingdate is not null
					    and posaddress_store_openingdate <> '0000-00-00'
					    and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00')
					    and posaddress_export_to_web = 1
						order by posaddress_customerservice_posaddress_id, customerservice_text
					"; 
			$stmt = $db->prepare($query);
			$stmt->execute(array($country_id));
		}
		
			
		

		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$customerservices = array();

		$old_pos_id = '';
		$tmp = array();
		foreach($rows as $key=>$data)
		{
			if($old_pos_id != $data['pos_id'])
			{
				if(count($tmp) > 0) {
					$customerservices[$old_pos_id] = implode(';', $tmp);
				}

				$tmp = array();
				$old_pos_id = $data['pos_id'];
			}
			
			$tmp[] = $data['customerservice_text'];
		}

		//last POS
		if(count($tmp) > 0) {
			$customerservices[$old_pos_id] = implode(';', $tmp);
		}

		$this->_customerservices = $customerservices;
		$db = null;
	}


	private function _getPOSlocations($country_id = 0, $language_id = 36, $type = 'all')
	{
		
		if($language_id > 0 and $language_id != 36) {
			
			$db = new PDO("mysql:host=" . $this->_storelocator_db['server']. ";dbname=" . $this->_storelocator_db['db'] . ";charset=utf8mb4", $this->_storelocator_db['user'], $this->_storelocator_db['password']);

			$query = "SELECT loc_posaddresses.loc_posaddress_posaddress_id as posaddress_id, 
				loc_posaddresses.loc_posaddress_name as posaddress_name,
				loc_places.loc_place_name as place_name,
				loc_posaddresses.loc_posaddress_name2 as posaddress_name2,
				loc_posaddresses.loc_posaddress_address as posaddress_address, 
				loc_posaddresses.loc_posaddress_address2 as posaddress_address2,
				loc_posaddresses.loc_posaddress_zip as posaddress_zip,
				loc_posaddresses.loc_posaddress_phone as posaddress_phone,
				loc_posaddresses.loc_posaddress_mobile_phone as posaddress_mobile_phone,
				posaddresses.posaddress_website,
				posaddresses.posaddress_email, loc_postype_name
				FROM
				loc_posaddresses
				INNER JOIN posaddresses ON loc_posaddresses.loc_posaddress_posaddress_id = posaddresses.posaddress_id
				INNER JOIN loc_places ON posaddresses.posaddress_place_id = loc_places.loc_place_place_id
				LEFT JOIN loc_postypes on loc_postype_id =  posaddress_store_postype 
				where loc_posaddresses.loc_posaddress_country_id = ?
				and loc_posaddresses.loc_posaddress_language_id = ?
				and loc_places.loc_place_country_id = ?
				and loc_places.loc_place_language_id = ?";

			$stmt = $db->prepare($query);
			$stmt->execute(array($country_id, $language_id, $country_id, $language_id));

			$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

			
			$db = new PDO("mysql:host=" . $this->_retailnet_db['server']. ";dbname=" . $this->_retailnet_db['db'] . ";charset=utf8mb4", $this->_retailnet_db['user'], $this->_retailnet_db['password']);

			foreach($rows as $key=>$data)
			{
				$query = "SELECT posaddress_google_lat, posaddress_google_long, postype_name, posowner_type_name
				         FROM posaddresses 
						 LEFT JOIN postypes on postype_id = posaddress_store_postype
						 LEFT JOIN posowner_types on posowner_type_id = posaddress_ownertype
						 LEFT JOIN countries on country_id = posaddress_country
						 WHERE posaddress_id = ?";

				$stmt = $db->prepare($query);
				$stmt->execute(array($data['posaddress_id']));

				$row2 = $stmt->fetchAll(PDO::FETCH_ASSOC);

				if(count($row2) > 0) {
					$rows[$key]['postype_name'] = $row2[0]['postype_name'];
					$rows[$key]['posowner_type_name'] = $row2[0]['posowner_type_name'];
					$rows[$key]['posaddress_google_lat'] = $row2[0]['posaddress_google_lat'];
					$rows[$key]['posaddress_google_long'] = $row2[0]['posaddress_google_long'];
					$rows[$key]['country_website_swatch'] = $row2[0]['country_website_swatch'];
				}
			
			
			}


		}
		else
		{
			$db = new PDO("mysql:host=" . $this->_retailnet_db['server']. ";dbname=" . $this->_retailnet_db['db'] . ";charset=utf8mb4", $this->_retailnet_db['user'], $this->_retailnet_db['password']);

			
			$query = "SELECT * FROM posaddresses 
					 LEFT JOIN places on place_id = posaddress_place_id 
					 LEFT JOIN postypes on postype_id = posaddress_store_postype
					 LEFT JOIN posowner_types on posowner_type_id = posaddress_ownertype
					 LEFT JOIN countries on country_id = posaddress_country
					 WHERE posaddress_country=? 
					 and posaddress_store_openingdate is not null
					 and posaddress_store_openingdate <> '0000-00-00'
					 and (posaddress_store_closingdate is null or posaddress_store_closingdate = '0000-00-00')
					 and posaddress_export_to_web = 1
					 order by posaddress_country, place_name, posaddress_name";
			
			$stmt = $db->prepare($query);
			$stmt->execute(array($country_id));

			$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}
		

		$db = null;

		return $rows;
	}



	
	
	private function _get_Brand_Descriptions($language_id = 36 ) {
	
		$data = array();

		$db = new PDO("mysql:host=" . $this->_retailnet_db['server']. ";dbname=" . $this->_retailnet_db['db'] . ";charset=utf8mb4", $this->_retailnet_db['user'], $this->_retailnet_db['password']);


		$query = "SELECT * FROM brands where brand_language_id = ?";

		$stmt = $db->prepare($query);
		$stmt->execute(array($language_id));

		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$db = null;

		foreach($rows as $key=>$row)
		{
			$data[$row["brand_name"]] = $row;
		}

		$this->_brand_descriptions = $data;


	}


	private function _get_Social_Profiles($language_id = 36, $brand_name = '', $retailnet_country_id = 0, $pos_id = 0) {
		
		$data = array();

		$db = new PDO("mysql:host=" . $this->_retailnet_db['server']. ";dbname=" . $this->_retailnet_db['db'] . ";charset=utf8mb4", $this->_retailnet_db['user'], $this->_retailnet_db['password']);


		$query = "SELECT * FROM socialmedias 
		          LEFT JOIN socialmediachannels on socialmediachannel_id = socialmedia_channel_id
				  LEFT JOIN brands on brand_id = socialmedia_brand_id 
		          where socialmedia_language_id = ? 
				  and brand_name = ?
				  and socialmedia_country_id = ?
				  and socialmedia_posaddress_id = ? 
				  order by socialmediachannel_name ";

		$stmt = $db->prepare($query);
		$stmt->execute(array($language_id, $brand_name, $retailnet_country_id, $pos_id));

		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach($rows as $key=>$row)
		{
			if($row["socialmedia_url"]) {
				$data[$row["socialmediachannel_name"]] = $row["socialmedia_url"];
			}
			elseif($row["socialmedia_video_url"]) {
				$data[$row["socialmediachannel_name"]] = $row["socialmedia_video_url"];
			}
		}


		
		$query = "SELECT * FROM socialmedias 
			      LEFT JOIN socialmediachannels on socialmediachannel_id = socialmedia_channel_id
				  LEFT JOIN brands on brand_id = socialmedia_brand_id
				  where socialmedia_language_id = ? 
				  and brand_name = ?
				  and socialmedia_country_id = ?
				  and (socialmedia_posaddress_id is null or socialmedia_posaddress_id = '' or socialmedia_posaddress_id = 0)
				  order by socialmediachannel_name ";

		$stmt = $db->prepare($query);
		$stmt->execute(array($language_id, $brand_name, $retailnet_country_id));

		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach($rows as $key=>$row)
		{
			if(!array_key_exists($row["socialmediachannel_name"], $data)){
				if($row["socialmedia_url"]) {
					$data[$row["socialmediachannel_name"]] = $row["socialmedia_url"];
				}
				elseif($row["socialmedia_video_url"]) {
					$data[$row["socialmediachannel_name"]] = $row["socialmedia_video_url"];
				}
			}
		}

		
		$query = "SELECT * FROM socialmedias 
			      LEFT JOIN socialmediachannels on socialmediachannel_id = socialmedia_channel_id
				  LEFT JOIN brands on brand_id = socialmedia_brand_id
				  where socialmedia_language_id = ? 
				  and brand_name = ?
				  and (socialmedia_country_id is null or socialmedia_country_id = '' or socialmedia_country_id = 0)
				  and (socialmedia_posaddress_id is null or socialmedia_posaddress_id = '' or socialmedia_posaddress_id = 0)
				  order by socialmediachannel_name ";

		$stmt = $db->prepare($query);
		$stmt->execute(array($language_id, $brand_name));

		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach($rows as $key=>$row)
		{
			if(!array_key_exists($row["socialmediachannel_name"], $data)){
				if($row["socialmedia_url"]) {
					$data[$row["socialmediachannel_name"]] = $row["socialmedia_url"];
				}
				elseif($row["socialmedia_video_url"]) {
					$data[$row["socialmediachannel_name"]] = $row["socialmedia_video_url"];
				}
			}
		}

		return $data;
	}

	private function _check_if_pos_is_open_right_now($posaddress_id = 0) {
		
		$db = new PDO("mysql:host=" . $this->_retailnet_db['server']. ";dbname=" . $this->_retailnet_db['db'] . ";charset=utf8mb4", $this->_retailnet_db['user'], $this->_retailnet_db['password']);


		$query = "SELECT posclosing_id as num_recs 
				from posclosings
		          where 
				  posclosing_startdate <= ?
				  and posclosing_enddate >= ?
				  and posclosing_posaddress = ?";

		$stmt = $db->prepare($query);
		$stmt->execute(array(date("Y-m-d"), date("Y-m-d"), $posaddress_id));

		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$db = null;

		if(count($rows) > 0)
		{
			return 'no';
		}

		return 'yes';

	}
	
	
	private function _get_string_translations($language_id = 36) {
		
		$data = array();

		$db = new PDO("mysql:host=" . $this->_retailnet_db['server']. ";dbname=" . $this->_retailnet_db['db'] . ";charset=utf8mb4", $this->_retailnet_db['user'], $this->_retailnet_db['password']);


		$query = "SELECT string_translation_keyword, string_translation_text FROM string_translations 
		          where string_translation_language_id = ?";

		$stmt = $db->prepare($query);
		$stmt->execute(array($language_id));

		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$db = null;

		foreach($rows as $key=>$row)
		{
			$data[$row["string_translation_keyword"]] = $row["string_translation_text"];
		}

		if(count($data) == 0)
		{
			$this->_get_string_translations(36);
		}

		$this->_string_translations = $data;
	}
}